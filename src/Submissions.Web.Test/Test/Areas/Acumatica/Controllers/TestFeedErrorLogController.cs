﻿using EF.Submission;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Submissions.Web.Areas.Acumatica.Models;

namespace Submissions.Web.Test.Test.Areas.Acumatica.Controllers
{
	[TestClass]
	public class TestFeedErrorLogController
	{
		private SubmissionContext _submissionContext;
		private string _defaultAcumaticaException;

		[TestInitialize]
		public void Initialize()
		{
			_defaultAcumaticaException = "{\"Message\":\"An error has occurred.\",\"ExceptionMessage\":\"PX.Data.PXException: Error: 'Parent Project ID' cannot be found in the system.\\r\\n ---> PX.Data.PXOuterException: Error: Inserting  'Project' record raised at least one error. Please review the errors.\\r\\n   at PX.Data.PXUIFieldAttribute.CommandPreparing(PXCache sender, PXCommandPreparingEventArgs e)\\r\\n   at PX.Data.PXCache.OnCommandPreparing(String name, Object row, Object value, PXDBOperation operation, Type table, FieldDescription& description)\\r\\n   at PX.Data.PXCache`1.PersistInserted(Object row, Boolean bypassInterceptor)\\r\\n   at PX.Data.PXCache`1.Persist(PXDBOperation operation)\\r\\n   at PX.Data.PXGraph.Persist(Type cacheType, PXDBOperation operation)\\r\\n   at PX.Data.PXGraph.Persist()\\r\\n   at PX.Objects.PM.ProjectEntry.Persist()\\r\\n   at PX.Data.PXSave`1.<Handler>d__2.MoveNext()\\r\\n   at PX.Data.PXAction`1.<Press>d__32.MoveNext()\\r\\n   at PX.Data.PXAction`1.<Press>d__32.MoveNext()\\r\\n   at PX.Api.SyImportProcessor.SyStep.a(Object A_0, PXFilterRow[] A_1, PXFilterRow[] A_2)\\r\\n   at PX.Api.SyImportProcessor.ExportTableHelper.ExportTable()\\r\\n   --- End of inner exception stack trace ---\\nProject.GLIProperties.ConsolidationProjectId: Error: 'Parent Project ID' cannot be found in the system.\"}";
			SubmissionContext.dbType = SubmissionContext.Environment.Development;
			_submissionContext = new SubmissionContext();
		}

		[TestMethod]
		public void ParseAndSetException_Target_ParentProjectID_DoesntExistError_TargetIsParentProjectID()
		{
			// Arrange
			var result = new AcumaticaException();
			result.RawException = _defaultAcumaticaException;

			// Act
			result.ParseAndSetException();

			//Assert
			Assert.AreEqual("ParentProjectID", result.Target);
		}

		[TestMethod]
		public void ParseAndSetException_Target_ParentProjectID_DoesntExistErrorAlternateFormat_TargetIsParentProjectID()
		{
			// Arrange
			var result = new AcumaticaException();
			result.RawException = "{\"Message\":\"An error has occurred.\",\"ExceptionMessage\":\"Project.GLIProperties.ConsolidationProjectId: 'Parent Project ID' cannot be found in the system.\\nInserting  'Project' record raised at least one error. Please review the errors.\"}"; ;

			// Act
			result.ParseAndSetException();

			//Assert
			Assert.AreEqual("ParentProjectID", result.Target);
		}


	}
}
