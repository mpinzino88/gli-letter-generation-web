﻿using EF.Submission;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Submissions.Common;
using Submissions.Common.Email;
using Submissions.Core;
using Submissions.Web.Areas.Protrack.Controllers;
using Submissions.Web.Areas.Protrack.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Web.Mvc;

namespace Submissions.Web.Test
{
	[TestClass]
	public class TestTargetDateController
	{
		private TargetDateController _targetDateController;

		private readonly Mock<ISubmissionContext> _mockDbSubmission = new Mock<ISubmissionContext>();
		private readonly Mock<IUserContext> _mockUserContext = new Mock<IUserContext>();
		private readonly Mock<IEmailService> _mockEmailService = new Mock<IEmailService>();
		private readonly Mock<IProjectService> _mockProjectService = new Mock<IProjectService>();
		private readonly Mock<INotificationService> _mockNotificationService = new Mock<INotificationService>();

		private Mock<DbSet<trProject>> _mockTrProjectDbSet;

		private static readonly string BatchTargetViewName = "~/Areas/Protrack/Views/TargetDate/_BatchTargetDate.cshtml";
		private static readonly string CannotUpdateViewName = "~/Areas/Protrack/Views/TargetDate/_CannotUpdate.cshtml";

		[TestInitialize]
		public void SetUp()
		{
			_targetDateController = new TargetDateController(_mockDbSubmission.Object, _mockUserContext.Object, _mockProjectService.Object, _mockNotificationService.Object, _mockEmailService.Object);
		}

		[TestMethod]
		public void BatchOriginalTargetDate_AllProjectsEligibleForOrginalDateUpdate_ReturnsPartialView()
		{
			SetUpTwoOriginalTargetUpdateEligibleProjects();

			var result = _targetDateController.BatchOriginalTargetDate("1,2");

			Assert.IsInstanceOfType(result, typeof(PartialViewResult));
			var partialView = (PartialViewResult)result;
		}

		[TestMethod]
		public void BatchOriginalTargetDate_AllProjectsEligibleForOrginalDateUpdate_ReturnsPartialViewWithUpdateViewViewName()
		{
			SetUpTwoOriginalTargetUpdateEligibleProjects();

			var result = (PartialViewResult)_targetDateController.BatchOriginalTargetDate("1,2");

			Assert.AreEqual(BatchTargetViewName, result.ViewName);
		}

		[TestMethod]
		public void BatchOriginalTargetDate_AllProjectsEligibleForOrginalDateUpdate_ReturnsPartialViewWithBatchTargetProjectModel()
		{
			SetUpTwoOriginalTargetUpdateEligibleProjects();

			var result = (PartialViewResult)_targetDateController.BatchOriginalTargetDate("1,2");

			Assert.IsInstanceOfType(result.Model, typeof(BatchProjectTarget));
		}

		[TestMethod]
		public void BatchOriginalTargetDate_AllProjectsEligibleForOrginalDateUpdate_ReturnsPartialViewWithExpectedBatchTargetProjectModel()
		{
			SetUpTwoOriginalTargetUpdateEligibleProjects();

			var result = (PartialViewResult)_targetDateController.BatchOriginalTargetDate("1,2");
			var batchTargetProject = (BatchProjectTarget)result.Model;

			Assert.AreEqual("1,2", batchTargetProject.ProjectIds);
			Assert.AreEqual(TargetDateReviseMode.ModifyOriginalTarget, batchTargetProject.ReviseMode);
			Assert.AreEqual(1, batchTargetProject.ApplyToProject);
			Assert.AreEqual("Other", batchTargetProject.Reason);
			Assert.AreEqual("", batchTargetProject.Detail);
		}

		[TestMethod]
		public void BatchOriginalTargetDate_AnIneligibleAndEligibleProjectForOriginalDateUpdate_ReturnsPartialView()
		{
			SetUpOriginalTargetUpdateIneligibleAndEligibleProjects();

			var result = _targetDateController.BatchOriginalTargetDate("1,2");

			Assert.IsInstanceOfType(result, typeof(PartialViewResult));
		}

		[TestMethod]
		public void BatchOriginalTargetDate_AnIneligibleAndEligibleProjectForOriginalDateUpdate_ReturnsPartialViewWithCannotUpdateViewViewName()
		{
			SetUpOriginalTargetUpdateIneligibleAndEligibleProjects();

			var result = (PartialViewResult)_targetDateController.BatchOriginalTargetDate("1,2");

			Assert.AreEqual(CannotUpdateViewName, result.ViewName);
		}

		[TestMethod]
		public void BatchOriginalTargetDate_AnIneligibleAndEligibleProjectForOriginalDateUpdate_ReturnsOffendingFileNumber()
		{
			var ineligibleProjectFileNumbers = SetUpOriginalTargetUpdateIneligibleAndEligibleProjects();

			var result = (PartialViewResult)_targetDateController.BatchOriginalTargetDate("1,2");

			Assert.AreEqual(ineligibleProjectFileNumbers, result.Model);
		}

		[TestMethod]
		public void BatchOriginalTargetDate_IneligibleProjectsForOriginalDateUpdate_ReturnsCannotUpdateView()
		{
			SetUpOriginalTargetUpdateIneligibleProjects();

			var result = (PartialViewResult)_targetDateController.BatchOriginalTargetDate("1,2");

			Assert.AreEqual(CannotUpdateViewName, result.ViewName);
		}

		[TestMethod]
		public void BatchOriginalTargetDate_IneligibleProjectsForOriginalDateUpdate_ReturnsFileNumbers()
		{
			var ineligibleProjectFileNumbers = SetUpOriginalTargetUpdateIneligibleProjects();

			var result = (PartialViewResult)_targetDateController.BatchOriginalTargetDate("1,2");

			Assert.AreEqual(ineligibleProjectFileNumbers, result.Model);
		}

		[TestMethod]
		public void BatchOriginalTargetDate_NullENAcceptDate_ReturnsPartialViewWithCannotUpdateViewViewName()
		{
			SetUpTrProjectData(new List<trProject>()
			{
				new trProject
				{
					Id = 1,
					FileNumber = "Test1",
					ENAcceptDate = null
				}
			});

			var result = (PartialViewResult)_targetDateController.BatchOriginalTargetDate("1");

			Assert.AreEqual(CannotUpdateViewName, result.ViewName);
		}

		private void SetUpTwoOriginalTargetUpdateEligibleProjects()
		{
			SetUpTrProjectData(new List<trProject>
			{
				AddOriginalTargetUpdateEligibleProject(1, "Test1"),
				AddOriginalTargetUpdateEligibleProject(2, "Test2"),
			});
		}

		private string SetUpOriginalTargetUpdateIneligibleProjects()
		{
			var ineligibleProjectFileNumberOne = "Test1";
			var ineligibleProjectFileNumberTwo = "Test2";
			SetUpTrProjectData(new List<trProject>
			{
				AddOriginalTargetUpdateIneligibleProject(1, ineligibleProjectFileNumberOne),
				AddOriginalTargetUpdateIneligibleProject(2, ineligibleProjectFileNumberTwo)
			});
			return ineligibleProjectFileNumberOne + "\n" + ineligibleProjectFileNumberTwo;
		}

		private string SetUpOriginalTargetUpdateIneligibleAndEligibleProjects()
		{
			var ineligibleProjectFileNumber = "Test1";
			SetUpTrProjectData(new List<trProject>
			{
				AddOriginalTargetUpdateIneligibleProject(1, ineligibleProjectFileNumber),
				AddOriginalTargetUpdateEligibleProject(2, "Test2")
			});
			return ineligibleProjectFileNumber;
		}

		private trProject AddOriginalTargetUpdateIneligibleProject(int id, string fileNumber)
		{
			return new trProject
			{
				Id = id,
				FileNumber = fileNumber,
				ENAcceptDate = DateTime.Now.Date.AddDays(-3).AddMilliseconds(-1)
			};
		}

		private trProject AddOriginalTargetUpdateEligibleProject(int id, string fileNumber)
		{
			return new trProject
			{
				Id = id,
				FileNumber = fileNumber,
				ENAcceptDate = DateTime.Now
			};
		}

		private void SetUpTrProjectData(IList<trProject> trProjects)
		{
			_mockTrProjectDbSet = new Mock<DbSet<trProject>>().SetupData(trProjects);
			_mockDbSubmission.Setup(x => x.trProjects).Returns(_mockTrProjectDbSet.Object);
		}
	}
}
