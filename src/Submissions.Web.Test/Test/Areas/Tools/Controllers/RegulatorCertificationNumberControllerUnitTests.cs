﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using EF.GPS;
using EF.Submission;
using Submissions.Common;
using Submissions.Web.Areas.Tools.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using Submissions.Web.Areas.Tools.Controllers;

namespace Submissions.Web.Test
{
	[TestClass]
	public class RegulatorCertificationNumberControllerUnitTests
	{
		[TestMethod]
		public void PreCertUploadTest()
		{
			using (var subContext = new SubmissionContext())
			{
				var regController = new Submissions.Web.Areas.Tools.Controllers.RegulatorCertificationNumberController(subContext, null);

				//The function call is private, so for the time being I set it to public, so if you wanna test this out, do that and run this!
				//regController.UploadToGPS(@"C:\Users\michael_g\Documents\TestExport.txt", "MO-73-IGT-20-27", "42");
			}
		}
	}
}
