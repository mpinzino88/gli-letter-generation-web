﻿using EF.Submission;
using EF.Util;
using Submissions.Common;
using System;
using System.Data;
using System.Data.Common;
using System.Data.Entity.Infrastructure.Interception;
using System.Linq;

namespace Submissions.Web.Utility
{
	public class DbInterceptor : IDbCommandInterceptor
	{
		public void NonQueryExecuted(DbCommand command, DbCommandInterceptionContext<int> interceptionContext)
		{
		}

		public void NonQueryExecuting(DbCommand command, DbCommandInterceptionContext<int> interceptionContext)
		{
			SetCommandText(command, interceptionContext);
		}

		public void ReaderExecuted(DbCommand command, DbCommandInterceptionContext<DbDataReader> interceptionContext)
		{
		}

		public void ReaderExecuting(DbCommand command, DbCommandInterceptionContext<DbDataReader> interceptionContext)
		{
			SetCommandText(command, interceptionContext);
		}

		public void ScalarExecuted(DbCommand command, DbCommandInterceptionContext<object> interceptionContext)
		{
		}

		public void ScalarExecuting(DbCommand command, DbCommandInterceptionContext<object> interceptionContext)
		{
			SetCommandText(command, interceptionContext);
		}

		private static void SetCommandText<T>(IDbCommand command, DbCommandInterceptionContext<T> interceptionContext)
		{
			command.CommandText = command.CommandText.Trim();
			if (command.CommandText.StartsWith("SELECT", StringComparison.OrdinalIgnoreCase))
			{
				var cmdText = string.Format("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;{0}{1}", System.Environment.NewLine, command.CommandText);

				var dbContext = interceptionContext.DbContexts.OfType<IDBContext>().SingleOrDefault();

				if (dbContext != null)
				{
					if (dbContext.OptionRecompile)
					{
						cmdText += string.Format("{0} OPTION(RECOMPILE)", System.Environment.NewLine);
					}
				}

				command.CommandText = cmdText;
			}
			else if (command.CommandText.StartsWith("INSERT", StringComparison.OrdinalIgnoreCase) || command.CommandText.StartsWith("UPDATE", StringComparison.OrdinalIgnoreCase) || command.CommandText.Contains("DELETE", StringComparison.OrdinalIgnoreCase))
			{
				// Entity Framework Plus may add this as a subquery so we need to remove it
				command.CommandText = command.CommandText.Replace("SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;", "");

				var dbSubmissionContext = interceptionContext.DbContexts.OfType<SubmissionContext>().SingleOrDefault();

				if (dbSubmissionContext != null && dbSubmissionContext.UserId != null)
				{
					command.CommandText = string.Format("EXEC devin_setcontext {0};{1}{2}", dbSubmissionContext.UserId, System.Environment.NewLine, command.CommandText);
				}
			}
		}
	}
}