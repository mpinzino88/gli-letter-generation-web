﻿using AutoMapper;
using EF.GLiCloud;
using Submissions.Common;
using Submissions.Web.Areas.Lookups.Models;

namespace Submissions.Web.Utility.Email
{
	public class EmailProfile : Profile
	{
		public EmailProfile()
		{
			CreateMap<GLIVerifyLicenseGroup, VerifyGroupLicenseEmail>();
			CreateMap<JurisdictionMappingEditViewModel, EditJurisdictionMappingEmail>()
				.ForMember(dest => dest.jurisdictionType, opt => opt.Ignore());
			CreateMap<Areas.Protrack.Models.ProjectReview.Review, ProjectReviewEmail>()
				.ForMember(dest => dest.Link, opt => opt.Ignore())
				.ForMember(dest => dest.Note, opt => opt.Ignore())
				.ForMember(dest => dest.Header, opt => opt.Ignore())
				.ForMember(dest => dest.AssigneeNames, opt => opt.Ignore())
				.ForMember(dest => dest.ReviewerNames, opt => opt.Ignore())
				.ForMember(dest => dest.LetterwriterNames, opt => opt.Ignore())
				.ForMember(dest => dest.EmailToReviewer, scr => scr.MapFrom(x => x.Status == ProjectReviewStatus.VerificationPending.ToString() ? true : false))
				.ForMember(dest => dest.EmailToAssignee, scr => scr.MapFrom(x => x.Status == ProjectReviewStatus.CorrectionsPending.ToString() ? true : false))
				.ForMember(dest => dest.IsDev, opt => opt.Ignore())
				.ForMember(dest => dest.EmailAddressForTesting, opt => opt.Ignore());
		}
	}
}