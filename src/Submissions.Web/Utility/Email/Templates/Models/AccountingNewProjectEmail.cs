﻿namespace Submissions.Web.Utility.Email
{
	public class AccountingNewProjectEmail
	{
		public string FileNumber { get; set; }
		public string Company { get; set; }
		public string ContractType { get; set; }
		public string Currency { get; set; }
		public string WON { get; set; }
		public string ContractNumber { get; set; }
		public string GameName { get; set; }
		public string Memo { get; set; }
	}
}