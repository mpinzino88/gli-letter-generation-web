﻿using System;
using System.Collections.Generic;

namespace Submissions.Web.Utility.Email
{
    public class AddNewSubmissionJurisdictionsEmail
    {
        public string FileNumber { get; set; }
        public List<JurisdictionAdd> Jurisdictions { get; set; }
      
    }

    public class JurisdictionAdd
    {
        public string IdNumber { get; set; }
        public string GameName { get; set; }
        public string DateCode { get; set; }
        public string Version { get; set; }
        public string JurisdictionName { get; set; }
        public string JurisdictionId { get; set; }
    }
}