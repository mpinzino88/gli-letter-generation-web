﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Submissions.Web.Utility.Email
{
	public class PointClickWithdrawEmail
	{
		public PointClickWithdrawEmail()
		{
			RequestItems = new List<PointClickWithdrawEmailItem>();
		}

		public string ActionName { get; set; }
		public string ProcessorEmail { get; set; }
		public string ProccessorName { get; set; }
		public string RequestorEmail { get; set; }
		public string RequestiorName { get; set; }
		public IList<PointClickWithdrawEmailItem> RequestItems { get; set; }
	}

	public class PointClickWithdrawEmailItem
	{
		public string FileNumber { get; set; }
		public string Manufacturer { get; set; }
		public string IDNumber { get; set; }
		public string GameName { get; set; }
		public string Version { get; set; }
		public string Function { get; set; }
		public string Position { get; set; }
		public string Jurisdiction { get; set; }
	}
}