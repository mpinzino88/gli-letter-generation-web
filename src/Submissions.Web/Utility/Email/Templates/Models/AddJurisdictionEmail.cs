﻿using System.Collections.Generic;
namespace Submissions.Web.Utility.Email
{
	public class AddJurisdictionEmail
	{
		public string Id { get; set; }
		public string Jurisdiction { get; set; }
		public string jurisdictionType { get; set; }
		public string QMSLink { get; set; }
		public List<string> pendingManufacturerRequests { get; set; }
		public List<string> pendingLabsForBilling { get; set; }
	}
}