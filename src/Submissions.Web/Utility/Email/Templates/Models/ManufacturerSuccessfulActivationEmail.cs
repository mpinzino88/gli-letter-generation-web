﻿using System;

namespace Submissions.Web.Utility.Email.Templates.Models
{
	public class ManufacturerSuccessfulActivationEmail
	{
		public string ManufacturerCode { get; set; }
		public string ManufacturerDescription { get; set; }
		public int? ProductSubmitterListItemId { get; set; }
		public Uri ProductSubmittersListItemUri { get; set; }
	}
}
