﻿using System.Collections.Generic;

namespace Submissions.Web.Utility.Email
{
	public class IGTFlatFileBadColumnsEmail
	{
		public IGTFlatFileBadColumnsEmail()
		{
			Studios = new List<string>();
			BillingParties = new List<string>();
		}

		public List<string> Studios { get; set; }
		public string Filename { get; set; }
		public List<string> BillingParties { get; set; }
	}
}