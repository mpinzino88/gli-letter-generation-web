﻿using Submissions.Web.Models;
using System.Collections.Generic;

namespace Submissions.Web.Utility.Email
{
	public class IGTFlatFileUploadEmail
	{
		public IGTFlatFileUploadEmail()
		{
			this.UploadResults = new List<IGTFlatFileUploadResult>();
		}
		public List<IGTFlatFileUploadResult> UploadResults { get; set; }
	}
}