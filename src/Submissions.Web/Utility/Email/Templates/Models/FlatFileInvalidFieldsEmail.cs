﻿using System.Collections.Generic;

namespace Submissions.Web.Utility.Email
{
	public class FlatFileInvalidFieldsEmail
	{
		public FlatFileInvalidFieldsEmail()
		{
			Studios = new List<string>();
			Platforms = new List<string>();
		}

		public List<string> Studios { get; set; }
		public List<string> Platforms { get; set; }
		public List<string> Vendors { get; set; }
		public string Filename { get; set; }
		public string EmailTitle { get; set; }
	}
}