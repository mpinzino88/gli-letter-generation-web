﻿using System;
using System.Collections.Generic;

namespace Submissions.Web.Utility.Email
{
	public class EngineeringReviewEmail
	{
		public EngineeringReviewEmail()
		{
			Attachments = new List<string>();
		}
		public string AdditionalComments { get; set; }
		public string BundleName { get; set; }
		public string ReviewerEmail { get; set; }
		public DateTime TargetDate { get; set; }

		public List<EngineeringReviewDocument> Documents { get; set; }
		public List<string> Recipients { get; set; }
		public List<string> Attachments { get; set; }
	}

	public class EngineeringReviewDocument
	{
		public string FilePath { get; set; }

		public List<string> Components { get; set; }
		public List<string> Jurisdictions { get; set; }
	}

	public class ApprovalEmail
	{
		public string BundleName { get; set; }
		public string FromEmail { get; set; }
		public string Body { get; set; }
		public DateTime? TargetDate { get; set; }
		public List<string> Recipients { get; set; }
	}
	public class SupplementalEmail
	{
		public string BundleName { get; set; }
		public string FromEmail { get; set; }
		public string Body { get; set; }
		public List<string> Recipients { get; set; }
	}
}