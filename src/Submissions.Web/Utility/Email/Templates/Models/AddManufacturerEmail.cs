﻿using System;
using System.Collections.Generic;

namespace Submissions.Web.Utility.Email
{
	public class AddManufacturerEmail
	{
		public Guid? Id { get; set; }
		public string Code { get; set; }
		public string Description { get; set; }
		public string AddedBy { get; set; }
		public Uri ProductSubmittersListItemUri { get; set; }
		public Uri ActivateManufacturerUri { get; set; }
		public bool ForDynamicsOnly { get; set; }
		public List<string> pendingJurisdictionRequests { get; set; }
		public List<string> pendingCompaniesForBilling { get; set; }
		public bool Expedite { get; set; }
	}
}