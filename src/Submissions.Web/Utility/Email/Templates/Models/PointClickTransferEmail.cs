﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Submissions.Web.Utility.Email
{
	public class PointClickTransferEmail
	{
		public string Manufacturer { get; set; }
		public string FileNumber { get; set; }
		public string Jurisdiction { get; set; }
		public string Status { get; set; }
		public DateTime? Requested { get; set; }
		public DateTime Processed { get; set; }
	}
}