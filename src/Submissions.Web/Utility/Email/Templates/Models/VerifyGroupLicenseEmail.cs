﻿using System;

namespace Submissions.Web.Utility.Email
{
	public class VerifyGroupLicenseEmail
	{
		public Guid License { get; set; }
		public string FirstName { get; set; }
		public string LastName { get; set; }
		public string Organization { get; set; }
		public string Phone { get; set; }
		public string Address { get; set; }
		public string Address2 { get; set; }
		public string City { get; set; }
		public string State { get; set; }
		public string Zip { get; set; }
	}
}