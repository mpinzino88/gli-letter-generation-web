﻿
namespace Submissions.Web.Utility.Email
{
	public class EditJurisdictionMappingEmail
	{
		public int Id { get; set; }
		public string Manufacturer { get; set; }
		public string ExternalJurisdictionCode { get; set; }
		public string ExternalJurisdictionId { get; set; }
		public string ExternalJurisdictionDescription { get; set; }
		public string JurisdictionId { get; set; }
		public string Jurisdiction { get; set; }
		public string jurisdictionType { get; set; }
		//public int CompanyId { get; set; }
		//public string Company { get; set; }
		//public int CurrencyId { get; set; }
		//public string Currency { get; set; }
		//public int ContractTypeId { get; set; }
		//public string ContractType { get; set; }
		//public int? TestLabId { get; set; }
		//public string TestLab { get; set; }
	}
}