﻿using Submissions.Common;
using Submissions.Web.Areas.Protrack.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Submissions.Web.Utility.Email
{
	public class MasterFileLinkageEmail
	{
		public MasterFileLinkageEmail()
		{
			this.Props = new MasterFileEmailProperties();
		}

		public string GetEmailSubject()
		{
			if (this.IsInitial)
			{
				return "Initial Masterfile mapping established for " + this.CurrentFileNumber;
			}
			else
			{
				return "Masterfile mapping updated for " + this.CurrentFileNumber;
			}
		}

		public string CurrentFileNumber { get; set; }
		public string MasterFileNumber { get; set; }
		public string PreviousMasterFileNumber { get; set; }
		public ResubmissionOptions MappingType { get; set; }
		public bool IsInitial { get; set; }
		public MasterFileEmailProperties Props { get; set; }

	}
}