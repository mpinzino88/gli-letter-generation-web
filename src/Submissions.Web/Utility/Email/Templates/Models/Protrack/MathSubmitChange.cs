﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Submissions.Web.Utility.Email
{
	public class MathSubmitChange
	{
		public string ProjectName { get; set; }
		public string Theme { get; set; }
		public List<ChangeForm> FormChanges {get; set;}
	}

	public class ChangeForm
	{
		public string FieldName { get; set; }
		public string OldVal { get; set; }
		public string NewVal { get; set; }
	}
}