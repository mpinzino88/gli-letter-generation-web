﻿namespace Submissions.Web.Utility.Email
{
	public class ProjectReviewEmail
	{
		public int Id { get; set; }
		public string Header { get; set; }
		public string Filenumber { get; set; }
		public string Status { get; set; }
		public string Link { get; set; }
		public string AssigneeNames { get; set; }
		public string ReviewerNames { get; set; }
		public string LetterwriterNames { get; set; }
		public string Note { get; set; }
		public bool EmailToAssignee { get; set; }
		public bool EmailToReviewer { get; set; }
		public bool IsDev { get; set; }
		public string EmailAddressForTesting { get; set; }
	}
}