﻿using Submissions.Core.Models.ProjectService;
using System.Collections.Generic;

namespace Submissions.Web.Utility.Email
{
	public class TasksEmail
	{
		public string Action { get; set; }
		public string FileNumber { get; set; }
		public string UserName { get; set; }
		public List<Task> Tasks { get; set; }
	}
}