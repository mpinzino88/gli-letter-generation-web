﻿using System;
using System.Collections.Generic;

namespace Submissions.Web.Utility.Email
{
	public class ResourceFinderRequestUserEmail
	{
		public ResourceFinderRequestUserEmail()
		{
			this.Tasks = new List<ResourceFinderTaskEmail>();
		}

		public string Action { get; set; }
		public string ManagerName { get; set; }
		public string ProjectName { get; set; }
		public string ProjectUrl { get; set; }
		public int RequestUserId { get; set; }
		public string RequestUserName { get; set; }
		public DateTime? RequestStartDate { get; set; }
		public string AdditionalInfo { get; set; }
		public IList<ResourceFinderTaskEmail> Tasks { get; set; }
		public string ProcessRequestUrl { get; set; }
		public string DeclineReason { get; set; }
	}

	public class ResourceFinderTaskEmail
	{
		public int? TaskDefinitionId { get; set; }
		public string TaskDefinition { get; set; }
		public decimal EstimateHours { get; set; }
	}
}