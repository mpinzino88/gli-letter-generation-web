﻿using System;
using System.Collections.Generic;

namespace Submissions.Web.Utility.Email
{
	public class TargetDateChangeEmailModel
	{
		public string ProjectName { get; set; }
		public DateTime TargetDate { get; set; }
		public string ProjectStatus { get; set; }
		public string TestLab { get; set; }
		public List<ProjectJurisdiction> ProjectJurisdictions { get; set; }
		public bool DisplayOnlyProject { get; set; }
		public string AffectedPrimary { get; set; }
	}

	public class ProjectJurisdiction
	{
		public string FileNumber { get; set; }
		public string IdNumber { get; set; }
		public string PartNumber { get; set; }
		public string DateCode { get; set; }
		public string Version { get; set; }
		public string JurisdictionalDataId { get; set; }
		public string Jurisdiction { get; set; }
		public string JurisdictionId { get; set; }
		public DateTime? JurisdictionRcvdDate { get; set; }
		public DateTime? OrgTargetDate { get; set; }
		public DateTime? TargetDate { get; set; }
	}

}