using AutoMapper;
using EF.eResults;
using EF.eResultsManaged;
using EF.GLIAccess;
using EF.GLiCloud;
using EF.GLIExtranet;
using EF.GPS;
using EF.LegacyModernized;
using EF.QANotes;
using EF.Submission;
using Elmah;
using Fissoft.EntityFramework.Fts;
using GLI.Accounting.Acumatica;
using GLI.EF.LetterContent;
using StackExchange.Profiling;
using StackExchange.Profiling.EntityFramework6;
using StackExchange.Profiling.Mvc;
using Submissions.Common;
using Submissions.Web.Areas.PointClick;
using Submissions.Web.Areas.Reports;
using Submissions.Web.Helpers.CustomFilters;
using Submissions.Web.Mappings;
using Submissions.Web.Utility;
using Submissions.Web.Utility.Email;
using System.Data.Entity.Infrastructure.Interception;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Enum = System.Enum;

namespace Submissions.Web
{
	public class MvcApplication : HttpApplication
	{
		protected void Application_Start()
		{
			AreaRegistration.RegisterAllAreas();
			RouteConfig.RegisterRoutes(RouteTable.Routes);

			InitializeViewEngines();
			RegisterGlobalFilters(GlobalFilters.Filters);
			BundleConfig.RegisterBundles(BundleTable.Bundles);
			InitializeProfilerSettings();
			InitializeDatabase();
			InitalizeAcumaticaLibrary();
			InitializeMappings();
			var licenseExcel = new Aspose.Cells.License();
			licenseExcel.SetLicense("Aspose.Cells.lic");
		}

		protected void Application_BeginRequest()
		{
			if (Request.IsLocal)
			{
				MiniProfiler.StartNew();
			}
		}

		protected void Application_EndRequest()
		{
			MiniProfiler.Current?.Stop();
		}

		protected void Application_Error()
		{
			if (Request.IsLocal)
			{
				return;
			}

			var ex = Server.GetLastError();
			Response.Clear();
			HttpContext.Current.ClearError();

			Response.Redirect("~/Error?errorMessage=" + ex.Message);
		}

		protected void ErrorLog_Filtering(object sender, ExceptionFilterEventArgs e)
		{
			if (e.Exception.GetBaseException() is IgnoreLogException)
			{
				e.Dismiss();
			}
			// ignore this error that stems from base64 encoded images in css
			else if (e.Exception.Message == "A potentially dangerous Request.Path value was detected from the client (:).")
			{
				e.Dismiss();
			}
		}

		protected void Session_Start()
		{
			SessionConfig.Register();
		}

		public override string GetVaryByCustomString(HttpContext context, string arg)
		{
			if (arg == "User")
			{
				return "User=" + context.Request.LogonUserIdentity.Name;
			}

			return base.GetVaryByCustomString(context, arg);
		}

		#region initializers
		private void InitializeProfilerSettings()
		{
			MiniProfiler.Configure(new MiniProfilerOptions
			{
				ResultsAuthorize = r => r.IsLocal,
				PopupRenderPosition = RenderPosition.BottomRight,
				TrackConnectionOpenClose = false
			}
			.AddViewProfiling()
			.AddEntityFramework());

			MiniProfilerEF6.Initialize();
		}

		private static void InitializeViewEngines()
		{
			ViewEngines.Engines.Clear();
			ViewEngines.Engines.Add(new RazorViewEngine());
		}

		private static void RegisterGlobalFilters(GlobalFilterCollection filters)
		{
			filters.Add(new HandleErrorAttribute());
			filters.Add(new AuthorizeAttribute());
			filters.Add(new SessionExpireFilterAttribute());
		}

		private static T GetEnvironment<T>(string env)
		{
			return (T)Enum.Parse(typeof(T), env);
		}

		private static void InitializeDatabase()
		{
			var environment = WebConfigurationManager.AppSettings["Environment"];

			SubmissionContext.dbType = GetEnvironment<SubmissionContext.Environment>(environment);
			GLIAccessContext.dbType = GetEnvironment<GLIAccessContext.Environment>(environment);
			GLIExtranetContext.dbType = GetEnvironment<GLIExtranetContext.Environment>(environment);
			GLiCloudContext.dbType = GetEnvironment<GLiCloudContext.Environment>(environment);
			QANotesContext.dbType = GetEnvironment<QANotesContext.Environment>(environment);
			eResultsContext.dbType = GetEnvironment<eResultsContext.Environment>(environment);
			eResultsManagedContext.dbType = GetEnvironment<eResultsManagedContext.Environment>(environment);
			GPSContext.dbType = GetEnvironment<GPSContext.Environment>(environment);
			LegacyModernizedContext.dbType = GetEnvironment<LegacyModernizedContext.Environment>(environment);
			LetterContentContext.dbType = GetEnvironment<LetterContentContext.Environment>(environment);
			DbInterception.Add(new DbInterceptor());
			DbInterception.Add(new FtsInterceptor());

			// Core
			GLI.EFCore.Submission.SubmissionContext.DbType = GetEnvironment<GLI.EFCore.Submission.SubmissionContext.Environment>(environment);
			GLI.LetterContent.Dapper.Settings.DbType = GetEnvironment<GLI.LetterContent.Dapper.Settings.Environment>(environment);
		}

		private static void InitalizeAcumaticaLibrary()
		{
			var environment = WebConfigurationManager.AppSettings["Environment"];
			AcumaticaService.env = GetEnvironment<GLI.Accounting.Acumatica.Enum.Environment>(environment);
		}

		private static void InitializeMappings()
		{
			Mapper.Initialize(cfg =>
			{
				cfg.AddProfile<EmailProfile>();
				cfg.AddProfile<EvolutionProfile>();
				cfg.AddProfile<LettersProfile>();
				cfg.AddProfile<LookupsProfile>();
				cfg.AddProfile<PointClickProfile>();
				cfg.AddProfile<ProtrackProfile>();
				cfg.AddProfile<ReportsProfile>();
				cfg.AddProfile<RootProfile>();
				cfg.AddProfile<SubmissionProfile>();
				cfg.AddProfile<ToolsProfile>();
				cfg.AddProfile<VerifyProfile>();
			});

			Mapper.AssertConfigurationIsValid();
		}
		#endregion

	}
}