﻿using EF.Submission;
using Submissions.Common;
using System;
using System.Linq;
using System.Web.Mvc;
using System.Web.SessionState;

namespace Submissions.Web.Controllers
{
	[SessionState(SessionStateBehavior.ReadOnly)]
	public class SingleInputSearchController : Controller
	{
		private readonly ISubmissionContext _dbSubmission;
		private readonly IUserContext _userContext;

		public SingleInputSearchController(ISubmissionContext dbSubmission, IUserContext userContext)
		{
			_dbSubmission = dbSubmission;
			_userContext = userContext;
		}

		#region Projects
		public JsonResult ProjectFileNumber(string term)
		{
			var query = _dbSubmission.vw_Projects.AsNoTracking().AsQueryable();

			try
			{
				if (term.Count(x => x == '-') < 4)
				{
					throw new Exception("Not a valid File Number so we can't search with Manufacturer Code");
				}

				var validFileNumber = new FileNumber(term);
				var fileNumber = validFileNumber.ToString();
				var manufacturerCode = validFileNumber.ManufacturerCode;

				query = query.Where(x => x.Manufacturer == manufacturerCode);
				query = query.Where(x => x.ProjectName.StartsWith(fileNumber));
			}
			catch
			{
				query = query.Where(x => x.ProjectName.Contains(term));
			}

			var dataSource = query
				.Select(x => new { name = x.ProjectName })
				.Distinct()
				.Take(5)
				.ToList();

			return Json(dataSource, JsonRequestBehavior.AllowGet);
		}

		public JsonResult ProjectGameName(string term)
		{
			var dataSource = _dbSubmission.vw_Projects.AsNoTracking()
				.Where(x => x.ProjectJurisdictionalData.Any(y => y.JurisdictionalData.Submission.GameName.Contains(term)))
				.Select(x => new { name = x.ProjectJurisdictionalData.Where(y => y.JurisdictionalData.Submission.GameName.Contains(term)).FirstOrDefault().JurisdictionalData.Submission.GameName })
				.Distinct()
				.Take(5)
				.ToList();

			return Json(dataSource, JsonRequestBehavior.AllowGet);
		}

		public JsonResult ProjectIdNumber(string term)
		{
			var dataSource = _dbSubmission.vw_Projects.AsNoTracking()
				.Where(x => x.ProjectJurisdictionalData.Any(y => y.JurisdictionalData.Submission.IdNumber.Contains(term)))
				.Select(x => new { name = x.ProjectJurisdictionalData.Where(y => y.JurisdictionalData.Submission.IdNumber.Contains(term)).FirstOrDefault().JurisdictionalData.Submission.IdNumber })
				.Distinct()
				.Take(5)
				.ToList();

			return Json(dataSource, JsonRequestBehavior.AllowGet);
		}
		#endregion

		#region Submissions
		public JsonResult SubmissionFileNumber(string term)
		{
			var query = _dbSubmission.Submissions.AsNoTracking().AsQueryable();

			try
			{
				if (term.Count(x => x == '-') < 4)
				{
					throw new Exception("Not a valid File Number so we can't search with Manufacturer Code");
				}

				var validFileNumber = new FileNumber(term);
				var fileNumber = validFileNumber.ToString();
				var manufacturerCode = validFileNumber.ManufacturerCode;

				query = query.Where(x => x.ManufacturerCode == manufacturerCode);
				query = query.Where(x => x.FileNumber.StartsWith(fileNumber));
			}
			catch
			{
				query = query.Where(x => x.FileNumber.Contains(term));
			}

			var dataSource = query
				.Select(x => new { name = x.FileNumber })
				.Distinct()
				.Take(5)
				.ToList();

			return Json(dataSource, JsonRequestBehavior.AllowGet);
		}

		public JsonResult SubmissionGameName(string term)
		{
			var dataSource = _dbSubmission.Submissions.AsNoTracking()
				.Where(x => x.GameName.Contains(term))
				.Select(x => new { name = x.GameName })
				.Distinct()
				.Take(5)
				.ToList();

			return Json(dataSource, JsonRequestBehavior.AllowGet);
		}

		public JsonResult SubmissionIdNumber(string term)
		{
			var dataSource = _dbSubmission.Submissions.AsNoTracking()
				.Where(x => x.IdNumber.Contains(term))
				.Select(x => new { name = x.IdNumber })
				.Distinct()
				.Take(5)
				.ToList();

			return Json(dataSource, JsonRequestBehavior.AllowGet);
		}
		#endregion
	}
}