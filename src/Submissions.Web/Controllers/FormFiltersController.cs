﻿extern alias ZEFCore;
using GLI.EFCore.Submission;
using Newtonsoft.Json;
using Submissions.Common;
using Submissions.Web.Models;
using Submissions.Web.Models.Query;
using System;
using System.Linq;
using System.Web.Mvc;
using ZEFCore::Z.EntityFramework.Plus;

namespace Submissions.Web.Controllers
{
	public class FormFiltersController : Controller
	{
		private readonly SubmissionContext _dbSubmission;
		private readonly IUserContext _userContext;

		public FormFiltersController(SubmissionContext dbSubmission, IUserContext userContext)
		{
			_dbSubmission = dbSubmission;
			_userContext = userContext;
		}

		[HttpPost]
		public ActionResult Edit(int? id, string filterName, string section, string filters)
		{
			if (id == null)
			{
				id = AddFormFilters((FormFilterSection)Enum.Parse(typeof(FormFilterSection), section), filterName, filters);
			}
			else
			{
				SaveFormFilters((int)id, filters);
			}

			return Json(id);
		}

		[HttpPost]
		public ActionResult EditProjectLegacy(int? id, string filterName, ProjectSearch filters)
		{
			var filtersJson = JsonConvert.SerializeObject(filters);

			if (id == null)
			{
				id = AddFormFilters(FormFilterSection.ProjectLegacy, filterName, filtersJson);
				return Json(id);
			}

			SaveFormFilters((int)id, filtersJson);

			return Json("ok");
		}

		public ActionResult EditQAProjectLegacy(int? id, string filterName, ProjectSearch filters)
		{
			var filtersJson = JsonConvert.SerializeObject(filters);

			if (id == null)
			{
				id = AddFormFilters(FormFilterSection.QAProjectLegacy, filterName, filtersJson);
				return Json(id);
			}

			SaveFormFilters((int)id, filtersJson);

			return Json("ok");
		}

		public ActionResult Load(FormFilterSection section)
		{
			var vm = new FormFiltersViewModel();
			vm.FormFilters = _dbSubmission.tbl_FormFilters
				.Where(x => x.UserId == _userContext.User.Id && x.Section == section.ToString())
				.Select(x => new FormFilter { Id = x.Id, AddDate = x.AddDate, FilterName = x.FilterName })
				.OrderBy(x => x.FilterName)
				.ToList();

			return PartialView("_FormFilters", vm);
		}

		[HttpGet]
		public ActionResult LoadJson(FormFilterSection section)
		{
			var formFilters = _dbSubmission.tbl_FormFilters
				.Where(x => x.UserId == _userContext.User.Id && x.Section == section.ToString())
				.Select(x => new FormFilter { Id = x.Id, AddDate = x.AddDate, FilterName = x.FilterName })
				.OrderBy(x => x.FilterName)
				.ToList();

			return Content(ComUtil.JsonEncodeCamelCase(formFilters), "application/json");
		}

		public ActionResult Remove(int id)
		{
			var formFilter = new tbl_FormFilter { Id = id, Current = false };
			_dbSubmission.tbl_FormFilters.Attach(formFilter);
			_dbSubmission.Entry(formFilter).Property(x => x.Current).IsModified = true;
			_dbSubmission.SaveChanges();

			return Json("ok");
		}

		[HttpPost]
		public ActionResult Delete(int id)
		{
			_dbSubmission.tbl_FormFilters.Where(x => x.Id == id).Delete();

			return Json("ok");
		}

		[HttpPost]
		public ActionResult Apply(int id, FormFilterSection section)
		{
			_dbSubmission.tbl_FormFilters
				.Where(x => x.Section == section.ToString())
				.Update(x => new tbl_FormFilter { Current = false });

			var formFilter = new tbl_FormFilter { Id = id, Current = true };
			_dbSubmission.tbl_FormFilters.Attach(formFilter);
			_dbSubmission.Entry(formFilter).Property(x => x.Current).IsModified = true;
			_dbSubmission.SaveChanges();

			return Json("ok");
		}

		#region Helper Methods
		private int AddFormFilters(FormFilterSection section, string filterName, string filtersJson)
		{
			_dbSubmission.tbl_FormFilters
				.Where(x => x.Section == section.ToString())
				.Update(x => new tbl_FormFilter { Current = false });

			var formFilter = new tbl_FormFilter
			{
				Section = section.ToString(),
				UserId = _userContext.User.Id,
				FilterName = filterName,
				Filters = filtersJson,
				Current = true
			};

			_dbSubmission.tbl_FormFilters.Add(formFilter);
			_dbSubmission.SaveChanges();

			return formFilter.Id;
		}

		private void SaveFormFilters(int id, string filtersJson)
		{
			var formFilter = new tbl_FormFilter { Id = id, Filters = filtersJson };
			_dbSubmission.tbl_FormFilters.Attach(formFilter);
			_dbSubmission.Entry(formFilter).Property(x => x.Filters).IsModified = true;
			_dbSubmission.SaveChanges();
		}
		#endregion
	}
}