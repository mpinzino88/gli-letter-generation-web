﻿using AutoMapper;
using EF.Submission;
using Submissions.Common;
using Submissions.Web.Models.Dashboard;
using Submissions.Web.Models.Widgets;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Z.EntityFramework.Plus;

namespace Submissions.Web.Controllers
{
	public class DashboardController : Controller
	{
		private readonly ISubmissionContext _dbSubmission;
		private readonly IUserContext _userContext;

		public DashboardController(ISubmissionContext dbSubmission, IUserContext userContext)
		{
			_dbSubmission = dbSubmission;
			_userContext = userContext;
		}

		#region Main
		public ActionResult Index(int? defaultDashboardId)
		{
			var currentDashboardId = (defaultDashboardId == -1) ? null : (defaultDashboardId ?? _userContext.DefaultDashboardId);
			_userContext.DefaultDashboardId = currentDashboardId;

			var dashboards = _dbSubmission.tbl_Dashboards
				.Where(x => x.UserId == _userContext.User.Id)
				.OrderBy(x => x.Name)
				.Select(x => new SelectListItem { Text = x.Name, Value = x.Id.ToString(), Selected = (x.Id == currentDashboardId) })
				.ToList();

			if (dashboards.Count > 0)
			{
				dashboards.Insert(0, new SelectListItem { Text = Globals.DefaultDashboardName, Value = "", Selected = (currentDashboardId == null) });
			}

			var widgets = _dbSubmission.tbl_Widgets
				.Where(x => x.UserId == _userContext.User.Id && x.ShowOnDashboard == true && x.DashboardId == currentDashboardId)
				.ToList();

			var vm = new DashboardIndexViewModel
			{
				Dashboards = dashboards,
				DefaultDashboard = dashboards.Where(x => x.Selected == true).Select(x => x.Text).SingleOrDefault() ?? Globals.DefaultDashboardName,
				Widgets = Mapper.Map<List<Widget>>(widgets)
			};

			return View(vm);
		}

		public ActionResult Edit(Mode mode)
		{
			var vm = new DashboardEditViewModel();

			if (mode == Mode.Edit)
			{
				var item = _dbSubmission.tbl_Dashboards.Find((int)_userContext.DefaultDashboardId);

				vm.Id = item.Id;
				vm.Mode = Mode.Edit;
				vm.Name = item.Name;
				vm.Description = item.Description;
			}

			return PartialView("_Edit", vm);
		}

		[HttpPost]
		public ActionResult Edit(DashboardEditViewModel vm)
		{
			if (vm.Mode == Mode.Add)
			{
				// add new
				var newDashboard = new tbl_Dashboard
				{
					Name = vm.Name,
					Description = vm.Description,
					UserId = _userContext.User.Id
				};

				_dbSubmission.tbl_Dashboards.Add(newDashboard);
				_dbSubmission.SaveChanges();

				if (vm.CloneCurrent)
				{
					CloneWidgets(newDashboard.Id);
				}

				SetDefaultDashboard(newDashboard.Id);
			}
			else
			{
				var item = new tbl_Dashboard { Id = (int)vm.Id, Name = vm.Name, Description = vm.Description };
				_dbSubmission.tbl_Dashboards.Attach(item);
				_dbSubmission.Entry(item).Property(x => x.Name).IsModified = true;
				_dbSubmission.Entry(item).Property(x => x.Description).IsModified = true;
				_dbSubmission.SaveChanges();
			}

			return RedirectToAction("Index", "Dashboard");
		}

		[HttpPost]
		public ActionResult Delete(int id)
		{
			_dbSubmission.tbl_Dashboards.Where(x => x.Id == id).Delete();
			_dbSubmission.tbl_Widgets.Where(x => x.DashboardId == id).Delete();

			if (id == _userContext.DefaultDashboardId)
			{
				int? newDefaultDashboardId = _dbSubmission.tbl_Dashboards.Where(x => x.UserId == _userContext.User.Id).Take(1).Select(x => x.Id).SingleOrDefault();
				newDefaultDashboardId = newDefaultDashboardId == 0 ? null : newDefaultDashboardId;

				SetDefaultDashboard(newDefaultDashboardId);
			}

			return RedirectToAction("Index");
		}
		#endregion

		#region Helpers
		private void CloneWidgets(int newDashboardId)
		{
			var currentDashboardId = _userContext.DefaultDashboardId;

			var widgets = new List<tbl_Widget>();
			if (currentDashboardId == null)
			{
				widgets = _dbSubmission.tbl_Widgets.Where(x => x.UserId == _userContext.User.Id && x.DashboardId == null).ToList();
			}
			else
			{
				widgets = _dbSubmission.tbl_Widgets.Where(x => x.DashboardId == currentDashboardId).ToList();
			}

			if (widgets.Count > 0)
			{
				widgets.ForEach(x => x.DashboardId = newDashboardId);
				_dbSubmission.tbl_Widgets.AddRange(widgets);
				_dbSubmission.SaveChanges();
			}
		}

		private void SetDefaultDashboard(int? newDashboardId)
		{
			var user = new trLogin { Id = _userContext.User.Id, DefaultDashboardId = newDashboardId };
			_dbSubmission.trLogins.Attach(user);
			_dbSubmission.Entry(user).Property(x => x.DefaultDashboardId).IsModified = true;
			_dbSubmission.Configuration.ValidateOnSaveEnabled = false;
			_dbSubmission.SaveChanges();

			_userContext.DefaultDashboardId = newDashboardId;
		}
		#endregion
	}
}