﻿using Submissions.Web.Models;
using System.Web.Mvc;

namespace Submissions.Web.Controllers
{
	public class HomeController : Controller
	{
		public ActionResult Index()
		{
			return View();
		}

		public ActionResult PermissionDenied(int id)
		{
			var vm = new PermissionDeniedViewModel
			{
				MissingPermission = (Permission)id
			};
			return View(vm);
		}
	}
}