﻿using FUWUtil;
using System.IO;
using System.Web;
using System.Web.Mvc;

namespace Submissions.Web.Controllers
{
	public class DocumentViewerController : Controller
	{
		/// <summary>
		/// TempData["File"] should be the physical path and file name.
		/// </summary>
		public ActionResult Index()
		{
			var file = TempData["File"].ToString();

			if (file.Length == 0)
			{
				return RedirectToAction("NotFound");
			}
			else
			{
				return GetFile(file);
			}
		}

		public ActionResult GPSFile(int? gpsItemId, string filePath, string altName)
		{
			if (gpsItemId != null)
			{
				var gpsItem = FUWUtil.FUWUtil.GetItemById((int)gpsItemId);

				if (gpsItem == null)
				{
					return RedirectToAction("NotFound");
				}

				filePath = FUWOperations.KernelFileOperations.EnsureNotUNCName(gpsItem.FullName);
			}

			var fileContents = GPSFileInfo.GPSFileContent(filePath);
			var fileDetails = new GPSFileInfo(filePath);

			var contentType = MimeMapping.GetMimeMapping(fileDetails.Name);
			var fileName = altName ?? fileDetails.Name;
			Response.AppendHeader("Content-Disposition", "inline; filename=" + fileName.Replace(',', '_'));

			return File(fileContents, contentType);
		}

		public ActionResult NotFound()
		{
			ViewBag.Message = "Document could not be found.";
			return View("Error");
		}

		public ActionResult Permission()
		{
			ViewBag.Message = "You do not have permission to view this document";
			return View("Error");
		}

		public ActionResult GetFile(string file)
		{
			var fileName = Path.GetFileName(file);
			var contentType = MimeMapping.GetMimeMapping(fileName);

			if (!System.IO.File.Exists(file))
			{
				return RedirectToAction("NotFound");
			}

			Response.Headers.Add("content-disposition", "inline; filename=\"" + fileName + "\"");
			return File(file, contentType);
		}
	}
}