﻿using EF.eResultsManaged;
using EF.QANotes;
using EF.Submission;
using System.Linq;
using System.Web.Mvc;
using System.Web.SessionState;
using Trirand.Web.Mvc;

namespace Submissions.Web.Controllers
{
	[SessionState(SessionStateBehavior.ReadOnly)]
	public class GridsAutoCompleteController : Controller
	{
		private readonly IeResultsManagedContext _dbeResultsManaged;
		private readonly IQANotesContext _dbQANotes;
		private readonly ISubmissionContext _dbSubmission;

		public GridsAutoCompleteController(IeResultsManagedContext dbeResultsManaged, IQANotesContext dbQANotes, ISubmissionContext dbSubmission)
		{
			_dbeResultsManaged = dbeResultsManaged;
			_dbQANotes = dbQANotes;
			_dbSubmission = dbSubmission;
		}

		#region General (use these before making custom ones)
		public JsonResult DateCode(string term)
		{
			var dataSource = _dbSubmission.Submissions.AsNoTracking()
				.Where(x => x.DateCode.StartsWith(term))
				.Select(x => new { DateCode = x.DateCode })
				.Distinct()
				.Take(10);

			var autoComplete = new JQAutoComplete
			{
				DataField = "DateCode",
				AutoCompleteMode = AutoCompleteMode.BeginsWith,
				DataSource = dataSource
			};

			return autoComplete.DataBind();
		}

		public JsonResult Department(string term)
		{
			var dataSource = _dbSubmission.trDepts.AsNoTracking()
				.Where(x => x.Code.StartsWith(term))
				.Select(x => new { Department = x.Code })
				.Take(10);

			var autoComplete = new JQAutoComplete
			{
				DataField = "Department",
				AutoCompleteMode = AutoCompleteMode.BeginsWith,
				DataSource = dataSource
			};

			return autoComplete.DataBind();
		}

		public JsonResult FileNumber(string term)
		{
			var dataSource = _dbSubmission.Submissions.AsNoTracking()
				.Where(x => x.FileNumber.StartsWith(term))
				.Select(x => new { FileNumber = x.FileNumber })
				.Distinct()
				.Take(10);

			var autoComplete = new JQAutoComplete
			{
				DataField = "FileNumber",
				AutoCompleteMode = AutoCompleteMode.BeginsWith,
				DataSource = dataSource
			};

			return autoComplete.DataBind();
		}

		public JsonResult GameName(string term)
		{
			var dataSource = _dbSubmission.Submissions.AsNoTracking()
				.Where(x => x.GameName.StartsWith(term))
				.Select(x => new { GameName = x.GameName })
				.Distinct()
				.Take(10);

			var autoComplete = new JQAutoComplete
			{
				DataField = "GameName",
				AutoCompleteMode = AutoCompleteMode.BeginsWith,
				DataSource = dataSource
			};

			return autoComplete.DataBind();
		}

		public JsonResult Function(string term)
		{
			var dataSource = _dbSubmission.Submissions.AsNoTracking()
				.Where(x => x.Function.StartsWith(term))
				.Select(x => new { Function = x.Function })
				.Distinct()
				.Take(10);

			var autoComplete = new JQAutoComplete
			{
				DataField = "Function",
				AutoCompleteMode = AutoCompleteMode.BeginsWith,
				DataSource = dataSource
			};

			return autoComplete.DataBind();
		}

		public JsonResult IdNumber(string term)
		{
			var dataSource = _dbSubmission.Submissions.AsNoTracking()
				.Where(x => x.IdNumber.StartsWith(term))
				.Select(x => new { IdNumber = x.IdNumber })
				.Distinct()
				.Take(10);

			var autoComplete = new JQAutoComplete
			{
				DataField = "IdNumber",
				AutoCompleteMode = AutoCompleteMode.BeginsWith,
				DataSource = dataSource
			};

			return autoComplete.DataBind();
		}

		public JsonResult Lab(string term)
		{
			var dataSource = _dbSubmission.trLaboratories.AsNoTracking()
				.Where(x => x.Location.StartsWith(term))
				.Select(x => new { Lab = x.Location })
				.Distinct()
				.Take(10);

			var autoComplete = new JQAutoComplete
			{
				DataField = "Lab",
				AutoCompleteMode = AutoCompleteMode.BeginsWith,
				DataSource = dataSource
			};

			return autoComplete.DataBind();
		}

		public JsonResult Manufacturer(string term)
		{
			var dataSource = _dbSubmission.tbl_lst_fileManu.AsNoTracking()
				.Where(x => x.Description.StartsWith(term) && x.Active == true)
				.Select(x => new { Manufacturer = x.Description })
				.Take(10);

			var autoComplete = new JQAutoComplete
			{
				DataField = "Manufacturer",
				AutoCompleteMode = AutoCompleteMode.BeginsWith,
				DataSource = dataSource
			};

			return autoComplete.DataBind();
		}

		public JsonResult Position(string term)
		{
			var dataSource = _dbSubmission.Submissions.AsNoTracking()
				.Where(x => x.Position.StartsWith(term))
				.Select(x => new { Position = x.Position })
				.Distinct()
				.Take(10);

			var autoComplete = new JQAutoComplete
			{
				DataField = "Position",
				AutoCompleteMode = AutoCompleteMode.BeginsWith,
				DataSource = dataSource
			};

			return autoComplete.DataBind();
		}

		public JsonResult Version(string term)
		{
			var dataSource = _dbSubmission.Submissions.AsNoTracking()
				.Where(x => x.Version.StartsWith(term))
				.Select(x => new { Version = x.Version })
				.Distinct()
				.Take(10);

			var autoComplete = new JQAutoComplete
			{
				DataField = "Version",
				AutoCompleteMode = AutoCompleteMode.BeginsWith,
				DataSource = dataSource
			};

			return autoComplete.DataBind();
		}

		public new JsonResult User(string term)
		{
			var dataSource = _dbSubmission.trLogins.AsNoTracking()
				.Where(x => (x.FName.StartsWith(term) || x.LName.StartsWith(term)) && x.Status == "A")
				.Select(x => new { User = x.FName + " " + x.LName })
				.Take(10);

			var autoComplete = new JQAutoComplete
			{
				DataField = "User",
				AutoCompleteMode = AutoCompleteMode.Contains,
				DataSource = dataSource
			};

			return autoComplete.DataBind();
		}
		#endregion

		#region QA Notes
		public JsonResult ApplicableResultAutoComplete(string term)
		{
			var dataSource = _dbQANotes.ValRule_To_Note.AsNoTracking()
				.Where(x => x.ApplicableResult.StartsWith(term))
				.Select(x => new { ApplicableResult = x.ApplicableResult })
				.Distinct()
				.Take(10);

			var autoComplete = new JQAutoComplete
			{
				DataField = "ApplicableResult",
				AutoCompleteMode = AutoCompleteMode.BeginsWith,
				DataSource = dataSource
			};

			return autoComplete.DataBind();
		}

		public JsonResult HeadingAutoComplete(string term)
		{
			var dataSource = _dbQANotes.Notes.AsNoTracking()
				.Where(x => x.Heading.StartsWith(term))
				.Select(x => new { Heading = x.Heading })
				.Distinct()
				.Take(10);

			var autoComplete = new JQAutoComplete
			{
				DataField = "Heading",
				AutoCompleteMode = AutoCompleteMode.BeginsWith,
				DataSource = dataSource
			};

			return autoComplete.DataBind();
		}

		public JsonResult LanguageAutoComplete(string term)
		{
			var dataSource = _dbQANotes.LanguageNote.AsNoTracking()
				.Where(x => x.Language.StartsWith(term))
				.Select(x => new { Language = x.Language })
				.Distinct()
				.Take(10);

			var autoComplete = new JQAutoComplete
			{
				DataField = "Language",
				AutoCompleteMode = AutoCompleteMode.BeginsWith,
				DataSource = dataSource
			};

			return autoComplete.DataBind();
		}

		public JsonResult LanguageNoteAutoComplete(string term)
		{
			var dataSource = _dbQANotes.LanguageNote.AsNoTracking()
				.Where(x => x.Note.StartsWith(term))
				.Select(x => new { LanguageNote = x.Note })
				.Distinct()
				.Take(10);

			var autoComplete = new JQAutoComplete
			{
				DataField = "LanguageNote",
				AutoCompleteMode = AutoCompleteMode.BeginsWith,
				DataSource = dataSource
			};

			return autoComplete.DataBind();
		}

		public JsonResult NoteIdAutoComplete(string term)
		{
			var dataSource = _dbQANotes.LanguageNote.AsNoTracking()
				.Where(x => x.NoteId.ToString().StartsWith(term))
				.Select(x => new { NoteId = x.NoteId.ToString() })
				.Distinct()
				.Take(10);

			var autoComplete = new JQAutoComplete
			{
				DataField = "NoteId",
				AutoCompleteMode = AutoCompleteMode.BeginsWith,
				DataSource = dataSource
			};

			return autoComplete.DataBind();
		}

		public JsonResult QAMemoAutoComplete(string term)
		{
			var dataSource = _dbQANotes.Notes.AsNoTracking()
				.Where(x => x.QAMemo.StartsWith(term))
				.Select(x => new { QAMemo = x.QAMemo })
				.Distinct()
				.Take(10);

			var autoComplete = new JQAutoComplete
			{
				DataField = "QAMemo",
				AutoCompleteMode = AutoCompleteMode.BeginsWith,
				DataSource = dataSource
			};

			return autoComplete.DataBind();
		}

		public JsonResult QBTextAutoComplete(string term)
		{
			var dataSource = _dbeResultsManaged.QuestionBase.AsNoTracking()
				.Where(x => x.Text.StartsWith(term))
				.Select(x => new { QBText = x.Text })
				.Distinct()
				.Take(10);

			var autoComplete = new JQAutoComplete
			{
				DataField = "QBText",
				AutoCompleteMode = AutoCompleteMode.BeginsWith,
				DataSource = dataSource
			};

			return autoComplete.DataBind();
		}

		public JsonResult RuleMemoAutoComplete(string term)
		{
			var dataSource = _dbQANotes.vw_QANotesQuery.AsNoTracking()
				.Where(x => x.Memo.StartsWith(term))
				.Select(x => new { RuleMemo = x.Memo })
				.Distinct()
				.Take(10);

			var autoComplete = new JQAutoComplete
			{
				DataField = "RuleMemo",
				AutoCompleteMode = AutoCompleteMode.BeginsWith,
				DataSource = dataSource
			};

			return autoComplete.DataBind();
		}

		public JsonResult SectionAutoComplete(string term)
		{
			var dataSource = _dbQANotes.Notes.AsNoTracking()
				.Where(x => x.Section.StartsWith(term))
				.Select(x => new { Section = x.Section })
				.Distinct()
				.Take(10);

			var autoComplete = new JQAutoComplete
			{
				DataField = "Section",
				AutoCompleteMode = AutoCompleteMode.BeginsWith,
				DataSource = dataSource
			};

			return autoComplete.DataBind();
		}

		public JsonResult TechLogicAutoComplete(string term)
		{
			var dataSource = _dbQANotes.Notes.AsNoTracking()
				.Where(x => x.TechLogic.StartsWith(term))
				.Select(x => new { TechLogic = x.TechLogic })
				.Distinct()
				.Take(10);

			var autoComplete = new JQAutoComplete
			{
				DataField = "TechLogic",
				AutoCompleteMode = AutoCompleteMode.BeginsWith,
				DataSource = dataSource
			};

			return autoComplete.DataBind();
		}

		public JsonResult UniqueIdAutoComplete(string term)
		{
			var dataSource = _dbQANotes.LanguageNote.AsNoTracking()
				.Where(x => x.Id.ToString().StartsWith(term))
				.Select(x => new { UniqueId = x.Id })
				.Distinct()
				.Take(10);

			var autoComplete = new JQAutoComplete
			{
				DataField = "UniqueId",
				AutoCompleteMode = AutoCompleteMode.BeginsWith,
				DataSource = dataSource
			};

			return autoComplete.DataBind();
		}
		#endregion

		#region Protrack

		public JsonResult ProjectAutoComplete(string term)
		{
			var dataSource = _dbSubmission.trProjects.AsNoTracking()
				.Where(x => x.ProjectName.ToString().StartsWith(term))
				.Select(x => new { FileNumber = x.ProjectName })
				.Distinct()
				.Take(10);

			var autoComplete = new JQAutoComplete
			{
				DataField = "FileNumber",
				AutoCompleteMode = AutoCompleteMode.BeginsWith,
				DataSource = dataSource
			};

			return autoComplete.DataBind();
		}

		public JsonResult SubmissionGameNameAutoComplete(string term)
		{
			var dataSource = _dbSubmission.Submissions.AsNoTracking()
				.Where(x => x.GameName.ToString().StartsWith(term))
				.Select(x => new { SubmissionGameName = x.GameName })
				.Distinct()
				.Take(10);

			var autoComplete = new JQAutoComplete
			{
				DataField = "SubmissionGameName",
				AutoCompleteMode = AutoCompleteMode.BeginsWith,
				DataSource = dataSource
			};

			return autoComplete.DataBind();
		}

		#endregion
	}
}