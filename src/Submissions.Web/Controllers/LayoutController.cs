﻿using Submissions.Common;
using Submissions.Web.Models;
using System.Web.Mvc;

namespace Submissions.Web.Controllers
{
	public class LayoutController : Controller
	{
		private readonly IUserContext _userContext;
		private readonly IConfigContext _configContext;

		public LayoutController(IUserContext userContext, IConfigContext configContext)
		{
			_userContext = userContext;
			_configContext = configContext;
		}

		[ChildActionOnly]
		public ActionResult NavBar()
		{
			var vm = new LayoutNavBarViewModel();
			vm.JiraUrl = _configContext.Config.JiraBaseUrl;
			vm.SharepointUrl = _configContext.Config.SharepointBaseUrl;
			vm.SubmissionsUrl = _configContext.SubmissionsBaseUrl;

			// default navbar search to submission for data entry users
			if (_userContext.Role == Role.QAE)
			{
				vm.DefaultSearchType = "Submissions";
				vm.DefaultSearchInputClass = "singleinputsearch-input-submissions";
				vm.DefaultSearchMagGlassClass = "singleinputsearch-magglass-submissions";
			}
			else
			{
				vm.DefaultSearchType = "Projects";
				vm.DefaultSearchInputClass = "singleinputsearch-input-projects";
				vm.DefaultSearchMagGlassClass = "singleinputsearch-magglass-projects";
			}

			if (_userContext.User.Environment != Common.Environment.Production)
			{
				var environment = _userContext.User.Environment.ToString().Substring(0, 3).ToUpper();
				vm.NavBarClass = "navbar-" + environment;
				vm.NavBarTitle = vm.NavBarTitle + " " + environment;
			}

			return PartialView("_LayoutNavBar", vm);
		}

		[ChildActionOnly]
		public ActionResult UserInfo()
		{
			var vm = new LayoutViewModel
			{
				Name = _userContext.User.Name
			};

			return PartialView("_LayoutUserInfo", vm);
		}
	}
}