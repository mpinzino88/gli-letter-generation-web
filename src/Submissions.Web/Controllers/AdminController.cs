﻿using System.Web.Mvc;
using Submissions.Common;

namespace Submissions.Web.Controllers
{
	[AuthorizeUser(Permissions = new Permission[] { Permission.Admin, Permission.AdminManager }, HasAccessRight = AccessRight.Edit)]
	public class AdminController : Controller
	{
		public ActionResult Index()
		{
			return View();
		}
	}
}