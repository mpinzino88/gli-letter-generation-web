﻿using System.Web.Mvc;

namespace Submissions.Web.Controllers
{
	public class ErrorController : Controller
	{
		public ActionResult Index(string errorMessage)
		{
			ViewBag.Message = errorMessage;
			return View();
		}
	}
}