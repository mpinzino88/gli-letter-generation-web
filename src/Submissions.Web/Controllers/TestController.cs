﻿using Submissions.Common.Email;
using Submissions.Core.Utility.Email;
using Submissions.Web.Utility.Email;
using System.Net.Mail;
using System.Web.Mvc;

namespace Submissions.Web.Controllers
{
	public class TestController : Controller
	{
		private readonly IEmailService _emailService;

		public TestController(IEmailService emailService)
		{
			_emailService = emailService;
		}

		public ActionResult Emails()
		{
			// Compile GLI.Submissions.Core razor template
			var mailMsg = new MailMessage();
			mailMsg.From = new MailAddress("noreply@gaminglabs.com");
			mailMsg.To.Add(new MailAddress("r.nyman@gaminglabs.com"));
			mailMsg.Subject = "GLI.Submissions.Core Test Email";
			mailMsg.Body = _emailService.RenderTemplate(EmailType.ReassignTasks.ToString(), new ReassignTasksEmail());
			_emailService.Send(mailMsg);

			mailMsg = new MailMessage();
			mailMsg.From = new MailAddress("noreply@gaminglabs.com");
			mailMsg.To.Add(new MailAddress("r.nyman@gaminglabs.com"));
			mailMsg.Subject = "GLI.Submissions.Core Test Task Email";
			mailMsg.Body = _emailService.RenderTemplate("AddTask", new AddTaskEmail());
			_emailService.Send(mailMsg);

			// Compile GLI.Submissions.Web razor template
			mailMsg.Subject = "GLI.Submissions.Web Test Email";
			mailMsg.Body = _emailService.RenderTemplate(EmailType.VerifyGroupLicense.ToString(), new VerifyGroupLicenseEmail { Address = "test" });
			_emailService.Send(mailMsg);

			return new EmptyResult();
		}
	}
}