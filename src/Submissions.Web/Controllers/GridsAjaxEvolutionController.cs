﻿using EF.eResultsManaged;
using Newtonsoft.Json;
using Submissions.Core;
using Submissions.Core.Models.EvolutionService.Query;
using Submissions.Web.Models.Grids.Evolution;
using System.Web.Mvc;
using System.Web.SessionState;

namespace Submissions.Web.Controllers
{
	[SessionState(SessionStateBehavior.ReadOnly)]
	public class GridsAjaxEvolutionController : Controller
	{
		private readonly IeResultsManagedContext _dbeResultsManaged;
		private readonly IEvolutionService _evolutionService;

		public GridsAjaxEvolutionController
		(
			IeResultsManagedContext dbeResultsManaged,
			IEvolutionService evolutionService
		)
		{
			_dbeResultsManaged = dbeResultsManaged;
			_evolutionService = evolutionService;
		}

		public JsonResult RegulationGrid(string customFilters)
		{
			var grid = new RegulationGrid();
			var filters = JsonConvert.DeserializeObject<RegulationSearch>(customFilters);
			var results = _evolutionService.RegulationSearch(filters);
			return grid.Grid.DataBind(results);
		}

		public JsonResult TestScriptHistoryGrid(string testScriptFilters)
		{
			var filters = JsonConvert.DeserializeObject<TestScriptHistorySearch>(testScriptFilters);
			var results = _evolutionService.TestScriptHistorySearch(filters);
			return new TestScriptHistoryGrid().Grid.DataBind(results);
		}
	}
}