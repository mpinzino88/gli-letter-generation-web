using AutoMapper.QueryableExtensions;
using EF.eResultsManaged;
using EF.Submission;
using Newtonsoft.Json;
using Submissions.Common;
using Submissions.Common.Linq.Dynamic;
using Submissions.Core;
using Submissions.Web.Models.Grids;
using Submissions.Web.Models.Grids.Documents;
using Submissions.Web.Models.Grids.GamingGuidelines;
using Submissions.Web.Models.Grids.Lookups;
using Submissions.Web.Models.Grids.PointClick;
using Submissions.Web.Models.Grids.Protrack;
using Submissions.Web.Models.Grids.Reports;
using Submissions.Web.Models.Grids.Submission;
using Submissions.Web.Models.Grids.Tools;
using Submissions.Web.Models.LookupsAjax;
using Submissions.Web.Models.Query;
using Submissions.Web.Models.Query.Protrack;
using Submissions.Web.Models.Query.Reports;
using Submissions.Web.Models.Query.Submission;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.SqlServer;
using System.Linq;
using System.Web.Mvc;
using System.Web.SessionState;

namespace Submissions.Web.Controllers
{
	[SessionState(SessionStateBehavior.ReadOnly)]
	public class GridsAjaxController : Controller
	{
		private readonly ISubmissionContext _dbSubmission;
		private readonly IAccountingService _accountingService;
		private readonly IQueryService _queryService;
		private readonly IColumnChooserService _columnChooserService;
		private readonly IWorkloadManagerService _workloadManagerService;
		private readonly IUserContext _userContext;
		private readonly IGamingGuidelineService _gamingGuidelineService;
		private readonly IeResultsManagedContext _dbeResultsManaged;

		public GridsAjaxController
		(
			ISubmissionContext dbSubmission,
			IAccountingService accountingService,
			IQueryService queryService,
			IColumnChooserService columnChooserService,
			IWorkloadManagerService workloadManagerService,
			IUserContext userContext,
			IGamingGuidelineService gamingGuidelineService,
			IeResultsManagedContext dbeResultsManaged
		)
		{
			_dbSubmission = dbSubmission;
			_accountingService = accountingService;
			_queryService = queryService;
			_columnChooserService = columnChooserService;
			_workloadManagerService = workloadManagerService;
			_userContext = userContext;
			_gamingGuidelineService = gamingGuidelineService;
			_dbeResultsManaged = dbeResultsManaged;
		}

		#region Compliance

		public JsonResult LoadCPAdminReviewGrid(string customFilters)
		{
			var filters = JsonConvert.DeserializeObject<AdminReviewSearch>(customFilters);
			var query = _dbSubmission.JurisdictionalData.Where(x => x.Status == JurisdictionStatus.CP.ToString()).AsQueryable();

			if (!string.IsNullOrEmpty(filters.FileNumber))
			{
				query = query.Where(x => x.JurisdictionalDataProjects.Any(y => y.Project.ProjectName.Contains(filters.FileNumber)));
			}

			if (!string.IsNullOrEmpty(filters.ProjectName))
			{
				query = query.Where(x => x.JurisdictionalDataProjects.Any(y => y.Project.ProjectName == filters.ProjectName));
			}

			if (!string.IsNullOrEmpty(filters.ManufacturerShort))
			{
				query = query.Where(x => x.Submission.ManufacturerCode == filters.ManufacturerShort);
			}

			if (!string.IsNullOrEmpty(filters.IdNumber))
			{
				query = query.Where(x => x.Submission.IdNumber.Contains(filters.IdNumber));
			}

			if (!string.IsNullOrEmpty(filters.GameName))
			{
				query = query.Where(x => x.Submission.GameName.Contains(filters.GameName));
			}

			if (!string.IsNullOrEmpty(filters.Version))
			{
				query = query.Where(x => x.Submission.Version.Contains(filters.Version));
			}

			if (!string.IsNullOrEmpty(filters.Jurisdiction))
			{
				query = query.Where(x => x.JurisdictionId == filters.Jurisdiction);
			}

			if (filters.TestLabId > 0)
			{
				query = query.Where(x => x.JurisdictionalDataProjects.Any(y => y.Project.LabId == filters.TestLabId));
			}

			var results = query.Select(x => new
			{
				JurisdictionInfo = x,
				SubmissionInfo = x.Submission,
				ProjectInfo = (x.JurisdictionalDataProjects.Count > 0) ? x.JurisdictionalDataProjects.OrderByDescending(y => y.AddDate).Select(y => y.Project).FirstOrDefault() : null,
			}).Select(x => new AdminReviewJurisdictionGridRecord()
			{
				Id = x.JurisdictionInfo.Id,
				FileNumber = x.SubmissionInfo.FileNumber,
				IdNumber = x.SubmissionInfo.IdNumber,
				GameName = x.SubmissionInfo.GameName,
				Version = x.SubmissionInfo.Version,
				JurisdictionId = x.JurisdictionInfo.JurisdictionId,
				JurisdictionName = x.JurisdictionInfo.JurisdictionName,
				Status = x.JurisdictionInfo.Status,
				ProjectId = x.ProjectInfo.Id,
				ProjectName = x.ProjectInfo.ProjectName,
				ProjectHolder = x.ProjectInfo.Holder,
				ENAcceptDate = x.ProjectInfo.ENAcceptDate,
				ENCompleteDate = x.ProjectInfo.ENCompleteDate,
				QAAcceptDate = x.ProjectInfo.QAAcceptDate,
				QACompleteDate = x.ProjectInfo.QACompleteDate,
				TestLab = x.ProjectInfo.Lab,
				IsProjectMerged = (x.ProjectInfo.MergedWith != null && x.ProjectInfo.MergedWith != "") || _dbSubmission.trProjects.Where(p => p.MergedWith == x.ProjectInfo.ProjectName).Any(),
				ProjectDesc = (x.ProjectInfo.Holder == "ENCP" ? x.ProjectInfo.ProjectName + " (In Engineering)" : x.ProjectInfo.ProjectName + " (In QA)"),

			});

			//var result1 = results.ToList();

			var grid = new AdminReviewGrid();
			return grid.Grid.DataBind(results);
		}

		#endregion

		#region JiraList
		public JsonResult LoadJiraListGrid(int projectId) //if needed create search object later on
		{
			var results = _queryService.GetJiraIssues(projectId)
				.Select(x => new JirasGridRecord
				{
					Id = x.Id,
					Key = x.Key,
					Status = x.Status,
					Summary = x.Summary,
					Resolution = x.Resolution
				});

			var grid = new JirasGrid();
			return grid.Grid.DataBind(results);
		}

		#endregion

		#region Lookups
		public JsonResult LoadAcumaticaBillWithAndCustomerExceptionsGrid() => new AcumaticaBillWithAndCustomerExceptionsGrid().Grid.DataBind(_dbSubmission.tbl_lst_AcumaticaBillwithAndCustomerExceptions.AsNoTracking().AsQueryable().ProjectTo<AcumaticaBillWithAndCustomerExceptionsGridRecord>());
		public JsonResult LoadAcumaticaTenantsIndexGrid()
		{
			var query = _dbSubmission.tbl_lst_AcumaticaTenants.AsNoTracking().AsQueryable();
			var results = query.ProjectTo<AcumaticaTenantsGridRecord>();
			var grid = new AcumaticaTenantsGrid();
			return grid.Grid.DataBind(results);
		}

		public JsonResult LoadAppFeeGrid()
		{
			var query = _dbSubmission.tbl_lst_AppFees.AsNoTracking().AsQueryable();
			var results = query.ProjectTo<AppFeeGridRecord>();
			var grid = new AppFeeGrid();
			return grid.Grid.DataBind(results);
		}

		public JsonResult LoadBillingSheetGrid()
		{
			var results = _dbSubmission.tbl_lst_BillingSheets
						.Select(x => new
						{
							x.Id,
							x.Name,
							x.Labs,
							x.AppFee,
							x.BilledItem1AppFee,
							x.BilledItem2AppFee,
							x.BilledItem3AppFee,
							x.Active,
							x.GetsGatChargeIfApplicable,
							x.Description
						}).ToList()
						.Select(x => new BillingSheetGridRecord()
						{
							Id = x.Id,
							Labs = string.Join("<br>", x.Labs.Select(y => y.Lab.Location).ToList()),
							Name = x.Name,
							Description = x.Description,
							GetsGatChargeIfApplicable = x.GetsGatChargeIfApplicable,
							AppFeeName = (x.AppFee != null) ? x.AppFee.Name : "",
							BilledItem1AppFeeName = (x.BilledItem1AppFee != null) ? x.BilledItem1AppFee.Name : "",
							BilledItem2AppFeeName = (x.BilledItem2AppFee != null) ? x.BilledItem2AppFee.Name : "",
							BilledItem3AppFeeName = (x.BilledItem3AppFee != null) ? x.BilledItem3AppFee.Name : "",
							Active = x.Active
						}).AsQueryable();

			var grid = new BillingSheetGrid();
			return grid.Grid.DataBind(results);
		}

		public JsonResult LoadBusinessOwnersGrid()
		{
			var query = _dbSubmission.tbl_lst_BusinessOwners.AsNoTracking().AsQueryable();
			var results = query.ProjectTo<BusinessOwnersGridRecord>();
			var grid = new BusinessOwnersGrid();
			return grid.Grid.DataBind(results);
		}

		public JsonResult LoadBusinessOwnerUsersGrid(string customFilters)
		{
			var filters = JsonConvert.DeserializeObject<UserSearch>(customFilters);
			var grid = new BusinessOwnerUsersGrid();
			var results = _queryService.UserSearch(filters).ProjectTo<BusinessOwnerUsersGridRecord>();

			return grid.Grid.DataBind(results);
		}

		public JsonResult LoadJurisdictionsIndexGrid()
		{
			var query = _dbSubmission.tbl_lst_fileJuris.AsNoTracking().AsQueryable();
			var results = query.ProjectTo<JurisdictionsGridRecord>();
			var grid = new JurisdictionsGrid();
			return grid.Grid.DataBind(results);
		}

		public JsonResult LoadJurisdictionGroupsGrid()
		{
			var query = _dbSubmission.JurisdictionGroups.AsNoTracking().AsQueryable();
			var results = query.ProjectTo<JurisdictionGroupGridRecord>();
			var grid = new JurisdictionGroupsGrid();
			return grid.Grid.DataBind(results);
		}

		public JsonResult LoadJurisdictionGroupCategoriesGrid()
		{
			var query = _dbSubmission.JurisdictionGroupCategories.AsNoTracking().AsQueryable();
			var results = query.ProjectTo<JurisdictionGroupCategoriesGridRecord>();
			var grid = new JurisdictionGroupCategoriesGrid();
			return grid.Grid.DataBind(results);
		}
		public JsonResult LoadLBDefaultsGrid()
		{
			var results = _dbSubmission.LetterbookDefaultingOptions
						.Select(x => new
						{
							x.Id,
							x.Jurisdictions,
							x.Manufacturers,
							x.SubTypes,
							x.DocumentTypes,
							x.ContractTypes,
							x.BillingParties,
							x.Locations,
							x.Companies,
							x.ApprovalStatus,
							x.Billable
						}).ToList()
						.Select(x => new LBDefaultsGridRecord()
						{
							Id = x.Id,
							Jurisdictions = string.Join("<br>", x.Jurisdictions.Select(y => y.Jurisdiction.Name + " <font style='color:red'><b>" + y.JurisdictionId + "</b></font>").ToList()),
							Manufacturers = string.Join("<br>", x.Manufacturers.Select(y => y.ManufacturerCode).ToList()),
							SubmissionTypes = string.Join("<br>", x.SubTypes.Select(y => y.SubmissionType.Code).ToList()),
							DocumentTypes = string.Join("<br>", x.DocumentTypes.Select(y => y.DocumentType.Description).ToList()),
							ContractTypes = string.Join("<br>", x.ContractTypes.Select(y => y.ContractType.Code).ToList()),
							BillingParties = string.Join("<br>", x.BillingParties.Select(y => y.BillingParty.BillingParty).ToList()),
							LWLocations = string.Join("<br>", x.Locations.Select(y => y.Location.Location).ToList()),
							Companies = string.Join("<br>", x.Companies.Select(y => y.Company.Name).ToList()),
							Status = x.ApprovalStatus,
							Billable = (x.Billable) ? "Yes" : "No"
						}).AsQueryable();

			var grid = new LBDefaultsGrid();
			return grid.Grid.DataBind(results);
		}

		public JsonResult LoadManufacturerGrid()
		{
			var query = _dbSubmission.tbl_lst_fileManu.AsNoTracking().AsQueryable();
			var results = query.ProjectTo<ManufacturerGridRecord>();
			var grid = new ManufacturersGrid();
			return grid.Grid.DataBind(results);
		}

		public JsonResult LoadManufacturerGroupsGrid(string customFilters)
		{
			var filters = JsonConvert.DeserializeObject<ManufacturerGroupSearch>(customFilters);

			var query = _queryService.ManufacturerGroupSearch(filters).AsQueryable();
			var results = query.ProjectTo<ManufacturerGroupsGridRecord>();

			return new ManufacturerGroupsGrid().Grid.DataBind(results);
		}

		public JsonResult LoadManufacturerJurisdictionCodesIndexGrid()
		{
			var query = _dbSubmission.tbl_lst_JurisdictionMappings.AsNoTracking().AsQueryable()
				.OrderBy(x => x.ManufacturerCode).ThenBy(x => x.JurisdictionId);
			var results = query.ProjectTo<ManufacturerJurisdictionCodeMappingRecord>().ToList();
			var grid = new ManufacturerJurisdictionCodeMappingsGrid();
			return grid.Grid.DataBind(results.AsQueryable());
		}

		public JsonResult LoadMobileDevicesGrid()
		{
			var results = _dbSubmission
				.tbl_lst_MobileDevice
				.AsNoTracking()
				.ProjectTo<MobileDeviceGridRecord>()
				.ToList();
			var grid = new MobileDevicesGrid();
			return grid.Grid.DataBind(results.AsQueryable());
		}

		public JsonResult LoadTargetOffSetGrid()
		{
			var query = _dbSubmission.tbl_lst_Juris_TargetOffSet.AsNoTracking().AsQueryable();
			var results = query.ProjectTo<TargetOffSetGridRecord>();
			var grid = new TargetOffSetGrid();
			return grid.Grid.DataBind(results);
		}

		public JsonResult LoadTaskDefinitionsGrid()
		{
			var query = _dbSubmission.trTaskDefs.AsNoTracking().AsQueryable();
			var results = query.ProjectTo<TaskDefinitionsGridRecord>();
			var grid = new TaskDefinitionsGrid();
			return grid.Grid.DataBind(results);
		}

		public JsonResult LoadTaskDocumentsGrid()
		{
			var query = _dbSubmission.trTaskDocs.AsNoTracking().AsQueryable();
			var results = query.ProjectTo<TaskDocumentsGridRecord>();
			var grid = new TaskDocumentsGrid();
			return grid.Grid.DataBind(results);
		}
		public JsonResult LoadTaskTemplateGrid()
		{
			var grid = new TaskTemplateGrid();
			var results = _queryService.TaskTemplateSearch().ProjectTo<TaskTemplateGridRecord>();

			return grid.Grid.DataBind(results);
		}

		public JsonResult LoadTeamsGrid()
		{
			var query = _dbSubmission.tbl_lst_Teams.AsNoTracking().AsQueryable();
			var results = query.ProjectTo<TeamsGridRecord>();
			var grid = new TeamsGrid();
			return grid.Grid.DataBind(results);
		}

		public JsonResult LoadTechnicalStandardsGrid()
		{
			var grid = new TechnicalStandardsGrid();
			var results = _queryService.TechnicalStandardsSearch()
				.Where(tech => tech.Lifecycle == "ACTIVE")
				.Select(x => new
				{
					x.Id,
					x.OriginalId,
					x.Mandatory,
					x.Result,
					JurisdictionIds = x.Jurisdictions.Select(j => j.JurisdictionId).ToList(),
					x.Description,
				})
				.Select(d => d)
				.ToList()
				.Select(x => new TechnicalStandardsGridRecord
				{
					Id = x.Id,
					OriginalId = x.OriginalId,
					Mandatory = x.Mandatory,
					Result = x.Result,
					Jurisdictions = x.JurisdictionIds.Count > 0 ? string.Join(", ", x.JurisdictionIds.ToArray()) : "All Jurisdictions",
					Description = x.Description,
				})
				.Distinct()
				.AsQueryable();

			return grid.Grid.DataBind(results);
		}

		public JsonResult LoadUserGrid(string customFilters)
		{
			var filters = JsonConvert.DeserializeObject<UserSearch>(customFilters);
			var grid = new UsersGrid();
			var results = _queryService.UserSearch(filters).ProjectTo<UserGridRecord>();

			return grid.Grid.DataBind(results);
		}
		public JsonResult LoadTeamUsersGrid(string customFilters)
		{
			var filters = JsonConvert.DeserializeObject<UserSearch>(customFilters);
			var grid = new TeamUsersGrid();
			var results = _queryService.UserSearch(filters).ProjectTo<TeamUsersGridRecord>();

			return grid.Grid.DataBind(results);
		}

		public JsonResult LoadSupplierFocusUsersGrid(string customFilters)
		{
			var filters = JsonConvert.DeserializeObject<UserSearch>(customFilters);
			var grid = new SupplierFocusUsersGrid();
			var results = _queryService.UserSearch(filters).ProjectTo<SupplierFocusUsersGridRecord>();

			return grid.Grid.DataBind(results);
		}
		#endregion

		#region PointClick
		public JsonResult LoadSubmitRequestGrid(string customFilters)
		{
			var filters = JsonConvert.DeserializeObject<SubmitRequestSearch>(customFilters);
			var grid = new SubmitRequestGrid();
			var results = _queryService.SubmitRequestSearch(filters).ProjectTo<SubmitRequestGridRecord>();

			return grid.Grid.DataBind(results);
		}

		public JsonResult LoadTransferRequestGrid(string customFilters)
		{
			var filters = JsonConvert.DeserializeObject<TransferRequestSearch>(customFilters);
			var grid = new TransferRequestGrid();
			var results = _queryService.TransferRequestSearch(filters).ProjectTo<TransferRequestGridRecord>();

			return grid.Grid.DataBind(results);
		}

		public JsonResult LoadTransferRequestDetailGrid(string customFilters)
		{
			var filters = JsonConvert.DeserializeObject<TransferRequestDetailSearch>(customFilters);
			var grid = new TransferRequestDetailGrid();
			var results = _queryService.TransferRequestDetailSearch(filters).ProjectTo<TransferRequestDetailGridRecord>();

			return grid.Grid.DataBind(results);
		}

		public JsonResult LoadWithdrawRequestGrid(string customFilters)
		{
			var withdrawStatusCancelled = WithdrawStatus.Cancelled.ToString();
			var withdrawStatusPending = WithdrawStatus.Pending.ToString();
			var withdrawStatusProcessed = WithdrawStatus.Processed.ToString();
			var submissionStatusWD = SubmissionStatus.WD.ToString();
			var submissionStatusRJ = SubmissionStatus.RJ.ToString();

			var filters = JsonConvert.DeserializeObject<WithdrawRequestSearch>(customFilters);
			var grid = new WithdrawRequestGrid();
			var results = _queryService.WithdrawRequestSearch(filters)
				.GroupBy(x => x.RequestId)
				.OrderByDescending(x => x.Max(y => y.RequestDate))
				.Select(x => new WithdrawRequestGridRecord
				{
					Id = x.Key,
					SubmissionId = x.Max(y => (int)y.SubmissionId),
					AddUserId = x.Max(y => (int)y.AddUserId),
					QAMemo = x.Max(y => y.QAMemo),
					RequestDate = (DateTime)x.Max(y => y.RequestDate),
					OrderNumber = x.Max(y => y.OrderNumber),
					Status = x.Max(y =>
						y.ProcessDate == null ? withdrawStatusPending :
						y.ProcessDate != null && y.JurisdictionalDataId == null && (y.Submission.Status == submissionStatusWD || y.Submission.Status == submissionStatusRJ) ? withdrawStatusProcessed :
						y.ProcessDate != null && y.JurisdictionalDataId != null && (y.JurisdictionalData.Status == submissionStatusWD || y.JurisdictionalData.Status == submissionStatusRJ) ? withdrawStatusProcessed :
						y.ProcessDate != null && y.JurisdictionalDataId == null && (y.Submission.Status != submissionStatusWD && y.Submission.Status != submissionStatusRJ) ? withdrawStatusCancelled :
						y.ProcessDate != null && y.JurisdictionalDataId != null && (y.JurisdictionalData.Status != submissionStatusWD && y.JurisdictionalData.Status != submissionStatusRJ) ? withdrawStatusCancelled :
						""),
					SubmissionFileNumber = x.Max(y => y.Submission.FileNumber),
					SubmissionIdNumber = x.Max(y => y.Submission.IdNumber),
					SubmissionVersion = x.Max(y => y.Submission.Version),
					SubmissionGameName = x.Max(y => y.Submission.GameName),
					SubmissionManufacturer = x.Max(y => y.Submission.ManufacturerDescription),
					ProcessDate = x.Max(y => y.ProcessDate)
				}).OrderByDescending(x => x.RequestDate);

			return grid.Grid.DataBind(results);
		}

		public JsonResult LoadWithdrawRequestJurisdictionGrid(string customFilters)
		{
			var filters = JsonConvert.DeserializeObject<WithdrawRequestSearch>(customFilters);
			var grid = new WithdrawRequestJurisdictionGrid();
			var results = _queryService.WithdrawRequestSearch(filters)
							.Select(x => new WithdrawRequestJurisdictionGridRecord
							{
								Id = x.Id,
								JurisdictionalDataId = x.JurisdictionalDataId,
								JurisdictionalDataJurisdictionName = x.JurisdictionalData.JurisdictionName,
								JurisdictionalDataStatus = x.JurisdictionalData.Status,
								SubmissionChipType = x.Submission.ChipType,
								SubmissionDateCode = x.Submission.DateCode,
								SubmissionFileNumber = x.Submission.FileNumber,
								SubmissionGameName = x.Submission.GameName,
								SubmissionIdNumber = x.Submission.IdNumber,
								SubmissionManufacturer = x.Submission.ManufacturerDescription,
								SubmissionVersion = x.Submission.Version
							});
			return grid.Grid.DataBind(results);
		}

		public JsonResult LoadWithdrawRequestRecordGrid(string customFilters)
		{
			var filters = JsonConvert.DeserializeObject<WithdrawRequestSearch>(customFilters);
			var grid = new WithdrawRequestRecordGrid();
			var results = _queryService.WithdrawRequestSearch(filters)
						.Select(x => new WithdrawRequestRecordGridRecord
						{
							Id = x.Id,
							SubmissionId = x.SubmissionId,
							SubmissionChipType = x.Submission.ChipType,
							SubmissionDateCode = x.Submission.DateCode,
							SubmissionFileNumber = x.Submission.FileNumber,
							SubmissionGameName = x.Submission.GameName,
							SubmissionIdNumber = x.Submission.IdNumber,
							SubmissionManufacturer = x.Submission.ManufacturerDescription,
							SubmissionVersion = x.Submission.Version,
							SubmissionStatus = x.Submission.Status
						});
			return grid.Grid.DataBind(results);
		}
		#endregion

		#region Protrack
		public JsonResult LoadMathReviewGrid(string customFilters)
		{
			var filters = JsonConvert.DeserializeObject<MathSearch>(customFilters);
			var resultsReview = _queryService.MathReviewSearch(filters)
								.Select(x => new MathReviewGridRecord
								{
									Id = x.Id,
									ProjectId = x.ProjectId,
									ProjectName = x.Project.ProjectName,
									Theme = x.Themes,
									DateRequested = x.Submitted,
									DateSubmitted = x.Project.AddDate,
									CompleteDateRequested = x.CompletionRequested,
									ReviewType = MathReviewType.Submit.ToString(),
									Type = x.GameType,
									Location = x.Project.Lab,
									IsRush = (_dbSubmission.QABundles.Where(y => y.ProjectId == x.ProjectId).Select(q => q.IsRush).FirstOrDefault() == 1) ? true : false,
									OnHold = (x.Project.OnHold == 1) ? true : false,
									Status = (x.Project.OnHold == 1) ? x.Project.ENStatus + " (" + ProjectStatus.OH.ToString() + ")" : x.Project.ENStatus,
									ProtrackTime = ((_dbSubmission.trTasks.Where(y => y.ProjectId == x.ProjectId && y.DepartmentId == (int)Department.MTH).Select(y => y.EstimateHours).Sum() ?? 0) == 0
													? _dbSubmission.trQuotedHours.Where(y => y.ProjectId == x.ProjectId && y.DeactivateDate == null && y.Project.ENStatus == ProjectStatus.UU.ToString()).Select(y => y.Hours).FirstOrDefault()
													: _dbSubmission.trTasks.Where(y => y.ProjectId == x.ProjectId && y.DepartmentId == (int)Department.MTH).Select(y => y.EstimateHours).Sum()),
									ReviewTaskTime = _dbSubmission.trTasks.Where(y => y.Id == x.TaskId).Select(y => y.EstimateHours).Sum(),
									ProgramType = x.ProgramType,
									TargetDate = x.Project.TargetDate,
									ReviewStatus = x.Status,
									CompletedBy = x.CompletedBy.FName + " " + x.CompletedBy.LName,
									IsMathRush = x.IsMathRush,
									CompleteDate = x.Completed,
									IsPeerReview = false,
									EstimateCompleteDate = x.EstimateCompleteDate
								});

			var resultsComplete = _queryService.MathCompleteSearch(filters)
								.Select(x => new MathReviewGridRecord
								{
									Id = x.Id,
									ProjectId = x.ProjectId,
									ProjectName = x.Project.ProjectName,
									Theme = x.MathSubmit.Themes,
									DateRequested = x.Submitted,
									DateSubmitted = x.Project.AddDate,
									CompleteDateRequested = x.Project.MAEstimateCompleteDate == null ? x.MathSubmit.CompletionRequested : x.Project.MAEstimateCompleteDate,
									ReviewType = MathReviewType.Review.ToString(),
									Type = x.MathSubmit.GameType,
									Location = x.Project.Lab,
									IsRush = (_dbSubmission.QABundles.Where(y => y.ProjectId == x.ProjectId).Select(q => q.IsRush).FirstOrDefault() == 1) ? true : false,
									OnHold = (x.Project.OnHold == 1) ? true : false,
									Status = (x.Project.OnHold == 1) ? x.Project.ENStatus + " (" + ProjectStatus.OH.ToString() + ")" : x.Project.ENStatus,
									ProtrackTime = ((_dbSubmission.trTasks.Where(y => y.ProjectId == x.ProjectId && y.DepartmentId == (int)Department.MTH).Select(y => y.EstimateHours).Sum() ?? 0) == 0
													? _dbSubmission.trQuotedHours.Where(y => y.ProjectId == x.ProjectId && y.DeactivateDate == null && y.Project.ENStatus == ProjectStatus.UU.ToString()).Select(y => y.Hours).FirstOrDefault()
													: _dbSubmission.trTasks.Where(y => y.ProjectId == x.ProjectId && y.DepartmentId == (int)Department.MTH).Select(y => y.EstimateHours).Sum()),
									ReviewTaskTime = _dbSubmission.trTasks.Where(y => y.Id == x.TaskId).Select(y => y.EstimateHours).Sum(),
									ProgramType = x.MathSubmit.ProgramType,
									TargetDate = x.Project.TargetDate,
									ReviewStatus = x.Status,
									CompletedBy = x.CompletedBy.FName + " " + x.CompletedBy.LName,
									IsMathRush = x.IsReviewRush,
									CompleteDate = x.Completed,
									IsPeerReview = x.IsPeerReview,
									EstimateCompleteDate = null
								});

			var results = resultsReview.Union(resultsComplete);
			var grid = new MathReviewGrid();
			return grid.Grid.DataBind(results);
		}

		public JsonResult LoadOHApplyAllGrid()
		{
			var jurisdictionGrid = new Areas.Protrack.Models.ApplyToAllGrid();
			return jurisdictionGrid.Grid.DataBind();
		}

		public JsonResult LoadOHJurisdictionGrid(string customFilters)
		{
			var filters = JsonConvert.DeserializeObject<Areas.Protrack.Models.OHSearch>(customFilters);

			var results = (from s in _dbSubmission.trProjectJurisdictionalData
						   where s.ProjectId == filters.ProjectId
						   select (new Areas.Protrack.Models.SubmissionJurisdictionGridModel
						   {
							   SubmissionId = (int)s.JurisdictionalData.SubmissionId,
							   PrimaryId = (int)s.JurisdictionalData.Id,
							   IdNumber = s.JurisdictionalData.Submission.IdNumber,
							   Version = s.JurisdictionalData.Submission.Version,
							   GameName = s.JurisdictionalData.Submission.GameName,
							   DateCode = s.JurisdictionalData.Submission.DateCode,
							   JurisdictionStatus = s.JurisdictionalData.Status,
							   SubmissionStatus = s.JurisdictionalData.Submission.Status,
							   Jurisdiction = s.JurisdictionalData.JurisdictionName,
							   JurisdictionId = s.JurisdictionalData.JurisdictionId,
							   JurRcvdDate = s.JurisdictionalData.ReceiveDate
						   })).Distinct().OrderBy(x => x.IdNumber).AsQueryable();

			if (filters.JurisdictionIDList != null && filters.JurisdictionIDList.Count > 0)
			{
				results = results.Where(r => filters.JurisdictionIDList.Contains(r.JurisdictionId)).AsQueryable();
			}
			if (filters.SubmissionIDList != null && filters.SubmissionIDList.Count > 0)
			{
				results = results.Where(r => filters.SubmissionIDList.Contains(r.SubmissionId)).AsQueryable();
			}
			if (filters.OnHold == true)
			{
				results = results.Where(r => r.JurisdictionStatus == JurisdictionStatus.OH.ToString());
			}

			if (filters.OHPeriodFrom != null || filters.OHPeriodTo != null)
			{
				if (filters.OHPeriodFrom == null)
				{
					var rcvd = _dbSubmission.trProjects.Where(x => x.Id == filters.ProjectId).Select(x => x.ReceiveDate).SingleOrDefault();
					filters.OHPeriodFrom = rcvd;
				}

				if (filters.OHPeriodTo == null)
				{
					filters.OHPeriodTo = DateTime.Today;
				}

				var ohHistory = (from h in _dbSubmission.trOH_Histories
								 join r in _dbSubmission.trProjectJurisdictionalData on h.JurisdictionalDataId equals r.JurisdictionalDataId
								 where (r.ProjectId == filters.ProjectId)
								 && ((h.StartDate > filters.OHPeriodFrom && h.StartDate < filters.OHPeriodTo) ||
										 (h.EndDate > filters.OHPeriodFrom && h.EndDate < filters.OHPeriodTo) ||
										 (h.StartDate < filters.OHPeriodFrom && h.EndDate > filters.OHPeriodTo) ||
										 (filters.OHPeriodFrom >= h.StartDate && filters.OHPeriodTo < h.EndDate) ||
										 (filters.OHPeriodFrom > h.StartDate && filters.OHPeriodTo <= h.EndDate) ||
										 (filters.OHPeriodFrom == h.StartDate && filters.OHPeriodTo == h.EndDate))
								 select h.JurisdictionalDataId).ToList();
				results = results.Where(r => ohHistory.Contains(r.PrimaryId)).AsQueryable();
			}

			var jurisdictionGrid = new Areas.Protrack.Models.SubmissionJurisdictionGrid(true);
			return jurisdictionGrid.Grid.DataBind(results);
		}

		public JsonResult LoadOHSubmissionGrid(string customFilters)
		{
			var filters = JsonConvert.DeserializeObject<Areas.Protrack.Models.OHSearch>(customFilters);

			var results = (from s in _dbSubmission.trProjectJurisdictionalData
						   where s.ProjectId == filters.ProjectId
						   select (new Areas.Protrack.Models.SubmissionJurisdictionGridModel
						   {
							   SubmissionId = (int)s.JurisdictionalData.SubmissionId,
							   IdNumber = s.JurisdictionalData.Submission.IdNumber,
							   Version = s.JurisdictionalData.Submission.Version,
							   GameName = s.JurisdictionalData.Submission.GameName,
							   DateCode = s.JurisdictionalData.Submission.DateCode,
							   SubmissionStatus = s.JurisdictionalData.Submission.Status,
							   SubRcvdDate = s.JurisdictionalData.Submission.ReceiveDate
						   })).Distinct().AsQueryable();

			if (filters.SubmissionIDList != null && filters.SubmissionIDList.Count > 0)
			{
				results = results.Where(r => filters.SubmissionIDList.Contains(r.SubmissionId)).AsQueryable();
			}
			if (filters.OnHold == true)
			{
				results = results.Where(r => r.SubmissionStatus == SubmissionStatus.OH.ToString());
			}

			if (filters.OHPeriodFrom != null || filters.OHPeriodTo != null)
			{
				if (filters.OHPeriodFrom == null)
				{
					var rcvd = _dbSubmission.trProjects.Where(x => x.Id == filters.ProjectId).Select(x => x.ReceiveDate).SingleOrDefault();
					filters.OHPeriodFrom = rcvd;
				}

				if (filters.OHPeriodTo == null)
				{
					filters.OHPeriodTo = DateTime.Today;
				}

				var ohHistory = (from h in _dbSubmission.trOH_Histories
								 join r in _dbSubmission.trProjectJurisdictionalData on h.SubmissionId equals r.JurisdictionalData.SubmissionId
								 //where (r.ProjectId == filters.ProjectId) && (h.StartDate > filters.OHPeriodFrom || filters.OHPeriodFrom == null) && (h.EndDate < filters.OHPeriodTo || filters.OHPeriodTo == null)
								 where (r.ProjectId == filters.ProjectId)
								  && //((filters.OHPeriodFrom >= h.StartDate && filters.OHPeriodFrom <= h.EndDate || filters.OHPeriodFrom == null) && (filters.OHPeriodTo >= h.StartDate && filters.OHPeriodTo <= h.EndDate || filters.OHPeriodTo == null)
								 ((h.StartDate > filters.OHPeriodFrom && h.StartDate < filters.OHPeriodTo) ||
														   (h.EndDate > filters.OHPeriodFrom && h.EndDate < filters.OHPeriodTo) ||
														   (h.StartDate < filters.OHPeriodFrom && h.EndDate > filters.OHPeriodTo) ||
														   (filters.OHPeriodFrom >= h.StartDate && filters.OHPeriodTo < h.EndDate) ||
														   (filters.OHPeriodFrom > h.StartDate && filters.OHPeriodTo <= h.EndDate) ||
														   (filters.OHPeriodFrom == h.StartDate && filters.OHPeriodTo == h.EndDate))
								 select h.SubmissionId
								).ToList();
				results = results.Where(r => ohHistory.Contains(r.SubmissionId)).AsQueryable();
			}

			var jurisdictionGrid = new Areas.Protrack.Models.SubmissionJurisdictionGrid(false);
			return jurisdictionGrid.Grid.DataBind(results);
		}

		public JsonResult LoadProjectGrid(string customFilters, string selColumns = null)
		{
			var filters = JsonConvert.DeserializeObject<ProjectSearch>(customFilters);

			var testETATechReviewCode = Tasks.TestETATechReviewOut.GetEnumDescription();
			var techReview = Tasks.TechReview.GetEnumDescription();
			var results = _queryService.ProjectSearch(filters)
				.Select(x => new ProjectGridRecord
				{
					Id = x.Id,
					StatusSort = x.StatusSort,
					AcceptDate = x.EngAcceptDate,
					CompleteDate = x.EngCompleteDate,
					SubmissionDateCode = x.SubmissionDateCode,
					SubmissionGameName = x.SubmissionGameName,
					SubmissionIdNumber = x.SubmissionIdNumber,
					SubmissionVersion = x.SubmissionVersion,
					TestingType = x.TestingType,
					EstCompleteDate = x.EstCompleteDate,
					ReceiveDate = x.ReceiveDate,
					Status = x.Status,
					BundleStatus = x.BundleStatus,
					StatusDescription = x.StatusDescription,
					FileNumber = x.ProjectName,
					Tasks = x.Tasks,
					ReqQADelivery = x.ReqQADelivery,
					TargetDate = x.TargetDate,
					MissingTarget = x.MissingTarget,
					IsRush = (x.IsRush == 1) ? true : false,
					QATasks = x.QATasks,
					QAAcceptDate = x.QAAcceptDate,
					QAReceiveDate = x.QAReceivedDate,
					QALWStarted = x.QALWStarted,
					QAStatusDescription = x.QAStatusDescription,
					QAProjectIndicator = x.QAProjectIndicator,
					QAStatusSort = x.QAStatusSort,
					IsTransfer = x.IsTransfer,
					Laboratory = x.Laboratory,
					TestLab = x.TestLab,
					CreateDate = x.CreateDate,
					Manufacturer = x.Manufacturer,
					EstTestCompleteDate = x.EstTestCompleteDate,
					EstMathCompleteDate = x.EstMathCompleteDate,
					IsEstMathCompleteTBD = x.IsTBDEstMathCompleteDate,
					OrgTargetDate = x.OrgTargetDate,
					DaysToTarget = x.DaysToTarget,
					ProjectAge = x.ProjectAge,
					Manager = x.Manager,
					TotalOHTime = x.TotalOHTime,
					OHStartDate = x.OHStartDate,
					ProjectLead = x.ProjectLead,
					MAProjectLead = x.MAProjectLead,
					QALetterWriter = x.QALetterWriter,
					Complexity = x.Complexity.ToString(),
					ContractNumber = x.ContractNumber,
					MasterFile = (x.IsTransfer == true) ? null : _dbSubmission.Submissions.Where(s => s.FileNumber == x.FileNumber && s.MasterFileNumber != null).Select(s => s.MasterFileNumber).FirstOrDefault(),
					TaskStartDate = x.TaskStartDate,
					EstHours = x.EstHours,
					ActHours = x.ActHours,
					NoCharge = x.NoCharge,
					BudgetPercentage = x.BudgetPercentage,
					TargetDateWithOH = x.TargetDateWithOH,
					DevRep = x.DevRep,
					QATime = x.QATime,
					QASupervisor = x.QASupervisor,
					Quoted = x.Quoted,
					GlimpseFilter = x.GlimpseCustomFilter,
					QACompleteDate = x.QACompleteDate,
					EngNotes = x.EngNotes,
					TotalTasks = x.ProjectTasks.Count(),
					ManufFilter = x.Manufacturer,
					ProjectLeadFilter = x.ProjectLead,
					MAProjectLeadFilter = x.MAProjectLead,
					DeclineReason = x.DeclineReason,
					DeclineRequestor = x.DeclineRequestor,
					ProjectHolder = x.Holder,
					QALWLocation = x.QALWLocation,
					QAReviewer = x.QAReviewer,
					BundleTaskTarget = x.BundleTaskTarget,
					MathCompleteDate = x.MathCompleteDate,
					ENTestCompleteDate = x.ENTestCompleteDate,
					CertLab = x.CertLab,
					Jurisdiction = (x.Jurisdiction != null) ? _dbSubmission.tbl_lst_fileJuris.Where(j => j.Id == x.Jurisdiction).Select(j => j.Name).FirstOrDefault() + "(" + x.Jurisdiction + ")" : null,
					ENInvolvementWhileFL = x.ENInvolvementWhileFL,
					ReviewOpen = _dbSubmission.tbl_Reviews.Where(r => r.ProjectId == x.Id && r.Status != ProjectReviewStatus.Mediation.ToString()).Count().ToString(),
					ETATask = x.ProjectTasks
						.Where(t => (t.Definition.Code == testETATechReviewCode || t.Definition.Code == techReview) && t.ETAQueue)
						.Select(t =>
							new ETATaskViewModel()
							{
								UserId = t.UserId,
								StartDate = t.StartDate,
								CompleteDate = t.CompleteDate
							})
						.FirstOrDefault(),
					QAOffice = x.QAOffice
				});

			var grid = new ProjectGrid();
			if (selColumns != null) { grid.InitializeSelColumns(selColumns); }

			var finalGrid = grid.Grid.DataBind(results);
			return finalGrid;
		}

		public JsonResult LoadProjectNoteGrid(string customFilters)
		{
			var filters = JsonConvert.DeserializeObject<ProjectNoteSearch>(customFilters);
			var results = _queryService.ProjectNoteSearch(filters)
				.Select(x => new ProjectNoteGridRecord
				{
					Id = x.Id,
					AddDate = x.AddDate,
					Department = x.Dept,
					Note = x.Note,
					Scope = x.Scope,
					User = (x.User.FName + " " + x.User.LName).Trim(),
					UserId = x.UserId
				});

			var grid = new ProjectNoteGrid();
			return grid.Grid.DataBind(results);
		}

		public JsonResult LoadProjectDetailNoteGrid(string customFilters)
		{
			var filters = JsonConvert.DeserializeObject<ProjectNoteSearch>(customFilters);
			var results = _queryService.ProjectAllNoteSearch(filters)
				.Select(x => new ProjectNoteGridRecord
				{
					Id = x.NoteId,
					AddDate = x.AddDate,
					Department = x.Dept,
					Note = x.Note,
					Scope = x.Scope,
					User = x.User,
					UserId = x.UserId,
					ForeignProject = x.ForeignProject,
					NoteType = x.NoteType
				});

			var grid = new ProjectNoteGrid();
			return grid.Grid.DataBind(results);
		}

		public JsonResult LoadBundleTasksGrid(string customFilters)
		{
			var filters = JsonConvert.DeserializeObject<TaskSearch>(customFilters);
			var results = _queryService.QATaskSearch(filters)
						.Select(x => new TasksGridRecord
						{
							Id = x.Id,
							ProjectId = x.ProjectId,
							IsRush = (x.IsRush == 1) ? true : false,
							User = (x.User.FName + " " + x.User.LName).Trim(),
							ProjectName = x.Project.ProjectName,
							GameName = x.GameName,
							Name = x.Name,
							AssignDate = x.AssignDate,
							StartDate = x.StartDate,
							CompleteDate = x.CompleteDate,
							TargetDate = x.TargetDate,
							ProjectTargetDate = x.Project.TargetDate,
							EstMathCompletionDate = x.Project.MAEstimateCompleteDate,
							ProjectStatus = (x.Project.OnHold == 1) ? x.Project.ENStatus + " (" + ProjectStatus.OH.ToString() + ")" : x.Project.ENStatus,
							Manager = x.Project.ENManager.FName + " " + x.Project.ENManager.LName,
							MathSupervisor = "",
							QAReviewer = x.QABundle.QAReviewer.FName + " " + x.QABundle.QAReviewer.LName,
							EstCompleteDate = x.EstimateEndDate,
							ProjectHolder = x.ProjectHolder,
							BundleStatus = x.BundleStatus,
							ProjectIndicator = x.ProjectIndicator,
							ProjectTargetDiff = x.ProjectTargetDiff,
							BundleType = x.BundleType,
							QATaskStatus = x.QATaskStatus,
							QATaskOrder = x.QAMyTaskOrder,
							QAAccepted = x.QABundle.QAAcceptDate,
							AssignedBy = x.AddUser.FName + " " + x.AddUser.LName,
							BundleId = x.BundleId,
							IsBundleResubmission = x.ReSubmission,
							UserId = x.User.Id,
							JurisdictionId = _dbSubmission.vw_Projects.Where(i => i.Id == x.ProjectId).Select(j => j.Jurisdiction).FirstOrDefault(),
							ENInvolvementWhileFL = x.ENInvolvementWhileFL
						})
						.ToList();
			foreach (var item in results)
			{
				if (item.JurisdictionId != null)
					item.JurisdictionName = _dbSubmission.tbl_lst_fileJuris
						.SingleOrDefault(x => x.Id == item.JurisdictionId)
						.Name + "(" + item.JurisdictionId + ")";
			}

			var grid = new TasksGrid();
			return grid.Grid.DataBind(results.AsQueryable());
		}

		public JsonResult LoadProjectTasksGrid(string customFilters)
		{
			var filters = JsonConvert.DeserializeObject<TaskSearch>(customFilters);

			var results = _queryService.TaskSearch(filters)
						.Select(x => new TasksGridRecord
						{
							Id = x.Id,
							TaskDefinitionId = x.TaskDefinitionId,
							Department = x.Department.Code,
							ProjectId = x.ProjectId,
							IsRush = (x.IsRush == 1) ? true : false,
							UserId = x.UserId,
							User = (x.User.FName + " " + x.User.LName).Trim(),
							ProjectName = x.Project.ProjectName,
							GameName = x.GameName,
							IdNumber = x.IdNumber,
							EstimateHours = x.EstimateHours,
							Name = x.Name,
							Description = x.Description,
							DynamicsID = x.Definition.DynamicsId,
							AssignDate = x.AssignDate,
							StartDate = x.StartDate,
							CompleteDate = x.CompleteDate,
							TargetDate = x.TargetDate,
							ProjectTargetDate = x.Project.TargetDate,
							EstMathCompletionDate = x.Project.MAEstimateCompleteDate,
							ProjectStatus = (x.Project.OnHold == 1) ? x.Project.ENStatus + " (" + ProjectStatus.OH.ToString() + ")" : x.Project.ENStatus,
							Manager = x.Project.ENManager.FName + " " + x.Project.ENManager.LName,
							AssignedBy = x.AddUser.FName + " " + x.AddUser.LName,
							MathSupervisor = "",
							QAReviewer = "",
							Status = x.TaskStatus,
							AssociatedJur = x.AssignedJur,
							OtherEngTasks = x.OtherEngTasks,
							EstCompleteDate = x.EstimateCompleteDate,
							ProjectHolder = x.ProjectHolder,
							ProjectIndicator = x.ProjectIndicator,
							ProjectTargetDiff = x.ProjectTargetDiff,
							BundleType = x.BundleType,
							EngTaskOrder = x.EngMyTaskOrder,
							QADeliveryDate = x.Project.QADeliveryDate
						});

			var grid = new TasksGrid();
			return grid.Grid.DataBind(results);
		}

		public JsonResult LoadProjectForeignTaskWithActualHoursGrid(string customFilters)
		{
			var filters = JsonConvert.DeserializeObject<TaskSearch>(customFilters);

			if (filters.ProjectId == null)
			{
				filters.ProjectId = 0;//can not be run without projectid filter
			}

			var query = "exec GetProjectTasksWithActualHours " + filters.ProjectId + "," + (int)TasksWithActualHours.ProjectForeign;
			var results = _dbSubmission.Database.SqlQuery<TasksGridRecord>(query).AsQueryable();

			var grid = new ProjectTaskForeignGrid();
			return grid.Grid.DataBind(results);
		}

		public JsonResult LoadProjectReviewGrid(string customFilters)
		{
			var reviewUserType = _dbSubmission.tbl_lst_ReviewUserType
				.Select(x => new
				{
					x.UserType,
					x.Id
				})
				.ToDictionary(
					x => x.UserType,
					x => x.Id);
			var revieweeId = reviewUserType[ReviewUserType.Reviewee.GetEnumDescription()];
			var reviewerId = reviewUserType[ReviewUserType.Reviewer.GetEnumDescription()];
			var letterWriterId = reviewUserType[ReviewUserType.LetterWriter.GetEnumDescription()];

			var filters = JsonConvert.DeserializeObject<ProjectReviewSearch>(customFilters);
			var grid = new ProjectReviewGrid();

			var review = _queryService.ProjectReviewSearch(filters)
						.Select(x => new
						{
							Id = x.Id,
							ProjectId = x.ProjectId,
							FileNumber = x.Project.ProjectName,
							Department = x.Department.Code,
							Description = x.Description,
							Status = x.Status,
							Assignee = x.ReviewUsers.Where(y => y.UserTypeId == revieweeId).Select(y => y.User.FName + " " + y.User.LName).Distinct().ToList(),
							Reviewer = x.ReviewUsers.Where(y => y.UserTypeId == reviewerId).Select(y => y.User.FName + " " + y.User.LName).Distinct().ToList(),
							Letterwriter = x.ReviewUsers.Where(y => y.UserTypeId == letterWriterId).Select(y => y.User.FName + " " + y.User.LName).Distinct().ToList(),
							AddDate = x.AddDate,
							CompleteDate = x.CompleteDate,
							TargetDate = x.Project.TargetDate
						})
						.Select(d => d)
						.ToList();

			var results = review.Select(x => new ProjectReviewGrid.ProjectReviewGridRecord
			{
				Id = x.Id,
				ProjectId = x.ProjectId,
				FileNumber = x.FileNumber,
				Department = x.Department,
				Description = x.Description,
				Status = x.Status,
				Assignee = String.Join(",", x.Assignee),
				Reviewer = String.Join(",", x.Reviewer),
				Letterwriter = String.Join(",", x.Letterwriter),
				AddDate = x.AddDate,
				CompleteDate = x.CompleteDate,
				TargetDate = x.TargetDate
			})
			.Distinct()
			.AsQueryable();

			return grid.Grid.DataBind(results);
		}

		public JsonResult LoadSubProjectsGrid(string customFilters)
		{
			var filters = JsonConvert.DeserializeObject<SubProjectSearch>(customFilters);

			var completed = SubProjectStatus.Closed.GetEnumDescription();
			var notStarted = SubProjectStatus.NotStarted.GetEnumDescription();
			var inProgress = SubProjectStatus.InProgress.GetEnumDescription();

			var results = _queryService.SubProjectSearch(filters)
				.Select(x => new SubProjectGridRecord
				{
					SubProjectId = x.Id,
					ProjectId = x.ProjectId,
					AnalysisType = x.AnalysisType.Name,
					GameType = x.GameType,
					ProjectName = x.Project.ProjectName,
					ProjectNameCopy = x.Project.ProjectName,
					RequestedCompletion = x.RequestedCompletion,
					Type = x.Type.Code + " - " + x.Type.Description,
					FirstTask = x.Tasks.Select(y => new SubProjectFirstTaskGridRecord
					{
						Id = y.Id,
						AssigneeId = y.User.Id,
						Assignee = y.User.FName + " " + y.User.LName,
						TargetDate = (DateTime)y.TargetDate,
						Status = y.StartDate == null && y.UserId != null ? notStarted :
							y.CompleteDate != null ? completed :
							y.StartDate != null && y.CompleteDate == null ? inProgress :
							""
					})
					.FirstOrDefault()
				}).ToList();

			var grid = new SubProjectGrid();
			return grid.Grid.DataBind(results.AsQueryable());
		}
		#endregion

		public JsonResult LoadDraftLettersGrid(int projectId)
		{
			var results = _dbSubmission.DraftLetters
				.Where(x => x.SubmissionDrafts
					.Any(y => y.JurisdictionalData.JurisdictionalDataProjects
						.Any(z => z.ProjectId == projectId)
					)
				).Select(x =>
					new DraftLettersGridRecord
					{
						ApprovalDate = x.ApprovalDate,
						DraftDate = x.DraftDate,
						FilePath = x.FilePath,
						GPSFilePath = x.GPSFilePath,
						Id = x.Id,
						EditDate = x.EditDate,
						EditUser = x.EditUserId,
						Status = x.Status,
						TimelineReqested = x.IsTimelineRequested,
						TimelineType = x.TimelineType,
						UpdatedDateReason = x.UpdatedDateReason.HasValue ? ((UpdateDateReason)x.UpdatedDateReason).ToString() : null
					}
				);

			var grid = new DraftLettersGrid();
			return grid.Grid.DataBind(results);
		}

		public JsonResult LoadProjectTransferTaskWithActualHoursGrid(string customFilters)
		{
			var filters = JsonConvert.DeserializeObject<TaskSearch>(customFilters);

			if (filters.ProjectId == null)
			{
				filters.ProjectId = 0;//can not be run without projectid filter
			}

			var query = "exec GetProjectTasksWithActualHours " + filters.ProjectId + "," + (int)TasksWithActualHours.ProjectTransfers;

			var results = _dbSubmission.Database.SqlQuery<TasksGridRecord>(query).AsQueryable();

			var grid = new ProjectTaskTransferGrid();
			return grid.Grid.DataBind(results);
		}

		public JsonResult QuickDetailTasks(string customFilters)
		{
			var filters = JsonConvert.DeserializeObject<TaskSearch>(customFilters);

			if (filters.ProjectId == null)
			{
				filters.ProjectId = 0;//can not be run without projectid filter
			}

			var query = "exec [GetQuickDetailTaskInfo] '" + filters.ProjectId + "'";

			var results = _dbSubmission.Database.SqlQuery<TasksGridRecord>(query).AsQueryable();

			var grid = new QuickDetailTaskGrid();
			return grid.Grid.DataBind(results);
		}

		public JsonResult LoadProjectTaskQAGrid(string customFilters)
		{
			var filters = JsonConvert.DeserializeObject<TaskSearch>(customFilters);
			if (filters.ProjectId == null)
			{
				filters.ProjectId = 0;//can not be run without projectid filter
			}

			var query = "SELECT t.TaskID Id, t.taskDefId TaskDefinitionId,";
			query += "Q.projectId ProjectID, ";
			query += "7 DepartmentId,";
			query += "fName + ' ' + lName [User],";
			query += "td.TaskShortDesc [Name],";
			query += "td.TaskDefDesc [Description],";
			query += "'QA'Department,";
			query += "t.CreatedTime As AssignDate,";
			query += "StartDate,";
			query += "t.TargetDate,";
			query += "t.completeDate,";
			query += "convert(decimal(5,2),0) EstimateHours,";
			query += "convert(decimal(5,2),0) OrigEstHours,";
			query += "convert(decimal(5,2),0) ActualHours ";
			query += ",null As DynamicsID,  Convert(bit,0) IsDeleted ";
			query += "FROM QABundles_task AS t ";
			query += "join QABundles q on t.BundleID=q.BundleID ";
			query += "LEFT OUTER JOIN trLogin AS l ON t.userID = l.loginID  ";
			query += "LEFT OUTER JOIN QABundles_taskdef AS td ON td.taskDefId = t.taskDefId   ";
			if (filters.ProjectId != null)
			{
				query += "WHERE q.projectId = " + filters.ProjectId;
			}

			var results = _dbSubmission.Database.SqlQuery<TasksGridRecord>(query).AsQueryable();

			var grid = new ProjectTaskGrid();
			var data = grid.Grid.DataBind(results);
			return grid.Grid.DataBind(results);
		}

		public JsonResult LoadProjectTaskWithActualHoursGrid(string customFilters)
		{
			var filters = JsonConvert.DeserializeObject<TaskSearch>(customFilters);

			if (filters.ProjectId == null)
			{
				filters.ProjectId = 0;//can not be run without projectid filter
			}

			var query = "exec GetProjectTasksWithActualHours " + filters.ProjectId + "," + (int)TasksWithActualHours.Project;
			var results = _dbSubmission.Database.SqlQuery<TasksGridRecord>(query).AsQueryable();

			var grid = new ProjectTaskGrid();
			var data = grid.Grid.DataBind(results);
			return grid.Grid.DataBind(results);
		}

		public JsonResult LoadProjectMetricsGrid(string customFilters)
		{
			var filters = JsonConvert.DeserializeObject<ProjectMetricsSearch>(customFilters);
			var results = _queryService.ProjectMetricsSearch(filters)
						.Select(x => new ProjectMetricsGridRecord
						{
							Holder = x.Holder,
							Status = x.Status,
							TimeStamp = x.TimeStamp,
							Event = x.Event,
							ChangeDate = x.ChangeDate,
							BusinessDays = x.BusinessDays,
							ProjectId = x.ProjectId,
							DepartmentId = x.DepartmentId
						});

			var grid = new ProjectMetricsGrid();
			return grid.Grid.DataBind(results);
		}

		public JsonResult LoadProjectHistoryLogGrid(string customFilters)
		{
			var filters = JsonConvert.DeserializeObject<ProjectHistoryLogSearch>(customFilters);
			var results = _queryService.ProjectHistoryLogSearch(filters)
				.Select(x => new ProjectHistoryLogGridRecord
				{
					Id = x.Id,
					AddDate = x.AddDate,
					ForeignProject = x.ForeignProject,
					Employee = (x.User == null) ? "System" : x.User.FName + " " + x.User.LName,
					Location = x.User.Location.Location.ToString(),
					ProjectHolder = x.ProjectHolder,
					ProjectStatus = x.ENProjectStatus,
					BundleStatus = "",
					Notes = x.Notes
				});

			var qaResults = _queryService.QABundleHistoryLogSearch(filters)
				.Select(x => new ProjectHistoryLogGridRecord
				{
					Id = x.Id,
					AddDate = x.AddDate,
					ForeignProject = x.ForeignProject,
					Employee = (x.User == null) ? "System" : x.User.FName + " " + x.User.LName,
					Location = x.User.Location.Location.ToString(),
					ProjectHolder = "",
					ProjectStatus = "",
					BundleStatus = x.QAProjectStatus,
					Notes = x.Notes
				});

			results = results.Union(qaResults);
			var grid = new ProjectHistoryLogGrid();
			return grid.Grid.DataBind(results);
		}

		public JsonResult LoadLetterContactsGrid(string customFilters)
		{
			var filters = JsonConvert.DeserializeObject<LetterContactsSearch>(customFilters);
			var grid = new LetterContactsGrid();

			var contact = _queryService.LetterContactsSearch(filters)
						.Select(x => new
						{
							Id = x.Id,
							Name = x.Name,
							ContactTypes = x.ContactTypes.Select(y => y.ContactType.ContactType).ToList(),
							ManuFacturers = x.Manufacturer.Select(y => y.Manufacturer.Description).Distinct().ToList(),
							Jurisdictions = x.Jurisdiction.Select(y => y.Jurisdiction.Name).Distinct().ToList()
						})
						.Select(d => d)
						.ToList();

			var results = contact.Select(x => new LetterContactsGridRecord
			{
				Id = x.Id,
				Name = x.Name,
				ContactType = string.Join(",<br/>", x.ContactTypes),
				ManufacturerList = String.Join(",<br/>", x.ManuFacturers),
				JurisdictionList = String.Join(",<br/>", x.Jurisdictions)
			})
			.Distinct()
			.AsQueryable();

			return grid.Grid.DataBind(results);
		}

		public JsonResult LoadCreateTransferGrid(string customFilters)
		{
			var filters = JsonConvert.DeserializeObject<ProjectJurisdictionItemSearch>(customFilters);
			var results = _queryService.TransfersSearch(filters)
				.Select(x => new CreateTransferGridRecord
				{
					JurId = x.Id,
					SubId = x.SubmissionId,
					FileNumber = x.Submission.FileNumber,
					IDNumber = x.Submission.IdNumber,
					Game = x.Submission.GameName,
					DateCode = x.Submission.DateCode,
					Position = x.Submission.Position,
					Version = x.Submission.Version,
					Status = x.Submission.Status,
					Jurisdiction = x.JurisdictionName,
					ReceiveDate = x.ReceiveDate,
					JurisdictionType = x.Status,
					TargetDate = x.TargetDate,
					IsTransfer = x.TransferAvailable,
					JurNum = x.JurisdictionId,
					BundleExistsForFile = _dbSubmission.trProjects.Where(p => p.FileNumber == x.Submission.FileNumber && p.IsTransfer == true).Any()
				});

			var grid = new CreateTransferGrid();
			return grid.Grid.DataBind(results);
		}

		public JsonResult LoadProjectJurisdictionItemGrid(string customFilters)
		{
			var filters = JsonConvert.DeserializeObject<ProjectJurisdictionItemSearch>(customFilters);
			var results = _queryService.ProjectJurisdictionItemSearch(filters)
			   .Select(x => new ProjectJurisdictionItemGridRecord
			   {
				   JurId = x.JurisdictionalDataId,
				   SubId = x.JurisdictionalData.SubmissionId,
				   FileNumber = x.JurisdictionalData.Submission.FileNumber,
				   IDNumber = x.JurisdictionalData.Submission.IdNumber,
				   Game = x.JurisdictionalData.Submission.GameName,
				   DateCode = x.JurisdictionalData.Submission.DateCode,
				   Position = x.JurisdictionalData.Submission.Position,
				   Version = x.JurisdictionalData.Submission.Version,
				   Status = x.JurisdictionalData.Submission.Status,
				   Jurisdiction = x.JurisdictionalData.JurisdictionName,
				   ReceiveDate = x.JurisdictionalData.ReceiveDate,
				   JurisdictionType = x.JurisdictionalData.Status,
				   ContractType = x.JurisdictionalData.ContractType.Code,
				   TestingType = x.JurisdictionalData.TestingPerformed.Code,
				   TargetDate = x.JurisdictionalData.TargetDate,
				   JurNum = x.JurisdictionalData.JurisdictionId
			   });

			var grid = new ProjectJurisdictionItemGrid();
			return grid.Grid.DataBind(results);
		}

		public JsonResult LoadTargetDateHistoryGrid(int projectId) //if needed later create search object
		{
			var results = _queryService.ProjectTargetDateHistory(projectId)
						.Select(x => new TargetDateGridRecord
						{
							Id = x.Id,
							ModifiedBy = x.ModifiedBy.FName + " " + x.ModifiedBy.LName,
							ModifiedDate = x.ModifiedDate,
							IsDeleted = x.IsDeleted,
							TargetDate = x.TargetDate,
							Note = x.Note,
							Reason = x.TargetDateReasonForChange.Reason

						});

			var grid = new TargetDateGrid();
			return grid.Grid.DataBind(results);
		}

		public JsonResult LoadOHHistoryGrid(int projectId) //May need to be chnaged based on history of keytbl or jur if need
		{
			var results = _queryService.ProjectOHHistory(projectId)
						.Select(x => new OnHoldHistoryGridRecord
						{
							Id = x.Id,
							StartDate = x.StartDate,
							EndDate = x.EndDate,
							IsBackDated = x.IsBackdated,
							ShortDescription = x.ShortDesc,
							Reason = x.Reason

						});

			var grid = new OnHoldHistoryGrid();
			return grid.Grid.DataBind(results);
		}

		public JsonResult LoadResourceFinderGrid(string customFilters)
		{
			var filters = JsonConvert.DeserializeObject<ResourceFinderSearch>(customFilters);

			var userQuery = (from l in _dbSubmission.trLogins
							 .Include(x => x.Department)
							 .Include(x => x.Manager)
							 from ru in _dbSubmission.tbl_ResourceUnavailability.Where(y => y.UserId == l.Id).DefaultIfEmpty()
							 select new { User = l, ResourceUnavailability = ru })
							 .Where(x => x.User.Status == "A" && (x.User.Department.Code == Department.ENG.ToString() || x.User.Department.Code == Department.MTH.ToString()));

			// Workload Balancer Filters
			var includeDepartmentIds = _dbSubmission.trDepts
				.Where(x => x.Code == Department.CP.ToString() || x.Code == Department.MTH.ToString() || x.Code == Department.ENG.ToString())
				.Select(x => x.Id)
				.ToList();

			var excludeDepartmentIds = new List<int>();
			var excludeTaskDefinitionIds = new List<int>();
			var dbFormFilters = _dbSubmission.tbl_FormFilters
				.Where(x => x.UserId == _userContext.User.Id && x.Section == FormFilterSection.WorkloadBalancer.ToString())
				.Select(x => x.Filters)
				.SingleOrDefault();

			var formFilters = new Submissions.Web.Areas.Protrack.Models.WorkloadManager.FiltersViewModel();
			if (dbFormFilters != null)
			{
				formFilters = JsonConvert.DeserializeObject<Submissions.Web.Areas.Protrack.Models.WorkloadManager.FiltersViewModel>(dbFormFilters);
			}

			if (formFilters.ExcludeDepartments.Count > 0)
			{
				excludeDepartmentIds = formFilters.ExcludeDepartments.Select(x => x.Id).ToList();
			}

			if (formFilters.ExcludeTasks.Count > 0)
			{
				excludeTaskDefinitionIds = formFilters.ExcludeTasks.Select(x => (int)x.Id).ToList();
			}

			// Company Filters
			if (filters.ManagerIds.Length > 0)
			{
				var managerDirectReportUserIds = _workloadManagerService.GetManagerDirectReports(filters.ManagerIds.Select(int.Parse).ToList()).Select(x => x.UserId).ToList();
				userQuery = userQuery.Where(x => managerDirectReportUserIds.Contains(x.User.Id));
			}

			if (filters.LocationIds.Length > 0)
			{
				var locationIds = filters.LocationIds.Select(y => (int?)Convert.ToInt32(y)).ToList();
				userQuery = userQuery.Where(x => locationIds.Contains(x.User.LocationId));
			}

			if (filters.ResourceIds.Length > 0)
			{
				var resourceIds = filters.ResourceIds.Select(y => (int?)Convert.ToInt32(y)).ToList();
				userQuery = userQuery.Where(x => resourceIds.Contains(x.User.Id));
			}

			// Project Filters
			if (filters.ManufacturerCodes.Length > 0 || filters.TaskDefinitionIds.Length > 0)
			{
				var userIdQuery = _dbSubmission.trTasks
					.Where(x =>
						x.UserId != null &&
						x.User.Status == "A" &&
						x.EstimateHours > 0 &&
						SqlFunctions.DateDiff("yy", x.CompleteDate, DateTime.Now) < 4);

				if (filters.ManufacturerCodes.Length > 0)
				{
					userIdQuery = userIdQuery.Where(x => x.Project.JurisdictionalData.Any(y => filters.ManufacturerCodes.Contains(y.JurisdictionalData.Submission.ManufacturerCode)));
				}

				if (filters.TaskDefinitionIds.Length > 0)
				{
					var taskDefinitionIds = filters.TaskDefinitionIds.Select(y => (int?)Convert.ToInt32(y)).ToList();
					userIdQuery = userIdQuery.Where(x => taskDefinitionIds.Contains(x.TaskDefinitionId));
				}

				var projectFilterUserIds = userIdQuery.Select(x => x.UserId).Distinct().ToList();

				userQuery = userQuery.Where(x => projectFilterUserIds.Contains(x.User.Id));
			}

			// Load all users
			var users = userQuery
				.Where(x => includeDepartmentIds.Contains((int)x.User.DepartmentId))
				.Select(x => new ResourceFinderGridRecord
				{
					UserId = x.User.Id,
					UserName = x.User.FName + " " + x.User.LName,
					UserLocationId = (int)x.User.LocationId,
					UserWorkdayHours = x.User.WorkdayHours,
					ManagerId = x.User.ManagerId,
					ManagerName = x.User.Manager.FName + " " + x.User.Manager.LName,
					ManagerOffice = x.User.Manager.Phone,
					ManagerMobile = x.User.Manager.MobilePhone,
					AssignedHours = 0,
					AssignedAndOHHours = 0,
					UnavailableId = x.ResourceUnavailability.Id,
					UnavailableStartDate = x.ResourceUnavailability.StartDate,
					UnavailableEndDate = x.ResourceUnavailability.EndDate,
					InterofficeAssignment = x.ResourceUnavailability.InterofficeAssignment
				})
				.ToList();

			var userIds = users.Select(x => x.UserId).ToList();

			// Load all tasks associated with users
			var taskQuery = _dbSubmission.trTasks
				.Include(x => x.Project)
				.Include(x => x.User.Manager)
				.Where(x =>
					userIds.Contains(x.UserId ?? 0) &&
					x.CompleteDate == null &&
					x.EstimateHours > 0 &&
					x.Project.ENStatus != ProjectStatus.DN.ToString());

			if (excludeDepartmentIds.Count > 0)
			{
				taskQuery = taskQuery.Where(x => !excludeDepartmentIds.Contains(x.DepartmentId ?? 0));
			}

			if (excludeTaskDefinitionIds.Count > 0)
			{
				taskQuery = taskQuery.Where(x => !excludeTaskDefinitionIds.Contains((int)x.TaskDefinitionId));
			}

			if (!string.IsNullOrEmpty(formFilters.IncludeTasksBeginWith))
			{
				taskQuery = taskQuery.Where(x => x.Name.StartsWith(formFilters.IncludeTasksBeginWith));
			}

			// Load tasks and actual hours 
			var tasksTemp = taskQuery.ToList();
			var tasksIds = tasksTemp.Select(x => x.Id).ToList();
			var actualHours = _accountingService.GetActualHours(tasksIds);

			var tasks = (from t in tasksTemp
						 join ah in actualHours on t.Id equals ah.TaskId into ahg
						 from ah2 in ahg.DefaultIfEmpty()
						 group new { t, ah2 } by t.UserId into g
						 select new ResourceFinderGridRecord
						 {
							 UserId = g.Key ?? 0,
							 UserName = g.Max(x => x.t.User.FName + " " + x.t.User.LName),
							 UserLocationId = g.Max(x => (int)x.t.User.LocationId),
							 UserWorkdayHours = g.Max(x => x.t.User.WorkdayHours),
							 ManagerId = g.Max(x => x.t.User.ManagerId),
							 ManagerName = g.Max(x => x.t.User.Manager.FName + " " + x.t.User.Manager.LName),
							 ManagerOffice = g.Max(x => x.t.User.Manager.Phone),
							 ManagerMobile = g.Max(x => x.t.User.Manager.MobilePhone),
							 AssignedHours = g.Where(x => x.t.Project.OnHold == 0 && !x.t.WeekendWork).Sum(x => x.ah2 == null ? 0 : x.t.EstimateHours > x.ah2.ActualHours ? x.t.EstimateHours - x.ah2.ActualHours : 0) ?? 0,
							 AssignedAndOHHours = g.Where(x => (x.t.WeekendWork && x.t.Project.OnHold == 1) || !x.t.WeekendWork).Sum(x => x.ah2 == null ? 0 : x.t.EstimateHours > x.ah2.ActualHours ? x.t.EstimateHours - x.ah2.ActualHours : 0) ?? 0
						 })
						.ToList();

			// Left join users so that results return users without tasks
			var resultsTemp = (from u in users
							   join t in tasks on u.UserId equals t.UserId into grp
							   from t in grp.DefaultIfEmpty()
							   select t ?? u)
						   .ToList();

			// Fill in unavailability data
			var results = (from u in users
						   join rT in resultsTemp on u.UserId equals rT.UserId
						   select new ResourceFinderGridRecord
						   {
							   UserId = rT.UserId,
							   UserName = rT.UserName,
							   UserLocationId = rT.UserLocationId,
							   UserWorkdayHours = rT.UserWorkdayHours,
							   ManagerId = rT.ManagerId,
							   ManagerName = rT.ManagerName,
							   ManagerOffice = rT.ManagerOffice,
							   ManagerMobile = rT.ManagerMobile,
							   AssignedHours = rT.AssignedHours,
							   AssignedAndOHHours = rT.AssignedAndOHHours,
							   UnavailableId = u.UnavailableId,
							   UnavailableStartDate = u.UnavailableStartDate,
							   UnavailableEndDate = u.UnavailableEndDate,
							   InterofficeAssignment = u.InterofficeAssignment
						   })
							.ToList();

			// update dto/holidays/workday schedules
			var dtoRequests = _workloadManagerService.GetDTORequests(userIds);
			var holidays = _workloadManagerService.GetHolidays(userIds);
			var workdaySchedules = _workloadManagerService.GetWorkdaySchedules(0, userIds);

			foreach (var item in results)
			{
				item.DTORequests = dtoRequests.Where(x => x.UserId == item.UserId).ToList();
				item.Holidays = holidays.Where(x => x.LocationId == item.UserLocationId).ToList();
				item.WorkdaySchedules = workdaySchedules.Where(x => x.UserId == item.UserId).ToList();
			}

			if (filters.AvailableDate != null)
			{
				results = results.Where(x => x.AssignedEstCompleteDate < filters.AvailableDate).ToList();
			}

			var grid = new ResourceFinderGrid();
			return grid.Grid.DataBind(results.AsQueryable());
		}

		#region Search
		public JsonResult LoadAdvancedSearchGrid(string customFilters)
		{
			var filters = JsonConvert.DeserializeObject<AdvancedSearchSearch>(customFilters);
			var grid = new AdvancedSearchGrid();
			var results = _queryService.AdvancedSearch(filters).ProjectTo<AdvancedSearchGridRecord>();

			return grid.Grid.DataBind(results);
		}
		#endregion

		#region Submissions
		public JsonResult LoadChangeOfStatusGrid(string fileNumber)
		{
			var results = _queryService.ChangeOfStatusSearch(fileNumber)
						   .Select(x => new ChangeOfStatusGridRecord
						   {
							   FileNumber = x.FileNumber,
							   SubmitDate = x.timeEntered,
							   Id = x.Id,
							   Approved = x.timeProcessed,
							   Submitter = x.Submitter.FName + " " + x.Submitter.LName,
							   CosType = (x.Items.Count > 0) ? x.Items.FirstOrDefault().typeOfChange : "",
							   Status = ((COSStatus)x.status).ToString()

						   });
			var grid = new ChangeOfStatusGrid();
			return grid.Grid.DataBind(results);
		}

		public JsonResult LoadLetterBookGrid(string customFilters)
		{
			var filters = JsonConvert.DeserializeObject<LetterBookSearch>(customFilters);
			var grid = new LetterBookGrid(filters.Department);

			var letterBookData = _queryService.LetterBookSearch(filters)
				.Select(x => new
				{
					x.Id,
					x.BatchNumber,
					BillingUser = x.BillingUser != null ? x.BillingUser.FName + " " + x.BillingUser.LName : "",
					DepartmentCode = x.Department.Code,
					x.DocumentVersion.DocumentId,
					EntryUserName = x.EntryUser.FName + " " + x.EntryUser.LName,
					x.DocumentVersion.FinalLetterGPSItemId,
					LaboratoryLocation = x.Laboratory.Location,
					LetterType = x.DocumentVersion.Document.DocumentType.Description,
					x.Note,
					x.ReportDate,
					x.BallyType,
					Status = x.ACTStatus ?? x.QAStatus,
					BillingSheet = x.BillingSheet,
					DocumentVersion = x.DocumentVersion.Version,
					DocumentVersionLatest = x.DocumentVersion.Document.DocumentVersions.Max(y => y.Version),
					JurisdictionalData = x.DocumentVersion.DocumentVersionLinks
					.Select(y => new
					{
						Id = y.JurisdictionalDataId,
						Jurisdiction = y.JurisdictionalData.JurisdictionName,
						y.Billable,
						BilledCount = y.BilledCount ?? 0,
						y.JurisdictionalData.SubmissionId,
						SubmissionFileNumber = y.JurisdictionalData.Submission.FileNumber,
						JurisdictionName = y.JurisdictionalData.Jurisdiction.Name,
						JurisdictionId = y.JurisdictionalData.Jurisdiction.Id,
						y.ProjectId,
						ProjectFileNumber = y.Project.FileNumber,
						Company = y.JurisdictionalData.Company.DynamicsCompanyId,
						ContractType = y.JurisdictionalData.ContractType.Code,
						Currency = y.JurisdictionalData.Currency.Code
					})
					.ToList()
				})
				.ToList();

			var results = letterBookData
				.Where(x => x.JurisdictionalData.Count > 0)
				.Select(x => new LetterBookGridRecord
				{
					BatchNumber = x.BatchNumber,
					IsBillableJurisdiction = x.JurisdictionalData.Any(y => y.Billable),
					BilledCount = x.JurisdictionalData.Select(y => y.BilledCount).First(),
					BillingUser = x.BillingUser,
					DepartmentCode = x.DepartmentCode,
					DocumentId = x.DocumentId,
					EntryUserName = x.EntryUserName,
					FileNumbers = x.JurisdictionalData.Select(y => new { y.SubmissionId, FileNumber = y.SubmissionFileNumber }).Distinct().ToList<object>(),
					FinalLetterGPSItemId = x.FinalLetterGPSItemId,
					Id = x.Id,
					Jurisdiction = x.JurisdictionalData.Select(y => "<b><font color=navy>" + y.JurisdictionId + "</font></b> - " + y.JurisdictionName).First(),
					LaboratoryLocation = x.LaboratoryLocation,
					JurisdictionId = x.JurisdictionalData.Where(y => y.SubmissionFileNumber == y.ProjectFileNumber).Select(y => y.JurisdictionId).FirstOrDefault(),
					LetterType = x.LetterType,
					Note = x.Note,
					ProjectId = x.JurisdictionalData.Select(y => y.ProjectId).First(),
					ReportDate = x.ReportDate,
					Status = x.Status,
					BillingSheet = x.BillingSheet,
					FileNumber = x.JurisdictionalData.Select(y => y.ProjectFileNumber).First(),
					Version = x.DocumentVersion.ToString(),
					IsLatest = x.DocumentVersion == x.DocumentVersionLatest,
					Company = String.Join(", ", x.JurisdictionalData.Select(y => y.Company).Distinct().ToList()),
					ContractType = String.Join(", ", x.JurisdictionalData.Select(y => y.ContractType).Distinct().ToList()),
					Currency = String.Join(", ", x.JurisdictionalData.Select(y => y.Currency).Distinct().ToList()),
					BallyType = x.BallyType == null ? "N/A" : x.BallyType
				})
				.ToList();

			// filters
			var query = results.AsQueryable();
			if (!string.IsNullOrWhiteSpace(filters.DynamicsFileNumString))
			{
				var replaced = filters.DynamicsFileNumString.Replace("-", "");
				query = query.Where(x => x.DynamicsFileNumber == replaced);
			}

			if (filters.OnlyLatest)
			{
				query = query.Where(x => x.IsLatest);
			}

			results = query.ToList();

			// get letter writer data
			var projectIds = results.Select(x => x.ProjectId).Distinct().ToList();
			var letterWriters = _dbSubmission.QABundles_Task
				.Where(x => projectIds.Contains(x.QABundle.ProjectId) && (x.TaskDefID == (int)BundleTask.LetterWriting || x.TaskDefID == (int)BundleTask.Addendum || x.TaskDefID == (int)BundleTask.Correction))
				.Select(x => new { x.QABundle.ProjectId, Name = x.User.FName + " " + x.User.LName })
				.Distinct()
				.ToList();

			results = (from r in results
					   join lw in letterWriters on r.ProjectId equals lw.ProjectId into temp
					   from lw in temp.DefaultIfEmpty()
					   select new LetterBookGridRecord
					   {
						   BatchNumber = r.BatchNumber,
						   IsBillableJurisdiction = r.IsBillableJurisdiction,
						   BilledCount = r.BilledCount,
						   BillingUser = r.BillingUser,
						   DepartmentCode = r.DepartmentCode,
						   DocumentId = r.DocumentId,
						   EntryUserName = r.EntryUserName,
						   FileNumbers = r.FileNumbers,
						   FinalLetterGPSItemId = r.FinalLetterGPSItemId,
						   Id = r.Id,
						   Jurisdiction = r.Jurisdiction,
						   LaboratoryLocation = r.LaboratoryLocation,
						   JurisdictionId = r.JurisdictionId,
						   LetterType = r.LetterType,
						   Note = r.Note,
						   ProjectId = r.ProjectId,
						   ReportDate = r.ReportDate,
						   Status = r.Status,
						   BillingSheet = r.BillingSheet,
						   FileNumber = r.FileNumber,
						   Version = r.Version,
						   IsLatest = r.IsLatest,
						   Company = r.Company,
						   ContractType = r.ContractType,
						   Currency = r.Currency,
						   LetterWriter = lw == null ? "" : lw.Name,
						   BallyType = r.BallyType
					   })
					  .ToList();

			return grid.Grid.DataBind(results.AsQueryable());
		}

		public JsonResult LoadGamingGuidelineGrid(string customFilters)
		{
			var filters = JsonConvert.DeserializeObject<GamingGuidelineSearch>(customFilters);
			var userTemplate = _dbSubmission.GamingGuidelineUserTemplate.Where(x => x.UserId == _userContext.User.Id).SingleOrDefault() ?? new GamingGuidelineUserTemplate();
			var grid = new GamingGuidelineGrid(userTemplate);
			var results = _queryService.GamingGuidelineSearch(filters)
				.OrderBy(x => x.Jurisdiction.Name)
				.Select(x => new GamingGuidelineGridRecord()
				{
					Jurisdiction = new
					{
						x.Id,
						Name = (x.SpecialName ?? x.Jurisdiction.Name),
						x.JurisdictionId,
						x.NonCompliantPaytableHandling,
						x.WirelessTechnology,
						x.SkillGames,
						x.BonusPot,
						x.RandomNumberGenerator,
						x.GeographicalTestingLocationOnly,
						x.PhysicalTestingLocation,
						x.SpecialMathInstructions,
						x.NewTechnologyReviewNeeded,
						x.CryptographicRNG,
						PrimaryOwner = (x.OwnerId != null ? x.Owner.FName + " " + x.Owner.LName : ""),
						SecondaryOwner = (x.SecondaryOwnerId != null ? x.SecondaryOwner.FName + " " + x.SecondaryOwner.LName : "")
					},
					GamingGuidelineId = x.Id,
					SpecialHandling = x.SpecialHandling ? "Yes" : "No",
					SourceCodeCompilation = x.SourceCodeCompilation ? "Yes" : "No",
					Forensic = x.Forensic ? "Yes" : "No",
					ProgressiveReconciliation = x.ProgressiveReconciliation ? "Yes" : "No",
					OffensiveMaterialEvaluation = x.OffensiveMaterialEvaluation ? "Yes" : "No",
					UnderageAdvertisingEvaluation = x.UnderageAdvertisingEvaluation ? "Yes" : "No",
					ResponsibleGaming = x.ResponsibleGaming ? "Yes" : "No",
					RemoteAccessITL = x.RemoteAccessITL ? "Yes" : "No",
					RemoteAccess = x.RemoteAccess ? "Yes" : "No",
					Portal = x.Portal ? "Yes" : "No",
					NewTechnologyReviewNeeded = x.NewTechnologyReviewNeeded ? "Yes" : "No",
					SpecialMathInstructions = x.SpecialMathInstructions,
					Comments = x.Comments,
					DocumentedRestrictionsOnSkill = x.DocumentedRestrictionsOnSkill,
					NonCompliantPaytableHandling = ((GamingGuidelineNonCompliantPaytableHandling)x.NonCompliantPaytableHandling).ToString(),
					WirelessTechnology = ((GamingGuidelineAllowedProhibited)x.WirelessTechnology).ToString(),
					SkillGames = ((GamingGuidelineAllowedProhibited)x.SkillGames).ToString(),
					BonusPot = ((GamingGuidelineBonusPot)x.BonusPot).ToString(),
					RandomNumberGenerator = ((GamingGuidelineRandomNumberGenerator)x.RandomNumberGenerator).ToString(),
					GeographicalTestingLocationOnly = ((GamingGuidelineGeographicalTestingLocationOnly)x.GeographicalTestingLocationOnly).ToString(),
					PhysicalTestingLocation = ((GamingGuidelinePhysicalTestingLocation)x.PhysicalTestingLocation).ToString(),
					JurId = x.JurisdictionId,
					Country = x.Jurisdiction.Country.Description,
					Attributes = x.GamingGuidelineAttributes.ToList(),
					Limits = x.GamingGuidelineLimits.ToList(),
					OddsRequirements = x.GamingGuidelineOddsRequirements.ToList(),
					ProhibitedGameTypes = x.GamingGuidelineProhibitedGameTypes.ToList(),
					Protocols = x.GamingGuidelineProtocolTypes.ToList(),
					RTPRequirements = x.GamingGuidelineRTPRequirements.ToList(),
					TechnologyTypes = x.GamingGuidelineTechTypes.ToList()
				})
				.ToList();

			foreach (var item in results)
			{
				var jurid = int.Parse(item.JurId);
				item.SportsBettingId = _dbSubmission.GamingGuidelineSportsBetting
					.Where(x => x.JurisdictionId == jurid)
					.Select(x => x.Id)
					.FirstOrDefault();

				item.Jurisdiction = JsonConvert.SerializeObject(new
				{
					jur = item.Jurisdiction,
					sportbetting = item.SportsBettingId
				});
				//Attributes TODODD: replace strings with enum.GetDescription / whatever
				item.ConditionalApproval = _gamingGuidelineService.GetAttrValue(item.Attributes, "Conditional Approval");
				item.ConditionalRevocation = _gamingGuidelineService.GetAttrValue(item.Attributes, "Conditional Revocation");
				item.GAT = _gamingGuidelineService.GetAttrValue(item.Attributes, "GAT");
				item.Interop = _gamingGuidelineService.GetAttrValue(item.Attributes, "Interop");
				item.ProtocolTesting = _gamingGuidelineService.GetAttrValue(item.Attributes, "Protocol Testing");
				item.RequiredSafetyCertifications = _gamingGuidelineService.GetAttrValue(item.Attributes, "Required Safety Certifications (ex UL, CSA, etc)");
				item.NUStatus = _gamingGuidelineService.GetAttrValue(item.Attributes, "NU status");
				// limits
				item.CreditLimit = _gamingGuidelineService.GetLimValue(item.Limits, "Credit Limit");
				item.LinkMaxWin = _gamingGuidelineService.GetLimValue(item.Limits, "Link Max Win");
				item.LinkedSmallEstablishments = _gamingGuidelineService.GetLimValue(item.Limits, "Linked Small Establishments");
				item.MaxBet = _gamingGuidelineService.GetLimValue(item.Limits, "Max Bet");
				item.MaxWin = _gamingGuidelineService.GetLimValue(item.Limits, "Max Win");
				item.MinBet = _gamingGuidelineService.GetLimValue(item.Limits, "Min Bet");
				item.MTGMMaxBet = _gamingGuidelineService.GetLimValue(item.Limits, "MTGM Max Bet");
				item.MTGMMaxWin = _gamingGuidelineService.GetLimValue(item.Limits, "MTGM Max Win");
				item.ProgressiveCap = _gamingGuidelineService.GetLimValue(item.Limits, "Progressive Cap");
				item.ProgressiveMaxWin = _gamingGuidelineService.GetLimValue(item.Limits, "Progressive Max Win");
				item.SAPMaxWin = _gamingGuidelineService.GetLimValue(item.Limits, "SAP Max Win");
				item.WAPMaxWin = _gamingGuidelineService.GetLimValue(item.Limits, "WAP Max Win");
				item.WinCap = _gamingGuidelineService.GetLimValue(item.Limits, "Win Cap");
				//Odds
				item.AllAwardOR = _gamingGuidelineService.GetOdds(item.OddsRequirements, GamingGuidelinePrizeType.AllAward);
				item.AnyAwardOR = _gamingGuidelineService.GetOdds(item.OddsRequirements, GamingGuidelinePrizeType.AnyAward);
				item.TopAwardOR = _gamingGuidelineService.GetOdds(item.OddsRequirements, GamingGuidelinePrizeType.TopAward);
				//Prohibited gametypes
				item.NotAllowedGames = _gamingGuidelineService.GetProhibitedGames(item.ProhibitedGameTypes, GamingGuidelineProhibitedGameTypeStatus.NotAllowed);
				//RTP
				item.MaxRTP = _gamingGuidelineService.GetRTP(item.RTPRequirements, GamingGuidelineRTPType.Max);
				item.MinRTP = _gamingGuidelineService.GetRTP(item.RTPRequirements, GamingGuidelineRTPType.Min);
				item.AverageRTP = _gamingGuidelineService.GetRTP(item.RTPRequirements, GamingGuidelineRTPType.Avg);
				item.WeightedRTP = _gamingGuidelineService.GetRTP(item.RTPRequirements, GamingGuidelineRTPType.WtAvg);
				//Tech types
				item.NotAllowedTech = _gamingGuidelineService.GetTechType(item.TechnologyTypes, GamingGuidelineTechTypeStatus.NotAllowed);
				//Protocols
				item.ProtocolValue = string.Join(", <br/>", item.Protocols.Select(x => x.ProtocolType.Name + " (" + x.Version + ") "));
				//CompilationMethod
				item.CompilationMethod = _gamingGuidelineService.LoadAllCompilationMethods(item.GamingGuidelineId);
			}

			return grid.Grid.DataBind(results.AsQueryable());
		}
		public JsonResult LoadGamingGuidelineSportsBettingGrid(string customFilters)
		{
			var filters = JsonConvert.DeserializeObject<GamingGuidelinesSportsBettingSearch>(customFilters);
			var grid = new GamingGuidelinesSportsBettingGrid();
			var results = _queryService.GamingGuidelineSportsBettingSearch(filters)
				.Select(x => new GamingGuidelinesSportsBettingGridRecord
				{
					Id = x.Id,
					JurisdictionId = x.JurisdictionId,
					LocationId = x.LocationId,
					ITLRequired = x.ITLRequired ? "Yes" : "No",
					AuditRequired = ((SportsBettingSelectOptions)x.AuditRequired).ToString(),
					AuditNote = x.AuditNote,
					LandBased = x.LandBased ? "Yes" : "No",
					Online = x.Online != null ? ((bool)x.Online ? "Yes" : "No") : "Unknown",
					LimitOnEventTypeAllowed = x.LimitOnEventTypeAllowed,
					MaxCaps = x.MaxCaps,
					ChangeManagementSystem = ((SportsBettingSelectOptions)x.ChangeManagementSystem).ToString(),
					DataVaults = ((SportsBettingSelectOptions)x.DataVaults).ToString(),
					DataRetentionRequirement = ((SportsBettingSelectOptions)x.DataRetentionRequirement).ToString(),
					RetentionPeriod = x.RetentionPeriod,
					TaxPercentage = x.TaxPercentage,
					TaxNotes = x.TaxNotes,
					ServerLocationRequirement = ((SportsBettingSelectOptions)x.ServerLocationRequirement).ToString(),
					LiabilityAmountRequired = ((SportsBettingSelectOptions)x.LiabilityAmountRequired).ToString(),
					LiabilityNote = x.LiabilityNote,
					AnonymousPlayAllowed = ((SportsBettingSelectOptions)x.AnonymousPlayAllowed).ToString(),
					GeneralNotes = x.GeneralNotes,
					Comments = x.Comments
				})
				.ToList();

			foreach (var item in results)
			{
				var jurIdString = item.JurisdictionId.ToJurisdictionIdString();
				item.JurisdictionName = _dbSubmission.tbl_lst_fileJuris
					.SingleOrDefault(x => x.Id == jurIdString)
					.Name + " (" + item.JurisdictionId + ")";
				item.Location = _dbSubmission.tbl_lst_fileJuris
					.SingleOrDefault(x => x.Id == jurIdString)
					.Country.Code;
			}

			return grid.Grid.DataBind(results.AsQueryable());
		}

		public JsonResult LoadSoftwareCheckGrid(string customFilters)
		{
			var projectStatusFilter = new List<string>
			{
				ProjectStatus.RC.ToString(),
				ProjectStatus.QA.ToString(),
				ProjectStatus.DN.ToString()
			};

			var courierFilter = ShipmentCourier.Elect.GetEnumDescription();

			var filters = JsonConvert.DeserializeObject<SoftwareCheckSearch>(customFilters);
			var grid = new SoftwareCheckGrid();

			var results = (from s in _dbSubmission.ShippingData
						   join p in _dbSubmission.trProjects on s.FileNumber equals p.FileNumber
						   join b in _dbSubmission.QABundles on p.Id equals b.ProjectId
						   where (s.SoftwareCheckDate == null || s.SoftwareCheckUserId == null) &&
							  p.OnHold == 0 &&
							  projectStatusFilter.Contains(p.ENStatus) &&
							  (s.Courier != courierFilter) &&
							  s.Type == ShipmentType.Receiving.ToString() &&
							  (s.ShippingRecipientCode.Contains(CustomLab.NJ.ToString()) || s.ShippingRecipientCode.Contains(CustomLab.LV.ToString()))
						   group s by s.Id into g
						   select new SoftwareCheckGridRecord
						   {
							   Id = g.Key,
							   FileNumber = g.Max(x => x.FileNumber),
							   ShippingRecipientCode = g.Max(x => x.ShippingRecipientCode),
							   ProcessDateString = g.Max(x => x.ProcessDate),
							   Memo = g.Max(x => x.Memo),
							   SoftwareCheckUser = g.Max(x => x.SoftwareCheckUser.FName + " " + x.SoftwareCheckUser.LName),
							   SoftwareWithEngineering = g.FirstOrDefault().SoftwareWithEngineering ? "Yes" : "No",
							   InitialProject = _dbSubmission.trProjects
								  .Where(x => x.FileNumber == g.Max(y => y.FileNumber) && x.IsTransfer == false)
								  .Select(x => new SoftwareCheckInitialProjectGridRecord { Id = x.Id })
								  .DefaultIfEmpty(new SoftwareCheckInitialProjectGridRecord { Id = 0 })
								  .FirstOrDefault()
						   });

			results = results.Where(x => x.InitialProject.Id > 0);

			if (filters.LocationId != null)
			{
				var location = _dbSubmission.trLaboratories.Where(x => x.Id == filters.LocationId).Select(x => x.Location).Single();
				results = results.Where(x => x.ShippingRecipientCode.Contains(location));
			}

			return grid.Grid.DataBind(results);
		}
		#endregion

		#region Reports
		public JsonResult LoadReportHistoryLogGrid(string customFilters)
		{
			var filters = JsonConvert.DeserializeObject<HistoryLogSearch>(customFilters);
			var grid = new HistoryLogReportGrid();

			var primaryKeys = new List<int>();
			var primarykeysGUID = new List<Guid>();

			if (filters.FileNumber != null) //General Report
			{
				if (filters.Table == HistoryLogTableName.Submissions.ToString())
				{
					var submissionIds = _dbSubmission.Submissions.Where(x => x.FileNumber == filters.FileNumber)
					.Select(x => x.Id).ToList();

					primaryKeys.AddRange(submissionIds);
				}
				else if (filters.Table == HistoryLogTableName.JurisdictionalData.ToString())
				{
					var JurisdictionalDataIds = _dbSubmission.JurisdictionalData.Where(x => x.Submission.FileNumber == filters.FileNumber)
					.Select(x => x.Id).ToList();

					primaryKeys.AddRange(JurisdictionalDataIds);
				}
			}
			else //various history in app
			{
				if (filters.BugBoxId != null)
				{
					primaryKeys.Add((int)filters.BugBoxId);
				}
				else if (filters.JurisdictionalDataId != null)
				{
					if (filters.Table == HistoryLogTableName.Letters.ToString())//Though each entry has a history, we will just show all, that is how it's being done
					{
						var allLetterIds = _dbSubmission.SubmissionLetters
						.Where(x => x.JurisdictionalDataId == filters.JurisdictionalDataId)
						.Select(x => (int)x.LetterId)
						.ToList();

						primaryKeys.AddRange(allLetterIds);
					}
					else if (filters.Table == HistoryLogTableName.DraftLetters.ToString()) //we are doing list but there will be only one
					{
						var draftIds = _dbSubmission.SubmissionDrafts
					   .Where(x => x.JurisdictionDataId == filters.JurisdictionalDataId)
					   .Select(x => (int)x.DraftLetterId)
					   .ToList();

						primaryKeys.AddRange(draftIds);
					}
					else
					{
						primaryKeys.Add((int)filters.JurisdictionalDataId);
					}
				}
				else if (filters.ShippingDataId != null)
				{
					primaryKeys.Add((int)filters.ShippingDataId);
				}
				else if (filters.SignatureId != null)
				{
					primaryKeys.Add((int)filters.SignatureId);
				}
				else if (filters.SourceCodeStorageId != null)
				{
					primaryKeys.Add((int)filters.SourceCodeStorageId);
				}
				else if (filters.SubmissionId != null)
				{
					primaryKeys.Add((int)filters.SubmissionId);
				}
			}

			//this is for tbl_lst_filejuris table
			if (filters.JurisdictionId != null)
			{
				var pkJurisdictionId = _dbSubmission.tbl_lst_fileJuris
					.Where(x => x.Id == filters.JurisdictionId)
					.Select(x => x.PKJurisID)
					.Single();
				primaryKeys.Add(pkJurisdictionId);
			}

			//this is for tbl_lst_filemanu table
			if (!string.IsNullOrEmpty(filters.ManufacturerCode))
			{
				var manufacturerId = _dbSubmission.tbl_lst_fileManu
					.Where(x => x.Code == filters.ManufacturerCode)
					.Select(x => (Guid)x.Id)
					.Single();

				primarykeysGUID.Add(manufacturerId);
			}

			var primaryKeysStr = string.Join(",", primaryKeys);
			var primaryKeysGUIDStr = string.Join(",", primarykeysGUID);
			var query = "exec [dbo].[GetHistoryLogReportData] '" + filters.Table + "','" + filters.Column + "','" + filters.StartDate + "','" + filters.EndDate + "','" + filters.UserId + "','" + filters.ActionType + "','" + primaryKeysStr + "','" + primaryKeysGUIDStr + "'";

			var results = _dbSubmission.Database.SqlQuery<HistoryLogReportGridRecord>(query).AsQueryable();

			return grid.Grid.DataBind(results);
		}

		public JsonResult LoadReportJurisdictionalDataGrid(string customFilters)
		{
			var filters = JsonConvert.DeserializeObject<JurisdictionalDataSearch>(customFilters);
			var savedColumns = _columnChooserService.LoadColumns(ColumnChooserGrid.JurisdictionalDataReport);
			var grid = new JurisdictionalDataReportGrid();
			grid.InitializeDynamicGrid(savedColumns);
			var selectStatement = grid.GetColumnsDynamicLinqFormat();
			var results = _queryService.JurisdictionalDataSearch(filters).GenericSort(filters).Select(selectStatement);

			return grid.Grid.DataBind(results);
		}

		public JsonResult LoadReportProjectsGrid(string customFilters)
		{
			var filters = JsonConvert.DeserializeObject<ProjectSearch>(customFilters);
			filters.IsTransfer = ProjectType.All;
			var savedColumns = _columnChooserService.LoadColumns(ColumnChooserGrid.ProjectsReport);
			var grid = new ProjectsReportGrid();
			grid.InitializeDynamicGrid(savedColumns);
			var selectStatement = grid.GetColumnsDynamicLinqFormat();
			var results = _queryService.ProjectSearch(filters).GenericSort(filters).Select(selectStatement);

			return grid.Grid.DataBind(results);
		}

		public JsonResult LoadReportSubmissionsGrid(string customFilters)
		{
			var filters = JsonConvert.DeserializeObject<SubmissionSearch>(customFilters);
			var savedColumns = _columnChooserService.LoadColumns(ColumnChooserGrid.SubmissionsReport);
			var grid = new SubmissionsReportGrid();
			grid.InitializeDynamicGrid(savedColumns);
			var selectStatement = grid.GetColumnsDynamicLinqFormat();
			var results = _queryService.SubmissionSearch(filters).GenericSort(filters).Select(selectStatement);

			return grid.Grid.DataBind(results);
		}

		public JsonResult LoadReportTasksGrid(string customFilters)
		{
			var filters = JsonConvert.DeserializeObject<TaskSearch>(customFilters);
			var savedColumns = _columnChooserService.LoadColumns(ColumnChooserGrid.TasksReport);
			var grid = new TasksReportGrid();
			grid.InitializeDynamicGrid(savedColumns);
			var selectStatement = grid.GetColumnsDynamicLinqFormat();
			var results = _queryService.TaskSearch(filters).GenericSort(filters).Select(selectStatement);

			return grid.Grid.DataBind(results);
		}

		public JsonResult LoadReportQAAdvancedGrid(string customFilters)
		{
			var filters = JsonConvert.DeserializeObject<QAAdvancedSearch>(customFilters);

			var sStatus = "";
			if (filters.CustomSearchStatus != null)
			{
				if (filters.CustomSearchStatus.Count > 0)
				{
					for (int i = 0; i < filters.CustomSearchStatus.Count; i++)
					{
						if (filters.CustomSearchStatus[i] == "EN")
						{
							filters.CustomSearchStatus[i] = "";
						}
					}
					sStatus = string.Join("|", filters.CustomSearchStatus);
				}
				sStatus = sStatus + "|";
			}

			var isExpedite = (filters.Expedite) ? 1 : 0;
			var qaAcceptFrom = filters.QAAcceptedFrom;
			var qaAcceptTo = filters.QAAcceptedTo;
			var rcvdFrom = filters.RecievedFrom;
			var rcvdTo = filters.RecievedTo;
			var reportType = 0;
			if (filters.IsTransfer == ProjectType.Projects)
			{
				reportType = 1;
			}
			else if (filters.IsTransfer == ProjectType.Transfers)
			{
				reportType = 2;
			}
			var SummaryReport = 1;
			var qaRcvdFrom = filters.QAReceivedFrom;
			var qaRcvdTo = filters.QAReceivedTo;
			var engCompletedFrom = filters.ENCompletedFrom;
			var engCompletedTo = filters.ENCompletedTo;
			var qaCompleteFrom = filters.QACompletedFrom;
			var qaCompleteTo = filters.QACompletedTo;
			var manufacturer = filters.ManufacturerShort;
			var jurisdiction = (filters.JurisdictionId == null) ? -1 : filters.JurisdictionId;
			var bundleTypeId = (filters.CustomSearchBundleType != null) ? (int)(filters.CustomSearchBundleType) : 0;
			var locations = "";
			var workedByLocations = "";
			var acceptedByLocations = "";
			if (filters.LaboratoryIds != null)
			{
				if (filters.LaboratoryIds.Count > 0)
				{
					var locationsCode = _dbSubmission.trLaboratories.Where(x => filters.LaboratoryIds.Contains(x.Id)).Select(x => x.Location).ToList();
					locations = string.Join("|", locationsCode);
				}
			}
			if (filters.WorkedByLocationIds != null)
			{
				if (filters.WorkedByLocationIds.Count > 0)
				{
					var locationsCode = _dbSubmission.trLaboratories.Where(x => filters.WorkedByLocationIds.Contains(x.Id)).Select(x => x.Location).ToList();
					workedByLocations = string.Join("|", locationsCode);
				}
			}

			if (filters.AcceptedByLocationIds != null)
			{
				if (filters.AcceptedByLocationIds.Count > 0)
				{
					var locationsCode = _dbSubmission.trLaboratories.Where(x => filters.AcceptedByLocationIds.Contains(x.Id)).Select(x => x.Location).ToList();
					acceptedByLocations = string.Join("|", locationsCode);
				}
			}

			var taskDesc = (filters.TaskDefIds != null ? string.Join(",", filters.TaskDefIds) : null);
			var taskTarget = filters.TaskTarget.ToString("d");
			var qFINeeded = (filters.QFINeeded == true) ? 1 : 0;
			var enInvolvementNeeded = (filters.ENInvolvementWhileFL) ? 1 : 0;
			var noCertNeeded = (filters.NoCertNeeded) ? 1 : 0;
			var admWorkNeeded = (filters.ADMWorkNeeded == true) ? 1 : 0;
			var qaLocation = filters.QALocationId != null ? filters.QALocationId.ToString() : "null";
			var complianceRequired = (filters.ComplianceRequired == true) ? 1 : 0;
			var query = "exec [dbo].[GetQAReportData] '" + sStatus + "','" + isExpedite + "','" + qaAcceptFrom + "','" + qaAcceptTo + "','" + rcvdFrom + "','" + rcvdTo + "','" + reportType + "'," + SummaryReport + ",'" + qaRcvdFrom + "','" + qaRcvdTo + "','" + engCompletedFrom + "','" + engCompletedTo + "','" + qaCompleteFrom + "','" + qaCompleteTo + "','" + manufacturer + "','" + jurisdiction + "','" + bundleTypeId + "',0,'" + qFINeeded + "',0,'" + locations + "', '" + taskDesc + "', '" + taskTarget + "'," + admWorkNeeded + "," + enInvolvementNeeded + "," + noCertNeeded + ",'" + workedByLocations + "','" + acceptedByLocations + "'," + qaLocation + "," + complianceRequired;
			var results = _dbSubmission.Database.SqlQuery<QAAdvancedSearchGridRecord>(query).AsQueryable();
			var grid = new QAAdvancedSearchGrid();
			return grid.Grid.DataBind(results);
		}

		#endregion

		#region Tools
		public JsonResult LoadExternalDocumentsGrid()
		{
			var results = _dbSubmission.tbl_ExternalDocuments
				.Select(x => new ExternalDocumentsGridRecord
				{
					Id = x.Id,
					DocumentDate = x.DocumentDate,
					ManufacturerCode = x.ManufacturerCode,
					Description = x.Description,
					FileName = x.FileName
				});

			var grid = new ExternalDocumentsGrid();
			return grid.Grid.DataBind(results);
		}

		public JsonResult LoadPDFRulesGrid(string customFilters)
		{
			var results = _dbSubmission.Rules
				.Select(x => new PDFEmailRulesGridRecord
				{
					RuleId = x.Id,
					Description = x.Description,
					CreateDate = x.CreateTS,
					EditDate = x.EditTS,
					CreatedBy = (x.CreatedById != null) ? x.CreatedBy.FName + " " + x.CreatedBy.LName : "",
					EditedBy = (x.LastEditedById != null) ? x.LastEditedBy.FName + " " + x.LastEditedBy.LName : x.CreatedBy.FName + " " + x.CreatedBy.LName,
					Scheduled = x.Schedule
				});

			var grid = new PDFEmailRulesGrid();
			return grid.Grid.DataBind(results);
		}
		public JsonResult LoadClauseTemplates(string customFilters)
		{
			var existing = _dbeResultsManaged.ClauseTemplates
				.Where(template => !template.Inactive)
				.Select(template => new ClauseTemplateModel { Id = template.Id, Label = template.Label, JurisdictionId = template.JurisdictionId, ManufacturerId = template.ManufacturerId, Inactive = template.Inactive })
				.ToList();
			var jurIds = existing.Select(template => template.JurisdictionId).Distinct().ToList();
			var mfgIds = existing.Select(template => template.ManufacturerId).Distinct().ToList();
			var juris = _dbSubmission.tbl_lst_fileJuris
				.Where(juri => jurIds.Contains(juri.FixId))
				.Select(juri => new { juri.Name, juri.FormattedJurisdictionName, juri.FixId })
				.ToList();
			var mfgs = _dbSubmission.tbl_lst_fileManu
				.Where(mfg => mfgIds.Contains(mfg.Code))
				.Select(mfg => new { mfg.Description, mfg.Code })
				//.Take(10)
				.ToList();
			foreach (var template in existing)
			{
				var juri = juris.FirstOrDefault(jur => jur.FixId == template.JurisdictionId);
				template.Jurisdiction = string.Format("{0} ({1})",
										string.IsNullOrEmpty(juri.FormattedJurisdictionName) ? juri.Name : juri.FormattedJurisdictionName,
										juri.FixId.ToString());

				var manufacturer = mfgs.FirstOrDefault(mfg => mfg.Code == template.ManufacturerId);
				template.Manufacturer = string.Format("{0} ({1})", manufacturer.Description, manufacturer.Code);
			}
			var grid = new ClauseTemplatesGrid();
			return grid.Grid.DataBind(existing.AsQueryable());

		}
		#endregion

		#region Widget
		public JsonResult LoadWidgetJurisdictionalDataGrid(string jqGridID)
		{
			var grid = new JurisdictionalDataReportGrid();

			var widgetId = Convert.ToInt32(jqGridID.Replace(grid.GridBaseID, ""));
			var widget = _dbSubmission.tbl_Widgets.Where(x => x.Id == widgetId).Select(x => new { Id = x.Id, SavedColumns = x.SavedColumns, Filters = x.Filters }).Single();
			var filters = (widget.Filters == null) ? new JurisdictionalDataSearch() : JsonConvert.DeserializeObject<JurisdictionalDataSearch>(widget.Filters);

			grid.InitializeWidgetGrid(widget.Id, widget.SavedColumns);

			var selectStatement = grid.GetColumnsDynamicLinqFormat();
			var results = _queryService.JurisdictionalDataSearch(filters).GenericSort(filters).Select(selectStatement);

			return grid.Grid.DataBind(results);
		}

		public JsonResult LoadWidgetProjectsGrid(string jqGridID)
		{
			var grid = new ProjectsReportGrid();

			var widgetId = Convert.ToInt32(jqGridID.Replace(grid.GridBaseID, ""));
			var widget = _dbSubmission.tbl_Widgets.Where(x => x.Id == widgetId).Select(x => new { Id = x.Id, SavedColumns = x.SavedColumns, Filters = x.Filters }).Single();
			var filters = (widget.Filters == null) ? new ProjectSearch() : JsonConvert.DeserializeObject<ProjectSearch>(widget.Filters);

			grid.InitializeWidgetGrid(widget.Id, widget.SavedColumns);

			var selectStatement = grid.GetColumnsDynamicLinqFormat();
			var results = _queryService.ProjectSearch(filters).GenericSort(filters).Select(selectStatement);

			return grid.Grid.DataBind(results);
		}

		public JsonResult LoadWidgetSubmissionsGrid(string jqGridID)
		{
			var grid = new SubmissionsReportGrid();

			var widgetId = Convert.ToInt32(jqGridID.Replace(grid.GridBaseID, ""));
			var widget = _dbSubmission.tbl_Widgets.Where(x => x.Id == widgetId).Select(x => new { Id = x.Id, SavedColumns = x.SavedColumns, Filters = x.Filters }).Single();
			var filters = (widget.Filters == null) ? new SubmissionSearch() : JsonConvert.DeserializeObject<SubmissionSearch>(widget.Filters);

			grid.InitializeWidgetGrid(widget.Id, widget.SavedColumns);

			var selectStatement = grid.GetColumnsDynamicLinqFormat();
			var results = _queryService.SubmissionSearch(filters).GenericSort(filters).Select(selectStatement);

			return grid.Grid.DataBind(results);
		}

		public JsonResult LoadWidgetTasksGrid(string jqGridID)
		{
			var grid = new TasksReportGrid();

			var widgetId = Convert.ToInt32(jqGridID.Replace(grid.GridBaseID, ""));
			var widget = _dbSubmission.tbl_Widgets.Where(x => x.Id == widgetId).Select(x => new { Id = x.Id, SavedColumns = x.SavedColumns, Filters = x.Filters }).Single();
			var filters = (widget.Filters == null) ? new TaskSearch() : JsonConvert.DeserializeObject<TaskSearch>(widget.Filters);

			grid.InitializeWidgetGrid(widget.Id, widget.SavedColumns);

			var selectStatement = grid.GetColumnsDynamicLinqFormat();
			var results = _queryService.TaskSearch(filters).GenericSort(filters).Select(selectStatement);

			return grid.Grid.DataBind(results);
		}
		#endregion

		#region Helper Methods
		#endregion
	}
}