using EF.eResults;
using EF.eResultsManaged;
using EF.GLIExtranet;
using EF.QANotes;
using EF.Submission;
using GLI.EF.LetterContent;
using Newtonsoft.Json;
using Submissions.Common;
using Submissions.Common.Filters;
using Submissions.Core;
using Submissions.Core.Models.EvolutionService;
using Submissions.Web.Models.LookupsAjax;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.SqlServer;
using System.Linq;
using System.Runtime.Caching;
using System.Web.Mvc;
using System.Web.SessionState;

namespace Submissions.Web.Controllers
{
	/// <summary>
	/// The data that is loaded via ajax for the pagedown fields
	/// </summary>
	[SessionState(SessionStateBehavior.ReadOnly)]
	public class LookupsAjaxController : Controller
	{
		private readonly ISubmissionContext _dbSubmission;
		private readonly ILetterContentContext _dbLetterContent;
		private readonly IGLIExtranetContext _dbGLIExtranet;
		private readonly IeResultsContext _dbEResults;
		private readonly IeResultsManagedContext _dbEResultsManaged;
		private readonly IEvolutionService _evolutionService;
		private readonly IUserContext _userContext;
		private readonly ObjectCache _memCache;
		private readonly DateTimeOffset _cacheExpirationHours;
		private readonly IQANotesContext _qaNotes;
		private readonly ILetterContentContext _letterContent;

		public LookupsAjaxController
		(
			ISubmissionContext dbSubmission,
			ILetterContentContext dbLetterContent,
			IGLIExtranetContext dbGLIExtranet,
			IeResultsContext dbEResults,
			IeResultsManagedContext dbEResultsManaged,
			IEvolutionService evolutionService,
			IUserContext userContext,
			IQANotesContext qaNotes,
			ILetterContentContext letterContent
		)
		{
			_dbSubmission = dbSubmission;
			_dbLetterContent = dbLetterContent;
			_dbGLIExtranet = dbGLIExtranet;
			_dbEResults = dbEResults;
			_dbEResultsManaged = dbEResultsManaged;
			_evolutionService = evolutionService;
			_userContext = userContext;
			_memCache = MemoryCache.Default;
			_cacheExpirationHours = DateTimeOffset.UtcNow.AddHours(8);
			_qaNotes = qaNotes;
			_letterContent = letterContent;
		}

		// ADD THE CONTROLLER ACTIONS IN ALPHABETICAL ORDER
		// ADD THE LOOKUP AS AN ENUM IN Models/Enums.cs (it should match the controller action name)

		[HttpGet]
		public ActionResult AccountStatus(string searchTerm, int pageSize, int pageNum)
		{
			//load all records from database or cache
			var lookupRecords = _memCache[LookupAjax.AccountStatus.ToString()] as List<Select2Result>;

			if (lookupRecords == null)
			{
				var blankRecord = new Select2Result() { id = null, text = "" };

				var dataRecords = (from x in _dbSubmission.tbl_lst_ManufAcctStatus
								   orderby x.Status
								   select new Select2Result() { id = SqlFunctions.StringConvert((double)x.Id).Trim(), text = x.Status })
								   .ToList();

				lookupRecords = new List<Select2Result>();
				lookupRecords.Add(blankRecord);
				lookupRecords.AddRange(dataRecords);

				_memCache.Set(LookupAjax.AccountStatus.ToString(), lookupRecords, new CacheItemPolicy() { AbsoluteExpiration = _cacheExpirationHours });
			}

			return PageRecords(lookupRecords, searchTerm, pageSize, pageNum);
		}

		[HttpGet]
		public ActionResult Accreditations(string searchTerm, int pageSize, int pageNum)
		{
			//load all records from database or cache
			var lookupRecords = _memCache[LookupAjax.Accreditations.ToString()] as List<Select2Result>;

			if (lookupRecords == null)
			{
				var dataRecords = _dbSubmission.tbl_lst_Accreditation
					.Where(x => x.Active)
					.OrderBy(x => x.Name)
					.Select(x => new Select2Result { id = x.Id.ToString(), text = x.Name })
					.Distinct();

				lookupRecords = new List<Select2Result>();
				lookupRecords.AddRange(dataRecords);

				_memCache.Set(LookupAjax.Accreditations.ToString(), lookupRecords, new CacheItemPolicy() { AbsoluteExpiration = _cacheExpirationHours });
			}

			return PageRecords(lookupRecords, searchTerm, pageSize, pageNum, LookupsAjaxSearchType.StartsWith);
		}

		[HttpGet]
		public ActionResult AcumaticaBillingExceptionTypes(string searchTerm, int pageSize, int pageNum)
		{
			//load all records from database or cache
			var lookupRecords = _memCache[LookupAjax.AcumaticaBillingExceptionTypes.ToString()] as List<Select2Result>;

			if (lookupRecords == null)
			{
				var dataRecords = _dbSubmission.tbl_lst_AcumaticaBillingExceptionTypes
					.Where(x => x.Active)
					.OrderBy(x => x.Id)
					.Select(x => new Select2Result { id = x.Id.ToString(), text = x.Name })
					.Distinct();

				lookupRecords = new List<Select2Result>();
				lookupRecords.AddRange(dataRecords);

				_memCache.Set(LookupAjax.AcumaticaBillingExceptionTypes.ToString(), lookupRecords, new CacheItemPolicy() { AbsoluteExpiration = _cacheExpirationHours });
			}

			return PageRecords(lookupRecords, searchTerm, pageSize, pageNum, LookupsAjaxSearchType.StartsWith);
		}

		[HttpGet]
		public ActionResult AcumaticaTenants(string searchTerm, int pageSize, int pageNum)
		{
			//load all records from database or cache
			var lookupRecords = _memCache[LookupAjax.AcumaticaTenants.ToString()] as List<Select2Result>;

			if (lookupRecords == null)
			{
				var dataRecords = _dbSubmission.tbl_lst_AcumaticaTenants
					.OrderBy(x => x.TenantName)
					.Select(x => new Select2Result { id = x.Id.ToString(), text = x.TenantName })
					.Distinct();

				lookupRecords = new List<Select2Result>();
				lookupRecords.AddRange(dataRecords);

				_memCache.Set(LookupAjax.AcumaticaTenants.ToString(), lookupRecords, new CacheItemPolicy() { AbsoluteExpiration = _cacheExpirationHours });
			}

			return PageRecords(lookupRecords, searchTerm, pageSize, pageNum, LookupsAjaxSearchType.StartsWith);
		}

		[HttpGet]
		public ActionResult AnalysisType(string searchTerm, int pageSize, int pageNum, string filter)
		{
			var blankRecord = new Select2Result() { id = null, text = "" };
			var query = _dbSubmission.tbl_lst_AnalysisType.AsQueryable();

			if (!string.IsNullOrEmpty(filter))
			{
				var filters = JsonConvert.DeserializeObject<AnalysisTypeFilter>(filter);
				if (filters.TeamId != null)
				{
					var analysisTypeIds = _dbSubmission.tbl_TeamAnalysisTypes.Where(x => x.TeamId == filters.TeamId).Select(x => x.AnalysisTypeId).ToList();
					query = query.Where(x => analysisTypeIds.Contains(x.Id));
				}
			}

			var dataRecords = query
					.Where(x => x.Active == true)
					.OrderBy(x => x.Name)
					.Select(x => new Select2Result { id = x.Id.ToString(), text = x.Name })
					.ToList();

			var lookupRecords = new List<Select2Result>();
			lookupRecords.Add(blankRecord);
			lookupRecords.AddRange(dataRecords);
			return PageRecords(lookupRecords, searchTerm, pageSize, pageNum, LookupsAjaxSearchType.Contains);
		}

		[HttpGet]
		public ActionResult AppFees(string searchTerm, int pageSize, int pageNum, string filter)
		{
			var blankRecord = new Select2Result() { id = null, text = "" };
			var query = _dbSubmission.tbl_lst_AppFees.AsQueryable();

			var dataRecords = query
					.Where(x => x.Active == true)
					.OrderBy(x => x.Name)
					.Select(x => new Select2Result { id = x.Id.ToString(), text = x.Name })
					.ToList();

			var lookupRecords = new List<Select2Result>();
			lookupRecords.Add(blankRecord);
			lookupRecords.AddRange(dataRecords);
			return PageRecords(lookupRecords, searchTerm, pageSize, pageNum, LookupsAjaxSearchType.Contains);
		}

		[HttpGet]
		public ActionResult ArchiveLocations(string searchTerm, int pageSize, int pageNum)
		{
			//load all records from database or cache
			var lookupRecords = _memCache[LookupAjax.ArchiveLocations.ToString()] as List<Select2Result>;

			if (lookupRecords == null)
			{
				var blankRecord = new Select2Result() { id = null, text = "" };

				var dataRecords = _dbSubmission.trLaboratories
					.Where(x => x.Active == 1 && x.ArchiveLocation != null)
					.OrderBy(x => x.ArchiveLocation)
					.Select(x => new Select2Result { id = x.ArchiveLocation, text = x.ArchiveLocation })
					.Distinct();

				lookupRecords = new List<Select2Result>();
				lookupRecords.Add(blankRecord);
				lookupRecords.AddRange(dataRecords);

				_memCache.Set(LookupAjax.ArchiveLocations.ToString(), lookupRecords, new CacheItemPolicy() { AbsoluteExpiration = _cacheExpirationHours });
			}

			return PageRecords(lookupRecords, searchTerm, pageSize, pageNum, LookupsAjaxSearchType.StartsWith);
		}

		[HttpGet]
		public ActionResult BillingParties(string searchTerm, int pageSize, int pageNum, string filter)
		{
			var blankRecord = new Select2Result() { id = null, text = "" };
			var query = _dbSubmission.tbl_lst_BillingParty.AsQueryable();

			var dataRecords = query
					.Where(x => x.Active == true)
					.OrderBy(x => x.BillingParty)
					.Select(x => new Select2Result { id = x.Id.ToString(), text = x.BillingParty })
					.ToList();

			var lookupRecords = new List<Select2Result>();
			lookupRecords.Add(blankRecord);
			lookupRecords.AddRange(dataRecords);
			return PageRecords(lookupRecords, searchTerm, pageSize, pageNum, LookupsAjaxSearchType.Contains);
		}

		[HttpGet]
		public ActionResult BillingSheets(string searchTerm, int pageSize, int pageNum, string filter)
		{
			var blankRecord = new Select2Result() { id = null, text = "" };
			var query = _dbSubmission.tbl_lst_BillingSheets.AsQueryable();
			if (!string.IsNullOrEmpty(filter))
			{
				var projectCutOffDay = DateTime.Now.AddYears(-5);
				var filters = JsonConvert.DeserializeObject<BillingSheetFilter>(filter);

				if (filters.LocationId != null)
				{
					query = query.Where(x => x.Labs.Select(l => l.LabId).ToList().Contains((int)filters.LocationId));
				}
				if (filters.ShowOnlyBillingSheets.Count > 0)
				{
					query = query.Where(x => filters.ShowOnlyBillingSheets.Contains(x.Name));
				}
			}

			var dataRecords = query
				.Where(x => x.Active == true)
				.OrderBy(x => x.Name)
				.Select(x => new Select2Result { id = x.Id.ToString(), text = x.Name })
				.ToList();

			var lookupRecords = new List<Select2Result>();
			lookupRecords.Add(blankRecord);
			lookupRecords.Add(new Select2Result() { id = "0", text = "None" }); ;
			lookupRecords.AddRange(dataRecords);
			return PageRecords(lookupRecords, searchTerm, pageSize, pageNum, LookupsAjaxSearchType.Contains);
		}

		[HttpGet]
		public ActionResult Bundles(string searchTerm, int pageSize, int pageNum, string searchType)
		{
			var blankRecord = new Select2Result() { id = null, text = "" };
			var lookupRecords = new List<Select2Result>();
			lookupRecords.Add(blankRecord);

			if (searchType.ToLower() == "startswith")
			{
				lookupRecords.AddRange(_dbSubmission.QABundles
					.Where(x => x.ProjectName.StartsWith(searchTerm))
					.OrderBy(x => x.ProjectName)
					.Select(bundle => new Select2Result { id = bundle.Id.ToString(), text = bundle.ProjectName })
					.Distinct());
			}
			else if (searchType.ToLower() == "contains")
			{
				lookupRecords.AddRange(_dbSubmission.QABundles
					.Where(x => x.ProjectName.Contains(searchTerm))
					.OrderBy(x => x.ProjectName)
					.Select(bundle => new Select2Result { id = bundle.Id.ToString(), text = bundle.ProjectName })
					.Distinct());
			}
			else
			{
				lookupRecords.AddRange(_dbSubmission.QABundles
					.Where(x => x.ProjectName == searchTerm)
					.OrderBy(x => x.ProjectName)
					.Select(bundle => new Select2Result { id = bundle.Id.ToString(), text = bundle.ProjectName })
					.Distinct());
			}

			return PageRecords(lookupRecords, searchTerm, pageSize, pageNum);
		}

		[HttpGet]
		public ActionResult BusinessOwners(string searchTerm, int pageSize, int pageNum)
		{
			//load all records from database or cache
			var lookupRecords = _memCache[LookupAjax.BusinessOwners.ToString()] as List<Select2Result>;

			if (lookupRecords == null)
			{
				var blankRecord = new Select2Result() { id = null, text = "" };

				var dataRecords = (from x in _dbSubmission.tbl_lst_BusinessOwners
								   where x.Active == true
								   select new Select2Result() { id = SqlFunctions.StringConvert((double)x.Id).Trim(), text = x.Description })
								   .ToList();

				lookupRecords = new List<Select2Result>();
				lookupRecords.Add(blankRecord);
				lookupRecords.AddRange(dataRecords);

				_memCache.Set(LookupAjax.BusinessOwners.ToString(), lookupRecords, new CacheItemPolicy() { AbsoluteExpiration = _cacheExpirationHours });
			}

			return PageRecords(lookupRecords, searchTerm, pageSize, pageNum);
		}

		[HttpGet]
		public ActionResult Cabinets(string searchTerm, int pageSize, int pageNum)
		{
			//load all records from database or cache
			var lookupRecords = _memCache[LookupAjax.Cabinets.ToString()] as List<Select2Result>;

			if (lookupRecords == null)
			{
				var blankRecord = new Select2Result() { id = null, text = "" };

				var dataRecords = (from x in _dbSubmission.tbl_lst_Cabinet
								   where x.Active == true
								   orderby x.Cabinet
								   select new Select2Result() { id = SqlFunctions.StringConvert((double)x.Id).Trim(), text = x.Cabinet })
								   .ToList();

				lookupRecords = new List<Select2Result>();
				lookupRecords.Add(blankRecord);
				lookupRecords.AddRange(dataRecords);

				_memCache.Set(LookupAjax.Cabinets.ToString(), lookupRecords, new CacheItemPolicy() { AbsoluteExpiration = _cacheExpirationHours });
			}

			return PageRecords(lookupRecords, searchTerm, pageSize, pageNum);
		}

		[HttpGet]
		public ActionResult CertificationIssuers(string searchTerm, int pageSize, int pageNum)
		{
			//load all records from database or cache
			var lookupRecords = _memCache[LookupAjax.CertificationIssuers.ToString()] as List<Select2Result>;

			if (lookupRecords == null)
			{
				var blankRecord = new Select2Result() { id = null, text = "" };

				var dataRecords = _dbSubmission.CertificationIssuer
					.OrderBy(x => x.LastName)
					.Select(x => new Select2Result { id = x.Id.ToString(), text = x.FirstName + " " + x.LastName })
					.ToList();

				lookupRecords = new List<Select2Result>();
				lookupRecords.Add(blankRecord);
				lookupRecords.AddRange(dataRecords);

				_memCache.Set(LookupAjax.CertificationIssuers.ToString(), lookupRecords, new CacheItemPolicy() { AbsoluteExpiration = _cacheExpirationHours });
			}
			return PageRecords(lookupRecords, searchTerm, pageSize, pageNum);
		}

		[HttpGet]
		public ActionResult ClassificationTypes(string searchTerm, int pageSize, int pageNum)
		{
			//load all records from database or cache
			var lookupRecords = _memCache[LookupAjax.ClassificationTypes.ToString()] as List<Select2Result>;

			if (lookupRecords == null)
			{
				var blankRecord = new Select2Result() { id = null, text = "" };

				var dataRecords = _dbSubmission.tbl_lst_ClassificationTypes
					.Where(x => x.Active == true)
					.OrderBy(x => x.Name)
					.Select(x => new Select2Result { id = x.Id.ToString(), text = x.Name })
					.ToList();

				lookupRecords = new List<Select2Result>();
				lookupRecords.Add(blankRecord);
				lookupRecords.AddRange(dataRecords);

				_memCache.Set(LookupAjax.ClassificationTypes.ToString(), lookupRecords, new CacheItemPolicy() { AbsoluteExpiration = _cacheExpirationHours });
			}
			return PageRecords(lookupRecords, searchTerm, pageSize, pageNum);
		}

		[HttpGet]
		public ActionResult ChipTypes(string searchTerm, int pageSize, int pageNum)
		{
			//load all records from database or cache
			var lookupRecords = _memCache[LookupAjax.ChipTypes.ToString()] as List<Select2Result>;

			if (lookupRecords == null)
			{
				var blankRecord = new Select2Result() { id = null, text = "" };

				var dataRecords = (from x in _dbSubmission.tbl_lst_EPROMsize
								   where x.Active == true
								   orderby x.Code
								   select new Select2Result() { id = SqlFunctions.StringConvert((double)x.Id).Trim(), text = x.Code })
								   .ToList();

				lookupRecords = new List<Select2Result>();
				lookupRecords.Add(blankRecord);
				lookupRecords.AddRange(dataRecords);

				_memCache.Set(LookupAjax.ChipTypes.ToString(), lookupRecords, new CacheItemPolicy() { AbsoluteExpiration = _cacheExpirationHours });
			}

			return PageRecords(lookupRecords, searchTerm, pageSize, pageNum);
		}

		[HttpGet]
		public ActionResult Companies(string searchTerm, int pageSize, int pageNum, string filter)
		{
			//load all records from database or cache
			var dataRecords = _memCache[LookupAjax.Companies.ToString()] as IEnumerable<dynamic>;

			if (dataRecords == null)
			{
				dataRecords = _dbSubmission.tbl_lst_Company
					.Where(x => x.Active)
					.OrderBy(x => x.Name)
					.Select(x => new { Id = SqlFunctions.StringConvert((double)x.Id).Trim(), Text = x.Name, x.DynamicsDatabase })
					.ToList();

				_memCache.Set(LookupAjax.Companies.ToString(), dataRecords, new CacheItemPolicy() { AbsoluteExpiration = _cacheExpirationHours });
			}

			if (!string.IsNullOrEmpty(filter))
			{
				var filters = JsonConvert.DeserializeObject<CompaniesFilter>(filter);

				if (filters.HasDynamicsDatabase)
				{
					dataRecords = dataRecords.Where(x => x.DynamicsDatabase != null);
				}
			}

			var blankRecord = new Select2Result() { id = null, text = "" };
			var lookupRecords = new List<Select2Result>();
			lookupRecords.Add(blankRecord);
			lookupRecords.AddRange(dataRecords.Select(x => new Select2Result() { id = x.Id, text = x.Text }).ToList());

			return PageRecords(lookupRecords, searchTerm, pageSize, pageNum);
		}

		[HttpGet]
		public ActionResult CompilationMethod(string searchTerm, int pageSize, int pageNum)
		{
			var lookupRecords = _memCache[LookupAjax.CompilationMethod.ToString()] as List<Select2Result>;

			if (lookupRecords == null)
			{
				var blankRecord = new Select2Result() { id = null, text = "" };

				var dataRecords = _dbSubmission.tbl_lst_CompilationMethod
					.Where(x => x.Active)
					.OrderBy(x => x.Name)
					.Select(x => new Select2Result() { id = x.Id.ToString(), text = x.Name })
					.ToList();

				lookupRecords = new List<Select2Result>();
				lookupRecords.Add(blankRecord);
				lookupRecords.AddRange(dataRecords);

				_memCache.Set(LookupAjax.CompilationMethod.ToString(), lookupRecords, new CacheItemPolicy() { AbsoluteExpiration = _cacheExpirationHours });
			}

			return PageRecords(lookupRecords, searchTerm, pageSize, pageNum);
		}

		[HttpGet]
		public ActionResult ComplexityRating(string searchTerm, int pageSize, int pageNum)
		{
			var lookupRecords = _memCache[LookupAjax.ComplexityRating.ToString()] as List<Select2Result>;
			if (lookupRecords == null)
			{
				var blankRecord = new Select2Result() { id = null, text = "" };
				var dataRecords = (from x in _dbSubmission.tbl_lst_ComplexityRating
								   orderby x.Id
								   select new Select2Result()
								   {
									   id = x.Id.ToString(),
									   text = x.Rating
								   })
								   .ToList();
				lookupRecords = new List<Select2Result>
				{
					blankRecord
				};
				lookupRecords.AddRange(dataRecords);
				_memCache.Set(LookupAjax.ComplexityRating.ToString(), lookupRecords, new CacheItemPolicy() { AbsoluteExpiration = _cacheExpirationHours });
			}
			return PageRecords(lookupRecords, searchTerm, pageSize, pageNum);
		}

		[HttpGet]
		public ActionResult ComponentGroups(string searchTerm, int pageSize, int pageNum, string filter)
		{
			var filters = JsonConvert.DeserializeObject<ComponentGroupsFilter>(filter);
			var pageRecords = new List<Select2Result>();

			int firstResult = pageSize * (pageNum - 1);

			var lookupRecords = _dbEResultsManaged.InstanceCompGroup
				.Where(x => x.Description.StartsWith(searchTerm));

			if (!string.IsNullOrEmpty(filter))
			{
				if (filters.ProjectId != null)
				{
					lookupRecords = lookupRecords.Where(x => x.Instance.ProjectId == filters.ProjectId);
					var inactiveComponentGroupIds = lookupRecords.Where(x => x.State == (int)EvolutionComponentState.Inactive).Select(x => x.CompGroupID).Distinct().ToList();
					lookupRecords = lookupRecords.Where(x => !inactiveComponentGroupIds.Contains(x.CompGroupID));

				}
			}

			if (filters.ProjectId != null)
			{
				pageRecords = lookupRecords
					.OrderBy(x => x.Description)
					.Skip(firstResult)
					.Take(pageSize)
					.GroupBy(x => x.CompGroupID)
					.Select(x => new Select2Result
					{
						id = x.FirstOrDefault().CompGroupID.ToString(),
						text = x.FirstOrDefault().Description
					})
					.ToList();
			}
			else
			{
				pageRecords = lookupRecords
					.OrderBy(x => x.Description)
					.Skip(firstResult)
					.Take(pageSize)
					.Select(x => new Select2Result { id = x.CompGroupID.ToString(), text = x.Description })
					.ToList();
			}

			return new JsonResult()
			{
				Data = new Select2PagedResult() { Total = lookupRecords.Count(), Results = pageRecords },
				JsonRequestBehavior = JsonRequestBehavior.AllowGet
			};
		}

		[HttpGet]
		public ActionResult ComponentTypes(string searchTerm, int pageSize, int pageNum)
		{
			//load all records from database or cache
			var lookupRecords = _memCache[LookupAjax.ComponentTypes.ToString()] as List<Select2Result>;

			if (lookupRecords == null)
			{
				var blankRecord = new Select2Result() { id = null, text = "" };

				var dataRecords = (from x in _dbSubmission.tbl_lst_ComponentType
								   where x.Active == true
								   orderby x.Description
								   select new Select2Result() { id = SqlFunctions.StringConvert((double)x.Id).Trim(), text = x.Description })
								   .ToList();

				lookupRecords = new List<Select2Result>();
				lookupRecords.Add(blankRecord);
				lookupRecords.AddRange(dataRecords);

				_memCache.Set(LookupAjax.ComponentTypes.ToString(), lookupRecords, new CacheItemPolicy() { AbsoluteExpiration = _cacheExpirationHours });
			}

			return PageRecords(lookupRecords, searchTerm, pageSize, pageNum);
		}

		[HttpGet]
		public ActionResult ContractTypes(string searchTerm, int pageSize, int pageNum)
		{
			//load all records from database or cache
			var lookupRecords = _memCache[LookupAjax.ContractTypes.ToString()] as List<Select2Result>;

			if (lookupRecords == null)
			{
				var blankRecord = new Select2Result() { id = null, text = "" };

				var dataRecords = (from x in _dbSubmission.tbl_lst_ContractType
								   where x.Active == true
								   orderby x.Code
								   select new Select2Result() { id = SqlFunctions.StringConvert((double)x.Id).Trim(), text = x.Code + " - " + x.Description })
								   .ToList();

				lookupRecords = new List<Select2Result>();
				lookupRecords.Add(blankRecord);
				lookupRecords.AddRange(dataRecords);

				_memCache.Set(LookupAjax.ContractTypes.ToString(), lookupRecords, new CacheItemPolicy() { AbsoluteExpiration = _cacheExpirationHours });
			}

			return PageRecords(lookupRecords, searchTerm, pageSize, pageNum);
		}

		[HttpGet]
		public ActionResult Countries(string searchTerm, int pageSize, int pageNum)
		{
			//load all records from database or cache
			var lookupRecords = _memCache[LookupAjax.Countries.ToString()] as List<Select2Result>;

			if (lookupRecords == null)
			{
				var blankRecord = new Select2Result() { id = null, text = "" };

				var dataRecords = _dbSubmission.tbl_lst_Country
					.Where(x => x.Active == true)
					.OrderBy(x => x.Code)
					.Select(x => new Select2Result { id = x.Id.ToString(), text = x.Code + " - " + x.Description })
					.ToList();

				lookupRecords = new List<Select2Result>();
				lookupRecords.Add(blankRecord);
				lookupRecords.AddRange(dataRecords);

				_memCache.Set(LookupAjax.Countries.ToString(), lookupRecords, new CacheItemPolicy() { AbsoluteExpiration = _cacheExpirationHours });
			}
			return PageRecords(lookupRecords, searchTerm, pageSize, pageNum);
		}

		[HttpGet]
		public ActionResult CPReviewTasks(string searchTerm, int pageSize, int pageNum)
		{
			//load all records from database or cache
			var lookupRecords = _memCache[LookupAjax.CPReviewTasks.ToString()] as List<Select2Result>;

			if (lookupRecords == null)
			{
				var blankRecord = new Select2Result() { id = null, text = "" };

				var dataRecords = _dbSubmission.tbl_lst_CPReviewTasks
					.Where(x => x.Active == true)
					.OrderBy(x => x.Jurisdiction.FormattedJurisdictionName)
					.Select(x => new Select2Result { id = x.Id.ToString(), text = x.Jurisdiction.FormattedJurisdictionName })
					.ToList();

				lookupRecords = new List<Select2Result>();
				lookupRecords.Add(blankRecord);
				lookupRecords.AddRange(dataRecords);

				_memCache.Set(LookupAjax.CPReviewTasks.ToString(), lookupRecords, new CacheItemPolicy() { AbsoluteExpiration = _cacheExpirationHours });
			}
			return PageRecords(lookupRecords, searchTerm, pageSize, pageNum);
		}


		[HttpGet]
		public ActionResult Currencies(string searchTerm, int pageSize, int pageNum)
		{
			//load all records from database or cache
			var lookupRecords = _memCache[LookupAjax.Currencies.ToString()] as List<Select2Result>;

			if (lookupRecords == null)
			{
				var blankRecord = new Select2Result() { id = null, text = "" };

				var dataRecords = (from x in _dbSubmission.tbl_lst_Currency
								   where x.Active == true
								   orderby x.Code
								   select new Select2Result() { id = SqlFunctions.StringConvert((double)x.Id).Trim(), text = x.Code + " - " + x.Description })
								   .ToList();

				lookupRecords = new List<Select2Result>();
				lookupRecords.Add(blankRecord);
				lookupRecords.AddRange(dataRecords);

				_memCache.Set(LookupAjax.Currencies.ToString(), lookupRecords, new CacheItemPolicy() { AbsoluteExpiration = _cacheExpirationHours });
			}

			return PageRecords(lookupRecords, searchTerm, pageSize, pageNum);
		}

		[HttpGet]
		public ActionResult DeliveryMechanism(string searchTerm, int pageSize, int pageNum)
		{
			//load all records from database or cache
			var lookupRecords = _memCache[LookupAjax.DeliveryMechanism.ToString()] as List<Select2Result>;

			if (lookupRecords == null)
			{
				var blankRecord = new Select2Result() { id = null, text = "" };

				var dataRecords = _dbSubmission.tbl_lst_DeliveryMechanism
					.Where(x => x.Active == true)
					.OrderBy(x => x.Name)
					.Select(x => new Select2Result { id = x.Id.ToString(), text = x.Name })
					.ToList();

				lookupRecords = new List<Select2Result>();
				lookupRecords.Add(blankRecord);
				lookupRecords.AddRange(dataRecords);

				_memCache.Set(LookupAjax.DeliveryMechanism.ToString(), lookupRecords, new CacheItemPolicy() { AbsoluteExpiration = _cacheExpirationHours });
			}
			return PageRecords(lookupRecords, searchTerm, pageSize, pageNum);
		}

		[HttpGet]
		public ActionResult Departments(string searchTerm, int pageSize, int pageNum, string filter)
		{
			var blankRecord = new Select2Result() { id = null, text = "" };

			var query = _dbSubmission.trDepts.AsQueryable();

			if (!string.IsNullOrEmpty(filter))
			{
				var filters = JsonConvert.DeserializeObject<DepartmentsFilter>(filter);
				if (filters.DepartmentIds.Count > 0)
				{
					query = query.Where(x => filters.DepartmentIds.Contains(x.Id));
				}
			}

			var dataRecords = query
				.Where(x => x.Active == true)
				.OrderBy(x => x.Description)
				.Select(x => new Select2Result() { id = x.Id.ToString(), text = x.Code + " - " + x.Description })
				.ToList();

			var lookupRecords = new List<Select2Result>();
			lookupRecords.Add(blankRecord);
			lookupRecords.AddRange(dataRecords);

			return PageRecords(lookupRecords, searchTerm, pageSize, pageNum, LookupsAjaxSearchType.Contains);
		}

		[HttpGet]
		public ActionResult DocumentType(string searchTerm, int pageSize, int pageNum, string filter)
		{
			var blankRecord = new Select2Result() { id = null, text = "" };
			var query = _dbSubmission.tbl_lst_DocumentTypes.AsQueryable();

			if (!string.IsNullOrEmpty(filter))
			{
				var filters = JsonConvert.DeserializeObject<DocumentTypeFilter>(filter);
				if (filters.IsSupplementType)
				{
					query = query.Where(x => x.IsSupplementType == filters.IsSupplementType);
				}
				if (filters.IsCertType)
				{
					query = query.Where(x => x.IsCertType == filters.IsCertType);
				}
			}

			var dataRecords = query
				//.Where(x => x.Active == true)
				.OrderBy(x => x.Description)
				.Select(x => new Select2Result() { id = x.DocumentTypeId.ToString(), text = x.Description })
				.ToList();
			var lookupRecords = new List<Select2Result>();
			lookupRecords.Add(blankRecord);
			lookupRecords.AddRange(dataRecords);

			return PageRecords(lookupRecords, searchTerm, pageSize, pageNum, LookupsAjaxSearchType.Contains);
		}

		[HttpGet]
		public ActionResult EResultsAUEnums(string searchTerm, int pageSize, int pageNum, string filter)
		{
			var blankRecord = new Select2Result() { id = null, text = "" };

			var query = _dbEResults.AUEnums.AsQueryable();

			if (!string.IsNullOrEmpty(filter))
			{
				var filters = JsonConvert.DeserializeObject<EResultsAUEnumsFilter>(filter);
				if (!String.IsNullOrEmpty(filters.Node))
				{
					query = query.Where(x => x.Node == filters.Node);
				}
			}

			var dataRecords = query
				.OrderBy(x => x.Display)
				.Select(x => new Select2Result() { id = x.Value.ToString(), text = x.Display })
				.ToList();

			var lookupRecords = new List<Select2Result>();
			lookupRecords.Add(blankRecord);
			lookupRecords.AddRange(dataRecords);

			return PageRecords(lookupRecords, searchTerm, pageSize, pageNum, LookupsAjaxSearchType.Contains);
		}

		[HttpGet]
		public ActionResult EvolutionDocuments(string searchTerm, int pagesize, int pageNum, string filter)
		{
			var filters = new EvolutionDocumentsFilter();
			if (!string.IsNullOrEmpty(filter))
			{
				filters = JsonConvert.DeserializeObject<EvolutionDocumentsFilter>(filter);
			}
			var blankRecord = new Select2Result() { id = null, text = "" };
			var dataRecords = new List<Select2Result>();
			var lookupRecords = new List<Select2Result>();

			if (!string.IsNullOrEmpty(filters.ComponentGroupId))
			{
				var nativeLanguages = _evolutionService.GetDocsCompGroupNativeLanguage(new NativeCompGroupJur() { compGroupId = filters.ComponentGroupId, nativeLanguage = filters.NativeLanguage, jurisdictionIds = filters.JurisdictionIds });
				var selectedNativeLanguage = nativeLanguages.Where(x => x.LanguageString == filters.NativeLanguage).ToList();
				dataRecords = selectedNativeLanguage
				.OrderBy(x => x.DocumentName)
				.Select(x => new Select2Result { id = x.DocID.ToString(), text = x.DocumentName + " (" + x.Version + ")" })
				.ToList();

				lookupRecords.Add(blankRecord);
				lookupRecords.AddRange(dataRecords);
			}
			else
			{
				//Evo Dcouments
				dataRecords = _dbEResultsManaged.Document
					.Where(x => x.Inactive == false && x.Name.Contains(searchTerm))
					.OrderBy(x => x.Name)
					.Select(x => new Select2Result
					{
						id = SqlFunctions.StringConvert((double)x.Id).Trim(),
						text = string.IsNullOrEmpty(x.Version) ? x.Name + " (" + x.Id + ")" : x.Name + " (" + x.Version + ")"
					})
					.ToList();
				lookupRecords = new List<Select2Result>();
				lookupRecords.Add(blankRecord);
				lookupRecords.AddRange(dataRecords);
			}
			return PageRecords(lookupRecords, searchTerm, pagesize, pageNum, LookupsAjaxSearchType.None);
		}

		[HttpGet]
		public ActionResult HolidayYears(string searchTerm, int pageSize, int pageNum)
		{
			var lookupRecords = _memCache[LookupAjax.HolidayYears.ToString()] as List<Select2Result>;

			if (lookupRecords == null)
			{

				var blankRecord = new Select2Result() { id = null, text = "" };
				var firstYear = _dbSubmission.tbl_lst_Holidays
					.Select(x => x.HolidayDate.Year)
					.ToList()
					.Min();
				var lastYear = _dbSubmission.tbl_lst_Holidays
					.Select(x => x.HolidayDate.Year)
					.ToList()
					.Max();

				lookupRecords = new List<Select2Result>();
				lookupRecords.Add(blankRecord);

				for (int i = firstYear - 2; i < lastYear + 5; i++)
				{
					lookupRecords.Add(new Select2Result()
					{
						id = i.ToString(),
						text = i.ToString()
					});
				}
			}
			return PageRecords(lookupRecords, searchTerm, pageSize, pageNum);
		}

		[HttpGet]
		public ActionResult NativeLanguages(string searchTerm, int pageSize, int pageNum, string filter)
		{
			var filters = JsonConvert.DeserializeObject<NativeLanguagesFilter>(filter);

			var blankRecord = new Select2Result() { id = null, text = "" };
			var nativeLanguages = _evolutionService.GetDocsCompGroupNativeLanguage(new NativeCompGroupJur() { compGroupId = filters.ComponentGroupId });
			var distinctLanguage = nativeLanguages.GroupBy(x => x.LanguageString).Select(y => y.First()).ToList();
			IList<Select2Result> DocumentLanguage = new List<Select2Result>();
			foreach (DocumentNativeLanguage temp in distinctLanguage)
			{
				DocumentLanguage.Add(new Select2Result { id = temp.UID.ToString(), text = temp.LanguageString == null ? "English" : temp.LanguageString });
			}

			var lookupRecords = new List<Select2Result>();
			lookupRecords.Add(blankRecord);
			lookupRecords.AddRange(DocumentLanguage);

			return PageRecords(lookupRecords, searchTerm, pageSize, pageNum, LookupsAjaxSearchType.None);
		}

		[HttpGet]
		public ActionResult DynamicsDatabases(string searchTerm, int pageSize, int pageNum)
		{
			//load all records from database or cache
			var lookupRecords = _memCache[LookupAjax.DynamicsDatabases.ToString()] as List<Select2Result>;

			if (lookupRecords == null)
			{
				var blankRecord = new Select2Result() { id = null, text = "" };

				var dataRecords = (from x in _dbSubmission.tbl_lst_Company
								   where x.Active == true && x.DynamicsDatabase != null
								   orderby x.Name
								   select new Select2Result() { id = x.DynamicsDatabase.Trim(), text = x.DynamicsDatabase })
								   .ToList();

				lookupRecords = new List<Select2Result>();
				lookupRecords.Add(blankRecord);
				lookupRecords.AddRange(dataRecords);

				_memCache.Set(LookupAjax.DynamicsDatabases.ToString(), lookupRecords, new CacheItemPolicy() { AbsoluteExpiration = _cacheExpirationHours });
			}

			return PageRecords(lookupRecords, searchTerm, pageSize, pageNum);
		}

		[HttpGet]
		public ActionResult EmailDistributions(string searchTerm, int pageSize, int pageNum)
		{
			//load all records from database or cache
			var lookupRecords = _memCache[LookupAjax.EmailDistributions.ToString()] as List<Select2Result>;

			if (lookupRecords == null)
			{
				var blankRecord = new Select2Result() { id = null, text = "" };

				var dataRecords = (from x in _dbSubmission.EmailDistributions
								   orderby x.Code
								   select new Select2Result() { id = SqlFunctions.StringConvert((double)x.Id).Trim(), text = x.Code + " - " + x.Description })
								   .ToList();

				lookupRecords = new List<Select2Result>();
				lookupRecords.Add(blankRecord);
				lookupRecords.AddRange(dataRecords);

				_memCache.Set(LookupAjax.EmailDistributions.ToString(), lookupRecords, new CacheItemPolicy() { AbsoluteExpiration = _cacheExpirationHours });
			}

			return PageRecords(lookupRecords, searchTerm, pageSize, pageNum);
		}

		[HttpGet]
		public ActionResult EmailTypes(string searchTerm, int pageSize, int pageNum)
		{
			//load all records from database or cache
			var lookupRecords = _memCache[LookupAjax.EmailTypes.ToString()] as List<Select2Result>;

			if (lookupRecords == null)
			{
				var blankRecord = new Select2Result() { id = null, text = "" };

				var dataRecords = (from x in _dbSubmission.EmailTypes
								   orderby x.Code
								   select new Select2Result() { id = SqlFunctions.StringConvert((double)x.Id).Trim(), text = x.Code + " - " + x.Description })
								   .ToList();

				lookupRecords = new List<Select2Result>();
				lookupRecords.Add(blankRecord);
				lookupRecords.AddRange(dataRecords);

				_memCache.Set(LookupAjax.EmailTypes.ToString(), lookupRecords, new CacheItemPolicy() { AbsoluteExpiration = _cacheExpirationHours });
			}

			return PageRecords(lookupRecords, searchTerm, pageSize, pageNum);
		}

		[HttpGet]
		public ActionResult ExternalLabs(string searchTerm, int pageSize, int pageNum)
		{
			//load all records from database or cache
			var lookupRecords = _memCache[LookupAjax.ExternalLabs.ToString()] as List<Select2Result>;

			if (lookupRecords == null)
			{
				var blankRecord = new Select2Result() { id = null, text = "" };

				var dataRecords = (from x in _dbSubmission.tbl_lst_ExternalTestLab
								   where !x.InternalLab
								   orderby x.Code
								   select new Select2Result() { id = SqlFunctions.StringConvert((double)x.Id).Trim(), text = x.Code })
								   .ToList();

				lookupRecords = new List<Select2Result>();
				lookupRecords.Add(blankRecord);
				lookupRecords.AddRange(dataRecords);

				_memCache.Set(LookupAjax.ExternalLabs.ToString(), lookupRecords, new CacheItemPolicy() { AbsoluteExpiration = _cacheExpirationHours });
			}

			return PageRecords(lookupRecords, searchTerm, pageSize, pageNum);
		}

		[HttpGet]
		public ActionResult FileNumber(string searchTerm, int pageSize, int pageNum, string filter)
		{
			int firstResult = pageSize * (pageNum - 1);

			var query = _dbSubmission.Submissions.AsQueryable();
			query = query.Where(x => x.FileNumber.StartsWith(searchTerm));

			if (!string.IsNullOrEmpty(filter))
			{
				var filters = JsonConvert.DeserializeObject<SubmissionsFilter>(filter);

				if (filters.ManufacturerCode.Count > 0)
				{
					query = query.Where(x => filters.ManufacturerCode.Contains(x.ManufacturerCode));
				}

				if (filters.FileNumbersToExclude.Count > 0)
				{
					query = query.Where(x => !filters.FileNumbersToExclude.Contains(x.FileNumber));
				}
			}

			var lookupRecords = query.GroupBy(x => x.FileNumber);

			var pageRecords = lookupRecords
				.OrderBy(x => x.Max(y => y.FileNumber))
				.Skip(firstResult)
				.Take(pageSize)
				.Select(x => new Select2Result { id = x.Key, text = x.Max(y => y.FileNumber) })
				.ToList();

			return new JsonResult()
			{
				Data = new Select2PagedResult() { Total = lookupRecords.Count(), Results = pageRecords },
				JsonRequestBehavior = JsonRequestBehavior.AllowGet
			};
		}

		[HttpGet]
		public ActionResult Functions(string searchTerm, int pageSize, int pageNum)
		{
			//load all records from database or cache
			var lookupRecords = _memCache[LookupAjax.Functions.ToString()] as List<Select2Result>;

			if (lookupRecords == null)
			{
				var blankRecord = new Select2Result() { id = null, text = "" };

				var dataRecords = (from x in _dbSubmission.tbl_lst_EPROMFunction
								   orderby x.Code
								   select new Select2Result() { id = SqlFunctions.StringConvert((double)x.Id).Trim(), text = x.Code })
								   .ToList();

				lookupRecords = new List<Select2Result>();
				lookupRecords.Add(blankRecord);
				lookupRecords.AddRange(dataRecords);

				_memCache.Set(LookupAjax.Functions.ToString(), lookupRecords, new CacheItemPolicy() { AbsoluteExpiration = _cacheExpirationHours });
			}

			return PageRecords(lookupRecords, searchTerm, pageSize, pageNum);
		}

		[HttpGet]
		public ActionResult FunctionalRoles(string searchTerm, int pageSize, int pageNum)
		{
			//load all records from database or cache
			var lookupRecords = _memCache[LookupAjax.FunctionalRoles.ToString()] as List<Select2Result>;

			if (lookupRecords == null)
			{
				var blankRecord = new Select2Result() { id = null, text = "" };

				var dataRecords = _dbEResultsManaged.FunctionalRole
					.Select(x => new Select2Result { id = x.Id.ToString(), text = x.Name })
					.OrderBy(x => x.text)
					.ToList();

				lookupRecords = new List<Select2Result>();
				lookupRecords.Add(blankRecord);
				lookupRecords.AddRange(dataRecords);

				_memCache.Set(LookupAjax.FunctionalRoles.ToString(), lookupRecords, new CacheItemPolicy() { AbsoluteExpiration = _cacheExpirationHours });
			}

			return PageRecords(lookupRecords, searchTerm, pageSize, pageNum);
		}

		[HttpGet]
		public ActionResult GameDescriptionStatuses(string searchTerm, int pageSize, int pageNum)
		{
			var lookupRecords = _memCache[LookupAjax.GameDescriptionStatuses.ToString()] as List<Select2Result>;

			if (lookupRecords == null)
			{
				var blankRecord = new Select2Result() { id = null, text = "" };
				var dataRecords = (from status in _dbSubmission.tbl_lst_GameDescriptionStatuses
								   orderby status.Id
								   select new Select2Result() { id = status.Id.ToString(), text = status.Name })
									.ToList();
				lookupRecords = new List<Select2Result>();
				lookupRecords.Add(blankRecord);
				lookupRecords.AddRange(dataRecords);

				_memCache.Set(LookupAjax.GameDescriptionStatuses.ToString(), lookupRecords, new CacheItemPolicy() { AbsoluteExpiration = _cacheExpirationHours });
			}
			return PageRecords(lookupRecords, searchTerm, pageSize, pageNum);
		}

		[HttpGet]
		public ActionResult GameDescriptionTypes(string searchTerm, int pageSize, int pageNum)
		{
			var lookupRecords = _memCache[LookupAjax.GameDescriptionTypes.ToString()] as List<Select2Result>;

			if (lookupRecords == null)
			{
				var blankRecord = new Select2Result() { id = null, text = "" };
				var dataRecords = (from type in _dbSubmission.tbl_lst_GameDescriptionTypes
								   orderby type.Id
								   select new Select2Result() { id = type.Id.ToString(), text = type.Name })
									.ToList();
				lookupRecords = new List<Select2Result>();
				lookupRecords.Add(blankRecord);
				lookupRecords.AddRange(dataRecords);

				_memCache.Set(LookupAjax.GameDescriptionTypes.ToString(), lookupRecords, new CacheItemPolicy() { AbsoluteExpiration = _cacheExpirationHours });
			}
			return PageRecords(lookupRecords, searchTerm, pageSize, pageNum);
		}

		[HttpGet]
		public ActionResult GameSubmissionTypes(string searchTerm, int pageSize, int pageNum)
		{
			//load all records from database or cache
			var lookupRecords = _memCache[LookupAjax.GameSubmissionTypes.ToString()] as List<Select2Result>;

			if (lookupRecords == null)
			{
				var blankRecord = new Select2Result() { id = null, text = "" };

				var dataRecords = _dbSubmission.tbl_lst_GameSubmissionTypes
					.Where(x => x.Active)
					.OrderBy(x => x.Description)
					.Select(x => new Select2Result { id = SqlFunctions.StringConvert((double)x.Id).Trim(), text = x.Description })
					.ToList();

				lookupRecords = new List<Select2Result>();
				lookupRecords.Add(blankRecord);
				lookupRecords.AddRange(dataRecords);

				_memCache.Set(LookupAjax.GameSubmissionTypes.ToString(), lookupRecords, new CacheItemPolicy() { AbsoluteExpiration = _cacheExpirationHours });
			}

			return PageRecords(lookupRecords, searchTerm, pageSize, pageNum);
		}

		[HttpGet]
		public ActionResult GameTypes(string searchTerm, int pageSize, int pageNum)
		{
			//load all records from database or cache
			var lookupRecords = _memCache[LookupAjax.GameTypes.ToString()] as List<Select2Result>;

			if (lookupRecords == null)
			{
				var blankRecord = new Select2Result() { id = null, text = "" };

				var dataRecords = (from x in _dbSubmission.tbl_lst_ProductGames
								   where x.Active == true
								   orderby x.Code
								   select new Select2Result() { id = x.Id.ToString(), text = x.Code })
								   .ToList();

				lookupRecords = new List<Select2Result>();
				lookupRecords.Add(blankRecord);
				lookupRecords.AddRange(dataRecords);

				_memCache.Set(LookupAjax.GameTypes.ToString(), lookupRecords, new CacheItemPolicy() { AbsoluteExpiration = _cacheExpirationHours });
			}

			return PageRecords(lookupRecords, searchTerm, pageSize, pageNum);
		}

		[HttpGet]
		public ActionResult GamingGuidelines(string searchTerm, int pageSize, int pageNum)
		{
			var lookupRecords = _memCache[LookupAjax.GamingGuidelines.ToString()] as List<Select2Result>;

			if (lookupRecords == null)
			{
				var dataRecords = (from x in _dbSubmission.tbl_lst_GamingGuidelines
								   orderby x.Jurisdiction.Name
								   select new Select2Result() { id = SqlFunctions.StringConvert((double)x.Id).Trim(), text = x.Jurisdiction.Name })
								   .ToList();

				lookupRecords = new List<Select2Result>();
				lookupRecords.AddRange(dataRecords);

				_memCache.Set(LookupAjax.GamingGuidelines.ToString(), lookupRecords, new CacheItemPolicy() { AbsoluteExpiration = _cacheExpirationHours });
			}
			return PageRecords(lookupRecords, searchTerm, pageSize, pageNum);
		}

		[HttpGet]
		public ActionResult GamingGuidelinesAttributes(string searchTerm, int pageSize, int pageNum)
		{

			//load all records from database or cache
			var lookupRecords = _memCache[LookupAjax.GamingGuidelinesAttributes.ToString()] as List<Select2Result>;

			if (lookupRecords == null)
			{
				var blankRecord = new Select2Result() { id = null, text = "" };

				var dataRecords = (from x in _dbSubmission.tbl_lst_GamingGuidelineAttributes
								   orderby x.Name
								   select new Select2Result() { id = SqlFunctions.StringConvert((double)x.Id).Trim(), text = x.Name })
								   .ToList();

				lookupRecords = new List<Select2Result>();
				lookupRecords.Add(blankRecord);
				lookupRecords.AddRange(dataRecords);

				_memCache.Set(LookupAjax.GamingGuidelinesAttributes.ToString(), lookupRecords, new CacheItemPolicy() { AbsoluteExpiration = _cacheExpirationHours });
			}

			return PageRecords(lookupRecords, searchTerm, pageSize, pageNum);
		}

		[HttpGet]
		public ActionResult GamingGuidelinesGameTypes(string searchTerm, int pageSize, int pageNum)
		{

			//load all records from database or cache
			var lookupRecords = _memCache[LookupAjax.GamingGuidelinesGameTypes.ToString()] as List<Select2Result>;

			if (lookupRecords == null)
			{
				var blankRecord = new Select2Result() { id = null, text = "" };

				var dataRecords = (from x in _dbSubmission.tbl_lst_GamingGuidelineGameTypes
								   orderby x.Name
								   select new Select2Result() { id = SqlFunctions.StringConvert((double)x.Id).Trim(), text = x.Name })
								   .ToList();

				lookupRecords = new List<Select2Result>();
				lookupRecords.Add(blankRecord);
				lookupRecords.AddRange(dataRecords);

				_memCache.Set(LookupAjax.GamingGuidelinesGameTypes.ToString(), lookupRecords, new CacheItemPolicy() { AbsoluteExpiration = _cacheExpirationHours });
			}

			return PageRecords(lookupRecords, searchTerm, pageSize, pageNum);
		}


		[HttpGet]
		public ActionResult LetterContentFormatFilters(string searchTerm, int pageSize, int pageNum)
		{
			var lookupRecords = _memCache[LookupAjax.LetterContentFormatFilter.ToString()] as List<Select2Result>;
			if (lookupRecords == null)
			{
				var blankRecord = new Select2Result() { id = null, text = "" };

				var dataRecords = (from x in _dbLetterContent.FormatFilters
								   orderby x.Name
								   select new Select2Result() { id = x.Id.ToString(), text = x.Name.ToString() })
								   .Distinct()
								   .ToList();
				lookupRecords = new List<Select2Result>();
				lookupRecords.Add(blankRecord);
				lookupRecords.AddRange(dataRecords);

				_memCache.Set(LookupAjax.LimitNumber.ToString(), lookupRecords, new CacheItemPolicy() { AbsoluteExpiration = _cacheExpirationHours });
			}

			return PageRecords(lookupRecords, searchTerm, pageSize, pageNum);
		}


		[HttpGet]
		public ActionResult LetterContentQuestions(string searchTerm, int pageSize, int pageNum, string filter)
		{
			List<Select2Result> lookupRecords = _memCache[LookupAjax.LetterContentQuestions.ToString()] as List<Select2Result>;

			if (lookupRecords == null)
			{
				var query = _dbLetterContent.Questions.AsQueryable();
				if (!string.IsNullOrEmpty(filter))
				{
					var filters = JsonConvert.DeserializeObject<LetterContentQuestionFilter>(filter);

					if (filters.ExcludeQuestionIds.Count > 0)
					{
						query = query.Where(x => !filters.ExcludeQuestionIds.Contains(x.Id));
					}
				}

				var blankRecord = new Select2Result() { id = null, text = "" };
				var dataRecords =
					query
					.OrderBy(x => x.Name)
					.Select(x => new Select2Result() { id = x.Id.ToString(), text = x.Name.ToString() })
					.Distinct()
					.ToList();

				lookupRecords = new List<Select2Result>
				{
					blankRecord
				};
				lookupRecords.AddRange(dataRecords);

				_memCache.Set(LookupAjax.LimitNumber.ToString(), lookupRecords, new CacheItemPolicy() { AbsoluteExpiration = _cacheExpirationHours });
			}

			return PageRecords(lookupRecords, searchTerm, pageSize, pageNum);
		}

		[HttpGet]
		public ActionResult LimitNumber(string searchTerm, int pageSize, int pageNum)
		{
			var lookupRecords = _memCache[LookupAjax.LimitNumber.ToString()] as List<Select2Result>;
			if (lookupRecords == null)
			{
				var blankRecord = new Select2Result() { id = null, text = "" };

				var dataRecords = (from x in _dbSubmission.GamingGuidelineLimits
								   orderby x.Limit
								   select new Select2Result() { id = x.Limit.ToString(), text = x.Limit.ToString() })
								   .Distinct()
								   .ToList();
				lookupRecords = new List<Select2Result>();
				lookupRecords.Add(blankRecord);
				lookupRecords.AddRange(dataRecords);

				_memCache.Set(LookupAjax.LimitNumber.ToString(), lookupRecords, new CacheItemPolicy() { AbsoluteExpiration = _cacheExpirationHours });
			}

			return PageRecords(lookupRecords, searchTerm, pageSize, pageNum);
		}

		[HttpGet]
		public ActionResult OddNumber(string searchTerm, int pageSize, int pageNum)
		{
			var lookupRecords = _memCache[LookupAjax.OddNumber.ToString()] as List<Select2Result>;
			if (lookupRecords == null)
			{
				var blankRecord = new Select2Result() { id = null, text = "" };
				var distinctOdds = _dbSubmission.GamingGuidelineOddsRequirement.GroupBy(x => x.Odds)
							.Select(g => g.FirstOrDefault())
							.Select(x => new Select2Result() { id = x.Odds.ToString(), text = x.Odds.ToString() })
							.ToList();

				lookupRecords = new List<Select2Result>();
				lookupRecords.Add(blankRecord);
				lookupRecords.AddRange(distinctOdds);

				_memCache.Set(LookupAjax.OddNumber.ToString(), lookupRecords, new CacheItemPolicy() { AbsoluteExpiration = _cacheExpirationHours });
			}

			return PageRecords(lookupRecords, searchTerm, pageSize, pageNum);
		}

		[HttpGet]
		public ActionResult RTPNumber(string searchTerm, int pageSize, int pageNum)
		{
			var lookupRecords = _memCache[LookupAjax.RTPNumber.ToString()] as List<Select2Result>;
			if (lookupRecords == null)
			{
				var blankRecord = new Select2Result() { id = null, text = "" };

				var dataRecords = (from x in _dbSubmission.GamingGuidelineRTPRequirements
								   orderby x.RTP
								   select new Select2Result() { id = x.RTP.ToString(), text = x.RTP.ToString() })
								   .Distinct()
								   .ToList();
				lookupRecords = new List<Select2Result>();
				lookupRecords.Add(blankRecord);
				lookupRecords.AddRange(dataRecords);

				_memCache.Set(LookupAjax.RTPNumber.ToString(), lookupRecords, new CacheItemPolicy() { AbsoluteExpiration = _cacheExpirationHours });
			}

			return PageRecords(lookupRecords, searchTerm, pageSize, pageNum);
		}

		[HttpGet]
		public ActionResult GamingGuidelineLimits(string searchTerm, int pageSize, int pageNum, string filter)
		{
			var lookupRecords = _memCache[LookupAjax.GamingGuidelineLimits.ToString()] as List<Select2Result>;

			if (lookupRecords == null)
			{
				var blankRecord = new Select2Result() { id = null, text = "" };
				var dataRecords = (from x in _dbSubmission.tbl_lst_GamingGuidelineLimitType
								   orderby x.Name
								   select new Select2Result() { id = SqlFunctions.StringConvert((double)x.Id).Trim(), text = x.Name })
								   .ToList();

				lookupRecords = new List<Select2Result>();
				lookupRecords.Add(blankRecord);
				lookupRecords.AddRange(dataRecords);

				_memCache.Set(LookupAjax.GamingGuidelineLimits.ToString(), lookupRecords, new CacheItemPolicy() { AbsoluteExpiration = _cacheExpirationHours });
			}

			return PageRecords(lookupRecords, searchTerm, pageSize, pageNum);
		}

		[HttpGet]
		public ActionResult GamingGuidelineOddsRequirement(string searchTerm, int pageSize, int pageNum)
		{
			var lookupRecords = _memCache[LookupAjax.GamingGuidelineOddsRequirement.ToString()] as List<Select2Result>;

			if (lookupRecords == null)
			{
				var blankRecord = new Select2Result() { id = null, text = "" };

				var dataRecords = (from x in _dbSubmission.tbl_lst_GamingGuidelineOddsRequirement
								   orderby x.Odds
								   select new Select2Result() { id = SqlFunctions.StringConvert((double)x.Id).Trim(), text = x.Odds.ToString() })
								   .ToList();
				lookupRecords = new List<Select2Result>();
				lookupRecords.Add(blankRecord);
				lookupRecords.AddRange(dataRecords);

				_memCache.Set(LookupAjax.GamingGuidelineOddsRequirement.ToString(), lookupRecords, new CacheItemPolicy() { AbsoluteExpiration = _cacheExpirationHours });
			}
			return PageRecords(lookupRecords, searchTerm, pageSize, pageNum);
		}

		[HttpGet]
		public ActionResult GamingGuidelineProtocols(string searchTerm, int pageSize, int pageNum)
		{
			var lookupRecords = _memCache[LookupAjax.GamingGuidelineProtocols.ToString()] as List<Select2Result>;

			if (lookupRecords == null)
			{
				var blankRecord = new Select2Result() { id = null, text = "" };

				var dataRecords = (from x in _dbSubmission.tbl_lst_GamingGuidelineProtocolType
								   orderby x.Name
								   select new Select2Result() { id = SqlFunctions.StringConvert((double)x.Id).Trim(), text = x.Name })
								   .ToList();

				lookupRecords = new List<Select2Result>();
				lookupRecords.Add(blankRecord);
				lookupRecords.AddRange(dataRecords);

				_memCache.Set(LookupAjax.GamingGuidelineProtocols.ToString(), lookupRecords, new CacheItemPolicy() { AbsoluteExpiration = _cacheExpirationHours });
			}

			return PageRecords(lookupRecords, searchTerm, pageSize, pageNum);
		}

		[HttpGet]
		public ActionResult GamingGuidelineTechTypes(string searchTerm, int pageSize, int pageNum)
		{
			var lookupRecords = _memCache[LookupAjax.GamingGuidelineTechTypes.ToString()] as List<Select2Result>;

			if (lookupRecords == null)
			{
				var blankRecord = new Select2Result() { id = null, text = "" };

				var dataRecords = (from x in _dbSubmission.tbl_lst_GamingGuidelineTechType
								   orderby x.Name
								   select new Select2Result() { id = SqlFunctions.StringConvert((double)x.Id).Trim(), text = x.Name })
									   .ToList();
				lookupRecords = new List<Select2Result>();
				lookupRecords.Add(blankRecord);
				lookupRecords.AddRange(dataRecords);

				_memCache.Set(LookupAjax.GamingGuidelineTechTypes.ToString(), lookupRecords, new CacheItemPolicy() { AbsoluteExpiration = _cacheExpirationHours });
			}
			return PageRecords(lookupRecords, searchTerm, pageSize, pageNum);
		}

		[HttpGet]
		public ActionResult Jurisdictions(string searchTerm, int pageSize, int pageNum, string filter)
		{
			var lookupRecords = _memCache[LookupAjax.Jurisdictions.ToString()] as List<Select2Result>;

			if (lookupRecords == null)
			{
				var blankRecord = new Select2Result() { id = null, text = "" };
				var dataRecords = _dbSubmission.tbl_lst_fileJuris
					.Where(x => x.Active)
					.OrderBy(x => x.Name)
					.Select(x => new Select2Result { id = x.Id.Trim(), text = x.Name })
					.ToList();

				lookupRecords = new List<Select2Result>();
				lookupRecords.Add(blankRecord);
				lookupRecords.AddRange(dataRecords);

				_memCache.Set(LookupAjax.Jurisdictions.ToString(), lookupRecords, new CacheItemPolicy() { AbsoluteExpiration = _cacheExpirationHours });
			}

			if (!string.IsNullOrEmpty(searchTerm))
			{
				var exactMatch = lookupRecords.Where(x => x.id == searchTerm).ToList();
				var exactMatchIds = lookupRecords.Where(x => x.id == searchTerm).Select(x => x.id).ToList();
				var notExactMatch = lookupRecords.Where(x => !exactMatchIds.Contains(x.id) && x.text.Contains(searchTerm, StringComparison.OrdinalIgnoreCase)).OrderBy(x => x.text).ToList();
				lookupRecords = exactMatch.Union(notExactMatch).ToList();

				//lookupRecords = lookupRecords.Where(x => x.id.Contains(searchTerm, StringComparison.OrdinalIgnoreCase) || x.text.Contains(searchTerm, StringComparison.OrdinalIgnoreCase)).ToList();
			}

			if (!string.IsNullOrEmpty(filter))
			{
				var filters = JsonConvert.DeserializeObject<JurisdictionsFilter>(filter);

				if (filters.ExcludedJurisdictionIds.Count > 0)
				{
					lookupRecords = lookupRecords.Where(x => !filters.ExcludedJurisdictionIds.Contains(x.id)).ToList();
				}

				if (filters.JurisdictionIds.Count > 0)
				{
					lookupRecords = lookupRecords.Where(x => filters.JurisdictionIds.Contains(x.id)).ToList();
				}
			}

			return PageRecords(lookupRecords, searchTerm, pageSize, pageNum, LookupsAjaxSearchType.None);
		}

		[HttpGet]
		public ActionResult GamingGuidelineJurisdictions(string searchTerm, int pageSize, int pageNum, string filter)
		{
			var lookupRecords = _memCache[LookupAjax.GamingGuidelineJurisdictions.ToString()] as List<Select2Result>;

			if (lookupRecords == null)
			{
				var blankRecord = new Select2Result() { id = null, text = "" };

				var usedRecords = _dbSubmission.tbl_lst_GamingGuidelines.Select(x => x.JurisdictionId).ToList();

				var dataRecords = _dbSubmission.tbl_lst_fileJuris
					.Where(x => x.Active && !usedRecords.Contains(x.Id))
					.OrderBy(x => x.Name)
					.Select(x => new Select2Result
					{
						id = x.Id.Trim(),
						text = (x.FormattedJurisdictionName != "" && x.FormattedJurisdictionName != null) ? x.FormattedJurisdictionName + " (" + x.Id.ToString() + ")" : x.Name + " (" + x.Id.ToString() + ")"
					})
					.ToList();

				lookupRecords = new List<Select2Result>();
				lookupRecords.Add(blankRecord);
				lookupRecords.AddRange(dataRecords);

				_memCache.Set(LookupAjax.GamingGuidelineJurisdictions.ToString(), lookupRecords, new CacheItemPolicy() { AbsoluteExpiration = _cacheExpirationHours });
			}

			if (!string.IsNullOrEmpty(searchTerm))
			{
				var exactMatch = lookupRecords.Where(x => x.id == searchTerm).ToList();
				var exactMatchIds = lookupRecords.Where(x => x.id == searchTerm).Select(x => x.id).ToList();
				var notExactMatch = lookupRecords.Where(x => !exactMatchIds.Contains(x.id) && x.text.Contains(searchTerm, StringComparison.OrdinalIgnoreCase)).OrderBy(x => x.text).ToList();
				lookupRecords = exactMatch.Union(notExactMatch).ToList();
			}

			if (!string.IsNullOrEmpty(filter))
			{
				var filters = JsonConvert.DeserializeObject<JurisdictionsFilter>(filter);

				if (filters.ExcludedJurisdictionIds.Count > 0)
				{
					lookupRecords = lookupRecords.Where(x => !filters.ExcludedJurisdictionIds.Contains(x.id)).ToList();
				}

				if (filters.JurisdictionIds.Count > 0)
				{
					lookupRecords = lookupRecords.Where(x => filters.JurisdictionIds.Contains(x.id)).ToList();
				}
			}

			return PageRecords(lookupRecords, searchTerm, pageSize, pageNum, LookupsAjaxSearchType.None);
		}

		[HttpGet]
		public ActionResult GamingGuidelineJurisdictionsSelected(string searchTerm, int pageSize, int pageNum, string filter)
		{
			var lookupRecords = _memCache[LookupAjax.GamingGuidelineJurisdictionsSelected.ToString()] as List<Select2Result>;

			if (lookupRecords == null)
			{
				var blankRecord = new Select2Result() { id = null, text = "" };

				var usedRecords = _dbSubmission.tbl_lst_GamingGuidelines.Select(x => x.JurisdictionId).ToList();

				var dataRecords = _dbSubmission.tbl_lst_fileJuris
					.Where(x => x.Active && usedRecords.Contains(x.Id))
					.OrderBy(x => x.Name)
					.Select(x => new Select2Result { id = x.Id.Trim(), text = x.Name + " (" + x.Id + ")" })
					.ToList();

				lookupRecords = new List<Select2Result>();
				lookupRecords.Add(blankRecord);
				lookupRecords.AddRange(dataRecords);

				_memCache.Set(LookupAjax.GamingGuidelineJurisdictionsSelected.ToString(), lookupRecords, new CacheItemPolicy() { AbsoluteExpiration = _cacheExpirationHours });
			}

			if (!string.IsNullOrEmpty(searchTerm))
			{
				var exactMatch = lookupRecords.Where(x => x.id == searchTerm).ToList();
				var exactMatchIds = lookupRecords.Where(x => x.id == searchTerm).Select(x => x.id).ToList();
				var notExactMatch = lookupRecords.Where(x => !exactMatchIds.Contains(x.id) && x.text.Contains(searchTerm, StringComparison.OrdinalIgnoreCase)).OrderBy(x => x.text).ToList();
				lookupRecords = exactMatch.Union(notExactMatch).ToList();
			}

			if (!string.IsNullOrEmpty(filter))
			{
				var filters = JsonConvert.DeserializeObject<JurisdictionsFilter>(filter);

				if (filters.ExcludedJurisdictionIds.Count > 0)
				{
					lookupRecords = lookupRecords.Where(x => !filters.ExcludedJurisdictionIds.Contains(x.id)).ToList();
				}

				if (filters.JurisdictionIds.Count > 0)
				{
					lookupRecords = lookupRecords.Where(x => filters.JurisdictionIds.Contains(x.id)).ToList();
				}
			}

			return PageRecords(lookupRecords, searchTerm, pageSize, pageNum, LookupsAjaxSearchType.None);
		}

		[HttpGet]
		public ActionResult IGTJurisdictionMappings(string searchTerm, int pageSize, int pageNum)
		{
			//load all records from database or cache
			var lookupRecords = _memCache[LookupAjax.IGTJurisdictionMappings.ToString()] as List<Select2Result>;

			if (lookupRecords == null)
			{
				var blankRecord = new Select2Result() { id = null, text = "" };

				var dataRecords = (from x in _dbSubmission.tbl_lst_JurisdictionMappings
								   where x.FlatFile && x.ManufacturerCode == "IGT"
								   orderby x.ExternalJurisdictionDescription
								   select new Select2Result() { id = x.JurisdictionId.Trim(), text = (x.ExternalJurisdictionDescription + " (IGT# " + x.ExternalJurisdictionId + ") - (GLI# " + x.JurisdictionId + ")") })
								   .ToList();

				lookupRecords = new List<Select2Result>();
				lookupRecords.Add(blankRecord);
				lookupRecords.AddRange(dataRecords);

				_memCache.Set(LookupAjax.IGTJurisdictionMappings.ToString(), lookupRecords, new CacheItemPolicy() { AbsoluteExpiration = _cacheExpirationHours });
			}

			return PageRecords(lookupRecords, searchTerm, pageSize, pageNum);
		}

		[HttpGet]
		public ActionResult JurisdictionGroupCategories(string searchTerm, int pageSize, int pageNum)
		{
			var lookupRecords = _memCache[LookupAjax.JurisdictionGroupCategories.ToString()] as List<Select2Result>;

			if (lookupRecords == null)
			{
				var blankRecord = new Select2Result() { id = null, text = "" };

				var dataRecords = (from x in _dbSubmission.JurisdictionGroupCategories
								   orderby x.Code
								   select new Select2Result() { id = SqlFunctions.StringConvert((double)x.Id).Trim(), text = x.Code })
								   .ToList();

				lookupRecords = new List<Select2Result>();
				lookupRecords.Add(blankRecord);
				lookupRecords.AddRange(dataRecords);

				_memCache.Set(LookupAjax.JurisdictionGroupCategories.ToString(), lookupRecords, new CacheItemPolicy() { AbsoluteExpiration = _cacheExpirationHours });
			}

			return PageRecords(lookupRecords, searchTerm, pageSize, pageNum);
		}

		[HttpGet]
		public ActionResult JurisdictionGroups(string searchTerm, int pageSize, int pageNum)
		{
			var lookupRecords = _memCache[LookupAjax.JurisdictionGroups.ToString()] as List<Select2Result>;

			if (lookupRecords == null)
			{
				var blankRecord = new Select2Result() { id = null, text = "" };

				var dataRecords = (from x in _dbSubmission.JurisdictionGroups
								   orderby x.Code
								   select new Select2Result() { id = SqlFunctions.StringConvert((double)x.Id).Trim(), text = x.Code })
								   .ToList();

				lookupRecords = new List<Select2Result>();
				lookupRecords.Add(blankRecord);
				lookupRecords.AddRange(dataRecords);

				_memCache.Set(LookupAjax.JurisdictionGroups.ToString(), lookupRecords, new CacheItemPolicy() { AbsoluteExpiration = _cacheExpirationHours });
			}

			return PageRecords(lookupRecords, searchTerm, pageSize, pageNum);
		}

		[HttpGet]
		public ActionResult Laboratories(string searchTerm, int pageSize, int pageNum, string filter)
		{
			//load all records from database or cache
			var lookupRecords = _memCache[LookupAjax.Laboratories.ToString()] as List<Select2Result>;

			if (lookupRecords == null)
			{
				var blankRecord = new Select2Result() { id = null, text = "" };

				var dataRecords = (from x in _dbSubmission.trLaboratories
								   where x.Active == 1
								   orderby x.Location
								   select new Select2Result() { id = SqlFunctions.StringConvert((double)x.Id).Trim(), text = x.Location + " - " + x.Name })
								   .ToList();

				lookupRecords = new List<Select2Result>();
				lookupRecords.Add(blankRecord);
				lookupRecords.AddRange(dataRecords);

				_memCache.Set(LookupAjax.Laboratories.ToString(), lookupRecords, new CacheItemPolicy() { AbsoluteExpiration = _cacheExpirationHours });
			}

			return PageRecords(lookupRecords, searchTerm, pageSize, pageNum, LookupsAjaxSearchType.Contains);
		}
		[HttpGet]
		public ActionResult LaboratoryLetterAddress(string searchTerm, int pageSize, int pageNum)
		{
			//load all records from database or cache
			var lookupRecords = _memCache[LookupAjax.LaboratoryLetterAddress.ToString()] as List<Select2Result>;

			if (lookupRecords == null)
			{
				var blankRecord = new Select2Result() { id = null, text = "" };

				var labDataRows = _qaNotes.Letter_LabData.Select(ld => new
				{
					ld.ID,
					address = new List<string>() { ld.Address1, ld.Address2, ld.Address3, ld.Address4, ld.Address5 }
				}).ToList();
				var dataRecords = labDataRows.Select(ldr => new Select2Result()
				{
					id = string.Join("<br/>", ldr.address.Where(addy => !string.IsNullOrWhiteSpace(addy)).ToArray()),
					text = ldr.ID
				}).ToList();

				lookupRecords = new List<Select2Result>();
				lookupRecords.Add(blankRecord);
				lookupRecords.AddRange(dataRecords);

				_memCache.Set(LookupAjax.LaboratoryLetterAddress.ToString(), lookupRecords, new CacheItemPolicy() { AbsoluteExpiration = _cacheExpirationHours });
			}

			return PageRecords(lookupRecords, searchTerm, pageSize, pageNum, LookupsAjaxSearchType.Contains);
		}
		[HttpGet]
		public ActionResult Language(string searchTerm, int pageSize, int pageNum)
		{
			//load all records from database or cache
			var lookupRecords = _memCache[LookupAjax.Language.ToString()] as List<Select2Result>;

			if (lookupRecords == null)
			{
				var dataRecords = (from x in _dbSubmission.tbl_lst_Language
								   orderby x.Language
								   select new Select2Result() { id = SqlFunctions.StringConvert((double)x.Id).Trim(), text = x.Language })
								   .ToList();

				lookupRecords = new List<Select2Result>();
				lookupRecords.AddRange(dataRecords);

				_memCache.Set(LookupAjax.Language.ToString(), lookupRecords, new CacheItemPolicy() { AbsoluteExpiration = _cacheExpirationHours });
			}

			return PageRecords(lookupRecords, searchTerm, pageSize, pageNum, LookupsAjaxSearchType.Contains);
		}

		[HttpGet]
		public ActionResult Liaisons(string searchTerm, int pageSize, int pageNum)
		{
			//load all records from database or cache
			var lookupRecords = _memCache[LookupAjax.Liaisons.ToString()] as List<Select2Result>;

			if (lookupRecords == null)
			{
				var blankRecord = new Select2Result() { id = null, text = "" };

				var dataRecords = (from x in _dbSubmission.trLogins
								   where x.Status == "A" && x.IsLiaison
								   orderby x.LName
								   select new Select2Result() { id = SqlFunctions.StringConvert((double)x.Id).Trim(), text = x.FName + " " + x.LName })
								   .ToList();

				lookupRecords = new List<Select2Result>();
				lookupRecords.Add(blankRecord);
				lookupRecords.AddRange(dataRecords);

				_memCache.Set(LookupAjax.Liaisons.ToString(), lookupRecords, new CacheItemPolicy() { AbsoluteExpiration = _cacheExpirationHours });
			}

			return PageRecords(lookupRecords, searchTerm, pageSize, pageNum, LookupsAjaxSearchType.Contains);
		}

		public ActionResult LetterContentCompareCriteria(string searchTerm, int pageSize, int pageNum)
		{
			//load all records from database or cache
			var lookupRecords = _memCache[LookupAjax.LetterContentCompareCriteria.ToString()] as List<Select2Result>;

			if (lookupRecords == null)
			{
				var blankRecord = new Select2Result() { id = null, text = "" };

				var dataRecords = _letterContent.CompareCriterias
					.Where(x => x.Active)
					.OrderBy(x => x.Order)
					.Select(x => new Select2Result { id = x.Id.ToString(), text = x.Description })
					.ToList();

				lookupRecords = new List<Select2Result>();
				lookupRecords.Add(blankRecord);
				lookupRecords.AddRange(dataRecords);

				_memCache.Set(LookupAjax.LetterContentCompareCriteria.ToString(), lookupRecords, new CacheItemPolicy() { AbsoluteExpiration = _cacheExpirationHours });
			}

			return PageRecords(lookupRecords, searchTerm, pageSize, pageNum);
		}

		[HttpGet]
		public ActionResult LetterContact(string searchTerm, int pageSize, int pageNum, string filter)
		{

			var blankRecord = new Select2Result { id = null, text = "" };
			var query = _dbSubmission.tbl_lst_LetterContacts.AsQueryable();

			if (!string.IsNullOrWhiteSpace(filter))
			{
				var parsedFilter = JsonConvert.DeserializeObject<LetterContactsFilter>(filter);

				if (!string.IsNullOrWhiteSpace(parsedFilter.ContactType))
				{
					//query = query.Where(x => x.LetterType == parsedFilter.ContactType);
				}
				if (!string.IsNullOrWhiteSpace(parsedFilter.ManufacturerId))
				{
					query = query.Where(x => x.Manufacturer.Select(manu => manu.ManufacturerId).Contains(parsedFilter.ManufacturerId));
				}
				if (!string.IsNullOrWhiteSpace(parsedFilter.ContactType))
				{
					query = query.Where(x => x.Jurisdiction.Select(jur => jur.JurisdictionId).Contains(parsedFilter.JurisdictionId));
				}
			}

			var dataRecords = query.Select(contact => new Select2Result { id = contact.Id.ToString(), text = contact.Name }).ToList();

			var lookupResults = new List<Select2Result>
			{
				blankRecord
			};

			lookupResults.AddRange(dataRecords);

			return PageRecords(lookupResults, searchTerm, pageSize, pageNum, LookupsAjaxSearchType.Contains);
		}

		[HttpGet]
		public ActionResult LetterContentReportTypes(string searchTerm, int pageSize, int pageNum)
		{
			//load all records from database or cache
			var lookupRecords = _memCache[LookupAjax.LetterContentReportTypes.ToString()] as List<Select2Result>;

			if (lookupRecords == null)
			{
				var blankRecord = new Select2Result() { id = null, text = "" };

				var dataRecords = _dbSubmission.tbl_lst_ReportType
					.Where(x => x.Active)
					.OrderBy(x => x.Type)
					.Select(x => new Select2Result { id = x.Id.ToString(), text = x.Type })
					.ToList();

				lookupRecords = new List<Select2Result>();
				lookupRecords.Add(blankRecord);
				lookupRecords.AddRange(dataRecords);

				_memCache.Set(LookupAjax.LetterContentReportTypes.ToString(), lookupRecords, new CacheItemPolicy() { AbsoluteExpiration = _cacheExpirationHours });
			}

			return PageRecords(lookupRecords, searchTerm, pageSize, pageNum);
		}

		[HttpGet]
		public ActionResult LetterContentSection(string searchTerm, int pageSize, int pageNum)
		{
			//load all records from database or cache
			var lookupRecords = _memCache[LookupAjax.LetterContentSection.ToString()] as List<Select2Result>;

			if (lookupRecords == null)
			{
				var blankRecord = new Select2Result() { id = null, text = "" };

				var dataRecords = _letterContent.Sections
					.Where(x => x.Active)
					.OrderBy(x => x.Name)
					.Select(x => new Select2Result { id = x.Id.ToString(), text = x.Name })
					.ToList();

				lookupRecords = new List<Select2Result>();
				lookupRecords.Add(blankRecord);
				lookupRecords.AddRange(dataRecords);

				_memCache.Set(LookupAjax.LetterContentSection.ToString(), lookupRecords, new CacheItemPolicy() { AbsoluteExpiration = _cacheExpirationHours });
			}

			return PageRecords(lookupRecords, searchTerm, pageSize, pageNum);
		}

		[HttpGet]
		public ActionResult LetterContentMemos(string searchTerm, int pageSize, int pageNum)
		{
			//load all records from database or cache
			var lookupRecords = _memCache[LookupAjax.LetterContentMemos.ToString()] as List<Select2Result>;

			if (lookupRecords == null)
			{
				var blankRecord = new Select2Result() { id = null, text = "" };

				var dataRecords = _letterContent.Memos
					.Where(x => x.Version.Lifecycle == Lifecycle.Active)
					.OrderBy(x => x.Name)
					.Select(x => new Select2Result { id = x.Id.ToString(), text = x.Name })
					.ToList();

				lookupRecords = new List<Select2Result>();
				lookupRecords.Add(blankRecord);
				lookupRecords.AddRange(dataRecords);

				_memCache.Set(LookupAjax.LetterContentSection.ToString(), lookupRecords, new CacheItemPolicy() { AbsoluteExpiration = _cacheExpirationHours });
			}

			return PageRecords(lookupRecords, searchTerm, pageSize, pageNum);
		}

		[HttpGet]
		public ActionResult LetterContentTestingResult(string searchTerm, int pageSize, int pageNum)
		{
			//load all records from database or cache
			var lookupRecords = _memCache[LookupAjax.LetterContentTestingResult.ToString()] as List<Select2Result>;

			if (lookupRecords == null)
			{
				var blankRecord = new Select2Result() { id = null, text = "" };

				var dataRecords = _dbSubmission.tbl_lst_TestingResult
					.Where(x => x.Active)
					.OrderBy(x => x.Result)
					.Select(x => new Select2Result { id = x.Id.ToString(), text = x.Result })
					.ToList();

				lookupRecords = new List<Select2Result>();
				lookupRecords.Add(blankRecord);
				lookupRecords.AddRange(dataRecords);

				_memCache.Set(LookupAjax.LetterContentTestingResult.ToString(), lookupRecords, new CacheItemPolicy() { AbsoluteExpiration = _cacheExpirationHours });
			}

			return PageRecords(lookupRecords, searchTerm, pageSize, pageNum);
		}

		[HttpGet]
		public ActionResult LetterGenJurisdictions(string searchTerm, int pageSize, int pageNum, int filter)
		{
			var lookupRecords = _memCache[LookupAjax.LetterGenJurisdictions.ToString()] as List<Select2Result>;

			if (lookupRecords == null)
			{
				var blankRecord = new Select2Result() { id = "0", text = "All Jurisdictions" };

				var dataRecords = new List<Select2Result>();
				if (filter != 0) // pass bundle id as filter? 
				{
					dataRecords = _dbSubmission.QABundles.Find(filter).Project.JurisdictionalData
						.Select(x => new Select2Result
						{
							id = x.JurisdictionalData.JurisdictionId,
							text = x.JurisdictionalData.JurisdictionName
						}).GroupBy(selectObj => selectObj.text).Select(group => group.First()).ToList();
					lookupRecords = new List<Select2Result>();
					lookupRecords.Add(blankRecord);
					lookupRecords.AddRange(dataRecords);
					_memCache.Set(LookupAjax.LetterGenJurisdictions.ToString(), lookupRecords, new CacheItemPolicy() { AbsoluteExpiration = _cacheExpirationHours });
				}
			}
			return PageRecords(lookupRecords, searchTerm, pageSize, pageNum, LookupsAjaxSearchType.Contains);
		}

		[HttpGet]
		public ActionResult LetterHeads(string searchTerm, int pageSize, int pageNum, bool? filter)
		{
			var blankRecord = new Select2Result() { id = ((int)LetterHeadType.None).ToString(), text = LetterHeadType.None.ToString() };

			var query = _dbSubmission.tbl_lst_Letterheads.AsQueryable();
			if (filter != null)
			{
				if (!(bool)filter)
				{
					query = query.Where(x => !x.Code.StartsWith("Draft"));
				}
			}

			var dataRecords = query
				.Where(x => x.Active)
				.OrderBy(x => x.Code)
				.Select(x => new Select2Result { id = SqlFunctions.StringConvert((double)x.Id).Trim(), text = x.Description })
				.ToList();

			var lookupRecords = new List<Select2Result> { blankRecord };
			lookupRecords.AddRange(dataRecords);

			return PageRecords(lookupRecords, searchTerm, pageSize, pageNum);
		}

		[HttpGet]
		public ActionResult Manufacturers(string searchTerm, int pageSize, int pageNum)
		{
			//load all records from database or cache
			var lookupRecords = _memCache[LookupAjax.Manufacturers.ToString()] as List<Select2Result>;

			if (lookupRecords == null)
			{
				var blankRecord = new Select2Result() { id = null, text = "" };

				var dataRecords = _dbSubmission.tbl_lst_fileManu
					.Where(x => x.Active)
					.OrderBy(x => x.Code)
					.Select(x => new Select2Result() { id = x.Code, text = x.Description })
					.ToList();

				lookupRecords = new List<Select2Result>();
				lookupRecords.Add(blankRecord);
				lookupRecords.AddRange(dataRecords);

				_memCache.Set(LookupAjax.Manufacturers.ToString(), lookupRecords, new CacheItemPolicy() { AbsoluteExpiration = _cacheExpirationHours });
			}

			if (!string.IsNullOrEmpty(searchTerm))
			{
				var exactMatch = lookupRecords.Where(x => x.id.Contains(searchTerm, StringComparison.OrdinalIgnoreCase)).ToList();
				var exactMatchIds = lookupRecords.Where(x => x.id.Contains(searchTerm, StringComparison.OrdinalIgnoreCase)).Select(x => x.id).ToList();
				var notExactMatch = lookupRecords.Where(x => !exactMatchIds.Contains(x.id) && x.text.Contains(searchTerm, StringComparison.OrdinalIgnoreCase)).OrderBy(x => x.text).ToList();
				lookupRecords = exactMatch.Union(notExactMatch).ToList();

				//lookupRecords = lookupRecords.Where(x => x.id.Contains(searchTerm, StringComparison.OrdinalIgnoreCase) || x.text.Contains(searchTerm, StringComparison.OrdinalIgnoreCase)).OrderBy(x => x.text).ToList();
			}

			return PageRecords(lookupRecords, searchTerm, pageSize, pageNum, LookupsAjaxSearchType.None);
		}
		[HttpGet]
		public ActionResult SASPollNames(string searchTerm, int pageSize, int pageNum)
		{
			//load all records from database or cache
			var lookupRecords = _memCache[LookupAjax.SASPollNames.ToString()] as List<Select2Result>;

			if (lookupRecords == null)
			{
				var blankRecord = new Select2Result() { id = null, text = "" };

				var dataRecords = _dbSubmission.tbl_lst_SASPollNames
					.OrderBy(x => x.PollName)
					.Select(x => new Select2Result() { id = x.Id.ToString(), text = x.PollName })
					.ToList();

				lookupRecords = new List<Select2Result>();
				lookupRecords.Add(blankRecord);
				lookupRecords.AddRange(dataRecords);

				_memCache.Set(LookupAjax.SASPollNames.ToString(), lookupRecords, new CacheItemPolicy() { AbsoluteExpiration = _cacheExpirationHours });
			}

			if (!string.IsNullOrEmpty(searchTerm))
			{
				var exactMatch = lookupRecords.Where(x => x.id.Contains(searchTerm, StringComparison.OrdinalIgnoreCase)).ToList();
				var exactMatchIds = lookupRecords.Where(x => x.id.Contains(searchTerm, StringComparison.OrdinalIgnoreCase)).Select(x => x.id).ToList();
				var notExactMatch = lookupRecords.Where(x => !exactMatchIds.Contains(x.id) && x.text.Contains(searchTerm, StringComparison.OrdinalIgnoreCase)).OrderBy(x => x.text).ToList();
				lookupRecords = exactMatch.Union(notExactMatch).ToList();

				//lookupRecords = lookupRecords.Where(x => x.id.Contains(searchTerm, StringComparison.OrdinalIgnoreCase) || x.text.Contains(searchTerm, StringComparison.OrdinalIgnoreCase)).OrderBy(x => x.text).ToList();
			}

			return PageRecords(lookupRecords, searchTerm, pageSize, pageNum, LookupsAjaxSearchType.None);
		}
		[HttpGet]
		public ActionResult ManufacturerGroups(string searchTerm, int pageSize, int pageNum)
		{
			//load all records from database or cache
			var lookupRecords = _memCache[LookupAjax.ManufacturerGroups.ToString()] as List<Select2Result>;

			if (lookupRecords == null)
			{
				var blankRecord = new Select2Result() { id = null, text = "" };

				var dataRecords = _dbSubmission.ManufacturerGroups.Include(x => x.ManufacturerGroupCategory)
					.Select(x => new Select2Result { id = x.Id.ToString(), text = x.Description + ": " + x.ManufacturerGroupCategory.Code })
					.OrderBy(x => x.text)
					.ToList();

				lookupRecords = new List<Select2Result>();
				lookupRecords.Add(blankRecord);
				lookupRecords.AddRange(dataRecords);

				_memCache.Set(LookupAjax.ManufacturerGroups.ToString(), lookupRecords, new CacheItemPolicy() { AbsoluteExpiration = _cacheExpirationHours });
			}

			return PageRecords(lookupRecords, searchTerm, pageSize, pageNum);
		}

		[HttpGet]
		public ActionResult ManufacturerGroupCategories(string searchTerm, int pageSize, int pageNum)
		{
			//load all records from database or cache
			var lookupRecords = _memCache[LookupAjax.ManufacturerGroupCategories.ToString()] as List<Select2Result>;

			if (lookupRecords == null)
			{
				var blankRecord = new Select2Result() { id = null, text = "" };

				var dataRecords = (from x in _dbSubmission.ManufacturerGroupCategories
								   orderby x.Id
								   select new Select2Result() { id = x.Id.ToString(), text = x.Code })
								   .ToList(); ;

				lookupRecords = new List<Select2Result>();
				lookupRecords.Add(blankRecord);
				lookupRecords.AddRange(dataRecords);

				_memCache.Set(LookupAjax.ManufacturerGroupCategories.ToString(), lookupRecords, new CacheItemPolicy() { AbsoluteExpiration = _cacheExpirationHours });
			}

			return PageRecords(lookupRecords, searchTerm, pageSize, pageNum);
		}

		[HttpGet]
		public ActionResult MathQueueManufacturerGroups(string searchTerm, int pageSize, int pageNum)
		{
			//load all records from database or cache
			var lookupRecords = _memCache[LookupAjax.MathQueueManufacturerGroups.ToString()] as List<Select2Result>;

			if (lookupRecords == null)
			{
				var blankRecord = new Select2Result() { id = null, text = "" };

				var dataRecords = _dbSubmission.ManufacturerGroups.Include(x => x.ManufacturerGroupCategory)
					.Where(x => x.ManufacturerGroupCategoryId == (int)ManufacturerGroupCategory.MathQueue)
					.Select(x => new Select2Result { id = x.Id.ToString(), text = x.Description + ": " + x.ManufacturerGroupCategory.Code })
					.OrderBy(x => x.text)
					.ToList();

				lookupRecords = new List<Select2Result>();
				lookupRecords.Add(blankRecord);
				lookupRecords.AddRange(dataRecords);

				_memCache.Set(LookupAjax.MathQueueManufacturerGroups.ToString(), lookupRecords, new CacheItemPolicy() { AbsoluteExpiration = _cacheExpirationHours });
			}

			return PageRecords(lookupRecords, searchTerm, pageSize, pageNum);
		}

		[HttpGet]
		public ActionResult ProtocolPlatformsManufacturerGroups(string searchTerm, int pageSize, int pageNum)
		{
			//load all records from database or cache
			var lookupRecords = _memCache[LookupAjax.ProtocolPlatformsManufacturerGroups.ToString()] as List<Select2Result>;

			if (lookupRecords == null)
			{
				var blankRecord = new Select2Result() { id = null, text = "" };

				var dataRecords = _dbSubmission.ManufacturerGroups.Include(x => x.ManufacturerGroupCategory)
					//.Where(x => x.ManufacturerGroupCategoryId == (int)ManufacturerGroupCategory.MathQueue)
					//TODO: Add ManufacturerGroupCategory.ProtocolPlatforms to ManufacturerGroupCategory Enum in Submissions.Common
					.Where(x => x.ManufacturerGroupCategory.Code == "ProtocolPlatforms")
					.Select(x => new Select2Result { id = x.Id.ToString(), text = x.Description + ": " + x.ManufacturerGroupCategory.Code })
					.OrderBy(x => x.text)
					.ToList();

				lookupRecords = new List<Select2Result>();
				lookupRecords.Add(blankRecord);
				lookupRecords.AddRange(dataRecords);

				_memCache.Set(LookupAjax.ProtocolPlatformsManufacturerGroups.ToString(), lookupRecords, new CacheItemPolicy() { AbsoluteExpiration = _cacheExpirationHours });
			}

			return PageRecords(lookupRecords, searchTerm, pageSize, pageNum);
		}



		[HttpGet]
		public ActionResult MobileDevices(string searchTerm, int pageSize, int pageNum)
		{
			var lookupRecords = _memCache[LookupAjax.MobileDevices.ToString()] as List<Select2Result>;

			if (lookupRecords == null)
			{
				var blankRecord = new Select2Result() { id = null, text = "" };
				var dataRecords = _dbSubmission.tbl_lst_MobileDevice
					.Select(x => new Select2Result { id = x.Id.ToString(), text = x.Device })
					.OrderBy(x => x.text)
					.ToList();
				lookupRecords = new List<Select2Result>
				{
					blankRecord
				};
				lookupRecords.AddRange(dataRecords);
				_memCache.Set(LookupAjax.MobileDevices.ToString(), lookupRecords, new CacheItemPolicy() { AbsoluteExpiration = _cacheExpirationHours });
			}
			return PageRecords(lookupRecords, searchTerm, pageSize, pageNum);
		}

		[HttpGet]
		public ActionResult MobileDevicesTested(string searchTerm, int pageSize, int pageNum)
		{
			var lookupRecords = _memCache[LookupAjax.MobileDevicesTested.ToString()] as List<Select2Result>;

			if (lookupRecords == null)
			{
				var blankRecord = new Select2Result() { id = null, text = "" };
				var dataRecords = _dbSubmission.tbl_lst_MobileDevice
					.Select(x => new Select2Result { id = x.Model, text = x.Device })
					.OrderBy(x => x.text)
					.ToList();
				lookupRecords = new List<Select2Result>
				{
					blankRecord
				};
				lookupRecords.AddRange(dataRecords);
				_memCache.Set(LookupAjax.MobileDevicesTested.ToString(), lookupRecords, new CacheItemPolicy() { AbsoluteExpiration = _cacheExpirationHours });
			}
			return PageRecords(lookupRecords, searchTerm, pageSize, pageNum);
		}

		[HttpGet]
		public ActionResult PaytableIds(string searchTerm, int pageSize, int pageNum, string filter)
		{
			var query = _dbSubmission.MathCoversheets.Where(x => x.MathFileNumber == filter);
			var dataRecords = new List<Select2Result>();

			dataRecords = query
					.OrderBy(x => x.PaytableId)
					.Select(x => new Select2Result() { id = x.Id.ToString(), text = x.PaytableId })
					.ToList();

			return PageRecords(dataRecords, searchTerm, pageSize, pageNum);
		}

		[HttpGet]
		public ActionResult Platforms(string searchTerm, int pageSize, int pageNum, string filter)
		{
			var blankRecord = new Select2Result() { id = null, text = "" };
			var query = _dbSubmission.tbl_lst_Platforms.AsQueryable();
			var dataRecords = new List<Select2Result>();

			if (!string.IsNullOrEmpty(filter))
			{
				var filters = JsonConvert.DeserializeObject<PlatformsFilter>(filter);
				if (filters.ManufactureGroupId != 0)
					query = query.Where(x => x.ManufacturerGroupId == filters.ManufactureGroupId);
				else if (!String.IsNullOrEmpty(filters.ManufactureShortId))
				{
					var groupCategoryId = 14;//ManufacturerPlatforms is 14, ProtocolPlatforms is 16
					if (filters.ManufacturerGroupCategoryId > 0)
						groupCategoryId = filters.ManufacturerGroupCategoryId;
					var id = _dbSubmission.ManufacturerGroups
						.Include(Group => Group.ManufacturerLinks)
						.Where(Group => Group.ManufacturerGroupCategoryId == groupCategoryId
										&& Group.ManufacturerLinks.Select(Link => Link.ManufacturerCode).Contains(filters.ManufactureShortId))
						.FirstOrDefault()?.Id ?? 0;
					query = query.Where(x => x.ManufacturerGroupId == id);

				}

				dataRecords = query
					.OrderBy(x => x.Name)
					.Select(x => new Select2Result() { id = x.Id.ToString(), text = x.Name })
					.ToList();
			}
			else
			{
				dataRecords = query
					.Select(x => new Select2Result() { id = x.Id.ToString(), text = x.Name + "  (" + x.ManufacturerGroup.ManufacturerGroupCategory.Code + ")" })
					.ToList();
			}

			var lookupRecords = new List<Select2Result>();
			lookupRecords.Add(blankRecord);
			lookupRecords.AddRange(dataRecords);

			return PageRecords(lookupRecords, searchTerm, pageSize, pageNum);
		}

		[HttpGet]
		public ActionResult PlatformPiecesTemplates(string searchTerm, int pageSize, int pageNum, string filter)
		{
			var blankRecord = new Select2Result() { id = null, text = "" };
			var query = _dbSubmission.PlatformPiecesTemplates
									.Include(ppt => ppt.Jurisdictions)
									.Where(ppt => ppt.Active && !ppt.DeactivateDate.HasValue)
									.AsQueryable();

			if (!string.IsNullOrEmpty(filter))
			{
				var filters = JsonConvert.DeserializeObject<PlatformPiecesTemplatesFilter>(filter);

				if (filters.JurisdictionIds != null && filters.JurisdictionIds.Count > 0)
				{
					var jurIds = filters.JurisdictionIds.Select(jid => jid.ToJurisdictionIdString()).ToList();
					query = query.Where(ppt => ppt.Jurisdictions.Select(j => j.JurisdictionId).Intersect(jurIds).Any());
				}

				if (filters.JurisdictionIds != null && filters.JurisdictionIds.Count > 0)
					query = query.Where(ppt => ppt.Jurisdictions.Select(j => j.JurisdictionId).Intersect(filters.JurisdictionIds).Any());

				if (filters.PlatformId > 0)
					query = query.Where(ppt => ppt.PlatformId == filters.PlatformId);

				if (!string.IsNullOrWhiteSpace(filters.ManufactureShortId))
					query = query.Where(ppt => ppt.ManufacturerCode == filters.ManufactureShortId);

			}

			var dataRecords = query
				.OrderBy(x => x.Name)
				.Select(x => new Select2Result() { id = x.Id.ToString(), text = x.Name })
				.ToList();

			var lookupRecords = new List<Select2Result>();
			lookupRecords.Add(blankRecord);
			lookupRecords.AddRange(dataRecords);

			return PageRecords(lookupRecords, searchTerm, pageSize, pageNum);
		}


		[HttpGet]
		public ActionResult ProductLines(string searchTerm, int pageSize, int pageNum, string filter)
		{
			var blankRecord = new Select2Result() { id = null, text = "" };

			var query = _dbSubmission.tbl_lst_ProductLines.AsQueryable();

			if (!string.IsNullOrEmpty(filter))
			{
				var filters = JsonConvert.DeserializeObject<ProductLinesFilter>(filter);

				if (filters.ManufacturerCode != null)
				{
					var manufacturerGroupIds = _dbSubmission.tbl_lst_fileManuManufacturerGroups
						.Where(x => x.ManufacturerCode == filters.ManufacturerCode)
						.Select(x => x.ManufacturerGroupId).ToList();
					query = query.Where(x => x.ManufacturerGroupId != null &&
					manufacturerGroupIds.Contains((int)x.ManufacturerGroupId) &&
					x.TestingTypeId == filters.TestingTypeId);
				}

			}

			var dataRecords = query
				.OrderBy(x => x.Name)
				.Select(x => new Select2Result() { id = SqlFunctions.StringConvert((double)x.Id).Trim(), text = x.Name })
				.ToList();

			//Unknown option at the end of the list
			string endText = "Unknown";
			if (dataRecords.Where(x => x.text == endText).FirstOrDefault() != null)
			{
				Select2Result temp = dataRecords.Where(x => x.text == endText).FirstOrDefault();
				dataRecords.RemoveAll(x => x.text == endText);
				dataRecords.Add(temp);
			}

			var lookupRecords = new List<Select2Result>();
			lookupRecords.Add(blankRecord);
			lookupRecords.AddRange(dataRecords);

			return PageRecords(lookupRecords, searchTerm, pageSize, pageNum);
		}

		[HttpGet]
		public ActionResult ProductClassifications(string searchTerm, int pagesize, int pageNum, string filter)
		{
			var blankRecord = new Select2Result() { id = null, text = "" };

			var query = _dbSubmission.tbl_lst_Classifications.AsQueryable();

			if (!string.IsNullOrEmpty(filter))
			{
				var filters = JsonConvert.DeserializeObject<ProductClassificationsFilter>(filter);

				if (filters.ManufacturerCode != null)
				{
					query = query.Where(x => x.ManufacturerId == filters.ManufacturerCode);
				}

			}

			var dataRecords = query
				.Where(x => x.Active == true)
				.Select(x => new Select2Result() { id = x.Id.ToString(), text = x.Name })
				.ToList();

			var lookupRecords = new List<Select2Result>();
			lookupRecords.Add(blankRecord);
			lookupRecords.AddRange(dataRecords);

			return PageRecords(lookupRecords, searchTerm, pagesize, pageNum);
		}

		[HttpGet]
		public ActionResult ProductJurisdictionalStatus(string searchTerm, int pageSize, int pageNum)
		{
			var lookupRecords = _memCache[LookupAjax.ProductJurisdictionalStatus.ToString()] as List<Select2Result>;

			if (lookupRecords == null)
			{
				var blankRecord = new Select2Result() { id = null, text = "" };
				var dataRecords = (from x in _dbSubmission.tbl_lst_ProductJurisdictionStatus
								   where x.Active
								   orderby x.Status
								   select new Select2Result() { id = SqlFunctions.StringConvert((double)x.Id).Trim(), text = x.Status })
								   .ToList();

				lookupRecords = new List<Select2Result>();
				lookupRecords.Add(blankRecord);
				lookupRecords.AddRange(dataRecords);

				_memCache.Set(LookupAjax.ProductJurisdictionalStatus.ToString(), lookupRecords, new CacheItemPolicy() { AbsoluteExpiration = _cacheExpirationHours });
			}

			return PageRecords(lookupRecords, searchTerm, pageSize, pageNum);
		}
		[HttpGet]
		public ActionResult ProjectReviewDetailCategory(string searchTerm, int pageSize, int pageNum, int filter)
		{
			var blankRecord = new Select2Result() { id = null, text = "" };

			var query = _dbSubmission.tbl_lst_ReviewDetailCategories.AsQueryable();

			if (filter != 0)
			{
				query = query.Where(x => x.DeptId == filter);
			}

			var dataRecords = query
				.Where(x => x.Active)
				.OrderBy(x => x.Description)
				.Select(x => new Select2Result { id = SqlFunctions.StringConvert((double)x.Id).Trim(), text = x.Description })
				.ToList();

			var lookupRecords = new List<Select2Result> { blankRecord };
			lookupRecords.AddRange(dataRecords);


			return PageRecords(lookupRecords, searchTerm, pageSize, pageNum);
		}

		[HttpGet]
		public ActionResult ProductTypes(string searchTerm, int pageSize, int pageNum, string filter)
		{
			var blankRecord = new Select2Result() { id = null, text = "" };
			var query = _dbSubmission.tbl_lst_ProductType.AsQueryable();

			if (!string.IsNullOrEmpty(filter))
			{
				var filters = JsonConvert.DeserializeObject<ProductTypesFilter>(filter);
				if (!string.IsNullOrWhiteSpace(filters.ManufacturerCode))
				{
					query = query.Where(x => x.ManufacturerId == null || x.ManufacturerId == filters.ManufacturerCode);
				}
			}
			else
			{
				query = query.Where(x => x.ManufacturerId == null);
			}

			var dataRecords = query
				.OrderBy(x => x.Name)
				.Select(x => new Select2Result() { id = x.Id.ToString(), text = x.Name })
				.ToList();

			var lookupRecords = new List<Select2Result>
				{
					blankRecord
				};
			lookupRecords.AddRange(dataRecords);

			return PageRecords(lookupRecords, searchTerm, pageSize, pageNum);
		}

		[HttpGet]
		public ActionResult ProjectOutputs(string searchTerm, int pageSize, int pageNum)
		{
			//load all records from database or cache
			var lookupRecords = _memCache[LookupAjax.ProjectOutputs.ToString()] as List<Select2Result>;

			if (lookupRecords == null)
			{
				var blankRecord = new Select2Result() { id = null, text = "" };

				var dataRecords = _dbSubmission.tbl_lst_ProjectOutputs
					.Where(x => x.Active == true)
					.OrderBy(x => x.Name)
					.Select(x => new Select2Result { id = x.ProjectOutputId.ToString(), text = x.Name })
					.ToList();

				lookupRecords = new List<Select2Result>();
				lookupRecords.Add(blankRecord);
				lookupRecords.AddRange(dataRecords);

				_memCache.Set(LookupAjax.ProjectOutputs.ToString(), lookupRecords, new CacheItemPolicy() { AbsoluteExpiration = _cacheExpirationHours });
			}
			return PageRecords(lookupRecords, searchTerm, pageSize, pageNum);
		}

		[HttpGet]
		public ActionResult ProjectName(string searchTerm, int pageSize, int pageNum, string filter)
		{
			var blankRecord = new Select2Result() { id = null, text = "" };

			var query = (from x in _dbSubmission.trProjects
						 join q in _dbSubmission.QABundles on x.Id equals q.ProjectId
						 where x.ProjectName.StartsWith(searchTerm)
						 select (new
						 {
							 x.Id,
							 x.ENStatus,
							 BundleStatus = q.QAStatus,
							 x.IsTransfer,
							 x.BundleTypeId,
							 x.ProjectName
						 })
								 ).AsQueryable();


			if (!string.IsNullOrEmpty(filter))
			{
				var filters = JsonConvert.DeserializeObject<ProjectFilter>(filter);

				if (filters.InStatus.Count > 0)
				{
					query = query.Where(x => filters.InStatus.Contains(x.ENStatus));
				}

				if (filters.InBundleStatus.Count > 0)
				{
					query = query.Where(x => filters.InBundleStatus.Contains(x.BundleStatus));
				}

				if (filters.IsTransfer != null)
				{
					query = query.Where(x => x.IsTransfer == filters.IsTransfer);
				}

				if (filters.ProjectType != null)
				{
					query = query.Where(x => x.BundleTypeId == (int)filters.ProjectType);
				}
			}

			var dataRecords = query
				.OrderBy(x => x.ProjectName)
				.Select(x => new Select2Result { id = SqlFunctions.StringConvert((double)x.Id).Trim(), text = x.ProjectName })
				.ToList();

			var lookupRecords = new List<Select2Result>();
			lookupRecords.Add(blankRecord);
			lookupRecords.AddRange(dataRecords);

			return PageRecords(lookupRecords, searchTerm, pageSize, pageNum, LookupsAjaxSearchType.Contains);

		}

		[HttpGet]
		public ActionResult QATaskDefinitions(string searchTerm, int pageSize, int pageNum, string filter)
		{
			var blankRecord = new Select2Result() { id = null, text = "" };
			var query = _dbSubmission.QABundles_TaskDef.Where(x => x.TaskDefDesc != null);

			if (!string.IsNullOrEmpty(filter))
			{
				var filters = JsonConvert.DeserializeObject<QATasksFilter>(filter);

				if (filters.IsTaskForBundlesDN != null)
				{
					if (filters.IsTaskForBundlesDN == true)
					{
						query = query.Where(x => x.TaskForBundlesDN == true);
					}
					else
					{
						query = query.Where(x => x.TaskForBundles == true);
					}
				}
			}
			else
			{
				query = query.Where(x => x.TaskForBundles == true);
			}

			var dataRecords = query
				.OrderBy(x => x.TaskDefDesc)
				.Select(x => new Select2Result { id = SqlFunctions.StringConvert((double)x.Id).Trim(), text = x.TaskDefDesc })
				.ToList();

			var lookupRecords = new List<Select2Result>();
			lookupRecords.Add(blankRecord);
			lookupRecords.AddRange(dataRecords);

			return PageRecords(lookupRecords, searchTerm, pageSize, pageNum, LookupsAjaxSearchType.Contains);
		}

		[HttpGet]
		public ActionResult QAUsers(string searchTerm, int pageSize, int pageNum, string filter)
		{
			var blankRecord = new Select2Result() { id = null, text = "" };

			var query = _dbSubmission.trLogins
				.Where(x => x.Status == "A" && x.Department.Code == "QA");

			if (!string.IsNullOrEmpty(filter))
			{

			}

			var dataRecords = query
				.OrderBy(x => x.FName)
				.ThenBy(x => x.LName)
				.Select(x => new Select2Result() { id = SqlFunctions.StringConvert((double)x.Id).Trim(), text = x.FName + " " + x.LName })
				.ToList();

			var lookupRecords = new List<Select2Result>();
			lookupRecords.Add(blankRecord);
			lookupRecords.AddRange(dataRecords);

			return PageRecords(lookupRecords, searchTerm, pageSize, pageNum, LookupsAjaxSearchType.Contains);
		}

		[HttpGet]
		public ActionResult Roles(string searchTerm, int pageSize, int pageNum)
		{
			//load all records from database or cache
			var lookupRecords = _memCache[LookupAjax.Roles.ToString()] as List<Select2Result>;

			if (lookupRecords == null)
			{
				var blankRecord = new Select2Result() { id = null, text = "" };

				var dataRecords = (from x in _dbSubmission.Roles
								   orderby x.Code
								   select new Select2Result() { id = x.Id.ToString(), text = x.Description })
								   .ToList();

				lookupRecords = new List<Select2Result>();
				lookupRecords.Add(blankRecord);
				lookupRecords.AddRange(dataRecords);

				_memCache.Set(LookupAjax.Roles.ToString(), lookupRecords, new CacheItemPolicy() { AbsoluteExpiration = _cacheExpirationHours });
			}

			return PageRecords(lookupRecords, searchTerm, pageSize, pageNum);
		}

		[HttpGet]
		public ActionResult ProjectReviews(string searchTerm, int pageSize, int pageNum)
		{
			//load all records from database or cache
			var lookupRecords = _memCache[LookupAjax.ProjectReviews.ToString()] as List<Select2Result>;

			if (lookupRecords == null)
			{
				var blankRecord = new Select2Result() { id = null, text = "" };

				var dataRecords = _dbSubmission.tbl_lst_ReviewDetailCategories
					.OrderBy(x => x.Description)
					.Select(x => new Select2Result { id = x.Id.ToString(), text = x.Description })
					.ToList();

				lookupRecords = new List<Select2Result>();
				lookupRecords.Add(blankRecord);
				lookupRecords.AddRange(dataRecords);

				_memCache.Set(LookupAjax.ProjectReviews.ToString(), lookupRecords, new CacheItemPolicy() { AbsoluteExpiration = _cacheExpirationHours });
			}
			return PageRecords(lookupRecords, searchTerm, pageSize, pageNum);
		}

		[HttpGet]
		public ActionResult RevisionReasons(string searchTerm, int pageSize, int pageNum)
		{
			var lookupRecords = _memCache[LookupAjax.RevisionReasons.ToString()] as List<Select2Result>;

			if (lookupRecords == null)
			{
				var blankRecord = new Select2Result() { id = null, text = "" };

				var dataRecords = _dbSubmission.tbl_lst_RevisionReasons
					.Where(x => x.Active == true)
					.OrderBy(x => x.Name)
					.Select(x => new Select2Result { id = x.Id.ToString(), text = x.Name })
					.ToList();

				lookupRecords = new List<Select2Result>();
				lookupRecords.Add(blankRecord);
				lookupRecords.AddRange(dataRecords);

				_memCache.Set(LookupAjax.RevisionReasons.ToString(), lookupRecords, new CacheItemPolicy() { AbsoluteExpiration = _cacheExpirationHours });
			}
			return PageRecords(lookupRecords, searchTerm, pageSize, pageNum);
		}

		[HttpGet]
		public ActionResult SecondaryFunctions(string searchTerm, int pageSize, int pageNum)
		{
			var lookupRecords = _memCache[LookupAjax.SecondaryFunctions.ToString()] as List<Select2Result>;

			if (lookupRecords == null)
			{
				var blankRecord = new Select2Result() { id = null, text = "" };

				var dataRecords = _dbSubmission.SubmissionsSecondaryFunctions
					.OrderBy(x => x.Name)
					.Select(x => new Select2Result { id = x.Id.ToString(), text = x.Name })
					.ToList();

				lookupRecords = new List<Select2Result>();
				lookupRecords.Add(blankRecord);
				lookupRecords.AddRange(dataRecords);

				_memCache.Set(LookupAjax.SecondaryFunctions.ToString(), lookupRecords, new CacheItemPolicy() { AbsoluteExpiration = _cacheExpirationHours });
			}

			return PageRecords(lookupRecords, searchTerm, pageSize, pageNum);
		}

		[HttpGet]
		public ActionResult ShipmentRecipients(string searchTerm, int pageSize, int pageNum)
		{
			//load all records from database or cache
			var lookupRecords = _memCache[LookupAjax.ShipmentRecipients.ToString()] as List<Select2Result>;

			if (lookupRecords == null)
			{
				var blankRecord = new Select2Result() { id = null, text = "" };

				var dataRecords = _dbSubmission.tbl_lst_ShippingRecipients
					.Where(x => x.Active)
					.OrderBy(x => x.ShippedTo)
					.Select(x => new Select2Result { id = x.Id.ToString(), text = x.ShippedTo })
					.ToList();

				lookupRecords = new List<Select2Result>();
				lookupRecords.Add(blankRecord);
				lookupRecords.AddRange(dataRecords);

				_memCache.Set(LookupAjax.ShipmentRecipients.ToString(), lookupRecords, new CacheItemPolicy() { AbsoluteExpiration = _cacheExpirationHours });
			}

			return PageRecords(lookupRecords, searchTerm, pageSize, pageNum);
		}

		[HttpGet]
		public ActionResult ShipmentCouriers(string searchTerm, int pageSize, int pageNum)
		{
			//load all records from database or cache
			var lookupRecords = _memCache[LookupAjax.ShipmentCouriers.ToString()] as List<Select2Result>;

			if (lookupRecords == null)
			{
				var blankRecord = new Select2Result() { id = null, text = "" };

				var dataRecords = _dbSubmission.tbl_lst_Shippers
					.OrderBy(x => x.Code)
					.Select(x => new Select2Result { id = x.Id.ToString(), text = x.Code })
					.ToList();

				lookupRecords = new List<Select2Result>();
				lookupRecords.Add(blankRecord);
				lookupRecords.AddRange(dataRecords);

				_memCache.Set(LookupAjax.ShipmentCouriers.ToString(), lookupRecords, new CacheItemPolicy() { AbsoluteExpiration = _cacheExpirationHours });
			}

			return PageRecords(lookupRecords, searchTerm, pageSize, pageNum);
		}

		[HttpGet]
		public ActionResult SignatureScopes(string searchTerm, int pageSize, int pageNum)
		{
			var lookupRecords = _memCache[LookupAjax.SignatureScopes.ToString()] as List<Select2Result>;

			if (lookupRecords == null)
			{
				var blankRecord = new Select2Result() { id = null, text = "" };

				var dataRecords = _dbSubmission.tbl_lst_SignatureScope
					.Where(x => x.Active)
					.OrderBy(x => x.Scope)
					.Select(x => new Select2Result { id = x.Id.ToString(), text = x.Scope })
					.ToList();

				lookupRecords = new List<Select2Result>();
				lookupRecords.Add(blankRecord);
				lookupRecords.AddRange(dataRecords);

				_memCache.Set(LookupAjax.SignatureScopes.ToString(), lookupRecords, new CacheItemPolicy() { AbsoluteExpiration = _cacheExpirationHours });
			}

			return PageRecords(lookupRecords, searchTerm, pageSize, pageNum);
		}

		[HttpGet]
		public ActionResult SignatureTypes(string searchTerm, int pageSize, int pageNum, string filter)
		{
			var blankRecord = new Select2Result() { id = null, text = "" };
			var query = _dbSubmission.tbl_lst_SignatureTypes.AsQueryable();
			if (!string.IsNullOrEmpty(filter))
			{
				var filters = JsonConvert.DeserializeObject<SignatureFilter>(filter);
				query = query.Where(x => !filters.ExcludedTypes.Contains(x.Type));
			}

			var dataRecords = query.Where(x => x.Type.StartsWith(searchTerm) && x.Active == true)
				.OrderBy(x => x.Type)
				.Select(x => new Select2Result() { id = SqlFunctions.StringConvert((double)x.Id).Trim(), text = x.Type })
				.ToList();

			var lookupRecords = new List<Select2Result>();
			lookupRecords = new List<Select2Result>();
			lookupRecords.Add(blankRecord);
			lookupRecords.AddRange(dataRecords);

			return PageRecords(lookupRecords, searchTerm, pageSize, pageNum);
		}

		[HttpGet]
		public ActionResult SignatureVersions(string searchTerm, int pageSize, int pageNum, string filter)
		{
			var blankRecord = new Select2Result() { id = null, text = "" };
			var query = _dbSubmission.tbl_lst_SignatureVersions.AsQueryable();

			if (!string.IsNullOrEmpty(filter))
			{
				var filters = JsonConvert.DeserializeObject<SignatureFilter>(filter);
				int? signatureTypeToInt = filters.TypeId == null ? (int?)null : int.Parse(filters.TypeId);
				var signatureVersions = _dbSubmission.tbl_SignatureType_Versions
					.Where(x => x.SignatureTypeId == signatureTypeToInt)
					.Select(x => x.SignatureVersionId).ToList();
				query = query.Where(x => signatureVersions.Contains((int)x.Id));
			}

			var dataRecords = query.Where(x => x.Version.StartsWith(searchTerm) && x.Active == true)
				.OrderBy(x => x.Version)
				.Select(x => new Select2Result() { id = SqlFunctions.StringConvert((double)x.Id).Trim(), text = x.Version })
				.ToList();

			var lookupRecords = new List<Select2Result>();
			lookupRecords = new List<Select2Result>();
			lookupRecords.Add(blankRecord);
			lookupRecords.AddRange(dataRecords);

			return PageRecords(lookupRecords, searchTerm, pageSize, pageNum);
		}

		[HttpGet]
		public ActionResult SpecializedTypes(string searchTerm, int pageSize, int pageNum, string filter)
		{
			var blankRecord = new Select2Result() { id = null, text = "" };
			var query = _dbSubmission.tbl_lst_SpecializedTypes.AsQueryable();

			var dataRecords = query
					.Where(x => x.Active == true)
					.OrderBy(x => x.Name)
					.Select(x => new Select2Result { id = x.Id.ToString(), text = x.Name })
					.ToList();

			var lookupRecords = new List<Select2Result>();
			lookupRecords.Add(blankRecord);
			lookupRecords.AddRange(dataRecords);
			return PageRecords(lookupRecords, searchTerm, pageSize, pageNum, LookupsAjaxSearchType.Contains);
		}

		[HttpGet]
		public ActionResult SportsBettingJurisdictions(string searchTerm, int pageSize, int pageNum, string filter)
		{
			var lookupRecords = _memCache[LookupAjax.SportsBettingJurisdictions.ToString()] as List<Select2Result>;

			if (lookupRecords == null)
			{
				var blankRecord = new Select2Result() { id = null, text = "" };

				var usedRecords = _dbSubmission.GamingGuidelineSportsBetting.Select(x => x.JurisdictionId.ToString()).ToList();

				var dataRecords = _dbSubmission.tbl_lst_fileJuris
					.Where(x => x.Active && !usedRecords.Contains(x.Id))
					.OrderBy(x => x.Name)
					.Select(x => new Select2Result
					{
						id = x.Id.Trim(),
						text = (x.FormattedJurisdictionName != "" && x.FormattedJurisdictionName != null) ? x.FormattedJurisdictionName + " (" + x.Id.ToString() + ")" : x.Name + " (" + x.Id.ToString() + ")"
					})
					.ToList();

				lookupRecords = new List<Select2Result>();
				lookupRecords.Add(blankRecord);
				lookupRecords.AddRange(dataRecords);

				_memCache.Set(LookupAjax.SportsBettingJurisdictions.ToString(), lookupRecords, new CacheItemPolicy() { AbsoluteExpiration = _cacheExpirationHours });
			}

			if (!string.IsNullOrEmpty(searchTerm))
			{
				var exactMatch = lookupRecords.Where(x => x.id == searchTerm).ToList();
				var exactMatchIds = lookupRecords.Where(x => x.id == searchTerm).Select(x => x.id).ToList();
				var notExactMatch = lookupRecords.Where(x => !exactMatchIds.Contains(x.id) && x.text.Contains(searchTerm, StringComparison.OrdinalIgnoreCase)).OrderBy(x => x.text).ToList();
				lookupRecords = exactMatch.Union(notExactMatch).ToList();
			}

			if (!string.IsNullOrEmpty(filter))
			{
				var filters = JsonConvert.DeserializeObject<JurisdictionsFilter>(filter);

				if (filters.ExcludedJurisdictionIds.Count > 0)
				{
					lookupRecords = lookupRecords.Where(x => !filters.ExcludedJurisdictionIds.Contains(x.id)).ToList();
				}

				if (filters.JurisdictionIds.Count > 0)
				{
					lookupRecords = lookupRecords.Where(x => filters.JurisdictionIds.Contains(x.id)).ToList();
				}
			}

			return PageRecords(lookupRecords, searchTerm, pageSize, pageNum, LookupsAjaxSearchType.None);
		}
		[HttpGet]
		public ActionResult Studios(string searchTerm, int pageSize, int pageNum, string filter)
		{
			var blankRecord = new Select2Result() { id = null, text = "" };

			var query = _dbSubmission.tbl_lst_Studios.AsQueryable();

			if (!string.IsNullOrEmpty(filter))
			{
				var filters = JsonConvert.DeserializeObject<StudiosFilter>(filter);

				if (filters.ManufacturerCode != null)
				{
					var manufacturerGroupIds = _dbSubmission.tbl_lst_fileManuManufacturerGroups
						.Where(x => x.ManufacturerCode == filters.ManufacturerCode)
						.Select(x => x.ManufacturerGroupId).ToList();
					query = query.Where(x => x.ManufacturerGroupId != null &&
					manufacturerGroupIds.Contains((int)x.ManufacturerGroupId) &&
					x.TestingTypeId == filters.TestingTypeId);
				}

			}

			var dataRecords = query
				.OrderBy(x => x.Name)
				.Select(x => new Select2Result() { id = SqlFunctions.StringConvert((double)x.Id).Trim(), text = x.Name })
				.ToList();

			//Unknown option at the end of the list
			string endText = "Unknown";
			if (dataRecords.Where(x => x.text == endText).FirstOrDefault() != null)
			{
				Select2Result temp = dataRecords.Where(x => x.text == endText).FirstOrDefault();
				dataRecords.RemoveAll(x => x.text == endText);
				dataRecords.Add(temp);
			}

			var lookupRecords = new List<Select2Result>();
			lookupRecords.Add(blankRecord);
			lookupRecords.AddRange(dataRecords);

			return PageRecords(lookupRecords, searchTerm, pageSize, pageNum);
		}

		[HttpGet]
		public ActionResult StandardJurisdictions(string searchTerm, int pageSize, int pageNum)
		{
			//load all records from database or cache
			var lookupRecords = _memCache[LookupAjax.StandardJurisdictions.ToString()] as List<Select2Result>;
			if (lookupRecords == null)
			{
				var blankRecord = new Select2Result() { id = null, text = "" };
				var dataRecords = (from x in _dbSubmission.tbl_lst_StandardsAll
								   where x.Active == true
								   orderby x.Jurisdiction.SubCode
								   select new Select2Result()
								   {
									   id = x.JurisdictionId.Trim(),
									   text = x.Jurisdiction.SubCode + " - " + x.Jurisdiction.Name
								   })
								   .OrderBy(x => x.text)
								   .Distinct()
								   .ToList();
				lookupRecords = new List<Select2Result>();
				lookupRecords.Add(blankRecord);
				lookupRecords.AddRange(dataRecords);
				_memCache.Set(LookupAjax.StandardJurisdictions.ToString(), lookupRecords, new CacheItemPolicy() { AbsoluteExpiration = _cacheExpirationHours });
			}
			return PageRecords(lookupRecords, searchTerm, pageSize, pageNum);
		}

		[HttpGet]
		public ActionResult Standards(string searchTerm, int pageSize, int pageNum, string filter)
		{
			//load all records from database or cache
			var lookupRecords = _memCache[LookupAjax.Standards.ToString()] as List<Select2Result>;

			var query = _dbSubmission.tbl_lst_StandardsAll.Where(x => x.Active == true).AsQueryable();
			if (!string.IsNullOrWhiteSpace(filter))
			{
				var jurisdiction = JsonConvert.DeserializeObject<string>(filter);
				if (!string.IsNullOrEmpty(jurisdiction))
				{
					query = query.Where(x => x.JurisdictionId == jurisdiction);
				}

			}

			var blankRecord = new Select2Result() { id = null, text = "" };
			var dataRecords = query
								.Select(x => new Select2Result()
								{
									id = SqlFunctions.StringConvert((double)x.Id).Trim(),
									text = x.Standard
								})
								.OrderBy(x => x.text)
								.ToList();
			lookupRecords = new List<Select2Result>();
			lookupRecords.Add(blankRecord);
			lookupRecords.AddRange(dataRecords);
			_memCache.Set(LookupAjax.Standards.ToString(), lookupRecords, new CacheItemPolicy() { AbsoluteExpiration = _cacheExpirationHours });

			return PageRecords(lookupRecords, searchTerm, pageSize, pageNum);
		}

		[HttpGet]
		public ActionResult SnapShots(string searchTerm, int pageSize, int pageNum)
		{
			//load all records from database or cache
			var lookupRecords = _memCache[LookupAjax.SnapShots.ToString()] as List<Select2Result>;

			if (lookupRecords == null)
			{
				var blankRecord = new Select2Result() { id = null, text = "" };

				var dataRecords = _dbEResultsManaged.SnapshotStatus
					.Where(x => x.Status != "deleted")
					.OrderByDescending(x => x.SnapshotCreated)
					.Select(x => new Select2Result { id = x.SnapshotCreated.ToString(), text = x.SnapshotCreated.ToString() })
					.ToList();

				lookupRecords = new List<Select2Result>();
				lookupRecords.Add(blankRecord);
				lookupRecords.AddRange(dataRecords);

				_memCache.Set(LookupAjax.TestCase.ToString(), lookupRecords, new CacheItemPolicy() { AbsoluteExpiration = _cacheExpirationHours });
			}

			return PageRecords(lookupRecords, searchTerm, pageSize, pageNum);
		}

		[HttpGet]
		public ActionResult SubmissionTypes(string searchTerm, int pageSize, int pageNum)
		{
			//load all records from database or cache
			var lookupRecords = _memCache[LookupAjax.SubmissionTypes.ToString()] as List<Select2Result>;

			if (lookupRecords == null)
			{
				var blankRecord = new Select2Result() { id = null, text = "" };

				var dataRecords = (from x in _dbSubmission.tbl_lst_SubmissionType
								   where x.Active == true
								   orderby x.Code
								   select new Select2Result() { id = SqlFunctions.StringConvert((double)x.Id).Trim(), text = x.Code + " - " + x.Description })
								   .ToList();

				lookupRecords = new List<Select2Result>();
				lookupRecords.Add(blankRecord);
				lookupRecords.AddRange(dataRecords);

				_memCache.Set(LookupAjax.SubmissionTypes.ToString(), lookupRecords, new CacheItemPolicy() { AbsoluteExpiration = _cacheExpirationHours });
			}

			return PageRecords(lookupRecords, searchTerm, pageSize, pageNum);
		}

		[HttpGet]
		public ActionResult SubscriptionGroups(string searchTerm, int pageSize, int pageNum)
		{
			//load all records from database or cache
			var lookupRecords = _memCache[LookupAjax.SubscriptionGroups.ToString()] as List<Select2Result>;

			if (lookupRecords == null)
			{
				var blankRecord = new Select2Result() { id = null, text = "" };

				var dataRecords = (from x in _dbSubmission.tbl_lst_Subscription_Groups
								   orderby x.Name
								   select new Select2Result() { id = SqlFunctions.StringConvert((double)x.Id).Trim(), text = x.Name })
								   .ToList();

				lookupRecords = new List<Select2Result>();
				lookupRecords.Add(blankRecord);
				lookupRecords.AddRange(dataRecords);

				_memCache.Set(LookupAjax.SubscriptionGroups.ToString(), lookupRecords, new CacheItemPolicy() { AbsoluteExpiration = _cacheExpirationHours });
			}

			return PageRecords(lookupRecords, searchTerm, pageSize, pageNum);
		}

		[HttpGet]
		public ActionResult Subscriptions(string searchTerm, int pageSize, int pageNum)
		{
			//load all records from database or cache
			var lookupRecords = _memCache[LookupAjax.Subscriptions.ToString()] as List<Select2Result>;

			if (lookupRecords == null)
			{
				var blankRecord = new Select2Result() { id = null, text = "" };

				var dataRecords = (from x in _dbSubmission.Subscription
								   orderby x.Name
								   select new Select2Result() { id = SqlFunctions.StringConvert((double)x.Id).Trim(), text = x.Name })
								   .ToList();

				lookupRecords = new List<Select2Result>();
				lookupRecords.Add(blankRecord);
				lookupRecords.AddRange(dataRecords);

				_memCache.Set(LookupAjax.Subscriptions.ToString(), lookupRecords, new CacheItemPolicy() { AbsoluteExpiration = _cacheExpirationHours });
			}

			return PageRecords(lookupRecords, searchTerm, pageSize, pageNum);
		}

		[HttpGet]
		public ActionResult SupplierFocus(string searchTerm, int pageSize, int pageNum)
		{
			//load all records from database or cache
			var lookupRecords = _memCache[LookupAjax.SupplierFocus.ToString()] as List<Select2Result>;

			if (lookupRecords == null)
			{
				var blankRecord = new Select2Result() { id = null, text = "" };

				var dataRecords = _dbSubmission.tbl_lst_SupplierFocus
					.Where(x => x.Active)
					.OrderBy(x => x.Description)
					.Select(x => new Select2Result() { id = x.SupplierFocusId.ToString(), text = x.Description })
					.ToList();

				lookupRecords = new List<Select2Result>();
				lookupRecords.Add(blankRecord);
				lookupRecords.AddRange(dataRecords);

				_memCache.Set(LookupAjax.SupplierFocus.ToString(), lookupRecords, new CacheItemPolicy() { AbsoluteExpiration = _cacheExpirationHours });
			}

			return PageRecords(lookupRecords, searchTerm, pageSize, pageNum);
		}

		[HttpGet]
		public ActionResult TargetDateChangeReason(string searchTerm, int pageSize, int pageNum)
		{
			//load all records from database or cache
			var lookupRecords = _memCache[LookupAjax.TargetDateChangeReason.ToString()] as List<Select2Result>;

			if (lookupRecords == null)
			{
				var blankRecord = new Select2Result() { id = null, text = "" };

				var dataRecords = (from x in _dbSubmission.tbl_lst_TargetDateChangeReasson
								   orderby x.Reason
								   select new Select2Result() { id = SqlFunctions.StringConvert((double)x.Id).Trim(), text = x.Reason })
								   .ToList();

				lookupRecords = new List<Select2Result>();
				lookupRecords.Add(blankRecord);
				lookupRecords.AddRange(dataRecords);

				_memCache.Set(LookupAjax.TargetDateChangeReason.ToString(), lookupRecords, new CacheItemPolicy() { AbsoluteExpiration = _cacheExpirationHours });
			}

			return PageRecords(lookupRecords, searchTerm, pageSize, pageNum);
		}

		[HttpGet]
		public ActionResult TaskDefinitions(string searchTerm, int pageSize, int pageNum, string filter)
		{
			var blankRecord = new Select2Result() { id = null, text = "" };
			var query = _dbSubmission.trTaskDefs.Where(x => x.Disabled == 0);

			if (!string.IsNullOrEmpty(filter))
			{
				var filters = JsonConvert.DeserializeObject<TasksFilter>(filter);

				if (filters.DepartmentIds.Count > 0)
				{
					query = query.Where(x => filters.DepartmentIds.Contains((int)x.DepartmentId));
				}
				if (filters.IsDynamicSet)
				{
					query = query.Where(x => !x.IsAcumaticaSet);
				}
				else
				{
					query = query.Where(x => x.IsAcumaticaSet);
				}
			}

			var dataRecords = query
				.OrderBy(x => x.Code)
				.Select(x => new Select2Result { id = SqlFunctions.StringConvert((double)x.Id).Trim(), text = x.Code + (x.DynamicsId == null ? "" : " - " + x.DynamicsId), deprecated = (x.IsAcumaticaSet) ? false : true })
				.ToList();

			var lookupRecords = new List<Select2Result>();
			lookupRecords.Add(blankRecord);
			lookupRecords.AddRange(dataRecords);

			return PageRecords(lookupRecords, searchTerm, pageSize, pageNum, LookupsAjaxSearchType.Contains);
		}

		[HttpGet]
		public ActionResult TaskDocuments(string searchTerm, int pageSize, int pageNum)
		{
			//load all records from database or cache
			var lookupRecords = _memCache[LookupAjax.TaskDocuments.ToString()] as List<Select2Result>;

			if (lookupRecords == null)
			{
				var blankRecord = new Select2Result() { id = null, text = "" };

				var dataRecords = _dbSubmission.trTaskDocs
					.Select(x => new Select2Result { id = SqlFunctions.StringConvert((double)x.Id).Trim(), text = x.Name })
					.ToList();

				lookupRecords = new List<Select2Result>();
				lookupRecords.Add(blankRecord);
				lookupRecords.AddRange(dataRecords);

				_memCache.Set(LookupAjax.TaskDocuments.ToString(), lookupRecords, new CacheItemPolicy() { AbsoluteExpiration = _cacheExpirationHours });
			}

			return PageRecords(lookupRecords, searchTerm, pageSize, pageNum);
		}

		[HttpGet]
		public ActionResult TaskTemplates(string searchTerm, int pageSize, int pageNum, string filter)
		{
			var blankRecord = new Select2Result() { id = null, text = "" };

			var query = _dbSubmission.trTaskTemplates.AsQueryable();

			if (!string.IsNullOrEmpty(filter))
			{
				var filters = JsonConvert.DeserializeObject<TaskTemplatesFilter>(filter);

				if (filters.UserIds.Count > 0)
				{
					query = query.Where(x => filters.UserIds.Contains((int)x.UserId) || x.IsGlobal == 1);
				}
			}

			var dataRecords = query
				.OrderBy(x => x.Code)
				.Select(x => new Select2Result { id = SqlFunctions.StringConvert((double)x.Id).Trim(), text = x.Code })
				.ToList();


			var lookupRecords = new List<Select2Result>();
			lookupRecords.Add(blankRecord);
			lookupRecords.AddRange(dataRecords);

			return PageRecords(lookupRecords, searchTerm, pageSize, pageNum);
		}

		[HttpGet]
		public ActionResult Teams(string searchTerm, int pageSize, int pageNum)
		{
			var lookupRecords = _memCache[LookupAjax.Teams.ToString()] as List<Select2Result>;

			if (lookupRecords == null)
			{
				var blankRecord = new Select2Result() { id = null, text = "" };

				var dataRecords = _dbSubmission.tbl_lst_Teams
					.Where(x => x.Active)
					.OrderBy(x => x.Code)
					.Select(x => new Select2Result() { id = x.Id.ToString(), text = x.Code + " - " + x.Description })
					.ToList();

				lookupRecords = new List<Select2Result>();
				lookupRecords.Add(blankRecord);
				lookupRecords.AddRange(dataRecords);

				_memCache.Set(LookupAjax.Teams.ToString(), lookupRecords, new CacheItemPolicy() { AbsoluteExpiration = _cacheExpirationHours });
			}

			return PageRecords(lookupRecords, searchTerm, pageSize, pageNum);
		}

		[HttpGet]
		public ActionResult TestCase(string searchTerm, int pageSize, int pageNum)
		{
			//load all records from database or cache
			var lookupRecords = _memCache[LookupAjax.TestCase.ToString()] as List<Select2Result>;

			if (lookupRecords == null)
			{
				var blankRecord = new Select2Result() { id = null, text = "" };

				var dataRecords = _dbEResultsManaged.QuestionBase
					.Where(x => x.Inactive == false && x.aType == 9 && x.State != 3)
					.OrderBy(x => x.Name)
					.Select(x => new Select2Result { id = x.Id.ToString(), text = x.Name })
					.ToList();

				lookupRecords = new List<Select2Result>();
				lookupRecords.Add(blankRecord);
				lookupRecords.AddRange(dataRecords);

				_memCache.Set(LookupAjax.TestCase.ToString(), lookupRecords, new CacheItemPolicy() { AbsoluteExpiration = _cacheExpirationHours });
			}

			return PageRecords(lookupRecords, searchTerm, pageSize, pageNum);
		}

		[HttpGet]
		public ActionResult TestingPerformed(string searchTerm, int pageSize, int pageNum)
		{
			//load all records from database or cache
			var lookupRecords = _memCache[LookupAjax.TestingPerformed.ToString()] as List<Select2Result>;

			if (lookupRecords == null)
			{
				var blankRecord = new Select2Result() { id = null, text = "" };

				var dataRecords = (from x in _dbSubmission.tbl_lst_TestingPerformed
								   orderby x.Code
								   select new Select2Result() { id = SqlFunctions.StringConvert((double)x.Id).Trim(), text = x.Code })
								   .ToList();

				lookupRecords = new List<Select2Result>();
				lookupRecords.Add(blankRecord);
				lookupRecords.AddRange(dataRecords);

				_memCache.Set(LookupAjax.TestingPerformed.ToString(), lookupRecords, new CacheItemPolicy() { AbsoluteExpiration = _cacheExpirationHours });
			}

			return PageRecords(lookupRecords, searchTerm, pageSize, pageNum);
		}

		[HttpGet]
		public ActionResult TestingTypes(string searchTerm, int pageSize, int pageNum)
		{
			//load all records from database or cache
			var lookupRecords = _memCache[LookupAjax.TestingTypes.ToString()] as List<Select2Result>;

			if (lookupRecords == null)
			{
				var blankRecord = new Select2Result() { id = null, text = "" };

				var dataRecords = (from x in _dbSubmission.tbl_lst_TestingType
								   where x.Active == true
								   orderby x.Code
								   select new Select2Result() { id = SqlFunctions.StringConvert((double)x.Id).Trim(), text = x.Code })
								   .ToList();

				lookupRecords = new List<Select2Result>();
				lookupRecords.Add(blankRecord);
				lookupRecords.AddRange(dataRecords);

				_memCache.Set(LookupAjax.TestingTypes.ToString(), lookupRecords, new CacheItemPolicy() { AbsoluteExpiration = _cacheExpirationHours });
			}

			return PageRecords(lookupRecords, searchTerm, pageSize, pageNum);
		}

		[HttpGet]
		public ActionResult Themes(string searchTerm, int pageSize, int pageNum)
		{
			var blankRecord = new Select2Result() { id = null, text = "" };

			var query = _dbSubmission.tbl_lst_Themes.Where(x => x.Active).AsQueryable();

			var dataRecords = query
				.OrderBy(x => x.Name)
				.Select(x => new Select2Result { id = SqlFunctions.StringConvert((double)x.Id).Trim(), text = x.Name })
				.ToList();


			var lookupRecords = new List<Select2Result>();
			lookupRecords.Add(blankRecord);
			lookupRecords.AddRange(dataRecords);

			return PageRecords(lookupRecords, searchTerm, pageSize, pageNum);
		}

		[HttpGet]
		public ActionResult TranslationHouses(string searchTerm, int pageSize, int pageNum)
		{
			//load all records from database or cache
			var lookupRecords = _memCache[LookupAjax.TranslationHouses.ToString()] as List<Select2Result>;

			if (lookupRecords == null)
			{
				var blankRecord = new Select2Result() { id = null, text = "" };

				var dataRecords = _dbSubmission.tbl_lst_TranslationHouses
					.Where(x => x.Active)
					.OrderBy(x => x.Description)
					.Select(x => new Select2Result { id = x.Id.ToString(), text = x.Description })
					.ToList();

				lookupRecords = new List<Select2Result>();
				lookupRecords.Add(blankRecord);
				lookupRecords.AddRange(dataRecords);

				_memCache.Set(LookupAjax.TranslationHouses.ToString(), lookupRecords, new CacheItemPolicy() { AbsoluteExpiration = _cacheExpirationHours });
			}

			return PageRecords(lookupRecords, searchTerm, pageSize, pageNum, LookupsAjaxSearchType.StartsWith);
		}

		[HttpGet]
		public ActionResult UpdatedDateReason(string searchTerm, int pageSize, int pageNum, string filter)
		{
			var lookupRecords = _memCache[LookupAjax.UpdatedDateReason.ToString()] as List<Select2Result>;

			if (lookupRecords == null)
			{
				var blankRecord = new Select2Result { id = null, text = "" };
				var dataRecords = _dbSubmission.tbl_lst_UpdatedDateReason
					.Select(x => new Select2Result { id = x.Id.ToString(), text = x.Reason })
					.ToList();
				lookupRecords = new List<Select2Result> { blankRecord };
				lookupRecords.AddRange(dataRecords);

				_memCache.Set(LookupAjax.UpdatedDateReason.ToString(), lookupRecords, new CacheItemPolicy() { AbsoluteExpiration = _cacheExpirationHours });
			}
			return PageRecords(lookupRecords, searchTerm, pageSize, pageNum);
		}

		[HttpGet]
		public ActionResult Users(string searchTerm, int pageSize, int pageNum, string filter)
		{
			var blankRecord = new Select2Result() { id = null, text = "" };

			var query = _dbSubmission.trLogins
				.Include(x => x.Department)
				.Where(x => x.Status == "A");

			if (!string.IsNullOrEmpty(filter))
			{
				var projectCutOffDay = DateTime.Now.AddYears(-5);
				var filters = JsonConvert.DeserializeObject<UsersFilter>(filter);

				if (filters.Departments != null)
				{
					var departmentCodes = filters.Departments.Select(x => x.ToString()).ToList();
					query = query.Where(x => departmentCodes.Contains(x.Department.Code));
				}

				if (filters.DevReps)
				{
					var devReps = _dbSubmission.trProjects.Select(x => x.DevelopmentRepresentativeId).Distinct().ToList();
					query = query.Where(x => devReps.Contains(x.Id));
				}

				if (filters.DropDownType == UserDropDownType.Task)
				{
					var departmentCodes = new List<string>
					{
						Department.ENG.ToString(),
						Department.MTH.ToString(),
						Department.QA.ToString()
					};

					query = query.Where(x => departmentCodes.Contains(x.Department.Code));
				}

				if (filters.ExcludeUnavailable)
				{
					var today = DateTime.Now.Date;
					var endDate = today.AddYears(1);
					var unavailableUserIds = _dbSubmission.tbl_ResourceUnavailability
						.Where(x => (x.User.ManagerId != _userContext.User.Id) && (x.StartDate <= today && (x.EndDate ?? endDate) >= today))
						.Select(x => x.UserId)
						.ToList();

					query = query.Where(x => !unavailableUserIds.Contains(x.Id));
				}

				if (filters.ExcludeUserIds != null)
				{
					query = query.Where(x => !filters.ExcludeUserIds.Contains(x.Id));
				}

				if (filters.LocationIds != null)
				{
					query = query.Where(x => filters.LocationIds.Contains((int)x.LocationId));
				}
				else if (filters.LocationId != null)
				{
					query = query.Where(x => x.LocationId == filters.LocationId);
				}

				if (filters.ManagerSearch)
				{
					query = query.Where(x => x.Role.Description.Contains("Sup") || x.Role.Code == Role.MG.ToString());
				}

				if (filters.ProjectLeads)
				{
					var projectLeadIds = _dbSubmission.trProjects.Where(x => x.AddDate > projectCutOffDay).Select(x => x.ENProjectLeadId).Distinct().ToList();
					query = query.Where(x => projectLeadIds.Contains(x.Id));
				}

				if (filters.MAProjectLeads)
				{
					var mathProjectLeadIds = _dbSubmission.trProjects.Where(x => x.AddDate > projectCutOffDay).Select(x => x.MAProjectLeadId).Distinct().ToList();
					query = query.Where(x => mathProjectLeadIds.Contains(x.Id));
				}

				if (filters.ProjectManagers)
				{
					var projectManagerIds = _dbSubmission.trProjects.Where(x => x.AddDate > projectCutOffDay).Select(x => x.ENManagerId).Distinct().ToList();
					query = query.Where(x => projectManagerIds.Contains(x.Id));
				}

				if (filters.RoleOrPermission && filters.Roles != null && filters.Permission.HasValue)
				{
					var roles = filters.Roles.Select(x => x.ToString()).ToList();
					query = query.Where(x => roles.Contains(x.Role.Code) || x.UserPermissions.Any(y => y.Permission.Code == filters.Permission.ToString() && y.PermissionValue > 0));
				}
				else
				{
					if (filters.Roles != null)
					{
						var roles = filters.Roles.Select(x => x.ToString()).ToList();
						query = query.Where(x => roles.Contains(x.Role.Code));
					}

					if (filters.Permission.HasValue)
					{
						query = query.Where(x => x.UserPermissions.Any(y => y.Permission.Code == filters.Permission.ToString() && y.PermissionValue > 0));
					}
				}

				if (filters.TemplateOwner)
				{
					var templateOwnerIds = _dbSubmission.trTaskTemplates.Select(x => x.UserId).Distinct().ToList();
					query = query.Where(x => templateOwnerIds.Contains(x.Id));
				}
			}

			var dataRecords = query
				.OrderBy(x => x.FName)
				.ThenBy(x => x.LName)
				.Select(x => new Select2Result() { id = SqlFunctions.StringConvert((double)x.Id).Trim(), text = x.FName + " " + x.LName + " - " + x.Department.Code })
				.ToList();

			var lookupRecords = new List<Select2Result>();
			lookupRecords.Add(blankRecord);
			lookupRecords.AddRange(dataRecords);

			return PageRecords(lookupRecords, searchTerm, pageSize, pageNum, LookupsAjaxSearchType.Contains);
		}

		[HttpGet]
		public ActionResult Vendors(string searchTerm, int pageSize, int pageNum)
		{
			//load all records from database or cache
			var lookupRecords = _memCache[LookupAjax.Vendors.ToString()] as List<Select2Result>;

			if (lookupRecords == null)
			{
				var blankRecord = new Select2Result() { id = null, text = "" };

				var dataRecords = _dbSubmission.tbl_lst_Vendors
					.Where(x => x.Active == true)
					.OrderBy(x => x.Description)
					.Select(x => new Select2Result { id = x.Id.ToString(), text = x.Description })
					.ToList();

				lookupRecords = new List<Select2Result>();
				lookupRecords.Add(blankRecord);
				lookupRecords.AddRange(dataRecords);

				_memCache.Set(LookupAjax.Vendors.ToString(), lookupRecords, new CacheItemPolicy() { AbsoluteExpiration = _cacheExpirationHours });
			}
			return PageRecords(lookupRecords, searchTerm, pageSize, pageNum);
		}


		[HttpGet]
		public ActionResult WithdrawReason(string searchTerm, int pageSize, int pageNum)
		{
			//load all records from database or cache
			var lookupRecords = _memCache[LookupAjax.WithdrawReason.ToString()] as List<Select2Result>;

			if (lookupRecords == null)
			{
				var blankRecord = new Select2Result() { id = null, text = "" };

				var dataRecords = (from x in _dbSubmission.tbl_lst_WDRJReason
								   orderby x.Reason
								   select new Select2Result() { id = SqlFunctions.StringConvert((double)x.Id).Trim(), text = x.Reason })
								   .ToList();

				lookupRecords = new List<Select2Result>();
				lookupRecords.Add(blankRecord);
				lookupRecords.AddRange(dataRecords);

				_memCache.Set(LookupAjax.WithdrawReason.ToString(), lookupRecords, new CacheItemPolicy() { AbsoluteExpiration = _cacheExpirationHours });
			}
			return PageRecords(lookupRecords, searchTerm, pageSize, pageNum, LookupsAjaxSearchType.Contains);
		}

		[HttpGet]
		public ActionResult WorkPerformed(string searchTerm, int pageSize, int pageNum)
		{
			//load all records from database or cache
			var lookupRecords = _memCache[LookupAjax.WorkPerformed.ToString()] as List<Select2Result>;

			if (lookupRecords == null)
			{
				var blankRecord = new Select2Result() { id = null, text = "" };

				var dataRecords = (from x in _dbSubmission.tbl_lst_WorkPerformed
								   where x.Active == true
								   orderby x.Code
								   select new Select2Result() { id = SqlFunctions.StringConvert((double)x.Id).Trim(), text = x.Code })
								   .ToList();

				lookupRecords = new List<Select2Result>();
				lookupRecords.Add(blankRecord);
				lookupRecords.AddRange(dataRecords);

				_memCache.Set(LookupAjax.WorkPerformed.ToString(), lookupRecords, new CacheItemPolicy() { AbsoluteExpiration = _cacheExpirationHours });
			}

			return PageRecords(lookupRecords, searchTerm, pageSize, pageNum);
		}
		[HttpGet]
		public ActionResult AssociatedSoftwaresTemplate(string searchTerm, int pageSize, int pageNum, string filter)
		{

			var blankRecord = new Select2Result() { id = null, text = "" };
			var query = _dbSubmission.AssociatedSoftwareTemplates.AsQueryable();

			if (!string.IsNullOrEmpty(filter))
			{
				var filters = JsonConvert.DeserializeObject<ComponentTemplateTypeFilter>(filter);
				if (!string.IsNullOrEmpty(filters.manufacturersShortId))
				{
					query = query.Where(x => x.ManufacturerCode == filters.manufacturersShortId);
				}

				if (filters.JurisdictionIds.Any())
				{
					var sJurIds = filters.JurisdictionIds.Select(jid => jid.ToJurisdictionIdString()).ToList();
					query = query.Where(x => x.Jurisdictions.Select(j => j.JurisdictionId).Intersect(sJurIds).Any());
				}
				if (filters.PlatformIds.Any())
				{
					query = query.Where(x => filters.PlatformIds.Contains(x.PlatformId));
				}
			}

			var dataRecords = query
				.OrderBy(x => x.Name)
				.Where(x => !x.DeactivateDate.HasValue)
				.Select(x => new Select2Result()
				{
					id = x.Id.ToString(),
					text = x.Name
				})
				.ToList();

			var lookupRecords = new List<Select2Result>();
			lookupRecords.Add(blankRecord);
			lookupRecords.AddRange(dataRecords);

			return PageRecords(lookupRecords, searchTerm, pageSize, pageNum);
		}
		//[HttpGet]
		//public ActionResult SearchOCUITBundle(string searchTerm, int pageSize, int pageNum, string filter)
		//{

		//	var blankRecord = new Select2Result() { id = null, text = "" };

		//	var query = _dbSubmission.OCUITs
		//		.Where(OCUIT => OCUIT.Lifecycle == "Active")
		//		.Include(OCUIT => OCUIT.Project)
		//		.AsQueryable();

		//	if (!string.IsNullOrEmpty(filter))
		//	{
		//		var filters = JsonConvert.DeserializeObject<OCUITTypeFilter>(filter);
		//		if (!string.IsNullOrEmpty(filters.ManufacturersShortId))
		//		{
		//			query = query.Where(x => x.Project.FileNumber.Contains(filters.ManufacturersShortId));
		//		}
		//		if (!string.IsNullOrEmpty(filters.Type))
		//		{
		//			query = query.Where(x => x.Type.ToUpper() == filters.Type.ToUpper());
		//		}
		//		if (filters.ExcludeProjectId != 0)
		//		{
		//			query = query.Where(x => x.ProjectId != filters.ExcludeProjectId);
		//		}
		//	}


		//	var dataRecords = query
		//		.OrderBy(x => x.Project.FileNumber)
		//		.Where(x => !x.DeactivateDate.HasValue)
		//		.Select(x => new Select2Result()
		//		{
		//			id = x.ProjectId.ToString(),
		//			text = x.Project.ProjectName,
		//		})
		//		.ToList();


		//	var lookupRecords = new List<Select2Result>();
		//	lookupRecords.Add(blankRecord);
		//	lookupRecords.AddRange(dataRecords);

		//	return PageRecords(lookupRecords, searchTerm, pageSize, pageNum);

		//}
		private JsonResult PageRecords(List<Select2Result> lookupRecords, string searchTerm, int pageSize, int pageNum, LookupsAjaxSearchType searchType = LookupsAjaxSearchType.StartsWith)
		{
			//page the records
			int firstResult = pageSize * (pageNum - 1);

			var query = lookupRecords.AsQueryable();
			if (searchType != LookupsAjaxSearchType.None)
			{
				if (searchType == LookupsAjaxSearchType.StartsWith)
				{
					query = query.Where(x => x.text.StartsWith(searchTerm, StringComparison.OrdinalIgnoreCase));
				}
				else
				{
					query = query.Where(x => x.text.ToLower().Contains(searchTerm.ToLower()));
				}
			}

			var pageRecords = query
				.Skip(firstResult)
				.Take(pageSize)
				.ToList();

			//return select2 formatted result
			return new JsonResult()
			{
				Data = new Select2PagedResult() { Total = lookupRecords.Count(), Results = pageRecords },
				JsonRequestBehavior = JsonRequestBehavior.AllowGet
			};
		}

		// Clear Cache
		public ActionResult Clear(string cacheItem)
		{
			if (string.IsNullOrEmpty(cacheItem))
			{
				foreach (var item in Enum.GetValues(typeof(LookupAjax)))
				{
					_memCache.Remove(item.ToString());
				}
			}
			else
			{
				_memCache.Remove(cacheItem);
			}

			Response.Write("Success! - Cache has been Cleared");

			return new EmptyResult();
		}
	}
}