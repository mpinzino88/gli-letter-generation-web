﻿using AutoMapper;
using EF.Submission;
using Newtonsoft.Json;
using Submissions.Common;
using Submissions.Common.JQGrid;
using Submissions.Web.Areas.Reports.Models;
using Submissions.Web.Models.Grids.Reports;
using Submissions.Web.Models.Query;
using Submissions.Web.Models.Widgets;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;
using System.Web.SessionState;
using Z.EntityFramework.Plus;

namespace Submissions.Web.Controllers
{
	[SessionState(SessionStateBehavior.ReadOnly)]
	public class WidgetController : Controller
	{
		private readonly IUserContext _userContext;
		private readonly ISubmissionContext _dbSubmission;
		private readonly IQueryService _queryService;
		private readonly IWidgetService _widgetService;

		public WidgetController(IUserContext userContext, ISubmissionContext dbSubmssion, IQueryService queryService, IWidgetService widgetService)
		{
			_userContext = userContext;
			_dbSubmission = dbSubmssion;
			_queryService = queryService;
			_widgetService = widgetService;
		}

		#region General Widget
		public ActionResult Add(WidgetType widgetType)
		{
			var pageSource = GetPageSource();

			switch (widgetType)
			{
				case WidgetType.Chart:
					var vmChart = new EditChartViewModel { Mode = Mode.Add, Widget = new ChartWidget { Type = widgetType } };
					return PartialView("_EditChart", vmChart);
				case WidgetType.List:
					var vmList = new EditListViewModel { Mode = Mode.Add, Widget = new ListWidget { Type = widgetType } };
					vmList.Widget.ShowOnDashboard = true;
					vmList.Widget.DashboardId = _userContext.DefaultDashboardId;
					vmList.PageSource = pageSource;
					return PartialView("_EditList", vmList);
				default:
					return new EmptyResult();
			}
		}

		public ActionResult Edit(int id)
		{
			var dbWidget = _dbSubmission.tbl_Widgets.Find(id);
			var widgetType = (WidgetType)Enum.Parse(typeof(WidgetType), dbWidget.Type);
			var pageSource = GetPageSource();

			switch (widgetType)
			{
				case WidgetType.Chart:
					var vmChart = new EditChartViewModel { Widget = Mapper.Map<ChartWidget>(dbWidget) };
					vmChart.PageSource = pageSource;
					return PartialView("_EditChart", vmChart);
				case WidgetType.List:
					var vmList = new EditListViewModel { Widget = Mapper.Map<ListWidget>(dbWidget) };
					vmList.PageSource = pageSource;
					vmList.Widget.Columns = LoadColumnChooser((WidgetDataSource)Enum.Parse(typeof(WidgetDataSource), dbWidget.DataSource), dbWidget.SavedColumns);
					RenderFiltersView(vmList);

					return PartialView("_EditList", vmList);
				default:
					return new EmptyResult();
			}
		}

		[HttpPost]
		public ActionResult Delete(EditListViewModel model)
		{
			_dbSubmission.tbl_Widgets.Where(x => x.Id == model.Widget.Id).Delete();

			if (model.PageSource == "Dashboard")
			{
				return RedirectToAction("Index", "Dashboard");
			}

			return RedirectToAction("Gallery");
		}

		public ActionResult ChooseType()
		{
			var vm = new ChooseTypeViewModel();
			return PartialView("_ChooseType", vm);
		}

		public JsonResult SavePositions(List<Position> positions)
		{
			foreach (var position in positions)
			{
				var widget = new tbl_Widget { Id = position.WidgetId, Position = JsonConvert.SerializeObject(position) };

				_dbSubmission.tbl_Widgets.Attach(widget);
				_dbSubmission.Entry(widget).Property(x => x.Position).IsModified = true;
			}

			_dbSubmission.SaveChanges();

			return Json("ok");
		}

		public JsonResult LoadColumns(int? widgetId, WidgetDataSource dataSource)
		{
			var savedColumns = "";
			if (widgetId != null)
			{
				savedColumns = _dbSubmission.tbl_Widgets.Where(x => x.Id == widgetId).Select(x => x.SavedColumns).Single();
			}

			var result = LoadColumnChooser(dataSource, savedColumns);
			return Json(result, JsonRequestBehavior.AllowGet);
		}

		public ActionResult LoadSearchCriteria(int? widgetId, WidgetDataSource dataSource)
		{
			switch (dataSource)
			{
				case WidgetDataSource.JurisdictionalData:
					{
						var vm = new JurisdictionalDataSearchPartialViewModel();
						if (widgetId != null)
						{
							var filters = _dbSubmission.tbl_Widgets.Where(x => x.Id == widgetId).Select(x => x.Filters).Single();
							vm.Filters = JsonConvert.DeserializeObject<JurisdictionalDataSearch>(filters);
						}
						return PartialView(vm.PartialViewName, vm);
					}
				case WidgetDataSource.Project:
					{
						var vm = new ProjectSearchPartialViewModel();
						if (widgetId != null)
						{
							var filters = _dbSubmission.tbl_Widgets.Where(x => x.Id == widgetId).Select(x => x.Filters).Single();
							vm.Filters = JsonConvert.DeserializeObject<ProjectSearch>(filters);
						}
						return PartialView(vm.PartialViewName, vm);
					}
				case WidgetDataSource.Submission:
					{
						var vm = new SubmissionSearchPartialViewModel();
						if (widgetId != null)
						{
							var filters = _dbSubmission.tbl_Widgets.Where(x => x.Id == widgetId).Select(x => x.Filters).Single();
							vm.Filters = JsonConvert.DeserializeObject<SubmissionSearch>(filters);
						}
						return PartialView(vm.PartialViewName, vm);
					}
				case WidgetDataSource.Task:
					{
						var vm = new TaskSearchPartialViewModel();
						if (widgetId != null)
						{
							var filters = _dbSubmission.tbl_Widgets.Where(x => x.Id == widgetId).Select(x => x.Filters).Single();
							vm.Filters = JsonConvert.DeserializeObject<TaskSearch>(filters);
						}
						return PartialView(vm.PartialViewName, vm);
					}
			}

			return new EmptyResult();
		}

		public ActionResult Gallery()
		{
			var dbWidgets = _dbSubmission.tbl_Widgets
				.Include(x => x.Dashboard)
				.Where(x => x.UserId == _userContext.User.Id)
				.OrderBy(x => x.Dashboard.Name)
				.ThenBy(x => x.Title)
				.ToList();

			var widgets = Mapper.Map<List<Widget>>(dbWidgets);

			var vm = new GalleryViewModel
			{
				Widgets = widgets
			};

			return View(vm);
		}
		#endregion

		#region Lists
		public ActionResult Display(int id)
		{
			var dbWidget = _dbSubmission.tbl_Widgets.Find(id);
			var widget = Mapper.Map<ListWidget>(dbWidget);

			switch (widget.DataSource)
			{
				case WidgetDataSource.JurisdictionalData:
					{
						var grid = new JurisdictionalDataReportGrid();
						grid.InitializeWidgetGrid(id, widget.SavedColumns);

						var vm = new JurisdictionalDataDisplayViewModel
						{
							WidgetId = id,
							Title = widget.Title,
							JurisdictionalDataGrid = grid,
							Filters = JsonConvert.DeserializeObject<JurisdictionalDataSearch>(widget.Filters)
						};
						return View("~/Areas/Reports/Views/JurisdictionalData/Display.cshtml", vm);
					}
				case WidgetDataSource.Project:
					{
						var grid = new ProjectsReportGrid();
						grid.InitializeWidgetGrid(id, widget.SavedColumns);

						var vm = new ProjectsDisplayViewModel
						{
							WidgetId = id,
							Title = widget.Title,
							ProjectGrid = grid,
							Filters = JsonConvert.DeserializeObject<ProjectSearch>(widget.Filters)
						};
						return View("~/Areas/Reports/Views/Projects/Display.cshtml", vm);
					}
				case WidgetDataSource.Submission:
					{
						var grid = new SubmissionsReportGrid();
						grid.InitializeWidgetGrid(id, widget.SavedColumns);

						var vm = new SubmissionsDisplayViewModel
						{
							WidgetId = id,
							Title = widget.Title,
							SubmissionGrid = grid,
							Filters = JsonConvert.DeserializeObject<SubmissionSearch>(widget.Filters)
						};
						return View("~/Areas/Reports/Views/Submissions/Display.cshtml", vm);
					}
				case WidgetDataSource.Task:
					{
						var grid = new TasksReportGrid();
						grid.InitializeWidgetGrid(id, widget.SavedColumns);

						var vm = new TasksDisplayViewModel
						{
							WidgetId = id,
							Title = widget.Title,
							TasksGrid = grid,
							Filters = JsonConvert.DeserializeObject<TaskSearch>(widget.Filters)
						};
						return View("~/Areas/Reports/Views/Tasks/Display.cshtml", vm);
					}
			}

			return new EmptyResult();
		}

		[HttpPost]
		public void Export(int widgetId, ExportType exportType, string filtersToolbar)
		{
			var dbWidget = _dbSubmission.tbl_Widgets.Find(widgetId);
			var widget = Mapper.Map<ListWidget>(dbWidget);

			switch (widget.DataSource)
			{
				case WidgetDataSource.JurisdictionalData:
					{
						var query = _queryService.JurisdictionalDataSearch(JsonConvert.DeserializeObject<JurisdictionalDataSearch>(widget.Filters));
						var export = new Export
						{
							ExportType = exportType,
							FiltersToolbar = filtersToolbar,
							SavedColumns = widget.SavedColumns,
							Title = widget.Title,
							WidgetId = widgetId
						};

						var grid = new JurisdictionalDataReportGrid();
						grid.Export(export, query);
						break;
					}
				case WidgetDataSource.Project:
					{
						var query = _queryService.ProjectSearch(JsonConvert.DeserializeObject<ProjectSearch>(widget.Filters));
						var export = new Export
						{
							ExportType = exportType,
							FiltersToolbar = filtersToolbar,
							SavedColumns = widget.SavedColumns,
							Title = widget.Title,
							WidgetId = widgetId
						};

						var grid = new ProjectsReportGrid();
						grid.Export(export, query);
						break;
					}
				case WidgetDataSource.Submission:
					{
						var query = _queryService.SubmissionSearch(JsonConvert.DeserializeObject<SubmissionSearch>(widget.Filters));
						var export = new Export
						{
							ExportType = exportType,
							FiltersToolbar = filtersToolbar,
							SavedColumns = widget.SavedColumns,
							Title = widget.Title,
							WidgetId = widgetId
						};

						var grid = new SubmissionsReportGrid();
						grid.Export(export, query);
						break;
					}
				case WidgetDataSource.Task:
					{
						var query = _queryService.TaskSearch(JsonConvert.DeserializeObject<TaskSearch>(widget.Filters));
						var export = new Export
						{
							ExportType = exportType,
							FiltersToolbar = filtersToolbar,
							SavedColumns = widget.SavedColumns,
							Title = widget.Title,
							WidgetId = widgetId
						};

						var grid = new TasksReportGrid();
						grid.Export(export, query);
						break;
					}
			}
		}

		[HttpPost]
		public JsonResult EditList(EditListViewModel model)
		{
			var widget = model.Widget;
			var dbWidget = (model.Mode == Mode.Add) ? new tbl_Widget() : _dbSubmission.tbl_Widgets.Find(widget.Id);
			dbWidget.Type = widget.Type.ToString();
			dbWidget.DataSource = widget.DataSource.ToString();
			dbWidget.Title = widget.Title;
			dbWidget.Color = widget.Color;
			dbWidget.ShowOnDashboard = widget.ShowOnDashboard;
			dbWidget.ToolbarSearch = widget.ToolbarSearch;
			dbWidget.SavedColumns = model.Widget.SavedColumns;
			dbWidget.UserId = _userContext.User.Id;
			dbWidget.DashboardId = model.Widget.DashboardId;
			dbWidget.Filters = model.Widget.Filters;

			if (model.Mode == Mode.Add)
			{
				_widgetService.AddWidget(dbWidget);
			}
			else
			{
				_dbSubmission.SaveChanges();
			}

			return Json("ok");
		}

		[OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
		public ActionResult LoadList(int id)
		{
			var dbWidget = _dbSubmission.tbl_Widgets.Find(id);
			var widget = Mapper.Map<ListWidget>(dbWidget);

			var vm = new LoadListViewModel();

			switch (widget.DataSource)
			{
				case WidgetDataSource.JurisdictionalData:
					{
						var grid = new JurisdictionalDataReportGrid();
						grid.InitializeWidgetGrid(id, widget.SavedColumns);

						if (widget.ToolbarSearch)
						{
							grid.Grid.ToolBarSettings.ShowSearchToolBar = true;
						}

						vm.Grid = grid.Grid;
						break;
					}
				case WidgetDataSource.Project:
					{
						var grid = new ProjectsReportGrid();
						grid.InitializeWidgetGrid(id, widget.SavedColumns);

						if (widget.ToolbarSearch)
						{
							grid.Grid.ToolBarSettings.ShowSearchToolBar = true;
						}

						vm.Grid = grid.Grid;
						break;
					}
				case WidgetDataSource.Submission:
					{
						var grid = new SubmissionsReportGrid();
						grid.InitializeWidgetGrid(id, widget.SavedColumns);

						if (widget.ToolbarSearch)
						{
							grid.Grid.ToolBarSettings.ShowSearchToolBar = true;
						}

						vm.Grid = grid.Grid;
						break;
					}
				case WidgetDataSource.Task:
					{
						var grid = new TasksReportGrid();
						grid.InitializeWidgetGrid(id, widget.SavedColumns);

						if (widget.ToolbarSearch)
						{
							grid.Grid.ToolBarSettings.ShowSearchToolBar = true;
						}

						vm.Grid = grid.Grid;
						break;
					}
			}

			return PartialView("_LoadList", vm);
		}
		#endregion

		#region Helper Methods
		private string GetPageSource()
		{
			return Request.UrlReferrer.AbsolutePath.Contains("Gallery") ? "Gallery" : "Dashboard";
		}

		private IList<ColumnChooserColumn> GetAllColumns(WidgetDataSource source)
		{
			switch (source)
			{
				case WidgetDataSource.JurisdictionalData:
					{
						var grid = new JurisdictionalDataReportGrid();
						var allColumns = grid.AllColumns.Where(x => x.Visible).Select(x => new ColumnChooserColumn { DatabaseName = x.DataField, DisplayName = x.HeaderText }).ToList();
						return allColumns;
					}
				case WidgetDataSource.Project:
					{
						var grid = new ProjectsReportGrid();
						var allColumns = grid.AllColumns.Where(x => x.Visible).Select(x => new ColumnChooserColumn { DatabaseName = x.DataField, DisplayName = x.HeaderText }).ToList();
						return allColumns;
					}
				case WidgetDataSource.Submission:
					{
						var grid = new SubmissionsReportGrid();
						var allColumns = grid.AllColumns.Where(x => x.Visible).Select(x => new ColumnChooserColumn { DatabaseName = x.DataField, DisplayName = x.HeaderText }).ToList();
						return allColumns;
					}
				case WidgetDataSource.Task:
					{
						var grid = new TasksReportGrid();
						var allColumns = grid.AllColumns.Where(x => x.Visible).Select(x => new ColumnChooserColumn { DatabaseName = x.DataField, DisplayName = x.HeaderText }).ToList();
						return allColumns;
					}
				default:
					return new List<ColumnChooserColumn>();
			}
		}

		private ColumnChooser LoadColumnChooser(WidgetDataSource dataSource, string savedColumns)
		{
			var availableColumns = GetAllColumns(dataSource);
			var selectedColumns = new List<ColumnChooserColumn>();

			if (!string.IsNullOrEmpty(savedColumns))
			{
				var savedColumnsList = JsonConvert.DeserializeObject<List<string>>(savedColumns);

				selectedColumns = (from x in savedColumnsList
								   join y in availableColumns on x equals y.DatabaseName
								   select y).ToList();

				availableColumns = availableColumns.Where(x => !savedColumnsList.Contains(x.DatabaseName)).OrderBy(x => x.DisplayName).ToList();
			}

			return new ColumnChooser { AvailableColumns = availableColumns, SelectedColumns = selectedColumns };
		}

		private void RenderFiltersView(EditListViewModel vmList)
		{
			switch (vmList.Widget.DataSource)
			{
				case WidgetDataSource.JurisdictionalData:
					{
						var searchViewModel = new JurisdictionalDataSearchPartialViewModel { Filters = JsonConvert.DeserializeObject<JurisdictionalDataSearch>(vmList.Widget.Filters) };
						vmList.Widget.FiltersView = this.RenderRazorViewToString(searchViewModel.PartialViewName, searchViewModel);
						break;
					}
				case WidgetDataSource.Project:
					{
						var searchViewModel = new ProjectSearchPartialViewModel { Filters = JsonConvert.DeserializeObject<ProjectSearch>(vmList.Widget.Filters) };
						vmList.Widget.FiltersView = this.RenderRazorViewToString(searchViewModel.PartialViewName, searchViewModel);
						break;
					}
				case WidgetDataSource.Submission:
					{
						var searchViewModel = new SubmissionSearchPartialViewModel { Filters = JsonConvert.DeserializeObject<SubmissionSearch>(vmList.Widget.Filters) };
						vmList.Widget.FiltersView = this.RenderRazorViewToString(searchViewModel.PartialViewName, searchViewModel);
						break;
					}
				case WidgetDataSource.Task:
					{
						var searchViewModel = new TaskSearchPartialViewModel { Filters = JsonConvert.DeserializeObject<TaskSearch>(vmList.Widget.Filters) };
						vmList.Widget.FiltersView = this.RenderRazorViewToString(searchViewModel.PartialViewName, searchViewModel);
						break;
					}
			}
		}
		#endregion
	}
}