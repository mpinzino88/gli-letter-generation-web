﻿extern alias ZEFCore;
using GLI.EFCore.Submission;
using Submissions.Web.Models;
using System.Linq;
using System.Web.Mvc;
using ZEFCore::Z.EntityFramework.Plus;

namespace Submissions.Web.Controllers
{
	public class ColumnTemplatesController : Controller
	{
		private readonly SubmissionContext _dbSubmission;
		private readonly IUserContext _userContext;

		public ColumnTemplatesController(SubmissionContext dbSubmission, IUserContext userContext)
		{
			_dbSubmission = dbSubmission;
			_userContext = userContext;
		}

		public ActionResult Load(ColumnChooserGrid gridName)
		{
			var vm = new ColumnChooserTemplateViewModel();
			vm.ColumnTemplates = _dbSubmission.tbl_ColumnChooserGrids
				.Where(x => x.UserId == _userContext.User.Id && x.Grid == gridName.ToString())
				.Select(x => new ColumnChooserTemplate { Id = x.Id, TemplateName = x.Name, Current = x.Current })
				.OrderBy(x => x.TemplateName)
				.ToList();

			return PartialView("_ColumnChooserTemplates", vm);
		}

		[HttpPost]
		public ActionResult Apply(int id, ColumnChooserGrid gridName)
		{
			_dbSubmission.tbl_ColumnChooserGrids
				.Where(x => x.UserId == _userContext.User.Id && x.Grid == gridName.ToString())
				.Update(x => new tbl_ColumnChooserGrid { Current = false });

			var colTemplate = new tbl_ColumnChooserGrid { Id = id, Current = true };
			_dbSubmission.tbl_ColumnChooserGrids.Attach(colTemplate);
			_dbSubmission.Entry(colTemplate).Property(x => x.Current).IsModified = true;
			_dbSubmission.SaveChanges();

			return Json("ok");
		}

		[HttpPost]
		public ActionResult Delete(int id)
		{
			_dbSubmission.tbl_ColumnChooserGrids.Where(x => x.Id == id).Delete();

			return Json("ok");
		}
	}
}