﻿function pageCount(items, localId) {
	if (isNull(items) || items.length === 0) return 0;

	let pages = items[items.length - 1].paging;
	if (isNull(pages) || pages.length === 0) return 0;

	let lastPage = pages.find(pag => pag.localId === localId);

	if (isNull(lastPage) || lastPage.length === 0)
		return 0;
	else
		return lastPage.page;
}

function getItemsOnPage(items, page, localId, jurId) {
	return items.filter(item => onPage(item, page, localId, jurId));
}
function onPage(obj, page, localId, jurId) {
	return obj.paging.some(pag => pag.localId === localId && pag.page === page && pag.jurisdictionId === jurId);
}

function getPaging(obj, localId, jurId) {
	if (isNull(obj) || isNull(obj.paging)) {
		return null;
	}
	else {
		return obj.paging.find(pag => pag.localId === localId && pag.jurisdictionId === jurId);
	}
}

function addInitialPaging(payload) {
	if (isNull(payload.obj.paging)) {
		payload.obj.paging = [{ page: payload.initialPage, height: 0, localId: payload.localId, jurisdictionId: payload.jurId, isLandscape: false }];
	} else if (!payload.obj.paging.some(page => page.localId === payload.localId && page.jurisdictionId === payload.jurId)) {
		payload.obj.paging.push({ page: payload.initialPage, height: 0, localId: payload.localId, jurisdictionId: payload.jurId });
	}
}

function setPages(payload) {
	//setPages: (state, getters) => (items, localId, jurId) => {
	let pageIter = 1;
	for (let itemIter = 0; itemIter < payload.items.length; itemIter++) {
		//obj, localId, initialPage, jurId
		addInitialPaging({ obj: payload.items[itemIter], localId: payload.localId, initialPage: 1, jurId: payload.jurId });

		//if (itemIter % 10 === 2)
		//	pageIter += 10;
	}
}

//Splits a source memo by cutting the number of target source Items based on the value in splitCount
//if there's only one source item, it checks splittable fields and cuts them in half
//------This currently only supports splitting a memo once. We'll have to add support for splitting multiple times
function splitSourcedMemo(memo, sources, splitCount) {

	if (isNull(memo) || isNull(memo.sourceItemTargets) || memo.sourceItemTargets.length === 0) {
		return [];
	}
	//easy case - multiple source items in this memo. Split and return.
	if (memo.sourceItemTargets.length > 1) {
		//console.log('multiple SITs');
		let ret = [];

		let count = memo.sourceItemTargets.length;
		let perCloneCount = Math.floor(count / splitCount);//129 clauses split 50 times = perclonecount of 3
		let splitIterations = splitCount - 1;
		if (perCloneCount === 0) {
			perCloneCount = 1;
			splitIterations--;
		}
		//console.log('splitSourcedMemo count - ' + count + ' perCloneCount - ' + perCloneCount);
		for (let splitIter = 0; splitIter < splitIterations; splitIter++) {
			ret.push(cloneAndMoveSourceItemTargets(memo, perCloneCount));
		}
		//if (memo.sourceItemTargets.length < 1 || ret.some(cl => cl.sourceItemTargets.length === 0)) {
		//	console.error("Omg no sourceItemTargets");
		//}
		//console.log('sourceItemTargets length - ' + memo.sourceItemTargets.length);
		ret.reverse();
		//console.log(ret);
		return ret;
	}
	else {//only got one target source item that we have to split
		//console.log('single SIT');
		let sourceItemTarget = memo.sourceItemTargets[0];
		let sourceItem = sources.find(src => getSourceItemId(memo.sourceId, src) === sourceItemTarget.id);
		//The one single source item for this memo is too big for the page.
		//Now we find the longest value and split that 
		//first, we have to find the longest field to split on that first.
		let maxField = getMaxFieldSourceMemo(memo, sourceItem, sourceItemTarget);
		let clone = cloneMemo(memo);
		let cloneSourceItemTarget = clone.sourceItemTargets[0];
		//If the max field is on a control for a Source, we have to check the SourceItemTargets to get the field and split it there
		if (maxField.sourceControl) {
			let targetControl = sourceItemTarget.letterContentControls.find(control => control.label === maxField.name);
			let cloneTargetControl = cloneSourceItemTarget.letterContentControls.find(control => control.label === maxField.name);
			splitSourceControlsContent(targetControl, cloneTargetControl);
		}
		//if the max field is a basic control for the memo, split it here
		else if (maxField.control) {
			let targetControl = memo.letterContentControls.find(control => control.label === maxField.name);
			let cloneTargetControl = clone.letterContentControls.find(control => control.label === maxField.name);
			splitSourceControlsContent(targetControl, cloneTargetControl);
		}
		//otherwise, the longest field is a value on the Source and it needs to be split. 
		//For now, we're assuming values on Sources to be plain text. This will have to be updated to detect HTML when the need arises
		else {

			if (isNotNull(sourceItemTarget.splitSourceItems) && sourceItemTarget.splitSourceItems.some(ssi => ssi.property === maxField.name)) {
				let splitSource = sourceItemTarget.splitSourceItems.some(ssi => ssi.property === maxField.name);
				let cloneSplitSource = cloneSourceItemTarget.splitSourceItems.some(ssi => ssi.property === maxField.name);
				//this memo goes from some midpoint all the way to the end
				if (splitSource.end === 0) {
					let splitIndex = getSplitIndex(sourceItem[maxField.name].substring(ssi.start));
					//the new end of the first memo is the halfway point
					splitSource.end = splitIndex + splitSource.start;
					//the new start of the second memo is the halfway point
					cloneSplitSource.start = splitIndex + splitSource.start;
				}
				//this memo goes from the start to the midpoint
				else if (splitSource.start === 0) {
					let splitIndex = getSplitIndex(sourceItem[maxField.name].substring(0, ssi.end));
					//now it ends halfway to the old midpoint
					splitSource.end = splitIndex;
					//and the new memo starts at the old midpoint
					cloneSplitSource.start = splitIndex;
				}
				//this memo is just a middle chunk
				else {
					let splitIndex = getSplitIndex(sourceItem[maxField.name].substring(ssi.start, ssi.end));
					//now it ends halfway to the old midpoint
					splitSource.end = splitIndex + splitSource.start;
					//and the new memo starts at the old midpoint
					cloneSplitSource.start = splitIndex + splitSource.start;
				}
			}
			else {
				let splitIndex = getSplitIndex(sourceItem[maxField.name]);
				sourceItemTarget.splitSourceItems = [{ property: maxField.name, start: 0, end: splitIndex }];
				cloneSourceItemTarget.splitSourceItems = [{ property: maxField.name, start: splitIndex, end: 0 }];
			}

		}
		let ret = [];
		ret.push(clone);
		return ret;
	}
}
function cloneAndMoveSourceItemTargets(memo, count) {
	let clone = cloneMemo(memo);
	clone.sourceItemTargets = [];
	//I've no idea why slice doesn't work here
	for (let i = 0; i < count; i++) {
		clone.sourceItemTargets.push(memo.sourceItemTargets.pop());
	}
	clone.sourceItemTargets.reverse();
	return clone;
}

function splitSourceControlsContent(targetControl, cloneTargetControl) {
	//console.log('splitSourceControlsContent');
	//console.log(targetControl);
	let htmlSplitSuccess = false;
	if (targetControl.type === 'HTML') {
		//console.log('type === html');
		let htmlHalves = splitHTMLContentInHalf(targetControl.content);
		//console.log(htmlHalves);
		if (htmlHalves[1].length > 0) {
			htmlSplitSuccess = true;
			targetControl.content = htmlHalves[0];
			cloneTargetControl.content = htmlHalves[1];
			//console.log('htmlHalves1');
			//console.log(htmlHalves);
		}
	}
	if (!htmlSplitSuccess) {
		let splitIndex = getSplitIndex(targetControl.content);
		targetControl.content = targetControl.content.substring(0, splitIndex);
		cloneTargetControl.content = cloneTargetControl.content.substring(splitIndex);
	}
}
//This won't work so well if the memo mixes HTML with other controls. Let's just assume there's only HTML controls here
//TODO: throw everything out and start from scratch
function splitHTMLMemo(memo) {
	if (isNull(memo) || isNull(memo.letterContentControls) || memo.letterContentControls.length === 0) {
		return {};
	}

	//Check to see if there's more than one HTMl control. If there is, split the memo into 2, leave the html alone.
	//If there's just one HTML control, use html-split to split it in half across 2 memos.
	let splittableControls = memo.letterContentControls.filter(control => control.splittable && control.type === "HTML");
	let count = splittableControls.length;
	if (count === 0) {
		return {};
	}
	else if (count === 1) {
		let clone = cloneMemo(memo);
		let cloneHtmlControl = clone.letterContentControls.find(control => control.splittable && control.type === "HTML");
		cloneHtmlControl.localId = generateGuid();

		let origHtmlControl = splittableControls[0];

		if (origHtmlControl.content.length === 0) {
			//console.log('why is this content empty?');
			return undefined;
		}

		let htmlHalves = splitHTMLContentInHalf(origHtmlControl.content);
		origHtmlControl.content = htmlHalves[0];
		cloneHtmlControl.content = htmlHalves[1];

		//We should really be checking for an all-whitespace memo. That would require loading the html into a DOM node and using JQuery $.trim(node.val())
		//Which we'll save for another day https://stackoverflow.com/questions/1172206/how-to-check-if-a-text-is-all-white-space-characters-in-client-side
		if (cloneHtmlControl.content.length === 0) {
			return undefined;
		}
		else if (origHtmlControl.content.length === 0 && cloneHtmlControl.content.length > 0) {
			origHtmlControl.content = cloneHtmlControl.content;
			return undefined;
		}
		else {
			return clone;
		}


	}
	else {
		let clone = cloneMemo(memo);

		clone.letterContentControls = [];


		let htmlControlIndices = [];
		splittableControls.forEach(control => {
			htmlControlIndices.push(memo.letterContentControls.findIndex(item => item.localId === control.localId));
		});
		let mid = Math.ceil(count / 2);
		for (let i = htmlControlIndices.length - 1; i >= mid; i--) {
			clone.letterContentControls.push(memo.letterContentControls.splice(htmlControlIndices[i], 1));
		}
		clone.letterContentControls.reverse();
		clone.letterContentControls.forEach(control => control.localId = generateGuid());
		return clone;
	}


}
function splitHTMLContentInHalf(content) {
	//console.log('splitHTMLContentInHalf 1');
	//console.log(content);
	let htmlParts = splitHTMLStringDiv(content);
	//console.log('splitHTMLContentInHalf 2');
	if (htmlParts.length === 1) {
		//console.log('splitHTMLContentInHalf 3');
		htmlParts = splitHTMLStringP(content);
	}
	if (htmlParts.length === 1) {
		//console.log('splitHTMLContentInHalf 4');
		htmlParts = splitHTMLStringSpan(content);
	}
	//console.log('splitHTMLContentInHalf 5');
	htmlParts = htmlParts.filter(part => part.length > 0);
	//console.log('splitHTMLPartsInHalf');
	let htmlHalves = splitHTMLPartsInHalf(htmlParts, false);
	//console.log('htmlHalves');
	//console.log(htmlHalves);
	return [htmlHalves[0].join(''), htmlHalves[1].join('')];
}
function splitHTMLStringDiv(htmlString) {
	return splitHtml(htmlString, 'div');
}
function splitHTMLStringP(htmlString) {
	return splitHtml(htmlString, 'p');
}
function splitHTMLStringSpan(htmlString) {
	return splitHtml(htmlString, 'span');
}


function splitMemoContentDirectly(contentToSplit) {
	if (contentToSplit.slice(0, 5) !== '<div>')
		contentToSplit = `<div>${contentToSplit}</div>`
	let splitPoint = findSplitPoint(contentToSplit);
	let contentToSplitAddSplitPoint = [contentToSplit.slice(0, splitPoint), `<template></template>`, contentToSplit.slice(splitPoint)].join('');
	let fragments = splitHtml(contentToSplitAddSplitPoint, 'template');

	// Cleanup and remove divs from splitting.
	fragments[0] = fragments[0].slice(5);
	fragments[0] = fragments[0].slice(0, -6);
	if (fragments[2].length > 0) {
		fragments[2] = fragments[2].slice(5);
		fragments[2] = fragments[2].slice(0, -6);
	}

	return fragments;
}

function splitHTMLPartsInHalf(htmlParts, debug) {
	if (htmlParts.length <= 1) {
		return [htmlParts, []];
	}
	else if (htmlParts.length === 2) {
		return [[htmlParts[0]], [htmlParts[1]]];
	}

	let count = lengthOfItemsInArray(htmlParts);
	let halfCount = Math.ceil(count / 2);
	let firstHalf = [];
	firstHalf.push(htmlParts.shift());

	let firstItem = htmlParts[0];
	if (debug) {
		//console.log('count - ' + count + ' halfCount - ' + halfCount + ' first item - ' + firstItem.length);
	}
	while (halfCount > (lengthOfItemsInArray(firstHalf) + firstItem.length)) {
		firstHalf.push(htmlParts.shift());
		firstItem = htmlParts[0];
		if (debug) {
			//console.log('length of items - ' + lengthOfItemsInArray(firstHalf) + ' length of First item - ' + firstItem.length);
		}
	}

	return [firstHalf, htmlParts];
}
function lengthOfItemsInArray(someArray) {
	let count = 0;
	someArray.forEach(item => count += item.length);
	return count;
}
//This won't work so well if the memo mixes tables with other controls. 
//TODO: throw everything out and start from scratch
function splitTable(memo) {
	let tables = memo.letterContentControls.filter(control => control.type === 'TABLE' && control.tableRows.length > 0);
	let tableCount = tables.length;
	if (tableCount === 0) {
		return {};
	}
	else if (tableCount === 1) {
		let clone = cloneMemo(memo);

		let cloneTable = clone.letterContentControls.find(control => control.type === 'TABLE' && control.tableRows.length > 0);
		cloneTable.tableRows = [];
		let count = tables[0].tableRows.length;
		let mid = Math.ceil(count / 2);

		//I've no idea why slice doesn't work here
		//cloneTable.tableRows = tables[0].tableRows.slice(mid, count - mid);
		for (let i = 0; i < mid; i++)
			cloneTable.tableRows.push(tables[0].tableRows.pop());
		cloneTable.tableRows.reverse();

		return clone;
	}
	else {
		let clone = cloneMemo(memo);

		//here, I was trying to move the whole table to the clone. That had a bunch of issues with the memo referencing a table that no 
		//longer existed. Let's try move the table rows.
		//clone.letterContentControls = [];
		//let tablesToMove = tables.skip(Math.ceil(tableCount / 2)).length;
		//for (let tableIter = memo.letterContentControls.length - 1; tableIter >= 0; tableIter--) {
		//	if (memo.letterContentControls[tableIter].type === 'TABLE') {
		//		clone.letterContentControls.push(memo.letterContentControls.splice(tableIter, 1));
		//	}
		//	if (clone.letterContentControls.length === tablesToMove) {
		//		break;
		//	}
		//}

		let cloneTables = clone.letterContentControls.filter(control => control.type === 'TABLE' && control.tableRows.length > 0);
		//clear out all rows on the clone
		cloneTables.forEach(table => table.tableRows = []);
		//move all the rows from the second half of tables into the clone
		tables.skip(Math.ceil(tableCount / 2)).forEach(table => {
			let cloneTable = cloneTables.find(cTab => cTab.id === table.id);
			cloneTable.tableRows = [...table.tableRows];
			table.tableRows = [];
		});
		return clone;
	}
}

//As of this writing, we're only worrying about splitting/recombining Tables, HTML,  and Sourced items
function recombineMemos(firstMemo, secondMemo) {
	//console.log('recombining memos');
	let hasTable = firstMemo.letterContentControls.some(control => control.type === 'TABLE');
	let hasHTML = firstMemo.letterContentControls.some(control => control.splittable && control.type === 'HTML');
	let hasSource = isNotNull(firstMemo.sourceId) && firstMemo.sourceId.length > 0;

	if (hasSource) {
		while (secondMemo.sourceItemTargets.length > 0) {
			let secondSIT = secondMemo.sourceItemTargets.shift();
			let firstSIT = firstMemo.sourceItemTargets.find(sit => sit.id === secondSIT.id);

			if (isNull(firstSIT)) {
				firstMemo.sourceItemTargets.push(secondSIT);
			}
			else if (isNotNull(secondSIT.letterContentControls)) {
				//console.log('we have a source split by a control');
				while (secondSIT.letterContentControls.length > 0) {
					let secondSITControl = secondSIT.letterContentControls.shift();
					let firstSITControl = firstSIT.letterContentControls.find(control => control.id === secondSITControl.id);
					//console.log(secondSITControl);
					//console.log(firstSITControl);
					if (isNull(firstSITControl)) {
						firstSIT.letterContentControls.push(secondSITControl);
					}
					else {
						firstSITControl.content += secondSITControl.content;
					}
				}
			}

		}
		return true;
	}
	else if (hasTable) {
		let nextTables = secondMemo.letterContentControls.filter(control => control.type === 'TABLE');
		nextTables.forEach(nextTable => {
			let firstTable = firstMemo.letterContentControls.find(control => control.id === nextTable.id);
			nextTable.tableRows.forEach(nextRow => firstTable.tableRows.push(nextRow));
		});
		return true;
	}
	else if (hasHTML) {
		let nextHtmls = secondMemo.letterContentControls.filter(control => control.splittable && control.type === 'HTML');
		nextHtmls.forEach(nextHtml => {
			let firstHtml = firstMemo.letterContentControls.find(control => control.id === nextHtml.id);
			firstHtml.content += nextHtml.content;
		});
		return true;
	}
	return false;
}
///Recombines the splitsourceitems - last of the firstMemo, first of hte secondMemo
///Assumes a source Item can only be split by one property at a time...
function CombineSplitSourceItem(firstMemo, secondMemo) {
	let firstHalf = firstMemo.sourceItemTargets[firstMemo.sourceItemTargets.length - 1];
	let secondHalf = secondMemo.sourceItemTargets[0];
	let firstHalfSSI = firstHalf.splitSourceItems[0];
	let secondHalfSSI = secondHalf.splitSourceItems[0];
	firstHalfSSI.end = secondHalfSSI.end;
	secondHalf.sourceItemTargets.shift();

}

///returns true if the last soureItemTarget on firstMemo is the same as the first sourceItemTarget on the second memo
///and they are split
function isSplitSourceItem(firstMemo, secondMemo) {
	if (isNull(firstMemo.sourceItemTargets) || isNull(secondMemo.sourceItemTargets) ||
		firstMemo.sourceItemTargets.length === 0 || secondMemo.sourceItemTargets.length === 0) {
		return false;
	}
	let lastSIT = firstMemo.sourceItemTargets[firstMemo.sourceItemTargets.length - 1];
	let firstSIT = secondMemo.sourceItemTargets[0];
	if (firstSIT.id !== lastSIT.id || isNull(firstSIT.splitSourceItems) || firstSIT.splitSourceItems.length === 0 ||
		isNull(lastSIT.splitSourceItems) || lastSIT.splitSourceItems.length === 0 ||
		firstSIT.splitSourceItems[0].property != lastSIT.splitSourceItems[0].property) {
		return false;
	}
	else {
		return true;
	}
}

function isNotNull(obj) {
	return typeof obj !== 'undefined' && obj !== null;
}

function isNull(obj) {
	return typeof obj === 'undefined' || obj === null;
}

function findSplitPoint(contentToSplit) {
	let splitPoint = Math.ceil(contentToSplit.length / 2);
	let firstHalf = contentToSplit.slice(0, splitPoint);
	let secondHalf = contentToSplit.slice(splitPoint);
	let nextWhiteSpace = secondHalf.indexOf(' ');
	// lucky guess
	if (nextWhiteSpace == 0) {
		return splitPoint;
	}
	// Times like these we need to look back and reflect.
	else if (nextWhiteSpace === -1) {
		nextWhiteSpace = firstHalf.lastIndexOf(' ');
		let amountToGoBack = splitPoint - nextWhiteSpace;
		splitPoint -= amountToGoBack;
	}
	// Always forward
	else {
		splitPoint += nextWhiteSpace;
	}
	return splitPoint;
}


function getRenderedHeight(content) {
	let tempPage = document.createElement("div");
	tempPage.style = "position: absolute; left: -1000px; top: -1000px; width: 793px;";
	tempPage.innerHTML = content;
	document.body.appendChild(tempPage);
	let offSetheight = tempPage.offsetHeight;
	document.body.removeChild(tempPage);
	return offSetheight;
}
