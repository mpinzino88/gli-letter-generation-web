﻿function generateGuid() { // Stoled : https://stackoverflow.com/questions/105034/create-guid-uuid-in-javascript
	var d = new Date().getTime();
	if (typeof performance !== 'undefined' && performance !== null && typeof performance.now === 'function') {
		d += performance.now(); //use high-precision timer if available
	}
	return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
		var r = (d + Math.random() * 16) % 16 | 0;
		d = Math.floor(d / 16);
		return (c === 'x' ? r : (r & 0x3 | 0x8)).toString(16);
	});
}

function filterByJurisdiction(sources, jurId) {
	return sources.filter(doc => doc.jurisdictionId === jurId || jurId === 0 || isNull(doc.jurisdictionId));
}

function getClauseCounts(clauses, jurisdictionId) {
	if (isNull(clauses) || clauses.length === 0)
		return {
			pass: 0,
			na: 0,
			naStar: 0,
			fail: 0
		};
	else
		return {
			pass: clauses.filter(clause => clause.answer === "PASS" && clause.jurisdictionId == jurisdictionId).length,
			na: clauses.filter(clause => clause.answer === "NA" && clause.jurisdictionId == jurisdictionId).length,
			naStar: clauses.filter(clause => clause.answer === "NA*" && clause.jurisdictionId == jurisdictionId).length,
			fail: clauses.filter(clause => clause.answer === "FAIL" && clause.jurisdictionId == jurisdictionId).length
		};
}

function getClauseReportCounts(clauseReports, jurId) {
	if (jurId === 0) {
		return {
			pass: clauseReports.map(cr => cr.passCount).reduce((a, b) => a + b, 0),
			na: clauseReports.map(cr => cr.naCount).reduce((a, b) => a + b, 0),
			naStar: clauseReports.map(cr => cr.naStarCount).reduce((a, b) => a + b, 0),
			fail: clauseReports.map(cr => cr.failCount).reduce((a, b) => a + b, 0)
		};
	}
	else {
		let cr = clauseReports.find(cr => cr.jurisdictionId === jurId);
		if (isNotNull(cr)) {
			return {
				pass: cr.passCount,
				na: cr.naCount,
				naStar: cr.naStarCount,
				fail: cr.failCount
			};
		}
		else {
			return {
				pass: 0,
				na: 0,
				naStar: 0,
				fail: 0
			};
		}
	}
}
function buildComputed(control) {
	if (control.type !== "TABLE") {
		return {
			[control.label]: control.content
		};
	}
}
//returns a deep clone of the memo with new localId values for the memos, paging objcts, and controls
function cloneMemo(memo) {
	let clone = JSON.parse(JSON.stringify(memo));
	clone.localId = generateGuid();
	if (isNotNull(clone.letterContentControls)) {
		clone.letterContentControls.forEach(control => control.localId = generateGuid());
	}
	if (isNotNull(clone.paging)) {
		clone.paging.forEach(pag => pag.localId = clone.localId);
	}
	if (isNotNull(clone.sourceItemTargets)) {
		clone.sourceItemTargets.selectMany(sit => sit.letterContentControls).forEach(control => control.localId = generateGuid());
	}
	return clone;
}

function isNotNull(obj) {
	return typeof obj !== 'undefined' && obj !== null;
}

function isNull(obj) {
	return typeof obj === 'undefined' || obj === null;
}

//Different sources have different properties that hold their unique Id value. The SourceItem object only has 1 Id value. This method returns the 
//correct Id value from the correct property based on the type of Source
function getSourceItemId(sourceId, sourceItem) {
	switch (sourceId) {
		case "ClauseReports":
			return sourceItem.clauseID;
		case "Paytables":
			return sourceItem.id; //I'm not super certain this is actually unique, just putting in a placeholder for now
		case "Components":
			return sourceItem.id;
		case "SpecializedJurisdictionClauseReports":
			return sourceItem.name;
		case "OCUITs":
			return sourceItem.id;
		default:
			return '';
	}
}

function AddSourceItemTarget(memo, src, sourceProperty) {
	let repeatableExpression = memo.repeatableContentControls.length > 0 ? memo.repeatableContentControls.find(control => control.repeatableContent).sourceFilter : null;
	if (isNull(repeatableExpression) || repeatableExpression.length === 0 || eval(repeatableExpression)) {
		let sourceItemTarget = { id: getSourceItemId(memo.sourceId, src), splitSourceItems: [], letterContentControls: [] };
		let sourceControls = memo.letterContentControls.filter(control => control.sourceProperty === sourceProperty && !control.isGameDescription && !control.isModification);

		if (sourceControls.length >= 1) {
			sourceItemTarget.letterContentControls.push(...sourceControls);
		}
		else {
			sourceItemTarget.letterContentControls = [];
		}

		let modificationOrGameDescriptionControls = memo.letterContentControls.filter(control => control.sourceProperty === sourceProperty && (control.isModification || control.isGameDescription));

		for (let i = 0; i < modificationOrGameDescriptionControls.length; i++) {
			// Clone the control.
			let initialClone = { ...modificationOrGameDescriptionControls[i] };

			// Find all sourceTargetAnswers that match this sourceID
			let sourceItemTargetAnswers = initialClone.sourceTargetAnswers.filter(x => x.sourceTargetId == sourceItemTarget.id);

			// We have no existing answers.
			if (sourceItemTargetAnswers.length === 0) {
				// When we dont have any existing answers we need our default / defacto answer
				let clone = { ...initialClone };
				clone.applicableJurisdictions = [];
				clone.localId = generateGuid();
				// The default answer should not be considered split(in this case split is in reference to jurisdiction break out.)
				clone.isSplitFromDefault = false;
				sourceItemTarget.letterContentControls.push({ ...clone });
			}
			// We have existing answers
			else {
				// for each of them clone the  control and update its content and applicable jurisdictions respectively.
				sourceItemTargetAnswers.forEach(x => {
					let clone = { ...initialClone };
					clone.localId = generateGuid();
					clone.content = x.answer;
					clone.applicableJurisdictions = x.applicableJurisdictions;
					clone.isSplitFromDefault = x.isSplitFromDefault;
					sourceItemTarget.letterContentControls.push({ ...clone });
				});
			}

		}

		memo.sourceItemTargets.push(sourceItemTarget);
	}
}

//Splits a source item based on the SplitSourceItems objects. Returns a clone of the sourceItem.
//If the source is not split, returns a regular clone of the object.
//This is how we display half of a sourceItem without modifying that sourceItem for other memos. 
function createSplitSource(srcItem, targetSourceItem, jurId) {
	let clone = JSON.parse(JSON.stringify(srcItem));

	targetSourceItem.letterContentControls
		.filter(control => control.applicableJurisdictions.length === 0 || control.applicableJurisdictions.includes(jurId))
		.forEach(control => {
			clone[control.label] = control.content;
		});
	targetSourceItem.splitSourceItems.forEach(ssi => {
		if (ssi.start === 0) {
			clone[ssi.property] = clone[ssi.property].substring(0, ssi.end);
		}
		else if (ssi.end === 0) {
			clone[ssi.property] = clone[ssi.property].substring(ssi.start);
		} else {
			clone[ssi.property] = clone[ssi.property].substring(ssi.start, ssi.end);
		}
	});
	return clone;
}
//This is used to find the biggest field on this memo/sourceItem.
//When splitting across pages, we find the property with the most content (by .length) and split that content
//This determines the name of the property with the most content, its length, whether or not it's on a control, a sourceControl, and whether or not it's HTML
function getMaxFieldSourceMemo(memo, sourceItem, sourceItemTarget) {
	let maxField = { name: '', length: 0, control: false, sourceControl: false, html: false };
	//first check the splittable fields on the object itself
	if (isNotNull(memo.splittableFields) && memo.splittableFields.length > 0) {
		memo.splittableFields.forEach(splittableField => {
			let length = getSplitLength(sourceItem, splittableField, sourceItemTarget.splitSourceItems);
			if (length > maxField.length) {

				maxField.name = splittableField;
				maxField.length = sourceItem[splittableField].length;
			}
		});
	}
	//Then check any of the controls that may be splittable
	maxField = checkMaxFieldControls(maxField, memo.letterContentControls, false);
	maxField = checkMaxFieldControls(maxField, sourceItemTarget.letterContentControls, true);
	return maxField;
}

function checkMaxFieldControls(maxField, controls, isSource) {
	if (isNull(controls) || controls.length === 0) {
		return maxField;
	}
	let splittableControls = controls.filter(control => control.splittable);
	if (splittableControls.length === 0) {
		return maxField;
	}
	splittableControls.forEach(control => {
		if (control.content.length > maxField.length) {
			maxField.name = control.label;
			maxField.length = control.content.length;
			maxField.control = true;
			maxField.sourceControl = isSource;
			if (control.type === "HTML") {
				maxField.html = true;
			}
		}
	});
	return maxField;
}
function getSplitLength(sourceItem, splittableField, splitSourceItems) {
	if (isNotNull(splitSourceItems) && splitSourceItems.length > 0 && splitSourceItems.some(ssi => ssi.property === splittableField)) {
		let splitItem = splitSourceItems.find(ssi => ssi.property === splittableField);
		if (splitItem.end === 0) {
			return sourceItem[splittableField].length - splitItem.start;
		}
		else if (splitItem.start === 0) {
			return splitItem.end;
		}
		else {
			return splitItem.end - splitItem.start;
		}
	}
	else {
		return sourceItem[splittableField].length;
	}
}
function getSplitIndex(splitValue) {
	let size = splitValue.length;
	let splitIndex = splitValue.indexOf(".", size / 2) + 1;

	if (splitIndex === 0) {
		splitIndex = splitValue.indexOf(" ", size / 2) + 1;
		if (splitIndex === 0) {
			splitIndex = size / 2;
		}
	}
	return splitIndex;
}
