﻿// Regex Patterns for Matching Submission and Dynamics Files
var submissionFileNumberPattern = new RegExp("([a-zA-Z][a-zA-Z])-([1-9]?[0-9][0-9])-([a-zA-Z0-9][a-zA-Z0-9][a-zA-Z0-9])-([0-9][0-9])-([1-9]?[0-9][0-9])-?([0-9]?[0-9][0-9])?", "i");
var dynamicsFileNumber = new RegExp("^([a-zA-Z][a-zA-Z][0-9][0-9][0-9][a-zA-Z1-9][a-zA-Z1-9][a-zA-Z1-9][0-9][0-9][0-9][0-9][0-9])$", "i");
var dynamicsTransferFileNumber = new RegExp("([a-zA-Z][a-zA-Z])([0-9][0-9][0-9])([a-zA-Z1-9][a-zA-Z1-9][a-zA-Z1-9])([0-9][0-9])([0-9][0-9][0-9])([0-9][0-9][0-9])", "i");

// -- Queue data structure for JS
// -- its just an array with wrappers for existing functions..
function Queue() {
	this.queue = [];
}

Queue.prototype.enqueue = function (item) {
	this.queue.push(item);
}

Queue.prototype.dequeue = function () {
	return this.queue.shift();
}

Queue.prototype.size = function () {
	return this.queue.length;
}

Queue.prototype.peak = function () {
	return (this.queue[0] !== null) ? this.queue[0] : null;
}

var datePickerDefaultOptions = {
	format: 'l',
	useCurrent: false,
	showClear: true
};

// Select2 defaults
$.fn.select2.defaults.set('theme', 'bootstrap');
$.fn.select2.defaults.set('width', 'resolve');
$.fn.select2.defaults.set('placeholder', '');
$.fn.select2.defaults.set('minimumInputLength', 0);
$.fn.select2.defaults.set('allowClear', true);
$.fn.select2.defaults.set('dropdownAutoWidth', true);
$.fn.select2.defaults.set('escapeMarkup', function (m) { return m; });
$.fn.modal.Constructor.prototype.enforceFocus = function () { };

// Remove json object from array by name
Array.prototype.removeValue = function (name, value) {
	var array = $.map(this, function (v, i) {
		return v[name] === value ? null : v;
	});
	this.length = 0;
	this.push.apply(this, array);
}

// Return unique list of items in a flat array
Array.prototype.unique = function () {
	return this.filter(function (value, index, self) {
		return self.indexOf(value) === index;
	});
}

// Manually clear jquery unobtrusive validation
$.fn.clearValidation = function () { var v = $(this).validate(); $('[name]', this).each(function () { v.successList.push(this); }); v.showErrors(); v.resetForm(); v.reset(); };

// serialize form to object
$.fn.serializeObject = function (removePrefix) {
	var o = {};
	var a = this.serializeArray();
	var prefix = removePrefix || '';
	$.each(a, function () {
		var name = this.name.replace(prefix, '');

		if (o[name] !== undefined) {
			if (!o[name].push) {
				o[name] = [o[name]];
			}
			o[name].push(this.value || '');
		} else {
			o[name] = this.value || '';
		}
	});
	return o;
};

$.ajaxSetup({ cache: false });

$(function () {
	loadDocumentReady();

	// Form Reset
	$('form').on('reset', function () {
		var self = $(this);
		setTimeout(function () {
			self.find('select').find('option').prop('selected', function () {
				return this.defaultSelected;
			});
			self.find('select').trigger('change');
			self.clearValidation();
		});
	});

	// Back-to-Top
	$(window).scroll(function () {
		if ($(this).scrollTop() > 250) {
			$('#BackToTop').fadeIn(300);
		} else {
			$('#BackToTop').fadeOut(300);
		}
	});

	$('#BackToTop').click(function (e) {
		e.preventDefault();
		$('html, body').animate({ scrollTop: 0 }, 300);
		return false;
	});

	$("#GLIGlobeImg").click(function () {
		window.location.replace(jsPath);
	});

	initializeSingleInputSearch();

	$('.singleinputsearch-dropdown-menu li a').on('click', function (e) {
		e.preventDefault();

		var searchType = $(this).data('searchtype');
		$(this).parents('.singleinputsearch-dropdown').find('.btn').html($(this).text() + ' <span class="caret"></span>');
		$(this).parents('.singleinputsearch-dropdown').find('.btn').val($(this).data('searchtype'));

		initializeSingleInputSearch();

		var searchMagGlass = $('.singleinputsearch-container .fainput .fa-search');
		var searchInput = $('#SingleInputSearch');
		if (searchType === 'Projects') {
			searchInput.removeClass('singleinputsearch-input-submissions').addClass('singleinputsearch-input-projects');
			searchMagGlass.removeClass('singleinputsearch-magglass-submissions').addClass('singleinputsearch-magglass-projects');
		}
		else {
			searchInput.removeClass('singleinputsearch-input-projects').addClass('singleinputsearch-input-submissions');
			searchMagGlass.removeClass('singleinputsearch-magglass-projects').addClass('singleinputsearch-magglass-submissions');
		}

		searchInput.val('');
		searchInput.focus();
	});


	$.post(jsPath + 'notifications/monitor/getqueuecount/')
		.done(function (queueCount) {
			if (queueCount) {
				$('#messageCountBar').text(queueCount);
				$('#messageCountMenu').text(queueCount);
			}
		});

	$('i[data-toggle="tooltip"]').tooltip();
});

function Loading() {
	$('body').addClass('transp');
	$('#loadingSymbol').show();
}

function DoneLoading() {
	$('body').removeClass('transp');
	$('#loadingSymbol').hide();
}

function loadDocumentReady() {
	// Select2 clears validation
	$('select').on('change', function (e) {
		if ($(this).closest('form').length > 0) {
			$(this).valid();
		}
	});

	// Select2 Standard Dropdowns
	$('select').not('table.ui-jqgrid-htable thead tr.ui-search-toolbar select, [data-lookupstandard], [data-lookupajax], [data-lookupmultipleajax], [data-tags], .vue-select, .vanilla-select, div.editorToolbarWindow select, select.k-fontName, select.k-fontSize, select.k-formatting').each(function () {
		initializeSelect(this, $(this).data('lookuptextfield'));
	});

	// Select2 Ajax Single Input Dropdown
	$('select[data-lookupajax]').not('[data-bind], .vue-select').each(function () {
		initializeLookupAjax(this);
	});

	// Select2 Ajax Multi-Select Dropdown
	$('select[data-lookupmultipleajax]').not('[data-bind], .vue-select').each(function () {
		initializeLookupMultipleAjax(this);
	});

	// Select2 Tags
	$('select[data-tags]').not('[data-bind], .vue-select').each(function () {
		initializeTags(this);
	});

	// Select2 Grid Dropdown
	bindGridElements();

	// Datetime Picker
	$('input.datetimepicker').datetimepicker({
		format: 'l LT',
		useCurrent: false,
		sideBySide: true,
		showClear: true,
		widgetParent: 'body'
	}).attr('autocomplete', 'off');

	// Date Picker
	$('input.datepicker').datetimepicker({
		format: 'l',
		useCurrent: false,
		showClear: true
	}).attr('autocomplete', 'off');

	// Remove spaces on blur
	$('body').on('blur', 'input, textarea', function () {
		$(this).val($.trim($(this).val()));
	});

	// Form Reset
	$('form').on('reset', function (e) {
		setTimeout(function () {
			$(this).find('input:hidden').val('');
			$(this).find('select').find('option').prop('selected', function () {
				return this.defaultSelected;
			});
			$(this).find('select').trigger('change');
		});
	});
}

function GoToProjectDisplay() {
	$('#showList').val("true");
	var sisForm = $('#SingleInputSearchForm');
	sisForm.submit();
}

function initializeSingleInputSearch() {
	var sisForm = $('#SingleInputSearchForm');
	var sis = $('#SingleInputSearch');
	sis.off('typeahead:select');
	sis.typeahead('destroy');

	// Single Input Search Remote Queries
	var sisFileNumberUrl;
	var sisGameNameUrl;
	var sisFileNumberHeader;

	var searchProject = $('#SingleInputSearchBtn').val();
	if (searchProject === 'Projects') {
		sisForm.attr('action', jsPath + 'reports/projects/singleinputsearch');
		sisFileNumberUrl = jsPath + 'singleinputsearch/projectfilenumber?term=%QUERY';
		sisGameNameUrl = jsPath + 'singleinputsearch/projectgamename?term=%QUERY';
		sisIdNumberUrl = jsPath + 'singleinputsearch/projectidnumber?term=%QUERY';
		sisFileNumberHeader = "Project&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<font size=1><a target='_new' style='cursor:pointer' onclick='GoToProjectDisplay()'>[GoTo Detailed list]</a></font>";
	}
	else {
		sisForm.attr('action', jsPath + 'reports/submissions/singleinputsearch');
		sisFileNumberUrl = jsPath + 'singleinputsearch/submissionfilenumber?term=%QUERY';
		sisGameNameUrl = jsPath + 'singleinputsearch/submissiongamename?term=%QUERY';
		sisIdNumberUrl = jsPath + 'singleinputsearch/submissionidnumber?term=%QUERY';
		sisFileNumberHeader = "Submission";
	}

	var taFileNumbers = new Bloodhound({
		datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
		queryTokenizer: Bloodhound.tokenizers.whitespace,
		remote: {
			url: sisFileNumberUrl,
			wildcard: '%QUERY'
		}
	});

	var taGameNames = new Bloodhound({
		datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
		queryTokenizer: Bloodhound.tokenizers.whitespace,
		remote: {
			url: sisGameNameUrl,
			wildcard: '%QUERY'
		}
	});

	var taIdNumbers = new Bloodhound({
		datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
		queryTokenizer: Bloodhound.tokenizers.whitespace,
		remote: {
			url: sisIdNumberUrl,
			wildcard: '%QUERY'
		}
	});

	sis.typeahead({
		minLength: 2,
		highlight: true,
		hint: false
	},
		{
			name: 'taFileNumbers',
			display: 'name',
			source: taFileNumbers,
			templates: {
				header: '<h5 class="singleInputSearchHeader">' + sisFileNumberHeader + '</h3>'
			}
		},
		{
			name: 'taGameNames',
			display: 'name',
			source: taGameNames,
			templates: {
				header: '<h5 class="singleInputSearchHeader">Game Name</h3>'
			}
		},
		{
			name: 'taIdNumbers',
			display: 'name',
			source: taIdNumbers,
			templates: {
				header: '<h5 class="singleInputSearchHeader">ID Number</h3>'
			}
		})
		.on('typeahead:select', function (e, datum) {
			$('#showList').val("false");
			sisForm.submit();
		});
}

function bindGridElements() {
	// Select2 Standard Drop Downs found on Grid
	$('table.ui-jqgrid-htable thead tr.ui-search-toolbar select').each(function () {
		$(this).select2().addClass('select2Grid');
	});
}

function initializeSelect(el, lookuptextfield) {
	var lookupTextField = $('#' + lookuptextfield);

	$(el).select2();

	if (lookupTextField.length) {
		$(el).on('change', function () {
			lookupTextField.val($('option:selected', el).text()).trigger('change');
		});
	}

	lockScrollbar(el);
}

function initializeLookupAjax(el) {
	var element = $(el);
	var pageSize = 50;
	var lookupAjaxUrl = jsPath + 'lookupsajax/' + element.data('lookupajax');
	var lookupTextField = $('#' + element.data('lookuptextfield'));
	var templateName = element.data('templatename') ? window[element.data('templatename')] : undefined;

	element.select2({
		ajax: {
			url: lookupAjaxUrl,
			dataType: 'json',
			delay: 250,
			data: function (params) {
				params.page = params.page || 1;
				params.term = params.term || '';

				return {
					pageSize: pageSize,
					pageNum: params.page,
					searchTerm: params.term,
					filter: JSON.stringify(element.data('filter'))
				};
			},
			processResults: function (data, params) {
				params.page = params.page || 1;

				return {
					results: data.Results,
					pagination: {
						more: (params.page * pageSize) < data.Total
					}
				};
			}
		},
		templateResult: templateName,
		templateSelection: templateName
	});

	element.on('change', function () {
		lookupTextField.val($('option:selected', el).text()).trigger('change');
	});

	lockScrollbar(el);
}


function initializeLookupTaggedAjax(el) {
	var element = $(el);
	var pageSize = 50;
	var lookupAjaxUrl = jsPath + 'lookupsajax/' + element.data('lookupajax');
	var lookupTextField = $('#' + element.data('lookuptextfield'));
	var templateName = element.data('templatename') ? window[element.data('templatename')] : undefined;

	element.select2({
		tags: true,
		ajax: {
			url: lookupAjaxUrl,
			dataType: 'json',
			delay: 250,
			data: function (params) {
				params.page = params.page || 1;
				params.term = params.term || '';

				return {
					pageSize: pageSize,
					pageNum: params.page,
					searchTerm: params.term,
					filter: JSON.stringify(element.data('filter'))
				};
			},
			processResults: function (data, params) {
				params.page = params.page || 1;

				return {
					results: data.Results,
					pagination: {
						more: (params.page * pageSize) < data.Total
					}
				};
			}
		},
		templateResult: templateName,
		templateSelection: templateName
	});

	element.on('change', function () {
		lookupTextField.val($('option:selected', el).text()).trigger('change');
	});

	lockScrollbar(el);
};

function initializeLookupMultipleAjax(el) {
	var element = $(el);
	var pageSize = 50;
	var lookupAjaxUrl = jsPath + 'lookupsajax/' + element.data('lookupmultipleajax');
	var templateName = element.data('templatename') ? window[element.data('templatename')] : undefined;

	element
		.select2({
			ajax: {
				url: lookupAjaxUrl,
				dataType: 'json',
				delay: 250,
				data: function (params) {
					params.page = params.page || 1;
					params.term = params.term || '';

					return {
						pageSize: pageSize,
						pageNum: params.page,
						searchTerm: params.term,
						filter: JSON.stringify(element.data('filter'))
					};
				},
				processResults: function (data, params) {
					params.page = params.page || 1;

					return {
						results: data.Results,
						pagination: {
							more: (params.page * pageSize) < data.Total
						}
					};
				}
			},
			closeOnSelect: false,
			templateResult: templateName,
			templateSelection: templateName
		})
		.on('select2:select', function () {
			var searchField = $(this).next().find('.select2-search__field');
			if (searchField !== undefined) {
				searchField.val('');
			}
		});

	lockScrollbar(el);

	element.removeClass('hidden');
}

function initializeTags(el) {
	$(el).select2({
		tags: true,
		allowClear: false
	});
}

function Em(str) {
	return (!str || str.length === 0);
}

// used to lock scrollbar outside dropdown
// code prevents page jitter in chrome when hiding scrollbar
function lockScrollbar(dropDown) {
	$(dropDown).unbind('select2:open').on('select2:open', function () {
		var html = $('html');
		var body = $('body');
		var initWidth = body.outerWidth();
		var initHeight = body.outerHeight();

		var scrollPosition = [
			self.pageXOffset || document.documentElement.scrollLeft || document.body.scrollLeft,
			self.pageYOffset || document.documentElement.scrollTop || document.body.scrollTop
		];
		html.data('scroll-position', scrollPosition);
		html.css('overflow', 'hidden');
		window.scrollTo(scrollPosition[0], scrollPosition[1]);

		var marginR = body.outerWidth() - initWidth;
		var marginB = body.outerHeight() - initHeight;
		body.css({ 'margin-right': marginR, 'margin-bottom': marginB });
		$('#MainNavBar').css('margin-right', marginR);
	});

	$(dropDown).unbind('select2:close').on('select2:close', function () {
		var html = $('html');
		var body = $('body');
		html.css('overflow', 'visible');

		var scrollPosition = html.data('scroll-position') ? html.data('scroll-position') : [0, 0];
		window.scrollTo(scrollPosition[0], scrollPosition[1]);

		body.css({ 'margin-right': 0, 'margin-bottom': 0 });
		$('#MainNavBar').css('margin-right', 0);
	});
}

function openDialog(modalDialog, url) {
	$.get(url, {}, function (data) {
		// prevent modal from opening if returning a javascript redirect
		if (data.indexOf('window') === -1) {
			modalDialog.html(data);
			modalDialog.modal('show');
		}
	}).fail(function (jqXHR, textStatus, errorThrown) {
		alert('Error: ' + jqXHR.responseText);
	});
}

// -- File Number Object used to split a valid File number i.e. MO-22-WMS-14-44 or MO-22-WMS-14-44-37
// -- Into the appropriate parts.
function FileNumber(term) {
	var splitString = term.split("-");

	if (splitString.length < 4) {
		return null;
	}

	this.ProjectType = splitString[0];
	this.PrimaryJurisdiction = splitString[1].length < 3 ? '0' + splitString[1] : splitString[1];
	this.ManufacturerShortCode = splitString[2];
	this.Year = splitString[3];
	this.SequenceNumber = splitString[4].length < 3 ? '0' + splitString[4] : splitString[4];
	this.TransferExtension = splitString[5] !== undefined && splitString[5].length < 3 ? '0' + splitString[5] : splitString[5];
}

function formatJurisdictions(data) {
	if (Em(data.id)) {
		return data;
	}

	return data.text + "<span class='dd-ch'>" + data.id + "</span>";
}

function formatManufacturers(data) {
	if (Em(data.id)) {
		return data;
	}

	return data.text + "<span class='dd-ch'>" + data.id + "</span>";
}

// Form Filters
function AddFormFilter(editActionName, formObject) {
	bootbox.prompt("Please enter a Filter Name", function (filterName) {
		if (!Em(filterName)) {
			var url = jsPath + 'formfilters/' + editActionName;
			var formData = formObject.serializeArray();
			formData.push({ name: "FilterName", value: filterName });

			$.post(url, formData)
				.done(function (filterId) {
					$('#FormFiltersId').val(filterId);
					$('#FormFiltersName').val(filterName);
					$('#FormFiltersNameDisplay').text(filterName);
					$('#CurrentFormFilter, #SaveFormFilter').removeClass('hidden');
				});
		}
	});
}

function RemoveFormFilter() {
	var url = jsPath + 'formfilters/remove';
	var modalDialog = $('#FormFiltersModal');
	var id = $('#FormFiltersId').val();

	$.post(url, { id: id })
		.done(function () {
			window.location = modalDialog.data('returnurl');
		});
}

function SaveFormFilter(editActionName, formObject) {
	var url = jsPath + 'formfilters/' + editActionName;
	var id = $('#FormFiltersId').val();
	var formData = formObject.serializeArray();
	formData.push({ name: "Id", value: id });

	$.post(url, formData)
		.done(function () {
			bootbox.alert('Filter has been saved');
		});
}

function ViewFormFilter() {
	var url = jsPath + 'formfilters/load';
	var modalDialog = $('#FormFiltersModal');
	var section = modalDialog.data('section');

	$.get(url, { section: section }, function (data) {
		modalDialog.html(data);
		modalDialog.modal('show');
	});
}

function ViewColumnTemplates() {
	var url = jsPath + 'columntemplates/load';
	var modalDialog = $('#ColChooserTemplatesModal');
	var section = modalDialog.data('section');

	$.get(url, { gridName: section }, function (data) {
		modalDialog.html(data);
		modalDialog.modal('show');
	});
}

function convertToDynamicsFileNumberFormat(fileNumber) {
	// Given a filenumber string
	let term = fileNumber;
	let error;

	if (typeof (term) !== 'string') {
		error = new Error("Paramater provided to convertToDynamicsFileNumberFormat() was not a string.");
		error.name = '@ convertToDynamicsFileNumberFormat';
		error.description = "Provide a valid string to convertToDynamicsFileNumberFormat().";
		console.log(error);
		return null;
	}

	// Determine if it is currently in valid format
	if (dynamicsFileNumber.test(term) || dynamicsTransferFileNumber.test(term)) {
		//  This term is already a valid dynamics format filenumber stirng return.
		return term.toUpperCase();
	} else {
		// Strip all characters except for Alphanumeric and dashes
		if (term) {
			term.replace(/[^-A-Za-z0-9]/gi, '');
			if (term.indexOf('-') > -1 && submissionFileNumberPattern.test(term)) {
				// Format filenumber string to dynamics filenumber string format
				let FileNumberObj = new FileNumber(term);
				term = FileNumberObj.ProjectType +
					FileNumberObj.PrimaryJurisdiction +
					FileNumberObj.ManufacturerShortCode +
					FileNumberObj.Year +
					FileNumberObj.SequenceNumber;
				if (FileNumberObj.TransferExtension !== undefined) {
					term = term + FileNumberObj.TransferExtension;
				}
				// Return valid dynamics formated filenumber string
				return term.toUpperCase();
			} else {
				//  console log error and return null 
				error = new Error("Invalid filenumber format.");
				error.name = '@ convertToDynamicsFileNumberFormat';
				error.description = "The string provided was not a valid Dynamics or submissions filenumber format.";
				console.log(error);
				return null;
			}
		} else {
			//  console log error and return null 
			error = new Error("No filenumber string provided");
			error.name = '@ convertToDynamicsFileNumberFormat';
			error.description = "The string provided was undefined, null, blank, false, or 0. Please provide a string.";
			console.log(error);
			return null;
		}
	}
}

var LetterBook = (function () {
	const US_SPECIALIZED = "USSpecialized";
	const EU_SPECIALIZED = "EUSpecialized";
	const US_NON_SPECIALIZED = "USNonSpecialized";

	function IsValidBillingInfo(billingInfo, callback) {
		let msg = null;
		if ((billingInfo.BillingSheet() === US_SPECIALIZED && billingInfo.ReportType() === '') && !billingInfo.IsFixedPrice()) {
			msg = "A US specialized billing sheet must either provide a report type or have a fixed price.";
		} else if (billingInfo.BillingSheet() === EU_SPECIALIZED && billingInfo.EUReportType() === '') {
			msg = "An EU specialized billing sheet must provide a report type.";
		} else if (billingInfo.BillingSheet() === US_NON_SPECIALIZED
			&& billingInfo.ItemsBeingBilledCount().toString() === "0") {
			msg = "A Non-specialized billing sheet can only be used when there is at least one item being billed.";
		}

		if (msg) {
			callback(msg);
		}
		return !msg;
	}

	return {
		IsValidBillingInfo: IsValidBillingInfo
	}
})();