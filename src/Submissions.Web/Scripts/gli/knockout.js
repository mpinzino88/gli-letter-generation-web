﻿ko.validation.init({
	grouping: {
		deep: true,
		live: true,
		observable: true
	},
	insertMessages: true,
	decorateInputElement: true,
	registerExtenders: true,
	messagesOnModified: true,
	parseInputAttributes: true,
	messageTemplate: null,
	errorMessageClass: "field-validation-error",
	errorElementClass: "input-validation-error"
});

ko.observableArray.fn.totalVisible = function () {
	var items = this(), count = 0;

	if (items == null || typeof items.length === 'undefined') {
		return 0;
	}

	for (var i = 0, l = items.length; i < l; i++) {
		if (items[i]._destroy !== true) {
			count++;
		}
	}

	return count;
};

ko.bindingHandlers.modal = {
	init: function (element, valueAccessor) {
		$(element).modal({
			show: false
		});

		var value = valueAccessor();
		if (ko.isObservable(value)) {
			$(element).on('hide.bs.modal', function () {
				value(false);
			});
		}
	},
	update: function (element, valueAccessor) {
		var value = valueAccessor();
		if (ko.utils.unwrapObservable(value)) {
			$(element).modal('show');
		} else {
			$(element).modal('hide');
		}
	}
};

ko.bindingHandlers.textDate = {
	init: function (element, valueAccessor, allBindings) {

	},
	update: function (element, valueAccessor, allBindings, viewModel, bindingContext) {
		var date = $(element);
		//when the view model is updated, update the widget
		if (date) {
			var koDate = ko.utils.unwrapObservable(valueAccessor());

			// Check if already js Date obj or not
			if (typeof (koDate) !== 'object') {

				// Check if Date is JSON Date format i.e. /Date(93989393)/
				if (koDate.indexOf('Date') > -1) {
					koDate = new Date(parseFloat(koDate.replace(/[^0-9]/g, '')));
					koDate = koDate.toDateString().substring(4);
				}
				// Assume ISO 8601 format i.e. 2012-09-27
				else {
					koDate = new Date(koDate);
				}

				date.text(koDate);
			}
		}
	}
};

ko.bindingHandlers.datePicker = {
	init: function (element, valueAccessor, allBindings) {
		//initialize datepicker with default options from gli.js
		$(element).datetimepicker(datePickerDefaultOptions);
		$(element).removeClass('hidden');
		$(element).attr('autocomplete', 'off');

		//when a user changes the date, update the view model
		ko.utils.registerEventHandler(element, 'dp.change', function (event) {
			var value = valueAccessor();
			if (ko.isObservable(value)) {
				if (event.date === false) {
					value(null);
				} else if (event.date != null && !(event.date instanceof Date)) {
					value(event.date.toDate());
				} else {
					value(event.date);
				}
			}
		});

		ko.utils.domNodeDisposal.addDisposeCallback(element, function () {
			var picker = $(element).data('DateTimePicker');
			if (picker) {
				picker.destroy();
			}
		});
	},
	update: function (element, valueAccessor, allBindings, viewModel, bindingContext) {
		var picker = $(element).data('DateTimePicker');
		//when the view model is updated, update the widget
		if (picker) {
			var koDate = ko.utils.unwrapObservable(valueAccessor());

			// Check if already js Date obj or not
			if (typeof (koDate) !== 'object') {

				// Check if Date is JSON Date format i.e. /Date(93989393)/
				if (koDate.indexOf('Date') > -1) {
					koDate = moment(koDate).format("MM/DD/YYYY");
					koDate = new Date(koDate);
				}
				// Assume ISO 8601 format i.e. 2012-09-27
				else {
					koDate = new Date(koDate);
				}
			}

			picker.date(koDate);
		}
	}
};

ko.bindingHandlers.datePickerWithFormat = {
	init: function (element, valueAccessor, allBindings) {
		//initialize datepicker with default options from gli.js
		$(element).datetimepicker(datePickerDefaultOptions);
		$(element).removeClass('hidden');
		$(element).attr('autocomplete', 'off');

		//when a user changes the date, update the view model
		ko.utils.registerEventHandler(element, 'dp.change', function (event) {
			var value = valueAccessor();
			if (element.value !== null && element.value !== undefined && element.value.length > 0) {
				value(element.value);
			}
			else {
				value('');
			}
		});

		ko.utils.domNodeDisposal.addDisposeCallback(element, function () {
			var picker = $(element).data('DateTimePicker');
			if (picker) {
				picker.destroy();
			}
		});
	},
	update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
		var picker = $(element).data('DateTimePicker');
		var value = valueAccessor();
		var allBindings = allBindingsAccessor();
		var valueUnwrapped = ko.utils.unwrapObservable(value);

		//when the view model is updated, update the widget
		if (picker) {

			// Date formats: http://momentjs.com/docs/#/displaying/format/
			var pattern = allBindings.format || 'MM/DD/YYYY';

			var output = "";
			if (valueUnwrapped !== null && valueUnwrapped !== undefined && valueUnwrapped.length > 0) {
				output = moment(valueUnwrapped).format(pattern);
			}

			if ($(element).is("input") === true) {
				$(element).val(output);
			} else {
				$(element).text(output);
			}
		}
	}
};

// standard dropdown
ko.bindingHandlers.select = {
	init: function (el, valueAccessor, allBindings) {
		var lookuptextfield = $(el).data('lookuptextfield');
		initializeSelect(el, lookuptextfield);

		ko.utils.domNodeDisposal.addDisposeCallback(el, function () {
			$(el).select2('destroy');
		});
	},
	update: function (el, valueAccessor, allBindings) {
		var val = ko.utils.unwrapObservable(valueAccessor());
		$(el).val(val).trigger('change');
	}
};


// ajax single input dropdown
ko.bindingHandlers.lookup = {
	init: function (el, valueAccessor, allBindings) {
		initializeLookupAjax(el);

		ko.utils.domNodeDisposal.addDisposeCallback(el, function () {
			$(el).select2('destroy');
		});
	},
	update: function (el, valueAccessor, allBindings) {
		$(el).trigger('change');
	}
};

// ajax multi-select dropdown
ko.bindingHandlers.lookupmultiple = {
	init: function (el, valueAccessor, allBindings) {
		initializeLookupMultipleAjax(el);

		ko.utils.domNodeDisposal.addDisposeCallback(el, function () {
			$(el).select2('destroy');
		});
	},
	update: function (element, valueAccessor, allBindings) { }
};

ko.bindingHandlers.lookupTags = {
	init: function (el, valueAccessor, allBindings) {
		initializeLookupTaggedAjax(el);

		ko.utils.domNodeDisposal.addDisposeCallback(el, function () {
			$(el).select2('destroy');
		});
	},

	update: function (el, valueAccessor, allBindings) { }
};

// tags
ko.bindingHandlers.tags = {
	init: function (el, valueAccessor, allBindings) {
		initializeTags(el);

		ko.utils.domNodeDisposal.addDisposeCallback(el, function () {
			$(el).select2('destroy');
		});
	},
	update: function (el, valueAccessor, allBindings) { }
};

ko.components.register('functionalrole-node', {
	viewModel: function (params) {
		var self = this;
		ko.mapping.fromJS(params.Node, {}, self);

		self.syncChildState = function () {
			var queue = new Queue();
			queue.enqueue(self);

			currentTree = queue.dequeue();

			while (currentTree) {
				for (var i = 0, length = currentTree.Children().length; i < length; i++) {
					queue.enqueue(currentTree.Children()[i]);
				}

				self.deselectDescendant(currentTree);
				currentTree = queue.dequeue();
			}
		}

		self.deselectDescendant = function (currentTree) {
			currentTree.FunctionalRole.Selected(false);
		}


		self.changeState = function (data, event) {
			self.FunctionalRole.Selected(event.currentTarget.checked);
			if (!self.FunctionalRole.Selected()) {
				self.syncChildState();
			}

			return true;
		}

		self.test = function (parents, data, event) {
			let checked = event.currentTarget.checked;
			self.FunctionalRole.Selected(checked);
			if (self.FunctionalRole.Selected())
				$('#functionalRoleGroup_' + self.FunctionalRole.GroupId() + '_' + self.FunctionalRole.Id()).prop("checked", true);


			if (self.FunctionalRole.TypeId() == 1 && self.FunctionalRole.ParentId() !== null) // -- radio && not root
			{
				let parent = parents[2];
				let siblings = ko.utils.arrayFilter(parent.Children(), function (item) {
					if (item.FunctionalRole.Id() != self.FunctionalRole.Id())
						return item;
				});

				ko.utils.arrayForEach(siblings, function (sibling) {
					sibling.FunctionalRole.Selected(false);
					var queue = new Queue();
					queue.enqueue(sibling);

					currentTree = queue.dequeue();

					while (currentTree) {
						for (var i = 0, length = currentTree.Children().length; i < length; i++) {
							queue.enqueue(currentTree.Children()[i]);
						}

						currentTree.FunctionalRole.Selected(false);
						currentTree = queue.dequeue();
					}
				});
			}
			else if (self.FunctionalRole.TypeId() == 3) // -- checkbox
			{

				if (!checked) {
					self.syncChildState();
				}
			}

			//return true;
		};

		return self;
	},
	template: "<div data-bind=\"template: {name: 'functionalrole-node' } \"></div>"
});

ko.components.loaders.push({
	getConfig: function (name, callback) {
		var gridName;
		var templateString;

		if (name.indexOf("grid-") > -1) {
			gridName = name.substr(5).toLowerCase();
			templateString = "<div data-bind=\"template: {name: '" + gridName + "' } \"></div>";

			callback({
				viewModel: function (params) {
					var self = this;
					self.rawData = params.data;
					self.currentPageIndex = ko.observable(0);
					self.pageSize = params.config.pageSize;
					self.enableMultiSelect = params.config.enableMultiSelect;
					self.enableSorting = params.config.enableSorting;
					self.enableSubGrid = params.config.enableSubGrid;
					self.enableAddRow = params.config.enableAddRow;
					self.rowTemplate = params.config.rowTemplate;
					self.headerTemplate = params.config.headerTemplate;
					self.subGridTemplate = params.config.subGridTemplate;
					self.addRowTemplate = params.config.addRowTemplate;
					self.columns = params.config.columns;

					var dataFilterObj = {};
					dataFilterObj['property'] = '';
					dataFilterObj['filter'] = '';
					self.dataFilter = ko.mapping.fromJS(dataFilterObj);

					self.filteredData = ko.computed(function () {
						if (!self.dataFilter.property() || !self.dataFilter.filter()) {
							return self.rawData();
						} else {
							var search = self.dataFilter["filter"]().toString().toLowerCase();

							return ko.utils.arrayFilter(self.rawData(), function (item) {
								var prop = item[self.dataFilter.property()]();
								prop = prop ? prop.toString().toLowerCase() : '';
								return prop.indexOf(search) >= 0;
							});
						}
					});

					self.selectAll = ko.observable();

					self.itemsOnCurrentPage = ko.computed(function () {
						var startIndex = self.pageSize * self.currentPageIndex();
						return ko.unwrap(self.filteredData).slice(startIndex, startIndex + self.pageSize);
					}, this);

					self.maxPageIndex = ko.computed(function () {
						return Math.ceil(ko.unwrap(self.filteredData).length / self.pageSize) - 1;
					}, this);

					self.sortByHeader = function (header) {
						self.rawData.sort(function (a, b) {
							if (header.order) {
								return a[header.rowText]() < b[header.rowText]() ? -1 : 1;
							}
							else {
								return a[header.rowText]() > b[header.rowText]() ? -1 : 1;
							}
						});
						header.order = !header.order;
					}

					self.selectAll.subscribe(function (value) {
						ko.utils.arrayForEach(self.filteredData(), function (item) {
							item.Selected(value);
						})
					}, this)

					self.toggleFilter = function (data, event) {
						self.dataFilter.filter(null);
						self.dataFilter.property(null);
					}

					self.setFilterProperty = function (data) {
						if (data.rowText == self.dataFilter.property()) {
							self.dataFilter.filter(null);
							self.dataFilter.property(null);
						}
						else {
							self.dataFilter.property(data.rowText);
						}
					}

					self.toggleSubGrid = function (data, event) {
						if (self.enableSubGrid) {
							var bodeh = $(event.target).closest('tbody')[0];

							var currentRowSubGrid = $(event.target).closest('tr').next('tr');
							var subGridIsHidden = currentRowSubGrid.hasClass('hidden');

							$.each(bodeh.children, function (key, value) {
								if (key % 2 > 0) {
									$(value).addClass('hidden');
								}
							});

							if (subGridIsHidden) {
								currentRowSubGrid.removeClass('hidden');
							}
							else {
								currentRowSubGrid.addClass('hidden');
							}

							return true;
						}
						else {
							return false;
						}
					}
				},
				template: templateString
			});
		} else {
			callback(null);
		}
	},
	loadComponent: ko.components.defaultLoader.loadComponent
});

ko.components.register('dynamic-tree-menu', {
	viewModel: function (params) {
		var self = this;
		self.ComponentGroupId = params.ComponentGroupId;
		self.UserId = params.UserId;
		self.Tree = params.Tree;
		self.UserTemplates = params.UserTemplates;
		self.SelectedTemplate = ko.observable();
		self.SelectedComponentGroup = ko.observable();

		self.applyTemplate = function () {
			self.clearTree();
			ko.utils.arrayForEach(self.getNodesForSelection(), function (Node) {
				if (Node.SelectList().length > 0) {
					var itemToSelect = self.getSelectedOptionFromSelectList(Node);
					itemToSelect.Selected(true);
					Node.SelectedSelectListOption(itemToSelect.Value());
				}
				else {
					Node.Selected(true);
					$('input[name="radioGroup' + Node.GroupId() + '"]').prop('checked', true);
				}
			});
		};

		self.showConfigurationModal = function () {
			$('#dynamic-tree-copyConfiguration-modal').modal();
		};

		self.copyConfiguration = function () {
			var destinationGroup;
			var copyToId = self.SelectedComponentGroup()[0].Value;
			if (typeof copyToId == 'number') {
				destinationGroup = viewModel.getComponentGroupById(copyToId);
			}
			else {
				destinationGroup = viewModel.getComponentGroupByGroupId(copyToId);
			}

			destinationGroup.NextGenFunctionalRoles([]);

			var newTree = ko.mapping.fromJS(ko.mapping.toJS(self.Tree()));

			ko.utils.arrayForEach(newTree(), function (item) {
				destinationGroup.NextGenFunctionalRoles.push(new FunctionalRoleViewModel(item));
			});

			$('#dynamic-tree-copyConfiguration-modal').modal('hide');
		};

		self.resetTree = function () {
			bootbox.confirm("Are you sure you would like to reset the configuration tree to its default state?", function (result) {
				if (result) {
					self.clearTree();
				}
			});
		};

		self.clearTree = function () {
			ko.utils.arrayForEach(self.Tree(), function (Node) {
				Node.Selected(false);
				$('input[name="radioGroup' + Node.GroupId() + '"]').prop('checked', false);
				if (Node.SelectList().length > 0) {
					Node.SelectedSelectListOption(null);
				}
			});
		};

		self.getNodesForSelection = function () {
			return ko.utils.arrayFilter(self.Tree(), function (x) {
				if (x.SelectList().length > 0) {
					var selectList = ko.utils.arrayFilter(x.SelectList(), function (y) {
						return self.SelectedTemplate()[0].SelectedIds.indexOf(parseInt(y.Value())) >= 0;
					});

					return selectList;
				}
				else {
					return self.SelectedTemplate()[0].SelectedIds.indexOf(x.Id()) >= 0;
				}
			});
		};

		self.getSelectedOptionFromSelectList = function (Node) {
			return ko.utils.arrayFilter(Node.SelectList(), function (x) {
				return self.SelectedTemplate()[0].SelectedIds.indexOf(parseInt(x.Value())) >= 0;
			})[0];
		};
	},
	template: "<div data-bind=\"template: {name: 'dynamic-tree-menu' } \"></div>"
});

ko.components.register('dyanmic-tree', {
	viewModel: function (params) {
		var self = this;
		self.Tree = params.Tree;

		self.selectedNodes = ko.computed(function () {
			var selected = [];
			ko.utils.arrayForEach(self.Tree(), function (Node) {
				if (Node.Selected()) {
					selected.push(Node.Id());
				}
				else if (Node.SelectList().length > 0) {
					if (Node.SelectedSelectListOption()) {
						selected.push(Node.SelectedSelectListOption());
					}
				}
			});

			return selected;
		}, this);

		self.DistinctRadioGroupIds = ko.pureComputed(function () {
			var groupIds = ko.utils.arrayMap(self.Tree(), function (Node) { if (Node.TypeId() == 1) return Node.GroupId() })
			return ko.utils.arrayGetDistinctValues(groupIds).sort();
		}, this);


		ko.utils.arrayForEach(self.DistinctRadioGroupIds(), function (Id) {
			if (Id) {
				var dynamicName = 'RadioGroup' + Id;
				self[dynamicName] = ko.observable();

				self[dynamicName].subscribe(function (role) {
					self.syncGroupState(role);
				});
			}
		});

		self.setGroupSelection = function (role) {
			role.Selected(true);
			var dynamicName = 'RadioGroup' + role.GroupId();
			self[dynamicName](role);
			return true;
		};

		self.syncGroupState = function (role) {
			ko.utils.arrayForEach(self.Tree(), function (Node) {
				if (Node.GroupId() == role.GroupId() && Node.Id() != role.Id()) {
					Node.Selected(false);
				}
			});
		};
	},
	template: "<div data-bind=\"template: {name: 'standard-dynamic-tree-template' } \"></div>"
});

ko.bindingHandlers.dynamicTreeChildState = {
	init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
	},
	update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
		var role = ko.unwrap(valueAccessor());

		if (!role.ParentId()) {
			$(element).show();
			return false;
		}

		var allRoles = bindingContext.$component.Tree();
		var parent = allRoles.filter(function (r) {
			return r.Id() == role.ParentId();
		});

		if (parent[0].Selected() && parent[0].TypeId() != 3) {
			$(element).show();
		}
		else {
			$(element).hide();
			ko.utils.arrayForEach(allRoles, function (r) {
				if (r.GroupId() == role.GroupId()) {
					r.Selected(false);
					$('input[name="radioGroup' + r.GroupId() + '"]').prop('checked', false);
				}
			});
		}
	}
};

ko.bindingHandlers.dynamicTreeChildInputState = {
	init: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
		if (viewModel.TypeId() == 1 || viewModel.TypeId() == 2) {
			$(element).prop('checked', viewModel.Selected());
		}
		else if (viewModel.TypeId() == 3) {
			if (viewModel.SelectList().length > 0) {
				if (viewModel.SelectedSelectListOption()) {
					viewModel.SelectList.push(SelectedSelectListOption())
				}
			}
		}
	},

	update: function (element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {

	}
};

ko.extenders.simpleNumeric = function (target, precision) {
	//create a writable computed observable to intercept writes to our observable
	var result = ko.pureComputed({
		read: target,  //always return the original observables value
		write: function (newValue) {
			var current = target(),
				roundingMultiplier = Math.pow(10, precision),
				newValueAsNum = isNaN(newValue) ? 0 : +newValue,
				valueToWrite = Math.round(newValueAsNum * roundingMultiplier) / roundingMultiplier;

			//only write if it changed
			if (valueToWrite !== current) {
				target(valueToWrite);
			} else {
				//if the rounded value is the same, but a different value was written, force a notification for the current field
				if (newValue !== current) {
					target.notifySubscribers(valueToWrite);
				}
			}
		}
	}).extend({ notify: 'always' });

	//initialize with current value to make sure it is rounded appropriately
	result(target());

	//return the new computed observable
	return result;
};


ko.extenders.numeric = function (target, options) {
	//create a writable computed observable to intercept writes to our observable
	var result = ko.pureComputed({
		read: target,  //always return the original observables value
		write: function (newValue) {
			var current = target(),
				roundingMultiplier = Math.pow(10, options.precision),
				newValueAsNum = isNaN(newValue) ? 0 : Math.abs(+newValue),
				validValueToWrite = isNaN(options.interval) ? true : options.RegExValidator.test((Math.round(newValueAsNum * options.interval) / options.interval).toFixed(options.precision)),
				valueToWrite = validValueToWrite ? (Math.round(newValueAsNum * options.interval) / options.interval).toFixed(options.precision) : 0;

			//only write if it changed
			if (valueToWrite !== current) {
				target(valueToWrite);
			} else {
				//if the rounded value is the same, but a different value was written, force a notification for the current field
				if (newValue !== current) {
					target.notifySubscribers(valueToWrite);
				}
			}
		}
	}).extend({ notify: 'always' });

	//initialize with current value to make sure it is rounded appropriately
	result(target());

	//return the new computed observable
	return result;
};


ko.extenders.dynamicsFilenumberFormat = function (target, options) {
	//create a writable computed observable to intercept writes to our observable
	let result = ko.pureComputed({
		read: target,  //always return the original observables value
		write: function (newValue) {
			let current = target();
			let valueToWrite = convertToDynamicsFileNumberFormat(newValue);
			//only write if it changed
			if (newValue !== current) {
				target(valueToWrite);
			} else {
				//if the value is the same, but a different value was written, force a notification for the current field
				if (newValue !== current) {
					target.notifySubscribers(valueToWrite);
				}
			}
		}
	}).extend({ notify: 'always' });

	//initialize with current value
	result(target());

	//return the new computed observable
	return result;
};