﻿koGLI = (function () {
	function koGLI() {

	}

	var koGLI = {
		get: function () {

		}
	};

	koGLI.gridConfig = function (configuration) {
		this.data = configuration.data;
		this.currentPageIndex = ko.observable(0);
		this.pageSize = configuration.pageSize || 10;
		this.enableMultiSelect = configuration.enableMultiSelect || false;
		this.enableSorting = configuration.enableSorting || false;
		this.enableSubGrid = configuration.enableSubGrid || false;
		this.enableAddRow = configuration.enableAddRow || false;
		this.rowTemplate = configuration.rowTemplate || '';
		this.headerTemplate = configuration.headerTemplate || '';
		this.subGridTemplate = configuration.subGridTemplate || '';
		this.addRowTemplate = configuration.addRowTemplate || '';
		this.columns = configuration.columns;

		return this;
	}

	koGLI.dirtyFlag = function (root, isInitiallyDirty) {

		var result = function () { },
			_initalState = ko.observable(ko.toJSON(root)),
			_isInitiallyDirty = ko.observable(isInitiallyDirty);

		result.isDirty = ko.computed(function () {
			return _isInitiallyDirty() || _initalState() !== ko.toJSON(root);
		});

		result.reset = function () {
			_initalState(ko.toJSON(root));
			_isInitiallyDirty(false);
		}

		return result;
	}

	return koGLI;
}());