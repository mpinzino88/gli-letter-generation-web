﻿//Mapping---
var DocumentVersionViewModelMapping = {
	'BillingInfo': {
		create: function (options) {
			return new BillingInfoViewModel(options.data);
		}
	},
	'Jurisdiction': {
		create: function (options) {
			return new JurisdictionGroupViewModel(options.data);
		}
	},
	'Jurisdictions': {
		create: function (options) {
			return new JurisdictionGroupViewModel(options.data);
		}
	},
	'JurisdictionsToAdd': {
		create: function (options) {
			return new JurisdictionGroupViewModel(options.data);
		}
	},
	'LetterLanguageOptions': {
		create: function (options) {
			return new OptionsViewModel(options.data);
		}
	},
	'LetterTypeOptions': {
		create: function (options) {
			return new OptionsViewModel(options.data);
		}
	},
	'UpdatedDateReasons': {
		create: function (options) {
			return new OptionsViewModel(options.data);
		}
	}

};

var SupplementalLettersViewModelMapping = {
	'Letterhead': {
		create: function (options) {
			return new OptionsViewModel(options.data);
		}
	}
};

var BillingInfoViewModelMapping = {
	'ReportTypes': {
		create: function (options) {
			return new OptionsViewModel(options.data);
		}
	},
	'SpecializedTypes': {
		create: function (options) {
			return new OptionsViewModel(options.data);
		}
	},
	'LetterBookBillingSheetTypes': {
		create: function (options) {
			return new OptionsViewModel(options.data);
		}
	},
	'LetterBookBallyTypes': {
		create: function (options) {
			return new OptionsViewModel(options.data);
		}
	}
};

var JurisdictionGroupViewModelMapping = {
	'JurisdictionRequests': {
		create: function (options) {
			return new JurisdictionRequestViewModel(options.data);
		}
	}
};

var BillingInfoViewModel = function (data) {
	var self = this;
	ko.mapping.fromJS(data, BillingInfoViewModelMapping, self);
};

//var SupplementalLettersViewModel = function (data) {
//    var self = this;
//    ko.mapping.fromJS(data, SupplementalLettersViewModelMapping, self);
//    self.LetterheadId = ko.computed(function () {
//        return self.Letterhead.Value() !== undefined ? parseInt(self.Letterhead.Value()) : 0;
//    });
//}

var JurisdictionGroupViewModel = function (data) {
	var self = this;
	self.Name = ko.observable();
	self.JurisdictionId = ko.observable();
	self.UseDraftLetters = ko.observable();
	ko.mapping.fromJS(data, JurisdictionGroupViewModelMapping, self);

	self._destroy(false);
	self.selected(false);

	//custom fields
	self.NameAndJurisdictionId = ko.computed(function () {
		var fieldVal = this.Name() + " (<strong>" + this.JurisdictionId() + "</strong>)";
		return fieldVal;
	}, self);

	self.IdNumbersAndVersions = ko.computed(function () {
		var fieldVal = '';
		ko.utils.arrayForEach(this.JurisdictionRequests(), function (request) {
			fieldVal = fieldVal + request.FileNumber() + ' - <i class="idnumberFont">' + request.IdNumber() + '</i> / <i class="versionFont">' + request.Version() + '</i><br />';
		});
		return fieldVal;
	}, self);

	self.Version = ko.computed(function () {
		return (self.UseDraftLetters() ? 0.01 : 1);
	});
	self.LetterheadId = ko.computed(function () {
		return self.Letterhead.Value() !== undefined ? parseInt(self.Letterhead.Value()) : 0;
	});
	//remove this and just use the one value
	self.UseDraftWatermark = ko.computed(function () {
		return self.UseDraftLetters();
	});
};

var JurisdictionRequestViewModel = function (data) {
	var self = this;
	ko.mapping.fromJS(data, {}, self);
};

var OptionsViewModel = function (data) {
	var self = this;
	ko.mapping.fromJS(data, {}, self);
};