﻿var initialGridRequest = true;
var initialGridRequestExternal = true;

// extend jqGrid
$.jgrid.extend({
	getColumnIndexByName: function (columnName) {
		var cm = this.jqGrid('getGridParam', 'colModel'), i, l = cm.length;
		for (i = 0; i < l; i++) {
			if (typeof (cm[i]) != typeof (undefined) && cm[i].name === columnName) {
				var colOffset = 0;
				if (cm[0].name === 'subgrid' || cm[1].name === 'subgrid') {
					colOffset += 1;
				}

				if (this.jqGrid('getGridParam', 'multiselect')) {
					colOffset += 1;
				}

				return i - colOffset;
			}
		}
		return -1;
	}
});

$(function () {
	//resize grid when window is resized
	$(window).bind('resize', function () {
		$("table[id$='Grid']").each(function () {
			$(this).setGridWidth($(".body-content").width());
		});
	}).trigger('resize');

	//save grid state when leaving page
	$(window).on('unload', function () {
		$("table[id$='Grid']").each(function () {
			$(this).saveGridState();
		});
	});

	//set grid state on initial load of page
	if (initialGridRequest) {
		initialGridRequest = false;

		$("table[id$='Grid']").each(function () {
			$(this).setGridState();
		});
	}

	//clear select2 on search toolbar X click
	$('.clearsearchclass').on('click', function () {
		var dropDownObject = $(this).closest('tr').find('select[id^="gs_"]').first();
		if (dropDownObject) {
			dropDownObject.val(null).trigger('change.select2');
		}
	});

	//clear select2 on grid refresh icon click
	$('.ui-icon-refresh').on('click', function () {
		var dropDownObjects = $(this).closest('div.ui-jqgrid-view').find('select[id^="gs_"]');
		for (i = 0; i < dropDownObjects.length; i++) {
			$(dropDownObjects[i]).val(null).trigger('change.select2');
		}
	});
});

//functions
$.jgrid.extend({
	loadGridState: function () {
		var gridId = $(this).jqGrid('getGridParam', 'id');
		var gridState = localStorage.getItem("GLI." + gridId);

		if (gridState != null) {
			var gridInfo = $.parseJSON(gridState);

			$(this).jqGrid('setGridParam', { sortname: gridInfo.sortname });
			$(this).jqGrid('setGridParam', { sortorder: gridInfo.sortorder });
			$(this).jqGrid('setGridParam', { selrow: gridInfo.selrow });
			$(this).jqGrid('setGridParam', { page: gridInfo.page });
			$(this).jqGrid('setGridParam', { rowNum: gridInfo.rowNum });
			$(this).jqGrid('setGridParam', { postData: gridInfo.postData });
			$(this).jqGrid('setGridParam', { search: gridInfo.search });
		}
	},
	setGridState: function () {
		//set sort icon
		var sortName = $(this).jqGrid('getGridParam', 'sortname');
		var sortOrder = $(this).jqGrid('getGridParam', 'sortorder');
		$(this).jqGrid('sortGrid', sortName, false, sortOrder);

		//set filter boxes
		var gridId = $(this).jqGrid('getGridParam', 'id');
		var gridState = localStorage.getItem("GLI." + gridId);

		if (gridState != null && gridState != "") {
			var gridInfo = JSON.parse(gridState);
			var postData = gridInfo.postData;

			$.each(postData, function (k, v) {
				var searchBox = $('#gs_' + k);
				if (searchBox.length) {
					//select2
					if (searchBox.prop('tagName') && searchBox.prop('tagName').toLowerCase() === 'select') {
						searchBox.val(v).trigger('change.select2');
					} else {
						searchBox.val(v);
					}
				}
			});
		}
	},
	saveGridState: function () {
		var gridId = $(this).jqGrid('getGridParam', 'id');

		var gridInfo = new Object();
		gridInfo.sortname = $(this).jqGrid('getGridParam', 'sortname');
		gridInfo.sortorder = $(this).jqGrid('getGridParam', 'sortorder');
		gridInfo.selrow = $(this).jqGrid('getGridParam', 'selrow');
		gridInfo.page = $(this).jqGrid('getGridParam', 'page');
		gridInfo.rowNum = $(this).jqGrid('getGridParam', 'rowNum');
		gridInfo.postData = $(this).jqGrid('getGridParam', 'postData');
		gridInfo.search = $(this).jqGrid('getGridParam', 'search');

		localStorage.setItem("GLI." + gridId, JSON.stringify(gridInfo));
	}
});

//this function is set up on the grid (ClientSideEvents.BeforeAjaxRequest)
function gridStateBeforeAjaxRequest() {
	if (initialGridRequest) {
		$("table[id$='Grid']").each(function () {
			$(this).loadGridState();
		});
	}
}

//grid links
function fileNumberLink(cellValue, options, rowObject) {
	var submissionId = rowObject[$(this).getColumnIndexByName("SubmissionId")] || rowObject[$(this).getColumnIndexByName("Id")];
	return formatFileNumberLink(submissionId, cellValue);
}

function fileNumberLink2(cellValue, options, rowObject) {
	var submissionId = rowObject[$(this).getColumnIndexByName("SubmissionId")] || rowObject[$(this).getColumnIndexByName("Id")];
	var url = jsPath + 'submission/submission/detail/' + submissionId;
	return '<a href="' + url + '" target="_blank">' + cellValue + '</a>';
}

function projectLink(cellValue, options, rowObject) {
	var projectId = rowObject[$(this).getColumnIndexByName("ProjectId")] || rowObject[$(this).getColumnIndexByName("Id")];

	if (projectId == 0) {
		return cellValue;
	}

	var url = jsPath + 'protrack/project/detail/' + projectId;
	return '<a href="' + url + '" target="_blank">' + cellValue + '</a>';
}

function getBundleById(bundleId) {
	return "/QABundle/EditBundle.aspx?BundleID=" + bundleId;
}

//grid link helpers
function formatFileNumberLink(submissionId, fileNumber) {
	var url = jsPath + "submission/submission/detail?filenumber=" + fileNumber;
	return '<a href="' + url + '" target="_blank">' + fileNumber + '</a>';
}

function formatYesNo(cellValue, options, rowObject) {
	return (cellValue == "True") ? 'Yes' : 'No';
}

//grid custom html
function formatJurisdictionStatus(jurisdictionStatus) {
	return '<div title="Letter Type">' + jurisdictionStatus + '</div>';
}

// grid external filters
function loadFilters(itemName, storageType) {
	if (initialGridRequestExternal) {
		var sessionObj = sessionStorage;
		if (storageType && storageType === 'local') {
			sessionObj = localStorage;
		}

		var storage = sessionObj.getItem(itemName);
		if (!Em(storage)) {
			var storageArray = JSON.parse(storage);

			$.each(storageArray, function (i, field) {
				var fieldId = $('#' + field.id);
				if (fieldId) {
					if (field.text) {
						$('#' + field.id).append($('<option/>', { value: field.val, text: field.text }));
					}
					$('#' + field.id).val(field.val).trigger('change');
				}
			});
		}
		initialGridRequestExternal = false;
	}
}

function saveFilters(itemName, storageType) {
	var sessionObj = sessionStorage;
	if (storageType && storageType === 'local') {
		sessionObj = localStorage;
	}

	var storage = [];
	$('#SearchForm input, #SearchForm select').each(function () {
		var thisField = $(this);
		var field = {};
		field.id = thisField.attr('id');
		field.val = thisField.val();

		var lookupTextField = thisField.data('lookuptextfield');
		if (lookupTextField) {
			field.text = $('#' + lookupTextField).val();
		}

		storage.push(field);
	});

	if (storage.length > 0) {
		sessionObj.setItem(itemName, JSON.stringify(storage));
	}
	else {
		sessionObj.removeItem(itemName);
	}
}