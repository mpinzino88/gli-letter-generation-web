﻿// variables
var gridster;
var widget;
var widgetGrid, widgetChart;
var widgetStartHeight;
var widgetStartWidth;
var widgetGridCurrentHeight, widgetChartCurrentHeight;
var widgetGridCurrentWidth, widgetChartCurrentWidth;
var runGridAfterAjaxRequest = true;

var sortable1Default = $('<li class="empty ui-state-highlight ui-state-disabled">No more fields available.</li>');
var sortable2Default = $('<li class="empty ui-state-highlight ui-state-disabled">Drag fields here.</li>');

// used for configuring height
var gridsterContents = [];
var widgetDimension = 166;
var widgetMargin = 5;
var widgetPanelGridHeight = 31;
var jqGridColHeaderHeight = 25;
var jqGridPagerHeight = 25;
var jqGridToolbarSearchHeight = 50;

$(function () {
	// ajax event handler to reset client validation on page load
	$(document).ajaxComplete(function (event, request, settings) {
		$.validator.unobtrusive.parse(document);
	});

	// initialize gridster
	gridster = $('.gridster ul').gridster({
		widget_margins: [widgetMargin, widgetMargin],
		widget_base_dimensions: [widgetDimension, widgetDimension],
		resize: {
			enabled: true,
			start: function (e, ui, $widget) {
				widgetGrid = $widget.find('table.ui-jqgrid-btable').first();
				widgetGridCurrentHeight = widgetGrid.jqGrid('getGridParam', 'height');
				widgetGridCurrentWidth = widgetGrid.jqGrid('getGridParam', 'width');

				widgetChart = $widget.find('canvas').first();
				widgetChartCurrentHeight = widgetChart.height();
				widgetChartCurrentWidth = widgetChart.width();

				widgetStartHeight = $widget[0].clientHeight;
				widgetStartWidth = $widget[0].clientWidth;
			},
			resize: function (e, ui, $widget) {
				widgetGrid.jqGrid('setGridHeight', widgetGridCurrentHeight + ui.pointer.diff_top);
				widgetGrid.jqGrid('setGridWidth', widgetGridCurrentWidth + ui.pointer.diff_left);

				widgetChart.height(widgetChartCurrentHeight + ui.pointer.diff_top);
				widgetChart.width(widgetChartCurrentWidth + ui.pointer.diff_left);
			},
			stop: function (e, ui, $widget) {
				var widgetDiffTop = $widget[0].clientHeight - widgetStartHeight;
				var widgetDiffLeft = $widget[0].clientWidth - widgetStartWidth;

				widgetGrid.jqGrid('setGridHeight', widgetGridCurrentHeight + widgetDiffTop);
				widgetGrid.jqGrid('setGridWidth', widgetGridCurrentWidth + widgetDiffLeft);

				widgetChart.height(widgetChartCurrentHeight + widgetDiffTop);
				widgetChart.width(widgetChartCurrentWidth + widgetDiffLeft);

				savePositions();
			}
		},
		draggable: {
			handle: 'header, header *',
			stop: function (e, ui) {
				savePositions();
			}
		}
	}).data('gridster');

	// properly set widget content height
	$('#Dashboard > li').each(function () {
		var widgetId = $(this).data('widgetid');
		var sizey = $(this).data('sizey');
		var height = sizey * widgetDimension;
		var totalMargin = widgetMargin * 2;
		if (sizey > 1) {
			height = height + ((sizey - 1) * totalMargin);
		}

		var contentHeight = { widgetId: widgetId, height: height };
		gridsterContents.push(contentHeight);
	});

	// load widget content
	$('.widget-content').each(function (index, item) {
		var url = $(item).data('url');
		$(item).load(url);
	});

	// edit widget content
	$('#Dashboard i.fa-cogs').on('click', function () {
		widget = $(this).closest('li[data-widgetid]');
		widgetGrid = widget.find('table.ui-jqgrid-btable').first();
		var widgetId = widget.data('widgetid');
		editWidget(widgetId);
	});

	// maximize widget
	$('#Dashboard i.fa-window-maximize').on('click', function () {
		var modalDialog = $('#MaximizeWidgetModal');
		widget = $(this).closest('li[data-widgetid]');
		widgetGrid = widget.find('table.ui-jqgrid-btable').first();
		widgetStartHeight = widgetGrid.jqGrid('getGridParam', 'height');
		widgetStartWidth = widgetGrid.jqGrid('getGridParam', 'width');

		modalDialog.find('.modal-title').text(widget.find('header').text());
		modalDialog.modal('show');

		var gridHeightOffset = getGridHeightOffset(widget, widgetGrid);
		var widgetContent = widget.find('.widget-content').contents();
		modalDialog.find('.modal-body').append(widgetContent);
		widgetGrid.jqGrid('setGridHeight', modalDialog.find('.modal-dialog').height() + gridHeightOffset);
		widgetGrid.jqGrid('setGridWidth', modalDialog.find('.modal-dialog').width() - 3);

		runGridAfterAjaxRequest = false;
	});

	// close/minimize widget
	$('#MaximizeWidgetModal').on('hide.bs.modal', function (e) {
		var modalContent = $(this).find('.modal-body').contents();
		var widgetContent = widget.find('.widget-content').append(modalContent);
		widgetGrid.jqGrid('setGridHeight', widgetStartHeight);
		widgetGrid.jqGrid('setGridWidth', widgetStartWidth);

		runGridAfterAjaxRequest = true;
	});

	// tools
	$('#AddWidgetBtn').on('click', function (e) {
		e.preventDefault();
		$('#WidgetType').val("");
		var modalDialog = $('#ChooseWidgetTypeModal');
		var url = modalDialog.data('url');

		$.get(url, {}, function (data) {
			modalDialog.modal('show');
			modalDialog.html(data);
		});
	});

	$('#GalleryBtn').on('click', function (e) {
		e.preventDefault();
		location.replace(jsPath + "Widget/Gallery");
	});

	$('.EditDashboardBtn').on('click', function (e) {
		e.preventDefault();
		var modalDialog = $('#EditDashboardModal');
		var url = modalDialog.data('url') + '?Mode=' + $(this).data('mode');

		$.get(url, {}, function (data) {
			modalDialog.modal('show');
			modalDialog.html(data);
			$('#DashboardEditForm #Name').focus();
		});
	});

	$('.ChooseDashboardBtn').on('click', function (e) {
		e.preventDefault();
		var dashboardId = $(this).data('id');
		dashboardId = (dashboardId == '') ? -1 : dashboardId;
		$('#DefaultDashboardId').val(dashboardId);
		$('#DashboardForm').submit();
	});

	$('#ChooseWidgetTypeModal').on('hidden.bs.modal', function () {
		var widgetType = $('#WidgetType').val();

		if (widgetType) {
			var modalDialog = $('#EditWidgetModal');
			var url = jsPath + "Widget/Add";

			$.get(url, { widgetType: widgetType }, function (data) {
				modalDialog.modal('show');
				modalDialog.html(data);
			});
		}
	});

	// export
	$('#EditWidgetModal').on('click', '#ExportMenu a', function (e) {
		e.preventDefault();
		var postData = widgetGrid.jqGrid('getGridParam', 'postData');

		$('#FiltersToolbar').val($.param(postData));
		$('#WidgetId').val($(this).data('widgetid'));
		$('#ExportType').val($(this).data('exporttype'));
		$('#ExportForm').submit();
	});
});

// helper functions
function editWidget(widgetId) {
	var modalDialog = $('#EditWidgetModal');
	var url = modalDialog.data('url');

	$.get(url, { id: widgetId }, function (data) {
		modalDialog.modal('show');
		modalDialog.html(data);
	});
}

function getGridsterContentByWidgetId(widgetId) {
	for (var i = 0, len = gridsterContents.length; i < len; i++) {
		if (gridsterContents[i].widgetId === widgetId)
			return gridsterContents[i];
	}
	return null;
}

function gridAfterAjaxRequest() {
	if (runGridAfterAjaxRequest) {
		bindGridElements()

		var widget = $(this).closest('li[data-widgetid]');
		var widgetId = widget.data('widgetid');
		var height = getGridsterContentByWidgetId(widgetId).height - widgetPanelGridHeight;
		height += getGridHeightOffset(widget, $(this));

		$(this).jqGrid('setGridHeight', height);
	}
}

function getGridHeightOffset(widget, grid) {
	var toolbarSearch = widget.find('table.ui-jqgrid-htable thead tr.ui-search-toolbar');
	var topPager = grid.jqGrid('getGridParam', 'toppager');
	var offSet = 0;

	if (topPager) {
		offSet -= jqGridPagerHeight;
	}

	if (toolbarSearch.length > 0) {
		offSet -= jqGridToolbarSearchHeight;
	}
	else {
		offSet -= jqGridColHeaderHeight;
	}

	return offSet;
}

function savePositions() {
	var positions = gridster.serialize();

	$.each(positions, function (i, position) {
		var widget = $('#Dashboard > li')[i];
		position.WidgetId = $(widget).data('widgetid');
	});

	positions = JSON.stringify({ 'positions': positions });

	$.ajax({
		url: jsPath + 'Widget/SavePositions',
		type: 'post',
		contentType: 'application/json',
		dataType: 'json',
		data: positions,
		success: function (data) {
			//alert(data);
		}
	})
}

function widgetPostComplete() {
	$('#EditWidgetModal').modal('hide');
	var pageSource = $('#PageSource').val();
	var mode = $('#Mode').val();

	if (pageSource === "Dashboard") {
		if (mode === "Add") {
			location.replace(jsPath + "Dashboard/Index");
		}
		else {
			var widgetId = $('#widget_Id').val();
			var widget = $('li[data-widgetid=' + widgetId + ']');
			$('#WidgetTitle' + widgetId).text($('#widget_Title').val());
			widget.find('header').first().attr('style', 'background-color: ' + $('#widget_Color').val() + ';');

			var widgetContent = widget.find('.widget-content');
			var url = widgetContent.data('url');
			widgetContent.load(url);
		}
	}
	else {
		location.replace(jsPath + "Widget/Gallery");
	}
}

// edit list helper functions
function toggleColumns(dataSource) {
	if (Em(dataSource)) {
		setColumns('', '');
	}

	var url = jsPath + 'Widget/LoadColumns';
	var widgetId = $('#widget_Id').val();
	var info = {
		widgetId: widgetId,
		dataSource: dataSource
	};

	$.getJSON(url, info, function (data) {
		setColumns(data.AvailableColumns, data.SelectedColumns);
	});
}

function toggleSearchCriteria(dataSource) {
	var searchCriteria = $('#SearchCriteria');

	if (Em(dataSource)) {
		searchCriteria.html('');
	}
	else {
		var url = jsPath + 'Widget/LoadSearchCriteria';
		var widgetId = $('#widget_Id').val();
		var info = {
			widgetId: widgetId,
			dataSource: dataSource
		};

		$.get(url, info, function (data) {
			searchCriteria.html(data);
			loadDocumentReady();
		});
	}
}

function setColumns(availableColumns, selectedColumns) {
	$('#sortable1, #sortable2').empty();

	if (Em(availableColumns) && Em(selectedColumns)) {
		$('#sortable1').append(sortable1Default);
		$('#sortable2').append(sortable2Default);
		return;
	}

	var availableColList = '';
	var selectedColList = '';

	$.each(availableColumns, function (key, value) {
		availableColList += '<li class="ui-state-default" id="' + value.DatabaseName + '">' + value.DisplayName + '</li>';
	});

	$.each(selectedColumns, function (key, value) {
		selectedColList += '<li class="ui-state-default" id="' + value.DatabaseName + '">' + value.DisplayName + '</li>';
	});

	$('#sortable1').append(availableColList);
	$('#sortable2').append(selectedColList);
}