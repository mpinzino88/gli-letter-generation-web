
var ListToTree = ( function( $ ) {
    function init( id, url ) {
        $( document ).ready( function() {
            listToTree( 'GPSDocumentFileTree' );

            $( "#collapse" ).on( "click", function() {
                var el = $( this );
                $( "li.expanded" ).each( function() {
                    var t = $( this );
                    collapse( t );
                } );
            } );
        } );
    }

    function listToTree( listId ) {
        var theList = $( '#' + listId );

        var collapsedItems = theList.find( 'li.collapsed, li.collapsed li' );
        var expandedItems = theList.find( 'li' ).not( collapsedItems );

        collapsedItems.on( 'click', expandList );
        expandedItems.on( 'click', collapseList );
        collapsedItems.children( 'ul' ).hide();
    }

    function collapseList( event ) {
        event.stopPropagation();

        var thisThing = $( this );
        collapse( thisThing );
    }

    function expandList( event ) {
        event.stopPropagation();
        
        var thisThing = $( this );
        expand( thisThing );
    }

    function collapse( el ) {
        el.off( 'click', collapseList );
        el.on( 'click', expandList );
        el.children( 'ul' ).hide();
        el.removeClass( 'expanded' ).addClass( 'collapsed' );
    }

    function expand( el ) {
        el.off( 'click', expandList );
        el.on( 'click', collapseList );
        el.children( 'ul' ).show();
        el.removeClass( 'collapsed' ).addClass( 'expanded' );
    }

    function getDirInfo( id, url) {
        url = url + "&path=" + id;
        $.ajax( {
            url: url,
            type: 'POST',
            dataType: 'json',
            data: '',
            success: function( data ) {
                    var target = document.getElementById( data.ID );
                    target.outerHTML = data.Value;
                    ListToTree.Convert( 'GPSDocumentFileTree' );
            }
        } );
    }

    function getTemplateDirInfo( id, fileNumber, url ) {
        url = url + "?templatepath=" + id;
        url = url + "&fileNumber=" + fileNumber;
        $( "div.loader" ).fadeIn( 'slow' );
        $.ajax( {
            url: url,
            type: 'POST',
            dataType: 'json',
            data: '',
            success: function( data ) {
                    var target = document.getElementById( data.ID );
                    target.innerHTML = data.Value;
                    $( ".loader" ).fadeOut( 'slow' );
            }
        } );
    }

    return {
        Convert: listToTree,
        Collapse: collapse,
        Expand: expand,
        GetDirInfo: getDirInfo,
        GetTemplateDirInfo: getTemplateDirInfo,
        Init: init
    }
} )( jQuery );

