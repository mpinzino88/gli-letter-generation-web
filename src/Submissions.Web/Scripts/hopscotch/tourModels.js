﻿// Master Billing Maintenance Tours
// -- Index Tour:
var MasterBillingTour = {
	id: "Index-masterBilling",
	steps: [
		{
			title: "Master Billing Search",
			content: "Search by Master Billing Project Id in either  Submissions File Format or Dynamics project format. i.e. MO-22-WMS-14-44 or MO022WMS14044",
			target: document.getElementById("searchField"),
			placement: "left",
			onNext: function () { }
		},
		{
			title: "Jurisdiction Filtter",
			content: "Allow for filtering result set by Jurisdiction.",
			target: document.getElementById("filter"),
			placement: "bottom",

		},
		{
			title: "Search Results",
			content: "Search results are displayed here. clicking the Edit link of the given row will take you to the Edit Association page.",
			target: document.getElementById("searchResults"),
			placement: "bottom",
			onNext: function () {
				$('#toggleSwitchContainer').show();
				$('#toggleSwitchContainer').prop('disabled', true);
			}
		},
		{
			title: "Bulk Update",
			content: "Use this option to turn on Bulk Update mode. This will allow you to change the association of multiple items at once.",
			target: document.getElementById("toggleSwitchContainer"),
			placement: "right",
			onNext: function () {
				if ($('#searchResults tr').length == 1) {
					$('#toggleSwitchContainer').hide();
					$('#toggleSwitchContainer').prop('disabled', false);
				}
				if ($('#searchResults tr').length > 1) {
					$('#toggleSwitchContainer').prop('disabled', false);
				}
				$('#AddBtn:hidden').show();
				$('#AddBtn').prop('disabled', true);
			}
		},
		{
			title: "Add Association",
			content: "Add a new association to the given Master Billing Project Id.",
			target: document.getElementById("AddBtn"),
			placement: "bottom",
			onNext: function (item) {
				if ($('#searchResults tr').length == 1) {
					$('#AddBtn:visible').hide();
					$('#AddBtn').prop('disabled', false);
				}

				if ($('#searchResults tr').length > 1) {
					$('#AddBtn').prop('disabled', false);
				}
			}
		},
		{
			title: "Exit",
			content: "Return to Tools menu.",
			target: document.getElementById("CancelBtn"),
			placement: "left",
		}
	]
};
