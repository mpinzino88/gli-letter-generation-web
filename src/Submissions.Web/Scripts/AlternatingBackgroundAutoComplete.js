﻿$.widget("ui.alternatingBackgroundAutoComplete", $.ui.autocomplete, {

    _renderItem: function (ul, item) {

        var ulCount = ul[0].childElementCount;
    
        //var li = $("<li>").text(item.label).appendTo(ul);

        if (ulCount % 2 == 0) {
            var li = $('<li class="autocomplete-dropdown-li-even autocomplete-dropdown-li">').text(item.label);
            var appended = li.appendTo(ul);
        } else {
            var li = $('<li class="autocomplete-dropdown-li-odd autocomplete-dropdown-li">').text(item.label);
            var appended = li.appendTo(ul);
        }

        //var list = $('#testDiv ul');
        //list.append(li.clone());

        return appended;
    }
});

