# Submissions.web
All notable changes to this project will be documented in this file.

## [Unreleased]


%Latest%
---
## [4.21.0](https://gd-code.gaminglabs.com/projects/GUS/repos/gli.submissions.web/commits?bb=t&until=4.21.0) - 2021-04-08
[JIRA List 4.21.0](https://gd-jira.gaminglabs.com/issues/?jql=labels%20%3D%204.21.0)
# Added
- [SUB-3687](https://gd-jira.gaminglabs.com/browse/SUB-3687)
	- Adds a new lookup 'Bussiness Owner' for use with employee metrics and relation to Senior leadership.'
- [PT-1706](https://gd-jira.gaminglabs.com/browse/PT-1706)
	- Adds warning message when deleting posted letters.
- [PT-1649](https://gd-jira.gaminglabs.com/browse/PT-1649)
	- Adds Exception manager interface to PK Utilization tool.
- [LETAUT-1241](https://gd-jira.gaminglabs.com/browse/LETAUT-1241)
	- Adds component type as an available data point for submission records.
- [LETAUT-1240](https://gd-jira.gaminglabs.com/browse/LETAUT-1240)
	- Conformance Criteria Finalization.

# Changed
- [LETAUT-1233](https://gd-jira.gaminglabs.com/browse/LETAUT-1233)
	- Changes the OCUIT interface to be Platform piece focused as opposed to template focused.
- [LETAUT-1231](https://gd-jira.gaminglabs.com/browse/LETAUT-1231)
	- Ability to copy a game description and updated available Jurisdictions in the Game Description interface. 

# Fixed
- [PT-1709](https://gd-jira.gaminglabs.com/browse/PT-1709)
	- Fixed a bug that previously would set Australia projects to NJ Cert Lab.
---
%Latest%

## [4.20.1](https://gd-code.gaminglabs.com/projects/GUS/repos/gli.submissions.web/commits?bb=t&until=4.20.1) - 2021-04-02
- Fix for Protrack not properly splitting bundles and missing a required document and jurisdiction.

---
## [4.20.0](https://gd-code.gaminglabs.com/projects/GUS/repos/gli.submissions.web/commits?bb=t&until=4.20.0) - 2021-03-31
[JIRA List 4.20.0](https://gd-jira.gaminglabs.com/issues/?jql=labels%20%3D%204.20.0)
# Added
- [PT-1696](https://gd-jira.gaminglabs.com/browse/PT-1696)
	- Add ADM Work Needed checkbox to Review Complete Modal
# Changed
- [SUB-3681](https://gd-jira.gaminglabs.com/browse/SUB-3681)
	- If some how bad data gets through, default Company, Currency, Contract Type, and Testing Type to prevent errors.
- [LETAUT-1235](https://gd-jira.gaminglabs.com/browse/LETAUT-1235)
	- Turn All Locks/Restrictions Back On for the Letter Generation interface.
- [LETAUT-1221](https://gd-jira.gaminglabs.com/browse/LETAUT-1221)
	- Resurrect Conformance Criteria.
- [LETAUT-1218](https://gd-jira.gaminglabs.com/browse/LETAUT-1218)
	- Changes the technical standard lookup to allow for relating technical standards to either evolution documents or functional roles.
# Fixed
- [LETAUT-1238](https://gd-jira.gaminglabs.com/browse/LETAUT-1238)
	- Fixes a bug that would prevent large platform piece templates from saving.
---

## [4.19.2](https://gd-code.gaminglabs.com/projects/GUS/repos/gli.submissions.web/commits?bb=t&until=4.19.2) - 2021-03-25
# Fixed
- Fixes blocking paytables issue in the Letter Generate interface where Gaming Guidelines is missing maximum RTP.
---

## [4.19.1](https://gd-code.gaminglabs.com/projects/GUS/repos/gli.submissions.web/commits?bb=t&until=4.19.1) - 2021-03-24
# Fixed
- Fixes loading the first instance of OCUIT.
- Fixes loading jurisdictions and selections in Evo Incoming Review.

## [4.19.0](https://gd-code.gaminglabs.com/projects/GUS/repos/gli.submissions.web/commits?bb=t&until=4.19.0) - 2021-03-24
[JIRA List 4.19.0](https://gd-jira.gaminglabs.com/issues/?jql=labels%20%3D%204.19.0)
# Added
- [PT-1683](https://gd-jira.gaminglabs.com/browse/PT-1683)
	- Adds the ability to retain any existing jurisdiction/document selections while adding or removing components from component group(s).
- [SUB-3671](https://gd-jira.gaminglabs.com/browse/SUB-3671)
	- Adds the Date Code to the replacement tracking search results.

# Changed
- [ACUM-73](https://gd-jira.gaminglabs.com/browse/ACUM-73)
	- Changes the billing party lookup to allow for adding / editing the Billing Party Code.
- [LETAUT-1213](https://gd-jira.gaminglabs.com/browse/LETAUT-1213)
	- Changes the Math tab of the Letter Generation interface to only show iGaming Math Fields for iGaming submissions.
- [LETAUT-1220](https://gd-jira.gaminglabs.com/browse/LETAUT-1220)
	- OCUIT doesn't retain for jurs that get split out(partiall fix- saving against primarys)
- [PT-1684](https://gd-jira.gaminglabs.com/browse/PT-1684)
	- Changes the way letters post via digital letter book in order to help alleviate stress on the web server.
	
# Fixed
- [LETAUT-1102](https://gd-jira.gaminglabs.com/browse/LETAUT-1102)
	- Fixes an issue with the Paytable blocking note for Any game type in the Letter Generation interface. 
- [LETAUT-1217](https://gd-jira.gaminglabs.com/browse/LETAUT-1217)
	- Fixes an issue that would cause certain bundles to not be able to open in the Letter Generation interface if the parent question ID was not properly set.
- [PT-1705](https://gd-jira.gaminglabs.com/browse/PT-1705)
	- Fixes an issue with the clause report interface that would cause a long instance list to run behind the GLI globe footer.
- [PT-1712](https://gd-jira.gaminglabs.com/browse/PT-1712)
	- Fixes an issue where the change reason provided from the task edit page would be too large for the database to hold.
---


## [4.18.0](https://gd-code.gaminglabs.com/projects/GUS/repos/gli.submissions.web/commits?bb=t&until=4.18.0) - 2021-03-19
[JIRA List 4.18.0](https://gd-jira.gaminglabs.com/issues/?jql=labels%20%3D%204.18.0)
# Added
- [SUB-3606](https://gd-jira.gaminglabs.com/browse/SUB-3606)
	- Adds ability to search for products.
- [PT-1688](https://gd-jira.gaminglabs.com/browse/PT-1688)
	- Adds new field to user edit (admin) page for Public Knowledge User Ids
- [LETAUT-1208](https://gd-jira.gaminglabs.com/browse/LETAUT-1208)
	- Adds new cover page for iGaming based on testing type to the Letter Generation interface.

# Changed
- [SUB-3656](https://gd-jira.gaminglabs.com/browse/SUB-3656)
	- Changed the Flat file interface to set the submission type to SY if the flat file material ID contains either "Margin Maker" or "QBMM".
- [PT-1633](https://gd-jira.gaminglabs.com/browse/PT-1633)
	- Changes the submission delete routine to restrict deletion of bundles with engineering tasks.
- [PT-1600](https://gd-jira.gaminglabs.com/browse/PT-1600)
	- Digital Letter Book Queue - Blank Billing sheet.

# Fixed
- [PT-1652](https://gd-jira.gaminglabs.com/browse/PT-1652)
	- Fixed an issue in the project review interface where removing a review item would sometimes cause problems with references to other items in the collection.
- [LETAUT-1215](https://gd-jira.gaminglabs.com/browse/LETAUT-1215)
	- Fixed an issue with the WV cover page and modifications that did not account specifically for WV.
- [LETAUT-1214](https://gd-jira.gaminglabs.com/browse/LETAUT-1214)
	- Fixed an issue with the WV cover page where Standard for Compliance was listed twice.
- [LETAUT-1102](https://gd-jira.gaminglabs.com/browse/LETAUT-1102)
	- Fixed an issue in Letter generate where maryland and delaware shows duplicate paytables. In addition, updated "General - Blocked Paytable Note with Jur Chip" to use "Additional Blocked Paytables" question.
- [LETAUT-1213](https://gd-jira.gaminglabs.com/browse/LETAUT-1213)
	- iGaming Math Fields - only show for iGaming
---

## [4.17.0](https://gd-code.gaminglabs.com/projects/GUS/repos/gli.submissions.web/commits?bb=t&until=4.17.0) - 2021-03-17
[JIRA List 4.17.0](https://gd-jira.gaminglabs.com/issues/?jql=labels%20%3D%204.17.0)
# Added
- [LETAUT-1123](https://gd-jira.gaminglabs.com/browse/LETAUT-1123)
	- Adds Text Search to the Notes Manager interface.
- [LETAUT-1187](https://gd-jira.gaminglabs.com/browse/LETAUT-1187)
	- Adds the ability to edit additional math fields for iGaming in the Letter Generation interface.
- [SUB-3634](https://gd-jira.gaminglabs.com/browse/SUB-3634)
	- Adds Product Status UI component to the Product interface.
- [SUB-3658](https://gd-jira.gaminglabs.com/browse/SUB-3658)
	- Adds the ability to track cabinet information in our database from data provided in flat file.
- [SUB-3663](https://gd-jira.gaminglabs.com/browse/SUB-3663)
	- Adds Testing Type as an option to be edited for Billing Party.
- [SUB-3667](https://gd-jira.gaminglabs.com/browse/SUB-3667)
	- Adds the ability to process cabinet data from a flat file and add it to submissions records.
- [SUB-3666](https://gd-jira.gaminglabs.com/browse/SUB-3666)
	- Adds Cabinet to the Submissions Detail page under More Info and allows the Cabinet field to be edited for manufacturers that have cabinets associated with them.
- [PT-1609](https://gd-jira.gaminglabs.com/browse/PT-1609)
	- Adds the accepted by field as a filter for the QA Advanced search interface.

# Changed
- [LETAUT-1204](https://gd-jira.gaminglabs.com/browse/LETAUT-1204) 
	- Changes to the RI paytable blocking routine used in the Letter Generation interface.
- [LETAUT-1191](https://gd-jira.gaminglabs.com/browse/LETAUT-1191)
	- Changes to restrict the jurisdiction dropdown in the game description interface so that merged components can't be linked to all jurisdictions from the main bundle.
- [SUB-3651](https://gd-jira.gaminglabs.com/browse/SUB-3651)
	- Changes to the flatfile process to set default values for Function, Position, Chip Type, System, and Project Detail when a flat file submission has a Testying Type of iGaming.
- [SUB-3669](https://gd-jira.gaminglabs.com/browse/SUB-3669)
	- Changes iGaming billing parties to be data driven for use in the flat file interface.
- [PT-1628](https://gd-jira.gaminglabs.com/browse/PT-1628)
	- Changes to bundle splitting logic to retain Evo/ProTrack links through bundle splits.
- [PT-1633](https://gd-jira.gaminglabs.com/browse/PT-1633)
	- Changes to restrict Bundle Deletion for Bundles with Engineering tasks.
- [PT-1607](https://gd-jira.gaminglabs.com/browse/PT-1607)
	- Changes to Digital Letter Book to make billable false when QA is reposting a letter.

# Fixed
- [LETAUT-1207](https://gd-jira.gaminglabs.com/browse/LETAUT-1207)
	- Fixes a typo and adds in additional fields to the WV first page of the Letter Generation interface.


---

## [4.16.2](https://gd-code.gaminglabs.com/projects/GUS/repos/gli.submissions.web/commits?bb=t&until=4.16.2) - 2021-03-12

# Fixed
- [ACUM-79](https://gd-jira.gaminglabs.com/browse/ACUM-79)
	- Fix for bill to itself (IsParentProject).

---
%Latest%
---
## [4.16.1](https://gd-code.gaminglabs.com/projects/GUS/repos/gli.submissions.web/commits?bb=t&until=4.16.1) - 2021-03-11

# Changed
- [PT-1669](https://gd-jira.gaminglabs.com/browse/PT-1669)
	- Updated IGT billwith mapping.
---
## [4.16.0](https://gd-code.gaminglabs.com/projects/GUS/repos/gli.submissions.web/commits?bb=t&until=4.16.0) - 2021-03-10
[JIRA List 4.16.0](https://gd-jira.gaminglabs.com/issues/?jql=labels%20%3D%204.16.0)
# Added
- [LETAUT-986](https://gd-jira.gaminglabs.com/browse/LETAUT-986)
	- Adds support for child questions to the Letter Generation interface.
- [SUB-3610](https://gd-jira.gaminglabs.com/browse/SUB-3610)
	- Adds the product list view to the Component detail page.
	(Hidden for now until product is complete.)
- [SUB-3649](https://gd-jira.gaminglabs.com/browse/SUB-3649)
	- Adds the ability to view jurisdiction details for flat file new submissions.

# Changed
- [LETAUT-1193](https://gd-jira.gaminglabs.com/browse/LETAUT-1193)
	- Changes the OCUIT interface for bundle specific editor, to use the same validation warning as the OCUIT templates.
- [PT-1606](https://gd-jira.gaminglabs.com/browse/PT-1606)
	- Changes the Digital Letterbook interface for QA to assist in choosing the right billing sheets.

---

## [4.15.0](https://gd-code.gaminglabs.com/projects/GUS/repos/gli.submissions.web/commits?bb=t&until=4.15.0) - 2021-03-08
	- [SUB-3657](https://gd-jira.gaminglabs.com/browse/SUB-3657)
		- Adds a new Cabinets lookup page that can be accessed from Tools.
	- [LETAUT-1132](https://gd-jira.gaminglabs.com/browse/LETAUT-1132)
		- Updates Standards in Letter Builder to pull from Evolution.

# Fixed
	- [PT-1666](https://gd-jira.gaminglabs.com/browse/PT-1666)
		- Bug fix for "Repeat" not being populated.
	- [LETAUT-1203](https://gd-jira.gaminglabs.com/browse/LETAUT-1203)
		- Fixes the CheckRTPRange calculation used for pay table blocking to account for a scenario where an explicit Gaming Guidelines  game type rule could exist in addition to an 'ANY' gaming guideline rule in the Letter Generation interface.
	- Fixed a bug where Copying Submissions would error outon trying to send a New Project Email.

---



## [4.15.0](https://gd-code.gaminglabs.com/projects/GUS/repos/gli.submissions.web/commits?bb=t&until=4.15.0) - 2021-03-08
	- [SUB-3657](https://gd-jira.gaminglabs.com/browse/SUB-3657)
		- Adds a new Cabinets lookup page that can be accessed from Tools.
	- [LETAUT-1132](https://gd-jira.gaminglabs.com/browse/LETAUT-1132)
		- Updates Standards in Letter Builder to pull from Evolution.

# Fixed
	- [PT-1666](https://gd-jira.gaminglabs.com/browse/PT-1666)
		- Bug fix for "Repeat" not being populated.
	- [LETAUT-1203](https://gd-jira.gaminglabs.com/browse/LETAUT-1203)
		- Fixes the CheckRTPRange calculation used for pay table blocking to account for a scenario where an explicit Gaming Guidelines  game type rule could exist in addition to an 'ANY' gaming guideline rule in the Letter Generation interface.
	- Fixed a bug where Copying Submissions would error outon trying to send a New Project Email.

---

## [4.14.2] - 2021-03-08
# Fixed
	- Sending a Project Creation Email would sometimes fail because of missing Jurisdictional data.  This is now fixed.

---

## [4.14.1] - 2021-03-08
# Changed
	- Flat File will not longer make use of the Hot column.

--

## [4.14.0](https://gd-code.gaminglabs.com/projects/GUS/repos/gli.submissions.web/commits?bb=t&until=4.14.0) - 2021-03-05
[JIRA List 4.14.0](https://gd-jira.gaminglabs.com/issues/?jql=labels%20%3D%204.14.0)
# Added
	- [SUB-3607](https://gd-jira.gaminglabs.com/browse/SUB-3607)
		- Adds the ability to delete Products from the Product interface.
	- [SUB-3609](https://gd-jira.gaminglabs.com/browse/SUB-3609)
		- Adds the ability to view and manage continuity and critical component lists to the Product interface.
	- [SUB-3430](https://gd-jira.gaminglabs.com/browse/SUB-3430)
		- Adds Game Name to the Submissions Bulk edit page.
	- [PT-1629](https://gd-jira.gaminglabs.com/browse/PT-16)
		- Adds new Help Content component.
	- [LETAUT-1166](https://gd-jira.gaminglabs.com/browse/LETAUT-1166)
		- Adds manual block paytable question and integrations with the blocked paytable note in the Letter Generation interface.
	- [LETAUT-1069](https://gd-jira.gaminglabs.com/browse/LETAUT-1069)
		- Adds the ability to save Product configuration in the Product interface.
	- [LETAUT-1092] (https://gd-jira.gaminglabs.com/browse/LETAUT-1092)
		- Adds new specific cover page with table and logic for West Virginia for use in the Letter Generation interface.

# Fixed
	- [LETAUT-1183](https://gd-jira.gaminglabs.com/browse/LETAUT-1183)
		- Fixes for NV Test Cases and Clauses not pulling in the Letter Generation interface.
	- [LETAUT-1165](https://gd-jira.gaminglabs.com/browse/LETAUT-1165)
		- Fixes for some of the console errors displaying in the Letter Generation interface.
	- [LETAUT-1159](https://gd-jira.gaminglabs.com/browse/LETAUT-1159)
		- Fixes for performance of the math paytable note in the Letter Generation interface.
	- [LETAUT-1197](https://gd-jira.gaminglabs.com/browse/LETAUT-1197)
		- Rhode Island Erroneous Paytable Block.
	- [LETAUT-1194](https://gd-jira.gaminglabs.com/browse/LETAUT-1194)
		- Fixes an issue with the Paytable blocking note for Wisconsin in the Letter Generation interface.

---

## [4.13.3] - 2021-02-26
# Fixed
	- Flat File Transfers mapping options will now display correctly.

## [4.13.2] - 2021-02-25
# Fixed
	- lift all Answer Locks so old files can be tested

## [4.13.1] - 2021-02-25
# Fixed
	- (LETAUT-1167) https://gd-jira.gaminglabs.com/browse/LETAUT-1167
	- Make Evo Link block Finalize instead of Answers

## [4.13.0](https://gd-code.gaminglabs.com/projects/GUS/repos/gli.submissions.web/commits?bb=t&until=4.13.0) - 2021-02-18
[JIRA List 4.13.0](https://gd-jira.gaminglabs.com/issues/?jql=labels%20%3D%204.13.0)
# Added
- [LETAUT-956](https://gd-jira.gaminglabs.com/browse/LETAUT-956)
	- Added Modification type in game description edit and shows previous component in the letter preview.
- [LETAUT-1111](	https://gd-jira.gaminglabs.com/browse/LETAUT-1111)
	- Added a new note for use in the Letter Generation interface for compiled vs witnessed for Nevada.
- [LETAUT-1135](https://gd-jira.gaminglabs.com/browse/LETAUT-1135)
	- Added a new property ProgressiveTopAwards to the math coversheet for use in the Letter Generation interface.
- [LETAUT-1142](https://gd-jira.gaminglabs.com/browse/LETAUT-1142)
	- Added new question type paytable selector for use in the Letter Generation interface.
- [LETAUT-1143](https://gd-jira.gaminglabs.com/browse/LETAUT-1143)
	- Added answer restrictions to the Letter Generation interface that prevents changing answers once the bundle is in QA.
	- Added exception permission to allow QA to change Custom Memos, once the bundle is in QA.
- [LERTAUT-1158](https://gd-jira.gaminglabs.com/browse/LETAUT-1158)
	- Added support for merged bundles to the Description interface.

# Changed
- [LETAUT-1129](https://gd-jira.gaminglabs.com/browse/LETAUT-1129)
	- Changed the Letter Generation interface to not Allow LetterGen if Evo/Protrack are not linked.
- [LETAUT-1144](https://gd-jira.gaminglabs.com/browse/LETAUT-1144)
	- Tweaks for QA LetterGen Workflow
- [LETAUT-1147](https://gd-jira.gaminglabs.com/browse/LETAUT-1147)
	- Changed headers and footers to work better with Panama and Peru in the Letter Generation interface.
- [LETAUT-1153](https://gd-jira.gaminglabs.com/browse/LETAUT-1153)
	- Changed the Letter Generation interface to no longer ask for MaxBet as the math tab should now be used.
- [LETAUT-1161](https://gd-jira.gaminglabs.com/browse/LETAUT-1161)
	- Changed the default view to be the content view in the Letter Generation interface.
- [PT-1597](https://gd-jira.gaminglabs.com/browse/PT-1597)
	- Changed Protrack to not allow project deletion if tasks exist.
- [PT-1613](https://gd-jira.gaminglabs.com/browse/PT-1613)
	- Updated logic for retrieving AppFee data so that billing sheets without a default won't return $0.00.
- [SUB-3615](https://gd-jira.gaminglabs.com/browse/SUB-3615)
	- Jurisdiction billing field changes made in a record will always carry over to all records to prevent projects from being owned by 2 different GLI entities.

# Fixed
- [LETAUT-1149](https://gd-jira.gaminglabs.com/browse/LETAUT-1149)
	- Fixed an issue where the Letter Generation interface would crash if certain jurisdiction default values were not found.
- [LETAUT-1152](https://gd-jira.gaminglabs.com/browse/LETAUT-1152)
	- Fixed an issue with the letter preview where it would not account for selected Language.
- [LETAUT-1154](https://gd-jira.gaminglabs.com/browse/LETAUT-1154)
	- Fixed an issue with math maxbet validation routine of the Letter Generation interface.
- [LETAUT-1157](https://gd-jira.gaminglabs.com/browse/LETAUT-1157)
	- Fixed an issue with the letter generation PDF preview, where the PDF would be cached by the browser and occasionally not update the on demand preview.

# Removed
- [SUB-3619](https://gd-jira.gaminglabs.com/browse/SUB-3619)
	- Removed Could the previously added ability to manually override the file number mapping in all scenarios.

---

## [4.12.0](https://gd-code.gaminglabs.com/projects/GUS/repos/gli.submissions.web/commits?bb=t&until=4.12.0) - 2021-02-18

# Added
- [LETAUT-910](https://gd-jira.gaminglabs.com/browse/LETAUT-910)
	- Adds the ability to modify Platform Piece Definitions and have those changes reflected in templates using those piece definitions in the OCUIT interface.
- [LETAUT-911](https://gd-jira.gaminglabs.com/browse/LETAUT-911)
	-Adds the ability to make modifications to an ASTemplate without starting over entirely in the OCUIT interface.

# Changed
- [LETAUT-1127](https://gd-jira.gaminglabs.com/browse/LETAUT-1127)
	- Changes the Date of Report for the First Page to be a variable to be replaced in Letter Book in the Letter Generation interface.
- [LETAUT-1128](https://gd-jira.gaminglabs.com/browse/LETAUT-1128)
	- Changes the Letterbook interface to Replace Date of Report in the Generated Letter.
- [LETAUT-1124](https://gd-jira.gaminglabs.com/browse/LETAUT-1124)
	- Allows Max Bet to be changed and saved per paytable.
	- Adds Bulk update to allow for changing all paytables max bet at once.
	- Adds informational message to indicate when changes to max bet have not been saved.
	- Restricts finalization step if unsaved max bet changes currently exist.
	- Changes table styling.
- [LETAUT-1122](https://gd-jira.gaminglabs.com/browse/LETAUT-1122)
	- Coverpages added to a new tab.
	- Coverpages stylings updated to match the card styling of the other content items.
- General Changes
	- Changes the IGT Flat File process to display an error if the Project Request Type is invalid.

# Fixed
- [LETAUT-1082] (https://gd-jira.gaminglabs.com/browse/LETAUT-1082)
	- Fixes for Ref/Network Numbers applicable to component/jurisdictions only.
- [LETAUT-1134](https://gd-jira.gaminglabs.com/browse/LETAUT-1134)
	- Fixes typos in the description tab of the Letter Generation interface.
- [LETAUT-1137](https://gd-jira.gaminglabs.com/browse/LETAUT-1137)
	- Fixes Math Exception logic to properly work with decimals.
- [LETAUT-1146](https://gd-jira.gaminglabs.com/browse/LETAUT-1146)
	- Fixes Component List Question type to properly load answers.
- [LETAUT-1058] (https://gd-jira.gaminglabs.com/browse/LETAUT-1058)
	- Fixes to show a warning when jurisdiction does not exist in component while creating a template in the OCUIT interface.
- [LETAUT-1120](https://gd-jira.gaminglabs.com/browse/LETAUT-1120)
	- Fixes an error that would sometimes occur when opening the Letter Generation interface.

- General Letter Generation Fix
	- Fixed a performance issue with the Letter Preview functionality.

---

## [4.11.1](https://gd-code.gaminglabs.com/projects/GUS/repos/gli.submissions.web/commits?bb=t&until=4.11.1) - 2021-02-11
# Fixed
	- Fixes a bug where Flat File does not create Transfer for the special South Africa Jurisditions 49, 552, 553.

---

## [4.11.0](https://gd-code.gaminglabs.com/projects/GUS/repos/gli.submissions.web/commits?bb=t&until=4.11.0) - 2021-02-11

# Added
- [SUB-3608](https://gd-jira.gaminglabs.com/browse/SUB-3608)
	- Adds the ability to edit product information on products edit page.

# Changed
- [LETAUT-1023](https://gd-jira.gaminglabs.com/browse/LETAUT-1023)
	- Changes the Letter Generation interface to carry answers to questions from prime bundles to transfer bundles.
- [LETAUT-1025](https://gd-jira.gaminglabs.com/browse/LETAUT-1025)
	- Changes the Technical Standards Lookup interface to match the Legacy Standards.
- [LETAUT-1094](https://gd-jira.gaminglabs.com/browse/LETAUT-1094)
	- Changes the Letter Generation interface first page reference number section to display reference / network numbers for merged bundles.
- [LETAUT-1095](https://gd-jira.gaminglabs.com/browse/LETAUT-1095)
	- Changes the Letter Generation interface first page file number section to display file numbers for merged bundles.
- [LETAUT-1097](https://gd-jira.gaminglabs.com/browse/LETAUT-1097)
	- Changes the Game description tab of the Letter Generation Interface to now read "Component / Mod Description".
- [SUB-3587](https://gd-jira.gaminglabs.com/browse/SUB-3587)
	- Changes the Bulk Update feature to include additional fields for ARI.
- [SUB-3619](https://gd-jira.gaminglabs.com/browse/SUB-3619)
	- Changes the IGT Flat File import to support file number mapping override at all times.

- General Letter Generation interface changes
	- Changed the Letter Generation interface to remove the approximation letter preview, and replace it with a 'content' view and in browser PDF view.

# Fixed
- [LETAUT-1071](https://gd-jira.gaminglabs.com/browse/LETAUT-1071)
	- Fixes the Custom Memo feature of the Letter Generation interface to properly anchor and interface with the new 'content' view.
- [LETAUT-1073](https://gd-jira.gaminglabs.com/browse/LETAUT-1073)
	- Fixes an issue in the Letter Generation interface where math exception information was not working properly for brand new bundles.
- [LETAUT-1088](https://gd-jira.gaminglabs.com/browse/LETAUT-1088)
	- Fixes Rhode Island denom issue in the letter generation interface.
- [LETAUT-1110](https://gd-jira.gaminglabs.com/browse/LETAUT-1110)
	- Fixes an issue in Letter Generation where Pennsylvania ITL and Michigan ITL uses RTP without increment rate.
- [LETAUT-1112](https://gd-jira.gaminglabs.com/browse/LETAUT-1112)
	- Fixes an issue in the Letter Generation interface where previously, un-splitting a split question would cause answers to not save.
- [LETAUT-1133](https://gd-jira.gaminglabs.com/browse/LETAUT-1133)
	- Fixes an issue with the Description interface that would previously redirect to an incorrect url in the deployed environment.

# Removed
- [LETAUT-1093](https://gd-jira.gaminglabs.com/browse/LETAUT-1093)
	- Removes the "Standards of Compliance" section for the Nevada First Page in the Letter Generation interface.

---

## [4.10.1](https://gd-code.gaminglabs.com/projects/GUS/repos/gli.submissions.web/commits?bb=t&until=4.10.1) - 2021-02-08

# Fixed
	- Fixes an issue in Flat File mapping where inactive mappings were being used.

---

## [4.10.0](https://gd-code.gaminglabs.com/projects/GUS/repos/gli.submissions.web/commits?bb=t&until=4.10.0) - 2021-02-04

# Added
- [LETAUT-1072](https://gd-jira.gaminglabs.com/browse/LETAUT-1072)
	- Adds support for marking a question as required or not in notes manager, question manager, and the Letter Generation interface.

# Changed
- [ACUM-69](https://gd-jira.gaminglabs.com/browse/ACUM-69)
	- Changes Acumatica Rules Engine to support new IGT Billing Party Manchester  (99000IGT00140).
- [LETAUT-989](https://gd-jira.gaminglabs.com/browse/LETAUT-989)
	- Changes the Math table of the Letter Generation interface to only show the Weighted Average column for DE and MD.
- [LETAUT-1023](https://gd-jira.gaminglabs.com/browse/LETAUT-1023)
	- Adds logic to better interact with the IGT Flat File when importing answers form transfers.
-[LETAUT-1093](https://gd-jira.gaminglabs.com/browse/LETAUT-1093)
	- Changes to remove the standards section of the US First Page for Nevada in the Letter Generation interface.

# Fixed
- [LETAUT-1056](https://gd-jira.gaminglabs.com/browse/LETAUT-1056)
	- Fixes a bug with the Game Description interface that previously would redirect to an incorrect path after saving.
- [PT-1601](https://gd-jira.gaminglabs.com/browse/PT-1601)
	- Fixes an issue with Letterbook Re-Drafts for Pennsylvania Interactive.

---

## [4.9.0](https://gd-code.gaminglabs.com/projects/GUS/repos/gli.submissions.web/commits?bb=t&until=4.9.0) - 2021-02-01

### Added
- [SUB-3586](https://gd-jira.gaminglabs.com/browse/SUB-3586)
	- Added Aristocrat fields to the Submissions Search

### Changed
- [LETAUT-1059](https://gd-jira.gaminglabs.com/browse/LETAUT-1059)
	- Changes letter gen to use the new percentage format.
	- Checks skill/non-skill if no game type exist in Gaming Guidelines.

### Fixed
- [LETAUT-959](https://gd-jira.gaminglabs.com/browse/LETAUT-959)
	- Fixed an issue with NV Evaulated Clauses showing as Failed instaed of Pass*.
	- Fixed a bug where new Submissionc could not be created if the Contract Number was more than 50 characters.

---

## [4.8.0](https://gd-code.gaminglabs.com/projects/GUS/repos/gli.submissions.web/commits?bb=t&until=4.8.0) - 2021-01-28

### Added
- [GAN-589](https://gd-jira.gaminglabs.com/browse/GAN-589)
	- Adds testing type to the Manufacturer Lookup, and Add iGaming fields to PCS

### Changed
- [LETAUT-1002](https://gd-jira.gaminglabs.com/browse/LETAUT-1002)
	- Changes notes related to Maryland and Delaware math to use CI90 values.

### Fixed
- [PT-1617](https://gd-jira.gaminglabs.com/browse/PT-1617)
	- Fixed a bug on the create document interface that would previously break on the sub type code.

---

## [4.7.1](https://gd-code.gaminglabs.com/projects/GUS/repos/gli.submissions.web/commits?bb=t&until=4.7.1) - 2021-01-27
	- Fixes bug with Acumatica manual push interface regarding FP projects failing when attempting to reference themselves.

---

## [4.7.0](https://gd-code.gaminglabs.com/projects/GUS/repos/gli.submissions.web/commits?bb=t&until=4.7.0) - 2021-01-26

### Letter Generation
- [LETAUT-1026](https://gd-jira.gaminglabs.com/browse/LETAUT-1026)
	- Fixes blocked paytable rounding issue in the letter generation interface.

- [LETAUT-1049](https://gd-jira.gaminglabs.com/browse/LETAUT-1049)
	- Added the ablity to note items tested in parallel.

- [LETAUT-1018](https://gd-jira.gaminglabs.com/browse/LETAUT-1018)
	- Added error text for items failing Neveda clauses.

# Changed
- Alpha 5x signatures will now have the Alpha Version field available.

# Fixed
- Fixed an error where letter book defaulting rules without a biling sheet could not be edited.

---

## [4.6.0](https://gd-code.gaminglabs.com/projects/GUS/repos/gli.submissions.web/commits?bb=t&until=4.6.0) - 2021-01-25

### Acumatica
-[ACUM-45](https://gd-jira.gaminglabs.com/browse/ACUM-45)
- Changes made to the Currency, Contract Type, Company, and Billing Party fields in Submissions will automatically be carried over to Acumatica.

---

## [4.5.2](https://gd-code.gaminglabs.com/projects/GUS/repos/gli.submissions.web/commits?bb=t&until=4.5.2) - 2021-01-21

# Fixed
- Fixed an error where bundles in EN would not open in the Letter Generation interface.

---

## [4.5.1](https://gd-code.gaminglabs.com/projects/GUS/repos/gli.submissions.web/commits?bb=t&until=4.5.1) - 2021-01-21

# Fixed
- Fixed an error where Submissions could not be edited.

## [4.5.0](https://gd-code.gaminglabs.com/projects/GUS/repos/gli.submissions.web/commits?bb=t&until=4.5.0) - 2021-01-21

# Added

### Letter Generation
- [LETAUT-924](https://gd-jira.gaminglabs.com/browse/LETAUT-924)
	- Adds support for Panama and Peru First and Second Page templates respectively in the Letter Generation interface.
- [LETAUT-939](https://gd-jira.gaminglabs.com/browse/LETAUT-939)
	- Adds the ability to set a default answer for questions in letter question manager.

### Protrack
- [PT-1547](https://gd-jira.gaminglabs.com/browse/PT-1547)
	- Adds a select / deselect All checkbox to the Evo configuration, jurisdiction selection section of the Incoming Review Interface.

# Changed

### Submissions
- [SUB-3599](https://gd-jira.gaminglabs.com/browse/SUB-3599)
	- Changes the Submission search so that it can be filtered on jurisdiction.
	- Changes the Submission search to return company in the result set.

### Letter Generation
- [LETAUT-940](https://gd-jira.gaminglabs.com/browse/LETAUT-940)
	- Changes the Game Description tab of the Letter Generation interface to link to the game description maintenance page.
	- Adds a brief overview of how to use the description maintenance page found at the following location:
		- [Help Page](http://submissions/gliintranet/lookups/help/edit/9?mode=Edit)
	- Removes the Modification list tab from the Letter Generation Interface.
- [LETAUT-955](https://gd-jira.gaminglabs.com/browse/LETAUT-955)
	- Changes OCUIT Templates to only allow Approved Components.
- [LETAUT-957](https://gd-jira.gaminglabs.com/browse/LETAUT-957)
	- Changes the Letter Generation interface to display merged components.
	- Changes the Letter Generation interface to display an indication that merged projects exist in the bundle.
- [LETAUT-959](https://gd-jira.gaminglabs.com/browse/LETAUT-959)
	- Changes the Evaluated Clauses list for Nevada to display PASS* instead of FAIL when applicable.
- [LETAUT-962](https://gd-jira.gaminglabs.com/browse/LETAUT-962)
	- Changes Description field to be editable in OCUIT.

### General Changes
- Changes to better facilitate testing the FlatFile interface.

# Removed

### Letter Generation
- [LETAUT-961](https://gd-jira.gaminglabs.com/browse/LETAUT-961)
	- Removes the "Select Memo" action from the right click context menu of the letter preview in the Letter Generation interface.

# Fixed

### Letter Generation
- [LETAUT-934](https://gd-jira.gaminglabs.com/browse/LETAUT-934)
	- Fixes an issue with a query that would previously bring in memos for unrelated jurisdictions in the Letter Generation interface.
- [LETAUT-954](https://gd-jira.gaminglabs.com/browse/LETAUT-954)
	- Fixes a bug that previously would cause the OCUIT interface to crash when opened from the Letter Generation Interface for a project that had not previously set up an OCUIT template.
- [LETAUT-976](https://gd-jira.gaminglabs.com/browse/LETAUT-976)
	- Fixes a bug that previously would cause the Letter Generation interface to crash when null was encountered as the answer to a multi value answer.
- [LETAUT-978](https://gd-jira.gaminglabs.com/browse/LETAUT-978)
	- Fixes a bug that caused the letter generation lookup-multiple to not save.
- [LETAUT-979](https://gd-jira.gaminglabs.com/browse/LETAUT-979)
	- Fixes a bug that caused the letter generation lookup to not save and load.
- [LETAUT-982](https://gd-jira.gaminglabs.com/browse/LETAUT-982)
	- Fixes a bug that previously would pull the incorrect start date for Nevada. The Nevada start date now always will come from the main bundle.
- [LETAUT-991](https://gd-jira.gaminglabs.com/browse/LETAUT-991)
	- Fixes for the DE and MN blocked pay table note.
- [LETAUT-1007](https://gd-jira.gaminglabs.com/browse/LETAUT-1007)
	- Fixes a bug in the description management interface that would restrict the use of the jurisdiction association picker if description field grew too large.

---

## [4.4.0](https://gd-code.gaminglabs.com/projects/GUS/repos/gli.submissions.web/commits?bb=t&until=4.4.0) - 2021-01-13

### Changed
- Changed the styles for the math pay table memo, and increased the width of the table to take up less vertical space. This should help with the paging of table memos in the Letter Generation interface.

### Fixed
- [LETAUT-934](https://gd-jira.gaminglabs.com/browse/LETAUT-934)
	- Fixes issue that previously would display memos for negated jurisdictions in letter generation bundles that did not contain the jurisdiction.
- [LETAUT-938](https://gd-jira.gaminglabs.com/browse/LETAUT-938)
	- Fixes issue that previously would cause the Letter Generation Interface to fail when Evolution validations were not linked to memos.

---

## [4.3.0](https://gd-code.gaminglabs.com/projects/GUS/repos/gli.submissions.web/commits?bb=t&until=4.3.0) - 2021-01-11

### Changed
- Changed the styles for the math pay table memo, and increased the width of the table to take up less vertical space. This should help with the paging of table memos in the Letter Generation interface.

### Fixed
- [LETAUT-934](https://gd-jira.gaminglabs.com/browse/LETAUT-934)
	- Fixes issue that previously would display memos for negated jurisdictions in letter generation bundles that did not contain the jurisdiction.
- [LETAUT-938](https://gd-jira.gaminglabs.com/browse/LETAUT-938)
	- Fixes issue that previously would cause the Letter Generation Interface to fail when Evolution validations were not linked to memos.

---

## [4.3.0](https://gd-code.gaminglabs.com/projects/GUS/repos/gli.submissions.web/commits?bb=t&until=4.3.0) - 2021-01-11

### Fixed
- Fixed bug that caused the letter generation instance to crash.

---

## [4.2.0](https://gd-code.gaminglabs.com/projects/GUS/repos/gli.submissions.web/commits?bb=t&until=4.2.0) - 2021-01-11

### Fixed
- Fixed bug that caused the letter generation instance to crash.

---

## [4.1.0](https://gd-code.gaminglabs.com/projects/GUS/repos/gli.submissions.web/commits?bb=t&until=4.1.0) - 2021-01-11

### Changed
- Changes to add standards information to the Letter Generation interface.
- Changes to add a link to the Description interface, in the tools section.

### Fixed
- [LETAUT-931](https://gd-jira.gaminglabs.com/browse/LETAUT-931)
	- Fixed a bug that would cause some bundles to not load properly.

---

## [4.0.1](https://gd-code.gaminglabs.com/projects/GUS/repos/gli.submissions.web/commits?bb=t&until=4.0.1) - 2021-01-11

### Fixed
- [SUB-3601](https://gd-jira.gaminglabs.com/browse/SUB-3601)
	- Kansas City has a different process for Revokes and needs to be able to select a DR jurisdiction for a revoke.  This is going out as a temporary fix.
The issue has been sent to the Process Committee so they can decide on what the special work flow for Kansas City should be.

---
## [4.0.0](https://gd-code.gaminglabs.com/projects/GUS/repos/gli.submissions.web/commits?bb=t&until=4.0.0) - 2021-01-08

### Added
- [LETAUT-917](https://gd-jira.gaminglabs.com/browse/LETAUT-917)
	- Adds new Description Interface, to allow users to maintain various descriptions, i.e. (game, modification, etc.)

### Changed
- [LETAUT-874](https://gd-jira.gaminglabs.com/browse/LETAUT-874)
	- Changes the Letter Generation Interface to be compatible with the OCUIT data source.
- [LETAUT-882](https://gd-jira.gaminglabs.com/browse/LETAUT-882)
	- Changes the Letter Generation Interface to require that all question have been answered prior to allowing for generation and finalization. (This feature is currently disabled, until some additional requirements are satisfied.)
- [LETAUT-919](https://gd-jira.gaminglabs.com/browse/LETAUT-919)
	- Changes to the way pay table blocking works in the Letter Generation Interface. Adds the ability for the engineer to provide an override.

### Fixed
- [LETAUT-901](https://gd-jira.gaminglabs.com/browse/LETAUT-901)
	- Fixed a bug that would cause letter content that was larger than a letter page to not properly split in the Letter Generation Interface.
- [LETAUT-904](https://gd-jira.gaminglabs.com/browse/LETAUT-904)
	- Fixed an issue where all verification procedures are shown in Letter Generation preview. In addition, updated platforms dropdown to show group category code.
- General Fixes
	- Fixed a bug that would previously cause the Manufacturer Edit Interface to error out when the Default QA Office was changed.
	- Fixed a bug with the Top navigation bar that had malformed markup.

---
## [3.17.0](https://gd-code.gaminglabs.com/projects/GUS/repos/gli.submissions.web/commits?bb=t&until=3.17.0) - 2020-12-30

### Changed
- [SUB-3580](https://gd-jira.gaminglabs.com/browse/SUB-3580)
	- Display the Product Category after users upload it.
- In Letter Generate set the default table text alignment to left

### Fixed
- [SUB-3547](https://gd-jira.gaminglabs.com/browse/SUB-3547)
	- Fix an issue where Flat File tries to add South Africa National when it wasn't requested.
- [PT-1599](https://gd-jira.gaminglabs.com/browse/PT-1599)
	- Fixed an issue where On Hold emails would not go out if a projet lead was not added to a project.

---
## [3.16.0](https://gd-code.gaminglabs.com/projects/GUS/repos/gli.submissions.web/commits?bb=t&until=3.16.0) - 2020-12-22

### Added
- [SUB-3546](https://gd-jira.gaminglabs.com/browse/SUB-3546)
	- Users and Email Addresses for Email Distributions are now tracked in the History Log.  The ability to search this has been added to the History Log Report.
- [SUB-3580](https://gd-jira.gaminglabs.com/browse/SUB-3580)
	- Began storing in the database the Product Category from the Flat File.  We will be adding a view for that next.

### Changed
- [LETAUT-909](https://gd-jira.gaminglabs.com/browse/LETAUT-909)
	- Added Jurisdictions to the Associated Software page to make it easier for users to find what they need.

### Fixed
- [PT-1495](https://gd-jira.gaminglabs.com/browse/PT-1495)
	- When saving Target Dates in Incoming Review ask users if they wish to apply the Target Date Offsets.

- Fixed an issue in Flat File Upload where the selected primary jurisdiction was not used.
---

## [3.15.0](https://gd-code.gaminglabs.com/projects/GUS/repos/gli.submissions.web/commits?bb=t&until=3.15.0) - 2020-12-16

### Added
- [LETAUT-798](https://gd-jira.gaminglabs.com/browse/LETAUT-798)
	- Adds a clear button to the Notes Manager search interface.
- [LETAUT-865](https://gd-jira.gaminglabs.com/browse/LETAUT-865)
	- Adds Submitted, Start, and End Dates to the Letter Generation interface.

### Changed
- [LETAUT-903](https://gd-jira.gaminglabs.com/browse/LETAUT-903)
	- Changes the OCUIT to allow an engineer to be able to mark which components are being listed for continuity purposes only.

### Fixed
- Fixed a bug in Notes Manager that previously would cause a memo to version improperly after the first edit.

---

## [3.14.5](https://gd-code.gaminglabs.com/projects/GUS/repos/gli.submissions.web/commits?bb=t&until=3.14.5) - 2020-12-11
### Fixed
- [PT-1593](https://gd-jira.gaminglabs.com/browse/PT-1593)
- Prevent no longer used Dynamics tasks from showing up in the incoming review task drop down.

---

## [3.14.4](https://gd-code.gaminglabs.com/projects/GUS/repos/gli.submissions.web/commits?bb=t&until=3.14.4) - 2020-12-10
### Fixed
- [LETAUT-902](https://gd-jira.gaminglabs.com/browse/LETAUT-902)
	- Fixed a bug that would prevent users from saving platform pieces in the OCUIT.
---

---
## [3.14.3](https://gd-code.gaminglabs.com/projects/GUS/repos/gli.submissions.web/commits?bb=t&until=3.14.3) - 2020-12-10
### Fixed
- Updated permissions for the Platform Piece Definitions and Associated Software Templates lookups from Admin permission to LetterGeneration permission.

---
## [3.14.2](https://gd-code.gaminglabs.com/projects/GUS/repos/gli.submissions.web/commits?bb=t&until=3.14.2) - 2020-12-10
### Fixed
- [SUB-3593](https://gd-jira.gaminglabs.com/browse/SUB-3593)
	- Sometimes GPS Uploads would time out because of Evo data.  This fix should prevent that from happening.

---

## [3.14.1](https://gd-code.gaminglabs.com/projects/GUS/repos/gli.submissions.web/commits?bb=t&until=3.14.1) - 2020-12-10
### Fixed
	- Fix loading/searching issue in Notes Manager.

---

## [3.14.0](https://gd-code.gaminglabs.com/projects/GUS/repos/gli.submissions.web/commits?bb=t&until=3.14.0) - 2020-12-10

### Added
- [LETAUT-836](https://gd-jira.gaminglabs.com/browse/LETAUT-836)
	- Adds new Manufacturer Guidelines Interface, for use with providing manufacturer focuses business rules.

### Changed
- [LETAUT-777](https://gd-jira.gaminglabs.com/browse/LETAUT-777)
	- Changed the components tab of the Letter Generation interface to now include validations for IDnumber, Version, Date Code and Game Name.
	- Changed the components tab of the Letter Generation interface workspace to not allow editing when the project status is RC.
- [LETAUT-837](https://gd-jira.gaminglabs.com/browse/LETAUT-837)
	- Changed the OCUIT interface to allow for platform piece configuration and multiple jurisdiction assignment for templates.
- [LETAUT-868](https://gd-jira.gaminglabs.com/browse/LETAUT-868)
	- Changed the Letter Generation Interface to limit the applicable jurisdiction set of memos to be only inclusive of relevant jurisdictions of the current bundle instead of all jurisdictions in the submissions DB.
- Change to Letter Generation interface to display currently hidden memos, in the workspace, so that the guard question which reveals or hides the memo could be answered.
- Change to Letter Generation interface to display question last save information on page load for questions with a prior answer.

### Fixed
- [LETAUT-758](https://gd-jira.gaminglabs.com/browse/LETAUT-758)
	- Fixed an issue with the Letter Generation interface jurisdiction picker where the order was not alphabetical.
- [LETAUT-858](https://gd-jira.gaminglabs.com/browse/LETAUT-858)
	- Fixed an issue with Letter Generation interface jurisdiction picker where occasionally it would be blank on page load.
	- Fixed an issue with Letter Generation interface page numbering where occasionally it would display as infinity.
- [SUB-3544](https://gd-jira.gaminglabs.com/browse/SUB-3544)
	- Fixed an issue with the IGT Flat File process that would occur when IGT Cruise Lines jurisdiction and South Africa Jurisdiction would overlap for mapping.
- [SUB-3488](https://gd-jira.gaminglabs.com/browse/SUB-3488)
	- Fixed an issue with the Submission Upload From Excel interface where data sometimes gets truncated when uploading submissions information.
- [LETAUT-857](https://gd-jira.gaminglabs.com/browse/LETAUT-857)
	- Fixed an issue with the Test Cases / evaluated clauses data sources that would cause them to not render properly in memos in some cases.

---

## [3.13.2](https://gd-code.gaminglabs.com/projects/GUS/repos/gli.submissions.web/commits?bb=t&until=3.13.1) - 2020-12-08
### Changed
	- Gave access to Event Notification Setting to QA.

---

## [3.13.1](https://gd-code.gaminglabs.com/projects/GUS/repos/gli.submissions.web/commits?bb=t&until=3.13.1) - 2020-12-03
### Fixed
	- Fix null reference issue in letter generation.

---

## [3.13.0](https://gd-code.gaminglabs.com/projects/GUS/repos/gli.submissions.web/commits?bb=t&until=3.13.0) - 2020-12-03
### Changed
- [LETAUT-759](https://gd-jira.gaminglabs.com/browse/LETAUT-759)
	- Changes the workspace area Math table of the Letter Generation interface to show more fields.
- [NM-34](https://gd-jira.gaminglabs.com/browse/NM-34)
	- Changed the look of the user event notification subscription page.
	- Updated the way event subscriptions are added, edited, and deleted to improve user experience.

### Fixed
- [LETAUT-762](https://gd-jira.gaminglabs.com/browse/LETAUT-762)
	- Fixes location issue for Headers in Letter Generation. Only use current filenumber and exclude master filenumbers.
- [SUB-3590](https://gd-jira.gaminglabs.com/browse/SUB-3590)
	- Fixes ordering of column data, specifically Game Name in the copy to submissions interface.
- General fixes for the Letter Generation question and answer loading scenarios.

---

## [3.12.2](https://gd-code.gaminglabs.com/projects/GUS/repos/gli.submissions.web/commits?bb=t&until=3.12.2) - 2020-12-03
### Fixed
- For Flat File offer Submissions in the TC status as potential mappings for Transfers.


## [3.12.1](https://gd-code.gaminglabs.com/projects/GUS/repos/gli.submissions.web/commits?bb=t&until=3.12.1) - 2020-12-02
### Fixed
- [SUB-3589](https://gd-jira.gaminglabs.com/browse/SUB-3589)
	- For Flat File do not automap to files that are WD or RJ.

---

## [3.12.0](https://gd-code.gaminglabs.com/projects/GUS/repos/gli.submissions.web/commits?bb=t&until=3.12.0) - 2020-11-30
### Fixed
- [LETAUT-833](https://gd-jira.gaminglabs.com/browse/LETAUT-833)
	- Fixes issue that would leave a blank page when a section contained no relevant memos.

### Changed
- [LETAUT-821](https://gd-code.gaminglabs.com/projects/GUS/repos/gli.submissions.web/pull-requests/182/overview)
	- Changes content to be justified and removes extra white space in the Letter Generation interface.

---

## [3.11.0](https://gd-code.gaminglabs.com/projects/GUS/repos/gli.submissions.web/commits?bb=t&until=3.11.0) - 2020-11-24
### Added
[PT-1576](https://gd-jira.gaminglabs.com/browse/PT-1576)
	- Allow to export batchnumber without removing and readding
[LETAUT-787](https://gd-jira.gaminglabs.com/browse/LETAUT-787)
	- Allow Letterbook to use "LetterGenSourceDocument" and "LetterGenSourcePDF" document types as source documents.  Require the user to enter a reason why they selected the Word document and not the PDF document as a source.

### Fixed
[SUB-3581](https://gd-jira.gaminglabs.com/browse/SUB-3581)
	- Remove PCT emails due to errors

- History Log has an issue where the report does not work for tables with Guids for primary keys.

---

## [3.10.1](https://gd-code.gaminglabs.com/projects/GUS/repos/gli.submissions.web/commits?bb=t&until=3.10.1) - 2020-11-24
### Fixed
- Fixed a bug when uploading Checksum signatures with a spreadsheet.


## [3.10.0](https://gd-code.gaminglabs.com/projects/GUS/repos/gli.submissions.web/commits?bb=t&until=3.10.0) - 2020-11-23
### Added
- [LETAUT-761](https://gd-jira.gaminglabs.com/browse/LETAUT-761)
	- Adds ability to format decimals as percentages for use with Notes Manager and Letter Generation Interfaces.
- [LETAUT-826](https://gd-jira.gaminglabs.com/browse/LETAUT-826)
	- Adds a link from the Letter Generation interface, to the OCUIT interface.
- [NM-22](https://gd-jira.gaminglabs.com/browse/NM-22)
	- Adds ability to the Notification Monitor interface to import and copy all the subscriptions of another user so that if someone is out, there notifications can be caught.

### Changed
- [LETAUT-720](https://gd-jira.gaminglabs.com/browse/LETAUT-720)
	- Changes to question answering for the Letter Generation interface.
- [LETAUT-820](https://gd-jira.gaminglabs.com/browse/LETAUT-820)
	- Changes to footer used in Letter Generation, and addition of footer to US first page.
- [PT-1475](https://gd-jira.gaminglabs.com/browse/PT-1475)
	- Changes the top navigation bar to provide access to Notification Monitor to the following GLI offices: CO, LV, EU.

### Fixed
- [LETAUT-831](https://gd-jira.gaminglabs.com/browse/LETAUT-831)
	- Fix for error that would occur with jurisdiction picker in the Letter Generation interface when a jurisdiction ID was a single digit.
- [PT-1496](https://gd-jira.gaminglabs.com/browse/PT-1496)
	- Fix for transfer process emails being duplicated and users receiving multiple emails.

---

## [3.9.1](https://gd-code.gaminglabs.com/projects/GUS/repos/gli.submissions.web/commits?bb=t&until=3.9.1) - 2020-11-19
### Changed
- Temporarily disable the department lock in the Letter Generation interface.

---

## [3.9.0](https://gd-code.gaminglabs.com/projects/GUS/repos/gli.submissions.web/commits?bb=t&until=3.9.0) - 2020-11-19
### Added
- [LETAUT-632](https://gd-jira.gaminglabs.com/browse/LETAUT-632)
	- Adds the ability to delete a question from the Question Manager interface.
- [LETAUT-730](https://gd-jira.gaminglabs.com/browse/LETAUT-730)
	- Adds Letter Generation Department Lock, when the department lock is engaged a Letter Generation user is unable to make changes. The Lock will be engaged when a user is not a member of the department that currently holds ownership of the project.
- [LETAUT-733](https://gd-jira.gaminglabs.com/browse/LETAUT-733)
	- Adds the ability to link a note in Notes Manager to an Evolution Conditional.
- [LETAUT-763](https://gd-jira.gaminglabs.com/browse/LETAUT-763)
	- Adds new data source for use in Notes Manager and Letter Generation interfaces, modification list.
- [LETAUT-801](https://gd-jira.gaminglabs.com/browse/LETAUT-801)
	- Adds paging controls to help correct spacing issues in the letter preview of the Letter Generation interface.

### Changed
- [LETAUT-731](https://gd-jira.gaminglabs.com/browse/LETAUT-731)
	- Changes the way answers to questions are saved from the Letter Generation interface.
- [LETAUT-762](https://gd-jira.gaminglabs.com/browse/LETAUT-762)
	- Remove GLI File number / location from Page 1 and add to the header from page 2+
- [LETAUT-822](https://gd-jira.gaminglabs.com/browse/LETAUT-822)
	- Changes to the Letter Generation US Page 1 to remove letter lab codes, the phone number, and the legalese at the bottom of page 1.
- [LETAUT-823](https://gd-jira.gaminglabs.com/browse/LETAUT-823)
	- Changes to the Letter Generation OCUIT to aide in searching and comparing components.
- [SUB-3577](https://gd-jira.gaminglabs.com/browse/SUB-3577)
	- Changes the associated jurisdiction from Ontario to Ontario Gaming for Aristocrat.

### Fixed
- [LETAUT-736](https://gd-jira.gaminglabs.com/browse/LETAUT-736)
	- Fix for the Letter Generation interface to not pull in Withdrawn or Rejected submissions.

---

## [3.8.1](https://gd-code.gaminglabs.com/projects/GUS/repos/gli.submissions.web/commits?bb=t&until=3.8.1) - 2020-11-17
### Added
- [LETAUT-727](https://gd-jira.gaminglabs.com/browse/LETAUT-727)
	- Adds volatility charts to Notes Manager and Letter Gen.

## [3.8.0](https://gd-code.gaminglabs.com/projects/GUS/repos/gli.submissions.web/commits?bb=t&until=3.8.0) - 2020-11-16
### Added
- [LETAUT-642](https://gd-jira.gaminglabs.com/browse/LETAUT-642)
	- Adds new editor tool bar option to wrap a note in a radio button question for the Notes Manager interface.
- [LETAUT-647](https://gd-jira.gaminglabs.com/browse/LETAUT-647)
	- Adds ability to search by note ID in notes manager.
- [LETAUT-706](https://gd-jira.gaminglabs.com/browse/LETAUT-706)
	- Adds ability to launch the Letter Generation interface in clause report mode from the project detail action menu.
- [LETAUT-718](https://gd-jira.gaminglabs.com/browse/LETAUT-718)
	- Adds support for Nevada and Peru clause reports as a data source in Notes Manager and Letter Generation interfaces.
- [LETAUT-723](https://gd-jira.gaminglabs.com/browse/LETAUT-723)
	- Adds secondary language support for custom memos in the Letter Generation interface.

### Changed
- [LETAUT-713](https://gd-jira.gaminglabs.com/browse/LETAUT-713)
	- Changes to support the generic pay tables out of range math note used in Letter Generation.
- [LETAUT-721](https://gd-jira.gaminglabs.com/browse/LETAUT-721)
	- Changes the Letter Generation finalization step to also handle secondary language support for jurisdictions requiring letters in secondary language.
- [LETAUT-731](https://gd-jira.gaminglabs.com/browse/LETAUT-731)
	- Changes to answer saving in Letter Generation Interface, to better support bundle splitting.
- [SUB-3551](https://gd-jira.gaminglabs.com/browse/SUB-3551)
	- Changes the IGT Import interface to exclude RJ files from the potential transfer resolution list.

### Fixed
- [LETAUT-726](https://gd-jira.gaminglabs.com/browse/LETAUT-726)
	- Fix for incorrect ordering observed in the Letter Generation interface.
- [PT-1559](https://gd-jira.gaminglabs.com/browse/PT-1559)
	- Fix to pass the selected jurisdictions to filter the documents for the Evolution Clause Report.
- Fixes a bug in the IGT Flat file interface to use a non-sports jurisdiction mapping when the jurisdiction does not have a respective sports jurisdiction
- Fixes to properly display not relevant jurisdiction and manufacturer choices in the memo more information popups in the Letter Generation interface.

---

## [3.7.1](https://gd-code.gaminglabs.com/projects/GUS/repos/gli.submissions.web/commits?bb=t&until=3.7.1) - 2020-11-10
### Fixed
- If a Flat File comes in for Sports Book, but a Sports Book jurisdiction mapping does not exist, use the existing jurisdiction mapping.

---

## [3.7.0](https://gd-code.gaminglabs.com/projects/GUS/repos/gli.submissions.web/commits?bb=t&until=3.7.0) - 2020-11-09
### Added
- [GAN-558](https://gd-jira.gaminglabs.com/browse/GAN-558)
	- Adds support for Delayed Certifications.
- [LETAUT-617](https://gd-jira.gaminglabs.com/browse/LETAUT-617)
	- Adds functionality to Letter Generation to automatically upload the resultant PDF created by the Finalization process to the respective GPS location.
- [LETAUT-650](https://gd-jira.gaminglabs.com/browse/LETAUT-650)
	- Adds Section Jump functionality to the Letter Generation Interface, when selecting a section form the quick navigation menu both the workspace and the 	preview will scroll the relevant section into view.
- [LETAUT-658](https://gd-jira.gaminglabs.com/browse/LETAUT-658)
	- Adds functionality to scroll the Letter Preview to the relevant memo when answering a question in the workspace of the Letter Generation Interface.
- [LETAUT-678](https://gd-jira.gaminglabs.com/browse/LETAUT-678)
	- Adds interface to manage Other Components Used In Testing to support the Letter Generation process.
- [LETAUT-707](https://gd-jira.gaminglabs.com/browse/LETAUT-707)
	- Adds a link from Digital Letter Book to Letter Generation.
- [LETAUT-710](https://gd-jira.gaminglabs.com/browse/LETAUT-710)
	- Adds support for secondary language in the Letter Generation Preview for Panama, Peru, and Chile.
- [LETAUT-713](https://gd-jira.gaminglabs.com/browse/LETAUT-713)
	- Adds Gaming Guidelines as a data source available to Notes Manager and Letter Generation Interfaces.
- [LETAUT-714](https://gd-jira.gaminglabs.com/browse/LETAUT-714)
	- Adds ability to save modification lists and game descriptions from the Letter Generation Interface.

### Changed
- [PT-1500](https://gd-jira.gaminglabs.com/browse/PT-1500)
	- Changes date format of task target date in QA Advance search to resolve a type mis match error.
- [SUB-3545](https://gd-jira.gaminglabs.com/browse/SUB-3545)
	- Change to omit WD or RJ submissions from the ambiguous Transfer Resolution dropdown in the Flat File Interface.
- [SUB-3551](https://gd-jira.gaminglabs.com/browse/SUB-3551)
	- Change to utilize date provided by IGT on their Flat Files as the Submitted and Received Dates for submissions created by this process.
- Changed data sources in Notes Manager and Letter Generation Interfaces to now include Chip type, Function, and Position on the component level.
- Changed Letter Generation Interface to add liens to tables in both preview and PDF views.
- Changed IGT Flat file interface to now check if the relevant sports jurisdiction should be used.
- Changed IGT Flat file interface to utilize South Africa mapping.

### Fixed
- [LETAUT-611](https://gd-jira.gaminglabs.com/browse/LETAUT-611)
	- Fix for multi-select question types in Letter Generation workspace, previously answering the question would display a list of the index values as the answer, now the text should properly display as the answer to the question in the letter preview.
- [LETAUT-612](https://gd-jira.gaminglabs.com/browse/LETAUT-612)
	- Fixes Letter Generation Preview so that it does not grow in width past the default page width when its content could push the width.
- [LETAUT-711](https://gd-jira.gaminglabs.com/browse/LETAUT-711)
	- Fixes to support all available question types in Letter Generation Workspace.
- [LETAUT-716](https://gd-jira.gaminglabs.com/browse/LETAUT-716)
	- Fixes Letter Generation issue that previously would show memos that were not relevant to the current letter.
- [LETAUT-717](https://gd-jira.gaminglabs.com/browse/LETAUT-717)
	- Fixes the More Info Modal dialog in Letter Generation to properly display associated jurisdictions.
- [PT-1548](https://gd-jira.gaminglabs.com/browse/PT-1548)
	- Fixes a bug in Evolution Incoming Review that prevented removing specific components from component groups.
- [SUB-3549](https://gd-jira.gaminglabs.com/browse/SUB-3549)
	- Fix for submission edit page to address an issue that prevented data entry operators from loading the page.
- Fixes the US first page in Letter Generation Interface to no longer show errant HTML tags in the submitting party field.

---

## [3.6.2](https://gd-code.gaminglabs.com/projects/GUS/repos/gli.submissions.web/commits?bb=t&until=3.6.2) - 2020-11-04
### Fixed
- [PT-1555](https://gd-jira.gaminglabs.com/browse/PT-1555)
	- Shows all active documents in Evo Incoming regardless of the selection made by a user.

## [3.6.1](https://gd-code.gaminglabs.com/projects/GUS/repos/gli.submissions.web/commits?bb=t&until=3.6.1) - 2020-11-02
### Fixed
	- Added a null check to optionDelimiter.

## [3.6.0](https://gd-code.gaminglabs.com/projects/GUS/repos/gli.submissions.web/commits?bb=t&until=3.6.0) - 2020-11-02

### Added
- [LETAUT-537](https://gd-jira.gaminglabs.com/browse/LETAUT-537)
	- Adds Other Components Used in Testing interface for use with Letter Generation.
- [LETAUT-563](https://gd-jira.gaminglabs.com/browse/LETAUT-563)
	- Adds new data source JIRA Revocation wording for use in Notes Manager and Letter Generation.
- [LETAUT-613](https://gd-jira.gaminglabs.com/browse/LETAUT-613)
	- Adds new question type Component List for use in Notes Manager and Letter Generation.
- [LETAUT-615](https://gd-jira.gaminglabs.com/browse/LETAUT-615)
	- Adds new data source JIRA Disclosure wording for use in Notes Manager and Letter Generation.
- [LETAUT-649](https://gd-jira.gaminglabs.com/browse/LETAUT-649)
	- Adds ability to copy Memo ID from workspace or letter preview in Letter Generation Interface.
	- Adds ability to navigate to NotesManager from workspace or letter preview in Letter Generation Interface.
- [LETAUT-653](https://gd-jira.gaminglabs.com/browse/LETAUT-653)
	- Adds new memo association, Chip Type for use in Notes Manager and Letter Generation.
- [LETAUT-654](https://gd-jira.gaminglabs.com/browse/LETAUT-654)
	- Adds new memo association, Negate Jurisidcition(s) for use in Notes Manager and Letter Generation.
- [LETAUT-656](https://gd-jira.gaminglabs.com/browse/LETAUT-656)
	- Adds ability to select delimiter when creating a new question in Question Manager.

### Changed
- [LETAUT-575](https://gd-jira.gaminglabs.com/browse/LETAUT-575)
	- Changes to bring US First Page in line with cover page requirements in Letter Generation Interface.
- [LETAUT-700](https://gd-jira.gaminglabs.com/browse/LETAUT-700)
	- Changes to Notes Manager Search to increase performance.

### Fixed
- [LETAUT-712](https://gd-jira.gaminglabs.com/browse/LETAUT-712)
	- Fixes regression introduced from last release that caused certain buttons in the Notes Manager and Letter Generation interfaces to no longer work.
- [PT-1548](https://gd-jira.gaminglabs.com/browse/PT-1548)
	- Fixes Evo Incoming, so that users can include or exclude components that have a WD status.

---

## [3.5.6](https://gd-code.gaminglabs.com/projects/GUS/repos/gli.submissions.web/commits?bb=t&until=3.5.6) - 2020-10-27
### Fixed
- Fixes project manager issue in protrack
---

## [3.5.5](https://gd-code.gaminglabs.com/projects/GUS/repos/gli.submissions.web/commits?bb=t&until=3.5.5) - 2020-10-26
### Fixed
- Fix LetterBook- GPS File order
---

## [3.5.4](https://gd-code.gaminglabs.com/projects/GUS/repos/gli.submissions.web/commits?bb=t&until=3.5.4) - 2020-10-26
### Fixed
- Fix for visual issue in letter generation interface
---

## [3.5.3](https://gd-code.gaminglabs.com/projects/GUS/repos/gli.submissions.web/commits?bb=t&until=3.5.3) - 2020-10-26
### Fixed
- Moves additional share-point link to new share-point.
---

## [3.5.2](https://gd-code.gaminglabs.com/projects/GUS/repos/gli.submissions.web/commits?bb=t&until=3.5.2) - 2020-10-26
### Fixed
- [PT-1542] (https://gd-jira.gaminglabs.com/browse/PT-1542)
- [PT-1541] (https://gd-jira.gaminglabs.com/browse/PT-1541)
	- Letterbook fixes.
---

## [3.5.1](https://gd-code.gaminglabs.com/projects/GUS/repos/gli.submissions.web/commits?bb=t&until=3.5.1) - 2020-10-26
### Fixed
- Fix for visual issue in letter generation interface
---

## [3.5.0](https://gd-code.gaminglabs.com/projects/GUS/repos/gli.submissions.web/commits?bb=t&until=3.5.1) - 2020-10-26
### Added
- [LETAUT-575](https://gd-jira.gaminglabs.com/browse/LETAUT-575)
	- Adds support for the first page of the GLI Letter, in the Letter Generation Interface.
-[LETAUT-607](https://gd-jira.gaminglabs.com/browse/LETAUT-607)
	- Adds a column chooser to the component datagrid in the Notes Manager Interface.
- [LETAUT-645](https://gd-jira.gaminglabs.com/browse/LETAUT-645)
	- Adds memo description to the memo information popup in the Letter Generation Interface.
- [LETAUT-651](https://gd-jira.gaminglabs.com/browse/LETAUT-651)
	- Adds Signature Scope as an available association for Memos in the Notes Manager Interface.
- [LETAUT-652](https://gd-jira.gaminglabs.com/browse/LETAUT-652)
	- Adds Platform as an available association for Memos in the Notes Manager Interface.

### Changed
- [LETAUT-606](https://gd-jira.gaminglabs.com/browse/LETAUT-606)
	- Changes the Letter Generation Interface to re-enable the Modification Descriptions section.
- [PT-1506](https://gd-jira.gaminglabs.com/browse/PT-1506)
	- Changes incoming review, so that you can no longer assign a transfer to an inactive manager in protrack.
- [PT-1537](https://gd-jira.gaminglabs.com/browse/PT-1537)
	- Changes to move letterhead and status together, and changes billing to be the more specific billing sheet.

### Fixed
- [LETAUT-610](https://gd-jira.gaminglabs.com/browse/LETAUT-610)
	- Fixes lookup question types that previously would break if more than one existed in the Letter Generation Workspace at the same time.
- [LETAUT-624](https://gd-jira.gaminglabs.com/browse/LETAUT-624)
	- Fixes an issue with the ordering when saving custom memos in the Letter Generation Interface.
- [PT-1505](https://gd-jira.gaminglabs.com/browse/PT-1505)
	- Fix to correct upload to GPS based on selected jurisdictions and their respective file numbers Only.
- [PT-1526](https://gd-jira.gaminglabs.com/browse/PT-1526)
	- Fixes issue where test lab was being updated while posting letter from Digital Letter Book.
- [PT-1494](https://gd-jira.gaminglabs.com/browse/PT-1494)
	- Fixes bug with document selection for Evolution in the Incoming Review interface.
- [SUB-3540](https://gd-jira.gaminglabs.com/browse/SUB-3540)
	- Fixes issue where certain functionality would error out when trying to build event notifications for actions on submissions that do not have an associated project in the database.
---

## [3.4.1](https://gd-code.gaminglabs.com/projects/GUS/repos/gli.submissions.web/commits?bb=t&until=3.4.1) - 2020-10-21
### Changed
- Updated the Legal Product Submitter's list SharePoint link in the new Manufacturer's email
---

## [3.4.0](https://gd-code.gaminglabs.com/projects/GUS/repos/gli.submissions.web/commits?bb=t&until=3.4.0) - 2020-10-20

### Added
- [LETAUT-634](https://gd-jira.gaminglabs.com/browse/LETAUT-634)
	- Adds a new special flag 'SectionHeader' for use with Notes Manager and Letter Generation Interfaces.
- [PT-1477](https://gd-jira.gaminglabs.com/browse/PT-1477)
	- Adds Math Project Lead to the project search page.
- [PT-1491](https://gd-jira.gaminglabs.com/browse/PT-1491)
	- Adds Company to the Digital Letter Book Interface and adds it as a filter for the defaulting rules.

### Changed
- [LETAUT-622](https://gd-jira.gaminglabs.com/browse/LETAUT-622)
	- Changes to support saving custom memos for Letter Generation.
- [LETAUT-644](https://gd-jira.gaminglabs.com/browse/LETAUT-644)
	- Changes to Notes Manager search to increase performance.
- [PT-1487](https://gd-jira.gaminglabs.com/browse/PT-1487)
	- Changes Evo incoming so the user can add a new component to an existing component group without losing old answers.
- [PT-1490](https://gd-jira.gaminglabs.com/browse/PT-1490)
	- General changes to grid columns and the overall grid in Digital Letter Book.
- [PT-1503](https://gd-jira.gaminglabs.com/browse/PT-1503)
	- Changes the export functionality in Digital Letter Book to include the jurisdiction currency in the output
- [PT-1518](https://gd-jira.gaminglabs.com/browse/PT-1518)
	- Corrects typo in Jurisdiction added email.
- General Letter Generation performance improvements.
- Updates max length of the Email distribution info field in the Email Distributions interface.
- Updates to integrate protrack with new sharepoint legal site.

### Removed
- [LETAUT-614](https://gd-jira.gaminglabs.com/browse/LETAUT-614)
	- Removes Drag and Drop functionality from Letter Generation Interface.
- Removes Component level questions tab in the Letter Generation Interface.
- Removes the Letter Details tab in the Letter Generation Interface.

### Fixed
- [JI-1834](https://gd-jira.gaminglabs.com/browse/JI-1834)
	- Fixes error that occurs when attempting to set an invalid user to any of the jira user fields when creating a compliance outgoing review from Digital Letter Book.
- [LETAUT-610](https://gd-jira.gaminglabs.com/browse/LETAUT-610)
	- Fixes 'Lookup' question type to function properly when more than one exist in the workspace at a time in Letter Generation Interface.
- [LETAUT-623](https://gd-jira.gaminglabs.com/browse/LETAUT-623)
	- Fixes bug in Letter Generation Interface, where updating content on the left wont update the content on the right.
- [SUB-3531](https://gd-jira.gaminglabs.com/browse/SUB-3531)
	- Pending Email Queue Interface, fix to only display Email Queued if Is Enforce Run has been set and the email has not yet been sent.
---

## [3.3.7](https://gd-code.gaminglabs.com/projects/GUS/repos/gli.submissions.web/commits?bb=t&until=3.3.7) - 2020-10-13
### Changed
- Configuration changes for Acumatica.
---

## [3.3.6](https://gd-code.gaminglabs.com/projects/GUS/repos/gli.submissions.web/commits?bb=t&until=3.3.6)  - 2020-10-07

### Fixed
- [SUB-3530](https://gd-jira.gaminglabs.com/browse/SUB-3530)
	- Fixes duplicate platform key issue in SGI flat file Core.
	- Added logger to log GPS upload exception in SGI flat file Core.
	- Ability to see the newly created project even if the GPS upload fails.
---

## [3.3.5](https://gd-code.gaminglabs.com/projects/GUS/repos/gli.submissions.web/commits/65513ac52a556473733773f0803fce08bf7adcaa) - 2020-10-07

### Fixed
- IGT Flat File: Hotfix to allow notes to be up to 8000 characters.
---

## [3.3.4](https://gd-code.gaminglabs.com/projects/GUS/repos/gli.submissions.web/commits/8d001e4dbd5dcd46ef1716bce64272c93cc55eb9) - 2020-10-07

### Fixed
- [LETAUT-605](https://gd-jira.gaminglabs.com/browse/LETAUT-605)
	- Fixes source id mismatch between Notes Manager save and Letter Generation parse.
	- Display game description in the  Letter Generation Interface.
---

## [3.3.3](https://gd-code.gaminglabs.com/projects/GUS/repos/gli.submissions.web/commits/8399ae62397499ea73f72af0e969e892078a2fed) - 2020-10-06

### Fixed
- Letter Automation: Hotfix to make the Footer work in Letter Builder.
---

## [3.3.2](https://gd-code.gaminglabs.com/projects/GUS/repos/gli.submissions.web/commits/540f892b0a7ae906eea1f97815e36716430adaec) - 2020-10-06

### Fixed
- General: Fixes datetime stamp for the current build version to be the correct date.
---

## [3.3.1](https://gd-code.gaminglabs.com/projects/GUS/repos/gli.submissions.web/branches?base=hotifx/LetterGenPerformance) - 2020-10-05
### Changed
- Letter Gen: Commented out the Evo Validations query as it is (n+1)ing and tanking Letter Gen performance.
---


## [3.3.0](https://gd-code.gaminglabs.com/projects/GUS/repos/gli.submissions.web/branches?base=release%2F3.3.0) - 2020-10-05
### Added
- [LETAUT-548](https://gd-jira.gaminglabs.com/browse/LETAUT-548) - Adds styling for letter preview and PDF generation for the Letter Generation Interface.
- [LETAUT-559](https://gd-jira.gaminglabs.com/browse/LETAUT-559) - Adds the ability to load a letter at a given date in the past and load relevant memos, thus recreating the letter at that point in time for the Letter Generation Interface.

### Changed
- [SUB-3515](https://gd-jira.gaminglabs.com/browse/SUB-3515) - Updates the IGT Flat File Interface to support the submission of NEW submissions as well as TRANSFERS in the same flat file submission.
---

## [3.2.1](https://gd-code.gaminglabs.com/projects/GUS/repos/gli.submissions.web/branches?base=release%2F3.2.1) - 2020-10-05

### Added
- [LETAUT-550](https://gd-jira.gaminglabs.com/browse/LETAUT-550) - Adds modal dialog to the memos displayed in the Letter Generation Interface to show Memo Associations.
- [PT-1383](https://gd-jira.gaminglabs.com/browse/PT-1383) - Adds ability for individuals with a specific permission level to Re-open closed projects via a button from the Project Detail page.

### Changed
- [LETAUT-552](https://gd-jira.gaminglabs.com/browse/LETAUT-552) - Code clean up and minor stlying changes for the Letter Generation Interface.
- [LETAUT-555](https://gd-jira.gaminglabs.com/browse/LETAUT-555) - Previously Custom Memos were associated to the current Jurisdiction you were working on, now additional jurisdictions from the current bundle can be associated to the respective custom memo within the Letter Generation Interface.
- [SUB-3510](https://gd-jira.gaminglabs.com/browse/SUB-3510)
	- Naming of IGT billing party information "Sports Testing" changed to "Sports - Testing".
	- Naming of IGT billing party information "Sports Transfers" changed to "Sports - Transfers".

### Fixed
- [LETAUT-565](https://gd-jira.gaminglabs.com/browse/LETAUT-565)
	- Removes errant 'Under construction' message from the Notes Manager answer question modal for questions of type 'Lookup Multiple'.
	- Fixes bug where the 'Dropdown' question type would not display options in the Notes Manager question answer modal.
	- Fixes bug that would cause 'Multi Select' question types answered in Notes Manager to render as a list of ids instead of text inside the memo template.
	- Display labels for 'Radio Button' question types in Notes Manager answer question modal.
	- Fixes bug in Letter Generation Interface where questions of type 'Text Area' were being displayed incorrectly.
- [PT-1481](https://gd-jira.gaminglabs.com/browse/PT-1481) - Allow TKNumber as documentType and treat TKNumber type as Draft approval in Digital Letterbook
---
