﻿import api from './index';
import { basename } from 'path';

const baseUrl = 'evolution/tcmclause/';

export default {
	loadClauseSearchResults(clauseSearch) {
		return api.get(baseUrl + 'Search', {
			params: {
				clauseSearch: JSON.stringify(clauseSearch)
			}
		});
	},
	postClauseData(clauseModel) {
		return api.post(baseUrl + 'Edit', {
			clauseEditViewModel: JSON.stringify(clauseModel)
		});
	},
	loadRelatedClauseData(sourceData) {
		return api.get(baseUrl + 'RelatedClauses', {
			params: {
				sourceData: JSON.stringify(sourceData)
			}
		});
	},
	deprecateAndCloneClause(clauseModel) {
		return api.post(baseUrl + 'DeprecateAndCloneClause', {
			clauseEditViewModel: JSON.stringify(clauseModel)
		});
	}
};