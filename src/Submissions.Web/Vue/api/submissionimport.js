﻿import api from './index'
const baseUrl = 'submission/import/';

export default {
	createNewSubmission: newSubmission => { return api.post(baseUrl + 'createnewsubmission/', newSubmission); },
	createTransferRequests: followUpRequests => { return api.post(baseUrl + 'createtransferrequests/', followUpRequests); },

	addRecordToExistingSubmission: newSubmission => { return api.post(baseUrl + 'addRecordToExistingSubmission/', newSubmission); },

	deleteStagedNewSubmission: letterNumber => { return api.post(baseUrl + 'deletestagednewsubmission/' + letterNumber); },
	deleteTransferRequest: transferRequestId => { return api.post(baseUrl + 'deletestagedtransferrequest/' + transferRequestId); },
	deleteTransferPackageRequest: letterNumber => { return api.post(baseUrl + 'deletetransferpackagerequest/' + letterNumber); },
	deleteWDRJRequest: wdrjRequestId => { return api.post(baseUrl + 'deletestagedwdrjrequest/' + wdrjRequestId); },
	deleteWDRJPackageRequest: letterNumber => { return api.post(baseUrl + 'deletewdrjpackagerequest/' + letterNumber); },
	deleteComponent: componentId => { return api.post(baseUrl + 'deletecomponent/' + componentId); },
	deactivateComponentHeader: headerId => { return api.post(baseUrl + 'deactivateheader/' + headerId); },

	fetchStagedNewSubmissions: () => { return api.post(baseUrl + 'getstagednewsubmissions/'); },
	fetchStagedTransferRequests: () => { return api.post(baseUrl + 'getstagedtransferrequests/'); },
	fetchStagedWDRJRequests: () => { return api.post(baseUrl + 'getstagedwdrjrequests/'); },
	fetchRequestsRequiringReview: () => { return api.post(baseUrl + 'getrequestsrequiringreview'); },
	fetchCompletedSGISubmissions: () => { return api.post(baseUrl + 'getCompletedSGISubmissions'); },
	fetchRequestsRequiringReviewCount: () => { return api.post(baseUrl + 'getrequestsrequiringreviewcount'); },
	fetchSGICompletedCount: () => { return api.post(baseUrl + 'getSGICompletedCount'); },

	getRecentlyCompletedNewSubmissions: () => { return api.post(baseUrl + 'getrecentlycompletednewsubmissions/'); },
	getRecentlyCompletedTransfers: () => { return api.post(baseUrl + 'getrecentlycompletedtransfers/'); },
	getRecentlyCompletedWDRJRequests: () => { return api.post(baseUrl + 'getrecentlycompletedwdrjs/'); },
	getSubmissions: submissionFilter => { return api.post(baseUrl + 'getsubmissions/', submissionFilter); },

	mapFollowUpRequest: payload => { return api.post(baseUrl + 'mapfollowuprequest/', payload); },
	moveStagedRequest: payload => { return api.post(baseUrl + 'moveStagedRequest', payload); },
	unmapFollowUpRequest: folowUpRequestId => { return api.post(baseUrl + 'unmapfollowuprequest/' + folowUpRequestId); },
	updateElectronicSubmissionComponent: component => { return api.post(baseUrl + 'updateElectronicSubmissionComponent/', component); },
	updateElectronicSubmissionComponents: components => { return api.post(baseUrl + 'updateElectronicSubmissionComponents/', components); },
	processWDRJRequests: requests => { return api.post(baseUrl + 'processwdrjrequests/', requests); }
}