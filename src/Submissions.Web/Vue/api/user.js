﻿import api from './index';

const baseUrl = 'User/';

export default {
	loadSubscription(subscriptionId) {
		return api.post(baseUrl + 'Notifications/LoadSubscription', {
			subscriptionId: subscriptionId
		});
	},
	importSubscriptions(importUserId, userId) {
		return api.post(baseUrl + 'Notifications/ImportSubscriptions', {
			importUserId: importUserId,
			userId: userId
		});
	},
}