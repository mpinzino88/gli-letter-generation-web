﻿import api from './index';
const baseUrl = 'documents/letterSection/';

export default {
	post_commitLetterSectionOrder: payload => { return api.post(baseUrl + 'CommitLetterSectionOrder', payload); },
	post_saveLetterSection: payload => { return api.post(baseUrl + 'Save', payload); },
	post_getLetterSectionOrderSet: () => { return api.post(baseUrl + 'GetLetterSectionOrderSet'); },
	post_commitLetterContentOrder: (payload) => { return api.post(baseUrl + 'CommitLetterContentOrder', payload); },
	post_letterContentOrderBySectionId: (sectionId) => { return api.post(baseUrl + 'GetLetterContentBySectionId', { sectionId: sectionId }); },
};

