﻿import api from './index';

const baseUrl = '/documents/';

export default {
	deleteSupplement(letterId, jurisdictionDataIds) {
		return api.post(baseUrl + 'createvue/DeleteSupplement', {
		
				letterId: letterId,
				jurisdictionDataIds: jurisdictionDataIds
			
		});
	},
	deleteLetterFromDB(documentVersionId, updateJurisdictionDataIds, deleteConfirmData, updateFields) {
		return api.post(baseUrl + 'createvue/DeleteLetterFromDB', {
			documentVersionId: documentVersionId,
			updateJurisdictionDataIds: updateJurisdictionDataIds,
			deleteConfirmData: deleteConfirmData,
			updateFields: updateFields
		});
	},
	getBillingInfo(documentVersionId) {
		return api.get(baseUrl + 'createvue/getBillingInfo', {
			params: {
				documentVersionId: documentVersionId
			}
		});
	},
	getBillingSheetsNeedBilledCount() {
		return api.get(baseUrl + 'createvue/GetBillingSheetsNeedBilledCount', {
			params: {
			}
		});
	},
	getComponentName(submissionId) {
		return api.get(baseUrl + 'createvue/GetComponentName', {
			params: {
				submissionId: submissionId
			}
		});
	},
	getConfirmation(bundleId, letters) {
		return api.post(baseUrl + 'createvue/GetConfirmationBeforeProcess', {
			bundleId: bundleId,
			letters: letters
		});
	},
	getDeleteConfirmData(jurisdictionDataIds) {
		return api.post(baseUrl + 'createvue/getDeleteConfirmData', {
				jurisdictionDataIds: jurisdictionDataIds
		});
	},
	getDefaults(jurisdictionId, manufacturer, subType) {
		return api.get(baseUrl + 'createvue/GetDefaults', {
			params: {
				jurisdictionId: jurisdictionId,
				manufacturer: manufacturer,
				subType: subType
			}
		});
	},
	getDraftLetterData(draftLetterId) {
		return api.get(baseUrl + 'createvue/GetDraftInfo', {
			params: {
				draftLetterId: draftLetterId
			}
		});
	},
	getJurisdictionPdfVersions(jurisdictionalDataIds) {
		return api.post(baseUrl + 'createvue/GetJurisdictionDocumentVersions', {
				jurisdictionalDataIds: jurisdictionalDataIds
		});
	},
	getLetterModels(bundleId) {
		return api.get(baseUrl + 'createvue/GetLetterModels', {
			params: {
				bundleId: bundleId
			}
		});
	},
	getProjectJurisdictionIndicator(projectId, jurisdictionalDataIds) {
		console.log(projectId)
		console.log(jurisdictionalDataIds)
		return api.post(baseUrl + 'createvue/GetProjectJurisdictionIndicator', {
			projectId: projectId,
			jurisdictionalDataIds: jurisdictionalDataIds
		});
	},
	getSupplementLetters(jurisdictionalDataIds) {
		return api.post(baseUrl + 'createvue/GetAllSupplements', {
			jurisdictionalDataIds: jurisdictionalDataIds
		});
	},
	getSupplementLetterModels(bundleId) {
		return api.get(baseUrl + 'createvue/GetSupplementLetterModels', {
			params: {
				bundleId: bundleId
			}
		});
	},
	getQANotesComment(fileNumber, jurisdictionId) {
		return api.get(baseUrl + 'createvue/GetQANotesComment', {
			params: {
				fileNumber: fileNumber,
				jurisdictionId: jurisdictionId
			}
		});
	},
	postCompliance(complianceLetter, complianceData) {
		return api.post(baseUrl + 'createvue/ProcessCompliance', {
			complianceData: complianceData,
			complianceLetter: complianceLetter
		});
	},
	postApproval(model, billingModel) {
		return api.post(baseUrl + 'createvue/ProcessLetter', {
			letterToApprove: model
		});
	},
	postTranslations(model, documentVersionId) {
		return api.post(baseUrl + 'createvue/PostTranslationLetters', {
			letterModel: model,
			documentVersionId: documentVersionId
		});
	},
	postSupplement(model, billingModel) {
		return api.post(baseUrl + 'createvue/postsupplementalletter', {
			letterModel: model
		});
	},
	previewPdf(model) {
		return api.post(baseUrl + 'createvue/previewpdf', {
			letterModel: model
		});
	},
	previewSupplementPdf(model) {
		return api.post(baseUrl + 'createvue/previewsupplementpdf', {
			letterModel: model
		});
	},
	pullFromAcct(documentVersionId) {
		return api.get(baseUrl + 'createvue/PullFromAccounting', {
			params: {
				documentVersionId: documentVersionId
			}
		});
	},
	pushBackToAcct(documentVersionId) {
		return api.get(baseUrl + 'createvue/PushBackToAccounting', {
			params: {
				documentVersionId: documentVersionId
			}
		});
	},
	resetTestCase(bundleId) {
		return api.get(baseUrl + 'createvue/ResetLetterbookTestCase', {
			params: {
				bundleId: bundleId
			}			
		});
	},
	saveBillingInfo(documentVersionId, billingInfo, isBillable) {
		return api.post(baseUrl + 'createvue/saveBillingInfo', {
				documentVersionId: documentVersionId,
				billingData: billingInfo,
				isBillable: isBillable
		});
	},
	saveDraftLetterInfo(model) {
		return api.post(baseUrl + 'createvue/SaveDraftInfo', {
			vm: model
		});
	},
	sendConfirmationEmail(bundleId, approvedLetters, failedLetters) {
		return api.post(baseUrl + 'createvue/SendConfirmationEmail', {
			bundleId: bundleId,
			approvedLetters: approvedLetters,
			failedLetters: failedLetters
		});
	},
	sendToENForReview(letters, reviewData, bundleName, bundleTarget) {
		return api.post(baseUrl + 'createvue/SendForENReview', {
			letters: letters,
			reviewData: reviewData,
			bundleName: bundleName,
			targetDate: bundleTarget
		});
	},
	sendForLatinAmericaReview(letters, bundleId, userId) {
		return api.post(baseUrl + 'createvue/SendForLatinAmericaReview', {
			letters: letters,
			bundleId: bundleId,
			userId: userId
		});
	},
	sendForSecondaryReview(letters, bundleId) {
		return api.post(baseUrl + 'createvue/SendForSecondaryReview', {
			letters: letters,
			bundleId: bundleId
		});
	},
	updateCerLab(approvalData, certLabId) {
		return api.post(baseUrl + 'createvue/UpdateCertificationLab', {
			approvalData: approvalData,
			certLabId: certLabId
		});
	},
	updateCloseDate(approvalData, closeDate) {
		return api.post(baseUrl + 'createvue/UpdateJurisdictionCloseDate', {
			approvalData: approvalData,
			closeDate: closeDate
		});
	},
	updateDraftDate(approvalData, draftDate) {
		return api.post(baseUrl + 'createvue/UpdateJurisdictionDraftDate', {
			approvalData: approvalData,
			draftDate: draftDate
		});
	},
	updateUpdateDate(approvalData, updateDate, updateDateReasonId) {
		return api.post(baseUrl + 'createvue/UpdateJurisdictionUpdatedDate', {
			approvalData: approvalData,
			updateDate: updateDate,
			updateDateReasonId: updateDateReasonId
		});
	},
	viewPdf(filePath) {
		return api.post(baseUrl + 'createvue/ViewPdf', {
			filePath: filePath
		});
	}
};