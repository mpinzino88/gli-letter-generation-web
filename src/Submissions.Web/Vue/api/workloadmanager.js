﻿import api from './index'

const baseUrl = 'protrack/workloadmanager/';

export default {
	commitChanges(tasks) {
		return api.post(baseUrl + 'commitchanges', {
			tasks: tasks
		});
	},
	loadResource(userId, mainFilter) {
		return api.post(baseUrl + 'loadResource', {
			userId: userId,
			mainFilter: mainFilter
		});
	},
	rearrangeDates(resource, tasks) {
		return api.post(baseUrl + 'rearrangedates', {
			resource: resource,
			tasks: tasks
		});
	},
	updateResourceUnavailability(resources) {
		return api.post(baseUrl + 'resourceunavailability', {
			resources: resources
		});
	}
}