﻿import api from './index'
const baseUrl = 'protrack/product/';

export default {
	deleteProduct(id) {
		return api.post(baseUrl + 'Delete', { id: id });
	},
	saveProduct(vm) {
		console.log({ vm })
		return api.post(baseUrl + 'Save', vm);
	},
	searchComponents(filter) {
		return api.post(baseUrl + 'SearchComponents', filter);
	},
	getProducts(filter) {
		return api.post(baseUrl + 'GetProducts', filter);
	}
}