﻿import api from './index';

const baseUrl = 'tools/';

export default {
	validateComponents(componentsList) {
		return api.get(baseUrl + 'ocuit/ValidateComponents', {
			params: {
				compatibilityComponents: componentsList,
			}
		});
	}
};