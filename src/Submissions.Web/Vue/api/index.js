﻿import Axios from 'axios';

export default Axios.create({
	baseURL: jsPath,
	headers: {
		'Cache-Control': 'no-cache',
		'Cache-control': 'no-store',
		'Pragma': 'no-cache',
		'Expires': '0'
	}
})