﻿import api from './index';
import { basename } from 'path';
import { PROJECT_REVIEW_STATUS } from '@/common';

const baseUrl = 'protrack/projectreview/';

export default {
	getRevieweeSpecificStatus: id => api.get(baseUrl + 'getRevieweeSpecificStatus', {
		params: { reviewId: id }
	}),
	getUserSuggestions: (id, departmentId) => api.get(baseUrl + 'getUserSuggestions', {
		params: { projectId: id, departmentId: departmentId }
	}),
	addNewReview: (projectId, departmentId, reviewees, reviewers, letterwriters, watchers, description) => api.post(baseUrl + 'addnewreview', {
		projectId: projectId,
		departmentId: departmentId,
		reviewees: reviewees,
		reviewers: reviewers,
		letterwriters: letterwriters,
		watchers: watchers,
		description: description
	}),
	addNewUsers: (reviewId, reviewerIds, revieweeIds, letterwriterIds, watcherIds) => api.post(baseUrl + 'addnewusers', {
		reviewerIds: reviewerIds,
		revieweeIds: revieweeIds,
		letterwriterIds: letterwriterIds,
		watcherIds: watcherIds,
		reviewId: reviewId
	}),
	deleteReview: (reviewId) => api.post(baseUrl + 'deletereview', {
		reviewId: reviewId
	}),
	reopenReview: (reviewId) => api.post(baseUrl + 'reopenreview', {
		reviewId: reviewId,
		reviewStatus: PROJECT_REVIEW_STATUS.VERIFICATIONPENDING
	}),
	completeReview: (viewModel, note) => api.post(baseUrl + 'completereview', {
		vm: viewModel,
		note: note
	}),
	completeReviewFinalization: (viewModel, note) => api.post(baseUrl + 'completereviewfinalization', {
		vm: viewModel,
		note: note
	}),
	completeReviewLetterbook: (viewModel, note) => api.post(baseUrl + 'completereviewletterbook', {
		vm: viewModel,
		note: note
	}),
	deleteIssue: (detailId) => api.post(baseUrl + 'deletereviewitem', {
		detailId: detailId
	}),
	markAllresolved: (viewModel, note) => api.post(baseUrl + 'markallresolved', {
		vm: viewModel,
		note: note
	}),
	updateCorrectionComplete: (viewModel, revieweeId, note) => api.post(baseUrl + 'UpdateCorrectionComplete', {
		vm: viewModel,
		revieweeId: revieweeId,
		emailNote: note
	}),
	mediationEmail: (viewModel, detailId) => api.post(baseUrl + 'mediationemail', {
		vm: viewModel,
		detailId: detailId
	}),
	saveReviewDetail: (issue, reviewId) => api.post(baseUrl + 'savereviewdetail', {
		item: issue,
		reviewId: reviewId
	}),
	assignReview: (viewModel, note) => api.post(baseUrl + 'assignreview', {
		vm: viewModel,
		note: note
	}),
	saveComment: (detailId, comment) => api.post(baseUrl + 'savecomment', {
		detailId: detailId,
		comment: comment
	}),
	updateResolved: (detailId, value) => api.post(baseUrl + 'updateresolved', {
		detailId: detailId,
		value: value
	}),
	saveDescription: (reviewId, description) => api.post(baseUrl + 'savedescription', {
		reviewId: reviewId,
		description: description
	}),
	saveDepartment: (reviewId, departmentId) => api.post(baseUrl + 'savedepartment', {
		reviewId: reviewId,
		departmentId: departmentId
	}),
	deleteUser: (reviewId, userId) => api.post(baseUrl + 'deleteuser', {
		reviewId: reviewId,
		userId: userId
	})
}