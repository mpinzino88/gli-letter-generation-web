﻿import api from './index';

const baseUrl = '/lookups/letterbookdefaultingoptions';

export default {
	postClone(sourceId, jurIds) {
		return api.post(baseUrl + '/clone', {
			jurIds: jurIds,
			sourceId: sourceId
		});
	}
};