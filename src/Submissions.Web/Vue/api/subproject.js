﻿import api from './index'
import { basename } from 'path';

const baseUrl = 'Protrack/SubProjects/';

export default {
	getSubProject: id => api.get(baseUrl + 'GetSubProject', { params: { id: id } }),
	loadTask: id => api.get(baseUrl + 'LoadTask', { params: { id: id } }),
	saveSubProject: (subProject, projectId) => api.post(baseUrl + 'SaveSubProject', { subProject: subProject, projectId: projectId }),
	saveTask: task => api.post(baseUrl + 'SaveTask', { task: task }),
	startTask: taskId => api.post(baseUrl + 'StartTask', { taskId: taskId }),
	completeTask: task => api.post(baseUrl + 'CompleteTask', { task: task })
}