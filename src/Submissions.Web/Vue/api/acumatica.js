﻿import api from './index'
const baseUrl = 'acumatica/feederrorlog/';

export default {
	post_MarkResolved: payload => { return api.post(baseUrl + 'MarkResolved', payload); },
	post_SetupProjects: payload => { return api.post(baseUrl + 'SetupProjects', payload); },
	post_GetUnResolvedAcumaticaFeedErrors: () => { return api.post(baseUrl + 'getUnResolvedAcumaticaFeedErrors'); }
};

