﻿import api from './index';

const baseUrl = '/lookups/help/';

export default {
	getHelpContentByCode(code) {
		return api.get(baseUrl + 'GetHelpContentByCode', {
			params: {
				code: code
			}
		});
	}
};