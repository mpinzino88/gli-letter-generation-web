﻿import api from './index';
const baseUrl = 'documents/noteManager/';

export default {
	post_saveLetterContent: payload => { return api.post(baseUrl + 'Save', payload); },
	post_getLetterContent: id => { return api.post(baseUrl + 'GetLetterContent', id); },
	post_getQuestion: id => { return api.post(baseUrl + 'GetQuestion', { id: id }); },
	post_getMockDataByBundleId: id => { return api.post(baseUrl + 'GetMockDataByBundleId', { id: id }); },
	post_getBaseTemplate: name => { return api.post(baseUrl + 'GetBaseTemplate', { name: name }); },
	post_getMemoLineage: originialId => { return api.post(`${baseUrl}GetMemoLineage`, { originialId: originialId }); },
	post_getMemo: id => { return api.post(`${baseUrl}GetMemo`, { id: id }); },
	post_searchQuestions: filter => { return api.post(`${baseUrl}SearchQuestions`, { filter: filter }) },
	post_searchMemos: filter => { return api.post(`${baseUrl}SearchMemos`, { filter: filter }) },
	post_deprecateMemo: id => { return api.post(baseUrl + 'DeprecateMemo', { id: id }); },
	post_deprecateLetterContent: payload => { return api.post(baseUrl + 'DeprecateLetterContent', payload); },
	post_getMemos: letterContentId => { return api.post(`${baseUrl}GetMemos`, { letterContentId: letterContentId }); },
	post_validateMemoQuestions: payload => { return api.post(`${baseUrl}ValidateMemoQuestions`, payload) },
	post_getValidations: payload => { return api.post(`${baseUrl}GetValidations`, payload) },
};

