﻿import api from './../index';
import { set } from 'lodash';
const baseUrl = 'documents/generate/';

export default {
	getSignatureData(submissionId) {
		return api.get(baseUrl + 'GetSignatureData', {
			params: {
				submissionId: submissionId
			}
		});
	},
	getSubmissionSignatureData(submissionIds) {
		return api.get(baseUrl + 'getSubmissionSignatureData', {
			params: {
				submissionIds: submissionIds
			}
		});
	},
	getComponentData(bundleId) {
		return api.get(baseUrl + 'GetComponentData', {
			params: {
				bundleId: bundleId
			}
		});
	},
	getLetterContactData(letterContactId) {
		return api.get(baseUrl + 'GetLetterContactData', {
			params: {
				letterContactId: letterContactId
			}
		});
	},
	postNewSignatureData(signatureViewModel) {
		return api.post(baseUrl + 'PostNewSignatureData', {
			signatureViewModel: signatureViewModel
		});
	},
	searchComponents(searchVM) {
		return api.get(baseUrl + 'SearchComponents', {
			params: {
				searchVM: searchVM
			}
		});
	},
	saveStates(model) {
		return api.post(baseUrl + 'Save', {
			projectLetter: model
		});
	},
	loadApplicableTemplates(jurisdictionIds, manufacturerId) {
		return api.get(baseUrl + 'LoadApplicableTemplates', {
			params: {
				jurisdictionIds: jurisdictionIds,
				manufacturerId: manufacturerId
			}
		});
	},
	loadTemplateExplanations(templateId) {
		return api.get(baseUrl + 'GetTemplate', {
			params: {
				templateId: templateId
			}
		});
	},
	saveClauseExplanations(projectId, clauseReports) {
		console.log(clauseReports);
		return api.get(baseUrl + 'SaveEvoExplanations', {
			params: {
				projectId: projectId,
				clauseReports: clauseReports
			}
		});
	},
	saveAnswer(answer) {
		return api.post(baseUrl + 'SaveAnswer', {
			answer: answer
		});
	},
	loadAnswer(questionId, jurisdictionalDataIds) {
		return api.get(baseUrl + 'GetAnswer', {
			params: {
				questionId: questionId,
				jurisdictionalDataIds: jurisdictionalDataIds
			}
		})
	},
	finalize(projectId, documents) {
		return api.post(baseUrl + 'FinalizeAnswers', {
			projectId: projectId,
			documents: documents
		});
	},
	saveCustomMemo(CustomMemo) {
		return api.post(baseUrl + 'saveCustomMemo', {
			CustomMemo: CustomMemo
		});
	},
	deleteCustomMemo(memoId) {
		return api.post(baseUrl + 'DeleteCustomMemo', {
			memoId: memoId
		});
	},
	getMemoAssociations(memoId) {
		return api.get(baseUrl + 'GetMemoAssociations', {
			params: {
				memoId: memoId,
			}
		});
	},
	saveUserColumns(settingsString) {
		return api.post(baseUrl + 'SaveUserColumnSettings', {
			userColumnSettings: settingsString
		});
	},
	getUserColumnSettings() {
		return api.get(baseUrl + 'GetUserColumnSettings');
	},
	updateComponentChanges(record) {
		return api.post(baseUrl + 'updatecomponent', {
			row: record
		});
	},

	getLetterPDFPreview(payload) {
		return api.post(baseUrl + 'PreviewPDFPost', payload);
	},
	savePaytables(payload) {
		return api.post(baseUrl + 'SavePaytables', payload);
	}
};