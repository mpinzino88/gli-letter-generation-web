﻿import api from './../index';
const baseUrl = 'documents/Coversheet/';

export default {
	createMathEngineeringInfo(coversheetId) {
		return api.post(baseUrl + 'CreateMathEngineeringInfo', {
			coversheetId: coversheetId
		});
	},

	deleteMathEngineeringInfo(mathEngineeringInfoId) {
		return api.post(baseUrl + 'DeleteMathEngineeringInfo', {
			mathEngineeringInfoId: mathEngineeringInfoId
		});
	},

	editMathEngineeringInfo(mathEngineeringInformation) {
		return api.post(baseUrl + 'EditMathEngineeringInfo', {
			mathEngineeringInfoViewModel: mathEngineeringInformation
		});
	},

	getCoversheets(fileNumber) {
		return api.get(baseUrl + 'GetCoversheets', {
			params: {
				fileNumber: fileNumber
			}
		});
	},

	getMathEngineeringInformations(coversheetId) {
		return api.get(baseUrl + 'GetMathEngineeringInfos', {
			params: {
				coversheetId: coversheetId
			}
		});
	}
};