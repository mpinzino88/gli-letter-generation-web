﻿import api from './../index';

const baseUrl = 'Documents/GameDescription/';

export default {
	getGameDescriptionInfo(payload) {
		return api.post(baseUrl + 'GetGameDescriptions', payload);
	},
	getGameDescription(payload) {
		return api.post(baseUrl + 'GetGameDescription', payload);
	},
	saveGameDescription(payload) {
		return api.post(baseUrl + 'SaveGameDescription', payload);
	},
	deleteGameDescription(gameDescriptionId) {
		return api.post(baseUrl + 'DeleteGameDescription', { gameDescriptionId: gameDescriptionId });
	}
}; 