﻿import api from './index';
import { basename } from 'path';

const baseUrl = 'Evolution/Review/';

export default {
	loadTestCase(componentGroupId, clauseId) {
		return api.post(baseUrl + 'LoadTestCase', {
			componentGroupId: componentGroupId,
			clause: clauseId
		});
	},
	loadClauseReport(componentGroupId, jurisdictionIds, documentIds, nativeLanguage) {
		return api.post(baseUrl + 'LoadClauseReport', {
			componentGroupId: componentGroupId,
			jurisdictionIds: jurisdictionIds,
			documentIds: documentIds,
			nativeLanguage: nativeLanguage
		}); 
	},
	loadInstances(projectName) {
		return api.get(baseUrl + 'LoadInstances', {
			params: {
				projectName: projectName
			}
		});
	},
	loadComponentGroupsFromInstance(instanceId) { //implementation of using instanceId
		return api.get(baseUrl + 'LoadComponentForInstance', {
			params: {
				instanceId: instanceId
			}
		});
	},
	loadComponentGroupFromId(componentGroupId) { //implementation of using instanceId
		return api.get(baseUrl + 'LoadComponentGroupFromId', {
			params: {
				componentGroupId: componentGroupId
			}
		});
	},
	loadComponentGroupJurisdictions(componentGroupId) {
		return api.get(baseUrl + 'LoadComponentGroupJurisdictions', {
			params: {
				componentGroupId: componentGroupId
			}
		});
	},
	loadComponentGroupDocuments(componentGroupId) {
		return api.get(baseUrl + 'LoadComponentGroupDocuments', {
			params: {
				componentGroupId: componentGroupId
			}
		});
	},
	loadFunctionalRoles(componentGroupId) {
		return api.post(baseUrl + 'LoadFunctionalRoles', {
			componentGroupId: componentGroupId
		});
	},
	loadEvolutionDocuments(evoDoc) {
		return api.get(baseUrl + 'LoadEvolutionDocuments', {
			params: {
				evoDoc: evoDoc
			}
		});
	}
}