﻿import api from './index'

export default {
	apply(id, section) {
		return api.post('formfilters/apply', {
			id: id,
			section: section
		});
	},
	delete(id) {
		return api.post('formfilters/delete', {
			id: id
		});
	},
	edit(id, filterName, section, filters) {
		return api.post('formfilters/edit', {
			id: id,
			filterName: filterName,
			section: section,
			filters: filters
		});
	},
	load(section) {
		return api.get('formfilters/loadjson', {
			params: {
				section: section
			}
		});
	},
	remove(id) {
		return api.post('formfilters/remove', {
			id: id
		});
	}
}