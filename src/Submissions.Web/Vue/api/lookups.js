﻿import api from './index';
const baseUrl = 'LookupsAjax/';

export default {
	get_manufacturers: filter => { return api.get(`${baseUrl}manufacturers`, { params: { searchTerm: filter.searchTerm, pageSize: filter.pageSize || 50, pageNum: filter.pageNum || 1 } }) },
	get_jurisdictions: filter => { return api.get(`${baseUrl}jurisdictions`, { params: { searchTerm: filter.searchTerm, pageSize: filter.pageSize || 50, pageNum: filter.pageNum || 1, filter: filter.queryFilter || null } }) },
	get_bundles: filter => { return api.get(`${baseUrl}bundles`, { params: { searchTerm: filter.searchTerm, pageSize: filter.pageSize || 50, pageNum: filter.pageNum || 1 } }) },
	get_letterSections: filter => { return api.get(`${baseUrl}letterContentSection`, { params: { searchTerm: filter.searchTerm, pageSize: filter.pageSize || 50, pageNum: filter.pageNum || 1 } }) },
	get_letterMemos: filter => { return api.get(`${baseUrl}letterContentMemos`, { params: { searchTerm: filter.searchTerm, pageSize: filter.pageSize || 50, pageNum: filter.pageNum || 1 } }) },
	get_languages: filter => { return api.get(`${baseUrl}language`, { params: { searchTerm: filter.searchTerm, pageSize: filter.pageSize || 50, pageNum: filter.pageNum || 1 } }) },
	get_functions: filter => { return api.get(`${baseUrl}functions`, { params: { searchTerm: filter.searchTerm, pageSize: filter.pageSize || 50, pageNum: filter.pageNum || 1 } }) },
	get_compareCriterias: filter => { return api.get(`${baseUrl}LetterContentCompareCriteria`, { params: { searchTerm: filter.searchTerm, pageSize: filter.pageSize || 50, pageNum: filter.pageNum || 1 } }) },
	get_subTypes: filter => { return api.get(`${baseUrl}SubmissionTypes`, { params: { searchTerm: filter.searchTerm, pageSize: filter.pageSize || 50, pageNum: filter.pageNum || 1 } }) },
	get_testingTypes: filter => { return api.get(`${baseUrl}TestingTypes`, { params: { searchTerm: filter.searchTerm, pageSize: filter.pageSize || 50, pageNum: filter.pageNum || 1 } }) },
	get_signatureScopes: filter => { return api.get(`${baseUrl}SignatureScopes`, { params: { searchTerm: filter.searchTerm, pageSize: filter.pageSize || 50, pageNum: filter.pageNum || 1 } }) },
	get_platforms: filter => { return api.get(`${baseUrl}Platforms`, { params: { searchTerm: filter.searchTerm, pageSize: filter.pageSize || 50, pageNum: filter.pageNum || 1 } }) },
	get_chiptypes: filter => { return api.get(`${baseUrl}ChipTypes`, { params: { searchTerm: filter.searchTerm, pageSize: filter.pageSize || 50, pageNum: filter.pageNum || 1 } }) }
};



