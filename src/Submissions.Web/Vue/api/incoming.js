﻿import api from './index'
import { basename } from 'path';
const baseUrl = 'Protrack/IncomingReview/';

export default {
	loadJurisdiction(projectId, submissionIds, requiredJuridictionIds) {
		return api.post(baseUrl + 'getjurisdictionsforcomponentgroup', {
			ProjectId: projectId,
			SubmissionsIds: submissionIds,
			RequiredJuridictionIds: requiredJuridictionIds
		})
	},

	saveSubProject: (subProject, projectId) => api.post(baseUrl + 'SaveSubProject', { subProject: subProject, projectId: projectId }),
	deleteSubProject: (id, projectId) => api.post(baseUrl + 'DeleteSubProject', { id: id, projectId: projectId }),
	getSubProject: id => api.get(baseUrl + 'GetSubProject', { params: { id: id } }),

	
	loadExtraneousJurisdictionById(id) {
		return api.get(baseUrl + 'GetExtraneousJurisdictionById', {
			params: {
				id: id
			}
		});
	},
	saveComponentGroups(model) {
		return api.post(baseUrl + 'savecomponentgroups', {
			ProjectId: model.ProjectId,
			ProjectName: model.ProjectName,
			ComponentGroups: model.ComponentGroups,
			PreserveAnswer: model.PreserveAnswer,
			PreserveAnswers: model.PreserveAnswers
		});
	},
	getpotentialTasksForComponentGroup(testCaseSeed) {
		return api.get(baseUrl + 'GetpotentialTasksForComponentGroup', {
			params: {
				testCaseSeed: JSON.stringify(testCaseSeed)
			}
		}); 
	},
	saveTasks(projectId, taskLockEnforced,tasks) {
		return api.post(baseUrl + 'SaveTasks', {
			ProjectId: projectId,
			TaskLockEnforced: taskLockEnforced,
			EngTasks: tasks
		});
	},
	getTemplateTasks(template) {
		return api.get(baseUrl + 'GetTemplateTasks', {
			params: {
				template: template
			}
		});
	},
	newAvailableComponents(projectId, componentsInGroups) {
		return api.post(baseUrl + 'NewAvailableComponents', {
			ProjectId: projectId,
			ComponentsInGroups: componentsInGroups
		});
	},
	lockUnlockComponentGroup(componentGroupId, answerId, versionId, lockUnlock) {
		return api.post(baseUrl + 'LockUnlockComponentGroup', {
			componentGroupId: componentGroupId,
			answerId: answerId,
			versionId: versionId,
			lockUnlock: lockUnlock
		});
	},
	deleteComponentGroup(componentGroupId) {
		return api.post(baseUrl + 'DeleteComponentGroup', {
			componentGroupId: componentGroupId
		});
	},
	loadComponentGroups: filenumber => api.get(baseUrl + 'LoadComponentGroups', { params: { filenumber: filenumber } })
}