import Vue from 'vue';
import Router from 'vue-router';

import about from '@/components/help/about/Index';
import acumaticaFeedErrorLogIndex from '@/components/acumatica/feederrorlog/Index';
import acumaticaPushProjectIndex from '@/components/acumatica/pushproject/Index';
import acumaticaDashboardIndex from '@/components/acumatica/dashboard/Index';
import appFeeIndex from '@/components/lookup/appfee/Index';
import appFeeEdit from '@/components/lookup/appfee/Edit';
import associatedSoftwareTemplatesIndex from '@/components/lookup/associatedsoftwaretemplates/Index';
import associatedSoftwareTemplatesEdit from '@/components/lookup/associatedsoftwaretemplates/Edit';
import billingPartyIndex from '@/components/lookup/billingparty/Index';
import billingPartyEdit from '@/components/lookup/billingparty/Edit';
import billingSheetIndex from '@/components/lookup/billingsheet/Index';
import billingSheetEdit from '@/components/lookup/billingsheet/Edit';
import cabinetIndex from '@/components/lookup/cabinet/Index';
import cabinetEdit from '@/components/lookup/cabinet/Edit';
import clauseReport from '@/components/evolution/clausereport/Index';
import clauseTemplateManagerIndex from '@/components/tools/clausetemplatemanagement/Index';
import clauseTemplateManagerEdit from '@/components/tools/clausetemplatemanagement/Edit';
import conformanceCriteria from '@/components/documents/conformancecriteria/Index';
import documentsCreateIndex from '@/components/documents/create/Index';
import documentsGenerateIndex from '@/components/documents/generate/Index';
import emailManageIndex from '@/components/tools/emailsmanagement/Index';
import formatFiltersIndex from '@/components/lookup/formatfilters/Index';
import formatFiltersEdit from '@/components/lookup/formatfilters/Edit';
import gameDescriptionIndex from '@/components/documents/gameDescription/Index';
import gameDescriptionEdit from '@/components/documents/gameDescription/Edit';
import holidayEdit from '@/components/lookup/holiday/Edit';
import holidayIndex from '@/components/lookup/holiday/Index';
import incomingReview from '@/components/incoming/Index';
import importImporter from '@/components/submission/import/Importer';
import importStage from '@/components/submission/import/Stage';
import importNewSubmissions from '@/components/submission/import/NewSubmissions';
import importTransfers from '@/components/submission/import/Transfers';
import importRequestsRequiringReview from '@/components/submission/import/RequestsRequiringReview';
import importSGICompletedSubmission from '@/components/submission/import/SGICompletedNewSubmissionTable';
import jurisdicationAddEdit from '@/components/submission/jurisdictionaldata/Edit';
import jurisdicationLinkLetter from '@/components/submission/jurisdictionlinkletter/Index';
import jurisdicationReplaceLink from '@/components/submission/jurisdictionreplacelink/Index';
import importWithdrawAndRejects from '@/components/submission/import/WithdrawAndRejects';
import importMapRequests from '@/components/submission/import/MapRequest';
import laboratoryEdit from '@/components/lookup/laboratory/Edit';
import letterbookBillingOptionsIndex from '@/components/lookup/letterbookbillingoptions/index';
import letterbookBillingOptionsEdit from '@/components/lookup/letterbookbillingoptions/edit';
import letterQuestionIndex from '@/components/documents/letterQuestion/Index';
import letterQuestionEdit from '@/components/documents/letterQuestion/Edit';
import letterSectionIndex from '@/components/documents/letterSection/Index';
import letterSectionEdit from '@/components/documents/letterSection/Edit';
import letterbookDefaultingOptionsIndex from '@/components/lookup/letterbookdefaultingoptions/Index';
import letterbookDefaultingOptionsEdit from '@/components/lookup/letterbookdefaultingoptions/Edit';
import manufacturerGroupIndex from '@/components/lookup/manufacturergroup/Index';
import manufacturerGroupEdit from '@/components/lookup/manufacturergroup/Edit';
import manufacturerGuidelinesEdit from '@/components/lookup/manufacturerguidelines/Edit';
import manufacturerGuidelinesIndex from '@/components/lookup/manufacturerguidelines/Index';
import notificationsIndex from '@/components/user/notifications/Index';
import noteManagerEdit from '@/components/documents/noteManager/Edit';
import noteManagerIndex from '@/components/documents/noteManager/Index';
import ocuitIndex from '@/components/tools/ocuit/Index';
import pdfEmailRulesEdit from '@/components/tools/emailsmanagement/pdfemailrules/Edit';
import platformPiecesIndex from '@/components/lookup/platformpieces/Index';
import platformPiecesEdit from '@/components/lookup/platformpieces/Edit';
import productEdit from '@/components/protrack/product/Edit';
import productIndex from '@/components/protrack/product/Index';
import projectReview from '@/components/protrack/project/projectreview/Index';
import protocolDeficienciesIndex from '@/components/tools/protocoldeficiencies/Index';
import protocolDeficienciesEdit from '@/components/tools/protocoldeficiencies/Edit';
import qaTaskDefinitionsEdit from '@/components/lookup/qataskdefinitions/Edit';
import qaTaskDefinitionsIndex from '@/components/lookup/qataskdefinitions/Index';
import resourceFinder from '@/components/workloadmanager/resourcefinder/Index';
import resourceRequests from '@/components/workloadmanager/resourcerequests/Index';
import review from '@/components/evolution/review/Index';
import specializedTypeIndex from '@/components/lookup/specializedtype/Index';
import specializedTypeEdit from '@/components/lookup/specializedtype/Edit';
import submissionCopyMoveToFile from '@/components/submission/submissioncopymovetofile/Index';
import submissionAdd from '@/components/submission/submission/Add';
import submissionEdit from '@/components/submission/submission/Edit';
import subProjectIndex from '@/components/protrack/project/subprojects/Index';
import tasktemplateEdit from '@/components/lookup/tasktemplate/Edit';
import technicalStandardsEdit from '@/components/lookup/technicalstandards/Edit'
import uploadOptions from '@/components/submission/upload/Index';
import workloadBalancer from '@/components/workloadmanager/workloadbalancer/Index';

Vue.use(Router);

export default new Router({
	base: jsPath.toLowerCase(),
	mode: 'history',
	routes: [
		{
			name: 'about',
			path: '/help/about/',
			component: about,
			children: [
				{ path: '', component: about },
				{ path: 'index', component: about }
			]
		},
		{
			name: 'acumatica-dashboard-index',
			path: '/acumatica/dashboard',
			component: acumaticaDashboardIndex
		},
		{
			name: 'acumatica-feed-error-log-index',
			path: '/acumatica/feederrorlog',
			component: acumaticaFeedErrorLogIndex
		},
		{
			name: 'acumatica-push-project-index',
			path: '/acumatica/pushproject',
			component: acumaticaPushProjectIndex
		},
		{
			name: 'associatedsoftwaretemplates-index',
			path: '/lookups/associatedsoftwaretemplates/',
			component: associatedSoftwareTemplatesIndex
		},
		{
			name: 'associatedsoftwaretemplates-edit',
			path: '/lookups/associatedsoftwaretemplates/',
			component: associatedSoftwareTemplatesEdit,
			children: [
				{ path: 'edit/:id' },
				{ path: 'add' }
			]
		},
		{
			name: 'appFeeIndex',
			path: '/lookups/appfees',
			component: appFeeIndex
		},
		{
			name: 'appFeeEdit',
			path: '/lookups/appfees/edit',
			component: appFeeEdit
		},
		{
			name: 'billingPartyIndex',
			path: '/lookups/billingparties',
			component: billingPartyIndex
		},
		{
			name: 'billingPartyEdit',
			path: '/lookups/billingparties/edit',
			component: billingPartyEdit
		},
		{
			name: 'billingSheetIndex',
			path: '/lookups/billingsheet',
			component: billingSheetIndex
		},
		{
			name: 'billingSheetEdit',
			path: '/lookups/billingsheet/edit',
			component: billingSheetEdit
		},
		{
			name: 'cabinetIndex',
			path: '/lookups/cabinets',
			component: cabinetIndex
		},
		{
			name: 'cabinetEdit',
			path: '/lookups/cabinets/edit',
			component: cabinetEdit
		},
		{
			name: 'clause-report',
			path: '/evolution/clausereport',
			component: clauseReport
		},
		{
			name: 'pending-letters',
			path: '/documents/createvue',
			component: documentsCreateIndex
		},
		{
			path: '/documents/generate/',
			component: documentsGenerateIndex,
			children: [
				{ path: '', component: documentsGenerateIndex },
				{ path: 'index', component: documentsGenerateIndex }
			]
		},
		{
			path: '/documents/conformancecriteria/',
			component: conformanceCriteria,
			children: [
				{ path: '', component: conformanceCriteria },
				{ path: 'index', component: conformanceCriteria }
			]
		},
		{
			name: 'clausetemplatemanagement-index',
			path: '/tools/clausetemplatemanagement',
			component: clauseTemplateManagerIndex
		},
		{
			name: 'clausetemplatemanagement-edit',
			path: '/tools/clausetemplatemanagement/edit',
			component: clauseTemplateManagerEdit
		},
		{
			name: 'emailManageIndex',
			path: '/tools/emailsmanagement',
			component: emailManageIndex
		},
		{
			name: 'emailManageIndex2',
			path: '/tools/emailsmanagement/index',
			component: emailManageIndex
		},
		{
			name: 'formatFiltersIndex',
			path: '/lookups/formatfilters',
			component: formatFiltersIndex
		},
		{
			name: 'formatFiltersEdit',
			path: '/lookups/formatfilters/edit',
			component: formatFiltersEdit
		},
		{
			name: 'game-description-index',
			path: '/documents/gameDescription',
			component: gameDescriptionIndex
		},
		{
			name: 'game-description-edit',
			path: '/documents/gameDescription',
			component: gameDescriptionEdit,
			children: [
				{ path: 'edit' },
				{ path: 'edit/:id' }
			]
		},
		{
			name: 'holiday-edit',
			path: '/lookups/holidays/edit',
			component: holidayEdit
		},
		{
			name: 'holiday-index',
			path: '/lookups/holidays',
			component: holidayIndex
		},
		{
			name: 'laboratory-edit',
			path: '/lookups/laboratories/edit',
			component: laboratoryEdit
		},
		{
			name: 'letterbook-defaulting-index',
			path: '/lookups/letterbookdefaultingoptions',
			component: letterbookDefaultingOptionsIndex
		},
		{
			name: 'letterbook-defaulting-edit',
			path: '/lookups/letterbookdefaultingoptions/edit',
			component: letterbookDefaultingOptionsEdit
		},
		{
			path: '/submission/import/',
			component: importStage,
			children: [
				{ path: '', component: importImporter },
				{ name: 'newSubmissions', path: 'newSubmissions', component: importNewSubmissions },
				{ name: 'transfers', path: 'transfers', component: importTransfers },
				{ name: 'withdrawAndRejects', path: 'withdrawAndRejects', component: importWithdrawAndRejects },
				{ name: 'mapRequests', path: 'mapRequests', component: importMapRequests, props: true },
				{ name: 'requestsRequiringReview', path: 'requestsRequiringReview', component: importRequestsRequiringReview },
				{ name: 'completedSGISubmissions', path: 'completedSGISubmissions', component: importSGICompletedSubmission }
			]
		},
		{
			name: 'incoming-review',
			path: '/protrack/incomingreview/index/:id',
			component: incomingReview
		},
		{
			name: 'jurisdicationAddEdit',
			path: '/submission/jurisdictionaldata/edit',
			component: jurisdicationAddEdit
		},
		{
			name: 'jurisdicationLinkLetter',
			path: '/submission/jurisdictionlinkletter/index',
			component: jurisdicationLinkLetter
		},
		{
			name: 'jurisdicationLinkLetterDraft',
			path: '/submission/jurisdictionlinkletter/linkdrafts',
			component: jurisdicationLinkLetter
		},
		{
			name: 'jurisdicationReplaceLink',
			path: '/submission/jurisdictionreplacelink',
			component: jurisdicationReplaceLink,
			children: [
				{ path: 'index' }
			]
		},
		{
			name: 'letterbookBillingOptions-edit',
			path: '/lookups/letterbookbillingoptions/edit',
			component: letterbookBillingOptionsEdit
		},
		{
			name: 'letterbookBillingOptions-index',
			path: '/lookups/letterbookbillingoptions/index',
			component: letterbookBillingOptionsIndex
		},
		{
			name: 'letterQuestionIndex',
			path: '/documents/letterQuestion/',
			component: letterQuestionIndex
		},
		{
			name: 'letterQuestionEdit',
			path: '/documents/letterQuestion/',
			component: letterQuestionEdit,
			children: [
				{ path: 'edit' },
				{ path: 'edit/:id' }
			]
		},
		{
			name: 'letterSectionIndex',
			path: '/documents/letterSection/',
			component: letterSectionIndex
		},
		{
			name: 'letterSectionEdit',
			path: '/documents/letterSection/',
			component: letterSectionEdit,
			children: [
				{ path: 'edit' },
				{ path: 'edit/:id' }
			]
		},
		{
			name: 'manufacturerGroup-index',
			path: '/lookups/manufacturergroups/',
			component: manufacturerGroupIndex
		},
		{
			name: 'manufacturerGroup-edit',
			path: '/lookups/manufacturergroups/',
			component: manufacturerGroupEdit,
			children: [

				{ path: 'edit/:id' },
				{ path: 'add' }
			]
		},
		{
			name: 'manufacturerGuidelines-index',
			path: '/lookups/manufacturerguidelines',
			component: manufacturerGuidelinesIndex
		},
		{
			name: 'manufacturerGuidelines-edit',
			path: '/lookups/manufacturerguidelines/edit',
			component: manufacturerGuidelinesEdit
		},
		{
			name: 'notifications-index',
			path: '/user/notifications',
			component: notificationsIndex
		},
		{
			name: 'noteManager-index',
			path: '/documents/noteManager/',
			component: noteManagerIndex,
			children: [
				{ path: 'index' }
			]
		},
		{
			name: 'noteManager-edit',
			path: '/documents/noteManager/',
			component: noteManagerEdit,
			children: [
				{ path: 'edit' },
				{ path: 'edit/:id' }
			]
		},
		{
			name: 'ocuit-index',
			path: '/tools/ocuit/',
			component: ocuitIndex,
			children: [
				{ path: 'index' }
			]
		},
		{
			name: 'pdfEmailRulesEdit',
			path: '/tools/emailsmanagement/editpdfrule/:id',
			component: pdfEmailRulesEdit
		},
		{
			name: 'pdfEmailRulesEdit2',
			path: '/tools/emailsmanagement/editpdfrule',
			component: pdfEmailRulesEdit
		},
		{
			name: 'platformpieces-index',
			path: '/lookups/platformpieces/',
			component: platformPiecesIndex
		},
		{
			name: 'platformpieces-edit',
			path: '/lookups/platformpieces/',
			component: platformPiecesEdit,
			children: [
				{ path: 'edit/:id' },
				{ path: 'add' }
			]
		},

		{
			name: 'product-index',
			path: '/protrack/product/',
			component: productIndex
		},
		{
			name: 'product-edit',
			path: '/protrack/product/',
			component: productEdit,
			children: [
				{ path: 'edit/:id' },
			]
		},







		{
			name: 'projectReview',
			path: '/protrack/projectReview',
			component: projectReview
		},
		{
			name: 'protocoldeficiencies-index',
			path: '/tools/protocoldeficiencies/',
			component: protocolDeficienciesIndex
		},
		{
			name: 'protocoldeficiencies-edit',
			path: '/tools/protocoldeficiencies/',
			component: protocolDeficienciesEdit,
			children: [
				{ path: 'edit' },
				{ path: 'add' },
			]
		},
		{
			name: 'protocoldeficiencies-review',
			path: '/tools/protocoldeficiencies/',
			component: protocolDeficienciesEdit,
			children: [
				{ path: 'review' },
			]
		},
		{
			name: 'qaTaskDefinitionsIndex',
			path: '/lookups/qataskdefinitions',
			component: qaTaskDefinitionsIndex
		},
		{
			name: 'qaTaskDefinitionsEdit',
			path: '/lookups/qataskdefinitions/edit',
			component: qaTaskDefinitionsEdit
		},
		{
			name: 'resource-finder',
			path: '/protrack/workloadmanager/resourcefinder',
			component: resourceFinder
		},
		{
			name: 'resource-requests',
			path: '/protrack/workloadmanager/resourcerequests',
			component: resourceRequests
		},
		{
			name: 'review',
			path: '/evolution/review',
			component: review
		},
		{
			name: 'specializedTypeIndex',
			path: '/lookups/specializedtype',
			component: specializedTypeIndex
		},
		{
			name: 'specializedTypeEdit',
			path: '/lookups/specializedtype/edit',
			component: specializedTypeEdit
		},
		{
			name: 'submissionCopyMoveToFile',
			path: '/submission/submissioncopymovetofile',
			component: submissionCopyMoveToFile
		},
		{
			path: '/submission/submission/',
			component: submissionAdd,
			children: [
				{ path: 'add/:id' },
				{ path: 'add' }
			]
		},
		{
			path: '/submission/submission/',
			component: submissionEdit,
			children: [
				{ path: 'edit/:id' },
				{ path: 'edit' },
				{ path: 'clone' }
			]
		},
		{
			name: 'subProjectIndex',
			path: '/protrack/subprojects',
			component: subProjectIndex
		},
		{
			name: 'taskTemplateEdit',
			path: '/lookups/tasktemplates/edit',
			component: tasktemplateEdit
		},
		{
			name: 'technicalStandardsEdit',
			path: '/lookups/technicalstandards/',
			component: technicalStandardsEdit,
			children: [
				{ path: 'edit/:id' },
				{ path: 'edit' }
			]
		},
		{
			name: 'uploadOptions',
			path: '/submission/upload/',
			component: uploadOptions
		},
		{
			name: 'workload-balancer',
			path: '/protrack/workloadmanager/workloadbalancer',
			component: workloadBalancer
		}
	]
});