﻿import Vue from 'vue';
import api from '../../api/acumatica';
import * as types from '../mutation-types';
import { request } from 'https';

const state = {
	unResolved: [],
	showErrorDetailPopUp: false,
	selectedAcumaticaFeedError: {},
	setupProjectsResults: [],
	refreshInProgress: false
};

const getters = {
	unResolved: state => state.unResolved,
	showErrorDetailPopUp: state => state.showErrorDetailPopUp,
	selectedAcumaticaFeedError: state => state.selectedAcumaticaFeedError,
	setupProjectsResults: state => state.setupProjectsResults,
	refreshInProgress: state => state.refreshInProgress
};

const actions = {
	setRefreshInProgress({ commit }, value) {
		commit(types.ACUMATICA_SET_REFRESHINPROGRESS, value);
	},
	setUnResolved({ commit }, unResolved) {
		commit(types.ACUMATICA_SET_UNRESOLVED, unResolved);
	},
	setErrorDetailPopUp({ commit }, show) {
		commit(types.ACUMATICA_SET_ERRORDETAILPOPUP, show);
	},
	setSelectedAcumaticaFeedError({ commit }, id) {
		commit(types.ACUMATICA_SET_SELECTEDACUMATICAFEEDERROR, id);
	},
	postMarkResolved({
		commit,
		state
	}, payload) {
		Loading();
		api.post_MarkResolved(payload)
			.then(response => {
				api.post_GetUnResolvedAcumaticaFeedErrors()
					.then(response => {
						commit(types.ACUMATICA_SET_UNRESOLVED, response.data);
					});
			})
			.catch((error) => { })
			.finally(() => {
				DoneLoading();
			});
	},
	postSetupProjects({ commit }, payload) {
		Loading();
		api.post_SetupProjects(payload)
			.then(response => {
				commit(types.ACUMATICA_SET_SETUPPROJECTSRESULTS, response.data.results);
				api.post_GetUnResolvedAcumaticaFeedErrors()
					.then(response => {
						commit(types.ACUMATICA_SET_UNRESOLVED, response.data);
					});
			})
			.then(() => {
				setTimeout(() => {
					commit(types.ACUMATICA_EMPTY_SETUPPROJECTSRESULTS);
				}, 3000);
			})
			.catch((error) => {
				console.log("postSetupProjects: ", error);
			})
			.finally(() => {
				DoneLoading();
			});
	},
	postGetUnResolvedAcumaticaFeedErrors({ commit }) {
		Loading();
		commit(types.ACUMATICA_SET_REFRESHINPROGRESS, true);
		api.post_GetUnResolvedAcumaticaFeedErrors()
			.then(response => {
				commit(types.ACUMATICA_SET_UNRESOLVED, response.data);
			})
			.catch((error) => { })
			.finally(() => {
				commit(types.ACUMATICA_SET_REFRESHINPROGRESS, false);
				DoneLoading();
			});
	}
};

const mutations = {
	[types.ACUMATICA_SET_UNRESOLVED](state, unResolved) {
		state.unResolved = unResolved;
	},
	[types.ACUMATICA_SET_ERRORDETAILPOPUP](state, show) {
		state.showErrorDetailPopUp = show;
	},
	[types.ACUMATICA_SET_SELECTEDACUMATICAFEEDERROR](state, id) {
		state.selectedAcumaticaFeedError = state.unResolved.find(function (item) { return item.id === id; });
	},
	[types.ACUMATICA_SET_SETUPPROJECTSRESULTS](state, results) {
		state.setupProjectsResults = results;
	},
	[types.ACUMATICA_EMPTY_SETUPPROJECTSRESULTS](state) {
		state.setupProjectsResults = [];
	},
	[types.ACUMATICA_SET_REFRESHINPROGRESS](state, value) {
		state.refreshInProgress = value;
	}
};

export default {
	namespaced: true,
	state,
	getters,
	actions,
	mutations
};