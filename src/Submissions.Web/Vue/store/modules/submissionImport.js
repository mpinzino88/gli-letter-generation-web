﻿import Vue from 'vue';
import api from '../../api/submissionimport'
import * as types from '../mutation-types'
import { request } from 'https';

const state = {
	wdrjReasonList: [],
	manufacturerList: [],
	newSubmissionTemplates: [],
	newSubmissionDuplicateGuardOverride: {},
	stagedItemProcessResult: {
		duplicates: []
	},

	stagedNewSubmissions: [],
	stagedNewSubmissionColumnOptionSet: [],

	completedNewSubmissions: [],
	completedNewSubmissionColumnOptionSet: [],

	selectedStagedNewSubmission: {
		index: 0,
		item: {}
	},
	showStagedNewSubmissionPopup: false,

	stagedTransferRequests: [],
	transferRequestDetail: {
		index: undefined,
		items: [],
		mode: ''
	},
	showTransferRequestDetailPopup: false,
	showStagedRequestMovePopup: false,

	completedTransferRequests: [],
	transferResult: {
		failedTransferRequests: [],
		successfulRequests: []
	},

	stagedWithdrawAndRejectRequests: [],
	wdrjRequestDetail: {
		index: undefined,
		items: [],
		mode: ''
	},
	showWDRJRequestDetailPopup: false,

	completedWithdrawAndRejectRequests: [],
	wdrjResult: {
		successfulRequests: [],
		failedWDRJRequests: [],
	},

	requestsRequiringReview: [],
	manufacturerGroupTileDefinitions: [],
	selectedManufacturerGroupTile: {},
	links: {},
	completedSGISubmissions: [],

	count: {
		inReview: 0,
		completed: 0
	},

	moveStagedRequest: {
		currentQueue: '',
		letterNumber: ''
	}
};

const getters = {
	manufacturerList: state => state.manufacturerList,
	newSubmissionTemplates: state => state.newSubmissionTemplates,
	getStagedItemProcessResult: state => state.stagedItemProcessResult,
	newSubmissionDuplicateGuardOverride: state => state.newSubmissionDuplicateGuardOverride,
	stagedNewSubmissionColumnOptionSet: state => state.stagedNewSubmissionColumnOptionSet,
	showStagedNewSubmissionPopup: state => state.showStagedNewSubmissionPopup,
	selectedStagedNewSubmission: state => state.selectedStagedNewSubmission,
	stagedNewSubmissions: state => state.stagedNewSubmissions,
	completedNewSubmissions: state => state.completedNewSubmissions,
	completedNewSubmissionColumnOptionSet: state => state.completedNewSubmissionColumnOptionSet,
	stagedTransferRequests: state => state.stagedTransferRequests,
	showTransferRequestDetailPopup: state => state.showTransferRequestDetailPopup,
	showStagedRequestMovePopup: state => state.showStagedRequestMovePopup,
	transferRequestDetail: state => state.transferRequestDetail,
	completedTransferRequests: state => state.completedTransferRequests,
	transferResult: state => state.transferResult,
	stagedWithdrawAndRejectRequests: state => state.stagedWithdrawAndRejectRequests,
	completedWithdrawAndRejectRequests: state => state.completedWithdrawAndRejectRequests,
	showWDRJRequestDetailPopup: state => state.showWDRJRequestDetailPopup,
	wdrjRequestDetail: state => state.wdrjRequestDetail,
	wdrjReasonList: state => state.wdrjReasonList,
	wdrjResult: state => state.wdrjResult,
	requestsRequiringReview: state => state.requestsRequiringReview,
	completedSGISubmissions: state => state.completedSGISubmissions,
	manufacturerGroupTileDefinitions: state => state.manufacturerGroupTileDefinitions,
	selectedManufacturerGroupTile: state => state.selectedManufacturerGroupTile,
	links: state => state.links,
	count: state => state.count,
	moveStagedRequest: state => state.moveStagedRequest
};

const actions = {
	setManufacturerList({ commit }, manufacturerList) {
		commit(types.SUBMISSIONIMPORT_SET_MANUFACTURERLIST, manufacturerList);
	},
	setStagedItemProcessResult({ commit, state }, result) {
		commit(types.SUBMISSIONIMPORT_SET_STAGEDIITEMPROCESSRESULT, result);
	},
	setSubmissionTemplateSet({ commit }, templateSet) {
		commit(types.SUBMISSIONIMPORT_SET_SUBMISSIONTEMPLATES, templateSet);
	},
	setStagedNewSubmissions({ commit }, stagedNewSubmissions) {
		commit(types.SUBMISSIONIMPORT_SET_STAGEDNEWSUBMISSIONS, stagedNewSubmissions);
	},
	deleteStagedNewSubmission({ commit, state }, index) {
		let forDeletion = state.stagedNewSubmissions[index]
		Loading();
		return api.deleteStagedNewSubmission(forDeletion.letterNumber)
			.then(response => {
				commit(types.SUBMISSIONIMPORT_DELETE_STAGEDNEWSUBMISSION, index);
			})
			.catch((error) => {

			})
			.finally(() => {
				DoneLoading();
			});
	},
	test({ commit, state }, itemWrapper) {
		Loading();
		api.addRecordToExistingSubmission(itemWrapper.item)
			.finally(() => {
				DoneLoading();
			});
	},
	createNewSubmission({ commit, state }, itemWrapper) {
		Loading();
		commit(types.SUBMISSIONIMPORT_SET_NEWSUBMISSIONDUPLICATEGUARDOVERRIDE, itemWrapper.item);

		if (itemWrapper.item.addToExistingSubmission) {
			api.addRecordToExistingSubmission(itemWrapper.item)
				.then(response => {
					api.getRecentlyCompletedNewSubmissions()
						.then(response => {
							commit(types.SUBMISSIONIMPORT_SET_COMPLETEDNEWSUBMISSIONS, response.data);
						});

					commit(types.SUBMISSIONIMPORT_DELETE_STAGEDNEWSUBMISSION, itemWrapper.index);
					commit(types.SUBMISSIONIMPORT_SET_STAGEDIITEMPROCESSRESULT, response.data);
					commit(types.SUBMISSIONIMPORT_SET_STAGEDIITEMPROCESSRESULT, { duplicates: [] });
					commit(types.SUBMISSIONIMPORT_SET_NEWSUBMISSIONDUPLICATEGUARDOVERRIDE, {});
				})
				.catch((error) => {
					let responseData = error.response.data;

					// -- .NET error.
					if (!responseData.hasOwnProperty('duplicates')) {
						responseData = {
							overallResult: false,
							gpsUploadFailed: false,
							fileNumber: '',
							errorMessage: 'An error occured while processing this request.',
							stagedItemType: 'New Submission',
							duplicates: []
						};
					}

					commit(types.SUBMISSIONIMPORT_SET_STAGEDIITEMPROCESSRESULT, responseData);

					if (responseData.duplicates.length === 0) {
						setTimeout(() => {
							commit(types.SUBMISSIONIMPORT_SET_STAGEDIITEMPROCESSRESULT, { duplicates: [] });
							commit(types.SUBMISSIONIMPORT_SET_NEWSUBMISSIONDUPLICATEGUARDOVERRIDE, {});
						}, 10000);
					}
				})
				.finally(() => {
					DoneLoading();
				});
		}
		else {

			api.createNewSubmission(itemWrapper.item)
				.then(response => {
					api.getRecentlyCompletedNewSubmissions()
						.then(response => {
							commit(types.SUBMISSIONIMPORT_SET_COMPLETEDNEWSUBMISSIONS, response.data);
						});

					commit(types.SUBMISSIONIMPORT_DELETE_STAGEDNEWSUBMISSION, itemWrapper.index);
					commit(types.SUBMISSIONIMPORT_SET_STAGEDIITEMPROCESSRESULT, response.data);
					setTimeout(() => {
						commit(types.SUBMISSIONIMPORT_SET_STAGEDIITEMPROCESSRESULT, { duplicates: [] });
						commit(types.SUBMISSIONIMPORT_SET_NEWSUBMISSIONDUPLICATEGUARDOVERRIDE, {});
					}, 10000);
				})
				.catch((error) => {
					let responseData = error.response.data;

					// -- .NET error.
					if (!responseData.hasOwnProperty('duplicates')) {
						responseData = {
							overallResult: false,
							gpsUploadFailed: false,
							fileNumber: '',
							errorMessage: 'An error occured while processing this request.',
							stagedItemType: 'New Submission',
							duplicates: []
						};
					}

					commit(types.SUBMISSIONIMPORT_SET_STAGEDIITEMPROCESSRESULT, responseData);

					if (responseData.duplicates.length === 0) {
						setTimeout(() => {
							commit(types.SUBMISSIONIMPORT_SET_STAGEDIITEMPROCESSRESULT, { duplicates: [] });
							commit(types.SUBMISSIONIMPORT_SET_NEWSUBMISSIONDUPLICATEGUARDOVERRIDE, {});
						}, 10000);
					}
				})
				.finally(() => {
					DoneLoading();
				});
		}
	},
	setStagedNewSubmissionColumnOptionSet({ commit }, options) {
		commit(types.SUBMISSIONIMPORT_SET_STAGEDNEWSUBMISSIONCOLUMNOPTIONS, options);
	},
	setNewSubmissionDuplicateGuardOverride({ commit }, newSubmission) {
		commit(types.SUBMISSIONIMPORT_SET_NEWSUBMISSIONDUPLICATEGUARDOVERRIDE, newSubmission);
	},
	setStagedNewSubmissionPopup({ commit }, show) {
		commit(types.SUBMISSIONIMPORT_SET_STAGEDNEWSUBMISSIONPOPUP, show);
	},
	setNewSubmissionChildrenVisibilityFlag({ commit }, options) {
		commit(types.SUBMISSIONIMPORT_SET_NEWSUBMISSIONCHILDRENVISIBILITYFLAG, options);
	},
	setSelectedStagedNewSubmission({ commit }, index) {
		commit(types.SUBMISSIONIMPORT_SET_SELECTEDSTAGEDNEWSUBMISSION, index);
	},
	setCompletedNewSubmissions({ commit }, completedNewSubmissions) {
		commit(types.SUBMISSIONIMPORT_SET_COMPLETEDNEWSUBMISSIONS, completedNewSubmissions);
	},
	setCompletedNewSubmissionColumnOptionSet({ commit }, options) {
		commit(types.SUBMISSIONIMPORT_SET_COMPLETEDNEWSUBMISSIONCOLUMNOPTIONS, options);
	},
	fetchStagedNewSubmissions({ commit, state }) {
		Loading();
		api.fetchStagedNewSubmissions()
			.then(response => {
				commit(types.SUBMISSIONIMPORT_SET_STAGEDNEWSUBMISSIONS, response.data);
			})
			.catch((error) => {

			})
			.finally(() => {
				DoneLoading();
			});
	},
	setStagedTransferRequests({ commit }, stagedTransferRequests) {
		commit(types.SUBMISSIONIMPORT_SET_STAGEDTRANSFERREQUESTS, stagedTransferRequests);
	},
	deleteTransferRequest({ commit, state }, options) {
		let forDeletion = state.stagedTransferRequests[options.index].children[options.childIndex];
		Loading();
		return api.deleteTransferRequest(forDeletion.id)
			.then(response => {
				api.fetchStagedTransferRequests()
					.then(response => {
						commit(types.SUBMISSIONIMPORT_SET_STAGEDTRANSFERREQUESTS, response.data);
					});
			})
			.catch((error) => {

			})
			.finally(() => {
				DoneLoading();
			});
	},
	fetchStagedTransferRequests({ commit, state }) {
		Loading();
		api.fetchStagedTransferRequests()
			.then(response => {
				commit(types.SUBMISSIONIMPORT_SET_STAGEDTRANSFERREQUESTS, response.data);
			})
			.catch((error) => {

			})
			.finally(() => {
				DoneLoading();
			});
	},
	createTransferRequests({ commit, state }, selectedStagedTransferRequests) {
		Loading();
		api.createTransferRequests(selectedStagedTransferRequests)
			.then(response => {
				commit(types.SUBMISSIONIMPORT_SET_TRANSFERRESULT, response.data);
				setTimeout(() => {
					commit(types.SUBMISSIONIMPORT_SET_TRANSFERRESULT, {
						failedTransferRequests: [],
						successfulRequests: []
					});
				}, 10000);
				api.fetchStagedTransferRequests()
					.then(response => {
						commit(types.SUBMISSIONIMPORT_SET_STAGEDTRANSFERREQUESTS, response.data);
						api.getRecentlyCompletedTransfers()
							.then(response => {
								commit(types.SUBMISSIONIMPORT_SET_COMPLETEDTRANSFERS, response.data);
							});
					});
			})
			.catch((error) => {
				commit(types.SUBMISSIONIMPORT_SET_TRANSFERRESULT, error.response.data);
				setTimeout(() => {
					commit(types.SUBMISSIONIMPORT_SET_TRANSFERRESULT, {
						failedTransferRequests: [],
						successfulRequests: []
					});
				}, 10000);
			})
			.finally(() => {
				DoneLoading();
			});
	},
	setTransferPackageChildrenVisibilityFlag({ commit }, options) {
		commit(types.SUBMISSIONIMPORT_SET_TRANSFERPACKAGECHILDRENVISIBILITYFLAG, options);
	},
	setTransferRequestDetailPopup({ commit }, show) {
		commit(types.SUBMISSIONIMPORT_SET_TRANSFERREQUESTDETAILPOPUP, show);
	},
	setStagedRequestMovePopup({ commit }, show) {
		commit(types.SUBMISSIONIMPORT_SET_STAGEDREQUESTMOVEPOPUP, show);
	},
	setTransferRequestDetail({ commit }, options) {
		commit(types.SUBMISSIONIMPORT_SET_TRANSFERREQUESTDETAIL, options);
	},
	setMoveStagedRequest({ commit }, stagedRequest) {
		commit(types.SUBMISSIONIMPORT_SET_MOVESTAGEDREQUEST, stagedRequest);
	},
	updateLocalTransferRequest({ commit, state }, updatedTransferRequest) {
		commit(types.SUBMISSIONIMPORT_UPDATE_LOCALTRANSFERREQUEST, updatedTransferRequest);
	},
	deleteTransferPackageRequest({ commit, state }, options) {
		let forDeletion = state.stagedTransferRequests[options.index];
		Loading();
		return api.deleteTransferPackageRequest(forDeletion.letterNumber)
			.then(response => {
				api.fetchStagedTransferRequests()
					.then(response => {
						commit(types.SUBMISSIONIMPORT_SET_STAGEDTRANSFERREQUESTS, response.data);
					});
			})
			.catch((error) => {

			})
			.finally(() => {
				DoneLoading();
			});
	},
	setCompletedTransfers({ commit }, completedTransfers) {
		commit(types.SUBMISSIONIMPORT_SET_COMPLETEDTRANSFERS, completedTransfers);
	},
	fetchCompletedTransfers({ commit, state }) {
		Loading();
		api.getRecentlyCompletedTransfers()
			.then(response => {
				commit(types.SUBMISSIONIMPORT_SET_COMPLETEDTRANSFERS, response.data);
			})
			.catch((error) => {

			})
			.finally(() => {
				DoneLoading();
			});
	},
	setTransferResult({ commit, state }, result) {
		commit(types.SUBMISSIONIMPORT_SET_TRANSFERRESULT, result);
	},
	fetchStagedWDRJRequests({ commit, state }) {
		Loading();
		api.fetchStagedWDRJRequests()
			.then(response => {
				commit(types.SUBMISSIONIMPORT_SET_STAGEDWDRJREQUESTS, response.data);
			})
			.catch((error) => {

			})
			.finally(() => {
				DoneLoading();
			});
	},
	fetchCompletedNewSubmissions({ commit, state }) {
		Loading();
		api.getRecentlyCompletedNewSubmissions()
			.then(response => {
				commit(types.SUBMISSIONIMPORT_SET_COMPLETEDNEWSUBMISSIONS, response.data);
			})
			.catch((error) => {

			})
			.finally(() => {
				DoneLoading();
			});
	},
	fetchCompletedWDRJRequests({ commit, state }) {
		Loading();
		api.getRecentlyCompletedWDRJRequests()
			.then(response => {
				commit(types.SUBMISSIONIMPORT_SET_COMPLETEDWDRJREQUESTS, response.data);
			})
			.catch((error) => {

			})
			.finally(() => {
				DoneLoading();
			});
	},
	setWDRJPackageChildrenVisibilityFlag({ commit }, options) {
		commit(types.SUBMISSIONIMPORT_SET_WDRJPACKAGECHILDRENVISIBILITYFLAG, options);
	},
	setWDRJRequestDetail({ commit }, options) {
		commit(types.SUBMISSIONIMPORT_SET_WDRJREQUESTDETAIL, options);
	},
	setWDRJRequestDetailPopup({ commit }, show) {
		commit(types.SUBMISSIONIMPORT_SET_WDRJREQUESTDETAILPOPUP, show);
	},
	updateLocalWDRJRequest({ commit, state }, updatedWDRJRequest) {
		commit(types.SUBMISSIONIMPORT_UPDATE_LOCALWDRJREQUEST, updatedWDRJRequest);
	},
	deleteWDRJRequest({ commit, state }, options) {
		let forDeletion = state.stagedWithdrawAndRejectRequests[options.index].children[options.childIndex];
		Loading();
		return api.deleteWDRJRequest(forDeletion.id)
			.then(response => {
				api.fetchStagedWDRJRequests()
					.then(response => {
						commit(types.SUBMISSIONIMPORT_SET_STAGEDWDRJREQUESTS, response.data);
					});
			})
			.catch((error) => {

			})
			.finally(() => {
				DoneLoading();
			});
	},
	deleteWDRJPackageRequest({ commit, state }, options) {
		let forDeletion = state.stagedWithdrawAndRejectRequests[options.index];
		Loading();
		return api.deleteWDRJPackageRequest(forDeletion.letterNumber)
			.then(response => {
				api.fetchStagedWDRJRequests()
					.then(response => {
						commit(types.SUBMISSIONIMPORT_SET_STAGEDWDRJREQUESTS, response.data);
					});
			})
			.catch((error) => {

			})
			.finally(() => {
				DoneLoading();
			});
	},
	setWDRJReasons({ commit }, wdrjReasons) {
		commit(types.SUBMISSIONIMPORT_SET_WDRJREASONS, wdrjReasons);
	},
	setStagedWDRJRequests({ commit }, stagedWDRJRequests) {
		commit(types.SUBMISSIONIMPORT_SET_STAGEDWDRJREQUESTS, stagedWDRJRequests);
	},
	setCompletedWDRJRequests({ commit }, completedWDRJRequests) {
		commit(types.SUBMISSIONIMPORT_SET_COMPLETEDWDRJREQUESTS, completedWDRJRequests);
	},
	processWDRJRequests({ commit, state }, selectedStagedWDRJRequests) {
		Loading();
		api.processWDRJRequests(selectedStagedWDRJRequests)
			.then(response => {
				commit(types.SUBMISSIONIMPORT_SET_WDRJRESULT, response.data);
				setTimeout(() => {
					commit(types.SUBMISSIONIMPORT_SET_WDRJRESULT, {
						successfulRequests: [],
						failedWDRJRequests: []
					});
				}, 10000);
				api.fetchStagedWDRJRequests()
					.then(response => {
						commit(types.SUBMISSIONIMPORT_SET_STAGEDWDRJREQUESTS, response.data);
						api.getRecentlyCompletedWDRJRequests()
							.then(response => {
								commit(types.SUBMISSIONIMPORT_SET_COMPLETEDWDRJREQUESTS, response.data);
							});
					});
			})
			.catch((error) => {
				commit(types.SUBMISSIONIMPORT_SET_WDRJRESULT, error.response.data);
				setTimeout(() => {
					commit(types.SUBMISSIONIMPORT_SET_WDRJRESULT, {
						successfulRequests: [],
						failedWDRJRequests: []
					});
				}, 10000);
			})
			.finally(() => {
				DoneLoading();
			});
	},
	updateAmbiguousMapping({ commit, state }, options) {
		commit(types.SUBMISSIONIMPORT_UPDATE_AMBIGUOUSMAPPINGFORREQUEST, options);
	},
	setRequestsRequiringReview({ commit }, requestsRequiringReview) {
		commit(types.SUBMISSIONIMPORT_SET_REQUESTSREQUIRINGREVIEW, requestsRequiringReview);
	},
	fetchRequestsRequiringReview({ commit, state }) {
		api.fetchRequestsRequiringReview()
			.then(response => {	commit(types.SUBMISSIONIMPORT_SET_REQUESTSREQUIRINGREVIEW, response.data); })
			.catch((error) => {})
			.finally(() => { DoneLoading(); });
	},
	fetchRequestsRequiringReviewCount({ commit, state }) {
		api.fetchRequestsRequiringReviewCount()
			.then(response => {
				commit(types.SUBMISSIONIMPORT_SET_REQUESTSREQUIRINGREVIEWCOUNT, response.data);
			});
	},
	fetchSGICompletedCount({ commit, state }) {
		Loading();
		api.fetchSGICompletedCount()
			.then(response => {
				commit(types.SUBMISSIONIMPORT_SET_SGICOMPLETEDCOUNT, response.data);
			});
	},
	fetchSGICompletedSubmissions({ commit, state }) {
		Loading();
		api.fetchCompletedSGISubmissions()
			.then(response => {
				commit(types.SUBMISSIONIMPORT_SET_COMPLETEDSUBMISSIONSGI, response.data);
			})
			.catch((error) => {

			})
			.finally(() => {
				DoneLoading();
			});
	},
	setSelectedManufacturerGroupTile({ commit }, selectedManufacturerGroupTile) {
		commit(types.SUBMISSIONIMPORT_SET_SELECTEDMANUFACTURERGROUPTILE, selectedManufacturerGroupTile);
	},
	setManufacturerGroupTileDefinitions({ commit }, manufacturerGroupTileDefinitions) {
		commit(types.SUBMISSIONIMPORT_SET_MANUFACTURERGROUPTILEDEFINITIONS, manufacturerGroupTileDefinitions);
	},
	populateLinks({ commit }, links) {
		commit(types.SUBMISSIONIMPORT_SET_LINKS, links)
	}
};

const mutations = {
	[types.SUBMISSIONIMPORT_SET_MANUFACTURERLIST](state, manufacturerList) {
		state.manufacturerList = manufacturerList;
	},
	[types.SUBMISSIONIMPORT_SET_STAGEDNEWSUBMISSIONS](state, stagedNewSubmissions) {
		state.stagedNewSubmissions = stagedNewSubmissions;
	},
	[types.SUBMISSIONIMPORT_SET_STAGEDNEWSUBMISSIONCOLUMNOPTIONS](state, options) {
		state.stagedNewSubmissionColumnOptionSet = options;
	},
	[types.SUBMISSIONIMPORT_SET_STAGEDNEWSUBMISSIONPOPUP](state, show) {
		state.showStagedNewSubmissionPopup = show;
	},
	[types.SUBMISSIONIMPORT_SET_NEWSUBMISSIONCHILDRENVISIBILITYFLAG](state, options) {
		switch (options.type) {
			case 'pending':
				state.stagedNewSubmissions[options.index].childrenVisible = !state.stagedNewSubmissions[options.index].childrenVisible;
				break;
			case 'completed':
				state.completedNewSubmissions[options.index].childrenVisible = !state.completedNewSubmissions[options.index].childrenVisible;
				break;
			default:
				break;
		}
	},
	[types.SUBMISSIONIMPORT_SET_SELECTEDSTAGEDNEWSUBMISSION](state, index) {
		state.selectedStagedNewSubmission =
			{
				index: index,
				item: state.stagedNewSubmissions[index]
			};
	},
	[types.SUBMISSIONIMPORT_SET_STAGEDIITEMPROCESSRESULT](state, result) {
		state.stagedItemProcessResult = result;
	},
	[types.SUBMISSIONIMPORT_DELETE_STAGEDNEWSUBMISSION](state, index) {
		state.stagedNewSubmissions.splice(index, 1);
	},
	[types.SUBMISSIONIMPORT_SET_COMPLETEDNEWSUBMISSIONS](state, completedNewSubmissions) {
		state.completedNewSubmissions = completedNewSubmissions;
	},
	[types.SUBMISSIONIMPORT_SET_COMPLETEDNEWSUBMISSIONCOLUMNOPTIONS](state, options) {
		state.completedNewSubmissionColumnOptionSet = options;
	},
	[types.SUBMISSIONIMPORT_SET_SUBMISSIONTEMPLATES](state, templates) {
		state.newSubmissionTemplates = templates;
	},
	[types.SUBMISSIONIMPORT_SET_STAGEDTRANSFERREQUESTS](state, stagedTransferRequests) {
		state.stagedTransferRequests = stagedTransferRequests;
	},
	[types.SUBMISSIONIMPORT_SET_NEWSUBMISSIONDUPLICATEGUARDOVERRIDE](state, newSubmission) {
		state.newSubmissionDuplicateGuardOverride = newSubmission;
	},
	[types.SUBMISSIONIMPORT_SET_TRANSFERREQUESTDETAILPOPUP](state, show) {
		state.showTransferRequestDetailPopup = show;
	},
	[types.SUBMISSIONIMPORT_SET_STAGEDREQUESTMOVEPOPUP](state, show) {
		state.showStagedRequestMovePopup = show;
	},
	[types.SUBMISSIONIMPORT_SET_TRANSFERREQUESTDETAIL](state, options) {
		const target = options.mode === 'single' ? state.stagedTransferRequests[options.index].children[options.childIndex] : {};

		state.transferRequestDetail =
			{
				index: options.index,
				childIndex: options.childIndex,
				item: target,
				jurisdictionCount: state.stagedTransferRequests[options.index].children.filter((item) => { return item.selected }).length,
				letterNumber: options.letterNumber,
				mode: options.mode
			};
	},
	[types.SUBMISSIONIMPORT_SET_MOVESTAGEDREQUEST](state, stagedRequest) {
		state.moveStagedRequest =
		{
			currentQueue: stagedRequest.currentQueue,
			letterNumber: stagedRequest.letterNumber
		};
	},
	[types.SUBMISSIONIMPORT_UPDATE_LOCALTRANSFERREQUEST](state, updatedTransferRequest) {
		let transferPackage;
		transferPackage = state.stagedTransferRequests.filter((item) => { return item.letterNumber === updatedTransferRequest.letterNumber })[0];

		if (updatedTransferRequest.mode === 'multiple') {
			transferPackage.children.filter((item) => { return item.selected }).forEach(
				(transfer) => {
					transfer.testLab = updatedTransferRequest.item.testLab || transfer.testLab;
					transfer.testLabId = updatedTransferRequest.item.testLabId || transfer.testLabId;
					transfer.receivedDate = updatedTransferRequest.item.receivedDate || transfer.receivedDate;
					transfer.requestDate = updatedTransferRequest.item.requestDate || transfer.requestDate;
					transfer.workOrderNumber = updatedTransferRequest.item.workOrderNumber || transfer.workOrderNumber;
				});
		}
		else {
			transferPackage.children[updatedTransferRequest.childIndex].testLab = updatedTransferRequest.item.testLab;
			transferPackage.children[updatedTransferRequest.childIndex].testLabId = updatedTransferRequest.item.testLabId;
			transferPackage.children[updatedTransferRequest.childIndex].receivedDate = updatedTransferRequest.item.receivedDate;
			transferPackage.children[updatedTransferRequest.childIndex].requestDate = updatedTransferRequest.item.requestDate;
			transferPackage.children[updatedTransferRequest.childIndex].workOrderNumber = updatedTransferRequest.item.workOrderNumber;
		}
	},
	[types.SUBMISSIONIMPORT_SET_TRANSFERPACKAGECHILDRENVISIBILITYFLAG](state, options) {
		switch (options.type) {
			case 'staged':
				state.stagedTransferRequests[options.index].childrenVisible = !state.stagedTransferRequests[options.index].childrenVisible;
				break;
			case 'completed':
				state.completedTransferRequests[options.index].childrenVisible = !state.completedTransferRequests[options.index].childrenVisible;
				break;
			default:
				break;
		}
	},
	[types.SUBMISSIONIMPORT_SET_COMPLETEDTRANSFERS](state, completedTransfers) {
		state.completedTransferRequests = completedTransfers;
	},
	[types.SUBMISSIONIMPORT_SET_TRANSFERRESULT](state, result) {
		state.transferResult = result;
	},
	[types.SUBMISSIONIMPORT_SET_STAGEDWDRJREQUESTS](state, stagedWDRJRequests) {
		state.stagedWithdrawAndRejectRequests = stagedWDRJRequests;
	},
	[types.SUBMISSIONIMPORT_SET_COMPLETEDWDRJREQUESTS](state, completedWDRJRequests) {
		state.completedWithdrawAndRejectRequests = completedWDRJRequests;
	},
	[types.SUBMISSIONIMPORT_SET_WDRJPACKAGECHILDRENVISIBILITYFLAG](state, options) {
		switch (options.type) {
			case 'staged':
				state.stagedWithdrawAndRejectRequests[options.index].childrenVisible = !state.stagedWithdrawAndRejectRequests[options.index].childrenVisible;
				break;
			case 'completed':
				state.completedWithdrawAndRejectRequests[options.index].childrenVisible = !state.completedWithdrawAndRejectRequests[options.index].childrenVisible;
				break;
			default:
				break;
		}
	},
	[types.SUBMISSIONIMPORT_SET_WDRJREQUESTDETAIL](state, options) {
		const target = options.mode === 'single' ? state.stagedWithdrawAndRejectRequests[options.index].children[options.childIndex] : {};

		state.wdrjRequestDetail =
			{
				index: options.index,
				childIndex: options.childIndex,
				item: target,
				jurisdictionCount: state.stagedWithdrawAndRejectRequests[options.index].children.filter((item) => { return item.selected }).length,
				letterNumber: options.letterNumber,
				mode: options.mode
			};
	},
	[types.SUBMISSIONIMPORT_SET_WDRJREQUESTDETAILPOPUP](state, show) {
		state.showWDRJRequestDetailPopup = show;
	},
	[types.SUBMISSIONIMPORT_UPDATE_LOCALWDRJREQUEST](state, updatedWDRJRequest) {
		let wdrjPackage;
		wdrjPackage = state.stagedWithdrawAndRejectRequests.filter((item) => { return item.letterNumber === updatedWDRJRequest.letterNumber })[0];

		if (updatedWDRJRequest.mode === 'multiple') {
			wdrjPackage.children.filter((item) => { return item.selected }).forEach(
				(wdrjRequest) => {
					wdrjRequest.closeDate = updatedWDRJRequest.item.closeDate || wdrjRequest.closeDate;
					wdrjRequest.selectedStatus = updatedWDRJRequest.item.selectedStatus || wdrjRequest.selectedStatus;
					wdrjRequest.wdrjReason = updatedWDRJRequest.item.wdrjReason || wdrjRequest.wdrjReason;
				});
		}
		else {
			wdrjPackage.children[updatedWDRJRequest.childIndex].closeDate = updatedWDRJRequest.item.closeDate;
			wdrjPackage.children[updatedWDRJRequest.childIndex].selectedStatus = updatedWDRJRequest.item.selectedStatus;
			wdrjPackage.children[updatedWDRJRequest.childIndex].wdrjReason = updatedWDRJRequest.item.wdrjReason;
		}
	},
	[types.SUBMISSIONIMPORT_SET_WDRJREASONS](state, wdrjReasonList) {
		state.wdrjReasonList = wdrjReasonList;
	},
	[types.SUBMISSIONIMPORT_SET_WDRJRESULT](state, result) {
		state.wdrjResult = result;
	},
	[types.SUBMISSIONIMPORT_UPDATE_AMBIGUOUSMAPPINGFORREQUEST](state, options) {
		let request;

		switch (options.requestType) {
			case 'transfer':
				request = state.stagedTransferRequests[options.requestIndex];
				break;
			case 'withdraw':
				request = state.stagedWithdrawAndRejectRequests[options.requestIndex];
				break;
			default:
				break;
		};

		let material = request.children[options.mappedSelectionOptions.transferRequestKeys.childIndex].material;

		request.children.filter(ambiguousMapResolver, material)
			.forEach(x => {
				x.submissionId = options.mappedSelectionOptions.id || null;
				x.mappedSubmissionId = options.mappedSelectionOptions.id || null;
				x.fileNumber = options.mappedSelectionOptions.fileNumber || null;
				x.idNumber = options.mappedSelectionOptions.idNumber || null;
				x.gameName = options.mappedSelectionOptions.gameName || null;
				x.version = options.mappedSelectionOptions.version || null;
				x.dateCode = options.mappedSelectionOptions.dateCode || null;
			});
	},
	[types.SUBMISSIONIMPORT_SET_REQUESTSREQUIRINGREVIEW](state, requestsRequiringReview) {
		state.count.inReview = requestsRequiringReview.length;
		state.requestsRequiringReview = [];
		state.requestsRequiringReview = requestsRequiringReview;
	},
	[types.SUBMISSIONIMPORT_SET_COMPLETEDSUBMISSIONSGI](state, completedSubmissions) {
		state.completedSGISubmissions = completedSubmissions;
		state.count.completed = completedSubmissions.length;
	},
	[types.SUBMISSIONIMPORT_SET_SELECTEDMANUFACTURERGROUPTILE](state, selectedManufacturerGroupTile) {
		state.selectedManufacturerGroupTile = selectedManufacturerGroupTile;
	},
	[types.SUBMISSIONIMPORT_SET_MANUFACTURERGROUPTILEDEFINITIONS](state, manufacturerGroupTileDefinitions) {
		state.manufacturerGroupTileDefinitions = manufacturerGroupTileDefinitions;
	},
	[types.SUBMISSIONIMPORT_SET_REQUESTSREQUIRINGREVIEWCOUNT](state, requestsRequiringReviewCount) {
		state.count.inReview = requestsRequiringReviewCount;
	},
	[types.SUBMISSIONIMPORT_SET_SGICOMPLETEDCOUNT](state, completedCount) {
		state.count.completed = completedCount;
	},
	[types.SUBMISSIONIMPORT_SET_LINKS](state, links) {
		state.links = links
	}
};

function ambiguousMapResolver(request) {
	return request.material == this;
}

export default {
	namespaced: true,
	state,
	getters,
	actions,
	mutations
};