﻿import Vue from 'vue';

const state = {
	showPopup: false,
	sourceId: null
};

const getters = {
	showPopup: state => state.showPopup,
	sourceId: state => state.sourceId
};

const actions = {
	setYear: ({ commit }, payload) => {
		commit('setYear', payload);
	},
	setShowPopup: ({ commit }, show) => {
		commit('setShowPopup', show);
	},
	setSourceId: ({ commit }, sourceId) => {
		commit('setSourceId', sourceId);
	}
};

const mutations = {
	setShowPopup: (state, show) => {
		state.showPopup = show;
	},
	setSourceId: (state, sourceId) => {
		state.sourceId = sourceId;
	}
};

export default {
	namespaced: true,
	state,
	getters,
	actions,
	mutations
};