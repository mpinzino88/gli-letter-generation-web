﻿import Vue from 'vue'
import api from '../../api/workloadmanager'
import * as types from '../mutation-types'
import lodash from 'lodash'

const taskEnum = { FREE: 0, OH: 1, SCHEDULED: 2, LENTORBORROWED: 3, DTO: 4, HOLIDAY: 5, WEEKEND: 6, NOWORK: 7, INTEROFFICEASSIGNMENT: 8 };
const taskRank = ["free", "oh", "scheduled", "lentorborrowed", "dto", "holiday", "weekend", "nowork", "interofficeassignment"];
const taskListSortConfig = { sortKey: 'estimateStartDate', sortOrders: { estimateStartDate: 'asc' } };

const state = {
	calendarOptions: {},
	changeEstimateTasks: [],
	checkAll: false,
	checkedTasks: [],
	checkedUnassignedTasks: [],
	dtoRequests: {},
	favorites: [],
	filters: {},
	holidays: {},
	mainFilter: {},
	resources: [],
	selectedUser: { id: null, name: '' },
	selectedUserTasks: [],
	showChangeEstimatePopup: false,
	showFavoritesPopup: false,
	showFiltersPopup: false,
	showResourcePopup: false,
	showResourceUnavailabilityPopup: false,
	showUnassignedPopup: false,
	tasks: [],
	taskListSortConfig: taskListSortConfig
}

const getters = {
	calendarOptions: state => state.calendarOptions,
	changeEstimateTasks: state => state.changeEstimateTasks,
	checkAll: state => state.checkAll,
	checkedTasks: state => state.checkedTasks,
	checkedUnassignedTasks: state => state.checkedUnassignedTasks,
	dtoRequests: state => state.dtoRequests,
	externalResources: (state, getters) => {
		return lodash.sortBy(getters.resourceData.filter(x => x.user.type === 'external'), (x) => { return x.user.name });
	},
	favoriteResources: (state, getters) => {
		return lodash.sortBy(getters.resourceData.filter(x => x.user.type === 'favorite'), (x) => { return x.user.name });
	},
	favorites: state => state.favorites,
	filters: state => state.filters,
	holidays: state => state.holidays,
	mainFilter: state => state.mainFilter,
	resources: state => state.resources,
	resourceData: (state, getters) => {
		const calendarStartDate = moment().format('l');
		const calendarDays = getters.calendarOptions.days;

		const resources = state.resources.map(x => {
			return {
				user: x,
				days: []
			};
		});

		const resourceData = [];
		for (let i = 0; i < resources.length; i++) {
			const workdayHours = resources[i].user.workdayHours;
			const workdaySchedule = resources[i].user.workdaySchedule;
			const resourceTasks = state.tasks.filter(x => x.userId === resources[i].user.id).map(x => ({ userId: x.userId, onHold: x.onHold, weekendWork: x.weekendWork, lentOrBorrowed: x.lentOrBorrowed, hours: x.remainingHours }));
			const holidays = state.holidays.filter(x => x.locationId === resources[i].user.locationId).map(x => moment(x.date));
			const dtoRequests = state.dtoRequests.filter(x => x.userId === resources[i].user.id).map(x => ({ date: moment(x.date), hours: x.hours }));

			let unavailable = null;
			if (resources[i].user.unavailableStartDate) {
				unavailable = {
					startDate: moment(resources[i].user.unavailableStartDate),
					endDate: resources[i].user.unavailableEndDate ? moment(resources[i].user.unavailableEndDate) : moment().add(1, 'y'),
					interofficeAssignment: resources[i].user.interofficeAssignment
				};
			}

			let totalHours = {
				lentOrBorrowed: resourceTasks.filter(x => x.onHold === 0 && !x.weekendWork && x.lentOrBorrowed).map(x => x.hours).reduce((a, b) => a + b, 0),
				scheduled: resourceTasks.filter(x => x.onHold === 0 && !x.weekendWork && !x.lentOrBorrowed).map(x => x.hours).reduce((a, b) => a + b, 0),
				lentOrBorrowedWeekend: resourceTasks.filter(x => x.onHold === 0 && x.weekendWork && x.lentOrBorrowed).map(x => x.hours).reduce((a, b) => a + b, 0),
				scheduledWeekend: resourceTasks.filter(x => x.onHold === 0 && x.weekendWork && !x.lentOrBorrowed).map(x => x.hours).reduce((a, b) => a + b, 0),
				onHold: resourceTasks.filter(x => x.onHold === 1).map(x => x.hours).reduce((a, b) => a + b, 0),
				dto: 0,
				workday: 0
			};

			const days = [];
			for (let d = 0; d < calendarDays; d++) {
				const dayDate = moment(calendarStartDate).add(d, 'd');
				const dayDateWk = moment(dayDate).format('dd');
				const dayOfWeek = moment(dayDate).day();

				// only get dtoRequest if remaining dto hours is 0 (dtoHours can span multiple days)
				if (totalHours.dto === 0) {
					const dtoRequest = dtoRequests.find(x => x.date.isSame(dayDate));
					totalHours.dto = dtoRequest ? dtoRequest.hours : 0;
				}

				totalHours.workday = workdaySchedule.find(x => x.dayOfWeek === dayOfWeek).hours;

				let scheduledHoursOverride = 0;
				let adjustedHours = 0;
				let remainingWorkdayHours = totalHours.workday;
				let dayClass = taskRank[taskEnum.FREE];
				let canCalcOnHold = true;

				if (holidays.findIndex(x => x.isSame(dayDate)) > -1) {
					dayClass = taskRank[taskEnum.HOLIDAY];
					remainingWorkdayHours -= totalHours.workday;
				}
				else if (dayDateWk === 'Su' || dayDateWk === 'Sa') {
					totalHours.workday = remainingWorkdayHours = workdayHours;

					if (totalHours.lentOrBorrowedWeekend > 0) {
						dayClass = taskRank[taskEnum.LENTORBORROWED];
					}
					else if (totalHours.scheduledWeekend > 0) {
						dayClass = taskRank[taskEnum.SCHEDULED];
					}
					else {
						dayClass = taskRank[taskEnum.WEEKEND];
					}

					if (totalHours.lentOrBorrowedWeekend > 0 && remainingWorkdayHours > 0) {
						adjustedHours = Math.min(totalHours.lentOrBorrowedWeekend, remainingWorkdayHours);
						totalHours.lentOrBorrowedWeekend -= adjustedHours;
						remainingWorkdayHours -= adjustedHours;
					}

					if (totalHours.scheduledWeekend > 0 && remainingWorkdayHours > 0) {
						adjustedHours = Math.min(totalHours.scheduledWeekend, remainingWorkdayHours);
						totalHours.scheduledWeekend -= adjustedHours;
						remainingWorkdayHours -= adjustedHours;
					}
				}
				else if (unavailable && unavailable.interofficeAssignment && dayDate.isBetween(unavailable.startDate, unavailable.endDate, null, '[]')) {
					dayClass = taskRank[taskEnum.INTEROFFICEASSIGNMENT];
					remainingWorkdayHours -= totalHours.workday;
				}
				else {
					if (totalHours.dto > 0) {
						dayClass = taskRank[taskEnum.DTO];
					}
					else if (totalHours.workday === 0) {
						dayClass = taskRank[taskEnum.NOWORK];
					}
					else if (totalHours.lentOrBorrowed > 0) {
						dayClass = taskRank[taskEnum.LENTORBORROWED];
					}
					else if (totalHours.scheduled > 0) {
						dayClass = taskRank[taskEnum.SCHEDULED];
					}
					else if (totalHours.onHold > 0) {
						dayClass = taskRank[taskEnum.OH];
					}

					if (totalHours.dto > 0 && remainingWorkdayHours > 0) {
						scheduledHoursOverride = totalHours.dto;
						adjustedHours = Math.min(totalHours.dto, remainingWorkdayHours);
						totalHours.dto -= adjustedHours;
						remainingWorkdayHours -= adjustedHours;
						canCalcOnHold = false;
					}

					if (totalHours.lentOrBorrowed > 0 && remainingWorkdayHours > 0) {
						adjustedHours = Math.min(totalHours.lentOrBorrowed, remainingWorkdayHours);
						totalHours.lentOrBorrowed -= adjustedHours;
						remainingWorkdayHours -= adjustedHours;
						canCalcOnHold = false;
					}

					if (totalHours.scheduled > 0 && remainingWorkdayHours > 0) {
						adjustedHours = Math.min(totalHours.scheduled, remainingWorkdayHours);
						totalHours.scheduled -= adjustedHours;
						remainingWorkdayHours -= adjustedHours;
						canCalcOnHold = false;
					}

					if (canCalcOnHold && totalHours.onHold > 0 && remainingWorkdayHours > 0) {
						adjustedHours = Math.min(totalHours.onHold, remainingWorkdayHours);
						totalHours.onHold -= adjustedHours;
						remainingWorkdayHours -= adjustedHours;
					}
				}

				days.push({
					class: dayClass,
					date: dayDate.format('l'),
					scheduledHours: lodash.round((scheduledHoursOverride > 0) ? scheduledHoursOverride : totalHours.workday - remainingWorkdayHours, 2),
					workdayHours: totalHours.workday
				});
			}

			resources[i].days = days;

			resourceData.push(resources[i]);
		}

		return resourceData;
	},
	selectedUser: state => state.selectedUser,
	selectedUserTasks: state => {
		return lodash.orderBy(state.tasks.filter(x => x.userId === state.selectedUser.id), ['estimateStartDate', 'estimateCompleteDate', 'projectTargetDate']);
	},
	showChangeEstimatePopup: state => state.showChangeEstimatePopup,
	showFavoritesPopup: state => state.showFavoritesPopup,
	showFiltersPopup: state => state.showFiltersPopup,
	showResourcePopup: state => state.showResourcePopup,
	showResourceUnavailabilityPopup: state => state.showResourceUnavailabilityPopup,
	showUnassignedPopup: state => state.showUnassignedPopup,
	taskListSortConfig: state => state.taskListSortConfig,
	tasks: state => state.tasks,
	teamResources: (state, getters) => {
		return getters.resourceData.filter(x => x.user.type === 'team');
	},
	unassignedTasks: state => {
		return state.tasks.filter(x => x.userName === 'Unassigned');
	}
}

const actions = {
	addCheckedTasks({ commit }, taskId) {
		commit(types.WORKLOADMANAGER_ADD_CHECKEDTASKS, taskId);
	},
	addCheckedUnassignedTasks({ commit }, taskId) {
		commit(types.WORKLOADMANAGER_ADD_CHECKEDUNASSIGNEDTASKS, taskId);
	},
	addOtherResource({ commit, state }, payload) {
		if (state.resources.findIndex(x => x.id === payload.resources[0].id) > -1) {
			return;
		}

		commit(types.WORKLOADMANAGER_ADD_RESOURCE, payload.resources[0]);

		const holidays = payload.holidays;
		const dtoRequests = payload.dtoRequests;
		const tasks = payload.tasks;

		if (holidays.length > 0) {
			const locationId = holidays[0].locationId;
			if (state.holidays.findIndex(x => x.locationId === locationId) === -1) {
				commit(types.WORKLOADMANAGER_ADD_HOLIDAYS, holidays);
			}
		}

		if (dtoRequests.length > 0) {
			commit(types.WORKLOADMANAGER_ADD_DTOREQUESTS, dtoRequests);
		}

		if (tasks.length > 0) {
			commit(types.WORKLOADMANAGER_ADD_TASKS, tasks);
		}
	},
	assignTasks({ dispatch, commit, state, getters }, payload) {
		let checkedList = state.checkedTasks;
		if (payload.checkedListType === 'Unassigned') {
			checkedList = state.checkedUnassignedTasks;
		}

		// if actual hours > estimate hours, force user to manually enter new estimate hours
		const changeEstimateTasks = state.tasks
			.filter(x => checkedList.includes(x.taskId))
			.filter(x => x.actualHours > x.estimateHours)
			.map(x => {
				return {
					taskId: x.taskId,
					task: x.task,
					projectId: x.projectId,
					fileNumber: x.fileNumber,
					oldUserId: x.userId,
					oldUserName: x.userName,
					oldEstimateHours: x.estimateHours,
					oldActualHours: x.actualHours,
					newUserId: payload.resource.id,
					newUserName: payload.resource.name,
					newEstimateHours: 0
				};
			});

		if (changeEstimateTasks.length > 0) {
			commit(types.WORKLOADMANAGER_SET_CHANGEESTIMATETASKS, changeEstimateTasks);
			dispatch('setShowChangeEstimatePopup', true);
			return;
		}

		const teamResourceIds = getters.teamResources.map(x => x.user.id);
		if (getters.mainFilter.managerId) {
			teamResourceIds.push(getters.mainFilter.id);
		}
		const externalResourceIds = getters.externalResources.map(x => x.user.id);

		checkedList.forEach((taskId) => {
			const task = state.tasks.find(x => x.taskId === taskId);
			const idx = state.tasks.indexOf(task);
			const updatedTask = Object.assign({}, task);
			updatedTask.userId = payload.resource.id;
			updatedTask.userName = payload.resource.name;
			updatedTask.commitStatus = (updatedTask.userId === updatedTask.userIdOld && updatedTask.weekendWork === updatedTask.weekendWorkOld) ? null : 'Pending';

			// when reassigning back to original resource, reset hours back to original state
			if (updatedTask.userId === updatedTask.userIdOld) {
				updatedTask.estimateHours = updatedTask.estimateHoursOld;
				updatedTask.actualHours = updatedTask.actualHoursOld;
			}
			else {
				updatedTask.estimateHours = updatedTask.estimateHours - updatedTask.actualHours;
				updatedTask.actualHours = 0;
			}

			updatedTask.remainingHours = updatedTask.estimateHours - updatedTask.actualHours < 0 ? 0 : updatedTask.estimateHours - updatedTask.actualHours;

			updatedTask.lentOrBorrowed = false;
			if (getters.mainFilter.managerId) {
				if (getters.mainFilter.managerDepartment === 'MTH') {
					if (updatedTask.taskDepartment === 'ENG') {
						updatedTask.lentOrBorrowed = true;
					}
				}
				else {
					if (teamResourceIds.includes(payload.resource.id) && !teamResourceIds.includes(task.projectManagerId)) {
						updatedTask.lentOrBorrowed = true;
					}
					else if ((payload.resource.type === 'favorite' || externalResourceIds.includes(payload.resource.id)) && teamResourceIds.includes(task.projectManagerId)) {
						updatedTask.lentOrBorrowed = true;
					}
				}
			}

			commit(types.WORKLOADMANAGER_UPDATE_TASK, { task: updatedTask, idx: idx });
		});

		dispatch('rearrangeDates', getters.selectedUser); // rearrange selected user's tasks
		dispatch('rearrangeDates', payload.resource); // rearrange newly assigned tasks

		// unassigned checked tasks are cleared in TaskListUnassigned on modal close
		if (payload.checkedListType !== 'Unassigned') {
			dispatch('setCheckedTasks', []);
		}
	},
	commitChanges({ commit, state }) {
		const changedTasks = state.tasks.filter(x => x.commitStatus === 'Pending');
		return api.commitChanges(changedTasks)
			.then(r => {
				for (let i = 0; i < changedTasks.length; i++) {
					const updatedTask = changedTasks[i];
					const idx = state.tasks.findIndex(x => x.taskId === updatedTask.taskId);

					updatedTask.commitStatus = null;
					updatedTask.userIdOld = updatedTask.userId;
					updatedTask.weekendWorkOld = updatedTask.weekendWork;

					// a new task will be created when there's actual hours
					// update the taskId with the new one
					const newTask = r.data.find(x => x.oldTaskId === updatedTask.taskId);
					if (newTask) {
						updatedTask.taskId = newTask.newTaskId;
					}

					commit(types.WORKLOADMANAGER_UPDATE_TASK, { task: updatedTask, idx: idx });
				}
			});
	},
	rearrangeDates({ commit, state, getters }, resource) {
		const tasks = state.tasks.filter(x => x.userId === resource.id);

		if (tasks.length === 0) {
			return;
		}

		api.rearrangeDates(resource, tasks)
			.then(r => {
				for (let i = 0; i < r.data.length; i++) {
					const updatedTask = r.data[i];
					const idx = state.tasks.findIndex(x => x.taskId === updatedTask.taskId);
					commit(types.WORKLOADMANAGER_UPDATE_TASK, { task: updatedTask, idx: idx });
				}
			});
	},
	setCalendarOptions({ commit }, options) {
		commit(types.WORKLOADMANAGER_SET_CALENDAROPTIONS, options);
	},
	setChangeEstimateTasks({ commit }, tasks) {
		commit(types.WORKLOADMANAGER_SET_CHANGEESTIMATETASKS, tasks);
	},
	setCheckAll({ commit }, checkAll) {
		commit(types.WORKLOADMANAGER_SET_CHECKALL, checkAll);
	},
	setCheckedTasks({ commit }, checkedTasks) {
		commit(types.WORKLOADMANAGER_SET_CHECKEDTASKS, checkedTasks);
	},
	setCheckedUnassignedTasks({ commit }, checkedUnassignedTasks) {
		commit(types.WORKLOADMANAGER_SET_CHECKEDUNASSIGNEDTASKS, checkedUnassignedTasks);
	},
	setDTORequests({ commit }, dtoRequests) {
		commit(types.WORKLOADMANAGER_SET_DTOREQUESTS, dtoRequests);
	},
	setFavorites({ commit }, favorites) {
		commit(types.WORKLOADMANAGER_SET_FAVORITES, favorites);
	},
	setFilters({ commit }, filters) {
		commit(types.WORKLOADMANAGER_SET_FILTERS, filters);
	},
	setHolidays({ commit }, holidays) {
		commit(types.WORKLOADMANAGER_SET_HOLIDAYS, holidays);
	},
	setMainFilter({ commit }, mainFilter) {
		commit(types.WORKLOADMANAGER_SET_MAINFILTER, mainFilter);
	},
	setResources({ commit }, resources) {
		commit(types.WORKLOADMANAGER_SET_RESOURCES, resources);
	},
	setSelectedUser({ commit }, user) {
		commit(types.WORKLOADMANAGER_SET_SELECTEDUSER, user);
		commit(types.WORKLOADMANAGER_SET_CHECKEDTASKS, []);
		commit(types.WORKLOADMANAGER_SET_TASKLISTSORTCONFIG, taskListSortConfig);
	},
	setShowFavoritesPopup({ commit }, show) {
		commit(types.WORKLOADMANAGER_SET_SHOWFAVORITESPOPUP, show);
	},
	setShowFiltersPopup({ commit }, show) {
		commit(types.WORKLOADMANAGER_SET_SHOWFILTERSPOPUP, show);
	},
	setShowResourcePopup({ commit }, show) {
		commit(types.WORKLOADMANAGER_SET_SHOWRESOURCEPOPUP, show);
	},
	setShowResourceUnavailabilityPopup({ commit }, show) {
		commit(types.WORKLOADMANAGER_SET_SHOWRESOURCEUNAVAILABILITYPOPUP, show);
	},
	setShowChangeEstimatePopup({ commit }, show) {
		commit(types.WORKLOADMANAGER_SET_SHOWCHANGEESTIMATEPOPUP, show);
	},
	setShowUnassignedPopup({ commit }, show) {
		commit(types.WORKLOADMANAGER_SET_SHOWUNASSIGNEDPOPUP, show);
	},
	setTaskListSortConfig({ commit }, config) {
		commit(types.WORKLOADMANAGER_SET_TASKLISTSORTCONFIG, config);
	},
	setTasks({ commit }, tasks) {
		commit(types.WORKLOADMANAGER_SET_TASKS, tasks);
	},
	updateTask({ commit, state }, task) {
		const idx = state.tasks.findIndex(x => x.taskId === task.taskId);
		commit(types.WORKLOADMANAGER_UPDATE_TASK, { task: task, idx: idx });
	},
	updateTaskEstimates({ commit, state }, changeEstimateTasks) {
		for (let i = 0; i < changeEstimateTasks.length; i++) {
			const task = changeEstimateTasks[i];
			let updatedTask = state.tasks.find(x => x.taskId === task.taskId);
			const idx = state.tasks.indexOf(updatedTask);

			updatedTask.actualHours = 0;
			updatedTask.estimateHours = task.newEstimateHours;
			updatedTask.remainingHours = task.newEstimateHours;

			commit(types.WORKLOADMANAGER_UPDATE_TASK, { task: updatedTask, idx: idx });
		}

		commit(types.WORKLOADMANAGER_SET_CHANGEESTIMATETASKS, []);
	}
}

const mutations = {
	[types.WORKLOADMANAGER_ADD_CHECKEDTASKS](state, taskId) {
		state.checkedTasks.push(taskId);
	},
	[types.WORKLOADMANAGER_ADD_CHECKEDUNASSIGNEDTASKS](state, taskId) {
		state.checkedUnassignedTasks.push(taskId);
	},
	[types.WORKLOADMANAGER_ADD_DTOREQUESTS](state, dtoRequests) {
		state.dtoRequests.push.apply(state.dtoRequests, dtoRequests);
	},
	[types.WORKLOADMANAGER_ADD_HOLIDAYS](state, holidays) {
		state.holidays.push.apply(state.holidays, holidays);
	},
	[types.WORKLOADMANAGER_ADD_RESOURCE](state, resource) {
		state.resources.push(resource);
	},
	[types.WORKLOADMANAGER_ADD_TASKS](state, tasks) {
		state.tasks.push.apply(state.tasks, tasks);
	},
	[types.WORKLOADMANAGER_SET_CALENDAROPTIONS](state, options) {
		state.calendarOptions = options;
	},
	[types.WORKLOADMANAGER_SET_CHECKALL](state, checkAll) {
		state.checkAll = checkAll;
	},
	[types.WORKLOADMANAGER_SET_CHECKEDTASKS](state, checkedTasks) {
		state.checkedTasks = checkedTasks;
	},
	[types.WORKLOADMANAGER_SET_CHECKEDUNASSIGNEDTASKS](state, checkedUnassignedTasks) {
		state.checkedUnassignedTasks = checkedUnassignedTasks;
	},
	[types.WORKLOADMANAGER_SET_CHANGEESTIMATETASKS](state, changeEstimateTasks) {
		state.changeEstimateTasks = changeEstimateTasks;
	},
	[types.WORKLOADMANAGER_SET_DTOREQUESTS](state, dtoRequests) {
		state.dtoRequests = dtoRequests;
	},
	[types.WORKLOADMANAGER_SET_FAVORITES](state, favorites) {
		state.favorites = favorites;
	},
	[types.WORKLOADMANAGER_SET_FILTERS](state, filters) {
		state.filters = filters;
	},
	[types.WORKLOADMANAGER_SET_HOLIDAYS](state, holidays) {
		state.holidays = holidays;
	},
	[types.WORKLOADMANAGER_SET_MAINFILTER](state, mainFilter) {
		state.mainFilter = mainFilter;
	},
	[types.WORKLOADMANAGER_SET_RESOURCES](state, resources) {
		state.resources = resources;
	},
	[types.WORKLOADMANAGER_SET_SELECTEDUSER](state, user) {
		state.selectedUser = user;
	},
	[types.WORKLOADMANAGER_SET_SHOWFAVORITESPOPUP](state, show) {
		state.showFavoritesPopup = show;
	},
	[types.WORKLOADMANAGER_SET_SHOWFILTERSPOPUP](state, show) {
		state.showFiltersPopup = show;
	},
	[types.WORKLOADMANAGER_SET_SHOWRESOURCEPOPUP](state, show) {
		state.showResourcePopup = show;
	},
	[types.WORKLOADMANAGER_SET_SHOWRESOURCEUNAVAILABILITYPOPUP](state, show) {
		state.showResourceUnavailabilityPopup = show;
	},
	[types.WORKLOADMANAGER_SET_SHOWCHANGEESTIMATEPOPUP](state, show) {
		state.showChangeEstimatePopup = show;
	},
	[types.WORKLOADMANAGER_SET_SHOWUNASSIGNEDPOPUP](state, show) {
		state.showUnassignedPopup = show;
	},
	[types.WORKLOADMANAGER_SET_TASKLISTSORTCONFIG](state, config) {
		state.taskListSortConfig = config;
	},
	[types.WORKLOADMANAGER_SET_TASKS](state, tasks) {
		state.tasks = tasks;
	},
	[types.WORKLOADMANAGER_UPDATE_TASK](state, payload) {
		Vue.set(state.tasks, payload.idx, payload.task);
	}
}

export default {
	namespaced: true,
	state,
	getters,
	actions,
	mutations
}