import beautify from 'js-beautify';
import api from '../../../api/noteManager';

const jsBeautifySettings = {
	"indent_size": "4",
	"indent_char": " ",
	"max_preserve_newlines": "5",
	"preserve_newlines": true,
	"keep_array_indentation": false,
	"break_chained_methods": false,
	"indent_scripts": "normal",
	"brace_style": "collapse",
	"space_before_conditional": false,
	"unescape_strings": false,
	"jslint_happy": false,
	"end_with_newline": true,
	"wrap_line_length": "0",
	"indent_inner_html": false,
	"comma_first": false,
	"e4x": true,
	"indent_empty_lines": false
};

const state = {
	letterContent: {},
	currentView: 'LetterContent',
	memoTemplate: {},
	currentMemo: {},
	currentQuestion: {},
	showQuestionModal: false,
	showQuestionDetailModal: false,
	showQuestionAnswerModal: false,
	showMockDataBundlePickerModal: false,
	currentlySaving: false,
	showSaveSuccessToast: false,
	deprecatedMemoCache: [],
	questionTypes: [],
	sections: [],
	showOrderConfigurationModal: false,
	showMockDataModal: false,
	showEditor: false,
	showMemoModal: false,
	showSecondaryLanguageEditor: false,
	compareCriterias: []
};

const getters = {
	getMemoById: (state) => (id) => {
		return state.letterContent.memos.find(memo => memo.id === id);
	},
	getMemoByLocalId: (state) => (localId) => {
		return state.letterContent.memos.find(memo => memo.localId === localId);
	},
	memoTemplate: state => state.memoTemplate,
	letterContent: state => state.letterContent,
	currentMemo: state => state.currentMemo,
	currentQuestion: state => state.currentQuestion,
	memos: state => state.letterContent.memos,
	currentView: state => state.currentView,
	showQuestionModal: state => state.showQuestionModal,
	showQuestionDetailModal: state => state.showQuestionDetailModal,
	showQuestionAnswerModal: state => state.showQuestionAnswerModal,
	getQuestionById: (state) => (id) => {
		return state.currentMemo.questions.find(x => x.id === id);
	},
	showMockDataBundlePickerModal: state => state.showMockDataBundlePickerModal,
	currentlySaving: state => state.currentlySaving,
	showSaveSuccessToast: state => state.showSaveSuccessToast,
	currentMemosQuestionIds: state => state.currentMemo.questions ? state.currentMemo.questions.map(x => { return x.id; }) : [],
	deprecatedMemoExists: (state) => (id) => state.deprecatedMemoCache.some(x => x.id === id),
	getMemoFromDeprecatedCache: (state) => (id) => state.deprecatedMemoCache.find(x => x.id === id),
	currentMemoIsDeprecated: state => state.currentMemo && !!state.currentMemo?.version?.deprecateDate,
	questionTypes: state => state.questionTypes,
	sections: state => state.sections,
	showOrderConfigurationModal: state => state.showOrderConfigurationModal,
	showMockDataModal: state => state.showMockDataModal,
	showEditor: state => state.showEditor,
	showMemoModal: state => state.showMemoModal,
	showSecondaryLanguageEditor: state => state.showSecondaryLanguageEditor,
	compareCriterias: state => state.compareCriterias,
};

const actions = {
	initalize({ commit }, initialData) {
		commit('setLetterContent', initialData.letterContent);
		commit('setMemoTemplate', initialData.memoTemplate);
		commit('setQuestionTypes', initialData.questionTypes);
		commit('setSections', initialData.sections);
		commit('setCompareCriterias', initialData.compareCriterias);
	},
	setCurrentView({ commit, getters }, viewName) {
		if (viewName === 'LetterContent') {
			//commit('setCurrentMemo', getters.memos.find(x => x.version.originalId === getters.currentMemo.version.originalId));
		}

		if (viewName === 'Memo') {
			commit('setShowEditor', false);
			commit('setShowSecondaryLanguageEditor', false);
		}

		commit('setCurrentView', viewName);
	},
	setCurrentMemo({ commit, getters }, localId) {
		const memo = getters.getMemoByLocalId(localId);
		commit('setCurrentMemo', memo);
	},
	resetCurrentMemo({ commit }) {
		commit('setCurrentMemo', {});
	},
	setLetterContent({ commit }, payload) {
		commit('setLetterContent', payload);
	},
	toggleQuestionModal({ commit }) {
		commit('toggleQuestionModal');
	},
	toggleQuestionDetailModal({ commit }) {
		commit('toggleQuestionDetailModal');
	},
	toggleQuestionAnswerModal({ commit }) {
		commit('toggleQuestionAnswerModal');
	},
	setCurrentMemoLanguage({ commit }, payload) {
		commit('setCurrentMemoLanguage', payload);
	},
	setCurrentMemoSecondaryLanguage({ commit }, payload) {
		commit('setCurrentMemoSecondaryLanguage', payload);
	},
	addQuestion({ commit }, payload) {
		api.post_getQuestion(payload)
			.then(response => {
				commit('addQuestion', response.data);
			})
			.catch((error) => { })
			.finally(() => {
			});
	},
	setCurrentQuestion({ commit }, question) {
		commit('setCurrentQuestion', question);
	},
	setCurrentQuestionbyId({ commit, dispatch, getters }, questionId) {
		let question = getters.getQuestionById(questionId);
		return dispatch('setCurrentQuestion', question);
	},
	toggleMockDataBundlePickerModal({ commit }) {
		commit('toggleMockDataBundlePickerModal');
	},
	saveLetterContent({ commit, dispatch, getters }) {
		const payload = getters.letterContent;
		return api.post_saveLetterContent(payload)
			.then(response => {
				commit('setLetterContent', response.data);
			});
	},
	setCurrentlySaving({ commit }, payload) {
		commit('setCurrentlySaving', payload);
	},
	removeQuestion({ commit, getters }, questionId) {
		const payload = {
			memo: getters.currentMemo,
			questionId: questionId
		};
		commit('removeQuestionFromMemo', payload);
	},
	removeMemo({ commit, getters }, localMemoId) {
		let forRemoval = getters.getMemoByLocalId(localMemoId);
		commit('removeMemo', forRemoval);
	},
	addMemo({ commit, getters }, payload) {
		if (payload === 0) {
			let memo = _.cloneDeep(getters.memoTemplate);
			memo.localId = uuidv4();
			commit('addMemo', memo);
			return memo.localId;
		}
		else {
			commit('addMemo', payload);
			return payload.localId;
		}
	},
	beautifyMemo({ commit, getters }) {
		commit('beautifyMemo', getters.currentMemo);
	},
	setCurrentMemoFromDeprecatedCache({ commit, getters }, deprecatedMemoId) {
		let deprecatedMemo = getters.getMemoFromDeprecatedCache(deprecatedMemoId);
		commit('setCurrentMemo', deprecatedMemo);
	},
	addDeprecatedMemoToCache({ commit }, payload) {
		commit('addDeprecatedMemoToCache', payload);
	},
	setCurrentMemoWorkspaceCategory({ commit }, payload) {
		commit('setCurrentMemoWorkspaceCategory', payload);
	},
	setCurrentMemoSource({ commit, getters }, payload) {
		commit('setCurrentMemoSource', payload);
	},
	setSectionId({ commit, getters }, sectionId) {
		commit('setSectionId', sectionId);
	},
	setShowOrderConfigurationModal({ commit }, payload) {
		commit('setShowOrderConfigurationModal', payload);
	},
	setMemoOrder({ commit }) {
		commit('setMemoOrder');
	},
	toggleMockDataModal({ commit }) {
		commit('toggleMockDataModal');
	},
	setShowEditor({ commit }, payload) {
		commit('setShowEditor', payload);
	},
	cloneMemo({ commit, getters }, payload) {
		commit('addMemo', payload.localId ? cleanUpClonedMemo(_.cloneDeep(getters.getMemoByLocalId(payload.localId))) : cleanUpClonedMemo(payload.memo));
	},
	toggleMemoModal({ commit }) {
		commit('toggleMemoModal');
	},
	setShowSecondaryLanguageEditor({ commit }, payload) {
		commit('setShowSecondaryLanguageEditor', payload);
	},
	unLinkValidationFromCurrentMemo({ commit, getters }, validationId) {
		const payload = {
			memo: getters.currentMemo,
			validationId: validationId
		};
		commit('unLinkValidationFromCurrentMemo', payload);
	}
};

const mutations = {
	setLetterContent(state, payload) {
		state.letterContent = payload;
	},
	setCurrentView(state, payload) {
		state.currentView = payload;
	},
	setCurrentMemo(state, memo) {
		memo.content = beautify.html(memo.content, jsBeautifySettings);
		state.currentMemo = memo;
	},
	addMemo(state, memo) {
		state.letterContent.memos.push(memo);
	},
	setMemoTemplate(state, memoTemplate) {
		state.memoTemplate = memoTemplate;
	},
	toggleQuestionModal(state) {
		state.showQuestionModal = !state.showQuestionModal;
	},
	toggleQuestionAnswerModal(state) {
		state.showQuestionAnswerModal = !state.showQuestionAnswerModal;
	},
	setCurrentMemoLanguage(state, payload) {
		state.currentMemo.language = payload;
	},
	setCurrentMemoSecondaryLanguage(state, payload) {
		state.currentMemo.secondaryLanguage = payload;
	},
	addQuestion(state, payload) {
		state.currentMemo.questions.push(payload);
	},
	toggleQuestionDetailModal(state) {
		state.showQuestionDetailModal = !state.showQuestionDetailModal;
	},
	setCurrentQuestion(state, payload) {
		if (payload && payload.type == 'Multiselect' && payload.tokenValue === null)
			payload.tokenValue = []

		state.currentQuestion = payload;
	},
	toggleMockDataBundlePickerModal(state) {
		state.showMockDataBundlePickerModal = !state.showMockDataBundlePickerModal;
	},
	setCurrentlySaving(state, payload) {
		state.currentlySaving = payload;
	},
	removeQuestionFromMemo(state, payload) {
		let toUpdate = payload.memo;
		toUpdate.questions = toUpdate.questions.filter(x => x.id !== payload.questionId);
		state.currentMemo = toUpdate;
	},
	removeMemo(state, payload) {
		state.letterContent.memos = state.letterContent.memos.filter(x => x.localId !== payload.localId);
	},
	beautifyMemo(state, payload) {
		payload.secondaryLanguageContent = beautify.html(payload.secondaryLanguageContent, jsBeautifySettings);
		payload.content = beautify.html(payload.content, jsBeautifySettings);
		state.currentMemo = payload;
	},
	addDeprecatedMemoToCache(state, payload) {
		state.deprecatedMemoCache.push(payload);
	},
	setCompareCriterias(state, payload) {
		state.compareCriterias = payload;
	},
	setCurrentMemoWorkspaceCategory(state, payload) {
		state.currentMemo.workspaceCategory = payload;
	},
	setCurrentMemoSource(state, payload) {
		state.currentMemo.sourceId = payload.value;
		state.currentMemo.sourceName = payload.text;
	},
	setQuestionTypes(state, payload) {
		state.questionTypes = payload;
	},
	setSections(state, payload) {
		state.sections = payload;
	},
	setSectionId(state, sectionId) {
		state.letterContent.sectionId = sectionId;
	},
	setShowOrderConfigurationModal(state, payload) {
		state.showOrderConfigurationModal = payload;
	},
	setMemoOrder(state) {
		state.letterContent.memos.forEach((x, index) => {
			x.orderId = index + 1;
		});
	},
	toggleMockDataModal(state) {
		state.showMockDataModal = !state.showMockDataModal;
	},
	setShowEditor(state, payload) {
		state.showEditor = payload;
	},
	toggleMemoModal(state) {
		state.showMemoModal = !state.showMemoModal;
	},
	setShowSecondaryLanguageEditor(state, payload) {
		state.showSecondaryLanguageEditor = payload;
	},
	unLinkValidationFromCurrentMemo(state, payload) {
		let toUpdate = payload.memo;
		let validation = toUpdate.validations.find(x => x.validationId === payload.validationId);
		toUpdate.validations.splice(toUpdate.validations.indexOf(validation), 1);
		state.currentMemo = toUpdate;
	}
};

export default {
	namespaced: true,
	state,
	getters,
	actions,
	mutations
};

function uuidv4() {
	return ([1e7] + -1e3 + -4e3 + -8e3 + -1e11).replace(/[018]/g, c =>
		(c ^ crypto.getRandomValues(new Uint8Array(1))[0] & 15 >> c / 4).toString(16)
	);
}

function cleanUpClonedMemo(memo) {
	memo.name = `Cloned From - ${memo.name}`;
	memo.localId = uuidv4();
	memo.id = 0;
	memo.orderId = null;
	memo.version.addDate = null;
	memo.version.createdBy = null;
	memo.version.lifecylce = 0;
	memo.version.originalId = 0;
	if (memo.letterContentId !== null)
		memo.letterContentId = null;
	return memo;
}