﻿import api from '../../../api/noteManager';

const state = {
	config: {
		letterPreviewJurisdiction: 0
	},
	jurisdictions: {
		letterDetails: []
	},
	evolution: {
		clauseReports: [],
		specializedJurisdictionClauseReports: []
	},
	submission: {
		components: [],
		currentSignatureData: []
	},
	math: {
		paytables: [],
		gamingGuidelines: []
	},
	manufacturerId: '',
	fileNumber: '',
	manufacturerName: '',
	bundleName: '',
	letterContent: {},
	certificationIssuers: [],
	sourceItems: [],
	footer: {},
	jira: {
		revocationInformation: [],
		disclosureWording: []
	},
	ocuitComponents: []
};

const getters = {
	letterPreviewJurisdiction: state => state.config.letterPreviewJurisdiction,
	jurisdictionLetterDetails: (state) => (jurId) => state.jurisdictions.letterDetails.find(jur => jur.jurisdictionId === jurId),
	jurisdictionsLetterDetails: (state) => state.jurisdictions.letterDetails,
	components: state => state.submission.components,
	clauseReports: state => state.evolution.clauseReports,
	ocuitComponents: state => state.ocuitComponents,
	paytables: (state) => state.math.paytables,
	gamingGuidelines: (state) => state.math.gamingGuidelines,
	manufacturerName: state => state.manufacturerName,
	manufacturerId: state => state.manufacturerId,
	letterContent: state => state.letterContent,
	fileNumber: state => state.fileNumber,
	bundleName: state => state.bundleName,
	certificationIssuers: state => state.certificationIssuers,
	footer: state => state.footer,
	certificationIssuerForCertificationLab: (state, getters) => (certificationLab) => {
		return getters.certificationIssuers.find(x => x.presidesOver.some(x => x === certificationLab));
	},
	certificationIssuerByName: (state, getters) => (name) => {
		return getters.certificationIssuers.find(x => x.lastName === name.split(" ")[1]);
	},
	certificationIssuerByLastName: (state, getters) => (name) => {
		return getters.certificationIssuers.find(x => x.lastName === name);
	},
	sourceItems: state => state.sourceItems,
	getSources: (state, getters) => (sourceId) => {
		switch (sourceId) {
			case "ClauseReports":
				return state.evolution.clauseReports.selectMany(doc => doc.clauseReports);
			case "Paytables":
				return getters.paytables;
			case "Components":
				return getters.components;
			case "SpecializedJurisdictionClauseReports":
				return getters.specializedJurisdictionClauseReports;
			case "OCUITs":
				return getters.ocuitComponents;
			default:
				return [];
		}
	},
	revocationInformation: state => state.jira.revocationInformation,
	disclosureWording: state => state.jira.disclosureWording,
	jira: state => state.jira,
	specializedJurisdictionClauseReports: state => state.evolution.specializedJurisdictionClauseReports,
	modificationList: () => {
		const result = [
			{
				id: 1,
				sourceItemId: 1,
				content: `<div>Modification 1 </div>`,
				applicableJurisdictions: [],
				isSplitFromDefault: false
			},
			{
				id: 1,
				sourceItemId: 1,
				content: `<div>Modification 1 For Arizona </div>`,
				applicableJurisdictions: [22],
				isSplitFromDefault: true
			},
			{
				id: 1,
				sourceItemId: 2,
				content: `<div>Modification 2 For All</div>`,
				applicableJurisdictions: [],
				isSplitFromDefault: false
			},
		];
		return result;
	},
	modificationListForJurisdiction: (state, getters) => (jurID) => {

		let modList = getters.modificationList;

		if (modList.length == 0) {
			return [];
		} else {
			let modsSplitFromDefault = modList.filter(x => x.isSplitFromDefault);
			// -- Mods Split for jurisdictions exist
			if (modsSplitFromDefault.length > 0) {
				let splitForThisJurisdiction = modsSplitFromDefault.filter(x => x.applicableJurisdictions.includes(jurID));
				// -- Mods Split for the current letter preview jurisdiction exist, return the set of non split and split for this jurisdiction
				if (splitForThisJurisdiction.length > 0) {
					let sourceIdsOfModsSplitForThisJurisdiction = splitForThisJurisdiction.flatMap(x => x.sourceItemId)
					let defaultMods = modList.filter(x => !x.isSplitFromDefault);
					let defaultModsNotSplitForThisJurisdiction = [];
					(defaultMods).forEach(element => {
						if (!sourceIdsOfModsSplitForThisJurisdiction.includes(element.sourceItemId))
							defaultModsNotSplitForThisJurisdiction.push(element)
					});
					let result = [...splitForThisJurisdiction, ...defaultModsNotSplitForThisJurisdiction];
					return result;
					// -- Mods Split for the current letter preview jurisdiction DONT exist, return the non split mods.
				} else {
					return modList.filter(x => !x.isSplitFromDefault);
				}
				// -- No jurisdiction splits, return the mod List.
			} else {
				return modList;
			}
		}
	}
};

const actions = {
	getMockDataByBundleId({ commit }, payload) {
		return api.post_getMockDataByBundleId(payload)
			.then(response => {
				commit('setMockData', response.data);
			});
	},
	setMockManufacturerId({ commit }, manufId) {
		commit('setMockManufacturerId', manufId);
	},
	setMockMemoData({ commit }, payload) {
		commit('setMockData', payload);
	},
	setSourceItems({ commit }, payload) {
		commit('setSourceItems', payload);
	},
	setLetterPreviewJurisdiction({ commit }, payload) {
		commit('setLetterPreviewJurisdiction', payload);
	}
};

const mutations = {
	setMockData(state, payload) {
		state.fileNumber = payload.fileNumber;
		state.manufacturerId = payload.manufacturerId;
		state.manufacturerName = payload.manufacturerName;
		state.bundleName = payload.bundleName;
		state.submission.components = payload.components;
		state.evolution.clauseReports = payload.clauseReports;
		state.math.paytables = payload.paytables;
		state.math.gamingGuidelines = payload.gamingGuidelines;
		state.footer = payload.footer;
		state.certificationIssuers = payload.certificationIssuers;
		state.jira.revocationInformation = payload.jiraInformation.revocationInformations;
		state.jira.disclosureWording = payload.jiraInformation.disclosureInformations;
		state.evolution.specializedJurisdictionClauseReports = payload.specializedJurisdictionClauseReports;
		state.jurisdictions.letterDetails = payload.jurisdictionData;
		state.ocuitComponents = payload.ocuiTs;
	},
	setMockManufacturerId(state, payload) {
		state.manufacturerId = payload;
	},
	setSourceItems(state, payload) {
		state.sourceItems = payload;
	},
	setLetterPreviewJurisdiction(state, payload) {
		state.config.letterPreviewJurisdiction = parseInt(payload, 10);
	}
};

export default {
	namespaced: true,
	state,
	getters,
	actions,
	mutations
};