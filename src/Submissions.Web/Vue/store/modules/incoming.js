﻿import Vue from 'vue';
import api from '../../api/incoming'

const state = {
	//Incoming Review
	incomingReview: {
		projectDetails: {
			projectDefinition: {}
		}
	},
	oldIncomingReview: [],
	error: {
		general: {
			productType: {
				flag: false,
				errorMsg: "Product Type can not be empty."
			},
			complexityRating: {
				flag: false,
				errorMsg: "Complexity Rating is a required field for New/Mod/Clone Game Product-Types."
			},
			quotedHours: {
				flag: false,
				errorMsg: "Quoted hours must be in increments of .25 hours"
			},
		},
		originalTargetDate: {
			onHold: {
				flag: false,
				errorMsg: "Project is currently OH, take off hold to change Target Date."
			},
			targetDate: {
				flag: false,
				errorMsg: "Project was accepted more than three days ago."
			},
			reason: {
				flag: false,
				errorMsg: "Changing the originial target date reqires a reason be provided."
			},
			detail: {
				flag: false,
				errorMsg: "Changing the originial target date reqires a change detail message be provided."
			}
		},
		projectDefinition: {
			mappingType: {
				flag: false,
				errorMsg: "A Master File Number is required when indicating that the current submission is a Resubmission / Continuation / Pre-Certification."
			},
			mapFile: {
				flag: false,
				errorMsg: "Mapping is required for the current submission."
			}
		},
		manufacturerSpecific: {
			studio: {
				flag: false,
				errorMsg: "Studio is a required field for New/Mod/Clone Game Product-Types."
			},
			productLine: {
				flag: false,
				errorMsg: "Product Line is a required field for New/Mod/Clone Game Product-Types."
			},
			platform: {
				flag: false,
				errorMsg: "Platform is a required field."
			}
		},
		billing: {
			productClassifications: {
				flag: false,
				errorMsg: "Product classification is required when indicating that the current submission is a Resubmission / Continuation / Pre-Certification."
			}
		}
	},
	showResourcePopup: false,
	showTargetOffsetConfirmationPopup: false,
	resetTargetDateValues: false,
	initialPageLoad: false,
	billingVisibility: false,
	confirmation: {
		show: false,
		proceed: false,
		updatedProperties: [],
		initialValues: {
			studio: '',
			productline: '',
			productClassification: ''
		}
	},
	incomingSaveCompleteValidation: false,
	studioUpdateWarning: false,
	//Evo Config
	componentGroups: {
		clientId: 0,
		components: [],
		description: "",
		evolutionComponentGroupIdentifier: "",
		functionalRoles: [],
		compGroupID: null,
		inEditMode: "",
		jurisdictions: [],
		locked: false,
		selected: false,
		themes: [],
		activeComponentCounts: 0
	},
	evoEditToggle: {
		editIndex: -1,
		editToggle: false
	},
	evoConfig: {
		evoJurisdictionVisibility: false,
		trackIndividualComponents: [],
		unusedTrackedComponentsCount: 0,
		jurisdictionSpinner: false,
		potentialTaskSpinner: false,
		collapsiblePanel: {
			availableComponent: false,
			jurisdiction: false,
			functionalroles: false,
			potentialTask: false
		},
		errorMessages: {
			individualComponents: "All non-withdrawn components must be in a component group. Please add the remaining components to a group.",
			componentGroup: "Please remove empty component group.",
			emptyName: "Please provide a name for the component group.",
			nameFieldMaxLimit: "The field Name has a maximum length of 255.",
			functionNotSelected: "Please select a functional role for ",
			documentsNotSelected: "Please select a document for ",
			jurisdictionNotSelected: "Please select a jurisdiction for ",
			compGroupEmpty: "Component Group(s) is empty."
		},
		potentialTasks: [],
		compGroupDeleted: false,
		originalCompGroupSet: [],
		selectAllJurisdictionsAndDocuments: false,
	},
	taskCopy: [],
	previouslySelectedJurDocs: []
};

const getters = {
	getIncomingReview: state => {
		return state.incomingReview;
	},
	showResourcePopup: state => {
		return state.showResourcePopup;
	},
	showTargetOffsetConfirmationPopup: state => {
		return state.showTargetOffsetConfirmationPopup;
	},
	resetTargetDateValues: state => {
		return state.resetTargetDateValues;
	},
	getError: state => {
		return state.error;
	},
	getOldIncoming: state => {
		return state.oldIncomingReview;
	},
	getMapTypeFlag: state => {
		return state.error.projectDefinition.mappingType.flag;
	},
	getCurrentSubmissionRecords: state => {
		return state.incomingReview.projectDetails.projectDefinition.currentSubmissionRecords;
	},
	getInitialPageLoad: state => {
		return state.initialPageLoad;
	},
	getConfirmation: state => {
		return state.confirmation;
	},
	getBillingVisibility: state => {
		return state.billingVisibility;
	},
	getIncomingSaveCompleteValidation: state => {
		return state.incomingSaveCompleteValidation
	},
	getComponentGroupTemplate: state => {
		return state.componentGroups
	},
	UK: state => {
		return state.incomingReview.uk;
	},
	getEditToggle: state => {
		return state.evoEditToggle
	},
	studioUpdateWarning: state => {
		return state.studioUpdateWarning
	},
	getEvoConfig: state => {
		return state.evoConfig
	},
	unusedTrackedComponentsCount: state => {
		return state.evoConfig.unusedTrackedComponentsCount
	},
	copyOfTasks: state => {
		return state.taskCopy
    }
};

const actions = {
	setEntireModel: (state, payload) => {
		state.commit('setEntireModel', payload);
	},
	setFileName: (state, payload) => {
		state.commit('setFileName', payload);
	},
	setProjectDetailUrl: (state, payload) => {
		state.commit('setProjectDetailUrl', payload);
	},
	setProjectStatus: (state, payload) => {
		state.commit('setProjectStatus', payload);
	},
	setProjectLead: (state, payload) => {
		state.commit('setProjectLead', payload);
	},
	setProjectLeadId: (state, payload) => {
		state.commit('setProjectLeadId', payload);
	},
	setManager: (state, payload) => {
		state.commit('setManager', payload);
	},
	setManagerId: (state, payload) => {
		state.commit('setManagerId', payload);
	},
	setDevRep: (state, payload) => {
		state.commit('setDevRep', payload);
	},
	setDevRepId: (state, payload) => {
		state.commit('setDevRepId', payload);
	},
	setQuotedHours: (state, payload) => {
		state.commit('setQuotedHours', payload);
	},
	setMasterFileNumber: (state, payload) => {
		state.commit('setMasterFileNumber', payload);
	},
	setSelectedMappingType: (state, payload) => {
		state.commit('setSelectedMappingType', payload);
	},
	setTargetDateReasonType: (state, payload) => {
		state.commit('setTargetDateReasonType', payload);
	},
	setTargetDateReasonTypeId: (state, payload) => {
		state.commit('setTargetDateReasonTypeId', payload);
	},
	setManufacturerSpecificStudio: (state, payload) => {
		state.commit('setManufacturerSpecificStudio', payload);
	},
	setShowResourcePopup: (state, payload) => {
		state.commit('setShowResourcePopup', payload);
	},
	setShowTargetOffsetConfirmationPopup: (state, payload) => {
		state.commit('setShowTargetOffsetConfirmationPopup', payload)
	},
	setManufacturerSpecificStudioId: (state, payload) => {
		state.commit('setManufacturerSpecificStudioId', payload);
	},
	setManufacturerSpecificProductLineId: (state, payload) => {
		state.commit('setManufacturerSpecificProductLineId', payload);
	},
	setManufacturerSpecificProductLine: (state, payload) => {
		state.commit('setManufacturerSpecificProductLine', payload);
	},
	setManufacturerSpecificBillingParty: (state, payload) => {
		state.commit('setManufacturerSpecificBillingParty', payload);
	},
	setIncomingReviewStatusComplete: (state, payload) => {
		state.commit('setIncomingReviewStatusComplete', payload);
	},
	setProductTypeFlag: (state, payload) => {
		state.commit('setProductTypeFlag', payload);
	},
	setComplexityRatingFlag: (state, payload) => {
		state.commit('setComplexityRatingFlag', payload);
	},
	setQuotedHoursFlag: (state, payload) => {
		state.commit('setQuotedHoursFlag', payload);
	},
	setQAOffice: (state, payload) => {
		state.commit('setQAOffice', payload);
	},
	setOnHoldFlag: (state, payload) => {
		state.commit('setOnHoldFlag', payload);
	},
	setTargetDateFlag: (state, payload) => {
		state.commit('setTargetDateFlag', payload);
	},
	setReasonFlag: (state, payload) => {
		state.commit('setReasonFlag', payload);
	},
	setDetailFlag: (state, payload) => {
		state.commit('setDetailFlag', payload);
	},
	setProductType: (state, payload) => {
		state.commit('setProductType', payload);
	},
	setProductTypeId: (state, payload) => {
		state.commit('setProductTypeId', payload);
	},
	setTestingType: (state, payload) => {
		state.commit('setTestingType', payload);
	},
	setTestingTypeId: (state, payload) => {
		state.commit('setTestingTypeId', payload);
	},
	setMapTypeFlag: (state, payload) => {
		state.commit('setMapTypeFlag', payload);
	},
	setStudioFlag: (state, payload) => {
		state.commit('setStudioFlag', payload);
	},
	setComplexityRating: (state, payload) => {
		state.commit('setComplexityRating', payload);
	},
	setComplexityRatingId: (state, payload) => {
		state.commit('setComplexityRatingId', payload);
	},
	setProductLineFlag: (state, payload) => {
		state.commit('setProductLineFlag', payload);
	},
	setTargetDateDetail: (state, payload) => {
		state.commit('setTargetDateDetail', payload);
	},
	setResetTargetDateValues: (state, payload) => {
		state.commit('setResetTargetDateValues', payload);
	},
	setOldIncomingReview: (state, payload) => {
		state.commit('setOldIncomingReview', payload);
	},
	setProductClassificationsFlag: (state, payload) => {
		state.commit('setProductClassificationsFlag', payload);
	},
	setProductClassificationsErrorMsg: (state, payload) => {
		state.commit('setProductClassificationsErrorMsg', payload);
	},
	setProductClassification: (state, payload) => {
		state.commit('setProductClassification', payload);
	},
	setProductClassificationId: (state, payload) => {
		state.commit('setProductClassificationId', payload);
	},
	setResubmissionOptionExplanation: (state, payload) => {
		state.commit('setResubmissionOptionExplanation', payload);
	},
	setCurrentSubmissionRecords: (state, payload) => {
		state.commit('setCurrentSubmissionRecords', payload);
	},
	setInitialPageLoad: (state, payload) => {
		state.commit('setInitialPageLoad', payload);
	},
	setIncomingReviewVm: (state, payload) => {
		state.commit('setIncomingReviewVm', payload);
	},
	setShowConfirmation: (state, payload) => {
		state.commit('setShowConfirmation', payload);
	},
	setProceedConfirmation: (state, payload) => {
		state.commit('setProceedConfirmation', payload);
	},
	setProductClassificationMasterFile: (state, payload) => {
		state.commit('setProductClassificationMasterFile', payload);
	},
	setManufacturerMasterFile: (state, payload) => {
		state.commit('setManufacturerMasterFile', payload);
	},
	setMapFile: (state, payload) => {
		state.commit('setMapFile', payload)
	},
	setUpdatedPropertiesConfirmation: (state, payload) => {
		state.commit('setUpdatedPropertiesConfirmation', payload)
	},
	setStudioInitVal: (state, payload) => {
		state.commit('setStudioInitVal', payload)
	},
	setProductLineInitVal: (state, payload) => {
		state.commit('setProductLineInitVal', payload)
	},
	setProductClassificationInitVal: (state, payload) => {
		state.commit('setProductClassificationInitVal', payload)
	},
	setBillingVisibility: (state, payload) => {
		state.commit('setBillingVisibility', payload)
	},
	setIncomingSaveCompleteValidation: (state, payload) => {
		state.commit('setIncomingSaveCompleteValidation', payload)
	},
	setStudioUpdateWarning: (state, payload) => {
		state.commit('setStudioUpdateWarning', payload)
	},
	platformErrorFlag: (state, payload) => {
		state.commit('platformErrorFlag', payload)
	},
	validatePlatform: (state, payload) => {
		if (state.state.incomingReview.manufacturerSpecific.platformIds.length == 0 && state.state.incomingReview.manufacturerSpecific.platformRequired && state.state.incomingReview.manufacturerSpecific.platformVisibility) {
			state.commit('platformErrorFlag', true);
		} else {
			state.commit('platformErrorFlag', false);
		}
	},
	setJurisdictionComponent: (parentState, index) => {
		let submissionIds = [], trackerComponents = [];
		trackerComponents = parentState.state.evoConfig.trackIndividualComponents;
		for (let i = 0; i < trackerComponents.length; i++) {
			if (trackerComponents[i].isUsed === index) {
				submissionIds.push(trackerComponents[i].id);
			}
		}
		if (submissionIds.length) {
			Loading();
			parentState.state.incomingReview.evolutionViewModel.componentGroups[index].jurisdictions = [];
			parentState.commit('jurisdictionSpinner', true);
			api.loadJurisdiction(parentState.state.incomingReview.projectId, submissionIds, parentState.state.incomingReview.evolutionViewModel.requiredJuridictionIds)
			.then(response => {
				const result = response.data;
				for (let i = 0; i < result.length; i++) {
					let compGroup = parentState.state.previouslySelectedJurDocs[index];
					let previousJurDoc = compGroup && compGroup.length > 0 ? compGroup[0].find(x => x.jur.id === result[i].id) : null;
					if (previousJurDoc) {
						result[i].selected = previousJurDoc.jur.selected;
					}
					let documents = result[i].documents;
					let hasRequiredJur = parentState.state.incomingReview.evolutionViewModel.requiredJuridictionIds.indexOf(parseInt(result[i].id)) >= 0;
					for (let j = 0; j < documents.length; j++) {
						if (parentState.state.incomingReview.evolutionViewModel.requiredDocuments.indexOf(documents[j].name) >= 0) {
							documents[j].selected = true;
						} else {
							if (!compGroup || compGroup.length == 0) {
								documents[j].selected = hasRequiredJur ? false : true;
							} else {
								let previousDoc = previousJurDoc.docs.find(x => x === documents[j].id);
								documents[j].selected = previousDoc ? true : false;
							}
						}
					}
					parentState.commit('setJurisdictionComponent', { index: index, result: result[i] });
				}
			})
			.catch(e => {
				console.log(e);
			})
			.finally(x => {
				parentState.commit('jurisdictionSpinner', false);
				DoneLoading();
			});
		} else {
			parentState.state.incomingReview.evolutionViewModel.componentGroups[index].jurisdictions = [];
		}
	},
	moveCompToIndividualComponents: (parentState, payload) => {
		let components = [];
		if (payload.componentIndex !== null) {
			components = parentState.state.incomingReview.evolutionViewModel.componentGroups[payload.compGroupIndex].components[payload.componentIndex];
			parentState.commit('moveCompToIndividualComponents', components);
			parentState.state.incomingReview.evolutionViewModel.componentGroups[payload.compGroupIndex].components.splice(payload.componentIndex,1);
		} else {
			components = parentState.state.incomingReview.evolutionViewModel.componentGroups[payload.compGroupIndex].components;
			for (let i = 0; i < components.length; i++) {
				parentState.commit('moveCompToIndividualComponents', components[i]);
			}
		}
	},
	setEvoEditToggle: (parentState, payload) => {
		if (payload !== null) {
			parentState.commit('evoEditToggleIndex', payload.editIndex);
			parentState.commit('evoEditToggleT', payload.editToggle);
		} else {
			parentState.commit('evoEditToggleIndex', -1);
			parentState.commit('evoEditToggleT', false);
		}
	},
	setEvoConfig: (state, payload) => {
		state.commit('setEvoConfig', payload);
	},
	setTrackIndividualComponents: (state, payload) =>  {
		state.commit('setTrackIndividualComponents', payload);
	},
	setEvoConfigVM: (state, payload) => {
		state.commit('setEvoConfigVM', payload);
	},
	jurisdictionSpinner: (state, payload) => {
		state.commit('jurisdictionSpinner', payload);
	},
	availablePanel: (state, payload) => {
		state.commit('availablePanel', payload);
	},
	jurisdictionPanel: (state, payload) => {
		state.commit('jurisdictionPanel', payload);
	},
	functionalRolePanel: (state, payload) => {
		state.commit('functionalRolePanel', payload);
	},
	potentialTaskPanel: (state, payload) => {
		state.commit('potentialTaskPanel', payload);
	},
	potentialTask: (state, payload) => {
		state.commit('potentialTask', payload);
	},
	potentialTaskSpinner: (state, payload) => {
		state.commit('potentialTaskSpinner', payload);
	},
	unusedTrackedComponentsCount: (state, payload) => {
		let count = 0;
		for (let i = 0; i < state.getters.getEvoConfig.trackIndividualComponents.length; i++) {
			if (state.getters.getEvoConfig.trackIndividualComponents[i].isUsed < 0) {
				count++;
			}
		}
		state.commit('unusedTrackedComponentsCount', count);
	},
	updateDeleteCompGroupFlag: (state, payload) => {
		state.commit('updateDeleteCompGroupFlag', payload)
	},
	updateCopyOfTasks: (state, payload) => {
		state.commit('updateCopyOfTasks', payload)
	},
	populateOriginalCompGroupSet: (state, payload) => {
		state.state.evoConfig.originalCompGroupSet = [];
		state.state.incomingReview.evolutionViewModel.componentGroups.forEach(x => {
			let selectedComponents = _.filter(x.components, function (x) { return x.selected && x.isUsed > -1 });
			state.commit('populateOriginalCompGroupSet', { components: _.map(selectedComponents, 'id') });
		});
	},
	updatePreviouslySelectedJurDocs: (state, payload) => {
		let compJurDocInfo = [];
		state.state.incomingReview.evolutionViewModel.componentGroups.forEach(x => {
			let jurDocInfo = [];
			for (let i = 0; i < x.jurisdictions.length; i++) {
				let jurisdictionsDocuments = {
					jur: { id: x.jurisdictions[i].id, selected: x.jurisdictions[i].selected },
					docs: x.jurisdictions[i].documents.filter(x => x.selected).map(x => x.id)
				}
				jurDocInfo.push(jurisdictionsDocuments);
			}
			compJurDocInfo.push(jurDocInfo);
		});
		state.commit('updatePreviouslySelectedJurDocs', compJurDocInfo);
	}
};

const mutations = {
	setEntireModel: (state, payload) => {
		state.incomingReview = payload;
	},
	setFileName: (state, payload) => {
		state.incomingReview.fileName = payload;
	},
	setProjectDetailUrl: (state, payload) => {
		state.incomingReview.projectDetailUrl = payload;
	},
	setProjectStatus: (state, payload) => {
		state.incomingReview.projectStatus = payload;
	},
	setProjectLead: (state, payload) => {
		state.incomingReview.projectDetails.general.projectLead = payload;
	},
	setProjectLeadId: (state, payload) => {
		state.incomingReview.projectDetails.general.projectLeadId = parseInt(payload);
	},
	setManager: (state, payload) => {
		state.incomingReview.projectDetails.general.manager = payload;
	},
	setManagerId: (state, payload) => {
		state.incomingReview.projectDetails.general.managerId = parseInt(payload);
	},
	setDevRep: (state, payload) => {
		state.incomingReview.projectDetails.general.developmentRepresentative = payload;
	},
	setDevRepId: (state, payload) => {
		state.incomingReview.projectDetails.general.developmentRepresentativeId = parseInt(payload);
	},
	setQuotedHours: (state, payload) => {
		state.incomingReview.projectDetails.general.quotedHours = payload;
	},
	setMasterFileNumber: (state, payload) => {
		state.incomingReview.projectDetails.projectDefinition.masterFileNumber = payload;
	},
	setSelectedMappingType: (state, payload) => {
		state.incomingReview.projectDetails.projectDefinition.selectedMappingType = payload;
	},
	setTargetDateReasonType: (state, payload) => {
		state.incomingReview.originalTargetDate.changeReason = payload;
	},
	setTargetDateReasonTypeId: (state, payload) => {
		state.incomingReview.originalTargetDate.changeReasonId = payload;
	},
	setTargetDateDetail: (state, payload) => {
		state.incomingReview.originalTargetDate.detail = payload;
	},
	setShowResourcePopup: (state, payload) => {
		state.showResourcePopup = payload;
	},
	setShowTargetOffsetConfirmationPopup: (state, payload) => {
		state.showTargetOffsetConfirmationPopup = payload;
	},
	setManufacturerSpecificStudio: (state, payload) => {
		state.incomingReview.manufacturerSpecific.studio = payload;
	},
	setManufacturerSpecificStudioId: (state, payload) => {
		state.incomingReview.manufacturerSpecific.studioId = parseInt(payload);
	},
	setManufacturerSpecificProductLineId: (state, payload) => {
		state.incomingReview.manufacturerSpecific.productLineId = parseInt(payload);
	},
	setManufacturerSpecificProductLine: (state, payload) => {
		state.incomingReview.manufacturerSpecific.productLine = payload;
	},
	setManufacturerSpecificBillingParty: (state, payload) => {
		state.incomingReview.manufacturerSpecific.billingParty = payload;
	},
	setIncomingReviewStatusComplete: (state, payload) => {
		state.incomingReview.incomingReviewStatus.complete = payload;
	},
	setProductTypeFlag: (state, payload) => {
		state.error.general.productType.flag = payload;
	},
	setComplexityRatingFlag: (state, payload) => {
		state.error.general.complexityRating.flag = payload;
	},
	setQuotedHoursFlag: (state, payload) => {
		state.error.general.quotedHours.flag = payload;
	},
	setQAOffice: (state, payload) => {
		state.incomingReview.projectDetails.general.qaOffice = payload;
	},
	setOnHoldFlag: (state, payload) => {
		state.error.originalTargetDate.onHold.flag = payload;
	},
	setTargetDateFlag: (state, payload) => {
		state.error.originalTargetDate.targetDate.flag = payload;
	},
	setReasonFlag: (state, payload) => {
		state.error.originalTargetDate.reason.flag = payload;
	},
	setDetailFlag: (state, payload) => {
		state.error.originalTargetDate.detail.flag = payload;
	},
	setProductType: (state, payload) => {
		state.incomingReview.projectDetails.general.productType = payload;
	},
	setProductTypeId: (state, payload) => {
		state.incomingReview.projectDetails.general.productTypeId = parseInt(payload);
	},
	setTestingType: (state, payload) => {
		state.incomingReview.projectDetails.projectDefinition.testingType = payload;
	},
	setTestingTypeId: (state, payload) => {
		state.incomingReview.projectDetails.projectDefinition.testingTypeId = parseInt(payload);
	},
	setMapTypeFlag: (state, payload) => {
		state.error.projectDefinition.mappingType.flag = payload;
	},
	setStudioFlag: (state, payload) => {
		state.error.manufacturerSpecific.studio.flag = payload;
	},
	setComplexityRating: (state, payload) => {
		state.incomingReview.projectDetails.general.complexityRating = payload;
	},
	setComplexityRatingId: (state, payload) => {
		state.incomingReview.projectDetails.general.complexityRatingId = parseInt(payload);
	},
	setProductLineFlag: (state, payload) => {
		state.error.manufacturerSpecific.productLine.flag = payload;
	},
	setResetTargetDateValues: (state, payload) => {
		state.resetTargetDateValues = payload;
	},
	setOldIncomingReview: (state, payload) => {
		state.oldIncomingReview = payload;
	},
	setProductClassification: (state, payload) => {
		state.incomingReview.billing.productClassification = payload;
	},
	setProductClassificationId: (state, payload) => {
		state.incomingReview.billing.productClassificationId = parseInt(payload);
	},
	setProductClassificationsFlag: (state, payload) => {
		state.error.billing.productClassifications.flag = payload;
	},
	setProductClassificationsErrorMsg: (state, payload) => {
		state.error.billing.productClassifications.errorMsg = payload;
	},
	setResubmissionOptionExplanation: (state, payload) => {
		state.incomingReview.projectDetails.projectDefinition.masterFileRecordSet.resubmissionOptionExplanation = payload;
	},
	setCurrentSubmissionRecords: (state, payload) => {
		state.incomingReview.projectDetails.projectDefinition.currentSubmissionRecords = payload;
	},
	setInitialPageLoad: (state, payload) => {
		state.initialPageLoad = payload;
	},
	setIncomingReviewVm: (state, payload) => {
		state.incomingReview = payload;
	},
	setShowConfirmation: (state, payload) => {
		state.confirmation.show = payload;
	},
	setProceedConfirmation: (state, payload) => {
		state.confirmation.proceed = payload;
	},
	setProductClassificationMasterFile: (state, payload) => {
		if (state.billingVisibility === true) {
			state.incomingReview.billing.masterFile = payload;
		} else {
			state.incomingReview.billing.masterFile = "";
		}
	},
	setManufacturerMasterFile: (state, payload) => {
		state.incomingReview.manufacturerSpecific.masterFile = payload;
	},
	setMapFile: (state, payload) => {
		state.error.projectDefinition.mapFile.flag = payload;
	},
	setUpdatedPropertiesConfirmation: (state, payload) => {
		state.confirmation.updatedProperties = payload;
	},
	setStudioInitVal: (state, payload) => {
		state.confirmation.initialValues.studio = payload;
	},
	setProductLineInitVal: (state, payload) => {
		state.confirmation.initialValues.productline = payload;
	},
	setProductClassificationInitVal: (state, payload) => {
		state.confirmation.initialValues.productClassification = payload;
	},
	setBillingVisibility: (state, payload) => {
		state.billingVisibility = payload;
	},
	setIncomingSaveCompleteValidation: (state, payload) => {
		state.incomingSaveCompleteValidation = payload
	},
	setStudioUpdateWarning: (state, payload) => {
		state.studioUpdateWarning = payload
	},
	setJurisdictionComponent: (state, payload) => {
		state.incomingReview.evolutionViewModel.componentGroups[payload.index].jurisdictions.push(payload.result);
	},
	moveCompToIndividualComponents: (state, payload) => {
		state.incomingReview.evolutionViewModel.individualComponents.push(payload);
	},
	platformErrorFlag: (state, payload) => {
		state.error.manufacturerSpecific.platform.flag = payload;
	},
	evoEditToggleIndex: (state, payload) => {
		state.evoEditToggle.editIndex = payload;
	},
	evoEditToggleT : (state, payload) => {
		state.evoEditToggle.editToggle = payload;
	},
	setEvoConfig: (state, payload) => {
		state.evoConfig = payload;
	},
	setTrackIndividualComponents: (state, payload) => {
		state.evoConfig.trackIndividualComponents = payload;
	},
	setEvoConfigVM: (state, payload) => {
		state.incomingReview.evolutionViewModel = payload;
	},
	jurisdictionSpinner: (state, payload) => {
		state.evoConfig.jurisdictionSpinner = payload;
	},
	availablePanel: (state, payload) => {
		state.evoConfig.collapsiblePanel.availableComponent = payload;
	},
	jurisdictionPanel: (state, payload) => {
		state.evoConfig.collapsiblePanel.jurisdiction = payload;
	},
	functionalRolePanel: (state, payload) => {
		state.evoConfig.collapsiblePanel.functionalroles = payload;
	},
	potentialTaskPanel: (state, payload) => {
		state.evoConfig.collapsiblePanel.potentialTask = payload;
	},
	potentialTask: (state, payload) => {
		state.evoConfig.potentialTasks = payload;
	},
	potentialTaskSpinner: (state, payload) => {
		state.evoConfig.potentialTaskSpinner = payload;
	},
	unusedTrackedComponentsCount: (state, payload) => {
		state.evoConfig.unusedTrackedComponentsCount = payload;
	},
	updateDeleteCompGroupFlag: (state, payload) => {
		state.evoConfig.compGroupDeleted = payload;
	},
	updateCopyOfTasks: (state, payload) => {
		state.taskCopy = payload;
	},
	populateOriginalCompGroupSet: (state, payload) => {
		state.evoConfig.originalCompGroupSet.push(payload);
	},
	updatePreviouslySelectedJurDocs: (state, payload) => {
		state.previouslySelectedJurDocs = [];
		state.previouslySelectedJurDocs.push(payload);
	}
};

export default {
	namespaced: true,
	state,
	getters,
	actions,
	mutations
};