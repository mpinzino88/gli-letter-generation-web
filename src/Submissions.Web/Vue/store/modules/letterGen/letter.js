import { isNull } from '@/common';
import { isNotNull } from '@/common';

/*
 * Use this store for any data inherent to the letter that doesn't belong in Sources. 
 * Basically, don't put UI state values here and don't put Source data in here.
 * */

const state = {
	bundleId: 75,
	bundleName: "MO-22-IGT-07-06-10",
	fileNumber: "MO-22-IGT-07-06",
	manufacturerName: "IGT",
	manufacturerId: "IGT",
	projectId: 0,
	projectStatus: "",
	projectHolder: "",
	projectEvalLab: "",
	qaStatus: "",
	jurisdictionIds: [],//
	letterContent: {},
	headerMap: [], // jurisdiction => sectionIndex => page => headerMemoId
	certificationIssuers: [],
	filteredMemos: [],
	filteredMemosByJur: {},
	secondaryControls: [],
	currentMemoSet: 'PDF Preview',
	//Evaluation lab address
	evalLabs: [],
	//Lab Codes for the Letter
	evalLabLetterCodes: [],
	compatibilityComponents: [],
	platformPieceTemplates: [],
	platformIds: [],
	platformsSelected: "",
	revocationInformation: [],
	workSpaceSectionIcons: [],
	containsRJWD: false,
	preDefinedPages: 1,
	mergedProjectIds: []
};

const getters = {
	bundleId: state => state.bundleId,
	bundleName: state => state.bundleName,
	containsRJWD: state => state.containsRJWD,
	mergedProjectIds: state => state.mergedProjectIds,
	currentMemoSet: state => state.currentMemoSet,
	fileNumber: state => state.fileNumber,
	manufacturerName: state => state.manufacturerName,
	manufacturerId: state => state.manufacturerId,
	jurisdictionIds: state => state.jurisdictionIds,
	projectStatus: state => state.projectStatus,
	projectHolder: state => state.projectHolder,
	projectEvalLab: state => state.projectEvalLab,
	qaStatus: state => state.qaStatus,
	certificationIssuers: state => state.certificationIssuers,
	letterState: state => state,
	projectId: state => state.projectId,
	evalLabs: state => state.evalLabs,
	evalLabLetterCodes: state => state.evalLabLetterCodes,
	//no filtering
	letterContent: state => state.letterContent,
	sections: state => state.letterContent.sections,
	headers: state => state.letterContent.headers,
	preDefinedPages: state => state.preDefinedPages,
	memos: (state, getters) => {
		if (isNotNull(getters) && isNotNull(getters.sections) && getters.sections.length > 0)
			return getters.sections.selectMany(sec => sec.memos);
		else return [];
	},
	filteredMemos: (state, getters) => state.filteredMemos,
	filteredMemosByJur: state => state.filteredMemosByJur,
	secondaryControls: state => state.secondaryControls,
	controls: (state, getters) => {
		let controls = [];
		if (isNotNull(getters) && isNotNull(getters.memos) && getters.memos.length > 0) {
			getters.memos.forEach(memo => {
				if (memo.letterContentControls.length > 0) {
					let allMemoControls = memo.letterContentControls.filter(control => isNotNull(control));
					allMemoControls.forEach(control => {
						control.memoLocalId = memo.localId;
						control.memoId = memo.id;
						controls.push(control);
					});
				}
			});
			var union = [...new Set([...controls, ...state.secondaryControls])];
			return union;
			//	return getters.memos.selectMany(memo => memo.letterContentControls).filter(control => isNotNull(control));
		}
		else return [];
	},
	otherMemosForControl: (state, getters) => (memo, control) => {
		return getters.memos.filter(m => m.localId !== memo.localId && (m.letterContentControls.some(c => c.id === control.id || c.tableRows.some(t => t.some(ti => ti.id === control.id)))));
	},

	//filtering
	clauseReportMemos: (state, getters) => {
		if (isNotNull(getters) && isNotNull(getters.memos) && getters.memos.length > 0) {
			return getters.memos.filter(memo => memo.sourceId === "ClauseReports");
		}
		else {
			return [];
		}
	},
	componentMemos: (state, getters) => {
		if (isNotNull(getters) && isNotNull(getters.memos) && getters.memos.length > 0) {
			return getters.memos.filter(memo => memo.sourceId === "Components");
		}
		else {
			return [];
		}
	},
	paytableMemos: (state, getters) => {
		if (isNotNull(getters) && isNotNull(getters.memos) && getters.memos.length > 0) {
			return getters.memos.filter(memo => memo.sourceId === "Paytables");
		}
		else {
			return [];
		}
	},
	specializedJurClauseReportMemos: (state, getters) => {
		if (isNotNull(getters) && isNotNull(getters.memos) && getters.memos.length > 0) {
			return getters.memos.filter(memo => memo.sourceId === "SpecializedJurisdictionClauseReports");
		}
		else {
			return [];
		}
	},
	ocuitMemos: (state, getters) => {
		if (isNotNull(getters) && isNotNull(getters.memos) && getters.memos.length > 0) {
			return getters.memos.filter(memo => memo.sourceId === "OCUITs");
		}
		else {
			return [];
		}
	},
	clauseIdMemos: (state, getters) => (clauseId) => {
		return getters.clauseReportMemos.filter(cr => isNotNull(cr.sourceItemTargets) && cr.sourceItemTargets.find(sit => sit.id === clauseId));
	},
	componentIdMemos: (state, getters) => (componentId) => {
		return getters.componentMemos.filter(cr => isNotNull(cr.sourceItemTargets) && cr.sourceItemTargets.find(sit => sit.id === componentId));
	},
	paytableIdMemos: (state, getters) => (paytableId) => {
		return getters.paytableMemos.filter(cr => isNotNull(cr.sourceItemTargets) && cr.sourceItemTargets.find(sit => sit.id === paytableId));
	},
	ocuitIdMemos: (state, getters) => (ocuitId) => {
		return getters.ocuitMemos.filter(cr => isNotNull(cr.sourceItemTargets) && cr.sourceItemTargets.find(sit => sit.id === ocuitId));
	},
	sectionsForJurisdiction: (state, getters) => (sections, jurId) => {
		return sections.filter((sec) => {
			return getters.memosForJurisdiction(sec.memos, jurId).length > 0;
		});
	},
	displayedSectionsForJurisdictions: (state, getters) => (sections, jurId) => {
		let jurSections = sections.filter((sec) => {
			return getters.memosForJurisdiction(sec.memos, jurId).length > 0;
		});
		return jurSections.filter(s => getters.memosForJurisdiction(s.memos, jurId).some(m => m.include && m.specialFlag != "SectionHeader"));
	},
	nonDisplayedSectionsForJurisdictions: (state, getters) => (sections, jurId) => {
		let jurSections = sections.filter((sec) => {
			return getters.memosForJurisdiction(sec.memos, jurId).length > 0;
		});
		return jurSections.filter(s => !getters.memosForJurisdiction(s.memos, jurId).some(m => m.include && m.specialFlag != "SectionHeader"));
	},
	memosForJurisdiction: () => (memos, jurId) => {
		return memos.filter((memo) => {
			return isNull(memo.applicableJurisdictions) ||
				memo.applicableJurisdictions.length === 0 ||
				memo.applicableJurisdictions.includes(jurId);
		});
	},
	controlsForJurisdiction: () => (controls, jurId) => {
		return controls.filter(control => {
			return isNull(control.jurisdictionIds) ||
				control.applicableJurisdictions.length === 0 ||
				control.applicableJurisdictions.includes(jurId);
		});
	},
	certificationIssuerByCertificationLab: (state, getters) => (certificationLab) => {
		return getters.certificationIssuers.find(x => x.presidesOver.some(x => x === certificationLab));
	},
	certificationIssuerByName: (state, getters) => (name) => {
		if (name.val !== undefined) {
			return getters.certificationIssuers.find(x => x.lastName === name.val.split(" ")[1]);
		}
	},
	headerMap: state => state.headerMap,
	getHeaderId: (state, getters) => (jurId, sectionId, page) => {
		let jurMap = state.headerMap.filter(map => map.jurId === jurId);
		if (jurMap.length > 0) {
			let sectionMap = jurMap[0].sections.filter(map => map.sectionId === sectionId);
			if (sectionMap.length > 0) {
				let pageMap = sectionMap[0].pages.filter(map => map.pageId === page);
				if (pageMap.length > 0) {
					return pageMap[0].headerId;
				}
				else {
					return getters.headers[0].id;
				}
			}
			else {
				return getters.headers[0].id;
			}
		} else {
			return getters.headers[0].id;
		}
	},
	isProjectLocked: (state, getters) => {
		if ((getters.projectStatus === "QA" && getters.qaStatus === "FL") || getters.projectStatus === "DN") {
			return true;
		}
		else {
			return false;
		}
	},
	compatibilityComponents: state => state.compatibilityComponents,
	platformPieceTemplates: state => state.platformPieceTemplates,
	//continuityComponents: state => state.continuityComponents,
	//jurisdictionChipComponents: state => state.jurisdictionChipComponents,
	platformIds: state => state.platformIds,
	platformsSelected: state => state.platformsSelected,
	workSpaceSectionIcons: state => state.workSpaceSectionIcons,
	totalLetterPages: (state, getters) => (letterPreviewJurisdiction) => {
		let maxPage = 0;
		let currentSections = getters.displayedSectionsForJurisdictions(state.letterContent.sections, letterPreviewJurisdiction);

		for (let sectionItr = 0; sectionItr < currentSections.length; sectionItr++) {
			let section = currentSections[sectionItr];
			let pagingObjs = getters.memosForJurisdiction(section.memos, letterPreviewJurisdiction);
			let pagingMap = pagingObjs.map(memo => getPaging(memo, memo.localId, letterPreviewJurisdiction));
			let pageNums = pagingMap.filter(obj => isNotNull(obj)).map(pag => pag.page);
			if (pageNums.length === 0) {
				continue;
			}
			let max = Math.max(...pageNums);
			maxPage += max;
		}
		return maxPage + state.preDefinedPages;
	},
	modificationList: state => {
		let modificationSection = state.letterContent.sections.find(x => x.name === "Modifications");
		if (modificationSection) {
			if (modificationSection.memos.length > 0) {
				let modificationMemo = modificationSection.memos.find(x => x.isModification);
				if (modificationMemo.sourceItemTargets.length > 0) {

					(modificationMemo.sourceItemTargets).forEach(x => {
						x.letterContentControls.forEach(y => {
							y.sourceItemId = x.id
						});
					});

					let modificationMemoControls = modificationMemo.sourceItemTargets.flatMap(x => x.letterContentControls);
					if (modificationMemoControls.length > 0) {
						let modificationMemoDetails = modificationMemoControls.map(x => { return { id: x.id, sourceItemId: x.sourceItemId, content: x.content, applicableJurisdictions: x.applicableJurisdictions, isSplitFromDefault: x.isSplitFromDefault } });
						return modificationMemoDetails;
					}
				}
			}
		}

		return [];
	},
	modificationListForJurisdiction: (state, getters) => (jurID) => {

		let modList = getters.modificationList;

		if (modList.length == 0) {
			return [];
		} else {
			let modsSplitFromDefault = modList.filter(x => x.isSplitFromDefault);
			// -- Mods Split for jurisdictions exist
			if (modsSplitFromDefault.length > 0) {
				let splitForThisJurisdiction = modsSplitFromDefault.filter(x => x.applicableJurisdictions.includes(jurID));
				// -- Mods Split for the current letter preview jurisdiction exist, return the set of non split and split for this jurisdiction
				if (splitForThisJurisdiction.length > 0) {
					let sourceIdsOfModsSplitForThisJurisdiction = splitForThisJurisdiction.flatMap(x => x.sourceItemId)
					let defaultMods = modList.filter(x => !x.isSplitFromDefault);
					let defaultModsNotSplitForThisJurisdiction = [];
					(defaultMods).forEach(element => {
						if (!sourceIdsOfModsSplitForThisJurisdiction.includes(element.sourceItemId))
							defaultModsNotSplitForThisJurisdiction.push(element)
					});
					let result = [...splitForThisJurisdiction, ...defaultModsNotSplitForThisJurisdiction];
					return result;
					// -- Mods Split for the current letter preview jurisdiction DONT exist, return the non split mods.
				} else {
					return modList.filter(x => !x.isSplitFromDefault);
				}
				// -- No jurisdiction splits, return the mod List.
			} else {
				return modList;
			}
		}
	},
	platformPieceGroups: (state, getters) => {
		return _.groupBy(getters.platformPieceTemplates.flatMap(x => x.platformPieces), 'name');
	}
};
const actions = {
	setFilteredMemos: (context, payload) => {
		context.commit('setFilteredMemos', payload);
	},
	setFilteredMemosByJur: (context, payload) => {
		context.commit('setFilteredMemosByJur', payload);
	},
	setProjectInfo: (context, payload) => {
		context.commit('setProjectInfo', payload);
	},
	setLetterContent: (context, payload) => {
		context.commit('setLetterContent', payload);
	},
	splitQuestion: (context, payload) => {
		context.commit('splitQuestion', payload);
	},
	modifySplitQuestion: (context, payload) => {
		context.commit('modifySplitQuestion', payload);
	},
	deleteSplitQuestion: (context, payload) => {
		context.commit('deleteSplitQuestion', payload);
	},
	removeMemo: (context, payload) => {
		context.commit('removeMemo', payload);
	},
	insertMemo: (context, payload) => {
		context.commit('insertMemo', payload);
	},
	insertMemos: (context, payload) => {
		context.commit('insertMemos', payload);
	},
	insertClonedMemos: ({ commit, getters }, payload) => {
		commit('insertClonedMemos', { memos: payload, sections: getters.sections });
	},
	setCertificationIssuers: ({ commit, getters }, payload) => {
		commit('setCertificationIssuers', payload);
	},
	buildHeaderMap: (context, payload) => {
		context.commit('buildHeaderMap', payload);
	},
	addHeaderMap: (context, payload) => {
		context.commit('addHeaderMap', payload);
	},
	setProjectEvalLab: (context, payload) => {
		context.commit('setProjectEvalLab', payload);
	},
	setSecondaryControls: (context, payload) => {
		context.commit('setSecondaryControls', payload);
	},
	updateFilteredMemoSection: (context, payload) => {
		context.commit('updateFilteredMemoSection', payload);
	},
	setCurrentMemoSet: (context, payload) => {
		context.commit('setCurrentMemoSet', payload);
	},
	updateFilteredMemosByJur: (context, payload) => {
		context.commit('updateFilteredMemosByJur', payload);
	},
	setPreDefinedPages: (context, payload) => {
		context.commit('setPreDefinedPages', payload);
	},
	setRevocationInformation: (context, payload) => {
		context.commit('setRevocationInformation', payload);
	},
	loadSavedOCUITComponents: (context, payload) => {

		let components = payload;
		components.forEach(comp => {
			let row = {
				linkedTemplates: [],
				description: undefined,
				selectedJurisdictions: [],
			};
			row.linkedTemplates = comp.associatedSoftwareTemplates.map(temp => ({ name: temp.associatedSoftwareTemplate, id: temp.associatedSoftwareTemplateId }));
			row.description = comp.description;

			row.selectedJurisdictions = comp.jurisdictions;
			row.componentId = comp.submissionId;
			row.component = comp.submission;
			row.continuity = comp.continuity;
			row.testedInParallel = comp.testedInParallel;
			row.jurisdictionChip = comp.jurisdictionChip;
			row.platformPieceId = comp.platformPieceId;
			row.component = {
				fileNumber: comp.submission.fileNumber,
				idNumber: comp.submission.idNumber,
			},

				context.commit('addComponent', row);
		});
		context.commit('sortComponentList', "");
	},
	addPlatformPiecesTemplates: (context, payload) => {
		context.commit('addPlatformPiecesTemplates', payload);
	},
	addCompatibilityComponents: (context, payload) => {
		let components = payload.components;
		let templateImport = payload.templateImport;
		components.forEach(comp => {
			if (!templateImport) {
				comp.componentId = comp.id;
			}
			//check to see if this component already exists
			if (!state.compatibilityComponents.some(x => x.componentId === comp.componentId && x.platformPieceId === comp.platformPieceId)) {
				let row = {
					linkedTemplates: [],
					description: comp.description,
					testedInParallel: comp.testedInParallel,
					jurisdictionChip: comp.jurisdictionChip,
					selectedJurisdictions: [],
					platformPieceId: comp.platformPieceId,
					continuity: comp.continuity
				};

				if (templateImport) {
					row.linkedTemplates = [{ name: comp.associatedSoftwareTemplate.name, id: comp.associatedSoftwareTemplate.id }];

					row.componentId = comp.componentId;
					row.component = comp.component;
					row.description = comp.description;
					row.testedInParallel = comp.testedInParallel;
					row.jurisdictionChip = comp.jurisdictionChip;
					comp.associatedSoftwareTemplate.jurisdictions.forEach(jur => {
						if (state.jurisdictionIds.includes(jur.jurisdictionId)) {
							row.selectedJurisdictions.push({
								jurisdiction: jur.jurisdiction,
								jurisdictionId: jur.jurisdictionId
							});
						}
					});
					//row.selectedJurisdictions.push({
					//	jurisdiction: comp.associatedSoftwareTemplate.jurisdiction,
					//	jurisdictionId: parseInt(comp.associatedSoftwareTemplate.jurisdictionId),
					//	//status: comp.component.status.toUpperCase()
					//});

				} else {

					row.customGroup = true;
					row.componentId = comp.id;
					row.description = comp.description;
					row.testedInParallel = comp.testedInParallel;
					row.jurisdictionChip = comp.jurisdictionChip;
					row.component = {
						fileNumber: comp.fileNumber,
						idNumber: comp.idNumber,
					},


						comp.jurisdictionalData.forEach(juri => {
							//TODO: only add approved juri
							if (state.jurisdictionIds.includes(parseInt(juri.jurisdictionId))) {
								row.selectedJurisdictions.push({
									jurisdiction: juri.jurisdictionName,
									jurisdictionId: parseInt(juri.jurisdictionId),
								});
							}
						});
				}

				context.commit('addComponent', row);
			} else {
				let newJurisdiction = [];
				let exsistComp = state.compatibilityComponents.find(x => x.componentId === comp.componentId && x.platformPieceId === comp.platformPieceId);

				if (exsistComp != undefined) {
					let templateInfo;

					if (templateImport && !exsistComp.linkedTemplates.some(temp => temp.id === comp.associatedSoftwareTemplate.id)) {
						templateInfo = { name: comp.associatedSoftwareTemplate.name, id: comp.associatedSoftwareTemplate.id };
					}
					if (templateImport) {
						comp.associatedSoftwareTemplate.jurisdictions.forEach(jur => {
							const inComp = exsistComp.selectedJurisdictions.some(temp => temp.jurisdictionId === jur.jurisdictionId);
							const inBundle = state.jurisdictionIds.includes(jur.jurisdictionId);
							if (!inComp && inBundle) {
								newJurisdiction.push({
									jurisdiction: jur.jurisdiction,
									jurisdictionId: jur.jurisdictionId,
								});
							}
						});
					} else {
						comp.jurisdictionalData.forEach(juri => {
							if (!exsistComp.selectedJurisdictions.some(temp => temp.jurisdictionId === parseInt(juri.jurisdictionId))) {
								if (state.jurisdictionIds.includes(parseInt(juri.jurisdictionId))) {
									newJurisdiction.push({
										jurisdiction: juri.jurisdictionName,
										jurisdictionId: parseInt(juri.jurisdictionId),
									});
								}
							}
						});
					}
					context.commit('updateComponentJuridiction', { component: comp, templateInfo: templateInfo, newJurisdiction: newJurisdiction });
				}
			}

		});
		context.commit('sortComponentList', "");
	},
	removeCompatibilityComponents: (context, payload) => {
		context.commit('removeCompatibilityComponents', payload);
	},
	removePlatformPiecesTemplates: (context, payload) => {
		context.commit('removePlatformPiecesTemplates', payload);
	},
	setPlatformIds: (context, payload) => {
		context.commit('setPlatformIds', payload);
	},
	setPlatformsSelected: (context, payload) => {
		context.commit('setPlatformsSelected', payload);
	},
	setComponentJurisdictions: (context, payload) => {
		context.commit('setComponentJurisdictions', payload);
	},
	setWorkSpaceSectionIcons: (context, payload) => {
		if (!payload) {
			payload = [
				{ name: 'Product Details', fontAwesomeIcon: 'fa-pencil-alt' },
				{ name: 'Descriptions', fontAwesomeIcon: 'fa-file-alt' },
				{ name: 'Software', fontAwesomeIcon: 'fa-code' },
				{ name: 'Game Information', fontAwesomeIcon: 'fa-info' },
				{ name: 'Modifications', fontAwesomeIcon: 'fa-bullhorn' },
				{ name: 'Notes', fontAwesomeIcon: 'fa-sticky-note' },
				{ name: 'Verification Procedures', fontAwesomeIcon: 'fa-user-check' },
				{ name: 'Test Environment', fontAwesomeIcon: 'fa-laptop' },
				{ name: 'Terms and Conditions', fontAwesomeIcon: 'fa-file-signature' }
			]
		}

		context.commit('setWorkSpaceSectionIcons', payload);
	}
};

const mutations = {
	setProjectInfo: (state, payload) => {
		state.bundleId = payload.bundleId;
		state.bundleName = payload.bundleName;
		state.containsRJWD = payload.containsRJWD;
		state.mergedProjectIds = payload.mergedProjectIds;
		state.fileNumber = payload.fileNumber;
		state.projectId = payload.projectId;
		state.manufacturerName = payload.manufacturerName;
		state.manufacturerId = payload.manufacturerId;
		state.jurisdictionIds = payload.jurisdictionIds;
		state.projectId = payload.projectId;
		state.projectStatus = payload.projectStatus;
		state.projectHolder = payload.projectHolder;
		state.qaStatus = payload.qaStatus;
		state.evalLabs = payload.evalLabs;
		state.evalLabLetterCodes = payload.evalLabLetterCodes;
	},
	setLetterContent: (state, payload) => {
		state.letterContent = payload;
	},
	splitQuestion: (state, payload) => {
		let memo = payload.memo;
		let whichControl = payload.control;
		let applyJurSet = payload.jur;
		let sourceTarget = payload.source;

		//just make sure if it exists for memo applicablejurs because with one Question mulitple memos can pass not applicable jurs
		let applyJur = [];
		if (memo.applicableJurisdictions.length > 0) { // otherwise it's all jur
			for (let i = 0; i < applyJurSet.length; i++) {
				if (memo.applicableJurisdictions.includes(parseInt(applyJurSet[i]))) {
					applyJur.push(applyJurSet[i]);
				}
			}
		}
		else {
			applyJur = applyJurSet;
		}

		if (applyJur.length > 0) { // just to be sure since we have one question for multiple memos
			let addControl = _.cloneDeep(whichControl);
			addControl.applicableJurisdictions = [];
			addControl.applicableJurisdictions.push(...applyJur);

			//this is to maintain for question group belonging to multiple memo
			addControl.splitGroupApplicable = [];
			addControl.splitGroupApplicable.push(...applyJurSet);

			//find current control index and insert next to it
			let addToIndex = 0;
			let id = 0;
			let source = (sourceTarget !== undefined) ? sourceTarget : memo;
			for (let i = 0; i < source.letterContentControls.length; i++) {
				if (source.letterContentControls[i].id === whichControl.id) {
					addToIndex = i + 1;
				}
			}

			addControl.id = whichControl.id;
			addControl.label = whichControl.label;//"juriLetterDetails" + id + "." + whichControl.label;// need to make it unique or the label for all jur dont work
			addControl.isSplitFromDefault = true;
			addControl.localId = generateGuid();
			addControl.answerId = null;
			addControl.savedBy = '';
			addControl.savedOn = null;
			source.letterContentControls.splice(addToIndex, 0, addControl);
		}

	},
	modifySplitQuestion: (state, payload) => {
		let memo = payload.memo;
		let whichControl = payload.control;
		let applyJurSet = payload.jur;
		let source = payload.source;

		let applyJur = [];
		if (memo.applicableJurisdictions.length > 0) { // otherwise it's all jur
			for (let i = 0; i < applyJurSet.length; i++) {
				if (memo.applicableJurisdictions.includes(parseInt(applyJurSet[i]))) {
					applyJur.push(applyJurSet[i]);
				}
			}
		}
		else {
			applyJur = applyJurSet;
		}

		whichControl.applicableJurisdictions = [];
		whichControl.applicableJurisdictions.push(...applyJur);

		//below is to maintain the group when question belongs to multiple memos and applicablejurs differ
		whichControl.splitGroupApplicable = [];
		whichControl.splitGroupApplicable.push(...applyJurSet);

	},
	deleteSplitQuestion: (state, payload) => {
		let memo = payload.memo;
		let whichControl = payload.control;
		let sourceTarget = payload.source;

		let source = (sourceTarget !== undefined) ? sourceTarget : memo;
		for (let i = 0; i < source.letterContentControls.length; i++) {
			if (source.letterContentControls[i].localId === whichControl.localId) {
				source.letterContentControls.splice(i, 1);
			}
		}
	},
	removeMemo: (state, payload) => {
		let memoIndex = payload.section.memos.indexOf(payload.memo);
		payload.section.memos.splice(memoIndex, 1);
	},
	//payload: { item: newMemo, section: this.section, jurId: this.letterPreviewJurisdiction, orderId: newOrderId });
	insertMemo: (state, payload) => {
		payload.section.memos.forEach(memo => {
			if (memo.orderId >= payload.orderId) {
				memo.orderId++;
			}
		});
		payload.item.orderId = payload.orderId;
		payload.section.memos.push(payload.item);
		payload.section.memos.sort((a, b) => a.orderId - b.orderId);
		//payload.section.memos.splice(payload.newIndex, 0, payload.item);
	},
	//payload: { items: newMemos, section: this.section, jurId: this.letterPreviewJurisdiction });
	insertMemos: (state, payload) => {
		//console.log('insertMemos');
		//console.log(payload);
		payload.items.sort((a, b) => a.orderId - b.orderId);
		let minOrderId = payload.items[0].orderId; //10 
		let maxOrderId = payload.items[payload.items.length - 1].orderId; //15 
		let range = maxOrderId - minOrderId; //5
		let newBaseOrderId = range + 1;//6
		//console.log('min - ' + minOrderId + ' max - ' + maxOrderId + ' range - ' + range + ' new - ' + newBaseOrderId);
		payload.section.memos.forEach(memo => {
			if (memo.orderId >= minOrderId) {
				memo.orderId += newBaseOrderId;
			}
		});
		//This could be improved if we don't sort the list after inserting, use splice instead. 
		//console.log('memo counts - ' + payload.section.memos.length);
		for (let memoIter = 0; memoIter < payload.items.length; memoIter++) {
			payload.section.memos.push(payload.items[memoIter]);
		}
		//payload.section.memos.concat(payload.items);
		//console.log('memo counts - ' + payload.section.memos.length);
		payload.section.memos.sort((a, b) => a.orderId - b.orderId);

	},
	insertClonedMemos: (state, payload) => {
		payload.memos.forEach(clone => {
			let section = payload.sections.find(sec => sec.memos.some(memo => memo.id === clone.id));
			section.memos.push(clone);
		});
		payload.sections.forEach(section => section.memos.sort((a, b) => a.orderId - b.orderId));
	},
	buildHeaderMap: (state, payload) => {
		state.headerMap = payload;
	},
	setCertificationIssuers: (state, payload) => {
		state.certificationIssuers = payload;
	},
	addHeaderMap: (state, payload) => {
		//payload:{jurId, sectionId, pageId, newHeaderId}
		let jurMap = state.headerMap.filter(map => map.jurId === payload.jurId);
		if (jurMap.length > 0) {
			let sectionMap = jurMap[0].sections.filter(map => map.sectionId === payload.sectionId);
			if (sectionMap.length > 0) {
				let pageMap = sectionMap[0].pages.filter(map => map.pageId === payload.pageId);
				if (pageMap.length > 0) {
					pageMap[0].headerId = payload.newHeaderId;
				}
				else {
					sectionMap[0].pages.push({ pageId: payload.pageId, headerId: payload.newHeaderId });
				}
			}
			else {
				jurMap[0].sections.push({ sectionId: payload.sectionId, pages: [{ pageId: payload.pageId, headerId: payload.newHeaderId }] });
			}
		} else {
			state.headerMap.push({ jurId: payload.jurId, sections: [{ sectionId: payload.sectionId, pages: [{ pageId: payload.pageId, headerId: payload.newHeaderId }] }] });
		}
	},
	setProjectEvalLab: (state, payload) => {
		state.projectEvalLab = payload;
	},
	setFilteredMemos: (state, payload) => {
		state.filteredMemos = payload;
	},
	setFilteredMemosByJur: (state, payload) => {
		state.filteredMemosByJur[payload.jur] = payload.filteredMemos;
	},
	setPreDefinedPages: (state, payload) => {
		state.preDefinedPages = payload;
	},
	updateFilteredMemosByJur: (state, payload) => {
		state.filteredMemosByJur[payload.jur][payload.key] = payload.value;
	},
	updateFilteredMemoSection: (state, payload) => {
		state.filteredMemos[payload.key] = payload.value;
	},
	setSecondaryControls: (state, payload) => {
		state.secondaryControls.push(...payload);
	},
	setCurrentMemoSet: (state, payload) => {
		state.currentMemoSet = payload;
	},
	setRevocationInformation: (state, payload) => {
		state.revocationInformation = payload;
	},
	addPlatformPiecesTemplates: (state, payload) => {
		//payload.forEach(item => state.platformPieceTemplates.push(item));
		payload.forEach(newTemplate => {
			const exists = state.platformPieceTemplates.includes(template => template.id === newTemplate.id);
			if (!exists) {
				state.platformPieceTemplates.push(newTemplate);
			}
		});
		//state.platformPieceTemplates.push(...payload);
	},
	addComponent: (state, payload) => {
		state.compatibilityComponents.push(payload);

	},
	updateComponentJuridiction: (state, payload) => {
		let componentId = payload.component.componentId;
		let platformPieceId = payload.component.platformPieceId;
		let templateInfo = payload.templateInfo;
		let newJurisdiction = payload.newJurisdiction;

		let exsistComp = state.compatibilityComponents.find(x => x.componentId === componentId && x.platformPieceId === platformPieceId);

		if (templateInfo != undefined) {
			if (exsistComp.linkedTemplates === undefined)
				exsistComp.linkedTemplates = [];
			exsistComp.linkedTemplates.push(templateInfo);
		}

		exsistComp.customGroup = (exsistComp.linkedTemplates.length === 0);

		if (newJurisdiction.length > 0)
			exsistComp.selectedJurisdictions.push.apply(exsistComp.selectedJurisdictions, newJurisdiction);

		exsistComp.selectedJurisdictions.sort(function (a, b) {
			if (a.jurisdiction < b.jurisdiction)
				return -1;
			else if (a.jurisdiction == b.jurisdiction)
				return 0;
			else
				return 1;
		});
	},
	removeCompatibilityComponents: (state, payload) => {
		const targetIndex = state.compatibilityComponents.findIndex(comp => comp.componentId === payload.componentId && comp.platformPieceId === payload.platformPieceId);
		state.compatibilityComponents.splice(targetIndex, 1);
	},
	removePlatformPiecesTemplates: (state, payload) => {
		const targetIndex = state.platformPieceTemplates.findIndex(ppt => ppt.id === payload);
		state.platformPieceTemplates.splice(targetIndex, 1);
	},
	sortComponentList: (state, payload) => {
		state.compatibilityComponents.sort(function (a, b) {
			if (a.customGroup != b.customGroup) {
				return a.customGroup - b.customGroup;
			}
			else {
				if (a.component.fileNumber != b.component.fileNumber) {


					return a.component.fileNumber.localeCompare(b.component.fileNumber);
				}
				else {
					return a.component.idNumber.localeCompare(b.component.idNumber);
				}
			}
		});
	},
	setPlatformIds: (state, payload) => {
		state.platformIds = payload;
	},
	setPlatformsSelected: (state, payload) => {
		state.platformsSelected = payload;
	},
	setComponentJurisdictions: (state, payload) => {
		let componentId = payload.component.componentId;
		let platformPieceId = payload.component.platformPieceId;
		let jurisdictions = payload.jurisdictions;

		let component = state.compatibilityComponents.find(x => x.componentId === componentId && x.platformPieceId === platformPieceId);
		component.selectedJurisdictions = jurisdictions;
	},
	setWorkSpaceSectionIcons: (state, payload) => {
		state.workSpaceSectionIcons = payload;
	}

};

export default {
	namespaced: true,
	state,
	getters,
	actions,
	mutations
};



