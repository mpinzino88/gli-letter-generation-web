//var cookies = require('../../javascript/cookies.js');
import { JURISDICTION, GAME_TYPE } from '@/common';

const state = {
	submission: {
		components: [],
		currentSignatureData: [],
		modifications: []
	},
	evolution: {
		clauseReports: [],
		specializedJurisdictionClauseReports: []
	},
	jurisdictions: {
		letterDetails: []
	},
	math: {
		paytables: [],
		gamingGuidelines: [],
	},
	jira: {
		revocationInformation: [],
		disclosureWording: []
	},
	specializedJurisdictionClauseReportsForAllJur: [],
	OCUITComponents: [],
	isProjectiGaming: false
};

const getters = {
	//No filtering
	jurisdictionsLetterDetails: (state) => state.jurisdictions.letterDetails,
	clauseReports: (state) => state.evolution.clauseReports,
	clauses: (state) => state.evolution.clauseReports.selectMany(doc => doc.clauseReports),//The double-naming of clauseReport models is infuriating
	components: state => state.submission.components,
	isProjectiGaming: state => state.isProjectiGaming,
	currentSignatureData: state => state.submission.currentSignatureData,
	modifications: state => state.submission.modifications,
	letterGenSourcesState: (state) => state,
	revocationInformation: state => state.jira.revocationInformation,
	disclosureWording: state => state.jira.disclosureWording,
	jiraInformation: state => state.jira,
	specializedJurisdictionClauseReportsForAllJur: state => state.specializedJurisdictionClauseReportsForAllJur,
	specializedJurisdictionClauseReports: state => state.evolution.specializedJurisdictionClauseReports,
	OCUITComponents: state => state.OCUITComponents,
	//filtering
	jurisdictionLetterDetails: (state) => (jurId) => state.jurisdictions.letterDetails.find(jur => jur.jurisdictionId === jurId),
	workspaceJurisdictionLetterDetails: (state) => (jurIds) => {
		if (jurIds === undefined || jurIds.length === 0) {
			return state.jurisdictions.letterDetails;
		}
		else {
			return state.jurisdictions.letterDetails.filter(jur => jurIds.includes(jur.jurisdictionId));
		}
	},
	jurisdictionClauses: (state) => (jurId) => {
		if (jurId === 0)
			return state.evolution.clauseReports.selectMany(doc => doc.clauseReports);
		else {
			let report = state.evolution.clauseReports.find(doc => doc.jurisdictionId === jurId);
			return report !== undefined ? report.clauseReports : [];
		}
	},
	workspaceClauses: (state) => (jurIds) => {
		if (jurIds === undefined || jurIds.length === 0) {
			return state.evolution.clauseReports.selectMany(doc => doc.clauseReports);
		}
		else {
			let reports = state.evolution.clauseReports.filter(doc => jurIds.includes(doc.jurisdictionId));
			return reports.selectMany(rep => rep.clauseReports);
		}
	},
	jurisdictionClauseReports: (state) => (jurId) => {
		if (jurId === 0)
			return state.evolution.clauseReports;
		else {
			let report = state.evolution.clauseReports.find(doc => doc.jurisdictionId === jurId);
			return [report];
		}
	},
	paytables: (state) => state.math.paytables,
	gamingGuidelines: (state) => state.math.gamingGuidelines,
	jurisdictionRevocationInformation: (state) => (jurId) => state.jira.revocationInformation.filter(x => x.jurisdcitionID == jurId),
	jurisdictionDisclosureWording: (state) => (jurId) => state.jira.disclosureWording.filter(x => x.jurisdcitionID == jurId),
	//Used for returning the correct source collection based on the .sourceId string
	getSourcesForMemo: (state, getters) => (memo) => {
		switch (memo.sourceId) {
			case "ClauseReports":
				return getters.workspaceClauses(memo.applicableJurisdictions);
			case "Paytables":
				return getters.paytables;
			case "Components":
				return getters.components.filter(component => {
					let compJurIds = component.jurisdictionalData.map(jur => parseInt(jur.jurisdictionId));
					return memo.applicableJurisdictions.length === 0 ||
						memo.applicableJurisdictions.some(appJur => compJurIds.includes(appJur));
				});
			case "SpecializedJurisdictionClauseReports":
				return getters.specializedJurisdictionClauseReports;
			case "OCUITs":
				return getters.OCUITComponents;
			default:
				return [];
		}
	},
	getSourcesForJurisdiction: (state, getters) => (sourceId, jurId) => {
		switch (sourceId) {
			case "ClauseReports":
				return getters.workspaceClauses([jurId]);
			case "Paytables":
				return getters.paytables;
			case "Components":
				return getters.components.filter(component => component.jurisdictionalData.map(jur => parseInt(jur.jurisdictionId)).includes(jurId));
			case "SpecializedJurisdictionClauseReports":
				return getters.specializedJurisdictionClauseReports;
			case "OCUITs":
				return getters.OCUITComponents;
			default:
				return [];
		}
	},
	jurisdictionalDataIds: (state, getters) => (jurisdictionIds) => {
		if (getters.components != []) {
			return _.flatMap(getters.components, x => _.filter(x.jurisdictionalData, function (x) {
				return jurisdictionIds.indexOf(parseInt(x.jurisdictionId)) > -1;
			})).map(x => x.id);
		}
	},
	getcheckRTPRange: (state, getters) => (memo, letterPreviewJurisdiction, UseWeighted, self, selectedPayables) => {

		let rtpMinMaxPaytables = [];
		let minControl = memo.letterContentControls.find(x => x.label === "token_min_rtp_override" && (x.applicableJurisdictions.indexOf(letterPreviewJurisdiction) > -1 || x.applicableJurisdictions.length == 0));
		let minOverride = minControl !== undefined ? parseFloat(minControl.content) : 0;
		let maxControl = memo.letterContentControls.find(x => x.label === "token_max_rtp_override" && (x.applicableJurisdictions.indexOf(letterPreviewJurisdiction) > -1 || x.applicableJurisdictions.length == 0));
		let maxOverride = maxControl !== undefined ? parseFloat(maxControl.content) : 0;

		for (let i = 0; i < getters.paytables.length; i++) {
			let gamingGuideline = getters.gamingGuidelines.find(x => x.jurisdictionId === letterPreviewJurisdiction);

			let useWithoutIncrementRate = [parseInt(JURISDICTION.PENNSYLVANIAITL.id), parseInt(JURISDICTION.MICHIGANITL.id)].filter(x => x === letterPreviewJurisdiction).length > 0;

			let roundedMin = parseFloat((self.$format.percentage(useWithoutIncrementRate ? getters.paytables[i].minReturnExcludingProgressive : getters.paytables[i].minReturn, { minimumFractionDigits: 2 })).split("%")[0]);
			let roundedMax = parseFloat((self.$format.percentage(useWithoutIncrementRate ? getters.paytables[i].maxReturnExcludingProgressive : getters.paytables[i].maxReturn, { minimumFractionDigits: 2 })).split("%")[0]);

			// Manual Block Short Circuit.
			if (getters.paytables[i].manuallyBlocked) {
				rtpMinMaxPaytables.push({ paytable: getters.paytables[i] });
				continue;
			}

			if (((minOverride !== 0 && !isNaN(minOverride)) || (maxOverride !== 0 && !isNaN(maxOverride)))) {
				let rtpRequirement = { type: "Any", min: minOverride, max: maxOverride };
				if (roundedMin !== null && !isNaN(roundedMin) && minControl) {
					if (roundedMin < minOverride) {
						rtpMinMaxPaytables.push({ paytable: getters.paytables[i], rtpRequirement: rtpRequirement });
						continue;
					}
				}
				if (roundedMax !== null && !isNaN(roundedMax) && maxControl) {
					if (roundedMax > maxOverride) {
						rtpMinMaxPaytables.push({ paytable: getters.paytables[i], rtpRequirement: rtpRequirement });
					}
				}
			}
			else if (gamingGuideline) {
				let denomMaxValue = null;
				let paytableDenom = getters.paytables[i].riDenom; //0.01
				if (paytableDenom && letterPreviewJurisdiction === parseInt(JURISDICTION.RHODE_ISLAND.id)) {
					let denomRTPReqs = gamingGuideline.rtpRequirements.filter(x => x.type.toLowerCase() === getters.paytables[i].type.toLowerCase());

					//Check if denom is equal
					let equals = denomRTPReqs.filter(x => x.symbol === '=');
					for (let i = 0; i < equals.length; i++) {
						if (equals[i].denomOne == paytableDenom) {
							denomMaxValue = equals[i].rtp;
							break;
						}
					}

					//Check if denom is in range
					if (!denomMaxValue) {
						let ranges = denomRTPReqs.filter(x => x.symbol === '-');
						for (let i = 0; i < ranges.length; i++) {
							if (paytableDenom >= ranges[i].denomOne && paytableDenom <= ranges[i].denomTwo) {
								denomMaxValue = ranges[i].rtp;
								break;
							}
						}
					}
					
					//Check if denom is Greater than
					if (!denomMaxValue) {
						let greaterThan = denomRTPReqs.filter(x => x.symbol === ">");
						for (let i = 0; i < greaterThan.length; i++) {
							if (paytableDenom > greaterThan[i].denomOne) {
								denomMaxValue = greaterThan[i].rtp;
								break;
							}
						}
					}

					//Check if denom is Less than
					if (!denomMaxValue) {
						let lessThan = denomRTPReqs.filter(x => x.symbol === "<");
						for (let i = 0; i < lessThan.length; i++) {
							if (paytableDenom < lessThan[i].denomOne) {
								denomMaxValue = lessThan[i].rtp;
								break;
							}
						}
					}
				}

				let newRTPModel = [];
				let groupedGameTypes = _.groupBy(gamingGuideline.rtpRequirements, "type");

			
				for (let key of Object.keys(groupedGameTypes)) {
					let rtpRequirements = groupedGameTypes[key];
					let newMax = null;
					if (key.toLocaleLowerCase() === getters.paytables[i].type.toLowerCase() && paytableDenom && denomMaxValue) {
						newMax = denomMaxValue;
					}
					if (rtpRequirements.length > 0) {
						newRTPModel.push({
							type: rtpRequirements[0].type,
							min: _.minBy(rtpRequirements.filter(x => x.rtpType.toLowerCase() === "min"), "rtp")?.rtp,
							max: newMax ? newMax : _.maxBy(rtpRequirements.filter(x => x.rtpType.toLowerCase() === "max"), "rtp")?.rtp
						});
					}
				}

				let rtpRequirement = newRTPModel.find(x => x.type.toLowerCase() === getters.paytables[i].type.toLowerCase());

				if (!rtpRequirement) {
					if (getters.paytables[i].skillGame) {
						rtpRequirement = newRTPModel.find(x => x.type.toLowerCase() === GAME_TYPE.TRADITIONALSKILL.description.toLowerCase());
					} else {
						rtpRequirement = newRTPModel.find(x => x.type.toLowerCase() === GAME_TYPE.NONSKILL.description.toLowerCase());
					}
				}
				if (!rtpRequirement) {
					rtpRequirement = newRTPModel.find(x => x.type.toLowerCase() === GAME_TYPE.ANY.description.toLowerCase());
				}

				//If max is still null, then use max of Any game type
				if (rtpRequirement && !rtpRequirement.max) {
					let anyGameType = newRTPModel.find(x => x.type.toLowerCase() === GAME_TYPE.ANY.description.toLowerCase());
					rtpRequirement.max = anyGameType ? anyGameType.max : null;
				}

				if (rtpRequirement) {
					if (UseWeighted) {

						let jurSpecificAverage = letterPreviewJurisdiction === parseInt(JURISDICTION.DELAWARE_LOTTERY.id) ? getters.paytables[i].deAverage : getters.paytables[i].mdAverage;
						let roundedAvg = parseFloat((self.$format.percentage(jurSpecificAverage, { minimumFractionDigits: 2 })).split("%")[0]);

						if (!(roundedAvg >= rtpRequirement.min && roundedAvg <= rtpRequirement.max)) {
							rtpMinMaxPaytables.push({ paytable: getters.paytables[i], rtpRequirement: rtpRequirement });
						}
					} else {
						if (roundedMin !== null && rtpRequirement.min !== 0) {
							if (roundedMin < rtpRequirement.min) {
								rtpMinMaxPaytables.push({ paytable: getters.paytables[i], rtpRequirement: rtpRequirement });
								continue;
							}
						}
						if (roundedMax !== null && rtpRequirement.max && rtpRequirement.max !== 0) {
							if (roundedMax > rtpRequirement.max) {
								if (letterPreviewJurisdiction === parseInt(JURISDICTION.RHODE_ISLAND.id) &&
									getters.paytables[i].type.toLowerCase() !== GAME_TYPE.REEL.description.toLowerCase() &&
									getters.paytables[i].riDenom > 1) {
									continue;
								}
								rtpMinMaxPaytables.push({ paytable: getters.paytables[i], rtpRequirement: rtpRequirement });
							}
						}
					}
				}

				// Account for an 'Any' gaming guideline rule, which could be in addition to a specific type.
				// LETAUT-1203
				// https://gd-jira.gaminglabs.com/browse/LETAUT-1203
				let anyAny = newRTPModel.find(x => x.type.toLowerCase() === GAME_TYPE.ANY.description.toLowerCase());
				if (anyAny && letterPreviewJurisdiction === parseInt(JURISDICTION.RHODE_ISLAND.id)) {
					if (roundedMin !== null && anyAny.min !== 0) {
						if (roundedMin < anyAny.min) {
							rtpMinMaxPaytables.push({ paytable: getters.paytables[i], rtpRequirement: anyAny });
							continue;
						}
					}
					if (roundedMax !== null && anyAny.max !== 0) {
						if (roundedMax > anyAny.max) {
							rtpMinMaxPaytables.push({ paytable: getters.paytables[i], rtpRequirement: anyAny });
						}
					}
				}
			}
		}
		if (selectedPayables !== null && selectedPayables !== undefined) {
			if (typeof (selectedPayables) === "string") {
				selectedPayables = JSON.parse(selectedPayables);
			}
			const ids = selectedPayables.map(x => x.id);
			for (var i = 0; i < ids.length; i++) {
				let selectedSpecific = getters.paytables.find(x => x.id === ids[i]);
				if (selectedSpecific && !(rtpMinMaxPaytables.map(x => x.paytable.id).indexOf(selectedSpecific.id) > -1)) {
					rtpMinMaxPaytables.push({ paytable: selectedSpecific });
				}
			}
		}
		return rtpMinMaxPaytables;
	},
};
const actions = {
	///Expects a payload object like: { item: {}, jurId: 0, source: 'Clause', index: 0 }
	insertSource: (context, payload) => {
		context.commit('insertSource', payload);
	},
	setJurisdictionLetterDetails: (context, payload) => {
		context.commit('setJurisdictionLetterDetails', payload);
	},
	setComponents: (context, payload) => {
		let componentMemos = payload.memos;
		componentMemos.forEach(memo => {
			if (isNull(memo.sourceItemTargets)) {
				memo.sourceItemTargets = [];
			}
		});
		payload.components.forEach(component => {

			//applicableJurisdictions.push(parseInt(components[componentIter].jurisdictionalData[jurCounter].jurisdictionId));

			//associate this component to the correct memos
			let compJurIds = component.jurisdictionalData.map(jur => parseInt(jur.jurisdictionId));

			let targetMemos = componentMemos.filter(memo =>
				memo.applicableJurisdictions.length === 0 ||
				memo.applicableJurisdictions.some(appJur => compJurIds.includes(appJur)));
			targetMemos.forEach(memo => {
				AddSourceItemTarget(memo, component, "Component");
			});
		});
		//if a memo was for a subset of jurisdictions that resulted in no applicable components
		//*OR*
		//All of the business logic evaluated to false for all components
		//we'll have to deactivate the memo, as it has no applicable components
		componentMemos.filter(memo => memo.sourceItemTargets.length === 0 && !memo.letterContentControls.some(c => c.sourceId !== 'Components')).forEach(memo => { //PA- if memo is like coverpage, where you have source controls and non souce controls, you will have to include
			memo.include = false;
		});
		context.commit('setComponents', payload.components)

		
		let iGaming = !payload.components.some(x => x.testingTypeId !== 2);
		context.commit('setProjectiGaming', iGaming)
	},
	setCurrentSignatureData: (context, payload) => {
		context.commit('setCurrentSignatureData', payload);
	},
	setClauseReports({ commit, dispatch }, payload) {

		let clauseMemos = payload.memos;
		clauseMemos.forEach(memo => {
			if (isNull(memo.sourceItemTargets)) {
				memo.sourceItemTargets = [];
			}
		});

		let clauseJurIds = payload.reports.map(report => report.jurisdictionId);
		//we'll have to insert our clones back into the store via the proper setters
		let memoClones = [];
		//we'll clone each of these memos for each clause report. Clause Reports are for just one jurisdiction each time.
		clauseMemos.forEach(memo => {
			if (isNull(memo.applicableJurisdictions) || memo.applicableJurisdictions.length === 0) {

				clauseJurIds.skip(1).forEach(jurId => {
					let clone = cloneMemo(memo);
					clone.applicableJurisdictions = [jurId];
					memoClones.push(clone);
				});
				memo.applicableJurisdictions = [clauseJurIds[0]];
			}
			//if the memo is already for just one jurisdiction, we don't need to do anything
			else if (memo.applicableJurisdictions.length > 1) {
				while (memo.applicableJurisdictions.length > 1) {
					let clone = cloneMemo(memo);
					clone.applicableJurisdictions = [];
					clone.applicableJurisdictions.push(memo.applicableJurisdictions.pop());
					memoClones.push(clone);
				}
			}
		});

		//Different SourceProperties may be needed in the future. Right now, we're only providing support for
		//clause-related controls
		//let clauseControls = ccControls.filter(control => control.sourceProperty === "Clause");
		//Right now, assuming just one control. This will have to be expanded in the future
		//let clauseControl = clauseControls[0];
		payload.reports.forEach(report => {
			let clauses = report.clauseReports;
			for (let clauseIter = 0; clauseIter < clauses.length; clauseIter++) {

				//associate this clause to the correct memos
				//later, language will have to be considered. W00T
				let targetMemos = clauseMemos.filter(memo => memo.applicableJurisdictions.includes(clauses[clauseIter].jurisdictionId));
				targetMemos.forEach(memo => {
					AddSourceItemTarget(memo, clauses[clauseIter], "Clause");
				});
				let targetCloneMemos = memoClones.filter(memo => memo.applicableJurisdictions.includes(clauses[clauseIter].jurisdictionId));
				targetCloneMemos.forEach(memo => {
					AddSourceItemTarget(memo, clauses[clauseIter], "Clause");
				});
			}

		});
		dispatch('letter/insertClonedMemos', memoClones, { root: true });
		commit('setClauseReports', payload.reports);

	},
	setPaytables: ({ commit }, payload) => {
		let paytableMemos = payload.memos;
		paytableMemos.forEach(memo => {
			if (isNull(memo.sourceItemTargets)) {
				memo.sourceItemTargets = [];
			}
		});

		payload.paytables.forEach(paytable => {
			paytable._origMaxBet = paytable.maxBet;
			paytable._origMinSupplierReturn = paytable.minSupplierReturn;
			paytable._origMaxSupplierReturn = paytable.maxSupplierReturn;
			paytable._origMinInGameReturn = paytable.minInGameReturn;
			paytable._origMaxInGameReturn = paytable.maxInGameReturn;


			paytable.maxBetIsDirty = function () {
				let a = this._origMaxBet;
				let b = this.maxBet;
				if (a === '')
					a = null;
				if (b === '')
					b = null;

				return a != b
			};

			paytable.minSupplierReturnIsDirty = function () {
				let a = this._origMinSupplierReturn;
				let b = this.minSupplierReturn;
				if (a === '')
					a = null;
				if (b === '')
					b = null;

				return a != b
			};

			paytable.maxSupplierReturnIsDirty = function () {
				let a = this._origMaxSupplierReturn;
				let b = this.maxSupplierReturn;
				if (a === '')
					a = null;
				if (b === '')
					b = null;

				return a != b
			};

			paytable.minInGameReturnIsDirty = function () {
				let a = this._origMinInGameReturn;
				let b = this.minInGameReturn;
				if (a === '')
					a = null;
				if (b === '')
					b = null;

				return a != b
			};

			paytable.maxInGameReturnIsDirty = function () {
				let a = this._origMaxInGameReturn;
				let b = this.maxInGameReturn;
				if (a === '')
					a = null;
				if (b === '')
					b = null;

				return a != b
			};

			//paytables don't currently have a jurisdiction...
			paytableMemos.forEach(memo => {
				AddSourceItemTarget(memo, paytable, "Paytable");
			});
		});
		commit('setPaytables', payload.paytables);
	},
	setGamingGuidelines: (context, payload) => {
		context.commit('setGamingGuidelines', payload)
	},
	revertSignatureChanges: (context, payload) => {
		context.commit('revertSignatureChanges', payload);
	},
	setSignatureDataPerComponent: (context, payload) => {
		context.commit('setSignatureDataPerComponent', payload);
	},
	setSignatureData: (context, payload) => {
		context.commit('setSignatureData', payload);
	},
	setJurisdictionSplitInitializer: (context, payload) => {
		context.commit('setJurisdictionSplitInitializer', payload);
	},
	setForSplitJurisdictionData: (context, payload) => {
		context.commit('setForSplitJurisdictionData', payload);
	},
	setJIRARevocationInformation: (context, payload) => {
		context.commit('setJIRARevocationInformation', payload);
	},
	setJIRADisclosureWording: (context, payload) => {
		context.commit('setJIRADisclosureWording', payload);
	},
	setSpecializedJurisdictionClauseReportsForAllJur: (context, payload) => {
		context.commit('setSpecializedJurisdictionClauseReportsForAllJur', payload);
	},
	setSpecializedJurisdictionClauseReports: (context, payload) => {
		context.commit('setSpecializedJurisdictionClauseReports', payload);
	},
	setOCUITComponents: (context, payload) => {
		let ocMemos = payload.memos;
		ocMemos.forEach(memo => {
			if (isNull(memo.sourceItemTargets)) {
				memo.sourceItemTargets = [];
			}
		});

		payload.ocuit.forEach(oc => {
			ocMemos.forEach(memo => {
				AddSourceItemTarget(memo, oc, "OCUITs");
			});
		});
		context.commit('setOCUITComponents', payload.ocuit);
	},
	bulkUpdate_paytables: (context, payload) => {
		context.commit('bulkUpdate_paytables', payload);
	},
	setModifications: (context, payload) => {
		context.commit('setModifications', payload)
	},
	resetOriginalsFor_paytables: (context) => {
		context.commit('resetOriginalsFor_paytables');
	}
};

const mutations = {
	insertSource: (state, payload) => {
		switch (payload.source) {
			case "Clause":
				let report = state.evolution.clauseReports.find(doc => doc.jurisdictionId === payload.jurId);
				report.clauseReports.splice(payload.index, 0, payload.item);
				break;
		}

	},
	setJurisdictionLetterDetails: (state, payload) => {
		state.jurisdictions.letterDetails = payload;
	},
	setComponents: (state, payload) => {
		state.submission.components = payload;
	},
	setProjectiGaming: (state, payload) => {
		state.isProjectiGaming = payload;
	},
	setCurrentSignatureData: (state, payload) => {
		state.submission.currentSignatureData = payload;
	},
	setModifications: (state, payload) => {
		state.submission.modifications = payload;
	},
	setClauseReports: (state, payload) => {
		state.evolution.clauseReports = payload;
	},
	setPaytables: (state, payload) => {
		state.math.paytables = payload;
	},
	setGamingGuidelines: (state, payload) => {
		state.math.gamingGuidelines = payload;
	},
	revertSignatureChanges: (state, payload) => {
		payload.vm.$set(state.submission.currentSignatureData, payload.index, payload.signatureCopy);
	},
	setSignatureDataPerComponent: (state, payload) => {
		let comp = _.find(state.submission.components, ['id', payload.id]);
		comp.signatureData = payload.signatureData;
	},
	setSignatureData: (state, payload) => {
		payload.signatures.forEach(function (obj) {
			let comp = _.find(state.submission.components, ['id', obj.submissionId]);
			if (comp !== undefined) {
				comp.signatureData = obj.signatures;
			}
		});
	},
	setJurisdictionSplitInitializer: (state, payload) => {
		let control = payload.control;
		state.jurisdictions.letterDetails.forEach(function (obj) {
			payload.vm.$set(obj, control.label, "");
		});
	},
	setForSplitJurisdictionData: (state, payload) => {

		let applicableJurisdictions = payload.applicableJurisdictions;
		let label = payload.label;
		let content = payload.content;

		for (let i = 0; i < applicableJurisdictions.length; i++) {
			state.jurisdictions.letterDetails.forEach(function (obj) {
				//find match
				if (obj.jurisdictionId === applicableJurisdictions[i]) {
					payload.vm.$set(obj, label, content);
				}
			});
		}
	},
	setJIRARevocationInformation: (state, payload) => {
		state.jira.revocationInformation = payload;
	},
	setJIRADisclosureWording: (state, payload) => {
		state.jira.disclosureWording = payload;
	},
	setSpecializedJurisdictionClauseReports: (state, payload) => {
		const speJurClauseReportMemos = payload.memos;
		const specializedJurisdictionClauseReports = payload.specializedJurisdictionClauseReports.find(x => x.jurisdictionID === payload.previewJurisdictionId);

		speJurClauseReportMemos.forEach(memo => {
			if (isNull(memo.sourceItemTargets)) {
				memo.sourceItemTargets = [];
			}
		});

		speJurClauseReportMemos.forEach(memo => {
			if (memo.sourceProperty === "TestCases" && specializedJurisdictionClauseReports !== undefined) {
				specializedJurisdictionClauseReports.testCases.forEach(jurClause => {
					AddSourceItemTarget(memo, jurClause, "SpecializedJurisdictionClauseReport");
				});
			}
			else if (memo.sourceProperty === "EvaluatedClauses" && specializedJurisdictionClauseReports !== undefined) {
				specializedJurisdictionClauseReports.evaluatedClauses.forEach(jurClause => {
					AddSourceItemTarget(memo, jurClause, "SpecializedJurisdictionClauseReport");
				});
			}
		});
		state.evolution.specializedJurisdictionClauseReports = specializedJurisdictionClauseReports;
	},
	setSpecializedJurisdictionClauseReportsForAllJur: (state, payload) => {
		state.specializedJurisdictionClauseReportsForAllJur = payload;
	},
	setOCUITComponents: (state, payload) => {
		state.OCUITComponents = payload;
	},
	bulkUpdate_paytables: (state, payload) => {
		for (let i = 0; i < state.math.paytables.length; i++) {
			let temp = { ...state.math.paytables[i] };

			if (payload.maxBet !== null) {
				temp.maxBet = payload.maxBet;
			}
			if (payload.minSupplierReturn !== null) {
				temp.minSupplierReturn = payload.minSupplierReturn;
			}
			if (payload.maxSupplierReturn !== null) {
				temp.maxSupplierReturn = payload.maxSupplierReturn;
			}
			if (payload.minInGameReturn !== null) {
				temp.minInGameReturn = payload.minInGameReturn;
			}
			if (payload.maxInGameReturn !== null) {
				temp.maxInGameReturn = payload.maxInGameReturn;
			}

			state.math.paytables.splice(i, 1, temp);
		}
	},
	resetOriginalsFor_paytables: (state) => {
		for (let i = 0; i < state.math.paytables.length; i++) {
			let temp = { ...state.math.paytables[i] };
			temp._origMaxBet = temp.maxBet;
			temp._origMinSupplierReturn = temp.minSupplierReturn;
			temp._origMaxSupplierReturn = temp.maxSupplierReturn;
			temp._origMinInGameReturn = temp.minInGameReturn;
			temp._origMaxInGameReturn = temp.maxInGameReturn;
			state.math.paytables.splice(i, 1, temp);
		}
	}
};

export default {
	namespaced: true,
	state,
	getters,
	actions,
	mutations
};



