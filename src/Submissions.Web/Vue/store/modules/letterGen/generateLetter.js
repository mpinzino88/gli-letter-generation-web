import { isNotNull } from '@/common';

//This module contains session-specific information. Data that will not really be saved anywhere. 
//Things like whether the PDF is being generated, the current user, etc

const state = {
	config: {
		answersLoaded: {},
		blankMemo: {},
		evolutionInstanceIsSecure: true, // An Evolution Instance is considered secure if all ComponentGroups of the respective Instance are 'Double-Locked'
		bundleLoaded: false,
		generatingPDF: false,
		savingHTML: false,
		pageTypes: [{
			name: "A4",
			height: 1010,
			width: 793,
			threshold: 0.5
		}],
		currentMode: 0, //0 - ALL, 1 = EVOLUTION, 2 - MODIFICATIONS, 4 - GAMEDESCRIPTIONS, 8 - MATH
		contentChanged: 0,//0 - no update, 1 - growing, 2 - shrinking
		contentChangedMemoId: 0,
		templateSelector: [],
		lettersInitializing: [],//list of JurIds that are initializing right now
		lettersInitialized: [],//list of JurIds that are done initializing
		sectionsUpdatingPages: [],//list of objects with { jurId: 0, sectionId 0 }
		selectedTemplateId: -1,
		letterPreviewJurisdictions: [],
		letterPreviewJurisdiction: 0,
		secondaryLanguageJurisdictions: [59, 132, 282], //peru, panama, chile
		secondaryLetterLanguage: "",
		isSecondaryLanguageLetter: false,
		workspaceJurisdictions: [],
		currentPageType: {},
		selectedMemo: { localId: -1, sourceId: -1, sourceProperty: "" },
		footerPageTemplate: { internalPageId: null, memoIds: [] },
	},
	notifications: {
		saveResults: {
			show: false
		}
	},
	user: {
		userId: 0,
		hasLetterGenSubSigPermission: false,
		hasLetterGenEditPermission: false,
		permissionSet: {}
	}
};

const getters = {
	answersLoaded: state => state.config.answersLoaded,
	generateLetterState: state => state,
	evolutionInstanceIsSecure: state => state.config.evolutionInstanceIsSecure,
	bundleLoaded: state => state.config.bundleLoaded,
	generatingPDF: state => state.config.generatingPDF,
	savingHTML: state => state.config.savingHTML,
	pageTypes: state => state.config.pageTypes,
	isSecondaryLanguageLetter: state => state.config.isSecondaryLanguageLetter,
	secondaryLetterLanguage: state => state.config.secondaryLetterLanguage,
	letterPreviewJurisdiction: state => state.config.letterPreviewJurisdiction,
	letterPreviewJurisdictions: state => state.config.letterPreviewJurisdictions,
	secondaryLanguageJurisdictions: state => state.config.secondaryLanguageJurisdictions,
	workspaceJurisdictions: state => state.config.workspaceJurisdictions,
	saveResults: state => state.notifications.saveResults,
	selectedMemo: state => state.config.selectedMemo,
	letterInitializing: state => jurId => state.config.lettersInitializing.includes(jurId),
	letterInitialized: state => jurId => state.config.lettersInitialized.includes(jurId),
	sectionsUpdatingPages: state => jurId => state.config.sectionsUpdatingPages.filter(sup => sup.jurId === jurId),
	currentMode: state => state.config.currentMode,
	isDefaultMode: state => state.config.currentMode === 0,
	hasEvoMode: state => state.config.currentMode === 1 || state.config.currentMode === 0,
	hasModMode: state => state.config.currentMode === 2 || state.config.currentMode === 0,
	hasGameDescriptionsMode: state => state.config.currentMode === 4 || state.config.currentMode === 0,
	hasMathMode: state => state.config.currentMode === 8 || state.config.currentMode === 0,
	contentChanged: state => state.config.contentChanged,
	contentChangedMemoId: state => state.config.contentChangedMemoId,
	currentPageType: state => state.config.currentPageType,
	showApplicableJurisdictions: (state, getters) => (applyJurIds) => {
		var jurName = '';
		if (applyJurIds.length > 0) {
			for (let i = 0; i < getters.letterPreviewJurisdictions.length; i++) {
				var jurDisplay = (applyJurIds.includes(parseInt(getters.letterPreviewJurisdictions[i].value))) ? getters.letterPreviewJurisdictions[i].text : null;
				if (isNotNull(jurDisplay)) {
					if (Em(jurName)) {
						jurName = jurDisplay;
					}
					else {
						jurName += ", " + jurDisplay;
					}
				}
			}
		}
		return jurName;
	},
	user: state => state.user,
	permissionSet: state => state.user.permissionSet,
};

const actions = {
	//dispatch('notification/triggerSelfDismissingNotifcation', {...}, {root:true})
	//dispatch('evolution/setClauseReport', [], { root: true });
	//dispatch('letter/insertClonedMemos', memoClones, { root: true });
	//rootGetters["user/isUserAuthenticated"]
	//https://vuex.vuejs.org/api/#mutations see the Actions part
	initializeLetterGen: ({ dispatch, rootGetters, getters, commit }, payload) => {
		commit('setMode', payload.currentMode);
		commit('setUserPermissionSet', payload.permissionSet);
		commit('setLetterGenSubSigPermission', payload.editLetterGenSubSig);
		commit('setLetterGenEditPermission', payload.editLetterGeneration);

		if (getters.hasEvoMode) {
			dispatch('setEvolutionInstanceIsSecure', payload.evolutionInfo.evolutionInstanceIsSecure);
		}
		dispatch('letterGenSources/setModifications', payload.modifications, { root: true });
		dispatch('letter/setProjectInfo', payload.projectInfo, { root: true });

		dispatch('setLetterJurisdictionOptions', payload.letterJurisdictionOptions);

		dispatch('letter/setLetterContent', payload.letter, { root: true });

		dispatch('letterGenSources/setJurisdictionLetterDetails', payload.jurisdictionData, { root: true });

		dispatch('setBlankMemo', payload.blankMemo);
		dispatch('letterGenSources/setJIRARevocationInformation', payload.jiraInformation.revocationInformations, { root: true });
		dispatch('letterGenSources/setJIRADisclosureWording', payload.jiraInformation.disclosureInformations, { root: true });

		let ocuitPayload = { ocuit: payload.ocuitComponents, memos: rootGetters["letter/ocuitMemos"] };
		dispatch('letterGenSources/setOCUITComponents', ocuitPayload, { root: true });

		dispatch('letterGenSources/setSpecializedJurisdictionClauseReportsForAllJur', payload.specializedJurisdictionClauseReports, { root: true })

		let specPayload = { specializedJurisdictionClauseReports: payload.specializedJurisdictionClauseReports, previewJurisdictionId: getters.letterPreviewJurisdiction, memos: rootGetters["letter/specializedJurClauseReportMemos"] };
		dispatch('letterGenSources/setSpecializedJurisdictionClauseReports', specPayload, { root: true })


		if (getters.hasMathMode) {
			let paytablePayload = { paytables: payload.paytables, memos: rootGetters["letter/paytableMemos"] };
			dispatch('letterGenSources/setPaytables', paytablePayload, { root: true });

			dispatch('letterGenSources/setGamingGuidelines', payload.gamingGuidelines, { root: true });
		}

		if (getters.hasEvoMode) {
			let crPayload = { reports: payload.clauseReports, memos: rootGetters["letter/clauseReportMemos"] };
			dispatch('letterGenSources/setClauseReports', crPayload, { root: true });


			let mainClauseReport = rootGetters["evolution/clauseReport"];
			mainClauseReport.hideClausePanel = false;

			let crs = rootGetters["letterGenSources/clauseReports"];
			let curJur = getters.letterPreviewJurisdiction;
			let crCounts = getClauseReportCounts(crs, curJur);
			dispatch('evolution/setClauseReportCounts', crCounts, { root: true });

			let crsAgain = rootGetters["letterGenSources/jurisdictionClauses"](curJur);
			dispatch('evolution/setClauseReport', crsAgain, { root: true });
		}
	},
	setEvolutionInstanceIsSecure: (context, payload) => {
		context.commit('setEvolutionInstanceIsSecure', payload);
	},
	setContentChanged: (context, payload) => {
		context.commit('setContentChanged', payload);
	},
	setContentChangedMemoId: (context, payload) => {
		context.commit('setContentChangedMemoId', payload);
	},
	setSaveResults: (context, payload) => {
		context.commit('setSaveResults', payload);
	},

	setBundleLoaded: (context, payload) => {
		context.commit('setBundleLoaded', payload);
	},
	setSavingHTML: (context, payload) => {
		context.commit('setSavingHTML', payload);
	},
	setGeneratingPDF: (context, payload) => {
		context.commit('setGeneratingPDF', payload);
	},

	//payload => {jurId: 0, sectionId: 0 }
	startUpdatingSectionPaging: (context, payload) => {
		context.commit('startUpdatingSectionPaging', payload);
	},
	//payload => {jurId: 0, sectionId: 0 }
	finishUpdatingSectionPaging: (context, payload) => {
		context.commit('finishUpdatingSectionPaging', payload);
	},
	startLetterInitializing: (context, payload) => {
		context.commit('startLetterInitializing', payload);
	},
	finishLetterInitializing: (context, payload) => {
		context.commit('finishLetterInitializing', payload);
	},
	setLetterJurisdictionOptions: ({ commit, dispatch }, payload) => {
		commit('setLetterJurisdictionOptions', payload);
		if (payload != null) {
			dispatch('setLetterSelectedJurisdiction', payload[0].value);
		}

	},

	setLetterSelectedJurisdiction: (context, payload) => {
		context.commit('setLetterSelectedJurisdiction', payload);
	},
	setWorkspaceJurisdictions: (context, payload) => {
		context.commit('setWorkspaceJurisdictions', payload);
	},

	updateFirstPageSection: (context, payload) => {
		context.commit('updateFirstPageSection', payload);
	},

	setBlankMemo: (context, payload) => {
		context.commit('setBlankMemo', payload);
	},

	moveMemo: (context, payload) => {
		context.commit('moveMemo', payload);
	},

	jumpToMemo: (context, memo) => {
		let locId = memo.localId;
		context.commit('setJumpToMemoId', locId);
		if (typeof memo.applicableJurisdictions !== 'undefined' && memo.applicableJurisdictions !== null && memo.applicableJurisdictions.length > 0) {
			let allavailable = state.config.letterPreviewJurisdictions.map(x => x.value);
			let setJur = memo.applicableJurisdictions.find(x => allavailable.includes(x));
			context.commit('setLetterSelectedJurisdiction', memo.applicableJurisdictions.indexOf(context.state.config.letterPreviewJurisdiction) == -1 && setJur != null ? setJur : context.state.config.letterPreviewJurisdiction);
		}
	},

	setSelectedMemo: (context, memo) => {
		const selectedMemo = {
			localId: memo.localId,
			sourceId: memo.sourceId,
			sourceProperty: memo.sourceProperty
		};

		context.commit('setSelectedMemo', selectedMemo);
		if (typeof memo.applicableJurisdictions !== 'undefined' && memo.applicableJurisdictions !== null && memo.applicableJurisdictions.length > 0) {
			let allavailable = state.config.letterPreviewJurisdictions.map(x => x.value);
			let setJur = memo.applicableJurisdictions.find(x => allavailable.includes(x));
			context.commit('setLetterSelectedJurisdiction', memo.applicableJurisdictions.indexOf(context.state.config.letterPreviewJurisdiction) == -1 && setJur != null ? setJur : context.state.config.letterPreviewJurisdiction);
		}
	},

	splitClause({ commit }, payload) {
		commit('splitClause', payload);

	},

	addBlankMemo: (context, payload) => {
		context.commit('addBlankMemo', payload);
	},

	setCurrentPageType: (context, payload) => {
		context.commit('setCurrentPageType', payload);
	},
	addCustomLoadedMemos: (context, payload) => {
		context.commit('addCustomLoadedMemos', payload);
	},
	updateJurisdictionSelection: (context, payload) => {
		context.commit('updateJurisdictionSelection', payload);
	},
	setSecondaryLetterLanguage: (context, payload) => {
		context.commit('setSecondaryLetterLanguage', payload);
	},
	setIsSecondaryLanguage: (context, payload) => {
		context.commit('setIsSecondaryLanguage', payload);
	},
	setUserPermissionSet: (context, payload) => {
		context.commit('setUserPermissionSet', payload);
	},
	setMode: (context, payload) => {
		context.commit('setMode', payload);
	},
};

const mutations = {
	setMode: (state, payload) => {
		state.config.currentMode = payload;
	},
	setLetterGenSubSigPermission: (state, payload) => {
		state.user.hasLetterGenSubSigPermission = payload;
	},
	setLetterGenEditPermission: (state, payload) => {
		state.user.hasLetterGenEditPermission = payload;
	},
	setEvolutionInstanceIsSecure: (state, payload) => {
		state.config.evolutionInstanceIsSecure = payload;
	},
	setContentChanged: (state, payload) => {
		state.config.contentChanged = payload;
	},
	setContentChangedMemoId: (state, payload) => {
		state.config.contentChangedMemoId = payload;
	},
	setBundleLoaded: (state, payload) => {
		state.config.bundleLoaded = payload;
	},
	//payload => {jurId: 0, sectionId: 0 }
	startUpdatingSectionPaging: (state, payload) => {
		let exists = state.config.sectionsUpdatingPages.some(sup => sup.jurId === payload.jurId && sup.sectionId === payload.sectionId);
		if (!exists) {
			state.config.sectionsUpdatingPages.push(payload);
		}
	},
	//payload => {jurId: 0, sectionId: 0 }
	finishUpdatingSectionPaging: (state, payload) => {//const index = Data.findIndex(item => item.name === 'John');
		let index = state.config.sectionsUpdatingPages.findIndex(sup => sup.jurId === payload.jurId && sup.sectionId === payload.sectionId);
		if (index >= 0) {
			state.config.sectionsUpdatingPages.splice(index, 1);
		}
	},
	//payload => jurId (as an int)
	startLetterInitializing: (state, payload) => {
		if (!state.config.lettersInitializing.includes(payload)) {
			state.config.lettersInitializing.push(payload);
		}
	},
	//payload => jurId (as an int)
	finishLetterInitializing: (state, payload) => {
		let index = state.config.lettersInitializing.indexOf(payload);
		if (index >= 0) {
			state.config.lettersInitializing.splice(index, 1);
		}
		if (!state.config.lettersInitialized.includes(payload)) {
			state.config.lettersInitialized.push(payload);
		}
	},
	setSavingHTML: (state, payload) => {
		state.config.savingHTML = payload;
	},
	setGeneratingPDF: (state, payload) => {
		state.config.generatingPDF = payload;
	},

	setSaveResults: (state, payload) => {
		state.notifications.saveResults = payload;
	},

	setLetterJurisdictionOptions: (state, payload) => {
		state.config.letterPreviewJurisdictions = payload;
	},

	setLetterSelectedJurisdiction: (state, payload) => {
		state.config.letterPreviewJurisdiction = parseInt(payload, 10);
	},

	setSecondaryLetterLanguage: (state, payload) => {
		let lanaguage = ""
		let exists = payload.find(x => x.secondaryLanguageId != 0 && x.secondaryLanguageId != null)
		if (exists != null) {
			lanaguage = exists.secondaryLanguage;
		}
		//get Desc
		state.config.secondaryLetterLanguage = lanaguage;
	},

	setIsSecondaryLanguage: (state, payload) => {
		state.config.isSecondaryLanguageLetter = payload;
	},

	setWorkspaceJurisdictions: (state, payload) => {
		state.config.workspaceJurisdictions = payload;
	},

	setBlankMemo: (state, payload) => {
		state.config.blankMemo = payload;
	},

	moveMemo: (state, payload) => {
		//if (isNull(payload.section)) return;
		//var fromIndex = payload.section.memos.indexOf(payload.memoToMove);
		//var toIndex = payload.section.memos.indexOf(payload.destinationMemo);
		//payload.section.memos.splice(toIndex, 0, payload.section.memos.splice(fromIndex, 1)[0]);
		let destinationOrder = payload.destinationMemo.orderId;
		payload.section.memos.forEach(memo => {
			if (memo.orderId >= destinationOrder) {
				memo.orderId++;
			}
		});
		payload.memoToMove.orderId = destinationOrder;
		payload.section.memos.sort((a, b) => a.orderId - b.orderId);
	},

	addBlankMemo: (state, payload) => {
		var clone = cloneMemo(state.config.blankMemo);
		clone.orderId = payload.orderId;
		if (payload.jurId > 0) {
			clone.applicableJurisdictions = [payload.jurId];
		}
		else {
			clone.applicableJurisdictions = [];
		}
		clone.paging.push({ page: payload.page, localId: clone.localId, jurisdictionId: payload.jurId });

		if (clone.custom) {
			clone.letterContentControls[0].applicableJurisdictions = clone.applicableJurisdictions;
			clone.anchorMemoId = payload.anchorMemoId;
		}

		let childCount = payload.section.memos.length;
		if (payload.index === childCount - 1) {

			payload.section.memos.push(clone);
		}
		else {
			payload.section.memos.filter(memo => memo.orderId >= payload.orderId).forEach(memo => memo.orderId++);
			payload.section.memos.splice(payload.index, 0, clone);

		}


	},
	// payload = {section, newMemos}
	addCustomLoadedMemos: (state, payload) => {
		var newMemos = payload.newMemos;
		newMemos.forEach(newMemo => {
			payload.section.memos.push(newMemo);
		});
	},
	setSelectedMemo: (state, payload) => {
		if (state.config.currentMode === 0) { //As per Zack in single mode, do not highlight
			state.config.selectedMemo = payload;
		}
	},
	setCurrentPageType: (state, payload) => {
		state.config.currentPageType = payload;
	},
	updateJurisdictionSelection: (state, payload) => {
		payload.memo.applicableJurisdictions = payload.value.map(Number);
		payload.memo.letterContentControls[0].applicableJurisdictions = payload.value.map(Number);
	},
	setUserPermissionSet: (state, payload) => {
		state.user.permissionSet = payload;
	},
};
export default {
	namespaced: true,
	state,
	getters,
	actions,
	mutations
};