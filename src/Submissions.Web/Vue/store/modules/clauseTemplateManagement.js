﻿import Vue from 'vue';
import { isNull } from '@/common';
import { isNotNull } from '@/common';
import api from '../../api/index';
import { rejects } from 'assert';
const baseUrl = 'tools/clausetemplatemanagement/';

const state = {
	config: {
		mode: "list",
		modes: ["list", "add", "edit"],
		list: 0,
		add: 1,
		edit: 2
	},
	jurisdictions: [],
	manufacturers: [],
	templates: [],
	selectedJurisdictionId: -1,
	selectedManufacturerId: "",
	selectedTemplateId: -1,
	clauses: [],
	label: "",
	loadingTemplates: false,
	loadingClauses: false,
	editingTemplate: {},
	validationMessage: "",
	exceptionMessage: "",
	savingTemplate: false,
	deactivatingTemplate: false,
	modifiedManufacturer: false,
	modifiedJurisdiction: false
};

const getters = {
	savingTemplate: state => state.savingTemplate,
	mode: state => state.config.mode,
	isListMode: state => state.config.mode === state.config.modes[state.config.list],
	isEditMode: state => state.config.mode === state.config.modes[state.config.edit],
	isAddMode: state => state.config.mode === state.config.modes[state.config.add],
	validationMessage: state => state.validationMessage,
	exceptionMessage: state => state.exceptionMessage,
	jurisdictions: state => state.jurisdictions,
	manufacturers: state => state.manufacturers,
	templates: state => state.templates,
	jurisdictionSelected: state => state.selectedJurisdictionId !== -1,
	manufacturerSelected: state => state.selectedManufacturerId !== "",
	templateSelected: state => state.selectedTemplateId !== -1,
	labelSelected: state => isNotNull(state.label) && state.label.length > 0,
	clausesLoaded: state => isNotNull(state.clauses) && state.clauses.length > 0,
	templatesLoaded: state => isNotNull(state.templates) && state.templates.length > 0,
	selectedJurisdictionId: state => state.selectedJurisdictionId,
	selectedManufacturerId: state => state.selectedManufacturerId,
	selectedTemplateId: state => state.selectedTemplateId,//isNull(state.selectedTemplate) ? -1 : state.selectedTemplate.value,
	clauses: state => state.clauses,
	label: state => state.label,
	loadingTemplates: state => state.loadingTemplates,
	modifiedClauses: state => state.clauses.filter(clause => clause.template.modified || clause.nativeTemplate.modified),
	creatingNewTemplate: (state, getters) => state.selectedTemplateId === 0,
	loadingClauses: state => state.loadingClauses,
	editingTemplate: state => state.editingTemplate,
	deactivatingTemplate: state => state.deactivatingTemplate,
	modifiedManufacturer: state => state.modifiedManufacturer,
	modifiedJurisdiction: state => state.modifiedJurisdiction
};

const actions = {
	setSavingTemplate: (context, payload) => {
		context.commit('setSavingTemplate', payload);
	},
	setValidationMessage: (context, payload) => {
		context.commit('setValidationMessage', payload);
	},
	setExceptionMessage: (context, payload) => {
		context.commit('setExceptionMessage', payload);
	},
	setAddMode: (context, payload) => {
		context.commit('setAddMode');
	},
	setEditMode: ({ commit, state, getters, dispatch }, payload)  => {
		let template = getters.templates.find(temp => temp.id === payload);
		commit('setEditingTemplate', template);
		commit('setEditMode', payload);
		dispatch('loadClauses');
	},
	setListMode: (context, payload) => {
		context.commit('setListMode');
	},
	resetSelections: ({ commit, state, getters, dispatch }, payload) => {
		commit('setSelectedJurisdictionId', -1);
		commit('setSelectedManufacturerId', -1);
		commit('setLabel', '');
		commit('setClauses', []);
		dispatch('evolution/setClauseReport', [], { root: true });
	},
	setSelectedJurisdictionId: (context, payload) => {
		context.commit('setSelectedJurisdictionId', payload);

	},
	setJurisdictions: (context, payload) => {
		return new Promise((resolve, reject) => {
			//context.commit('setJurisdictions', payload); 
			api.get(baseUrl + 'GetJurisdictions', {//int jurisdictionId, string manufacturerId
				params: {
					//jurisdictionId: getters.selectedJurisdictionId,
					//manufacturerId: getters.selectedManufacturerId
				}
			})
				.then(response => {
					const result = response.data;
					context.commit('setJurisdictions', result);
				})
				.catch(e => {
				})
				.finally(x => {

				});
		});
	},
	setSelectedManufacturerId: (context, payload) => {
		context.commit('setSelectedManufacturerId', payload);
	},
	setManufacturers: (context, payload) => {
		return new Promise((resolve, reject) => {
			//context.commit('setManufacturers', payload); 
			api.get(baseUrl + 'GetManufacturers', {//int jurisdictionId, string manufacturerId
				params: {
					//jurisdictionId: getters.selectedJurisdictionId,
					//manufacturerId: getters.selectedManufacturerId
				}
			})
				.then(response => {
					const result = response.data;
					context.commit('setManufacturers', result);
				})
				.catch(e => {
				})
				.finally(x => {

				});
		});
	},
	setSelectedTemplateId: (context, payload) => {
		context.commit('setSelectedTemplateId', payload);
	},
	setTemplates: (context, payload) => {
		context.commit('setTemplates', payload);
	},
	setClauses: (context, payload) => {
		context.commit('setClauses', payload);
	},
	setLabel: (context, payload) => {
		context.commit('setLabel', payload);
	},
	setLoadingTemplates: (context, payload) => {
		context.commit('setLoadingTemplates', payload);
	},
	loadApplicableTemplates: ({ commit, getters }, payload) => {
		return new Promise((resolve, reject) => {
			commit('setLoadingTemplates', true);
			api.get(baseUrl + 'LoadApplicableTemplates', {//int jurisdictionId, string manufacturerId
				params: {
					//jurisdictionId: getters.selectedJurisdictionId,
					//manufacturerId: getters.selectedManufacturerId
				}
			})
				.then(response => {
					const result = response.data;
					commit('setTemplates', result);
					commit('setLoadingTemplates', false);
					resolve(0);
				})
				.catch(e => {
					reject(e);
				})
				.finally(x => {

				});
		});
	},
	loadClauses: ({ commit, state, getters, dispatch }) => {
		commit('setLoadingClauses', true);
		api.get(baseUrl + 'LoadClauses', {
			params: {
				jurisdictionId: getters.selectedJurisdictionId,
				templateId: getters.selectedTemplateId
			}
		})
			.then(response => {
				const result = response.data;
				commit('setClauses', result);
				dispatch('evolution/setClauseReport', getters.clauses, { root: true });
				commit('setLoadingClauses', false);
			})
			.catch(e => {
			})
			.finally(x => {

			});
		//});
	},
	setModifiedManufacturer: (context, payload) => {
		context.commit('setModifiedManufacturer', payload);
	},
	setModifiedJurisdiction: (context, payload) => {
		context.commit('setModifiedJurisdiction', payload);
	},
	resetModifiedFlag: (context, payload) => {
		context.commit('resetModifiedFlag', payload);
	},

	updateTemplate: ({ commit, state, getters, dispatch }) => {
		return new Promise((resolve, reject) => {
			//commit('setSavingTemplate', true);
			api.post(baseUrl + 'UpdateTemplatePost', {
				formTemplate: JSON.stringify(state.clauses),
				templateId: getters.selectedTemplateId
			})
				.then(response => {
					const result = response.data;
					if (result !== 0) {
						//this.exceptionMessage = result.Message;
						reject(result.Message);
					}
					resolve(0);
					//commit('setSavingTemplate', false);
				})
				.catch(e => {
					reject(e);
				})
				.finally(x => {

				});
		});
	},
	saveNewTemplate: ({ commit, state, getters, dispatch }) => {
		return new Promise((resolve, reject) => {
			//commit('setSavingTemplate', true);
			api.post(baseUrl + 'SaveNewTemplatePost', {// int jurisdictionId, string manufacturerId, string label
				formTemplate: JSON.stringify(state.clauses),
				jurisdictionId: getters.selectedJurisdictionId,
				manufacturerId: getters.selectedManufacturerId,
				label: state.label
			})
				.then(response => {
					const result = response.data;
					console.log('saveNewTemplate.then');
					if (isNotNull(result) && isNotNull(result.id)) {
						commit('setEditMode', result);
						console.log('saveNewTemplate.then set edit mode');
					}
					else {
						reject(result.Message);
						console.log('saveNewTemplate.then error');
					}
				})
				.catch(e => {
					reject(e);
				})
				.finally(x => {
					resolve(0);
				});
		});
	},

	deactivateTemplate: ({ commit, state, getters, dispatch }, payload) => {
		//TODO Implement
		commit('setDeactivatingTemplate', true);

		api.get(baseUrl + 'DeactivateTemplate', {//int jurisdictionId, string manufacturerId, string templateId
			params: {
				templateId: payload
			}
		})
			.then(response => {
				dispatch('loadApplicableTemplates')
					.then(response => {
						commit('setDeactivatingTemplate', false);
					});
			})
			.catch(e => {
				console.log(e);
			})
			.finally(x => {

			});

	}


};

const mutations = {
	setDeactivatingTemplate: (state, payload) => {
		state.deactivatingTemplate = payload;
	},
	setSavingTemplate: (state, payload) => {
		state.savingTemplate = payload;
	},
	setValidationMessage: (state, payload) => {
		state.validationMessage = payload;
	},
	setExceptionMessage: (state, payload) => {
		state.exceptionMessage = payload;
	},
	setEditingTemplate: (state, payload) => {
		state.editingTemplate = payload;
	},
	setLoadingClauses: (state, payload) => {
		state.loadingClauses = payload;
	},
	setAddMode: (state) => {
		state.config.mode = state.config.modes[state.config.add];
	},
	setEditMode: (state, payload) => {
		state.selectedTemplateId = payload.id;
		state.editingTemplate = payload;
		state.label = payload.label;
		state.selectedJurisdictionId = payload.jurisdictionId;
		state.selectedManufacturerId = payload.manufacturerId;
		state.config.mode = state.config.modes[state.config.edit];
	},
	setListMode: (state) => {
		state.config.mode = state.config.modes[state.config.list];
	},

	setSelectedJurisdictionId: (state, payload) => {
		if (isNull(payload)) {
			state.selectedJurisdictionId = -1;
		}
		else {
			state.selectedJurisdictionId = payload;
		}
	},
	setJurisdictions: (state, payload) => {
		state.jurisdictions = payload;
	},
	setSelectedManufacturerId: (state, payload) => {
		if (isNull(payload)) {
			state.selectedManufacturerId = "";
		}
		else {
			state.selectedManufacturerId = payload;
		}
	},
	setManufacturers: (state, payload) => {
		state.manufacturers = payload;
	},
	setSelectedTemplateId: (state, payload) => {
		state.selectedTemplateId = payload;
	},
	setTemplates: (state, payload) => {
		state.templates = payload;
	},
	setClauses: (state, payload) => {
		state.clauses = payload;
	},
	setLabel: (state, payload) => {
		state.label = payload;
	},
	setLoadingTemplates: (state, payload) => {
		state.loadingTemplates = payload;
	},
	setModifiedManufacturer: (state, payload) => {
		state.modifiedManufacturer = payload;
	},
	setModifiedJurisdiction: (state, payload) => {
		state.modifiedJurisdiction = payload;
	},
	resetModifiedFlag: (state, payload) => {
		let modified = state.clauses.filter(clause => clause.template.modified && clause.nativeTemplate.modified);
		modified.forEach(clause => clause.template.modified = clause.nativeTemplate.modified = false);
	}
};

export default {
	namespaced: true,
	state,
	getters,
	actions,
	mutations
};