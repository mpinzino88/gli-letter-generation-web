﻿import Vue from 'vue';

const state = {
	instance: {
		projectName: '',
		selectedComponentGroupId: '',
		selectedComponentLockStatus: '',
		showHelpModalPopup: false,
		showInstanceInformationPopup: false
	},
	functionalRole: {
		functionalRoles: [],
		hideFunctionalRolePanel: true,
		hideFunctionalRoleSpinner: true,
		collapsedFunctionalRolePanel: false,
	},
	filter: {
		hideButtons: true,
		hideFilterPanel: true,
		hideJurisdictionAndDocumentPanel: true,
		filterChecked: false,
		hideJurisdictionWarning: true,
		allContributingUsers: [],
		selectedContributingUsers: [],
		updateFilterCounts: false,
		jurisdictionFilter: {
			jurisdictionMultiSelectFilter: {
				empty: false
			},
			jurisdictionIdsFilter: [], //maintains all jurisdictions
			currentSelectedJurisdictionIds: [], //what has been selected
			loadedSelectedFilterJurisdictionIds: [], //what has been loaded.
		},
		nativeLanguageFilter: {
			nativeLanguageSelectFilter: {
				empty: false
			},
			nativeLanguageIds: [],
			compGroupID: ''
		},
		documentFilter: {
			documentMultiSelectFilter: {
				disable: false,
				empty: false
			},
			documentIds: [],
			jurisdictionIds: [],
			compGroupID: '',
			nativeLanguage: ''
		}
	},
	applicableTestCase: {
		applicableTestCases: [],
		hideApplicableTestCase: true,
		hasNevada: false,
		hasPeru: false,
		collapsedApplicableTestCasePanel: false
	},
	clauseReport: {
		clauseReport: [],
		documentNameHeader: '',
		hideClausePanel: true,
		hideClauseReportSpinner: true,
		selectedClauseID: '',
		counts: {
			pass: 0,
			na: 0,
			naStar: 0,
			fail: 0
		}
	},
	review: {
		//hideReviewPanel: true,
		answerHistoryEmailPopUp: false,
		fileNumber: '',
		testScript: {
			reviewViewModel: [],
			hideQuestionsPanel: true,
			hideTestScriptSpinner: true,
			hideTestScriptPanel: true,
			hideTestScriptCountPanel: true,
			counts: {
				totalCount: 0,
				passCount: 0,
				failCount: 0,
				naCount: 0,
				conditionalCount: 0,
				openJiras: 0
			}
		},
		testCase: {
			testCase: [],
			hideTestCaseSpinner: true,
			hideTestCasePanel: true,
			testCaseAllDisplay: false
		},
		showConditionalBranchPopup: false,
		showGPSLinkPopup: false,
		hideInstanceActions: false,
		modifiedTestCases: []
	},
	TCMtestScriptQuestions: {
		testCaseSelectedIds: ['']
	},
	summary: {
		summary: [],
		hideSummary: true,
		collapsedSummaryPanel: false
	},
	userSettings: {
		showImportedInformation: true,
		showValidationToolBar: true
	}
};

const getters = {
	applicableTestCase: state => state.applicableTestCase,
	clauseReport: state => state.clauseReport,
	filter: state => state.filter,
	functionalRole: state => state.functionalRole,
	instance: state => state.instance,
	summary: state => state.summary,
	TCMtestScriptQuestions: state => state.TCMtestScriptQuestions,
	review: state => state.review,
	userSettings: state => state.userSettings
};

const actions = {
	setFileNumber: (state, payload) => {
		state.commit('setFileNumber', payload)
	},
	setShowImportedInformation: (state, payload) => {
		state.commit('setShowImportedInformation', payload)
	},
	setShowValidationToolBar: (state, payload) => {
		state.commit('setShowValidationToolBar', payload)
	},
	setFunctionalRoles: (state, payload) => {
		state.commit('setFunctionalRoles', payload);
	},
	setHiddenFunctionalRolePanel: (state, payload) => {
		state.commit('setHiddenFunctionalRolePanel', payload);
	},
	setHiddenFunctionalRoleSpinner: (state, payload) => {
		state.commit('setHiddenFunctionalRoleSpinner', payload);
	},
	setHiddenFilterPanel: (state, payload) => {
		state.commit('setHiddenFilterPanel', payload);
	},
	setHiddenJurisdictionAndDocumentPanel: (state, payload) => {
		state.commit('setHiddenJurisdictionAndDocumentPanel', payload)
	},
	setHiddenTestScriptCountPanel: (state, payload) => {
		state.commit('setHiddenTestScriptCountPanel', payload);
	},
	setFilterChecked: (state, payload) => {
		state.commit('setFilterChecked', payload);
	},
	setHiddenJurisdictionWarning: (state, payload) => {
		state.commit('setHiddenJurisdictionWarning', payload);
	},
	setJurisdictionIdsFilter: (state, payload) => {
		state.commit('setJurisdictionIdsFilter', payload);
	},
	setUpdateFilterCounts: (state, payload) => {
		state.commit('setUpdateFilterCounts', payload)
	},
	setCurrentSelectedJurisdictionIds: (state, payload) => {
		state.commit('setCurrentSelectedJurisdictionIds', payload)
	},
	setSelectedContributingUsers: (state, payload) => {
		state.commit('setSelectedContributingUsers', payload)
	},
	setAllContributingUsers: (state, payload) => {
		state.commit('setAllContributingUsers', payload)
	},
	setLoadedSelectedFilterJurisdictionIds: (state, payload) => {
		state.commit('setLoadedSelectedFilterJurisdictionIds', payload)
	},
	setNativeLanguageCompGroupID: (state, payload) => {
		state.commit('setNativeLanguageCompGroupID', payload);
	},
	setCollapsedFunctionalRolePanel: (state, payload) => {
		state.commit('setCollapsedFunctionalRolePanel', payload);
	},
	setCollapsedApplicableTestCasePanel: (state, payload) => {
		state.commit('setCollapsedApplicableTestCasePanel', payload);
	},
	setCollapsedSummaryPanel: (state, payload) => {
		state.commit('setCollapsedSummaryPanel', payload);
	},
	setDocumentCompGroupID: (state, payload) => {
		state.commit('setDocumentCompGroupID', payload);
	},
	setSelectedComponentGroupId: (state, payload) => {
		state.commit('setSelectedComponentGroupId', payload);
	},
	setSelectedComponentLockStatus: (state, payload) => { //selectedComponentLockStatus
		state.commit('setSelectedComponentLockStatus', payload)
	},
	setSelectedFilenumber: (state, payload) => {
		state.commit('setSelectedFilenumber', payload)
	},
	setProjectName: (state, payload) => {
		state.commit('setProjectName', payload);
	},
	setHiddenApplicableTestCase: (state, payload) => {
		state.commit('setHiddenApplicableTestCase', payload);
	},
	setHasNevada: (state, payload) => {
		state.commit('setHasNevada', payload);
	},
	setHasPeru: (state, payload) => {
		state.commit('setHasPeru', payload);
	},
	setApplicableTestCases: (state, payload) => {
		state.commit('setApplicableTestCases', payload);
	},
	setClauseReport: (state, payload) => {
		state.commit('setClauseReport', payload);
	},
	setDocumentNameHeader: (state, payload) => {
		state.commit('setDocumentNameHeader', payload);
	},
	setHiddenClausePanel: (state, payload) => {
		state.commit('setHiddenClausePanel', payload);
	},
	setHiddenClauseReportSpinner: (state, payload) => {
		state.commit('setHiddenClauseReportSpinner', payload);
	},
	setSelectedClauseID: (state, payload) => {
		state.commit('setSelectedClauseID', payload);
	},
	setClauseReportCounts: (state, payload) => {
		state.commit('setClauseReportCounts', payload);
	},
	setReviewCounts: (state, payload) => {
		state.commit('setReviewCounts', payload);
	},
	setHiddenSummary: (state, payload) => {
		state.commit('setHiddenSummary', payload);
	},
	setSummary: (state, payload) => {
		state.commit('setSummary', payload);
	},
	setDocumentMultiSelectFilterDisable: (state, payload) => {
		state.commit('setDocumentMultiSelectFilterDisable', payload);
	},
	setDocumentMultiSelectEmpty: (state, payload) => {
		state.commit('setDocumentMultiSelectEmpty', payload);
	},
	setNativeLanguageSelectEmpty: (state, payload) => {
		state.commit('setNativeLanguageSelectEmpty', payload);
	},
	setJurisdictionMultiSelectEmpty: (state, payload) => {
		state.commit('setJurisdictionMultiSelectEmpty', payload);
	},
	setJurisdictionMultiSelectValue: (state, payload) => {
		state.commit('setJurisdictionMultiSelectValue', payload);
	},
	setHiddenReviewPanel: (state, payload) => {
		state.commit('setHiddenReviewPanel', payload);
	},
	setReviewTestScript: (state, payload) => {
		state.commit('setReviewTestScript', payload);
	},
	setHiddenTestScriptSpinner: (state, payload) => {
		state.commit('setHiddenTestScriptSpinner', payload);
	},
	setHiddenQuestionsPanel: (state, payload) => {
		state.commit('setHiddenQuestionsPanel', payload);
	},
	setHiddenTestScriptPanel: (state, payload) => {
		state.commit('setHiddenTestScriptPanel', payload);
	},
	setReviewTestCase: (state, payload) => {
		state.commit('setReviewTestCase', payload);
	},
	setHiddenTestCaseSpinner: (state, payload) => {
		state.commit('setHiddenTestCaseSpinner', payload);
	},
	setHiddenTestCasePanel: (state, payload) => {
		state.commit('setHiddenTestCasePanel', payload);
	},
	setTestCaseAllDisplay: (state, payload) => {
		state.commit('setTestCaseAllDisplay', payload);
	},
	setShowConditionalBranchPopup(state, payload) {
		state.commit('setShowConditionalBranchPopup', payload);
	},
	setShowHelpModalPopup(state, payload) {
		state.commit('setShowHelpModalPopup', payload);
	},
	setShowInstanceInformationPopup(state, payload) {
		state.commit('setShowInstanceInformationPopup', payload);
	},
	setShowAnswerHistoryEmailPopup: (state, payload) => {
		state.commit('setAnswerHistoryEmailPopUp', payload)
	},
	setNewGPSLinkPopup: (state, payload) => {
		state.commit('setNewGPSLinkPopup', payload)
	},
	pushTCMTestScriptQuestions: (state, payload) => {
		state.commit('pushTCMTestScriptQuestions', payload)
	},
	removeTCMTestScriptQuestions: (state, payload) => {
		state.commit('removeTCMTestScriptQuestions', payload)
	},
	resetModifiedFlag: (state, payload) => {
		state.commit('resetModifiedFlag', payload);
	},
	setHideInstanceActions: (state, payload) => {
		state.commit('setHideInstanceActions', payload);
	},
	addModifiedTestCase: (state, payload) => {
		state.commit('addModifiedTestCase', payload);
	},
	removeModifiedTestCase: (state, payload) => {
		state.commit('removeModifiedTestCase', payload);
	},


}

const mutations = {
	setFileNumber: (state, payload) => {
		state.review.fileNumber = payload;
	},
	setSelectedComponentGroupId: (state, payload) => {
		state.instance.selectedComponentGroupId = payload;
	},
	setSelectedFilenumber: (state, payload) => {
		state.instance.selectedFilenumber = payload;
	},
	setSelectedComponentLockStatus: (state, payload) => {
		state.instance.selectedComponentLockStatus = payload;
	},
	setProjectName: (state, payload) => {
		state.instance.projectName = payload;
	},
	setShowImportedInformation: (state, payload) => {
		state.userSettings.showImportedInformation = payload;
	},
	setShowValidationToolBar: (state, payload) => {
		state.userSettings.showValidationToolBar = payload;
	},
	setFunctionalRoles: (state, payload) => {
		state.functionalRole.functionalRoles = payload;
	},
	setHiddenFunctionalRolePanel: (state, payload) => {
		state.functionalRole.hideFunctionalRolePanel = payload;
	},
	setHiddenFunctionalRoleSpinner: (state, payload) => {
		state.functionalRole.hideFunctionalRoleSpinner = payload;
	},
	setCollapsedFunctionalRolePanel: (state, payload) => {
		state.functionalRole.collapsedFunctionalRolePanel = payload;
	},
	setCollapsedApplicableTestCasePanel: (state, payload) => {
		state.applicableTestCase.collapsedApplicableTestCasePanel = payload;
	},
	setCollapsedSummaryPanel: (state, payload) => {
		state.summary.collapsedSummaryPanel = payload;
	},
	setHiddenFilterPanel: (state, payload) => {
		state.filter.hideFilterPanel = payload;
	},
	setHiddenJurisdictionAndDocumentPanel: (state, payload) => {
		state.filter.hideJurisdictionAndDocumentPanel = payload;
	},
	setHiddenTestScriptCountPanel: (state, payload) => {
		state.review.testScript.hideTestScriptCountPanel = payload;
	},
	setFilterChecked: (state, payload) => {
		state.filter.filterChecked = payload;
	},
	setHiddenJurisdictionWarning: (state, payload) => {
		state.filter.hideJurisdictionWarning = payload;
	},
	setJurisdictionIdsFilter: (state, payload) => {
		state.filter.jurisdictionFilter.jurisdictionIdsFilter = payload;
	},
	setUpdateFilterCounts: (state, payload) => {
		state.filter.updateFilterCounts = payload;
	},
	setCurrentSelectedJurisdictionIds: (state, payload) => {
		state.filter.jurisdictionFilter.currentSelectedJurisdictionIds = payload;
	},
	setSelectedContributingUsers: (state, payload) => {
		state.filter.selectedContributingUsers = payload;
	},
	setAllContributingUsers: (state, payload) => {
		state.filter.allContributingUsers = payload;
	},
	setLoadedSelectedFilterJurisdictionIds: (state, payload) => {
		state.filter.jurisdictionFilter.loadedSelectedFilterJurisdictionIds = payload;
	},
	setNativeLanguageCompGroupID: (state, payload) => {
		state.filter.nativeLanguageFilter.compGroupID = payload;
	},
	setDocumentCompGroupID: (state, payload) => {
		state.filter.documentFilter.compGroupID = payload;
	},
	setHiddenApplicableTestCase: (state, payload) => {
		state.applicableTestCase.hideApplicableTestCase = payload;
	},
	setHasNevada: (state, payload) => {
		state.applicableTestCase.hasNevada = payload;
	},
	setHasPeru: (state, payload) => {
		state.applicableTestCase.hasPeru = payload;
	},
	setApplicableTestCases: (state, payload) => {
		state.applicableTestCase.applicableTestCases = payload;
	},
	setClauseReport: (state, payload) => {
		state.clauseReport.clauseReport = payload;
	},
	setDocumentNameHeader: (state, payload) => {
		state.clauseReport.documentNameHeader = payload;
	},
	setHiddenClausePanel: (state, payload) => {
		state.clauseReport.hideClausePanel = payload;
	},
	setHiddenClauseReportSpinner: (state, payload) => {
		state.clauseReport.hideClauseReportSpinner = payload;
	},
	setSelectedClauseID: (state, payload) => {
		state.clauseReport.selectedClauseID = payload;
	},
	setClauseReportCounts: (state, payload) => {
		state.clauseReport.counts = payload;
	},
	setReviewCounts: (state, payload) => {
		state.review.testScript.counts = payload;
	},
	setHiddenSummary: (state, payload) => {
		state.summary.hideSummary = payload;
	},
	setSummary: (state, payload) => {
		state.summary.summary = payload;
	},
	setDocumentMultiSelectFilterDisable: (state, payload) => {
		state.filter.documentFilter.documentMultiSelectFilter.disable = payload;
	},
	setDocumentMultiSelectEmpty: (state, payload) => {
		state.filter.documentFilter.documentMultiSelectFilter.empty = payload;
	},
	setNativeLanguageSelectEmpty: (state, payload) => {
		state.filter.nativeLanguageFilter.nativeLanguageSelectFilter.empty = payload;
	},
	setJurisdictionMultiSelectEmpty: (state, payload) => {
		state.filter.jurisdictionFilter.jurisdictionMultiSelectFilter.empty = payload;
	},
	setJurisdictionMultiSelectValue: (state, payload) => {
		state.filter.jurisdictionFilter.jurisdictionMultiSelectFilter.value = payload;
	},
	setHiddenReviewPanel: (state, payload) => {
		state.review.hideReviewPanel = payload;
	},
	setReviewTestScript: (state, payload) => {
		state.review.testScript.reviewViewModel = payload;
	},
	setHiddenTestScriptSpinner: (state, payload) => {
		state.review.testScript.hideTestScriptSpinner = payload;
	},
	setHiddenQuestionsPanel: (state, payload) => {
		state.review.testScript.hideQuestionsPanel = payload;
	},
	setReviewTestCase: (state, payload) => {
		state.review.testCase.testCase = payload;
	},
	setHiddenTestCaseSpinner: (state, payload) => {
		state.review.testCase.hideTestCaseSpinner = payload;
	},
	setHiddenTestScriptPanel: (state, payload) => {
		state.review.testScript.hideTestScriptPanel = payload;
	},
	setHiddenTestCasePanel: (state, payload) => {
		state.review.testCase.hideTestCasePanel = payload;
	},
	setTestCaseAllDisplay: (state, payload) => {
		state.review.testCase.testCaseAllDisplay = payload;
	},
	setShowConditionalBranchPopup(state, payload) {
		state.review.showConditionalBranchPopup = payload
	},
	setShowHelpModalPopup(state, payload) {
		state.instance.showHelpModalPopup = payload
	},
	setShowInstanceInformationPopup(state, payload) {
		state.instance.showInstanceInformationPopup = payload
	},
	setAnswerHistoryEmailPopUp: (state, payload) => {
		state.review.answerHistoryEmailPopUp = payload;
	},
	setNewGPSLinkPopup: (state, payload) => {
		state.review.showGPSLinkPopup = payload
	},
	pushTCMTestScriptQuestions: (state, payload) => {
		//state.TCMtestScriptQuestions.testCaseSelectedIds.push(payload); //If test cases become multiple selection
		state.TCMtestScriptQuestions.testCaseSelectedIds = payload;
	},
	removeTCMTestScriptQuestions: (state, payload) => {
		//state.TCMtestScriptQuestions.testCaseSelectedIds.splice(payload, 1);
		state.TCMtestScriptQuestions.testCaseSelectedIds = null;
	},
	resetModifiedFlag: (state, payload) => {
		payload.forEach(x => x.modified = false);
	},
	setHideInstanceActions: (state, payload) => {
		state.review.hideInstanceActions = payload;
	},
	addModifiedTestCase: (state, payload) => {
		if (state.review.modifiedTestCases.indexOf(payload) < 0) {
			state.review.modifiedTestCases.push(payload);
		}
	},
	removeModifiedTestCase: (state, payload) => {
		if (state.review.modifiedTestCases.some(payload)) {
			const index = state.review.modifiedTestCases.indexOf(payload);
			state.review.modifiedTestCases.splice(index, index >= 0 ? 1 : 0);
		}
	},
}

export default {
	namespaced: true,
	state,
	getters,
	actions,
	mutations
}
