﻿import Vue from 'vue';
import Vuex from 'vuex';
import * as actions from './actions';
import * as getters from './getters';

import acumatica from './modules/acumatica';
import clauseTemplateManagement from './modules/clauseTemplateManagement';
import evolution from './modules/evolution';
import generateLetter from './modules/letterGen/generateLetter';
import holiday from './modules/holiday';
import incoming from './modules/incoming';
import letter from './modules/letterGen/letter';
import letterGenSources from './modules/letterGen/letterGenSources';
import notesManager from './modules/noteManager/NoteManager';
import notesManagerMockMemoData from './modules/noteManager/MockMemoData';
import submissionImport from './modules/submissionImport';
import workloadManager from './modules/workloadmanager';

Vue.use(Vuex);

export default new Vuex.Store({
	actions,
	getters,
	modules: {
		acumatica,
		clauseTemplateManagement,
		evolution,
		generateLetter,
		holiday,
		incoming,
		letter,
		letterGenSources,
		notesManager,
		notesManagerMockMemoData,
		submissionImport,
		workloadManager
	},
	strict: false
});