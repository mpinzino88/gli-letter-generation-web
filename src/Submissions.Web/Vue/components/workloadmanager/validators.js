﻿import { withParams } from 'vuelidate/lib/validators/common'

const greaterThanOrEqualTo = (startDate) => withParams(
	{ type: 'greaterThanOrEqualTo' },
	function (value, parentVm) {
		const tempStartDate = parentVm[startDate];

		if (!value || !tempStartDate) {
			return true;
		}

		if (moment(value).isSameOrAfter(tempStartDate)) {
			return true;
		}

		return false;
	}
)

export { greaterThanOrEqualTo }