﻿import { withParams } from 'vuelidate/lib/validators/common';

const sameYearAs = () => withParams(
	{ type: 'sameYearAs' },
	function (date) {
		if (this.vm.year === moment(date).format('YYYY')) {
			return true;
		}

		return false;
	}
);

const uniqueHolidayName = (name) => withParams(
	{ type: 'uniqueHolidayName' },
	function (name) {
		var tempArray = this.vm.holidays;

		if (!name || !tempArray) {
			return true;
		}
		var count = 0;
		for (var i = 0; i < tempArray.length; i++) {
			if (name == tempArray[i].name) {
				count += 1;
				if (count > 1) {
					return false;
				}
			}
		}

		return true;
	}
);

export { sameYearAs, uniqueHolidayName };