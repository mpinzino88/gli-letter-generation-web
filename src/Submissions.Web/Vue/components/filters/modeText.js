export default function (value) {
	switch (value) {
		case 0: return "Activate";
		case 1: return "Add";
		case 2: return "Deactivate";
		case 3: return "Edit";
		case 4: return "Delete";
		default: return "";
	}
}