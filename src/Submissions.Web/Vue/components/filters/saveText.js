export default function (value) {
	switch (value) {
		case 1: return "Add";
		case 3: return "Save";
		default: return "";
	}
}