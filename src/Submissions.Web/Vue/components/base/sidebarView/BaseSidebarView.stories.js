import BaseSidebarView from "./BaseSidebarView.vue";

export default { 
	title: "Base Sidebar View",
	component: BaseSidebarView
};

export const example = args => ({
	props: {
		title: {
			default: args.title
		},
		noSidebarPadding: {
			default: args.noSidebarPadding
		}
	},
	template: `
	<base-sidebar-view
		:title="title"
		:noSidebarPadding="noSidebarPadding">
		<template v-slot:sidebar>
			<em>Sidebar Content Here</em>
		</template>
		<template v-slot:actions>
			<base-button href="#" inverse-style>Example Action Button</base-button>
			<base-button href="#" inverse-style is-primary>Example Action Button</base-button>
		</template>
		<em>Main Content Here</em>
	</base-sidebar-view>`
});

example.args = {
	title: "Sidebar View",
	noSidebarPadding: false
};