import BaseFieldLabels from "./BaseFieldLabels.vue";

export default { 
	title: "Base Field Labels",
	component: BaseFieldLabels
};

export const example = args => ({
	props: {
		title: {
			default: args.title
		},
		helpText: {
			default: args.helpText
		}
	},
	template: `
	<base-field-labels
		:title="title"
		:helpText="helpText">
		<em>Form field goes here</em>
	</base-field-labels>`
});

example.args = {
	title: "Base Field Labels",
	helpText: "A component providing a standard style for rendering the title and helpText for a form field."
};