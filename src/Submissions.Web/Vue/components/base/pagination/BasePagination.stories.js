import BasePagination from "./BasePagination.vue";

export default { 
	title: "Base Pagination",
	component: BasePagination
};

const standardProps = args => {
	return {
		itemCount: {
			default: args.itemCount
		},
		page: {
			default: args.page
		},
		pageLength: {
			default: args.pageLength
		}
	};
};

export const example = args => ({
	props: standardProps(args),
	template: `
		<base-pagination 
			:item-count="itemCount"
			:page="page"
			:page-length="pageLength"
		></base-pagination>
	`
});


example.args = {
	itemCount: 71,
	page: 2,
	pageLength: 10
};