import BaseButton from "./BaseButton.vue";

export default { 
	title: "Base Button",
	component: BaseButton
};

export const example = args => ({
	props: {
		text: {
			default: args.text
		},
		href: {
			default: args.href
		},
		inverseStyle: {
			default: args.inverseStyle
		},
		isPrimary: {
			default: args.isPrimary
		},
		isDestructive: {
			default: args.isDestructive
		},
		isDisabled: {
			default: args.isDisabled
		}
	},
	template: `<base-button 
		:href='href' 
		:disabled="isDisabled" 
		:is-primary="isPrimary" 
		:is-destructive="isDestructive"
		:inverse-style="inverseStyle">
			{{ text }}
		</base-button>`
});

example.args = {
	text: "Base Button",
	href: "#"
};