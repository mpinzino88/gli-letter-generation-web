import BaseRadioButton from "./BaseRadioButton.vue";

export default { 
	title: "Base Radio Button",
	component: BaseRadioButton
};

export const example = args => ({
	props: {
		label: {
			default: args.label
		},
		options: {
			default: args.options
		}
	},
	template: `
	<base-radio-button
		:options="options"
		:label="label" />`
});

example.args = {
	label: "Base Radio Button",
	options: [
		{
			label: "Option One",
			value: 1
		},
		{
			label: "Option Two",
			value: 2
		},
		{
			label: "Option Three",
			value: 3
		}
	]
}