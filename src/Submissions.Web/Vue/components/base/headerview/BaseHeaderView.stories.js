import BaseHeaderView from "./BaseHeaderView.vue";

export default { 
	title: "Base Header View",
	component: BaseHeaderView
};

export const example = args => ({
	props: {
		title: {
			default: args.title
		},
		fixedWidth: {
			default: args.fixedWidth
		}
	},
	template: `
	<base-header-view
		:title="title"
		:fixedWidth="fixedWidth">
		<template v-slot:actions>
			<base-button href="#" inverse-style>Example Button</base-button>
			<base-button href="#" inverse-style is-primary>Example Primary Button</base-button>
		</template>
		<em>Main Content Here</em>
	</base-header-view>`
});

example.args = {
	title: "Header View",
	fixedWidth: false
};