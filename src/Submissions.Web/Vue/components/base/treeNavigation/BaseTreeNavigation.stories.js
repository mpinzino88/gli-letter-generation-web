import BaseTreeNavigation from '../treeNavigation/BaseTreeNavigation.vue';

export default { 
	title: "Base Tree Navigation",
	component: BaseTreeNavigation
};

const defaultProps = args => {
	return {
		items: {
			default: args.items
		}
	};
};

export const example = args => ({
	props: {
		...defaultProps(args)
	},
	template: `
	<base-tree-navigation :items="items" />`
});

example.args = {
	items: [
		{
			id: 0,
			name: "Sample Thing",
			children: [
				{
					id: 1,
					name: "Sample Child"
				},
				{
					id: 2,
					name: "Sample Child 2",
					children: [
						{
							id: 4,
							name: "Sample Grandchild"
						}
					]
				},
				{
					id: 3,
					name: "Sample Child"
				}
			]
		}
	]
};

export const withTemplate = args => ({
	props: {
		...defaultProps(args),
		selectedId: {
			default: args.selectedId
		}
	},
	template: `
	<base-tree-navigation :items="items">
		<template v-slot:default="row">
			<a
				v-if="row.item.id !== selectedId">
				{{ row.item.name }}
			</a>
			<strong 
				v-if="row.item.id === selectedId">
				{{ row.item.name }}
			</strong>
		</template>
	</base-tree-navigation>`
});

withTemplate.args = {
	items: [
		{
			id: 0,
			name: "Sample Thing",
			children: [
				{
					id: 1,
					name: "Sample Child"
				},
				{
					id: 2,
					name: "Sample Child 2",
					children: [
						{
							id: 4,
							name: "Sample Grandchild"
						}
					]
				},
				{
					id: 3,
					name: "Sample Child"
				}
			]
		}
	],
	selectedId: 0
};