import BaseFieldWrapper from "./BaseFieldWrapper.vue";

export default { 
	title: "Base Field Wrapper",
	component: BaseFieldWrapper
};

export const example = args => ({
	props: {
		dirty: {
			default: args.dirty
		},
		error: {
			default: args.error
		}
	},
	template: `
	<base-field-wrapper
		:error="error"
		:dirty="dirty">
		<em>Form field goes here</em>
		<template v-slot:errors>
			<p>
				This field is required.
			</p>
		</template>
	</base-field-wrapper>`
});

example.args = {
	dirty: false,
	error: true
};

export const withoutError = args => ({
	props: {
		dirty: {
			default: args.dirty
		},
		error: {
			default: args.error
		}
	},
	template: `
	<base-field-wrapper
		:error="error"
		:dirty="dirty">
		<em>Form field goes here</em>
	</base-field-wrapper>`
});

withoutError.args = {
	dirty: false,
	error: false
};