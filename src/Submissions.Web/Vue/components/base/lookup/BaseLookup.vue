﻿<template>
	<vue-select v-bind="$attrs"
				:multiple="multiple"
				:disabled="disabled"
				:options="options"
				@search="triggerAjaxSearch"
				@search:focus="forceTriggerAjaxSearch"
				:filterable="false"
				label="text"
				:class="{
			'gli-base-lookup_dirty': dirty
		}"
				v-on="$listeners">
		<template v-if="ajaxLoading || nextQuery"
				  slot="spinner">
			<i class="gli-base-select_spinner fa fa-spinner fa-pulse"></i>
		</template>
		<template v-slot:option="option">
			<!-- @slot A custom Vue template for rendering each list item, both
			in the dropdown and in the search box. -->
			<slot name="item"
				  :option="option">
				<old-template v-if="itemTemplate"
							  :template-name="itemTemplate"
							  :data="option">
				</old-template>
				<span v-else>
					{{ option.text }}
				</span>
			</slot>
		</template>
		<template v-slot:selected-option="option">
			<slot name="item"
				  :option="option">
				<old-template v-if="itemTemplate"
							  :template-name="itemTemplate"
							  :data="option">
				</old-template>
				<span v-else>
					{{ option.text }}
				</span>
			</slot>
		</template>
		<template v-slot:no-options="{ search, searching }">
			<template v-if="ajaxLoading || nextQuery">
				<i class="gli-base-select_spinner fa fa-spinner fa-pulse"></i>
			</template>
			<template v-else-if="searching">
				{{ noResultsText }}
				<em>{{ search }}</em>.
			</template>
			<em v-else>{{ startTypingText }}</em>
		</template>
		<li slot="list-footer"
			v-if="optionCount > pageSize">
			<base-pagination :item-count="optionCount"
							 :page="page"
							 :page-length="pageSize"
							 @new-page-number="setPage" />
		</li>
	</vue-select>
</template>

<script>
	import Vue from "vue";
	import VueSelect from "vue-select";
	import debounce from "lodash/debounce";
	import cloneDeep from "lodash/clonedeep";
	import isEqual from "lodash/isequal";
	import OldTemplate from "./subcomponents/OldTemplate.vue";

	/**
	 * -----------
	 *
	 * #### 🚨 BETA 🚨
	 * The properties and behavior for this component are not finalized, and may change over time.
	 *
	 * -----------
	 *
	 * A dropdown list that takes its data from the LookupAjax API endpoint.
	 *
	 * The component is based on the [Vue Select](https://vue-select.org/) dropdown component.
	 * This has built-in support for most dropdown features we would want to use, including single
	 * & multiple choices, as well as pagination. It is intended to replace the older `Lookup` and
	 * `LookupMultiple` Vue components, and is no longer based on the jQuery [Select2](https://select2.org/)
	 * component. The C# LookupAjax endpoints themselves are unchanged.
	 *
	 * Some enhancements have been made in an attempt to make the lookup easier to use. By default,
	 * only the `lookupAjax` property, which tells it which data source to use, and something to grab
	 * the value (`v-model` or `:value`) should be required. See the property descriptions below and
	 * the [Vue Select documentation](https://vue-select.org/) for more details. All Vue Select props
	 * and events are passed through and made available, unless Base Lookup props described below
	 * override them.
	 *
	 * Backwards compatibility for GLI's Select2 templates have been made available as well. Annotations
	 * for Manufacturers and Jurisdictions are brought in automatically, no longer requiring `templateName`
	 * to be specified, and can be extended in the same manner as before.
	 *
	 * However, this lookup has a template slot (`item`) for rendering each item. Consider using this in
	 * place of the Select2 templates - this is cleaner and eliminates a security vulnerability from
	 * writing raw HTML received from the server.
	 *
	 * The `value` is the object (or an array of objects) corresponding to the user's selection(s).
	 * This can be [reduced down to the object(s)' ID property](https://vue-select.org/guide/values.html#returning-a-single-key-with-reduce),
	 * but keep in mind that you'll need the whole object if you want to render a pre-existing
	 * selection when reloading your component.
	 *
	 * -----------
	 *
	 * #### Known Limitations
	 *
	 * * When viewing this in Storybook, keep in mind that API calls are not possible from there due
	 * to CORS restrictions. A mock dataset for manufacturers is provided, and changes to `lookupAjax`
	 * do not affect the content as they would in GLI.Submissions.Web.
	 * * While infinite scrolling was present in the Select2-based lookups, and infinite scrolling
	 * [can work with Vue Select](https://vue-select.org/guide/infinite-scroll.html), given the
	 * complexity of infinite scrolling's implementation, this uses the simpler pagination method
	 * for handling lengthy results. This may change in the future, but should not represent a
	 * significant user experience regression in the vast majority of use cases, and should not be
	 * a breaking change if and when it is implemented.
	 * * The border of the dropdown should be the Edit color when changed from its original value,
	 * but this has not been implemented yet.
	 * * After selecting an item and the search box is cleared, it runs another query for the contents,
	 * showing the spinner. This should be hidden, if not removed, in a future release.
	 *
	 * -----------
	 *
	 * | Date       | Change Made            		|
	 * | ---------- | ---------------------- 		|
	 * | 2020-11-02 | Switched to base pagination 	|
	 * | 2020-10-22 | Initial Release - Beta 		|
	 *
	 */
	export default Vue.extend({
		components: {
			VueSelect,
			OldTemplate
		},
		props: {
			/**
			 * Set to true to disable the input. Changes will not be permitted when this is set.
			 */
			disabled: {
				type: Boolean,
				default: false
			},
			/**
			 * Sets the content of the `filter` parameter for the lookupAjax endpoint. Expects
			 * a javascript object which will be stringified to JSON before being sent.
			 */
			filter: {
				type: Object,
				required: false,
				default: null
			},
			/**
			 * Specifies the specific lookupAjax endpoint to retrieve values from.
			 */
			lookupAjax: {
				type: String,
				required: true
			},
			/**
			 * Set to true to allow users to select multiple entries in the list.
			 */
			multiple: {
				type: Boolean,
				default: false
			},
			/**
			 * (Most of) the text to display to the user when there are no results for the
			 * text they entered. A space, the query and a period will be appended to the
			 * end of this string when set.
			 */
			noResultsText: {
				type: String,
				required: false,
				default: "No results found for"
			},
			/**
			 * How many results to query the lookupAjax endpoint for. Also limits the amount
			 * of options in the dropdown list that can be viewed without the use of pagination.
			 */
			pageSize: {
				type: Number,
				required: false,
				default: 50
			},
			/**
			 * The text to display to the user when there are no results *and* the query is empty.
			 * The lookup executes a lookupAjax request when focused, so this should only be present
			 * when that first query returns no results.
			 */
			startTypingText: {
				type: String,
				required: false,
				default: "Start typing to find an item"
			},
			/**
			 * Indicates the window function to use (specified in gli.js) to use for rendering the
			 * content of each list item. Unused in most cases, but when `lookupAjax` is set to
			 * "Manufacturers" or "Jurisdictions", their respective functions are used by default
			 * to provide annotations.
			 *
			 * If you want to change the content of each list item, consider passing a Vue component
			 * into the `item` template slot instead. This is cleaner and eliminates a security
			 * vulnerability from writing raw HTML received from the server.
			 */
			templateName: {
				type: String,
				required: false,
				default: null
			},
			/**
			 * Use the prop if you would like to explicitly set the type of search to perform.
			 * i.e. Direct equivilance, starts with, contains etc..
			 * This param alone will not really do anything unless the server side method is configured to handle it.
			 * currenlty this is prob one of the following strings:
			 * startsWith
			 * contains
			 * equals
			 * but there could be more, once again this depends on the servers consumption.
			 **/
			searchType: {
				type: String,
				required: false,
				default: null
			}
		},
		data: function () {
			// If the endpoint routinely responds in under 750ms, it should
			// be added to this list to increase the responsiveness
			const fastEndpoints = [
				"jurisdictions", "manufacturers"
			];
			const key = this.lookupAjax.toLowerCase();
			const debouncePeriod = fastEndpoints.includes(key) ? 50 : 350;
			return {
				ajaxLoading: false,
				ajaxSearchDebounced: debounce(this.ajaxSearch, debouncePeriod),
				nextQuery: null,
				currentQuery: null,
				lastQuery: null,
				options: [],
				optionCount: 0,
				originalValue: cloneDeep(this.$attrs.value),
				page: 1
			};
		},
		computed: {
			dirty: function () {
				return !isEqual(this.$attrs.value, this.originalValue);
			},
			itemTemplate: function () {
				if (this.templateName !== null) return this.templateName;
				const defaultTemplates = {
					"manufacturers": "formatManufacturers",
					"jurisdictions": "formatJurisdictions"
				};
				const key = this.lookupAjax.toLowerCase();
				if (key in defaultTemplates) {
					return defaultTemplates[key];
				}
				return null;
			},
			lookupUrl: function () {
				return "LookupsAjax/" + this.lookupAjax;
			}
		},
		watch: {
		},

		methods: {

			// This base lookup handles multiple requests with queueing - once each response is
			// received, another request is triggered for the last update received. If four letters
			// are typed before the first request is returned, two requests are made in total.

			// This base lookup handles multiple requests with queueing - once each response is
			// received, another request is triggered for the last update received. If four letters
			// are typed before the first request is returned, two requests are made in total.

			// NOTE: Axios now has cancel token support, which promises to eliminate the need for
			// either queuing or debouncing. Worth looking into for potential speed improvements.
			// https://github.com/axios/axios#cancellation

			ajaxSearch: function () {
				if (this.currentQuery == null) {
					this.ajaxLoading = true;
					this.currentQuery = this.nextQuery;
					this.nextQuery = null;
					this.options = [];
					const params = {
						pageSize: this.pageSize,
						pageNum: this.page,
						searchTerm: this.currentQuery,
						searchType: this.searchType
					};

					if (this.filter) {
						params.filter = JSON.stringify(this.filter);
					}
					this.$http.get(this.lookupUrl, { params })
						.then(response => {
							// If another query queued, toss the result
							if (this.ajaxQueue) {
								return;
							}
							if (response.status === 200) {
								const results = response.data.Results
									.filter(r => r !== null && r.id !== null);
								if (this.nextQuery === null) {
									this.options = results;
								}
								this.optionCount = response.data.Total || results.length;
							} else {
								console.error(response);
							}
						})
						.catch(e => {
							console.error(e);
						})
						.finally(() => {
							this.ajaxLoading = false;
							this.lastQuery = this.currentQuery;
							this.currentQuery = null;
							if (this.nextQuery !== null) {
								this.ajaxSearch();
							}
						});
				}
			},
			forceTriggerAjaxSearch: function (query) {
				const queryStr = query == null ? "" : query;
				this.options = [];
				this.optionCount = 0;
				this.page = 1;
				this.nextQuery = queryStr;
				this.ajaxSearch();
			},
			setPage: function (newPage) {
				this.page = newPage;
				this.nextQuery = this.lastQuery;
				this.ajaxSearch();
			},
			triggerAjaxSearch: function (query) {
				const queryStr = query == null ? "" : query;
				this.options = [];
				this.optionCount = 0;
				this.page = 1;
				this.nextQuery = queryStr;
				this.ajaxSearchDebounced();
			}
		}
	});
</script>

<style lang="scss" scoped>

	@use "@/components/theme/Colors";
	@use "@/components/theme/Spacing";

	::v-deep {
		$vs-border-width: 2px;
		$vs-border-radius: 4px;
		$vs-border-color: Colors.$accentLight;
		$vs-state-disabled-bg: Colors.$accentLight;
		$vs-selected-bg: Colors.$accentLight;
		$vs-controls-color: Colors.$primary;
		$vs-state-active-bg: Colors.$primaryLight;
		$vs-controls-size: 0.7;

		@import "vue-select/src/scss/vue-select.scss";
		.vs__dropdown-menu

	{
		position: relative;
	}

	.vs__dropdown-toggle {
		padding: Spacing.$half Spacing.$quarter calc(#{Spacing.$half} + 4px);
		transition: border 0.1s;
		.gli-base-lookup_dirty &

	{
		border-color: Colors.$edit;
	}

	// Not sure why this wasn't already happening
	.vs--disabled & {
		background: $vs-state-disabled-bg;
		color: Colors.$accentDark;
	}

	.vs--open & {
		border-color: Colors.$primaryLight;
	}

	}

	.vs__open-indicator {
		fill: $vs-controls-color;
		.vs--disabled &

	{
		fill: Colors.$accent;
	}

	}

	.gli-base-select_spinner {
		margin-left: Spacing.$half;
	}
	}
</style>