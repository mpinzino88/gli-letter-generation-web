import BaseLookup from "./BaseLookup.vue";

export default { 
	title: "Base Lookup",
	component: BaseLookup
};

const standardProps = args => {
	return {
		lookupAjax: {
			default: args.lookupAjax
		},
		disabled: {
			default: args.disabled
		},
		filter: {
			default: args.filter
		},
		multiple: {
			default: args.multiple
		},		
		noResultsText: {
			default: args.noResultsText
		},
		pageSize: {
			default: args.pageSize
		},
		startTypingText: {
			default: args.startTypingText
		},
		templateName: {
			default: args.templateName
		}
	};
};

export const example = args => ({
	props: standardProps(args),
	template: `
		<base-lookup 
			:lookup-ajax="lookupAjax"
			:disabled="disabled"
			:filter="filter"
			:multiple="multiple"
			:no-results-text="noResultsText"
			:page-size="pageSize"
			:start-typing-text="startTypingText"
			:template-name="templateName"
		></base-lookup>
	`
});

export const withTemplate = args => ({
	props: standardProps(args),
	template: `
		<base-lookup 
			:lookup-ajax="lookupAjax"
			:disabled="disabled"
			:filter="filter"
			:multiple="multiple"
			:no-results-text="noResultsText"
			:page-size="pageSize"
			:start-typing-text="startTypingText"
			:template-name="templateName"
		>
			<template v-slot:item="{ option }">
				<span><strong>Custom Template</strong> &bull; {{ option.text }}</span>
			</template>
		</base-lookup>
	`
});

example.args = {
	lookupAjax: "Manufacturers"
};

withTemplate.args = {
	lookupAjax: "Manufacturers"
};