import BaseCheckbox from "./BaseCheckbox.vue";

export default { 
	title: "Base Checkbox",
	component: BaseCheckbox
};

export const example = args => ({
	props: {
		isDisabled: {
			default: args.disabled
		},
		label: {
			default: args.label
		},
		value: {
			default: args.value
		}
	},
	template: `
	<base-checkbox
		:disabled="isDisabled"
		:label="label"
		:value="value" />`
});

example.args = {
	label: "Base Checkbox",
	value: false
};