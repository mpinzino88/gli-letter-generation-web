import BaseFieldset from "./BaseFieldset.vue";

export default { 
	title: "Base Fieldset",
	component: BaseFieldset
};

export const example = args => ({
	props: {
		isCollapsible: {
			default: args.isCollapsible
		}
	},
	template: `
	<base-fieldset :is-collapsible="isCollapsible">
		<legend>Choose your favorite monster</legend>

		<input type="radio" id="kraken" name="monster">
		<label for="kraken">Kraken</label><br/>

		<input type="radio" id="sasquatch" name="monster">
		<label for="sasquatch">Sasquatch</label><br/>

		<input type="radio" id="mothman" name="monster">
		<label for="mothman">Mothman</label>
	</base-fieldset>`
});

example.args = {
	isCollapsible: true
};