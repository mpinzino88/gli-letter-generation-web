import BaseInput from "./BaseInput.vue";

export default { 
	title: "Base Input",
	component: BaseInput
};

const template = `<base-input
	:disabled="isDisabled"
	:value="value"
	:textArea="textArea">
	<template v-slot:errors>
		<p>
			This field is required.
		</p>
	</template>
	</base-input>`;

const args = {
	disabled: false,
	textArea: false,
	value: null
};

const propSet = args => {
	return {
		isDisabled: {
			default: args.disabled
		},
		value: {
			default: args.value
		},
		textArea: {
			default: args.textArea
		}
	};
};

export const example = args => ({
	props: {
		...propSet(args)
	},
	template: template
});

example.args = args;

