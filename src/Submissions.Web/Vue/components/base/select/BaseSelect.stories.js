import BaseSelect from "./BaseSelect.vue";

export default { 
	title: "Base Select",
	component: BaseSelect
};

export const example = args => ({
	props: {
		disabled: {
			default: args.disabled
		},
		label: {
			default: args.label
		},
		options: {
			default: args.options
		},
		value: {
			default: args.value
		}
	},
	template: `
	<base-select
		:disabled="disabled"
		:options="options"
		label="label"
		track-by="value"
		:value="value" />`
});

example.args = {
	disabled: false,
	label: "Base Select",
	value: null,
	options: [
		{
			label: "Option One",
			value: 1
		},
		{
			label: "Option Two",
			value: 2
		},
		{
			label: "Option Three",
			value: 3
		}
	]
};