import BaseModal from "./BaseModal.vue";

export default { 
	title: "Base Modal",
	component: BaseModal
};

export const example = args => ({
	props: {
		header: {
			default: args.header
		},
		body: {
			default: args.body
		}
	},
	template: `
	<base-modal>
		<template v-slot:header>
			{{ header }}
		</template>
		{{ body }}
	</base-modal>`
});

example.args = {
	header: "Base Modal",
	body: "Base Modal Content"
};