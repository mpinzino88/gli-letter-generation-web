﻿import { createNamespacedHelpers } from 'vuex';
const { mapGetters: mapMockMemoDataGetters } = createNamespacedHelpers('notesManagerMockMemoData');

import { isNull } from '@/common';
import { isNotNull } from '@/common';
import { JURISDICTION } from '@/common'

export function buildMemoComponentPreview(memo, sourceParameter,isNativeLanguage) {
	return {
		name: "memoComponentPreview",
		template: buildMemoPreviewTemplate(isNativeLanguage ? memo.content : memo.secondaryLanguageContent || ''),
		data: function () {
			return {
				memo: memo,
				source: sourceParameter,
			};
		},
		computed: {
			...mapMockMemoDataGetters([
				'letterPreviewJurisdiction',
				'jurisdictionLetterDetails',
				'jurisdictionsLetterDetails',
				'components',
				'paytables',
				'manufacturerName',
				'manufacturerId',
				'letterContent',
				'fileNumber',
				'bundleName',
				'certificationIssuerForCertificationLab',
				'certificationIssuerByName',
				'certificationIssuerByLastName',
				'footer',
				'revocationInformation',
				'disclosureWording',
				'jira',
				'gamingGuidelines',
				'specializedJurisdictionClauseReports',
				'modificationList',
				'modificationListForJurisdiction'
			]),

			...buildComputedForMemo(memo, sourceParameter),
			fileNumberJurSuffix: function () {
				return this.fileNumber + "-" + this.letterPreviewJurisdiction;
			},
			sourceItems: function () {
				if (isNull(this.source)) {
					return [];
				}
				else {
					return this.source;
				}
			},
			allJurisdictionDetails: function () {
				return this.jurisdictionsLetterDetails;
			},
			juriLetterDetails: function () {
				return this.jurisdictionLetterDetails(this.letterPreviewJurisdiction);
			},
			date: function () {
				return new Date().toLocaleDateString();
			},
			currentPage: function () {
				return this.footer.currentPage;
			},
			totalPages: function () {
				return this.footer.totalPages;
			}
		},
		methods: {
			uniqueSourceItems: function (arr, key) {
				var output = [];
				for (var i = 0; i < arr.length; i++) {
					if (!output.includes(arr[i][key])) {
						output.push(arr[i][key]);
					}
				}
				return output;
			},
			groupBy: function (xs, key) {
				return xs.reduce(function (rv, x) {
					(rv[x[key]] = rv[x[key]] || []).push(x);
					return rv;
				}, {});
			},
			buildSourceItem: function (sourceItem) {
				let targetSrcItem = this.memo.sourceItemTargets.find(sit => sit.id === getSourceItemId(this.memo.sourceId, sourceItem));
				return createSplitSource(sourceItem, targetSrcItem, this.letterPreviewJurisdiction);
			},
			businessLogicUpdated: function (newVal, oldVal) {
				this.evaluateBusinessLogic();
			},
			isNull: function (obj) {
				return isNull(obj);
			},
			isNotNull: function (obj) {
				return isNotNull(obj);
			},
			checkRTPRange: function (UseWeighted) {
				let paytablesRTPNotInRange = [];
				for (let i = 0; i < this.paytables.length; i++) {
					let gamingGuideline = this.gamingGuidelines.find(x => x.jurisdictionId === this.letterPreviewJurisdiction);
					let truncatedMin = Math.trunc(this.paytables[i].minReturn * Math.pow(10, 2)) / Math.pow(10, 2);
					let truncatedMax = Math.trunc(this.paytables[i].maxReturn * Math.pow(10, 2)) / Math.pow(10, 2);
					if (gamingGuideline) {
						let rtpRequirement = gamingGuideline.rtpRequirements.find(x => x.type.toLowerCase() === this.paytables[i].type.toLowerCase());
						if (!rtpRequirement) {
							rtpRequirement = gamingGuideline.rtpRequirements.find(x => x.type.toLowerCase() === "any");
						}
						if (rtpRequirement) {
							if (UseWeighted) {
								let jurSpecificAverage = this.letterPreviewJurisdiction === parseInt(JURISDICTION.DELAWARE_LOTTERY.id) ? this.paytables[i].deAverage : this.paytables[i].mdAverage;
								let truncatedMinAvg = Math.trunc(jurSpecificAverage * Math.pow(10, 2)) / Math.pow(10, 2);
								let truncatedMaxAvg = Math.trunc(jurSpecificAverage * Math.pow(10, 2)) / Math.pow(10, 2);
								if (!(truncatedMinAvg >= rtpRequirement.min / 100 && truncatedMaxAvg <= rtpRequirement.max / 100)) {
									paytablesRTPNotInRange.push({ paytable: this.paytables[i], rtpRequirement: rtpRequirement });
								}
							} else {
								if (truncatedMin != null) {
									if (truncatedMin < (rtpRequirement.min) / 100) {
										paytablesRTPNotInRange.push({ paytable: this.paytables[i], rtpRequirement: rtpRequirement });
										continue;
									}
								}
								if (truncatedMax != null) {
									if (truncatedMax > (rtpRequirement.max) / 100) {
										paytablesRTPNotInRange.push({ paytable: this.paytables[i], rtpRequirement: rtpRequirement });
									}
								}
							}
						}
					}
				}
				return paytablesRTPNotInRange;
			},
			paytableRowStyle: function (item, selectedOptions) {
				return [
					((
						(
							this.differenceWrapper().length > 0 ?
								_.minBy(
									this.differenceWrapper(),
									function (x) { return x.minReturn; }
								).paytableId === item.paytableId : false
						) ||
						(
							this.differenceWrapper().length > 0 ?
								_.maxBy(
									this.differenceWrapper(),
									function (x) {
										return x.maxReturn;
									}
								).paytableId === item.paytableId : false
						)
					) && (this.letterPreviewJurisdiction === 8)) || selectedOptions.indexOf(item.paytableId) !== -1
						? { border: '3px solid black' } : {}
				];
			},
		}
	};
}


function buildComputedForMemo(memo, source) {
	if (isNotNull(memo) &&
		isNotNull(memo.questions) &&
		memo.questions.length > 0) {
		let ret = contentToComputed(memo.questions[0], memo.questions, source);
		if (memo.questions.length > 1) {
			for (let iter = 1; iter < memo.questions.length; iter++) {
				ret = { ...ret, ...contentToComputed(memo.questions[iter], memo.questions, source) };
			}
		}
		return ret;
	}
	else {
		return {
			['blank']: {
				get: function () {
					return 'blank';
				},
				set: function (sel) {
				}
			}
		};
	}
}

function buildMemoPreviewTemplate(content) {
	let ret = "<div ref=\"MemoPreviewRef\">";

	ret += content;

	ret += "</div>";
	return ret;
}

function tableColumnToComputed(control) {
	return {
		[control.tokenKey]: control.tokenValue
	};
}
function tableRowToComputed(controls) {
	let ret = tableColumnToComputed(controls[0]);
	if (controls.length > 1) {
		for (let iter = 1; iter < controls.length; iter++) {
			ret = { ...ret, ...tableColumnToComputed(controls[iter]) };
		}
	}
	return ret;
};
/// uses JavaScript spread operator to convert the label/content pair of the given control to a new object, where the property name is control.label and the value returned is control.content
///This method is how we write e.g. {{ productType }} in the note content and have it return the value in the control with the label 'productType'
function contentToComputed(control, controls, source) {
	if (control.type !== "TABLE") {
		return {
			[control.tokenKey]: {
				get: function () {
					return control.tokenValue;
				},
				set: function (sel) {
					control.tokenValue = sel;
				}
			}
		};
	}
	else if (control.tableRows.length > 0) {
		return {
			[control.tokenKey]: {
				get: function () {
					let ret = tableRowToComputed(control.tableRows[0]);
					let arrayRet = new Array(ret);
					if (control.tableRows.length > 1) {
						for (let iter = 1; iter < control.tableRows.length; iter++) {
							arrayRet.push(tableRowToComputed(control.tableRows[iter]));
						}
					}
					return arrayRet;
				},
				set: function (sel) {

				}
			}
		};
	}
	else {
		return {
			[control.tokenKey]: {
				get: function () {
					return [];
				},
				set: function (sel) {

				}
			}
		};
	}
}
