import { createNamespacedHelpers } from 'vuex';
const { mapGetters, mapActions } = createNamespacedHelpers('generateLetter');
const { mapGetters: sourceGetters, mapActions: sourceActions } = createNamespacedHelpers('letterGenSources');
const { mapGetters: letterGetters, mapActions: letterActions } = createNamespacedHelpers('letter');

import { isNull } from '@/common';
import { isNotNull } from '@/common';

export function buildMemoComponent(memo, sourceParameter, pageNumber, currentPageCounter, isSecondaryLanguageLetter) {
	return {
		name: "lettercontent",
		filters: {
			spacing: function (value) {
				if (!value) return '';
				return value.indexOf(' ') >= 0 ? value : value.split('').join(' ');
			}
		},
		template: buildMemoTemplate(memo.letterContent, isSecondaryLanguageLetter, memo.secondaryLanguageLetterContent),
		data: function () {
			return {
				memo: memo,
				source: sourceParameter,
				currentPageCounter: currentPageCounter,
				illinoisPaytables: []
			};
		},
		components: {

		},
		computed: {
			currentPage: function () {
				return this.currentPageCounter + this.preDefinedPages;
			},
			fileNumberJurSuffix: function () {
				return this.fileNumber + "-" + this.letterPreviewJurisdiction;
			},
			detectContentChanged: function () {
				return this.memo.contentChanged;
			},
			//This is how a memo can target a subset of sources, such as the first 5 paytables, etc.
			//This is also how sourceItems can be split across memos without affecting the sourceItem directly - the objects returned from buildSourceItem
			//are clones of the sourceItem
			sourceItems: function () {
				if (isNull(this.source)) {
					return [];
				}
				else if (isNull(this.memo.sourceItemTargets) || this.memo.sourceItemTargets.length === 0) {
					return this.source;
				}
				//check to see if any of the source items are split. If none are split, just return those source items
				else {
					let actualSource = this.source;
					if (this.memo.sourceId === "SpecializedJurisdictionClauseReports") {
						if (this.memo.sourceProperty === "TestCases") {
							actualSource = actualSource.testCases;
						}
						else if (this.memo.sourceProperty === "EvaluatedClauses") {
							actualSource = actualSource.evaluatedClauses;
						}
					}
					return actualSource
						.filter(src => this.memo.sourceItemTargets.some(sit => sit.id === getSourceItemId(this.memo.sourceId, src)))
						.map(src => this.buildSourceItem(src));
				}
			},
			allJurisdictionDetails: function () {
				return this.jurisdictionsLetterDetails;
			},
			juriLetterDetails: function () {
				return this.jurisdictionLetterDetails(this.letterPreviewJurisdiction);
			},
			///This converts all the input control objects to the correct getters
			...buildComputedForMemo(memo, sourceParameter, isSecondaryLanguageLetter),
			...mapGetters([
				'letterPreviewJurisdiction',
			]),
			...sourceGetters([
				'jurisdictionLetterDetails',
				'jurisdictionsLetterDetails',
				'components',
				'paytables',
				'revocationInformation',
				'disclosureWording',
				'gamingGuidelines',
				'specializedJurisdictionClauseReports',
				'getcheckRTPRange',
				'OCUITComponents',
				'modifications'
			]),
			...letterGetters([
				'manufacturerId',
				'manufacturerName',
				'letterContent',
				'memosForJurisdiction',
				'fileNumber',
				'bundleName',
				'certificationIssuerForCertificationLab',
				'certificationIssuerByName',
				'projectEvalLab',
				'modificationList',
				'modificationListForJurisdiction',
				'displayedSectionsForJurisdictions',
				'totalLetterPages',
				'preDefinedPages'
			]),
			totalPages: function () {
				return this.totalLetterPages(this.letterPreviewJurisdiction);
			},
			date: function () {
				return new Date().toLocaleDateString();
			},
			overrides: function () {
				let minControl = memo.letterContentControls.find(x => x.label === "token_min_rtp_override" && (x.applicableJurisdictions.indexOf(this.letterPreviewJurisdiction) > -1 || x.applicableJurisdictions.length == 0));
				let minOverride = minControl !== undefined ? parseFloat(minControl.content) : 0;
				let maxControl = memo.letterContentControls.find(x => x.label === "token_max_rtp_override" && (x.applicableJurisdictions.indexOf(this.letterPreviewJurisdiction) > -1 || x.applicableJurisdictions.length == 0));
				let maxOverride = maxControl !== undefined ? parseFloat(maxControl.content) : 0;

				let result = {
					minOverride: minOverride,
					maxOverride: maxOverride
				};

				return result;
			}
		},
		watch: {
			detectContentChanged: function (newVal, oldVal) {
				this.setHeight();
			},
			overrides: function (newVal, oldVal) {
				if (this.letterPreviewJurisdiction === 8) {
					let a = this.differenceWrapper();
					let b = [
						_.minBy(a, function (x) { return x.minReturn; }),
						_.maxBy(a, function (x) { return x.maxReturn; })
					];
					this.illinoisPaytables = b;
				}
			}
		},
		created: function () {
			this.addWatchers();
			if (this.letterPreviewJurisdiction === 8) {
				let a = this.differenceWrapper();
				let b = [
					_.minBy(a, function (x) { return x.minReturn; }),
					_.maxBy(a, function (x) { return x.maxReturn; })
				];
				this.illinoisPaytables = b;
			}
		},
		mounted: function () {
			this.setHeight();
		},
		updated: function () {
			if (isNotNull(this.memo) && isNotNull(this.memo.letterContentControls) &&
				this.memo.letterContentControls.some(control => isNotNull(control) && control.type === 'HTML')) {
				this.setHeight();
			}
		},
		methods: {
			uniqueSourceItems: function (arr, key) {
				var output = [];
				for (var i = 0; i < arr.length; i++) {
					if (!output.includes(arr[i][key])) {
						output.push(arr[i][key]);
					}
				}
				return output;
			},
			groupBy: function (xs, key) {
				return xs.reduce(function (rv, x) {
					(rv[x[key]] = rv[x[key]] || []).push(x);
					return rv;
				}, {});
			},
			buildSourceItem: function (sourceItem) {
				let targetSrcItem = this.memo.sourceItemTargets.find(sit => sit.id === getSourceItemId(this.memo.sourceId, sourceItem));
				return createSplitSource(sourceItem, targetSrcItem, this.letterPreviewJurisdiction);
			},
			addWatchers: function () {
				if (isNotNull(this.memo.letterContentControls)) {
					this.memo.letterContentControls.forEach((control) => {
						if (control.isBusinessLogic) {
							this.$watch(() => control.content, this.businessLogicUpdated, { deep: false });
						}
					});

					this.evaluateBusinessLogic();
				}
			},
			businessLogicUpdated: function (newVal, oldVal) {
				this.evaluateBusinessLogic();
			},
			evaluateBusinessLogic: function () {
				//console.log('evaluateBusinessLogic');
				if (isNotNull(this.memo.businessLogicExpression) && this.memo.businessLogicExpression.length > 0) {
					try {
						this.memo.include = eval(this.memo.businessLogicExpression);
					} catch (err) {
						console.groupCollapsed('We should probably tell the user this businessLogicExpression failed.');
						console.log(err);
						console.log(this.memo);
						console.groupEnd();
					}
				}
			},
			setHeight: function () {
				let memoRef = this.$refs.MemoRef;
				if (isNotNull(memoRef)) {
					let size = memoRef.offsetHeight;
					//console.log('set height memocomponent ' + size);
					let page = getPaging(this.memo, this.memo.localId, this.letterPreviewJurisdiction);
					if (isNull(page)) {
						//console.log('why is paging null?');
						//console.log(this.memo);
					}
					else {
						page.height = size;
					}
				}
			},
			isNull: function (obj) {

				return isNull(obj);
			},
			isNotNull: function (obj) {
				return isNotNull(obj);
			},
			formatAsPercent: function (value, numDigits) {
				return Number(value).toLocaleString(undefined, { style: 'percent', minimumFractionDigits: numDigits });

			},
			internalPageId: function (secIndex, pageNum) {
				return parseInt('' + secIndex + pageNum);
			},
			applySpacingStyle: function (value) {
				if (value !== null && value !== undefined) {
					if (value.indexOf(' ') < 0) {
						return { 'word-spacing': '-3px' };
					}
				}
			},
			checkRTPRange: function (UseWeighted, selectedPayables) {
				return this.getcheckRTPRange(this.memo, this.letterPreviewJurisdiction, UseWeighted, this, selectedPayables);
			},
			paytableRowStyle: function (item) {
				if (this.letterPreviewJurisdiction == 8 && this.illinoisPaytables.some(x => x.paytableId == item.paytableId))
					return ['illinois-row-border'];
				else
					return [];
			},
			differenceWrapper: function () {
				return _.differenceWith(
					this.paytables,
					this.checkRTPRange().map(x => x.paytable.paytableId),
					function (arrValue, exValue) {
						return arrValue.paytableId === exValue;
					});
			},
		}
	};
}


function buildComputedForMemo(memo, source, isSecondaryLanguageLetter) {
	if (isNotNull(memo) &&
		isNotNull(memo.letterContentControls) &&
		memo.letterContentControls.length > 0) {
		let ret = contentToComputed(memo.letterContentControls[0], memo.letterContentControls, source, isSecondaryLanguageLetter);
		if (memo.letterContentControls.length > 1) {
			for (let iter = 1; iter < memo.letterContentControls.length; iter++) {
				ret = { ...ret, ...contentToComputed(memo.letterContentControls[iter], memo.letterContentControls, source, isSecondaryLanguageLetter) };
			}
		}
		return ret;
	}
	else {
		return {
			['blank']: {
				get: function () {
					return 'blank';
				},
				set: function (sel) {
				}
			}
		};
	}
}

function buildMemoTemplate(content, isSecondaryLanguageLetter, secondaryContent) {
	let ret = "<div class=\"gli-letter-gen-memo\" ref=\"MemoRef\">";
	if (isSecondaryLanguageLetter && !Em(secondaryContent)) {
		ret += secondaryContent;
	}
	else {
		ret += content;
	}


	ret += "</div>";
	return ret;
}

function tableColumnToComputed(control) {
	return {
		[control.label]: control.content
	};
}
function tableRowToComputed(controls) {
	let ret = tableColumnToComputed(controls[0]);
	if (controls.length > 1) {
		for (let iter = 1; iter < controls.length; iter++) {
			ret = { ...ret, ...tableColumnToComputed(controls[iter]) };
		}
	}
	return ret;
};
/// uses JavaScript spread operator to convert the label/content pair of the given control to a new object, where the property name is control.label and the value returned is control.content
///This method is how we write e.g. {{ productType }} in the note content and have it return the value in the control with the label 'productType'
function contentToComputed(control, controls, source, isSecondaryLanguageLetter) {
	if (control.type !== "TABLE") {
		return {
			[control.label]: {
				get: function () {
					let targetControl = controls.find(c => c.id === control.id && (c.applicableJurisdictions.length === 0 || c.applicableJurisdictions.some(jur => jur === this.letterPreviewJurisdiction)));
					return targetControl.content;
				},
				set: function (sel) {
					let targetControl = controls.find(c => c.id === control.id && (c.applicableJurisdictions.length === 0 || c.applicableJurisdictions.some(jur => jur === this.letterPreviewJurisdiction)));
					targetControl.content = sel;
					//control.content = sel;
				}
			},
			["secondary" + control.label]: {
				get: function () {
					let targetControl = controls.find(c => c.id === control.id && (c.applicableJurisdictions.length === 0 || c.applicableJurisdictions.some(jur => jur === this.letterPreviewJurisdiction)));
					let val = (isSecondaryLanguageLetter && !Em(targetControl.secondaryContent)) ? targetControl.secondaryContent : targetControl.content;
					return val;
				},
				set: function (sel) {
					let targetControl = controls.find(c => c.id === control.id && (c.applicableJurisdictions.length === 0 || c.applicableJurisdictions.some(jur => jur === this.letterPreviewJurisdiction)));
					targetControl.secondaryContent = sel;
					//control.content = sel;
				}
			}
		};
	}
	else if (control.tableRows.length > 0) {
		//let ret = this.tableColumnToComputed(control.tableRows[0][0]);
		//let ret2 = this.tableColumnToComputed(control.tableRows[0][1]);

		//let ret3 = { ...ret, ...ret2 };
		return {
			[control.label]: {
				get: function () {
					let targetControl = controls.find(c => c.id === control.id && (c.applicableJurisdictions.length === 0 || c.applicableJurisdictions.some(jur => jur === this.letterPreviewJurisdiction)));
					let ret = tableRowToComputed(targetControl.tableRows[0]);
					let arrayRet = new Array(ret);
					if (targetControl.tableRows.length > 1) {
						for (let iter = 1; iter < targetControl.tableRows.length; iter++) {
							arrayRet.push(tableRowToComputed(targetControl.tableRows[iter]));
						}
					}
					return arrayRet;
				},
				set: function (sel) {

				}
			}
		};
	}
	else {
		return {
			[control.label]: {
				get: function () {
					return [];
				},
				set: function (sel) {

				}
			}
		};
	}
}
