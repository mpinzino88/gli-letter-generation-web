export default function (value) {
	if (!value) return "";
	return moment.utc(value).format("MMMM DD, YYYY");
}