export default function (value) {
	if (!value) return "";
	return moment.utc(value).format("dddd, MMMM DD, YYYY");
}