export default function(value) {
	if (!value) return "";
	return moment(value).format("l LT");
}