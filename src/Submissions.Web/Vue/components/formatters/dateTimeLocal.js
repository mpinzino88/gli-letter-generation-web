export default function (value) {
	if (!value) return "";
	return moment.utc(value).local().format("l LT");
}