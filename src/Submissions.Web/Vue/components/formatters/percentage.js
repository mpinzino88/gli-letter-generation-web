export default function (value, options, locale) {
	if (!value) return "";	
	return (value).toLocaleString(
		locale,	// leave null to use browser profile
		{ minimumFractionDigits: 1, style: "percent", ...options }
		// options object properties override defaults above
	);
}