﻿export const MODE = Object.freeze({
	ACTIVATE: { id: 0, code: 'Activate' },
	ADD: { id: 1, code: 'Add' },
	DEACTIVATE: { id: 2, code: 'Deactivate' },
	EDIT: { id: 3, code: 'Edit' },
	DELETE: { id: 4, code: 'Delete' }
});

//Accounting LetterBook Status
export const LETTERBOOKACCT_STATUS = Object.freeze({
	PENDING: { id: 1, description: 'Pending' },
	INPROGRESS: { id: 0, description: 'In Progress' },
	BILLED: { id: 2, description: 'Billed' },
	CLOSED: { id: 3, description: 'Closed' },
	PULLEDBACK: { id: 4, description: 'PulledBack' }
});

// id is the string like '07', not 7
export const JURISDICTION = Object.freeze({
	CALIFORNIA_INDIANS: { id: '73', description: 'California Indians' },
	DELAWARE_LOTTERY: { id: '50', description: 'Delaware Lottery' },
	MARYLAND_LOTTERY: { id: '231', description: 'Maryland Lottery' },
	RHODE_ISLAND: { id: '21', description: 'Rhode Island' },
	PENNSYLVANIAITL: { id: '508', description: 'Pennsylvania - ITL' },
	MICHIGANITL: { id: '536', description: 'Michigan ITL' }
});

export const JURISDICTION_STATUS = Object.freeze({
	AP: { code: 'AP' },
	CP: { code: 'CP' },
	DR: { code: 'DR' },
	LQ: { code: 'LQ' },
	MS: { code: 'MS' },
	MO: { code: 'MO' },
	PW: { code: 'PW' },
	NU: { code: 'NU' },
	OH: { code: 'OH' },
	RJ: { code: 'RJ' },
	RA: { code: 'RA' },
	TC: { code: 'TC' },
	RV: { code: 'RV' },
	WD: { code: 'WD' }
});

export const MANUFACTURER = Object.freeze({
	APL: { code: 'APL', description: 'Aristocrat Pty Limited' },
	ARP: { code: 'ARP' },
	ARI: { code: 'ARI' },
	ATA: { code: 'ATA' },
	ATR: { code: 'ATR' },
	BSM: { code: 'BSM' },
	GAL: { code: 'GAL' },
	GTE: { code: 'GTE' },
	GUK: { code: 'GUK' },
	IGT: { code: 'IGT' },
	IGU: { code: 'IGU' },
	LGT: { code: 'LGT' },
	SPE: { code: 'SPE' },
	VGT: { code: 'VGT' }
});

export const SUBMISSION_STATUS = Object.freeze({
	AP: { code: 'AP' },
	CP: { code: 'CP' },
	NL: { code: 'NL' },
	NU: { code: 'NU' },
	OH: { code: 'OH' },
	PN: { code: 'PN' },
	PW: { code: 'PW' },
	RJ: { code: 'RJ' },
	RV: { code: 'RV' },
	WD: { code: 'WD' }
});

export const PROJECT_STATUS = Object.freeze({
	RC: { code: 'RC' }
});


export const SUBMISSION_TESTINGTYPE = Object.freeze({
	LANDBASED: { description: 'Landbased' },
	IGAMING: { description: 'iGaming' },
	GLICLOUD: { description: 'GLiCloud' },
	SPORTSBOOK: { description: 'Sportsbook' }
});

export const SUB_PROJECT_TYPE = Object.freeze({
	EN: { id: 1, code: 'EN', description: 'Engineering' },
	TA: { id: 2, code: 'TA', description: 'Technical Analysis' },
	AU: { id: 3, code: 'AU', description: 'Audit' }
});

export const SUB_PROJECT_STATUS = Object.freeze({
	UNASSIGNED: { code: 'Unassigned', description: 'Unassigned' },
	CLOSED: { code: 'Closed', description: 'Completed' },
	NOTSTARTED: { code: 'NotStarted', description: 'Not Started' },
	INPROGRESS: { code: 'InProgress', description: 'In Progress' }
});

export const PROJECT_REVIEW_STATUS = Object.freeze({
	PENDING: { code: 'Pending', description: 'Pending' },
	CORRECTIONSPENDING: { code: 'CorrectionsPending', description: 'Corrections Pending' },
	VERIFICATIONPENDING: { code: 'VerificationPending', description: 'Verification Pending' },
	COMPLETED: { code: 'Completed', description: 'Completed' }
});

export const GAME_TYPE = Object.freeze({
	TRADITIONALSKILL: { description: 'Traditional Skill' },
	NONSKILL: { description: 'Non-Skill' },
	ANY: { description: 'Any' },
	REEL: { description: 'Reel' }
});

export const GAME_DESCRIPTION_TYPE = Object.freeze({
	MODIFICATIONDESCRIPTION: { code: 'ModificationDescription'}
});

export const MODIFICATION_TYPE = Object.freeze({
	NONCRITICALMODIFICATION: { id: 0 },
	REVOKE: { id: 1 },
	NONMANDATORYUPGRADE: { id: 2 }
});

export function sortValue(val) {
	if (!val) {
		return '';
	}
	else if (isNaN(val)) {
		return val.toLowerCase();
	}
	else {
		return val;
	}
}

export function isNotNull(obj) {
	return typeof obj !== 'undefined' && obj !== null;
}

export function isNull(obj) {
	return typeof obj === 'undefined' || obj === null;
}

Array.prototype.selectMany = function (keyGetter) {
	let temp = this.map(x => keyGetter(x));
	if (temp.length === 0) return [];
	else return temp.reduce((a, b) => a.concat(b));
};

Array.prototype.skip = function (count) {
	return this.filter((_, i) => i >= count);
};

Array.prototype.skipWhile = function (predicate) {
	return this.filter((_, i) => !predicate(_, i));
};

Array.prototype.take = function (count) {
	return this.filter((_, i) => i < count);
};

Array.prototype.takeWhile = function (predicate) {
	return this.filter((_, i) => predicate(_, i));
};

export function fileNumberGroups(fileNumber) {
	if (isNull(fileNumber)) {
		return {};
	}
	else {
		return fileNumber.match(/(?<Type>\w{2})-(?<Juris>\d{1,3})-(?<Manuf>.{3})-(?<Year>\d{2})-(?<Job>\d{1,4})/).groups;
	}
};
export function projectNameGroups(fileNumber) {
	if (isNull(fileNumber)) {
		return null;
	}
	else {
		let match = fileNumber.match(/(?<Type>\w{2})-(?<Juris>\d{1,3})-(?<Manuf>.{3})-(?<Year>\d{2})-(?<Job>\d{1,4})([-]{1}(?<Transfer>\d{1,3})){0,1}/);
		if (isNotNull(match))
			return match.groups;
		else return null;
	}
};