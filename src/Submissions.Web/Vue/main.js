﻿import Vue from 'vue';
import Vuelidate from 'vuelidate';
import App from './App';
import api from './api/index';
import lodash from 'lodash';
import upperFirst from 'lodash/upperFirst';
import camelCase from 'lodash/camelCase';
import router from './router';
import store from './store';

import splitHtml from 'split-html';

// kendo ui
import '@progress/kendo-ui';
import '@progress/kendo-theme-bootstrap/dist/all.css';
import { Editor } from '@progress/kendo-editor-vue-wrapper';
import { EditorTool } from '@progress/kendo-editor-vue-wrapper';
import { EditorInstaller } from '@progress/kendo-editor-vue-wrapper';

// prototypes
Object.defineProperty(Vue.prototype, '$http', { value: api });
Object.defineProperty(Vue.prototype, '$lodash', { value: lodash });

Vue.use(EditorInstaller);
Vue.use(Vuelidate);

import wysiwyg from "vue-wysiwyg";
Vue.use(wysiwyg, {});
import "vue-wysiwyg/dist/vueWysiwyg.css";

import VueEasyCm from 'vue-easycm';
Vue.use(VueEasyCm);

import VueTippy, { TippyComponent } from "vue-tippy";
Vue.use(VueTippy);
Vue.component("tippy", TippyComponent);

const filterComponents = require.context(
	// The relative path of the components folder
	'./components/filters',
	// Whether or not to look in subfolders
	true,
	// The regular expression used to match base component filenames
	/\w+\.(js)$/
);
const formatComponents = require.context(
	// The relative path of the components folder
	'./components/formatters',
	// Whether or not to look in subfolders
	true,
	// The regular expression used to match base component filenames
	/\w+\.(js)$/
);

const allFilters = {};
filterComponents.keys().forEach(fileName => {
	
	const filterName = fileName
		.split('/')
		.pop()
		.replace(/\.\w+$/, '');
	const filterFunc = filterComponents(fileName).default;

	// DEPRECATED: Filters removed in Vue 3
	Vue.filter(filterName, value => {
		console.warn(`DEPRECATED: Pipe syntax filters will be removed in Vue 3. You should use $filters.${filterName}(...) instead.`);
		return filterFunc(value);
	});

	allFilters[filterName] = filterFunc;
});

const allFormatters = {};
formatComponents.keys().forEach(fileName => {
	
	// Gets the file name regardless of folder depth
	const filterName = fileName
		.split('/')
		.pop()
		.replace(/\.\w+$/, '');
	const filterOldName = `format${upperFirst(filterName)}`;
	const filterFunc = formatComponents(fileName).default;

	// DEPRECATED: Filters removed in Vue 3
	Vue.filter(filterOldName, value => {
		console.warn(`DEPRECATED: Pipe syntax filters will be removed in Vue 3. You should use $format.${filterName}(...) instead.`);
		return filterFunc(value);
	});

	allFormatters[filterName] = filterFunc;
});


// TODO: Global properties are defined differently between Vue 2 and Vue 3.
// The Vue 2 implementation is used here; the Vue 3 implementation should be something like
//     app.config.globalProperties.$filters = allFilters
//     app.config.globalProperties.$format = allFormatters
// See https://v3.vuejs.org/api/application-config.html#globalproperties for more details.
// The actual usage of the filters in other components should be the same either way.
Object.defineProperty(Vue.prototype, '$filters', { value: allFilters });
Object.defineProperty(Vue.prototype, '$format', { value: allFormatters });


// Include all base components by default
// https://vuejs.org/v2/guide/components-registration.html#Automatic-Global-Registration-of-Base-Components

const requireComponent = require.context(
	// The relative path of the components folder
	'./components/base',
	// Whether or not to look in subfolders
	true,
	// The regular expression used to match base component filenames
	/Base[A-Z]\w+\.(vue|js)$/
);

requireComponent.keys().forEach(fileName => {
	// Get component config
	const componentConfig = requireComponent(fileName)

	// Get PascalCase name of component
	const componentName = upperFirst(
		camelCase(
			// Gets the file name regardless of folder depth
			fileName
				.split('/')
				.pop()
				.replace(/\.\w+$/, '')
		)
	)

	// Register component globally
	Vue.component(
		componentName,
		// Look for the component options on `.default`, which will
		// exist if the component was exported with `export default`,
		// otherwise fall back to module's root.
		componentConfig.default || componentConfig
	)
});

Vue.filter('truncate', function (str, n) {
	if (str)
		return str.length > n ? str.substr(0, n - 1) + '...' : str;
	else
		return '';
});

new Vue({
	el: '#app',
	router,
	store,
	template: '<App/>',
	components: {
		App,
		Editor
	}
});