﻿const dynamicsFileNumberRegExp = new RegExp('([a-zA-Z]{2}[0-9]{3}[a-zA-Z0-9]{3}[0-9]{5})$');
const dynamicsTransferRegExp = new RegExp('([a-zA-Z]{2}[0-9]{3}[a-zA-Z0-9]{3}[0-9]{5})([0-9]{3})');
const fileNumberRegExp = new RegExp('([a-zA-Z]{2})-([1-9]?[0-9]{2})-([a-zA-Z0-9]{3})-([0-9]{2})-([1-9]?[0-9]{2})-?([1-9]?[0-9]{2})?');

export const dynamicsFormat = (value) => {
	if (!value) {
		return true;
	}
	return dynamicsFileNumberRegExp.test(value) || dynamicsTransferRegExp.test(value);
};

export const dynamicsOrFileNumberFormat = (value) => {
	if (!value) {
		return true;
	}
	return dynamicsFileNumberRegExp.test(value) || dynamicsTransferRegExp.test(value) || fileNumberRegExp.test(value);
};

export const fileNumberFormat = (value) => {
	if (!value) {
		return true;
	}
	return fileNumberRegExp.test(value);
};