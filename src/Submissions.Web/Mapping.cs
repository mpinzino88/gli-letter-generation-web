﻿using AutoMapper;
using EF.Submission;
using Newtonsoft.Json;
using Submissions.Web.Models.Widgets;
using System;

namespace Submissions.Web.Mappings
{
	public class RootProfile : Profile
	{
		public RootProfile()
		{
			CreateMap<tbl_Widget, Widget>()
				.ForSourceMember(src => src.UserId, opt => opt.DoNotValidate())
				.ForMember(dest => dest.StartColor, opt => opt.Ignore())
				.ForMember(dest => dest.Url, opt => opt.Ignore())
				.ForMember(dest => dest.FiltersView, opt => opt.Ignore())
				.ForMember(dest => dest.DashboardName, opt => opt.NullSubstitute(Globals.DefaultDashboardName))
				.ForMember(dest => dest.Type, opt => opt.MapFrom(src => (WidgetType)Enum.Parse(typeof(WidgetType), src.Type)))
				.ForMember(dest => dest.DataSource, opt => opt.MapFrom(src => src.DataSource == null ? (WidgetDataSource?)null : (WidgetDataSource)Enum.Parse(typeof(WidgetDataSource), src.DataSource)))
				.ForMember(dest => dest.Position, opt => opt.MapFrom(src => src.Position == null ? null : JsonConvert.DeserializeObject<Position>(src.Position)));

			CreateMap<tbl_Widget, ListWidget>()
				.IncludeBase<tbl_Widget, Widget>()
				.ForMember(dest => dest.Columns, opt => opt.Ignore());
		}
	}
}