﻿using Submissions.Common;

namespace Submissions.Web.Models.Widgets
{
	public class EditChartViewModel
	{
		public EditChartViewModel()
		{
			this.Mode = Mode.Edit;
		}

		public Mode Mode { get; set; }
		public ChartWidget Widget { get; set; }
		public string PageSource { get; set; }
	}

	public class EditListViewModel
	{
		public EditListViewModel()
		{
			this.Mode = Mode.Edit;
			this.Widget = new ListWidget();
		}

		public Mode Mode { get; set; }
		public ListWidget Widget { get; set; }
		public string PageSource { get; set; }
	}
}