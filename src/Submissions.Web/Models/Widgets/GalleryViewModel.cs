﻿using System.Collections.Generic;

namespace Submissions.Web.Models.Widgets
{
	public class GalleryViewModel
	{
		public IList<Widget> Widgets { get; set; }
	}
}