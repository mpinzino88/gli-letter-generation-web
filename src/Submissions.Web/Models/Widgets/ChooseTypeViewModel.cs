﻿namespace Submissions.Web.Models.Widgets
{
	public class ChooseTypeViewModel
	{
		public WidgetType? WidgetType { get; set; }
	}
}