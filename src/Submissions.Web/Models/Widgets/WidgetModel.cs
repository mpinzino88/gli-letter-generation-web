﻿using Newtonsoft.Json;
using Submissions.Common.JQGrid;
using System.ComponentModel.DataAnnotations;
using System.Drawing;
using System.Web;
using System.Web.Mvc;

namespace Submissions.Web.Models.Widgets
{
	public class Widget
	{
		public Widget()
		{
			this.Color = Globals.WidgetDefaultPanelColor;
		}

		public int? Id { get; set; }
		[Display(Name = "Type")]
		public WidgetType Type { get; set; }
		[Display(Name = "Title")]
		public string Title { get; set; }
		[Display(Name = "Display Toolbar Search")]
		public bool ToolbarSearch { get; set; }
		[Display(Name = "Show On Dashboard")]
		public bool ShowOnDashboard { get; set; }
		[Display(Name = "Color")]
		public string Color { get; set; }
		public string StartColor
		{
			get
			{
				if (string.IsNullOrEmpty(this.Color))
				{
					return string.Empty;
				}

				var startColor = ColorTranslator.FromHtml(this.Color);
				float correctionFactor = 0.4f;
				float red = (255 - startColor.R) * correctionFactor + startColor.R;
				float green = (255 - startColor.G) * correctionFactor + startColor.G;
				float blue = (255 - startColor.B) * correctionFactor + startColor.B;

				startColor = System.Drawing.Color.FromArgb(startColor.A, (int)red, (int)green, (int)blue);
				return ColorTranslator.ToHtml(startColor);
			}
		}
		[Display(Name = "Data Source"), Required]
		public WidgetDataSource? DataSource { get; set; }
		public Position Position { get; set; }
		public string Url
		{
			get
			{
				var httpContext = HttpContext.Current;
				if (httpContext == null)
					return "";

				string loadController = "";
				switch (this.Type)
				{
					case WidgetType.Chart:
						loadController = "LoadChart";
						break;
					case WidgetType.List:
						loadController = "LoadList";
						break;
				}

				if (string.IsNullOrEmpty(loadController))
				{
					return "";
				}
				else
				{
					return new UrlHelper(httpContext.Request.RequestContext).Action(loadController, "Widget", new { Id = this.Id });
				}
			}
		}
		public int? DashboardId { get; set; }
		[Display(Name = "Dashboard")]
		public string DashboardName { get; set; }
		public string Filters { get; set; }
		public string FiltersView { get; set; }
	}

	public class ChartWidget : Widget
	{
		//public ChartWidget()
		//{
		//	this.Filters = new ChartSearch();
		//}

		//public ChartSearch Filters { get; set; }

		[Display(Name = "ChartType")]
		public string ChartType { get; set; }
	}

	public class ListWidget : Widget
	{
		public ListWidget()
		{
			this.Columns = new ColumnChooser();
		}

		public string SavedColumns { get; set; }
		public ColumnChooser Columns { get; set; }
	}

	#region Helper Classes
	public class AvailableColumn
	{
		public string DatabaseName { get; set; }
		public string DisplayName { get; set; }
	}

	public class Position
	{
		public Position()
		{
			this.Col = 1;
			this.Row = 1;
			this.Size_X = 2;
			this.Size_Y = 2;
		}

		public int WidgetId { get; set; }
		public int Row { get; set; }
		public int Col { get; set; }
		public int Size_X { get; set; }
		public int Size_Y { get; set; }
		public int Height
		{
			get
			{
				var height = (this.Size_Y * Globals.WidgetDimension) - Globals.WidgetPanelGridHeight;
				if (this.Size_Y > 1)
				{
					height = height + ((this.Size_Y - 1) * Globals.WidgetTotalMargin);
				}

				return height;
			}
		}
		public int Width
		{
			get
			{
				var width = (this.Size_X * Globals.WidgetDimension);
				if (this.Size_X > 1)
				{
					width = width + ((this.Size_X - 1) * Globals.WidgetTotalMargin);
				}

				return width;
			}
		}
	}
	#endregion
}