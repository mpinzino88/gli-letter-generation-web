﻿using Trirand.Web.Mvc;

namespace Submissions.Web.Models.Widgets
{
	public class LoadListViewModel
	{
		public JQGrid Grid { get; set; }
	}
}