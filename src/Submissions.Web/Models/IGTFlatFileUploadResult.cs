﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Submissions.Web.Models
{
	public class IGTFlatFileUploadResult
	{
		public IGTFlatFileUploadResult(string FileNumber, string SpreadSheetName, string UploadResult, Exception Exception = null)
		{
			this.FileNumber = FileNumber;
			this.SpreadSheetName = SpreadSheetName;
			this.UploadResult = UploadResult;
			this.Exception = Exception;
		}
		public string FileNumber { get; set; }
		public string SpreadSheetName { get; set; }
		public string UploadResult { get; set; }
		public Exception Exception { get; set; }
	}
}