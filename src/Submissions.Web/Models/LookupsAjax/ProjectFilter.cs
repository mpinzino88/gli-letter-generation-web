﻿using Submissions.Common;
using System.Collections.Generic;

namespace Submissions.Web.Models.LookupsAjax
{
	public class ProjectFilter
	{
		public ProjectFilter()
		{
			this.InStatus = new List<string>();
			this.InBundleStatus = new List<string>();
			this.ProjectType = BundleType.OriginalTesting;
		}

		public List<string> InStatus { get; set; }
		public List<string> InBundleStatus { get; set; }
		public bool? IsTransfer { get; set; }
		public BundleType? ProjectType { get; set; }
	}
}