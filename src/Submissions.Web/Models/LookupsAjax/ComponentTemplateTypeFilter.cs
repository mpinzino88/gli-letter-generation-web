﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Submissions.Web.Models.LookupsAjax
{
	public class ComponentTemplateTypeFilter
	{
		public ComponentTemplateTypeFilter()
		{
			JurisdictionIds = new List<int>();
			PlatformIds = new List<int>();
		}

		public IList<int> JurisdictionIds { get; set; }
		public string manufacturersShortId { get; set; }
		public IList<int> PlatformIds { get; set; }
	}
}