﻿using System.Collections.Generic;

namespace Submissions.Web.Models.LookupsAjax
{
	public class PlatformPiecesTemplatesFilter
	{
		public IList<string> JurisdictionIds { get; set; }
		public string ManufactureShortId { get; set; }
		public int PlatformId { get; set; }
	}
}