﻿using System.Collections.Generic;

namespace Submissions.Web.Models.LookupsAjax
{
	public class BillingSheetFilter
	{
		public BillingSheetFilter()
		{
			this.ShowOnlyBillingSheets = new List<string>();
		}
		public int? LocationId { get; set; }
		public List<string> ShowOnlyBillingSheets { get; set; }
	}
}