﻿namespace Submissions.Web.Models.LookupsAjax
{
	public class LetterContactsFilter
	{
		public string JurisdictionId { get; set; }
		public string ManufacturerId { get; set; }
		public string ContactType { get; set; }
	}
}