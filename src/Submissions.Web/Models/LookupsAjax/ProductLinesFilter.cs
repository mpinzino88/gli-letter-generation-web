﻿namespace Submissions.Web.Models.LookupsAjax
{
	public class ProductLinesFilter
	{
		public string ManufacturerCode { get; set; }
		public int TestingTypeId { get; set; }
	}
}