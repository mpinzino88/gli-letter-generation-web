﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Submissions.Web.Models.LookupsAjax
{
	public class LetterContentQuestionFilter
	{
		public LetterContentQuestionFilter()
		{
			ExcludeQuestionIds = new List<int>();
		}

		public List<int> ExcludeQuestionIds { get; set; }
	}
}