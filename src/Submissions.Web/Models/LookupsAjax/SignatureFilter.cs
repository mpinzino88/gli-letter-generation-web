﻿using System.Collections.Generic;

namespace Submissions.Web.Models.LookupsAjax
{
	public class SignatureFilter
	{
		public SignatureFilter()
		{
			ExcludedTypes = new List<string>();
		}

		public string TypeId { get; set; }
		public List<string> ExcludedTypes { get; set; }
	}
}