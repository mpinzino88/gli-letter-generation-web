﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Submissions.Web.Models.LookupsAjax
{
	public class TaskTemplatesFilter
	{
		public TaskTemplatesFilter()
		{
			this.UserIds = new List<int>();
		}
		public List<int> UserIds { get; set; }
	}
}