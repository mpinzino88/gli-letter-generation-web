﻿using Submissions.Common;
using System.Collections.Generic;

namespace Submissions.Web.Models.LookupsAjax
{
	public class UsersFilter
	{
		public List<Department> Departments { get; set; }
		public bool DevReps { get; set; }
		public UserDropDownType? DropDownType { get; set; }
		public List<int> ExcludeUserIds { get; set; }
		public bool ExcludeUnavailable { get; set; }
		public int? LocationId { get; set; }
		public List<int> LocationIds { get; set; }
		public bool ManagerSearch { get; set; }
		public Permission? Permission { get; set; }
		public bool ProjectLeads { get; set; }
		public bool MAProjectLeads { get; set;  }
		public bool ProjectManagers { get; set; }
		public List<Role> Roles { get; set; }
		public bool TemplateOwner { get; set; }
		public bool RoleOrPermission { get; set; }
	}
}