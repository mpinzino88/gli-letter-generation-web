﻿using System.Collections.Generic;
using System.Web.Mvc;

namespace Submissions.Web.Models.LookupsAjax
{
	public class ManufacturerGroupFilter
	{
		public IList<SelectListItem> ManufacturerGroupCategoryCodes { get; set; }

		public IList<SelectListItem> ManufacturerCodes { get; set; }
	}
}