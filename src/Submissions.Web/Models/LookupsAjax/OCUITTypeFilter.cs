﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Submissions.Web.Models.LookupsAjax
{
	public class OCUITTypeFilter
	{
		public string ManufacturersShortId { get; set; }
		public string Type { get; set; } //compatibility, continuity or jurisdiction chip
		public int ExcludeProjectId { get; set; }
	}
}