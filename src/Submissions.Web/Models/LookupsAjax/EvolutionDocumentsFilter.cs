﻿using System.Collections.Generic;

namespace Submissions.Web.Models.LookupsAjax
{
	public class EvolutionDocumentsFilter
	{
		public EvolutionDocumentsFilter()
		{
			this.DocumentIds = new List<string>();
			this.JurisdictionIds = new List<int>();
		}

		public List<string> DocumentIds { get; set; }
		public IList<int> JurisdictionIds { get; set; }
		public string ComponentGroupId { get; set; }
		public string NativeLanguage { get; set; }
	}
}