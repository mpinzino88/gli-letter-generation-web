﻿namespace Submissions.Web.Models.LookupsAjax
{
	public class DocumentTypeFilter
	{
		public bool IsSupplementType { get; set; }
		public bool IsCertType { get; set; }
	}
}