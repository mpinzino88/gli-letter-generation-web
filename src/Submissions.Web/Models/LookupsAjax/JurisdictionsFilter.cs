﻿using System.Collections.Generic;

namespace Submissions.Web.Models.LookupsAjax
{
	public class JurisdictionsFilter
	{
		public JurisdictionsFilter()
		{
			this.ExcludedJurisdictionIds = new List<string>();
			this.JurisdictionIds = new List<string>();

		}

		public List<string> ExcludedJurisdictionIds { get; set; }
		public List<string> JurisdictionIds { get; set; }
	}
}