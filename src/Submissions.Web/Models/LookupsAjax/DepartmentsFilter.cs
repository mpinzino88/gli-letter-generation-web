﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Submissions.Web.Models.LookupsAjax
{
	public class DepartmentsFilter
	{
		public DepartmentsFilter()
		{
			this.DepartmentIds = new List<int>();
		}

		public List<int> DepartmentIds { get; set; }
	}
}