﻿using System.Collections.Generic;

namespace Submissions.Web.Models.LookupsAjax
{
	public class TasksFilter
	{
		public TasksFilter()
		{
			this.DepartmentIds = new List<int>();
		}

		public List<int> DepartmentIds { get; set; }
		public bool IsDynamicSet { get; set; }
	}
}