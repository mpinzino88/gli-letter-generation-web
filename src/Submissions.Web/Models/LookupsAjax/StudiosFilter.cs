﻿namespace Submissions.Web.Models.LookupsAjax
{
	public class StudiosFilter
	{
		public string ManufacturerCode { get; set; }
		public int TestingTypeId { get; set; }
	}
}