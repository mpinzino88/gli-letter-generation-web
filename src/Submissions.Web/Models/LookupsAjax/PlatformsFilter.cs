﻿namespace Submissions.Web.Models.LookupsAjax
{
	public class PlatformsFilter
	{
		public int ManufactureGroupId { get; set; }
		public string ManufactureShortId { get; set; }
		public int ManufacturerGroupCategoryId { get; set; }
	}
}