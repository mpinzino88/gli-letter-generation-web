﻿using System.Collections.Generic;
namespace Submissions.Web.Models.LookupsAjax
{
	public class NativeLanguagesFilter
	{
		public NativeLanguagesFilter()
		{
			this.NativeLanguageIds = new List<string>();
		}
		public List<string> NativeLanguageIds { get; set; }
		public string ComponentGroupId { get; set; }
	}
}