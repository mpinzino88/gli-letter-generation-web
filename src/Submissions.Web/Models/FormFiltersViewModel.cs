﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Submissions.Web.Models
{
	public class FormFiltersViewModel
	{
		public FormFiltersViewModel()
		{
			this.FormFilters = new List<FormFilter>();
		}

		public IList<FormFilter> FormFilters { get; set; }
	}

	public class FormFilter
	{
		public int Id { get; set; }
		[Display(Name = "Date")]
		public DateTime AddDate { get; set; }
		[Display(Name = "Name")]
		public string FilterName { get; set; }
	}
}