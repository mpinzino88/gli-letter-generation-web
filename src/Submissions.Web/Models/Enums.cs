using System.ComponentModel;

namespace Submissions.Web
{
	/// <summary>
	/// Enumerations that are common to Submissions and won't be used in other projects (ie. Submissions.Common.Enums)
	/// </summary>
	public enum BillType
	{
		[Description("Unassigned Billable")]
		Billable,
		[Description("Unassigned Non-Billable")]
		NonBillable,
		[Description("Unassigned No Charge")]
		NoCharge,
		[Description("Assigned")]
		Assigned,
		[Description("Billable")]
		TotalBillable
	}
	public enum ENProjectStatusFilter
	{
		[Description("Accepted/Assigned")]
		AA,
		[Description("Accepted/Unassigned")]
		AU,
		[Description("Delayed Certification (On Hold)")]
		DCOH,
		[Description("Delayed Certification (Requested)")]
		DCRQ,
		[Description("Completed(last 30 days)")]
		DN,
		[Description("On Hold")]
		OH,
		[Description("In Review")]
		IR,
		[Description("In Testing")]
		IT,
		[Description("In QA")]
		QA,
		[Description("Review Complete")]
		RC,
		[Description("Re-Review")]
		RR,
		[Description("Unaccepted/Unassigned")]
		UU,
		[Description("All Open")]
		OPEN
	}

	public enum ColumnChooserGrid
	{
		JurisdictionalDataReport,
		ProjectsReport,
		SubmissionsReport,
		TasksReport,
		GlimpseGrid
	}

	public enum ColumnChooserMode
	{
		Add,
		Edit,
		Apply
	}

	public enum FormFilterSection
	{
		ProjectLegacy,
		QAProjectLegacy,
		ResourceFinder,
		WorkloadBalancer
	}

	public enum GameDescriptionStatus
	{
		New = 1,
		Pending = 2,
		Rejected = 3,
		Revoked = 4,
		Approved = 5
	}

	public enum GlimpseCustomFilter
	{
		[Description("> 25 Days")]
		Age25,
		[Description("Past Target")]
		PastTarget,
		[Description("Near Target")]
		NearTarget,
		[Description("Off Target")]
		OffTarget,
		[Description("Near Budget")]
		NearBudget,
		[Description("Over Budget")]
		OverBudget
	}

	public enum GlimpseDownloadOptions
	{
		[Description("Latest EN Project Note")]
		LatestProjectNote,
		[Description("Latest QA Project Note")]
		LatestQAProjectNote,
		[Description("Latest On Hold Note")]
		LatestOHNote,
		[Description("EN Tasks")]
		Tasks,
		[Description("QA Tasks")]
		QATasks,
		[Description("Jurisdiction Authorization")]
		IncludeJurAuth,
	}

	public enum GLIVerifyLicenseType
	{
		[Description("Annual Invoice")]
		AnnualInvoice,
		[Description("Biennial Invoice")]
		BiennialInvoice,
		[Description("Annual")]
		Annual,
		[Description("Monthly")]
		Monthly
	}

	public enum HistoryLogTableName
	{
		[Description("Draft Letters")]
		DraftLetters,
		[Description("Email Distribution Users")]
		EmailDistributionUsers,
		[Description("Shipping Data")]
		File_Details,
		[Description("Jurisdictions Data")]
		JurisdictionalData,
		[Description("Language/Supplement Letters")]
		Letters,
		[Description("Source Code Storage")]
		SourceCodeStorage,
		[Description("Submissions Data")]
		Submissions,
		[Description("Jurisdiction List")]
		tbl_lst_fileJuris,
		[Description("Manufacture List")] //all email fields have neen changed and storing in distibution tables now
		tbl_lst_fileManu,
		[Description("Quote/Authorization Date")]
		tbl_QuoteAuthDate,
		[Description("Signatures")]
		tblSignatures,
		[Description("BugBox")]
		trBugBox
	}

	public enum LetterManagerBrowseMode
	{
		Local,
		Network
	}

	public enum LookupAjax
	{
		AccountStatus,
		Accreditations,
		AcumaticaBillingExceptionTypes,
		AcumaticaTenants,
		AnalysisType,
		AppFees,
		ArchiveLocations,
		AssociatedSoftwaresTemplate,
		BillingParties,
		BillingSheets,
		BillToMappings,
		Bundles,
		BusinessOwners,
		Cabinets,
		CertificationIssuers,
		ChipTypes,
		Classifications,
		ClassificationTypes,
		Companies,
		CompilationMethod,
		ComplexityRating,
		ComponentsAdd,
		ComponentGroups,
		ComponentTypes,
		ContractTypes,
		Countries,
		CountryAbb,
		Currencies,
		DeliveryMechanism,
		Departments,
		DocumentType,
		DynamicsDatabases,
		EmailDistributions,
		EmailTypes,
		EResultsAUEnums,
		EvolutionDocuments,
		ExternalLabs,
		FileNumber,
		Functions,
		FunctionalRoles,
		GameDescriptionStatuses,
		GameDescriptionTypes,
		GameSubmissionTypes,
		GamingGuidelines,
		GamingGuidelinesAttributes,
		GamingGuidelinesGameTypes,
		GamingGuidelineJurisdictions,
		GamingGuidelineJurisdictionsSelected,
		GamingGuidelineLimits,
		GamingGuidelineOddsRequirement,
		GamingGuidelineProhibitedGameTypes,
		GamingGuidelineProtocols,
		GamingGuidelineTechTypes,
		GameTypes,
		HolidayYears,
		HollandJurisdictions,
		IGTJurisdictionMappings,
		Jurisdictions,
		JurisdictionGroups,
		JurisdictionGroupCategories,
		Laboratories,
		LaboratoryLetterAddress,
		Language,
		LetterContentCompareCriteria,
		LetterContentDefaultValues,
		LetterContentFormatFilter,
		LetterContentMemos,
		LetterContentQuestions,
		LetterContentReportTypes,
		LetterContentSection,
		LetterContentTestingResult,
		LetterHeads,
		Liaisons,
		LimitNumber,
		Manufacturers,
		ManufacturerGroups,
		ManufacturerGroupCategories,
		MathQueueManufacturerGroups,
		MobileDevices,
		NativeLanguages,
		OddNumber,
		PaytableIds,
		PlatformPiecesTemplates,
		Platforms,
		PdfFolderDesignations,
		ProductClassification,
		ProductLine,
		ProductTypes,
		ProductJurisdictionalStatus,
		ProjectOutputs,
		ProjectReviews,
		QATaskDefinitions,
		QAUsers,
		RevisionReasons,
		Roles,
		RTPNumber,
		SecondaryFunctions,
		ShipmentRecipients,
		ShipmentCouriers,
		SignatureScopes,
		SignatureTypes,
		SignatureVersions,
		SpecializedTypes,
		SportsBettingJurisdictions,
		StandardJurisdictions,
		Standards,
		SnapShots,
		SubmissionIdNumbers,
		SubmissionTypes,
		SubscriptionGroups,
		Subscriptions,
		SupplierFocus,
		TargetDateChangeReason,
		TaskDefinitions,
		TaskDocuments,
		TaskTemplates,
		Teams,
		TestCase,
		TestingPerformed,
		TestingTypes,
		Themes,
		TranslationHouses,
		ProjectName,
		Users,
		UpdatedDateReason,
		Vendors,
		WithdrawReason,
		WorkPerformed,
		CPReviewTasks,
		LetterContact,
		LetterGenJurisdictions,
		MobileDevicesTested,
		SASPollNames,
		ProtocolPlatformsManufacturerGroups
	}

	public enum MathReviewType
	{
		[Description("Math Submit")]
		Submit,
		[Description("Math Complete")]
		Review,
		[Description("All")]
		All
	}

	public enum MathGridType
	{
		Reviews,
		Tasks,
		ReviewsDN
	}

	public enum OHViewMode
	{
		Project,
		Submission,
		Jurisdiction
	}

	public enum PageSource
	{
		SubmissionDetail,
		SubmissionDetailPartialView
	}

	public enum Permission
	{
		AccessMVC,
		ActAsQAR,
		ActAsQLW,
		Admin,
		AdminManager,
		BundlingGroups,
		CanRepostLetters,
		CPAdminQueue,
		Dynamics,
		EditCompleteIncRev,
		EmailsManagement,
		EngTasks,
		ETA,
		HRHolidays,
		LetterBookDefaults,
		LetterBookEntry,
		LetterBookReview,
		LetterBookBilling,
		LetterBookDeletePDF,
		LetterBookVue,
		LetterGeneration,
		LetterGenDetail,
		LetterGenSubSig,
		MasterBilling,
		MathCryptoStrength,
		MathForms,
		MathQueue,
		NoteManager,
		NoteManagerAdmin,
		OriginalTargetDate,
		PDFEmailQueue,
		PDFRuleActivate,
		PCT,
		PCW,
		QATools,
		RegCert,
		RegDoc,
		ReopenProject,
		RNGData,
		Expedite,
		TargetDate,
		TargetDateOffsets,
		Signatures,
		Submission,
		VerifyLicenseMgmt,
		ProjectActions,//This ties to Act As senior in classic site
		SLA,
		WorkloadManager,
		GamingGuideline,
		UKQueue,
		UserFields,
		JurisdictionMapping
	}

	public enum ProtrackType
	{
		QA,
		ENG
	}

	public enum ProtrackGridType
	{
		[Description("Protrack")]
		Protrack,
		[Description("Glimpse")]
		Glimpse,
		[Description("My Tasks")]
		MyTasks
	}

	public enum ProjectPersonnelType
	{
		[Description("Transfer Project(s) To Manager:")]
		Manager,
		[Description("Set Development Representative:")]
		DevRep,
		[Description("Set Project Lead:")]
		ProjectLead,
		[Description("Transfer Project(s) To Reviewer:")]
		QAReviewer,
	}

	public enum ProjectType
	{
		All = 2,
		[Description("Primary")]
		Projects = 0,
		[Description("Transfers")]
		Transfers = 1,
	}

	public enum QAProjectStatusFilter
	{
		[Description("All Open")]
		OPEN,
		[Description("In Engineering")]
		ENG,
		[Description("Accepted/Assigned")]
		AA,
		[Description("Accepted/Unassigned")]
		AU,
		[Description("Letter Drafting")]
		DL,
		[Description("Completed(last 30 Days)")]
		DN,
		[Description("Finalization")]
		FL,
		[Description("In Review")]
		IR,
		[Description("Engineering Re-Review")]
		ENRR,
		[Description("Unaccepted/Unassigned")]
		UU,
		[Description("On Hold")]
		OH
	}

	public enum QATaskStatusFilter
	{
		[Description("Open")]
		Open,
		[Description("Completed")]
		Closed,
		[Description("Completed - Bundles IR")]
		ClosedIR,
		[Description("Not Started")]
		NotStarted,
		[Description("In Progress")]
		InProgress,
		[Description("On Hold")]
		OnHold,
		[Description("Bundles In Finalization")]
		Finalization,
		[Description("Bundles Eng Re-Review")]
		EngReReview,
		[Description("Bundles with Issues")]
		Issue,
	}

	public enum ResourceFinderRequestStatus
	{
		Approved,
		Declined,
		Pending
	}

	public enum ReviewType
	{
		[Description("In Coming")]
		InComing,
		[Description("Out Going")]
		OutGoing,
	}

	public enum ShipmentCourier
	{
		[Description("Elect.")]
		Elect
	}

	public enum ShipmentRecipientCode
	{
		Manuf_NJ
	}

	public enum SportsBettingSelectOptions
	{
		[Description("No")]
		No = 0,
		[Description("Yes")]
		Yes = 1,
		[Description("Unknown")]
		Unknown = 2
	}

	public enum SubProjectStatus
	{
		[Description("Unassigned")]
		Unassigned,
		[Description("Completed")]
		Closed,
		[Description("Not Started")]
		NotStarted,
		[Description("In Progress")]
		InProgress
	}

	public enum TaskStatusFilter
	{
		[Description("Open")]
		Open,
		[Description("Completed")]
		Closed,
		[Description("Not Started")]
		NotStarted,
		[Description("In Progress")]
		InProgress,
		[Description("On Hold")]
		OnHold
	}

	public enum TasksWithActualHours
	{
		Project = 0,
		ProjectTransfers = 1,
		ProjectForeign = 2
	}
	public enum UserDropDownType
	{
		Task
	}

	public enum WidgetDataSource
	{
		[Description("Jurisdictions")]
		JurisdictionalData,
		[Description("Projects")]
		Project,
		[Description("Submissions")]
		Submission,
		[Description("Tasks")]
		Task
	}

	public enum WidgetType
	{
		Chart,
		List
	}
}