﻿namespace Submissions.Web.Models
{
	public class LayoutNavBarViewModel
	{
		public LayoutNavBarViewModel()
		{
			this.NavBarTitle = "GLI";
		}

		public string DefaultSearchType { get; set; }
		public string DefaultSearchInputClass { get; set; }
		public string DefaultSearchMagGlassClass { get; set; }
		public string JiraUrl { get; set; }
		public string NavBarClass { get; set; }
		public string NavBarTitle { get; set; }
		public string SharepointUrl { get; set; }
		public string SubmissionsUrl { get; set; }
	}
}