﻿using Submissions.Common;
using Submissions.Web.Models.Widgets;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Submissions.Web.Models.Dashboard
{
	public class DashboardIndexViewModel
	{
		public string DefaultDashboard { get; set; }
		public IList<SelectListItem> Dashboards { get; set; }
		public IList<Widget> Widgets { get; set; }
	}

	public class DashboardEditViewModel
	{
		public DashboardEditViewModel()
		{
			this.Mode = Mode.Add;
		}

		public Mode Mode { get; set; }
		public int? Id { get; set; }
		[Required, StringLength(50), Display(Name = "Name")]
		public string Name { get; set; }
		[StringLength(500), Display(Name = "Description")]
		public string Description { get; set; }
		[Display(Name = "Clone Current Dashboard")]
		public bool CloneCurrent { get; set; }
	}
}