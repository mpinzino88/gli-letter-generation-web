﻿using System;
using System.Web;

namespace Submissions.Web.Models.QueryService
{
	public class JiraItem
	{
		public int Id { get; set; }
		public string Key { get; set; }
		public string Status { get; set; }
		public string Summary { get; set; }
		public string Resolution { get; set; }
	}

}