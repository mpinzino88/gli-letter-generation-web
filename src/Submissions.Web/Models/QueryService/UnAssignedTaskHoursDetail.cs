﻿using System.ComponentModel.DataAnnotations;

namespace Submissions.Web.Models.QueryService
{
	public class UnassignedTaskHoursDetail
	{
		public int? ProjectId { get; set; }
		public bool? IsAssigned { get; set; }
		public string BillType { get; set; }
		[Display(Name = "Task Id")]
		public string TaskId { get; set; }
		[Display(Name = "Task Description")]
		public string Description { get; set; }
		[Display(Name = "User")]
		public string User { get; set; }
		[Display(Name = "Hours")]
		public decimal? Hours { get; set; }
	}
}