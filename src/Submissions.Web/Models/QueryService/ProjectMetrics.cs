﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Submissions.Web.Models.QueryService
{
	public class ProjectMetrics
	{
		public int ProjectId { get; set; }
		public int DepartmentId { get; set; }
		public string Holder { get; set; }
		public string Status { get; set; }
		public string Event { get; set; }
		public DateTime? TimeStamp { get; set; }
		public DateTime? ChangeDate { get; set; }
		public int? BusinessDays { get; set; }
	}
}