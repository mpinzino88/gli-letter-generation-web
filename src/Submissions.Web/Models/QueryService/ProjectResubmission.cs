﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Submissions.Web.Models.QueryService
{
	public class Resubmission
	{
		public string MasterFile { get; set; }
		public bool IsResubmission { get; set; }
		public bool IsContinuation { get; set; }
		public bool IsPreCertification { get; set; }
	}
		
}