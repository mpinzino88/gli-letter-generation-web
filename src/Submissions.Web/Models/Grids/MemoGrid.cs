﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;
using Trirand.Web.Mvc;

namespace Submissions.Web.Models.Grids
{
	public class MemoGrid
	{
		public MemoGrid()
		{
			Grid = new JQGrid
			{
				Columns = new List<JQGridColumn>
				{
					new JQGridColumn
					{
						DataField = "Id",
						HeaderText = "Memo Id",
						PrimaryKey = true,
						Editable = false,
						Visible = false
					},
					new JQGridColumn
					{
						DataField = "MemoDate",
						HeaderText = "Date",
						DataType = typeof(DateTime),
						Width = 250
					},
					new JQGridColumn
					{
						DataField = "UserName",
						HeaderText = "User",
						DataType = typeof(string),
						Width = 250
					},
					new JQGridColumn
					{
						DataField = "Note",
						HeaderText = "Note",
						DataType = typeof(string)
					},
					new JQGridColumn
					{
						DataField = "Edit",
						Formatter = new CustomFormatter{FormatFunction = "editLink"},
						Searchable = false,
						Sortable = false,
						Width = 50,
						CssClass = "padright"
					}
				}
			};
			Grid.ID = "MemoGrid";
			Grid.DataUrl = new UrlHelper(((MvcHandler)HttpContext.Current.Handler).RequestContext).Action("LoadMemoGrid", "GridsAjax", new { Area = "" });
			Grid.AutoWidth = true;
			Grid.Width = Unit.Percentage(80);
			Grid.Height = Unit.Percentage(100);
			Grid.ToolBarSettings = new ToolBarSettings
			{
				ShowRefreshButton = true,
				ToolBarPosition = ToolBarPosition.Top,
				ShowSearchButton = true,
				ShowSearchToolBar = true,
			};
			Grid.PagerSettings = new Trirand.Web.Mvc.PagerSettings
			{
				PageSize = 100,
				PageSizeOptions = "[]"
			};
			Grid.AppearanceSettings = new AppearanceSettings
			{
				AlternateRowBackground = true,
				HighlightRowsOnHover = true,
				ShowFooter = false
			};
			Grid.ClientSideEvents.BeforeAjaxRequest = "gridBeforeAjaxRequest";
		}
		public JQGrid Grid { get; set; }
	}

	public class MemoGridRecord
	{
		public int Id { get; set; }
		public DateTime MemoDate { get; set; }
		public string UserName { get; set; }
		public string Note { get; set; }
		public string Edit { get; set; }
	}
}