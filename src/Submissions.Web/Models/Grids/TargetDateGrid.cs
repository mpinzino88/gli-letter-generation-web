﻿using Submissions.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;

namespace Submissions.Web.Models.Grids
{
	using Trirand.Web.Mvc;

	public class TargetDateGrid
	{
		public TargetDateGrid()
		{
			this.Grid = new JQGrid
			{
				ID = "TargetDateGrid",
				Columns = LoadColumns(),
				DataUrl = new UrlHelper(((MvcHandler)HttpContext.Current.Handler).RequestContext).Action("LoadTargetDateHistoryGrid", "GridsAjax", new { Area = "" }),
				AutoWidth = true,
				Width = Unit.Percentage(100),
				Height = Unit.Percentage(100),
				ToolBarSettings = new ToolBarSettings
				{
					ShowRefreshButton = true,
					ToolBarPosition = ToolBarPosition.Top,
					ShowSearchToolBar = true,
				},
				PagerSettings = new PagerSettings
				{
					PageSize = 100,
					PageSizeOptions = "[]"
				},
				AppearanceSettings = new AppearanceSettings
				{
					AlternateRowBackground = true,
					HighlightRowsOnHover = true,
					ShowFooter = true,
					Caption = "Notes"
				},
				SortSettings = new SortSettings
				{
					InitialSortColumn = "ModifiedDate",
					InitialSortDirection = SortDirection.Desc
				},
				ClientSideEvents = new ClientSideEvents
				{
					BeforeAjaxRequest = "targetDateGridBeforeAjaxRequest"
				},
				RenderingMode = RenderingMode.Optimized
			};
		}

		public JQGrid Grid { get; set; }

		private List<JQGridColumn> LoadColumns()
		{
			var columns = new List<JQGridColumn>();

			columns.Add(new JQGridColumn
			{
				DataField = "Id",
				PrimaryKey = true,
				Editable = false,
				Visible = false
			});

			columns.Add(new JQGridColumn
			{
				DataField = "IsDeleted",
				Editable = false,
				Visible = false
			});


			columns.Add(new JQGridColumn
			{
				DataField = "ModifiedDate",
				HeaderText = "Modified Date",
				DataFormatString = "{0:d}",
				DataType = typeof(DateTime),
				Width = 100,
				Searchable = false
			});

			columns.Add(new JQGridColumn
			{
				DataField = "ModifiedBy",
				HeaderText = "Modified By",
				DataType = typeof(string),
				Width = 130,
				Searchable = false
			});

			columns.Add(new JQGridColumn
			{
				DataField = "TargetDate",
				HeaderText = "Target Date",
				DataType = typeof(DateTime),
				DataFormatString = "{0:d}",
				Width = 80,
				Searchable = false,
			});

			columns.Add(new JQGridColumn
			{
				DataField = "Reason",
				HeaderText = "Reason",
				DataType = typeof(string),
				Width = 150,
				Searchable = false,
				
			});

			columns.Add(new JQGridColumn
			{
				DataField = "Note",
				HeaderText = "Note",
				DataType = typeof(string),
				Width = 200,
				Searchable = false,
			});

			return columns;
		}
	}

	public class QuickTargetDateGrid : TargetDateGrid
	{
		public QuickTargetDateGrid()
		{
			this.Grid.ID = "QuickDetailTargetDate";
			
			this.Grid.ShrinkToFit = false;
			this.Grid.AutoWidth = true;

			this.Grid.AppearanceSettings = new AppearanceSettings
			{
				AlternateRowBackground = true,
				HighlightRowsOnHover = true,
				ShowFooter = false,
				Caption = "Target Date Notes"
			};
			this.Grid.ToolBarSettings = new ToolBarSettings
			{
				ShowRefreshButton = false,
				ToolBarPosition = ToolBarPosition.Top,
				ShowSearchToolBar = false
			};
		}
	}

	public class TargetDateGridRecord
	{
		public int Id { get; set; }
		public DateTime? ModifiedDate { get; set; }
		public string ModifiedBy { get; set; }
		public DateTime? TargetDate { get; set; }
		public string Reason { get; set; }
		public string Note { get; set; }
		public Boolean IsDeleted { get; set; }
	}
}