﻿using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;

namespace Submissions.Web.Models.Grids.Protrack
{
	using System;
	using Trirand.Web.Mvc;

	public class ProjectReviewGrid
	{
		public ProjectReviewGrid()
		{
			this.Grid = new JQGrid
			{
				ID = "ProjectReviewGrid",
				Columns = InitializeAllColumns(),
				DataUrl = new UrlHelper(((MvcHandler)HttpContext.Current.Handler).RequestContext).Action("LoadProjectReviewGrid", "GridsAjax", new { Area = "" }),
				AutoWidth = true,
				Width = Unit.Percentage(100),
				Height = Unit.Percentage(100),
				ToolBarSettings = new ToolBarSettings
				{
					ShowRefreshButton = true,
					ShowSearchToolBar = true,
					ToolBarPosition = ToolBarPosition.Top
				},
				PagerSettings = new PagerSettings
				{
					PageSize = 100,
					PageSizeOptions = "[]"
				},
				AppearanceSettings = new AppearanceSettings
				{
					AlternateRowBackground = true,
					HighlightRowsOnHover = true,
					ShowFooter = false
				},
				ClientSideEvents = new ClientSideEvents
				{
					BeforeAjaxRequest = "gridBeforeAjaxRequest"
				},
				SortSettings = new SortSettings
				{
					InitialSortColumn = "AddDate",
					InitialSortDirection = SortDirection.Desc,
				},
				RenderingMode = RenderingMode.Optimized
			};
		}

		public JQGrid Grid { get; set; }

		private List<JQGridColumn> InitializeAllColumns()
		{
			var columns = new List<JQGridColumn>();

			columns.Add(new JQGridColumn
			{
				DataField = "Id",
				PrimaryKey = true,
				Editable = false,
				Visible = false
			});
			columns.Add(new JQGridColumn
			{
				DataField = "ProjectId",
				Visible = false
			});
			columns.Add(new JQGridColumn
			{
				DataField = "Status",
				HeaderText = "Status",
				DataType = typeof(string),
				SearchToolBarOperation = SearchOperation.BeginsWith,
				Width = 60
			});

			columns.Add(new JQGridColumn
			{
				DataField = "FileNumber",
				HeaderText = "File Number",
				Formatter = new CustomFormatter { FormatFunction = "ProjectDetailLink" },
				DataType = typeof(string),
				SearchToolBarOperation = SearchOperation.BeginsWith,
				Width = 80
			});
			columns.Add(new JQGridColumn
			{
				DataField = "Description",
				HeaderText = "Description",
				DataType = typeof(string),
				SearchToolBarOperation = SearchOperation.Contains,
				Width = 80
			});
			columns.Add(new JQGridColumn
			{
				DataField = "Department",
				HeaderText = "Department",
				DataType = typeof(string),
				SearchToolBarOperation = SearchOperation.BeginsWith,
				Width = 40
			});
			columns.Add(new JQGridColumn
			{
				DataField = "Assignee",
				HeaderText = "Assignee(s)",
				DataType = typeof(string),
				Formatter = new CustomFormatter { FormatFunction = "nameColumnFormat" },
				SearchToolBarOperation = SearchOperation.Contains,
				Width = 55
			});
			columns.Add(new JQGridColumn
			{
				DataField = "Reviewer",
				HeaderText = "Reviewer(s)",
				DataType = typeof(string),
				Formatter = new CustomFormatter { FormatFunction = "nameColumnFormat" },
				SearchToolBarOperation = SearchOperation.Contains,
				Width = 55
			});
			columns.Add(new JQGridColumn
			{
				DataField = "Letterwriter",
				HeaderText = "Letterwriter(s)",
				DataType = typeof(string),
				Formatter = new CustomFormatter { FormatFunction = "nameColumnFormat" },
				SearchToolBarOperation = SearchOperation.Contains,
				Width = 55,
				Visible = false
			});
			columns.Add(new JQGridColumn
			{
				DataField = "AddDate",
				HeaderText = "Add Date",
				DataFormatString = "{0:d}",
				DataType = typeof(DateTime),
				Width = 50
			});
			columns.Add(new JQGridColumn
			{
				DataField = "CompleteDate",
				HeaderText = "Completion Date",
				DataFormatString = "{0:d}",
				DataType = typeof(DateTime),
				Width = 50
			});
			columns.Add(new JQGridColumn
			{
				DataField = "TargetDate",
				HeaderText = "Target Date",
				DataFormatString = "{0:d}",
				DataType = typeof(DateTime),
				Width = 50
			});
			columns.Add(new JQGridColumn
			{
				DataField = "ViewDetails",
				HeaderText = " ",
				Formatter = new CustomFormatter { FormatFunction = "ViewDetailsLink" },
				Searchable = false,
				Sortable = false,
				Width = 60,
				TextAlign = TextAlign.Right,
				CssClass = "padright"
			});

			return columns;
		}

		public class ProjectReviewGridRecord
		{
			public int Id { get; set; }
			public int ProjectId { get; set; }
			public string FileNumber { get; set; }
			public string Description { get; set; }
			public string Department { get; set; }
			public string Status { get; set; }
			public string Assignee { get; set; }
			public string Letterwriter { get; set; }
			public string Reviewer { get; set; }
			public DateTime? AddDate { get; set; }
			public DateTime? CompleteDate { get; set; }
			public DateTime? TargetDate { get; set; }
			public string ViewDetails { get; set; }

		}
	}
}