﻿using Submissions.Core.Models.WorkloadManagerService;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Submissions.Web.Models.Grids.Protrack
{
	using Trirand.Web.Mvc;

	public class ResourceFinderGrid
	{
		public ResourceFinderGrid()
		{
			this.Grid = new JQGrid
			{
				ID = "ResourceFinderGrid",
				Columns = InitializeAllColumns(),
				SortSettings = new SortSettings
				{
					InitialSortColumn = "AssignedEstCompleteDate, ManagerName",
					InitialSortDirectionString = "asc, asc"
				}
			};
		}

		public JQGrid Grid { get; set; }

		private List<JQGridColumn> InitializeAllColumns()
		{
			var allColumns = new List<JQGridColumn>();

			allColumns.Add(new JQGridColumn
			{
				DataField = "UserId",
				PrimaryKey = true,
				Editable = false,
				Visible = false
			});

			allColumns.Add(new JQGridColumn
			{
				DataField = "Unavailable",
				Visible = false
			});

			allColumns.Add(new JQGridColumn
			{
				DataField = "UserId",
				HeaderText = " ",
				DataType = typeof(string),
				Width = 70,
				Formatter = new CustomFormatter { FormatFunction = "cellReplace1" }
			});

			allColumns.Add(new JQGridColumn
			{
				DataField = "UserName",
				HeaderText = "Resource",
				DataType = typeof(string),
				Width = 150
			});

			allColumns.Add(new JQGridColumn
			{
				DataField = "AssignedHours",
				HeaderText = "Assigned Hrs",
				DataType = typeof(decimal),
				Formatter = new NumberFormatter { DecimalPlaces = 2 },
				Width = 100
			});

			allColumns.Add(new JQGridColumn
			{
				DataField = "AssignedEstCompleteDate",
				HeaderText = "Est. Completion",
				DataType = typeof(string),
				Width = 90
			});

			allColumns.Add(new JQGridColumn
			{
				DataField = "AssignedAndOHHours",
				HeaderText = "Assigned + OH Hrs",
				DataType = typeof(decimal),
				Formatter = new NumberFormatter { DecimalPlaces = 2 },
				Width = 100
			});

			allColumns.Add(new JQGridColumn
			{
				DataField = "AssignedAndOHEstCompleteDate",
				HeaderText = "Est. Completion",
				DataType = typeof(string),
				Width = 110
			});

			allColumns.Add(new JQGridColumn
			{
				DataField = "ManagerName",
				HeaderText = "Manager",
				DataType = typeof(string),
				Width = 150
			});

			allColumns.Add(new JQGridColumn
			{
				DataField = "ManagerOffice",
				HeaderText = "Manager Office",
				DataType = typeof(string),
				Width = 90
			});

			allColumns.Add(new JQGridColumn
			{
				DataField = "ManagerMobile",
				HeaderText = "Manager Mobile",
				DataType = typeof(string),
				Width = 90
			});

			return allColumns;
		}
	}

	public class ResourceFinderGridRecord
	{
		public ResourceFinderGridRecord()
		{
			this.CalendarStartDate = DateTime.Now.Date;
		}

		public DateTime CalendarStartDate { get; set; }
		public IList<DTORequest> DTORequests { get; set; }
		public IList<Holiday> Holidays { get; set; }
		public IList<WorkdaySchedule> WorkdaySchedules { get; set; }
		public int UserId { get; set; }
		public string UserName { get; set; }
		public int UserLocationId { get; set; }
		public decimal UserWorkdayHours { get; set; }
		public int? ManagerId { get; set; }
		public string ManagerName { get; set; }
		public string ManagerOffice { get; set; }
		public string ManagerMobile { get; set; }
		public decimal AssignedHours { get; set; }
		public decimal AssignedAndOHHours { get; set; }
		public DateTime AssignedEstCompleteDate
		{
			get
			{
				return GetEstimateCompleteDate("Assigned", this.CalendarStartDate, this.AssignedHours, this);
			}
		}
		public DateTime AssignedAndOHEstCompleteDate
		{
			get
			{
				var startDate = this.AssignedEstCompleteDate;
				var ohHours = this.AssignedAndOHHours - this.AssignedHours;

				if (this.AssignedHours > 0 && ohHours > 0)
				{
					startDate = startDate.AddDays(1);
				}

				return GetEstimateCompleteDate("AssignedAndOH", startDate, ohHours, this);
			}
		}
		public int? UnavailableId { get; set; }
		public DateTime? UnavailableStartDate { get; set; }

		private DateTime? _unavailableEndDate;
		public DateTime? UnavailableEndDate
		{
			get
			{
				return _unavailableEndDate ?? this.CalendarStartDate.AddYears(1);
			}
			set
			{
				_unavailableEndDate = value;
			}
		}

		public bool Unavailable
		{
			get
			{
				if (this.UnavailableId != null)
				{
					if ((this.ManagerId != GLISession.User.Id) && (this.UnavailableStartDate <= this.CalendarStartDate && this.UnavailableEndDate >= this.CalendarStartDate))
					{
						return true;
					}
				}

				return false;
			}
		}

		public string InterofficeAssignment { get; set; }

		private static DateTime GetEstimateCompleteDate(string calculationType, DateTime startDate, decimal scheduledHours, ResourceFinderGridRecord record)
		{
			var day = startDate;

			Holiday holiday = null;
			bool isUnavailable = false;
			decimal remainingWorkdayHours = 0;
			decimal adjustedHours = 0;

			while (scheduledHours > 0)
			{
				if (remainingWorkdayHours == 0)
				{
					var dtoRequest = record.DTORequests.Where(x => x.Date == day).SingleOrDefault();
					var dtoHours = Convert.ToDecimal(dtoRequest == null ? 0 : dtoRequest.Hours);
					holiday = record.Holidays.Where(x => x.Date == day).SingleOrDefault();
					isUnavailable = (!string.IsNullOrEmpty(record.InterofficeAssignment) && record.UnavailableStartDate <= day && record.UnavailableEndDate >= day) ? true : false;
					remainingWorkdayHours = record.WorkdaySchedules.Where(x => x.DayOfWeek == day.DayOfWeek).Select(x => x.Hours).Single();
					remainingWorkdayHours -= Math.Min(dtoHours, remainingWorkdayHours);
				}

				if (holiday != null)
				{
					remainingWorkdayHours = 0;
				}
				else if (isUnavailable)
				{
					remainingWorkdayHours = 0;
				}
				else
				{
					adjustedHours = Math.Min(scheduledHours, remainingWorkdayHours);
					scheduledHours -= adjustedHours;
					remainingWorkdayHours -= adjustedHours;
				}

				if (scheduledHours > 0 && remainingWorkdayHours == 0)
				{
					day = day.AddDays(1);
				}
			}

			return day;
		}
	}
}