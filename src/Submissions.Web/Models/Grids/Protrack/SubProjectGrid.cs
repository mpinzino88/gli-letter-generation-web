﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;

namespace Submissions.Web.Models.Grids.Protrack
{
	using Submissions.Common;
	using Trirand.Web.Mvc;
	public class SubProjectGrid
	{
		public JQGrid Grid { get; set; }
		public SubProjectGrid()
		{
			this.Grid = new JQGrid
			{
				ID = "SubProjectsGrid",
				Columns = InitializeAllColumns(),
				DataUrl = new UrlHelper(((MvcHandler)HttpContext.Current.Handler).RequestContext).Action("LoadSubProjectsGrid", "GridsAjax", new { Area = "" }),
				AutoWidth = false,
				Width = Unit.Percentage(100),
				Height = Unit.Percentage(100),
				ToolBarSettings = new ToolBarSettings
				{
					ShowRefreshButton = true,
					ToolBarPosition = ToolBarPosition.Top,
					ShowSearchToolBar = true
				},
				PagerSettings = new Trirand.Web.Mvc.PagerSettings
				{
					PageSize = 100,
					PageSizeOptions = "[]"
				},
				AppearanceSettings = new AppearanceSettings
				{
					AlternateRowBackground = true,
					HighlightRowsOnHover = true,
					ShowFooter = false
				},
				SortSettings = new SortSettings
				{
					InitialSortColumn = "ProjectName",
					InitialSortDirection = SortDirection.Asc
				},
				ClientSideEvents = new ClientSideEvents
				{
					BeforeAjaxRequest = "subProjectGridBeforeAjaxRequest"
				},
				RenderingMode = RenderingMode.Optimized
			};
		}

		private List<JQGridColumn> InitializeAllColumns()
		{
			var allColumns = new List<JQGridColumn>();
			allColumns.Add(new JQGridColumn
			{
				DataField = "SubProjectId",
				PrimaryKey = true,
				Editable = false,
				Visible = false
			});
			allColumns.Add(new JQGridColumn
			{
				DataField = "ProjectId",
				Editable = false,
				Visible = false
			});
			allColumns.Add(new JQGridColumn
			{
				DataField = "ProjectNameCopy",
				Editable = false,
				Visible = false
			});
			allColumns.Add(new JQGridColumn
			{
				DataField = "ProjectName",
				HeaderText = "File Number",
				DataType = typeof(string),
				Searchable = true,
				SearchToolBarOperation = SearchOperation.BeginsWith,
				Editable = false,
				Visible = true
			});
			allColumns.Add(new JQGridColumn
			{
				DataField = "Type",
				HeaderText = "Request Type",
				DataType = typeof(string),
				Searchable = true,
				Editable = false,
				Visible = true
			});
			allColumns.Add(new JQGridColumn
			{
				DataField = "AnalysisType",
				HeaderText = "Analysis Type",
				DataType = typeof(string),
				Searchable = true,
				Editable = false,
				Visible = true
			});
			allColumns.Add(new JQGridColumn
			{
				DataField = "GameType",
				HeaderText = "Game Type",
				DataType = typeof(string),
				Searchable = true,
				Editable = false,
				Visible = true
			});
			allColumns.Add(new JQGridColumn
			{
				DataField = "Assignee",
				HeaderText = "Assignee",
				DataType = typeof(string),
				Searchable = true,
				Editable = false,
				Visible = true
			});
			allColumns.Add(new JQGridColumn
			{
				DataField = "Status",
				HeaderText = "Status",
				DataType = typeof(string),
				Searchable = true,
				Editable = false,
				Visible = true
			});
			allColumns.Add(new JQGridColumn
			{
				DataField = "RequestedCompletion",
				HeaderText = "Requested Completion",
				DataFormatString = "{0:d}",
				DataType = typeof(DateTime),
				Searchable = true,
				Editable = false,
				Visible = true
			});
			allColumns.Add(new JQGridColumn
			{
				DataField = "TargetDate",
				HeaderText = "Task Target Date",
				DataFormatString = "{0:d}",
				DataType = typeof(DateTime),
				Searchable = true,
				Editable = false,
				Visible = true
			});
			return allColumns;
		}
	}

	public class SubProjectGridRecord
	{
		public int SubProjectId { get; set; }
		public int ProjectId { get; set; }
		public int TypeId { get; set; }
		public string Type { get; set; }
		public string ProgramType { get; set; }
		public string TestURL { get; set; }
		public string InternalURL { get; set; }
		public string BackOfficeURL { get; set; }
		public string TestHarness { get; set; }
		public string MARngType { get; set; }
		public string OutputsRequired { get; set; }
		public string ENForceTool { get; set; }
		public string ENAdditionalDeviceRequest { get; set; }
		public string AUTimePeriod { get; set; }
		public string Note { get; set; }
		public int? DeliveryMechanismId { get; set; }
		public string DeliveryMechanism { get; set; }
		public Mode Mode { get; set; }
		public string ProjectName { get; set; }
		public string ProjectNameCopy { get; set; }
		public string AnalysisType { get; set; }
		public int AnalysisTypeId { get; set; }
		public string GameType { get; set; }
		public DateTime RequestedCompletion { get; set; }
		public SubProjectFirstTaskGridRecord FirstTask { get; set; }
		public int? AssigneeId
		{
			get
			{
				return this.FirstTask == null ? null : (int?)this.FirstTask.AssigneeId;
			}
		}
		public string Assignee
		{
			get
			{
				return this.FirstTask == null ? null : this.FirstTask.Assignee;
			}
		}
		public DateTime? TargetDate
		{
			get
			{
				return this.FirstTask == null ? null : (DateTime?)this.FirstTask.TargetDate;
			}
		}
		public string Status
		{
			get
			{
				return this.FirstTask == null ? SubProjectStatus.Unassigned.GetEnumDescription() : this.FirstTask.Status;
			}
		}
	}

	public class SubProjectFirstTaskGridRecord
	{
		public int Id { get; set; }
		public DateTime TargetDate { get; set; }
		public int AssigneeId { get; set; }
		public string Assignee { get; set; }
		public string Status { get; set; }
	}
}