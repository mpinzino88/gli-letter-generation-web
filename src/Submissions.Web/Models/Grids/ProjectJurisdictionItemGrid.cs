﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;

namespace Submissions.Web.Models.Grids
{
	using Trirand.Web.Mvc;

	public class ProjectJurisdictionItemGrid
	{
		public ProjectJurisdictionItemGrid()
		{
			this.Grid = new JQGrid
			{
				ID = "ProjectJurisdictionItemGrid",
				Columns = LoadColumns(),
				DataUrl = new UrlHelper(((MvcHandler)HttpContext.Current.Handler).RequestContext).Action("LoadProjectJurisdictionItemGrid", "GridsAjax", new { Area = "" }),
				AutoWidth = true,
				ShrinkToFit = false,
				Width = Unit.Percentage(100),
				Height = Unit.Percentage(100),
				ToolBarSettings = new ToolBarSettings
				{
					ShowRefreshButton = true,
					ToolBarPosition = ToolBarPosition.TopAndBottom,
					ShowSearchToolBar = false
				},
				PagerSettings = new PagerSettings
				{
					PageSize = 100,
					PageSizeOptions = "[]"
				},
				AppearanceSettings = new AppearanceSettings
				{
					AlternateRowBackground = true,
					HighlightRowsOnHover = true,
					ShowFooter = false,
					Caption = "Jurisdictions"
				},
				SortSettings = new SortSettings
				{
					InitialSortColumn = "Jurisdiction",
					InitialSortDirection = SortDirection.Asc
				},
				ClientSideEvents = new ClientSideEvents
				{
					BeforeAjaxRequest = "projectJurisdictionItemGridBeforeAjaxRequest"
				},
				RenderingMode = RenderingMode.Optimized
			};
		}

		public JQGrid Grid { get; set; }

		private List<JQGridColumn> LoadColumns()
		{
			var columns = new List<JQGridColumn>();
			//quick detail has reorder of this columns, so pls check quick detail after adding new columns
			columns.Add(new JQGridColumn
			{
				DataField = "JurId",
				PrimaryKey = true,
				Editable = false,
				Visible = false
			});

			columns.Add(new JQGridColumn
			{
				DataField = "FileNumber",
				HeaderText = "File Number",
				DataType = typeof(string),
				Width = 120
			});

			columns.Add(new JQGridColumn
			{
				DataField = "IDNumber",
				HeaderText = "ID Number",
				DataType = typeof(string),
				Width = 140
			});

			columns.Add(new JQGridColumn
			{
				DataField = "Game",
				HeaderText = "Game",
				DataType = typeof(string),
				Width = 160
			});

			columns.Add(new JQGridColumn
			{
				DataField = "Version",
				HeaderText = "Version",
				DataType = typeof(string),
				Width = 80
			});

			columns.Add(new JQGridColumn
			{
				DataField = "DateCode",
				HeaderText = "Date Code",
				DataType = typeof(string),
				Width = 70
			});

			columns.Add(new JQGridColumn
			{
				DataField = "Status",
				HeaderText = "Status",
				DataType = typeof(string),
				Width = 40
			});

			columns.Add(new JQGridColumn
			{
				DataField = "Jurisdiction",
				HeaderText = "Jurisdiction",
				DataType = typeof(string),
				Width = 120
			});

			columns.Add(new JQGridColumn
			{
				DataField = "JurNum",
				HeaderText = "Jur #",
				DataType = typeof(string),
				Width = 40,
				Visible = true
			});

			columns.Add(new JQGridColumn
			{
				DataField = "ReceiveDate",
				HeaderText = "Received",
				DataFormatString = "{0:d}",
				DataType = typeof(DateTime),
				Width = 70
			});

			columns.Add(new JQGridColumn
			{
				DataField = "JurisdictionType",
				HeaderText = "Jur Status",
				DataType = typeof(string),
				Width = 70
			});

			columns.Add(new JQGridColumn
			{
				DataField = "ContractType",
				HeaderText = "Contract Type",
				DataType = typeof(string),
				Width = 90
			});

			columns.Add(new JQGridColumn
			{
				DataField = "TestingType",
				HeaderText = "Testing Type",
				DataType = typeof(string),
				Width = 80
			});


			columns.Add(new JQGridColumn
			{
				DataField = "TargetDate",
				HeaderText = "Target Date",
				DataFormatString = "{0:d}",
				DataType = typeof(DateTime),
				Width = 70,
				Formatter = new CustomFormatter { FormatFunction = "viewJurTargetHistory" }
			});

			return columns;
		}
	}

	public class QuickProjectJurisdictionItemGrid : ProjectJurisdictionItemGrid
	{
		public QuickProjectJurisdictionItemGrid()
		{
			this.Grid.ID = "QuickDetailJurisdictions";
			this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.DataField == "Status")].Visible = false;
			this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.DataField == "ContractType")].Visible = false;
			this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.DataField == "TestingType")].Visible = false;
			this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.DataField == "DateCode")].Visible = false;
			this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.DataField == "TargetDate")].Formatter = null;

			this.Grid.ShrinkToFit = false;
			this.Grid.AutoWidth = true;

			this.Grid.AppearanceSettings = new AppearanceSettings
			{
				AlternateRowBackground = true,
				HighlightRowsOnHover = true,
				ShowFooter = false,
				Caption = "Jurisdictions"
			};
			this.Grid.ToolBarSettings = new ToolBarSettings
			{
				ShowRefreshButton = false,
				ToolBarPosition = ToolBarPosition.Top,
				ShowSearchToolBar = false
			};
		}
	}

	public class ManageBundleProjectJurisdictionItemGrid : ProjectJurisdictionItemGrid
	{
		public ManageBundleProjectJurisdictionItemGrid()
		{
			this.Grid.ID = "ManageBundleJurisdictions";

			this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.DataField == "JurNum")].Visible = true;
			this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.DataField == "ContractType")].Visible = false;
			this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.DataField == "TestingType")].Visible = false;
			this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.DataField == "TargetDate")].Formatter = null;

			this.Grid.ShrinkToFit = false;
			this.Grid.AutoWidth = true;

			this.Grid.AppearanceSettings = new AppearanceSettings
			{
				AlternateRowBackground = true,
				HighlightRowsOnHover = true,
				ShowFooter = false,
				Caption = "Jurisdictions"
			};
			this.Grid.ToolBarSettings = new ToolBarSettings
			{
				ShowRefreshButton = false,
				ToolBarPosition = ToolBarPosition.Top,
				ShowSearchToolBar = false
			};
		}
	}

	//This will show as child grid
	public class ChildProjectJurisdictionItemGrid : ProjectJurisdictionItemGrid
	{
		public ChildProjectJurisdictionItemGrid()
		{
			this.Grid.ID = "ChildDetailJurisdictions";
			this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.DataField == "Status")].Visible = false;
			this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.DataField == "ContractType")].Visible = false;
			this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.DataField == "TestingType")].Visible = false;
			this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.DataField == "TargetDate")].Formatter = null;
			this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.DataField == "JurNum")].Visible = true;

			this.Grid.ShrinkToFit = false;
			this.Grid.AutoWidth = true;

			this.Grid.AppearanceSettings = new AppearanceSettings
			{
				AlternateRowBackground = true,
				HighlightRowsOnHover = true,
				ShowFooter = false,
				Caption = "Project Jurisdictions"
			};
			this.Grid.ToolBarSettings = new ToolBarSettings
			{
				ShowRefreshButton = false,
				ToolBarPosition = ToolBarPosition.Top,
				ShowSearchToolBar = false
			};

			this.Grid.Height = Unit.Percentage(15);
			this.Grid.RenderingMode = RenderingMode.Default;
			this.Grid.HierarchySettings.HierarchyMode = HierarchyMode.Child;

		}
	}

	public class ProjectJurisdictionItemGridRecord
	{
		public int JurId { get; set; }
		public int? SubId { get; set; }
		public string FileNumber { get; set; }
		public string IDNumber { get; set; }
		public string Game { get; set; }
		public string DateCode { get; set; }
		public string Position { get; set; }
		public string Version { get; set; }
		public string Status { get; set; }
		public string Jurisdiction { get; set; }
		public string JurNum { get; set; }
		public DateTime? ReceiveDate { get; set; }
		public string JurisdictionType { get; set; }
		public string ContractType { get; set; }
		public string TestingType { get; set; }
		public DateTime? TargetDate { get; set; }
	}

}