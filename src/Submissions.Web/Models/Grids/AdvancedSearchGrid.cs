﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;

namespace Submissions.Web.Models.Grids
{
	using Trirand.Web.Mvc;

	public class AdvancedSearchGrid
	{
		public AdvancedSearchGrid()
		{
			this.Grid = new JQGrid
			{
				ID = "AdvancedSearchGrid",
				Columns = LoadColumns(),
				DataUrl = new UrlHelper(((MvcHandler)HttpContext.Current.Handler).RequestContext).Action("LoadAdvancedSearchGrid", "GridsAjax", new { Area = "" }),
				AutoWidth = false,
				ShrinkToFit = false,
				Width = Unit.Percentage(100),
				Height = Unit.Percentage(100),
				ToolBarSettings = new ToolBarSettings
				{
					ShowRefreshButton = true,
					ToolBarPosition = ToolBarPosition.Top,
					ShowSearchToolBar = true,
				},
				PagerSettings = new PagerSettings
				{
					PageSize = 100,
					PageSizeOptions = "[]"
				},
				AppearanceSettings = new AppearanceSettings
				{
					AlternateRowBackground = true,
					HighlightRowsOnHover = true,
					ShowFooter = false
				},
				ClientSideEvents = new ClientSideEvents
				{
					BeforeAjaxRequest = "gridBeforeAjaxRequest"
				},
				RenderingMode = RenderingMode.Optimized
			};
		}

		public JQGrid Grid { get; set; }

		private List<JQGridColumn> LoadColumns()
		{
			var allColumns = new List<JQGridColumn>();

			allColumns.Add(new JQGridColumn
			{
				DataField = "Id",
				PrimaryKey = true,
				Editable = false,
				Visible = false
			});
			allColumns.Add(new JQGridColumn
			{
				DataField = "FileNumber",
				HeaderText = "File Number",
				DataType = typeof(string),
				Searchable = true,
				SearchToolBarOperation = SearchOperation.BeginsWith,
				Width = 150,
				Formatter = new CustomFormatter { FormatFunction = "fileNumberLink2" }
			});
			allColumns.Add(new JQGridColumn
			{
				DataField = "SubmitDate",
				HeaderText = "Submit Date",
				DataFormatString = "{0:d}",
				DataType = typeof(DateTime),
				Width = 100
			});
			allColumns.Add(new JQGridColumn
			{
				DataField = "ReceiveDate",
				HeaderText = "Receive Date",
				DataFormatString = "{0:d}",
				DataType = typeof(DateTime),
				Width = 100
			});
			allColumns.Add(new JQGridColumn
			{
				DataField = "Status",
				HeaderText = "Status",
				DataType = typeof(string),
				Searchable = true,
				SearchToolBarOperation = SearchOperation.BeginsWith,
				Width = 90
			});
			allColumns.Add(new JQGridColumn
			{
				DataField = "Lab",
				HeaderText = "Test Lab",
				DataType = typeof(string),
				Searchable = true,
				SearchToolBarOperation = SearchOperation.BeginsWith,
				SearchType = SearchType.AutoComplete,
				SearchControlID = "LabAutoComplete",
				Width = 100
			});
			allColumns.Add(new JQGridColumn
			{
				DataField = "CertificationLab",
				HeaderText = "Certification Lab",
				DataType = typeof(string),
				Searchable = true,
				SearchToolBarOperation = SearchOperation.BeginsWith,
				Width = 110
			});
			allColumns.Add(new JQGridColumn
			{
				DataField = "GameName",
				HeaderText = "Game Name",
				DataType = typeof(string),
				Searchable = true,
				SearchToolBarOperation = SearchOperation.BeginsWith,
				Width = 250
			});
			allColumns.Add(new JQGridColumn
			{
				DataField = "IdNumber",
				HeaderText = "ID Number",
				DataType = typeof(string),
				Searchable = true,
				SearchToolBarOperation = SearchOperation.BeginsWith,
				Width = 175
			});
			allColumns.Add(new JQGridColumn
			{
				DataField = "DateCode",
				HeaderText = "Date Code",
				DataType = typeof(string),
				Searchable = true,
				SearchToolBarOperation = SearchOperation.BeginsWith,
				Width = 175
			});
			allColumns.Add(new JQGridColumn
			{
				DataField = "Version",
				HeaderText = "Version",
				DataType = typeof(string),
				Searchable = true,
				SearchToolBarOperation = SearchOperation.BeginsWith,
				Width = 175
			});

			return allColumns;
		}
	}

	public class AdvancedSearchGridRecord
	{
		public int Id { get; set; }
		public string FileNumber { get; set; }
		public DateTime SubmitDate { get; set; }
		public DateTime ReceiveDate { get; set; }
		public string Status { get; set; }
		public string Lab { get; set; }
		public string CertificationLab { get; set; }
		public string GameName { get; set; }
		public string IdNumber { get; set; }
		public string DateCode { get; set; }
		public string Version { get; set; }
	}
}