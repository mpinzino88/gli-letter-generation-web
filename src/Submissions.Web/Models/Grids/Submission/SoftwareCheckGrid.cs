﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;

namespace Submissions.Web.Models.Grids.Submission
{
	using Trirand.Web.Mvc;

	public class SoftwareCheckGrid
	{
		public SoftwareCheckGrid()
		{
			Grid = new JQGrid
			{
				ID = "SoftwareCheckGrid",
				Columns = LoadColumns(),
				DataUrl = new UrlHelper(((MvcHandler)HttpContext.Current.Handler).RequestContext).Action("loadsoftwarecheckgrid", "gridsajax", new { area = "" }),
				AutoWidth = true,
				Width = Unit.Percentage(100),
				Height = Unit.Percentage(100),
				ToolBarSettings = new ToolBarSettings
				{
					ShowRefreshButton = true,
					ShowSearchToolBar = true,
					ToolBarPosition = ToolBarPosition.Top
				},
				PagerSettings = new PagerSettings
				{
					PageSize = 100,
					PageSizeOptions = "[]"
				},
				AppearanceSettings = new AppearanceSettings
				{
					AlternateRowBackground = true,
					HighlightRowsOnHover = true,
					ShowFooter = false
				},
				SortSettings = new SortSettings
				{
					InitialSortColumn = "Id",
					InitialSortDirection = SortDirection.Desc
				},
				ClientSideEvents = new ClientSideEvents
				{
					BeforeAjaxRequest = "gridBeforeAjaxRequest"
				},
				RenderingMode = RenderingMode.Optimized
			};
		}

		public JQGrid Grid { get; set; }

		private List<JQGridColumn> LoadColumns()
		{
			var columns = new List<JQGridColumn>();

			columns.Add(new JQGridColumn
			{
				DataField = "Id",
				PrimaryKey = true,
				Editable = false,
				Visible = false
			});

			columns.Add(new JQGridColumn
			{
				DataField = "ProjectId",
				Visible = false
			});

			columns.Add(new JQGridColumn
			{
				DataField = "FileNumber",
				HeaderText = "File Number",
				DataType = typeof(string),
				SearchToolBarOperation = SearchOperation.Contains,
				Formatter = new CustomFormatter { FormatFunction = "projectLink" },
				Width = 100
			});

			columns.Add(new JQGridColumn
			{
				DataField = "ShippingRecipientCode",
				HeaderText = "From/To",
				DataType = typeof(string),
				SearchToolBarOperation = SearchOperation.Contains,
				Width = 80
			});

			columns.Add(new JQGridColumn
			{
				DataField = "ProcessDate",
				HeaderText = "Ship/Receive Date",
				DataFormatString = "{0:d}",
				DataType = typeof(DateTime),
				Width = 80,
				Sortable = false
			});

			columns.Add(new JQGridColumn
			{
				DataField = "Memo",
				HeaderText = "Memo",
				DataType = typeof(string),
				SearchToolBarOperation = SearchOperation.Contains,
				Width = 150
			});

			columns.Add(new JQGridColumn
			{
				DataField = "SoftwareCheckUser",
				HeaderText = "Software Checker",
				DataType = typeof(string),
				SearchToolBarOperation = SearchOperation.Contains,
				Width = 120
			});

			columns.Add(new JQGridColumn
			{
				DataField = "SoftwareWithEngineering",
				HeaderText = "Software w/ Engineering",
				DataType = typeof(string),
				SearchToolBarOperation = SearchOperation.IsEqualTo,
				Width = 100
			});

			columns.Add(new JQGridColumn
			{
				DataField = "Actions",
				HeaderText = " ",
				Formatter = new CustomFormatter { FormatFunction = "editActionsLink" },
				Searchable = false,
				Sortable = false,
				Width = 40,
				TextAlign = TextAlign.Right,
				CssClass = "padright"
			});

			return columns;
		}
	}

	public class SoftwareCheckGridRecord
	{
		public SoftwareCheckGridRecord()
		{
			this.InitialProject = new SoftwareCheckInitialProjectGridRecord();
		}

		public int Id { get; set; }
		public int ProjectId
		{
			get
			{
				return this.InitialProject.Id;
			}
		}
		public string FileNumber { get; set; }
		public string ShippingRecipientCode { get; set; }
		public string ProcessDateString { get; set; }
		public DateTime? ProcessDate
		{
			get
			{
				DateTime d;
				return DateTime.TryParse(this.ProcessDateString, out d) ? d : (DateTime?)null;
			}
		}
		public string Memo { get; set; }
		public string SoftwareCheckUser { get; set; }
		public string SoftwareWithEngineering { get; set; }
		public string Actions { get; set; }
		public SoftwareCheckInitialProjectGridRecord InitialProject { get; set; }
	}

	public class SoftwareCheckInitialProjectGridRecord
	{
		public int Id { get; set; }
	}
}