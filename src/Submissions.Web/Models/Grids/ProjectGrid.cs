﻿using EF.Submission;
using Submissions.Common;
using Submissions.Common.JQGrid;
using Submissions.Common.Linq.Dynamic;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;

namespace Submissions.Web.Models.Grids
{
	using Trirand.Web.Mvc;

	public class ProjectGrid : IDynamicGrid
	{
		private const string gridId = "ProjectGrid";

		public ProjectGrid()
		{
			this.AllColumns = GetAllReqColumns();
			this.AllColumns.AddRange(GetAllColumns());

			this.Grid = new JQGrid
			{
				ID = gridId,
				Columns = this.AllColumns.Cast<JQGridColumn>().ToList(),
				DataUrl = new UrlHelper(((MvcHandler)HttpContext.Current.Handler).RequestContext).Action("LoadProjectGrid", "GridsAjax", new { Area = "" }),
				AutoWidth = false,
				ShrinkToFit = false,
				Width = Unit.Percentage(100),
				Height = Unit.Percentage(100),
				ToolBarSettings = new ToolBarSettings
				{
					ShowRefreshButton = true,
					ToolBarPosition = ToolBarPosition.Top,
					ShowSearchToolBar = true
				},
				PagerSettings = new PagerSettings
				{
					//PageSize = 100,
					PageSize = 4000,
					PageSizeOptions = "[]"
				},
				AppearanceSettings = new AppearanceSettings
				{
					AlternateRowBackground = true,
					HighlightRowsOnHover = true,
					ShowFooter = false
				},
				SortSettings = new SortSettings
				{
					InitialSortColumn = "StatusSort",
					InitialSortDirection = SortDirection.Asc
				},
				ClientSideEvents = new ClientSideEvents
				{
					BeforeAjaxRequest = "gridBeforeAjaxRequest"
				},
				RenderingMode = RenderingMode.Optimized
			};
		}

		public JQGrid Grid { get; set; }
		//public string SelColumns { set; get; }
		public List<JQGridColumnDynamic> AllColumns { private set; get; }
		public List<JQGridColumnDynamic> SelectedColumns { private set; get; }

		public string GetColumnsDynamicLinqFormat()
		{
			var selectFields = this.SelectedColumns;
			var selectStatement = new StringBuilder();
			selectStatement.Append("new (");

			foreach (var col in selectFields)
			{
				selectStatement.AppendFormat("{0} As {1},", col.SqlSelectName, col.DataField);
			}

			selectStatement.Length--;
			selectStatement.Append(")");

			return selectStatement.ToString();
		}

		public void SetColumns(string savedColumns)
		{
			var jqColumns = new List<JQGridColumnDynamic>();

			if (savedColumns == null)
			{
				jqColumns = this.AllColumns;
			}
			else
			{
				var widgetId = this.Grid.ID.Replace(gridId, "");
				var savedColumnsList = new List<string>(savedColumns.Replace("\"", "").Replace("[", "").Replace("]", "").Split(','));
				var hiddenColumns = this.AllColumns.Where(x => x.Visible == false).ToList();

				jqColumns.AddRange(hiddenColumns);
				foreach (var col in savedColumnsList)
				{
					var column = this.AllColumns.Where(x => x.DataField == col).SingleOrDefault();
					if (column != null)
					{
						column.SearchControlID += widgetId;
						jqColumns.Add(column);
					}
				}
			}

			this.Grid.Columns = jqColumns.Cast<JQGridColumn>().ToList();
			this.SelectedColumns = jqColumns;
		}

		public void InitializeSelColumns(string selColumns = null, string gridType = "")
		{
			this.AllColumns = null;
			selColumns = selColumns.Replace("\"", "").Replace("[", "").Replace("]", "");
			this.AllColumns = GetAllReqColumns();
			if (selColumns != null)
			{
				this.AllColumns.AddRange(GetAllDisplayColumns(selColumns));
			}
			else
			{
				this.AllColumns.AddRange(GetAllColumns());
			}
			this.Grid.Columns = this.AllColumns.Cast<JQGridColumn>().ToList();

			if (gridType == ProtrackGridType.Glimpse.ToString())
			{
				if (this.Grid.Columns.Any(x => x.DataField == "Status"))
				{
					this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.DataField == "Status")].Formatter = new CustomFormatter { FormatFunction = "statusFormat" };
				}
				if (this.Grid.Columns.Any(x => x.DataField == "Tasks"))
				{
					this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.DataField == "Tasks")].Formatter = new CustomFormatter { FormatFunction = "tasksFormat" };
				}
			}

			this.Grid.DataUrl = new UrlHelper(((MvcHandler)HttpContext.Current.Handler).RequestContext).Action("LoadProjectGrid", "GridsAjax", new { SelColumns = this.GetSelCol(), Area = "" });
		}

		private string SetDynamicLinqColumns(string selColumns = null)
		{
			selColumns = selColumns.Replace("\"", "").Replace("[", "").Replace("]", "");
			return selColumns;
		}

		private string GetSelCol()
		{
			//to get proper columns
			var selColName = this.AllColumns.Select(x => x.DataField).ToList();
			var colName = String.Join(",", selColName);

			return colName;
		}

		public List<ColumnChooserColumn> GetAllColumnChooserDisplayNames(string DatabaseField)
		{
			var cColumns = new List<ColumnChooserColumn>();

			var DatabaseFields = DatabaseField.Replace("\"", "").Replace("[", "").Replace("]", "").Split(',');

			var allAvailable = GetAllColumns();

			//jqColumns.AddRange(hiddenColumns);
			foreach (var col in DatabaseFields)
			{
				var displayName = this.AllColumns.Where(x => x.DataField == col).Select(x => x.HeaderText).SingleOrDefault();
				if (this.BPDataColumns().Contains(col))
				{
					displayName += " <font class='text-info'>(Business Portal Data)</font>";
				}

				cColumns.Add(new ColumnChooserColumn()
				{
					DatabaseName = col,
					DisplayName = displayName
				});
			}

			return cColumns;
		}

		//To categorize data
		private IList<string> BPDataColumns()
		{
			return new List<string>
			{
				"EstHours",
				"ActHours",
				"NoCharge",
				"NonBillable",
				"BudgetPercentage",
				"UnExpHrs",
				"MathEst",
				"MathAct",
				"MathPen"
			};
		}

		//This columns are Id,indicator, group purpose
		public List<JQGridColumnDynamic> GetAllReqColumns()
		{
			var allColumns = new List<JQGridColumnDynamic>();

			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "Id",
				PrimaryKey = true,
				Visible = false
			});

			//This column is used for Favorite for Porject list and Project Note in Project Accept
			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "",
				HeaderText = "",
				Searchable = false,
				Sortable = true,

				Width = 15,
				TextAlign = TextAlign.Center
			});

			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "StatusSort",
				DataType = typeof(string),
				Visible = false
			});

			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "QAStatusSort",
				DataType = typeof(int),
				Visible = false
			});

			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "IsRush",
				DataType = typeof(int),
				Visible = false
			});

			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "IsTransfer",
				DataType = typeof(int),
				Visible = false
			});

			allColumns.Add(new JQGridColumnDynamic
			{
				HeaderText = "StatusDescription",
				DataField = "StatusDescription",
				DataType = typeof(string),
				Searchable = true,
				Visible = false
			});

			allColumns.Add(new JQGridColumnDynamic
			{
				HeaderText = "QAStatusDescription",
				DataField = "QAStatusDescription",
				DataType = typeof(string),
				Searchable = true,
				Visible = false
			});

			allColumns.Add(new JQGridColumnDynamic
			{
				HeaderText = "QAProjectIndicator",
				DataField = "QAProjectIndicator",
				DataType = typeof(string),
				Visible = false
			});

			allColumns.Add(new JQGridColumnDynamic
			{
				HeaderText = "ENInvolvementWhileFL",
				DataField = "ENInvolvementWhileFL",
				DataType = typeof(string),
				Visible = false
			});

			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "MissingTarget",
				DataType = typeof(bool),
				Visible = false
			});

			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "GlimpseFilter",
				DataType = typeof(string),
				Searchable = true,
				Visible = false
			});
			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "JiraOpenURL",
				DataType = typeof(string),
				Searchable = true,
				Visible = false
			});
			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "JiraClosedURL",
				DataType = typeof(string),
				Searchable = true,
				Visible = false
			});
			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "SubJiraOpenURL",
				DataType = typeof(string),
				Searchable = true,
				Visible = false
			});
			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "SubJiraClosedURL",
				DataType = typeof(string),
				Searchable = true,
				Visible = false
			});
			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "TotalTasks",
				DataType = typeof(int),
				Visible = false
			});
			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "ManufFilter",
				DataType = typeof(string),
				Searchable = true,
				Visible = false
			});
			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "ProjectLeadFilter",
				DataType = typeof(string),
				Searchable = true,
				Visible = false
			});
			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "MAProjectLeadFilter",
				DataType = typeof(string),
				Searchable = true,
				Visible = false
			});

			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "IsFavorite",
				DataType = typeof(bool),
				Searchable = true,
				Visible = false
			});
			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "IsEstMathCompleteTBD",
				DataType = typeof(bool),
				Visible = false
			});

			return allColumns;
		}

		public void Export(Export export, IQueryable<vw_trProject> query)
		{
			var selectStatement = SetDynamicLinqColumns(export.SavedColumns);
			var results = query.Select("FileNumber");

			var fileName = string.Format(export.Title + " - {0:yyyy-MM-dd_hh-mm-ss-tt}", DateTime.Now);
			var jqGridHelper = new JQGridHelper();
			var jqGridState = new JQGridState();
			jqGridState.QueryString = HttpUtility.ParseQueryString(export.FiltersToolbar ?? "");

			if (export.ExportType == ExportType.Xls)
			{
				fileName += ".xls";
				this.Grid.ExportToExcel(results, fileName, jqGridState);
			}
			else if (export.ExportType == ExportType.Csv)
			{
				fileName += ".csv";
				this.Grid.ExportSettings.ExportDataRange = ExportDataRange.All;
				var exportData = this.Grid.GetExportData(results, jqGridState);
				jqGridHelper.ExportToCSV(exportData, fileName);
			}
			else if (export.ExportType == ExportType.Pdf)
			{
				fileName += ".pdf";
				this.Grid.ExportSettings.ExportDataRange = ExportDataRange.All;
				var exportData = this.Grid.GetExportData(results, jqGridState);
				jqGridHelper.ExportToPDF(exportData, fileName);
			}
		}

		public void ExportCustomData(Export export, DataTable dt)
		{
			var fileName = string.Format(export.Title + " - {0:yyyy-MM-dd_hh-mm-ss-tt}", DateTime.Now);
			var jqGridHelper = new JQGridHelper();
			var jqGridState = new JQGridState();
			if (export.ExportType == ExportType.Xls)
			{
				fileName += ".xls";
				jqGridHelper.RenderExcelToStream(dt, fileName, export.Title);
			}
			else if (export.ExportType == ExportType.Csv)
			{
				fileName += ".csv";
				jqGridHelper.ExportToCSV(dt, fileName);
			}
			else if (export.ExportType == ExportType.Pdf)
			{
				fileName += ".pdf";
				jqGridHelper.ExportToPDF(dt, fileName);
			}
		}

		public List<JQGridColumnDynamic> GetAllDisplayColumns(string savedColumns)
		{
			var jqColumns = new List<JQGridColumnDynamic>();

			var savedColumnsList = new List<string>(savedColumns.Replace("\"", "").Replace("[", "").Replace("]", "").Split(','));
			var allAvailable = GetAllColumns();

			//jqColumns.AddRange(hiddenColumns);
			foreach (var col in savedColumnsList)
			{
				var column = allAvailable.Where(x => x.DataField == col).SingleOrDefault();
				if (column != null)
				{
					column.Visible = true;
					jqColumns.Add(column);
				}
			}

			return jqColumns;
		}

		//List of all date columns so sorttype can be set specifically, this is is needed when type is local
		public List<string> GetAllDateColumns()
		{
			var allAvailable = GetAllColumns();
			var allDateColumns = allAvailable.Where(x => x.DataType == typeof(DateTime)).Select(x => x.DataField.ToString()).ToList();

			return allDateColumns;
		}

		public List<string> GetAllIntColumns()
		{
			var allAvailable = GetAllColumns();
			var allIntColumns = allAvailable.Where(x => (x.DataType == typeof(int) || x.DataType == typeof(decimal))).Select(x => x.DataField.ToString()).ToList();

			return allIntColumns;
		}

		//All display columns
		private List<JQGridColumnDynamic> GetAllColumns()
		{
			var allColumns = new List<JQGridColumnDynamic>();

			//All Display Columns
			allColumns.Add(new JQGridColumnDynamic()
			{
				DataField = "ReceiveDate",
				HeaderText = "Received",
				DataFormatString = "{0:d}",
				DataType = typeof(DateTime),
				Searchable = true,
				Width = 70,
				SearchControlID = "DatePicker",
				SearchType = SearchType.DatePicker
			});

			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "QAReceiveDate",
				HeaderText = "QA Received",
				DataFormatString = "{0:d}",
				DataType = typeof(DateTime),
				Searchable = true,
				Width = 100,
				SearchControlID = "DatePicker",
				SearchType = SearchType.DatePicker
			});

			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "CreateDate",
				HeaderText = "Created",
				DataFormatString = "{0:d}",
				DataType = typeof(DateTime),
				Searchable = true,
				Width = 70,
				Visible = false,
				SearchControlID = "DatePicker",
				SearchType = SearchType.DatePicker
			});

			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "AcceptDate",
				HeaderText = "Accepted",
				DataFormatString = "{0:d}",
				Searchable = true,
				DataType = typeof(DateTime),
				Width = 70,
				SearchControlID = "DatePicker",
				SearchType = SearchType.DatePicker
			});

			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "QAAcceptDate",
				HeaderText = "QA Accepted",
				DataFormatString = "{0:d}",
				Searchable = true,
				DataType = typeof(DateTime),
				Width = 80,
				SearchControlID = "DatePicker",
				SearchType = SearchType.DatePicker
			});

			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "FileNumber",
				HeaderText = "Project Name",
				Formatter = new CustomFormatter { FormatFunction = "detailLink" },
				DataType = typeof(string),
				Searchable = true,
				SearchToolBarOperation = SearchOperation.Contains,
				SearchType = SearchType.AutoComplete,
				SearchControlID = "ProjectAutoComplete",
				Width = 150
			});

			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "SubmissionIdNumber",
				HeaderText = "ID Number",
				DataType = typeof(string),
				Searchable = true,
				SearchToolBarOperation = SearchOperation.Contains,
				Width = 150,
				Visible = false
			});

			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "SubmissionGameName",
				HeaderText = "Game Name",
				DataType = typeof(string),
				Searchable = true,
				SearchToolBarOperation = SearchOperation.Contains,
				SearchType = SearchType.AutoComplete,
				SearchControlID = "SubmissionGameNameAutoComplete",
				Width = 150
			});

			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "SubmissionDateCode",
				HeaderText = "Date Code",
				DataType = typeof(string),
				Searchable = true,
				SearchToolBarOperation = SearchOperation.Contains,
				Width = 100
			});

			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "SubmissionVersion",
				HeaderText = "Version",
				DataType = typeof(string),
				Searchable = true,
				SearchToolBarOperation = SearchOperation.Contains,
				Width = 90
			});

			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "JurCount",
				HeaderText = "Jur Count",
				DataType = typeof(int),
				Searchable = true,
				Visible = false,
				Width = 70,
			});

			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "JiraOpen",
				HeaderText = "Open Jiras",
				Width = 70,
				Visible = false
			});

			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "ReviewOpen",
				HeaderText = "Review",
				Width = 70,
				Visible = false
			});

			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "JiraClosed",
				HeaderText = "Closed Jiras",
				Width = 80,
				Visible = false
			});

			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "SubJiraOpen",
				HeaderText = "Submission Open Jiras",
				Width = 90,
				Visible = false
			});

			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "SubJiraClosed",
				HeaderText = "Submission Closed Jiras",
				Width = 100,
				Visible = false
			});

			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "Laboratory",
				HeaderText = "Cur Location",
				DataType = typeof(string),
				Searchable = true,
				Width = 90,
				Visible = false
			});

			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "TestLab",
				HeaderText = "Test Lab",
				DataType = typeof(string),
				Searchable = true,
				Width = 90,
				Visible = false
			});
			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "QAOffice",
				HeaderText = "QA Office",
				DataType = typeof(string),
				Searchable = true,
				Width = 90,
				//Visible = false
			});

			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "Status",
				HeaderText = "Status",
				Width = 60,
				//Formatter = new CustomFormatter { FormatFunction = "statusFormat" },
				DataType = typeof(string),
			});

			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "BundleStatus",
				HeaderText = "QA Status",
				Width = 80,
				//Formatter = new CustomFormatter { FormatFunction = "statusFormat" },
				DataType = typeof(string),
			});

			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "ETAStatus",
				HeaderText = "ETA Status",
				Width = 80,
				DataType = typeof(string),
			});

			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "ProjectHolder",
				HeaderText = "Holder",
				DataType = typeof(string),
				Width = 80,
				Visible = false
			});

			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "Tasks",
				HeaderText = "Tasks",
				//Formatter = new CustomFormatter { FormatFunction = "tasksFormat" },
				DataType = typeof(string),
				Width = 120
			});

			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "QATasks",
				HeaderText = "QA Tasks",
				//Formatter = new CustomFormatter { FormatFunction = "tasksFormat" },
				DataType = typeof(string),
				Width = 120,
				Visible = false
			});

			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "QAReviewer",
				HeaderText = "QA Reviewer",
				//Formatter = new CustomFormatter { FormatFunction = "tasksFormat" },
				DataType = typeof(string),
				Width = 120,
				Visible = false
			});

			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "QALetterWriter",
				HeaderText = "Letter Writer",
				//Formatter = new CustomFormatter { FormatFunction = "tasksFormat" },
				DataType = typeof(string),
				Width = 120,
				Visible = false
			});

			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "QALWLocation",
				HeaderText = "LW Location",
				Searchable = true,
				DataType = typeof(string),
				Width = 80,
				Visible = false
			});

			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "EstCompleteDate",
				HeaderText = "Est Eng Complete",
				DataFormatString = "{0:d}",
				Searchable = true,
				DataType = typeof(DateTime),
				Width = 105,
				SearchControlID = "DatePicker",
				SearchType = SearchType.DatePicker
			});

			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "ReqQADelivery",
				HeaderText = "Req QA Delivery",
				DataFormatString = "{0:d}",
				Searchable = true,
				DataType = typeof(DateTime),
				Width = 100,
				SearchControlID = "DatePicker",
				SearchType = SearchType.DatePicker
			});

			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "CompleteDate",
				HeaderText = "Eng Complete",
				DataFormatString = "{0:d}",
				Searchable = true,
				DataType = typeof(DateTime),
				Width = 95,
				SearchControlID = "DatePicker",
				SearchType = SearchType.DatePicker
			});

			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "TargetDate",
				HeaderText = "Target Date",
				DataFormatString = "{0:d}",
				//Formatter = new CustomFormatter { FormatFunction = "manageTargetDate" },
				Searchable = true,
				DataType = typeof(DateTime),
				Width = 80,
				SearchControlID = "DatePicker",
				SearchType = SearchType.DatePicker
			});

			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "BundleTaskTarget",
				HeaderText = "Task Target",
				DataFormatString = "{0:d}",
				//Formatter = new CustomFormatter { FormatFunction = "manageTargetDate" },
				Searchable = true,
				DataType = typeof(DateTime),
				Width = 80,
				SearchControlID = "DatePicker",
				SearchType = SearchType.DatePicker,
				Visible = false
			});

			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "TaskStartDate",
				HeaderText = "Start Date",
				DataFormatString = "{0:d}",
				//Formatter = new CustomFormatter { FormatFunction = "manageTargetDate" },
				Searchable = true,
				DataType = typeof(DateTime),
				Width = 90,
				Visible = false,
				SearchControlID = "DatePicker",
				SearchType = SearchType.DatePicker
			});

			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "OrgTargetDate",
				HeaderText = "Orig Target",
				DataFormatString = "{0:d}",
				//Formatter = new CustomFormatter { FormatFunction = "manageTargetDate" },
				DataType = typeof(DateTime),
				Width = 90,
				Visible = false,
				SearchControlID = "DatePicker",
				SearchType = SearchType.DatePicker
			});

			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "DaysToTarget",
				HeaderText = "Days To Target",
				Searchable = true,
				DataType = typeof(int),
				Width = 90,
				Visible = false
			});

			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "EstTestCompleteDate",
				HeaderText = "Est Test Complete",
				DataFormatString = "{0:d}",
				Searchable = true,
				DataType = typeof(DateTime),
				Width = 110,
				Visible = false,
				SearchControlID = "DatePicker",
				SearchType = SearchType.DatePicker
			});

			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "EstMathCompleteDate",
				HeaderText = "Est Math Complete",
				DataFormatString = "{0:d}",
				Searchable = true,
				DataType = typeof(DateTime),
				Width = 120,
				Visible = false,
				SearchControlID = "DatePicker",
				SearchType = SearchType.DatePicker
			});

			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "MasterFile",
				HeaderText = "Master File",
				Searchable = true,
				DataType = typeof(string),
				Width = 120,
				Visible = false
			});

			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "ProjectAge",
				HeaderText = "Age",
				Searchable = true,
				DataType = typeof(int),
				Width = 90,
				Visible = false
			});

			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "Manager",
				HeaderText = "Manager",
				Searchable = true,
				DataType = typeof(string),
				Width = 130,
				Visible = false
			});

			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "TotalOHTime",
				HeaderText = "OH Time",
				Searchable = true,
				DataType = typeof(int),
				Width = 90,
				Visible = false
			});

			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "OHStartDate",
				HeaderText = "OH Date",
				DataFormatString = "{0:d}",
				Searchable = true,
				DataType = typeof(DateTime),
				Width = 110,
				Visible = false,
				SearchControlID = "DatePicker",
				SearchType = SearchType.DatePicker
			});

			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "Manufacturer",
				HeaderText = "MFG",
				Searchable = true,
				DataType = typeof(string),
				SearchToolBarOperation = SearchOperation.Contains,
				Width = 90,
				Visible = false
			});

			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "ProjectLead",
				HeaderText = "Project Lead",
				Searchable = true,
				DataType = typeof(string),
				Width = 90,
				Visible = false
			});

			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "MAProjectLead",
				HeaderText = "Math Project Lead",
				Searchable = true,
				DataType = typeof(string),
				Width = 90,
				Visible = false
			});

			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "Complexity",
				HeaderText = "Complexity",
				Searchable = true,
				DataType = typeof(string),
				Width = 155,
				Visible = false
			});

			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "ContractNumber",
				HeaderText = "Project/Contract #",
				Searchable = true,
				DataType = typeof(string),
				Width = 120,
				Visible = false
			});

			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "EstHours",
				HeaderText = "Est Hours",
				Searchable = true,
				DataType = typeof(decimal),
				Width = 90,
				Visible = false
			});

			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "ActHours",
				HeaderText = "Act Hours",
				Searchable = true,
				DataType = typeof(decimal),
				Width = 90,
				Visible = false
			});

			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "NoCharge",
				HeaderText = "No Charge",
				Searchable = true,
				DataType = typeof(decimal),
				Width = 90,
				Visible = false
			});

			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "NonBillable",
				HeaderText = "Non Billable",
				Searchable = true,
				DataType = typeof(decimal),
				Width = 90,
				Visible = false
			});

			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "BudgetPercentage",
				HeaderText = "Budget Pen",
				Searchable = true,
				DataType = typeof(decimal),
				Width = 90,
				Visible = false
			});

			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "MathEst",
				HeaderText = "Math Est",
				Searchable = true,
				DataType = typeof(decimal),
				Width = 90,
				Visible = false
			});

			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "MathAct",
				HeaderText = "Math Act",
				Searchable = true,
				DataType = typeof(decimal),
				Width = 90,
				Visible = false
			});

			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "MathPen",
				HeaderText = "Math Pen",
				Searchable = true,
				DataType = typeof(decimal),
				Width = 90,
				Visible = false
			});

			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "TargetDateWithOH",
				HeaderText = "Target OH",
				DataFormatString = "{0:d}",
				Searchable = true,
				DataType = typeof(DateTime),
				Width = 110,
				Visible = false,
				SearchControlID = "DatePicker",
				SearchType = SearchType.DatePicker
			});

			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "DevRep",
				HeaderText = "Dev Rep",
				Searchable = true,
				DataType = typeof(string),
				Width = 110,
				Visible = false
			});

			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "QATime",
				HeaderText = "QA Time",
				Searchable = true,
				DataType = typeof(int),
				Width = 110,
				Visible = false
			});

			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "QASupervisor",
				HeaderText = "QA Supervisor",
				Searchable = true,
				DataType = typeof(string),
				Width = 110,
				Visible = false
			});

			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "Quoted",
				HeaderText = "Quoted",
				Searchable = true,
				DataType = typeof(decimal),
				Width = 90,
				Visible = false
			});

			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "UnExpHrs",
				HeaderText = "UnExp Hrs",
				Searchable = true,
				DataType = typeof(decimal),
				Width = 90,
				Visible = false
			});

			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "ProjectNote",
				HeaderText = "",
				Visible = false
			});

			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "EngNotes",
				HeaderText = "",
				Visible = false
			});

			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "DeclineRequestor",
				HeaderText = "Decline Requestor",
				Searchable = true,
				DataType = typeof(string),
				Width = 180,
				Visible = false
			});

			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "DeclineReason",
				HeaderText = "Decline Reason",
				Searchable = true,
				DataType = typeof(string),
				Width = 350,
				Visible = false
			});

			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "MathCompleteDate",
				HeaderText = "Math Complete",
				DataFormatString = "{0:d}",
				Searchable = true,
				DataType = typeof(DateTime),
				Width = 110,
				Visible = false,
				SearchControlID = "DatePicker",
				SearchType = SearchType.DatePicker
			});

			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "ENTestCompleteDate",
				HeaderText = "EN Test Complete",
				DataFormatString = "{0:d}",
				Searchable = true,
				DataType = typeof(DateTime),
				Width = 110,
				Visible = false,
				SearchControlID = "DatePicker",
				SearchType = SearchType.DatePicker
			});

			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "CertLab",
				HeaderText = "Cert Lab",
				DataType = typeof(string),
				Searchable = true,
				Width = 90,
				Visible = false
			});

			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "Jurisdiction",
				HeaderText = "Main Jurisdiction",
				DataType = typeof(string),
				Searchable = true,
				Width = 150,
				Visible = false
			});

			allColumns.Add(new JQGridColumnDynamic
			{
				HeaderText = "NoCertNeeded",
				DataField = "NoCertNeeded",
				DataType = typeof(string),
				Visible = false
			});

			allColumns.Add(new JQGridColumnDynamic
			{
				HeaderText = "Testing Type",
				DataField = "TestingType",
				DataType = typeof(string),
				Visible = false
			});

			return allColumns;
		}

	}

	public class SupervisorMainProjectGrid : ProjectGrid
	{
		public SupervisorMainProjectGrid()
		{
			this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.DataField == "BundleStatus")].Visible = false;
			this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.DataField == "QAAcceptDate")].Visible = false;
			this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.DataField == "QAReceiveDate")].Visible = false;
			this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.DataField == "JiraOpen")].Visible = true;
			this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.DataField == "ReviewOpen")].Visible = true;
			this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.DataField == "ReviewOpen")].Formatter = new CustomFormatter { FormatFunction = "openReviewLink" };
			//this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.DataField == "JiraClosed")].Visible = true;
			this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.DataField == "TargetDate")].Formatter = new CustomFormatter { FormatFunction = "manageTargetDate" };
			this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.DataField == "Tasks")].Formatter = new CustomFormatter { FormatFunction = "tasksFormat" };

			this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.DataField == "Status")].Formatter = new CustomFormatter { FormatFunction = "statusFormat" };

			this.AllColumns.Find(x => x.DataField == "StatusSort").Formatter = new CustomFormatter { FormatFunction = "showGroup" };

			this.Grid.Columns.Insert(this.Grid.Columns.Count, new JQGridColumn
			{
				DataField = "",
				HeaderText = "",
				DataType = typeof(string),
				Formatter = new CustomFormatter { FormatFunction = "createTransferLink" },
				Searchable = false,
				Sortable = false,
				Width = 120,
				TextAlign = TextAlign.Left
			});

			this.Grid.MultiSelect = true;
		}
	}

	public class SupervisorAcceptProjectGrid : ProjectGrid
	{
		public SupervisorAcceptProjectGrid()
		{
			this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.DataField == "BundleStatus")].Visible = false;
			this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.DataField == "QAAcceptDate")].Visible = false;
			this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.DataField == "QAReceiveDate")].Visible = false;
			this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.DataField == "Tasks")].Formatter = null;
			this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.DataField == "Tasks")].Visible = false;
			this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.DataField == "Status")].Formatter = null;
			this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.DataField == "Status")].Visible = false;
			this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.DataField == "AcceptDate")].Visible = false;
			this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.DataField == "EstCompleteDate")].Visible = false;
			this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.DataField == "ReqQADelivery")].Visible = false;
			this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.DataField == "CompleteDate")].Visible = false;
			this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.DataField == "TargetDate")].Visible = false;
			this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.DataField == "Manufacturer")].Visible = true;
			this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.DataField == "")].Formatter = new CustomFormatter { FormatFunction = "showENProjectNote" };
			this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.DataField == "")].Width = 90;

			this.Grid.Columns.Insert(this.Grid.Columns.Count, new JQGridColumn
			{
				DataField = "",
				HeaderText = "InReview",
				DataType = typeof(string),
				Searchable = false,
				Formatter = new CustomFormatter { FormatFunction = "reviewLink" },
				Sortable = false,
				Width = 90,
				TextAlign = TextAlign.Left
			});
			this.Grid.Columns.Insert(this.Grid.Columns.Count, new JQGridColumn
			{
				DataField = "",
				HeaderText = "Assign InReview",
				DataType = typeof(string),
				Searchable = false,
				Formatter = new CustomFormatter { FormatFunction = "assignIncomingLink" },
				Sortable = false,
				Width = 170,
				TextAlign = TextAlign.Left
			});
			this.Grid.Columns.Insert(this.Grid.Columns.Count, new JQGridColumn
			{
				DataField = "",
				HeaderText = "Decline Reason",
				DataType = typeof(string),
				Searchable = false,
				Formatter = new CustomFormatter { FormatFunction = "showDeclineReason" },
				Sortable = false,
				Width = 200,
				TextAlign = TextAlign.Left
			});


			this.Grid.MultiSelect = true;
			this.Grid.RenderingMode = RenderingMode.Default;
			this.Grid.HierarchySettings.HierarchyMode = HierarchyMode.Parent;
			this.Grid.ClientSideEvents.SubGridRowExpanded = "showDetails";
			this.Grid.AutoWidth = true;
			this.Grid.ShrinkToFit = true;

		}
	}

	public class ReviewerMainProjectGrid : ProjectGrid
	{
		public ReviewerMainProjectGrid()
		{
			this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.DataField == "Jurisdiction")].Visible = true;
			this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.DataField == "Status")].Visible = false;
			this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.DataField == "Tasks")].Visible = false;
			this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.DataField == "ReqQADelivery")].Visible = false;
			this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.DataField == "EstCompleteDate")].Visible = false;
			this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.DataField == "CompleteDate")].Visible = false;
			this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.DataField == "AcceptDate")].Visible = false;
			this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.DataField == "ReceiveDate")].Visible = false;
			this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.DataField == "SubmissionDateCode")].Visible = false;
			this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.DataField == "JiraOpen")].Visible = true;
			this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.DataField == "ProjectHolder")].Visible = true;
			this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.DataField == "QATasks")].Visible = true;
			this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.DataField == "QALetterWriter")].Visible = true;
			this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.DataField == "QAReviewer")].Visible = true;
			this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.DataField == "QALWLocation")].Visible = true;
			this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.DataField == "JurCount")].Visible = true;

			//this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.DataField == "JiraClosed")].Visible = true;
			this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.DataField == "TargetDate")].Formatter = new CustomFormatter { FormatFunction = "manageTargetDate" };
			this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.DataField == "QATasks")].Formatter = new CustomFormatter { FormatFunction = "QAtasksFormat" };
			this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.DataField == "BundleStatus")].Formatter = new CustomFormatter { FormatFunction = "QAstatusFormat" };
			this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.DataField == "BundleStatus")].Width = 100;

			this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.DataField == "FileNumber")].Width = this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.DataField == "FileNumber")].Width + 60;
			this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.DataField == "SubmissionGameName")].Visible = false;
			this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.DataField == "SubmissionVersion")].Visible = false;
			this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.DataField == "BundleTaskTarget")].Visible = true;

			this.AllColumns.Find(x => x.DataField == "QAStatusSort").Formatter = new CustomFormatter { FormatFunction = "showQAGroup" };

			this.Grid.Columns.Insert(this.Grid.Columns.Count, new JQGridColumn
			{
				DataField = "SubmissionGameNameCalc",
				HeaderText = "Game Name",
				DataType = typeof(string),
				Searchable = true,
				Sortable = true,
				Width = 220,
				TextAlign = TextAlign.Left
			});

			this.Grid.Columns.Insert(this.Grid.Columns.Count, new JQGridColumn
			{
				DataField = "",
				HeaderText = "",
				DataType = typeof(string),
				Formatter = new CustomFormatter { FormatFunction = "createTransferLink" },
				Searchable = false,
				Sortable = false,
				Width = 120,
				TextAlign = TextAlign.Left
			});

			this.Grid.MultiSelect = true;
		}
	}

	public class ReviewerAcceptProjectGrid : ProjectGrid
	{
		public ReviewerAcceptProjectGrid()
		{
			this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.DataField == "Status")].Formatter = null;
			this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.DataField == "Status")].Visible = false;
			this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.DataField == "ReqQADelivery")].Visible = false;
			this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.DataField == "EstCompleteDate")].Visible = false;
			this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.DataField == "CompleteDate")].Visible = false;
			this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.DataField == "AcceptDate")].Visible = false;
			this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.DataField == "SubmissionDateCode")].Visible = false;
			this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.DataField == "ReceiveDate")].Visible = false;
			this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.DataField == "QAAcceptDate")].Visible = false;
			this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.DataField == "BundleStatus")].Visible = false;
			this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.DataField == "Tasks")].Formatter = null;
			this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.DataField == "Tasks")].Visible = false;
			this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.DataField == "TestLab")].Visible = true;
			this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.DataField == "CreateDate")].Visible = true;
			this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.DataField == "Jurisdiction")].Visible = true;
			this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.DataField == "Manufacturer")].Visible = true;
			this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.DataField == "")].Formatter = new CustomFormatter { FormatFunction = "showProjectNote" };
			this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.DataField == "")].Width = 90;
			this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.DataField == "FileNumber")].Width = this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.DataField == "FileNumber")].Width + 50;

			this.Grid.MultiSelect = true;
			this.Grid.SortSettings = new SortSettings
			{
				InitialSortColumn = "FileNumber",
				InitialSortDirection = SortDirection.Asc
			};

			this.Grid.RenderingMode = RenderingMode.Default;
			this.Grid.HierarchySettings.HierarchyMode = HierarchyMode.Parent;
			this.Grid.ClientSideEvents.SubGridRowExpanded = "showDetails";
			this.Grid.AutoWidth = true;
			this.Grid.ShrinkToFit = true;
		}
	}

	public class GlimpseGrid : ProjectGrid
	{

		public GlimpseGrid()
		{
			this.Grid.ColumnReordering = true;
			this.Grid.MultiSelect = true;

			//$("#list").remapColumns([1,3,2,5,6,7,8,9],true,false);
			//this.Grid.PagerSettings.PageSize = 20000;
		}

		/*public void SetGlimpseCustomization()
		{
			this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.DataField == "BudgetPercentage")].Formatter = new CustomFormatter { FormatFunction = "showGlimpseColor" };
			this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.DataField == "TargetDate")].Formatter = new CustomFormatter { FormatFunction = "showGlimpseColor" };
		
		}*/
	}

	public class DeclinedProjectGrid : ProjectGrid
	{
		public DeclinedProjectGrid()
		{
			this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.DataField == "Status")].Formatter = null;
			this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.DataField == "Status")].Visible = false;
			this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.DataField == "ReqQADelivery")].Visible = false;
			this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.DataField == "EstCompleteDate")].Visible = false;
			this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.DataField == "CompleteDate")].Visible = false;
			this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.DataField == "AcceptDate")].Visible = false;
			this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.DataField == "SubmissionDateCode")].Visible = false;
			this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.DataField == "ReceiveDate")].Visible = true;
			this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.DataField == "QAAcceptDate")].Visible = false;
			this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.DataField == "QAReceiveDate")].Visible = false;
			this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.DataField == "BundleStatus")].Visible = false;
			this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.DataField == "Tasks")].Formatter = null;
			this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.DataField == "Tasks")].Visible = false;
			this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.DataField == "CreateDate")].Visible = true;
			this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.DataField == "FileNumber")].Width = this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.DataField == "FileNumber")].Width + 90;
			this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.DataField == "SubmissionGameName")].Visible = false;
			this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.DataField == "SubmissionIdNumber")].Visible = false;
			this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.DataField == "SubmissionDateCode")].Visible = false;
			this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.DataField == "DeclineRequestor")].Visible = true;
			this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.DataField == "DeclineReason")].Visible = true;

			this.Grid.MultiSelect = true;
		}
	}

	public class ADDCORSearchProjectGrid : ProjectGrid
	{
		public ADDCORSearchProjectGrid()
		{
			this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.DataField == "Status")].Formatter = null;
			this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.DataField == "Status")].Visible = false;
			this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.DataField == "ReqQADelivery")].Visible = false;
			this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.DataField == "EstCompleteDate")].Visible = false;
			this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.DataField == "CompleteDate")].Visible = true;
			this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.DataField == "AcceptDate")].Visible = false;
			this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.DataField == "SubmissionDateCode")].Visible = false;
			this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.DataField == "ReceiveDate")].Visible = true;
			this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.DataField == "QAAcceptDate")].Visible = false;
			this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.DataField == "QAReceiveDate")].Visible = false;
			this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.DataField == "BundleStatus")].Visible = false;
			this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.DataField == "Tasks")].Formatter = null;
			this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.DataField == "Tasks")].Visible = false;
			this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.DataField == "CreateDate")].Visible = true;
			this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.DataField == "SubmissionGameName")].Visible = false;
			this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.DataField == "SubmissionIdNumber")].Visible = false;
			this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.DataField == "SubmissionDateCode")].Visible = false;
			this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.DataField == "DeclineRequestor")].Visible = false;
			this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.DataField == "DeclineReason")].Visible = false;
			this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.DataField == "TargetDate")].Visible = false;

			this.Grid.Columns.Insert(this.Grid.Columns.Count, new JQGridColumn
			{
				DataField = "",
				HeaderText = "",
				DataType = typeof(string),
				Formatter = new CustomFormatter { FormatFunction = "createTransferLink" },
				Searchable = false,
				Sortable = false,
				Width = 120,
				TextAlign = TextAlign.Left
			});

			this.Grid.AutoWidth = true;
			this.Grid.Width = Unit.Percentage(60);

			this.Grid.MultiSelect = true;
		}
	}

	public class ProjectGridRecord
	{
		public int Id { get; set; }
		public string StatusSort { get; set; }
		public string QAStatusSort { get; set; }

		public string FileNumber { get; set; }
		public string SubmissionIdNumber { get; set; }
		public string SubmissionGameName { get; set; }
		public string SubmissionDateCode { get; set; }
		public string SubmissionVersion { get; set; }

		public string Jurisdiction { get; set; }


		public string Status { get; set; }
		public string BundleStatus { get; set; }

		public string StatusDescription { get; set; }
		public string QAStatusDescription { get; set; }
		public string ProjectHolder { get; set; }
		public bool MissingTarget { get; set; }
		public bool? IsRush { get; set; }
		public bool IsTransfer { get; set; }

		public string Tasks { get; set; }
		public string QATasks { get; set; }

		public DateTime? ReceiveDate { get; set; }
		public DateTime? CreateDate { get; set; }
		public DateTime? AcceptDate { get; set; }
		public DateTime? QAReceiveDate { get; set; }
		public DateTime? QAAcceptDate { get; set; }
		public DateTime? QALWStarted { get; set; }
		public DateTime? EstCompleteDate { get; set; }
		public DateTime? CompleteDate { get; set; }
		public DateTime? ReqQADelivery { get; set; }
		public DateTime? TargetDate { get; set; }
		public DateTime? QACompleteDate { get; set; }

		public string QAProjectIndicator { get; set; } //This is for QA Project Indicator
		public bool ENInvolvementWhileFL { get; set; }
		public bool NoCertNeeded { get; set; }
		public string Laboratory { get; set; }
		public string TestingType { get; set; }
		public string TestLab { get; set; }
		public string ProjectNote { get; set; } //This on QA Project Accept Page
		public string EngNotes { get; set; } //While Eng Accept this is the note by QA
		public string DeclineReason { get; set; }
		public string DeclineRequestor { get; set; }

		//Glimpse Grid
		public string Manufacturer { get; set; }
		public DateTime? TaskStartDate { get; set; }
		public DateTime? EstTestCompleteDate { get; set; } // Needs to be same column name based on PersonnelType enum, after update, values r getting set without Refresh
		public DateTime? EstMathCompleteDate { get; set; } // Needs to be same column name based on PersonnelType enum, after update, values r getting set without Refresh
		public bool IsEstMathCompleteTBD { get; set; }
		public DateTime? MathCompleteDate { get; set; }
		public DateTime? ENTestCompleteDate { get; set; }
		public DateTime? OrgTargetDate { get; set; }
		public int? DaysToTarget { get; set; }
		public string MasterFile { get; set; }
		public int? ProjectAge { get; set; }
		public string Manager { get; set; }
		public int? TotalOHTime { get; set; }
		public DateTime? OHStartDate { get; set; }
		public int TotalTasks { get; set; }

		public string Complexity { get; set; } // Needs to be same column name based on PersonnelType enum, after update, values r getting rest without Refresh
		public string ContractNumber { get; set; }
		public decimal? EstHours { get; set; }
		public decimal? ActHours { get; set; }
		public decimal? NoCharge { get; set; }
		public decimal? NonBillable { get; set; }
		public decimal? BudgetPercentage { get; set; }
		public decimal? UnExpHrs { get; set; }

		public decimal? MathEst { get; set; }
		public decimal? MathAct { get; set; }
		public decimal? MathPen { get; set; }
		public DateTime? TargetDateWithOH { get; set; }
		public string DevRep { get; set; }  // Needs to be same column name based on PersonnelType enum, after update, values r getting rest without Refresh
		public string ProjectLead { get; set; } // Needs to be same column name based on PersonnelType enum, after update, values r getting rest without Refresh
		public string MAProjectLead { get; set; }
		public int? QATime { get; set; }
		public string QASupervisor { get; set; }
		public string QALetterWriter { get; set; }
		public string QAReviewer { get; set; }
		public DateTime? BundleTaskTarget { get; set; }
		public string QALWLocation { get; set; }
		public int JurCount { get; set; }
		public decimal? Quoted { get; set; }

		public string JiraOpen { get; set; }
		public string ReviewOpen { get; set; }
		public string JiraClosed { get; set; }
		public string JiraOpenURL { get; set; }
		public string JiraClosedURL { get; set; }
		public string SubJiraOpen { get; set; }
		public string SubJiraClosed { get; set; }
		public string SubJiraOpenURL { get; set; }
		public string SubJiraClosedURL { get; set; }

		public string GlimpseFilter { get; set; }
		public string ManufFilter { get; set; }
		public string ProjectLeadFilter { get; set; }
		public string MAProjectLeadFilter { get; set; }
		public string IsFavorite { get; set; }
		public string CertLab { get; set; }
		public ETATaskViewModel ETATask { get; set; }
		public string ETAStatus
		{
			get
			{
				if (ETATask == null)
				{
					return "";
				}
				else if (ETATask.StartDate == null && ETATask.UserId == null)
				{
					return TaskStatus.Unassigned.ToString();
				}
				else if (ETATask.StartDate == null && ETATask.UserId != null)
				{
					return TaskStatus.Assigned.ToString();
				}
				else if (ETATask.StartDate != null && ETATask.CompleteDate == null)
				{
					return TaskStatus.InProgress.GetEnumDescription();
				}
				else
				{
					return TaskStatus.Closed.GetEnumDescription();
				}
			}
		}
		public string QAOffice { get; set; }
	}

	public class ETATaskViewModel
	{
		public int? UserId { get; set; }
		public DateTime? StartDate { get; set; }
		public DateTime? CompleteDate { get; set; }
	}
}