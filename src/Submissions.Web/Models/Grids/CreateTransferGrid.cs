﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;
namespace Submissions.Web.Models.Grids
{
	using Trirand.Web.Mvc;
	public class CreateTransferGrid
	{
		public CreateTransferGrid()
		{
			this.Grid = new JQGrid
			{
				ID = "CreateTransferGrid",
				Columns = LoadColumns(),
				DataUrl = new UrlHelper(((MvcHandler)HttpContext.Current.Handler).RequestContext).Action("LoadCreateTransferGrid", "GridsAjax", new { Area = "" }),
				AutoWidth = true,
				Width = Unit.Percentage(100),
				Height = Unit.Percentage(100),
				ToolBarSettings = new ToolBarSettings
				{
					ShowRefreshButton = true,
					ToolBarPosition = ToolBarPosition.Top,
					ShowSearchToolBar = true,
				},
				PagerSettings = new PagerSettings
				{
					PageSize = 1000,
					PageSizeOptions = "[]"
				},
				AppearanceSettings = new AppearanceSettings
				{
					AlternateRowBackground = true,
					HighlightRowsOnHover = true,
					Caption = "Jurisdictions"
				},
				SortSettings = new SortSettings
				{
					InitialSortColumn = "Jurisdiction",
					InitialSortDirection = SortDirection.Asc
				},
				ClientSideEvents = new ClientSideEvents
				{
					BeforeAjaxRequest = "createTransferGridBeforeAjaxRequest"
				},
				RenderingMode = RenderingMode.Optimized
			};
			this.Grid.MultiSelect = true;
		}
		public JQGrid Grid { get; set; }

		private List<JQGridColumn> LoadColumns()
		{
			var columns = new List<JQGridColumn>();

			columns.Add(new JQGridColumn
			{
				DataField = "JurId",
				PrimaryKey = true,
				Editable = false,
				Visible = false
			});

			columns.Add(new JQGridColumn
			{
				DataField = "FileNumber",
				HeaderText = "File Number",
				DataType = typeof(string),
				Width = 120,
				Formatter = new CustomFormatter { FormatFunction = "detailLink" }
			});

			columns.Add(new JQGridColumn
			{
				DataField = "IDNumber",
				HeaderText = "ID Number",
				DataType = typeof(string),
				Width = 140
			});

			columns.Add(new JQGridColumn
			{
				DataField = "Game",
				HeaderText = "Game Name",
				DataType = typeof(string),
				Width = 160
			});

			columns.Add(new JQGridColumn
			{
				DataField = "Version",
				HeaderText = "Version",
				DataType = typeof(string),
				Width = 140
			});

			columns.Add(new JQGridColumn
			{
				DataField = "Status",
				HeaderText = "Status",
				DataType = typeof(string),
				Width = 50
			});

			columns.Add(new JQGridColumn
			{
				DataField = "Jurisdiction",
				HeaderText = "Jurisdiction",
				DataType = typeof(string),
				Width = 120
			});
			columns.Add(new JQGridColumn
			{
				DataField = "JurNum",
				HeaderText = "Jur #",
				DataType = typeof(string),
				Width = 50
			});
			columns.Add(new JQGridColumn
			{
				DataField = "ReceiveDate",
				HeaderText = "Received",
				DataFormatString = "{0:d}",
				DataType = typeof(DateTime),
				Width = 50
			});

			columns.Add(new JQGridColumn
			{
				DataField = "JurisdictionType",
				HeaderText = "Jur Status",
				DataType = typeof(string),
				Width = 80
			});

			columns.Add(new JQGridColumn
			{
				DataField = "TargetDate",
				HeaderText = "Target Date",
				DataFormatString = "{0:d}",
				DataType = typeof(DateTime),
				Width = 70

			});
			columns.Add(new JQGridColumn
			{
				DataField = "IsTransfer",
				Visible = false
			});
			columns.Add(new JQGridColumn
			{
				DataField = "BundleExistsForFile",
				Visible = false
			});
			columns.Add(new JQGridColumn
			{
				DataField = "ProjectId",
				Visible = false
			});

			return columns;
		}
	}

	public class CreateNewTransferGrid : CreateTransferGrid
	{
		public CreateNewTransferGrid()
		{
			this.Grid.SortSettings = new SortSettings
			{
				InitialSortColumn = "FileNumber",
				InitialSortDirection = SortDirection.Asc
			};
			
		}
	}

	public class CreateTransferGridRecord
	{
		public int JurId { get; set; }
		public int? SubId { get; set; }
		public string FileNumber { get; set; }
		public string IDNumber { get; set; }
		public string Game { get; set; }
		public string DateCode { get; set; }
		public string Position { get; set; }
		public string Version { get; set; }
		public string Status { get; set; }
		public string Jurisdiction { get; set; }
		public string JurNum { get; set; }
		public DateTime? ReceiveDate { get; set; }
		public string JurisdictionType { get; set; }
		public DateTime? TargetDate { get; set; }
		public int ProjectId { get; set; }
		public bool? IsTransfer { get; set; }
		public string OriginalProjectStatus { get; set; }
		public bool? BundleExistsForFile { get; set; }
	}
}