﻿using Submissions.Common;
using Submissions.Common.JQGrid;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;

namespace Submissions.Web.Models.Grids.Evolution
{
	using Trirand.Web.Mvc;

	[Serializable]
	public class QANotesGrid
	{
		private const string gridId = "QANotesGrid";
		public JQGrid Grid { get; set; }

		public QANotesGrid()
		{
			Grid = new JQGrid
			{
				ID = gridId,
				DataUrl = new UrlHelper(((MvcHandler)HttpContext.Current.Handler).RequestContext).Action("LoadQANotesIndexGrid"),
				AutoWidth = false,
				ShrinkToFit = false,
				Width = Unit.Percentage(100),
				Height = Unit.Percentage(100),
				ColumnReordering = false,
				Columns = InitializeColumns(),

				ToolBarSettings = new ToolBarSettings
				{
					ShowRefreshButton = true,
					ToolBarPosition = ToolBarPosition.Top,
					ShowSearchButton = true,
					ShowSearchToolBar = true
				},

				PagerSettings = new Trirand.Web.Mvc.PagerSettings
				{
					PageSize = 100,
					PageSizeOptions = "[]"
				},

				AppearanceSettings = new AppearanceSettings
				{
					AlternateRowBackground = true,
					HighlightRowsOnHover = true,
					ShowFooter = false
				}
			};
		}

		#region Initialize Columns
		public List<JQGridColumn> InitializeColumns()
		{
			var Cols = new List<JQGridColumn>
				{
					new JQGridColumn
					{
						DataField = "NoteId",
						HeaderText = "Note Id",
						Editable = false,
						Visible = true,
						SearchToolBarOperation = SearchOperation.IsEqualTo,
						DataType = typeof(string),
						SearchType = SearchType.AutoComplete,
						SearchControlID = "NoteIdAutoComplete",
						Width = 100
					},

					new JQGridColumn
					{
						DataField = "Active",
						HeaderText = "Active",
						Editable = false,
						Visible = true,
						SearchToolBarOperation = SearchOperation.IsEqualTo,
						DataType = typeof(bool),
						SearchType = SearchType.DropDown,
						SearchList = new List<SelectListItem> {
							new SelectListItem {
								Text = "All",
								Value = ""
							},
							new SelectListItem {
								Text = "True",
								Value = "True"
							},
							new SelectListItem {
								Text = "False",
								Value = "False"
							}
						},
						Width = 100
					},

					new JQGridColumn
					{
						DataField = "Heading",
						HeaderText = "Heading",
						Editable = false,
						Visible = true,
						SearchToolBarOperation = SearchOperation.BeginsWith,
						DataType = typeof(string),
						SearchType = SearchType.AutoComplete,
						SearchControlID = "HeadingAutoComplete",
						Searchable = true,
						Sortable = true,
						Width = 200
					},

					new JQGridColumn
					{
						DataField = "Section",
						HeaderText = "Section",
						Editable = false,
						Visible = true,
						SearchToolBarOperation = SearchOperation.BeginsWith,
						DataType = typeof(string),
						SearchType = SearchType.AutoComplete,
						SearchControlID = "SectionAutoComplete",
						Searchable = true,
						Sortable = true,
						Width = 150
					},

					new JQGridColumn
					{
						DataField = "TechLogic",
						HeaderText = "Technical Logic",
						Editable = false,
						Visible = true,
						SearchToolBarOperation = SearchOperation.BeginsWith,
						DataType = typeof(string),
						SearchType = SearchType.AutoComplete,
						SearchControlID = "TechLogicAutoComplete",
						Searchable = true,
						Sortable = true,
						Width = 250
					},

					new JQGridColumn
					{
						DataField = "UniqueId",
						HeaderText = "Id",
						PrimaryKey = true,
						Editable = false,
						Visible = true,
						DataType = typeof(string),
						SearchToolBarOperation = SearchOperation.IsEqualTo,
						SearchType = SearchType.AutoComplete,
						SearchControlID = "UniqueIdAutoComplete",
						Searchable = true,
						Sortable = true,
						Width = 100
					},

					new JQGridColumn
					{
						DataField = "Language",
						HeaderText = "Language",
						Editable = false,
						Visible = true,
						SearchToolBarOperation = SearchOperation.BeginsWith,
						DataType = typeof(string),
						SearchType = SearchType.AutoComplete,
						SearchControlID = "LanguageAutoComplete",
						Searchable = true,
						Sortable = true,
						Width = 150
					},

					new JQGridColumn
					{
						DataField = "LanguageNote",
						HeaderText = "Language Note",
						Editable = false,
						Visible = true,
						SearchToolBarOperation = SearchOperation.BeginsWith,
						DataType = typeof(string),
						SearchType = SearchType.AutoComplete,
						SearchControlID = "LanguageNoteAutoComplete",
						Searchable = true,
						Sortable = true,
						Width = 250
					},

					new JQGridColumn
					{
						DataField = "ValRuleId",
						HeaderText = "Value Rule Id",
						Editable = false,
						Visible = false,
					},

					new JQGridColumn
					{
						DataField = "ApplicableResult",
						HeaderText = "Applicable Result",
						Editable = false,
						Visible = true,
						SearchToolBarOperation = SearchOperation.BeginsWith,
						DataType = typeof(string),
						SearchType = SearchType.AutoComplete,
						SearchControlID = "ApplicableResultAutoComplete",
						Searchable = true,
						Sortable = true,
						Width = 100
					},

					new JQGridColumn
					{
						DataField = "QBText",
						HeaderText = "Question Base Text",
						Editable = false,
						Visible = true,
						SearchToolBarOperation = SearchOperation.BeginsWith,
						DataType = typeof(string),
						SearchType = SearchType.AutoComplete,
						SearchControlID = "QBTextAutoComplete",
						Searchable = true,
						Sortable = true,
						Width = 250
					},

					new JQGridColumn
					{
						DataField = "QAMemo",
						HeaderText = "QA Note Memo",
						Editable = false,
						Visible = true,
						SearchToolBarOperation = SearchOperation.BeginsWith,
						DataType = typeof(string),
						SearchType = SearchType.AutoComplete,
						SearchControlID = "QAMemoAutoComplete",
						Searchable = true,
						Sortable = true,
						Width = 250
					},

					new JQGridColumn
					{
						DataField = "RuleMemo",
						HeaderText = "Rule Memo",
						Editable = false,
						Visible = true,
						//SearchToolBarOperation = SearchOperation.BeginsWith,
						DataType = typeof(string),
						//SearchType = SearchType.AutoComplete,
						//SearchControlID = "RuleMemoAutoComplete",
						Searchable = false,
						Sortable = false,
						Width = 250
					}
				};
			return Cols;
		}
		#endregion

		#region Export
		/// <summary>
		/// Export from grid
		/// </summary>
		public void Export(ExportType exportType, IQueryable<QANotesIndexGridRecord> query)
		{
			var jqGridHelper = new JQGridHelper();
			var fileName = string.Format("QANotes Export - {0:yyyy-MM-dd_hh-mm-ss-tt}", DateTime.Now);

			if (exportType == ExportType.Xls)
			{
				fileName += ".xls";
				this.Grid.ExportSettings.ExportDataRange = ExportDataRange.Filtered;
				this.Grid.ExportToExcel(query, fileName);
			}
			else if (exportType == ExportType.Csv)
			{
				fileName += ".csv";
				var exportData = this.Grid.GetExportData(query);
				jqGridHelper.ExportToCSV(exportData, fileName);
			}
			else if (exportType == ExportType.Pdf)
			{
				fileName += ".pdf";
				var exportData = this.Grid.GetExportData(query);
				jqGridHelper.ExportToPDF(exportData, fileName);
			}
		}
		#endregion
	}

	[Serializable]
	public class QANotesIndexGridRecord
	{
		public bool Active { get; set; }
		public Guid? ValRuleId { get; set; }
		public string NoteId { get; set; }
		public string UniqueId { get; set; }
		public string Heading { get; set; }
		public string Note { get; set; }
		public string LanguageNote { get; set; }
		public string Language { get; set; }
		public string Section { get; set; }
		public string TechLogic { get; set; }
		public string RuleMemo { get; set; }
		public string QBText { get; set; }
		public string ApplicableResult { get; set; }
		public string QAMemo { get; set; }
	}
}