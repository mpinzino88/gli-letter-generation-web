﻿using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;

namespace Submissions.Web.Models.Grids.Evolution
{
	using Trirand.Web.Mvc;
	public class RegulationGrid
	{
		public RegulationGrid()
		{
			this.Grid = new JQGrid
			{
				ID = "RegulationGrid",
				Columns = InitializeAllColumns(),
				DataUrl = new UrlHelper(((MvcHandler)HttpContext.Current.Handler).RequestContext).Action("RegulationGrid", "GridsAjaxEvolution", new { Area = "" }),
				AutoWidth = true,
				Width = Unit.Percentage(100),
				Height = Unit.Percentage(100),
				ToolBarSettings = new ToolBarSettings
				{
					ShowRefreshButton = true,
					ToolBarPosition = ToolBarPosition.Top,
					ShowSearchToolBar = true
				},
				PagerSettings = new PagerSettings
				{
					PageSize = 100,
					PageSizeOptions = "[]"
				},
				AppearanceSettings = new AppearanceSettings
				{
					AlternateRowBackground = true,
					HighlightRowsOnHover = true,
					ShowFooter = false
				},
				SortSettings = new SortSettings
				{
					InitialSortColumn = "DocumentList",
					InitialSortDirection = SortDirection.Desc
				},
				ClientSideEvents = new ClientSideEvents
				{
					BeforeAjaxRequest = "gridBeforeAjaxRequest"
				},
				RenderingMode = RenderingMode.Optimized
			};
		}

		public JQGrid Grid { get; set; }

		private List<JQGridColumn> InitializeAllColumns()
		{
			var allColumns = new List<JQGridColumn>();

			//allColumns.Add(new JQGridColumn
			//{
			//	DataField = "ClauseID",
			//	HeaderText = "Clause ID",
			//	DataType = typeof(string),
			//	Width = 10,
			//	SearchToolBarOperation = SearchOperation.IsEqualTo
			//});

			//allColumns.Add(new JQGridColumn
			//{
			//	DataField = "Rank",
			//	HeaderText = "Rank",
			//	DataType = typeof(int),
			//	Width = 10,
			//	SearchToolBarOperation = SearchOperation.IsEqualTo
			//});
			allColumns.Add(new JQGridColumn
			{
				DataField = "DocumentList",
				HeaderText = "Document(s)",
				DataType = typeof(string),
				Width = 45
			});

			allColumns.Add(new JQGridColumn
			{
				DataField = "VersionList",
				HeaderText = "Document Version(s)",
				DataType = typeof(string),
				Width = 18
			});

			allColumns.Add(new JQGridColumn
			{
				DataField = "Section",
				HeaderText = "Requirement ID(s)",
				DataType = typeof(string),
				Width = 18
			});

			allColumns.Add(new JQGridColumn
			{
				DataField = "Memo",
				HeaderText = "Clause Text",
				DataType = typeof(string),
				Width = 80,

			});

			allColumns.Add(new JQGridColumn
			{
				DataField = "JurisList",
				HeaderText = "Jurisdiction(s)",
				Editable = false,
				DataType = typeof(string),
				Sortable = true,
				Width = 35
			});

			allColumns.Add(new JQGridColumn
			{
				DataField = "FunctionalRoleList",
				HeaderText = "Technical Category/Categories",
				DataType = typeof(string),
				Width = 45
			});

			return allColumns;
		}
	}
}