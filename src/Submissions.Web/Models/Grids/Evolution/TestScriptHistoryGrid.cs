﻿using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;

namespace Submissions.Web.Models.Grids.Evolution
{
	using Submissions.Common.JQGrid;
	using System;
	using Trirand.Web.Mvc;
	public class TestScriptHistoryGrid
	{
		public TestScriptHistoryGrid()
		{
			this.AllColumns = GetAllColumns();
			this.Grid = new JQGrid
			{
				ID = "TestScriptHistoryGrid",
				Columns = this.AllColumns.Cast<JQGridColumn>().ToList(),
				DataUrl = new UrlHelper(((MvcHandler)HttpContext.Current.Handler).RequestContext).Action("TestScriptHistoryGrid", "GridsAjaxEvolution", new { Area = "" }),
				AutoWidth = true,
				Width = Unit.Percentage(100),
				Height = Unit.Percentage(100),
				ToolBarSettings = new ToolBarSettings
				{
					ShowRefreshButton = true,
					ToolBarPosition = ToolBarPosition.Top,
					ShowSearchToolBar = true
				},
				PagerSettings = new PagerSettings
				{
					PageSize = 100,
					PageSizeOptions = "[]"
				},
				AppearanceSettings = new AppearanceSettings
				{
					AlternateRowBackground = true,
					HighlightRowsOnHover = true,
					ShowFooter = false
				},
				SortSettings = new SortSettings
				{
					InitialSortColumn = "CreatedOn",
					InitialSortDirection = SortDirection.Desc
				},
				ClientSideEvents = new ClientSideEvents
				{
					BeforeAjaxRequest = "gridBeforeAjaxRequest"
				},
				RenderingMode = RenderingMode.Optimized
			};
		}

		public JQGrid Grid { get; set; }
		public List<JQGridColumnDynamic> AllColumns { private set; get; }
		public List<JQGridColumnDynamic> GetAllColumns()
		{
			var Columns = new List<JQGridColumnDynamic>
			{
				new JQGridColumnDynamic
				{
					DataField = "TestCase",
					HeaderText = "Test Case",
					DataType = typeof(string),
					Width = 80,
				},

				new JQGridColumnDynamic
				{
					DataField = "CreatedOn",
					HeaderText = "Created On",
					DataFormatString = "{0:d}",
					DataType = typeof(DateTime),
					Width = 30
				},

				new JQGridColumnDynamic
				{
					DataField = "CreatedBy",
					HeaderText = "Created By",
					DataType = typeof(string),
					Width = 30
				},

				new JQGridColumnDynamic
				{
					DataField = "TableName",
					HeaderText = "Table Name",
					DataType = typeof(string),
					Width = 40
				},

				new JQGridColumnDynamic
				{
					DataField = "Description",
					HeaderText = "Description",
					DataType = typeof(string),
					Width = 30
				},

				new JQGridColumnDynamic
				{
					DataField = "NormalizedID",
					HeaderText = "Normalized ID",
					DataType = typeof(string),
					Width = 20
				},

				new JQGridColumnDynamic
				{
					DataField = "Old",
					HeaderText = "Old",
					DataType = typeof(string)

				},

				new JQGridColumnDynamic
				{
					DataField = "New",
					HeaderText = "New",
					DataType = typeof(string),
					Width = 120,
					Formatter = new CustomFormatter { FormatFunction = "ShowDiff"}
				},

				new JQGridColumnDynamic
				{
					DataField = "Link1",
					HeaderText = "Link1",
					DataType = typeof(string),
					Width = 30,
				},

				new JQGridColumnDynamic
				{
					DataField = "Link1NormalizedID",
					HeaderText = "Link1 NormalizedID",
					DataType = typeof(string),
					Width = 35,
				},

				new JQGridColumnDynamic
				{
					DataField = "Link1Content",
					HeaderText = "Link1 Content",
					DataType = typeof(string),
					Width = 80,

				},

				new JQGridColumnDynamic
				{
					DataField = "Link2",
					HeaderText = "Link2",
					DataType = typeof(string),
					Width = 25,
				},

				new JQGridColumnDynamic
				{
					DataField = "Link2NormalizedID",
					HeaderText = "Link2 NormalizedID",
					DataType = typeof(string),
					Width = 35,
				},

				new JQGridColumnDynamic
				{
					DataField = "Link2Content",
					HeaderText = "Link2 Content",
					DataType = typeof(string),
					Width = 80,
				}
			};
			return Columns;
		}
	}

	public class ContentChangesGrid : TestScriptHistoryGrid
	{
		public ContentChangesGrid()
		{
			this.Grid.ID = "ContentChangesGrid";
			this.Grid.SortSettings = new SortSettings
			{
				InitialSortColumn = "CreatedOn",
				InitialSortDirection = SortDirection.Desc
			};

			this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.DataField == "Link1")].Visible = false;
			this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.DataField == "Link1NormalizedID")].Visible = false;
			this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.DataField == "Link1Content")].Visible = false;
			this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.DataField == "Link2")].Visible = false;
			this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.DataField == "Link2NormalizedID")].Visible = false;
			this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.DataField == "Link2Content")].Visible = false;
			this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.DataField == "Old")].Visible = false;
			this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.DataField == "TestCase")].Visible = false;
		}
	}

	public class LinkageChangesGrid : TestScriptHistoryGrid
	{
		public LinkageChangesGrid()
		{
			this.Grid.ID = "LinkageChangesGrid";
			this.Grid.SortSettings = new SortSettings
			{
				InitialSortColumn = "CreatedOn",
				InitialSortDirection = SortDirection.Desc
			};

			this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.DataField == "Description")].Visible = false;
			this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.DataField == "NormalizedID")].Visible = false;
			this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.DataField == "Old")].Visible = false;
			this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.DataField == "New")].Visible = false;
			this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.DataField == "TestCase")].Visible = false;

		}
	}

	public class NevadaChangesGrid : TestScriptHistoryGrid
	{
		public NevadaChangesGrid()
		{
			this.Grid.ID = "NevadaChangesGrid";
			this.Grid.SortSettings = new SortSettings
			{
				InitialSortColumn = "CreatedOn",
				InitialSortDirection = SortDirection.Desc
			};

			this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.DataField == "TestCase")].Visible = false;
			this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.DataField == "Link1")].Visible = false;
			this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.DataField == "Link1NormalizedID")].Visible = false;
			this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.DataField == "Link1Content")].Visible = false;
			this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.DataField == "Link2")].Visible = false;
			this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.DataField == "Link2NormalizedID")].Visible = false;
			this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.DataField == "Link2Content")].Visible = false;
			this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.DataField == "Old")].Visible = false;
			this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.DataField == "TableName")].Visible = false;
		}
	}
	public class NevadaSideGrid
	{
		public NevadaSideGrid()
		{
			this.AllColumns = GetAllColumns();
			this.Grid = new JQGrid
			{
				ID = "SimpleTestCaseGrid",
				Columns = this.AllColumns.Cast<JQGridColumn>().ToList(),
				DataUrl = new UrlHelper(((MvcHandler)HttpContext.Current.Handler).RequestContext).Action("SimpleTestCaseGrid", "GridsAjaxEvolution", new { Area = "" }),
				Height = Unit.Percentage(100),
				AutoWidth = false,
				ShrinkToFit = true,
				ToolBarSettings = new ToolBarSettings
				{
					ShowRefreshButton = true,
					ToolBarPosition = ToolBarPosition.Top,
					ShowSearchToolBar = true
				},
				PagerSettings = new PagerSettings
				{
					PageSize = 100,
					PageSizeOptions = "[]"
				},
				AppearanceSettings = new AppearanceSettings
				{
					AlternateRowBackground = true,
					HighlightRowsOnHover = true,
					ShowFooter = false
				},
				SortSettings = new SortSettings
				{
					InitialSortColumn = "TestCase",
					InitialSortDirection = SortDirection.Desc
				},
				ClientSideEvents = new ClientSideEvents
				{
					BeforeAjaxRequest = "gridBeforeAjaxRequest",
					RowSelect = "SideTableRowClick"
				},
				RenderingMode = RenderingMode.Optimized,
			};
		}
		public JQGrid Grid { get; set; }
		public List<JQGridColumnDynamic> AllColumns { private set; get; }
		public List<JQGridColumnDynamic> GetAllColumns()
		{
			var Columns = new List<JQGridColumnDynamic>
			{
				new JQGridColumnDynamic
				{
					DataField = "TestCase",
					HeaderText = "Test Case",
					DataType = typeof(string),
					CssClass  = "TestCaseName"
				}
			};
			return Columns;
		}


	}
	public class TestScriptHistoryRecord
	{
		public DateTime? CreatedOn { get; set; }
		public string CreatedBy { get; set; }
		public string TableName { get; set; }
		public string Description { get; set; }
		public string Link1 { get; set; }
		public string Link1NormalizedID { get; set; }
		public string Link1Content { get; set; }
		public string Link2 { get; set; }
		public string Link2NormalizedID { get; set; }
		public string Link2Content { get; set; }
		public string TestCase { get; set; }
	}
}