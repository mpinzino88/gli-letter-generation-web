﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;

namespace Submissions.Web.Models.Grids.PointClick
{
	using Trirand.Web.Mvc;

	public class TransferRequestDetailGrid
	{
		public TransferRequestDetailGrid()
		{
			Grid = new JQGrid
			{
				ID = "TransferRequestDetailGrid",
				Columns = LoadColumns(),
				DataUrl = new UrlHelper(((MvcHandler)HttpContext.Current.Handler).RequestContext).Action("LoadTransferRequestDetailGrid", "GridsAjax", new { Area = "" }),
				AutoWidth = true,
				Width = Unit.Percentage(100),
				Height = Unit.Percentage(100),
				MultiSelect = true,
				ToolBarSettings = new ToolBarSettings
				{
					ToolBarPosition = ToolBarPosition.Hidden
				},
				PagerSettings = new PagerSettings
				{
					PageSize = 3000,
					PageSizeOptions = "[]"
				},
				AppearanceSettings = new AppearanceSettings
				{
					AlternateRowBackground = true,
					HighlightRowsOnHover = true,
					ShowFooter = false
				},
				SortSettings = new SortSettings
				{
					InitialSortColumn = "SubmissionFileNumber, JurisdictionName"
				},
				ClientSideEvents = new ClientSideEvents
				{
					BeforeAjaxRequest = "gridBeforeAjaxRequest"
				},
				RenderingMode = RenderingMode.Optimized
			};
		}

		public JQGrid Grid { get; set; }
		public void ShowRDAPPins(bool value)
		{
			this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.DataField == "ClientNumber")].Visible = value;
		}

		private List<JQGridColumn> LoadColumns()
		{
			var columns = new List<JQGridColumn>();

			columns.Add(new JQGridColumn
			{
				DataField = "Id",
				PrimaryKey = true,
				Editable = false,
				Visible = false
			});

			columns.Add(new JQGridColumn
			{
				DataField = "SubmissionId",
				Visible = false
			});

			columns.Add(new JQGridColumn
			{
				DataField = "JurisdictionName",
				HeaderText = "Jurisdiction",
				DataType = typeof(string)
			});

			columns.Add(new JQGridColumn
			{
				DataField = "SubmissionStatus",
				HeaderText = "Status",
				DataType = typeof(string),
				Width = 70
			});

			columns.Add(new JQGridColumn
			{
				DataField = "SubmissionManufacturer",
				HeaderText = "Manufacturer",
				DataType = typeof(string)
			});

			columns.Add(new JQGridColumn
			{
				DataField = "Notes",
				HeaderText = "Notes",
				DataType = typeof(string),
				Formatter = new CustomFormatter { FormatFunction = "notesLink" },
				Width = 60
			});

			columns.Add(new JQGridColumn
			{
				DataField = "SubmissionFileNumber",
				HeaderText = "File Number",
				DataType = typeof(string),
				Formatter = new CustomFormatter { FormatFunction = "fileNumberLink2" }
			});

			columns.Add(new JQGridColumn
			{
				DataField = "SubmissionIdNumber",
				HeaderText = "ID Number",
				DataType = typeof(string)
			});

			columns.Add(new JQGridColumn
			{
				DataField = "SubmissionDateCode",
				HeaderText = "Date Code",
				DataType = typeof(string)
			});

			columns.Add(new JQGridColumn
			{
				DataField = "SubmissionVersion",
				HeaderText = "Version",
				DataType = typeof(string)
			});

			columns.Add(new JQGridColumn
			{
				DataField = "RequestType",
				HeaderText = "Request Type",
				DataType = typeof(string)
			});

			columns.Add(new JQGridColumn
			{
				DataField = "ClientNumber",
				HeaderText = "RDAP Pin",
				DataType = typeof(string),
				Visible = false,
				Width = 80
			});

			columns.Add(new JQGridColumn
			{
				DataField = "Status",
				HeaderText = "Transfer Status",
				DataType = typeof(string),
				Width = 110
			});

			return columns;
		}
	}

	public class TransferRequestDetailGridRecord
	{
		public Guid Id { get; set; }
		public int? SubmissionId { get; set; }
		public string JurisdictionName { get; set; }
		public string SubmissionStatus { get; set; }
		public string SubmissionManufacturer { get; set; }
		public string Notes { get; set; }
		public string SubmissionFileNumber { get; set; }
		public string SubmissionIdNumber { get; set; }
		public string SubmissionDateCode { get; set; }
		public string SubmissionVersion { get; set; }
		public string RequestType { get; set; }
		public string ClientNumber { get; set; }
		public string Status { get; set; }
		public string Add { get; set; }
	}
}