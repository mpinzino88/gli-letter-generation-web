﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;

namespace Submissions.Web.Models.Grids.PointClick
{
	using Trirand.Web.Mvc;

	public class WithdrawRequestGrid
	{
		public WithdrawRequestGrid()
		{
			Grid = new JQGrid
			{
				ID = "WithdrawRequestGrid",
				Columns = LoadColumns(),
				DataUrl = new UrlHelper(((MvcHandler)HttpContext.Current.Handler).RequestContext).Action("LoadWithdrawRequestGrid", "GridsAjax", new { Area = "" }),
				AutoWidth = true,
				Width = Unit.Percentage(100),
				Height = Unit.Percentage(100),
				ToolBarSettings = new ToolBarSettings
				{
					ShowRefreshButton = true,
					ToolBarPosition = ToolBarPosition.Top
				},
				PagerSettings = new PagerSettings
				{
					PageSize = 100,
					PageSizeOptions = "[]"
				},
				AppearanceSettings = new AppearanceSettings
				{
					AlternateRowBackground = true,
					HighlightRowsOnHover = true,
					ShowFooter = false
				},
				ClientSideEvents = new ClientSideEvents
				{
					BeforeAjaxRequest = "gridBeforeAjaxRequest"
				},
				RenderingMode = RenderingMode.Optimized
			};
		}

		public JQGrid Grid { get; set; }

		private List<JQGridColumn> LoadColumns()
		{
			var columns = new List<JQGridColumn>();

			columns.Add(new JQGridColumn
			{
				DataField = "Id",
				PrimaryKey = true,
				Editable = false,
				Visible = false
			});

			columns.Add(new JQGridColumn
			{
				DataField = "SubmissionId",
				Visible = false
			});

			columns.Add(new JQGridColumn
			{
				DataField = "AddUserId",
				DataType = typeof(int),
				Editable = false,
				Visible = false
			});
			columns.Add(new JQGridColumn
			{
				DataField = "OrderNumber",
				HeaderText = "Order Num",
				DataType = typeof(int),
				Width = 90
			});
			columns.Add(new JQGridColumn
			{
				DataField = "RequestDate",
				HeaderText = "Request Date",
				DataType = typeof(DateTime),
				Width = 150,
				DataFormatString = "{0:g}",
			});
			columns.Add(new JQGridColumn
			{
				DataField = "SubmissionFileNumber",
				HeaderText = "File Number",
				DataType = typeof(string),
				Formatter = new CustomFormatter { FormatFunction = "fileNumberLink2" },
				Width = 175
			});
			columns.Add(new JQGridColumn
			{
				DataField = "SubmissionManufacturer",
				HeaderText = "Manufacturer",
				DataType = typeof(string),
				Width = 225
			});
			columns.Add(new JQGridColumn
			{
				DataField = "SubmissionIdNumber",
				HeaderText = "ID Number",
				DataType = typeof(string)
			});
			columns.Add(new JQGridColumn
			{
				DataField = "SubmissionVersion",
				HeaderText = "Version",
				DataType = typeof(string),
				Width = 120
			});
			columns.Add(new JQGridColumn
			{
				DataField = "SubmissionGameName",
				HeaderText = "Game Name",
				DataType = typeof(string)
			});
			columns.Add(new JQGridColumn
			{
				DataField = "QAMemo",
				HeaderText = "QA Memo",
				DataType = typeof(string)
			});
			columns.Add(new JQGridColumn
			{
				DataField = "Status",
				HeaderText = "Status",
				DataType = typeof(string),
				Width = 100
			});
			columns.Add(new JQGridColumn
			{
				DataField = "EditLink",
				HeaderText = " ",
				Formatter = new CustomFormatter { FormatFunction = "EditLink" },
				Searchable = false,
				Sortable = false,
				Width = 75,
				TextAlign = TextAlign.Right,
				CssClass = "padright"
			});
			return columns;
		}
	}

	public class WithdrawRequestGridRecord
	{
		public Guid Id { get; set; }
		public int SubmissionId { get; set; }
		public int AddUserId { get; set; }
		public string QAMemo { get; set; }
		public DateTime RequestDate { get; set; }
		public int OrderNumber { get; set; }
		public DateTime? ProcessDate { get; set; }
		public string SubmissionFileNumber { get; set; }
		public string SubmissionManufacturer { get; set; }
		public string SubmissionIdNumber { get; set; }
		public string SubmissionVersion { get; set; }
		public string SubmissionGameName { get; set; }
		public string Status { get; set; }
		public string EditLink { get; set; }
	}
}