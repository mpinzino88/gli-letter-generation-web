﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;

namespace Submissions.Web.Models.Grids.PointClick
{
	using Submissions.Common;
	using Trirand.Web.Mvc;

	public class SubmitRequestGrid
	{
		public SubmitRequestGrid()
		{
			Grid = new JQGrid
			{
				ID = "SubmitRequestGrid",
				Columns = LoadColumns(),
				DataUrl = new UrlHelper(((MvcHandler)HttpContext.Current.Handler).RequestContext).Action("loadsubmitrequestgrid", "gridsajax", new { area = "" }),
				AutoWidth = true,
				Width = Unit.Percentage(100),
				Height = Unit.Percentage(100),
				ToolBarSettings = new ToolBarSettings
				{
					ShowRefreshButton = true,
					ShowSearchToolBar = true,
					ToolBarPosition = ToolBarPosition.Top
				},
				PagerSettings = new PagerSettings
				{
					PageSize = 100,
					PageSizeOptions = "[]"
				},
				AppearanceSettings = new AppearanceSettings
				{
					AlternateRowBackground = true,
					HighlightRowsOnHover = true,
					ShowFooter = false
				},
				SortSettings = new SortSettings
				{
					InitialSortColumn = "SubmitDate",
					InitialSortDirection = SortDirection.Desc
				},
				ClientSideEvents = new ClientSideEvents
				{
					BeforeAjaxRequest = "gridBeforeAjaxRequest"
				},
				RenderingMode = RenderingMode.Optimized
			};
		}

		public JQGrid Grid { get; set; }

		private List<JQGridColumn> LoadColumns()
		{
			var columns = new List<JQGridColumn>();

			columns.Add(new JQGridColumn
			{
				DataField = "Id",
				PrimaryKey = true,
				Editable = false,
				Visible = false
			});

			columns.Add(new JQGridColumn
			{
				DataField = "ConfirmId",
				HeaderText = "Confirmation ID",
				DataType = typeof(string),
				Width = 100
			});

			columns.Add(new JQGridColumn
			{
				DataField = "SubmitDate",
				HeaderText = "Request Date",
				DataFormatString = "{0:d}",
				DataType = typeof(DateTime),
				Width = 80
			});

			columns.Add(new JQGridColumn
			{
				DataField = "ContactName",
				HeaderText = "Name",
				DataType = typeof(string)
			});

			columns.Add(new JQGridColumn
			{
				DataField = "ContactEmail",
				HeaderText = "Email",
				DataType = typeof(string)
			});

			columns.Add(new JQGridColumn
			{
				DataField = "ContactPhone",
				HeaderText = "Phone",
				DataType = typeof(string),
				Width = 120
			});

			columns.Add(new JQGridColumn
			{
				DataField = "FileNumber",
				HeaderText = "File Number",
				DataType = typeof(string),
				Width = 90,
				Formatter = new CustomFormatter { FormatFunction = "fileNumberLinkByFileNumber" }
			});

			columns.Add(new JQGridColumn
			{
				DataField = "Status",
				HeaderText = "Status",
				DataType = typeof(string),
				Searchable = true,
				SearchType = SearchType.DropDown,
				SearchToolBarOperation = SearchOperation.IsEqualTo,
				SearchList = (List<SelectListItem>)LookupsStandard.ConvertEnum<SubmitRequestHeaderStatus>(showBlank: true),
				Width = 90
			});

			columns.Add(new JQGridColumn
			{
				DataField = "EditLink",
				HeaderText = " ",
				Formatter = new CustomFormatter { FormatFunction = "editLink" },
				Searchable = false,
				Sortable = false,
				Width = 60,
				TextAlign = TextAlign.Right,
				CssClass = "padright"
			});

			return columns;
		}
	}

	public class SubmitRequestGridRecord
	{
		public int Id { get; set; }
		public string ConfirmId { get; set; }
		public DateTime SubmitDate { get; set; }
		public string ContactName { get; set; }
		public string ContactEmail { get; set; }
		public string ContactPhone { get; set; }
		public string FileNumber { get; set; }
		public string Status { get; set; }
		public string EditLink { get; set; }
	}
}