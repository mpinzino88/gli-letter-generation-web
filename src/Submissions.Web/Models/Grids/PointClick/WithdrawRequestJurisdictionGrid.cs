﻿using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;

namespace Submissions.Web.Models.Grids.PointClick
{
	using Trirand.Web.Mvc;

	public class WithdrawRequestJurisdictionGrid
	{
		public WithdrawRequestJurisdictionGrid()
		{
			Grid = new JQGrid
			{
				ID = "WithdrawRequestJurisdictionGrid",
				Columns = LoadColumns(),
				DataUrl = new UrlHelper(((MvcHandler)HttpContext.Current.Handler).RequestContext).Action("LoadWithdrawRequestJurisdictionGrid", "GridsAjax", new { Area = "" }),
				AutoWidth = true,
				Width = Unit.Percentage(100),
				Height = Unit.Percentage(100),
				MultiSelect = true,
				ToolBarSettings = new ToolBarSettings
				{
					ShowRefreshButton = true,
					ShowSearchToolBar = true,
					ToolBarPosition = ToolBarPosition.Top
				},
				PagerSettings = new PagerSettings
				{
					PageSize = 3000,
					PageSizeOptions = "[]"
				},
				AppearanceSettings = new AppearanceSettings
				{
					AlternateRowBackground = true,
					HighlightRowsOnHover = true,
					ShowFooter = false
				},
				ClientSideEvents = new ClientSideEvents
				{
					BeforeAjaxRequest = "gridBeforeAjaxRequestJurisdiction"
				},
				RenderingMode = RenderingMode.Optimized
			};
		}

		public JQGrid Grid { get; set; }

		private List<JQGridColumn> LoadColumns()
		{
			var columns = new List<JQGridColumn>();

			columns.Add(new JQGridColumn
			{
				DataField = "Id",
				PrimaryKey = true,
				Editable = false,
				Visible = false
			});
			columns.Add(new JQGridColumn
			{
				DataField = "JurisdictionalDataId",
				Editable = false,
				Visible = false
			});
			columns.Add(new JQGridColumn
			{
				DataField = "SubmissionFileNumber",
				HeaderText = "File Number",
				DataType = typeof(int),
				Width = 150
			});
			columns.Add(new JQGridColumn
			{
				DataField = "SubmissionManufacturer",
				HeaderText = "Manufacturer",
				DataType = typeof(string)
			});
			columns.Add(new JQGridColumn
			{
				DataField = "JurisdictionalDataJurisdictionName",
				HeaderText = "Jurisdiction",
				DataType = typeof(string),
				Width = 200
			});
			columns.Add(new JQGridColumn
			{
				DataField = "SubmissionIdNumber",
				HeaderText = "ID Number",
				DataType = typeof(string)
			});
			columns.Add(new JQGridColumn
			{
				DataField = "SubmissionVersion",
				HeaderText = "Version",
				DataType = typeof(string),
				Width = 120
			});
			columns.Add(new JQGridColumn
			{
				DataField = "SubmissionGameName",
				HeaderText = "Game Name",
				DataType = typeof(string),
			});
			columns.Add(new JQGridColumn
			{
				DataField = "SubmissionDateCode",
				HeaderText = "Date Code",
				DataType = typeof(string),
			});
			columns.Add(new JQGridColumn
			{
				DataField = "JurisdictionalDataStatus",
				HeaderText = "Status",
				DataType = typeof(string),
				Width = 110
			});
			return columns;
		}
	}

	public class WithdrawRequestJurisdictionGridRecord
	{
		public int Id { get; set; }
		public int? JurisdictionalDataId { get; set; }
		public string SubmissionFileNumber { get; set; }
		public string SubmissionManufacturer { get; set; }
		public string SubmissionIdNumber { get; set; }
		public string SubmissionVersion { get; set; }
		public string SubmissionGameName { get; set; }
		public string SubmissionDateCode { get; set; }
		public string SubmissionChipType { get; set; }
		public string JurisdictionalDataJurisdictionName { get; set; }
		public string JurisdictionalDataStatus { get; set; }
	}
}