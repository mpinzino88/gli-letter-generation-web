﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;

namespace Submissions.Web.Models.Grids.PointClick
{
	using Submissions.Common;
	using Trirand.Web.Mvc;

	public class TransferRequestGrid
	{
		public TransferRequestGrid()
		{
			Grid = new JQGrid
			{
				ID = "TransferRequestGrid",
				Columns = LoadColumns(),
				DataUrl = new UrlHelper(((MvcHandler)HttpContext.Current.Handler).RequestContext).Action("LoadTransferRequestGrid", "GridsAjax", new { Area = "" }),
				AutoWidth = true,
				Width = Unit.Percentage(100),
				Height = Unit.Percentage(100),
				ToolBarSettings = new ToolBarSettings
				{
					ShowRefreshButton = true,
					ShowSearchToolBar = true,
					ToolBarPosition = ToolBarPosition.Top
				},
				PagerSettings = new PagerSettings
				{
					PageSize = 100,
					PageSizeOptions = "[]"
				},
				AppearanceSettings = new AppearanceSettings
				{
					AlternateRowBackground = true,
					HighlightRowsOnHover = true,
					ShowFooter = false
				},
				ClientSideEvents = new ClientSideEvents
				{
					BeforeAjaxRequest = "gridBeforeAjaxRequest"
				},
				RenderingMode = RenderingMode.Optimized
			};
		}

		public JQGrid Grid { get; set; }

		private List<JQGridColumn> LoadColumns()
		{
			var columns = new List<JQGridColumn>();

			columns.Add(new JQGridColumn
			{
				DataField = "Id",
				PrimaryKey = true,
				Editable = false,
				Visible = false
			});

			columns.Add(new JQGridColumn
			{
				DataField = "OrderNumber",
				HeaderText = "Order Number",
				DataType = typeof(int),
				SearchToolBarOperation = SearchOperation.IsEqualTo,
				Width = 100
			});

			columns.Add(new JQGridColumn
			{
				DataField = "RequestDate",
				HeaderText = "Request Date",
				DataFormatString = "{0:d}",
				DataType = typeof(DateTime),
				Width = 80
			});

			columns.Add(new JQGridColumn
			{
				DataField = "ContactName",
				HeaderText = "Name",
				DataType = typeof(string)
			});

			columns.Add(new JQGridColumn
			{
				DataField = "ContactEmail",
				HeaderText = "Email",
				DataType = typeof(string)
			});

			columns.Add(new JQGridColumn
			{
				DataField = "ContactPhone",
				HeaderText = "Phone",
				DataType = typeof(string),
				Width = 120
			});

			columns.Add(new JQGridColumn
			{
				DataField = "RefNum",
				HeaderText = "Project Reference",
				DataType = typeof(string),
				SearchToolBarOperation = SearchOperation.IsEqualTo
			});

			columns.Add(new JQGridColumn
			{
				DataField = "Status",
				HeaderText = "Status",
				DataType = typeof(string),
				Searchable = true,
				SearchType = SearchType.DropDown,
				SearchToolBarOperation = SearchOperation.IsEqualTo,
				SearchList = (List<SelectListItem>)LookupsStandard.ConvertEnum(showBlank: true, enumValues: new TransferRequestHeaderStatus[] { TransferRequestHeaderStatus.Pending, TransferRequestHeaderStatus.Processed }),
				Width = 90
			});

			columns.Add(new JQGridColumn
			{
				DataField = "IsConfirmed",
				Visible = false
			});

			columns.Add(new JQGridColumn
			{
				DataField = "ViewDetails",
				HeaderText = " ",
				Formatter = new CustomFormatter { FormatFunction = "ViewDetailsLink" },
				Searchable = false,
				Sortable = false,
				Width = 105,
				TextAlign = TextAlign.Right,
				CssClass = "padright"
			});

			return columns;
		}
	}

	public class TransferRequestGridRecord
	{
		public Guid Id { get; set; }
		public int OrderNumber { get; set; }
		public DateTime? RequestDate { get; set; }
		public string ContactName { get; set; }
		public string ContactEmail { get; set; }
		public string ContactPhone { get; set; }
		public string RefNum { get; set; }
		public string Status { get; set; }
		public bool? IsConfirmed { get; set; }
		public string ViewDetails { get; set; }
	}
}