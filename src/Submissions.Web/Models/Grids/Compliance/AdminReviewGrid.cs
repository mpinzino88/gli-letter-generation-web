﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;

namespace Submissions.Web.Models.Grids
{
	using Trirand.Web.Mvc;

	public class AdminReviewGrid
	{
		public AdminReviewGrid()
		{
			this.Grid = new JQGrid
			{
				ID = "AdminReviewGrid",
				Columns = LoadColumns(),
				DataUrl = new UrlHelper(((MvcHandler)HttpContext.Current.Handler).RequestContext).Action("LoadCPAdminReviewGrid", "GridsAjax", new { Area = "" }),
				AutoWidth = false,
				Width = Unit.Percentage(100),
				Height = Unit.Percentage(100),
				MultiSelect = true,
				ToolBarSettings = new ToolBarSettings
				{
					ShowRefreshButton = true,
					ToolBarPosition = ToolBarPosition.Top,
					ShowSearchToolBar = true,
				},
				PagerSettings = new PagerSettings
				{
					PageSize = 100,
					PageSizeOptions = "[]"
				},
				AppearanceSettings = new AppearanceSettings
				{
					AlternateRowBackground = true,
					HighlightRowsOnHover = true,
					Caption = "Jurisdictions Held by Compliance"
				},
				SortSettings = new SortSettings
				{
					InitialSortColumn = "ProjectDesc",
					InitialSortDirection = SortDirection.Asc
				},
				ClientSideEvents = new ClientSideEvents
				{
					BeforeAjaxRequest = "adminReviewGridBeforeAjaxRequest"
				},
				RenderingMode = RenderingMode.Optimized
			};
		}

		public JQGrid Grid { get; set; }

		private List<JQGridColumn> LoadColumns()
		{
			var columns = new List<JQGridColumn>();

			columns.Add(new JQGridColumn
			{
				DataField = "Id",
				PrimaryKey = true,
				Editable = false,
				Visible = false
			});

			columns.Add(new JQGridColumn
			{
				DataField = "ProjectId",
				Visible = false
			});

			columns.Add(new JQGridColumn
			{
				DataField = "IsProjectMerged",
				DataType = typeof(Boolean),
				Visible = false
			});

			columns.Add(new JQGridColumn
			{
				DataField = "ProjectDesc",
				HeaderText = "ProjectDesc",
				DataType = typeof(string),
				Formatter = new CustomFormatter { FormatFunction = "formatProjectDesc" },
				Visible = false,

			});

			columns.Add(new JQGridColumn
			{
				DataField = "FileNumber",
				HeaderText = "FileNumber",
				DataType = typeof(string),
				Width = 130,
				Formatter = new CustomFormatter { FormatFunction = "formatFileNumber" }
			});

			columns.Add(new JQGridColumn
			{
				DataField = "IdNumber",
				HeaderText = "ID Number",
				DataType = typeof(string),
				Width = 200
			});

			columns.Add(new JQGridColumn
			{
				DataField = "GameName",
				HeaderText = "Game Name",
				DataType = typeof(string),
				Width = 150
			});

			columns.Add(new JQGridColumn
			{
				DataField = "Version",
				HeaderText = "Version",
				DataType = typeof(string),
				Width = 120
			});

			columns.Add(new JQGridColumn
			{
				DataField = "JurisdictionName",
				HeaderText = "Jurisdiction",
				DataType = typeof(string),
				Width = 120
			});

			columns.Add(new JQGridColumn
			{
				DataField = "JurisdictionId",
				HeaderText = "Jur Id",
				DataType = typeof(string),
				Width = 50
			});

			columns.Add(new JQGridColumn
			{
				DataField = "Status",
				HeaderText = "Jur Status",
				DataType = typeof(string),
				Width = 100
			});

			//columns.Add(new JQGridColumn
			//{
			//	DataField = "CPAcceptDate",
			//	HeaderText = "CP Accept",
			//	//DataFormatString = "{0:d}",
			//	DataType = typeof(DateTime),
			//	Width = 130,
			//	Searchable = false
			//});

			columns.Add(new JQGridColumn
			{
				DataField = "TestLab",
				HeaderText = "Test Lab",
				DataType = typeof(string),
				Width = 70,
				Searchable = false
			});

			columns.Add(new JQGridColumn
			{
				DataField = "QAAcceptDate",
				HeaderText = "QA Accept",
				//DataFormatString = "{0:d}",
				DataType = typeof(DateTime),
				Width = 130,
				Searchable = false
			});

			columns.Add(new JQGridColumn
			{
				DataField = "QACompleteDate",
				HeaderText = "QA Complete",
				//DataFormatString = "{0:d}",
				DataType = typeof(DateTime),
				Width = 130,
				Searchable = false
			});

			columns.Add(new JQGridColumn
			{
				DataField = "ENAcceptDate",
				HeaderText = "EN Accept",
				//DataFormatString = "{0:d}",
				DataType = typeof(DateTime),
				Width = 130,
				Searchable = false
			});

			columns.Add(new JQGridColumn
			{
				DataField = "ENCompleteDate",
				HeaderText = "EN Complete",
				//DataFormatString = "{0:d}",
				DataType = typeof(DateTime),
				Width = 130,
				Searchable = false
			});

			return columns;
		}
	}


	public class AdminReviewJurisdictionGridRecord
	{
		public int Id { get; set; }
		public DateTime? CPAcceptDate { get; set; }
		public int? UserId { get; set; }
		public string FileNumber { get; set; }
		public int? ProjectId { get; set; }
		public string ProjectName { get; set; }
		public string ProjectHolder { get; set; }
		public bool IsProjectMerged { get; set; }
		public string IdNumber { get; set; }
		public string GameName { get; set; }
		public string Version { get; set; }
		public string JurisdictionName { get; set; }
		public string JurisdictionId { get; set; }
		public string Status { get; set; }
		public DateTime? ENAcceptDate { get; set; }
		public DateTime? ENCompleteDate { get; set; }
		public DateTime? QAAcceptDate { get; set; }
		public DateTime? QACompleteDate { get; set; }
		public string TestLab { get; set; }
		public string ProjectDesc { get; set; }

	}
}