﻿using Submissions.Common;
using Submissions.Common.JQGrid;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;


namespace Submissions.Web.Models.Grids.Tools
{
    using Trirand.Web.Mvc;
    public class PDFEmailRulesGrid
    {
        public JQGrid Grid { get; set; }
        public PDFEmailRulesGrid()
        {
            this.Grid = new JQGrid
            {
                ID = "PDFEmailRulesGrid",
                Columns = InitializeAllColumns(),
                DataUrl = new UrlHelper(((MvcHandler)HttpContext.Current.Handler).RequestContext).Action("LoadPDFRulesGrid", "GridsAjax", new { Area = "" }),
                AutoWidth = false,
                Width = Unit.Percentage(100),
                Height = Unit.Percentage(100),
                ToolBarSettings = new ToolBarSettings
                {
                    ShowRefreshButton = true,
                    ToolBarPosition = ToolBarPosition.Top,
                    ShowSearchToolBar = true
                },
                PagerSettings = new PagerSettings
                {
                    PageSize = 100,
                    PageSizeOptions = "[]"
                },
                AppearanceSettings = new AppearanceSettings
                {
                    AlternateRowBackground = true,
                    HighlightRowsOnHover = true,
                    ShowFooter = false
                },
                SortSettings = new SortSettings
                {
                    InitialSortColumn = "Description",
                    InitialSortDirection = SortDirection.Asc
                },
                ClientSideEvents = new ClientSideEvents
                {
                    BeforeAjaxRequest = "gridBeforeAjaxRequest"
                },
                RenderingMode = RenderingMode.Optimized
            };
        }

        private List<JQGridColumn> InitializeAllColumns()
        {
            var allColumns = new List<JQGridColumn>();

            allColumns.Add(new JQGridColumn
            {
                DataField = "RuleId",
                PrimaryKey = true,
                Editable = false,
                Visible = false
            });

            allColumns.Add(new JQGridColumn
            {
                DataField = "Scheduled",
                Editable = false,
                Visible = false
            });

            allColumns.Add(new JQGridColumn
            {
                DataField = "Description",
                HeaderText = "Description",
                DataType = typeof(string),
                Sortable = true,
                Width = 150,
                Searchable = true
            });

            allColumns.Add(new JQGridColumn
            {
                DataField = "CreateDate",
                HeaderText = "Created On",
                DataFormatString = "{0:d}",
                DataType = typeof(DateTime),
                Sortable = true,
                Width = 80
            });

            allColumns.Add(new JQGridColumn
            {
                DataField = "EditedBy",
                HeaderText = "Edited By",
                DataType = typeof(string),
                Sortable = true,
                Width = 80
            });

            allColumns.Add(new JQGridColumn
            {
                DataField = "EditDate",
                HeaderText = "Edited On",
                DataFormatString = "{0:d}",
                DataType = typeof(DateTime),
                Sortable = true,
                Width = 80
            });


            return allColumns;
        }
    }


    public class PDFEmailRulesGridRecord
    {
        public int RuleId { get; set; }
        public bool Scheduled { get; set; }
        public string Description { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime? EditDate { get; set; }
        public string CreatedBy { get; set; }
        public string EditedBy { get; set; }
    }


}