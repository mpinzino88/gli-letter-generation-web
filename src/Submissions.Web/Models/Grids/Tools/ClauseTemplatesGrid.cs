﻿using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;


namespace Submissions.Web.Models.Grids.Tools
{
	using Trirand.Web.Mvc;
	public class ClauseTemplatesGrid
	{
		public JQGrid Grid { get; set; }
		public ClauseTemplatesGrid()
		{
			this.Grid = new JQGrid
			{
				ID = "ClauseTemplatesGrid",
				Columns = InitializeAllColumns(),
				DataUrl = new UrlHelper(((MvcHandler)HttpContext.Current.Handler).RequestContext).Action("LoadClauseTemplates", "GridsAjax", new { Area = "" }),
				AutoWidth = false,
				Width = Unit.Percentage(100),
				Height = Unit.Percentage(100),
				ToolBarSettings = new ToolBarSettings
				{
					ShowRefreshButton = true,
					ToolBarPosition = ToolBarPosition.Top,
					ShowSearchToolBar = true
				},
				PagerSettings = new PagerSettings
				{
					PageSize = 100,
					PageSizeOptions = "[]"
				},
				AppearanceSettings = new AppearanceSettings
				{
					AlternateRowBackground = true,
					HighlightRowsOnHover = true,
					ShowFooter = false
				},
				SortSettings = new SortSettings
				{
					InitialSortColumn = "Label",
					InitialSortDirection = SortDirection.Asc
				},
				ClientSideEvents = new ClientSideEvents
				{
					BeforeAjaxRequest = "gridBeforeAjaxRequest"
				},
				RenderingMode = RenderingMode.Optimized
			};
		}

		private List<JQGridColumn> InitializeAllColumns()
		{
			var allColumns = new List<JQGridColumn>
			{
				new JQGridColumn
				{
					DataField = "Id",
					PrimaryKey = true,
					Editable = false,
					Visible = false
				},

				new JQGridColumn
				{
					DataField = "Inactive",
					Editable = false,
					Visible = false
				},

				new JQGridColumn
				{
					DataField = "Manufacturer",
					HeaderText = "Description",
					DataType = typeof(string),
					Sortable = true,
					Width = 100,
					Searchable = true
				},

				new JQGridColumn
				{
					DataField = "Jurisdiction",
					HeaderText = "Jurisdiction",
					DataType = typeof(string),
					Sortable = true,
					Width = 50,
					Searchable = true

				},

				new JQGridColumn
				{
					DataField = "Label",
					HeaderText = "Name",
					DataType = typeof(string),
					Sortable = true,
					Width = 50,
					Searchable = true
				},
			};


			return allColumns;
		}
	}

	public class ClauseTemplateModel
	{
		public int Id { get; set; }
		/// <summary>
		/// Always true right now, but we may display inactive templates later
		/// </summary>
		public bool Inactive { get; set; }
		public string ManufacturerId { get; set; }
		public string Manufacturer { get; set; }
		public int JurisdictionId { get; set; }
		public string Jurisdiction { get; set; }
		public string Label { get; set; }

	}
}