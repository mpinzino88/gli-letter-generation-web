﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;

namespace Submissions.Web.Models.Grids.Tools
{
	using Trirand.Web.Mvc;

	public class ExternalDocumentsGrid
	{
		public ExternalDocumentsGrid()
		{
			this.Grid = new JQGrid
			{
				ID = "ExternalDocumentsGrid",
				Columns = LoadColumns(),
				DataUrl = new UrlHelper(((MvcHandler)HttpContext.Current.Handler).RequestContext).Action("LoadExternalDocumentsGrid", "GridsAjax", new { Area = "" }),
				AutoWidth = true,
				Width = Unit.Percentage(100),
				Height = Unit.Percentage(100),
				ToolBarSettings = new ToolBarSettings
				{
					ShowRefreshButton = true,
					ShowSearchToolBar = true,
					ToolBarPosition = ToolBarPosition.Top
				},
				PagerSettings = new PagerSettings
				{
					PageSize = 100,
					PageSizeOptions = "[]"
				},
				AppearanceSettings = new AppearanceSettings
				{
					AlternateRowBackground = true,
					HighlightRowsOnHover = true,
					ShowFooter = false
				},
				SortSettings = new SortSettings
				{
					InitialSortColumn = "DocumentDate, ManufacturerCode",
					InitialSortDirectionString = "desc, asc"
				},
				RenderingMode = RenderingMode.Optimized
			};
		}

		public JQGrid Grid { get; set; }

		private List<JQGridColumn> LoadColumns()
		{
			var columns = new List<JQGridColumn>();

			columns.Add(new JQGridColumn
			{
				DataField = "Id",
				PrimaryKey = true,
				Editable = false,
				Visible = false
			});

			columns.Add(new JQGridColumn
			{
				DataField = "DocumentDate",
				HeaderText = "Date",
				DataFormatString = "{0:d}",
				DataType = typeof(DateTime),
				Width = 50
			});

			columns.Add(new JQGridColumn
			{
				DataField = "ManufacturerCode",
				HeaderText = "Manufacturer",
				DataType = typeof(string),
				Width = 50
			});

			columns.Add(new JQGridColumn
			{
				DataField = "Description",
				HeaderText = "Description",
				DataType = typeof(string)
			});

			columns.Add(new JQGridColumn
			{
				DataField = "FileName",
				HeaderText = "File Name",
				DataType = typeof(string),
				Formatter = new CustomFormatter { FormatFunction = "fileNameLink" }
			});

			columns.Add(new JQGridColumn
			{
				DataField = "Edit",
				HeaderText = " ",
				Formatter = new CustomFormatter { FormatFunction = "editLink" },
				Searchable = false,
				Sortable = false,
				Width = 50,
				TextAlign = TextAlign.Right,
				CssClass = "padright"
			});

			return columns;
		}
	}

	public class ExternalDocumentsGridRecord
	{
		public int? Id { get; set; }
		public DateTime DocumentDate { get; set; }
		public string ManufacturerCode { get; set; }
		public string Description { get; set; }
		public string FileName { get; set; }
		public string Edit { get; set; }
	}
}