﻿using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;
using Trirand.Web.Mvc;

namespace Submissions.Web.Models.Grids.GamingGuidelines
{
	public class GamingGuidelinesSportsBettingGrid
	{
		public GamingGuidelinesSportsBettingGrid()
		{
			this.Grid = new JQGrid()
			{
				ID = "GamingGuidelinesSportsBettingGrid",
				Columns = InitializeAllColumns(),
				DataUrl = new UrlHelper(((MvcHandler)HttpContext.Current.Handler).RequestContext).Action("LoadGamingGuidelineSportsBettingGrid", "GridsAjax", new { Area = "" }),
				ShrinkToFit = false,
				AutoWidth = true,
				Width = Unit.Percentage(100),
				Height = Unit.Percentage(100),
				ToolBarSettings = new ToolBarSettings
				{
					ShowRefreshButton = true,
					ShowSearchToolBar = true,
					ToolBarPosition = ToolBarPosition.Top
				},
				PagerSettings = new Trirand.Web.Mvc.PagerSettings
				{
					PageSize = 1000,
					PageSizeOptions = "[]"
				},
				AppearanceSettings = new AppearanceSettings
				{
					AlternateRowBackground = true,
					HighlightRowsOnHover = true,
					ShowFooter = false
				},
				ClientSideEvents = new ClientSideEvents
				{
					BeforeAjaxRequest = "gridBeforeAjaxRequest"
				},
				RenderingMode = RenderingMode.Optimized,
				SortSettings =
				{
					InitialSortColumn = "JurisdictionName"
				}
			};
		}
		public JQGrid Grid { get; set; }
		private List<JQGridColumn> InitializeAllColumns()
		{
			var allColumns = new List<JQGridColumn>();
			allColumns.Add(new JQGridColumn
			{
				DataField = "Id",
				PrimaryKey = true,
				Editable = false,
				Visible = false
			});
			allColumns.Add(new JQGridColumn
			{
				DataField = "JurisdictionName",
				HeaderText = "Jurisdiction",
				DataType = typeof(string),
				Width = 200
			});
			allColumns.Add(new JQGridColumn
			{
				DataField = "Location",
				HeaderText = "Location",
				Width = 100,
				DataType = typeof(string)
			});
			allColumns.Add(new JQGridColumn
			{
				DataField = "ITLRequired",
				HeaderText = "ITL Required",
				DataType = typeof(string),
				Width = 100
			});
			allColumns.Add(new JQGridColumn
			{
				DataField = "AuditRequired",
				HeaderText = "Audit Required",
				DataType = typeof(string),
				Width = 100
			});
			allColumns.Add(new JQGridColumn
			{
				DataField = "Online",
				HeaderText = "Online",
				Width = 100,
				DataType = typeof(string)
			});
			allColumns.Add(new JQGridColumn
			{
				DataField = "LimitOnEventTypeAllowed",
				HeaderText = "Limit on Event Type Allowed",
				Width = 200,
				DataType = typeof(string)
			});
			allColumns.Add(new JQGridColumn
			{
				DataField = "MaxCaps",
				HeaderText = "Max Caps",
				Width = 100,
				DataType = typeof(string)
			});
			allColumns.Add(new JQGridColumn
			{
				DataField = "ChangeManagementSystem",
				HeaderText = "Change Mgmt System",
				Width = 200,
				DataType = typeof(string)
			});
			allColumns.Add(new JQGridColumn
			{
				DataField = "DataVaults",
				HeaderText = "Data Vaults",
				Width = 100
			});
			allColumns.Add(new JQGridColumn
			{
				DataField = "DataRetentionRequirement",
				HeaderText = "Data Retention Requirement",
				Width = 200,
				DataType = typeof(string)
			});
			allColumns.Add(new JQGridColumn
			{
				DataField = "RetentionPeriod",
				HeaderText = "Retention Period",
				Width = 120,
				DataType = typeof(string)
			});
			allColumns.Add(new JQGridColumn
			{
				DataField = "TaxPercentage",
				HeaderText = "Tax %",
				Width = 100,
				DataType = typeof(float),
				Formatter = new CustomFormatter { FormatFunction = "formatTaxPercentageField" }
			});
			allColumns.Add(new JQGridColumn
			{
				DataField = "TaxNotes",
				HeaderText = "Tax Notes",
				Width = 100,
				DataType = typeof(string)
			});
			allColumns.Add(new JQGridColumn
			{
				DataField = "ServerLocationRequirement",
				HeaderText = "Server Location Requirement",
				Width = 200,
				DataType = typeof(string)
			});
			allColumns.Add(new JQGridColumn
			{
				DataField = "LiabilityAmountRequired",
				HeaderText = "Liability Amount Required",
				Width = 200,
				DataType = typeof(string)
			});
			allColumns.Add(new JQGridColumn
			{
				DataField = "LiabilityNote",
				HeaderText = "Liability Note",
				Width = 100,
				DataType = typeof(string)
			});
			allColumns.Add(new JQGridColumn
			{
				DataField = "AnonymousPlayAllowed",
				HeaderText = "Anonymous Play Allowed",
				Width = 175,
				DataType = typeof(string)
			});
			allColumns.Add(new JQGridColumn
			{
				DataField = "GeneralNotes",
				HeaderText = "General Notes",
				Width = 100,
				DataType = typeof(string)
			});
			allColumns.Add(new JQGridColumn
			{
				DataField = "Comments",
				HeaderText = "Comments",
				Width = 100,
				DataType = typeof(string)
			});
			allColumns.Add(new JQGridColumn
			{
				DataField = "Edit",
				HeaderText = " ",
				Formatter = new CustomFormatter { FormatFunction = "editLink" },
				Searchable = false,
				Sortable = false,
				Width = 100,
				TextAlign = Trirand.Web.Mvc.TextAlign.Right,
				CssClass = "padright"
			});
			return allColumns;
		}
	}

	public class GamingGuidelinesSportsBettingGridRecord
	{
		public int Id { get; set; }
		public string JurisdictionName { get; set; }
		public int JurisdictionId { get; set; }
		public string Location { get; set; }
		public int? LocationId { get; set; }
		public string ITLRequired { get; set; }
		public string AuditRequired { get; set; }
		public string AuditNote { get; set; }
		public string LandBased { get; set; }
		public string Online { get; set; }
		public string LimitOnEventTypeAllowed { get; set; }
		public string MaxCaps { get; set; }
		public string ChangeManagementSystem { get; set; }
		public string DataVaults { get; set; }
		public string DataRetentionRequirement { get; set; }
		public string RetentionPeriod { get; set; }
		public double? TaxPercentage { get; set; }
		public string TaxNotes { get; set; }
		public string ServerLocationRequirement { get; set; }
		public string LiabilityAmountRequired { get; set; }
		public string LiabilityNote { get; set; }
		public string AnonymousPlayAllowed { get; set; }
		public string GeneralNotes { get; set; }
		public string Comments { get; set; }
		public string Edit { get; set; }

	}

}