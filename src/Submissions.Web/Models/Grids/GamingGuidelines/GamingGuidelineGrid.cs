﻿using EF.Submission;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;
using Trirand.Web.Mvc;

namespace Submissions.Web.Models.Grids.GamingGuidelines
{
	public class GamingGuidelineGrid
	{
		public GamingGuidelineGrid(GamingGuidelineUserTemplate userTemplate)
		{
			this.Grid = new JQGrid
			{
				ID = "GamingGuidelineGrid",
				Columns = InitializeAllColumns(userTemplate),
				DataUrl = new UrlHelper(((MvcHandler)HttpContext.Current.Handler).RequestContext).Action("LoadGamingGuidelineGrid", "GridsAjax", new { Area = "" }),
				ShrinkToFit = false,
				AutoWidth = true,
				Width = Unit.Percentage(100),
				Height = Unit.Percentage(100),
				ToolBarSettings = new ToolBarSettings
				{
					ShowRefreshButton = true,
					ShowSearchToolBar = true,
					ToolBarPosition = ToolBarPosition.Top
				},
				PagerSettings = new Trirand.Web.Mvc.PagerSettings
				{
					PageSize = 500,
					PageSizeOptions = "[]"
				},
				AppearanceSettings = new AppearanceSettings
				{
					AlternateRowBackground = true,
					HighlightRowsOnHover = true,
					ShowFooter = false
				},
				ClientSideEvents = new ClientSideEvents
				{
					BeforeAjaxRequest = "gridBeforeAjaxRequest"
				},
				RenderingMode = RenderingMode.Optimized,
			};
		}
		public JQGrid Grid { get; set; }
		private List<JQGridColumn> InitializeAllColumns(GamingGuidelineUserTemplate userTemplate)
		{
			var allColumns = new List<JQGridColumn>();
			var newPageUser = userTemplate.UserId == 0;
			allColumns.Add(new JQGridColumn
			{
				DataField = "Id",
				PrimaryKey = true,
				Editable = false,
				Visible = false
			});
			#region Jur Columns
			allColumns.Add(new JQGridColumn
			{
				HeaderText = "Jurisdiction",
				DataField = "Jurisdiction",
				DataType = typeof(string),
				Formatter = new CustomFormatter { FormatFunction = "formatJurisdictionField" },
				Sortable = false,
				Searchable = false,
				Width = 250
			});
			allColumns.Add(new JQGridColumn
			{
				HeaderText = "Country",
				DataField = "Country",
				DataType = typeof(string),
				Visible = newPageUser ? true : userTemplate.Country
			});
			allColumns.Add(new JQGridColumn
			{
				HeaderText = "Special Handling Jurisdiction",
				DataField = "SpecialHandling",
				DataType = typeof(string),
				Visible = newPageUser ? true : userTemplate.SpecialHandling
			});
			allColumns.Add(new JQGridColumn
			{
				HeaderText = "Compilation of Sourcecode",
				DataField = "SourceCodeCompilation",
				DataType = typeof(string),
				Visible = newPageUser ? true : userTemplate.SourceCodeCompilation
			});
			allColumns.Add(new JQGridColumn
			{
				HeaderText = "Forensic",
				DataField = "Forensic",
				DataType = typeof(string),
				Visible = newPageUser ? true : userTemplate.Forensic
			});
			allColumns.Add(new JQGridColumn
			{
				HeaderText = "Progressive Reconciliation",
				DataField = "ProgressiveReconciliation",
				DataType = typeof(string),
				Visible = newPageUser ? true : userTemplate.ProgressiveReconciliation
			});
			allColumns.Add(new JQGridColumn
			{
				HeaderText = "Offensive Material Evaluation",
				DataField = "OffensiveMaterialEvaluation",
				DataType = typeof(string),
				Visible = newPageUser ? true : userTemplate.OffensiveMaterialEvaluation
			});
			allColumns.Add(new JQGridColumn
			{
				HeaderText = "Underage Advertising Evaluation",
				DataField = "UnderageAdvertisingEvaluation",
				DataType = typeof(string),
				Visible = newPageUser ? true : userTemplate.UnderageAdvertisingEvaluation
			});
			allColumns.Add(new JQGridColumn
			{
				HeaderText = "Responsible Gaming",
				DataField = "ResponsibleGaming",
				DataType = typeof(string),
				Visible = newPageUser ? true : userTemplate.ResponsibleGaming
			});
			allColumns.Add(new JQGridColumn
			{
				HeaderText = "Remote Access ITL",
				DataField = "RemoteAccessITL",
				DataType = typeof(string),
				Visible = newPageUser ? true : userTemplate.RemoteAccessITL
			});
			allColumns.Add(new JQGridColumn
			{
				HeaderText = "Remote Access",
				DataField = "RemoteAccess",
				DataType = typeof(string),
				Visible = newPageUser ? true : userTemplate.RemoteAccess
			});
			allColumns.Add(new JQGridColumn
			{
				HeaderText = "Point Click Review",
				DataField = "Portal",
				DataType = typeof(string),
				Visible = newPageUser ? true : userTemplate.Portal
			});
			allColumns.Add(new JQGridColumn
			{
				HeaderText = "Non-Compliant Paytable Handling",
				DataField = "NonCompliantPaytableHandling",
				DataType = typeof(string),
				Visible = newPageUser ? true : userTemplate.NonCompliantPaytableHandling
			});
			allColumns.Add(new JQGridColumn
			{
				HeaderText = "Bonus Pot",
				DataField = "BonusPot",
				DataType = typeof(string),
				Visible = newPageUser ? true : userTemplate.BonusPot
			});
			allColumns.Add(new JQGridColumn
			{
				HeaderText = "Wireless Technology",
				DataField = "WirelessTechnology",
				DataType = typeof(string),
				Visible = newPageUser ? true : userTemplate.WirelessTechnology
			});
			allColumns.Add(new JQGridColumn
			{
				HeaderText = "Skill Games",
				DataField = "SkillGames",
				DataType = typeof(string),
				Visible = newPageUser ? true : userTemplate.SkillGames
			});
			allColumns.Add(new JQGridColumn
			{
				HeaderText = "Random Number Generator",
				DataField = "RandomNumberGenerator",
				DataType = typeof(string),
				Visible = newPageUser ? true : userTemplate.RandomNumberGenerator
			});
			allColumns.Add(new JQGridColumn
			{
				HeaderText = "Geographical Testing Location Only",
				DataField = "GeographicalTestingLocationOnly",
				DataType = typeof(string),
				Visible = newPageUser ? true : userTemplate.GeographicalTestingLocationOnly
			});
			allColumns.Add(new JQGridColumn
			{
				HeaderText = "Physical Testing Location",
				DataField = "PhysicalTestingLocation",
				DataType = typeof(string),
				Visible = newPageUser ? true : userTemplate.PhysicalTestingLocation
			});
			allColumns.Add(new JQGridColumn
			{
				HeaderText = "Acceptable Ways of Sourcecode",
				DataField = "CompilationMethod",
				DataType = typeof(string),
				Visible = newPageUser ? true : userTemplate.CompilationMethod
			});
			allColumns.Add(new JQGridColumn
			{
				HeaderText = "New Technology Review",
				DataField = "NewTechnologyReviewNeeded",
				DataType = typeof(string),
				Visible = newPageUser ? true : userTemplate.NewTechnologyReviewNeeded
			});
			allColumns.Add(new JQGridColumn
			{
				HeaderText = "Special Math Instructions",
				DataField = "SpecialMathInstructions",
				DataType = typeof(string),
				Visible = newPageUser ? true : userTemplate.SpecialMathInstructions
			});
			allColumns.Add(new JQGridColumn
			{
				HeaderText = "Comments",
				DataField = "Comments",
				DataType = typeof(string),
				Visible = newPageUser ? true : userTemplate.Comments
			});
			allColumns.Add(new JQGridColumn
			{
				HeaderText = "Documented Skill Restrictions",
				DataField = "DocumentedRestrictionsOnSkill",
				DataType = typeof(string),
				Visible = newPageUser ? true : userTemplate.DocumentedRestrictionsOnSkill
			});
			#endregion
			#region Attribute Columns
			allColumns.Add(new JQGridColumn
			{
				HeaderText = "Conditional Approval",
				DataField = "ConditionalApproval",
				DataType = typeof(string),
				Visible = newPageUser ? true : userTemplate.ConditionalApproval
			});
			allColumns.Add(new JQGridColumn
			{
				HeaderText = "Conditional Revocation",
				DataField = "ConditionalRevocation",
				DataType = typeof(string),
				Visible = newPageUser ? true : userTemplate.ConditionalRevocation
			});
			allColumns.Add(new JQGridColumn
			{
				HeaderText = "Game Authentication Terminal Verification Required",
				DataField = "GAT",
				DataType = typeof(string),
				Visible = newPageUser ? true : userTemplate.GAT
			});
			allColumns.Add(new JQGridColumn
			{
				HeaderText = "Inter-Operability Testing Required",
				DataField = "Interop",
				DataType = typeof(string),
				Visible = newPageUser ? true : userTemplate.Interop
			});
			allColumns.Add(new JQGridColumn
			{
				HeaderText = "Protocol Testing Required",
				DataField = "ProtocolTesting",
				DataType = typeof(string),
				Visible = newPageUser ? true : userTemplate.ProtocolTesting
			});
			allColumns.Add(new JQGridColumn
			{
				HeaderText = "Safety Certification",
				DataField = "RequiredSafetyCertifications",
				DataType = typeof(string),
				Visible = newPageUser ? true : userTemplate.RequiredSafetyCertifications
			});
			allColumns.Add(new JQGridColumn
			{
				HeaderText = "Non-Mandatory Update",
				DataField = "NUStatus",
				DataType = typeof(string),
				Visible = newPageUser ? true : userTemplate.NUStatus
			});
			#endregion
			#region Limit Columns
			allColumns.Add(new JQGridColumn
			{
				HeaderText = "Credit Limit",
				DataField = "CreditLimit",
				DataType = typeof(string),
				Visible = newPageUser ? true : userTemplate.CreditLimit
			});
			allColumns.Add(new JQGridColumn
			{
				HeaderText = "Link Maximum Win",
				DataField = "LinkMaxWin",
				DataType = typeof(string),
				Visible = newPageUser ? true : userTemplate.LinkMaxWin
			});

			allColumns.Add(new JQGridColumn
			{
				HeaderText = "Linked Small Establishments",
				DataField = "LinkedSmallEstablishments",
				DataType = typeof(string),
				Visible = newPageUser ? true : userTemplate.LinkedSmallEstablishments
			});
			allColumns.Add(new JQGridColumn
			{
				HeaderText = "Maximum Bet",
				DataField = "MaxBet",
				DataType = typeof(string),
				Visible = newPageUser ? true : userTemplate.MaxBet
			});
			allColumns.Add(new JQGridColumn
			{
				HeaderText = "Maximum Win",
				DataField = "MaxWin",
				DataType = typeof(string),
				Visible = newPageUser ? true : userTemplate.MaxWin
			});
			allColumns.Add(new JQGridColumn
			{
				HeaderText = "Minimum Bet",
				DataField = "MinBet",
				DataType = typeof(string),
				Visible = newPageUser ? true : userTemplate.MinBet
			});
			allColumns.Add(new JQGridColumn
			{
				HeaderText = "Multi-Game Maximum Bet",
				DataField = "MTGMMaxBet",
				DataType = typeof(string),
				Visible = newPageUser ? true : userTemplate.MTGMMaxBet
			});
			allColumns.Add(new JQGridColumn
			{
				HeaderText = "Multi-Game Maximum Win",
				DataField = "MTGMMaxWin",
				DataType = typeof(string),
				Visible = newPageUser ? true : userTemplate.MTGMMaxWin
			});
			allColumns.Add(new JQGridColumn
			{
				HeaderText = "Progressive Limit",
				DataField = "ProgressiveCap",
				DataType = typeof(string),
				Visible = newPageUser ? true : userTemplate.ProgressiveCap
			});
			allColumns.Add(new JQGridColumn
			{
				HeaderText = "Progressive Maximum Win",
				DataField = "ProgressiveMaxWin",
				DataType = typeof(string),
				Visible = newPageUser ? true : userTemplate.ProgressiveMaxWin
			});
			allColumns.Add(new JQGridColumn
			{
				HeaderText = "Stand Alone Progressive Maximum Win",
				DataField = "SAPMaxWin",
				DataType = typeof(string),
				Visible = newPageUser ? true : userTemplate.SAPMaxWin
			});
			allColumns.Add(new JQGridColumn
			{
				HeaderText = "Wide Area Progressive Maximum Win",
				DataField = "WAPMaxWin",
				DataType = typeof(string),
				Visible = newPageUser ? true : userTemplate.WAPMaxWin
			});
			allColumns.Add(new JQGridColumn
			{
				HeaderText = "Win Limit",
				DataField = "WinCap",
				DataType = typeof(string),
				Visible = newPageUser ? true : userTemplate.WinCap
			});
			#endregion
			#region Prohibited Games Columns
			allColumns.Add(new JQGridColumn
			{
				DataField = "NotAllowedGames",
				HeaderText = "Prohibited Game Types",
				DataType = typeof(string),
				Visible = newPageUser ? true : userTemplate.NotAllowedGames
			});
			#endregion
			#region Odds Requirements
			allColumns.Add(new JQGridColumn
			{
				DataField = "AnyAwardOR",
				HeaderText = "Any Award Odds Requirements",
				DataType = typeof(string),
				Visible = newPageUser ? true : userTemplate.AnyAwardOR
			});
			allColumns.Add(new JQGridColumn
			{
				DataField = "AllAwardOR",
				HeaderText = "All Award Odds Requirements",
				DataType = typeof(string),
				Visible = newPageUser ? true : userTemplate.AllAwardOR
			});
			allColumns.Add(new JQGridColumn
			{
				DataField = "TopAwardOR",
				HeaderText = "Top Award Odds Requirements",
				DataType = typeof(string),
				Visible = newPageUser ? true : userTemplate.TopAwardOR
			});
			#endregion
			#region Tech Type Columns
			allColumns.Add(new JQGridColumn
			{
				DataField = "NotAllowedTech",
				HeaderText = "Prohibited Technology",
				DataType = typeof(string),
				Visible = newPageUser ? true : userTemplate.NotAllowedTech
			});
			#endregion
			#region Protocol Columns
			allColumns.Add(new JQGridColumn
			{
				DataField = "ProtocolValue",
				DataType = typeof(string),
				HeaderText = "Protocols",
				Visible = newPageUser ? true : userTemplate.ProtocolValue
			});
			#endregion
			#region RTP columns
			allColumns.Add(new JQGridColumn
			{
				DataField = "MaxRTP",
				HeaderText = "Maximum RTP",
				DataType = typeof(string),
				Visible = newPageUser ? true : userTemplate.MaxRTP
			});
			allColumns.Add(new JQGridColumn
			{
				DataField = "MinRTP",
				HeaderText = "Minimum RTP",
				DataType = typeof(string),
				Visible = newPageUser ? true : userTemplate.MinRTP
			});
			allColumns.Add(new JQGridColumn
			{
				DataField = "AverageRTP",
				HeaderText = "Average RTP",
				DataType = typeof(string),
				Visible = newPageUser ? true : userTemplate.AverageRTP
			});
			allColumns.Add(new JQGridColumn
			{
				DataField = "WeightedRTP",
				HeaderText = "Weighted RTP",
				DataType = typeof(string),
				Visible = newPageUser ? true : userTemplate.WeightedRTP
			});
			#endregion
			return allColumns;
		}
	}
	public class GamingGuidelineGridRecord
	{
		public GamingGuidelineGridRecord()
		{
			//this.AttributesExpando = new ExpandoObject();
		}
		public int Id { get; set; }
		public int GamingGuidelineId { get; set; }
		public string GamingGuidelineName { get; set; }
		#region Jur Props
		public object Jurisdiction { get; set; }
		public string SpecialHandling { get; set; }
		public string SourceCodeCompilation { get; set; }
		public string Forensic { get; set; }
		public string ProgressiveReconciliation { get; set; }
		public string OffensiveMaterialEvaluation { get; set; }
		public string UnderageAdvertisingEvaluation { get; set; }
		public string ResponsibleGaming { get; set; }
		public string RemoteAccessITL { get; set; }
		public string RemoteAccess { get; set; }
		public string Portal { get; set; }
		public string NonCompliantPaytableHandling { get; set; }
		public string WirelessTechnology { get; set; }
		public string SkillGames { get; set; }
		public string BonusPot { get; set; }
		public string RandomNumberGenerator { get; set; }
		public string GeographicalTestingLocationOnly { get; set; }
		public string PhysicalTestingLocation { get; set; }
		public string CompilationMethod { get; set; }
		public string NewTechnologyReviewNeeded { get; set; }
		public string SpecialMathInstructions { get; set; }
		public string Comments { get; set; }
		public string DocumentedRestrictionsOnSkill { get; set; }
		public string PrimaryOwner { get; set; }
		public string SecondaryOwner { get; set; }
		#endregion
		#region Attribute Props
		public string ConditionalApproval { get; set; }
		public string ConditionalRevocation { get; set; }
		public string GAT { get; set; }
		public string ProtocolTesting { get; set; }
		public string Interop { get; set; }
		public string RequiredSafetyCertifications { get; set; }
		public string NUStatus { get; set; }
		public List<GamingGuidelineAttributes> Attributes { get; set; }
		#endregion
		#region Limit Props
		public string LinkMaxWin { get; set; }
		public string LinkedSmallEstablishments { get; set; }
		public string MaxBet { get; set; }
		public string CreditLimit { get; set; }
		public string MaxWin { get; set; }
		public string MinBet { get; set; }
		public string MTGMMaxBet { get; set; }
		public string MTGMMaxWin { get; set; }
		public string ProgressiveCap { get; set; }
		public string ProgressiveMaxWin { get; set; }
		public string SAPMaxWin { get; set; }
		public string WAPMaxWin { get; set; }
		public string WinCap { get; set; }
		public List<GamingGuidelineLimits> Limits { get; set; }
		#endregion
		#region Prohibited Game Type props
		public string NotAllowedGames { get; set; }
		public List<GamingGuidelineProhibitedGameTypes> ProhibitedGameTypes { get; set; }
		#endregion
		#region Odds Req props
		public string AnyAwardOR { get; set; }
		public string AllAwardOR { get; set; }
		public string TopAwardOR { get; set; }
		public List<GamingGuidelineOddsRequirements> OddsRequirements { get; set; }
		public string OddsRequirementsJson { get; set; }
		#endregion
		#region Tech types props
		public string NotAllowedTech { get; set; }
		public int? SportsBettingId { get; set; }
		public string JurId { get; set; }
		public string Country { get; set; }
		public List<GamingGuidelineTechTypes> TechnologyTypes { get; set; }
		#endregion
		#region Protocol props
		public string ProtocolValue { get; set; }
		public List<GamingGuidelineProtocolTypes> Protocols { get; set; }
		#endregion
		#region RTP Types
		public string MaxRTP { get; set; }
		public string MinRTP { get; set; }
		public string AverageRTP { get; set; }
		public string WeightedRTP { get; set; }
		public List<GamingGuidelineRTPRequirements> RTPRequirements { get; set; }
		#endregion
	}
}