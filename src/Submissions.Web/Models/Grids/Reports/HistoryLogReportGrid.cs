﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;

namespace Submissions.Web.Models.Grids.Reports
{
	using Trirand.Web.Mvc;

	public class HistoryLogReportGrid
	{
		public HistoryLogReportGrid()
		{
			this.Grid = new JQGrid
			{
				ID = "HistoryLogReportGrid",
				Columns = InitializeAllColumns(),
				DataUrl = new UrlHelper(((MvcHandler)HttpContext.Current.Handler).RequestContext).Action("LoadReportHistoryLogGrid", "GridsAjax", new { Area = "" }),
				AutoWidth = true,
				Width = Unit.Percentage(100),
				Height = Unit.Percentage(100),
				ToolBarSettings = new ToolBarSettings
				{
					ShowRefreshButton = true,
					ToolBarPosition = ToolBarPosition.Top
				},
				PagerSettings = new PagerSettings
				{
					PageSize = 100,
					PageSizeOptions = "[]"
				},
				AppearanceSettings = new AppearanceSettings
				{
					AlternateRowBackground = true,
					HighlightRowsOnHover = true,
					ShowFooter = false
				},
				SortSettings = new SortSettings
				{
					InitialSortColumn = "ChangeDate",
					InitialSortDirection = SortDirection.Desc
				},
				ClientSideEvents = new ClientSideEvents
				{
					BeforeAjaxRequest = "gridBeforeAjaxRequest"
				},
				RenderingMode = RenderingMode.Optimized
			};
		}

		public JQGrid Grid { get; set; }

		private List<JQGridColumn> InitializeAllColumns()
		{
			var allColumns = new List<JQGridColumn>();

			allColumns.Add(new JQGridColumn
			{
				DataField = "Id",
				PrimaryKey = true,
				Editable = false,
				Visible = false
			});

			allColumns.Add(new JQGridColumn
			{
				DataField = "Name",
				HeaderText = "Name",
				DataType = typeof(string),
				Sortable = false,
				Width = 70
			});

			allColumns.Add(new JQGridColumn
			{
				DataField = "ChangeDate",
				HeaderText = "Change Date",
				DataFormatString = "{0:g}",
				DataType = typeof(DateTime),
				Width = 70
			});

			allColumns.Add(new JQGridColumn
			{
				DataField = "ChangedFromConverted",
				HeaderText = "Changed From",
				DataType = typeof(string),
				Sortable = false,
				Width = 150
			});

			allColumns.Add(new JQGridColumn
			{
				DataField = "ChangedToConverted",
				HeaderText = "Changed To",
				DataType = typeof(string),
				Sortable = false,
				Width = 150
			});

			allColumns.Add(new JQGridColumn
			{
				DataField = "Column",
				HeaderText = "Column",
				DataType = typeof(string),
				Width = 70
			});

			allColumns.Add(new JQGridColumn
			{
				DataField = "Table",
				HeaderText = "Table",
				DataType = typeof(string),
				Width = 70
			});

			allColumns.Add(new JQGridColumn
			{
				DataField = "ActionType",
				HeaderText = "Action Type",
				DataType = typeof(string),
				Width = 70
			});

			allColumns.Add(new JQGridColumn
			{
				DataField = "PrimaryKey",
				HeaderText = "Primary Key",
				DataType = typeof(string),
				Width = 40
			});

			return allColumns;
		}
	}

	public class HistoryLogReportGridRecord
	{
		public Guid Id { get; set; }
		public string Name { get; set; }
		public DateTime ChangeDate { get; set; }
		public string Column { get; set; }
		public string Table { get; set; }
		public string ActionType { get; set; }
		public byte[] ChangedFrom { get; set; }
		public string ChangedFromConverted { get; set; }
		public byte[] ChangedTo { get; set; }
		public string ChangedToConverted { get; set; }
		public int? PrimaryKey { get; set; }
	}
}