using EF.Submission;
using Submissions.Common;
using Submissions.Common.JQGrid;
using Submissions.Common.Linq.Dynamic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;

namespace Submissions.Web.Models.Grids.Reports
{
	using Trirand.Web.Mvc;

	public class JurisdictionalDataReportGrid : IDynamicGrid, IColumnChooserGrid
	{
		public JurisdictionalDataReportGrid()
		{
			InitializeAllColumns();

			this.GridBaseID = "JurisdictionalDataReportGrid";
			this.Grid = new JQGrid
			{
				ID = this.GridBaseID,
				Columns = this.AllColumns.Cast<JQGridColumn>().ToList(),
				DataUrl = new UrlHelper(((MvcHandler)HttpContext.Current.Handler).RequestContext).Action("LoadReportJurisdictionalDataGrid", "GridsAjax", new { Area = "" }),
				AutoWidth = false,
				ShrinkToFit = false,
				Width = Unit.Percentage(100),
				Height = Unit.Percentage(100),
				ToolBarSettings = new ToolBarSettings
				{
					ShowRefreshButton = true,
					ToolBarPosition = ToolBarPosition.Top,
					ShowSearchToolBar = true
				},
				PagerSettings = new PagerSettings
				{
					PageSize = 100,
					PageSizeOptions = "[]"
				},
				AppearanceSettings = new AppearanceSettings
				{
					AlternateRowBackground = true,
					HighlightRowsOnHover = true,
					ShowFooter = false
				},
				SortSettings = new SortSettings
				{
					AutoSortByPrimaryKey = false
				},
				ClientSideEvents = new ClientSideEvents
				{
					BeforeAjaxRequest = "jurisdictionalDataReportGridBeforeAjaxRequest"
				},
				RenderingMode = RenderingMode.Optimized
			};
			InitializeDropDowns();
		}

		public void InitializeDynamicGrid(string savedColumns)
		{
			this.SetColumns(savedColumns);
		}

		public void InitializeWidgetGrid(int widgetId, string savedColumns)
		{
			var grid = this.Grid;
			grid.ID += widgetId.ToString();
			grid.DataUrl = new UrlHelper(((MvcHandler)HttpContext.Current.Handler).RequestContext).Action("LoadWidgetJurisdictionalDataGrid", "GridsAjax", new { Area = "" });
			grid.AutoWidth = true;
			grid.ToolBarSettings = new ToolBarSettings
			{
				ShowAddButton = false,
				ShowDeleteButton = false,
				ShowEditButton = false,
				ShowRefreshButton = false,
				ShowSearchButton = false,
				ToolBarPosition = ToolBarPosition.Top
			};
			grid.ClientSideEvents = new ClientSideEvents
			{
				AfterAjaxRequest = "gridAfterAjaxRequest"
			};
			this.SetColumns(savedColumns);
		}

		public JQGrid Grid { get; set; }
		public string GridBaseID { get; set; }
		public List<JQGridColumnDynamic> AllColumns { private set; get; }
		public List<JQGridColumnDynamic> SelectedColumns { private set; get; }
		public IList<ColumnChooserColumn> ColumnChooserSelectedColumns
		{
			get
			{
				return this.SelectedColumns
					.Where(x => x.Visible == true)
					.Select(x => new ColumnChooserColumn { DatabaseName = x.DataField, DisplayName = x.HeaderText })
					.ToList();

			}
		}
		public IList<ColumnChooserColumn> ColumnChooserAvailableColumns
		{
			get
			{
				return this.AllColumns.Except(this.SelectedColumns)
					.Where(x => x.Visible == true)
					.Select(x => new ColumnChooserColumn { DatabaseName = x.DataField, DisplayName = x.HeaderText })
					.OrderBy(x => x.DisplayName)
					.ToList();
			}
		}

		public string GetColumnsDynamicLinqFormat()
		{
			var selectFields = this.SelectedColumns;
			var selectStatement = new StringBuilder();
			selectStatement.Append("new (");

			foreach (var col in selectFields)
			{
				if (col.DataField == "ReplaceWith")
				{
					selectStatement.Append("(ReplaceWithLinks.FirstOrDefault().ReplaceWithJurisdictionalData.Submission.IdNumber) As ReplaceWith,");
				}
				else if (col.DataField == "ReplaceWithJurisdictionalDataSubmissionId")
				{
					selectStatement.Append("(ReplaceWithLinks.FirstOrDefault().ReplaceWithJurisdictionalData.SubmissionId) As ReplaceWithJurisdictionalDataSubmissionId,");
				}
				else
				{
					selectStatement.AppendFormat("{0} As {1},", col.SqlSelectName, col.DataField);
				}
			}

			selectStatement.Length--;
			selectStatement.Append(")");

			return selectStatement.ToString();
		}

		public void SetColumns(string savedColumns)
		{
			var jqColumns = new List<JQGridColumnDynamic>();

			if (savedColumns == null)
			{
				jqColumns = this.AllColumns;
			}
			else
			{
				var widgetId = this.Grid.ID.Replace(this.GridBaseID, "");
				var savedColumnsList = new List<string>(savedColumns.Replace("\"", "").Replace("[", "").Replace("]", "").Split(','));
				var hiddenColumns = this.AllColumns.Where(x => x.Visible == false).ToList();

				jqColumns.AddRange(hiddenColumns);
				foreach (var col in savedColumnsList)
				{
					var column = this.AllColumns.Where(x => x.DataField == col).SingleOrDefault();
					if (column != null)
					{
						column.SearchControlID += widgetId;
						jqColumns.Add(column);
					}
				}
			}

			this.Grid.Columns = jqColumns.Cast<JQGridColumn>().ToList();
			this.SelectedColumns = jqColumns;
		}

		public IList<SelectListItem> GetAvailableSorts()
		{
			var columnList = new List<SelectListItem>() { new SelectListItem { Text = "", Value = "" } };

			var allColumns = this.AllColumns
				.Where(x => x.Visible && x.Searchable)
				.Select(x => new SelectListItem { Text = x.HeaderText, Value = x.SqlSelectName })
				.OrderBy(x => x.Text)
				.ToList();

			columnList.AddRange(allColumns);

			return columnList;
		}

		public void Export(Export export, IQueryable<JurisdictionalData> query)
		{
			if (export.WidgetId == null)
			{
				this.InitializeDynamicGrid(export.SavedColumns);
			}
			else
			{
				this.InitializeWidgetGrid((int)export.WidgetId, export.SavedColumns);
			}

			var selectStatement = this.GetColumnsDynamicLinqFormat();
			var results = query.Select(selectStatement);

			var fileName = string.Format(export.Title + " - {0:yyyy-MM-dd_hh-mm-ss-tt}", DateTime.Now);
			var jqGridHelper = new JQGridHelper();
			var jqGridState = new JQGridState();
			jqGridState.QueryString = HttpUtility.ParseQueryString(export.FiltersToolbar ?? "");

			if (export.ExportType == ExportType.Xls)
			{
				fileName += ".xls";
				this.Grid.ExportToExcel(results, fileName, jqGridState);
			}
			else if (export.ExportType == ExportType.Csv)
			{
				fileName += ".csv";
				this.Grid.ExportSettings.ExportDataRange = ExportDataRange.All;
				var exportData = this.Grid.GetExportData(results, jqGridState);
				jqGridHelper.ExportToCSV(exportData, fileName);
			}
			else if (export.ExportType == ExportType.Pdf)
			{
				fileName += ".pdf";
				this.Grid.ExportSettings.ExportDataRange = ExportDataRange.All;
				var exportData = this.Grid.GetExportData(results, jqGridState);
				jqGridHelper.ExportToPDF(exportData, fileName);
			}
		}

		private void InitializeAllColumns()
		{
			var allColumns = new List<JQGridColumnDynamic>();

			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "Id",
				SqlSelectName = "Id",
				PrimaryKey = true,
				Editable = false,
				Visible = false
			});
			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "SubmissionId",
				SqlSelectName = "SubmissionId",
				Visible = false
			});
			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "ReplaceWithJurisdictionalDataSubmissionId",
				SqlSelectName = "ReplaceWithJurisdictionalDataSubmissionId",
				Visible = false
			});
			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "SubmissionManufacturer",
				SqlSelectName = "Submission.ManufacturerDescription",
				HeaderText = "Manufacturer",
				DataType = typeof(string),
				SearchToolBarOperation = SearchOperation.BeginsWith,
				SearchType = SearchType.AutoComplete,
				SearchControlID = "ManufacturerAutoComplete",
				Width = 200
			});
			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "SubmissionFileNumber",
				SqlSelectName = "Submission.FileNumber",
				HeaderText = "File Number",
				DataType = typeof(string),
				SearchToolBarOperation = SearchOperation.BeginsWith,
				SearchType = SearchType.AutoComplete,
				SearchControlID = "FileNumberAutoComplete",
				Width = 150,
				Formatter = new CustomFormatter { FormatFunction = "fileNumberLink" }
			});
			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "SubmissionIdNumber",
				SqlSelectName = "Submission.IdNumber",
				HeaderText = "ID Number",
				DataType = typeof(string),
				SearchToolBarOperation = SearchOperation.BeginsWith,
				SearchType = SearchType.AutoComplete,
				SearchControlID = "IdNumberAutoComplete",
				Width = 150
			});
			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "SubmissionDateCode",
				SqlSelectName = "Submission.DateCode",
				HeaderText = "Date Code",
				DataType = typeof(string),
				SearchToolBarOperation = SearchOperation.BeginsWith,
				SearchType = SearchType.AutoComplete,
				SearchControlID = "DateCodeAutoComplete",
				Width = 200
			});
			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "SubmissionVersion",
				SqlSelectName = "Submission.Version",
				HeaderText = "Version",
				DataType = typeof(string),
				SearchToolBarOperation = SearchOperation.BeginsWith,
				SearchType = SearchType.AutoComplete,
				SearchControlID = "VersionAutoComplete",
				Width = 150
			});
			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "SubmissionGameName",
				SqlSelectName = "Submission.GameName",
				HeaderText = "Game Name",
				DataType = typeof(string),
				SearchToolBarOperation = SearchOperation.BeginsWith,
				SearchType = SearchType.AutoComplete,
				SearchControlID = "GameNameAutoComplete",
				Width = 250
			});
			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "SubmissionPosition",
				SqlSelectName = "Submission.Position",
				HeaderText = "Position",
				DataType = typeof(string),
				SearchToolBarOperation = SearchOperation.BeginsWith,
				SearchType = SearchType.AutoComplete,
				SearchControlID = "PositionAutoComplete",
				Width = 150
			});
			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "SubmissionFunction",
				SqlSelectName = "Submission.Function",
				HeaderText = "Function",
				DataType = typeof(string),
				SearchToolBarOperation = SearchOperation.BeginsWith,
				SearchType = SearchType.AutoComplete,
				SearchControlID = "FunctionAutoComplete",
				Width = 150
			});
			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "SubmissionLab",
				SqlSelectName = "Submission.Lab",
				HeaderText = "Test Lab",
				DataType = typeof(string),
				SearchToolBarOperation = SearchOperation.IsEqualTo,
				SearchType = SearchType.AutoComplete,
				SearchControlID = "LabAutoComplete",
				Width = 80
			});
			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "JurisdictionName",
				SqlSelectName = "JurisdictionName",
				HeaderText = "Jurisdiction",
				DataType = typeof(string),
				SearchToolBarOperation = SearchOperation.BeginsWith,
				SearchType = SearchType.AutoComplete,
				SearchControlID = "JurisdictionAutoComplete",
				Width = 200
			});
			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "Status",
				SqlSelectName = "Status",
				HeaderText = "Status",
				DataType = typeof(string),
				SearchToolBarOperation = SearchOperation.IsEqualTo,
				SearchType = SearchType.DropDown,
				Width = 150,
				Formatter = new CustomFormatter { FormatFunction = "formatJurisdictionStatus" }
			});
			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "RequestDate",
				SqlSelectName = "RequestDate",
				HeaderText = "Request Date",
				DataFormatString = "{0:d}",
				DataType = typeof(DateTime),
				Width = 100
			});
			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "DraftDate",
				SqlSelectName = "DraftDate",
				HeaderText = "Draft Date",
				DataFormatString = "{0:d}",
				DataType = typeof(DateTime),
				Width = 100
			});
			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "CloseDate",
				SqlSelectName = "CloseDate",
				HeaderText = "Close Date",
				DataFormatString = "{0:d}",
				DataType = typeof(DateTime),
				Width = 100
			});
			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "RevokeDate",
				SqlSelectName = "RevokeDate",
				HeaderText = "Revoke Date",
				DataFormatString = "{0:d}",
				DataType = typeof(DateTime),
				Width = 100
			});
			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "ObsoleteDate",
				SqlSelectName = "ObsoleteDate",
				HeaderText = "NU Date",
				DataFormatString = "{0:d}",
				DataType = typeof(DateTime),
				Width = 100
			});
			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "UpgradeByDate",
				SqlSelectName = "UpgradeByDate",
				HeaderText = "Upgrade By Date",
				DataFormatString = "{0:d}",
				DataType = typeof(DateTime),
				Width = 110
			});
			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "TargetDate",
				SqlSelectName = "TargetDate",
				HeaderText = "Estimated Completion Date",
				DataFormatString = "{0:d}",
				DataType = typeof(DateTime),
				Width = 200//,
						   //Formatter = new CustomFormatter { FormatFunction = "targetDateLink" }
			});
			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "ReplaceWith",
				SqlSelectName = "ReplaceWith",
				HeaderText = "Replace With",
				DataType = typeof(string),
				SearchToolBarOperation = SearchOperation.BeginsWith,
				Width = 180//,
						   //Formatter = new CustomFormatter { FormatFunction = "replaceWithLink" }
			});
			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "ClientNumber",
				SqlSelectName = "ClientNumber",
				HeaderText = "Billing Reference",
				DataType = typeof(string),
				Searchable = true,
				SearchToolBarOperation = SearchOperation.BeginsWith,
				Width = 150
			});

			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "LetterNumber",
				SqlSelectName = "LetterNumber",
				HeaderText = "Letter Number",
				DataType = typeof(string),
				Searchable = true,
				SearchToolBarOperation = SearchOperation.BeginsWith,
				Width = 150
			});

			this.AllColumns = allColumns;
		}

		private void InitializeDropDowns()
		{
			var statusColumn = this.Grid.Columns.Find(x => x.DataField == "Status");
			if (statusColumn.Visible)
			{
				if (Grid.AjaxCallBackMode == AjaxCallBackMode.RequestData)
				{
					statusColumn.SearchList = LookupsStandard.ConvertEnum(showBlank: true, enumValues: ComUtil.InternalJurisdictionStatuses().ToArray())
						.Select(x => new SelectListItem { Value = x.Value, Text = HttpUtility.HtmlDecode(x.Text + " &nbsp; " + x.Value) })
						.ToList();
				}
			}
		}
	}
}