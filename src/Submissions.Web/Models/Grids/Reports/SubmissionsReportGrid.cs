﻿using Submissions.Common;
using Submissions.Common.JQGrid;
using Submissions.Common.Linq.Dynamic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;

namespace Submissions.Web.Models.Grids.Reports
{
	using EF.Submission;
	using Trirand.Web.Mvc;

	public class SubmissionsReportGrid : IDynamicGrid, IColumnChooserGrid
	{
		public SubmissionsReportGrid()
		{
			InitializeAllColumns();

			this.GridBaseID = "SubmissionsReportGrid";
			this.Grid = new JQGrid
			{
				ID = this.GridBaseID,
				Columns = this.AllColumns.Cast<JQGridColumn>().ToList(),
				DataUrl = new UrlHelper(((MvcHandler)HttpContext.Current.Handler).RequestContext).Action("LoadReportSubmissionsGrid", "GridsAjax", new { Area = "" }),
				AutoWidth = false,
				ShrinkToFit = false,
				Width = Unit.Percentage(100),
				Height = Unit.Percentage(100),
				ToolBarSettings = new ToolBarSettings
				{
					ShowRefreshButton = true,
					ToolBarPosition = ToolBarPosition.Top,
					ShowSearchToolBar = true,
				},
				PagerSettings = new PagerSettings
				{
					PageSize = 100,
					PageSizeOptions = "[]"
				},
				SortSettings = new SortSettings
				{
					AutoSortByPrimaryKey = false
				},
				AppearanceSettings = new AppearanceSettings
				{
					AlternateRowBackground = true,
					HighlightRowsOnHover = true,
					ShowFooter = false
				},
				ClientSideEvents = new ClientSideEvents
				{
					BeforeAjaxRequest = "submissionsReportGridBeforeAjaxRequest"
				},
				RenderingMode = RenderingMode.Optimized
			};
		}

		public void InitializeDynamicGrid(string savedColumns)
		{
			this.SetColumns(savedColumns);
		}

		public void InitializeWidgetGrid(int widgetId, string savedColumns)
		{
			var grid = this.Grid;
			grid.ID += widgetId.ToString();
			grid.DataUrl = new UrlHelper(((MvcHandler)HttpContext.Current.Handler).RequestContext).Action("LoadWidgetSubmissionsGrid", "GridsAjax", new { Area = "" });
			grid.AutoWidth = true;
			grid.ToolBarSettings = new ToolBarSettings
			{
				ShowAddButton = false,
				ShowDeleteButton = false,
				ShowEditButton = false,
				ShowRefreshButton = false,
				ShowSearchButton = false,
				ToolBarPosition = ToolBarPosition.Top
			};
			grid.ClientSideEvents = new ClientSideEvents
			{
				AfterAjaxRequest = "gridAfterAjaxRequest"
			};
			this.SetColumns(savedColumns);
		}

		public JQGrid Grid { get; set; }
		public string GridBaseID { get; set; }
		public List<JQGridColumnDynamic> AllColumns { private set; get; }
		public List<JQGridColumnDynamic> SelectedColumns { private set; get; }
		public IList<ColumnChooserColumn> ColumnChooserSelectedColumns
		{
			get
			{
				return this.SelectedColumns
					.Where(x => x.Visible == true)
					.Select(x => new ColumnChooserColumn { DatabaseName = x.DataField, DisplayName = x.HeaderText })
					.ToList();

			}
		}
		public IList<ColumnChooserColumn> ColumnChooserAvailableColumns
		{
			get
			{
				return this.AllColumns.Except(this.SelectedColumns)
					.Where(x => x.Visible == true)
					.Select(x => new ColumnChooserColumn { DatabaseName = x.DataField, DisplayName = x.HeaderText })
					.OrderBy(x => x.DisplayName)
					.ToList();
			}
		}

		public string GetColumnsDynamicLinqFormat()
		{
			var selectFields = this.SelectedColumns;
			var selectStatement = new StringBuilder();
			selectStatement.Append("new (");

			foreach (var col in selectFields)
			{
				selectStatement.AppendFormat("{0} As {1},", col.SqlSelectName, col.DataField);
			}

			selectStatement.Length--;
			selectStatement.Append(")");

			return selectStatement.ToString();
		}

		public void SetColumns(string savedColumns)
		{
			var jqColumns = new List<JQGridColumnDynamic>();

			if (savedColumns == null)
			{
				jqColumns = this.AllColumns;
			}
			else
			{
				var widgetId = this.Grid.ID.Replace(this.GridBaseID, "");
				var savedColumnsList = new List<string>(savedColumns.Replace("\"", "").Replace("[", "").Replace("]", "").Split(','));
				var hiddenColumns = this.AllColumns.Where(x => x.Visible == false).ToList();

				jqColumns.AddRange(hiddenColumns);
				foreach (var col in savedColumnsList)
				{
					var column = this.AllColumns.Where(x => x.DataField == col).SingleOrDefault();
					if (column != null)
					{
						column.SearchControlID += widgetId;
						jqColumns.Add(column);
					}
				}
			}

			this.Grid.Columns = jqColumns.Cast<JQGridColumn>().ToList();
			this.SelectedColumns = jqColumns;
		}

		public IList<SelectListItem> GetAvailableSorts()
		{
			var columnList = new List<SelectListItem>() { new SelectListItem { Text = "", Value = "" } };

			var allColumns = this.AllColumns
				.Where(x => x.Visible && x.Searchable)
				.Select(x => new SelectListItem { Text = x.HeaderText, Value = x.SqlSelectName })
				.OrderBy(x => x.Text)
				.ToList();

			columnList.AddRange(allColumns);

			return columnList;
		}

		public void Export(Export export, IQueryable<Submission> query)
		{
			if (export.WidgetId == null)
			{
				this.InitializeDynamicGrid(export.SavedColumns);
			}
			else
			{
				this.InitializeWidgetGrid((int)export.WidgetId, export.SavedColumns);
			}

			var selectStatement = this.GetColumnsDynamicLinqFormat();
			var results = query.Select(selectStatement);

			var fileName = string.Format(export.Title + " - {0:yyyy-MM-dd_hh-mm-ss-tt}", DateTime.Now);
			var jqGridHelper = new JQGridHelper();
			var jqGridState = new JQGridState();
			jqGridState.QueryString = HttpUtility.ParseQueryString(export.FiltersToolbar ?? "");

			if (export.ExportType == ExportType.Xls)
			{
				fileName += ".xls";
				this.Grid.ExportToExcel(results, fileName, jqGridState);
			}
			else if (export.ExportType == ExportType.Csv)
			{
				fileName += ".csv";
				this.Grid.ExportSettings.ExportDataRange = ExportDataRange.All;
				var exportData = this.Grid.GetExportData(results, jqGridState);
				jqGridHelper.ExportToCSV(exportData, fileName);
			}
			else if (export.ExportType == ExportType.Pdf)
			{
				fileName += ".pdf";
				this.Grid.ExportSettings.ExportDataRange = ExportDataRange.All;
				var exportData = this.Grid.GetExportData(results, jqGridState);
				jqGridHelper.ExportToPDF(exportData, fileName);
			}
		}

		private void InitializeAllColumns()
		{
			var allColumns = new List<JQGridColumnDynamic>();

			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "Id",
				SqlSelectName = "Id",
				PrimaryKey = true,
				Editable = false,
				Visible = false
			});
			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "FileNumber",
				SqlSelectName = "FileNumber",
				HeaderText = "File Number",
				DataType = typeof(string),
				Searchable = true,
				SearchToolBarOperation = SearchOperation.BeginsWith,
				SearchType = SearchType.AutoComplete,
				SearchControlID = "FileNumberAutoComplete",
				Width = 150,
				Formatter = new CustomFormatter { FormatFunction = "fileNumberLink2" }
			});
			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "Status",
				SqlSelectName = "Status",
				HeaderText = "Status",
				DataType = typeof(string),
				Searchable = true,
				SearchToolBarOperation = SearchOperation.BeginsWith,
				Width = 90
			});
			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "SubmitDate",
				SqlSelectName = "SubmitDate",
				HeaderText = "Submit Date",
				DataFormatString = "{0:d}",
				DataType = typeof(DateTime),
				Width = 100
			});
			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "ReceiveDate",
				SqlSelectName = "ReceiveDate",
				HeaderText = "Receive Date",
				DataFormatString = "{0:d}",
				DataType = typeof(DateTime),
				Width = 100
			});
			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "Type",
				SqlSelectName = "Type",
				HeaderText = "Type",
				DataType = typeof(string),
				Searchable = true,
				SearchToolBarOperation = SearchOperation.BeginsWith,
				Width = 100
			});
			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "ReplaceWith",
				SqlSelectName = "ReplaceWith",
				HeaderText = "Replace With",
				DataType = typeof(string),
				Searchable = true,
				SearchToolBarOperation = SearchOperation.BeginsWith,
				Width = 175
			});
			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "Lab",
				SqlSelectName = "Lab",
				HeaderText = "Test Lab",
				DataType = typeof(string),
				Searchable = true,
				SearchToolBarOperation = SearchOperation.BeginsWith,
				SearchType = SearchType.AutoComplete,
				SearchControlID = "LabAutoComplete",
				Width = 100
			});
			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "CertificationLab",
				SqlSelectName = "CertificationLab",
				HeaderText = "Certification Lab",
				DataType = typeof(string),
				Searchable = true,
				SearchToolBarOperation = SearchOperation.BeginsWith,
				SearchType = SearchType.AutoComplete,
				SearchControlID = "CertificationLabAutoComplete",
				Width = 110
			});
			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "DateCode",
				SqlSelectName = "DateCode",
				HeaderText = "Date Code",
				DataType = typeof(string),
				Searchable = true,
				SearchToolBarOperation = SearchOperation.BeginsWith,
				SearchType = SearchType.AutoComplete,
				SearchControlID = "DateCodeAutoComplete",
				Width = 175
			});
			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "ManufacturerName",
				SqlSelectName = "Manufacturer.Description",
				HeaderText = "Manufacturer",
				DataType = typeof(string),
				Searchable = true,
				SearchToolBarOperation = SearchOperation.BeginsWith,
				SearchType = SearchType.AutoComplete,
				SearchControlID = "ManufacturerAutoComplete",
				Width = 250
			});
			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "IdNumber",
				SqlSelectName = "IdNumber",
				HeaderText = "ID Number",
				DataType = typeof(string),
				Searchable = true,
				SearchToolBarOperation = SearchOperation.BeginsWith,
				SearchType = SearchType.AutoComplete,
				SearchControlID = "IdNumberAutoComplete",
				Width = 175
			});
			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "Version",
				SqlSelectName = "Version",
				HeaderText = "Version",
				DataType = typeof(string),
				Searchable = true,
				SearchToolBarOperation = SearchOperation.BeginsWith,
				SearchType = SearchType.AutoComplete,
				SearchControlID = "VersionAutoComplete",
				Width = 175
			});
			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "GameName",
				SqlSelectName = "GameName",
				HeaderText = "Game Name",
				DataType = typeof(string),
				Searchable = true,
				SearchToolBarOperation = SearchOperation.BeginsWith,
				SearchType = SearchType.AutoComplete,
				SearchControlID = "GameNameAutoComplete",
				Width = 250
			});
			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "ChipType",
				SqlSelectName = "ChipType",
				HeaderText = "Chip Type",
				DataType = typeof(string),
				Searchable = true,
				SearchToolBarOperation = SearchOperation.BeginsWith,
				Width = 150
			});
			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "Position",
				SqlSelectName = "Position",
				HeaderText = "Position",
				DataType = typeof(string),
				Searchable = true,
				SearchToolBarOperation = SearchOperation.BeginsWith,
				SearchType = SearchType.AutoComplete,
				SearchControlID = "PositionAutoComplete",
				Width = 150
			});
			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "Function",
				SqlSelectName = "Function",
				HeaderText = "Function",
				DataType = typeof(string),
				Searchable = true,
				SearchToolBarOperation = SearchOperation.BeginsWith,
				SearchType = SearchType.AutoComplete,
				SearchControlID = "FunctionAutoComplete",
				Width = 150
			});
			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "TestingType",
				SqlSelectName = "TestingType.Code",
				HeaderText = "Testing Type",
				DataType = typeof(string),
				Searchable = true,
				SearchToolBarOperation = SearchOperation.BeginsWith,
				Width = 150
			});
			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "ManufacturerBuildId",
				SqlSelectName = "ManufacturerBuildId",
				HeaderText = "Manufacturer Build ID",
				DataType = typeof(string),
				Searchable = true,
				SearchToolBarOperation = SearchOperation.BeginsWith,
				Width = 150
			});
			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "OrgIGTMaterial",
				SqlSelectName = "OrgIGTMaterial",
				HeaderText = "Orig Uploaded Material",
				DataType = typeof(string),
				Searchable = true,
				SearchToolBarOperation = SearchOperation.BeginsWith,
				Width = 150
			});
			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "Company",
				SqlSelectName = "Company.Name",
				HeaderText = "Company",
				DataType = typeof(string),
				Searchable = true,
				SearchToolBarOperation = SearchOperation.BeginsWith,
				Width = 150
			});
			this.AllColumns = allColumns;
		}
	}
}