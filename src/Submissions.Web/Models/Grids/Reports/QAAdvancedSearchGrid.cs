﻿using Submissions.Common;
using Submissions.Common.JQGrid;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;


namespace Submissions.Web.Models.Grids.Reports
{
	using Trirand.Web.Mvc;

	public class QAAdvancedSearchGrid
	{
		public QAAdvancedSearchGrid()
		{
			this.Grid = new JQGrid
			{
				ID = "QAAdvancedSearchGrid",
				Columns = InitializeAllColumns(),
				DataUrl = new UrlHelper(((MvcHandler)HttpContext.Current.Handler).RequestContext).Action("LoadReportQAAdvancedGrid", "GridsAjax", new { Area = "" }),
				AutoWidth = false,
				Width = Unit.Percentage(100),
				Height = Unit.Percentage(100),
				ToolBarSettings = new ToolBarSettings
				{
					ShowRefreshButton = true,
					ToolBarPosition = ToolBarPosition.Top
				},
				PagerSettings = new PagerSettings
				{
					PageSize = 100,
					PageSizeOptions = "[]"
				},
				AppearanceSettings = new AppearanceSettings
				{
					AlternateRowBackground = true,
					HighlightRowsOnHover = true,
					ShowFooter = false
				},
				SortSettings = new SortSettings
				{
					InitialSortColumn = "ReceivedDate",
					InitialSortDirection = SortDirection.Desc
				},
				ClientSideEvents = new ClientSideEvents
				{
					BeforeAjaxRequest = "gridBeforeAjaxRequest"
				},
				RenderingMode = RenderingMode.Optimized
			};
		}

		public JQGrid Grid { get; set; }

		private List<JQGridColumn> InitializeAllColumns()
		{
			var allColumns = new List<JQGridColumn>();

			allColumns.Add(new JQGridColumn
			{
				DataField = "BundleID",
				PrimaryKey = true,
				Editable = false,
				Visible = false
			});

			allColumns.Add(new JQGridColumn
			{
				DataField = "BundleName",
				HeaderText = "Bundle Name",
				DataType = typeof(string),
				Sortable = true,
				Width = 150
			});

			allColumns.Add(new JQGridColumn
			{
				DataField = "Manufacturer",
				HeaderText = "Manufacturer",
				DataType = typeof(string),
				Width = 80
			});

			allColumns.Add(new JQGridColumn
			{
				DataField = "PrimaryJurisdiction",
				HeaderText = "Primary Jur",
				DataType = typeof(string),
				Width = 80
			});

			allColumns.Add(new JQGridColumn
			{
				DataField = "Supervisor",
				HeaderText = "EN Supervisor",
				DataType = typeof(string),
				Width = 100
			});

			allColumns.Add(new JQGridColumn
			{
				DataField = "QAReviewer",
				HeaderText = "QA Reviewer",
				DataType = typeof(string),
				Width = 100
			});

			allColumns.Add(new JQGridColumn
			{
				DataField = "QALetterWriter",
				HeaderText = "QA Letter Writer",
				DataType = typeof(string),
				Width = 100
			});

			allColumns.Add(new JQGridColumn
			{
				DataField = "BundleStatus",
				HeaderText = "Bundle Status",
				DataType = typeof(string),
				Width = 90
			});

			allColumns.Add(new JQGridColumn
			{
				DataField = "ProjectHolder",
				HeaderText = "Holder",
				DataType = typeof(string),
				Width = 90
			});

			allColumns.Add(new JQGridColumn
			{
				DataField = "OwningOffice",
				HeaderText = "Owning Office",
				DataType = typeof(string),
				Width = 90
			});

			allColumns.Add(new JQGridColumn
			{
				DataField = "WorkedBy",
				HeaderText = "Worked By",
				DataType = typeof(string),
				Width = 90
			});

			allColumns.Add(new JQGridColumn
			{
				DataField = "AcceptedBy",
				HeaderText = "Accepted By",
				DataType = typeof(string),
				Width = 90
			});

			allColumns.Add(new JQGridColumn
			{
				DataField = "QAOffice",
				HeaderText = "QA Office",
				DataType = typeof(string),
				Width = 90
			});

			allColumns.Add(new JQGridColumn
			{
				DataField = "TestLab",
				HeaderText = "Test lab",
				DataType = typeof(string),
				Width = 50
			});

			allColumns.Add(new JQGridColumn
			{
				DataField = "NJDays",
				HeaderText = "NJ Days",
				DataType = typeof(int),
				Width = 50
			});

			allColumns.Add(new JQGridColumn
			{
				DataField = "JurCount",
				HeaderText = "Jur Count",
				DataType = typeof(int),
				Width = 60
			});

			allColumns.Add(new JQGridColumn
			{
				DataField = "ReceivedDate",
				HeaderText = "Received Date",
				DataFormatString = "{0:d}",
				DataType = typeof(DateTime),
				Width = 90
			});

			allColumns.Add(new JQGridColumn
			{
				DataField = "TargetDate",
				HeaderText = "Target Date",
				DataFormatString = "{0:d}",
				DataType = typeof(DateTime),
				Width = 80
			});

			allColumns.Add(new JQGridColumn
			{
				DataField = "EngCompleteDate",
				HeaderText = "EN Complete Date",
				DataFormatString = "{0:d}",
				DataType = typeof(DateTime),
				Width = 110
			});

			allColumns.Add(new JQGridColumn
			{
				DataField = "AcceptedDate",
				HeaderText = "Accepted Date",
				DataFormatString = "{0:d}",
				DataType = typeof(DateTime),
				Width = 100
			});

			allColumns.Add(new JQGridColumn
			{
				DataField = "CompletedTime",
				HeaderText = "Complete Date",
				DataFormatString = "{0:d}",
				DataType = typeof(DateTime),
				Width = 100
			});

			allColumns.Add(new JQGridColumn
			{
				DataField = "Task",
				HeaderText = "Task",
				DataType = typeof(string),
				Width = 120
			});

			allColumns.Add(new JQGridColumn
			{
				DataField = "TaskTarget",
				HeaderText = "Task Target",
				DataFormatString = "{0:d}",
				DataType = typeof(DateTime),
				Width = 80
			});

			allColumns.Add(new JQGridColumn
			{
				DataField = "TaskStartedDate",
				HeaderText = "Task Start",
				DataFormatString = "{0:d}",
				DataType = typeof(DateTime),
				Width = 80
			});

			allColumns.Add(new JQGridColumn
			{
				DataField = "TaskCompletedDate",
				HeaderText = "Task Complete",
				DataFormatString = "{0:d}",
				DataType = typeof(DateTime),
				Width = 90
			});

			allColumns.Add(new JQGridColumn
			{
				DataField = "NumDaysEng",
				HeaderText = "EN Days",
				DataType = typeof(int),
				Width = 60
			});

			allColumns.Add(new JQGridColumn
			{
				DataField = "NumDaysQA",
				HeaderText = "QA Days",
				DataType = typeof(int),
				Width = 60
			});

			allColumns.Add(new JQGridColumn
			{
				DataField = "OHInEng",
				HeaderText = "OH In EN",
				DataType = typeof(int),
				Width = 60
			});

			allColumns.Add(new JQGridColumn
			{
				DataField = "OHInQA",
				HeaderText = "OH In QA",
				DataType = typeof(int),
				Width = 60
			});

			allColumns.Add(new JQGridColumn
			{
				DataField = "NCRnum",
				HeaderText = "QFI Num",
				DataType = typeof(string),
				Width = 85
			});

			allColumns.Add(new JQGridColumn
			{
				DataField = "NoCertNeeded",
				HeaderText = "NoCert Needed",
				DataType = typeof(bool),
				Width = 85
			});

			allColumns.Add(new JQGridColumn
			{
				DataField = "ENInvolvementWhileFL",
				HeaderText = "EN Involvement While FL",
				DataType = typeof(bool),
				Width = 85
			});

			allColumns.Add(new JQGridColumn
			{
				DataField = "ProjectIndicator",
				HeaderText = "Project Indicator",
				DataType = typeof(string),
				Width = 110
			});

			allColumns.Add(new JQGridColumn
			{
				DataField = "IsTransfer",
				HeaderText = "Is Transfer",
				DataType = typeof(bool),
				Width = 80
			});

			allColumns.Add(new JQGridColumn
			{
				DataField = "QADeliveryDate",
				HeaderText = "QA Delivery Date",
				DataFormatString = "{0:d}",
				DataType = typeof(DateTime),
				Width = 110
			});

			allColumns.Add(new JQGridColumn
			{
				DataField = "ExpediteReason",
				HeaderText = "Expedite Reason",
				DataType = typeof(string),
				Width = 150
			});

			allColumns.Add(new JQGridColumn
			{
				DataField = "BundleNote",
				HeaderText = "Bundle Note",
				DataType = typeof(string),
				Width = 200
			});

			allColumns.Add(new JQGridColumn
			{
				DataField = "ProjectJurs",
				HeaderText = "Jurisdictions",
				DataType = typeof(string),
				Formatter = new CustomFormatter { FormatFunction = "formatJurisdictions" },
				Width = 200
			});

			allColumns.Add(new JQGridColumn
			{
				DataField = "CPRequired",
				HeaderText = "Compliance Required",
				DataType = typeof(bool),
				Width = 85
			});

			return allColumns;
		}

		public void Export(Export export, IQueryable<QAAdvancedSearchGridRecord> query)
		{
			var results = query;

			var fileName = string.Format(export.Title + " - {0:yyyy-MM-dd_hh-mm-ss-tt}", DateTime.Now);
			var jqGridHelper = new JQGridHelper();
			var jqGridState = new JQGridState();
			jqGridState.QueryString = HttpUtility.ParseQueryString(export.FiltersToolbar ?? "");

			if (export.ExportType == ExportType.Xls)
			{
				fileName += ".xls";
				this.Grid.ExportToExcel(results, fileName, jqGridState);
			}
			else if (export.ExportType == ExportType.Csv)
			{
				fileName += ".csv";
				this.Grid.ExportSettings.ExportDataRange = ExportDataRange.All;
				var exportData = this.Grid.GetExportData(results, jqGridState);
				jqGridHelper.ExportToCSV(exportData, fileName);
			}
			else if (export.ExportType == ExportType.Pdf)
			{
				fileName += ".pdf";
				this.Grid.ExportSettings.ExportDataRange = ExportDataRange.All;
				var exportData = this.Grid.GetExportData(results, jqGridState);
				jqGridHelper.ExportToPDF(exportData, fileName);
			}
		}
	}

	public class QAAdvancedSearchGridRecord
	{
		public int BundleID { get; set; }
		public string BundleName { get; set; }
		public string Manufacturer { get; set; }
		public string PrimaryJurisdiction { get; set; }
		public string Supervisor { get; set; }
		public string QAReviewer { get; set; }
		public string QALetterWriter { get; set; }
		public string BundleStatus { get; set; }
		public string ProjectHolder { get; set; }
		public string OwningOffice { get; set; }
		public string TestLab { get; set; }
		public int? NJDays { get; set; }
		public string BundleNote { get; set; }
		public int? JurCount { get; set; }
		public DateTime? ReceivedDate { get; set; }
		public DateTime? TargetDate { get; set; }
		public DateTime? EngCompleteDate { get; set; }
		public DateTime? AcceptedDate { get; set; }
		public DateTime? CompletedTime { get; set; }
		public string Task { get; set; }
		public DateTime? TaskTarget { get; set; }
		public DateTime? TaskStartedDate { get; set; }
		public DateTime? TaskCompletedDate { get; set; }
		public int? NumDaysEng { get; set; }
		public int? NumDaysQA { get; set; }
		public int? OHInEng { get; set; }
		public int? OHInQA { get; set; }
		public string ExpediteReason { get; set; }
		public string ProjectIndicator { get; set; }
		public bool? IsTransfer { get; set; }
		public string NCRNum { get; set; }
		public DateTime? QADeliveryDate { get; set; }
		public string WorkedBy { get; set; }
		public string AcceptedBy { get; set; }
		public bool NoCertNeeded { get; set; }
		public bool ENInvolvementWhileFL { get; set; }
		public string ProjectJurs { get; set; }
		public string QAOffice { get; set; }
		public int? QAOfficeId { get; set; }
		public bool? CPRequired { get; set; }
	}
}