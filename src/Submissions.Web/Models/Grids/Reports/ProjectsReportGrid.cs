﻿using EF.Submission;
using Submissions.Common;
using Submissions.Common.JQGrid;
using Submissions.Common.Linq.Dynamic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;

namespace Submissions.Web.Models.Grids.Reports
{
	using Trirand.Web.Mvc;

	public class ProjectsReportGrid : IDynamicGrid, IColumnChooserGrid
	{
		public ProjectsReportGrid()
		{
			InitializeAllColumns();

			this.GridBaseID = "ProjectsReportGrid";
			this.Grid = new JQGrid
			{
				ID = this.GridBaseID,
				Columns = this.AllColumns.Cast<JQGridColumn>().ToList(),
				DataUrl = new UrlHelper(((MvcHandler)HttpContext.Current.Handler).RequestContext).Action("LoadReportProjectsGrid", "GridsAjax", new { Area = "" }),
				AutoWidth = false,
				ShrinkToFit = false,
				Width = Unit.Percentage(100),
				Height = Unit.Percentage(100),
				ToolBarSettings = new ToolBarSettings
				{
					ShowRefreshButton = true,
					ToolBarPosition = ToolBarPosition.Top,
					ShowSearchToolBar = true
				},
				PagerSettings = new PagerSettings
				{
					PageSize = 100,
					PageSizeOptions = "[]"
				},
				SortSettings = new SortSettings
				{
					AutoSortByPrimaryKey = false
				},
				AppearanceSettings = new AppearanceSettings
				{
					AlternateRowBackground = true,
					HighlightRowsOnHover = true,
					ShowFooter = false
				},
				ClientSideEvents = new ClientSideEvents
				{
					BeforeAjaxRequest = "projectsReportGridBeforeAjaxRequest"
				},
				RenderingMode = RenderingMode.Optimized
			};
		}

		public void InitializeDynamicGrid(string savedColumns)
		{
			this.SetColumns(savedColumns);
		}

		public void InitializeWidgetGrid(int widgetId, string savedColumns)
		{
			var grid = this.Grid;
			grid.ID += widgetId.ToString();
			grid.DataUrl = new UrlHelper(((MvcHandler)HttpContext.Current.Handler).RequestContext).Action("LoadWidgetProjectsGrid", "GridsAjax", new { Area = "" });
			grid.AutoWidth = true;
			grid.ToolBarSettings = new ToolBarSettings
			{
				ShowAddButton = false,
				ShowDeleteButton = false,
				ShowEditButton = false,
				ShowRefreshButton = false,
				ShowSearchButton = false,
				ToolBarPosition = ToolBarPosition.Top
			};
			grid.ClientSideEvents = new ClientSideEvents
			{
				AfterAjaxRequest = "gridAfterAjaxRequest"
			};
			this.SetColumns(savedColumns);
		}

		public JQGrid Grid { get; set; }
		public string GridBaseID { get; set; }
		public List<JQGridColumnDynamic> AllColumns { private set; get; }
		public List<JQGridColumnDynamic> SelectedColumns { private set; get; }
		public IList<ColumnChooserColumn> ColumnChooserSelectedColumns
		{
			get
			{
				return this.SelectedColumns
					.Where(x => x.Visible == true)
					.Select(x => new ColumnChooserColumn { DatabaseName = x.DataField, DisplayName = x.HeaderText })
					.ToList();

			}
		}
		public IList<ColumnChooserColumn> ColumnChooserAvailableColumns
		{
			get
			{
				return this.AllColumns.Except(this.SelectedColumns)
					.Where(x => x.Visible == true)
					.Select(x => new ColumnChooserColumn { DatabaseName = x.DataField, DisplayName = x.HeaderText })
					.OrderBy(x => x.DisplayName)
					.ToList();
			}
		}

		public string GetColumnsDynamicLinqFormat()
		{
			var selectFields = this.SelectedColumns;
			var selectStatement = new StringBuilder();
			selectStatement.Append("new (");

			foreach (var col in selectFields)
			{
				selectStatement.AppendFormat("{0} As {1},", col.SqlSelectName, col.DataField);
			}

			selectStatement.Length--;
			selectStatement.Append(")");

			return selectStatement.ToString();
		}

		public void SetColumns(string savedColumns)
		{
			var jqColumns = new List<JQGridColumnDynamic>();

			if (savedColumns == null)
			{
				jqColumns = this.AllColumns;
			}
			else
			{
				var widgetId = this.Grid.ID.Replace(this.GridBaseID, "");
				var savedColumnsList = new List<string>(savedColumns.Replace("\"", "").Replace("[", "").Replace("]", "").Split(','));
				var hiddenColumns = this.AllColumns.Where(x => x.Visible == false).ToList();

				jqColumns.AddRange(hiddenColumns);
				foreach (var col in savedColumnsList)
				{
					var column = this.AllColumns.Where(x => x.DataField == col).SingleOrDefault();
					if (column != null)
					{
						column.SearchControlID += widgetId;
						jqColumns.Add(column);
					}
				}
			}

			this.Grid.Columns = jqColumns.Cast<JQGridColumn>().ToList();
			this.SelectedColumns = jqColumns;
		}

		public IList<SelectListItem> GetAvailableSorts()
		{
			var columnList = new List<SelectListItem>() { new SelectListItem { Text = "", Value = "" } };

			var allColumns = this.AllColumns
				.Where(x => x.Visible && x.Searchable)
				.Select(x => new SelectListItem { Text = x.HeaderText, Value = x.SqlSelectName })
				.OrderBy(x => x.Text)
				.ToList();

			columnList.AddRange(allColumns);

			return columnList;
		}

		public void Export(Export export, IQueryable<vw_trProject> query)
		{
			if (export.WidgetId == null)
			{
				this.InitializeDynamicGrid(export.SavedColumns);
			}
			else
			{
				this.InitializeWidgetGrid((int)export.WidgetId, export.SavedColumns);
			}

			var selectStatement = this.GetColumnsDynamicLinqFormat();
			var results = query.Select(selectStatement);

			var fileName = string.Format(export.Title + " - {0:yyyy-MM-dd_hh-mm-ss-tt}", DateTime.Now);
			var jqGridHelper = new JQGridHelper();
			var jqGridState = new JQGridState();
			jqGridState.QueryString = HttpUtility.ParseQueryString(export.FiltersToolbar ?? "");

			if (export.ExportType == ExportType.Xls)
			{
				fileName += ".xls";
				this.Grid.ExportToExcel(results, fileName, jqGridState);
			}
			else if (export.ExportType == ExportType.Csv)
			{
				fileName += ".csv";
				this.Grid.ExportSettings.ExportDataRange = ExportDataRange.All;
				var exportData = this.Grid.GetExportData(results, jqGridState);
				jqGridHelper.ExportToCSV(exportData, fileName);
			}
			else if (export.ExportType == ExportType.Pdf)
			{
				fileName += ".pdf";
				this.Grid.ExportSettings.ExportDataRange = ExportDataRange.All;
				var exportData = this.Grid.GetExportData(results, jqGridState);
				jqGridHelper.ExportToPDF(exportData, fileName);
			}
		}

		private void InitializeAllColumns()
		{
			var allColumns = new List<JQGridColumnDynamic>();

			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "Id",
				SqlSelectName = "Id",
				PrimaryKey = true,
				Visible = false
			});

			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "StatusSort",
				SqlSelectName = "StatusSort",
				DataType = typeof(int),
				Visible = false
			});

			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "QAStatusSort",
				SqlSelectName = "QAStatusSort",
				DataType = typeof(int),
				Visible = false
			});

			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "IsRush",
				SqlSelectName = "IsRush",
				DataType = typeof(bool?),
				Visible = false
			});

			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "IsTransfer",
				SqlSelectName = "IsTransfer",
				DataType = typeof(int),
				Visible = false
			});

			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "StatusDescription",
				SqlSelectName = "StatusDescription",
				HeaderText = "StatusDescription",
				DataType = typeof(string),
				Visible = false
			});

			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "QAStatusDescription",
				SqlSelectName = "QAStatusDescription",
				HeaderText = "QAStatusDescription",
				DataType = typeof(string),
				Visible = false
			});

			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "MissingTarget",
				SqlSelectName = "MissingTarget",
				DataType = typeof(bool),
				Visible = false
			});

			//All Display Columns
			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "ReceiveDate",
				SqlSelectName = "ReceiveDate",
				HeaderText = "Received",
				DataFormatString = "{0:d}",
				DataType = typeof(DateTime),
				Searchable = true,
				Width = 80
			});

			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "QAReceiveDate",
				SqlSelectName = "QAReceivedDate",
				HeaderText = "QA Received",
				DataFormatString = "{0:d}",
				DataType = typeof(DateTime),
				Searchable = true,
				Width = 100
			});

			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "CreateDate",
				SqlSelectName = "CreateDate",
				HeaderText = "Created",
				DataFormatString = "{0:d}",
				DataType = typeof(DateTime),
				Searchable = true,
				Width = 80,
				Visible = false
			});

			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "AcceptDate",
				SqlSelectName = "EngAcceptDate",
				HeaderText = "Accepted",
				DataFormatString = "{0:d}",
				Searchable = true,
				DataType = typeof(DateTime),
				Width = 80
			});

			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "QAAcceptDate",
				SqlSelectName = "QAAcceptDate",
				HeaderText = "QA Accepted",
				DataFormatString = "{0:d}",
				Searchable = true,
				DataType = typeof(DateTime),
				Width = 100
			});

			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "ProjectName",
				SqlSelectName = "ProjectName",
				HeaderText = "Project Name",
				Formatter = new CustomFormatter { FormatFunction = "projectLink" },
				DataType = typeof(string),
				Searchable = true,
				SearchToolBarOperation = SearchOperation.BeginsWith,
				SearchType = SearchType.AutoComplete,
				SearchControlID = "ProjectAutoComplete",
				Width = 150
			});

			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "SubmissionGameName",
				SqlSelectName = "SubmissionGameName",
				HeaderText = "Game Name",
				DataType = typeof(string),
				Searchable = true,
				SearchToolBarOperation = SearchOperation.BeginsWith,
				SearchType = SearchType.AutoComplete,
				SearchControlID = "SubmissionGameNameAutoComplete",
				Width = 200
			});

			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "SubmissionDateCode",
				SqlSelectName = "SubmissionDateCode",
				HeaderText = "Date Code",
				DataType = typeof(string),
				Searchable = true,
				SearchToolBarOperation = SearchOperation.BeginsWith,
				Width = 200
			});

			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "Laboratory",
				SqlSelectName = "Laboratory",
				HeaderText = "Cur Location",
				DataType = typeof(string),
				Searchable = true,
				Width = 90,
				Visible = false
			});

			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "Status",
				SqlSelectName = "Status",
				HeaderText = "Status",
				Width = 70,
				//Formatter = new CustomFormatter { FormatFunction = "statusFormat" },
				DataType = typeof(string),
			});

			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "BundleStatus",
				SqlSelectName = "BundleStatus",
				HeaderText = "QA Status",
				Width = 80,
				//Formatter = new CustomFormatter { FormatFunction = "statusFormat" },
				DataType = typeof(string),
			});

			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "TestLab",
				SqlSelectName = "TestLab",
				HeaderText = "Test Lab",
				DataType = typeof(string),
				Width = 90
			});

			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "Manager",
				SqlSelectName = "Manager",
				HeaderText = "Manager",
				Width = 90,
				//Formatter = new CustomFormatter { FormatFunction = "statusFormat" },
				DataType = typeof(string),
			});

			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "QAReviewer",
				SqlSelectName = "QAReviewer",
				HeaderText = "Reviewer",
				Width = 110,
				//Formatter = new CustomFormatter { FormatFunction = "statusFormat" },
				DataType = typeof(string),
			});

			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "QALetterWriter",
				SqlSelectName = "QALetterWriter",
				HeaderText = "Letter Writer",
				Width = 110,
				//Formatter = new CustomFormatter { FormatFunction = "statusFormat" },
				DataType = typeof(string),
			});

			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "EstCompleteDate",
				SqlSelectName = "EstCompleteDate",
				HeaderText = "Est Eng Complete",
				DataFormatString = "{0:d}",
				Searchable = true,
				DataType = typeof(DateTime),
				Width = 120
			});

			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "ReqQADelivery",
				SqlSelectName = "ReqQADelivery",
				HeaderText = "Req QA Delivery",
				DataFormatString = "{0:d}",
				Searchable = true,
				DataType = typeof(DateTime),
				Width = 100
			});

			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "CompleteDate",
				SqlSelectName = "EngCompleteDate",
				HeaderText = "Eng Complete",
				DataFormatString = "{0:d}",
				Searchable = true,
				DataType = typeof(DateTime),
				Width = 100
			});

			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "TargetDate",
				SqlSelectName = "TargetDate",
				HeaderText = "Target Date",
				DataFormatString = "{0:d}",
				//Formatter = new CustomFormatter { FormatFunction = "manageTargetDate" },
				Searchable = true,
				DataType = typeof(DateTime),
				Width = 90
			});

			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "TaskStartDate",
				SqlSelectName = "TaskStartDate",
				HeaderText = "Start Date",
				DataFormatString = "{0:d}",
				//Formatter = new CustomFormatter { FormatFunction = "manageTargetDate" },
				Searchable = true,
				DataType = typeof(DateTime),
				Width = 90,
				Visible = false
			});

			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "OrgTargetDate",
				SqlSelectName = "OrgTargetDate",
				HeaderText = "Orig Target",
				DataFormatString = "{0:d}",
				//Formatter = new CustomFormatter { FormatFunction = "manageTargetDate" },
				DataType = typeof(DateTime),
				Width = 90,
				Visible = false
			});

			this.AllColumns = allColumns;
		}
	}
}