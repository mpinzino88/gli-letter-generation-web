﻿using EF.Submission;
using Submissions.Common;
using Submissions.Common.JQGrid;
using Submissions.Common.Linq.Dynamic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;

namespace Submissions.Web.Models.Grids.Reports
{
	using Trirand.Web.Mvc;

	public class TasksReportGrid : IDynamicGrid, IColumnChooserGrid
	{
		public TasksReportGrid()
		{
			InitializeAllColumns();

			this.GridBaseID = "TasksReportGrid";
			this.Grid = new JQGrid
			{
				ID = this.GridBaseID,
				Columns = this.AllColumns.Cast<JQGridColumn>().ToList(),
				DataUrl = new UrlHelper(((MvcHandler)HttpContext.Current.Handler).RequestContext).Action("LoadReportTasksGrid", "GridsAjax", new { Area = "" }),
				AutoWidth = false,
				ShrinkToFit = false,
				Width = Unit.Percentage(100),
				Height = Unit.Percentage(100),
				ToolBarSettings = new ToolBarSettings
				{
					ShowRefreshButton = true,
					ToolBarPosition = ToolBarPosition.Top,
					ShowSearchToolBar = true,
				},
				PagerSettings = new PagerSettings
				{
					PageSize = 100,
					PageSizeOptions = "[]"
				},
				SortSettings = new SortSettings
				{
					AutoSortByPrimaryKey = false
				},
				AppearanceSettings = new AppearanceSettings
				{
					AlternateRowBackground = true,
					HighlightRowsOnHover = true,
					ShowFooter = false
				},
				ClientSideEvents = new ClientSideEvents
				{
					BeforeAjaxRequest = "tasksReportGridBeforeAjaxRequest"
				},
				RenderingMode = RenderingMode.Optimized
			};
		}

		public void InitializeDynamicGrid(string savedColumns)
		{
			this.SetColumns(savedColumns);
		}

		public void InitializeWidgetGrid(int widgetId, string savedColumns)
		{
			var grid = this.Grid;
			grid.ID += widgetId.ToString();
			grid.DataUrl = new UrlHelper(((MvcHandler)HttpContext.Current.Handler).RequestContext).Action("LoadWidgetTasksGrid", "GridsAjax", new { Area = "" });
			grid.AutoWidth = true;
			grid.ToolBarSettings = new ToolBarSettings
			{
				ShowAddButton = false,
				ShowDeleteButton = false,
				ShowEditButton = false,
				ShowRefreshButton = false,
				ShowSearchButton = false,
				ToolBarPosition = ToolBarPosition.Top
			};
			grid.ClientSideEvents = new ClientSideEvents
			{
				AfterAjaxRequest = "gridAfterAjaxRequest"
			};
			this.SetColumns(savedColumns);
		}

		public JQGrid Grid { get; set; }
		public string GridBaseID { get; set; }
		public List<JQGridColumnDynamic> AllColumns { private set; get; }
		public List<JQGridColumnDynamic> SelectedColumns { private set; get; }
		public IList<ColumnChooserColumn> ColumnChooserSelectedColumns
		{
			get
			{
				return this.SelectedColumns
					.Where(x => x.Visible == true)
					.Select(x => new ColumnChooserColumn { DatabaseName = x.DataField, DisplayName = x.HeaderText })
					.ToList();

			}
		}
		public IList<ColumnChooserColumn> ColumnChooserAvailableColumns
		{
			get
			{
				return this.AllColumns.Except(this.SelectedColumns)
					.Where(x => x.Visible == true)
					.Select(x => new ColumnChooserColumn { DatabaseName = x.DataField, DisplayName = x.HeaderText })
					.OrderBy(x => x.DisplayName)
					.ToList();
			}
		}

		public string GetColumnsDynamicLinqFormat()
		{
			var selectFields = this.SelectedColumns;
			var selectStatement = new StringBuilder();
			selectStatement.Append("new (");

			foreach (var col in selectFields)
			{
				selectStatement.AppendFormat("{0} As {1},", col.SqlSelectName, col.DataField);
			}

			selectStatement.Length--;
			selectStatement.Append(")");

			return selectStatement.ToString();
		}

		public void SetColumns(string savedColumns)
		{
			var jqColumns = new List<JQGridColumnDynamic>();

			if (savedColumns == null)
			{
				jqColumns = this.AllColumns;
			}
			else
			{
				var widgetId = this.Grid.ID.Replace(this.GridBaseID, "");
				var savedColumnsList = new List<string>(savedColumns.Replace("\"", "").Replace("[", "").Replace("]", "").Split(','));
				var hiddenColumns = this.AllColumns.Where(x => x.Visible == false).ToList();

				jqColumns.AddRange(hiddenColumns);
				foreach (var col in savedColumnsList)
				{
					var column = this.AllColumns.Where(x => x.DataField == col).SingleOrDefault();
					if (column != null)
					{
						column.SearchControlID += widgetId;
						jqColumns.Add(column);
					}
				}
			}

			this.Grid.Columns = jqColumns.Cast<JQGridColumn>().ToList();
			this.SelectedColumns = jqColumns;
		}

		public IList<SelectListItem> GetAvailableSorts()
		{
			var columnList = new List<SelectListItem>() { new SelectListItem { Text = "", Value = "" } };

			var allColumns = this.AllColumns
				.Where(x => x.Visible && x.Searchable)
				.Select(x => new SelectListItem { Text = x.HeaderText, Value = x.SqlSelectName })
				.OrderBy(x => x.Text)
				.ToList();

			columnList.AddRange(allColumns);

			return columnList;
		}

		public void Export(Export export, IQueryable<vw_trTask> query)
		{
			if (export.WidgetId == null)
			{
				this.InitializeDynamicGrid(export.SavedColumns);
			}
			else
			{
				this.InitializeWidgetGrid((int)export.WidgetId, export.SavedColumns);
			}

			var selectStatement = this.GetColumnsDynamicLinqFormat();
			var results = query.Select(selectStatement);

			var fileName = string.Format(export.Title + " - {0:yyyy-MM-dd_hh-mm-ss-tt}", DateTime.Now);
			var jqGridHelper = new JQGridHelper();
			var jqGridState = new JQGridState();
			jqGridState.QueryString = HttpUtility.ParseQueryString(export.FiltersToolbar ?? "");

			if (export.ExportType == ExportType.Xls)
			{
				fileName += ".xls";
				this.Grid.ExportToExcel(results, fileName, jqGridState);
			}
			else if (export.ExportType == ExportType.Csv)
			{
				fileName += ".csv";
				this.Grid.ExportSettings.ExportDataRange = ExportDataRange.All;
				var exportData = this.Grid.GetExportData(results, jqGridState);
				jqGridHelper.ExportToCSV(exportData, fileName);
			}
			else if (export.ExportType == ExportType.Pdf)
			{
				fileName += ".pdf";
				this.Grid.ExportSettings.ExportDataRange = ExportDataRange.All;
				var exportData = this.Grid.GetExportData(results, jqGridState);
				jqGridHelper.ExportToPDF(exportData, fileName);
			}
		}

		private void InitializeAllColumns()
		{
			var allColumns = new List<JQGridColumnDynamic>();

			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "Id",
				SqlSelectName = "Id",
				PrimaryKey = true,
				Editable = false,
				Visible = false
			});
			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "TaskDefinitionId",
				SqlSelectName = "TaskDefinitionId",
				Editable = false,
				Visible = false
			});
			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "ProjectId",
				SqlSelectName = "ProjectId",
				Editable = false,
				Visible = false
			});
			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "IsRush",
				SqlSelectName = "IsRush",
				Editable = false,
				Visible = false
			});

			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "BundleType",
				SqlSelectName = "BundleType",
				Editable = false,
				Visible = false
			});

			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "ProjectTargetDiff",
				SqlSelectName = "ProjectTargetDiff",
				Editable = false,
				Visible = false
			});

			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "ProjectHolder",
				SqlSelectName = "ProjectHolder",
				HeaderText = "Holder",
				DataType = typeof(string),
				Searchable = true,
				SearchToolBarOperation = SearchOperation.BeginsWith,
				Width = 110,
				Visible = false
			});

			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "Department",
				SqlSelectName = "Department.Code",
				HeaderText = "Department",
				DataType = typeof(string),
				Searchable = true,
				SearchToolBarOperation = SearchOperation.BeginsWith,
				Width = 100,
				SearchType = SearchType.DropDown,
				SearchList = LookupsStandard.ConvertEnum(showBlank: true, enumValues: new Department[] { Department.QA, Department.ENG, Department.MTH }).ToList()
			});

			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "ProjectName",
				SqlSelectName = "ProjectName",
				HeaderText = "Project",
				DataType = typeof(string),
				Searchable = true,
				SearchToolBarOperation = SearchOperation.Contains,
				Formatter = new CustomFormatter { FormatFunction = "projectLink" },
				Width = 175
			});

			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "Name",
				SqlSelectName = "Name",
				HeaderText = "Task",
				DataType = typeof(string),
				Searchable = true,
				SearchToolBarOperation = SearchOperation.BeginsWith,
				Width = 250
			});

			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "Status",
				SqlSelectName = "TaskStatus",
				HeaderText = "Status",
				DataType = typeof(string),
				Searchable = true,
				SearchToolBarOperation = SearchOperation.BeginsWith,
				Formatter = new CustomFormatter { FormatFunction = "taskStatus" },
				Width = 100
			});

			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "User",
				SqlSelectName = "User.FName + \" \" + User.LName",
				HeaderText = "Assigned To",
				DataType = typeof(string),
				SearchToolBarOperation = SearchOperation.BeginsWith,
				SearchType = SearchType.AutoComplete,
				SearchControlID = "UserAutoComplete",
				Width = 150
			});

			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "AssignDate",
				SqlSelectName = "AssignDate",
				HeaderText = "Assign Date",
				DataFormatString = "{0:d}",
				DataType = typeof(DateTime),
				Width = 90,
				Searchable = true
			});

			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "StartDate",
				SqlSelectName = "StartDate",
				HeaderText = "Start Date",
				DataFormatString = "{0:d}",
				DataType = typeof(DateTime),
				Width = 90,
				Searchable = true
			});

			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "TargetDate",
				SqlSelectName = "TargetDate",
				HeaderText = "Target Date",
				DataFormatString = "{0:d}",
				DataType = typeof(DateTime),
				Width = 90,
				Searchable = true
			});

			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "EstimateCompleteDate",
				SqlSelectName = "EstimateCompleteDate",
				HeaderText = "Est Complete",
				DataFormatString = "{0:d}",
				DataType = typeof(DateTime),
				Width = 90,
				Searchable = true
			});

			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "CompleteDate",
				SqlSelectName = "CompleteDate",
				HeaderText = "Complete Date",
				DataFormatString = "{0:d}",
				DataType = typeof(DateTime),
				Width = 90,
				Searchable = true
			});

			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "EstimateHours",
				SqlSelectName = "EstimateHours",
				HeaderText = "Est Hours",
				DataType = typeof(decimal),
				Width = 70,
				Searchable = true
			});

			this.AllColumns = allColumns;
		}
	}
}