﻿using System.Web.UI.WebControls;

namespace Submissions.Web.Models.Grids.Lookups
{
	using System.Collections.Generic;
	using System.Web;
	using System.Web.Mvc;
	using Trirand.Web.Mvc;

	public class MobileDevicesGrid
	{
		public JQGrid Grid { get; set; }

		public MobileDevicesGrid()
		{
			Grid = new JQGrid
			{
				AutoWidth = true,
				Columns = LoadColumns(),
				DataUrl = new UrlHelper(((MvcHandler)HttpContext.Current.Handler).RequestContext).Action("LoadMobileDevicesGrid", "GridsAjax", new { Area = "" }),
				Height = Unit.Percentage(100),
				ID = "MobileDevicesGrid",
				RenderingMode = RenderingMode.Optimized,
				Width = Unit.Percentage(100),
				AppearanceSettings = new AppearanceSettings
				{
					AlternateRowBackground = true,
					HighlightRowsOnHover = true,
					ShowFooter = false
				},
				PagerSettings = new PagerSettings
				{
					PageSize = 100,
					PageSizeOptions = "[]"
				},
				SortSettings = new SortSettings
				{
					InitialSortColumn = "Device",
					InitialSortDirectionString = "asc"
				},
				ToolBarSettings = new ToolBarSettings
				{
					ShowRefreshButton = true,
					ShowSearchToolBar = true,
					ToolBarPosition = ToolBarPosition.Top
				}
			};
		}

		private List<JQGridColumn> LoadColumns()
		{
			return new List<JQGridColumn>
			{
				new JQGridColumn
				{
					DataField = "Id",
					PrimaryKey = true,
					Editable = false,
					Visible = false
				},

				new JQGridColumn
				{
					DataField = "Device",
					HeaderText = "Device",
					DataType = typeof(string),
					Width = 100
				},

				new JQGridColumn
				{
					DataField = "Browser",
					HeaderText = "Browser",
					DataType = typeof(string),
					Width = 100
				},

				new JQGridColumn
				{
					DataField = "Model",
					HeaderText = "Model",
					DataType = typeof(string),
					Width = 100
				},

				new JQGridColumn
				{
					DataField = "OperatingSystem",
					HeaderText = "Operating System",
					DataType = typeof(string),
					Width = 50
				},

				new JQGridColumn
				{
					DataField = "Type",
					HeaderText = "Type",
					DataType = typeof(string),
					Width = 50
				},

				new JQGridColumn
				{
					DataField = "DeviceId",
					HeaderText = "Device Id",
					DataType = typeof(string),
					Width = 100
				},

				new JQGridColumn
				{
					DataField = "Location",
					HeaderText = "Location",
					DataType = typeof(string),
					SearchToolBarOperation = SearchOperation.BeginsWith,
					Width = 100
				},

				new JQGridColumn
				{
					DataField = "Edit",
					HeaderText = " ",
					Formatter = new CustomFormatter { FormatFunction = "editLink" },
					Searchable = false,
					Sortable = false,
					Width = 50,
					TextAlign = TextAlign.Right,
					CssClass = "padright"
				}
			};
		}
	}

	public class MobileDeviceGridRecord
	{
		public int Id { get; set; }

		public string Device { get; set; }

		public string Browser { get; set; }

		public string Model { get; set; }

		public string OperatingSystem { get; set; }

		public string Type { get; set; }

		public string DeviceId { get; set; }

		public string Location { get; set; }

		public string Edit { get; set; }
	}
}