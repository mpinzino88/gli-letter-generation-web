﻿using Submissions.Web.Models.LookupsAjax;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;

namespace Submissions.Web.Models.Grids.Lookups
{
	using Trirand.Web.Mvc;

	public class LetterContactsGrid
	{
		public LetterContactsGrid()
		{
			this.Grid = new JQGrid
			{
				ID = "LetterContactsGrid",
				Columns = LoadColumns(),
				DataUrl = new UrlHelper(((MvcHandler)HttpContext.Current.Handler).RequestContext).Action("LoadLetterContactsGrid", "GridsAjax", new { Area = "" }),
				AutoWidth = true,
				Width = Unit.Percentage(100),
				Height = Unit.Percentage(100),

				ToolBarSettings = new ToolBarSettings
				{
					ShowRefreshButton = true,
					ShowSearchToolBar = true,
					ToolBarPosition = ToolBarPosition.Top
				},
				PagerSettings = new PagerSettings
				{
					PageSize = 100,
					PageSizeOptions = "[]"
				},
				AppearanceSettings = new AppearanceSettings
				{
					AlternateRowBackground = true,
					HighlightRowsOnHover = true,
					ShowFooter = false
				},
				SortSettings = new SortSettings
				{
					InitialSortColumn = "Name",
					InitialSortDirectionString = "asc"
				},
				ClientSideEvents = new ClientSideEvents
				{
					BeforeAjaxRequest = "gridBeforeAjaxRequest"
				},


				RenderingMode = RenderingMode.Optimized
			};

			//this.Filters = new ManufacturerGroupFilter();
			this.Search = new LetterContactsSearch();
		}

		public LetterContactsSearch Search { get; set; }
		public JQGrid Grid { get; set; }

		private List<JQGridColumn> LoadColumns()
		{
			var columns = new List<JQGridColumn>();

			columns.Add(new JQGridColumn
			{
				DataField = "Id",
				PrimaryKey = true,
				Editable = false,
				Visible = false
			});

			columns.Add(new JQGridColumn
			{
				DataField = "Name",
				HeaderText = "Name",
				DataType = typeof(string),
				Searchable = false,
				Width = 50
			});

			columns.Add(new JQGridColumn
			{
				DataField = "ContactType",
				HeaderText = "Contact Type",
				DataType = typeof(string),
				Searchable = false,
				Width = 50
			});

			columns.Add(new JQGridColumn
			{
				DataField = "ManufacturerList",
				HeaderText = "Manufacturer List",
				DataType = typeof(string),
				Searchable = false,
				Width = 100
			});

			columns.Add(new JQGridColumn
			{
				DataField = "JurisdictionList",
				HeaderText = "Jurisdiction List",
				DataType = typeof(string),
				Searchable = false,
				Width = 100
			});

			columns.Add(new JQGridColumn
			{
				DataField = "Edit",
				HeaderText = " ",
				Formatter = new CustomFormatter { FormatFunction = "editLink" },
				Searchable = false,
				Sortable = false,
				Width = 50,
				TextAlign = TextAlign.Right,
				CssClass = "padright"
			});

			return columns;
		}
	}

	public class LetterContactsGridRecord
	{
		public int Id { get; set; }
		public string ContactType { get; set; }
		public string Name { get; set; }
		public string ManufacturerList { get; set; }
		public string JurisdictionList { get; set; }
		public string Edit { get; set; }

	}
}