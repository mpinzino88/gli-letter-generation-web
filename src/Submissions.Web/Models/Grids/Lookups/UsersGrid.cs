﻿using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;

namespace Submissions.Web.Models.Grids.Lookups
{
	using Trirand.Web.Mvc;

	public class UsersGrid
	{
		public UsersGrid()
		{
			this.Grid = new JQGrid
			{
				ID = "UserGrid",
				Columns = LoadColumns(),
				DataUrl = new UrlHelper(((MvcHandler)HttpContext.Current.Handler).RequestContext).Action("LoadUserGrid", "GridsAjax", new { Area = "" }),
				AutoWidth = true,
				Width = Unit.Percentage(100),
				Height = Unit.Percentage(100),
				ToolBarSettings = new ToolBarSettings
				{
					ShowRefreshButton = true,
					ShowSearchToolBar = true,
					ToolBarPosition = ToolBarPosition.Top
				},
				PagerSettings = new PagerSettings
				{
					PageSize = 100,
					PageSizeOptions = "[]"
				},
				AppearanceSettings = new AppearanceSettings
				{
					AlternateRowBackground = true,
					HighlightRowsOnHover = true,
					ShowFooter = false
				},
				ClientSideEvents = new ClientSideEvents
				{
					BeforeAjaxRequest = "gridBeforeAjaxRequest"
				},
				SortSettings = new SortSettings
				{
					InitialSortColumn = "Status, Name"
				},
				RenderingMode = RenderingMode.Optimized
			};
		}

		public JQGrid Grid { get; set; }

		private List<JQGridColumn> LoadColumns()
		{
			var columns = new List<JQGridColumn>();

			columns.Add(new JQGridColumn
			{
				DataField = "Id",
				PrimaryKey = true,
				Editable = false,
				Visible = false
			});

			columns.Add(new JQGridColumn
			{
				DataField = "StringId",
				HeaderText = "Id",
				Editable = false,
				DataType = typeof(string),
				Searchable = true,
				Sortable = true,
				SearchToolBarOperation = SearchOperation.Contains,
				Width = 40
			});

			columns.Add(new JQGridColumn
			{
				DataField = "Status",
				Visible = false
			});

			columns.Add(new JQGridColumn
			{
				DataField = "Name",
				HeaderText = "Name",
				DataType = typeof(string),
				SearchToolBarOperation = SearchOperation.Contains
			});

			columns.Add(new JQGridColumn
			{
				DataField = "Email",
				HeaderText = "Email",
				DataType = typeof(string),
				SearchToolBarOperation = SearchOperation.BeginsWith
			});

			columns.Add(new JQGridColumn
			{
				DataField = "CurrentOffice",
				HeaderText = "Current Office",
				DataType = typeof(string),
				SearchToolBarOperation = SearchOperation.BeginsWith,
				Width = 80
			});

			columns.Add(new JQGridColumn
			{
				DataField = "HomeOffice",
				HeaderText = "Home Office",
				DataType = typeof(string),
				SearchToolBarOperation = SearchOperation.BeginsWith,
				Width = 80
			});

			columns.Add(new JQGridColumn
			{
				DataField = "DepartmentCode",
				HeaderText = "Department",
				DataType = typeof(string),
				SearchToolBarOperation = SearchOperation.BeginsWith,
				Width = 80
			});

			columns.Add(new JQGridColumn
			{
				DataField = "RoleCode",
				HeaderText = "Role",
				DataType = typeof(string),
				SearchToolBarOperation = SearchOperation.BeginsWith,
				Width = 80
			});

			columns.Add(new JQGridColumn
			{
				DataField = "Edit",
				HeaderText = " ",
				Formatter = new CustomFormatter { FormatFunction = "editLink" },
				Searchable = false,
				Sortable = false,
				Width = 50,
				TextAlign = TextAlign.Right,
				CssClass = "padright"
			});

			return columns;
		}
	}

	public class UserGridRecord
	{
		public int Id { get; set; }
		public string StringId { get; set; }
		public string Status { get; set; }
		public string Name { get; set; }
		public string Email { get; set; }
		public string CurrentOffice { get; set; }
		public string HomeOffice { get; set; }
		public string DepartmentCode { get; set; }
		public string RoleCode { get; set; }
		public string Edit { get; set; }
	}
}