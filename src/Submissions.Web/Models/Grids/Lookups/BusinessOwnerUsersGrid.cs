﻿using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;
using Trirand.Web.Mvc;

namespace Submissions.Web.Models.Grids.Lookups
{
	public class BusinessOwnerUsersGrid
	{
		public BusinessOwnerUsersGrid()
		{
			Grid = new JQGrid
			{
				Columns = LoadColumns(),
				DataUrl = new UrlHelper(((MvcHandler)HttpContext.Current.Handler).RequestContext).Action("LoadBusinessOwnerUsersGrid", "GridsAjax", new { Area = "" }),
				AutoWidth = true,
				Width = Unit.Percentage(100),
				Height = Unit.Percentage(100),
				ToolBarSettings = new ToolBarSettings
				{
					ShowRefreshButton = true,
					ShowSearchToolBar = true,
					ToolBarPosition = ToolBarPosition.Top,
				},
				AppearanceSettings = new AppearanceSettings
				{
					AlternateRowBackground = true,
					HighlightRowsOnHover = true,
					ShowFooter = false
				},
				RenderingMode = RenderingMode.Optimized
			};
		}

		public JQGrid Grid { get; set; }

		private List<JQGridColumn> LoadColumns()
		{
			var columns = new List<JQGridColumn>
			{
				new JQGridColumn
				{
					DataField = "Id",
					PrimaryKey = true,
					Editable = false,
					Visible = false
				},
				new JQGridColumn
				{
					DataField = "FirstName",
					HeaderText = "First Name",
					DataType = typeof(string),
					Width = 50,
					SearchToolBarOperation = SearchOperation.Contains
				},
				new JQGridColumn
				{
					DataField = "LastName",
					HeaderText = "Last Name",
					DataType = typeof(string),
					Width = 50,
					SearchToolBarOperation = SearchOperation.Contains
				},
				new JQGridColumn
				{
					DataField = "Location",
					HeaderText = "Current Office",
					DataType = typeof(string)
				},
				new JQGridColumn
				{
					DataField = "Department",
					HeaderText = "Department",
					DataType = typeof(string)
				},
			};

			return columns;
		}
	}

	public class BusinessOwnerUsersGridRecord
	{
		public int Id { get; set; }
		public string FirstName { get; set; }
		public string LastName { get; set; }
		public string Location { get; set; }
		public string Department { get; set; }
		public string Edit { get; set; }
	}
}