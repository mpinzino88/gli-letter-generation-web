﻿using Submissions.Web.Models.LookupsAjax;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;

namespace Submissions.Web.Models.Grids.Lookups
{
	using Trirand.Web.Mvc;

	public class ManufacturerGroupsGrid
	{
		public ManufacturerGroupsGrid()
		{
			this.Grid = new JQGrid
			{
				ID = "ManufacturerGroupsGrid",
				Columns = LoadColumns(),
				DataUrl = new UrlHelper(((MvcHandler)HttpContext.Current.Handler).RequestContext).Action("LoadManufacturerGroupsGrid", "GridsAjax", new { Area = "" }),
				AutoWidth = true,
				Width = Unit.Percentage(100),
				Height = Unit.Percentage(100),

				ToolBarSettings = new ToolBarSettings
				{
					ShowRefreshButton = true,
					ShowSearchToolBar = true,
					ToolBarPosition = ToolBarPosition.Top
				},
				PagerSettings = new PagerSettings
				{
					PageSize = 100,
					PageSizeOptions = "[]"
				},
				AppearanceSettings = new AppearanceSettings
				{
					AlternateRowBackground = true,
					HighlightRowsOnHover = true,
					ShowFooter = false
				},
				SortSettings = new SortSettings
				{
					InitialSortColumn = "ManufacturerGroupCategoryCode, Description",
					InitialSortDirectionString = "asc, asc"
				},
				ClientSideEvents = new ClientSideEvents
				{
					BeforeAjaxRequest = "gridBeforeAjaxRequest"
				},


				RenderingMode = RenderingMode.Optimized
			};

			this.Filters = new ManufacturerGroupFilter();
			this.Search = new ManufacturerGroupSearch();
		}

		public ManufacturerGroupFilter Filters { get; set; }

		public ManufacturerGroupSearch Search { get; set; }

		public JQGrid Grid { get; set; }

		private List<JQGridColumn> LoadColumns()
		{
			var columns = new List<JQGridColumn>();

			columns.Add(new JQGridColumn
			{
				DataField = "Id",
				PrimaryKey = true,
				Editable = false,
				Visible = false
			});

			columns.Add(new JQGridColumn
			{
				DataField = "Description",
				HeaderText = "Group Name",
				DataType = typeof(string),
				Width = 50,

			});

			columns.Add(new JQGridColumn
			{
				DataField = "ManufacturerGroupCategoryCode",
				HeaderText = "Category",
				DataType = typeof(string),
				Searchable = false,
				Width = 50
			});

			columns.Add(new JQGridColumn
			{
				DataField = "ManufacturerGroupCategoryDescription",
				HeaderText = "Category Description",
				DataType = typeof(string),
				Width = 50
			});

			columns.Add(new JQGridColumn
			{
				DataField = "Edit",
				HeaderText = " ",
				Formatter = new CustomFormatter { FormatFunction = "editLink" },
				Searchable = false,
				Sortable = false,
				Width = 50,
				TextAlign = TextAlign.Right,
				CssClass = "padright"
			});

			return columns;
		}
	}
	public class ManufacturerGroupsGridRecord
	{
		public int Id { get; set; }
		public string Description { get; set; }
		public string ManufacturerGroupCategoryCode { get; set; }
		public string ManufacturerGroupCategoryDescription { get; set; }
		public string Edit { get; set; }
	}
}