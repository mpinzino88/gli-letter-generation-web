﻿using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using Trirand.Web.Mvc;

namespace Submissions.Web.Models.Grids.Lookups
{
	public class LBDefaultsGrid
	{
		public LBDefaultsGrid()
		{
			this.Grid = new JQGrid
			{
				ID = "LBDefaultsGrid",
				Columns = LoadColumns(),
				DataUrl = new UrlHelper(((MvcHandler)HttpContext.Current.Handler).RequestContext).Action("LoadLBDefaultsGrid", "GridsAjax", new { Area = "" }),
				ToolBarSettings = new ToolBarSettings
				{
					ShowRefreshButton = true,
					ToolBarPosition = ToolBarPosition.Top,
					ShowSearchToolBar = true
				},
			};
		}

		public JQGrid Grid { get; set; }
		private List<JQGridColumn> LoadColumns()
		{
			var columns = new List<JQGridColumn>();

			columns.Add(new JQGridColumn
			{
				DataField = "Id",
				Editable = false,
				Visible = false,
				DataType = typeof(int),
			});

			columns.Add(new JQGridColumn
			{
				DataField = "Jurisdictions",
				DataType = typeof(string),
				Sortable = true,
				Width = 100,
				Searchable = true
			});

			columns.Add(new JQGridColumn
			{
				DataField = "Manufacturers",
				DataType = typeof(string),
				Sortable = true,
				Width = 150,
				Searchable = true
			});

			columns.Add(new JQGridColumn
			{
				DataField = "SubmissionTypes",
				DataType = typeof(string),
				Sortable = true,
				Width = 50,
				Searchable = true
			});

			columns.Add(new JQGridColumn
			{
				DataField = "ContractTypes",
				DataType = typeof(string),
				Sortable = true,
				Width = 50,
				Searchable = true
			});

			columns.Add(new JQGridColumn
			{
				DataField = "BillingParties",
				DataType = typeof(string),
				Sortable = true,
				Width = 50,
				Searchable = true
			});

			columns.Add(new JQGridColumn
			{
				DataField = "LWLocations",
				DataType = typeof(string),
				Sortable = true,
				Width = 50,
				Searchable = true
			});

			columns.Add(new JQGridColumn
			{
				DataField = "Companies",
				DataType = typeof(string),
				Sortable = true,
				Width = 90,
				Searchable = true
			});

			columns.Add(new JQGridColumn
			{
				DataField = "DocumentTypes",
				DataType = typeof(string),
				Sortable = true,
				Width = 50,
				Searchable = true
			});

			columns.Add(new JQGridColumn
			{
				DataField = "Status",
				DataType = typeof(string),
				Sortable = true,
				Width = 30,
				Searchable = true
			});

			columns.Add(new JQGridColumn
			{
				DataField = "Billable",
				DataType = typeof(string),
				Sortable = true,
				Width = 30,
				Searchable = true
			});

			return columns;
		}
	}
	public class LBDefaultsGridRecord
	{
		public int Id { get; set; }
		public string Jurisdictions { get; set; }
		public string Manufacturers { get; set; }
		public string SubmissionTypes { get; set; }
		public string DocumentTypes { get; set; }
		public string Companies { get; set; }
		public string ContractTypes { get; set; }
		public string BillingParties { get; set; }
		public string LWLocations { get; set; }
		public string Status { get; set; }
		public string Billable { get; set; }
	}
}