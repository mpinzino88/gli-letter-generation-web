﻿using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using Trirand.Web.Mvc;

namespace Submissions.Web.Models.Grids.Lookups
{
	public class AppFeeGrid
	{
		public AppFeeGrid()
		{
			this.Grid = new JQGrid
			{
				ID = "AppFeeGrid",
				Columns = LoadColumns(),
				DataUrl = new UrlHelper(((MvcHandler)HttpContext.Current.Handler).RequestContext).Action("LoadAppFeeGrid", "GridsAjax", new { Area = "" }),
				ToolBarSettings = new ToolBarSettings
				{
					ShowRefreshButton = true,
					ToolBarPosition = ToolBarPosition.Top,
					ShowSearchToolBar = true
				},
			};
		}

		public JQGrid Grid { get; set; }
		private List<JQGridColumn> LoadColumns()
		{
			var columns = new List<JQGridColumn>();

			columns.Add(new JQGridColumn
			{
				DataField = "Id",
				Editable = false,
				Visible = false,
				DataType = typeof(int),
			});

			columns.Add(new JQGridColumn
			{
				DataField = "Name",
				DataType = typeof(string),
				Sortable = true,
				Width = 75,
				Searchable = true
			});

			columns.Add(new JQGridColumn
			{
				DataField = "Amount",
				DataType = typeof(int),
				Sortable = true,
				Width = 50,
				Searchable = true
			});

			columns.Add(new JQGridColumn
			{
				DataField = "Description",
				DataType = typeof(string),
				Sortable = true,
				Width = 50,
				Searchable = true
			});

			columns.Add(new JQGridColumn
			{
				DataField = "Active",
				DataType = typeof(bool),
				Sortable = true,
				Width = 50,
				Searchable = true
			});

			return columns;
		}
	}
	public class AppFeeGridRecord
	{
		public int Id { get; set; }
		public string Name { get; set; }
		public string Description { get; set; }
		public int Amount { get; set; }
		public bool Active { get; set; }
	}
}