﻿using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;

namespace Submissions.Web.Models.Grids.Lookups
{
	using Trirand.Web.Mvc;
	public class AcumaticaBillWithAndCustomerExceptionsGrid
	{
		public AcumaticaBillWithAndCustomerExceptionsGrid()
		{
			Grid = new JQGrid
			{
				ID = "AcumaticaBillWithAndCustomerExceptionsGrid",
				Columns = LoadColumns(),
				DataUrl = new UrlHelper(((MvcHandler)HttpContext.Current.Handler).RequestContext).Action("LoadAcumaticaBillWithAndCustomerExceptionsGrid", "GridsAjax", new { Area = "" }),
				AutoWidth = true,
				Width = Unit.Percentage(100),
				Height = Unit.Percentage(100),
				ToolBarSettings = new ToolBarSettings
				{
					ShowRefreshButton = true,
					ShowSearchToolBar = true,
					ToolBarPosition = ToolBarPosition.Top
				},
				PagerSettings = new PagerSettings
				{
					PageSize = 100,
					PageSizeOptions = "[]"
				},
				AppearanceSettings = new AppearanceSettings
				{
					AlternateRowBackground = true,
					HighlightRowsOnHover = true,
					ShowFooter = false
				},
				RenderingMode = RenderingMode.Optimized
			};
		}

		public JQGrid Grid { get; set; }

		private List<JQGridColumn> LoadColumns()
		{
			var columns = new List<JQGridColumn>();

			columns.Add(new JQGridColumn
			{
				DataField = "Id",
				PrimaryKey = true,
				Editable = false,
				Visible = false
			});

			columns.Add(new JQGridColumn
			{
				DataField = "CompanyId",
				DataType = typeof(int),
				Editable = false,
				Visible = false
			});

			columns.Add(new JQGridColumn
			{
				DataField = "CompanyCode",
				HeaderText = "Company",
				DataType = typeof(string),
				Searchable = true,
				Sortable = true,
			});

			columns.Add(new JQGridColumn
			{
				DataField = "ManufacturerCode",
				HeaderText = "Manufacturer",
				DataType = typeof(string),
				Searchable = true,
				Sortable = true,
			});

			columns.Add(new JQGridColumn
			{
				DataField = "Jurisdiction",
				HeaderText = "Jurisdiction",
				DataType = typeof(string),
				Searchable = true,
				Sortable = true,
			});

			columns.Add(new JQGridColumn
			{
				DataField = "TransferJurisdiction",
				HeaderText = "Transfer Jurisdiction",
				DataType = typeof(string),
				Searchable = true,
				Sortable = true,
			});

			columns.Add(new JQGridColumn
			{
				DataField = "SelfBill",
				HeaderText = "Self Bill",
				DataType = typeof(bool),
				Searchable = true,
				Sortable = true,
			});

			columns.Add(new JQGridColumn
			{
				DataField = "ProjectCustomer",
				HeaderText = "Project Customer",
				DataType = typeof(string),
				Searchable = true,
				Sortable = true,
			});

			columns.Add(new JQGridColumn
			{
				DataField = "ExceptionTypeId",
				DataType = typeof(int),
				Editable = false,
				Visible = false
			});

			columns.Add(new JQGridColumn
			{
				DataField = "ExceptionTypeName",
				HeaderText = "Exception Type",
				DataType = typeof(string),
				Searchable = true,
				Sortable = true,
				Width = 100
			});

			columns.Add(new JQGridColumn
			{
				DataField = "Edit",
				HeaderText = " ",
				Formatter = new CustomFormatter { FormatFunction = "editLink" },
				Searchable = false,
				Sortable = false,
				Width = 50,
				TextAlign = TextAlign.Right,
				CssClass = "padright"
			}); ;

			return columns;
		}
	}

	public class AcumaticaBillWithAndCustomerExceptionsGridRecord
	{
		public int Id { get; set; }
		public int CompanyId { get; set; }
		public string CompanyCode { get; set; }
		public string ManufacturerCode { get; set; }
		public string Jurisdiction { get; set; }
		public string TransferJurisdiction { get; set; }
		public bool SelfBill { get; set; }
		public string ProjectCustomer { get; set; }
		public int ExceptionTypeId { get; set; }
		public string ExceptionTypeName { get; set; }
		public bool Active { get; set; }
		public string Edit { get; set; }
	}
}