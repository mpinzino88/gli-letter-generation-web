﻿using Submissions.Common;
using Submissions.Common.JQGrid;
using Submissions.Common.Linq.Dynamic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;


namespace Submissions.Web.Models.Grids.Lookups
{
	using Trirand.Web.Mvc;

	public class TargetOffSetGrid
	{
		public TargetOffSetGrid()
		{
			this.Grid = new JQGrid
			{
				ID = "TargetOffSetGrid",
				Columns = LoadColumns(),
				DataUrl = new UrlHelper(((MvcHandler)HttpContext.Current.Handler).RequestContext).Action("LoadTargetOffSetGrid", "GridsAjax", new { Area = "" }),
				AutoWidth = true,
				Width = Unit.Percentage(100),
				Height = Unit.Percentage(100),
				ToolBarSettings = new ToolBarSettings
				{
					ShowRefreshButton = true,
					ShowSearchToolBar = true,
					ToolBarPosition = ToolBarPosition.Top
				},
				PagerSettings = new PagerSettings
				{
					PageSize = 100,
					PageSizeOptions = "[]"
				},
				AppearanceSettings = new AppearanceSettings
				{
					AlternateRowBackground = true,
					HighlightRowsOnHover = true,
					ShowFooter = false
				},
				SortSettings = new SortSettings
				{
					InitialSortColumn = "JurisdictionName",
					InitialSortDirectionString = "asc"
				},
				RenderingMode = RenderingMode.Optimized
			};
		}

		public JQGrid Grid { get; set; }

		private List<JQGridColumn> LoadColumns()
		{
			var columns = new List<JQGridColumn>();

			columns.Add(new JQGridColumn
			{
				DataField = "Id",
				PrimaryKey = true,
				Editable = false,
				Visible = false
			});

			columns.Add(new JQGridColumn
			{
				DataField = "JurisdictionName",
				HeaderText = "Jurisdiction",
				DataType = typeof(string),
				Width = 300
			});

			columns.Add(new JQGridColumn
			{
				DataField = "JurisdictionId",
				HeaderText = "Jurisdiction Id",
				DataType = typeof(string),
				Width = 70
			});

			columns.Add(new JQGridColumn
			{
				DataField = "OffSetDays",
				HeaderText = "OffSet Days",
				DataType = typeof(int),
				Width = 75
			});

			columns.Add(new JQGridColumn
			{
				DataField = "Reason",
				HeaderText = "Reason",
				DataType = typeof(string),
				Width = 200
			});

			columns.Add(new JQGridColumn
			{
				DataField = "",
				HeaderText = "",
				Formatter = new CustomFormatter { FormatFunction = "editLink" },
				Searchable = false,
				Sortable = false,
				Width = 50,
				TextAlign = TextAlign.Right,
			});

			return columns;
		}

		public string GetColumnsDynamicLinqFormat()
		{
			var selectFields = this.Grid.Columns;
			var selectStatement = new StringBuilder();
			selectStatement.Append("new (");

			foreach (var col in selectFields)
			{
				if (!string.IsNullOrEmpty(col.DataField))
				{
					if (col.DataField == "Jurisdiction")
					{
						selectStatement.AppendFormat("{0} As {1},", "Jurisdiction.Name", col.DataField);
					}
					else
					{
						selectStatement.AppendFormat("{0} As {1},", col.DataField, col.DataField);
					}
				}
			}

			selectStatement.Length--;
			selectStatement.Append(")");

			return selectStatement.ToString();
		}

		public void Export(Export export, IQueryable<TargetOffSetGridRecord> query)
		{
			var selectStatement = this.GetColumnsDynamicLinqFormat();
			var results = query.Select(selectStatement);

			var fileName = string.Format(export.Title + " - {0:yyyy-MM-dd_hh-mm-ss-tt}", DateTime.Now);
			var jqGridHelper = new JQGridHelper();
			var jqGridState = new JQGridState();
			jqGridState.QueryString = HttpUtility.ParseQueryString(export.FiltersToolbar ?? "");

			if (export.ExportType == ExportType.Xls)
			{
				fileName += ".xls";
				this.Grid.ExportToExcel(results, fileName, jqGridState);
			}
			else if (export.ExportType == ExportType.Csv)
			{
				fileName += ".csv";
				this.Grid.ExportSettings.ExportDataRange = ExportDataRange.All;
				var exportData = this.Grid.GetExportData(results, jqGridState);
				jqGridHelper.ExportToCSV(exportData, fileName);
			}
			else if (export.ExportType == ExportType.Pdf)
			{
				fileName += ".pdf";
				this.Grid.ExportSettings.ExportDataRange = ExportDataRange.All;
				var exportData = this.Grid.GetExportData(results, jqGridState);
				jqGridHelper.ExportToPDF(exportData, fileName);
			}
		}
	}

	public class TargetOffSetGridRecord
	{
		public int Id { get; set; }
		public string JurisdictionId { get; set; }
		public string JurisdictionName { get; set; }
		public string Manufacturer { get; set; }
		public int? OffSetDays { get; set; }
		public string Reason { get; set; }
	}

}
