﻿using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;

namespace Submissions.Web.Models.Grids.Lookups
{
	using Trirand.Web.Mvc;

	public class TechnicalStandardsGrid
	{
		public TechnicalStandardsGrid()
		{
			this.Grid = new JQGrid
			{
				ID = "TechnicalStandardsGrid",
				Columns = InitializeAllColumns(),
				DataUrl = new UrlHelper(((MvcHandler)HttpContext.Current.Handler).RequestContext).Action("LoadTechnicalStandardsGrid", "GridsAjax", new { Area = "" }),
				AutoWidth = true,
				Width = Unit.Percentage(100),
				Height = Unit.Percentage(100),
				ToolBarSettings = new ToolBarSettings
				{
					ShowRefreshButton = true,
					ShowSearchToolBar = true,
					ToolBarPosition = ToolBarPosition.Top
				},
				PagerSettings = new PagerSettings
				{
					PageSize = 100,
					PageSizeOptions = "[]"
				},
				AppearanceSettings = new AppearanceSettings
				{
					AlternateRowBackground = true,
					HighlightRowsOnHover = true,
					ShowFooter = false
				},
				ClientSideEvents = new ClientSideEvents
				{
					BeforeAjaxRequest = "gridBeforeAjaxRequest"
				},
				SortSettings = new SortSettings
				{
					InitialSortColumn = "OriginalId",
					InitialSortDirectionString = "asc, desc"
				},
				RenderingMode = RenderingMode.Optimized
			};
		}

		public JQGrid Grid { get; set; }

		private List<JQGridColumn> InitializeAllColumns()
		{
			var columns = new List<JQGridColumn>();

			columns.Add(new JQGridColumn
			{
				DataField = "Id",
				PrimaryKey = true,
				Editable = false,
				Visible = false
			});
			columns.Add(new JQGridColumn
			{
				DataField = "OriginalId",
				PrimaryKey = false,
				Editable = false,
				Visible = false
			});


			columns.Add(new JQGridColumn
			{
				DataField = "Description",
				HeaderText = "Standard",
				DataType = typeof(string),
				SearchToolBarOperation = SearchOperation.Contains,
				Width = 150
			});
			columns.Add(new JQGridColumn
			{
				DataField = "Result",
				HeaderText = "Result",
				DataType = typeof(string),
				SearchToolBarOperation = SearchOperation.Contains,
				Width = 30
			});
			columns.Add(new JQGridColumn
			{
				DataField = "Jurisdictions",
				HeaderText = "Jurisdictions",
				DataType = typeof(string),
				SearchToolBarOperation = SearchOperation.Contains,
				Width = 50
			});
			columns.Add(new JQGridColumn
			{
				DataField = "Mandatory",
				HeaderText = "Mandatory?",
				DataType = typeof(bool),
				SearchType = SearchType.DropDown,
				SearchList = new List<SelectListItem>() { new SelectListItem { Text = "True", Value = "True" }, new SelectListItem { Text = "False", Value = "False" }, },
				SearchToolBarOperation = SearchOperation.IsEqualTo,
				Width = 20
			});

			columns.Add(new JQGridColumn
			{
				DataField = "Edit",
				HeaderText = " ",
				Formatter = new CustomFormatter { FormatFunction = "editLink" },
				Searchable = false,
				Sortable = false,
				Width = 50,
				TextAlign = TextAlign.Right,
				CssClass = "padright"
			});

			columns.Add(new JQGridColumn
			{
				DataField = "Active",
				DataType = typeof(bool),
				Visible = false
			});

			return columns;
		}
	}
	public class TechnicalStandardsGridRecord
	{
		public int Id { get; set; }
		public int OriginalId { get; set; }
		public string Description { get; set; }
		public string Result { get; set; }
		public bool Mandatory { get; set; }
		public string Jurisdictions { get; set; }
		public string Edit { get; set; }
		public bool Active { get; set; }
	}
}