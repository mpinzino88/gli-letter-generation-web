﻿using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using Trirand.Web.Mvc;

namespace Submissions.Web.Models.Grids.Lookups
{
	public class BillingSheetGrid
	{
		public BillingSheetGrid()
		{
			this.Grid = new JQGrid
			{
				ID = "BillingSheetGrid",
				Columns = LoadColumns(),
				DataUrl = new UrlHelper(((MvcHandler)HttpContext.Current.Handler).RequestContext).Action("LoadBillingSheetGrid", "GridsAjax", new { Area = "" }),
				ToolBarSettings = new ToolBarSettings
				{
					ShowRefreshButton = true,
					ToolBarPosition = ToolBarPosition.Top,
					ShowSearchToolBar = true
				},
			};
		}

		public JQGrid Grid { get; set; }
		private List<JQGridColumn> LoadColumns()
		{
			var columns = new List<JQGridColumn>();

			columns.Add(new JQGridColumn
			{
				DataField = "Id",
				Editable = false,
				Visible = false,
				DataType = typeof(int),
			});

			columns.Add(new JQGridColumn
			{
				DataField = "Name",
				DataType = typeof(string),
				Sortable = true,
				Width = 75,
				Searchable = true
			});

			columns.Add(new JQGridColumn
			{
				DataField = "Labs",
				DataType = typeof(string),
				Sortable = true,
				Width = 50,
				Searchable = true
			});

			columns.Add(new JQGridColumn
			{
				DataField = "GetsGatChargeIfApplicable",
				DataType = typeof(bool),
				Sortable = true,
				Width = 50,
				Searchable = true
			});

			columns.Add(new JQGridColumn
			{
				DataField = "AppFeeName",
				DataType = typeof(string),
				Sortable = true,
				Width = 50,
				Searchable = true
			});

			columns.Add(new JQGridColumn
			{
				DataField = "BilledItem1AppFeeName",
				DataType = typeof(string),
				Sortable = true,
				Width = 50,
				Searchable = true
			});

			columns.Add(new JQGridColumn
			{
				DataField = "BilledItem2AppFeeName",
				DataType = typeof(string),
				Sortable = true,
				Width = 50,
				Searchable = true
			});

			columns.Add(new JQGridColumn
			{
				DataField = "BilledItem3AppFeeName",
				DataType = typeof(string),
				Sortable = true,
				Width = 50,
				Searchable = true
			});

			columns.Add(new JQGridColumn
			{
				DataField = "Active",
				DataType = typeof(bool),
				Sortable = true,
				Width = 50,
				Searchable = true
			});

			return columns;
		}
	}
	public class BillingSheetGridRecord
	{
		public int Id { get; set; }
		public string Name { get; set; }
		public string Description { get; set; }
		public string Labs { get; set; }
		public bool GetsGatChargeIfApplicable { get; set; }
		//public bool RequiredSpecializedType { get; set; }
		public string AppFeeName { get; set; }
		public string BilledItem1AppFeeName { get; set; }
		public string BilledItem2AppFeeName { get; set; }
		public string BilledItem3AppFeeName { get; set; }
		public bool Active { get; set; }
	}
}