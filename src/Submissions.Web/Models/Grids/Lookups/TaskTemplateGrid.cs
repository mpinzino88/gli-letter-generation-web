﻿using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;

namespace Submissions.Web.Models.Grids.Lookups
{
	using Trirand.Web.Mvc;

	public class TaskTemplateGrid
	{
		public TaskTemplateGrid()
		{
			this.Grid = new JQGrid
			{
				ID = "TaskTemplateGrid",
				Columns = LoadColumns(),
				DataUrl = new UrlHelper(((MvcHandler)HttpContext.Current.Handler).RequestContext).Action("LoadTaskTemplateGrid", "GridsAjax", new { Area = "" }),
				AutoWidth = true,
				Width = Unit.Percentage(100),
				Height = Unit.Percentage(100),
				ToolBarSettings = new ToolBarSettings
				{
					ShowRefreshButton = true,
					ShowSearchToolBar = true,
					ToolBarPosition = ToolBarPosition.Top
				},
				PagerSettings = new PagerSettings
				{
					PageSize = 100,
					PageSizeOptions = "[]"
				},
				AppearanceSettings = new AppearanceSettings
				{
					AlternateRowBackground = true,
					HighlightRowsOnHover = true,
					ShowFooter = false
				},
				ClientSideEvents = new ClientSideEvents
				{
					BeforeAjaxRequest = "gridBeforeAjaxRequest"
				},
				SortSettings = new SortSettings
				{
					InitialSortColumn = "UserName"
				},
				RenderingMode = RenderingMode.Optimized
			};
		}

		public JQGrid Grid { get; set; }

		private List<JQGridColumn> LoadColumns()
		{
			var columns = new List<JQGridColumn>();

			columns.Add(new JQGridColumn
			{
				DataField = "Id",
				PrimaryKey = true,
				Editable = false,
				Visible = false
			});
			columns.Add(new JQGridColumn
			{
				DataField = "UserId",
				Visible = false
			});

			columns.Add(new JQGridColumn
			{
				DataField = "UserName",
				HeaderText = "Owner",
				DataType = typeof(string),
				SearchToolBarOperation = SearchOperation.Contains,
				Width = 50
			});

			columns.Add(new JQGridColumn
			{
				DataField = "Template",
				HeaderText = "Template",
				DataType = typeof(string),
				SearchToolBarOperation = SearchOperation.Contains,
				Width = 60
			});

			columns.Add(new JQGridColumn
			{
				DataField = "Description",
				HeaderText = "Description",
				DataType = typeof(string),
				SearchToolBarOperation = SearchOperation.Contains,
				Width = 80
			});

			columns.Add(new JQGridColumn
			{
				DataField = "Edit",
				HeaderText = " ",
				Formatter = new CustomFormatter { FormatFunction = "editLink" },
				Searchable = false,
				Sortable = false,
				Width = 50,
				TextAlign = TextAlign.Right,
				CssClass = "padright"
			});

			return columns;
		}
	}

	public class TaskTemplateGridRecord
	{
		public int Id { get; set; }
		public string UserName { get; set; }
		public int? UserId { get; set; }
		public string Template { get; set; }
		public string Description { get; set; }
		public string Edit { get; set; }
	}
}