﻿using Submissions.Common;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;

namespace Submissions.Web.Models.Grids.Lookups
{
	using Trirand.Web.Mvc;
	public class ManufacturerJurisdictionCodeMappingsGrid
	{
		public ManufacturerJurisdictionCodeMappingsGrid()
		{
			Grid = new JQGrid
			{
				Columns = new List<JQGridColumn>
				{
					new JQGridColumn
					{
						DataField = "Id",
						PrimaryKey = true,
						Editable = false,
						Visible = false
					},
					new JQGridColumn
					{
						DataField = "ManufacturerShortID",
						HeaderText = "Manuf Code",
						DataType = typeof(string),
						Width = 25
					},
					new JQGridColumn
					{
						DataField = "JurisdictionId",
						HeaderText = "GLI Juris Id",
						DataType = typeof(string),
						Width = 25
					},
					new JQGridColumn
					{
						DataField = "JurisdictionName",
						HeaderText = "GLI Jurisdiction",
						DataType = typeof(string),
						Width = 50
					},
					new JQGridColumn
					{
						DataField = "MappedJurisdictionCode",
						HeaderText = "Manuf Letter Code ID",
						DataType = typeof(string),
						Width = 35
					},
					new JQGridColumn
					{
						DataField = "MappedJurisdictionId",
						HeaderText = "Manuf Juris ID",
						DataType = typeof(string),
						Width = 30
					},
					new JQGridColumn
					{
						DataField = "MappedJurisdictionName",
						HeaderText = "Manufacturer Jurisdiction Name",
						DataType = typeof(string),
						Width = 60
					},
					new JQGridColumn
					{
						DataField = "Billing",
						HeaderText = "Billing",
						Formatter = new CustomFormatter { FormatFunction = "formatYesNo" },
						DataType = typeof(bool),
						Searchable = true,
						SearchType = SearchType.DropDown,
						SearchToolBarOperation = SearchOperation.IsEqualTo,
						SearchList = (List<SelectListItem>)LookupsStandard.YesNoBool,
						Width = 25
					},
					new JQGridColumn
					{
						DataField = "Flatfile",
						HeaderText = "Flatfile",
						Formatter = new CustomFormatter { FormatFunction = "formatYesNo" },
						DataType = typeof(bool),
						Searchable = true,
						SearchType = SearchType.DropDown,
						SearchToolBarOperation = SearchOperation.IsEqualTo,
						SearchList = (List<SelectListItem>)LookupsStandard.YesNoBool,
						Width = 25
					},
					new JQGridColumn
					{
						DataField = "Edit",
						HeaderText = " ",
						Formatter = new CustomFormatter { FormatFunction = "editLink" },
						Searchable = false,
						Sortable = false,
						Width = 30,
						TextAlign = TextAlign.Right,
						CssClass = "padright"
					}
				}
			};
			Grid.ID = "ManufacturerJurisdictionCodesGrid";
			Grid.DataUrl = new UrlHelper(((MvcHandler)HttpContext.Current.Handler).RequestContext).Action("LoadManufacturerJurisdictionCodesIndexGrid", "GridsAjax", new { Area = "" });
			Grid.AutoWidth = true;
			Grid.Width = Unit.Percentage(100);
			Grid.Height = Unit.Percentage(100);
			Grid.ToolBarSettings = new ToolBarSettings
			{
				ShowRefreshButton = true,
				ToolBarPosition = ToolBarPosition.Top,
				ShowSearchToolBar = true
			};
			Grid.PagerSettings = new Trirand.Web.Mvc.PagerSettings
			{
				PageSize = 100,
				PageSizeOptions = "[]"
			};
			Grid.AppearanceSettings = new AppearanceSettings
			{
				AlternateRowBackground = true,
				HighlightRowsOnHover = true,
				ShowFooter = false
			};
		}

		public JQGrid Grid { get; set; }
	}

	public class ManufacturerJurisdictionCodeMappingRecord
	{
		public string Id { get; set; }
		public string ManufacturerShortID { get; set; }
		public string JurisdictionId { get; set; }
		public string JurisdictionName { get; set; }
		public string MappedJurisdictionId { get; set; }
		public string MappedJurisdictionCode { get; set; }
		public string MappedJurisdictionName { get; set; }
		public bool Billing { get; set; }
		public bool Flatfile { get; set; }
		public string Edit { get; set; }
	}
}