﻿using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;

namespace Submissions.Web.Models.Grids.Lookups
{
	using Trirand.Web.Mvc;

	public class JurisdictionGroupCategoriesGrid
	{
		public JurisdictionGroupCategoriesGrid()
		{
			this.Grid = new JQGrid
			{
				ID = "JurisdictionGroupCategoriesGrid",
				Columns = LoadColumns(),
				DataUrl = new UrlHelper(((MvcHandler)HttpContext.Current.Handler).RequestContext).Action("LoadJurisdictionGroupCategoriesGrid", "GridsAjax", new { Area = "" }),
				AutoWidth = true,
				ShrinkToFit = true,
				Width = Unit.Percentage(100),
				Height = Unit.Percentage(100),
				ToolBarSettings = new ToolBarSettings
				{
					ShowRefreshButton = false,
					ToolBarPosition = ToolBarPosition.Top,
					ShowSearchButton = false,
					ShowSearchToolBar = false
				},
				PagerSettings = new PagerSettings
				{
					PageSize = 25,
					PageSizeOptions = "[]"
				},
				AppearanceSettings = new AppearanceSettings
				{
					AlternateRowBackground = true,
					HighlightRowsOnHover = true,
					ShowFooter = false
				},
				RenderingMode = RenderingMode.Optimized
			};
		}

		public JQGrid Grid { get; set; }

		private List<JQGridColumn> LoadColumns()
		{
			var columns = new List<JQGridColumn>();

			columns.Add(new JQGridColumn
			{
				DataField = "Id",
				Editable = false,
				Visible = false,
				DataType = typeof(int),
			});

			columns.Add(new JQGridColumn
			{
				DataField = "Code",
				HeaderText = "Category",
				Editable = false,
				Visible = true,
				DataType = typeof(string),
				Searchable = false,
				Sortable = true,
			});

			columns.Add(new JQGridColumn
			{
				DataField = "Description",
				HeaderText = "Description",
				Editable = false,
				Visible = true,
				DataType = typeof(string),
				Searchable = false,
				Sortable = false,
			});

			columns.Add(new JQGridColumn
			{
				DataField = "Edit",
				Formatter = new CustomFormatter { FormatFunction = "EditLink" },
				Searchable = false,
				Sortable = false,
				Width = 50,
				CssClass = "padright"
			});

			columns.Add(new JQGridColumn
			{
				DataField = "Delete",
				Formatter = new CustomFormatter { FormatFunction = "DeleteLink" },
				Searchable = false,
				Sortable = false,
				Width = 50,
				CssClass = "padright"
			});

			return columns;
		}
	}

	public class JurisdictionGroupCategoriesGridRecord
	{
		public int Id { get; set; }
		public string Code { get; set; }
		public string Description { get; set; }
		public string Edit { get; set; }
		public string Delete { get; set; }
	}
}