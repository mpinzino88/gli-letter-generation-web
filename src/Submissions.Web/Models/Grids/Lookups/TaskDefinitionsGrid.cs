﻿using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;

namespace Submissions.Web.Models.Grids.Lookups
{
	using Trirand.Web.Mvc;

	public class TaskDefinitionsGrid
	{
		public TaskDefinitionsGrid()
		{
			this.Grid = new JQGrid
			{
				ID = "TaskDefinitionsGrid",
				Columns = LoadColumns(),
				DataUrl = new UrlHelper(((MvcHandler)HttpContext.Current.Handler).RequestContext).Action("LoadTaskDefinitionsGrid", "GridsAjax", new { Area = "" }),
				AutoWidth = true,
				Width = Unit.Percentage(100),
				Height = Unit.Percentage(100),
				ToolBarSettings = new ToolBarSettings
				{
					ShowRefreshButton = true,
					ShowSearchToolBar = true,
					ToolBarPosition = ToolBarPosition.Top
				},
				PagerSettings = new PagerSettings
				{
					PageSize = 100,
					PageSizeOptions = "[]"
				},
				AppearanceSettings = new AppearanceSettings
				{
					AlternateRowBackground = true,
					HighlightRowsOnHover = true,
					ShowFooter = false
				},
				SortSettings = new SortSettings
				{
					InitialSortColumn = "Active, Code",
					InitialSortDirectionString = "desc, asc"
				},
				RenderingMode = RenderingMode.Optimized
			};
		}

		public JQGrid Grid { get; set; }

		private List<JQGridColumn> LoadColumns()
		{
			var columns = new List<JQGridColumn>();

			columns.Add(new JQGridColumn
			{
				DataField = "Id",
				PrimaryKey = true,
				Editable = false,
				Visible = false
			});

			columns.Add(new JQGridColumn
			{
				DataField = "Active",
				DataType = typeof(bool),
				Visible = false
			});

			columns.Add(new JQGridColumn
			{
				DataField = "Code",
				HeaderText = "Code",
				DataType = typeof(string),
				Width = 300
			});

			columns.Add(new JQGridColumn
			{
				DataField = "DepartmentDescription",
				HeaderText = "Department",
				DataType = typeof(string),
				Width = 150
			});

			columns.Add(new JQGridColumn
			{
				DataField = "DynamicsId",
				HeaderText = "Task Id",
				DataType = typeof(string),
				Width = 100
			});

			columns.Add(new JQGridColumn
			{
				DataField = "Edit",
				HeaderText = " ",
				Formatter = new CustomFormatter { FormatFunction = "editLink" },
				Searchable = false,
				Sortable = false,
				Width = 50,
				TextAlign = TextAlign.Right,
				CssClass = "padright"
			});

			return columns;
		}
	}

	public class TaskDefinitionsGridRecord
	{
		public int Id { get; set; }
		public bool Active { get; set; }
		public string Code { get; set; }
		public string DepartmentDescription { get; set; }
		public string DynamicsId { get; set; }
		public string Edit { get; set; }
	}
}