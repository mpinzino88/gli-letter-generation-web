﻿using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;

namespace Submissions.Web.Models.Grids.Lookups
{
	using Trirand.Web.Mvc;

	public class JurisdictionsGrid
	{
		public JurisdictionsGrid()
		{
			Grid = new JQGrid
			{
				Columns = new List<JQGridColumn>
				{
					new JQGridColumn
					{
						DataField = "Active",
						DataType = typeof(bool),
						Visible = false
					},
					new JQGridColumn
					{
						DataField = "Id",
						HeaderText = "ID",
						PrimaryKey = true,
						DataType = typeof(string),
						Editable = false,
						Visible = true,
						Width = 50
					},
					new JQGridColumn
					{
						DataField = "Name",
						HeaderText = "Name",
						DataType = typeof(string)
					},
					new JQGridColumn
					{
						DataField = "AltName",
						HeaderText = "Alternate Name",
						DataType = typeof(string)
					},
					new JQGridColumn
					{
						DataField = "PendingApproval",
						HeaderText = "Pending Approval",
						Formatter = new CustomFormatter { FormatFunction = "PendingApproval" },
						Sortable = true,
						Searchable = false,
						Width = 70
					},
					new JQGridColumn
					{
						DataField = "Edit",
						HeaderText = " ",
						Formatter = new CustomFormatter { FormatFunction = "editLink" },
						Searchable = false,
						Sortable = false,
						Width = 50,
						TextAlign = TextAlign.Right,
						CssClass = "padright"
					}
				}
			};
			Grid.ID = "JurisdictionsGrid";
			Grid.DataUrl = new UrlHelper(((MvcHandler)HttpContext.Current.Handler).RequestContext).Action("LoadJurisdictionsIndexGrid", "GridsAjax", new { Area = "" });
			Grid.AutoWidth = true;
			Grid.Width = Unit.Percentage(100);
			Grid.Height = Unit.Percentage(100);
			Grid.ToolBarSettings = new ToolBarSettings
			{
				ShowRefreshButton = true,
				ToolBarPosition = ToolBarPosition.Top,
				ShowSearchToolBar = true
			};
			Grid.PagerSettings = new Trirand.Web.Mvc.PagerSettings
			{
				PageSize = 100,
				PageSizeOptions = "[]"
			};
			Grid.AppearanceSettings = new AppearanceSettings
			{
				AlternateRowBackground = true,
				HighlightRowsOnHover = true,
				ShowFooter = false
			};
		}

		public JQGrid Grid { get; set; }
	}

	public class JurisdictionsGridRecord
	{
		public string Id { get; set; }
		public string Name { get; set; }
		public string AltName { get; set; }
		public bool PendingApproval { get; set; }
		public string Edit { get; set; }
		public bool Active { get; set; }
	}
}