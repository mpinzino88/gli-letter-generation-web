﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;
using Trirand.Web.Mvc;

namespace Submissions.Web.Models.Grids.Lookups
{
	public class BusinessOwnersGrid
	{
		public BusinessOwnersGrid()
		{
			Grid = new JQGrid
			{
				ID = "BusinessOwnersGrid",
				Columns = LoadColumns(),
				DataUrl = new UrlHelper(((MvcHandler)HttpContext.Current.Handler).RequestContext).Action("LoadBusinessOwnersGrid", "GridsAjax", new { Area = "" }),
				AutoWidth = true,
				Width = Unit.Percentage(100),
				Height = Unit.Percentage(100),
				ToolBarSettings = new ToolBarSettings
				{
					ShowRefreshButton = true,
					ShowSearchToolBar = true,
					ToolBarPosition = ToolBarPosition.Top
				},
				AppearanceSettings = new AppearanceSettings
				{
					AlternateRowBackground = true,
					HighlightRowsOnHover = true,
					ShowFooter = false
				},
				SortSettings = new SortSettings
				{
					InitialSortColumn = "Active, Code",
					InitialSortDirectionString = "desc, asc"
				},
				RenderingMode = RenderingMode.Optimized
			};
		}

		public JQGrid Grid { get; set; }

		private List<JQGridColumn> LoadColumns()
		{
			var columns = new List<JQGridColumn>();

			columns.Add(new JQGridColumn
			{
				DataField = "Id",
				PrimaryKey = true,
				Editable = false,
				Visible = false
			});


			columns.Add(new JQGridColumn
			{
				DataField = "Code",
				HeaderText = "Abbreviation",
				DataType = typeof(string),
				Width = 50
			});

			columns.Add(new JQGridColumn
			{
				DataField = "Description",
				HeaderText = "Business Owner",
				DataType = typeof(string)
			});

			columns.Add(new JQGridColumn
			{
				DataField = "Active",
				DataType = typeof(bool),
				Visible = true
			});

			columns.Add(new JQGridColumn
			{
				DataField = "Edit",
				HeaderText = " ",
				Formatter = new CustomFormatter { FormatFunction = "editLink" },
				Searchable = false,
				Sortable = false,
				Width = 50,
				TextAlign = Trirand.Web.Mvc.TextAlign.Right,
				CssClass = "padright"
			});

			return columns;
		}
	}

	public class BusinessOwnersGridRecord
	{
		public int Id { get; set; }
		public bool Active { get; set; }
		public string Code { get; set; }
		public string Description { get; set; }
		public int CreatedBy { get; set; }
		public DateTime? CreatedOn { get; set; }
		public int? EditedBy { get; set; }
		public DateTime? EditedOn { get; set; }
		public string Edit { get; set; }
	}
}