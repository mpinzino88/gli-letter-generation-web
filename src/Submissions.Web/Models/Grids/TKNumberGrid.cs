﻿using Submissions.Common.JQGrid;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;


namespace Submissions.Web.Models.Grids
{
	using Trirand.Web.Mvc;
	public class TKNumberGrid
	{
		private const string gridId = "TKNumberGrid";
		public TKNumberGrid()
		{
			this.AllColumns = GetAllColumns();
			this.Grid = new JQGrid
			{
				ID = gridId,
				Columns = this.AllColumns.Cast<JQGridColumn>().ToList(),
				DataUrl = new UrlHelper(((MvcHandler)HttpContext.Current.Handler).RequestContext).Action("LoadTKNumberGrid", "GridsAjax", new { Area = "" }),
				AutoWidth = true,
				ShrinkToFit = false,
				Width = Unit.Percentage(100),
				Height = Unit.Percentage(100),
				ToolBarSettings = new ToolBarSettings
				{
					ShowRefreshButton = true,
					ToolBarPosition = ToolBarPosition.Top,
					ShowSearchToolBar = true
				},
				PagerSettings = new PagerSettings
				{
					PageSize = 100,
					PageSizeOptions = "[]"
				},
				AppearanceSettings = new AppearanceSettings
				{
					AlternateRowBackground = true,
					HighlightRowsOnHover = true,
					ShowFooter = false
				},
				SortSettings = new SortSettings
				{
					InitialSortColumn = "ProjectName",
					InitialSortDirection = SortDirection.Asc
				},
				/*ClientSideEvents = new ClientSideEvents
				{
					BeforeAjaxRequest = "gridBeforeAjaxRequest"
				},*/
				RenderingMode = RenderingMode.Optimized,
				MultiSelect = true
			};

		}

		public JQGrid Grid { get; set; }
		public List<JQGridColumnDynamic> AllColumns { private set; get; }
		public List<JQGridColumnDynamic> GetAllColumns()
		{
			var Columns = new List<JQGridColumnDynamic>
				{
					new JQGridColumnDynamic
					{
						DataField = "Id",
						HeaderText = "Id",
						PrimaryKey = true,
						Editable = false,
						Visible = false
					},
					new JQGridColumnDynamic
					{
						DataField = "ProjectId",
						HeaderText = "ProjectID",
						DataType = typeof(int),
						Visible = false
					},
					new JQGridColumnDynamic
					{
						DataField = "ProjectName",
						HeaderText = "File Number",
						//Formatter = new CustomFormatter{FormatFunction = "detailLink"},
						DataType = typeof(string),
						Searchable = true,
						SearchToolBarOperation = SearchOperation.BeginsWith,
						Width = 140
					},
					new JQGridColumnDynamic
					{
						DataField = "IdNumber",
						HeaderText = "ID Number",
						DataType = typeof(string),
						Searchable = true,
						SearchToolBarOperation = SearchOperation.BeginsWith,
						Width = 80
					},
					new JQGridColumnDynamic
					{
						DataField = "GameName",
						HeaderText = "Game Name",
						DataType = typeof(string),
						Searchable = true,
						SearchToolBarOperation = SearchOperation.BeginsWith,
						Width = 80
					},

					new JQGridColumnDynamic
					{
						DataField = "Version",
						HeaderText = "Version",
						DataType = typeof(string),
						//Formatter = new CustomFormatter{FormatFunction = "typeLink"},
						Searchable = true,
						SearchToolBarOperation = SearchOperation.BeginsWith,
						Width = 80
					},
					new JQGridColumnDynamic
					{
						DataField = "Jurisdiction",
						HeaderText = "Jurisdiction",
						DataType = typeof(string),
						Searchable = true,
						SearchToolBarOperation = SearchOperation.BeginsWith,
						Width = 150
					},
					
				};
			return Columns;
		}

	}

	public class TKNumberGridRecord
	{
		public int Id { get; set; }
		public int ProjectId { get; set; }
		public string ProjectName { get; set; }
		public string IdNumber { get; set; }
		public string GameName { get; set; }
		public string Version { get; set; }
		public string Jurisdiction { get; set; }
	}
}