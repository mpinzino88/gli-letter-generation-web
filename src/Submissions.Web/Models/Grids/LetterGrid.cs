﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;
using Trirand.Web.Mvc;

namespace Submissions.Web.Models.Grids
{
	public class LetterGrid
	{
		public LetterGrid()
		{
			Grid = new JQGrid
			{
				Columns = new List<JQGridColumn>
				{
					new JQGridColumn
					{
						DataField = "VersionId",
						HeaderText = "Version Id",
						PrimaryKey = true,
						Editable = false,
						Visible = false
					},
					new JQGridColumn
					{
						DataField = "DocumentId",
						HeaderText = "Document Id",
						Editable = false,
						Visible = false
					},
					new JQGridColumn
					{
						DataField = "VersionId",
						HeaderText = "PDF",
						Formatter = new CustomFormatter { FormatFunction = "pdfLink" },
						Width = 75
					},
					new JQGridColumn
					{
						DataField = "DocumentType",
						HeaderText = "Letter Type",
						DataType = typeof(string),
						Width = 225
					},
					new JQGridColumn
					{
						DataField = "DocumentVersion",
						HeaderText = "Version",
						DataType = typeof(string),
						Width = 225
					},
					new JQGridColumn
					{
						DataField = "DocumentDate",
						HeaderText = "Document Date",
						DataType = typeof(DateTime),
						Width = 250
					},
					new JQGridColumn
					{
						DataField = "Status",
						HeaderText = "Status",
						DataType = typeof(string),
						Width = 50
					},
					new JQGridColumn
					{
						DataField = "Edit",
						Formatter = new CustomFormatter{FormatFunction = "editLink"},
						Searchable = false,
						Sortable = false,
						Width = 50,
						CssClass = "padright"
					}
				}
			};
			Grid.ID = "LetterGrid";
			Grid.DataUrl = new UrlHelper(((MvcHandler)HttpContext.Current.Handler).RequestContext).Action("LoadLetterGrid", "GridsAjax", new { Area = "" });
			Grid.AutoWidth = true;
			Grid.Width = Unit.Percentage(80);
			Grid.Height = Unit.Percentage(100);
			Grid.ToolBarSettings = new ToolBarSettings
			{
				ShowRefreshButton = true,
				ToolBarPosition = ToolBarPosition.Top,
				ShowSearchButton = true,
				ShowSearchToolBar = true,
			};
			Grid.PagerSettings = new Trirand.Web.Mvc.PagerSettings
			{
				PageSize = 100,
				PageSizeOptions = "[]"
			};
			Grid.AppearanceSettings = new AppearanceSettings
			{
				AlternateRowBackground = true,
				HighlightRowsOnHover = true,
				ShowFooter = false
			};
			Grid.ClientSideEvents.BeforeAjaxRequest = "gridBeforeAjaxRequest";
		}
		public JQGrid Grid { get; set; }
	}

	public class LetterGridRecord
	{
		public int VersionId { get; set; }
		public int DocumentId { get; set; }
		public string DocumentType { get; set; }
		public float DocumentVersion { get; set; }
		public DateTime DocumentDate { get; set; }
		public string Status { get; set; }
		public string Edit { get; set; }
	}
}