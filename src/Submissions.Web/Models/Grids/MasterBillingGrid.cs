﻿using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;

namespace Submissions.Web.Models.Grids
{
	using Trirand.Web.Mvc;

	public class MasterBillingGrid
	{
		private const string parentGridId = "MasterBilling";
		private const string childGridId = "MasterBillingDetails";
		public JQGrid Grid { get; set; }
		public JQGrid GridDetails { get; set; }

		public MasterBillingGrid()
		{
			#region Parent Grid
			this.Grid = new JQGrid
			{
				ID = parentGridId,
				Columns = LoadColumns(),
				DataUrl = new UrlHelper(((MvcHandler)HttpContext.Current.Handler).RequestContext).Action("LoadMasterBillingGrid", "GridsAjax", new { Area = "" }),
				AutoWidth = true,
				Width = Unit.Percentage(100),
				Height = Unit.Percentage(100),
				ToolBarSettings = new ToolBarSettings
				{
					ShowRefreshButton = true,
					ToolBarPosition = ToolBarPosition.Top,
					ShowSearchButton = false,
					ShowSearchToolBar = false,
				},
				PagerSettings = new PagerSettings
				{
					PageSize = 25,
					PageSizeOptions = "[]"
				},
				AppearanceSettings = new AppearanceSettings
				{
					AlternateRowBackground = true,
					HighlightRowsOnHover = true,
					ShowFooter = false,
				},
				ClientSideEvents = new ClientSideEvents
				{
					SubGridRowExpanded = "showDetailsSubGrid"
				},
				HierarchySettings = new HierarchySettings
				{
					HierarchyMode = HierarchyMode.Parent,
					PlusIcon = "ui-icon-triangle-1-e",
					MinusIcon = "ui-icon-triangle-1-s",
					OpenIcon = "ui-icon-arrow-1-e",
					ReloadOnExpand = false
				}
			};
			#endregion

			#region Child Grid
			this.GridDetails = new JQGrid
			{
				ID = childGridId,
				Columns = LoadColumnsDetail(),
				DataUrl = new UrlHelper(((MvcHandler)HttpContext.Current.Handler).RequestContext).Action("LoadMasterBillingGridDetails", "GridsAjax", new { Area = "" }),
				AutoWidth = true,
				ShrinkToFit = false,
				Width = Unit.Percentage(100),
				Height = Unit.Percentage(100),
				PagerSettings = new PagerSettings
				{
					ScrollBarPaging = true,
					PageSize = 50
				},
				AppearanceSettings = new AppearanceSettings
				{
					AlternateRowBackground = true,
					HighlightRowsOnHover = true,
					ShowFooter = false,
					ShrinkToFit = true
				},
				HierarchySettings = new HierarchySettings
				{
					HierarchyMode = HierarchyMode.Child,

				}
			};
			#endregion
		}

		#region Load Parent Columns
		private List<JQGridColumn> LoadColumns()
		{
			var columns = new List<JQGridColumn>();

			columns.Add(new JQGridColumn
			{
				DataField = "Id",
				PrimaryKey = true,
				Visible = false,
				Editable = false,
				Searchable = false,
				Sortable = false,
			});

			columns.Add(new JQGridColumn
			{
				DataField = "SubmissionFileNumber",
				HeaderText = "File Number",
				DataType = typeof(string),
				Visible = true,
				Editable = false,
				Searchable = false,
				Sortable = true
			});

			columns.Add(new JQGridColumn
			{
				DataField = "SubmissionIdNumber",
				HeaderText = "Id Number",
				DataType = typeof(string),
				Visible = true,
				Editable = false,
				Searchable = false,
				Sortable = true
			});

			columns.Add(new JQGridColumn
			{
				DataField = "SubmissionVersion",
				HeaderText = "Version",
				DataType = typeof(string),
				Visible = true,
				Editable = false,
				Searchable = false,
				Sortable = true
			});

			columns.Add(new JQGridColumn
			{
				DataField = "SubmissionGameName",
				HeaderText = "Game Name",
				DataType = typeof(string),
				Visible = true,
				Editable = false,
				Searchable = false,
				Sortable = true
			});

			columns.Add(new JQGridColumn
			{
				DataField = "SubmissionDateCode",
				HeaderText = "Date Code",
				DataType = typeof(string),
				Visible = true,
				Editable = false,
				Searchable = false,
				Sortable = false
			});

			columns.Add(new JQGridColumn
			{
				DataField = "Id",
				HeaderText = "Edit",
				Formatter = new CustomFormatter { FormatFunction = "masterBillingEdit" },
				Width = 75,
				Visible = true,
				Searchable = false,
				Sortable = false,
			});
			return columns;
		}
		#endregion

		#region Load Child Columns
		private List<JQGridColumn> LoadColumnsDetail()
		{
			var columns = new List<JQGridColumn>();

			columns.Add(new JQGridColumn
			{
				DataField = "Id",
				PrimaryKey = true,
				Visible = false,
				Editable = false,
				Searchable = false,
				Sortable = false
			});

			columns.Add(new JQGridColumn
			{
				DataField = "SubmissionId",
				Visible = false,
				Editable = false,
				Searchable = false,
				Sortable = false
			});

			columns.Add(new JQGridColumn
			{
				DataField = "JurisdictionalDataJurisdictionName",
				HeaderText = "Jurisdiction",
				DataType = typeof(string),
				Visible = true,
				Editable = false,
				Searchable = false,
				Sortable = false
			});

			columns.Add(new JQGridColumn
			{
				DataField = "JurisdictionalDataJurisdictionId",
				HeaderText = "Jurisdiction Number",
				DataType = typeof(string),
				Visible = true,
				Editable = false,
				Searchable = false,
				Sortable = false
			});

			columns.Add(new JQGridColumn
			{
				DataField = "JurisdictionalDataMasterBillingProjectId",
				HeaderText = "Master Billing Project Id",
				DataType = typeof(string),
				Visible = true,
				Editable = false,
				Searchable = false,
				Sortable = false
			});

			return columns;
		}
		#endregion
	}

	public class MasterBillingGridRecord
	{
		public int Id { get; set; }
		public string SubmissionFileNumber { get; set; }
		public string SubmissionIdNumber { get; set; }
		public string SubmissionVersion { get; set; }
		public string SubmissionGameName { get; set; }
		public string SubmissionDateCode { get; set; }
	}

	public class MasterBillingDetailsGridRecord
	{
		public int Id { get; set; }
		public string JurisdictionalDataJurisdictionName { get; set; }
		public string JurisdictionalDataJurisdictionId { get; set; }
		public string JurisdictionalDataMasterBillingProjectId { get; set; }
		public int SubmissionId { get; set; }
	}
}