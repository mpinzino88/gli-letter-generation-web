﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;
using Trirand.Web.Mvc;
using PagerSettings = Trirand.Web.Mvc.PagerSettings;
using TextAlign = Trirand.Web.Mvc.TextAlign;

namespace Submissions.Web.Models.Grids.Documents
{
	public class DraftLettersGrid
	{
		public JQGrid Grid { get; set; }

		public DraftLettersGrid()
		{
			this.Grid = new JQGrid
			{
				ID = "DraftLettersGrid",
				Columns = InitializeAllColumns(),
				DataUrl = new UrlHelper(((MvcHandler)HttpContext.Current.Handler).RequestContext).Action("LoadDraftLettersGrid", "GridsAjax", new { Area = "" }),
				AutoWidth = true,
				Width = Unit.Percentage(100),
				Height = Unit.Percentage(100),
				ToolBarSettings = new ToolBarSettings
				{
					ShowRefreshButton = true,
					ShowSearchToolBar = true,
					ToolBarPosition = ToolBarPosition.Top
				},
				PagerSettings = new PagerSettings
				{
					PageSize = 100,
					PageSizeOptions = "[]"
				},
				AppearanceSettings = new AppearanceSettings
				{
					AlternateRowBackground = true,
					HighlightRowsOnHover = true,
					ShowFooter = false
				},
				ClientSideEvents = new ClientSideEvents
				{
					BeforeAjaxRequest = "gridBeforeAjaxRequest"
				},
				RenderingMode = RenderingMode.Optimized,
			};
		}

		private List<JQGridColumn> InitializeAllColumns()
		{
			var columns = new List<JQGridColumn>
			{


				new JQGridColumn
				{
					DataField = "FilePath",
					DataType = typeof(string),
					HeaderText = "Web Path",
					Width = 10
				},

				new JQGridColumn
				{
					DataField = "GPSFilePath",
					HeaderText = "GPS FilePath",
					DataType = typeof(string),
					Width = 10
				},

				new JQGridColumn
				{
					DataField = "Id",
					PrimaryKey = true,
					Editable = false,
					Visible = false
				},

				new JQGridColumn
				{
					DataField = "Status",
					HeaderText = "Status",
					DataType = typeof(string),
					Width = 5
				},

				new JQGridColumn
				{
					DataField = "TimelineReqested",
					HeaderText = "Timeline Reqested",
					DataType = typeof(bool),
					Width = 10
				},

				new JQGridColumn
				{
					DataField = "TimelineType",
					HeaderText = "Timeline Type",
					DataType = typeof(string),
					Width = 8
				},

				new JQGridColumn
				{
					DataField = "UpdatedDateReason",
					HeaderText = "Updated Date Reason",
					DataType = typeof(string),
					Width = 10
				},

				new JQGridColumn
				{
					DataField = "DraftDate",
					HeaderText = "Draft Date",
					DataFormatString = "{0:d}",
					DataType = typeof(DateTime),
					Width = 8
				},

				new JQGridColumn
				{
					DataField = "ApprovalDate",
					HeaderText = "Approval Date",
					DataFormatString = "{0:d}",
					DataType = typeof(DateTime),
					Width = 9,
					Editable = false
				},

				new JQGridColumn
				{
					DataField = "EditDate",
					HeaderText = "Edit Date",
					DataFormatString = "{0:d}",
					DataType = typeof(DateTime),
					Width = 7,
					Editable = false
				},

				new JQGridColumn
				{
					DataField = "EditUser",
					HeaderText = "Edit User",
					DataType = typeof(int),
					Width = 7,
					Editable = false
				},

				new JQGridColumn
				{
					DataField = "Edit",
					HeaderText = " ",
					Formatter = new CustomFormatter { FormatFunction = "editLink" },
					Searchable = false,
					Sortable = false,
					Width = 2,
					TextAlign = TextAlign.Right,
					CssClass = "padright"
				}
			};
			return columns;
		}
	}

	public class DraftLettersGridRecord
	{
		public DateTime? ApprovalDate { get; set; }
		public DateTime? DraftDate { get; set; }
		public string Edit { get; set; }
		public string FilePath { get; set; }
		public string GPSFilePath { get; set; }
		public int Id { get; set; }
		public DateTime? EditDate { get; set; }
		public int? EditUser { get; set; }
		public string Status { get; set; }
		public bool TimelineReqested { get; set; }
		public string TimelineType { get; set; }
		public string UpdatedDateReason { get; set; }

	}
}