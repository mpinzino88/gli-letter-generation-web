﻿using Submissions.Common;
using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;

namespace Submissions.Web.Models.Grids.Documents
{
	using Trirand.Web.Mvc;

	public class LetterBookGrid
	{
		public LetterBookGrid(Department department)
		{
			this.Department = department;
			this.Grid = new JQGrid
			{
				ID = "LetterBookGrid",
				Columns = InitializeAllColumns(),
				DataUrl = new UrlHelper(((MvcHandler)HttpContext.Current.Handler).RequestContext).Action("LoadLetterBookGrid", "GridsAjax", new { Area = "" }),
				AutoWidth = false,
				ShrinkToFit = false,
				Width = Unit.Percentage(100),
				Height = Unit.Percentage(100),
				ToolBarSettings = new ToolBarSettings
				{
					ShowRefreshButton = true,
					ShowSearchToolBar = true,
					ToolBarPosition = ToolBarPosition.Top
				},
				PagerSettings = new PagerSettings
				{
					PageSize = 1000,
					PageSizeOptions = "[]"
				},
				AppearanceSettings = new AppearanceSettings
				{
					AlternateRowBackground = true,
					HighlightRowsOnHover = true,
					ShowFooter = false
				},
				ClientSideEvents = new ClientSideEvents
				{
					BeforeAjaxRequest = "gridBeforeAjaxRequest"
				},
				RenderingMode = RenderingMode.Optimized,
			};
		}

		public Department Department { get; private set; }
		public JQGrid Grid { get; set; }

		private List<JQGridColumn> InitializeAllColumns()
		{
			var allColumns = new List<JQGridColumn>();

			allColumns.Add(new JQGridColumn
			{
				DataField = "Id",
				PrimaryKey = true,
				Editable = false,
				Visible = false
			});

			allColumns.Add(new JQGridColumn
			{
				DataField = "FileNumbers",
				DataType = typeof(List<object>),
				Visible = false
			});

			allColumns.Add(new JQGridColumn
			{
				DataField = "DepartmentCode",
				DataType = typeof(string),
				Visible = false
			});

			allColumns.Add(new JQGridColumn
			{
				DataField = "BatchNumber",
				HeaderText = "Batch",
				DataType = typeof(int?),
				Width = 50
			});

			allColumns.Add(new JQGridColumn
			{
				DataField = "BillingUser",
				HeaderText = "Biller",
				DataType = typeof(string),
				SearchToolBarOperation = SearchOperation.Contains,
				Width = 90
			});

			allColumns.Add(new JQGridColumn
			{
				DataField = "LetterWriter",
				HeaderText = "Letter Writer",
				DataType = typeof(string),
				Width = 115
			});

			allColumns.Add(new JQGridColumn
			{
				DataField = "DynamicsFileNumber",
				HeaderText = "Dynamics File",
				DataType = typeof(string),
				Width = 130
			});

			allColumns.Add(new JQGridColumn
			{
				DataField = "FinalLetterGPSItemId",
				HeaderText = "Letter",
				DataType = typeof(string),
				Width = 40,
				Formatter = new CustomFormatter { FormatFunction = "pdfLetterLink" },
				Searchable = false
			});

			allColumns.Add(new JQGridColumn
			{
				DataField = "LetterType",
				HeaderText = "Letter Type",
				DataType = typeof(string),
				Width = 95
			});

			allColumns.Add(new JQGridColumn
			{
				DataField = "Status",
				HeaderText = "Status",
				DataType = typeof(string),
				Width = 75,
				Formatter = new CustomFormatter { FormatFunction = "formatStatus" },
				SearchToolBarOperation = SearchOperation.Contains
			});

			allColumns.Add(new JQGridColumn
			{
				DataField = "IsLatest",
				Visible = true,
				HeaderText = "Old",
				Formatter = new CustomFormatter { FormatFunction = "formatIsLatestAlert" },
				Width = 30
			});
			allColumns.Add(new JQGridColumn
			{
				DataField = "Version",
				HeaderText = "Version",
				DataType = typeof(string),
				Searchable = false,
				Width = 50
			});

			allColumns.Add(new JQGridColumn
			{
				DataField = "FileNumbersJson",
				HeaderText = "File Number",
				DataType = typeof(string),
				Width = 120,
				Formatter = new CustomFormatter { FormatFunction = "formatFileNumbersJson" },
				SearchToolBarOperation = SearchOperation.Contains
			});

			allColumns.Add(new JQGridColumn
			{
				DataField = "Jurisdiction",
				HeaderText = "Jurisdiction",
				DataType = typeof(string),
				Searchable = false,
				Width = 180
			});

			allColumns.Add(new JQGridColumn
			{
				DataField = "Edit",
				HeaderText = "Edit",
				Formatter = new CustomFormatter { FormatFunction = "editLink" },
				Searchable = false,
				Sortable = false,
				Width = 60,
			});

			allColumns.Add(new JQGridColumn
			{
				DataField = "IsBillableJurisdiction",
				HeaderText = "Billable",
				DataType = typeof(string),
				Searchable = false,
				Width = 60,
				Formatter = new CustomFormatter { FormatFunction = "formatIsBillable" }
			});

			allColumns.Add(new JQGridColumn
			{
				DataField = "BillingSheet",
				HeaderText = "BillingSheet",
				DataType = typeof(string),
				Width = 130
			});

			allColumns.Add(new JQGridColumn
			{
				DataField = "ReportDate",
				HeaderText = "Report Date",
				DataFormatString = "{0:d}",
				DataType = typeof(DateTime),
				Width = 80
			});

			allColumns.Add(new JQGridColumn
			{
				DataField = "LaboratoryLocation",
				HeaderText = "Office",
				DataType = typeof(string),
				Width = 50
			});

			allColumns.Add(new JQGridColumn
			{
				DataField = "BilledCount",
				HeaderText = "# Items",
				DataType = typeof(int),
				Searchable = false,
				Width = 50
			});
			allColumns.Add(new JQGridColumn
			{
				DataField = "Company",
				HeaderText = "Company",
				DataType = typeof(string),
				Width = 80
			});
			allColumns.Add(new JQGridColumn
			{
				DataField = "ContractType",
				HeaderText = "Contract Type",
				DataType = typeof(string),
				Width = 100
			});
			allColumns.Add(new JQGridColumn
			{
				DataField = "BallyType",
				HeaderText = "Bally Type",
				DataType = typeof(string),
				Width = 70
			});
			allColumns.Add(new JQGridColumn
			{
				DataField = "Currency",
				HeaderText = "Currency",
				DataType = typeof(string),
				Width = 70
			});

			allColumns.Add(new JQGridColumn
			{
				DataField = "Note",
				DataType = typeof(string),
				HeaderText = "Invoice Comment",
				Formatter = new CustomFormatter { FormatFunction = "formatComment" },
				Width = 300
			});

			//allColumns.Add(new JQGridColumn // column for scrollbar
			//{
			//	HeaderText = " ",
			//	Width = 20,
			//	Searchable = false,
			//	Sortable = false
			//});
			return allColumns;
		}
	}

	public class LetterBookGridRecord
	{
		public int Id { get; set; }
		public int? BatchNumber { get; set; }
		public string BillingUser { get; set; }
		public string DepartmentCode { get; set; }
		public int DocumentId { get; set; }
		public List<object> FileNumbers { get; set; }
		public bool IsLatest { get; set; }
		public string Status { get; set; }
		public int? FinalLetterGPSItemId { get; set; }
		public string FileNumbersJson { get; set; }
		public string Jurisdiction { get; set; }
		public DateTime ReportDate { get; set; }
		public string EntryUserName { get; set; }
		public string LaboratoryLocation { get; set; }
		public bool IsBillableJurisdiction { get; set; }
		public string Edit { get; set; }
		public int BilledCount { get; set; }
		public string DynamicsFileNumber
		{
			get
			{
				return new FileNumber(string.Format("{0}{1}{2}", this.FileNumber, "-", this.JurisdictionId)).AccountingFormatNoDashes;
			}
		}
		public string Version { get; set; }
		public string Note { get; set; }
		public string HasNote { get { return !string.IsNullOrWhiteSpace(Note) && Note != "N/A" ? "" : "Needs Comment"; } }
		public string LetterType { get; set; }
		public string JurisdictionId { get; internal set; }
		public string FileNumber { get; internal set; }
		public string LetterWriter { get; set; }
		public int ProjectId { get; set; }
		public string Company { get; set; }
		public string ContractType { get; set; }
		public string BallyType { get; set; }
		public string Currency { get; set; }
		public string BillingSheet { get; set; }
	}
}