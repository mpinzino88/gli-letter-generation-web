﻿using Submissions.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;

namespace Submissions.Web.Models.Grids
{
	using Trirand.Web.Mvc;

	public class JurisdictionalDataGrid
	{
		public JurisdictionalDataGrid()
		{
			this.Grid = new JQGrid
			{
				ID = "JurisdicdtionalDataGrid",
				Columns = InitializeAllColumns(),
				DataUrl = new UrlHelper(((MvcHandler)HttpContext.Current.Handler).RequestContext).Action("LoadJurisdictionalDataGrid", "GridsAjax", new { Area = "" }),
				AutoWidth = true,
				Width = Unit.Percentage(100),
				Height = Unit.Percentage(100),
				ToolBarSettings = new ToolBarSettings
				{
					ShowRefreshButton = true,
					ToolBarPosition = ToolBarPosition.Top,
					ShowSearchToolBar = true,
				},
				PagerSettings = new PagerSettings
				{
					PageSize = 100,
					PageSizeOptions = "[]"
				},
				AppearanceSettings = new AppearanceSettings
				{
					AlternateRowBackground = true,
					HighlightRowsOnHover = true,
					ShowFooter = false
				},
				ClientSideEvents = new ClientSideEvents
				{
					BeforeAjaxRequest = "jurisdictionalDataGridBeforeAjaxRequest"
				},
				RenderingMode = RenderingMode.Optimized
			};
			InitializeDropDowns();
		}

		public JQGrid Grid { get; set; }

		private List<JQGridColumn> InitializeAllColumns()
		{
			var allColumns = new List<JQGridColumn>();

			allColumns.Add(new JQGridColumn
			{
				DataField = "Id",
				PrimaryKey = true,
				Editable = false,
				Visible = false
			});
			allColumns.Add(new JQGridColumn
			{
				DataField = "SubmissionId",
				Visible = false
			});
			allColumns.Add(new JQGridColumn
			{
				DataField = "ReplaceWithSubmissionId",
				Visible = false
			});
			allColumns.Add(new JQGridColumn
			{
				DataField = "SubmissionManufacturer",
				HeaderText = "Manufacturer",
				DataType = typeof(string),
				SearchToolBarOperation = SearchOperation.BeginsWith,
				SearchType = SearchType.AutoComplete,
				SearchControlID = "ManufacturerAutoComplete",
				Width = 200
			});
			allColumns.Add(new JQGridColumn
			{
				DataField = "SubmissionFileNumber",
				HeaderText = "File Number",
				DataType = typeof(string),
				SearchToolBarOperation = SearchOperation.BeginsWith,
				SearchType = SearchType.AutoComplete,
				SearchControlID = "FileNumberAutoComplete",
				Width = 150,
				Formatter = new CustomFormatter { FormatFunction = "fileNumberLink" }
			});
			allColumns.Add(new JQGridColumn
			{
				DataField = "SubmissionIdNumber",
				HeaderText = "ID Number",
				DataType = typeof(string),
				SearchToolBarOperation = SearchOperation.BeginsWith,
				SearchType = SearchType.AutoComplete,
				SearchControlID = "IdNumberAutoComplete",
				Width = 150
			});
			allColumns.Add(new JQGridColumn
			{
				DataField = "SubmissionDateCode",
				HeaderText = "Date Code",
				DataType = typeof(string),
				SearchToolBarOperation = SearchOperation.BeginsWith,
				SearchType = SearchType.AutoComplete,
				SearchControlID = "DateCodeAutoComplete",
				Width = 200
			});
			allColumns.Add(new JQGridColumn
			{
				DataField = "SubmissionVersion",
				HeaderText = "Version",
				DataType = typeof(string),
				SearchToolBarOperation = SearchOperation.BeginsWith,
				SearchType = SearchType.AutoComplete,
				SearchControlID = "VersionAutoComplete",
				Width = 150
			});
			allColumns.Add(new JQGridColumn
			{
				DataField = "SubmissionGameName",
				HeaderText = "Game Name",
				DataType = typeof(string),
				SearchToolBarOperation = SearchOperation.BeginsWith,
				SearchType = SearchType.AutoComplete,
				SearchControlID = "GameNameAutoComplete",
				Width = 250
			});
			allColumns.Add(new JQGridColumn
			{
				DataField = "SubmissionPosition",
				HeaderText = "Position",
				DataType = typeof(string),
				SearchToolBarOperation = SearchOperation.BeginsWith,
				SearchType = SearchType.AutoComplete,
				SearchControlID = "PositionAutoComplete",
				Width = 150
			});
			allColumns.Add(new JQGridColumn
			{
				DataField = "SubmissionFunction",
				HeaderText = "Function",
				DataType = typeof(string),
				SearchToolBarOperation = SearchOperation.BeginsWith,
				SearchType = SearchType.AutoComplete,
				SearchControlID = "FunctionAutoComplete",
				Width = 150
			});
			allColumns.Add(new JQGridColumn
			{
				DataField = "SubmissionLab",
				HeaderText = "Test Lab",
				DataType = typeof(string),
				SearchToolBarOperation = SearchOperation.IsEqualTo,
				SearchType = SearchType.AutoComplete,
				SearchControlID = "LabAutoComplete",
				Width = 80
			});
			allColumns.Add(new JQGridColumn
			{
				DataField = "JurisdictionName",
				HeaderText = "Jurisdiction",
				DataType = typeof(string),
				SearchToolBarOperation = SearchOperation.BeginsWith,
				SearchType = SearchType.AutoComplete,
				SearchControlID = "JurisdictionAutoComplete",
				Width = 200
			});
			allColumns.Add(new JQGridColumn
			{
				DataField = "Status",
				HeaderText = "Status",
				DataType = typeof(string),
				SearchToolBarOperation = SearchOperation.IsEqualTo,
				SearchType = SearchType.DropDown,
				Width = 150
			});
			allColumns.Add(new JQGridColumn
			{
				DataField = "RequestDate",
				HeaderText = "Request Date",
				DataFormatString = "{0:d}",
				DataType = typeof(DateTime),
				Width = 100
			});
			allColumns.Add(new JQGridColumn
			{
				DataField = "DraftDate",
				HeaderText = "Draft Date",
				DataFormatString = "{0:d}",
				DataType = typeof(DateTime),
				Width = 100
			});
			allColumns.Add(new JQGridColumn
			{
				DataField = "CloseDate",
				HeaderText = "Close Date",
				DataFormatString = "{0:d}",
				DataType = typeof(DateTime),
				Width = 100
			});
			allColumns.Add(new JQGridColumn
			{
				DataField = "RevokeDate",
				HeaderText = "Revoke Date",
				DataFormatString = "{0:d}",
				DataType = typeof(DateTime),
				Width = 100
			});
			allColumns.Add(new JQGridColumn
			{
				DataField = "ObsoleteDate",
				HeaderText = "NU Date",
				DataFormatString = "{0:d}",
				DataType = typeof(DateTime),
				Width = 100
			});
			allColumns.Add(new JQGridColumn
			{
				DataField = "UpgradeByDate",
				HeaderText = "Upgrade By Date",
				DataFormatString = "{0:d}",
				DataType = typeof(DateTime),
				Width = 110
			});
			allColumns.Add(new JQGridColumn
			{
				DataField = "TargetDate",
				HeaderText = "Estimated Completion Date",
				DataFormatString = "{0:d}",
				DataType = typeof(DateTime),
				Width = 200//,
						   //Formatter = new CustomFormatter { FormatFunction = "targetDateLink" }
			});
			allColumns.Add(new JQGridColumn
			{
				DataField = "ReplaceWith",
				HeaderText = "Replace With",
				DataType = typeof(string),
				SearchToolBarOperation = SearchOperation.BeginsWith,
				Width = 180//,
						   //Formatter = new CustomFormatter { FormatFunction = "replaceWithLink" }
			});

			return allColumns;
		}

		private void InitializeDropDowns()
		{
			var statusColumn = this.Grid.Columns.Find(x => x.DataField == "Status");
			if (statusColumn.Visible)
			{
				if (Grid.AjaxCallBackMode == AjaxCallBackMode.RequestData)
				{
					statusColumn.SearchList = LookupsStandard.ConvertEnum(showBlank: true, enumValues: ComUtil.InternalJurisdictionStatuses().ToArray())
						.Select(x => new SelectListItem { Value = x.Value, Text = HttpUtility.HtmlDecode(x.Text + " &nbsp; " + x.Value) })
						.ToList();
				}
			}
		}
	}

	public class JurisdictionalDataGridRecord
	{
		public int Id { get; set; }
		public int? SubmissionId { get; set; }
		public string SubmissionManufacturer { get; set; }
		public string SubmissionFileNumber { get; set; }
		public string SubmissionIdNumber { get; set; }
		public string SubmissionDateCode { get; set; }
		public string SubmissionVersion { get; set; }
		public string SubmissionGameName { get; set; }
		public string SubmissionPosition { get; set; }
		public string SubmissionFunction { get; set; }
		public string SubmissionLab { get; set; }
		public string JurisdictionName { get; set; }
		public string Status { get; set; }
		public DateTime? RequestDate { get; set; }
		public DateTime? DraftDate { get; set; }
		public DateTime? CloseDate { get; set; }
		public DateTime? RevokeDate { get; set; }
		public DateTime? ObsoleteDate { get; set; }
		public DateTime? UpgradeByDate { get; set; }
		public DateTime? TargetDate { get; set; }
		public string ReplaceWith { get; set; }
		public int? ReplaceWithSubmissionId { get; set; }
	}
}