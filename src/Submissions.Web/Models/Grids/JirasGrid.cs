﻿using Submissions.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;

namespace Submissions.Web.Models.Grids
{
	using Trirand.Web.Mvc;
	public class JirasGrid
	{
		public JQGrid Grid { get; set; }
		public JirasGrid()
		{
			this.Grid = new JQGrid
			{
				ID = "JirasGrid",
				Columns = LoadColumns(),
				DataUrl = new UrlHelper(((MvcHandler)HttpContext.Current.Handler).RequestContext).Action("LoadJiraListGrid", "GridsAjax", new { Area = "" }),
				AutoWidth = true,
				Width = Unit.Percentage(100),
				Height = Unit.Percentage(100),
				ToolBarSettings = new ToolBarSettings
				{
					ShowRefreshButton = true,
					ToolBarPosition = ToolBarPosition.Top,
					ShowSearchToolBar = true,
				},
				PagerSettings = new PagerSettings
				{
					PageSize = 100,
					PageSizeOptions = "[]"
				},
				AppearanceSettings = new AppearanceSettings
				{
					AlternateRowBackground = true,
					HighlightRowsOnHover = true,
					ShowFooter = true,
					Caption = "Jira Grid"
				},
				SortSettings = new SortSettings
				{
					InitialSortColumn = "Id",
					InitialSortDirection = SortDirection.Desc
				},
				ClientSideEvents = new ClientSideEvents
				{
					BeforeAjaxRequest = "jiraListGridBeforeAjaxRequest"
				},
				RenderingMode = RenderingMode.Optimized
			};

		}

		private List<JQGridColumn> LoadColumns()
		{
			var columns = new List<JQGridColumn>();

			columns.Add(new JQGridColumn
			{
				DataField = "Id",
				PrimaryKey = true,
				Editable = false,
				Visible = false
			});

			columns.Add(new JQGridColumn
			{
				DataField = "Key",
				HeaderText = "Key",
				DataType = typeof(string),
				Width = 120,
				Formatter = new CustomFormatter { FormatFunction = "jiraLink" },
				Searchable = false
			});

			columns.Add(new JQGridColumn
			{
				DataField = "Status",
				HeaderText = "Status",
				DataType = typeof(string),
				Width = 70,
				Searchable = false
			});

			columns.Add(new JQGridColumn
			{
				DataField = "Resolution",
				HeaderText = "Resolution",
				DataType = typeof(string),
				Width = 70,
				Searchable = false
			});

			columns.Add(new JQGridColumn
			{
				DataField = "Summary",
				HeaderText = "Summary",
				DataType = typeof(string),
				Width = 250,
				Searchable = false
			});
						
			return columns;
		}
	}


	public class QuickDetailJirasGrid : JirasGrid
	{
		public QuickDetailJirasGrid()
		{
			this.Grid.ID = "QuickDetailJirasGrid";

			this.Grid.ShrinkToFit = false;
			this.Grid.AutoWidth = true;

			this.Grid.AppearanceSettings = new AppearanceSettings
			{
				AlternateRowBackground = true,
				HighlightRowsOnHover = true,
				ShowFooter = false,
				Caption = "Project Jiras Associated To Jurisdictions"
			};
			this.Grid.ToolBarSettings = new ToolBarSettings
			{
				ShowRefreshButton = false,
				ToolBarPosition = ToolBarPosition.Top,
				ShowSearchToolBar = false
			};
		}
	}

	public class JirasGridRecord
	{
		public int Id { get; set; }
		public string Key { get; set; }
		public string Status { get; set; }
		public string Summary { get; set; }
		public string Resolution { get; set; }
	}
}