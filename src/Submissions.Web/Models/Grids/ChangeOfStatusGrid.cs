﻿using Submissions.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;

namespace Submissions.Web.Models.Grids
{
	using Trirand.Web.Mvc;

	public class ChangeOfStatusGrid
	{
		public ChangeOfStatusGrid()
		{
			this.Grid = new JQGrid
			{
				ID = "ChangeOfStatusGrid",
				Columns = LoadColumns(),
				DataUrl = new UrlHelper(((MvcHandler)HttpContext.Current.Handler).RequestContext).Action("LoadChangeOfStatusGrid", "GridsAjax", new { Area = "" }),
				AutoWidth = true,
				Width = Unit.Percentage(100),
				Height = Unit.Percentage(100),
				ToolBarSettings = new ToolBarSettings
				{
					ShowRefreshButton = true,
					ToolBarPosition = ToolBarPosition.Top,
					ShowSearchToolBar = true,
				},
				PagerSettings = new PagerSettings
				{
					PageSize = 100,
					PageSizeOptions = "[]"
				},
				AppearanceSettings = new AppearanceSettings
				{
					AlternateRowBackground = true,
					HighlightRowsOnHover = true,
					ShowFooter = true,
					Caption = "Change of Status Requests"
				},
				SortSettings = new SortSettings
				{
					InitialSortColumn = "SubmitDate",
					InitialSortDirection = SortDirection.Desc
				},
				ClientSideEvents = new ClientSideEvents
				{
					BeforeAjaxRequest = "changeOfStatusGridBeforeAjaxRequest"
				},
				RenderingMode = RenderingMode.Optimized
			};
		}

		public JQGrid Grid { get; set; }

		private List<JQGridColumn> LoadColumns()
		{
			var columns = new List<JQGridColumn>();

			columns.Add(new JQGridColumn
			{
				DataField = "Id",
				PrimaryKey = true,
				Editable = false,
				Visible = false
			});

			columns.Add(new JQGridColumn
			{
				DataField = "SubmitDate",
				HeaderText = "SubmitDate",
				DataFormatString = "{0:d}",
				DataType = typeof(DateTime),
				Width = 70,
				Searchable = false
			});

			columns.Add(new JQGridColumn
			{
				DataField = "Submitter",
				HeaderText = "Submitter",
				DataType = typeof(string),
				Width = 120,
				Searchable = false
			});

			columns.Add(new JQGridColumn
			{
				DataField = "CosType",
				HeaderText = "Cos Type",
				DataType = typeof(string),
				Width = 150,
				Searchable = false,

			});

			columns.Add(new JQGridColumn
			{
				DataField = "Status",
				HeaderText = "Status",
				DataType = typeof(string),
				Width = 70,
				Searchable = false,
			});

			columns.Add(new JQGridColumn
			{
				DataField = "",
				HeaderText = "View",
				Formatter = new CustomFormatter { FormatFunction = "cosViewLink" },
				DataType = typeof(string),
				Width = 70,
				Searchable = false,
			});

			return columns;
		}
	}

	public class QuickCOSGrid : ChangeOfStatusGrid
	{
		public QuickCOSGrid()
		{
			this.Grid.ID = "QuickCOSRequestsGrid";

			this.Grid.ShrinkToFit = false;
			this.Grid.AutoWidth = true;

			this.Grid.AppearanceSettings = new AppearanceSettings
			{
				AlternateRowBackground = true,
				HighlightRowsOnHover = true,
				ShowFooter = false,
				Caption = "DB Change Requests"
			};
			this.Grid.ToolBarSettings = new ToolBarSettings
			{
				ShowRefreshButton = false,
				ToolBarPosition = ToolBarPosition.Top,
				ShowSearchToolBar = false
			};
		}
	}

	public class ChangeOfStatusGridRecord
	{
		public System.Guid Id { get; set; }
		public string Submitter { get; set; }
		public DateTime? SubmitDate { get; set; }
		public string CosType { get; set; }
		public string FileNumber { get; set; }
		public DateTime? Approved { get; set; }
		public string Status { get; set; }
		
	}
}