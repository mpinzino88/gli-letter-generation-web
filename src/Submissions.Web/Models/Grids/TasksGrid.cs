using EF.Submission;
using Submissions.Common;
using Submissions.Common.JQGrid;
using Submissions.Common.Linq.Dynamic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;

namespace Submissions.Web.Models.Grids
{
	using Trirand.Web.Mvc;

	public class TasksGrid : IDynamicGrid, IColumnChooserGrid
	{
		public TasksGrid()
		{
			InitializeAllColumns();

			this.Grid = new JQGrid
			{
				ID = "TasksGrid",
				Columns = this.AllColumns.Cast<JQGridColumn>().ToList(),
				DataUrl = new UrlHelper(((MvcHandler)HttpContext.Current.Handler).RequestContext).Action("LoadProjectTasksGrid", "GridsAjax", new { Area = "" }),
				AutoWidth = true,
				Width = Unit.Percentage(100),
				Height = Unit.Percentage(100),
				ToolBarSettings = new ToolBarSettings
				{
					ShowRefreshButton = true,
					ToolBarPosition = ToolBarPosition.Top,
					ShowSearchToolBar = true
				},
				PagerSettings = new PagerSettings
				{
					PageSize = 150,
					PageSizeOptions = "[]"
				},
				AppearanceSettings = new AppearanceSettings
				{
					AlternateRowBackground = true,
					HighlightRowsOnHover = true,
					ShowFooter = true,
					Caption = "Tasks"
				},
				SortSettings = new SortSettings
				{
					InitialSortColumn = "TargetDate",
					InitialSortDirection = SortDirection.Desc
				},
				ClientSideEvents = new ClientSideEvents
				{
					BeforeAjaxRequest = "tasksGridBeforeAjaxRequest"
				},
				RenderingMode = RenderingMode.Optimized
			};
		}

		public void InitializeDynamicGrid(string savedColumns)
		{
			this.SetColumns(savedColumns);
		}

		public void InitializeWidgetGrid(int widgetId, string savedColumns)
		{
			var grid = this.Grid;
			grid.ID += widgetId.ToString();
			grid.DataUrl = new UrlHelper(((MvcHandler)HttpContext.Current.Handler).RequestContext).Action("LoadWidgetTasksGrid", "GridsAjax", new { Area = "" });
			grid.AutoWidth = true;
			grid.ToolBarSettings = new ToolBarSettings
			{
				ShowAddButton = false,
				ShowDeleteButton = false,
				ShowEditButton = false,
				ShowRefreshButton = false,
				ShowSearchButton = false,
				ToolBarPosition = ToolBarPosition.Top
			};
			grid.ClientSideEvents = new ClientSideEvents
			{
				AfterAjaxRequest = "gridAfterAjaxRequest"
			};
			this.SetColumns(savedColumns);
		}

		public JQGrid Grid { get; set; }
		public string GridBaseID { get; set; }
		public List<JQGridColumnDynamic> AllColumns { private set; get; }
		public List<JQGridColumnDynamic> SelectedColumns { private set; get; }
		public IList<ColumnChooserColumn> ColumnChooserSelectedColumns
		{
			get
			{
				return this.SelectedColumns
					.Where(x => x.Visible == true)
					.Select(x => new ColumnChooserColumn { DatabaseName = x.DataField, DisplayName = x.HeaderText })
					.ToList();

			}
		}
		public IList<ColumnChooserColumn> ColumnChooserAvailableColumns
		{
			get
			{
				return this.AllColumns.Except(this.SelectedColumns)
					.Where(x => x.Visible == true)
					.Select(x => new ColumnChooserColumn { DatabaseName = x.DataField, DisplayName = x.HeaderText })
					.OrderBy(x => x.DisplayName)
					.ToList();
			}
		}

		public string GetColumnsDynamicLinqFormat()
		{
			var selectFields = this.SelectedColumns;
			var selectStatement = new StringBuilder();
			selectStatement.Append("new (");

			foreach (var col in selectFields)
			{
				if (col.DataField == "DynamicsID")
				{
					selectStatement.Append("\"\" As DynamicsID,");
				}
				else if (col.DataField == "ActualHours")
				{
					selectStatement.Append("0 As ActualHours,");
				}
				else
				{
					selectStatement.AppendFormat("{0} As {1},", col.SqlSelectName, col.DataField);
				}
			}

			selectStatement.Length--;
			selectStatement.Append(")");

			return selectStatement.ToString();
		}

		public void SetColumns(string savedColumns)
		{
			var jqColumns = new List<JQGridColumnDynamic>();

			if (savedColumns == null)
			{
				jqColumns = this.AllColumns;
			}
			else
			{
				var widgetId = this.Grid.ID.Replace(this.GridBaseID, "");
				var savedColumnsList = new List<string>(savedColumns.Replace("\"", "").Replace("[", "").Replace("]", "").Split(','));
				var hiddenColumns = this.AllColumns.Where(x => x.Visible == false).ToList();

				jqColumns.AddRange(hiddenColumns);
				foreach (var col in savedColumnsList)
				{
					var column = this.AllColumns.Where(x => x.DataField == col).SingleOrDefault();
					if (column != null)
					{
						column.SearchControlID += widgetId;
						jqColumns.Add(column);
					}
				}
			}

			this.Grid.Columns = jqColumns.Cast<JQGridColumn>().ToList();
			this.SelectedColumns = jqColumns;
		}

		public IList<SelectListItem> GetAvailableSorts()
		{
			var columnList = new List<SelectListItem>() { new SelectListItem { Text = "", Value = "" } };

			var allColumns = this.AllColumns
				.Where(x => x.Visible && x.Searchable)
				.Select(x => new SelectListItem { Text = x.HeaderText, Value = x.SqlSelectName })
				.OrderBy(x => x.Text)
				.ToList();

			columnList.AddRange(allColumns);

			return columnList;
		}

		public void Export(Export export, IQueryable<vw_trTask> query)
		{
			if (export.WidgetId == null)
			{
				this.InitializeDynamicGrid(export.SavedColumns);
			}
			else
			{
				this.InitializeWidgetGrid((int)export.WidgetId, export.SavedColumns);
			}

			var selectStatement = this.GetColumnsDynamicLinqFormat();
			var results = query.Select(selectStatement);

			var fileName = string.Format(export.Title + " - {0:yyyy-MM-dd_hh-mm-ss-tt}", DateTime.Now);
			var jqGridHelper = new JQGridHelper();
			var jqGridState = new JQGridState();
			jqGridState.QueryString = HttpUtility.ParseQueryString(export.FiltersToolbar ?? "");

			if (export.ExportType == ExportType.Xls)
			{
				fileName += ".xls";
				this.Grid.ExportToExcel(results, fileName, jqGridState);
			}
			else if (export.ExportType == ExportType.Csv)
			{
				fileName += ".csv";
				this.Grid.ExportSettings.ExportDataRange = ExportDataRange.All;
				var exportData = this.Grid.GetExportData(results, jqGridState);
				jqGridHelper.ExportToCSV(exportData, fileName);
			}
			else if (export.ExportType == ExportType.Pdf)
			{
				fileName += ".pdf";
				this.Grid.ExportSettings.ExportDataRange = ExportDataRange.All;
				var exportData = this.Grid.GetExportData(results, jqGridState);
				jqGridHelper.ExportToPDF(exportData, fileName);
			}
		}

		private void InitializeAllColumns()
		{
			var allColumns = new List<JQGridColumnDynamic>();

			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "Id",
				SqlSelectName = "Id",
				PrimaryKey = true,
				Editable = false,
				Visible = false
			});
			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "TaskDefinitionId",
				SqlSelectName = "TaskDefinitionId",
				Editable = false,
				Visible = false
			});
			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "EngTaskOrder",
				SqlSelectName = "EngTaskOrder",
				Editable = false,
				Visible = false
			});
			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "QATaskOrder",
				SqlSelectName = "QATaskOrder",
				Editable = false,
				Visible = false
			});
			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "ProjectId",
				SqlSelectName = "ProjectId",
				Editable = false,
				Visible = false
			});

			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "BundleId",
				SqlSelectName = "BundleId",
				Editable = false,
				Visible = false
			});

			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "IsRush",
				SqlSelectName = "Project.IsRush",
				Editable = false,
				Visible = false
			});

			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "IsDeleted",
				SqlSelectName = "IsDeleted",
				Editable = false,
				Visible = false
			});

			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "AssociatedJur",
				SqlSelectName = "AssociatedJur",
				Editable = false,
				Visible = false
			});

			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "Status",
				SqlSelectName = "TaskStatus",
				Editable = false,
				Visible = false
			});

			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "QATaskStatus",
				SqlSelectName = "QATaskStatus",
				Editable = false,
				Visible = false
			});

			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "BundleType",
				SqlSelectName = "BundleType",
				Editable = false,
				Visible = false
			});

			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "ENInvolvementWhileFL",
				SqlSelectName = "ENInvolvementWhileFL",
				Editable = false,
				Visible = false
			});

			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "ProjectTargetDiff",
				SqlSelectName = "ProjectTargetDiff",
				Editable = false,
				Visible = false
			});

			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "UserId",
				SqlSelectName = "UserId",
				Editable = false,
				Visible = false
			});

			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "ProjectIndicator",
				SqlSelectName = "",
				HeaderText = " ",
				Editable = false,
				Visible = false,
				Searchable = false,
				Width = 50,
			});

			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "ProjectName",
				SqlSelectName = "Project.ProjectName",
				HeaderText = "Project Name",
				DataType = typeof(string),
				Searchable = true,
				SearchToolBarOperation = SearchOperation.Contains,
				Width = 170,
			});

			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "ProjectJurCount",
				HeaderText = "Jur Count",
				Searchable = false,
				Width = 70,
				Visible = false
			});

			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "JiraOpen",
				HeaderText = "Open Jiras",
				Width = 70,
				Searchable = false,
				Visible = false
			});

			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "GameName",
				SqlSelectName = "GameName",
				HeaderText = "Game Name",
				DataType = typeof(string),
				Searchable = true,
				SearchToolBarOperation = SearchOperation.Contains,
				Width = 160,
			});

			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "IdNumber",
				SqlSelectName = "IdNumber",
				HeaderText = "Id Number",
				DataType = typeof(string),
				Searchable = true,
				SearchToolBarOperation = SearchOperation.Contains,
				Width = 150,
			});

			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "Manager",
				SqlSelectName = "Project.Manager.FName + \" \" + Project.Manager.LName",
				HeaderText = "Manager",
				DataType = typeof(string),
				Searchable = true,
				SearchToolBarOperation = SearchOperation.BeginsWith,
				Width = 100,
			});

			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "QAAccepted",
				SqlSelectName = "QABundle.QAAcceptDate",
				HeaderText = "QA Accepted",
				DataFormatString = "{0:d}",
				DataType = typeof(DateTime),
				Width = 80,
				Searchable = true,
				SearchControlID = "DatePicker",
				SearchType = SearchType.DatePicker,
				TextAlign = TextAlign.Center,
				Visible = false
			});

			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "ProjectStatus",
				SqlSelectName = "Project.Status",
				HeaderText = "Status",
				DataType = typeof(string),
				Searchable = true,
				SearchToolBarOperation = SearchOperation.BeginsWith,
				Width = 60,
				TextAlign = TextAlign.Center
			});

			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "BundleStatus",
				SqlSelectName = "QABundle.Status",
				HeaderText = "Status",
				DataType = typeof(string),
				Searchable = true,
				SearchToolBarOperation = SearchOperation.BeginsWith,
				Width = 60,
				Visible = false
			});

			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "ProjectHolder",
				SqlSelectName = "ProjectHolder",
				HeaderText = "Holder",
				DataType = typeof(string),
				Searchable = true,
				SearchToolBarOperation = SearchOperation.BeginsWith,
				Width = 80,
				Visible = false
			});

			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "Department",
				SqlSelectName = "Department.Code",
				HeaderText = "Department",
				DataType = typeof(string),
				Searchable = true,
				SearchToolBarOperation = SearchOperation.BeginsWith,
				Width = 70
			});

			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "DynamicsID",
				SqlSelectName = "DynamicsID",
				HeaderText = "Task Id",
				DataType = typeof(string),
				Searchable = true,
				Width = 80,
				TextAlign = TextAlign.Center
			});

			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "BillType",
				SqlSelectName = "BillType",
				HeaderText = "BillType",
				DataType = typeof(string),
				Width = 60,
				Searchable = true,
				Visible = false
			});


			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "IsAssigned",
				SqlSelectName = "IsAssigned",
				HeaderText = "Assigned?",
				DataType = typeof(string),
				Width = 60,
				Searchable = true,
				Visible = false
			});

			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "Name",
				SqlSelectName = "Name",
				HeaderText = "Task",
				DataType = typeof(string),
				Searchable = true,
				SearchToolBarOperation = SearchOperation.BeginsWith,
				Width = 210
			});

			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "Description",
				SqlSelectName = "Description",
				HeaderText = "Description",
				DataType = typeof(string),
				Searchable = true,
				SearchToolBarOperation = SearchOperation.BeginsWith,
				Width = 220
			});

			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "",
				HeaderText = "Start/Complete",
				DataType = typeof(string),
				Searchable = false,
				Visible = false,
				Sortable = false,
				Width = 100,
				TextAlign = TextAlign.Center
			});

			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "User",
				SqlSelectName = "User.FName + \" \" + User.LName",
				HeaderText = "Assigned To",
				DataType = typeof(string),
				SearchToolBarOperation = SearchOperation.BeginsWith,
				SearchType = SearchType.AutoComplete,
				SearchControlID = "UserAutoComplete",
				Width = 150
			});

			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "AssignDate",
				SqlSelectName = "AssignDate",
				HeaderText = "Assign Date",
				DataFormatString = "{0:d}",
				DataType = typeof(DateTime),
				Width = 80,
				Searchable = true,
				SearchControlID = "DatePicker",
				SearchType = SearchType.DatePicker,
				TextAlign = TextAlign.Center
			});

			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "AssignedBy",
				SqlSelectName = "AddUser.FName+ \" \" + AddUser.LName",
				HeaderText = "Assigned By",
				DataType = typeof(string),
				SearchToolBarOperation = SearchOperation.BeginsWith,
				SearchType = SearchType.AutoComplete,
				SearchControlID = "UserAutoComplete",
				Width = 130
			});

			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "StartDate",
				SqlSelectName = "StartDate",
				HeaderText = "Start Date",
				DataFormatString = "{0:d}",
				DataType = typeof(DateTime),
				Width = 80,
				Searchable = true,
				SearchControlID = "DatePicker",
				SearchType = SearchType.DatePicker,
				TextAlign = TextAlign.Center
			});

			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "TargetDate",
				SqlSelectName = "TargetDate",
				HeaderText = "Task Target",
				DataFormatString = "{0:d}",
				DataType = typeof(DateTime),
				Width = 80,
				Searchable = true,
				SearchControlID = "DatePicker",
				SearchType = SearchType.DatePicker,
				TextAlign = TextAlign.Center
			});

			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "ProjectTargetDate",
				SqlSelectName = "Project.TargetDate",
				HeaderText = "Project Target",
				DataFormatString = "{0:d}",
				DataType = typeof(DateTime),
				Width = 100,
				Searchable = true,
				SearchControlID = "DatePicker",
				SearchType = SearchType.DatePicker,
				TextAlign = TextAlign.Center
			});

			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "EstCompleteDate",
				HeaderText = "Est Complete",
				DataFormatString = "{0:d}",
				DataType = typeof(DateTime),
				Width = 80,
				Searchable = true,
				SearchControlID = "DatePicker",
				SearchType = SearchType.DatePicker,
				TextAlign = TextAlign.Center
			});

			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "EstMathCompletionDate",
				HeaderText = "EMCD",
				DataFormatString = "{0:d}",
				DataType = typeof(DateTime),
				Width = 100,
				Searchable = true,
				SearchControlID = "DatePicker",
				SearchType = SearchType.DatePicker,
				TextAlign = TextAlign.Center
			});

			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "CompleteDate",
				SqlSelectName = "CompleteDate",
				HeaderText = "Complete Date",
				DataFormatString = "{0:d}",
				DataType = typeof(DateTime),
				Width = 90,
				Searchable = true,
				SearchControlID = "DatePicker",
				SearchType = SearchType.DatePicker,
				TextAlign = TextAlign.Center
			});

			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "OrigEstHours",
				SqlSelectName = "OrigEstHours",
				HeaderText = "Orig Hours",
				DataType = typeof(decimal),
				Width = 70,
				Searchable = true,
				TextAlign = TextAlign.Center
			});

			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "EstimateHours",
				SqlSelectName = "EstimateHours",
				HeaderText = "Est Hours",
				DataType = typeof(decimal),
				Width = 70,
				Searchable = true,
				TextAlign = TextAlign.Center
			});

			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "ActualHours",
				SqlSelectName = "ActualHours",
				HeaderText = "Act Hours",
				DataType = typeof(decimal),
				Width = 80,
				Searchable = true,
				Sortable = false,
				TextAlign = TextAlign.Center
			});

			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "OtherEngTasks",
				SqlSelectName = "OtherEngTasks",
				HeaderText = "Other Eng Tasks",
				Editable = false,
				Visible = false
			});

			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "JurisdictionName",
				HeaderText = "Main Jurisdiction",
				DataType = typeof(string),
				Searchable = true,
				Width = 150,
				Visible = false
			});
			allColumns.Add(new JQGridColumnDynamic
			{
				DataField = "QADeliveryDate",
				SqlSelectName = "QADeliveryDate",
				HeaderText = "QA Delivery",
				DataFormatString = "{0:d}",
				DataType = typeof(DateTime),
				Width = 80,
				Searchable = true,
				SearchControlID = "DatePicker",
				SearchType = SearchType.DatePicker,
				TextAlign = TextAlign.Center
			});

			this.AllColumns = allColumns;
		}
	}

	#region Custom Task Grids

	public class QuickDetailTaskGrid : TasksGrid
	{
		public QuickDetailTaskGrid()
		{
			this.Grid.ID = "QuickDetailTaskGrid";
			this.GridBaseID = this.Grid.ID;
			this.Grid.DataUrl = new UrlHelper(((MvcHandler)HttpContext.Current.Handler).RequestContext).Action("QuickDetailTasks", "GridsAjax", new { Area = "" });

			// remove columns not used in this grid
			this.AllColumns.Remove(this.AllColumns.Find(x => x.DataField == "ProjectName"));
			this.AllColumns.Remove(this.AllColumns.Find(x => x.DataField == "GameName"));
			this.AllColumns.Remove(this.AllColumns.Find(x => x.DataField == "IdNumber"));
			this.AllColumns.Remove(this.AllColumns.Find(x => x.DataField == "Manager"));
			this.AllColumns.Remove(this.AllColumns.Find(x => x.DataField == "ProjectStatus"));
			this.AllColumns.Remove(this.AllColumns.Find(x => x.DataField == "ProjectTargetDate"));
			this.AllColumns.Remove(this.AllColumns.Find(x => x.DataField == "EstMathCompletionDate"));
			this.AllColumns.Remove(this.AllColumns.Find(x => x.DataField == "Description"));


			//hiding
			this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.DataField == "Department")].Visible = false;
			this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.DataField == "DynamicsID")].Visible = false;
			this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.DataField == "AssignDate")].Visible = false;
			this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.DataField == "TargetDate")].Visible = false;
			this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.DataField == "EstCompleteDate")].Visible = false;
			this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.DataField == "BillType")].Visible = true;
			this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.DataField == "IsAssigned")].Visible = true;

			this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.DataField == "Name")].Width = 120;
			this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.DataField == "User")].Width = 90;

			this.Grid.ShrinkToFit = false;
			this.Grid.AutoWidth = true;
			this.Grid.AppearanceSettings = new AppearanceSettings
			{
				AlternateRowBackground = true,
				HighlightRowsOnHover = true,
				ShowFooter = false,
				Caption = "Tasks"
			};
			this.Grid.ToolBarSettings = new ToolBarSettings
			{
				ShowRefreshButton = false,
				ToolBarPosition = ToolBarPosition.Top,
				ShowSearchToolBar = false
			};
			this.Grid.Columns = this.AllColumns.Cast<JQGridColumn>().ToList();
		}
	}

	public class ProjectQATaskGrid : TasksGrid
	{
		public ProjectQATaskGrid()
		{
			this.Grid.ID = "ProjectQATaskGrid";
			this.Grid.AppearanceSettings = new AppearanceSettings
			{
				AlternateRowBackground = true,
				HighlightRowsOnHover = true,
				ShowFooter = false,
				Caption = "QA Tasks"
			};
			this.GridBaseID = this.Grid.ID;
			this.Grid.DataUrl = new UrlHelper(((MvcHandler)HttpContext.Current.Handler).RequestContext).Action("LoadProjectTaskQAGrid", "GridsAjax", new { Area = "" });

			// remove columns not used in this grid
			this.AllColumns.Remove(this.AllColumns.Find(x => x.DataField == "ProjectName"));
			this.AllColumns.Remove(this.AllColumns.Find(x => x.DataField == "GameName"));
			this.AllColumns.Remove(this.AllColumns.Find(x => x.DataField == "IdNumber"));
			this.AllColumns.Remove(this.AllColumns.Find(x => x.DataField == "Manager"));
			this.AllColumns.Remove(this.AllColumns.Find(x => x.DataField == "ProjectStatus"));
			this.AllColumns.Remove(this.AllColumns.Find(x => x.DataField == "ProjectTargetDate"));
			this.AllColumns.Remove(this.AllColumns.Find(x => x.DataField == "EstMathCompletionDate"));
			this.AllColumns.Remove(this.AllColumns.Find(x => x.DataField == "AssignedBy"));
			this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.DataField == "Name")].Visible = false;
			this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.DataField == "DynamicsID")].Visible = false;
			this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.DataField == "EstMathCompletionDate")].Visible = false;
			this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.DataField == "ActualHours")].Visible = false;
			this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.DataField == "OrigEstHours")].Visible = false;
			this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.DataField == "EstimateHours")].Visible = false;
			this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.DataField == "EstCompleteDate")].Visible = false;
			this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.DataField == "Department")].Visible = false;

			this.Grid.Columns = this.AllColumns.Cast<JQGridColumn>().ToList();
		}
	}


	public class ProjectTaskGrid : TasksGrid
	{
		public ProjectTaskGrid()
		{
			this.Grid.ID = "ProjectTaskGrid";
			this.GridBaseID = this.Grid.ID;
			this.Grid.DataUrl = new UrlHelper(((MvcHandler)HttpContext.Current.Handler).RequestContext).Action("LoadProjectTaskWithActualHoursGrid", "GridsAjax", new { Area = "" });

			// remove columns not used in this grid
			this.AllColumns.Remove(this.AllColumns.Find(x => x.DataField == "ProjectName"));
			this.AllColumns.Remove(this.AllColumns.Find(x => x.DataField == "GameName"));
			this.AllColumns.Remove(this.AllColumns.Find(x => x.DataField == "IdNumber"));
			this.AllColumns.Remove(this.AllColumns.Find(x => x.DataField == "Manager"));
			this.AllColumns.Remove(this.AllColumns.Find(x => x.DataField == "ProjectStatus"));
			this.AllColumns.Remove(this.AllColumns.Find(x => x.DataField == "ProjectTargetDate"));
			this.AllColumns.Remove(this.AllColumns.Find(x => x.DataField == "EstMathCompletionDate"));
			this.AllColumns.Remove(this.AllColumns.Find(x => x.DataField == "AssignedBy"));
			this.Grid.Columns = this.AllColumns.Cast<JQGridColumn>().ToList();
		}
	}

	public class ProjectTaskTransferGrid : TasksGrid
	{
		public ProjectTaskTransferGrid()
		{
			this.Grid.ID = "ProjectTaskTransferGrid";
			this.GridBaseID = this.Grid.ID;
			this.Grid.DataUrl = new UrlHelper(((MvcHandler)HttpContext.Current.Handler).RequestContext).Action("LoadProjectTransferTaskWithActualHoursGrid", "GridsAjax", new { Area = "" });
			this.Grid.AppearanceSettings.Caption = "Transfer Tasks";
			this.Grid.ClientSideEvents.BeforeAjaxRequest = "projectTransferTaskGridBeforeAjaxRequest";

			// remove columns not used in this grid
			this.AllColumns.Remove(this.AllColumns.Find(x => x.DataField == "GameName"));
			this.AllColumns.Remove(this.AllColumns.Find(x => x.DataField == "IdNumber"));
			this.AllColumns.Remove(this.AllColumns.Find(x => x.DataField == "Manager"));
			this.AllColumns.Remove(this.AllColumns.Find(x => x.DataField == "ProjectStatus"));
			this.AllColumns.Remove(this.AllColumns.Find(x => x.DataField == "ProjectTargetDate"));
			this.AllColumns.Remove(this.AllColumns.Find(x => x.DataField == "EstMathCompletionDate"));
			this.AllColumns.Find(x => x.DataField == "ProjectName").HeaderText = "Transfer Name";
			this.Grid.Columns = this.AllColumns.Cast<JQGridColumn>().ToList();
		}
	}

	public class ProjectTaskForeignGrid : TasksGrid
	{
		public ProjectTaskForeignGrid()
		{
			this.Grid.ID = "ProjectTaskForeignGrid";
			this.GridBaseID = this.Grid.ID;
			this.Grid.DataUrl = new UrlHelper(((MvcHandler)HttpContext.Current.Handler).RequestContext).Action("LoadProjectForeignTaskWithActualHoursGrid", "GridsAjax", new { Area = "" });
			this.Grid.AppearanceSettings.Caption = "Merged Project Tasks";
			this.Grid.ClientSideEvents.BeforeAjaxRequest = "projectForeignTaskGridBeforeAjaxRequest";

			// remove columns not used in this grid
			this.AllColumns.Remove(this.AllColumns.Find(x => x.DataField == "GameName"));
			this.AllColumns.Remove(this.AllColumns.Find(x => x.DataField == "IdNumber"));
			this.AllColumns.Remove(this.AllColumns.Find(x => x.DataField == "ProjectStatus"));
			this.AllColumns.Remove(this.AllColumns.Find(x => x.DataField == "Manager"));
			this.AllColumns.Remove(this.AllColumns.Find(x => x.DataField == "Department"));
			this.AllColumns.Remove(this.AllColumns.Find(x => x.DataField == "ProjectTargetDate"));
			this.AllColumns.Remove(this.AllColumns.Find(x => x.DataField == "EstMathCompletionDate"));
			this.AllColumns.Find(x => x.DataField == "ProjectName").HeaderText = "Merged Project";
			this.Grid.Columns = this.AllColumns.Cast<JQGridColumn>().ToList();
		}
	}

	public class MathTasksStatusGrid : TasksGrid
	{
		public MathTasksStatusGrid()
		{
			this.Grid.ID = "MathTasksStatusGrid";
			this.GridBaseID = this.Grid.ID;
			this.Grid.AutoWidth = true;
			this.Grid.ShrinkToFit = false;
			this.Grid.AppearanceSettings.ShowFooter = false;
			this.Grid.AppearanceSettings.Caption = "";

			// remove columns not used in this grid
			this.AllColumns.Find(x => x.DataField == "ProjectName").Formatter = new CustomFormatter { FormatFunction = "detailLink" };
			this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.DataField == "Department")].Visible = false;
			this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.DataField == "DynamicsID")].Visible = false;
			this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.DataField == "ActualHours")].Visible = false;

		}
	}

	public class EngTaskGrid : TasksGrid
	{
		public EngTaskGrid()
		{
			//this.Grid.ID = "EngTaskGrid";
			//this.Grid.DataUrl = new UrlHelper(((MvcHandler)HttpContext.Current.Handler).RequestContext).Action("LoadEngTasksGrid", "GridsAjax", new { Area = "" });
			this.Grid.AutoWidth = false;
			this.Grid.ShrinkToFit = true;
			this.Grid.MultiSelect = true;
			this.Grid.AppearanceSettings.ShowFooter = false;
			this.Grid.AppearanceSettings.Caption = "";
			this.Grid.SortSettings = new SortSettings { AutoSortByPrimaryKey = false };

			this.AllColumns.Find(x => x.DataField == "ProjectName").Formatter = new CustomFormatter { FormatFunction = "detailLink" };
			this.AllColumns.Find(x => x.DataField == "Name").Formatter = new CustomFormatter { FormatFunction = "taskNameLink" };
			this.AllColumns.Find(x => x.DataField == "EngTaskOrder").Formatter = new CustomFormatter { FormatFunction = "showGroup" };
			this.AllColumns.Find(x => x.DataField == "OtherEngTasks").Formatter = new CustomFormatter { FormatFunction = "showOtherTasks" };
			this.AllColumns.Find(x => x.HeaderText == "Start/Complete").Formatter = new CustomFormatter { FormatFunction = "showAction" };
			this.AllColumns.Find(x => x.DataField == "ProjectStatus").Formatter = new CustomFormatter { FormatFunction = "showProjectStatus" };

			this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.HeaderText == "Start/Complete")].Visible = true;
			this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.DataField == "OtherEngTasks")].Visible = true;
			this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.DataField == "Manager")].Visible = true;
			this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.DataField == "DynamicsID")].Visible = true;
			this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.DataField == "EstMathCompletionDate")].Visible = false;
			this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.DataField == "OrigEstHours")].Visible = false;
			this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.DataField == "Department")].Visible = false;
			this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.DataField == "User")].Visible = false;
			this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.DataField == "CompleteDate")].Visible = false;

			//this.Grid.Columns.Insert(this.Grid.Columns.FindIndex(x => x.DataField == "Description"), new JQGridColumn
			//{
			//	DataField = "",
			//	HeaderText = "Start/Complete",
			//	DataType = typeof(string),
			//	Searchable = false,
			//	Formatter = new CustomFormatter { FormatFunction = "showAction" },
			//	Sortable = false,
			//	Width = 100,
			//	TextAlign = TextAlign.Left
			//});

			//this.Grid.Columns.Insert(this.Grid.Columns.Count, new JQGridColumn
			//{
			//	DataField = "OtherTasks",
			//	HeaderText = "Other Project Tasks",
			//	DataType = typeof(string),
			//	Searchable = true,
			//	Formatter = new CustomFormatter { FormatFunction = "showOtherTasks" },
			//	Sortable = false,
			//	Width = 250,
			//	TextAlign = TextAlign.Left
			//});
		}
	}

	public class QATaskGrid : TasksGrid
	{
		public QATaskGrid()
		{
			//this.Grid.ID = "QATaskGrid";
			this.Grid.DataUrl = new UrlHelper(((MvcHandler)HttpContext.Current.Handler).RequestContext).Action("LoadBundleTasksGrid", "GridsAjax", new { Area = "" });
			this.Grid.AutoWidth = false;
			this.Grid.ShrinkToFit = false;
			this.Grid.AppearanceSettings.ShowFooter = false;
			this.Grid.AppearanceSettings.Caption = "";
			this.Grid.SortSettings = new SortSettings { AutoSortByPrimaryKey = false };

			this.AllColumns.Find(x => x.DataField == "ProjectName").Formatter = new CustomFormatter { FormatFunction = "detailLink" };
			this.AllColumns.Find(x => x.DataField == "ProjectName").Width = 170;
			this.AllColumns.Find(x => x.DataField == "QATaskOrder").Formatter = new CustomFormatter { FormatFunction = "showQAGroup" };
			this.AllColumns.Find(x => x.DataField == "ProjectTargetDate").Formatter = new CustomFormatter { FormatFunction = "targetDateIndicator" };

			this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.DataField == "ProjectIndicator")].Visible = false;
			this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.DataField == "Manager")].Visible = false;
			this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.DataField == "ProjectStatus")].Visible = false;
			this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.DataField == "ProjectHolder")].Visible = true;
			this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.DataField == "DynamicsID")].Visible = false;
			this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.DataField == "EstMathCompletionDate")].Visible = false;
			this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.DataField == "ActualHours")].Visible = false;
			this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.DataField == "OrigEstHours")].Visible = false;
			this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.DataField == "EstimateHours")].Visible = false;
			this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.DataField == "EstCompleteDate")].Visible = false;
			this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.DataField == "Department")].Visible = false;
			this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.DataField == "User")].Visible = false;
			this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.DataField == "Description")].Visible = false;
			this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.DataField == "CompleteDate")].Visible = true;
			this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.DataField == "IdNumber")].Visible = false;
			this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.DataField == "BundleStatus")].Visible = true;
			this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.DataField == "QAAccepted")].Visible = true;
			this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.DataField == "ProjectJurCount")].Visible = true;
			this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.DataField == "JiraOpen")].Visible = true;
			this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.DataField == "AssignedBy")].Visible = true;
			this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.HeaderText == "Start/Complete")].Visible = true;
			this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.DataField == "JurisdictionName")].Visible = true;
			this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.HeaderText == "Start/Complete")].Formatter = new CustomFormatter { FormatFunction = "showQAAction" };
			this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.DataField == "QADeliveryDate")].Visible = false;

			//this.Grid.Columns.Insert(this.Grid.Columns.Count, new JQGridColumn
			//{
			//	DataField = "Jurisdiction",
			//	HeaderText = "Main Jurisdiction",
			//	DataType = typeof(string),
			//	Searchable = true,
			//	Width = 150,
			//	Visible = true
			//});

			//         this.Grid.Columns.Insert(this.Grid.Columns.Count, new JQGridColumn
			//{
			//	DataField = "",
			//	HeaderText = "Start/Complete",
			//	DataType = typeof(string),
			//	Searchable = false,
			//	Formatter = new CustomFormatter { FormatFunction = "showQAAction" },
			//	Sortable = false,
			//	Width = 100,
			//	TextAlign = TextAlign.Left
			//});

		}

	}
	#endregion

	public class TasksGridRecord
	{
		public int Id { get; set; }
		public int? TaskDefinitionId { get; set; }
		public int ProjectId { get; set; }
		public bool IsBundleResubmission { get; set; } //This can be removed when ADD/Cor as Transfer with bundletype is udpated on prod
		public int? BundleId { get; set; }
		public bool? IsRush { get; set; }
		public string ProjectName { get; set; }
		public int? UserId { get; set; }
		public string User { get; set; }
		public string FName { get; set; }
		public string LName { get; set; }
		public string Name { get; set; }
		public string Description { get; set; }
		public string Department { get; set; }
		public DateTime? AssignDate { get; set; }
		public DateTime? StartDate { get; set; }
		public DateTime? TargetDate { get; set; }
		public DateTime? CompleteDate { get; set; }
		public decimal? EstimateHours { get; set; }
		public decimal? OrigEstHours { get; set; }
		public decimal? ActualHours { get; set; }
		public string DynamicsID { get; set; }
		public DateTime? EstMathCompletionDate { get; set; }
		public DateTime? EstCompleteDate { get; set; }
		public DateTime? ProjectTargetDate { get; set; }
		public string ProjectStatus { get; set; }
		public string GameName { get; set; }
		public string IdNumber { get; set; }
		public DateTime? NoteDate { get; set; }
		public string Manager { get; set; }
		public string QAReviewer { get; set; }
		public string MathSupervisor { get; set; }
		public string Status { get; set; }
		public string ProjectHolder { get; set; }

		public string BundleStatus { get; set; }
		public string ProjectIndicator { get; set; }
		public int? ProjectTargetDiff { get; set; }
		public bool? IsFinalizationTask { get; set; }
		public string BundleType { get; set; }
		public string QATaskStatus { get; set; }
		public bool ENInvolvementWhileFL { get; set; }

		//needs to be string in order show proper group order
		public string EngTaskOrder { get; set; }
		public string QATaskOrder { get; set; }

		public string IDVersion { get; set; }
		public string AssociatedJur { get; set; }
		public string AssociatedComponent { get; set; }
		public string OtherEngTasks { get; set; }

		public string BillType { get; set; }
		public string IsAssigned { get; set; }
		public DateTime? QADeliveryDate { get; set; }

		//QA Tasks
		public DateTime? QAAccepted { get; set; }
		public string AssignedBy { get; set; }
		public string JiraOpen { get; set; }
		public int? ProjectJurCount { get; set; }

		public bool? IsDeleted { get; set; }
		public string JurisdictionId { get; set; }
		public string JurisdictionName { get; set; }
	}
}