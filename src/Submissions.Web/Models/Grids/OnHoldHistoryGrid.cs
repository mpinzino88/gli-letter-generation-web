﻿using Submissions.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;

namespace Submissions.Web.Models.Grids
{
	using Trirand.Web.Mvc;

	public class OnHoldHistoryGrid
	{
		public OnHoldHistoryGrid()
		{
			this.Grid = new JQGrid
			{
				ID = "OHHistoryGrid",
				Columns = LoadColumns(),
				DataUrl = new UrlHelper(((MvcHandler)HttpContext.Current.Handler).RequestContext).Action("LoadOHHistoryGrid", "GridsAjax", new { Area = "" }),
				AutoWidth = true,
				Width = Unit.Percentage(100),
				Height = Unit.Percentage(100),
				ToolBarSettings = new ToolBarSettings
				{
					ShowRefreshButton = true,
					ToolBarPosition = ToolBarPosition.Top,
					ShowSearchToolBar = true,
				},
				PagerSettings = new PagerSettings
				{
					PageSize = 100,
					PageSizeOptions = "[]"
				},
				AppearanceSettings = new AppearanceSettings
				{
					AlternateRowBackground = true,
					HighlightRowsOnHover = true,
					ShowFooter = true,
					Caption = "Notes"
				},
				SortSettings = new SortSettings
				{
					InitialSortColumn = "StartDate",
					InitialSortDirection = SortDirection.Desc
				},
				ClientSideEvents = new ClientSideEvents
				{
					BeforeAjaxRequest = "ohHistoryGridBeforeAjaxRequest"
				},
				RenderingMode = RenderingMode.Optimized
			};
		}

		public JQGrid Grid { get; set; }

		private List<JQGridColumn> LoadColumns()
		{
			var columns = new List<JQGridColumn>();

			columns.Add(new JQGridColumn
			{
				DataField = "Id",
				PrimaryKey = true,
				Editable = false,
				Visible = false
			});

			columns.Add(new JQGridColumn
			{
				DataField = "IsBackDated",
				Editable = false,
				Visible = false
			});


			columns.Add(new JQGridColumn
			{
				DataField = "StartDate",
				HeaderText = "Start Date",
				DataType = typeof(DateTime),
				DataFormatString = "{0:d}",
				Width = 80,
				Searchable = false,
			});
						
			columns.Add(new JQGridColumn
			{
				DataField = "EndDate",
				HeaderText = "End Date",
				DataType = typeof(DateTime),
				DataFormatString = "{0:d}",
				Width = 80,
				Searchable = false,
			});

			columns.Add(new JQGridColumn
			{
				DataField = "Reason",
				HeaderText = "Reason",
				DataType = typeof(string),
				Width = 150,
				Searchable = false,
				
			});

			columns.Add(new JQGridColumn
			{
				DataField = "ShortDescription",
				HeaderText = "Short Desc",
				DataType = typeof(string),
				Width = 200,
				Searchable = false,
			});

			return columns;
		}
	}

	public class QuickOHHistoryGrid : OnHoldHistoryGrid
	{
		public QuickOHHistoryGrid()
		{
			this.Grid.ID = "QuickDetailOHHistory";

			this.Grid.ShrinkToFit = false;
			this.Grid.AutoWidth = true;

			this.Grid.AppearanceSettings = new AppearanceSettings
			{
				AlternateRowBackground = true,
				HighlightRowsOnHover = true,
				ShowFooter = false,
				Caption = "OH Notes"
			};
			this.Grid.ToolBarSettings = new ToolBarSettings
			{
				ShowRefreshButton = false,
				ToolBarPosition = ToolBarPosition.Top,
				ShowSearchToolBar = false
			};
		}
	}

	public class OnHoldHistoryGridRecord
	{
		public int Id { get; set; }
		public DateTime? StartDate { get; set; }
		public DateTime? EndDate { get; set; }
		public string Reason { get; set; }
		public string ShortDescription { get; set; }
		public Boolean IsBackDated { get; set; }
	}
}