﻿
using Submissions.Common.JQGrid;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;


namespace Submissions.Web.Models.Grids
{
	using Trirand.Web.Mvc;
	public class MathReviewGrid
	{
		private const string gridId = "MathReviewGrid";
		public MathReviewGrid()
		{
			this.AllColumns = GetAllColumns();
			this.Grid = new JQGrid
			{
				ID = gridId,
				Columns = this.AllColumns.Cast<JQGridColumn>().ToList(),
				DataUrl = new UrlHelper(((MvcHandler)HttpContext.Current.Handler).RequestContext).Action("LoadMathReviewGrid", "GridsAjax", new { Area = "" }),
				AutoWidth = true,
				ShrinkToFit = true,
				Width = Unit.Percentage(100),
				Height = Unit.Percentage(100),
				ToolBarSettings = new ToolBarSettings
				{
					ShowRefreshButton = true,
					ToolBarPosition = ToolBarPosition.Top,
					ShowSearchToolBar = true
				},
				PagerSettings = new PagerSettings
				{
					PageSize = 100,
					PageSizeOptions = "[]"
				},
				AppearanceSettings = new AppearanceSettings
				{
					AlternateRowBackground = true,
					HighlightRowsOnHover = true,
					ShowFooter = false
				},
				SortSettings = new SortSettings
				{
					InitialSortColumn = "ProjectName",
					InitialSortDirection = SortDirection.Asc
				},
				ClientSideEvents = new ClientSideEvents
				{
					BeforeAjaxRequest = "gridBeforeAjaxRequest"
				},
				RenderingMode = RenderingMode.Optimized,
				MultiSelect = true
			};

		}

		public JQGrid Grid { get; set; }
		public List<JQGridColumnDynamic> AllColumns { private set; get; }
		public List<JQGridColumnDynamic> GetAllColumns()
		{
			var Columns = new List<JQGridColumnDynamic>
				{
					new JQGridColumnDynamic
					{
						DataField = "Id",
						HeaderText = "Id",
						PrimaryKey = true,
						Editable = false,
						Visible = false
					},
					new JQGridColumnDynamic
					{
						DataField = "ReviewType",
						HeaderText = "ReviewType",
						Editable = false,
						Visible = false
					},
					new JQGridColumnDynamic
					{
						DataField = "IsRush",
						HeaderText = "IsRush",
						DataType = typeof(bool),
						Visible = false
					},
					new JQGridColumnDynamic
					{
						DataField = "OnHold",
						HeaderText = "OnHold",
						DataType = typeof(bool),
						Visible = false
					},
					new JQGridColumnDynamic
					{
						DataField = "ProjectId",
						HeaderText = "ProjectID",
						DataType = typeof(int),
						Visible = false
					},
					new JQGridColumnDynamic
					{
						DataField = "ReviewStatus",
						HeaderText = "ReviewStatus",
						DataType = typeof(string),
						Visible = false
					},
					new JQGridColumnDynamic
					{
						DataField = "IsMathRush",
						HeaderText = "Math Rush",
						DataType = typeof(string),
						Visible = false
					},
					new JQGridColumnDynamic
					{
						DataField = "ProjectName",
						HeaderText = "File Number",
						Formatter = new CustomFormatter{FormatFunction = "detailLink"},
						DataType = typeof(string),
						Searchable = true,
						SearchToolBarOperation = SearchOperation.BeginsWith,
						Width = 140
					},
					new JQGridColumnDynamic
					{
						DataField = "DateSubmitted",
						HeaderText = "Received",
						DataFormatString = "{0:d}",
						DataType = typeof(DateTime),
						Width = 80

					},
					new JQGridColumnDynamic
					{
						DataField = "DateRequested",
						HeaderText = "Submitted",
						DataFormatString = "{0:d}",
						DataType = typeof(DateTime),
						Width = 90

					},
					new JQGridColumnDynamic
					{
						DataField = "CompleteDateRequested",
						HeaderText = "Requested",
						DataFormatString = "{0:d}",
						DataType = typeof(DateTime),
						Width = 90

					},
					new JQGridColumnDynamic
					{
						DataField = "EstimateCompleteDate",
						HeaderText = "Accepted Delivery",
						DataFormatString = "{0:d}",
						DataType = typeof(DateTime),
						Width = 90
					},
					new JQGridColumnDynamic
					{
						DataField = "ProgramType",
						HeaderText = "File Type",
						DataType = typeof(string),
						Searchable = true,
						SearchToolBarOperation = SearchOperation.BeginsWith,
						Width = 90
					},
					new JQGridColumnDynamic
					{
						DataField = "Type",
						HeaderText = "Game Type",
						DataType = typeof(string),
						Searchable = true,
						SearchToolBarOperation = SearchOperation.BeginsWith,
						Width = 80
					},
					new JQGridColumnDynamic
					{
						DataField = "ReviewType",
						HeaderText = "Type",
						DataType = typeof(string),
						Formatter = new CustomFormatter{FormatFunction = "typeLink"},
						Searchable = true,
						SearchToolBarOperation = SearchOperation.BeginsWith,
						Width = 80
					},
					new JQGridColumnDynamic
					{
						DataField = "Theme",
						HeaderText = "Theme",
						DataType = typeof(string),
						Searchable = true,
						SearchToolBarOperation = SearchOperation.BeginsWith,
						Width = 150
					},
					new JQGridColumnDynamic
					{
						DataField = "Location",
						HeaderText = "Location",
						DataType = typeof(string),
						Width = 60
					},
					new JQGridColumnDynamic
					{
						DataField = "Status",
						HeaderText = "Status",
						DataType = typeof(string),
						Width = 50
					},
					new JQGridColumnDynamic
					{
						DataField = "ProtrackTime",
						HeaderText = "Scoped Hours",
						Formatter = new CustomFormatter{FormatFunction = "viewHours"},
						DataType = typeof(string),
						Width = 90
					},
					new JQGridColumnDynamic
					{
						DataField = "ReviewTaskTime",
						HeaderText = "Task Time",
						DataType = typeof(string),
						Visible = false
					},
					new JQGridColumnDynamic
					{
						HeaderText = "View",
						Formatter = new CustomFormatter{FormatFunction = "viewLink"},
						Searchable = false,
						Sortable = false,
						Width = 50,
						CssClass = "padright"
					},
					new JQGridColumnDynamic
					{
						HeaderText = "Task",
						Formatter = new CustomFormatter{FormatFunction = "assignLink"},
						Searchable = false,
						Sortable = false,
						Width = 60,
						CssClass = "padright"
					},
					new JQGridColumnDynamic
					{
						DataField = "TargetDate",
						HeaderText = "Target",
						DataFormatString = "{0:d}",
						DataType = typeof(DateTime),
						Width = 80
					},

					new JQGridColumnDynamic
					{
						DataField = "CompletedBy",
						HeaderText = "Completed By",
						DataType = typeof(String),
						Width = 100,
						Visible = false
					},

					new JQGridColumnDynamic
					{
						DataField = "CompleteDate",
						HeaderText = "Completed",
						DataFormatString = "{0:d}",
						DataType = typeof(DateTime),
						Width = 100,
						Visible = false
					},
					new JQGridColumnDynamic
					{
						DataField = "IsPeerReview",
						HeaderText = "Peer Review",
						DataType = typeof(string),
						Visible = false
					},

					//new JQGridColumnDynamic
					//{
					//	HeaderText = "Assign To",
					//	Formatter = new CustomFormatter{FormatFunction = "assignTaskformatter"},
					//	Searchable = false,
					//	Sortable = false,
					//	Width = 190,
					//	CssClass = "padright"
					//},
					//new JQGridColumnDynamic
					//{
					//	HeaderText = "Task Target",
					//	Formatter = new CustomFormatter{FormatFunction = "TargetTaskformatter"},
					//	Searchable = false,
					//	Sortable = false,
					//	Width = 120,
					//	CssClass = "padright"
					//},
					//new JQGridColumnDynamic
					//{
					//	HeaderText = "Est Hrs",
					//	Formatter = new CustomFormatter{FormatFunction = "estHrsTaskformatter"},
					//	Searchable = false,
					//	Sortable = false,
					//	Width = 100,
					//	CssClass = "padright"
					//}
			
				};
			return Columns;
		}

	}

	public class MathReviewGridDN : MathReviewGrid
	{
		public MathReviewGridDN()
		{
			this.Grid.SortSettings = new SortSettings
			{
				InitialSortColumn = "CompleteDate",
				InitialSortDirection = SortDirection.Desc
			};
			this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.HeaderText == "Task")].Visible = false;
			this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.HeaderText == "Task")].Formatter = null;
			this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.DataField == "CompletedBy")].Visible = true;
			this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.DataField == "CompleteDate")].Visible = true;
		}
	}

	public class MathReviewGridRecord
	{
		public int Id { get; set; }
		public int ProjectId { get; set; }
		public string ReviewType { get; set; }
		public string ProjectName { get; set; }
		public string Theme { get; set; }
		public string Type { get; set; }
		public DateTime? DateSubmitted { get; set; }
		public DateTime? DateRequested { get; set; }
		public DateTime? CompleteDateRequested { get; set; }
		public string Location { get; set; }
		public bool IsRush { get; set; }
		public bool? IsMathRush { get; set; }
		public bool OnHold { get; set; }
		public string Status { get; set; }
		public string ReviewStatus { get; set; }
		public decimal? ProtrackTime { get; set; }
		public decimal? ReviewTaskTime { get; set; }
		public DateTime? TargetDate { get; set; }
		public string ProgramType { get; set; }
		public string CompletedBy { get; set; }
		public DateTime? CompleteDate { get; set; }
		public bool IsPeerReview { get; set; }
		public DateTime? EstimateCompleteDate { get; set; }
	}
}