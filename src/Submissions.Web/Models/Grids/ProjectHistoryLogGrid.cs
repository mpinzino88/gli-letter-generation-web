﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;

namespace Submissions.Web.Models.Grids
{
	using Trirand.Web.Mvc;

	public class ProjectHistoryLogGrid
	{
		public ProjectHistoryLogGrid()
		{
			this.Grid = new JQGrid
			{
				ID = "ProjectHistoryLogGrid",
				Columns = LoadColumns(),
				DataUrl = new UrlHelper(((MvcHandler)HttpContext.Current.Handler).RequestContext).Action("LoadProjectHistoryLogGrid", "GridsAjax", new { Area = "" }),
				AutoWidth = true,
				Width = Unit.Percentage(100),
				Height = Unit.Percentage(100),
				ToolBarSettings = new ToolBarSettings
				{
					ShowRefreshButton = true,
					ToolBarPosition = ToolBarPosition.Top,
					ShowSearchToolBar = false
				},
				PagerSettings = new PagerSettings
				{
					PageSize = 100,
					PageSizeOptions = "[]"
				},
				AppearanceSettings = new AppearanceSettings
				{
					AlternateRowBackground = true,
					HighlightRowsOnHover = true,
					ShowFooter = false,
					Caption = "History"
				},
				SortSettings = new SortSettings
				{
					InitialSortColumn = "AddDate",
					InitialSortDirection = SortDirection.Desc
				},
				ClientSideEvents = new ClientSideEvents
				{
					BeforeAjaxRequest = "projectHistoryLogGridBeforeAjaxRequest"
				},
				RenderingMode = RenderingMode.Optimized
			};
		}

		public JQGrid Grid { get; set; }

		private List<JQGridColumn> LoadColumns()
		{
			var columns = new List<JQGridColumn>();

			columns.Add(new JQGridColumn
			{
				DataField = "AddDate",
				HeaderText = "TimeStamp",
				DataType = typeof(DateTime),
				Width = 125
			});

			columns.Add(new JQGridColumn
			{
				DataField = "Employee",
				HeaderText = "User",
				DataType = typeof(string),
				Width = 130
			});

			columns.Add(new JQGridColumn
			{
				DataField = "Location",
				HeaderText = "Location",
				DataType = typeof(string),
				Width = 50
			});

			columns.Add(new JQGridColumn
			{
				DataField = "ProjectHolder",
				HeaderText = "Holder",
				DataType = typeof(string),
				Width = 50
			});

			columns.Add(new JQGridColumn
			{
				DataField = "ProjectStatus",
				HeaderText = "Eng Status",
				DataType = typeof(string),
				Width = 60
			});

			columns.Add(new JQGridColumn
			{
				DataField = "BundleStatus",
				HeaderText = "QA Status",
				DataType = typeof(string),
				Width = 60
			});

			columns.Add(new JQGridColumn
			{
				DataField = "Notes",
				HeaderText = "Event",
				DataType = typeof(string),
				Width = 500
			});

			return columns;
		}
	}

	public class ProjectHistoryLogGridRecord
	{
		public int Id { get; set; }
		public DateTime? AddDate { get; set; }
		public string Employee { get; set; }
		public string Location { get; set; }
		public string ProjectHolder { get; set; }
		public string ProjectStatus { get; set; }
		public string BundleStatus { get; set; }
		public string Notes { get; set; }
		public string ForeignProject { get; set; }
		public string Department { get; set;}
	}
		
}