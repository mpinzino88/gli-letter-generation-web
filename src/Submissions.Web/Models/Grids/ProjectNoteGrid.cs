﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;

namespace Submissions.Web.Models.Grids
{
	using Trirand.Web.Mvc;

	public class ProjectNoteGrid
	{
		public ProjectNoteGrid()
		{
			this.Grid = new JQGrid
			{
				ID = "ProjectNoteGrid",
				Columns = LoadColumns(),
				//below only loads eng notes
				DataUrl = new UrlHelper(((MvcHandler)HttpContext.Current.Handler).RequestContext).Action("LoadProjectNoteGrid", "GridsAjax", new { Area = "" }),
				AutoWidth = true,
				Width = Unit.Percentage(100),
				Height = Unit.Percentage(100),
				ToolBarSettings = new ToolBarSettings
				{
					ShowRefreshButton = true,
					ToolBarPosition = ToolBarPosition.Top,
					ShowSearchToolBar = true,
				},
				PagerSettings = new PagerSettings
				{
					PageSize = 100,
					PageSizeOptions = "[]"
				},
				AppearanceSettings = new AppearanceSettings
				{
					AlternateRowBackground = true,
					HighlightRowsOnHover = true,
					ShowFooter = true,
					Caption = "Notes"
				},
				SortSettings = new SortSettings
				{
					InitialSortColumn = "AddDate",
					InitialSortDirection = SortDirection.Desc
				},
				ClientSideEvents = new ClientSideEvents
				{
					BeforeAjaxRequest = "projectNoteGridBeforeAjaxRequest"
				},
				RenderingMode = RenderingMode.Optimized
			};
		}

		public JQGrid Grid { get; set; }

		private List<JQGridColumn> LoadColumns()
		{
			var columns = new List<JQGridColumn>();

			columns.Add(new JQGridColumn
			{
				DataField = "Id",
				PrimaryKey = true,
				Editable = false,
				Visible = false
			});

			columns.Add(new JQGridColumn
			{
				DataField = "UserId",
				HeaderText = "UserId",
				Visible = false
			});

			columns.Add(new JQGridColumn
			{
				DataField = "ForeignProject",
				HeaderText = "ForeignProject",
				Visible = false
			});

			columns.Add(new JQGridColumn
			{
				DataField = "NoteType",
				Visible = false
			});

			columns.Add(new JQGridColumn
			{
				DataField = "AddDate",
				HeaderText = "Date",
				//DataFormatString = "{0:d}",
				DataType = typeof(DateTime),
				Width = 110,
				Searchable = false
			});

			columns.Add(new JQGridColumn
			{
				DataField = "User",
				HeaderText = "User",
				DataType = typeof(string),
				Width = 80,
				Searchable = false
			});

			columns.Add(new JQGridColumn
			{
				DataField = "Department",
				HeaderText = "Department",
				DataType = typeof(string),
				Width = 50
			});

			columns.Add(new JQGridColumn
			{
				DataField = "Scope",
				HeaderText = "Ext",
				DataType = typeof(string),
				Width = 50,
				Searchable = false,
				FooterValue = "*",
				TextAlign = TextAlign.Right,
				Formatter = new CustomFormatter { FormatFunction = "formatScope" },

			});

			columns.Add(new JQGridColumn
			{
				DataField = "Note",
				HeaderText = "Note",
				DataType = typeof(string),
				Width = 500,
				Searchable = false,
				FooterValue = "Note is available to Customers",
				Formatter = new CustomFormatter { FormatFunction = "formatNote" },
			});

			return columns;
		}
	}


	public class ProjectDetailNotesGrid : ProjectNoteGrid
	{
		public ProjectDetailNotesGrid()
		{
			//Need QA Notes
			this.Grid.DataUrl = new UrlHelper(((MvcHandler)HttpContext.Current.Handler).RequestContext).Action("LoadProjectDetailNoteGrid", "GridsAjax", new { Area = "" });

		}
	}

	public class QuickProjectNotesGrid : ProjectNoteGrid
	{
		public QuickProjectNotesGrid()
		{
			//Need QA Notes
			this.Grid.DataUrl = new UrlHelper(((MvcHandler)HttpContext.Current.Handler).RequestContext).Action("LoadProjectDetailNoteGrid", "GridsAjax", new { Area = "" });

			this.Grid.ID = "QuickDetailProjectNotes";
			this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.DataField == "Scope")].Formatter = null;
			this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.DataField == "Scope")].Visible = false;
			//this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.DataField == "Note")].Formatter = null;
			this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.DataField == "Department")].Searchable = false;

			this.Grid.ShrinkToFit = false;
			this.Grid.AutoWidth = true;

			this.Grid.AppearanceSettings = new AppearanceSettings
			{
				AlternateRowBackground = true,
				HighlightRowsOnHover = true,
				ShowFooter = false,
				Caption = "Project Notes"
			};
			this.Grid.ToolBarSettings = new ToolBarSettings
			{
				ShowRefreshButton = false,
				ToolBarPosition = ToolBarPosition.Top,
				ShowSearchToolBar = false
			};
		}
	}

	public class ProjectNoteGridRecord
	{
		public int Id { get; set; }
		public DateTime? AddDate { get; set; }
		public int? UserId { get; set; }
		public string User { get; set; }
		public string Scope { get; set; }
		public string Department { get; set; }
		public string Note { get; set; }
		public string ForeignProject { get; set; }
		public string NoteType { get; set; }
	}
}