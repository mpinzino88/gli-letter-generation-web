﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;

namespace Submissions.Web.Models.Grids
{
	using Trirand.Web.Mvc;

	public class ProjectMetricsGrid
	{
		public JQGrid Grid { get; set; }
		public ProjectMetricsGrid()
		{
			this.Grid = new JQGrid
			{
				ID = "ProjectMetricsGrid",
				Columns = LoadColumns(),
				DataUrl = new UrlHelper(((MvcHandler)HttpContext.Current.Handler).RequestContext).Action("LoadProjectMetricsGrid", "GridsAjax", new { Area = "" }),
				AutoWidth = true,
				Width = Unit.Percentage(100),
				Height = Unit.Percentage(100),
				ToolBarSettings = new ToolBarSettings
				{
					ShowRefreshButton = true,
					ToolBarPosition = ToolBarPosition.Top,
					ShowSearchToolBar = true,
				},
				PagerSettings = new PagerSettings
				{
					PageSize = 100,
					PageSizeOptions = "[]"
				},
				AppearanceSettings = new AppearanceSettings
				{
					AlternateRowBackground = true,
					HighlightRowsOnHover = true,
					ShowFooter = false,
					Caption = "Metrics"
				},
				SortSettings = new SortSettings
				{
					InitialSortColumn = "TimeStamp",
					InitialSortDirection = SortDirection.Asc
				},
				ClientSideEvents = new ClientSideEvents
				{
					BeforeAjaxRequest = "projectMetricsGridBeforeAjaxRequest"
				},
				RenderingMode = RenderingMode.Optimized
			};
		}

		private List<JQGridColumn> LoadColumns()
		{
			var columns = new List<JQGridColumn>();

			columns.Add(new JQGridColumn
			{
				DataField = "Holder",
				HeaderText = "Holder",
				DataType = typeof(string),
				Width = 30,
				Searchable = false
			});

			columns.Add(new JQGridColumn
			{
				DataField = "Status",
				HeaderText = "Status",
				DataType = typeof(string),
				Width = 30,
				Searchable = false
			});

			columns.Add(new JQGridColumn
			{
				DataField = "Event",
				HeaderText = "Event",
				DataType = typeof(string),
				Width = 100,
				Searchable = false
			});

			columns.Add(new JQGridColumn
			{
				DataField = "TimeStamp",
				HeaderText = "Time Stamp",
				DataType = typeof(string),
				Width = 50,
				Searchable = false
			});

			columns.Add(new JQGridColumn
			{
				DataField = "ChangeDate",
				HeaderText = "Change Date",
				DataType = typeof(string),
				Width = 50,
				Searchable = false
			});

			columns.Add(new JQGridColumn
			{
				DataField = "BusinessDays",
				HeaderText = "Business Days",
				DataType = typeof(string),
				Width = 30,
				Searchable = false
			});

			return columns;
		}

	}

	public class ProjectQAMetricsGrid : ProjectMetricsGrid
	{
		public ProjectQAMetricsGrid()
		{
			this.Grid.ID = "ProjectQAMetricsGrid";
			this.Grid.AppearanceSettings = new AppearanceSettings
				{
					AlternateRowBackground = true,
					HighlightRowsOnHover = true,
					ShowFooter = false,
					Caption = "QA Metrics"
				};
		}
	}


	public class ProjectMetricsGridRecord
	{
		public int ProjectId { get; set; }
		public int DepartmentId { get; set; }
		public string Holder { get; set; }
		public string Status { get; set; }
		public string Event { get; set; }
		public DateTime? TimeStamp { get; set; }
		public DateTime? ChangeDate { get; set; }
		public int? BusinessDays { get; set; }
	}
}