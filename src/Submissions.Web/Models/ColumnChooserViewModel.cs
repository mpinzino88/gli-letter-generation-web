﻿using System;
using Submissions.Common.JQGrid;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;


namespace Submissions.Web.Models
{
	public class ColumnChooserViewModel
	{
		public ColumnChooserViewModel()
		{
			ColChooserMode = ColumnChooserMode.Apply;
		}
		public string SubTitle { get; set; }
		public IList<ColumnChooserColumn> SelectedColumns { get; set; }
		public IList<ColumnChooserColumn> AvailableColumns { get; set; }
		public int ColChooserId { get; set; }
		public string ColChooserName { get; set; }
		public ColumnChooserMode ColChooserMode { get; set; }
	}

	public class ColumnChooserTemplateViewModel
	{
		public ColumnChooserTemplateViewModel()
		{
			ColumnTemplates = new List<ColumnChooserTemplate>();
		}
		public List<ColumnChooserTemplate> ColumnTemplates { get; set; }
	
	}

	public class ColumnChooserTemplate
	{
		public int Id { get; set; }
		[Display(Name = "Date")]
		public DateTime AddDate { get; set; }
		[Display(Name = "Name")]
		public string TemplateName { get; set; }
		public bool Current { get; set; }
	}
}