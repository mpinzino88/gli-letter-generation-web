﻿namespace Submissions.Web.Models.Query
{
	public class UserSearch
	{
		public int? ManagerId { get; set; }
		public int? TeamId { get; set; }
		public int? BusinessOwnerId { get; set; }
		public bool NotInTeam { get; set; }
		public int? SupplierFocusId { get; set; }
		public bool NotFocusedOnSupplier { get; set; }
		public bool NotInBussinessOwner { get; set; }
	}
}