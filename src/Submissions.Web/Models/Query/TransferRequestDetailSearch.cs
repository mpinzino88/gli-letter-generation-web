﻿using System;

namespace Submissions.Web.Models.Query
{
	public class TransferRequestDetailSearch
	{
		public Guid? TransferRequestId { get; set; }
	}
}