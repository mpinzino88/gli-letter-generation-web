﻿using Submissions.Common;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Submissions.Web.Models.Query
{
	public class TransferRequestSearch
	{
		public TransferRequestSearch()
		{
			TransferRequestStatusId = null;
			TransferRequestStatus = "";
			TransferRequestStatusList = LookupsStandard.ConvertEnum<TransferRequestStatus>(useDescriptionAsValue: true, showBlank: true);
		}

		[Display(Name = "Transfer Status")]
		public string TransferRequestStatusId { get; set; }
		public string TransferRequestStatus { get; set; }
		public IEnumerable<SelectListItem> TransferRequestStatusList { get; set; }
		[Display(Name = "File Number")]
		public string FileNumber { get; set; }
		[Display(Name = "Show Awaiting Secondary Approval Requests Only")]
		public bool ShowUnconfirmedOnly { get; set; }
	}
}