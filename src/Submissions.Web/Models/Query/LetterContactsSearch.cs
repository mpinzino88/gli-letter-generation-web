﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Submissions.Web.Models.LookupsAjax
{
	public class LetterContactsSearch
	{
		[Display(Name = "Manufacturers")]
		public List<string> ManufacturerCodes { get; set; }
		public IList<SelectListItem> Manufacturers { get; set; }
		[Display(Name = "Jurisdictions")]
		public List<string> JurisdictionCodes { get; set; }
		public IList<SelectListItem> Jurisdictions { get; set; }
		[Display(Name = "Contact Type")]
		public List<int> ContactTypes { get; set; }
		[Display(Name = "Search Term")]
		public string SearchTerm { get; set; }
	}
}