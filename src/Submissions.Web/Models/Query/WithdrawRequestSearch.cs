﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Submissions.Web.Models.Query
{
	public class WithdrawRequestSearch
	{
		public Guid? RequestId { get; set; }
		[Display(Name = "Request Date")]
		public DateTime? StartRequestDate { get; set; }
		public DateTime? EndRequestDate { get; set; }
		[Display(Name = "Order Number")]
		public int? OrderNumber { get; set; }
		[Display(Name = "File Number")]
		public string FileNumber { get; set; }
		[Display(Name = "Manufacturer")]
		public string ManufacturerCode { get; set; }
		public string Manufacturer { get; set; }
		[Display(Name = "Jurisdiction")]
		public string JurisdictionId { get; set; }
		public string Jurisdiction { get; set; }
		[Display(Name = "Status")]
		public string Status { get; set; }
		[Display(Name = "GameName")]
		public string GameName { get; set; }
		[Display(Name = "ID Number")]
		public string IdNumber { get; set; }
		[Display(Name = "Version")]
		public string Version { get; set; }
		[Display(Name = "Part Number")]
		public string PartNumber { get; set; }
		[Display(Name = "User")]
		public int? UserId { get; set; }
		public string Username { get; set; }
		public bool? IsJurisdiction { get; set; }
	}
}