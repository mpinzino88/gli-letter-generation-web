﻿using Submissions.Web.Models.Grids.Reports;
using System.Collections.Generic;
using System.Web.Mvc;

namespace Submissions.Web.Models.Query
{
	public class ProjectSearchPartialViewModel
	{
		public ProjectSearchPartialViewModel()
		{
			this.Filters = new ProjectSearch();
			this.SortList = new ProjectsReportGrid().GetAvailableSorts();
		}

		public string PartialViewName { get { return "EditorTemplates/Query/ProjectSearch"; } }
		public ProjectSearch Filters { get; set; }
		public IList<SelectListItem> SortList { get; set; }
	}
}