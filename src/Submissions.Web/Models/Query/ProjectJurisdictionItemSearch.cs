﻿using Submissions.Common;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Submissions.Web.Models.Query
{
	public class ProjectJurisdictionItemSearch
	{
		public ProjectJurisdictionItemSearch()
		{
			ProjectIds = new List<int>();
		}
		[Display(Name = "Projects")]
		public List<int> ProjectIds { get; set; }
		public IList<SelectListItem> Projects { get; set; }

		[Display(Name = "File Number")]
		public string FileNumber { get; set; }

		[Display(Name = "Is Transfer?")]
		public bool IsTransfer { get; set; }

		[Display(Name = "Manufacturer")]
		public string Manufacturer { get; set; }

		[Display(Name = "Manufacturer")]
		public string ManufacturerShort { get; set; }

		[Display(Name = "Jurisdiction")]
		public string JurisdictionDesc { get; set; }

		[Display(Name = "Jurisdiction")]
		public string Jurisdiction { get; set; }

		[Display(Name = "Location")]
		public int? LocationId { get; set; }
		public string Location { get; set; }

		public BundleType? ProjectTestingType { get; set; }

	}
}

