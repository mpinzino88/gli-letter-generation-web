﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace Submissions.Web.Models.Query
{
	public class MemoSearch
	{
		public int StandardMemoId { get; set; }
		public int OnHoldMemoId { get; set; }
		public int ComplianceMemoId { get; set; }

		[DisplayName("Date")]
		public DateTime? StartDate { get; set; }
		public DateTime? EndDate { get; set; }
		[DisplayName("Memo")]
		public string StandardText { get; set; }
		[DisplayName("On-Hold Memo")]
		public string OnHoldText { get; set; }
		[DisplayName("Compliance Memo")]
		public string ComplianceText { get; set; }
	}
}