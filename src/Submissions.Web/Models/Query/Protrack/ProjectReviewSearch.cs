﻿using System.Collections.Generic;
using System.Web.Mvc;

namespace Submissions.Web.Models.Query
{
	public class ProjectReviewSearch
	{
		public int? DepartmentId { get; set; }
		public string Department { get; set; }
		public List<int> UserId { get; set; }
		public IList<SelectListItem> User { get; set; }
		public string[] StatusIds { get; set; }
		public IList<SelectListItem> Status { get; set; }
		public string SearchTerm { get; set; }
		public int CurrentUserId { get; set; }
		public int? ProjectId { get; set; }
		public string Filenumber { get; set; }
	}
}