﻿using Newtonsoft.Json;
using Submissions.Common.CustomJsonConverters;
using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace Submissions.Web.Models.Query.Protrack
{
	public class ResourceFinderSearch
	{
		public ResourceFinderSearch()
		{
			this.Managers = new List<SelectListItem>();
			this.Locations = new List<SelectListItem>();
			this.Resources = new List<SelectListItem>();
			this.Manufacturers = new List<SelectListItem>();
			this.Platforms = new List<SelectListItem>();
			this.TaskDefinitions = new List<SelectListItem>();
		}

		[JsonConverter(typeof(SingleOrArrayConverter<string>))]
		public string[] ManagerIds { get; set; }
		public IList<SelectListItem> Managers { get; set; }
		[JsonConverter(typeof(SingleOrArrayConverter<string>))]
		public string[] LocationIds { get; set; }
		public IList<SelectListItem> Locations { get; set; }
		[JsonConverter(typeof(SingleOrArrayConverter<string>))]
		public string[] ResourceIds { get; set; }
		public IList<SelectListItem> Resources { get; set; }
		public DateTime? AvailableDate { get; set; }
		[JsonConverter(typeof(SingleOrArrayConverter<string>))]
		public string[] ManufacturerCodes { get; set; }
		public IList<SelectListItem> Manufacturers { get; set; }
		[JsonConverter(typeof(SingleOrArrayConverter<string>))]
		public string[] PlatformIds { get; set; }
		public IList<SelectListItem> Platforms { get; set; }
		[JsonConverter(typeof(SingleOrArrayConverter<string>))]
		public string[] TaskDefinitionIds { get; set; }
		public IList<SelectListItem> TaskDefinitions { get; set; }
	}
}