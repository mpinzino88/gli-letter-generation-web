﻿using System.Collections.Generic;

namespace Submissions.Web.Models.Query
{
	public class SubProjectSearch
	{
		public SubProjectSearch()
		{
			TeamIds = new List<int>();
			AssigneeIds = new List<int>();
			Statuses = new List<string>();
		}

		public IList<int> TeamIds { get; set; }
		public IList<int> AssigneeIds { get; set; }
		public int? ProjectId { get; set; }
		public IList<string> Statuses { get; set; }
	}
}