﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Submissions.Common;
using Submissions.Common.JQGrid;

namespace Submissions.Web.Models.Query
{
	[Serializable]
	public class ProjectSearch : ISort
	{
		public ProjectSearch()
		{
			this.ManagerIds = new List<string>();
			this.SeniorEngineerIds = new List<string>();
			this.MAProjectLeadIds = new List<string>();
			this.EngineerIds = new List<string>();
			this.ManufacturerShorts = new List<string>();
			this.DevelopmentRepIds = new List<string>();
			this.LaboratoryIds = new List<string>();
			this.ReviewerIds = new List<string>();
			this.QALaboratoryIds = new List<string>();
			this.TestLabIds = new List<string>();
			this.ExcludeFileTypes = new List<string>();
			this.Sort1 = string.Empty;
			this.Sort2 = string.Empty;
		}

		public string Status { get; set; }

		public string BundleStatus { get; set; }

		[Display(Name = "File Number")]
		public string FileNumber { get; set; }

		[Display(Name = "Project Name")]
		public string ProjectName { get; set; }

		[Display(Name = "QA Supervisor")]
		public int? QASupervisorId { get; set; }
		public string QASupervisor { get; set; }

		[Display(Name = "Eng Manager")]

		public int? ManagerId { get; set; }
		public string Manager { get; set; }

		//Multi
		public List<string> ManagerIds { get; set; }
		public List<SelectListItem> Managers { get; set; }

		[Display(Name = "Reviewer")]
		public int? ReviewerId { get; set; }
		public string Reviewer { get; set; }

		//Multi
		public List<string> ReviewerIds { get; set; }
		public List<SelectListItem> Reviewers { get; set; }

		[Display(Name = "Office")]
		public int? LaboratoryId { get; set; }
		public string Laboratory { get; set; }

		public List<string> LaboratoryIds { get; set; }
		public List<SelectListItem> Laboratories { get; set; }

		[Display(Name = "Test Lab")]
		public int? TestLabId { get; set; }
		public string TestLab { get; set; }

		public List<string> TestLabIds { get; set; }
		public List<SelectListItem> TestLabs { get; set; }

		//Reviewer's location
		[Display(Name = "QA Office")]
		public int? QALocationId { get; set; }
		public string QALocation { get; set; }

		public List<string> QALaboratoryIds { get; set; }
		public List<SelectListItem> QALaboratories { get; set; }

		[Display(Name = "Engineer")]
		public int? EngineerId { get; set; }
		public string Engineer { get; set; }

		public List<string> EngineerIds { get; set; }
		public List<SelectListItem> Engineers { get; set; }

		[Display(Name = "Project Lead")]
		public int? SeniorEngineerId { get; set; }
		public string SeniorEngineer { get; set; }
		public List<string> SeniorEngineerIds { get; set; }
		public List<SelectListItem> SeniorEngineers { get; set; }

		[Display(Name = "Math Project Lead")]
		public int? MAProjectLeadId { get; set; }
		public string MAProjectLead { get; set; }
		public List<string> MAProjectLeadIds { get; set; }
		public List<SelectListItem> MAProjectLeads { get; set; }

		[Display(Name = "Manufacturer")]
		public string Manufacturer { get; set; }

		[Display(Name = "Manufacturer")]
		public string ManufacturerShort { get; set; }

		public List<string> ManufacturerShorts { get; set; }
		public List<SelectListItem> Manufacturers { get; set; }

		[Display(Name = "Primary Jurisdiction")]
		public string MainJurisdictionDesc { get; set; }

		[Display(Name = "Primary Jurisdiction")]
		public string MainJurisdiction { get; set; }

		[Display(Name = "Jurisdiction")]
		public string JurisdictionDesc { get; set; }

		[Display(Name = "Jurisdiction")]
		public string Jurisdiction { get; set; }

		[Display(Name = "Submission Type")]
		public string SubmissionTypeDesc { get; set; }
		public int? SubmissionType { get; set; }

		[Display(Name = "Expedite")]
		public bool IsRush { get; set; }

		[Display(Name = "Projects/Transfers")]
		public ProjectType IsTransfer { get; set; }

		[Display(Name = "Received From")]
		public DateTime? ReceivedFrom { get; set; }

		[Display(Name = "Received To")]
		public DateTime? ReceivedTo { get; set; }

		//Currently only used to get Completed in last 30 days for EN protrack
		[Display(Name = "Complete From")]
		public DateTime? CompleteFrom { get; set; }
		[Display(Name = "Complete To")]
		public DateTime? CompleteTo { get; set; }
		//-------------------------------------------

		[Display(Name = "QA Accepted From")]
		public DateTime? QAAcceptedFrom { get; set; }

		[Display(Name = "QA Accepted To")]
		public DateTime? QAAcceptedTo { get; set; }

		[Display(Name = "Eng Completed From")]
		public DateTime? EngCompletedFrom { get; set; }

		[Display(Name = "Eng Completed To")]
		public DateTime? EngCompletedTo { get; set; }

		//Currently only used to get Completed in last 30 days for QA protrack
		[Display(Name = "QALW Completed From")]
		public DateTime? QALWCompletedFrom { get; set; }
		[Display(Name = "QALW Completed To")]
		public DateTime? QALWCompletedTo { get; set; }

		[Display(Name = "Target Date From")]
		public DateTime? TargetDateFrom { get; set; }
		[Display(Name = "Target Date To")]
		public DateTime? TargetDateTo { get; set; }
		//--------------------------------------------

		[Display(Name = "Billing Reference/RDAP Pin")]
		public string ClientNumber { get; set; }

		[Display(Name = "User Favorite")]
		public int? UserFavoriteId { get; set; }

		public bool IsDeclined { get; set; }
		public bool CheckHasEngTask { get; set; }

		public List<string> DevelopmentRepIds { get; set; }
		public List<SelectListItem> DevelopmentReps { get; set; }

		//All Multiselection comma separated
		public List<string> ProjectStatuses { get; set; }
		public List<string> BundleStatuses { get; set; }

		public string SingleInputSearch { get; set; }
		public bool SingleInputSearchProjectOnly { get; set; }

		//Applies to QA Advanced Search
		public DateTime? QATaskTarget { get; set; }
		public string QATask { get; set; }

		public List<string> ExcludeFileTypes { get; set; }

		[Display(Name = "Primary Sort")]
		public string Sort1 { get; set; }
		public SortOrder Sort1Order { get; set; }
		[Display(Name = "Secondary Sort")]
		public string Sort2 { get; set; }
		public SortOrder Sort2Order { get; set; }
	}
}