﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Submissions.Web.Models.Query
{
    public class GamingGuidelinesSportsBettingSearch
    {
        public GamingGuidelinesSportsBettingSearch()
        {
            this.Jurisdictions = new List<SelectListItem>();
        }
        [Display(Name = "Search for Jurisdiction")]
        public string[] JurisdictionIds { get; set; }
        public IList<SelectListItem> Jurisdictions { get; set; }
    }
}