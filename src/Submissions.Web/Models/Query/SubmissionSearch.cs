﻿using Submissions.Common;
using Submissions.Common.JQGrid;
using System;
using System.ComponentModel.DataAnnotations;

namespace Submissions.Web.Models.Query
{
	[Serializable]
	public class SubmissionSearch : ISort
	{
		public SubmissionSearch()
		{
			this.Sort1 = string.Empty;
			this.Sort2 = string.Empty;
		}

		public int? SubmissionId { get; set; }
		[Display(Name = "Test Lab")]
		public int? LabId { get; set; }
		public string Lab { get; set; }
		[Display(Name = "Status")]
		public string Status { get; set; }
		[Display(Name = "Type")]
		public int? TypeId { get; set; }
		public string Type { get; set; }
		[Display(Name = "Jurisdiction")]
		public string JurisdictionId { get; set; }
		public string Jurisdiction { get; set; }
		[Display(Name = "Manufacturer")]
		public string ManufacturerCode { get; set; }
		public string Manufacturer { get; set; }
		public string Year { get; set; }
		[Display(Name = "File Number")]
		public string FileNumber { get; set; }
		[Display(Name = "Hardware Part Number")]
		public string PartNumber { get; set; }
		public string Version { get; set; }
		[Display(Name = "Date Code")]
		public string DateCode { get; set; }
		[Display(Name = "ID Number")]
		public string IdNumber { get; set; }
		[Display(Name = "Game Name")]
		public string GameName { get; set; }
		public int? Sequence { get; set; }
		[Display(Name = "Certification Lab")]
		public int? CertificationLabId { get; set; }
		public string CertificationLab { get; set; }
		[Display(Name = "Archive Location")]
		public int? ArchiveLocationId { get; set; }
		public string ArchiveLocation { get; set; }
		[Display(Name = "Chip Type")]
		public int? ChipTypeId { get; set; }
		public string ChipType { get; set; }
		public string Position { get; set; }
		[Display(Name = "Game Type")]
		public int? GameTypeId { get; set; }
		public string GameType { get; set; }
		[Display(Name = "Function")]
		public int? FunctionId { get; set; }
		public string Function { get; set; }
		[Display(Name = "Regression Testing")]
		public bool? RegressionTesting { get; set; }
		[Display(Name = "Conditional Revoke")]
		public bool? ConditionalRevoke { get; set; }
		public string Vendor { get; set; }
		[Display(Name = "Submit Date")]
		public DateTime? StartSubmitDate { get; set; }
		public DateTime? EndSubmitDate { get; set; }
		public int? RollingSubmitDateDays { get; set; }
		[Display(Name = "Receive Date")]
		public DateTime? StartReceiveDate { get; set; }
		public DateTime? EndReceiveDate { get; set; }
		public int? RollingReceiveDateDays { get; set; }
		[Display(Name = "Replace With")]
		public string ReplaceWith { get; set; }
		[Display(Name = "Source Code Storage")]
		public string CodeStorage { get; set; }
		[Display(Name = "EU Source Code Storage")]
		public string EUCodeStorage { get; set; }
		[Display(Name = "Bug Box")]
		public int? BugBoxLocationId { get; set; }
		public string BugBoxLocation { get; set; }
		public string BugBox { get; set; }
		public string System { get; set; }
		public string Operator { get; set; }
		[Display(Name = "Project/Contract Number")]
		public string ContractNumber { get; set; }
		[Display(Name = "Testing Type")]
		public int? TestingTypeId { get; set; }
		public string TestingType { get; set; }
		[Display(Name = "Work Performed")]
		public int? WorkPerformedId { get; set; }
		public string WorkPerformed { get; set; }
		[Display(Name = "Company")]
		public int? CompanyId { get; set; }
		public string Company { get; set; }
		[Display(Name = "Currency")]
		public int? CurrencyId { get; set; }
		public string Currency { get; set; }
		[Display(Name = "Contract Type")]
		public int? ContractTypeId { get; set; }
		public string ContractType { get; set; }
		[Display(Name = "Testing Performed")]
		public int? TestingPerformedId { get; set; }
		public string TestingPerformed { get; set; }
		[Display(Name = "External Test Lab")]
		public int? ExternalLabId { get; set; }
		public string ExternalLab { get; set; }
		[Display(Name = "Purchase Order")]
		public string PurchaseOrder { get; set; }
		[Display(Name = "Billing Reference")]
		public string BillingReference { get; set; }
		[Display(Name = "Letter Number")]
		public string LetterNumber { get; set; }
		[Display(Name = "Project Detail")]
		public string ProjectDetail { get; set; }
		public string SingleInputSearch { get; set; }

		[Display(Name = "Primary Sort")]
		public string Sort1 { get; set; }
		public SortOrder Sort1Order { get; set; }
		[Display(Name = "Secondary Sort")]
		public string Sort2 { get; set; }
		public SortOrder Sort2Order { get; set; }
	}
}