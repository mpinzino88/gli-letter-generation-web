﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace Submissions.Web.Models.Query
{
	public class RNGSearch
	{
		[DisplayName("Game Name")]
		public string GameName { get; set; }
		public string Version { get; set; }
		[DisplayName("Game Type")]
		public string GameType { get; set; }
		[DisplayName("# of Reels")]
		public int? ReelCount { get; set; }
		[DisplayName("Range Start")]
		public int? StartRange { get; set; }
		[DisplayName("Range End")]
		public int? EndRange { get; set; }
		[DisplayName("Games Collected")]
		public string GamesCollected { get; set; }
		public bool? Replacement { get; set; }
		[DisplayName("Manufacturer")]
		public int? ManufacturerId { get; set; }
		public string Manufacturer { get; set; }
	}
}