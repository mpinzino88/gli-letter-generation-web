﻿using Submissions.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Submissions.Web.Models.Query.Reports
{
	public class QAAdvancedSearch
	{
		public QAAdvancedSearch()
		{


		}

		[Display(Name = "Manufacturer")]
		public string ManufacturerShort { get; set; }
		public string Manufacturer { get; set; }

		[Display(Name = "Primary Jurisdiction")]
		public int? JurisdictionId { get; set; }
		public string Jurisdiction { get; set; }

		[Display(Name = "Owning Office")]
		public List<int> LaboratoryIds { get; set; }
		public List<SelectListItem> Laboratories { get; set; }

		[Display(Name = "Task")]
		public List<int> TaskDefIds { get; set; }
		public List<SelectListItem> TaskDefs { get; set; }

		[Display(Name = "Expedite")]
		public bool Expedite { get; set; }

		[Display(Name = "Received In QA")]
		public DateTime? QAReceivedFrom { get; set; }
		public DateTime? QAReceivedTo { get; set; }

		[Display(Name = "Accepted In QA")]
		public DateTime? QAAcceptedFrom { get; set; }
		public DateTime? QAAcceptedTo { get; set; }

		[Display(Name = "Received")]
		public DateTime? RecievedFrom { get; set; }
		public DateTime? RecievedTo { get; set; }

		[Display(Name = "EN Completed")]
		public DateTime? ENCompletedFrom { get; set; }
		public DateTime? ENCompletedTo { get; set; }

		[Display(Name = "QA Completed")]
		public DateTime? QACompletedFrom { get; set; }
		public DateTime? QACompletedTo { get; set; }

		[Display(Name = "Task Target")]
		public DateTime? TaskTarget { get; set; }

		[Display(Name = "Projects/Transfers")]
		public ProjectType IsTransfer { get; set; }

		[Display(Name = "Status")]
		public List<string> CustomSearchStatus { get; set; }
		public List<SelectListItem> CustomSearchStatusItem { get; set; }

		[Display(Name = "Bundle Type")]
		public QACustomBundleType? CustomSearchBundleType { get; set; }

		[Display(Name = "QFI Needed?")]
		public bool QFINeeded { get; set; }

		[Display(Name = "QFI Number")]
		public bool QFINumber { get; set; }

		[Display(Name = "Add'l EN Involvement Needed?:")]
		public bool ENInvolvementWhileFL { get; set; }

		[Display(Name = "No Certification:")]
		public bool NoCertNeeded { get; set; }

		[Display(Name = "ADM Work Needed?")]
		public bool ADMWorkNeeded { get; set; }

		[Display(Name = "Worked By")]
		public List<int> WorkedByLocationIds { get; set; }
		public List<SelectListItem> WorkedByLocations { get; set; }
		
		[Display(Name = "Accepted By")]
		public List<int> AcceptedByLocationIds { get; set; }
		public List<SelectListItem> AcceptedByLocations { get; set; }
		
		[Display(Name = "QA Office")]
		public int? QALocationId { get; set; }
		public string QALocation { get; set; }
		[Display(Name = "Compliance Required")]
		public bool ComplianceRequired { get; set; }
	}
}