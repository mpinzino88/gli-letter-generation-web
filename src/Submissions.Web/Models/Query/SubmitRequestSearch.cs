﻿using System.ComponentModel.DataAnnotations;

namespace Submissions.Web.Models.Query
{
	public class SubmitRequestSearch
	{
		[Display(Name = "Testing Type")]
		public int? TestingTypeId { get; set; }
		public string TestingType { get; set; }
	}
}