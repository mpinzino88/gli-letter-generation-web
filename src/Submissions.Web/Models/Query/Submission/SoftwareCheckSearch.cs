﻿using System.ComponentModel.DataAnnotations;

namespace Submissions.Web.Models.Query.Submission
{
	public class SoftwareCheckSearch
	{
		[Display(Name = "Location")]
		public int? LocationId { get; set; }
		public string Location { get; set; }
	}
}