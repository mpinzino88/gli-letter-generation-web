﻿using Submissions.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Submissions.Web.Models.Query
{
	public class AdvancedSearchSearch
	{
		public AdvancedSearchSearch()
		{
			this.Jurisdiction = new AdvancedSearchJurisdiction();
			this.MachineCertificate = new AdvancedSearchMachineCertificate();
			this.Memo = new AdvancedSearchMemo();
			this.Signature = new AdvancedSearchSignature();
			this.Submission = new AdvancedSearchSubmission();
		}

		public AdvancedSearchJurisdiction Jurisdiction { get; set; }
		public AdvancedSearchMachineCertificate MachineCertificate { get; set; }
		public AdvancedSearchMemo Memo { get; set; }
		public AdvancedSearchSignature Signature { get; set; }
		public AdvancedSearchSubmission Submission { get; set; }
	}

	public class AdvancedSearchJurisdiction
	{
		[Display(Name = "Jurisdiction")]
		public string[] JurisdictionIds { get; set; }
		public IList<SelectListItem> Jurisdictions { get; set; }
		[Display(Name = "Status")]
		public string[] Statuses { get; set; }
		[Display(Name = "Company")]
		public int? CompanyId { get; set; }
		public string Company { get; set; }
		[Display(Name = "Currency")]
		public int? CurrencyId { get; set; }
		public string Currency { get; set; }
		[Display(Name = "Contract Type")]
		public int? ContractTypeId { get; set; }
		public string ContractType { get; set; }
		[Display(Name = "Conditionally Revoked")]
		public bool? ConditionalRevoke { get; set; }
		[Display(Name = "Draft Date")]
		public DateTime? StartDraftDate { get; set; }
		public DateTime? EndDraftDate { get; set; }
		[Display(Name = "Close Date")]
		public DateTime? StartCloseDate { get; set; }
		public DateTime? EndCloseDate { get; set; }
		[Display(Name = "Amend Date")]
		public DateTime? StartUpdateDate { get; set; }
		public DateTime? EndUpdateDate { get; set; }
		[Display(Name = "Revoke Date")]
		public DateTime? StartRevokeDate { get; set; }
		public DateTime? EndRevokeDate { get; set; }
		[Display(Name = "Quote Date")]
		public DateTime? StartQuoteDate { get; set; }
		public DateTime? EndQuoteDate { get; set; }
		[Display(Name = "Authorize Date")]
		public DateTime? StartAuthorizeDate { get; set; }
		public DateTime? EndAuthorizeDate { get; set; }
		[Display(Name = "NU Date")]
		public DateTime? StartObsoleteDate { get; set; }
		public DateTime? EndObsoleteDate { get; set; }
		[Display(Name = "WON")]
		public string WON { get; set; }
		[Display(Name = "Project/Contract Number")]
		public string ContractNumber { get; set; }
		[Display(Name = "Regulator Ref Number")]
		public string RegulatorRefNum { get; set; }
		[Display(Name = "Billing Reference/RDAP Pin")]
		public string ClientNumber { get; set; }
		[Display(Name = "Letter Number")]
		public string LetterNumber { get; set; }
		[Display(Name = "Regulator Certification/KSA Number")]
		public string CertNumber { get; set; }
	}

	public class AdvancedSearchMachineCertificate
	{
		[Display(Name = "Certificate Number")]
		public string CertificateNumber { get; set; }
		[Display(Name = "Serial Number")]
		public string SerialNumber { get; set; }
		[Display(Name = "Seal Number")]
		public string SealNumber { get; set; }
		[Display(Name = "Certificate Date")]
		public DateTime? StartCertificateDate { get; set; }
		public DateTime? EndCertificateDate { get; set; }
		[Display(Name = "Device Type")]
		public string DeviceType { get; set; }
		[Display(Name = "Replacement for Certificate Number")]
		public string CertificateThatWasReplaced { get; set; }
	}

	public class AdvancedSearchMemo
	{
		public DateTime? StartDate { get; set; }
		public DateTime? EndDate { get; set; }
		public MemoType? Type { get; set; }
		public string Memo { get; set; }
	}

	public class AdvancedSearchSignature
	{
		[Display(Name = "Type")]
		public int? TypeId { get; set; }
		public string Type { get; set; }
		[Display(Name = "Scope")]
		public int? ScopeId { get; set; }
		public string Scope { get; set; }
		public string Signature { get; set; }
	}

	public class AdvancedSearchSubmission
	{
		public int? SubmissionId { get; set; }
		[Display(Name = "Test Lab")]
		public int? LabId { get; set; }
		public string Lab { get; set; }
		[Display(Name = "Status")]
		public string Status { get; set; }
		[Display(Name = "Type")]
		public int? TypeId { get; set; }
		public string Type { get; set; }
		[Display(Name = "Manufacturer")]
		public string ManufacturerCode { get; set; }
		public string Manufacturer { get; set; }
		public string Year { get; set; }
		[Display(Name = "File Number")]
		public string FileNumber { get; set; }
		[Display(Name = "Part Number")]
		public string PartNumber { get; set; }
		public string Version { get; set; }
		[Display(Name = "Date Code")]
		public string DateCode { get; set; }
		[Display(Name = "ID Number")]
		public string IdNumber { get; set; }
		[Display(Name = "Game Name")]
		public string GameName { get; set; }
		[Display(Name = "Certification Lab")]
		public int? CertificationLabId { get; set; }
		public string CertificationLab { get; set; }
		[Display(Name = "Archive Location")]
		public int? ArchiveLocationId { get; set; }
		public string ArchiveLocation { get; set; }
		[Display(Name = "Chip Type")]
		public int? ChipTypeId { get; set; }
		public string ChipType { get; set; }
		public string Position { get; set; }
		[Display(Name = "Game Type")]
		public int? GameTypeId { get; set; }
		public string GameType { get; set; }
		[Display(Name = "Function")]
		public int? FunctionId { get; set; }
		public string Function { get; set; }
		[Display(Name = "Regression Testing")]
		public bool? RegressionTesting { get; set; }
		[Display(Name = "Conditionally Revoked")]
		public bool? ConditionalRevoke { get; set; }
		public string Vendor { get; set; }
		[Display(Name = "Submit Date")]
		public DateTime? StartSubmitDate { get; set; }
		public DateTime? EndSubmitDate { get; set; }
		[Display(Name = "Receive Date")]
		public DateTime? StartReceiveDate { get; set; }
		public DateTime? EndReceiveDate { get; set; }
		[Display(Name = "Replace With")]
		public string ReplaceWith { get; set; }
		[Display(Name = "Source Code Storage")]
		public int? CodeStorageLocationId { get; set; }
		public string CodeStorageLocation { get; set; }
		public string CodeStorage { get; set; }
		[Display(Name = "EU Source Code Storage")]
		public string EUCodeStorage { get; set; }
		[Display(Name = "Bug Box")]
		public int? BugBoxLocationId { get; set; }
		public string BugBoxLocation { get; set; }
		public string BugBox { get; set; }
		public string System { get; set; }
		public string Operator { get; set; }
		[Display(Name = "Project/Contract Number")]
		public string ContractNumber { get; set; }
		[Display(Name = "Testing Type")]
		public int? TestingTypeId { get; set; }
		public string TestingType { get; set; }
		[Display(Name = "Work Performed")]
		public int? WorkPerformedId { get; set; }
		public string WorkPerformed { get; set; }
		[Display(Name = "Company")]
		public int? CompanyId { get; set; }
		public string Company { get; set; }
		[Display(Name = "Currency")]
		public int? CurrencyId { get; set; }
		public string Currency { get; set; }
		[Display(Name = "Contract Type")]
		public int? ContractTypeId { get; set; }
		public string ContractType { get; set; }
		[Display(Name = "Testing Performed")]
		public int? TestingPerformedId { get; set; }
		public string TestingPerformed { get; set; }
		[Display(Name = "External Test Lab")]
		public int? ExternalLabId { get; set; }
		public string ExternalLab { get; set; }
		[Display(Name = "Purchase Order")]
		public string PurchaseOrder { get; set; }
		[Display(Name = "Letter Number")]
		public string LetterNumber { get; set; }
		[Display(Name = "Project Detail")]
		public string ProjectDetail { get; set; }
		[Display(Name = "Manufacturer Build ID")]
		public string ManufacturerBuildId { get; set; }
		[Display(Name = "Original Uploaded Material")]
		public string OrgIGTMaterial { get; set; }

		[Display(Name = "ARI Project Type")]
		public string AristocratTypeId { get; set; }
		public string AristocratType { get; set; }
		[Display(Name = "ARI Complexity")]
		public string AristocratComplexityId { get; set; }
		public string AristocratComplexity { get; set; }
		[Display(Name = "ARI Market")]
		public string AristocratMarketId { get; set; }
		public string AristocratMarket { get; set; }

	}
}