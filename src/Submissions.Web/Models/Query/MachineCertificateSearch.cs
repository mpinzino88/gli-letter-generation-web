﻿using System;
using System.ComponentModel;

namespace Submissions.Web.Models.Query
{
	public class MachineCertificateSearch
	{
		[DisplayName("Certificate Number")]
		public string CertificateNumber { get; set; }
		[DisplayName("Machine Serial Number")]
		public string MachineSerialNumber { get; set; }
		[DisplayName("Gaming Device Type")]
		public string GamingDeviceType { get; set; }
		[DisplayName("Date of Certificate")]
		public DateTime? StartCertificateDate { get; set; }
		public DateTime? EndCertificateDate { get; set; }
		[DisplayName("Seal Number")]
		public string SealNumber { get; set; }
		[DisplayName("Replacement of Certificate Number")]
		public string ReplacementCertificateNumber { get; set; }
	}
}