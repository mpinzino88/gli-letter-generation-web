﻿using System.ComponentModel.DataAnnotations;

namespace Submissions.Web.Models.Query
{
	public class AdminReviewSearch
	{
		public AdminReviewSearch()
		{

		}
		[Display(Name = "File Number")]
		public string FileNumber { get; set; }

		[Display(Name = "Game Name")]
		public string GameName { get; set; }
		[Display(Name = "ID Number")]
		public string IdNumber { get; set; }
		[Display(Name = "Jurisdiction")]
		public string Jurisdiction { get; set; }
		public string JurisdictionDesc { get; set; }
		[Display(Name = "Manufacturer")]
		public string ManufacturerShort { get; set; }
		public string ManufacturerShortDesc { get; set; }
		[Display(Name = "Project Name")]
		public string ProjectName { get; set; }
		[Display(Name = "Test Lab")]
		public int? TestLabId { get; set; }
		public string Testlab { get; set; }
		[Display(Name = "Version")]
		public string Version { get; set; }
	}
}