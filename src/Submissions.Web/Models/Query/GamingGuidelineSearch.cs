﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Submissions.Web.Models.Query
{
	public class GamingGuidelineSearch
	{
		public GamingGuidelineSearch()
		{
			this.Jurisdictions = new List<SelectListItem>();
			this.Attributes = new List<SelectListItem>();
			this.Limits = new List<SelectListItem>();
			this.ProhibitedGameTypes = new List<SelectListItem>();
			this.Protocols = new List<SelectListItem>();
			this.RTPMinValues = new List<SelectListItem>();
			this.RTPMaxValues = new List<SelectListItem>();
			this.OddsValues = new List<SelectListItem>();
			this.LimitValues = new List<SelectListItem>();
			this.AttributeIds = new List<int>();
			this.LimitIds = new List<int>();
			this.ProhibitedGameTypeIds = new List<int>();
			this.ProtocolIds = new List<int>();
			this.RTPTypeIds = new List<int>();
			this.OddsValueIds = new List<int>();
			this.LimitValueIds = new List<decimal>();
		}
		[Display(Name = "Search for Jurisdiction")]
		public string[] JurisdictionIds { get; set; }
		public IList<SelectListItem> Jurisdictions { get; set; }
		[Display(Name = "Attributes")]
		public List<int> AttributeIds { get; set; }
		public IList<SelectListItem> Attributes { get; set; }
		[Display(Name = "Limits")]
		public List<int> LimitIds { get; set; }
		public IList<SelectListItem> Limits { get; set; }
		[Display(Name = "Prohibited Game Types")]
		public List<int> ProhibitedGameTypeIds { get; set; }
		public IList<SelectListItem> ProhibitedGameTypes { get; set; }
		[Display(Name = "Protocols")]
		public List<int> ProtocolIds { get; set; }
		public IList<SelectListItem> Protocols { get; set; }
		[Display(Name = "RTP Type")]
		public List<int> RTPTypeIds { get; set; }
		public IList<SelectListItem> RTPTypes { get; set; }
		[Display(Name = "RTP Min (%)")]
		public double? RTPMinValueIds { get; set; }
		public IList<SelectListItem> RTPMinValues { get; set; }
		[Display(Name = "RTP Max (%)")]
		public double? RTPMaxValueIds { get; set; }
		public IList<SelectListItem> RTPMaxValues { get; set; }
		[Display(Name = "Odds (1 in ?)")]
		public List<int> OddsValueIds { get; set; }
		public IList<SelectListItem> OddsValues { get; set; }
		[Display(Name = "Limit Values")]
		public List<decimal> LimitValueIds { get; set; }
		public IList<SelectListItem> LimitValues { get; set; }
		[Display(Name = "Inactive Only:")]
		public bool InactiveToggle { get; set; }
		public string Alphabet { get; set; }
	}
}