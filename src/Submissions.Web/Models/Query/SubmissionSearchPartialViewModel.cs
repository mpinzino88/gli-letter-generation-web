﻿using Submissions.Common.Query;
using Submissions.Web.Models.Grids.Reports;
using System.Collections.Generic;
using System.Web.Mvc;

namespace Submissions.Web.Models.Query
{
	public class SubmissionSearchPartialViewModel : ISearchPartialViewModel<SubmissionSearch>
	{
		public SubmissionSearchPartialViewModel()
		{
			this.Filters = new SubmissionSearch();
			this.SortList = new SubmissionsReportGrid().GetAvailableSorts();
		}

		public string PartialViewName { get { return "EditorTemplates/Query/SubmissionSearch"; } }
		public SubmissionSearch Filters { get; set; }
		public IList<SelectListItem> SortList { get; set; }
	}
}