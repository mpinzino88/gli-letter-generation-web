﻿namespace Submissions.Web.Models.Query
{
	public class CurrentFilter
	{
		public int Id { get; set; }
		public string Name { get; set; }
	}
}