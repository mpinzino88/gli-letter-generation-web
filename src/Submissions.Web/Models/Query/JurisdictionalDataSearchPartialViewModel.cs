﻿using Submissions.Web.Models.Grids.Reports;
using System.Collections.Generic;
using System.Web.Mvc;

namespace Submissions.Web.Models.Query
{
	public class JurisdictionalDataSearchPartialViewModel
	{
		public JurisdictionalDataSearchPartialViewModel()
		{
			this.Filters = new JurisdictionalDataSearch();
			this.SortList = new JurisdictionalDataReportGrid().GetAvailableSorts();
		}

		public string PartialViewName { get { return "EditorTemplates/Query/JurisdictionalDataSearch"; } }
		public JurisdictionalDataSearch Filters { get; set; }
		public IList<SelectListItem> SortList { get; set; }
	}
}