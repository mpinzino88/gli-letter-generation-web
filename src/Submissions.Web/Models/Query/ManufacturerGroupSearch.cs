﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Submissions.Web.Models.LookupsAjax
{
	public class ManufacturerGroupSearch
	{
		[Display(Name = "Category")]
		public List<int> ManufacturerGroupCategoryIds { get; set; }
		public IList<SelectListItem> ManufacturerGroupCategories { get; set; }

		[Display(Name = "Manufacturer")]
		public List<string> ManufacturerCodes { get; set; }
		public IList<SelectListItem> Manufacturers { get; set; }
	}
}