﻿using Submissions.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web.Mvc;

namespace Submissions.Web.Models.Query
{
	public class LetterBookSearch
	{
		public LetterBookSearch()
		{
			this.BatchStatuses = LookupsStandard.ConvertEnum<LetterBookACTStatus>().ToList();
		}

		public Department Department { get; set; }
		public int? DepartmentId { get; set; }
		[Display(Name = "Batch Number")]
		public int? BatchNumber { get; set; }
		[Display(Name = "Status")]
		public string[] BatchStatus { get; set; }
		public IList<SelectListItem> BatchStatuses { get; set; }
		[Display(Name = "Biller")]
		public int? BillingUserId { get; set; }
		public string BillingUser { get; set; }
		[Display(Name = "File Number")]
		public string FileNumber { get; set; }
		[Display(Name = "Only Latest")]
		public bool OnlyLatest { get; set; }
		[Display(Name = "Start Date")]
		public DateTime? ReportStartDate { get; set; }
		[Display(Name = "End Date")]
		public DateTime? ReportEndDate { get; set; }
		[Display(Name = "Show Billable")]
		public bool ShowBillable { get; set; }
		[Display(Name = "Show Non Billable")]
		public bool ShowNonBillable { get; set; }
		[Display(Name = "Show Only Major Revisions")]
		public bool OnlyMajorRevisions { get; set; }
		[Display(Name = "Letter Type")]
		public int[] LetterTypeIds { get; set; }
		public IList<SelectListItem> LetterTypes { get; set; }
		[Display(Name = "Dynamics File Number")]
		public string DynamicsFileNumString { get; set; }
		[Display(Name = "Lab")]
		public int? LaboratoryId { get; set; }
		public string Laboratory { get; set; }
		[Display(Name = "Currency")]
		public int[] CurrencyIds { get; set; }
		public IList<SelectListItem> Currencies { get; set; }
	}
}