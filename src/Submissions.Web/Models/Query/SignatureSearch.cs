﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace Submissions.Web.Models.Query
{
	public class SignatureSearch
	{
		[DisplayName("Type")]
		public int? TypeId { get; set; }
		public string Type { get; set; }
		[DisplayName("Scope")]
		public int? ScopeId { get; set; }
		public string Scope { get; set; }
		[DisplayName("Signature")]
		public string Code { get; set; }
	}
}