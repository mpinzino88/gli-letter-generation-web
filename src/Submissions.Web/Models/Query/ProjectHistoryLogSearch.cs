﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Submissions.Web.Models.Query
{
	public class ProjectHistoryLogSearch
	{
		public int? ProjectId { get; set; }
		public int? DepartmentId { get; set; }
	}
}