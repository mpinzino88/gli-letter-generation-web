﻿using Submissions.Common;
using Submissions.Common.JQGrid;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Submissions.Web.Models.Query
{
	[Serializable]
	public class TaskSearch : ISort
	{
		public TaskSearch()
		{
			this.Sort1 = string.Empty;
			this.Sort2 = string.Empty;
		}

		public int? ProjectId { get; set; }

		[Display(Name = "Project")]
		public string ProjectName { get; set; }

		[Display(Name = "Manager")]
		public int? ManagerId { get; set; }
		public string Manager { get; set; }

		[Display(Name = "Math Manager")]
		public int? MAManagerId { get; set; }
		public string MAManager { get; set; }

		[Display(Name = "File Number")]
		public string FileNumber { get; set; }

		public TaskStatusFilter? Status { get; set; }

		public QATaskStatusFilter? QATaskViewStatus { get; set; }

		[Display(Name = "Assign Date")]
		public DateTime? StartAssignDate { get; set; }
		public DateTime? EndAssignDate { get; set; }
		public int? RollingAssignDateDays { get; set; }

		[Display(Name = "Start Date")]
		public DateTime? StartStartDate { get; set; }
		public DateTime? EndStartDate { get; set; }
		public int? RollingStartDateDays { get; set; }

		[Display(Name = "Target Date")]
		public DateTime? StartTargetDate { get; set; }
		public DateTime? EndTargetDate { get; set; }
		public int? RollingTargetDateDays { get; set; }

		[Display(Name = "Complete Date")]
		public DateTime? StartCompleteDate { get; set; }
		public DateTime? EndCompleteDate { get; set; }
		public int? RollingCompleteDateDays { get; set; }

		[Display(Name = "Assigned To")]
		public int? UserId { get; set; }
		public string User { get; set; }

		[Display(Name = "Department")]
		public int? DepartmentId { get; set; }
		public string Department { get; set; }

		[Display(Name = "Manufacturer")]
		public string ManufacturerShort { get; set; }

		[Display(Name = "Office")]
		public int? LaboratoryId { get; set; }
		public string Laboratory { get; set; }
		public List<string> LaboratoryIds { get; set; }
		public List<SelectListItem> Laboratories { get; set; }

		[Display(Name = "Rush")]
		public bool IsRush { get; set; }

		[Display(Name = "ETA Queue")]
		public bool ShowETAQueue { get; set; }

		[Display(Name = "Projects/Transfers")]
		public ProjectType IsTransfer { get; set; }

		// used by Gantt chart
		[Display(Name = "Date")]
		public DateTime? StartEncapsulateDate { get; set; }
		public DateTime? EndEncapsulateDate { get; set; }

		[Display(Name = "Primary Sort")]
		public string Sort1 { get; set; }
		public SortOrder Sort1Order { get; set; }

		[Display(Name = "Secondary Sort")]
		public string Sort2 { get; set; }
		public SortOrder Sort2Order { get; set; }
		public List<int> ManufacturerGroupIds { get; set; }
		public bool ExcludeMathQueueGroups { get; set; }
	}
}