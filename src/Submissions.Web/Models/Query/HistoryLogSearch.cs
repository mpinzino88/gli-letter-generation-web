﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Submissions.Web.Models.Query
{
	public class HistoryLogSearch
	{
		[Display(Name = "Change Date")]
		public DateTime? StartDate { get; set; }
		public DateTime? EndDate { get; set; }
		[Required]
		public string Table { get; set; }
		public string Column { get; set; }
		[Display(Name = "Action Type")]
		public string ActionType { get; set; }
		[Display(Name = "Jurisdiction")]
		public string JurisdictionId { get; set; }
		public string Jurisdiction { get; set; }
		[Display(Name = "Manufacturer")]
		public string ManufacturerCode { get; set; }
		public string Manufacturer { get; set; }
		[Display(Name = "User")]
		public int? UserId { get; set; }
		public string User { get; set; }
		public string FileNumber { get; set; }
		public string EmailDistribution { get; set; }

		//To view history of particular item
		public int? BugBoxId { get; set; }
		public int? EmailDistributionId { get; set; }
		public int? JurisdictionalDataId { get; set; }
		public int? ShippingDataId { get; set; }
		public int? SignatureId { get; set; }
		public int? SourceCodeStorageId { get; set; }
		public int? SubmissionId { get; set; }
	}
}