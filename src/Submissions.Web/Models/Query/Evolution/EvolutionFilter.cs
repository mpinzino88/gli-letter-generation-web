﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Submissions.Web.Models.Query.Evolution
{
	public class EvolutionFilter
	{
		public EvolutionFilter()
		{
			this.Jurisdictions = new List<SelectListItem>();
			this.NativeLanguages = new List<SelectListItem>();
			this.Documents = new List<SelectListItem>();
		}

		[Display(Name = "Jurisdictions")]
		public string[] JurisdictionIds { get; set; }

		[Display(Name = "Jurisdiction")]
		public IList<SelectListItem> Jurisdictions { get; set; }

		[Display(Name = "Native Languages")]
		public string[] NativeLanguageIds { get; set; }

		[Display(Name = "Native Language")]
		public IList<SelectListItem> NativeLanguages { get; set; }

		[Display(Name = "Documents")]
		public string[] DocumentIds { get; set; }

		[Display(Name = "Document")]
		public IList<SelectListItem> Documents { get; set; }
	}
}