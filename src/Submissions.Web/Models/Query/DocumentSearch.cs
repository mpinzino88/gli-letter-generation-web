﻿using Submissions.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace Submissions.Web.Models.Query
{
	public class DocumentSearch
	{
		[DisplayName("Letter Date")]
		public DateTime? DocumentDate { get; set; }
		[DisplayName("Letter Type")]
		public DocumentType? DocumentType { get; set; }
		[DisplayName("Version Type")]
		public DocumentVersionType? DocumentVersionType { get; set; }
		[DisplayName("Draft Date")]
		public DateTime? LatestDraftDate { get; set; }
		[DisplayName("Updated Date")]
		public DateTime? LatestDocumentDate { get; set; }
		[DisplayName("Original Draft Date")]
		public DateTime? OriginalDraftDate { get; set; }
		[DisplayName("Close Date")]
		public DateTime? OriginalDocumentDate { get; set; }
		public int? RegulatoryStatusId { get; set; }
		public int? StatusId { get; set; }
	}
}