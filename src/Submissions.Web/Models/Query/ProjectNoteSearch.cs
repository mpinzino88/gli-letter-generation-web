﻿namespace Submissions.Web.Models.Query
{
	public class ProjectNoteSearch
	{
		public int? ProjectId { get; set; }
		public int? DepartmentId { get; set; }
	}
}