﻿using Submissions.Common.Query;
using Submissions.Web.Models.Grids.Reports;
using System.Collections.Generic;
using System.Web.Mvc;

namespace Submissions.Web.Models.Query
{
	public class TaskSearchPartialViewModel : ISearchPartialViewModel<TaskSearch>
	{
		public TaskSearchPartialViewModel()
		{
			this.Filters = new TaskSearch();
			this.SortList = new TasksReportGrid().GetAvailableSorts();
		}

		public string PartialViewName { get { return "EditorTemplates/Query/TaskSearch"; } }
		public TaskSearch Filters { get; set; }
		public IList<SelectListItem> SortList { get; set; }
	}
}