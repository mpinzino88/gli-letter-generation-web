﻿using Submissions.Common;
using Submissions.Common.JQGrid;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Submissions.Web.Models.Query
{
	[Serializable]
	public class JurisdictionalDataSearch : ISort
	{
		public JurisdictionalDataSearch()
		{
			this.Manufacturers = new List<SelectListItem>();
			this.Jurisdictions = new List<SelectListItem>();
			this.Sort1 = string.Empty;
			this.Sort2 = string.Empty;
		}

		public int? SubmissionId { get; set; }
		[Display(Name = "Type")]
		public int? TypeId { get; set; }
		public string Type { get; set; }
		[Display(Name = "ID Number")]
		public string IdNumber { get; set; }
		public string Version { get; set; }
		[Display(Name = "Date Code")]
		public string DateCode { get; set; }
		[Display(Name = "Game Name")]
		public string GameName { get; set; }
		[Display(Name = "Chip Type")]
		public int? ChipTypeId { get; set; }
		public string ChipType { get; set; }
		public string Position { get; set; }
		[Display(Name = "Function")]
		public int? FunctionId { get; set; }
		public string Function { get; set; }
		[Display(Name = "Manufacturer")]
		public string[] ManufacturerCodes { get; set; }
		public IList<SelectListItem> Manufacturers { get; set; }
		[Display(Name = "Year")]
		public string Year { get; set; }
		[Display(Name = "File Number")]
		public string FileNumber { get; set; }
		[Display(Name = "Submit Date")]
		public DateTime? StartSubmitDate { get; set; }
		public DateTime? EndSubmitDate { get; set; }
		public int? RollingSubmitDateDays { get; set; }
		[Display(Name = "Receive Date")]
		public DateTime? StartReceiveDate { get; set; }
		public DateTime? EndReceiveDate { get; set; }
		public int? RollingReceiveDateDays { get; set; }
		[Display(Name = "Replaced With")]
		public string ReplaceWith { get; set; }
		[Display(Name = "Jurisdiction")]
		public string[] JurisdictionIds { get; set; }
		public IList<SelectListItem> Jurisdictions { get; set; }
		[Display(Name = "Status")]
		public string[] Statuses { get; set; }
		[Display(Name = "Conditionally Revoked")]
		public bool? ConditionalRevoke { get; set; }
		[Display(Name = "Draft Date")]
		public DateTime? StartDraftDate { get; set; }
		public DateTime? EndDraftDate { get; set; }
		public int? RollingDraftDateDays { get; set; }
		[Display(Name = "Approve Date")]
		public DateTime? StartCloseDate { get; set; }
		public DateTime? EndCloseDate { get; set; }
		public int? RollingCloseDateDays { get; set; }
		[Display(Name = "Amend Date")]
		public DateTime? StartUpdateDate { get; set; }
		public DateTime? EndUpdateDate { get; set; }
		public int? RollingUpdateDateDays { get; set; }
		[Display(Name = "Revoke Date")]
		public DateTime? StartRevokeDate { get; set; }
		public DateTime? EndRevokeDate { get; set; }
		public int? RollingRevokeDateDays { get; set; }
		[Display(Name = "Quote Date")]
		public DateTime? StartQuoteDate { get; set; }
		public DateTime? EndQuoteDate { get; set; }
		public int? RollingQuoteDateDays { get; set; }
		[Display(Name = "Authorize Date")]
		public DateTime? StartAuthorizeDate { get; set; }
		public DateTime? EndAuthorizeDate { get; set; }
		public int? RollingAuthorizeDateDays { get; set; }
		[Display(Name = "NU Date")]
		public DateTime? StartObsoleteDate { get; set; }
		public DateTime? EndObsoleteDate { get; set; }
		public int? RollingObsoleteDateDays { get; set; }

		[Display(Name = "Primary Sort")]
		public string Sort1 { get; set; }
		public SortOrder Sort1Order { get; set; }
		[Display(Name = "Secondary Sort")]
		public string Sort2 { get; set; }
		public SortOrder Sort2Order { get; set; }
	}
}