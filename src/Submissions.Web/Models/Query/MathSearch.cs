﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Submissions.Web.Models.Query
{
	public class MathSearch
	{
		public MathSearch()
		{
			LaboratoryIds = new List<string>();
			Laboratories = new List<SelectListItem>();
			ManufacturerGroupIds = new List<int>();
			ManufacturerGroups = new List<SelectListItem>();

		}
		[Display(Name = "Office")]
		public int? LaboratoryId { get; set; }
		public string Laboratory { get; set; }
		public List<int> ManufacturerGroupIds { get; set; }
		public List<SelectListItem> ManufacturerGroups { get; set; }
		public List<string> LaboratoryIds { get; set; }
		public List<SelectListItem> Laboratories { get; set; }
		[Display(Name = "Manufacturer")]
		public string ManufacturerShort { get; set; }
		public string Manufacturer { get; set; }
		[Display(Name = "FileNumber")]
		public string FileNumber { get; set; }

		[Display(Name = "Math Analyst")]
		public int MathAnalystId { get; set; }
		public string MathAnalyst { get; set; }
		[Display(Name = "Math Manager")]
		public int? MAManagerId { get; set; }
		public string MAManager { get; set; }

		public string Status { get; set; }
		public bool ExcludeMathQueueGroups { get; set; }

	}
}