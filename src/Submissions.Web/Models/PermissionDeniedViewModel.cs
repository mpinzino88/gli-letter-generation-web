﻿namespace Submissions.Web.Models
{
	public class PermissionDeniedViewModel
	{
		public Permission MissingPermission { get; set; }
	}
}