﻿namespace Submissions.Web.Models
{
	public class SubmissionSubTitle
	{
		public string FileNumber { get; set; }
		public string IdNumber { get; set; }
		public string GameName { get; set; }
		public string DateCode { get; set; }
		public string Function { get; set; }
	}
}