using Submissions.Common;
using System.Linq;
using System.Web.Mvc;

namespace Submissions.Web
{
	public static class Util
	{
		private static IUserContext UserContext
		{
			get
			{
				return DependencyResolver.Current.GetService<IUserContext>();
			}
		}

		public static bool HasAccess(params Role[] roles)
		{
			if (UserContext.RoleId == 0 || roles == null)
			{
				return false;
			}

			return roles.Contains(UserContext.Role);
		}

		public static bool HasAccess(Permission permission, AccessRight accessRight)
		{
			if (accessRight == AccessRight.None || UserContext.AccessRights.Count == 0 || !UserContext.AccessRights.ContainsKey(permission))
			{
				return false;
			}

			return UserContext.AccessRights[permission].HasFlag(accessRight);
		}

		public static bool HasAccess(Permission permission, AccessRight accessRight, params Role[] roles)
		{
			if (HasAccess(permission, accessRight) || HasAccess(roles))
			{
				return true;
			}

			return false;
		}

		public static bool HasAccess(Permission[] permissions, AccessRight accessRight, params Role[] roles)
		{
			foreach (var permission in permissions)
			{
				if (HasAccess(permission, accessRight))
				{
					return true;
				}
			}

			if (HasAccess(roles))
			{
				return true;
			}

			return false;
		}
	}
}