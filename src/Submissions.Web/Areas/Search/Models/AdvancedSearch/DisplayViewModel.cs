﻿using Submissions.Web.Models.Grids;
using Submissions.Web.Models.Query;

namespace Submissions.Web.Areas.Search.Models.AdvancedSearch
{
	public class DisplayViewModel
	{
		public DisplayViewModel()
		{
			this.AdvancedSearchGrid = new AdvancedSearchGrid();
			this.Filters = new AdvancedSearchSearch();
		}

		public AdvancedSearchGrid AdvancedSearchGrid { get; set; }
		public AdvancedSearchSearch Filters { get; set; }
	}
}