﻿using Submissions.Web.Models.Query;

namespace Submissions.Web.Areas.Search.Models.AdvancedSearch
{
	public class IndexViewModel
	{
		public IndexViewModel()
		{
			this.Filters = new AdvancedSearchSearch();
		}

		public AdvancedSearchSearch Filters { get; set; }
	}
}