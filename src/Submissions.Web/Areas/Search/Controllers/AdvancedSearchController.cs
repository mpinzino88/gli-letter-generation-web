﻿using GLI.EFCore.Submission;
using Submissions.Common;
using Submissions.Web.Areas.Search.Models.AdvancedSearch;
using System.Dynamic;
using System.Linq;
using System.Web.Mvc;

namespace Submissions.Web.Areas.Search.Controllers
{
	public class AdvancedSearchController : Controller
	{
		private readonly SubmissionContext _dbSubmission;
		private readonly IQueryService _queryService;

		public AdvancedSearchController(SubmissionContext dbSubmission, IQueryService queryService)
		{
			_dbSubmission = dbSubmission;
			_queryService = queryService;
		}

		public ActionResult Index()
		{
			var vm = new IndexViewModel();
			return View(vm);
		}

		[HttpPost]
		public ActionResult Display(IndexViewModel model)
		{
			var vm = new DisplayViewModel();
			vm.Filters = model.Filters;

			return View(vm);
		}

		[HttpGet]
		public ActionResult ShortCircuit(string fileNumber, string idNumber)
		{
			dynamic result = new ExpandoObject();

			if (!string.IsNullOrEmpty(fileNumber))
			{
				try
				{
					if (fileNumber.Count(x => x == '-') > 3)
					{
						var validFileNumber = new FileNumber(fileNumber);
						var fileNumberFormatted = validFileNumber.ToString();
						var manufacturerCode = validFileNumber.ManufacturerCode;

						var submissionHeaderId = _dbSubmission.SubmissionHeaders
							.Where(x => x.ManufacturerCode == manufacturerCode && x.FileNumber == fileNumberFormatted)
							.Select(x => x.Id)
							.SingleOrDefault();

						result.SubmissionHeaderId = submissionHeaderId;
					}
				}
				catch { }
			}

			if (!string.IsNullOrEmpty(idNumber))
			{
				var submissions = _dbSubmission.Submissions
					.Where(x => x.IdNumber == idNumber)
					.Select(x => new { x.Id, x.SubmissionHeaderId })
					.ToList();

				if (submissions.Count == 1)
				{
					result.SubmissionId = submissions.Select(x => x.Id).First();
				}
				else if (submissions.GroupBy(x => x.SubmissionHeaderId).Count() == 1)
				{
					result.SubmissionHeaderId = submissions.Select(x => x.SubmissionHeaderId).First();
				}
			}

			return Content(ComUtil.JsonEncodeCamelCase(result), "application/json");
		}
	}
}