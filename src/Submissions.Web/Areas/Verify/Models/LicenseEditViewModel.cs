﻿using Submissions.Common;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Submissions.Web.Areas.Verify.Models
{
	public class LicenseEditViewModel
	{
		public LicenseEditViewModel()
		{
			this.LicenseStatuses = new Collection<LicenseStatusGridRecord>();
			this.AvailableModules = new List<Module>();
			this.PostedModules = new PostedModules();
			this.SelectedModuleIds = new List<int>();
		}

		public Mode Mode { get; set; }
		public int Id { get; set; }
		[Required]
		public string Description { get; set; }
		[EmailAddress]
		public string Email { get; set; }
		[DisplayName("Product Code")]
		public Guid License { get; set; }
		[DisplayName("Licenses"), Required, Range(1, int.MaxValue)]
		public int NumOfLicenses { get; set; }
		public bool Paid { get; set; }
		[DisplayName("Paid Date"), DisplayFormat(DataFormatString = "{0:d}", ApplyFormatInEditMode = true)]
		public DateTime? PaidDate { get; set; }
		[DisplayName("Expiration Date"), DisplayFormat(DataFormatString = "{0:d}", ApplyFormatInEditMode = true)]
		public DateTime? ExpireDate { get; set; }
		public bool Advertisements { get; set; }
		public string Note { get; set; }
		public ICollection<LicenseStatusGridRecord> LicenseStatuses { get; set; }
		public int ActiveLicenseCount { get; set; }
		public bool HideProductCode { get; set; }
		[DisplayName("First Name")]
		public string FirstName { get; set; }
		[DisplayName("Last Name")]
		public string LastName { get; set; }
		public string Organization { get; set; }
		public string Address { get; set; }
		public string Address2 { get; set; }
		public string City { get; set; }
		public string State { get; set; }
		public string Zip { get; set; }
		public string Phone { get; set; }
		[DisplayName("Invoice #")]
		public string InvoiceNumber { get; set; }
		public IList<Module> AvailableModules { get; set; }
		public IList<int> SelectedModuleIds { get; set; }
		public PostedModules PostedModules { get; set; }
		public bool Promotional { get; set; }
		[DisplayName("Chargify Subscription ID")]
		public int? ChargifySubscriptionId { get; set; }
		[DisplayName("Chargify Site")]
		public string ChargifySite { get; set; }
	}

	public class LicenseStatusGridRecord
	{
		public Guid Id { get; set; }
		public bool Active { get; set; }
		[DisplayName("Registration Key")]
		public string RegistrationId { get; set; }
		public string Organization { get; set; }
		[DisplayName("GLI Access User")]
		public string GLIAccessUser { get; set; }
		public string FirstName { get; set; }
		public string LastName { get; set; }
		public string Phone { get; set; }
		public string Email { get; set; }
		[DisplayName("Request Date")]
		public DateTime RequestDateTime { get; set; }
		public string Name
		{
			get
			{
				return (FirstName + " " + LastName).Trim();
			}
		}
		[DisplayName("Version")]
		public string GLIVerifyVersionCode { get; set; }
	}

	public class Module
	{
		public int Id { get; set; }
		public string Code { get; set; }
		public string Description { get; set; }
		public string Type { get; set; }
		public bool IsSelected { get; set; }
	}

	public class PostedModules
	{
		public PostedModules()
		{
			this.ModuleIds = new string[] { };
		}

		public string[] ModuleIds { get; set; }
	}
}