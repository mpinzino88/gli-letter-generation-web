﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;
using Trirand.Web.Mvc;

namespace Submissions.Web.Areas.Verify.Models
{
	public class LicenseIndexViewModel
	{
		public LicenseIndexGridModel LicenseIndexGridModel;
	}

	#region Grid
	public class LicenseIndexGridModel
	{
		public LicenseIndexGridModel()
		{
			Grid = new JQGrid
			{
				Columns = new List<JQGridColumn>
				{
					new JQGridColumn
					{
						DataField = "Id",
						PrimaryKey = true,
						Editable = false,
						Visible = false
					},
					new JQGridColumn
					{
						DataField = "Description",
						HeaderText = "Description",
						DataType = typeof(string),
						Width = 200
					},
					new JQGridColumn
					{
						DataField = "NumOfLicenses",
						HeaderText = "Licenses",
						DataType = typeof(string),
						Width = 70
					},
					new JQGridColumn
					{
						DataField = "FirstName",
						HeaderText = "First Name",
						DataType = typeof(string),
					},
					new JQGridColumn
					{
						DataField = "LastName",
						HeaderText = "Last Name",
						DataType = typeof(string),
					},
					new JQGridColumn
					{
						DataField = "Organization",
						HeaderText = "Organization",
						DataType = typeof(string),
					},
					new JQGridColumn
					{
						DataField = "Phone",
						HeaderText = "Phone",
						DataType = typeof(string),
					},
					new JQGridColumn
					{
						DataField = "InvoiceNumber",
						HeaderText = "Invoice",
						DataType = typeof(string),
					},
					new JQGridColumn
					{
						DataField = "PaidDate",
						HeaderText = "Paid Date",
						DataFormatString = "{0:d}",
						DataType = typeof(DateTime),
						Width = 90
					},
					new JQGridColumn
					{
						DataField = "ExpireDate",
						HeaderText = "Expiration Date",
						DataFormatString = "{0:d}",
						DataType = typeof(DateTime),
						Width = 100
					},
                    new JQGridColumn
                    {
                        DataField = "License",
                        HeaderText = "Product Code",
                        DataType = typeof(string)
                    },
                    new JQGridColumn
					{
						DataField = "Edit",
						HeaderText = " ",
						Formatter = new CustomFormatter{FormatFunction = "EditLink"},
						Searchable = false,
						Sortable = false,
						Width = 50,
						TextAlign = Trirand.Web.Mvc.TextAlign.Right,
						CssClass = "padright"
					}
                }
			};

			Grid.ID = "LicenseIndexGrid";
			Grid.DataUrl = new UrlHelper(((MvcHandler)HttpContext.Current.Handler).RequestContext).Action("LoadLicenseIndexGrid");
			Grid.AutoWidth = true;
			Grid.Width = Unit.Percentage(100);
			Grid.Height = Unit.Percentage(100);
			Grid.ToolBarSettings = new ToolBarSettings
			{
				ShowRefreshButton = true,
				ToolBarPosition = ToolBarPosition.Top,
				ShowSearchToolBar = true
			};
			Grid.PagerSettings = new Trirand.Web.Mvc.PagerSettings
			{
				PageSize = 100,
				PageSizeOptions = "[]"
			};
			Grid.AppearanceSettings = new AppearanceSettings
			{
				AlternateRowBackground = true,
				HighlightRowsOnHover = true,
				ShowFooter = false
			};
		}

		public JQGrid Grid { get; set; }
	}

	public class LicenseIndexGridRecord
	{
		public int Id { get; set; }
		public string Description { get; set; }
		public int NumOfLicenses { get; set; }
		public DateTime? PaidDate { get; set; }
		public DateTime? ExpireDate { get; set; }
		public string Edit { get; set; }
		public bool HideProductCode { get; set; }
		public string FirstName { get; set; }
		public string LastName { get; set; }
		public string Organization { get; set; }
		public string Phone { get; set; }
		public string InvoiceNumber { get; set; }
        public string License { get; set; }
	}
	#endregion
}