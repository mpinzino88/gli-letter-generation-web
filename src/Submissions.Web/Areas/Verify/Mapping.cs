﻿using AutoMapper;
using EF.GLiCloud;
using Submissions.Web.Areas.Verify.Models;

namespace Submissions.Web.Mappings
{
	public class VerifyProfile : Profile
	{
		public VerifyProfile()
		{
			CreateMap<GLIVerifyLicenseGroup, LicenseIndexGridRecord>()
				.ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.LicenseGroupID))
				.ForMember(dest => dest.Edit, opt => opt.Ignore());
			CreateMap<GLIVerifyLicenseGroup, LicenseEditViewModel>()
				.ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.LicenseGroupID))
				.ForMember(dest => dest.LicenseStatuses, opt => opt.MapFrom(src => src.GLIVerifyLicenseStatuses))
				.ForMember(dest => dest.Mode, opt => opt.Ignore())
				.ForMember(dest => dest.ActiveLicenseCount, opt => opt.Ignore())
				.ForMember(dest => dest.AvailableModules, opt => opt.Ignore())
				.ForMember(dest => dest.SelectedModuleIds, opt => opt.Ignore())
				.ForMember(dest => dest.PostedModules, opt => opt.Ignore());
			CreateMap<LicenseEditViewModel, GLIVerifyLicenseGroup>()
				.ForMember(dest => dest.LicenseGroupID, opt => opt.MapFrom(src => src.Id))
				.ForMember(dest => dest.IsGLI, opt => opt.Ignore())
				.ForMember(dest => dest.LicenseType, opt => opt.Ignore())
				.ForMember(dest => dest.CouponCode, opt => opt.Ignore())
				.ForMember(dest => dest.PurchaseSource, opt => opt.Ignore())
				.ForMember(dest => dest.Country, opt => opt.Ignore())
				.ForMember(dest => dest.GLIVerifyLicenseStatuses, opt => opt.Ignore());
			CreateMap<GLIVerifyLicenseStatus, LicenseStatusGridRecord>()
				.ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.StatusKey))
				.ForMember(dest => dest.GLIVerifyVersionCode, opt => opt.MapFrom(src => src.GLIVerifyVersion.VersionText));
			CreateMap<GLIVerifyModule, Module>()
				.ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.ModuleId))
				.ForMember(dest => dest.Code, opt => opt.MapFrom(src => src.Module))
				.ForMember(dest => dest.IsSelected, opt => opt.Ignore());
		}
	}
}