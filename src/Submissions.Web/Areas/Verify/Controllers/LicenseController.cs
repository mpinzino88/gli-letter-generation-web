﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using EF.GLiCloud;
using Submissions.Common;
using Submissions.Common.Email;
using Submissions.Web.Areas.Verify.Models;
using Submissions.Web.Utility.Email;
using Submissions.Web.VerifyService;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net.Mail;
using System.Web.Mvc;
using Z.EntityFramework.Plus;

namespace Submissions.Web.Areas.Verify.Controllers
{
	[AuthorizeUser(Permission = Permission.VerifyLicenseMgmt, HasAccessRight = AccessRight.Edit)]
	public class LicenseController : Controller
	{
		private readonly IGLiCloudContext _dbCloud;
		private readonly IEmailService _emailService;
		private readonly IConfigContext _configContext;
		private GLIVerifyLicenseGroup _item;

		public LicenseController(IGLiCloudContext dbCloud, IEmailService emailService, IConfigContext configContext)
		{
			_dbCloud = dbCloud;
			_emailService = emailService;
			_configContext = configContext;
		}

		public ActionResult Index()
		{
			var results = new LicenseIndexViewModel();
			results.LicenseIndexGridModel = new LicenseIndexGridModel();

			return View(results);
		}

		public ActionResult Edit(int? id, Mode mode)
		{
			LicenseEditViewModel result;

			switch (mode)
			{
				case Mode.Add:
					_item = new GLIVerifyLicenseGroup() { License = Guid.NewGuid() };
					result = Mapper.Map<LicenseEditViewModel>(_item);
					result.SelectedModuleIds = _dbCloud.GLIVerifyModules.Where(x => x.Module != "MISSISSIPPI_DB").Select(x => x.ModuleId).ToList();
					break;
				case Mode.Edit:
					result = _dbCloud.GLIVerifyLicenseGroups.Where(x => x.LicenseGroupID == id).ProjectTo<LicenseEditViewModel>().Single();
					result.LicenseStatuses = (from x in result.LicenseStatuses
											  orderby x.RequestDateTime descending, x.Organization, x.LastName
											  select x).ToList();
					result.ActiveLicenseCount = result.LicenseStatuses.Where(x => x.Active).Count();
					result.SelectedModuleIds = _dbCloud.GLIVerifyLicenseGroupModules.Where(x => x.LicenseGroupId == id).Select(x => x.ModuleId).ToList();
					break;
				default:
					throw new Exception("Invalid Edit Mode");
			}

			result.AvailableModules = _dbCloud.GLIVerifyModules.Select(x => x).OrderBy(x => x.Module).ProjectTo<Module>().ToList();
			result.Mode = mode;

			return View(result);
		}

		[HttpPost]
		public ActionResult Edit(int? id, LicenseEditViewModel model)
		{
			_dbCloud.GLIVerifyLicenseGroupModules.Where(x => x.LicenseGroupId == id).Delete();

			switch (model.Mode)
			{
				case Mode.Add:
					_item = Mapper.Map<GLIVerifyLicenseGroup>(model);
					_dbCloud.GLIVerifyLicenseGroups.Add(_item);
					break;
				case Mode.Delete:
					_item = new GLIVerifyLicenseGroup() { LicenseGroupID = (int)id };
					_dbCloud.Entry(_item).State = EntityState.Deleted;
					break;
				case Mode.Edit:
					_item = Mapper.Map<GLIVerifyLicenseGroup>(model);
					_dbCloud.Entry(_item).State = EntityState.Modified;
					break;
				default:
					throw new Exception("Invalid Edit Mode");
			}

			if (model.Mode != Mode.Delete)
			{
				foreach (var item in model.PostedModules.ModuleIds)
				{
					var moduleId = Convert.ToInt32(item);
					_dbCloud.GLIVerifyLicenseGroupModules.Add(new GLIVerifyLicenseGroupModule { LicenseGroupId = (int)id, ModuleId = moduleId });
				}
			}

			_dbCloud.SaveChanges();

			return RedirectToAction("Index");
		}

		public ActionResult Activate(Guid id, int licenseGroupId)
		{
			var item = _dbCloud.GLIVerifyLicenseStatuses.Find(id);
			item.Active = true;

			_dbCloud.SaveChanges();

			return RedirectToAction("Edit", new { id = licenseGroupId, mode = Mode.Edit });
		}

		public ActionResult EmailGroupLicense(int id, string emailTo)
		{
			var licenseGroup = _dbCloud.GLIVerifyLicenseGroups.Find(id);
			var emailViewModel = Mapper.Map<VerifyGroupLicenseEmail>(licenseGroup);

			var mailMsg = new MailMessage("sales@kobetron.com", emailTo);
			mailMsg.Subject = "Verify+ Subscription";
			mailMsg.Body = _emailService.RenderTemplate(EmailType.VerifyGroupLicense.ToString(), emailViewModel);

			var embeddedImg = Server.MapPath("~/Img/VerifyLogo.png");
			_emailService.EmbeddedImages = new List<LinkedResource> { new LinkedResource(embeddedImg) { ContentId = "VerifyLogo" } };
			_emailService.Send(mailMsg);

			return Content("ok");
		}

		[HttpPost]
		public ActionResult DeleteRegistration(Guid licenseStatusId, int licenseGroupId)
		{
			var licenseStatus = new GLIVerifyLicenseStatus { StatusKey = licenseStatusId };
			_dbCloud.Entry(licenseStatus).State = EntityState.Deleted;
			_dbCloud.SaveChanges();

			return RedirectToAction("Edit", new { id = licenseGroupId, mode = Mode.Edit });
		}

		public ActionResult EmailRegistration(Guid id, string emailTo, bool activate)
		{
			var verifyService = new GLIVerifyLicensing();
			var result = verifyService.ResendLicenseKey(id, emailTo, _configContext.VerifySerivceAuthKey);

			return Content("ok");
		}

		#region Load Grid Data
		[OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
		public JsonResult LoadLicenseIndexGrid()
		{
			var regIDSearch = false;
			regIDSearch = Boolean.TryParse(this.Request.Params["registrationIDSearch"], out regIDSearch);
			IQueryable<GLIVerifyLicenseGroup> query;
			if (regIDSearch)
			{
				string searchText = this.Request.Params["searchText"];

				//IQueryable<GLIVerifyLicenseGroup> groupsWithRegID = (from statuses in _dbCloud.GLIVerifyLicenseStatuses
				//                       join groups in _dbCloud.GLIVerifyLicenseGroups on statuses.LicenseGroupId equals groups.LicenseGroupID
				//                       where statuses.RegistrationId.ToUpper().Contains(searchText) && groups != null
				//                       select groups).AsNoTracking().AsQueryable();
				var groupIds = _dbCloud.GLIVerifyLicenseStatuses.Where(x => x.RegistrationId.ToUpper().Contains(searchText)).Select(x => x.LicenseGroupId).ToList();
				query = _dbCloud.GLIVerifyLicenseGroups.Where(x => groupIds.Contains(x.LicenseGroupID)).AsNoTracking().AsQueryable();
			}
			else
			{
				query = _dbCloud.GLIVerifyLicenseGroups.AsNoTracking().AsQueryable();
			}
			var results = query.ProjectTo<LicenseIndexGridRecord>();

			var gridModel = new LicenseIndexGridModel();

			return gridModel.Grid.DataBind(results);
		}
		#endregion
	}
}