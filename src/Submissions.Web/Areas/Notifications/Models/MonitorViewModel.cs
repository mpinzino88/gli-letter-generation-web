﻿using GLI.Notification.Monitor.Models;
using System.Collections.Generic;

namespace Submissions.Web.Areas.Notifications.Models
{
	public class MonitorViewModel
	{
		public MonitorViewModel()
		{
			this.Messages = new List<Message>();
		}

		public IList<Message> Messages { get; set; }
	}

	public class Message
	{
		public Message()
		{
			this.Notification = new DashboardMessage();
			this.messagesAvailable = false;
		}

		public DashboardMessage Notification { get; set; }
		public bool messagesAvailable { get; set; }
	}
}