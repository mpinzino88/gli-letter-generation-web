﻿using GLI.Notification.Monitor.Models;
using Submissions.Core;
using Submissions.Web.Areas.Notifications.Models;
using System.Web.Mvc;
using System.Web.UI;

namespace Submissions.Web.Areas.Notifications.Controllers
{
	public class MonitorController : Controller
	{
		private readonly IUserContext _userContext;
		private readonly INotificationMonitorService _notificationMonitorService;

		public MonitorController(IUserContext userContext, INotificationMonitorService notificationMonitorService)
		{
			_userContext = userContext;
			_notificationMonitorService = notificationMonitorService;
		}

		public ActionResult Index()
		{
			var vm = new MonitorViewModel();

			for (int i = 0; i < 10; i++)
			{
				var message = new Message();

				var notification = _notificationMonitorService.GetNotification<DashboardMessage>("dashboard" + _userContext.User.Id);
				if (notification.MessageAvailable)
				{
					message.messagesAvailable = true;
					message.Notification.Subject = notification.Message.Body.Subject;
					message.Notification.EmailContent = notification.Message.Body.EmailContent;
					vm.Messages.Add(message);
				}
			}

			return View(vm);
		}

		[HttpPost]
		[OutputCache(Duration = 600, VaryByParam = "*", VaryByCustom = "User", Location = OutputCacheLocation.Any, NoStore = true)]
		public JsonResult GetQueueCount()
		{
			var queueCount = _notificationMonitorService.QueueCount("dashboard" + _userContext.User.Id);

			return Json(queueCount);
		}
	}
}