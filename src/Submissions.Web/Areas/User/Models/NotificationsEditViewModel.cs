﻿using Submissions.Common;
using Submissions.Web.Models.LookupsAjax;
using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace Submissions.Web.Areas.User.Models
{
	public class NotificationsEditViewModel
	{
		public NotificationsEditViewModel()
		{
			this.Subscriptions = new List<Subscription>();
			this.DefaultSubscription = new Subscription();
			this.DefaultSubscription.AdditionalFilters = "[]";
			this.DefaultSubscription.Jurisdictons = new List<SelectListItem>();
			this.DefaultSubscription.ManagerFilter = new UsersFilter { ManagerSearch = true };
		}

		public int UserId { get; set; }
		public string UserName { get; set; }
		public string NewSubscription { get; set; }
		public int NewSubscriptionId { get; set; }
		public string ImportUser { get; set; }
		public int ImportUserId { get; set; }
		public IList<Subscription> Subscriptions { get; set; }
		public Subscription DefaultSubscription { get; set; }
	}
	public class Subscription
	{
		public Subscription()
		{
			this.Id = Guid.NewGuid();
			this.Communications = LookupsStandard.ConvertEnum<SubscriptionCommunication>(useDescriptionAsValue: false, showBlank: true);
			this.SelectedCommunication = new List<string>();
			this.Jurisdictons = new List<SelectListItem>();
			this.ManagerFilter = new UsersFilter { ManagerSearch = true };
			this.YearOptions = LookupsStandard.Years;
		}

		public Guid Id { get; set; }
		public int UserSubscriptionId { get; set; }
		public int SubscriptionId { get; set; }
		public string Name { get; set; }
		public string Description { get; set; }
		public string Communication { get; set; }
		public string SubscriptionGroup { get; set; }
		public IEnumerable<string> SelectedCommunication { get; set; }
		public IEnumerable<SelectListItem> Communications { get; set; }
		public string AdditionalFilters { get; set; }
		public IList<SelectListItem> Jurisdictons { get; set; }
		public string[] SelectedJurisdicton { get; set; }
		public int? ManagerId { get; set; }
		public string SelectedManager { get; set; }
		public UsersFilter ManagerFilter { get; set; }
		public string ManufacturerCode { get; set; }
		public string SelectedManufacturer { get; set; }
		public string SubmissionType { get; set; }
		public string SelectedSubmissionType { get; set; }
		public int SelectedYear { get; set; }
		public IList<SelectListItem> YearOptions { get; set; }
		public string SequenceNumber { get; set; }
		public string UserAdditionalFilters { get; set; }
	}

	public class Communication
	{
		public bool Email { get; set; }
		public bool Dashboard { get; set; }
	}

	public class Filters
	{
		public string TableName { get; set; }
		public string ColumnName { get; set; }
		public string[] Values { get; set; }
		public List<FilterValues> FilterValues { get; set; }
	}

	public class FilterValues
	{
		public string Relation { get; set; }
		public string Value { get; set; }
	}
}