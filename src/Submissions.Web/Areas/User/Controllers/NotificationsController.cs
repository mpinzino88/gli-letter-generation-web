﻿extern alias ZEFCore;
using GLI.EFCore.Submission;
using Newtonsoft.Json;
using Submissions.Common;
using Submissions.Web.Areas.User.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using ZEFCore::Z.EntityFramework.Plus;

namespace Submissions.Web.Areas.User.Controllers
{
	public class NotificationsController : Controller
	{
		private readonly SubmissionContext _dbSubmission;
		private readonly IUserContext _userContext;

		public NotificationsController(SubmissionContext dbSubmission, IUserContext userContext)
		{
			_dbSubmission = dbSubmission;
			_userContext = userContext;
		}

		public ActionResult Index(int? id)
		{
			var vm = new NotificationsEditViewModel();
			id = (id == null) ? _userContext.User.Id : (int)id;
			var user = _dbSubmission.trLogins.Find(id);
			vm.UserId = (int)id;
			vm.UserName = user.FName + " " + user.LName;

			vm.Subscriptions = GetSubscriptions((int)id);
			vm.NewSubscription = "";
			vm.ImportUser = "";

			return View(vm);
		}

		[HttpPost]
		public ActionResult ImportSubscriptions(int importUserId, int userId)
		{
			var subscriptions = GetSubscriptions(importUserId);
			var user = _dbSubmission.trLogins.Find(userId);
			foreach (Models.Subscription subscription in subscriptions)
			{
				var communication = new Communication { Email = (subscription.SelectedCommunication.Contains("Email")), Dashboard = (subscription.SelectedCommunication.Contains("Dashboard")) };
				var userSubscription = new UserSubscription { UserId = userId, EmailId = user.Email, Delimiter = "|", AdditionalFilters = subscription.UserAdditionalFilters, AdditionalInfo = "[]", SubscriptionId = subscription.SubscriptionId, Active = subscription.SelectedCommunication.Any(), Communication = JsonConvert.SerializeObject(communication) };
				_dbSubmission.UserSubscriptions.Add(userSubscription);
			}
			_dbSubmission.SaveChanges();

			return Json(new { redirectToUrl = Url.Action("Index", "Notifications") });
		}

		[HttpPost]
		public ActionResult Edit(NotificationsEditViewModel vm, Models.Subscription subscription)
		{
			var user = _dbSubmission.trLogins.Find(vm.UserId);
			var subTypes = _dbSubmission.tbl_lst_SubmissionType.Where(x => x.Active).Select(x => new { x.Id, x.Code }).ToList();
			var communication = new Communication { Email = (subscription.SelectedCommunication.Contains("Email")), Dashboard = (subscription.SelectedCommunication.Contains("Dashboard")) };
			var filters = new List<Filters>();
			if ((subscription.SelectedJurisdicton != null) || (subscription.ManagerId != null) || (subscription.ManufacturerCode != null) || (subscription.SubmissionType != null) || (subscription.SelectedYear != 0) || (subscription.SequenceNumber != null))
			{
				if (subscription.SelectedJurisdicton != null)
				{
					var jurFilterValues = new List<FilterValues>();
					foreach (var jur in subscription.SelectedJurisdicton)
					{
						jurFilterValues.Add(new FilterValues { Relation = "=", Value = jur });
					}
					var jurisdictionFilter = new Filters { TableName = "tbl_lst_fileJuris", ColumnName = "Jurisdiction", Values = subscription.SelectedJurisdicton, FilterValues = jurFilterValues };
					filters.Add(jurisdictionFilter);
				}

				if (subscription.ManagerId != null)
				{
					var managerFilterValues = new List<FilterValues>();
					managerFilterValues.Add(new FilterValues { Relation = "=", Value = subscription.ManagerId.ToString() });

					var managerFilter = new Filters { TableName = "trLogin", ColumnName = "Manager", Values = new[] { subscription.ManagerId.ToString() }, FilterValues = managerFilterValues };
					filters.Add(managerFilter);
				}

				if (subscription.ManufacturerCode != null)
				{
					var manufFilterValues = new List<FilterValues>();
					manufFilterValues.Add(new FilterValues { Relation = "=", Value = subscription.ManufacturerCode });

					var manufacturerFilter = new Filters { TableName = "tbl_lst_fileManu", ColumnName = "Manufacturer", Values = new[] { subscription.ManufacturerCode }, FilterValues = manufFilterValues };
					filters.Add(manufacturerFilter);
				}

				if (subscription.SequenceNumber != null)
				{
					var sequenceNumberFilterValues = new List<FilterValues>();
					sequenceNumberFilterValues.Add(new FilterValues { Relation = "=", Value = subscription.SequenceNumber });

					var sequenceNumberFilter = new Filters { TableName = "Submissions", ColumnName = "SequenceNumber", Values = new[] { subscription.SequenceNumber }, FilterValues = sequenceNumberFilterValues };
					filters.Add(sequenceNumberFilter);
				}

				if (subscription.SubmissionType != null)
				{
					var subTypeFilterValues = new List<FilterValues>();
					var subTypeString = subTypes.Where(x => x.Id.ToString() == subscription.SubmissionType).Select(x => x.Code).FirstOrDefault();
					subTypeFilterValues.Add(new FilterValues { Relation = "=", Value = subTypeString });

					var subTypeFilter = new Filters { TableName = "tbl_lst_SubmissionType", ColumnName = "SubmissionType", Values = new[] { subTypeString }, FilterValues = subTypeFilterValues };
					filters.Add(subTypeFilter);
				}

				if (subscription.SelectedYear != 0)
				{
					var yearFilterValues = new List<FilterValues>();
					var yearString = subscription.SelectedYear.ToString();
					yearFilterValues.Add(new FilterValues { Relation = "=", Value = yearString.Substring(yearString.Length - 2) });

					var yearFilter = new Filters { TableName = "Submissions", ColumnName = "Year", Values = new[] { subscription.SelectedYear.ToString() }, FilterValues = yearFilterValues };
					filters.Add(yearFilter);
				}

				var userSubscription = new UserSubscription { UserId = vm.UserId, EmailId = user.Email, Delimiter = "|", AdditionalFilters = JsonConvert.SerializeObject(filters), AdditionalInfo = "[]", SubscriptionId = subscription.SubscriptionId, Active = subscription.SelectedCommunication.Any(), Communication = JsonConvert.SerializeObject(communication) };

				if (subscription.UserSubscriptionId > 0)
				{
					UserSubscription subscriptionToUpdate = _dbSubmission.UserSubscriptions.Where(x => x.Id == subscription.UserSubscriptionId).SingleOrDefault();
					subscriptionToUpdate.UserId = userSubscription.UserId;
					subscriptionToUpdate.EmailId = userSubscription.EmailId;
					subscriptionToUpdate.AdditionalFilters = userSubscription.AdditionalFilters;
					subscriptionToUpdate.SubscriptionId = userSubscription.SubscriptionId;
					subscriptionToUpdate.Active = userSubscription.Active;
					subscriptionToUpdate.Communication = userSubscription.Communication;
				}
				else
				{
					_dbSubmission.UserSubscriptions.Add(userSubscription);
				}
			}

			_dbSubmission.SaveChanges();
			return Json(new { redirectToUrl = Url.Action("Index", "Notifications") });
		}

		public JsonResult CheckSequenceNumber(string sequenceNumber)
		{
			var exists = _dbSubmission.Submissions.Any(x => x.Sequence == sequenceNumber);

			return Json(exists, JsonRequestBehavior.AllowGet);
		}

		[HttpPost]
		public ActionResult LoadSubscription(int subscriptionId)
		{
			var subscription = _dbSubmission.Subscriptions
								.Where(x => x.Id == subscriptionId)
								.Select(x => new Models.Subscription
								{
									Id = Guid.NewGuid(),
									SubscriptionId = x.Id,
									Name = x.Name,
									Description = x.Description,
									AdditionalFilters = x.AdditionalFilters
								});

			return Content(ComUtil.JsonEncodeCamelCase(subscription), "application/json");
		}

		private IList<Models.Subscription> GetSubscriptions(int userId)
		{
			var subscriptions = _dbSubmission.UserSubscriptions
				.Where(x => x.UserId == userId)
				.Select(x => new Models.Subscription
				{
					UserSubscriptionId = x.Id,
					SubscriptionId = x.SubscriptionId,
					Name = x.Subscription.Name,
					Description = x.Subscription.Description,
					SubscriptionGroup = x.Subscription.SubscriptionGroup.Name,
					Communication = x.Communication,
					AdditionalFilters = x.Subscription.AdditionalFilters,
					UserAdditionalFilters = x.AdditionalFilters
				})
				.OrderBy(x => x.SubscriptionGroup)
				.ThenBy(x => x.Name)
				.ToList();

			foreach (var subscription in subscriptions)
			{
				if (subscription.Communication != null)
				{
					var jsCommunication = JsonConvert.DeserializeObject<Communication>(subscription.Communication);

					foreach (var communication in subscription.Communications)
					{
						if (jsCommunication.Dashboard && (communication.Value == SubscriptionCommunication.Dashboard.ToString()))
						{
							communication.Selected = true;
							subscription.SelectedCommunication = subscription.SelectedCommunication.Concat(new[] { SubscriptionCommunication.Dashboard.ToString() });
						}

						if (jsCommunication.Email && (communication.Value == SubscriptionCommunication.Email.ToString()))
						{
							communication.Selected = true;
							subscription.SelectedCommunication = subscription.SelectedCommunication.Concat(new[] { SubscriptionCommunication.Email.ToString() });
						}
					}
				}

				if (subscription.UserAdditionalFilters != null)
				{
					var jsAdditionalFilters = JsonConvert.DeserializeObject<List<Filters>>(subscription.UserAdditionalFilters);

					foreach (var filter in jsAdditionalFilters)
					{
						if (filter.ColumnName == "Jurisdiction")
						{
							subscription.SelectedJurisdicton = filter.Values;

							var jur = _dbSubmission.tbl_lst_fileJuris
										.Where(x => filter.Values.Contains(x.Id))
										.Select(x => new SelectListItem { Value = x.Id.Trim(), Text = x.Name, Selected = true })
										.ToList();

							subscription.Jurisdictons = jur;
						}

						if (filter.ColumnName == "Manager")
						{
							var manager = _dbSubmission.trLogins
											.Where(x => filter.Values.Contains(x.Id.ToString()))
											.Select(x => x.FName + " " + x.LName)
											.FirstOrDefault();

							subscription.SelectedManager = manager;
							subscription.ManagerId = Int32.Parse(filter.Values.FirstOrDefault());
						}

						if (filter.ColumnName == "Manufacturer")
						{
							var manufacturerCodes = filter.Values;

							var manuf = _dbSubmission.tbl_lst_fileManu
											.Where(x => manufacturerCodes.Contains(x.Code))
											.Select(x => x.Description)
											.FirstOrDefault();

							subscription.SelectedManufacturer = manuf;
							subscription.ManufacturerCode = filter.Values.FirstOrDefault();
						}

						if (filter.ColumnName == "SequenceNumber")
						{
							subscription.SequenceNumber = filter.Values.FirstOrDefault();
						}

						if (filter.ColumnName == "SubmissionType")
						{
							var submissionType = filter.Values;

							var subType = _dbSubmission.tbl_lst_SubmissionType
											.Where(x => submissionType.Contains(x.Code))
											.Select(x => new { x.Id, Text = x.Code + " - " + x.Description })
											.FirstOrDefault();

							subscription.SelectedSubmissionType = subType.Text;
							subscription.SubmissionType = subType.Id.ToString();
						}

						if (filter.ColumnName == "Year")
						{
							subscription.SelectedYear = Int32.Parse(filter.Values.FirstOrDefault());
						}
					}
				}
			}

			return subscriptions;
		}

		[HttpPost]
		public ActionResult Delete(int id)
		{
			_dbSubmission.UserSubscriptions.Where(x => x.Id == id).Delete();
			_dbSubmission.SaveChanges();
			return Json(new { redirectToUrl = Url.Action("Index", "Notifications") });
		}
	}
}