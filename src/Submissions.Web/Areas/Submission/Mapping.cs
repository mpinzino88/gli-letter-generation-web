using AutoMapper;
using GLI.EFCore.Submission;
using Submissions.Common;
using Submissions.Web.Areas.Submission.Models.MachineCertificate;
using Submissions.Web.Areas.Submission.Models.Submission;
using Submissions.Web.Areas.Submission.Models.Submission.DetailPartialViews;
using System;
using System.Linq;

namespace Submissions.Web.Mappings
{
	public class SubmissionProfile : Profile
	{
		public SubmissionProfile()
		{
			DateTime d;

			CreateMap<Submission, Component>()
				.ForMember(dest => dest.Code, opt => opt.MapFrom(src => src.DateCode))
				.ForMember(dest => dest.PartName, opt => opt.MapFrom(src => src.GameName))
				.ForMember(dest => dest.PartNumber, opt => opt.MapFrom(src => src.PartNumber))
				.ForMember(dest => dest.Jurisdiction, opt => opt.MapFrom(src => src.JurisdictionId));
			CreateMap<ShippingData, Areas.Submission.Models.Shipment.EditViewModel>()
				.ForMember(dest => dest.CancelButtonUrl, opt => opt.Ignore())
				.ForMember(dest => dest.ChipCount, opt => opt.MapFrom(src => Convert.ToInt32(src.ChipCount)))
				.ForMember(dest => dest.CourierId, opt => opt.Ignore())
				.ForMember(dest => dest.DiskCount, opt => opt.MapFrom(src => Convert.ToInt32(src.DiskCount)))
				.ForMember(dest => dest.Mode, opt => opt.Ignore())
				.ForMember(dest => dest.ProcessDate, opt => opt.MapFrom(src => DateTime.TryParse(src.ProcessDate, out d) ? d.ToShortDateString() : src.ProcessDate))
				.ForMember(dest => dest.ShipmentRecipientId, opt => opt.Ignore())
				.ForMember(dest => dest.ShipmentRecipient, opt => opt.MapFrom(src => src.ShippingRecipientCode))
				.ForMember(dest => dest.SoftwareCheckUser, opt => opt.MapFrom(src => src.SoftwareCheckUser.FName + " " + src.SoftwareCheckUser.LName))
				.ForMember(dest => dest.Source, opt => opt.Ignore())
				.ForMember(dest => dest.SubmissionId, opt => opt.Ignore());
			CreateMap<ShippingData, ShipmentViewModel>()
				.ForMember(dest => dest.ChipCount, opt => opt.MapFrom(src => Convert.ToInt32(src.ChipCount)))
				.ForMember(dest => dest.DiskCount, opt => opt.MapFrom(src => Convert.ToInt32(src.DiskCount)))
				.ForMember(dest => dest.ProcessDate, opt => opt.MapFrom(src => DateTime.TryParse(src.ProcessDate, out d) ? d.ToShortDateString() : src.ProcessDate))
				.ForMember(dest => dest.SoftwareCheckUser, opt => opt.MapFrom(src => src.SoftwareCheckUser.FName + " " + src.SoftwareCheckUser.LName));
			CreateMap<ShippingData, Areas.Submission.Models.Shipment.ShipmentViewModel>()
				.ForMember(dest => dest.ProcessDate, opt => opt.MapFrom(src => DateTime.TryParse(src.ProcessDate, out d) ? d.ToShortDateString() : src.ProcessDate));
			CreateMap<Submission, SubmissionSelection>()
				.ForMember(dest => dest.Selected, opt => opt.Ignore())
				.ForMember(dest => dest.ExtraInfoDisplay, opt => opt.Ignore());
			CreateMap<Submission, Areas.Submission.Models.SubmissionBulkUpdate.ComponentViewModel>()
				.ForMember(dest => dest.IsChecked, opt => opt.Ignore());
			CreateMap<Submission, SubmissionDetailViewModel>()
				.ForMember(dest => dest.AddUser, opt => opt.MapFrom(src => src.AddUser.FName + " " + src.AddUser.LName))
				.ForMember(dest => dest.Cabinet, opt => opt.MapFrom(src => src.Cabinet.Cabinet))
				.ForMember(dest => dest.Complexity, opt => opt.Ignore())
				.ForMember(dest => dest.ContractType, opt => opt.MapFrom(src => src.ContractType.Code + " - " + src.ContractType.Description))
				.ForMember(dest => dest.Currency, opt => opt.MapFrom(src => src.Currency.Code + " - " + src.Currency.Description))
				.ForMember(dest => dest.EditUser, opt => opt.MapFrom(src => src.EditUser.FName + " " + src.EditUser.LName))
				.ForMember(dest => dest.Market, opt => opt.Ignore())
				.ForMember(dest => dest.IssueCount, opt => opt.Ignore())
				.ForMember(dest => dest.JurisdictionCount, opt => opt.Ignore())
				.ForMember(dest => dest.MachineCertCount, opt => opt.Ignore())
				.ForMember(dest => dest.MasterFileLabel, opt => opt.Ignore())
				.ForMember(dest => dest.MasterFileText, opt => opt.Ignore())
				.ForMember(dest => dest.MemoCount, opt => opt.Ignore())
				.ForMember(dest => dest.ProductCount, opt => opt.Ignore())
				.ForMember(dest => dest.NVLabNumber, opt => opt.MapFrom(src => (src.JurisdictionalData.Where(x => x.JurisdictionId == ((int)CustomJurisdiction.NevadaITL).ToString())).FirstOrDefault().CertNumber.CertNumber))
				.ForMember(dest => dest.ProductType, opt => opt.Ignore())
				.ForMember(dest => dest.ProjectId, opt => opt.Ignore())
				.ForMember(dest => dest.RNGDataCount, opt => opt.Ignore())
				.ForMember(dest => dest.ShipmentCount, opt => opt.Ignore())
				.ForMember(dest => dest.ShowNVLabNumber, opt => opt.MapFrom(src => (src.JurisdictionalData.Any(x => x.JurisdictionId == ((int)CustomJurisdiction.NevadaITL).ToString())) ? true : false))
				.ForMember(dest => dest.SignatureCount, opt => opt.Ignore())
				.ForMember(dest => dest.ComponentType, opt => opt.MapFrom(src => (src.ComponentType.Description)));
			CreateMap<Submission, SubmissionEditViewModel>()
				.ForMember(dest => dest.Cabinet, opt => opt.MapFrom(src => src.Cabinet.Cabinet))
				.ForMember(dest => dest.CertificationLabId, opt => opt.Ignore())
				.ForMember(dest => dest.ChipTypeId, opt => opt.Ignore())
				.ForMember(dest => dest.Company, opt => opt.MapFrom(src => src.Company.Name))
				.ForMember(dest => dest.ContractType, opt => opt.MapFrom(src => src.ContractType.Code + " - " + src.ContractType.Description))
				.ForMember(dest => dest.Currency, opt => opt.MapFrom(src => src.Currency.Code + " - " + src.Currency.Description))
				.ForMember(dest => dest.FunctionId, opt => opt.Ignore())
				.ForMember(dest => dest.GameTypeId, opt => opt.Ignore())
				.ForMember(dest => dest.IsRegressionTesting, opt => opt.MapFrom(src => src.IsRegressionTesting == true))
				.ForMember(dest => dest.Jurisdiction, opt => opt.Ignore())
				.ForMember(dest => dest.LabId, opt => opt.Ignore())
				.ForMember(dest => dest.Manufacturer, opt => opt.MapFrom(src => src.Manufacturer.Description))
				.ForMember(dest => dest.MemoReason, opt => opt.Ignore())
				.ForMember(dest => dest.MemoType, opt => opt.Ignore())
				.ForMember(dest => dest.PurchaseOrder, opt => opt.MapFrom(src => src.PurchaseOrder))
				.ForMember(dest => dest.SecondaryFunction, opt => opt.MapFrom(src => src.SecondaryFunction.Name))
				.ForMember(dest => dest.StatusOptions, opt => opt.Ignore())
				.ForMember(dest => dest.TestingPerformed, opt => opt.MapFrom(src => src.TestingPerformed.Code))
				.ForMember(dest => dest.TestingType, opt => opt.MapFrom(src => src.TestingType.Code))
				.ForMember(dest => dest.TypeId, opt => opt.Ignore())
				.ForMember(dest => dest.VendorId, opt => opt.Ignore())
				.ForMember(dest => dest.WON, opt => opt.Ignore())
				.ForMember(dest => dest.WorkPerformed, opt => opt.MapFrom(src => src.WorkPerformed.Code))
				.ForMember(dest => dest.YearOptions, opt => opt.Ignore())
				.ForMember(dest => dest.ComponentType, opt => opt.MapFrom(src => src.ComponentType.Description))
				// Aristocrat Fields
				.ForMember(dest => dest.ARIProjectNo, opt => opt.Ignore())
				.ForMember(dest => dest.ARIAccountCode, opt => opt.Ignore())
				.ForMember(dest => dest.ARIDeptCode, opt => opt.Ignore())
				.ForMember(dest => dest.AristocratTypeId, opt => opt.Ignore())
				.ForMember(dest => dest.AristocratType, opt => opt.Ignore())
				.ForMember(dest => dest.AristocratComplexityId, opt => opt.Ignore())
				.ForMember(dest => dest.AristocratComplexity, opt => opt.Ignore())
				.ForMember(dest => dest.AristocratMarketId, opt => opt.Ignore())
				.ForMember(dest => dest.AristocratMarket, opt => opt.Ignore())
				.ForMember(dest => dest.ClientNumber, opt => opt.Ignore());
			CreateMap<JurisdictionalData, JurisdictionViewModel>()
				.ForMember(dest => dest.CurrentProjectId, opt => opt.Ignore())
				.ForMember(dest => dest.IsDraftLetter, opt => opt.MapFrom(src => src.Jurisdiction.UseDraftLetters))
				.ForMember(dest => dest.IsLetter, opt => opt.Ignore())
				.ForMember(dest => dest.IsDocs, opt => opt.Ignore())
				.ForMember(dest => dest.IsMainJurisdiction, opt => opt.MapFrom(src => src.Submission.JurisdictionId == src.JurisdictionId))
				.ForMember(dest => dest.HasReplacements, opt => opt.MapFrom(src => src.PreviousVersions.Any()))
				.ForMember(dest => dest.Projects, opt => opt.MapFrom(src => src.JurisdictionalDataProjects.Select(x => new ProjectDetailViewModel
				{
					Id = x.Project.Id,
					IsTransfer = x.Project.IsTransfer == true,
					ProjectName = x.Project.ProjectName,
					BundleTypeId = x.Project.BundleType.Id,
					BundleTypeDescription = x.Project.BundleType.Description
				}).ToList()))
				.ForMember(dest => dest.HasLetter, opt => opt.Ignore())
				.ForMember(dest => dest.SubmissionLetterExists, opt => opt.MapFrom(src => src.SubmissionLetters.Any()))
				.ForMember(dest => dest.ShowStandards, opt => opt.Ignore())
				.ForMember(dest => dest.CertificationNumberDataType, opt => opt.MapFrom(src => src.JurisdictionalDataCertificationNumbers.FirstOrDefault().CertificationNumber.DataType))
				.ForMember(dest => dest.PdfGpsPath, opt => opt.MapFrom(src => src.JurisdictionGPSPDF))
				.ForMember(dest => dest.PdfDraftPath, opt => opt.Ignore())
				.ForMember(dest => dest.PdfDraftGpsPath, opt => opt.Ignore())
				.ForMember(dest => dest.IsLastJurisdiction, opt => opt.Ignore());
			CreateMap<JurisdictionalData, JurisdictionAltViewModel>()
				.ForMember(dest => dest.ManufacturerCode, opt => opt.MapFrom(src => src.Submission.ManufacturerCode))
				.ForMember(dest => dest.WON, opt => opt.MapFrom(src => src.MasterBillingProject))
				.ForMember(dest => dest.SpecialNote, opt => opt.MapFrom(src => src.SpecialNote == true ? "Yes" : "No"))
				.ForMember(dest => dest.IsMainJurisdiction, opt => opt.MapFrom(src => src.Submission.JurisdictionId == src.JurisdictionId))
				.ForMember(dest => dest.Projects, opt => opt.MapFrom(src => src.JurisdictionalDataProjects.Select(x => new ProjectDetailViewModel
				{
					Id = x.Project.Id,
					IsTransfer = x.Project.IsTransfer == true,
					ProjectName = x.Project.ProjectName,
					BundleTypeId = x.Project.BundleType.Id,
					BundleTypeDescription = x.Project.BundleType.Description,
					AddDate = x.Project.AddDate
				}).ToList()));

		}
	}
}
