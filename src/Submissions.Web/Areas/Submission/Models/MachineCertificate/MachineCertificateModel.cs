﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace Submissions.Web.Areas.Submission.Models.MachineCertificate
{
	public class MachineCertificateModel
	{
		public MachineCertificateModel()
		{
			ReplacedMachineCertificates = new List<ReplacedMachineCertificate>();
		}

		public int Id { get; set; }
		[Display(Name = "Certificate Number")]
		public string CertificateNumber { get; set; }
		[Display(Name = "Certificate Date")]
		public DateTime? CertificateDate { get; set; }
		[Display(Name = "Serial Number")]
		public string SerialNumber { get; set; }
		[Display(Name = "Seal Number")]
		public string SealNumber { get; set; }
		[Display(Name = "Device Type")]
		public string DeviceType { get; set; }
		public string JurisdictionId { get; set; }
		public string Jurisdiction { get; set; }
		[Display(Name = "Replacement for Certificate Number")]
		public IList<ReplacedMachineCertificate> ReplacedMachineCertificates { get; set; }
		[Display(Name = "Replacement for Certificate Number")]
		public string CertificatesThatWereReplacedString
		{
			get
			{
				return String.Join(", ", ReplacedMachineCertificates.Select(x => x.CertificationNumber).ToList());
			}
		}
	}

	public class ReplacedMachineCertificate
	{
		public int Id { get; set; }
		public string CertificationNumber { get; set; }
	}
}
