﻿using System.Collections.Generic;

namespace Submissions.Web.Areas.Submission.Models.MachineCertificate
{
	public class MachineCertificateIndexViewModel
	{
		public MachineCertificateIndexViewModel()
		{
			MachineCertificates = new List<MachineCertificateModel>();
			Component = new Component();
		}

		public int SubmissionRecordId { get; set; }
		public Component Component { get; set; }
		public IList<MachineCertificateModel> MachineCertificates { get; set; }
	}
}