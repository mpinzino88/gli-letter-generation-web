﻿namespace Submissions.Web.Areas.Submission.Models.MachineCertificate
{
	public class Component
	{
		public int Id { get; set; }
		public string FileNumber { get; set; }
		public string PartNumber { get; set; }
		public string PartName { get; set; }
		public string Version { get; set; }
		public string Code { get; set; }
		public string Jurisdiction { get; set; }
	}
}