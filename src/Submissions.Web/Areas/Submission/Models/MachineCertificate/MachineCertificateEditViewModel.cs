﻿using Submissions.Common;

namespace Submissions.Web.Areas.Submission.Models.MachineCertificate
{
	public class MachineCertificateEditViewModel
	{
		public MachineCertificateEditViewModel()
		{
			MachineCertificate = new MachineCertificateModel();
			Mode = Mode.Edit;
			Component = new Component();
		}
		public int SubmissionRecordId { get; set; }
		public string JurisdictionId { get; set; }
		public Mode Mode { get; set; }
		public bool AddToAllRecords { get; set; }
		public MachineCertificateModel MachineCertificate { get; set; }
		public Component Component { get; set; }
	}
}