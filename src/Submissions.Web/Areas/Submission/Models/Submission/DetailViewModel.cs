﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Submissions.Web.Areas.Submission.Models.Submission
{
	public class DetailViewModel
	{
		public DetailViewModel()
		{
			this.Submission = new SubmissionDetailViewModel();
			this.SubmissionRecordPaging = new SubmissionRecordPagingDetailViewModel();
		}

		public SubmissionDetailViewModel Submission { get; set; }
		public string SubmissionRecords { get; set; }
		public SubmissionRecordPagingDetailViewModel SubmissionRecordPaging { get; set; }
		public string JiraIssuesUrl { get; set; }
		public string NetworkDrive { get; set; }
		public bool ShowMachineCertificate { get; set; }
		public bool ShowOrgIGTMaterial { get; set; }
		public bool ShowCabinet { get; set; }
	}

	public class SubmissionDetailViewModel
	{
		public int Id { get; set; }
		public int SubmissionHeaderId { get; set; }
		public string FileNumber { get; set; }
		public string ManufacturerCode { get; set; }
		public string Year { get; set; }
		public int ProjectId { get; set; }
		public string SubmissionPDF { get; set; }

		// Counts
		public string IssueCount { get; set; }
		public int JurisdictionCount { get; set; }
		public int MemoCount { get; set; }
		public int ProductCount { get; set; }
		public int RNGDataCount { get; set; }
		public int ShipmentCount { get; set; }
		public int SignatureCount { get; set; }
		public int MachineCertCount { get; set; }

		// General Left - Component Info
		[Display(Name = "Manufacturer")]
		public string ManufacturerDescription { get; set; }
		[Display(Name = "ID Number")]
		public string IdNumber { get; set; }
		[Display(Name = "Game Name")]
		public string GameName { get; set; }
		[Display(Name = "Date Code")]
		public string DateCode { get; set; }
		public string Function { get; set; }
		public string Position { get; set; }
		public string Vendor { get; set; }
		[Display(Name = "Chip Type")]
		public string ChipType { get; set; }
		public string Version { get; set; }
		public string System { get; set; }
		public string Type { get; set; }
		[Display(Name = "Component Type")]
		public string ComponentType { get; set; }

		// General Middle - GLI Info
		[Display(Name = "Submit Date")]
		public DateTime SubmitDate { get; set; }
		[Display(Name = "Receive Date")]
		public DateTime ReceiveDate { get; set; }
		[Display(Name = "Test Lab")]
		public string Lab { get; set; }
		[Display(Name = "Certification Lab")]
		public string CertificationLab { get; set; }
		public string Status { get; set; }
		[Display(Name = "Archive Location")]
		public string ArchiveLocation { get; set; }
		[Display(Name = "Testing Type")]
		public string TestingTypeCode { get; set; }
		[Display(Name = "Regression Testing")]
		public bool? IsRegressionTesting { get; set; }
		[Display(Name = "Project Detail")]
		public string ProjectDetail { get; set; }

		// General Right - Accounting Info
		[Display(Name = "Company")]
		public string CompanyName { get; set; }
		[Display(Name = "Contract Type")]
		public string ContractType { get; set; }
		public string Currency { get; set; }
		[Display(Name = "Project/Contract Number")]
		public string ContractNumber { get; set; }
		[Display(Name = "Contract Value")]
		public decimal? ContractValue { get; set; }

		// More Info Left
		public string Cabinet { get; set; }
		[Display(Name = "Cryptographic (GLI-11)")]
		public string CryptographicallyStrongDescription { get; set; }
		[Display(Name = "Manufacturer Build ID")]
		public string ManufacturerBuildId { get; set; }
		[Display(Name = "Orig Uploaded Material")]
		public string OrgIGTMaterial { get; set; }
		[Display(Name = "Game Type")]
		public string GameType { get; set; }
		[Display(Name = "On-Hold (Days)")]
		public int? OHTotalTime { get; set; }
		[Display(Name = "Conditional Revoke")]
		public bool? ConditionalRevoke { get; set; }
		[Display(Name = "Testing Performed")]
		public string TestingPerformedCode { get; set; }
		[Display(Name = "External Lab")]
		public string ExternalLabCode { get; set; }
		[Display(Name = "Purchase Order")]
		public string PurchaseOrder { get; set; }
		[Display(Name = "Work Performed")]
		public string WorkPerformedCode { get; set; }
		[Display(Name = "Secondary Function")]
		public string SecondaryFunctionName { get; set; }
		[Display(Name = "NV Lab Number")]
		public string NVLabNumber { get; set; }
		public bool ShowNVLabNumber { get; set; }

		// More Info Right
		[Display(Name = "Created")]
		public string AddUser { get; set; }
		public DateTime? AddDate { get; set; }
		[Display(Name = "Modified")]
		public string EditUser { get; set; }
		public DateTime? EditDate { get; set; }
		[Display(Name = "Product Type")]
		public string ProductType { get; set; }
		public string Complexity { get; set; }
		public string Market { get; set; }
		[Display(Name = "Letter Number")]
		public string LetterNumber { get; set; }
		public string Operator { get; set; }

		// master file
		public string MasterFileLabel { get; set; }
		public string MasterFileNumber { get; set; }
		public string MasterFileText { get; set; }
		public bool IsContinuation { get; set; }
		public bool IsPreCertification { get; set; }
		public bool IsResubmission { get; set; }
	}

	public class SubmissionRecordDetailViewModel
	{
		public bool Selected { get; set; }
		public int Id { get; set; }
		public string IdNumber { get; set; }
		public string GameName { get; set; }
	}

	public class SubmissionRecordPagingDetailViewModel
	{
		public int FirstSubmissionId { get; set; }
		public int PrevSubmissionId { get; set; }
		public int NextSubmissionId { get; set; }
		public int LastSubmissionId { get; set; }
		public int Page { get; set; }
		public int Total { get; set; }
	}
}