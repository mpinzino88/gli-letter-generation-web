﻿using System;
using System.Collections.Generic;

namespace Submissions.Web.Areas.Submission.Models.Submission
{
	public class SummaryViewModel
	{
		public SummaryViewModel()
		{
			this.Submissions = new List<SummarySubmissionViewModel>();
		}

		public string FileNumber { get; set; }
		public IList<SummarySubmissionViewModel> Submissions { get; set; }
	}

	public class SummarySubmissionViewModel
	{
		public SummarySubmissionViewModel()
		{
			this.Jurisdictions = new List<SummaryJurisdictionViewModel>();
			this.Signatures = new List<SummarySignatureViewModel>();
		}

		public int Id { get; set; }
		public string DateCode { get; set; }
		public string IdNumber { get; set; }
		public string GameName { get; set; }
		public string Position { get; set; }
		public string TestingType { get; set; }
		public bool ShowSigFilePath { get; set; }
		public IList<SummaryJurisdictionViewModel> Jurisdictions { get; set; }
		public IList<SummarySignatureViewModel> Signatures { get; set; }
	}

	public class SummaryJurisdictionViewModel
	{
		public int Id { get; set; }
		public string JurisdictionId { get; set; }
		public string Name { get; set; }
		public DateTime? SubmitDate { get; set; }
		public DateTime? ReceiveDate { get; set; }
		public string Status { get; set; }
	}

	public class SummarySignatureViewModel
	{
		public int Id { get; set; }
		public int SubmissionId { get; set; }
		public string Scope { get; set; }
		public string Signature { get; set; }
		public string Version { get; set; }
		public string Type { get; set; }
		public string FilePath { get; set; }
	}
}