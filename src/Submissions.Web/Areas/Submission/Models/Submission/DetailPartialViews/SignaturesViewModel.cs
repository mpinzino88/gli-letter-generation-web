﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Submissions.Web.Areas.Submission.Models.Submission.DetailPartialViews
{
	public class SignaturesViewModel
	{
		public SignaturesViewModel()
		{
			this.Signatures = new List<SignatureViewModel>();
		}

		public int SubmissionId { get; set; }
		public IList<SignatureViewModel> Signatures { get; set; }
		public bool ShowFilePath { get; set; }
	}

	public class SignatureViewModel
	{
		public int Id { get; set; }
		public string Type { get; set; }
		public string Scope { get; set; }
		public string Signature { get; set; }
		public string Version { get; set; }
		[Display(Name = "Alpha Version")]
		public string AlphaVersion { get; set; }
		public bool ShowFilePath { get; set; }
		[Display(Name = "File Path")]
		public string FilePath { get; set; }
		public string Seed { get; set; }
		public int? SubmissionId { get; set; }
	}
}