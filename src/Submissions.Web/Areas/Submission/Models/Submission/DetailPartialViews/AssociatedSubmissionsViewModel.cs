﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Submissions.Web.Areas.Submission.Models.Submission.DetailPartialViews
{
	public class AssociatedSubmissionsViewModel
	{
		public AssociatedSubmissionsViewModel()
		{
			this.AssociatedSubmissions = new List<AssociatedSubmissionViewModel>();
		}

		public int SubmissionId { get; set; }
		public string FileNumber { get; set; }
		public IList<AssociatedSubmissionViewModel> AssociatedSubmissions { get; set; }
	}

	public class AssociatedSubmissionViewModel
	{
		public int Id { get; set; }
		public string Status { get; set; }
		[Display(Name = "ID Number")]
		public string IdNumber { get; set; }
		public string Version { get; set; }
		[Display(Name = "Date Code")]
		public string DateCode { get; set; }
		public string Position { get; set; }
		[Display(Name = "Game Name")]
		public string GameName { get; set; }
	}
}