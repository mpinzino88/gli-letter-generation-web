﻿using System.Collections.Generic;

namespace Submissions.Web.Areas.Submission.Models.Submission.DetailPartialViews
{
	public class CertificationNumbersViewModel
	{
		public CertificationNumbersViewModel()
		{
			this.CertificationNumbers = new List<CertificationNumberViewModel>();
		}

		public string FileNumber { get; set; }
		public string JurisdictionName { get; set; }
		public string JurisdictionId { get; set; }
		public IList<CertificationNumberViewModel> CertificationNumbers { get; set; }
	}

	public class CertificationNumberViewModel
	{
		public int Id { get; set; }
		public string CertificationNumber { get; set; }

		public int CertificationNumberId { get; set; }
		public string DocumentPath { get; set; }
	}
}