﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Submissions.Web.Areas.Submission.Models.Submission.DetailPartialViews
{
	public class CodeStoragesViewModel
	{
		public CodeStoragesViewModel()
		{
			this.CodeStorages = new List<CodeStorageViewModel>();
		}

		public int SubmissionId { get; set; }
		public IList<CodeStorageViewModel> CodeStorages { get; set; }
	}

	public class CodeStorageViewModel
	{
		public int Id { get; set; }
		public string Location { get; set; }
		[Display(Name = "Code Storage")]
		public int? CodeStorage { get; set; }
	}
}