﻿using System.Collections.Generic;

namespace Submissions.Web.Areas.Submission.Models.Submission.DetailPartialViews
{
	public class CopyToSubmissionsViewModel
	{
		public CopyToSubmissionsViewModel()
		{
			this.CopyToSubmissions = new List<SubmissionSelection>();
		}

		public int SourceSubmissionId { get; set; }
		public IList<SubmissionSelection> CopyToSubmissions { get; set; }
	}
}