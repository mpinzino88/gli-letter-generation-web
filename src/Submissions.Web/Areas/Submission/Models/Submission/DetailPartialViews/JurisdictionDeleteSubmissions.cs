﻿using Submissions.Web.Areas.Submission.Models.SubmissionBulkUpdate;
using System.Collections.Generic;

namespace Submissions.Web.Areas.Submission.Models.Submission.DetailPartialViews
{
	public class JurisdictionDeleteSubmissions
	{
		public JurisdictionDeleteSubmissions()
		{
			Components = new List<ComponentViewModel>();
		}
		public int SubmissionHeaderId { get; set; }
		public int SubmissionId { get; set; }
		public string FileNumber { get; set; }
		public string JurisdictionId { get; set; }
		public string JurisdictionName { get; set; }
		public IList<ComponentViewModel> Components { get; set; }
	}
}