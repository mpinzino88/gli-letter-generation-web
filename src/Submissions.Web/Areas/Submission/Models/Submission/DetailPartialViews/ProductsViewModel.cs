﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Submissions.Web.Areas.Submission.Models.Submission.DetailPartialViews
{
	public class ProductsViewModel
	{
		public ProductsViewModel()
		{
			Products = new List<ProductViewModel>();
		}
		public int SubmissionId { get; set; }
		public IList<ProductViewModel> Products { get; set; }

	}

	public class ProductViewModel
	{
		public int Id { get; set; }
		public string Name { get; set; }
		public string Version { get; set; }
		public string Type { get; set; }
	}
}