﻿using Submissions.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace Submissions.Web.Areas.Submission.Models.Submission.DetailPartialViews
{
	public class JurisdictionsViewModel
	{
		public JurisdictionsViewModel()
		{
			this.Jurisdictions = new List<JurisdictionViewModel>();
		}

		public int SubmissionId { get; set; }
		public string FileNumber { get; set; }
		public IList<JurisdictionViewModel> Jurisdictions { get; set; }
		public bool HasUpdateDate
		{
			get
			{
				return this.Jurisdictions.Any(x => x.UpdateDate != null);
			}
		}
		public bool HasRegulatoryStatus
		{
			get
			{
				return this.Jurisdictions.Any(x => x.RegulatoryStatus != null);
			}
		}
		public bool HasRevokeDate
		{
			get
			{
				return this.Jurisdictions.Any(x => x.RevokeDate != null);
			}
		}
		public bool HasObsoleteDate
		{
			get
			{
				return this.Jurisdictions.Any(x => x.ObsoleteDate != null);
			}
		}
		public bool HasDraftLetter
		{
			get
			{
				return this.Jurisdictions.Any(x => x.IsDraftLetter);
			}
		}
		public bool HasUpdgradeByDate
		{
			get
			{
				return this.Jurisdictions.Any(x => x.UpgradeByDate != null);
			}
		}
		public bool HasDocs
		{
			get
			{
				return this.Jurisdictions.Any(x => x.IsDocs);
			}
		}
	}

	public class JurisdictionViewModel
	{
		private readonly IList<string> _gpsJurisdictionIds = ComUtil.GPSDocumentJurisdictions().Select(x => ((int)x).ToJurisdictionIdString()).ToList();

		public JurisdictionViewModel()
		{
			this.Projects = new List<ProjectDetailViewModel>();
		}

		public int Id { get; set; }
		public string JurisdictionId { get; set; }
		[Display(Name = "Jurisdiction")]
		public string JurisdictionName { get; set; }
		public string Active { get; set; }
		[Display(Name = "Submit Date")]
		public DateTime RequestDate { get; set; }
		[Display(Name = "Receive Date")]
		public DateTime ReceiveDate { get; set; }
		[Display(Name = "Draft Date")]
		public DateTime? DraftDate { get; set; }
		[Display(Name = "Close Date")]
		public DateTime? CloseDate { get; set; }
		[Display(Name = "Update Date")]
		public DateTime? UpdateDate { get; set; }
		public string Status { get; set; }
		[Display(Name = "Regulatory Status")]
		public string RegulatoryStatus { get; set; }
		[Display(Name = "Revoke Date")]
		public DateTime? RevokeDate { get; set; }
		[Display(Name = "NU Date")]
		public DateTime? ObsoleteDate { get; set; }
		[Display(Name = "Upgrade By")]
		public DateTime? UpgradeByDate { get; set; }
		[Display(Name = "Test Lab")]
		public string LabLocation { get; set; }
		[Display(Name = "Certification Lab")]
		public string CertificationLabLocation { get; set; }
		public bool IsLastJurisdiction { get; set; }
		[Display(Name = "Draft")]
		public bool IsDraftLetter { get; set; }
		[Display(Name = "Letter")]
		public bool IsLetter { get; set; }
		[Display(Name = "Docs")]
		public bool IsDocs
		{
			get
			{
				return _gpsJurisdictionIds.Contains(this.JurisdictionId);
			}
		}
		public bool IsMainJurisdiction { get; set; }
		public IList<ProjectDetailViewModel> Projects { get; set; }
		public int? CurrentProjectId
		{
			get
			{
				return this.Projects.Count > 0 ? this.Projects.OrderByDescending(x => x.AddDate).Select(y => y.Id).First() : (int?)null;
			}
		}
		public int? TransferProjectId
		{
			get
			{
				var query = this.Projects.Where(x => x.BundleTypeId == (int)BundleType.OriginalTesting && x.IsTransfer);
				return query.Any() ? query.Select(x => x.Id).Single() : (int?)null;
			}
		}
		public bool HasMultipleProjects
		{
			get
			{
				return this.Projects.Where(x => x.BundleTypeId == (int)BundleType.ClosedTesting).Any();
			}
		}
		public bool HasReplacements { get; set; }
		public bool ShowStandards { get; set; }
		public string CertificationNumberDataType { get; set; }
		public bool HasLetter
		{
			get
			{
				return this.SubmissionLetterExists || !String.IsNullOrWhiteSpace(this.PdfPath) || !String.IsNullOrWhiteSpace(this.PdfGpsPath);
			}
		}
		public bool SubmissionLetterExists { get; set; }
		public string PdfPath { get; set; }
		public string PdfGpsPath { get; set; }
		public string PdfDraftPath { get; set; }
		public string PdfDraftGpsPath { get; set; }
	}

	public class JurisdictionsAltViewModel
	{
		public JurisdictionsAltViewModel()
		{
			this.Jurisdictions = new List<JurisdictionAltViewModel>();
		}

		public int SubmissionId { get; set; }
		public IList<JurisdictionAltViewModel> Jurisdictions { get; set; }
		public bool ShowRegulatorSpecNum
		{
			get
			{
				return CustomJurisdictionGroupPermissions.RegulatorSpecNum.Select(x => ((int)x).ToJurisdictionIdString()).Any(y => this.Jurisdictions.Select(z => z.JurisdictionId).Contains(y));
			}
		}
		public bool ShowRegulatorRefNum
		{
			get
			{
				return CustomJurisdictionGroupPermissions.RegulatorRefNum.Select(x => ((int)x).ToJurisdictionIdString()).Any(y => this.Jurisdictions.Select(z => z.JurisdictionId).Contains(y));
			}
		}
		public bool ShowSpecialNote
		{
			get
			{
				return CustomJurisdictionGroups.Missouri.Select(x => ((int)x).ToJurisdictionIdString()).Any(y => this.Jurisdictions.Select(z => z.JurisdictionId).Contains(y));
			}
		}
		public bool ShowCheckOffVersion
		{
			get
			{
				return this.Jurisdictions.Any(x => !String.IsNullOrEmpty(x.CheckOffVersion));
			}
		}
	}

	public class JurisdictionAltViewModel
	{
		public JurisdictionAltViewModel()
		{
			this.Projects = new List<ProjectDetailViewModel>();
		}

		public int Id { get; set; }
		public string JurisdictionId { get; set; }
		[Display(Name = "Jurisdiction")]
		public string JurisdictionName { get; set; }
		public string ManufacturerCode { get; set; }
		[Display(Name = "Company")]
		public string CompanyName { get; set; }
		[Display(Name = "Contract Type")]
		public string ContractTypeCode { get; set; }
		[Display(Name = "Currency")]
		public string CurrencyCode { get; set; }
		[Display(Name = "Letter Number")]
		public string LetterNumber { get; set; }
		[Display(Name = "Contract Number")]
		public string ContractNumber { get; set; }
		public string WON { get; set; }
		public string ClientNumber { get; set; }
		[Display(Name = "Project")]
		public string ProjectNumber { get; set; }
		[Display(Name = "Regulator Spec #")]
		public string RegulatorSpecNum { get; set; }
		[Display(Name = "Regulator Ref #")]
		public string RegulatorRefNum { get; set; }
		[Display(Name = "Special Note")]
		public string SpecialNote { get; set; }
		[Display(Name = "Test Script Version")] // this shouldn't really be here
		public string CheckOffVersion { get; set; }
		[Display(Name = "Billing Party")]
		public string PurchaseOrder { get; set; }
		public IList<ProjectDetailViewModel> Projects { get; set; }
		public int? TransferProjectId
		{
			get
			{
				var query = this.Projects.Where(x => x.BundleTypeId == (int)BundleType.OriginalTesting && x.IsTransfer);
				return query.Any() ? query.Select(x => x.Id).Single() : (int?)null;
			}
		}
		public bool HasMultipleProjects
		{
			get
			{
				return this.Projects.Where(x => x.BundleTypeId == (int)BundleType.ClosedTesting).Any();
			}
		}
		public bool IsMainJurisdiction { get; set; }
	}

	public class ProjectDetailViewModel
	{
		public int Id { get; set; }
		public bool IsTransfer { get; set; }
		public string ProjectName { get; set; }
		public byte? BundleTypeId { get; set; }
		public string BundleTypeDescription { get; set; }
		public DateTime? AddDate { get; set; }
	}
}