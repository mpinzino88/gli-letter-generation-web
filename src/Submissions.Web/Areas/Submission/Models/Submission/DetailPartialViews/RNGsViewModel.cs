﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Submissions.Web.Areas.Submission.Models.Submission.DetailPartialViews
{
	public class RNGsViewModel
	{
		public RNGsViewModel()
		{
			this.RNGs = new List<RNGViewModel>();
		}

		public int SubmissionId { get; set; }
		public string FileNumber { get; set; }
		public IList<RNGViewModel> RNGs { get; set; }
	}

	public class RNGViewModel
	{
		public int Id { get; set; }
		[Display(Name = "Test Case")]
		public string TestCase { get; set; }
		[Display(Name = "Range Start")]
		public string RangeStart { get; set; }
		[Display(Name = "Range End")]
		public string RangeEnd { get; set; }
		public string Draws { get; set; }
		public bool? Replacement { get; set; }
		public string Selections { get; set; }
		[Display(Name = "Note")]
		public string Notes { get; set; }
		[Display(Name = "Confidence Levels")]
		public string ConfidenceLevels { get; set; }
	}
}