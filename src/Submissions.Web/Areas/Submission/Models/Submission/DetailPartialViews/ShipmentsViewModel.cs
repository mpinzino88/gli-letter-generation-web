﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Submissions.Web.Areas.Submission.Models.Submission.DetailPartialViews
{
	public class ShipmentsViewModel
	{
		public ShipmentsViewModel()
		{
			this.Shipments = new List<ShipmentViewModel>();
		}

		public int SubmissionId { get; set; }
		public IList<ShipmentViewModel> Shipments { get; set; }
	}

	public class ShipmentViewModel
	{
		public int Id { get; set; }
		public string Type { get; set; }
		[Display(Name = "From/To")]
		public string ShippingRecipientCode { get; set; }
		[Display(Name = "Ship/Receive Date")]
		public string ProcessDate { get; set; }
		public string Courier { get; set; }
		[Display(Name = "Tracking Number")]
		public string TrackingNumber { get; set; }
		[Display(Name = "Chips")]
		public int ChipCount { get; set; }
		[Display(Name = "Disks")]
		public int DiskCount { get; set; }
		public string Memo { get; set; }
		[Display(Name = "Software Checker")]
		public string SoftwareCheckUser { get; set; }
		[Display(Name = "Software Check Date")]
		public DateTime? SoftwareCheckDate { get; set; }
	}
}