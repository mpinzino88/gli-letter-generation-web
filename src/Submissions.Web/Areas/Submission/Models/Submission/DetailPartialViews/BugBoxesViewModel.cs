﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Submissions.Web.Areas.Submission.Models.Submission.DetailPartialViews
{
	public class BugBoxesViewModel
	{
		public BugBoxesViewModel()
		{
			this.BugBoxes = new List<BugBoxViewModel>();
		}

		public int SubmissionId { get; set; }
		public IList<BugBoxViewModel> BugBoxes { get; set; }
	}

	public class BugBoxViewModel
	{
		public int Id { get; set; }
		public string Location { get; set; }
		[Display(Name = "Bug Box")]
		public string BugBox { get; set; }
	}
}