﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace Submissions.Web.Areas.Submission.Models.Submission.DetailPartialViews
{
	public class MachineCertificatesViewModel
	{
		public MachineCertificatesViewModel()
		{
			this.MachineCertificates = new List<MachineCertificateViewModel>();
		}

		public int SubmissionId { get; set; }
		public IList<MachineCertificateViewModel> MachineCertificates { get; set; }
	}

	public class MachineCertificateViewModel
	{
		public MachineCertificateViewModel()
		{
			this.CertificatesThatWereReplaced = new List<string>();
		}

		public int Id { get; set; }
		[Display(Name = "Certificate Number")]
		public string CertificateNumber { get; set; }
		[Display(Name = "Certificate Date")]
		public DateTime CertificateDate { get; set; }
		[Display(Name = "Serial Number")]
		public string SerialNumber { get; set; }
		[Display(Name = "Seal Number")]
		public string SealNumber { get; set; }
		[Display(Name = "Device Type")]
		public string DeviceType { get; set; }
		public string JurisdictionId { get; set; }
		public string Jurisdiction { get; set; }
		[Display(Name = "Replacement for Certificate Number")]
		public IList<string> CertificatesThatWereReplaced { get; set; }
		public string CertificatesThatWereReplacedString
		{
			get
			{
				return String.Join(", ", this.CertificatesThatWereReplaced.ToList());
			}
		}
	}
}