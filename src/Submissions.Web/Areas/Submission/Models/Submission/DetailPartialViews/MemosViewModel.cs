﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Submissions.Web.Areas.Submission.Models.Submission.DetailPartialViews
{
	public class MemosViewModel
	{
		public MemosViewModel()
		{
			this.Memos = new List<MemoViewModel>();
		}

		public int SubmissionId { get; set; }
		public IList<MemoViewModel> Memos { get; set; }
	}

	public class MemoViewModel
	{
		public int Id { get; set; }
		[Display(Name = "Date")]
		public DateTime AddDate { get; set; }
		[Display(Name = "User")]
		public string Username { get; set; }
		public string Type { get; set; }
		public string TypeText { get; set; }
		public string Memo { get; set; }
	}
}