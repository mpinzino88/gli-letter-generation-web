﻿using Submissions.Common;
using Submissions.Common.Validation;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web.Mvc;

namespace Submissions.Web.Areas.Submission.Models.Submission
{
	public class AddViewModel
	{
		public AddViewModel()
		{
			this.Submissions = new List<SubmissionSelection>();
		}

		public bool CanAdd { get; set; }
		public PageSource? Source { get; set; }
		public int SubmissionId { get; set; }
		public int SubmissionHeaderId { get; set; }
		public string FileNumber { get; set; }
		public List<SubmissionSelection> Submissions { get; set; }
		public List<string> JurisdictionIds { get; set; }
		public bool IncludeJurisdictions { get; set; }
		public bool IncludeSignatures { get; set; }
		public bool IncludeMemos { get; set; }
	}

	public class EditViewModel
	{
		public EditViewModel()
		{
			this.CloneJurisdictionIds = new List<string>();
		}
		public Mode Mode { get; set; }
		public int? SourceSubmissionId { get; set; }
		public SubmissionEditViewModel Submission { get; set; }
		public string Memo { get; set; }
		public bool CanAdd { get; set; }
		public bool CanEdit { get; set; }
		public bool CanDelete { get; set; }
		public bool CanEditCabinet { get; set; }

		public bool IsCloned { get; set; }
		public int? CloneSubmissionId { get; set; }
		public List<string> CloneJurisdictionIds { get; set; }
		public bool CloneJurisdictions { get; set; }
		public bool CloneMemos { get; set; }
		public bool CloneSignatures { get; set; }
	}

	public class SubmissionEditViewModel
	{
		public SubmissionEditViewModel()
		{
			this.StatusOptions = LookupsStandard.ConvertEnum<SubmissionStatus>(showValueInDescription: true).ToList();
			this.YearOptions = LookupsStandard.Years;
		}

		public string MemoReason { get; set; }
		public string MemoType { get; set; }

		// Engineering Fields
		[Display(Name = "Manufacturer"), Required]
		public string ManufacturerCode { get; set; }
		public string Manufacturer { get; set; }
		[Display(Name = "ID Number"), Required, StringLength(450)]
		public string IdNumber { get; set; }
		[Display(Name = "Game Name"), Required, StringLength(150)]
		public string GameName { get; set; }
		[Display(Name = "Date Code"), Required, StringLength(60)]
		public string DateCode { get; set; }
		[Display(Name = "Function"), Required]
		public int? FunctionId { get; set; }
		public string Function { get; set; }
		[StringLength(50)]
		public string Position { get; set; }
		[Display(Name = "Vendor")]
		public int? VendorId { get; set; }
		public string Vendor { get; set; }
		[Display(Name = "Chip Type")]
		public int? ChipTypeId { get; set; }
		public string ChipType { get; set; }
		[Display(Name = "Version")]
		public string Version { get; set; }
		[Display(Name = "Manufacturer Build ID"), StringLength(100)]
		public string ManufacturerBuildId { get; set; }
		[Display(Name = "Orig Uploaded Material"), StringLength(100)]
		public string OrgIGTMaterial { get; set; }
		[Display(Name = "Component Type")]
		public int? ComponentTypeId { get; set; }
		public string ComponentType { get; set; }

		// New Submission Only
		[Display(Name = "Type"), Required]
		public int? TypeId { get; set; }
		public string Type { get; set; }
		[Display(Name = "Jurisdiction"), Required]
		public string JurisdictionId { get; set; }
		public string Jurisdiction { get; set; }
		[Required]
		public int Year { get; set; }
		public IList<SelectListItem> YearOptions { get; set; }
		[Display(Name = "WON"), RegularExpression(RegularExpressionPatterns.Accounting, ErrorMessage = "Please enter a valid format")]
		public string WON { get; set; }

		// Quality Assurance Fields
		public int Id { get; set; }
		public string FileNumber { get; set; }
		[Display(Name = "Submit Date"), Required]
		public DateTime? SubmitDate { get; set; }
		[Display(Name = "Receive Date"), Required]
		public DateTime? ReceiveDate { get; set; }
		[Display(Name = "Test Lab"), Required]
		public int? LabId { get; set; }
		public string Lab { get; set; }
		[Display(Name = "Certification Lab"), Required]
		public int? CertificationLabId { get; set; }
		public string CertificationLab { get; set; }
		[Required]
		public string Status { get; set; }
		public IList<SelectListItem> StatusOptions { get; set; }
		[Display(Name = "Archive Location")]
		public string ArchiveLocation { get; set; }
		[Display(Name = "Testing Type"), Required]
		public int? TestingTypeId { get; set; }
		public string TestingType { get; set; }
		[Display(Name = "Regression Testing")]
		public bool IsRegressionTesting { get; set; }
		[Display(Name = "Project Detail"), StringLength(60)]
		public string ProjectDetail { get; set; }

		// Accounting Fields
		[Display(Name = "Company"), Required]
		public int? CompanyId { get; set; }
		public string Company { get; set; }
		[Display(Name = "Contract Type"), Required]
		public int? ContractTypeId { get; set; }
		public string ContractType { get; set; }
		[Display(Name = "Currency"), Required]
		public int? CurrencyId { get; set; }
		public string Currency { get; set; }
		[Display(Name = "Project/Contract Number"), StringLength(150)]
		public string ContractNumber { get; set; }
		[Display(Name = "Purchase Order")]
		public string PurchaseOrder { get; set; }
		[Display(Name = "Contract Value"), Range(0, 99999999.99)]
		public decimal? ContractValue { get; set; }

		// More Info
		public int? CabinetId { get; set; }
		public string Cabinet { get; set; }
		[StringLength(120)]
		public string System { get; set; }
		[Display(Name = "Work Performed")]
		public int? WorkPerformedId { get; set; }
		public string WorkPerformed { get; set; }
		[Display(Name = "Game Type")]
		public int? GameTypeId { get; set; }
		public string GameType { get; set; }
		[Display(Name = "Testing Performed")]
		public int? TestingPerformedId { get; set; }
		public string TestingPerformed { get; set; }
		[Display(Name = "Secondary Function"), Required]
		public int SecondaryFunctionId { get; set; }
		public string SecondaryFunction { get; set; }

		// Aristocrat
		[Display(Name = "Project Number"), StringLength(20)]
		public string ARIProjectNo { get; set; }
		[Display(Name = "Account Code"), StringLength(20)]
		public string ARIAccountCode { get; set; }
		[Display(Name = "Department Code"), StringLength(20)]
		public string ARIDeptCode { get; set; }

		[Display(Name = "Product Type")]
		public string AristocratTypeId { get; set; }
		public string AristocratType { get; set; }
		[Display(Name = "Complexity")]
		public string AristocratComplexityId { get; set; }
		public string AristocratComplexity { get; set; }
		[Display(Name = "Market")]
		public string AristocratMarketId { get; set; }
		public string AristocratMarket { get; set; }
		[Display(Name = "RDAP Pin")]
		public string ClientNumber { get; set; }
		[Display(Name = "Letter Number"), StringLength(250)]
		public string LetterNumber { get; set; }
		[StringLength(120)]
		public string Operator { get; set; }
	}
}