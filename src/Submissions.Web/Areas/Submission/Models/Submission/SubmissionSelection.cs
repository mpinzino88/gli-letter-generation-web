﻿using System;

namespace Submissions.Web.Areas.Submission.Models.Submission
{
	public class SubmissionSelection
	{
		public bool Selected { get; set; }
		public int Id { get; set; }
		public string Status { get; set; }
		public string Version { get; set; }
		public string DateCode { get; set; }
		public string Position { get; set; }
		public string GameName { get; set; }
		public string IdNumber { get; set; }
		public DateTime? SubmitDate { get; set; }
		public DateTime? ReceiveDate { get; set; }
		public string ExtraInfoDisplay { get; set; }
	}
}