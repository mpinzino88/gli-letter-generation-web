﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Submissions.Web.Areas.Submission.Models.SubmissionBulkUpdate
{
	public class IndexViewModel
	{
		public IndexViewModel()
		{
			this.UpdateFields = new UpdateFieldsViewModel();
			this.Components = new List<ComponentViewModel>();
		}

		public int SubmissionHeaderId { get; set; }
		public int SubmissionId { get; set; }
		public string FileNumber { get; set; }
		public UpdateFieldsViewModel UpdateFields { get; set; }
		public IList<ComponentViewModel> Components { get; set; }
	}

	public class DeleteViewModel
	{
		public DeleteViewModel()
		{
			this.Components = new List<ComponentViewModel>();
		}

		public int SubmissionHeaderId { get; set; }
		public string FileNumber { get; set; }
		public IList<int> SubmissionIdsToDelete { get; set; }
		public IList<ComponentViewModel> Components { get; set; }
		public bool HasEngTasks { get; set; }
	}

	public class ComponentViewModel
	{
		public int Id { get; set; }
		[Display(Name = "Date Code")]
		public string DateCode { get; set; }
		public string Function { get; set; }
		[Display(Name = "Game Name")]
		public string GameName { get; set; }
		[Display(Name = "ID Number")]
		public string IdNumber { get; set; }
		public string Position { get; set; }
		public string Version { get; set; }
		public string Status { get; set; }
		[Display(Name = "Receive Date")]
		public DateTime? ReceiveDate { get; set; }
		[Display(Name = "Submit Date")]
		public DateTime? SubmitDate { get; set; }
		public bool IsChecked { get; set; }
	}

	public class UpdateFieldsViewModel
	{
		public UpdateFieldsViewModel()
		{
			this.UpdateSubmissionIds = new List<int>();
			this.AddMemoSubmissionIds = new List<int>();
		}

		public IList<int> UpdateSubmissionIds { get; set; }
		public IList<int> AddMemoSubmissionIds { get; set; }
		[Display(Name = "Date Code"), StringLength(60)]
		public string DateCode { get; set; }
		[Display(Name = "Game Name"), StringLength(150)]
		public string GameName { get; set; }
		[Display(Name = "Function")]
		public int? FunctionId { get; set; }
		public string Function { get; set; }
		public string MemoReason { get; set; }
		[StringLength(50)]
		public string Position { get; set; }
		[Display(Name = "Receive Date")]
		public DateTime? ReceiveDate { get; set; }
		[Display(Name = "Submit Date")]
		public DateTime? SubmitDate { get; set; }
		[StringLength(75)]
		public string Version { get; set; }

		[Display(Name = "Manufacturer Build Id")]
		public string ManufacturerBuildId { get; set; }
		[Display(Name = "Letter Number")]
		public string LetterNumber { get; set; }
		[Display(Name = "Chip Type")]
		public int? ChipTypeId { get; set; }
		public string ChipType { get; set; }
	}
}