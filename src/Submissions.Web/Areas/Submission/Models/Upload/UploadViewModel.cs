﻿using Submissions.Web.Areas.Submission.Models.Submission;
using System.Collections.Generic;
using System.ComponentModel;

namespace Submissions.Web.Areas.Submission.Models.Upload
{
	public class IndexViewModel
	{
		public IndexViewModel()
		{
			Submissions = new List<SubmissionSelection>();
			UploadExcelOptions = new List<UploadExcelOption>();
			ColumnInfo = new List<ExcelColInfo>();
			SignatureDefaultVersions = new List<SignatureTypeInfo>();
			JurisdictionIds = new List<string>();
		}
		public string UploadOption { get; set; }
		public string FileNumber { get; set; }
		public bool CanAdd { get; set; }
		public bool CanEdit { get; set; }
		public bool CanEditSignatures { get; set; }
		public bool CanDeleteSignatures { get; set; }
		public List<ExcelColInfo> ColumnInfo { get; set; }
		public bool IncludeJurisdictions { get; set; }
		public List<string> JurisdictionIds { get; set; }
		public List<SubmissionSelection> Submissions { get; set; }
		public List<UploadExcelOption> UploadExcelOptions { get; set; }
		public string UploadedFileName { get; set; }
		public string ManufactureCode { get; set; }
		public int? TestingTypeId { get; set; }
		public List<SignatureTypeInfo> SignatureDefaultVersions { get; set; }
		public string UploadFileFormatError { get; set; }
	}

	public class SignatureTypeInfo
	{
		public int SignatureTypeId { get; set; }
		public int? DefaultVersionId { get; set; }
		public string DefaultVersion { get; set; }
	}

	public class ExcelColInfo
	{
		public ExcelColInfo()
		{

		}
		public string ExcelColName { get; set; }
		public string TableColName { get; set; }
		public string TableName { get; set; }
		public string SpecialDataType { get; set; }
		public string TableNameValidate { get; set; }
		public bool IsInvalidColumn { get; set; } //This is for warning for invalid column. upload can be done with valid columns
		public bool hasInValidData { get; set; } // if this true, can not upload
		public bool IsSignatureColumn { get; set; }
		public int? SignatureTypeId { get; set; }
		public string SignatureType { get; set; }
		public int? SignatureScopeId { get; set; }
		public string SignatureScope { get; set; }
		public int? SignatureVersionId { get; set; }
		public string SignatureVersion { get; set; }
		public string SignatureFilePath { get; set; }
		public string SignatureAlphaVersion { get; set; }
		public bool ShowAlphaVersion { get; set; }
		public bool SignatureMapped { get; set; }
		public string ErrorInfo { get; set; }
		public string SignatureExistsFor { get; set; } // valid column but just show warning for Data, but user can still upload

	}

	public class TableExcelMapping
	{
		public string TableColName { get; set; }
		public string ExcelColName { get; set; }
		public string DefaultValue { get; set; }
		public string MapFromTable { get; set; }
	}

	public class UploadExcelOption
	{
		public string Name { get; set; }
		public string Description { get; set; }
	}

	public enum UploadFromExcelOptions
	{
		[Description("Add New Submission Records")]
		AddNewSubmissions,
		[Description("Modify Existing Submission Records")]
		ModifySubmissions,
		[Description("Add Signatures for Existing Submission Records")]
		AddSignatures,
		[Description("Modify Signatures for Existing Submission Records")]
		ModifySignatures,
		[Description("Delete Signatures for Existing Submission Records")]
		DeleteSignatures
	}

	public class SignatureError
	{
		public int SubmissionId { get; set; }
		public string SignatureType { get; set; }
		public string Signature { get; set; }
		public string ErrorInfo { get; set; }
	}

}