﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;


namespace Submissions.Web.Areas.Submission.Models.JurisdictionReplaceLink
{
    public class JurisdictionReplaceLinkViewModel
    {
        public JurisdictionReplaceLinkViewModel()
        {
            this.ApplyReplaceLinksToJurisdictions = new List<string>();
            this.ApplyReplaceLinksToJurisdictionsNames = new List<SelectListItem>();
            this.ApplyReplaceLinksOptions = new List<SelectListItem>();
            this.JurisdictionReplaceSearch = new JurisdictionReplaceSearchVM();
            this.JurisdictionSelection = new List<Jurisdiction>();
            this.LoadReplaceLinksOptions = new List<SelectListItem>();
            this.SelJurisdictionReplaceLinks = new JurisdictionReplaceLink();
        }

        public List<string> ApplyReplaceLinksToJurisdictions { get; set; } //To Apply same links to different jurs
        public List<SelectListItem> ApplyReplaceLinksToJurisdictionsNames { get; set; } //To Apply same links to different jurs
        public List<SelectListItem> ApplyReplaceLinksOptions { get; set; }
        public List<SelectListItem> LoadReplaceLinksOptions { get; set; }
        public string FileNumber { get; set; }
        public string JIRAFullTextSearchURL { get; set; }
        public int JurisdictionDataId { get; set; }
        public IEnumerable<SelectListItem> JurisdictionLetterTypes { get; set; }
        public JurisdictionReplaceSearchVM JurisdictionReplaceSearch { get; set; } //Serach 
        public List<Jurisdiction> JurisdictionSelection { get; set; } //Saved links
        public string LinkMode { get; set; }
        public string LinkEditableMode { get; set; }
        public JurisdictionReplaceLink SelJurisdictionReplaceLinks { get; set; } //Search links
    }

    public class JurisdictionReplaceLink
    {
        public JurisdictionReplaceLink()
        {
            ReplaceLinks = new List<ReplaceWith>();
        }
        public int JurisdictionDataId { get; set; } 
        public List<ReplaceWith> ReplaceLinks { get; set; }

    }

    public class ReplaceWith
    {
        public ReplaceWith()
        {
            Jurisdiction = new Jurisdiction();
            WarningMsgs = new List<string>();
        }
        public int ReplaceWithId { get; set; }
        public Jurisdiction Jurisdiction { get; set; }

        //misc
        public bool Selected { get; set; }
        public List<string> WarningMsgs { get; set; }
    }

    public class Jurisdiction
    {
        public int Id { get; set; }
        public string JurisdictionName { get; set; }
        public string JurisdictionId { get; set; }
        public string FileNumber { get; set; }
        public string IDNumber { get; set; }
        public string Version { get; set; }
        public string GameName { get; set; }
        public string DateCode { get; set; }
        //public string Function { get; set; }
        //public string Position { get; set; }
        //public string ChipType { get; set; }
        public DateTime? CloseDate { get; set; }
        public string Status { get; set; }
        public DateTime? RevokedDate { get; set; }
        public DateTime? NUDate { get; set; }
        public DateTime? UpgradeByDate { get; set; }
        public string ReplaceWith { get; set; }
        public bool ConditionalRevoke { get; set; }

        public string AlreadyReplaced { get; set; }

        //misc
        public bool Selected { get; set; }
        public string ReplaceColumn { get; set; }
    }

    public class JurisdictionReplaceSearchVM
    {
        public JurisdictionReplaceSearchVM()
        {
            this.ReplaceSearch = new JurisdictionReplaceSearch();
            this.SelReplaceItems = new List<JurisdictionReplaceSelItems>();
            this.SearchResults = new List<Jurisdiction>();
        }
        public JurisdictionReplaceSearch ReplaceSearch { get; set; } //Search Options
        public IEnumerable<SelectListItem> ReplaceOptions { get; set; }
        public List<Jurisdiction> SearchResults { get; set; } //Search jurisdictions
        public List<JurisdictionReplaceSelItems> SelReplaceItems { get; set; } //Selected jurisdictions to Add
    }

    public class JurisdictionReplaceSelItems
    {
        public int JurisdictionDataId { get; set; }
        public string ReplaceOption { get; set; }
    }
    public class JurisdictionReplaceSearch
    {
        public string FileNumber { get; set; }
        public string IDNumber { get; set; }
        public string Version { get; set; }
        public string GameName { get; set; }
        public string DateCode { get; set; }
        public string JurisdictionId { get; set; }
        public string JurisdictionName { get; set; }
    }
}