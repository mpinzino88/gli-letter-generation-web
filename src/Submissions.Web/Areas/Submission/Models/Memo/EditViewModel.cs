﻿using Submissions.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Submissions.Web.Areas.Submission.Models.Memo
{
	public class EditViewModel
	{
		public EditViewModel()
		{
			this.CopyToSubmissionIds = new List<int>();
		}

		public Mode Mode { get; set; }
		public PageSource? Source { get; set; }
		public string CancelButtonUrl { get; set; }
		public int? Id { get; set; }
		public int SubmissionId { get; set; }
		[Display(Name = "User")]
		public string Username { get; set; }
		[Display(Name = "Date")]
		public DateTime? AddDate { get; set; }
		public MemoType Type { get; set; } = MemoType.GN;
		public IList<int> CopyToSubmissionIds { get; set; }
		[AllowHtml]
		public string Memo { get; set; }
	}
}