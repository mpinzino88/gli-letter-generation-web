﻿using Submissions.Web.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Submissions.Web.Areas.Submission.Models.Memo
{
	public class IndexViewModel
	{
		public IndexViewModel()
		{
			this.Memos = new List<MemoViewModel>();
		}

		public int SubmissionId { get; set; }
		public SubmissionSubTitle SubmissionSubTitle { get; set; }
		public IList<MemoViewModel> Memos { get; set; }
	}

	public class MemoViewModel
	{
		public int Id { get; set; }
		[Display(Name = "Date")]
		public DateTime AddDate { get; set; }
		[Display(Name = "User")]
		public string Username { get; set; }
		public string Type { get; set; }
		public string TypeText { get; set; }
		public string Memo { get; set; }
	}
}