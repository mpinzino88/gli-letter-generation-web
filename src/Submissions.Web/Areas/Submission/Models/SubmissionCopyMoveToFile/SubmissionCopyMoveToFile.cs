﻿using Submissions.Common;
using Submissions.Web.Areas.Submission.Models.Submission;
using System;
using System.Collections.Generic;
using System.Web.Mvc;


namespace Submissions.Web.Areas.Submission.Models.SubmissionCopyMoveToFile
{
	public class SubmissionCopyMoveToFile
	{
		public SubmissionCopyMoveToFile()
		{
			this.Submissions = new List<SubmissionSelection>();
			this.SelFileInfo = new SubmissionFileInfo();
			this.NewFileInfo = new SubmissionFileInfo();
			this.SubmissionStatuses = LookupsStandard.ConvertEnum<SubmissionStatus>(showBlank: true, showValueInDescription: true);
			this.DefaultCompanyCurrency = new List<CompanyCurreny>();
		}
		public bool CanAdd { get; set; }
		public string FileNumber { get; set; }
		public int SubmissionId { get; set; }
		public string Manufacturer { get; set; }
		public List<SubmissionSelection> Submissions { get; set; }
		public List<int> ApplyToSubmissions { get; set; }
		public string CopyMoveOption { get; set; }
		public SubmissionFileInfo SelFileInfo { get; set; }
		public SubmissionFileInfo NewFileInfo { get; set; }
		public bool IncludeSignatures { get; set; }
		public bool IncludeJurisdictions { get; set; }
		public bool IncludeMemo { get; set; }
		public bool IncludeShipping { get; set; }
		public bool UseSelFileWon { get; set; }
		public bool UseNewFileWon { get; set; }
		public bool UseSelFileAsWon { get; set; }
		public bool UseNewFileAsWon { get; set; }
		public string WorkOrderNumber { get; set; }
		public string NewSubmissionStatus { get; set; }
		public string PastDateReason { get; set; }
		public IEnumerable<SelectListItem> SubmissionStatuses { get; set; }
		public List<CompanyCurreny> DefaultCompanyCurrency { get; set; }
	}

	public class SubmissionFileInfo
	{
		public int? CompanyId { get; set; }
		public string Company { get; set; }
		public int CertificationLabId { get; set; }
		public string CertificationLab { get; set; }
		public int TestLabId { get; set; }
		public string TestLab { get; set; }
		public int? ContractTypeId { get; set; }
		public string ContractType { get; set; }
		public int? CurrencyId { get; set; }
		public string Currency { get; set; }
		public DateTime? SubmitDate { get; set; }
		public DateTime? ReceiveDate { get; set; }
		public string Type { get; set; }
		public int? TypeId { get; set; }
		public string JurisdictionId { get; set; }
		public string JurisdictionName { get; set; }
		public string ManufacturerCode { get; set; }
		public string Manufacturer { get; set; }
		public string Year { get; set; }
		public string Sequence { get; set; }
		public string PurchaseOrder { get; set; }
		public string ProjectDetail { get; set; }
		public string Status { get; set; }
		public string GameName { get; set; }
		public Decimal? ContractValue { get; set; }
	}

	public class CompanyCurreny
	{
		public int CompanyId { get; set; }
		public string Company { get; set; }
		public int? CurrencyId { get; set; }
		public string CurrencyCode { get; set; }
		public string Currency { get; set; }
	}

	public enum CopyMoveOption
	{
		Copy,
		Move
	}

	public class CopyMoveSubmissionOverWriteDTO
	{
		public string FileNumber { get; set; }
		public DateTime? RecieveDate { get; set; }
		public DateTime? SubmitDate { get; set; }
		public string SubmissionType { get; set; }
		public string Jurisdiction { get; set; }
		public string ManufacturerCode { get; set; }
		public string Year { get; set; }
		public string Sequence { get; set; }
		public int? CompanyId { get; set; }
		public int? CurrencyId { get; set; }
		public int? ContractTypeId { get; set; }
		public string Status { get; set; }
		public string ArchiveLocation { get; set; }
		public string MasterFile { get; set; }
		public bool? IsRegressionTesting { get; set; }
		public string PurchaseOrder { get; set; }
		public int TestLabId { get; set; }
		public string TestLab { get; set; }
		public int CertificationLabId { get; set; }
		public string CertificationLab { get; set; }
		public string PastDateReason { get; set; }
		public decimal? ContractValue { get; set; }
		public string ProjectDetail { get; set; }
		public string WorkOrderNumber { get; set; }
		public bool SourceFileWON { get; set; }
		public bool DestinationFileWON { get; set; }
		public int TestingPerformedId { get; set; }
		public bool RegressionTesting { get; set; }
		public int AddJurisdictionsProjectId { get; set; }
		public string GameName { get; set; }
	}
}