﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Submissions.Web.Areas.Submission.Models.JurisdictionLinkLetter
{
    public class JurisdictionLinkLetterViewModel
    {
        public JurisdictionLinkLetterViewModel()
        {
            Search = new LetterSearch();
            CurrentSubmissionInfo = new SubmissionDataInfo();
            SearchResults = new List<SearchDataInfo>();
        }
        public string LinkOption { get; set; } //Cert, Supplement, Draft
        public int JurisdictionDataId { get; set; }
        public int DraftId { get; set; } //While linking drafts
        public int SubmissionId { get; set; } //Supplement can be applied to both record and Jur currently
        public SubmissionDataInfo CurrentSubmissionInfo { get; set; } //Display Info for current selected based on what is passed JurID or SubMissionID
        public List<int> SubmissionIds { get; set; } //Apply to Other Record for supplement
        public List<SubmissionDataInfo> ApplytoOthers { get; set; } //Apply to Others
        public LetterSearch Search { get; set; }
        public List<SearchDataInfo> SearchResults { get; set; }
    }

    public class LetterSearch
    {
        public string FileNumber { get; set; }
        public string JurisdictionId { get; set; }
        public string Manufacturer { get; set; }
        public string IdNumber { get; set; }
        public string Version { get; set; }
    }

    public class SubmissionDataInfo
    {
        public int Id { get; set; }
        public string FileNumber { get; set; }
        public string IdNumber { get; set; }
        public string Version { get; set; }
        public string GameName { get; set; }
        public string JurisdictionName { get; set; }
        public string JurisdictionId { get; set; }
        public string Status { get; set; }

    }

    public class SearchDataInfo
    {
        public int JurisdictionDataId { get; set; }
        public string FileNumber { get; set; }
        public string IdNumber { get; set; }
        public string Version { get; set; }
        public string GameName { get; set; }
        public string JurisdictionName { get; set; }
        public string JurisdictionId { get; set; }
        public string Status { get; set; }
        public string PdfPath { get; set; }
        public string GpsPdfPath { get; set; }

        //Applies to Draft
        public int DraftId { get; set; }
        public string WordFilePath { get; set; }
        public string LetterheadPath { get; set; }
        public string AddendumPath { get; set; }
        public string LetterheadPathAddendum { get; set; }

        public bool? UseLetterhead { get; set; }
        public bool? UseLetterheadAddendum { get; set; }

        public bool? RepeatOverlay { get; set; }
        public bool? RepeatOverlayAddendum { get; set; }

        public string LetterheadAddendumSHA1 { get; set; }
        public string LetterheadSHA1 { get; set; }
        public bool? AttachCorrectionAddendum { get; set; }

    }
}