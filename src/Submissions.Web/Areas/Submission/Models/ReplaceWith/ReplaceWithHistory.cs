﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;


namespace Submissions.Web.Areas.Submission.Models.ReplaceWith
{
	public class ReplaceWithHistory
	{
		public ReplaceWithHistory()
		{
			ReplaceWithHistoryItems = new List<ReplaceWithHistoryItem>();
		}
		public List<ReplaceWithHistoryItem> ReplaceWithHistoryItems { get; set; }
		public int currentSelected { get; set; }
	}

	public class ReplaceWithHistoryItem
	{
		public int JurisdictionalDataId { get; set; }
		[Display(Name = "File Number")]
		public string FileNumber { get; set; }
		[Display(Name = "ID Number")]
		public string IdNumber { get; set; }
		[Display(Name = "Date Code")]
		public string DateCode { get; set; }
		[Display(Name = "Version")]
		public string Version { get; set; }
		[Display(Name = "Jurisdiction")]
		public string Jurisdiction { get; set; }
		[Display(Name = "Revoke Date")]
		public DateTime? RevokedDate { get; set; }
		[Display(Name = "NU Date")]
		public DateTime? ObsoleteDate { get; set; }
		[Display(Name = "Upgrade By")]
		public DateTime? UpgradeByDate { get; set; }
		[Display(Name = "Close Date")]
		public DateTime? ApprovedDate { get; set; }
		[Display(Name = "Action")]
		public string Action { get; set; }
		[Display(Name = "Action By")]
		public string ActionBy { get; set; }

		[Display(Name = "Change Date")]
		public DateTime ChangeDate { get; set; }
		public int ActionOrder { get; set; }
	}
}