﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Submissions.Web.Areas.Submission.Models.Submission.DetailPartialViews
{
	public class ReplaceWithViewModel
	{
		public ReplaceWithViewModel()
		{
			SubmissionReplacements = new List<SubmissionReplaceWithVM>();
			JurisdictionReplacements = new List<JurisdictionReplaceWithVM>();
		}
		public List<SubmissionReplaceWithVM> SubmissionReplacements { get; set; }
		public List<JurisdictionReplaceWithVM> JurisdictionReplacements { get; set; }
	}

	public class SubmissionReplaceWithVM
	{
		public int SubmissionId { get; set; }
		[Display(Name = "FileNumber")]
		public string FileNumber { get; set; }
		[Display(Name = "Id Number")]
		public string IdNumber { get; set; }
		[Display(Name = "Version")]
		public string Version { get; set; }
		[Display(Name = "Game Name")]
		public string GameName { get; set; }
	}

	public class JurisdictionReplaceWithVM
	{
		public int SubmissionId { get; set; }
		[Display(Name = "File Number")]
		public string FileNumber { get; set; }
		[Display(Name = "Game Name")]
		public string GameName { get; set; }
		[Display(Name = "Id Number")]
		public string IdNumber { get; set; }
		[Display(Name = "Jurisdiction")]
		public string JurisdictionName { get; set; }
		public int JurisdictionId { get; set; }
		public string ReplaceWith { get; set; }
		[Display(Name = "Version")]
		public string Version { get; set; }
	}

}