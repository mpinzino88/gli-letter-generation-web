﻿using Submissions.Web.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;


namespace Submissions.Web.Areas.Submission.Models.JurisdictionStandard
{
    public class IndexViewModel
    {
        public IndexViewModel()
        {
            this.Standards = new List<StandardViewModel>();
        }
        public int JurisdictionDataId { get; set; }
        public List<StandardViewModel> Standards { get; set; }
        public int SubmissionId { get; set; }
        public SubmissionSubTitle SubmissionSubTitle { get; set; }
        public string FileNumber { get; set; }
    }

    public class StandardViewModel
    {
        public int Id { get; set; }
        [Display(Name = "Jurisdiction Id")]
        public string JurisdictionId { get; set; }
        [Display(Name = "Jurisdiction")]
        public string Jurisdiction { get; set; }
        [Display(Name = "Standard")]
        public string Name { get; set; }
        [Display(Name = "Request Date")]
        public DateTime? AddDate { get; set; }
        [Display(Name = "Close Date")]
        public DateTime? CloseDate { get; set; }
        [Display(Name = "Status")]
        public string Status { get; set; }
        [Display(Name = "Active")]
        public string Active { get; set; }
    }
}