﻿using Submissions.Common;
using System;
using System.ComponentModel.DataAnnotations;

namespace Submissions.Web.Areas.Submission.Models.JurisdictionStandard
{
    public class EditViewModel
    {
        public EditViewModel()
        {
            this.ApplyToProps = new ApplyToAproperties();
            this.ApplyToProps.Standard = true;
            this.ApplyToProps.CloseDate = true;
            this.ApplyToProps.Active = true;
            this.ApplyToProps.Status = true;
        }
        public Mode Mode { get; set; }
        public int? Id { get; set; }
        public int? JurisdictionDataId { get; set; }
        public int SubmissionId { get; set; }

        [Display(Name = "Jurisdiction Id"), Required]
        public string JurisdictionId { get; set; }
        [Display(Name = "Jurisdiction"), Required]
        public string Jurisdiction { get; set; }

        [Display(Name = "Standard"), Required]
        public int? StandardId { get; set; }
        [Display(Name = "Standard")]
        public string StandardName { get; set; }

        public int? OrgStandardId { get; set; }
        
        [Display(Name = "Request Date"), Required]
        public DateTime? AddDate { get; set; }
        [Display(Name = "Close Date")]
        public DateTime? CloseDate { get; set; }
        [Display(Name = "Status"), Required]
        public string Status { get; set; }
        [Display(Name = "Active"), Required]
        public string Active { get; set; }

        public ApplyToAproperties ApplyToProps { get; set; }

        [Display(Name = "Apply Checked Items To All Records")]
        public bool ApplyToAllRecords { get; set; }
    }

    public class ApplyToAproperties
    {
        public bool Standard { get; set;}
        public bool AddDate { get; set; }
        public bool CloseDate { get; set; }
        public bool Active { get; set; }
        public bool Status { get; set; }
    }

}