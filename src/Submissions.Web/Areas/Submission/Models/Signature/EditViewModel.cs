﻿using Submissions.Common;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Submissions.Web.Areas.Submission.Models.Signature
{
	public class EditViewModel
	{
		public EditViewModel()
		{
			AlphaLogic = new AlphaLogic();
			TypeLogic = new TypeLogic();
		}

		public Mode Mode { get; set; }
		public PageSource? Source { get; set; }
		public bool ShowSaveButton { get; set; }
		public bool ShowDeleteButton { get; set; }
		public string CancelButtonUrl { get; set; }
		public int? Id { get; set; }
		public string Type { get; set; }
		[Display(Name = "Type"), Required]
		public int? TypeId { get; set; }
		public string Scope { get; set; }
		[Display(Name = "Scope")]
		public int? ScopeId { get; set; }
		[Required, Remote("ValidateSignatureLength", "Signature", AdditionalFields = "TypeId")]
		public string Signature { get; set; }
		public string Version { get; set; }
		[Display(Name = "Version")]
		public int? VersionId { get; set; }
		[Display(Name = "Alpha Version")]
		public string AlphaVersion { get; set; }
		public string Seed { get; set; }
		public int SubmissionId { get; set; }
		[Display(Name = "File Path")]
		public string FilePath { get; set; }
		public AlphaLogic AlphaLogic { get; set; }
		public TypeLogic TypeLogic { get; set; }
		public bool HasFilePath { get; set; }
	}

	public class AlphaLogic
	{
		public AlphaLogic()
		{
			TypeForHeader = new List<string> {
				SignatureType.ALPHA2VerificationCDCK.GetEnumDescription(),
				SignatureType.ALPHA2VerificationSHA1.GetEnumDescription(),
				SignatureType.AlphaCPU4xVerificationCDCK.GetEnumDescription(),
				SignatureType.AlphaCPU4xVerificationSHA1.GetEnumDescription(),
				SignatureType.AlphaCPU5xVerificationCDCK.GetEnumDescription(),
				SignatureType.AlphaCPU5xVerificationSHA1.GetEnumDescription()
			};
			Types = new List<string> {
				SignatureType.ArgOSVerifyToolSHA1.GetEnumDescription(),
				SignatureType.ArgOSVerifyToolCDCK.GetEnumDescription()
			};
		}
		public List<string> TypeForHeader { get; set; }
		public List<string> Types { get; set; }
	}

	public class TypeLogic
	{
		public TypeLogic()
		{
			Manufacturers = new List<string> {
				CustomManufacturer.BAL.ToString(),
				CustomManufacturer.BLY.ToString(),
				CustomManufacturer.SGI.ToString(),
				CustomManufacturer.SHU.ToString(),
				CustomManufacturer.WMS.ToString()
			};
			ExcludedTypes = new List<string> {
				SignatureType.ALPHA2VerificationCDCK.GetEnumDescription(),
				SignatureType.ALPHA2VerificationSHA1.GetEnumDescription(),
				SignatureType.ArgOSVerifyToolSHA1.GetEnumDescription(),
				SignatureType.ArgOSVerifyToolCDCK.GetEnumDescription(),
				SignatureType.GAT.GetEnumDescription()
			};
		}

		public List<string> Manufacturers { get; set; }
		public List<string> ExcludedTypes { get; set; }
	}
}