﻿using Submissions.Web.Models;
using System.Collections.Generic;

namespace Submissions.Web.Areas.Submission.Models.Signature
{
	public class IndexViewModel
	{
		public IndexViewModel()
		{
			Signatures = new List<EditViewModel>();
		}

		public int SubmissionId { get; set; }
		public SubmissionSubTitle SubmissionSubTitle { get; set; }
		public List<EditViewModel> Signatures { get; set; }
		public bool HasFilePath { get; set; }
		public string FileNumber { get; set; }
	}
}