﻿

using Submissions.Common;
using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace Submissions.Web.Areas.Submission.Models.JurisdictionalData
{
	public class JurisdictionAddEditViewModel
	{
		public JurisdictionAddEditViewModel()
		{
			this.Submissions = new List<Submission>();
			this.Jurisdictions = new List<Jurisdiction>();
			this.DefaultJurisdiction = new Jurisdiction();
			this.SelectedRecords = new List<int>();
			this.CopyOnlyModified = true;
			this.MemoInfo = new SubmmissionMemo();

			this.JurisdictionStatuses = LookupsStandard.ConvertEnum(showBlank: true, showValueInDescription: true, enumValues: new JurisdictionStatus[] {
				JurisdictionStatus .AP, JurisdictionStatus.CP, JurisdictionStatus.DR, JurisdictionStatus.LQ, JurisdictionStatus.MS,
				JurisdictionStatus.MO, JurisdictionStatus.NU, JurisdictionStatus .OH, JurisdictionStatus .RJ, JurisdictionStatus .RA,
				JurisdictionStatus .RV, JurisdictionStatus .TC, JurisdictionStatus .WD
			});
			this.JurisdictionActiveStatuses = LookupsStandard.ConvertEnum<JurisdictionActiveStatus>(showBlank: true);
			this.JurisdictionRegulatoryStatuses = LookupsStandard.ConvertEnum<JurisdictionRegulatoryStatus>(showBlank: true);
			this.JurisdictionUpdatedDateReasons = LookupsStandard.ConvertEnum<UpdateDateReason>(showBlank: true);
			this.JurisdictionRJWDReasons = LookupsStandard.ConvertEnum<JurisdictionRJWDReason>(showBlank: true);
		}

		public string FileNumber { get; set; }
		public string Manufacturer { get; set; }
		public bool IsFlatFileManuf { get; set; }
		public bool IsAristocratManuf { get; set; }
		public DateTime? SubmitDate { get; set; }
		public DateTime? ReceiveDate { get; set; }
		public bool isFileOpen { get; set; }
		public string Testlab { get; set; }
		public List<Submission> Submissions { get; set; }
		public List<Jurisdiction> Jurisdictions { get; set; }
		public Jurisdiction DefaultJurisdiction { get; set; }
		public List<int> SelectedRecords { get; set; }
		public bool CopyOnlyModified { get; set; }
		public SubmmissionMemo MemoInfo { get; set; }
		public string Mode { get; set; }

		//Drop down options
		public IEnumerable<SelectListItem> JurisdictionStatuses { get; set; }
		public IEnumerable<SelectListItem> JurisdictionActiveStatuses { get; set; }
		public IEnumerable<SelectListItem> JurisdictionRegulatoryStatuses { get; set; }
		public IEnumerable<SelectListItem> JurisdictionUpdatedDateReasons { get; set; }
		public IEnumerable<SelectListItem> JurisdictionRJWDReasons { get; set; }

	}

	public class SubmmissionMemo
	{
		public string Memo { get; set; }
		public string OHMemo { get; set; }
		public string DatesMoreThanSixDaysPast { get; set; }
	}
	public class Submission
	{
		public Submission()
		{

		}
		public int Id { get; set; }
		public string Status { get; set; }
		public string Version { get; set; }
		public string DateCode { get; set; }
		public string Position { get; set; }
		public string GameName { get; set; }
		public string IdNumber { get; set; }

		public DateTime? SubmitDate { get; set; }
		public DateTime? ReceiveDate { get; set; }

	}

	public class Jurisdiction
	{
		//Any Propery added which is DB should be added in TrackJurisdictionColumnChange class below to track what property changed and what info should be copied
		public Jurisdiction()
		{
			WarningMsgs = new List<string>();
			ColumnChanges = new TrackJurisdictionColumnChange();

		}
		public int Id { get; set; }

		public string JurisdictionId { get; set; }
		public string JurisdictionName { get; set; }
		public Nullable<int> CompanyId { get; set; }
		public string Company { get; set; }
		public Nullable<int> CurrencyId { get; set; }
		public string Currency { get; set; }
		public Nullable<int> ContractTypeId { get; set; }
		public string ContractType { get; set; }

		public Nullable<int> LabId { get; set; }
		public string lab { get; set; }
		public string Active { get; set; }
		public Nullable<System.DateTime> ReceiveDate { get; set; }
		public Nullable<System.DateTime> RequestDate { get; set; }
		public string Status { get; set; }
		public string LetterNumber { get; set; }
		public string ContractNumber { get; set; }
		public string ClientNumber { get; set; }
		public string ProjectNumber { get; set; }
		public string MasterBillingProject { get; set; }
		public string OrgIGTJurisdiction { get; set; } //for IGT

		//For ARP,APL, ARI
		public string ARIProjectNo { get; set; }
		public string ARIAcctCode { get; set; }
		public string ARIDeptCode { get; set; }

		//Testing data
		public int? TestingPerformedId { get; set; }
		public int? ExternalTestLabId { get; set; }
		public string ExternalTestLab { get; set; }

		//Cert Number, Only for 394
		public string CertNumber { get; set; }
		//jur-286
		public DateTime? QuoteDate { get; set; }

		//jur-"01" , "338" , "252" ,"352"
		public DateTime? AuthorizeDate { get; set; }

		public string RegulatorSpecNum { get; set; }
		public string RegulatorRefNum { get; set; }
		public bool SpecialNote { get; set; }
		//Items replacing
		public int ReplaceCount { get; set; }

		//All Close Jur Info
		public string RegulatoryStatus { get; set; }
		public string RJWDReason { get; set; }
		public DateTime? CloseDate { get; set; }
		public DateTime? DraftLetterDate { get; set; }
		public DateTime? RevokedDate { get; set; }
		public DateTime? UpgardeByDate { get; set; }
		public DateTime? NUDate { get; set; }
		public DateTime? UpdateDate { get; set; }
		public int? UpdateReasonId { get; set; }
		public string UpdateReason { get; set; }

		public Nullable<int> CertLabId { get; set; }
		public string CertLab { get; set; }
		public string JurisdictionPDF { get; set; }
		public string JurisdictionGPSPDF { get; set; }
		public bool ConditionalRevoke { get; set; }
		public bool PriorityDraft { get; set; }

		//===============Extra Info about Jur (Non Tracked DB info)===========
		public string PastDateMemo { get; set; }
		public bool SelToUpdate { get; set; }
		public bool IsDirty { get; set; }
		public List<string> WarningMsgs { get; set; }
		public TrackJurisdictionColumnChange ColumnChanges { get; set; }

	}

	public class TrackJurisdictionColumnChange
	{
		public bool JurisdictionId { get; set; }
		public bool JurisdictionName { get; set; }
		public bool CompanyId { get; set; }
		public bool Company { get; set; }
		public bool CurrencyId { get; set; }
		public bool Currency { get; set; }
		public bool ContractTypeId { get; set; }
		public bool ContractType { get; set; }
		public bool LabId { get; set; }
		public bool lab { get; set; }
		public bool Active { get; set; }
		public bool ReceiveDate { get; set; }
		public bool RequestDate { get; set; }
		public bool Status { get; set; }
		public bool LetterNumber { get; set; }
		public bool ContractNumber { get; set; }
		public bool ClientNumber { get; set; }
		public bool ProjectNumber { get; set; }
		public bool MasterBillingProject { get; set; }
		public bool OrgIGTJurisdiction { get; set; } //for IGT

		//For ARP,APL, ARI
		public bool ARIProjectNo { get; set; }
		public bool ARIAcctCode { get; set; }
		public bool ARIDeptCode { get; set; }

		//Testing data
		public bool TestingPerformedId { get; set; }
		public bool ExternalTestLabId { get; set; }
		public bool ExternalTestLab { get; set; }

		//Cert Number, Only for 394
		public bool CertNumber { get; set; }

		//All Close Jur Info
		public bool RegulatoryStatus { get; set; }
		public bool RJWDReason { get; set; }
		public bool CloseDate { get; set; }
		public bool DraftLetterDate { get; set; }
		public bool RevokedDate { get; set; }
		public bool UpgardeByDate { get; set; }
		public bool NUDate { get; set; }
		public bool UpdateDate { get; set; }
		public bool UpdatedReason { get; set; }
		public bool TxLab { get; set; }
		public bool JurisdictionPDF { get; set; }
		public bool JurisdictionGPSPDF { get; set; }
		public bool ConditionalRevoke { get; set; }
		public bool PriorityDraft { get; set; }

	}


}