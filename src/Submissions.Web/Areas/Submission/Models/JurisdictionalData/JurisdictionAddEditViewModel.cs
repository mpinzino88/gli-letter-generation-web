﻿using Submissions.Common;
using Submissions.Web.Areas.Submission.Models.Submission;
using System;
using System.Collections.Generic;
using System.Web.Mvc;


namespace Submissions.Web.Areas.Submission.Models.JurisdictionalData
{
	public class JurisdictionAddEditViewModel
	{
		public JurisdictionAddEditViewModel()
		{
			this.Submissions = new List<SubmissionSelection>();
			this.Jurisdictions = new List<Jurisdiction>();
			this.DefaultJurisdiction = new Jurisdiction();
			this.CopyOnlyModified = true;
			this.MemoInfo = new SubmmissionMemo();

			this.JurisdictionStatuses = LookupsStandard.ConvertEnum(showBlank: true, showValueInDescription: true, enumValues: ComUtil.InternalJurisdictionStatuses().ToArray());
			this.JurisdictionActiveStatuses = LookupsStandard.ConvertEnum<JurisdictionActiveStatus>(showBlank: true);
			this.JurisdictionRegulatoryStatuses = LookupsStandard.ConvertEnum<JurisdictionRegulatoryStatus>(showBlank: true);
			this.JurisdictionRJWDReasons = LookupsStandard.ConvertEnum<JurisdictionRJWDReason>(showBlank: true, useDescriptionAsValue: true);
			this.JurisdictionUpdatedDateReasons = new List<SelectListItem>();

			this.UserAndOtherInfo = new UserAndOtherInfo();

		}

		public string FileNumber { get; set; }
		public string Manufacturer { get; set; }
		public bool IsFlatFileManuf { get; set; }
		public bool IsAristocratManuf { get; set; }
		public bool IsProductLineManuf { get; set; }
		public DateTime? SubmitDate { get; set; }
		public DateTime? ReceiveDate { get; set; }
		public bool IsFileOpen { get; set; }
		public string Testlab { get; set; }
		public int SubmissionId { get; set; }
		public List<SubmissionSelection> Submissions { get; set; }
		public List<Jurisdiction> Jurisdictions { get; set; }
		public Jurisdiction DefaultJurisdiction { get; set; }
		//public List<int> SelectedRecords { get; set; }
		public bool CopyOnlyModified { get; set; }
		public bool ConditionalRevokeSubmission { get; set; }
		public SubmmissionMemo MemoInfo { get; set; }
		public string Mode { get; set; }
		public string TestingType { get; set; }

		//Drop down options
		public IEnumerable<SelectListItem> JurisdictionStatuses { get; set; }
		public IEnumerable<SelectListItem> JurisdictionActiveStatuses { get; set; }
		public IEnumerable<SelectListItem> JurisdictionRegulatoryStatuses { get; set; }
		public IEnumerable<SelectListItem> JurisdictionUpdatedDateReasons { get; set; }
		public IEnumerable<SelectListItem> JurisdictionRJWDReasons { get; set; }

		//User and Other Info
		public UserAndOtherInfo UserAndOtherInfo { get; set; }

		public bool CanEdit { get; set; }

	}
	public class UserAndOtherInfo
	{
		public UserAndOtherInfo()
		{
			this.ClosedStatuses = new List<string>();
			this.CheckOffVersions = new List<CheckOffVersion>();
		}
		public int UserlocationId { get; set; }
		public string Userlocation { get; set; }
		public string UserRole { get; set; }

		//All Available Closed Status
		public List<string> ClosedStatuses { get; set; }
		public List<CheckOffVersion> CheckOffVersions { get; set; }

		//Pass all Jur Enums later on

	}

	public class CheckOffVersion
	{
		public int Id { get; set; }
		public string Description { get; set; }
	}

	public class SubmmissionMemo
	{
		public string Memo { get; set; }
		public string OHMemo { get; set; }
		public string DatesMoreThanSixDaysPast { get; set; }
	}

	public class Jurisdiction
	{
		//Any Propery added which is DB should be added in TrackJurisdictionColumnChange class below to track what property changed and what info should be copied
		public Jurisdiction()
		{
			this.WarningMsgs = new List<string>();
			this.ColumnChanges = new TrackJurisdictionColumnChange();
			this.ProductLines = new List<ProductLine>();
		}
		public int Id { get; set; }

		public string JurisdictionId { get; set; }
		public string JurisdictionName { get; set; }
		public Nullable<int> CompanyId { get; set; }
		public string Company { get; set; }
		public Nullable<int> CurrencyId { get; set; }
		public string Currency { get; set; }
		public Nullable<int> ContractTypeId { get; set; }
		public string ContractType { get; set; }
		public Nullable<int> LabId { get; set; }
		public string lab { get; set; }
		public string Active { get; set; }
		public Nullable<System.DateTime> ReceiveDate { get; set; }
		public Nullable<System.DateTime> RequestDate { get; set; }
		public string Status { get; set; }
		public string OrigStatus { get; set; } //This is to track what was orig status instead of querying again
		public string LetterNumber { get; set; }
		public string ContractNumber { get; set; }
		public string ClientNumber { get; set; }
		public string ProjectNumber { get; set; }
		public string MasterBillingProject { get; set; }
		public string OrgIGTJurisdiction { get; set; } //for IGT
		public string PurchaseOrder { get; set; }

		//For ARP,APL, ARI
		public string ARIProjectNo { get; set; }
		public string ARIAccountCode { get; set; }
		public string ARIDeptCode { get; set; }

		//Test script version
		public string TestScriptVersions { get; set; }
		public List<int> TestScriptVersionIds { get; set; }


		// Product Lines
		public List<ProductLine> ProductLines { get; set; }


		//Cert Number, Only for 394
		public string CertNumber { get; set; }
		//jur-286
		public DateTime? QuoteDate { get; set; }

		//jur-"01" , "338" , "252" ,"352"
		public DateTime? AuthorizeDate { get; set; }

		public string RegulatorSpecNum { get; set; }
		public string RegulatorRefNum { get; set; }
		public bool SpecialNote { get; set; }
		//Items replacing
		public int ReplaceCount { get; set; }

		//All Close Jur Info
		public string RegulatoryStatus { get; set; }
		public string RJWDReason { get; set; }
		public DateTime? CloseDate { get; set; }
		public DateTime? DraftDate { get; set; }
		public DateTime? RevokeDate { get; set; }
		public DateTime? UpgradeByDate { get; set; }
		public DateTime? ObsoleteDate { get; set; }
		public DateTime? UpdateDate { get; set; }
		public int? UpdateReasonId { get; set; }
		public Nullable<int> CertificationLabId { get; set; }
		public string CertificationLab { get; set; }
		public string PdfPath { get; set; }
		public string JurisdictionGPSPDF { get; set; }
		public bool ConditionalRevoke { get; set; }
		public bool PriorityDraft { get; set; }

		//misc non editable info
		public string NationCode { get; set; }
		public string JurType { get; set; }

		//===============Extra Info about Jur (Non Tracked DB info)===========
		public string DraftGPSPDFPath { get; set; }
		public string DraftWebPDFPath { get; set; }
		public string PastDateMemo { get; set; }
		public bool SelToUpdate { get; set; }
		public bool IsDirty { get; set; }
		public List<string> WarningMsgs { get; set; }

		public bool HideWarningModeForStatus { get; set; } //This is while batch update
		public TrackJurisdictionColumnChange ColumnChanges { get; set; }

	}

	//Column for this must match above jurisdiction object, This object keeps track of what column changed. Also if new property needs to be tracked add to below enum as well.
	public class TrackJurisdictionColumnChange
	{
		public bool JurisdictionId { get; set; }
		//public bool JurisdictionName { get; set; }
		public bool CompanyId { get; set; }
		//public bool Company { get; set; }
		public bool CurrencyId { get; set; }
		//public bool Currency { get; set; }
		public bool ContractTypeId { get; set; }
		//public bool ContractType { get; set; }
		public bool LabId { get; set; }
		//public bool lab { get; set; }
		public bool Active { get; set; }
		public bool ReceiveDate { get; set; }
		public bool RequestDate { get; set; }

		public bool Status { get; set; }
		public bool LetterNumber { get; set; }
		public bool ContractNumber { get; set; }
		public bool ClientNumber { get; set; }
		public bool ProjectNumber { get; set; }
		public bool MasterBillingProject { get; set; }
		public bool OrgIGTJurisdiction { get; set; } //for IGT

		//For ARP,APL, ARI
		public bool ARIProjectNo { get; set; }
		public bool ARIAccountCode { get; set; }
		public bool ARIDeptCode { get; set; }

		//Test script version
		public bool TestScriptVersions { get; set; }

		// Product Lines
		public bool ProductLines { get; set; }

		//Cert Number, Only for 394
		public bool CertNumber { get; set; }
		public bool QuoteDate { get; set; }

		public bool AuthorizeDate { get; set; }
		public bool RegulatorSpecNum { get; set; }
		public bool RegulatorRefNum { get; set; }
		public bool SpecialNote { get; set; }

		//All Close Jur Info
		public bool RegulatoryStatus { get; set; }
		public bool RJWDReason { get; set; }
		public bool CloseDate { get; set; }
		public bool DraftDate { get; set; }
		public bool RevokeDate { get; set; }
		public bool UpgradeByDate { get; set; }
		public bool ObsoleteDate { get; set; }
		public bool UpdateDate { get; set; }
		public bool UpdateReasonId { get; set; }

		public bool CertificationLabId { get; set; }
		//public bool CertificationLab { get; set; }

		public bool PdfPath { get; set; }
		public bool JurisdictionGPSPDF { get; set; }
		public bool ConditionalRevoke { get; set; }
		public bool PriorityDraft { get; set; }
		public bool PurchaseOrder { get; set; }

	}
	#region Enum
	public enum TrackedJurisdictionColumns
	{

		JurisdictionId,
		JurisdictionName,
		CompanyId,
		CurrencyId,
		ContractTypeId,
		LabId,
		Active,
		ReceiveDate,
		RequestDate,
		Status,
		LetterNumber,
		ContractNumber,
		ClientNumber,
		ProjectNumber,
		MasterBillingProject,
		OrgIGTJurisdiction,
		ARIProjectNo,
		ARIAccountCode,
		ARIDeptCode,
		CertNumber,
		QuoteDate,
		AuthorizeDate,
		RegulatorSpecNum,
		RegulatorRefNum,
		SpecialNote,
		RegulatoryStatus,
		RJWDReason,
		CloseDate,
		DraftDate,
		RevokeDate,
		UpgradeByDate,
		ObsoleteDate,
		UpdateDate,
		UpdateReasonId,
		CertificationLabId,
		PdfPath,
		JurisdictionGPSPDF,
		ConditionalRevoke,
		PriorityDraft,
		PurchaseOrder,
		TestScriptVersions,
		ProductLines
	}

	public class PropertyDBColumnMapping
	{
		public string PropertyName { get; set; }
		public string DBColumnName { get; set; }

	}
	#endregion


	public class ProductLine
	{
		public string Name { get; set; }
		public int Id { get; set; }
	}
}