﻿using Submissions.Web.Models;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Submissions.Web.Areas.Submission.Models.BugBox
{
	public class IndexViewModel
	{
		public IndexViewModel()
		{
			this.BugBoxes = new List<BugBoxViewModel>();
		}

		public int SubmissionId { get; set; }
		public SubmissionSubTitle SubmissionSubTitle { get; set; }
		public IList<BugBoxViewModel> BugBoxes { get; set; }
	}

	public class BugBoxViewModel
	{
		public int Id { get; set; }
		[Display(Name = "Location")]
		public string Lab { get; set; }
		[Display(Name = "Bug Box")]
		public string BugBox { get; set; }
	}
}