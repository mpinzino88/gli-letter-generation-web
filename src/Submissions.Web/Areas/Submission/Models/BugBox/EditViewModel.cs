﻿using Submissions.Common;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Submissions.Web.Areas.Submission.Models.BugBox
{
	public class EditViewModel
	{
		public Mode Mode { get; set; }
		public PageSource? Source { get; set; }
		public string CancelButtonUrl { get; set; }
		public int? Id { get; set; }
		public int SubmissionId { get; set; }
		[Display(Name = "Location"), Required]
		public int? LabId { get; set; }
		public string Lab { get; set; }
		[Display(Name = "Bug Box"), Range(0, int.MaxValue), Remote("CheckDuplicate", "BugBox", AdditionalFields = "Id, SubmissionId, LabId", ErrorMessage = "{0} already exists")]
		public string BugBox { get; set; }
	}
}