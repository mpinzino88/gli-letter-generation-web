﻿using System;
using System.Collections.Generic;

namespace Submissions.Web.Areas.Submission.Models.Import
{
	public class StagedSubmission
	{
		public StagedSubmission()
		{
			this.Children = new List<StagedSubmissionChild>();
		}
		public bool ChildrenVisible { get; set; }
		public bool Selected { get; set; }
		public bool AddToExistingSubmission { get; set; }
		public int Id { get; set; }
		public string LetterNumber { get; set; }
		public string Status { get; set; }
		public DateTime? InsertDate { get; set; }
		public DateTime? ReceivedDate { get; set; }
		public DateTime? RequestDate { get; set; }
		public int? ContractTypeId { get; set; }
		public string ContractType { get; set; }
		public int? CertificationLabId { get; set; }
		public string CertificationLab { get; set; }
		public int? TestLabId { get; set; }
		public string TestLab { get; set; }
		public int? SubmissionTypeId { get; set; }
		public string SubmissionType { get; set; }
		public string PrimaryJurisdiction { get; set; }
		public string PrimaryJurisdictionName { get; set; }
		public int? CurrencyId { get; set; }
		public string Currency { get; set; }
		public int? CompanyId { get; set; }
		public string Company { get; set; }
		public int? UserId { get; set; }
		public string UserName { get; set; }
		public string ManufacturerShortCode { get; set; }
		public int HasMaterial964 { get; set; }
		public string FileNumber { get; set; }
		public string Material { get; set; }
		public string MaterialDescription { get; set; }
		public string JurisdictionDescription { get; set; }
		public string EmployeeName { get; set; }
		public string BillingReference { get; set; }
		public DateTime? CloseDate { get; set; }
		public string RJWDNote { get; set; }
		public int SubmissionRecordId { get; set; }
		public string WorkOrderNumber { get; set; }
		public int MatchExists { get; set; }
		public bool MissingBillingReference { get; set; }
		public string DateCode { get; set; }
		public bool OverRideDuplicateGuard { get; set; }
		public string Version { get; set; }
		public string ProductCategory { get; set; }
		public List<StagedSubmissionChild> Children { get; set; }
	}
	public class StagedSubmissionChild
	{
		public string Material { get; set; }
		public string Jurisdiction { get; set; }
		public int? JurisdictionId { get; set; }
		public string BillingReference { get; set; }
	}
	public class StagedFollowupRequest
	{
		public StagedFollowupRequest()
		{
			this.Children = new List<StagedFollowupRequest>();
			this.PotentialSubmissions = new List<PotentialSubmission>();
		}
		public bool ChildrenVisible { get; set; }
		public bool Selected { get; set; }
		public string Mode { get; set; }
		public string Label { get; set; }
		public bool Html { get; set; }
		public int? Id { get; set; }
		public string FileNumber { get; set; }
		public string IdNumber { get; set; }
		public string Version { get; set; }
		public string GameName { get; set; }
		public string DateCode { get; set; }
		public string LetterNumber { get; set; }
		public string BillingReference { get; set; }
		public string Status { get; set; }
		public string ManufacturerCode { get; set; }
		public DateTime? InsertDate { get; set; }
		public DateTime? ReceivedDate { get; set; }
		public DateTime? RequestDate { get; set; }
		public DateTime? CloseDate { get; set; }
		public int? UserId { get; set; }
		public string UserName { get; set; }
		public string Material { get; set; }
		public string ProjectId { get; set; }
		public string MaterialDescription { get; set; }
		public int? JurisdictionId { get; set; }
		public string Jurisdiction { get; set; }
		public string JurisdictionDescription { get; set; }
		public int? SubmissionId { get; set; }
		public string RJWDNote { get; set; }
		public int TestLabId { get; set; }
		public string TestLab { get; set; }
		public int? MappedSubmissionId { get; set; }
		public int AmbiguousFollowupRequest { get; set; }
		public string WorkOrderNumber { get; set; }
		public int SubmissionMatchCount { get; set; }
		public string SelectedStatus { get; set; }
		public string WDRJReason { get; set; }
		public bool MissingBillingReference { get; set; }
		public string PurchaseOrder { get; set; }
		public List<PotentialSubmission> PotentialSubmissions { get; set; }

		public List<StagedFollowupRequest> Children { get; set; }
	}

	public class StagedSubmissionColumnOptions
	{
		public string Label { get; set; }
		public string Field { get; set; }
		public ColumnType Type { get; set; }
		public string DateInputFormat { get; set; }
		public string DateOutputFormat { get; set; }
		public bool Sortable { get; set; }
		public bool Html { get; set; }
		public int Width { get; set; }
		public bool Hidden { get; set; }
		public string ThClass { get; set; }
		public string TdClass { get; set; }
		public bool GlobalSearchDisabled { get; set; }
	}

	public enum ColumnType
	{
		number,
		@decimal,
		percentage,
		boolean,
		date,
		text
	}

	public class PotentialSubmission
	{
		public int Id { get; set; }
		public string FileNumber { get; set; }
		public string Version { get; set; }
		public string DateCode { get; set; }
		public string IdNumber { get; set; }
		public string GameName { get; set; }
		public string JurisdictionStatus { get; set; }
	}

	public class MoveStagedRequest
	{
		public string LetterNumber { get; set; }
		public string DestinationQueue { get; set; }
	}
}