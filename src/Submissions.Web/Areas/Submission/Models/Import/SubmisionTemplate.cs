﻿namespace Submissions.Web.Areas.Submission.Models.Import
{
	public class SubmisionTemplate
	{
		public int Id { get; set; }
		public string Code { get; set; }
		public string Description { get; set; }
		public int CompanyId { get; set; }
		public string Company { get; set; }
		public int CurrencyId { get; set; }
		public string Currency { get; set; }
		public int ContractId { get; set; }
		public string Contract { get; set; }
		public int SubmissionTypeId { get; set; }
		public string SubmissionType { get; set; }
		public int CertificationLabId { get; set; }
		public string CertificationLab { get; set; }
		public int TestLabId { get; set; }
		public string TestLab { get; set; }
	}
}