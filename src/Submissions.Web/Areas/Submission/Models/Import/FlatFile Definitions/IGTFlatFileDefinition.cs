﻿using Aspose.Cells;
using GLI.EFCore.Submission;
using Submissions.Common;
using Submissions.Core.Interfaces;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Submissions.Web.Areas.Submission.Models.Import
{
	public class IGTFlatFileDefinition : FlatFileDefinition
	{
		#region Readonly Props
		private readonly List<string> _ALLSA = new List<string>() { "55", "62", "63", "82", "113", "130", "193", "200", "201" };
		private readonly List<string> _ALLSD = new List<string>() { "165", "166" };
		private readonly List<string> _ALLNM = new List<string>() { "121", "253" };
		private readonly List<string> _ALLSFBINGO = new List<string>() { "261", "402", "404", "409", "410", "498" };
		private readonly List<string> _ALLSFLPM = new List<string>() { "403", "405", "406", "407", "408", "412", "413", "414", "415" };
		private readonly string _IGTSTUDIOGROUPNAME = "IGT Studios";
		private readonly List<string> _COLUMNHEADERS = new List<string> {
			"MATERIAL",
			"JURISDICTION",
			"LETTERNUMBER",
			"STATUS",
			"RJWD_NOTE",
			"SUBMISSION NOTES",
			"CLOSEDATE",
			"REQUESTDATE",
			"RECIEVEDDATE",
			"BUSINESSPRIORITY DATE",
			"EMPLOYEENAME",
			"FOLLOWUPNOTES",
			"HOT",
			"JURISDICTIONDESCRIPTION",
			"MATERIALDESCRIPTION",
			"NETWORK",
			"PROJECT",
			"COMPONENT"
		};
		#endregion

		public IGTFlatFileDefinition(SubmissionContext dbSubmission, IUser user, IElectronicSubmissionService electronicSubmissionService, string manufacturer) : base(dbSubmission, user, electronicSubmissionService, manufacturer)
		{
			ColumnHeaders = _COLUMNHEADERS;
			GroupByKey = "MATERIAL";
		}

		#region Overides
		public override void ReadFlatFileToDataTable()
		{
			var workbook = new Workbook(File.InputStream);
			var worksheet = workbook.Worksheets[0];
			worksheet.Cells.DeleteBlankRows();
			var dataTable = worksheet.Cells.ExportDataTableAsString(0, 0, worksheet.Cells.MaxRow + 1, worksheet.Cells.MaxColumn + 1, true);

			for (var i = 0; i < dataTable.Columns.Count; i++)
			{
				dataTable.Columns[i].ColumnName = dataTable.Columns[i].ColumnName.ToString().ToUpper().Trim().Replace(" ", "");
			}

			DataTable = dataTable;
		}
		public override void ParseFlatFileDataTable()
		{
			for (int i = 0; i < DataTable.Rows.Count; i++)
			{
				if (
					!String.IsNullOrWhiteSpace(DataTable.Rows[i]["MATERIAL"].ToString().Trim())
					&& !String.IsNullOrWhiteSpace(DataTable.Rows[i]["JURISDICTION"].ToString().Trim())
					&& !String.IsNullOrWhiteSpace(DataTable.Rows[i]["STATUS"].ToString().Trim())
				)
				{
					ParseRow(DataTable.Rows[i], i);
				}
			}
		}
		public override void ParseRow(DataRow row, int rowNum)
		{
			var flatFileRow = new FlatFileDTO
			{
				IdNumber = String.IsNullOrWhiteSpace(row["MATERIAL"].ToString().Trim()) ? null : row["MATERIAL"].ToString().Trim(),
				Jurisdiction = String.IsNullOrWhiteSpace(row["JURISDICTION"].ToString().Trim().TrimStart('0')) ? null : row["JURISDICTION"].ToString().Trim().TrimStart('0'),
				SupplierReferenceNumber =
					String.IsNullOrWhiteSpace(row["LETTERNUMBER"].ToString().Trim()) &&
					!Enum.GetNames(typeof(FlatFileStatus)).Contains(row["STATUS"].ToString().ToUpper().Trim())
					?
						"MA_" + DateTime.Now.ToString().Replace(" ", "_")
						:
						row["LETTERNUMBER"].ToString().Trim(),
				Status =
					String.IsNullOrWhiteSpace(row["LETTERNUMBER"].ToString().Trim()) &&
					!Enum.GetNames(typeof(FlatFileStatus)).Contains(row["STATUS"].ToString().ToUpper().Trim())
					?
						"PND"
						:
						row["STATUS"].ToString().ToUpper().Trim(),
			};

			if (row.Table.Columns.IndexOf("SUBMISSIONNOTES") > 0)
			{
				flatFileRow.Notes = String.IsNullOrWhiteSpace(row["SUBMISSIONNOTES"].ToString().Trim()) ? null : row["SUBMISSIONNOTES"].ToString().Trim();
			}

			if (row.Table.Columns.IndexOf("EMPLOYEENAME") > 0)
			{
				flatFileRow.ContactName = String.IsNullOrWhiteSpace(row["EMPLOYEENAME"].ToString().Trim()) ? null : row["EMPLOYEENAME"].ToString().Trim();
			}

			if (row.Table.Columns.IndexOf("FOLLOWUPNOTES") > 0)
			{
				flatFileRow.AdditionalNotes = String.IsNullOrWhiteSpace(row["FOLLOWUPNOTES"].ToString().Trim()) ? null : row["FOLLOWUPNOTES"].ToString().Trim();
			}

			if (row.Table.Columns.IndexOf("HOT") > 0)
			{
				flatFileRow.Expedite = String.IsNullOrWhiteSpace(row["HOT"].ToString().Trim()) ? null : row["HOT"].ToString().Trim();
			}

			if (row.Table.Columns.IndexOf("JURISDICTIONDESCRIPTION") > 0)
			{
				flatFileRow.JurisdictionDescription = String.IsNullOrWhiteSpace(row["JURISDICTIONDESCRIPTION"].ToString().Trim()) ? null : row["JURISDICTIONDESCRIPTION"].ToString().Trim();
			}

			if (row.Table.Columns.IndexOf("MATERIALDESCRIPTION") > 0)
			{
				flatFileRow.GameName = String.IsNullOrWhiteSpace(row["MATERIALDESCRIPTION"].ToString().Trim()) ? null : row["MATERIALDESCRIPTION"].ToString().Trim();
			}

			if (row.Table.Columns.IndexOf("NETWORK") > 0)
			{
				flatFileRow.SupplierJurisdictionalReferenceNumber = String.IsNullOrWhiteSpace(row["NETWORK"].ToString().Trim()) ? null : row["NETWORK"].ToString().Trim();
			}

			if (row.Table.Columns.IndexOf("PROJECT") > 0)
			{
				flatFileRow.ProjectNumber = String.IsNullOrWhiteSpace(row["PROJECT"].ToString().Trim()) ? null : row["PROJECT"].ToString().Trim();
			}

			if (row.Table.Columns.IndexOf("COMPONENT") > 0)
			{
				flatFileRow.Studio = String.IsNullOrWhiteSpace(row["COMPONENT"].ToString().Trim()) ? null : row["COMPONENT"].ToString().Trim();
			}

			if (row.Table.Columns.IndexOf("RJWD_NOTE") > 0)
			{
				flatFileRow.RJWDNote = String.IsNullOrWhiteSpace(row["RJWD_NOTE"].ToString().Trim()) ? null : row["RJWD_NOTE"].ToString().Trim();
			}

			if (row.Table.Columns.IndexOf("CLOSEDATE") > 0)
			{
				DateTime.TryParse(row["CLOSEDATE"].ToString().Trim(), out DateTime closeDateValue);
				if (closeDateValue != DateTime.MinValue)
				{
					flatFileRow.CloseDate = closeDateValue;
				}
			}

			if (row.Table.Columns.IndexOf("REQUESTDATE") > 0)
			{
				DateTime.TryParse(row["REQUESTDATE"].ToString().Trim(), out DateTime requestDateValue);
				if (requestDateValue != DateTime.MinValue)
				{
					flatFileRow.RequestDate = requestDateValue;
				}
			}

			if (row.Table.Columns.IndexOf("RECIEVEDDATE") > 0)
			{
				DateTime.TryParse(row["RECIEVEDDATE"].ToString().Trim(), out DateTime recieveDateValue);
				if (recieveDateValue != DateTime.MinValue)
				{
					flatFileRow.ReceivedDate = recieveDateValue;
				}
			}

			if (row.Table.Columns.IndexOf("BUSINESSPRIORITYDATE") > 0)
			{
				DateTime.TryParse(row["BUSINESSPRIORITYDATE"].ToString().Trim(), out DateTime expediteDateValue);
				if (expediteDateValue != DateTime.MinValue)
				{
					flatFileRow.ExpediteDate = expediteDateValue;
				}
			}

			FlatDataSet.Add(flatFileRow);
		}
		public override void BuildAllElectronicSubmissions()
		{
			var now = DateTime.Now;
			var pndRows = new List<trIGTTemporaryTable>();

			foreach (var row in FlatDataSet)
			{
				if (
					(String.IsNullOrWhiteSpace(row.IdNumber.ToString()))
					|| (Enum.GetNames(typeof(FlatFileStatus)).Contains(row.Status.ToString().ToUpper().Trim()) && String.IsNullOrWhiteSpace(row.SupplierReferenceNumber.ToString()))
					|| (!Enum.GetNames(typeof(FlatFileStatus)).Contains(row.Status.ToString().ToUpper().Trim()) && !String.IsNullOrWhiteSpace(row.SupplierReferenceNumber.ToString()))
				)
				{
					continue;
				}
				else
				{
					var newTempTableRow = new trIGTTemporaryTable
					{
						Manufacturer = Manufacturer,
						sYear = DateTime.Now.ToString("yy"),
						Active = 1,
						LoginID = _user.Id,
						BusinessPriorityDate = row.ExpediteDate,
						CloseDate = row.CloseDate,
						Employee_Name = row.ContactName,
						FollowUpNotes = row.AdditionalNotes,
						Hot = row.Expedite,
						Jurisdiction = row.Jurisdiction,
						Jurisdiction_Description = row.JurisdictionDescription,
						Letter_Number = row.SupplierReferenceNumber,
						Material = row.IdNumber,
						Material_Description = row.GameName,
						BillingReference = row.SupplierJurisdictionalReferenceNumber,
						Project = row.ProjectNumber,
						ReceivedDate = row.ReceivedDate,
						RequestDate = row.RequestDate,
						RJWD_note = row.RJWDNote,
						SpreadSheetName = UploadedFileName,
						Status = row.Status,
						SubmissionNotes = row.Notes,
						InsertDate = now
					};

					// -- IGT decided to call the column of the FF 'Component' but what they put in it is Studio.. so yeah theres that.
					if (!String.IsNullOrWhiteSpace(row.Studio))
					{
						var dbStudio = _dbSubmission.tbl_lst_Studios.SingleOrDefault(x => x.Name.ToLower() == row.Studio.ToLower());
						newTempTableRow.StudioId = dbStudio == null ? CreateNewStudio(row.Studio) : dbStudio.Id;
					}

					pndRows.Add(newTempTableRow);
				}
			}

			ProcessSpecialJurisdictionLogic(pndRows);
			SeekExistingSubmissions(pndRows);
			SaveFlatFile(pndRows);
		}
		public override int CreateNewStudio(string StudioToCreate)
		{
			var studioManufacturerGroupCategoryId =
							_dbSubmission.ManufacturerGroupCategories
								.Where(x => x.Code.ToLower() == ManufacturerGroupCategory.Studios.ToString().ToLower())
								.Single().Id;

			var IGTStudioManufacturerGroupId =
				_dbSubmission.ManufacturerGroups
					.Where(x => x.ManufacturerGroupCategoryId == studioManufacturerGroupCategoryId && x.Description.ToLower() == _IGTSTUDIOGROUPNAME.ToLower())
					.Single().Id;

			var newlyCreatedStudio = new tbl_lst_Studios { ManufacturerGroupId = IGTStudioManufacturerGroupId, Name = StudioToCreate, TestingTypeId = 1 };

			_dbSubmission.tbl_lst_Studios.Add(newlyCreatedStudio);
			_dbSubmission.SaveChanges();

			return newlyCreatedStudio.Id;
		}
		#endregion

		#region Private members

		private void ProcessSpecialJurisdictionLogic(List<trIGTTemporaryTable> potentialRows)
		{
			var materialStatusGroups = potentialRows.GroupBy(x => new { x.Material, x.Status }).ToList();

			foreach (var materialStatusGroup in materialStatusGroups)
			{
				var SpecialJurisdictionCounts = new
				{
					AllSAExists = materialStatusGroup.Where(x => _ALLSA.Contains(x.Jurisdiction)).Select(x => x.Jurisdiction).Distinct().Count(),
					AllSDExists = materialStatusGroup.Where(x => _ALLSD.Contains(x.Jurisdiction)).Select(x => x.Jurisdiction).Distinct().Count(),
					AllNMExists = materialStatusGroup.Where(x => _ALLNM.Contains(x.Jurisdiction)).Select(x => x.Jurisdiction).Distinct().Count(),
					AllSFBingo = materialStatusGroup.Where(x => _ALLSFBINGO.Contains(x.Jurisdiction)).Select(x => x.Jurisdiction).Distinct().Count(),
					AllSFLPM = materialStatusGroup.Where(x => _ALLSFLPM.Contains(x.Jurisdiction)).Select(x => x.Jurisdiction).Distinct().Count(),
					RandomSFBingo = materialStatusGroup.Where(x => _ALLSFBINGO.Contains(x.Jurisdiction)).Select(x => x.Jurisdiction).Distinct().Max(),
					RandomSFLPM = materialStatusGroup.Where(x => _ALLSFLPM.Contains(x.Jurisdiction)).Select(x => x.Jurisdiction).Distinct().Max()
				};

				if (SpecialJurisdictionCounts.AllSAExists == 9)
				{
					foreach (var row in materialStatusGroup.Where(x => _ALLSA.Contains(x.Jurisdiction)))
					{
						HandleAllSAExistsUpdate(row);
					}
				}

				if (SpecialJurisdictionCounts.AllSDExists == 2)
				{
					foreach (var row in materialStatusGroup.Where(x => x.Jurisdiction == "166"))
					{
						HandleAllSDExistsUpdate(row);
					}
				}

				if (SpecialJurisdictionCounts.AllNMExists == 2)
				{
					foreach (var row in materialStatusGroup.Where(x => x.Jurisdiction == "121"))
					{
						HandleAllNMExistsUpdate(row);
					}
				}

				if (SpecialJurisdictionCounts.AllSFBingo > 0)
				{
					foreach (var row in materialStatusGroup.Where(x => _ALLSFBINGO.Contains(x.Jurisdiction) && x.Jurisdiction != "552"))
					{
						HandleAllSFBingoUpdate(row, SpecialJurisdictionCounts.RandomSFBingo);
					}
				}

				if (SpecialJurisdictionCounts.AllSFLPM > 0)
				{

					foreach (var row in materialStatusGroup.Where(x => _ALLSFLPM.Contains(x.Jurisdiction) && x.Jurisdiction != "553"))
					{
						HandleAllSFLPMUpdate(row, SpecialJurisdictionCounts.RandomSFLPM);
					}
				}
			}
		}
		private void SeekExistingSubmissions(List<trIGTTemporaryTable> IGTTempTableEntities)
		{
			var materialToSearchFor =
				IGTTempTableEntities
					.Where(x => x.Status == "XFR" || x.Status == "WDR")
					.GroupBy(x => x.Material)
					.ToList();

			foreach (var materialGroup in materialToSearchFor)
			{
				var material = materialGroup.Max(x => x.Material);

				var existingSubmission =
					_dbSubmission.Submissions
					.Where(x => x.OrgIGTMaterial == material && x.Type != "EE")
					.ToList();

				if (existingSubmission.Count == 1)
				{
					materialGroup
					.ToList()
					.ForEach(x => x.KeyTbl = existingSubmission.First().Id);
				}
			}
		}
		/// <summary>
		/// Commit the flatfile entities to the submissions database.
		/// </summary>
		/// <param name="IGTTempTableEntities">Entities to commit.</param>
		private void SaveFlatFile(List<trIGTTemporaryTable> IGTTempTableEntities)
		{
			UpdateExistingPendingRequests(IGTTempTableEntities);


			_dbSubmission
				.trIGTTemporaryTables
				.AddRange(IGTTempTableEntities);

			SaveChanges();
		}
		private void SaveChanges()
		{
			_dbSubmission.UserId = _user.Id;
			_dbSubmission.SaveChanges();
			_dbSubmission.UserId = null;
		}
		private void UpdateExistingPendingRequests(List<trIGTTemporaryTable> currentRequests)
		{
			foreach (var letterNumberGroup in currentRequests.GroupBy(x => x.Letter_Number).ToList())
			{
				var activeTempTableRows = _dbSubmission.trIGTTemporaryTables.Where(x => x.Letter_Number == letterNumberGroup.Key && x.Active == 1).ToList();
				var transfers = activeTempTableRows.Where(x => x.Status == "XFR").ToList();
				var withdrawalsAndRejections = activeTempTableRows.Where(x => x.Status == "WDR").ToList();

				var newTransfers = currentRequests.Where(x => x.Status == "XFR").ToList();
				foreach (var newTransfer in newTransfers)
				{
					var match = transfers.Where(x => x.Jurisdiction == newTransfer.Jurisdiction && x.Material == newTransfer.Material).SingleOrDefault();

					if (null != match)
					{
						UpdateExistingPendingRequest(match, newTransfer);
						currentRequests.Remove(newTransfer);
					}
				}

				var newWithdrawalRejections = currentRequests.Where(x => x.Status == "WDR").ToList();
				foreach (var newWithdrawalRejection in newWithdrawalRejections)
				{
					var match = transfers.Where(x => x.Jurisdiction == newWithdrawalRejection.Jurisdiction && x.Material == newWithdrawalRejection.Material).SingleOrDefault();

					if (null != match)
					{
						UpdateExistingPendingRequest(match, newWithdrawalRejection);
						currentRequests.Remove(newWithdrawalRejection);
					}
				}
			}
		}
		private void UpdateExistingPendingRequest(trIGTTemporaryTable previous, trIGTTemporaryTable current)
		{
			previous.BusinessPriorityDate = current.BusinessPriorityDate;
			previous.CertLab = current.CertLab;
			previous.CloseDate = current.CloseDate;
			previous.Company = current.Company;
			previous.ContractType = current.ContractType;
			previous.Currency = current.Currency;
			previous.Employee_Name = current.Employee_Name;
			previous.FollowUpNotes = current.FollowUpNotes;
			previous.Hot = current.Hot;
			previous.InsertDate = current.InsertDate;
			previous.Jurisdiction = current.Jurisdiction;
			previous.Jurisdiction_Description = current.Jurisdiction_Description;
			previous.KeyTbl = current.KeyTbl;
			previous.Letter_Number = current.Letter_Number;
			previous.LoginID = current.LoginID;
			previous.Manufacturer = current.Manufacturer;
			previous.MappedKeytbl = current.MappedKeytbl;
			previous.MasterBillingProject = current.MasterBillingProject;
			previous.Material = current.Material;
			previous.Material_Description = current.Material_Description;
			previous.BillingReference = current.BillingReference;
			previous.NewIDnumber = current.NewIDnumber;
			previous.NewJurisdiction = current.NewJurisdiction;
			previous.NewSubFileNumber = current.NewSubFileNumber;
			previous.NewSubItemExist = current.NewSubItemExist;
			previous.Primaryjur = current.Primaryjur;
			previous.Primary_ = current.Primary_;
			previous.Project = current.Project;
			previous.PurchaseOrder = current.PurchaseOrder;
			previous.ReceivedDate = current.ReceivedDate;
			previous.RequestDate = current.RequestDate;
			previous.RJWD_note = current.RJWD_note;
			previous.SavedBy = current.SavedBy;
			previous.SpreadSheetName = current.SpreadSheetName;
			previous.Status = current.Status;
			previous.SubmissionNotes = current.SubmissionNotes;
			previous.SubType = current.SubType;
			previous.sYear = current.sYear;
			previous.TestLab = current.TestLab;
			previous.TXLab = current.TXLab;
		}

		#region IGT Jurisdiction Logic Handlers
		/// <summary>
		/// Handle Special Jurisdiction pre-processing for the trIGTTemporaaryTable for South Africa National
		/// </summary>
		/// <param name="row">IGTTempTable Entity</param>
		private void HandleAllSAExistsUpdate(trIGTTemporaryTable row)
		{
			if (row.Jurisdiction == "63")
			{
				row.Jurisdiction = "49";
				row.Jurisdiction_Description = "SOUTH AFRICA NATIONAL";
			}
			else
			{
				row.Active = 2;
			}
		}

		/// <summary>
		/// Handle Special Jurisdiction pre-processing for the trIGTTemporaaryTable for South Dakota
		/// </summary>
		/// <param name="row">IGTTempTable Entity</param>
		private void HandleAllSDExistsUpdate(trIGTTemporaryTable row)
		{
			row.Active = 2;
		}

		/// <summary>
		/// Handle Special Jurisdiction pre-processing for the trIGTTemporaaryTable for New Mexico
		/// </summary>
		/// <param name="row">IGTTempTable Entity</param>
		private void HandleAllNMExistsUpdate(trIGTTemporaryTable row)
		{
			row.Active = 2;
		}

		/// <summary>
		/// Handle Special Jurisdiction pre-processing for the trIGTTemporaaryTable for South Africa National (LPM)
		/// </summary>
		/// <param name="row">IGTTempTable Entity</param>
		/// <param name="targetJurisdiction">Highest Jurisdiction ID from AllSFLPM Jur List</param>
		private void HandleAllSFLPMUpdate(trIGTTemporaryTable row, string targetJurisdiction)
		{
			if (row.Jurisdiction == targetJurisdiction)
			{
				row.Jurisdiction = "553";
				row.Jurisdiction_Description = "South Africa National (LPM)";
			}
			else
			{
				row.Active = 2;
			}
		}

		/// <summary>
		/// Handle Special Jurisdiction pre-processing for the trIGTTemporaaryTable for South Africa National (Bingo)
		/// </summary>
		/// <param name="row">IGTTempTable Entity</param>
		/// <param name="targetJurisdiction">Highest Jurisdiction ID from AllSFBingo Jur List</param>
		private void HandleAllSFBingoUpdate(trIGTTemporaryTable row, string targetJurisdiction)
		{
			if (row.Jurisdiction == targetJurisdiction)
			{
				row.Jurisdiction = "552";
				row.Jurisdiction_Description = "South Africa National (Bingo)";
			}
			else
			{
				row.Active = 2;
			}
		}
		#endregion

		#endregion
	}
}