﻿using Aspose.Cells;
using GLI.EFCore.Submission;
using Submissions.Common;
using Submissions.Core.Interfaces;
using Submissions.Core.Models.ElectronicSubmissionService;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;

namespace Submissions.Web.Areas.Submission.Models.Import
{
	public class FlatFileDefinition
	{
		#region ReadOnly
		protected readonly SubmissionContext _dbSubmission;
		protected readonly IUser _user;
		protected readonly IElectronicSubmissionService _electronicSubmissionService;
		protected readonly string _FlatfileUploadPath = System.Configuration.ConfigurationManager.AppSettings["FlatfileUploadPath"];
		#endregion

		public FlatFileDefinition(SubmissionContext dbSubmission, IUser user, IElectronicSubmissionService electronicSubmissionService, string manufacturer)
		{
			_dbSubmission = dbSubmission;
			_user = user;
			_electronicSubmissionService = electronicSubmissionService;
			ColumnHeaders = new List<string>();
			FlatDataSet = new List<FlatFileDTO>();
			ProjectNotes = new List<ElectronicSubmissionNoteDTO>();
			Manufacturer = manufacturer;
		}

		#region Properties
		public List<string> ColumnHeaders { get; set; }
		public List<FlatFileDTO> FlatDataSet { get; set; }
		public List<ElectronicSubmissionNoteDTO> ProjectNotes { get; set; }
		public string GroupByKey { get; set; }
		public string OriginalFlatFileName { get; set; }
		public string WorksheetName { get; set; }
		public HttpPostedFileBase File { get; set; }
		public DataTable DataTable { get; set; }
		public string Manufacturer { get; set; }
		public string UploadedFileName { get; set; }
		public Guid? ReferenceNumber { get; set; }
		#endregion

		#region Virtual Methods
		public virtual void BuildAllElectronicSubmissions()
		{
			throw new NotImplementedException();
		}
		public virtual void ParseRow(DataRow row, int rowNum)
		{
			throw new NotImplementedException();
		}
		public virtual int BuildNewSubmissions()
		{
			throw new NotImplementedException();
		}
		public virtual int BuildTransferRequests()
		{
			throw new NotImplementedException();
		}
		public virtual ElectronicSubmissionHeaderDTO BuildElectronicSubmissionObjectGraph(List<IGrouping<string, FlatFileDTO>> requests, ElectronicSubmissionJurisdictionalRequestAction elcetronicSubmissionAction)
		{
			throw new NotImplementedException();
		}
		public virtual ElectronicSubmissionHeaderDTO BuildElectronicSubmissionHeaderDTO(List<IGrouping<string, FlatFileDTO>> requests, ElectronicSubmissionJurisdictionalRequestAction elcetronicSubmissionAction)
		{
			var now = DateTime.Now;

			var header = new ElectronicSubmissionHeaderDTO()
			{
				Active = true,
				AddDate = now,
				CertificationLabId = null,
				ConfirmId = null,
				EditDate = null,
				ElectronicSubmissionSourceId = _dbSubmission.tbl_lst_ElectronicSubmissionSources.Where(x => x.Code == ElectronicSubmissionType.FLATFILE.ToString()).Single().Id,
				FileNumber = null,
				ManufacturerCode = Manufacturer,
				Memo = null,
				NumberOfItems = requests.Count,
				PrimaryJurisdictionId = null,
				ProcessedBy = null,
				ProcessedDate = null,
				PublicId = null,
				ReceivedDate = null,
				RequestSourceFileName = UploadedFileName,
				Status = SubmitRequestHeaderStatus.Pending.ToString(),
				SubmissionTypeId = null,
				SubmittedDate = null,
				SubmitterEmail = null,
				SubmitterName = requests.Max(x => x.First().ContactName),
				SubmitterPhone = null,
				SupplierCode1 = null,
				SupplierCode2 = null,
				SupplierReferenceNumber = File.FileName,
				TestLabId = null,
				AddedById = _user.Id,
				ElectronicSubmissionActionId = _dbSubmission.tbl_lst_ElectronicSubmissionActions.Where(x => x.Code.ToLower() == elcetronicSubmissionAction.ToString().ToLower()).Single().Id
			};

			return header;
		}
		public virtual ElectronicSubmissionComponentDTO BuildElectronicSubmissionComponentDTO(IGrouping<string, FlatFileDTO> componentData)
		{
			var now = DateTime.Now;

			var tempComponent = componentData.First();
			var manufGroupIds = _dbSubmission.tbl_lst_fileManuManufacturerGroups.Where(x => x.ManufacturerCode == tempComponent.ManufacturerCode).Select(x => x.ManufacturerGroupId).ToList();

			var productLines = _dbSubmission.tbl_lst_ProductLines
				.Where(x => manufGroupIds.Contains((int)x.ManufacturerGroupId))
				.Select(x => new { x.Name, x.Id })
				.ToDictionary(x => x.Name.Replace(" ", "").Trim().ToLower(), x => x.Id);
			var productlineId = productLines.ContainsKey(tempComponent.ProductLine) ? productLines[tempComponent.ProductLine] : (int?)null;
			var studioId = _dbSubmission.tbl_lst_Studios.Where(x => x.Name == tempComponent.Studio && manufGroupIds.Contains((int)x.ManufacturerGroupId)).Select(x => x.Id).SingleOrDefault();
			var functionId = _dbSubmission.tbl_lst_EPROMFunction.Where(x => x.Code == tempComponent.Function).Select(x => x.Id).SingleOrDefault();
			var secondaryFunctionId = DeriveSecondaryFunctionId(tempComponent);

			var component = new ElectronicSubmissionComponentDTO()
			{
				AddDate = now,
				ChipNumber = null,
				ChipType = null,
				ComponentCode1 = null,
				ComponentCode2 = null,
				ComponentIdentifier = componentData.Key,
				ComponentNumber = null,
				ComponentName = componentData.First().GameName,
				DateCode = componentData.First().Datecode,
				EditDate = null,
				ElectronicSubmissionHeaderId = null,
				FunctionId = functionId,
				ManufacturerCode = Manufacturer,
				Position = componentData.First().Position,
				ProductLineId = productlineId != 0 ? productlineId : (int?)null,
				PublicId = null,
				StudioId = studioId != 0 ? studioId : (int?)null,
				SubmissionRecordId = null,
				TrackingNumber = null,
				Version = tempComponent.Version,
				Platforms = tempComponent.Platform,
				VendorId = componentData.First().VendorId,
				SecondaryFunctionId = secondaryFunctionId
			};

			return component;
		}

		public int? DeriveSecondaryFunctionId(FlatFileDTO componentData)
		{
			var tempFunction = componentData.Function.Replace(" ", "").Trim().ToLower();
			var secondaryFunctions = _dbSubmission.SubmissionsSecondaryFunctions.ToList();
			var hardwareId = secondaryFunctions.Where(x => x.Name.Contains(SecondaryFunction.Hardware.ToString())).Select(x => x.Id).FirstOrDefault();
			var softwareId = secondaryFunctions.Where(x => x.Name.Contains(SecondaryFunction.Software.ToString())).Select(x => x.Id).FirstOrDefault();
			int? secondaryFunctionId = null;
			var hasSignatures = string.IsNullOrEmpty(componentData.VerifyPlusCDCK) && string.IsNullOrEmpty(componentData.VerifyPlusMD5) && string.IsNullOrEmpty(componentData.VerifyPlusSHA1) ? false : true;

			if (tempFunction == Function.Hardware.GetEnumDescription().ToLower() || tempFunction == Function.Machine.GetEnumDescription().ToLower())
			{
				secondaryFunctionId = hardwareId;
			}
			else if ((tempFunction == Function.BillValidator.GetEnumDescription().Replace(" ", "").ToLower() ||
			  tempFunction == Function.Printer.GetEnumDescription().ToLower()) && hasSignatures)
			{
				secondaryFunctionId = softwareId;
			}
			else if ((tempFunction == Function.BillValidator.GetEnumDescription().Replace(" ", "").ToLower() ||
			tempFunction == Function.Printer.GetEnumDescription().ToLower()) && !hasSignatures)
			{
				secondaryFunctionId = hardwareId;
			}
			else
			{
				secondaryFunctionId = softwareId;
			}
			return secondaryFunctionId;
		}

		public virtual ElectronicSubmissionJurisdictionalDataDTO BuildElectronicSubmissionJurisdictionalDataRequestDTO(FlatFileDTO jurisdictionalDataRequest)
		{
			var now = DateTime.Now;
			Int32.TryParse(jurisdictionalDataRequest.Jurisdiction, out int integerJurisdcitionId);

			var newjurisdictionalDataRequest = new ElectronicSubmissionJurisdictionalDataDTO()
			{
				AddDate = now,
				CompanyId = null,
				ContractId = null,
				ContractNumber = null,
				CurrencyId = null,
				EditDate = null,
				ElectronicSubmissionActionId = _dbSubmission.tbl_lst_ElectronicSubmissionActions.Where(x => x.Code == jurisdictionalDataRequest.Status).Single().Id,
				Expedite = false,
				ExternalJurisdictionId = null,
				ExternalJurisdictionName = null,
				InternalJurisdictionId = integerJurisdcitionId,
				InternalJurisdictionName = _dbSubmission.tbl_lst_fileJuris.Where(x => x.FixId == integerJurisdcitionId).Select(x => x.Name).Single(),
				JurisdictionalDataId = null,
				JurisdictionCode1 = null,
				JurisdictionCode2 = null,
				ProjectNumber = null,
				PublicId = null,
				ReceivedDate = null,
				RequestedDate = null,
				SupplierSpecific1 = null,
				SupplierSpecific2 = null,
				SupplierSpecific3 = null,
				SupplierSpecific4 = null,
				WorkOrderNumber = null,
			};

			return newjurisdictionalDataRequest;
		}
		public virtual ElectronicSubmissionNoteDTO BuildElectonicSubmissionNoteDTO(DateTime addDate, string text, string type, bool forProject)
		{
			var note = new ElectronicSubmissionNoteDTO()
			{
				AddDate = addDate,
				Text = text,
				Type = type,
				ForProject = forProject
			};

			return note;
		}
		public virtual List<ElectronicSubmissionSignatureDTO> BuildElectronicSubmissionSignatureDTOs(FlatFileDTO componentData)
		{
			var now = DateTime.Now;
			var signatures = new List<ElectronicSubmissionSignatureDTO>();
			var signatureTypes = _dbSubmission.tbl_lst_SignatureTypes
				.Where(x => x.Type == "Verify+ SHA1" || x.Type == "Verify+ CDCK" || x.Type == "Verify+ MD5")
				.Select(x => new { x.Id, x.Type, x.DefaultVersionId })
				.ToList();

			if (!String.IsNullOrWhiteSpace(componentData.VerifyPlusSHA1))
			{
				var signature = new ElectronicSubmissionSignatureDTO()
				{
					AddDate = now,
					EditDate = null,
					EndOffset = null,
					FilePath = null,
					PublicId = null,
					Result = componentData.VerifyPlusSHA1,
					Salt = null,
					Seed = null,
					ScopeId = null,
					StartOffset = null,
					TypeId = signatureTypes.Single(x => x.Type == "Verify+ SHA1").Id,
					UsedOffsets = false,
					UsedSalt = false,
					UsedSeed = false,
					VersionId = signatureTypes.Single(x => x.Type == "Verify+ SHA1").DefaultVersionId
				};
				signatures.Add(signature);
			}

			if (!String.IsNullOrWhiteSpace(componentData.VerifyPlusCDCK))
			{
				var signature = new ElectronicSubmissionSignatureDTO()
				{
					AddDate = now,
					EditDate = null,
					EndOffset = null,
					FilePath = null,
					PublicId = null,
					Result = componentData.VerifyPlusCDCK,
					Salt = null,
					Seed = null,
					ScopeId = null,
					StartOffset = null,
					TypeId = signatureTypes.Single(x => x.Type == "Verify+ CDCK").Id,
					UsedOffsets = false,
					UsedSalt = false,
					UsedSeed = false,
					VersionId = signatureTypes.Single(x => x.Type == "Verify+ CDCK").DefaultVersionId
				};
				signatures.Add(signature);
			}

			if (!String.IsNullOrWhiteSpace(componentData.VerifyPlusMD5))
			{
				var signature = new ElectronicSubmissionSignatureDTO()
				{
					AddDate = now,
					EditDate = null,
					EndOffset = null,
					FilePath = null,
					PublicId = null,
					Result = componentData.VerifyPlusMD5,
					Salt = null,
					Seed = null,
					ScopeId = null,
					StartOffset = null,
					TypeId = signatureTypes.Single(x => x.Type == "Verify+ MD5").Id,
					UsedOffsets = false,
					UsedSalt = false,
					UsedSeed = false,
					VersionId = signatureTypes.Single(x => x.Type == "Verify+ MD5").DefaultVersionId
				};
				signatures.Add(signature);
			}

			return signatures;
		}
		public virtual ElectronicSubmissionDocumentDTO BuildElectronicSubmissionDocumentDTO(string internalPath, string type, string name)
		{
			var now = DateTime.Now;

			return new ElectronicSubmissionDocumentDTO
			{
				AddDate = now,
				InternalPath = internalPath,
				Type = type,
				Name = name
			};
		}
		public virtual string GenerateFlatFileName(string fileName)
		{
			var timeStamp = DateTime.Now;
			return
				new StringBuilder()
				.Append(_user.Id)
				.Append("_")
				.Append(timeStamp.Day)
				.Append("-")
				.Append(timeStamp.Month)
				.Append("-")
				.Append(timeStamp.Year)
				.Append("_")
				.Append(timeStamp.Hour)
				.Append("-")
				.Append(timeStamp.Minute)
				.Append("-")
				.Append(timeStamp.Second)
				.Append("_")
				.Append(fileName)
				.ToString();
		}
		public virtual void UploadFlatFile()
		{
			var newFileName = GenerateFlatFileName(Path.GetFileName(File.FileName));
			var fullNewFileName = Path.Combine(_FlatfileUploadPath, newFileName);
			File.SaveAs(fullNewFileName);
			UploadedFileName = newFileName;
		}
		public virtual void ReadFlatFileToDataTable()
		{
			var workbook = new Workbook(File.InputStream);
			var worksheets = workbook.Worksheets.ToList();
			foreach (var sheet in worksheets)
			{
				sheet.Name = sheet.Name.Trim();
			}

			var worksheet = workbook.Worksheets[WorksheetName];
			if (worksheet != null)
			{
				var includedColumns = new List<ExcelColumn>();
				var excludedColumns = new List<int>();


				for (int i = 0; i < worksheet.Cells.MaxDisplayRange.ColumnCount; i++)
				{
					if (includedColumns.Any(x => x.Name.Equals(worksheet.Cells[0, i].StringValue.Trim(), StringComparison.CurrentCultureIgnoreCase)))
					{
						excludedColumns.Add(i);
					}
					else
					{
						includedColumns.Add(new ExcelColumn() { Name = worksheet.Cells[0, i].StringValue.Trim(), Index = i });
					}
				}

				excludedColumns.Reverse();
				excludedColumns.ForEach(x => { worksheet.Cells.DeleteColumn(x); });

				worksheet.Cells.DeleteBlankRows();
				worksheet.Cells.DeleteBlankColumns();

				Style style = workbook.CreateStyle();
				style.Number = 14;
				StyleFlag flag = new StyleFlag();
				flag.NumberFormat = true;

				FindOptions opts = new FindOptions();
				opts.LookInType = LookInType.Values;
				opts.LookAtType = LookAtType.Contains;

				Cell DateCell = worksheet.Cells.Find("Date", null, opts);
				if (DateCell != null)
				{
					worksheet.Cells.Columns[DateCell.Column].ApplyStyle(style, flag);
				}
				var dataTable = worksheet.Cells.ExportDataTableAsString(0, 0, worksheet.Cells.MaxDataRow + 1, includedColumns.Count, true);
				for (var i = 0; i < dataTable.Columns.Count; i++)
				{
					dataTable.Columns[i].ColumnName = dataTable.Columns[i].ColumnName.ToString().ToUpper().Trim().Replace(" ", "");
				}

				DataTable = dataTable;
			}
		}
		public virtual void ParseFlatFileDataTable()
		{
			for (int i = 0; i < DataTable.Rows.Count; i++)
			{
				ParseRow(DataTable.Rows[i], i);
			}
		}
		public virtual int CreateNewStudio(string StudioToCreate)
		{
			throw new NotImplementedException();
		}
		#endregion

		public class ExcelColumn
		{
			public string Name { get; set; }
			public int Index { get; set; }
		}
	}
}