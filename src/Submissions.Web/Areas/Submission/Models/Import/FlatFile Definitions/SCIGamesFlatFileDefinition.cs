﻿using GLI.EFCore.Submission;
using Submissions.Common;
using Submissions.Core.Interfaces;
using Submissions.Core.Models.ElectronicSubmissionService;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace Submissions.Web.Areas.Submission.Models.Import
{
	public class SCIGamesFlatFileDefinition : FlatFileDefinition
	{
		#region Readonly Variables
		private readonly List<string> _COLUMNHEADERS = new List<string> {
			"PARTNAME",
			"PARTNUMBER",
			"PLATFORM",
			"JURISDICTIONNUMBER",
			"JURISDICTIONNAME",
			"VERIFY+SHA-1",
			"VERIFY+CDCK",
			"VERIFY+MD5",
			"DATECODE",
			"STATUS",
			"CONTACTNAME",
			"GLIBILLINGNOTE",
			"POSITION",
			"CLONEORPRE",
			"SRM/DMG/FLEXERA",
			"SPECIALAPPROVALNUMBERS",
			"NOTES",
			"WORKSWITH",
			"PRODUCTLINE",
			"VERSION",
			"FUNCTION"
		};

		private readonly BillingDeriver _billingDeriver;
		private readonly VendorDeriver _vendorDeriver;
		private readonly Lookup _lookup;

		private string functionHolder;

		private readonly Dictionary<ErrorCode, string> _errorMessage = new Dictionary<ErrorCode, string>
		{
			{ ErrorCode.ISNULL_EMPTY_GAMENAME, "Partname is empty."},
			{ ErrorCode.ISNULL_EMPTY_IDNUMBER, "Partnumber is empty." },
			{ ErrorCode.ISNULL_EMPTY_PLATFORM, "Platform is empty." },
			{ ErrorCode.NOT_IN_DB_PLATFORM, "Platform doesn't exist in our database." },
			{ ErrorCode.MANUF_GROUP_PLATFORM, "Platform doesn't exist in Manufacture Group." },
			{ ErrorCode.ISNULL_EMPTY_JUR, "Jurisdiction is empty." },
			{ ErrorCode.NOT_IN_DB_JUR, "Jurisdiction mapping doesn't exist in our database." },
			{ ErrorCode.INACTIVE_JUR, "Jurisdiction is not active in our database." },
			{ ErrorCode.ISNULL_EMPTY_STUDIO, "Studio is empty." },
			{ ErrorCode.NOT_IN_DB_STUDIO, "Studio doesn't exist in our database." },
			{ ErrorCode.MANUF_GROUP_STUDIO, "Studio doesn't exist in Manufacturer Group." },
			{ ErrorCode.ISNULL_EMPTY_PRODUCTLINE, "Productline is empty." },
			{ ErrorCode.NOT_IN_DB_PRODUCTLINE, "Productline doesn't exist in our database." },
			{ ErrorCode.MANUF_GROUP_PRODUCTLINE, "Productline doesn't exist in Manufacturer Group." },
			{ ErrorCode.ISNULL_EMPTY_STATUS, "Status is empty." },
			{ ErrorCode.PND_OHR_STATUS, "Status is not PND or OHR."},
			{ ErrorCode.ISNULL_EMPTY_POSITION, "Position is empty." },
			{ ErrorCode.NOT_IN_DB_POSITION, "Position doesn't exist in our database." },
			{ ErrorCode.ISNULL_EMPTY_VERSION, "Version is empty." },
			{ ErrorCode.ISNULL_EMPTY_FUNCTION, "Function is empty." },
			{ ErrorCode.INACTIVE_FUNCTION, "Function is not active in our database." },
			{ ErrorCode.NOT_IN_DB_FUNCTION, "Function code doesn't match in our database." },
			{ ErrorCode.ISNULL_EMPTY_BILLINGNOTE, "GLI Billing note is empty." },
			{ ErrorCode.DUPLICATE_ID_MANUF_VERSION_SUBMISSION, "Found duplicate Idnumber, Version and manufacturer in Submissions." },
			{ ErrorCode.DUPLICATE_ID_MANUF_VERSION_STAGING, "Found duplicate Idnumber, Version and manufacturer in Staging." },
			{ ErrorCode.INVALID_PRODUCTLINE, "Invalid Productline."},
			{ ErrorCode.INVALID_MANUFACTURER, "Invalid Manufacturer."},
			{ErrorCode.UNABLE_TO_DERIVE_BILLINGINFO, "Unable to derive Billing information using productline." },
			{ErrorCode.PARTNAME_MISSING, "PARTNAME Column is missing." },
			{ErrorCode.PARTNAME_LENGTH, "Partname contains more than 60 characters." },
			{ErrorCode.PARTNUMBER_MISSING, "PARTNUMBER Column is missing." },
			{ErrorCode.PLATFORM_MISSING, "PLATFORM Column is missing." },
			{ErrorCode.JURISDICTIONNUMBER_MISSING, "JURISDICTIONNUMBER Column is missing." },
			{ErrorCode.JURISDICTIONNAME_MISSING, "JURISDICTIONNAME Column is missing." },
			{ErrorCode.VERIFYSHA1_MISSING, "VERIFY+SHA-1 Column is missing." },
			{ErrorCode.VERIFYCDCK_MISSING, "VERIFY+CDCK Column is missing." },
			{ErrorCode.VERIFYMD5_MISSING, "VERIFY+MD5 Column is missing." },
			{ErrorCode.DATECODE_MISSING, "DATECODE Column is missing." },
			{ErrorCode.STATUS_MISSING, "STATUS Column is missing." },
			{ErrorCode.VERSION_MISSING, "VERSION Column is missing." },
			{ErrorCode.FUNCTION_MISSING, "FUNCTION_MISSING Column is missing." },
			{ErrorCode.POSITION_MISSING, "POSITION Column is missing." },
			{ErrorCode.CLONEORPRE_MISSING, "CLONEORPRE Column is missing." },
			{ErrorCode.SRMDMGFLEXERA_MISSING, "SRM/DMG/FLEXERA Column is missing." },
			{ErrorCode.SPECIALAPPROVALNUMBERS_MISSING, "SPECIALAPPROVALNUMBERS Column is missing." },
			{ErrorCode.NOTES_MISSING, "Notes Column is missing." },
			{ErrorCode.WORKSWITH_MISSING, "WORKSWITH Column is missing." },
			{ErrorCode.PRODUCTLINE_MISSING, "PRODUCTLINE Column is missing." },
			{ErrorCode.VENDOR_GAMENAME_MISSING, "Unable to derive vendor for function printer or bill validator."},
			{ErrorCode.OH_STATUS, "OH Status" },
			{ErrorCode.ALL_JUR_OH, "All Jurisdictions are OH. Set one of the Jurisdiction status to PND." },
			{ErrorCode.MATH_OH, "Math is OH. Set the status to PND."},
			{ErrorCode.NULL_WORKSHEET, "Rename worksheet to FLAT FILE"},
			{ErrorCode.INVALID_REVISIONREASON, "Revision reason doesn't exist in our database."}
		};
		public List<ColumnError> Errors;
		enum ErrorCode
		{
			ISNULL_EMPTY_GAMENAME,
			ISNULL_EMPTY_IDNUMBER,
			DUPLICATE_ID_MANUF_VERSION_SUBMISSION,
			DUPLICATE_ID_MANUF_VERSION_STAGING,
			ISNULL_EMPTY_PLATFORM,
			NOT_IN_DB_PLATFORM,
			MANUF_GROUP_PLATFORM,
			ISNULL_EMPTY_JUR,
			NOT_IN_DB_JUR,
			INACTIVE_JUR,
			ISNULL_EMPTY_STUDIO,
			NOT_IN_DB_STUDIO,
			MANUF_GROUP_STUDIO,
			ISNULL_EMPTY_PRODUCTLINE,
			NOT_IN_DB_PRODUCTLINE,
			MANUF_GROUP_PRODUCTLINE,
			ISNULL_EMPTY_STATUS,
			PND_OHR_STATUS,
			ISNULL_EMPTY_POSITION,
			NOT_IN_DB_POSITION,
			ISNULL_EMPTY_VERSION,
			ISNULL_EMPTY_FUNCTION,
			NOT_IN_DB_FUNCTION,
			INACTIVE_FUNCTION,
			ISNULL_EMPTY_BILLINGNOTE,
			INVALID_PRODUCTLINE,
			INVALID_MANUFACTURER,
			UNABLE_TO_DERIVE_BILLINGINFO,
			PARTNAME_MISSING,
			PARTNAME_LENGTH,
			PARTNUMBER_MISSING,
			PLATFORM_MISSING,
			JURISDICTIONNUMBER_MISSING,
			JURISDICTIONNAME_MISSING,
			VERIFYSHA1_MISSING,
			VERIFYCDCK_MISSING,
			VERIFYMD5_MISSING,
			DATECODE_MISSING,
			STATUS_MISSING,
			VERSION_MISSING,
			FUNCTION_MISSING,
			POSITION_MISSING,
			CLONEORPRE_MISSING,
			SRMDMGFLEXERA_MISSING,
			SPECIALAPPROVALNUMBERS_MISSING,
			NOTES_MISSING,
			WORKSWITH_MISSING,
			PRODUCTLINE_MISSING,
			OH_STATUS,
			VENDOR_GAMENAME_MISSING,
			ALL_JUR_OH,
			MATH_OH,
			NULL_WORKSHEET,
			INVALID_REVISIONREASON
		}
		#endregion

		public SCIGamesFlatFileDefinition(SubmissionContext dbSubmission, IUser user,
			IElectronicSubmissionService electronicSubmissionService, string manufacturer) : base(dbSubmission, user, electronicSubmissionService, manufacturer)
		{
			ColumnHeaders = _COLUMNHEADERS;
			GroupByKey = "PARTNUMBER";
			WorksheetName = "Flat File";
			_billingDeriver = new BillingDeriver();
			Errors = new List<ColumnError>();
			Manufacturer = manufacturer ?? CustomManufacturer.BLY.ToString();
			_vendorDeriver = new VendorDeriver(dbSubmission);
			_lookup = new Lookup(dbSubmission);
		}

		#region Method Overides
		public override void ParseFlatFileDataTable()
		{
			if (DataTable == null)
			{
				Errors.Add(new ColumnError
				{
					RowNumber = -1,
					Column = "",
					Value = "",
					ErrorCodeDescription = _errorMessage[ErrorCode.NULL_WORKSHEET]
				});
				return;
			}

			if (!DataTable.Columns.Contains("PARTNAME"))
			{
				Errors.Add(new ColumnError()
				{
					RowNumber = -1,
					Column = "PARTNAME",
					Value = "",
					ErrorCodeDescription = _errorMessage[ErrorCode.PARTNAME_MISSING]
				});
			}
			if (!DataTable.Columns.Contains("PARTNUMBER"))
			{
				Errors.Add(new ColumnError()
				{
					RowNumber = -1,
					Column = "PARTNUMBER",
					Value = "",
					ErrorCodeDescription = _errorMessage[ErrorCode.PARTNUMBER_MISSING]
				});
			}
			if (!DataTable.Columns.Contains("WORKSWITH"))
			{
				Errors.Add(new ColumnError()
				{
					RowNumber = -1,
					Column = "WORKSWITH",
					Value = "",
					ErrorCodeDescription = _errorMessage[ErrorCode.WORKSWITH_MISSING]
				});
			}
			if (!DataTable.Columns.Contains("CLONEORPRE"))
			{
				Errors.Add(new ColumnError()
				{
					RowNumber = -1,
					Column = "CLONEORPRE",
					Value = "",
					ErrorCodeDescription = _errorMessage[ErrorCode.CLONEORPRE_MISSING]
				});
			}
			if (!DataTable.Columns.Contains("JURISDICTIONNUMBER"))
			{
				Errors.Add(new ColumnError()
				{
					RowNumber = -1,
					Column = "JURISDICTIONNUMBER",
					Value = "",
					ErrorCodeDescription = _errorMessage[ErrorCode.JURISDICTIONNUMBER_MISSING]
				});
			}
			if (!DataTable.Columns.Contains("JURISDICTIONNAME"))
			{
				Errors.Add(new ColumnError()
				{
					RowNumber = -1,
					Column = "JURISDICTIONNAME",
					Value = "",
					ErrorCodeDescription = _errorMessage[ErrorCode.JURISDICTIONNAME_MISSING]
				});
			}
			if (!DataTable.Columns.Contains("DATECODE"))
			{
				Errors.Add(new ColumnError()
				{
					RowNumber = -1,
					Column = "DATECODE",
					Value = "",
					ErrorCodeDescription = _errorMessage[ErrorCode.DATECODE_MISSING]
				});
			}
			if (!DataTable.Columns.Contains("VERSION"))
			{
				Errors.Add(new ColumnError()
				{
					RowNumber = -1,
					Column = "VERSION",
					Value = "",
					ErrorCodeDescription = _errorMessage[ErrorCode.VERSION_MISSING]
				});
			}
			if (!DataTable.Columns.Contains("MD5"))
			{
				if (!DataTable.Columns.Contains("VERIFY+MD5"))
				{
					Errors.Add(new ColumnError()
					{
						RowNumber = -1,
						Column = "Verify+MD5",
						Value = "",
						ErrorCodeDescription = _errorMessage[ErrorCode.VERIFYMD5_MISSING]
					});
				}
			}
			if (!DataTable.Columns.Contains("Notes"))
			{
				Errors.Add(new ColumnError()
				{
					RowNumber = -1,
					Column = "Notes",
					Value = "",
					ErrorCodeDescription = _errorMessage[ErrorCode.NOTES_MISSING]
				});
			}
			if (!DataTable.Columns.Contains("PLATFORM"))
			{
				Errors.Add(new ColumnError()
				{
					RowNumber = -1,
					Column = "PLATFORM",
					Value = "",
					ErrorCodeDescription = _errorMessage[ErrorCode.PLATFORM_MISSING]
				});
			}
			if (!DataTable.Columns.Contains("POSITION"))
			{
				Errors.Add(new ColumnError()
				{
					RowNumber = -1,
					Column = "POSITION",
					Value = "",
					ErrorCodeDescription = _errorMessage[ErrorCode.POSITION_MISSING]
				});
			}
			if (!DataTable.Columns.Contains("PRODUCTLINE"))
			{
				Errors.Add(new ColumnError()
				{
					RowNumber = -1,
					Column = "PRODUCTLINE",
					Value = "",
					ErrorCodeDescription = _errorMessage[ErrorCode.PRODUCTLINE_MISSING]
				});
			}
			if (!DataTable.Columns.Contains("SPECIALAPPROVALNUMBERS"))
			{
				Errors.Add(new ColumnError()
				{
					RowNumber = -1,
					Column = "SPECIALAPPROVALNUMBERS",
					Value = "",
					ErrorCodeDescription = _errorMessage[ErrorCode.SPECIALAPPROVALNUMBERS_MISSING]
				});
			}
			if (!DataTable.Columns.Contains("SRM/DMG/FLEXERA"))
			{
				Errors.Add(new ColumnError()
				{
					RowNumber = -1,
					Column = "SRM/DMG/FLEXERA",
					Value = "",
					ErrorCodeDescription = _errorMessage[ErrorCode.SRMDMGFLEXERA_MISSING]
				});
			}
			if (!DataTable.Columns.Contains("STATUS"))
			{
				Errors.Add(new ColumnError()
				{
					RowNumber = -1,
					Column = "STATUS",
					Value = "",
					ErrorCodeDescription = _errorMessage[ErrorCode.STATUS_MISSING]
				});
			}
			if (!DataTable.Columns.Contains("VERIFY+CDCK"))
			{
				Errors.Add(new ColumnError()
				{
					RowNumber = -1,
					Column = "VERIFY+CDCK",
					Value = "",
					ErrorCodeDescription = _errorMessage[ErrorCode.VERIFYCDCK_MISSING]
				});
			}
			if (!DataTable.Columns.Contains("VERIFY+SHA-1"))
			{
				Errors.Add(new ColumnError()
				{
					RowNumber = -1,
					Column = "VERIFY+SHA-1",
					Value = "",
					ErrorCodeDescription = _errorMessage[ErrorCode.VERIFYSHA1_MISSING]
				});
			}
			if (!DataTable.Columns.Contains("FUNCTION"))
			{
				Errors.Add(new ColumnError()
				{
					RowNumber = -1,
					Column = "FUNCTION",
					Value = "",
					ErrorCodeDescription = _errorMessage[ErrorCode.FUNCTION_MISSING]
				});
			}

			if (!Errors.Where(x => x.RowNumber < 0).Any())
			{
				for (int i = 0; i < DataTable.Rows.Count; i++)
				{
					ParseRow(DataTable.Rows[i], i);
				}
			}

			var nonMathRecords = FlatDataSet
				.Where(x => x.Function != Function.MathAnalysis.GetEnumDescription())
				.ToList();
			if (nonMathRecords.Count > 0)
			{
				var jurisdictionsOnHold = nonMathRecords.Where(x => x.Status == ElectronicSubmissionJurisdictionalRequestAction.OHR.ToString()).Count() == nonMathRecords.Count();
				if (jurisdictionsOnHold)
				{
					Errors.Add(new ColumnError()
					{
						RowNumber = -1,
						Column = "Status",
						Value = ElectronicSubmissionJurisdictionalRequestAction.OHR.ToString(),
						ErrorCodeDescription = _errorMessage[ErrorCode.ALL_JUR_OH]
					});
				}
			}


			//Handle Math validations
			var mathRecords = FlatDataSet.Where(x => x.Function == Function.MathAnalysis.GetEnumDescription()).ToList();
			if (mathRecords.GroupBy(x => x.IdNumber).Count() != mathRecords.Count())
			{
				Errors.Add(new ColumnError()
				{
					RowNumber = -1,
					Column = "Partnumber",
					Value = "",
					ErrorCodeDescription = _errorMessage[ErrorCode.DUPLICATE_ID_MANUF_VERSION_SUBMISSION]
				});
			}
		}
		public override void ParseRow(DataRow row, int rownum)
		{
			var flatFileRow = new FlatFileDTO()
			{
				GameName = row["PARTNAME"].ToString().Trim(),
				IdNumber = row["PARTNUMBER"].ToString().Trim(),
				ContactName = row.Table.Columns.Contains("Contactname") ? row["CONTACTNAME"].ToString().Trim() : null,
				CloneOrPreCertification = row["CLONEORPRE"].ToString().Trim(),
				Jurisdiction = row["JURISDICTIONNUMBER"].ToString().Trim(),
				JurisdictionDescription = row["JURISDICTIONNAME"].ToString().Trim(),
				Datecode = row["DATECODE"].ToString().Trim(),
				ManufacturerBillingNote = row.Table.Columns.Contains("GliBillingNote") ? row["GLIBILLINGNOTE"].ToString().Trim() : null,
				VerifyPlusMD5 = row.Table.Columns.Contains("MD5") ? row["MD5"].ToString().Trim() :
				row.Table.Columns.Contains("Verify+MD5") ? row["Verify+MD5"].ToString().Trim() : "",
				Notes = row["Notes"].ToString().Trim(),
				Platform = row["PLATFORM"].ToString().Trim(),
				Position = row["POSITION"].ToString().Trim(),
				ProductLine = row["PRODUCTLINE"].ToString().Trim(),
				SpecialApprovalNumbers = row["SPECIALAPPROVALNUMBERS"].ToString().Trim(),
				SRM_DMG_Flexera = row["SRM/DMG/FLEXERA"].ToString().Trim(),
				Status = row["STATUS"].ToString().Trim(),
				VerifyPlusCDCK = row["VERIFY+CDCK"].ToString().Trim(),
				VerifyPlusSHA1 = row["VERIFY+SHA-1"].ToString().Trim(),
				WorksWith = row["WORKSWITH"].ToString().Trim(),
				Studio = row.Table.Columns.Contains("Studio") ? row["STUDIO"].ToString().Trim() : null,
				Version = row["VERSION"].ToString().Trim(),
				Function = row["FUNCTION"].ToString().Trim(),
				RevisionReason = row.Table.Columns.Contains("REVISIONREASON") ? row["REVISIONREASON"].ToString().ToLower().Replace(" ", "").Trim() : null,
				MarketCode = row.Table.Columns.Contains("MARKETCODE") ? row["MARKETCODE"].ToString().Trim() : null
			};

			HandleDerivationRules(flatFileRow, rownum);

			FlatDataSet.Add(flatFileRow);
		}
		public override ElectronicSubmissionJurisdictionalDataDTO BuildElectronicSubmissionJurisdictionalDataRequestDTO(FlatFileDTO jurisdictionalDataRequest)
		{
			var now = DateTime.Now;
			Int32.TryParse(jurisdictionalDataRequest.Jurisdiction, out int integerJurisdcitionId);
			var electronicSubmissionAction = _dbSubmission.tbl_lst_ElectronicSubmissionActions.Where(x => x.Code == jurisdictionalDataRequest.Status).Single();
			var jurisdictionName = _dbSubmission.tbl_lst_fileJuris.Where(x => x.FixId == integerJurisdcitionId).Select(x => x.Name).Single();

			var sgiMapping = _dbSubmission.tbl_lst_JurisdictionMappings.Where(x => x.ExternalJurisdictionId == jurisdictionalDataRequest.Jurisdiction && x.ManufacturerCode == CustomManufacturer.SGI.ToString()).First();
			Int32.TryParse(sgiMapping.JurisdictionId, out int internalIntegerJurisdcitionId);


			var newjurisdictionalDataRequest = new ElectronicSubmissionJurisdictionalDataDTO()
			{
				AddDate = now,
				CompanyId = null,
				ContractId = null,
				ContractNumber = null,
				CurrencyId = null,
				EditDate = null,
				ElectronicSubmissionActionId = electronicSubmissionAction.Id,
				Expedite = false,
				ExternalJurisdictionId = integerJurisdcitionId,
				ExternalJurisdictionName = jurisdictionName,
				InternalJurisdictionId = internalIntegerJurisdcitionId,
				InternalJurisdictionName = sgiMapping.Jurisdiction.Name,
				JurisdictionalDataId = null,
				JurisdictionCode1 = null,
				JurisdictionCode2 = null,
				ProjectNumber = null,
				PublicId = null,
				ReceivedDate = null,
				RequestedDate = null,
				SupplierSpecific1 = jurisdictionalDataRequest.ProductLine,
				SupplierSpecific2 = jurisdictionalDataRequest.Platform,
				SupplierSpecific3 = null,
				SupplierSpecific4 = null,
				WorkOrderNumber = null,
				RevisionReasonId = jurisdictionalDataRequest.RevisionReasonId,
				MarketCode = string.IsNullOrEmpty(jurisdictionalDataRequest.MarketCode) ? null : jurisdictionalDataRequest.MarketCode
			};
			return newjurisdictionalDataRequest;
		}
		public override ElectronicSubmissionHeaderDTO BuildElectronicSubmissionHeaderDTO(List<IGrouping<string, FlatFileDTO>> requests, ElectronicSubmissionJurisdictionalRequestAction electronicSubmissionAction)
		{
			var now = DateTime.Now;

			var primaryJurisdictionInformation = requests.SelectMany(x => x)
				.Where(x => x.Status == ElectronicSubmissionJurisdictionalRequestAction.PND.ToString() &&
				x.Jurisdiction != ((int)CustomJurisdiction.AlbertaGaming).ToString() && x.JurisdictionNationCode == NationCode.US.ToString()
				)
				.Select(x => new
				{
					JurisdictionId = x.Jurisdiction,
					PartNumber = x.IdNumber
				})
				.FirstOrDefault();

			if (primaryJurisdictionInformation == null)
			{
				primaryJurisdictionInformation = requests.SelectMany(x => x)
				.Where(x => x.Status == ElectronicSubmissionJurisdictionalRequestAction.PND.ToString())
				.Select(x => new
				{
					JurisdictionId = x.Jurisdiction,
					PartNumber = x.IdNumber
				})
				.FirstOrDefault();
			}

			var header = new ElectronicSubmissionHeaderDTO()
			{
				Active = true,
				AddDate = now,
				CertificationLabId = null,
				ConfirmId = null,
				EditDate = null,
				ElectronicSubmissionSourceId = _dbSubmission.tbl_lst_ElectronicSubmissionSources.Where(x => x.Code == ElectronicSubmissionType.FLATFILE.ToString()).Single().Id,
				FileNumber = null,
				ManufacturerCode = requests.Max(x => x.First().ManufacturerBillingNote),
				Memo = null,
				NumberOfItems = requests.Count,
				PrimaryJurisdictionId = electronicSubmissionAction == ElectronicSubmissionJurisdictionalRequestAction.PND ? Int32.Parse(primaryJurisdictionInformation.JurisdictionId) : 0,
				PrimaryJurisdictionPartNumber = primaryJurisdictionInformation.PartNumber,
				ProcessedBy = null,
				ProcessedDate = null,
				PublicId = null,
				ReceivedDate = null,
				RequestSourceFileName = UploadedFileName,
				Status = SubmitRequestHeaderStatus.Pending.ToString(),
				SubmissionTypeId = null,
				SubmittedDate = null,
				SubmitterEmail = requests.Max(X => X.First().ContactEmail),
				SubmitterName = requests.Max(x => x.First().ContactName),
				SubmitterPhone = null,
				SupplierCode1 = null,
				SupplierCode2 = null,
				SupplierReferenceNumber = ReferenceNumber.ToString(),
				TestLabId = null,
				AddedById = _user.Id,
				ElectronicSubmissionActionId = _dbSubmission.tbl_lst_ElectronicSubmissionActions.Where(x => x.Code.ToLower() == electronicSubmissionAction.ToString().ToLower()).Single().Id
			};

			return header;
		}
		public override ElectronicSubmissionHeaderDTO BuildElectronicSubmissionObjectGraph(List<IGrouping<string, FlatFileDTO>> requests, ElectronicSubmissionJurisdictionalRequestAction elcetronicSubmissionAction)
		{
			var electronicSubmissionHeaderDTO = BuildElectronicSubmissionHeaderDTO(requests, elcetronicSubmissionAction);
			var fixedPriceId = _dbSubmission.tbl_lst_ContractType
				.Where(x => x.Code == ContractType.FPE.ToString())
				.Select(x => x.Id)
				.SingleOrDefault();
			var PrimaryJurIntToString = electronicSubmissionHeaderDTO.PrimaryJurisdictionId.ToString().Count() == 1 ? "0" + electronicSubmissionHeaderDTO.PrimaryJurisdictionId.ToString() : electronicSubmissionHeaderDTO.PrimaryJurisdictionId.ToString();
			var jurisdictionFunctionsMapping = _dbSubmission.JurisdictionFunctions.Select(x =>
				new
				{
					x.JurisdictionId,
					Function = x.Function.Code
				}).ToList();
			foreach (var component in requests)
			{
				var electronicSubmissionComponent = BuildElectronicSubmissionComponentDTO(component);

				if (!string.IsNullOrEmpty(component.First().ContactName))
				{
					var notes = new List<ElectronicSubmissionNoteDTO>();
					notes.Add(BuildElectonicSubmissionNoteDTO(
						DateTime.Now,
						"Contact Name: " + component.First().ContactName,
						"ContactName",
						false
					));
					electronicSubmissionComponent.ElectronicSubmissionNoteDTOs.AddRange(notes);
				}

				foreach (var juridictionalDataRequest in component)
				{
					if (!electronicSubmissionComponent.ElectronicSubmissionJurisdictionalDataDTOs.Any(x => x.InternalJurisdictionId.ToJurisdictionIdString() == juridictionalDataRequest.Jurisdiction))
					{
						var electronicSubmissionJurisdictionalData = BuildElectronicSubmissionJurisdictionalDataRequestDTO(juridictionalDataRequest);
						bool isPrimaryJurisdiction = juridictionalDataRequest.Jurisdiction == PrimaryJurIntToString && electronicSubmissionHeaderDTO.PrimaryJurisdictionPartNumber == juridictionalDataRequest.IdNumber;
						bool hasJurFunctionMapping = jurisdictionFunctionsMapping.Where(x => x.JurisdictionId == juridictionalDataRequest.Jurisdiction).Select(x => x.Function).Any(x => x.Equals(juridictionalDataRequest.Function, StringComparison.CurrentCultureIgnoreCase));
						SetAccountingDefaults(electronicSubmissionJurisdictionalData, juridictionalDataRequest, electronicSubmissionComponent.ManufacturerCode, fixedPriceId, hasJurFunctionMapping && !isPrimaryJurisdiction);
						if (isPrimaryJurisdiction)
						{
							electronicSubmissionComponent.ElectronicSubmissionJurisdictionalDataDTOs.Insert(0, electronicSubmissionJurisdictionalData);
						}
						else
						{
							electronicSubmissionComponent.ElectronicSubmissionJurisdictionalDataDTOs.Add(electronicSubmissionJurisdictionalData);

						}
						electronicSubmissionComponent.ElectronicSubmissionNoteDTOs.AddRange(ElectronicSubmissionJurisdictionalLevelNoteHandler(electronicSubmissionJurisdictionalData, juridictionalDataRequest));
					}
				}

				electronicSubmissionComponent.ElectronicSubmissionSignatureDTOs.AddRange(BuildElectronicSubmissionSignatureDTOs(component.First()));

				var componentNotes = ElectronicSubmissionNoteComponentLevelHandler(component);
				if (componentNotes.Count > 0)
					electronicSubmissionComponent.ElectronicSubmissionNoteDTOs.AddRange(componentNotes);

				electronicSubmissionHeaderDTO.ElectronicSubmissionComponents.Add(electronicSubmissionComponent);
			}

			ElectronicSubmissionNoteProjectLevelHandler(requests);
			electronicSubmissionHeaderDTO.ElectronicSubmissionComponents.First().ElectronicSubmissionNoteDTOs.AddRange(ProjectNotes);
			electronicSubmissionHeaderDTO.ElectronicSubmissionComponents
				.First()
				.ElectronicSubmissionDocumentDTOs
				.Add(BuildElectronicSubmissionDocumentDTO(_FlatfileUploadPath, ElectronicSubmissionType.FLATFILE.ToString(), electronicSubmissionHeaderDTO.RequestSourceFileName));

			return electronicSubmissionHeaderDTO;
		}
		public override string GenerateFlatFileName(string fileName)
		{
			return
				new StringBuilder()
				.Append(_user.Id)
				.Append("_")
				.Append(ReferenceNumber.ToString())
				.Append("_")
				.Append(fileName)
				.ToString();
		}
		public override int BuildTransferRequests()
		{
			var requests =
				FlatDataSet.Where(x => x.Status == ElectronicSubmissionJurisdictionalRequestAction.XFR.ToString()).GroupBy(x => x.IdNumber).ToList();

			if (requests.Count > 0)
			{
				var transferRequestHeader = BuildElectronicSubmissionObjectGraph(requests, ElectronicSubmissionJurisdictionalRequestAction.XFR);
				_electronicSubmissionService.Add(transferRequestHeader);
				return transferRequestHeader.Id;
			}
			return 0;
		}
		public override int BuildNewSubmissions()
		{
			ReferenceNumber = Guid.NewGuid();
			ProjectNotes.Clear();

			var newRecords = FlatDataSet.Where(x => x.Status == ElectronicSubmissionJurisdictionalRequestAction.PND.ToString() || x.Status == ElectronicSubmissionJurisdictionalRequestAction.OHR.ToString())
				.GroupBy(x => x.IdNumber).ToList();
			if (newRecords.Count > 0)
			{
				var newSubmissionHeader = BuildElectronicSubmissionObjectGraph(newRecords, ElectronicSubmissionJurisdictionalRequestAction.PND);
				_electronicSubmissionService.Add(newSubmissionHeader);
				return newSubmissionHeader.Id;
			}

			return 0;
		}
		#endregion

		#region Private Method
		private void SetAccountingDefaults(ElectronicSubmissionJurisdictionalDataDTO newJurisdictionalDataRequest, FlatFileDTO component, string manufCode, int fixedPriceId, bool hasJurFunctionMapping)
		{
			var forTransfer = component.Status == ElectronicSubmissionJurisdictionalRequestAction.XFR.ToString() ? true : false;
			//Convert values under 10 to two digit as this is how we represent Jur Ids in our database. Ex: 5 becomes 05
			var externalJurId = newJurisdictionalDataRequest.ExternalJurisdictionId?.ToString("00");

			var accountingDefaultsForJurisdiction =
				_dbSubmission.tbl_lst_JurisdictionMappings
				.Where(x => x.ExternalJurisdictionId == externalJurId && x.ManufacturerCode == "SGI")
				.SelectMany(y => y.AccountingDefaults.Where(z => z.ForTransfer == forTransfer))
				.FirstOrDefault();

			newJurisdictionalDataRequest.CompanyId = accountingDefaultsForJurisdiction.CompanyId;
			newJurisdictionalDataRequest.ContractId = accountingDefaultsForJurisdiction.ContractTypeId;
			newJurisdictionalDataRequest.CurrencyId = accountingDefaultsForJurisdiction.CurrencyId;
			newJurisdictionalDataRequest.LabId = accountingDefaultsForJurisdiction.LabId;
			newJurisdictionalDataRequest.CertLabId = accountingDefaultsForJurisdiction.CertificationLabId == null ? 1 : accountingDefaultsForJurisdiction.CertificationLabId;

			if (externalJurId == ((int)CustomJurisdiction.GreeceVlt).ToString() &&
				(manufCode == CustomManufacturer.BLY.ToString() || manufCode == CustomManufacturer.WMS.ToString()))
			{
				newJurisdictionalDataRequest.CompanyId = 1;
				newJurisdictionalDataRequest.ContractId = 1;
				newJurisdictionalDataRequest.CurrencyId = 1;
				newJurisdictionalDataRequest.LabId = (int)Laboratory.NJ;
				newJurisdictionalDataRequest.CertLabId = (int)Laboratory.NJ;
			}

			if (hasJurFunctionMapping)
			{
				newJurisdictionalDataRequest.ContractId = fixedPriceId;
			}

		}
		private List<ElectronicSubmissionNoteDTO> ElectronicSubmissionNoteComponentLevelHandler(IGrouping<string, FlatFileDTO> component)
		{
			var componentNotes = new List<ElectronicSubmissionNoteDTO>();

			var components = component.ToList();
			componentNotes = components
				.Where(x => !string.IsNullOrEmpty(x.WorksWith))
				.Select(x => new ElectronicSubmissionNoteDTO()
				{
					AddDate = DateTime.Now,
					Text = "Required Associated Components / Works With for part number " + x.IdNumber + ": " + x.WorksWith,
					Type = "AssociatedComponents",
					ForProject = true
				})
				.DistinctBy(x => x.Text)
				.ToList();

			if (components.Where(x => !String.IsNullOrEmpty(x.ProductLine)).Count() > 0)
			{
				var productlineMemo = "Productlines for: " + component.Select(x => x.IdNumber).FirstOrDefault() + "<br>";
				components.ForEach(x =>
				{
					productlineMemo = productlineMemo + x.JurisdictionDescription + " (" + x.Jurisdiction + ") : " + x.ProductLine + "<br>";
				});
				componentNotes.Add(BuildElectonicSubmissionNoteDTO(DateTime.Now, productlineMemo, "Productlines", false));
			}

			var firstComponent = components.FirstOrDefault();
			if (firstComponent != null && firstComponent.Function == Function.MathAnalysis.GetEnumDescription())
			{
				componentNotes.Add(BuildElectonicSubmissionNoteDTO(DateTime.Now, "Math Analysis for " + firstComponent.IdNumber, "MathIdNumber", false));
			}

			return componentNotes;
		}
		private List<ElectronicSubmissionNoteDTO> ElectronicSubmissionJurisdictionalLevelNoteHandler(ElectronicSubmissionJurisdictionalDataDTO jurisdictionalDataRequest, FlatFileDTO rawRequest)
		{
			var notes = new List<ElectronicSubmissionNoteDTO>();

			if (!string.IsNullOrWhiteSpace(rawRequest.SpecialApprovalNumbers))
			{
				notes.Add(BuildElectonicSubmissionNoteDTO(
					DateTime.Now,
					jurisdictionalDataRequest.InternalJurisdictionName + " (" + jurisdictionalDataRequest.InternalJurisdictionId + ") Special Approval Number: " + rawRequest.SpecialApprovalNumbers.Trim(),
					"SpecialApprovalNumbers",
					false
				));
			}

			if (rawRequest.Function == Function.MathAnalysis.GetEnumDescription() && !string.IsNullOrWhiteSpace(rawRequest.Platform))
			{
				notes.Add(BuildElectonicSubmissionNoteDTO(
					DateTime.Now,
					"Submission of Math files for " + rawRequest.GameName + " for  use on the " + rawRequest.Platform + ".",
					"MathNote",
					false
				));
			}

			return notes;
		}
		private void ElectronicSubmissionNoteProjectLevelHandler(List<IGrouping<string, FlatFileDTO>> requests)
		{
			CloneOrPreCertificationNoteHandler(requests);
			SRMFlexeraDMGNoteHandler(requests);
			AddNotes(requests);
		}
		private void SRMFlexeraDMGNoteHandler(List<IGrouping<string, FlatFileDTO>> requests)
		{
			var distinctSet = requests.SelectMany(x => x.Select(y => y.SRM_DMG_Flexera)).Distinct().ToList();
			foreach (var key in distinctSet)
			{
				if (!String.IsNullOrWhiteSpace(key))
				{
					ProjectNotes.Add(BuildElectonicSubmissionNoteDTO(
						DateTime.Now,
						"This submission uses: " + key.Trim(),
						"SRMFlexeraDMG",
						true
					));
				}
			}
		}
		private void CloneOrPreCertificationNoteHandler(List<IGrouping<string, FlatFileDTO>> requests)
		{
			requests.SelectMany(x => x.Select(y => y.CloneOrPreCertification)).Distinct().ToList().ForEach(x =>
			{
				if (x.ToLower() == "y" || x.ToLower() == "yes")
				{
					ProjectNotes.Add(BuildElectonicSubmissionNoteDTO(
					DateTime.Now,
					"This submission is a CloneOrPre-submitted math.",
					"Clone or PreCertification",
					true
				));
				}
				else
				{
					if (!String.IsNullOrEmpty(x) && x.ToLower() != "n" && x.ToLower() != "no")
					{
						ProjectNotes.Add(BuildElectonicSubmissionNoteDTO(
						DateTime.Now,
						"CloneOrPre of: " + x,
						"Clone or PreCertification",
						true
						));
					}
				}
			});
		}
		private void AddNotes(List<IGrouping<string, FlatFileDTO>> requests)
		{
			var distinctNotes = requests.SelectMany(x => x.Select(y => y.Notes)).Distinct().ToList();
			foreach (var key in distinctNotes)
			{
				if (!string.IsNullOrWhiteSpace(key))
				{
					ProjectNotes.Add(BuildElectonicSubmissionNoteDTO(
						DateTime.Now,
						"Note: " + key.Trim(),
						"GeneralNotes",
						true
					));
				}
			}
		}
		private void HandleDerivationRules(FlatFileDTO flatFileDTO, int rowNum)
		{
			rowNum = rowNum + 2;
			DeriveVendor(flatFileDTO, rowNum);
			DeriveRevisionReason(flatFileDTO, rowNum);
			ValidatePartName(flatFileDTO, rowNum);
			ValidateVersion(flatFileDTO, rowNum);
			ValidateFunction(flatFileDTO, rowNum);
			ValidateStatus(flatFileDTO, rowNum);
			ValidateJurisdiction(flatFileDTO, rowNum);
			ValidatePosition(flatFileDTO, rowNum);
			var billinginfo = ProcessBillingDerivations(flatFileDTO, rowNum);

			if (!string.IsNullOrEmpty(billinginfo.ManufacturerBillingNote))
			{
				ValidatePartNumber(flatFileDTO, rowNum, billinginfo.ManufacturerBillingNote);
				var manufGroupIds = _dbSubmission.tbl_lst_fileManuManufacturerGroups.Where(x => x.ManufacturerCode == billinginfo.ManufacturerBillingNote).Select(x => x.ManufacturerGroupId).ToList();
				ValidateProductLine(flatFileDTO, rowNum, manufGroupIds);
				ValidatePlatform(flatFileDTO, rowNum, manufGroupIds);
				ValidateStudio(flatFileDTO, rowNum, manufGroupIds);
			}
		}
		private FlatFileDTO ProcessBillingDerivations(FlatFileDTO flatFileDTO, int RowNum)
		{
			var contact = _billingDeriver.GetBillingInformationByProductLine(flatFileDTO.ProductLine.Replace(" ", "").Trim().ToLower());
			if (contact == null)
			{
				Errors.Add(new ColumnError()
				{
					RowNumber = RowNum,
					Column = "ProductLine",
					Value = flatFileDTO.ProductLine,
					ErrorCodeDescription = _errorMessage[ErrorCode.UNABLE_TO_DERIVE_BILLINGINFO]
				});
			}
			else
			{
				flatFileDTO.ManufacturerBillingNote = contact.Company;
				flatFileDTO.ManufacturerCode = contact.Company;
				flatFileDTO.ContactName = contact.FullName;
				flatFileDTO.ContactEmail = contact.Email;
			}
			return flatFileDTO;
		}

		private void ValidatePartName(FlatFileDTO flatFileDTO, int RowNum)
		{
			if (String.IsNullOrWhiteSpace(flatFileDTO.GameName.Trim()))
			{
				Errors.Add(new ColumnError()
				{
					RowNumber = RowNum,
					Column = "Partname",
					Value = flatFileDTO.GameName,
					ErrorCodeDescription = _errorMessage[ErrorCode.ISNULL_EMPTY_GAMENAME]
				});
			}
			else
			{
				if (flatFileDTO.GameName.Trim().Count() > 60)
				{
					Errors.Add(new ColumnError()
					{
						RowNumber = RowNum,
						Column = "Partname",
						Value = flatFileDTO.GameName,
						ErrorCodeDescription = _errorMessage[ErrorCode.PARTNAME_LENGTH]
					});
				}
			}
		}

		private void ValidatePartNumber(FlatFileDTO flatFileDTO, int RowNum, string manufCode)
		{
			if (String.IsNullOrEmpty(flatFileDTO.IdNumber.Trim()))
			{
				Errors.Add(new ColumnError()
				{
					RowNumber = RowNum,
					Column = "Partnumber",
					Value = flatFileDTO.IdNumber,
					ErrorCodeDescription = _errorMessage[ErrorCode.ISNULL_EMPTY_IDNUMBER]
				});
			}

			var query = _dbSubmission.JurisdictionalData.AsQueryable();
			var hasTPL = flatFileDTO.IdNumber.Contains("tpl", StringComparison.OrdinalIgnoreCase);
			var hasTPP = flatFileDTO.IdNumber.Contains("tpp", StringComparison.OrdinalIgnoreCase);

			if (hasTPL || hasTPP)
			{
				query = query.Where(x => x.Submission.IdNumber == flatFileDTO.IdNumber && x.JurisdictionId == flatFileDTO.Jurisdiction);
			}
			else
			{
				query = query.Where(x => x.Submission.IdNumber == flatFileDTO.IdNumber && x.Submission.ManufacturerCode == manufCode && x.Submission.Version == flatFileDTO.Version && x.JurisdictionId == flatFileDTO.Jurisdiction);
			}

			var jurisdictionStatus = query
				.Select(x => x.Status)
				.FirstOrDefault();

			if (jurisdictionStatus != null)
			{
				var tplTppLogic = (hasTPL || hasTPP) && flatFileDTO.Jurisdiction == ((int)CustomJurisdiction.WashingtonIndians).ToString();
				if (!tplTppLogic)
				{
					Errors.Add(new ColumnError()
					{
						RowNumber = RowNum,
						Column = "Partnumber",
						Value = flatFileDTO.IdNumber,
						ErrorCodeDescription = _errorMessage[ErrorCode.DUPLICATE_ID_MANUF_VERSION_SUBMISSION]
					});
				}
				else
				{
					flatFileDTO.JurisdictionStatus = jurisdictionStatus;
				}
			}

			var hasElectronicComponents = _dbSubmission.ElectronicSubmissionComponents
				.Where(x => x.ComponentIdentifier == flatFileDTO.IdNumber && x.ElectronicSubmissionHeader.Active && x.SubmissionRecordId == null)
				.Any();
			if (hasElectronicComponents)
			{
				Errors.Add(new ColumnError()
				{
					RowNumber = RowNum,
					Column = "Partnumber",
					Value = flatFileDTO.IdNumber,
					ErrorCodeDescription = _errorMessage[ErrorCode.DUPLICATE_ID_MANUF_VERSION_STAGING]
				});
			}
		}

		private void ValidatePlatform(FlatFileDTO flatFileDTO, int RowNum, List<int> manufIds)
		{
			if (!String.IsNullOrWhiteSpace(flatFileDTO.Platform.Trim()))
			{
				var platformNames = flatFileDTO.Platform.Split('|').Select(x => x.Trim()).ToList();

				var platforms = _dbSubmission.tbl_lst_Platforms.Where(x => platformNames.Contains(x.Name)).ToList();
				var manufGroups = platforms.Where(x => manufIds.Contains((int)x.ManufacturerGroupId)).ToList();
				if (manufGroups.Count == 0)
				{
					Errors.Add(new ColumnError()
					{
						RowNumber = RowNum,
						Column = "Platform",
						Value = flatFileDTO.Platform,
						ErrorCodeDescription = _errorMessage[ErrorCode.NOT_IN_DB_PLATFORM]
					});
				}
			}
		}

		private void ValidateJurisdiction(FlatFileDTO flatFileDTO, int RowNum)
		{
			if (String.IsNullOrWhiteSpace(flatFileDTO.Jurisdiction.Trim()))
			{
				Errors.Add(new ColumnError()
				{
					RowNumber = RowNum,
					Column = "Jurisdiction Number",
					Value = flatFileDTO.Jurisdiction,
					ErrorCodeDescription = _errorMessage[ErrorCode.ISNULL_EMPTY_JUR]
				});
			}
			else
			{
				flatFileDTO.Jurisdiction = flatFileDTO.Jurisdiction == "230" ? "72" :
				(flatFileDTO.Jurisdiction.Count() == 1 ? "0" + flatFileDTO.Jurisdiction : flatFileDTO.Jurisdiction);

				var internalJurMapping = _dbSubmission.tbl_lst_JurisdictionMappings
				.Where(x => x.ManufacturerCode.ToUpper() == "SGI" && x.ExternalJurisdictionId == flatFileDTO.Jurisdiction && x.FlatFile)
				.SingleOrDefault();
				if (internalJurMapping == null)
				{
					Errors.Add(new ColumnError()
					{
						RowNumber = RowNum,
						Column = "Jurisdiction Number",
						Value = flatFileDTO.Jurisdiction,
						ErrorCodeDescription = _errorMessage[ErrorCode.NOT_IN_DB_JUR]
					});
				}
				else
				{
					if (!internalJurMapping.Active)
					{
						Errors.Add(new ColumnError()
						{
							RowNumber = RowNum,
							Column = "Jurisdiction Number",
							Value = flatFileDTO.Jurisdiction,
							ErrorCodeDescription = _errorMessage[ErrorCode.INACTIVE_JUR]
						});
					}
					else
					{
						flatFileDTO.JurisdictionNationCode = internalJurMapping.Jurisdiction.NationCode;
					}
				}
			}
		}

		private void ValidateStudio(FlatFileDTO flatFileDTO, int RowNum, List<int> manufIds)
		{
			if (flatFileDTO.Studio != null && !String.IsNullOrWhiteSpace(flatFileDTO.Studio.Trim()))
			{
				var studios = _dbSubmission.tbl_lst_Studios.Where(x => x.Name == flatFileDTO.Studio).ToList();
				if (studios.Count == 0)
				{
					Errors.Add(new ColumnError()
					{
						RowNumber = RowNum,
						Column = "Studio",
						Value = flatFileDTO.Studio,
						ErrorCodeDescription = _errorMessage[ErrorCode.NOT_IN_DB_STUDIO]
					});
				}
			}
		}

		private void DeriveVendor(FlatFileDTO flatFileDTO, int RowNum)
		{
			var vendor = _vendorDeriver.GameNameVendors
				.Where(x => flatFileDTO.GameName.ToLower().Replace(" ", "").Trim().Contains(x.GameName))
				.Select(x => x.Vendor)
				.FirstOrDefault();
			if (vendor != null)
			{
				flatFileDTO.VendorId = flatFileDTO.GameName.Contains("gen") && flatFileDTO.Function != Function.Printer.GetEnumDescription() ? (int?)null : vendor.Id;
			}

			if ((flatFileDTO.Function == Function.Printer.GetEnumDescription() || flatFileDTO.Function == Function.BillValidator.GetEnumDescription()) && vendor == null)
			{
				Errors.Add(new ColumnError()
				{
					RowNumber = RowNum,
					Column = "Vendor",
					Value = "",
					ErrorCodeDescription = _errorMessage[ErrorCode.VENDOR_GAMENAME_MISSING]
				});
			}
		}

		private void DeriveRevisionReason(FlatFileDTO flatFileDTO, int RowNum)
		{
			if (!string.IsNullOrEmpty(flatFileDTO.RevisionReason))
			{
				if (_lookup.RevisionReasons.ContainsKey(flatFileDTO.RevisionReason))
				{
					var revisionReasonId = _lookup.RevisionReasons[flatFileDTO.RevisionReason];
					flatFileDTO.RevisionReasonId = revisionReasonId;
				}
				else
				{
					Errors.Add(new ColumnError()
					{
						RowNumber = RowNum,
						Column = "Revision Reason",
						Value = flatFileDTO.RevisionReason,
						ErrorCodeDescription = _errorMessage[ErrorCode.INVALID_REVISIONREASON]
					});
				}
			}
		}

		private void ValidateProductLine(FlatFileDTO flatFileDTO, int RowNum, List<int> manufIds)
		{
			if (String.IsNullOrWhiteSpace(flatFileDTO.ProductLine.Trim()))
			{
				Errors.Add(new ColumnError()
				{
					RowNumber = RowNum,
					Column = "ProductLine",
					Value = flatFileDTO.ProductLine,
					ErrorCodeDescription = _errorMessage[ErrorCode.UNABLE_TO_DERIVE_BILLINGINFO]
				});
			}
			else
			{
				var productlineTemp = flatFileDTO.ProductLine.Trim().ToLower().Replace(" ", "");
				flatFileDTO.ProductLine = productlineTemp == "classll" || productlineTemp == "classii" ? "class2" :
					(productlineTemp == "classlll" || productlineTemp == "classiii" ? "class3" : productlineTemp);
				var productline = _dbSubmission.tbl_lst_ProductLines.Where(x => x.Name.Trim().ToLower().Replace(" ", "") == flatFileDTO.ProductLine).ToList();
				if (productline.Count == 0)
				{
					Errors.Add(new ColumnError()
					{
						RowNumber = RowNum,
						Column = "ProductLine",
						Value = flatFileDTO.ProductLine,
						ErrorCodeDescription = _errorMessage[ErrorCode.NOT_IN_DB_PRODUCTLINE]
					});
				}
				var manufGroups = productline.Where(x => manufIds.Contains((int)x.ManufacturerGroupId)).ToList();
				if (manufGroups.Count == 0)
				{
					Errors.Add(new ColumnError()
					{
						RowNumber = RowNum,
						Column = "ProductLine",
						Value = flatFileDTO.ProductLine,
						ErrorCodeDescription = _errorMessage[ErrorCode.MANUF_GROUP_PRODUCTLINE]
					});
				}
			}
		}

		private void ValidateStatus(FlatFileDTO flatFileDTO, int RowNum)
		{
			if (String.IsNullOrWhiteSpace(flatFileDTO.Status.Trim()))
			{
				Errors.Add(new ColumnError()
				{
					RowNumber = RowNum,
					Column = "Status",
					Value = flatFileDTO.Status,
					ErrorCodeDescription = _errorMessage[ErrorCode.ISNULL_EMPTY_STATUS]
				});
			}
			else
			{
				if (flatFileDTO.Status.Trim() != ElectronicSubmissionJurisdictionalRequestAction.PND.ToString() &&
					flatFileDTO.Status.Trim() != ElectronicSubmissionJurisdictionalRequestAction.OHR.ToString())
				{
					Errors.Add(new ColumnError()
					{
						RowNumber = RowNum,
						Column = "Status",
						Value = flatFileDTO.Status,
						ErrorCodeDescription = _errorMessage[ErrorCode.PND_OHR_STATUS]
					});
				}

				if (flatFileDTO.Status.Trim() == "OH")
				{
					Errors.Add(new ColumnError()
					{
						RowNumber = RowNum,
						Column = "Status",
						Value = flatFileDTO.Status,
						ErrorCodeDescription = _errorMessage[ErrorCode.OH_STATUS]
					});
				}

				if (flatFileDTO.Status.Trim() == ElectronicSubmissionJurisdictionalRequestAction.OHR.ToString() && flatFileDTO.Function == Function.MathAnalysis.GetEnumDescription())
				{
					Errors.Add(new ColumnError()
					{
						RowNumber = RowNum,
						Column = "Status",
						Value = flatFileDTO.Status,
						ErrorCodeDescription = _errorMessage[ErrorCode.MATH_OH]
					});
				}
			}
		}

		private void ValidatePosition(FlatFileDTO flatFileDTO, int RowNum)
		{
			if (!String.IsNullOrWhiteSpace(flatFileDTO.Position.Trim()) && !flatFileDTO.Position.Replace(" ", "").Contains(ComGlobals.NA, StringComparison.OrdinalIgnoreCase))
			{
				var position = _lookup.Position(flatFileDTO.Position);
				if (!string.IsNullOrEmpty(position))
				{
					flatFileDTO.Position = position;
				}
				else
				{
					Errors.Add(new ColumnError()
					{
						RowNumber = RowNum,
						Column = "Position",
						Value = flatFileDTO.Position,
						ErrorCodeDescription = _errorMessage[ErrorCode.NOT_IN_DB_POSITION]
					});
				}
			}
		}

		private void ValidateVersion(FlatFileDTO flatFileDTO, int RowNum)
		{
			var excludedFunctions = new List<string>() {
				Function.Machine.ToString().ToLower(),
				Function.Hardware.ToString().ToLower(),
				Function.Printer.ToString().ToLower(),
				Function.BillValidator.ToString().ToLower(),
				Function.Other.ToString().ToLower()
			};

			if (String.IsNullOrWhiteSpace(flatFileDTO.Version.Trim()) && !excludedFunctions.Contains(flatFileDTO.Function.Replace(" ", "").ToLower().Trim()))
			{
				Errors.Add(new ColumnError()
				{
					RowNumber = RowNum,
					Column = "Version",
					Value = flatFileDTO.Version,
					ErrorCodeDescription = _errorMessage[ErrorCode.ISNULL_EMPTY_VERSION]
				});
			}
		}

		private void ValidateFunction(FlatFileDTO flatFileDTO, int RowNum)
		{
			if (flatFileDTO.Function.Contains(Function.Cabinet.ToString(), StringComparison.OrdinalIgnoreCase))
			{
				flatFileDTO.Function = Function.Machine.GetEnumDescription();
			}
			else if (flatFileDTO.GameName.Contains(Function.Firmware.ToString(), StringComparison.OrdinalIgnoreCase))
			{
				flatFileDTO.Function = Function.Other.GetEnumDescription();
			}
			else if (flatFileDTO.Function.Contains(Function.Security.ToString(), StringComparison.OrdinalIgnoreCase) || flatFileDTO.Function.Contains(Function.Bios.ToString(), StringComparison.OrdinalIgnoreCase))
			{
				flatFileDTO.Function = Function.SecurityConfig.GetEnumDescription();
			}

			if (String.IsNullOrEmpty(functionHolder) || (functionHolder != flatFileDTO.Function && !String.IsNullOrEmpty(flatFileDTO.Function)))
			{
				flatFileDTO.Function = flatFileDTO.Function;
				functionHolder = flatFileDTO.Function;
			}

			var function = _dbSubmission.tbl_lst_EPROMFunction.Where(x => x.Code == functionHolder).SingleOrDefault();
			if (function == null)
			{
				Errors.Add(new ColumnError()
				{
					RowNumber = RowNum,
					Column = "Function",
					Value = flatFileDTO.Function,
					ErrorCodeDescription = _errorMessage[ErrorCode.NOT_IN_DB_FUNCTION]
				});
			}
			else
			{
				if (!function.Active)
				{
					Errors.Add(new ColumnError()
					{
						RowNumber = RowNum,
						Column = "Function",
						Value = flatFileDTO.Function,
						ErrorCodeDescription = _errorMessage[ErrorCode.INACTIVE_FUNCTION]
					});
				}
				else
				{
					flatFileDTO.Function = functionHolder;
				}
			}
		}

		private void ValidateBillingNote(FlatFileDTO flatFileDTO, int RowNum)
		{
			if (String.IsNullOrWhiteSpace(flatFileDTO.ManufacturerBillingNote.Trim()))
			{
				Errors.Add(new ColumnError()
				{
					RowNumber = RowNum,
					Column = "GLI Billing Note",
					Value = flatFileDTO.ManufacturerBillingNote,
					ErrorCodeDescription = _errorMessage[ErrorCode.ISNULL_EMPTY_BILLINGNOTE]
				});
			}
		}
		#endregion
	}
	public class ColumnError
	{
		public int? RowNumber { get; set; }
		public string Column { get; set; }
		public string Value { get; set; }
		public string ErrorCodeDescription { get; set; }
		public string ErrorMessage
		{
			get
			{
				var tempRow = RowNumber < 0 ? "" : "Row: " + RowNumber.ToString() + " | ";
				return tempRow + "Column: " + Column + " | Value: " + Value + " | Error: " + ErrorCodeDescription;
			}
		}
	}
	class BillingDeriver
	{
		private readonly List<SCIContact> _SCIContacts = new List<SCIContact>();
		public BillingDeriver()
		{
			_SCIContacts.Add(new SCIContact("vlt", "Paul", "Warkentin", "BLY", "Paul.Warkentin@scientificgames.com"));
			_SCIContacts.Add(new SCIContact("cd", "Paul", "Warkentin", "BLY", "Paul.Warkentin@scientificgames.com"));
			_SCIContacts.Add(new SCIContact("classll", "Paul", "Warkentin", "BLY", "Paul.Warkentin@scientificgames.com"));
			_SCIContacts.Add(new SCIContact("classii", "Paul", "Warkentin", "BLY", "Paul.Warkentin@scientificgames.com"));
			_SCIContacts.Add(new SCIContact("class2", "Paul", "Warkentin", "BLY", "Paul.Warkentin@scientificgames.com"));
			_SCIContacts.Add(new SCIContact("hhr", "Paul", "Warkentin", "BLY", "Paul.Warkentin@scientificgames.com"));
			_SCIContacts.Add(new SCIContact("class3", "Michelle", "Rodriguez", "BAL", "Michelle.Rodriguez@scientificgames.com"));
			_SCIContacts.Add(new SCIContact("classlll", "Michelle", "Rodriguez", "BAL", "Michelle.Rodriguez@scientificgames.com"));
			_SCIContacts.Add(new SCIContact("classiii", "Michelle", "Rodriguez", "BAL", "Michelle.Rodriguez@scientificgames.com"));
			_SCIContacts.Add(new SCIContact("etg", "Brittan", "Zorn", "SHU", "Brittan.Zorn@scientificgames.com"));
			_SCIContacts.Add(new SCIContact("utl", "Danny", "Robinson", "SHU", "Danny.Robinson@scientificgames.com"));
		}

		public SCIContact GetBillingInformationByProductLine(string productLine) => _SCIContacts.Where(x => x.Productline == productLine).SingleOrDefault();
	}
	class VendorDeriver
	{
		public List<VendorVM> filterVendors = new List<VendorVM>();
		public List<GameNameVendor> GameNameVendors = new List<GameNameVendor>();
		private readonly SubmissionContext _dbSubmissions;
		private List<string> includedVendors = new List<string>() {
			Vendors.Mei.ToString().ToLower(),
			Vendors.Jcm.ToString().ToLower(),
			Vendors.Ithaca.ToString().ToLower(),
			Vendors.Transact.ToString().ToLower(),
			Vendors.FutureLogic.ToString().ToLower()
		};

		public VendorDeriver(SubmissionContext dbSubmission)
		{
			_dbSubmissions = dbSubmission;
			filterVendors = _dbSubmissions.tbl_lst_Vendors
				.Where(x => x.Active == true && includedVendors.Contains(x.Description.ToLower().Trim()))
				.Select(x => new VendorVM()
				{
					Id = x.Id,
					Description = x.Description.ToLower().Replace(" ", "").Trim()
				}).ToList();

			GameNameVendors.Add(new GameNameVendor() { GameName = "mei", Vendor = filterVendors.Where(x => x.Description == Vendors.Mei.ToString().ToLower()).FirstOrDefault() });
			GameNameVendors.Add(new GameNameVendor() { GameName = "jcm", Vendor = filterVendors.Where(x => x.Description == Vendors.Jcm.ToString().ToLower()).FirstOrDefault() });
			GameNameVendors.Add(new GameNameVendor() { GameName = "futurelogic", Vendor = filterVendors.Where(x => x.Description == Vendors.Ithaca.ToString().ToLower()).FirstOrDefault() });
			GameNameVendors.Add(new GameNameVendor() { GameName = "epicedge", Vendor = filterVendors.Where(x => x.Description == Vendors.Transact.ToString().ToLower()).FirstOrDefault() });
			GameNameVendors.Add(new GameNameVendor() { GameName = "gen", Vendor = filterVendors.Where(x => x.Description == Vendors.FutureLogic.ToString().ToLower()).FirstOrDefault() });
			GameNameVendors.Add(new GameNameVendor() { GameName = "ithaca", Vendor = filterVendors.Where(x => x.Description == Vendors.Ithaca.ToString().ToLower()).FirstOrDefault() });
		}
	}
	class VendorVM
	{
		public int Id { get; set; }
		public string Description { get; set; }
	}
	class GameNameVendor
	{
		public GameNameVendor()
		{
			this.Vendor = new VendorVM();
		}

		public string GameName { get; set; }
		public VendorVM Vendor { get; set; }
	}
	class SCIContact
	{
		public SCIContact(string productline, string firstName, string lastName, string company)
		{
			FirstName = firstName;
			LastName = lastName;
			Company = company;
			Productline = productline;
		}

		public SCIContact(string productline, string firstName, string lastName, string company, string email)
		{
			FirstName = firstName;
			LastName = lastName;
			Company = company;
			Email = email;
			Productline = productline;
		}

		public string Company { get; set; }
		public string Email { get; set; }
		public string FirstName { get; set; }
		public string FullName { get { return FirstName + ' ' + LastName; } }
		public string LastName { get; set; }
		public string Productline { get; set; }
	}
	public class Lookup
	{
		public Lookup(SubmissionContext dbSubmissions)
		{
			Positions = dbSubmissions.SCItbl_lst_Positions
				.Where(x => x.Active)
				.Select(x => x.Position)
				.ToList();
			RevisionReasons = dbSubmissions.tbl_lst_RevisionReasons
				.Where(x => x.Active)
				.Select(x => new { Name = x.Name.ToLower().Replace(" ", "").Trim(), x.Id })
				.ToDictionary(x => x.Name, x => x.Id);
		}

		public List<string> Positions { get; set; }
		public string Position(string position) => Positions.Where(x => x.Replace(" ", "").Equals(position.Replace(" ", ""), StringComparison.OrdinalIgnoreCase)).FirstOrDefault();
		public Dictionary<string, int> RevisionReasons { get; set; }
	}
}