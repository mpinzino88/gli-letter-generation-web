﻿using System;
using System.Collections.Generic;

namespace Submissions.Web.Areas.Submission.Models.Import
{
	public class FlatFileClientDTO : FlatFileDTO
	{
		public FlatFileClientDTO()
		{
			Children = new List<FlatFileClientDTO>();
			PotentialSubmissions = new List<FlatFileClientDTO>();
		}
		public int Id { get; set; }
		public bool Selected { get; set; }
		public bool AddToExistingSubmission { get; set; }
		public int? ContractTypeId { get; set; }
		public string ContractType { get; set; }
		public int? CertificationLabId { get; set; }
		public string CertificationLab { get; set; }
		public int? TestLabId { get; set; }
		public string TestLab { get; set; }
		public int? SubmissionTypeId { get; set; }
		public string SubmissionType { get; set; }
		public string PrimaryJurisdiction { get; set; }
		public int? CurrencyId { get; set; }
		public string Currency { get; set; }
		public int? CompanyId { get; set; }
		public string Company { get; set; }
		public int? UserId { get; set; }
		public string UserName { get; set; }
		public int? SubmissionRecordId { get; set; }
		public string WorkOrderNumber { get; set; }
		public int MatchExists { get; set; }
		public bool MissingBillingReference { get; set; }
		public bool OverRideDuplicateGuard { get; set; }
		public bool ChildrenVisible { get; set; }
		public string Mode { get; set; }
		public string Label { get; set; }
		public bool Html { get; set; }
		public int? MappedSubmissionId { get; set; }
		public int AmbiguousFollowupRequest { get; set; }
		public int SubmissionMatchCount { get; set; }
		public string SelectedStatus { get; set; }
		public string RequestedIdNumber { get; set; }
		public string RequestedGameName { get; set; }
		public List<FlatFileClientDTO> Children { get; set; }
		public List<FlatFileClientDTO> PotentialSubmissions { get; set; }
		public string LetterNumber { get; set; }
		public string ManufacturerShortCode { get; set; }
		public string SubmissionId { get; set; }
		public string SubmitterName { get; set; }
		public string SubmitterEmail { get; set; }
		public int? FunctionId { get; set; }
		public DateTime? ProcessedDate { get; set; }
		public int ElectronicSubmissionJurisdictionalDataId { get; set; }
	}

}