﻿using System;

namespace Submissions.Web.Areas.Submission.Models.Import
{
	public class FlatFileDTO
	{
		public string GameName { get; set; }
		public string IdNumber { get; set; }
		public string Jurisdiction { get; set; }
		public string JurisdictionNationCode { get; set; }
		public string JurisdictionDescription { get; set; }
		public string JurisdictionStatus { get; set; }
		public string Status { get; set; }
		public string ContactName { get; set; }
		public string ContactEmail { get; set; }
		public string ManufacturerCode { get; set; }
		public string Studio { get; set; }
		public string Platform { get; set; }
		public string VerifyPlusSHA1 { get; set; }
		public string VerifyPlusCDCK { get; set; }
		public string VerifyPlusMD5 { get; set; }
		public string Datecode { get; set; }
		public string Position { get; set; }
		public string CloneOrPreCertification { get; set; }
		public string SRM_DMG_Flexera { get; set; }
		public string SpecialApprovalNumbers { get; set; }
		public string Notes { get; set; }
		public string ManufacturerBillingNote { get; set; }
		public string AdditionalNotes { get; set; }
		public string WorksWith { get; set; }
		public string ProductLine { get; set; }
		public string SupplierReferenceNumber { get; set; }
		public DateTime? AddDate { get; set; }
		public DateTime? ReceivedDate { get; set; }
		public DateTime? RequestDate { get; set; }
		public DateTime? SubmitDate { get; set; }
		public DateTime? CloseDate { get; set; }
		public DateTime? ExpediteDate { get; set; }
		public string FileNumber { get; set; }
		public string SupplierJurisdictionalReferenceNumber { get; set; }
		public string Version { get; set; }
		public string OriginialIdNumber { get; set; }
		public string RJWDNote { get; set; }
		public string Expedite { get; set; }
		public string ProjectNumber { get; set; }
		public string Function { get; set; }
		public int? VendorId { get; set; }
		public string RevisionReason { get; set; }
		public int? RevisionReasonId { get; set; }
		public string MarketCode { get; set; }
	}
}