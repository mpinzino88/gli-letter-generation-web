﻿using System.Collections.Generic;
using System.Web.Mvc;

namespace Submissions.Web.Areas.Submission.Models.Import
{
	public class StageViewModel
	{
		public StageViewModel()
		{
			ManufacturerList = new List<SelectListItem>();
			WDRJReasonList = new List<SelectListItem>();
			SubmissionTemplates = new List<SubmisionTemplate>();
			StagedNewSubmissions = new List<StagedSubmission>();
			StagedNewSubmissionColumnOptionSet = new List<StagedSubmissionColumnOptions>();
			CompletedNewSubmissions = new List<StagedSubmission>();
			CompletedNewSubmissionColumnOptionSet = new List<StagedSubmissionColumnOptions>();
			StagedTransferRequests = new List<StagedFollowupRequest>();
			CompletedTransfers = new List<StagedFollowupRequest>();
			StagedWithdrawAndRejectRequests = new List<StagedFollowupRequest>();
			CompletedWithdrawAndRejectRequests = new List<StagedFollowupRequest>();
			RequestsRequiringReview = new List<FlatFileClientDTO>();
			CompletedSGISubmissions = new List<FlatFileClientDTO>();
			ManufacturerGroupTileDefinitions = new List<ManufacturerGroupTileDefinition>();
		}
		public List<SelectListItem> ManufacturerList { get; set; }
		public List<SelectListItem> WDRJReasonList { get; set; }
		/// <summary>
		/// Submission Templates
		/// </summary>
		public List<SubmisionTemplate> SubmissionTemplates { get; set; }
		/// <summary>
		/// Completed New Submissions
		/// </summary>
		public List<StagedSubmission> CompletedNewSubmissions { get; set; }
		public List<StagedSubmissionColumnOptions> CompletedNewSubmissionColumnOptionSet { get; set; }
		/// <summary>
		/// Staged New Submissions
		/// </summary>
		public List<StagedSubmission> StagedNewSubmissions { get; set; }
		public List<StagedSubmissionColumnOptions> StagedNewSubmissionColumnOptionSet { get; set; }
		/// <summary>
		/// Completed Transfers
		/// </summary>
		public List<StagedFollowupRequest> CompletedTransfers { get; set; }
		/// <summary>
		/// Staged Transfer Requests
		/// </summary>
		public List<StagedFollowupRequest> StagedTransferRequests { get; set; }
		/// <summary>
		/// Staged Withdrawal and Reject Requests
		/// </summary>
		public List<StagedFollowupRequest> StagedWithdrawAndRejectRequests { get; set; }
		/// <summary>
		/// Completed Withdrawal and Reject Requests
		/// </summary>
		public List<StagedFollowupRequest> CompletedWithdrawAndRejectRequests { get; set; }

		public List<FlatFileClientDTO> RequestsRequiringReview { get; set; }
		public List<FlatFileClientDTO> CompletedSGISubmissions { get; set; }
		public List<ManufacturerGroupTileDefinition> ManufacturerGroupTileDefinitions { get; set; }
	}
}