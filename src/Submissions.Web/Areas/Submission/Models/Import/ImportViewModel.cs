﻿using System.Collections.Generic;
using System.Web.Mvc;

namespace Submissions.Web.Areas.Submission.Models.Import
{
	public class ImportViewModel
	{
		public ImportViewModel()
		{
			ManufacturerList = new List<SelectListItem>();
		}
		public List<SelectListItem> ManufacturerList { get; set; }
	}
}