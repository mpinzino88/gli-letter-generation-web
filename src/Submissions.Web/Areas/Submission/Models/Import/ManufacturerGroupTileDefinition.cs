﻿using System.Collections.Generic;
using System.Web.Mvc;

namespace Submissions.Web.Areas.Submission.Models.Import
{
	public class ManufacturerGroupTileDefinition
	{
		public ManufacturerGroupTileDefinition()
		{
			Manufacturers = new List<SelectListItem>();
		}

		public int Id { get; set; }
		public string Title { get; set; }
		public string Color { get; set; }
		public string TextColor { get; set; }
		public bool RequiresManufacturerSelection { get; set; }
		public List<SelectListItem> Manufacturers { get; set; }
		public string SelectedManufacturer { get; set; }
	}
}