﻿using Submissions.Web.Models;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Submissions.Web.Areas.Submission.Models.CodeStorage
{
	public class IndexViewModel
	{
		public IndexViewModel()
		{
			this.CodeStorages = new List<CodeStorageViewModel>();
		}

		public int SubmissionId { get; set; }
		public SubmissionSubTitle SubmissionSubTitle { get; set; }
		public IList<CodeStorageViewModel> CodeStorages { get; set; }
	}

	public class CodeStorageViewModel
	{
		public int Id { get; set; }
		[Display(Name = "Location")]
		public string Lab { get; set; }
		[Display(Name = "Code Storage")]
		public int? CodeStorage { get; set; }
	}
}