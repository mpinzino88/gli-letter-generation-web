﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Submissions.Web.Areas.Submission.Models.AristocratData
{
	public class IndexViewModel
	{
		public IndexViewModel()
		{
			AristocratItems = new List<AristocratDataItem>();
		}
		public List<AristocratDataItem> AristocratItems { get; set; }
		public string FileNumber { get; set; }
		public int SubmissionHeaderId { get; set; }
	}

	public class AristocratDataItem
	{
		public AristocratDataItem()
		{
			SecondaryAristocratMarketIds = new List<string>();
			SecondaryAristocratMarkets = new List<SelectListItem>();
		}
		[Display(Name = "Jurisdiction")]
		public string Jurisdiction { get; set; }
		[Display(Name = "Jurisdiction Id")]
		public string JurisdictionId { get; set; }
		[Display(Name = "Product Type")]
		public string AristocratTypeId { get; set; }
		public string AristocratType { get; set; }
		[Display(Name = "Complexity")]
		public string AristocratComplexityId { get; set; }
		public string AristocratComplexity { get; set; }
		[Display(Name = "Primary Market")]
		public string AristocratMarketId { get; set; }
		public string AristocratMarket { get; set; }
		[Display(Name = "Secondary Markets")]
		public List<string> SecondaryAristocratMarketIds { get; set; }
		public List<SelectListItem> SecondaryAristocratMarkets { get; set; }

		public bool IsMainJurisdiction { get; set; }

	}
}