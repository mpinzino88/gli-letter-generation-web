﻿using Submissions.Common;
using System.ComponentModel.DataAnnotations;

namespace Submissions.Web.Areas.Submission.Models.Shipment
{
	public class EditViewModel
	{
		public Mode Mode { get; set; }
		public PageSource? Source { get; set; }
		public string CancelButtonUrl { get; set; }
		public int? Id { get; set; }
		public int SubmissionId { get; set; }
		public string Type { get; set; }
		[Display(Name = "From/To"), Required]
		public int? ShipmentRecipientId { get; set; }
		public string ShipmentRecipient { get; set; }
		[Display(Name = "Ship/Receive Date"), Required]
		public string ProcessDate { get; set; }
		[Display(Name = "Courier"), Required]
		public int? CourierId { get; set; }
		public string Courier { get; set; }
		[Display(Name = "Tracking Number"), Required, StringLength(50)]
		public string TrackingNumber { get; set; }
		[Display(Name = "Chips")]
		public int ChipCount { get; set; }
		[Display(Name = "Disks")]
		public int DiskCount { get; set; }
		[StringLength(100)]
		public string Memo { get; set; }
		[Display(Name = "Software Check")]
		public string SoftwareCheckUserId { get; set; }
		public string SoftwareCheckUser { get; set; }
	}
}