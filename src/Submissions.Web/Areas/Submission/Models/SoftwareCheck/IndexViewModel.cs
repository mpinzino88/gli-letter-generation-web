﻿using Submissions.Web.Models.Grids.Submission;
using Submissions.Web.Models.Query.Submission;
using System.ComponentModel;

namespace Submissions.Web.Areas.Submission.Models.SoftwareCheck
{
	public class IndexViewModel
	{
		public IndexViewModel()
		{
			this.SoftwareCheckGrid = new SoftwareCheckGrid();
			this.Filters = new SoftwareCheckSearch();
		}

		public SoftwareCheckGrid SoftwareCheckGrid { get; set; }
		public SoftwareCheckSearch Filters { get; set; }
	}

	public enum EditAction
	{
		Accept,
		Unaccept,
		Complete,
		[Description("In Engineering")]
		InEngYes,
		[Description("Returned from Engineering")]
		InEngNo
	}
}