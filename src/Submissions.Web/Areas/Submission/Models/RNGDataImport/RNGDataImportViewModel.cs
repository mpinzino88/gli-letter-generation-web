﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Submissions.Web.Areas.Submission.Models.RNGDataImport
{
	public class RNGDataImportViewModel
	{
		public RNGDataImportViewModel()
		{
			this.RNGDataSet = new RNGDataSet();
		}

		public PageSource? Source { get; set; }
		public string CancelButtonUrl { get; set; }
		public int SubmissionHeaderId { get; set; }
		public int SubmissionId { get; set; }
		public RNGDataSet RNGDataSet { get; set; }
	}

	#region RNG Data Set
	public class RNGDataSet
	{
		public RNGDataSet()
		{
			this.RNGData = new List<RNGData>();
		}
		public string FileNumber { get; set; }
		public int SubmissionHeaderId { get; set; }
		public List<RNGData> RNGData { get; set; }
	}

	public class ConfidenceLevel
	{
		public ConfidenceLevel()
		{
			this.Level = "";
		}
		public ConfidenceLevel(string Level)
		{
			this.Level = Level;
		}
		public string Level { get; set; }
	}

	public class RNGData
	{
		private string _RangeStart;
		private string _RangeEnd;
		private string _Draws;
		private string _Selections;

		[DisplayName("Test Case")]
		[Required(ErrorMessage = "Test Case is required")]
		public string TestCase { get; set; }

		public int? SubmissionHeaderId { get; set; }

		[DisplayName("Range Start")]
		[Required(ErrorMessage = "Range Start is required")]
		public string RangeStart
		{
			get
			{
				return _RangeStart;
			}
			set
			{
				_RangeStart = value.ToString();
			}
		}
		[DisplayName("Range End")]
		[Required(ErrorMessage = "Range End is required")]
		public string RangeEnd
		{
			get
			{
				return _RangeEnd;
			}
			set
			{
				_RangeEnd = value.ToString();
			}
		}
		[DisplayName("Draws")]
		[Required(ErrorMessage = "Draws is required")]
		public string Draws
		{
			get
			{
				return _Draws;
			}
			set
			{
				_Draws = value.ToString();
			}
		}
		[DisplayName("Replacement")]
		public bool Replacement { get; set; }

		[DisplayName("Selections")]
		[Required(ErrorMessage = "Selections is required")]
		public string Selections
		{
			get
			{
				return _Selections;
			}
			set
			{
				_Selections = value.ToString();
			}
		}
		[DisplayName("Note")]
		public string Note { get; set; }
		public int? Id { get; set; }
		public List<ConfidenceLevel> ConfidenceLevels { get; set; }
	}
	#endregion
}