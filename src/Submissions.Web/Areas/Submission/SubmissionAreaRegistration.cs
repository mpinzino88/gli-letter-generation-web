﻿using System.Web.Mvc;

namespace Submissions.Web.Areas.Submission
{
    public class SubmissionAreaRegistration : AreaRegistration 
    {
        public override string AreaName 
        {
            get 
            {
                return "Submission";
            }
        }

        public override void RegisterArea(AreaRegistrationContext context) 
        {
            context.MapRoute(
                "Submission_default",
                "Submission/{controller}/{action}/{id}",
                new { controller = "Submission", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}