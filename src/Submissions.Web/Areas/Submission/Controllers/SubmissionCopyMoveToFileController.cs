﻿extern alias ZEFCore;
using AutoMapper.QueryableExtensions;
using GLI.EFCore.Submission;
using Microsoft.EntityFrameworkCore;
using Submissions.Common;
using Submissions.Core;
using Submissions.Core.Models.DTO;
using Submissions.Web.Areas.Submission.Models.SubmissionCopyMoveToFile;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using ZEFCore::Z.EntityFramework.Plus;

namespace Submissions.Web.Areas.Submission.Controllers
{
	public class SubmissionCopyMoveToFileController : Controller
	{
		private readonly IJurisdictionalDataService _jurisdictionalDataService;
		private readonly IMemoService _memoService;
		private readonly IProjectService _projectService;
		private readonly SubmissionContext _dbSubmission;
		private readonly ISubmissionService _submissionService;
		private readonly IUserContext _userContext;

		public SubmissionCopyMoveToFileController
		(
			SubmissionContext dbSubmission,
			IUserContext userContext,
			IProjectService projectService,
			IMemoService memoService,
			ISubmissionService submissionService,
			IJurisdictionalDataService jurisdictionalDataService
		)
		{
			_dbSubmission = dbSubmission;
			_jurisdictionalDataService = jurisdictionalDataService;
			_projectService = projectService;
			_submissionService = submissionService;
			_userContext = userContext;
			_memoService = memoService;
		}

		public ActionResult Index(int submissionId, string fileNumber)
		{
			var vm = new SubmissionCopyMoveToFile();
			vm.SubmissionId = submissionId;

			vm.Submissions = _dbSubmission.Submissions
				.AsNoTracking()
				.Where(x => x.FileNumber == fileNumber)
				.ProjectTo<Models.Submission.SubmissionSelection>()
				.ToList();

			var currentSubmissionInfo = _dbSubmission.Submissions.Where(x => x.FileNumber == fileNumber).OrderBy(x => x.AddDate).Select(
											x => new SubmissionFileInfo()
											{
												CertificationLabId = _dbSubmission.trLaboratories.Where(y => y.Location == x.CertificationLab).Select(y => y.Id).FirstOrDefault(),
												CertificationLab = _dbSubmission.trLaboratories.Where(y => y.Location == x.CertificationLab).Select(y => y.Location + " -" + y.Name).FirstOrDefault(),
												TestLabId = _dbSubmission.trLaboratories.Where(y => y.Location == x.Lab).Select(y => y.Id).FirstOrDefault(),
												TestLab = _dbSubmission.trLaboratories.Where(y => y.Location == x.Lab).Select(y => y.Location + " -" + y.Name).FirstOrDefault(),
												Company = x.Company.Name,
												CompanyId = x.CompanyId,
												ContractType = x.ContractType.Code + " - " + x.ContractType.Description,
												ContractTypeId = x.ContractTypeId,
												Currency = x.Currency.Code + " - " + x.Currency.Description,
												CurrencyId = x.CurrencyId,
												JurisdictionId = x.JurisdictionId,
												ManufacturerCode = x.ManufacturerCode,
												ProjectDetail = x.ProjectDetail,
												PurchaseOrder = x.PurchaseOrder,
												ReceiveDate = x.ReceiveDate,
												SubmitDate = x.SubmitDate,
												Sequence = x.Sequence,
												Type = x.Type,
												Year = x.Year
											}
											).FirstOrDefault();

			vm.SelFileInfo = currentSubmissionInfo;
			if (currentSubmissionInfo != null)
			{
				vm.NewFileInfo.Company = vm.SelFileInfo.Company;
				vm.NewFileInfo.CompanyId = vm.SelFileInfo.CompanyId;
				vm.NewFileInfo.CertificationLab = vm.SelFileInfo.CertificationLab;
				vm.NewFileInfo.CertificationLabId = vm.SelFileInfo.CertificationLabId;
				vm.NewFileInfo.CertificationLab = vm.SelFileInfo.CertificationLab;
				vm.NewFileInfo.TestLabId = vm.SelFileInfo.TestLabId;
				vm.NewFileInfo.TestLab = vm.SelFileInfo.TestLab;
				vm.NewFileInfo.CurrencyId = vm.SelFileInfo.CurrencyId;
				vm.NewFileInfo.Currency = vm.SelFileInfo.Currency;
				vm.NewFileInfo.ProjectDetail = vm.SelFileInfo.ProjectDetail;
			}
			vm.NewFileInfo.Year = DateTime.Now.Year.ToString().Substring(2);
			vm.FileNumber = fileNumber;
			vm.Manufacturer = vm.SelFileInfo.ManufacturerCode;
			vm.IncludeJurisdictions = false;
			vm.CopyMoveOption = CopyMoveOption.Copy.ToString();
			vm.DefaultCompanyCurrency = _dbSubmission.tbl_lst_Company.Where(x => x.Active == true).Select(
											x => new CompanyCurreny()
											{
												CompanyId = x.Id,
												Company = x.Name,
												CurrencyId = x.CurrencyId,
												CurrencyCode = x.Currency.Code,
												Currency = x.Currency.Description
											}).ToList();
			vm.CanAdd = Util.HasAccess(Permission.Submission, AccessRight.Add);

			return View("Index", vm);
		}

		public JsonResult Save(SubmissionCopyMoveToFile vm)
		{
			var currenFileNumber = vm.FileNumber;

			//selected submissions
			var selectedSubmissions = vm.Submissions.Where(x => x.Selected == true).Select(x => x.Id).ToList();

			//get all new file info
			var newCopyMoveOverWriteDTO = new CopyMoveSubmissionOverWriteDTO();
			newCopyMoveOverWriteDTO.SubmissionType = _dbSubmission.tbl_lst_SubmissionType.Where(x => x.Id == vm.NewFileInfo.TypeId).Select(x => x.Code).SingleOrDefault();
			newCopyMoveOverWriteDTO.Jurisdiction = vm.NewFileInfo.JurisdictionId;
			newCopyMoveOverWriteDTO.ManufacturerCode = vm.NewFileInfo.ManufacturerCode;
			newCopyMoveOverWriteDTO.Sequence = vm.NewFileInfo.Sequence;
			newCopyMoveOverWriteDTO.PurchaseOrder = vm.NewFileInfo.PurchaseOrder;
			newCopyMoveOverWriteDTO.SubmitDate = vm.NewFileInfo.SubmitDate;
			newCopyMoveOverWriteDTO.RecieveDate = vm.NewFileInfo.ReceiveDate;
			newCopyMoveOverWriteDTO.ContractTypeId = vm.NewFileInfo.ContractTypeId;
			newCopyMoveOverWriteDTO.ContractValue = vm.NewFileInfo.ContractValue;
			newCopyMoveOverWriteDTO.CompanyId = vm.NewFileInfo.CompanyId;
			newCopyMoveOverWriteDTO.CurrencyId = vm.NewFileInfo.CurrencyId;
			newCopyMoveOverWriteDTO.ProjectDetail = vm.NewFileInfo.ProjectDetail;
			newCopyMoveOverWriteDTO.Year = vm.NewFileInfo.Year;
			newCopyMoveOverWriteDTO.CertificationLabId = vm.NewFileInfo.CertificationLabId;
			newCopyMoveOverWriteDTO.CertificationLab = _dbSubmission.trLaboratories.Where(x => x.Id == vm.NewFileInfo.CertificationLabId).Select(x => x.Location).FirstOrDefault();
			newCopyMoveOverWriteDTO.TestLabId = vm.NewFileInfo.TestLabId;
			newCopyMoveOverWriteDTO.TestLab = _dbSubmission.trLaboratories.Where(x => x.Id == vm.NewFileInfo.TestLabId).Select(x => x.Location).FirstOrDefault();
			newCopyMoveOverWriteDTO.Status = vm.NewFileInfo.Status;
			newCopyMoveOverWriteDTO.PastDateReason = vm.PastDateReason;
			//update work order number based on options later
			newCopyMoveOverWriteDTO.WorkOrderNumber = vm.WorkOrderNumber;
			newCopyMoveOverWriteDTO.GameName = vm.NewFileInfo.GameName;

			//see testing performed setting for new jur
			if (vm.CopyMoveOption == CopyMoveOption.Copy.ToString())
			{
				var testingJurs = ComUtil.TestingPerformedJurisdictions().Select(i => (int)i).ToList();
				if (!testingJurs.Contains(Convert.ToInt32(newCopyMoveOverWriteDTO.Jurisdiction)))
				{
					newCopyMoveOverWriteDTO.TestingPerformedId = (int)TestingPerformed.FullTesting;
				}
			}

			if (!string.IsNullOrEmpty(vm.NewFileInfo.Sequence)) //existing file
			{
				//find an existing file
				if (Convert.ToInt32(newCopyMoveOverWriteDTO.Sequence) < 10)
				{
					newCopyMoveOverWriteDTO.Sequence = newCopyMoveOverWriteDTO.Sequence.ToString().PadLeft(2, '0');
				}
				var selFileNumber = new FileNumber(newCopyMoveOverWriteDTO.SubmissionType, newCopyMoveOverWriteDTO.Jurisdiction, newCopyMoveOverWriteDTO.ManufacturerCode, newCopyMoveOverWriteDTO.Year, newCopyMoveOverWriteDTO.Sequence).ToString();
				var fileInfo = _dbSubmission.Submissions.Where(x => x.FileNumber == selFileNumber).Select(x => new { MasterFileNumber = x.MasterFileNumber, ArchiveLoc = x.ArchiveLocation, CompanyId = x.CompanyId, CurrencyId = x.CurrencyId, ContractTypeId = x.ContractTypeId }).FirstOrDefault();
				if (fileInfo == null)
				{
					return Json("NoFile");
				}
				else
				{
					if (fileInfo.CompanyId != newCopyMoveOverWriteDTO.CompanyId || fileInfo.CurrencyId != newCopyMoveOverWriteDTO.CurrencyId || fileInfo.ContractTypeId != newCopyMoveOverWriteDTO.ContractTypeId)
					{
						return Json("BillingMismatch");
					}
					else
					{
						newCopyMoveOverWriteDTO.ArchiveLocation = fileInfo.ArchiveLoc;
						newCopyMoveOverWriteDTO.FileNumber = selFileNumber;
					}
				}
			}
			else //new file
			{
				var fileInfo = _dbSubmission.Submissions.Where(x => x.FileNumber == currenFileNumber).Select(x => new { MasterFileNumber = x.MasterFileNumber, ArchiveLoc = x.ArchiveLocation }).FirstOrDefault();
				if (fileInfo != null)
				{
					newCopyMoveOverWriteDTO.ArchiveLocation = fileInfo.ArchiveLoc;
				}
			}
			//end set of new info

			//Add new file, copy/move shipping and set Projectid
			if ((string.IsNullOrEmpty(newCopyMoveOverWriteDTO.Sequence)))
			{
				//Add file and set new filenumber
				AddNewFileNumber(ref newCopyMoveOverWriteDTO);

				var projectId = _dbSubmission.trProjects.Where(x => x.ProjectName == newCopyMoveOverWriteDTO.FileNumber && x.BundleTypeId == 0).Select(x => x.Id).SingleOrDefault();
				newCopyMoveOverWriteDTO.AddJurisdictionsProjectId = projectId;

			}
			else
			{
				//set DTO obj to be used later on
				var projectId = _dbSubmission.trProjects.Where(x => x.ProjectName == newCopyMoveOverWriteDTO.FileNumber && x.BundleTypeId == 0).Select(x => x.Id).SingleOrDefault();
				newCopyMoveOverWriteDTO.AddJurisdictionsProjectId = projectId;

				//get regression Testing info
				var regressionTesting = _dbSubmission.Submissions.Where(x => x.FileNumber == newCopyMoveOverWriteDTO.FileNumber).Select(x => x.IsRegressionTesting).FirstOrDefault();
				newCopyMoveOverWriteDTO.IsRegressionTesting = regressionTesting;
			}


			for (int submissionCounter = 0; submissionCounter < selectedSubmissions.Count; submissionCounter++)
			{
				var submissionId = selectedSubmissions[submissionCounter];
				var newSubmissionId = 0;

				if (vm.CopyMoveOption == CopyMoveOption.Copy.ToString())//Copy
				{
					newSubmissionId = CopyData(submissionId, newCopyMoveOverWriteDTO, vm.IncludeJurisdictions, vm.IncludeSignatures, vm.IncludeMemo);
				}
				else //Move
				{
					newSubmissionId = MoveData(submissionId, newCopyMoveOverWriteDTO, vm.IncludeJurisdictions, vm.IncludeSignatures, vm.IncludeMemo);
				}

				//Update WON
				UpdateWONForOption(submissionId, newSubmissionId, vm.UseSelFileWon, vm.UseSelFileAsWon, vm.UseNewFileWon, vm.UseNewFileAsWon);
			}

			//shipping
			if (vm.IncludeShipping && vm.CopyMoveOption == CopyMoveOption.Copy.ToString())
			{
				CopyMoveShipping(currenFileNumber, newCopyMoveOverWriteDTO.FileNumber, true);
			}
			else if (vm.CopyMoveOption == CopyMoveOption.Move.ToString())
			{
				CopyMoveShipping(currenFileNumber, newCopyMoveOverWriteDTO.FileNumber, true);
			}

			//Update TargetDate
			if (newCopyMoveOverWriteDTO.AddJurisdictionsProjectId > 0)
			{
				var query = "exec sp_UpdateProjectTargetFromJurisdictions " + newCopyMoveOverWriteDTO.AddJurisdictionsProjectId + "," + _userContext.User.Id;
				_dbSubmission.Database.ExecuteSqlRaw(query);
			}

			if (CustomManufacturerGroups.Aristocrat.Contains(newCopyMoveOverWriteDTO.ManufacturerCode))
			{
				UpdateAristocratData(newCopyMoveOverWriteDTO.FileNumber);
			}

			return Json(new { Status = "OK", FileNumber = newCopyMoveOverWriteDTO.FileNumber });
		}


		#region Copy Data
		private int CopyData(int submissionId, CopyMoveSubmissionOverWriteDTO newSubmissionOverWriteInfo, bool copyJurisdictions, bool copySignatures, bool copyMemo)
		{
			var newSubmissionId = CopyMoveAndUpdateSubmissions(submissionId, newSubmissionOverWriteInfo, true);

			if (newSubmissionId != 0)
			{
				if (copySignatures)
				{
					CopyMoveSignatures(submissionId, newSubmissionId, true);
				}

				if (copyMemo)
				{
					CopyMoveMemo(submissionId, newSubmissionId, true);
				}

				//Add Past date Reason
				if (!string.IsNullOrEmpty(newSubmissionOverWriteInfo.PastDateReason))
				{
					_memoService.Add(new tblMemo()
					{
						SubmissionId = newSubmissionId,
						Memo = newSubmissionOverWriteInfo.PastDateReason,
						Username = _userContext.User.Username,
						AddDate = DateTime.Now
					});
				}

				CopyMoveAndUpdateJurisdictions(submissionId, newSubmissionId, newSubmissionOverWriteInfo, CopyMoveOption.Copy, copyJurisdictions);

				//update TestingPerformed 
				var testingPerformed = _dbSubmission.JurisdictionalData.Where(x => x.SubmissionId == newSubmissionId).Min(x => x.TestingPerformedId);
				_dbSubmission.Submissions.Where(x => x.Id == newSubmissionId).Update(x => new GLI.EFCore.Submission.Submission { TestingPerformedId = testingPerformed });
			}
			return newSubmissionId;

		}
		#endregion

		#region Move Data
		private int MoveData(int submissionId, CopyMoveSubmissionOverWriteDTO newSubmissionOverWriteInfo, bool copyJurisdictions, bool copySignatures, bool copyMemo)
		{
			//newSubmissionId and SubmissionId will be same
			var newSubmissionId = CopyMoveAndUpdateSubmissions(submissionId, newSubmissionOverWriteInfo, false);

			if (newSubmissionId != 0)
			{
				//Add Past date Reason
				if (!string.IsNullOrEmpty(newSubmissionOverWriteInfo.PastDateReason))
				{
					_memoService.Add(new tblMemo()
					{
						SubmissionId = newSubmissionId,
						Memo = newSubmissionOverWriteInfo.PastDateReason,
						Username = _userContext.User.Username,
						AddDate = DateTime.Now
					});
				}

				//memo and signatures will be copied over with same submissionId

				//so new info is updated like company, currency etc.
				CopyMoveAndUpdateJurisdictions(submissionId, newSubmissionId, newSubmissionOverWriteInfo, CopyMoveOption.Move, copyJurisdictions);

			}
			return newSubmissionId;

		}
		#endregion

		#region Common Updates for copy/Move

		private void AddNewFileNumber(ref CopyMoveSubmissionOverWriteDTO newCopyMoveOverWriteDTO)
		{
			//Add Header
			var newSubmissionHeader = new SubmissionHeaderDTO();
			newSubmissionHeader.Type = newCopyMoveOverWriteDTO.SubmissionType;
			newSubmissionHeader.ManufacturerCode = newCopyMoveOverWriteDTO.ManufacturerCode;
			newSubmissionHeader.JurisdictionId = newCopyMoveOverWriteDTO.Jurisdiction;
			newSubmissionHeader.Year = newCopyMoveOverWriteDTO.Year;
			newSubmissionHeader.Source = SubmissionSource.CopyMove;
			var addedSubmissionHeader = _submissionService.AddSubmissionHeader(newSubmissionHeader);

			//set DTO obj to be used later on
			newCopyMoveOverWriteDTO.Sequence = addedSubmissionHeader.Sequence;
			newCopyMoveOverWriteDTO.FileNumber = addedSubmissionHeader.FileNumber;

			//Create a Project
			var newFileNumber = newCopyMoveOverWriteDTO.FileNumber;
			var project = new ProjectDTO
			{
				AddDate = DateTime.Now,
				BundleTypeId = 0,
				CertificationLab = newCopyMoveOverWriteDTO.CertificationLab,
				ENStatus = ProjectStatus.UU.ToString(),
				FileNumber = newFileNumber,
				LabId = newCopyMoveOverWriteDTO.TestLabId,
				QAOfficeId = newCopyMoveOverWriteDTO.TestLabId,
				Lab = newCopyMoveOverWriteDTO.TestLab,
				Holder = ProjectHolder.EN.ToString(),
				IsTransfer = false,
				OnHold = 0,
				ReceiveDate = newCopyMoveOverWriteDTO.RecieveDate
			};

			_projectService.AddProject(project);

			//********************

		}
		private int CopyMoveAndUpdateSubmissions(int submissionId, CopyMoveSubmissionOverWriteDTO newCopyMoveSubmissionOverWrite, bool copy = false)
		{
			var updateSubmissionId = 0;

			var submissionUpdate = _dbSubmission.Submissions.Where(x => x.Id == submissionId).SingleOrDefault();

			if (copy)
			{
				var oldTestLab = submissionUpdate.Lab;
				//if Test labs are different get new Archivelocation
				if (oldTestLab != newCopyMoveSubmissionOverWrite.TestLab)
				{
					var newArchiveLocation = _dbSubmission.trLaboratories.Where(x => x.Location == newCopyMoveSubmissionOverWrite.TestLab).Select(x => x.ArchiveLocation).SingleOrDefault();
					newCopyMoveSubmissionOverWrite.ArchiveLocation = newArchiveLocation;
				}

				var newSubmissionToAdd = new GLI.EFCore.Submission.Submission();

				//overwrite from selection
				newSubmissionToAdd.FileNumber = newCopyMoveSubmissionOverWrite.FileNumber;
				newSubmissionToAdd.SubmissionHeaderId = _dbSubmission.SubmissionHeaders.Where(x => x.FileNumber == newCopyMoveSubmissionOverWrite.FileNumber).Select(x => x.Id).SingleOrDefault();
				newSubmissionToAdd.Type = newCopyMoveSubmissionOverWrite.SubmissionType;
				newSubmissionToAdd.JurisdictionId = newCopyMoveSubmissionOverWrite.Jurisdiction;
				newSubmissionToAdd.ManufacturerCode = newCopyMoveSubmissionOverWrite.ManufacturerCode;
				newSubmissionToAdd.Year = newCopyMoveSubmissionOverWrite.Year;
				newSubmissionToAdd.Sequence = newCopyMoveSubmissionOverWrite.Sequence;
				newSubmissionToAdd.ManufacturerDescription = _dbSubmission.tbl_lst_fileManu.Where(x => x.Code == newCopyMoveSubmissionOverWrite.ManufacturerCode).Select(x => x.Description).SingleOrDefault();

				newSubmissionToAdd.CompanyId = newCopyMoveSubmissionOverWrite.CompanyId;
				newSubmissionToAdd.ContractTypeId = newCopyMoveSubmissionOverWrite.ContractTypeId;
				newSubmissionToAdd.CurrencyId = newCopyMoveSubmissionOverWrite.CurrencyId;

				newSubmissionToAdd.SubmitDate = newCopyMoveSubmissionOverWrite.SubmitDate;
				newSubmissionToAdd.ReceiveDate = newCopyMoveSubmissionOverWrite.RecieveDate;

				newSubmissionToAdd.TestingPerformedId = newCopyMoveSubmissionOverWrite.TestingPerformedId;
				newSubmissionToAdd.CertificationLab = newCopyMoveSubmissionOverWrite.CertificationLab;
				newSubmissionToAdd.Lab = newCopyMoveSubmissionOverWrite.TestLab;

				if (!string.IsNullOrEmpty(newCopyMoveSubmissionOverWrite.PurchaseOrder))
				{
					newSubmissionToAdd.PurchaseOrder = newCopyMoveSubmissionOverWrite.PurchaseOrder;
				}

				if (!string.IsNullOrEmpty(newCopyMoveSubmissionOverWrite.ProjectDetail))
				{
					newSubmissionToAdd.ProjectDetail = newCopyMoveSubmissionOverWrite.ProjectDetail;
				}

				if (!string.IsNullOrEmpty(newCopyMoveSubmissionOverWrite.Status))
				{
					newSubmissionToAdd.Status = newCopyMoveSubmissionOverWrite.Status;
				}
				else
				{
					newSubmissionToAdd.Status = submissionUpdate.Status;
				}

				if (!string.IsNullOrEmpty(newCopyMoveSubmissionOverWrite.GameName))
				{
					newSubmissionToAdd.GameName = newCopyMoveSubmissionOverWrite.GameName;
				}
				else
				{
					newSubmissionToAdd.GameName = submissionUpdate.GameName;
				}

				newSubmissionToAdd.ArchiveLocation = newCopyMoveSubmissionOverWrite.ArchiveLocation;
				newSubmissionToAdd.IsRegressionTesting = newCopyMoveSubmissionOverWrite.IsRegressionTesting;

				//*** copy from current submission

				newSubmissionToAdd.IdNumber = submissionUpdate.IdNumber;
				newSubmissionToAdd.PartNumber = submissionUpdate.PartNumber;
				newSubmissionToAdd.Position = submissionUpdate.Position;
				newSubmissionToAdd.DateCode = submissionUpdate.DateCode;
				newSubmissionToAdd.Function = submissionUpdate.Function;
				newSubmissionToAdd.Version = submissionUpdate.Version;
				newSubmissionToAdd.System = submissionUpdate.System;
				newSubmissionToAdd.Operator = submissionUpdate.Operator;
				newSubmissionToAdd.ContractNumber = submissionUpdate.ContractNumber;
				newSubmissionToAdd.ChipType = submissionUpdate.ChipType;
				newSubmissionToAdd.Vendor = submissionUpdate.Vendor;
				newSubmissionToAdd.CryptographicallyStrongId = submissionUpdate.CryptographicallyStrongId;
				newSubmissionToAdd.SecondaryFunctionId = submissionUpdate.SecondaryFunctionId;
				newSubmissionToAdd.StudioId = submissionUpdate.StudioId;
				newSubmissionToAdd.TestingTypeId = submissionUpdate.TestingTypeId;
				newSubmissionToAdd.WorkPerformedId = submissionUpdate.WorkPerformedId;
				//******

				newSubmissionToAdd.AddDate = DateTime.Now;
				newSubmissionToAdd.AddUserId = _userContext.User.Id;
				newSubmissionToAdd.EditUserId = _userContext.User.Id;

				_dbSubmission.Submissions.Add(newSubmissionToAdd);
				_dbSubmission.UserId = _userContext.User.Id;
				_dbSubmission.SaveChanges();

				updateSubmissionId = newSubmissionToAdd.Id;

				if (newCopyMoveSubmissionOverWrite.Status == SubmissionStatus.OH.ToString())
				{
					_projectService.AddOHHistory(new trOH_History()
					{
						SubmissionId = submissionId,
						StartDate = DateTime.Now
					});
					_dbSubmission.SaveChanges();
				}

			}
			else //move to another file
			{
				submissionUpdate.FileNumber = newCopyMoveSubmissionOverWrite.FileNumber;
				submissionUpdate.SubmissionHeaderId = _dbSubmission.SubmissionHeaders.Where(x => x.FileNumber == newCopyMoveSubmissionOverWrite.FileNumber).Select(x => x.Id).SingleOrDefault();
				submissionUpdate.Type = newCopyMoveSubmissionOverWrite.SubmissionType;
				submissionUpdate.JurisdictionId = newCopyMoveSubmissionOverWrite.Jurisdiction;
				submissionUpdate.ManufacturerCode = newCopyMoveSubmissionOverWrite.ManufacturerCode;
				submissionUpdate.Year = newCopyMoveSubmissionOverWrite.Year;
				submissionUpdate.Sequence = newCopyMoveSubmissionOverWrite.Sequence;
				submissionUpdate.ManufacturerDescription = _dbSubmission.tbl_lst_fileManu.Where(x => x.Code == newCopyMoveSubmissionOverWrite.ManufacturerCode).Select(x => x.Code).SingleOrDefault();

				submissionUpdate.CompanyId = newCopyMoveSubmissionOverWrite.CompanyId;
				submissionUpdate.ContractTypeId = newCopyMoveSubmissionOverWrite.ContractTypeId;
				submissionUpdate.CurrencyId = newCopyMoveSubmissionOverWrite.CurrencyId;

				if (newCopyMoveSubmissionOverWrite.SubmitDate != null)
				{
					submissionUpdate.SubmitDate = newCopyMoveSubmissionOverWrite.SubmitDate;
				}
				if (newCopyMoveSubmissionOverWrite.RecieveDate != null)
				{
					submissionUpdate.ReceiveDate = newCopyMoveSubmissionOverWrite.RecieveDate;
				}

				submissionUpdate.TestingPerformedId = newCopyMoveSubmissionOverWrite.TestingPerformedId;
				submissionUpdate.CertificationLab = newCopyMoveSubmissionOverWrite.CertificationLab;
				submissionUpdate.Lab = newCopyMoveSubmissionOverWrite.TestLab;

				if (!string.IsNullOrEmpty(newCopyMoveSubmissionOverWrite.PurchaseOrder))
				{
					submissionUpdate.PurchaseOrder = newCopyMoveSubmissionOverWrite.PurchaseOrder;
				}

				if (!string.IsNullOrEmpty(newCopyMoveSubmissionOverWrite.ProjectDetail))
				{
					submissionUpdate.ProjectDetail = newCopyMoveSubmissionOverWrite.ProjectDetail;
				}

				submissionUpdate.ArchiveLocation = newCopyMoveSubmissionOverWrite.ArchiveLocation;
				submissionUpdate.IsRegressionTesting = newCopyMoveSubmissionOverWrite.IsRegressionTesting;

				submissionUpdate.EditDate = DateTime.Now;
				submissionUpdate.EditUserId = _userContext.User.Id;

				_dbSubmission.Entry(submissionUpdate).State = EntityState.Modified;
				_dbSubmission.UserId = _userContext.User.Id;
				_dbSubmission.SaveChanges();
				updateSubmissionId = submissionId;
			}

			return updateSubmissionId;

		}
		private void CopyMoveShipping(string oldFileNumber, string newFileNumber, bool copy = false)
		{
			_submissionService.CopyMoveShipping(oldFileNumber, newFileNumber, copy);
		}
		private void CopyMoveAndUpdateJurisdictions(int submissionId, int newSubmissionId, CopyMoveSubmissionOverWriteDTO newCopyMoveSubmissionOverWrite, CopyMoveOption copyMove, bool includeJurisdictions = false)
		{
			var affectedJurs = new List<JurisdictionalData>();

			//PrimaryJur for new file
			var newPrimaryJur = _dbSubmission.Submissions.Where(x => x.FileNumber == newCopyMoveSubmissionOverWrite.FileNumber).Select(x => x.JurisdictionId).FirstOrDefault();

			var queryJurisdictionIdList = _dbSubmission.JurisdictionalData.Where(x => x.SubmissionId == submissionId).AsQueryable();
			var jurisdictionIdList = (includeJurisdictions) ? queryJurisdictionIdList.Select(x => x.Id).ToList() : queryJurisdictionIdList.Where(x => x.JurisdictionId == newPrimaryJur).Select(x => x.Id).ToList();

			if (copyMove == CopyMoveOption.Copy)
			{
				foreach (var jurisdictionId in jurisdictionIdList)
				{
					var updateJurisdiction = _dbSubmission.JurisdictionalData.Where(x => x.Id == jurisdictionId).SingleOrDefault();
					if (!_dbSubmission.JurisdictionalData.Any(x => x.SubmissionId == newSubmissionId && x.JurisdictionId == updateJurisdiction.JurisdictionId))
					{
						var newJurisdiction = new JurisdictionalData();
						newJurisdiction.SubmissionId = newSubmissionId;
						newJurisdiction.JurisdictionName = updateJurisdiction.JurisdictionName;
						newJurisdiction.Txt = "Jurisdiction" + updateJurisdiction.JurisdictionId;
						newJurisdiction.JurisdictionId = updateJurisdiction.JurisdictionId;

						newJurisdiction.OrgIGTJurisdiction = updateJurisdiction.OrgIGTJurisdiction;
						newJurisdiction.ContractNumber = updateJurisdiction.ContractNumber;
						newJurisdiction.ClientNumber = updateJurisdiction.ClientNumber;
						newJurisdiction.LetterNumber = updateJurisdiction.LetterNumber;
						newJurisdiction.ProjectNumber = updateJurisdiction.ProjectNumber;
						newJurisdiction.ARIAccountCode = updateJurisdiction.ARIAccountCode;
						newJurisdiction.ARIDeptCode = updateJurisdiction.ARIDeptCode;
						newJurisdiction.ARIProjectNo = updateJurisdiction.ARIProjectNo;
						newJurisdiction.Restricted = updateJurisdiction.Restricted;
						newJurisdiction.TestingPerformedId = updateJurisdiction.TestingPerformedId;
						newJurisdiction.MasterBillingProject = newCopyMoveSubmissionOverWrite.WorkOrderNumber;
						newJurisdiction.PurchaseOrder = updateJurisdiction.PurchaseOrder;

						newJurisdiction.CompanyId = newCopyMoveSubmissionOverWrite.CompanyId;
						newJurisdiction.CurrencyId = newCopyMoveSubmissionOverWrite.CurrencyId;
						newJurisdiction.ContractTypeId = newCopyMoveSubmissionOverWrite.ContractTypeId;
						newJurisdiction.LabId = newCopyMoveSubmissionOverWrite.TestLabId;
						newJurisdiction.ReceiveDate = newCopyMoveSubmissionOverWrite.RecieveDate;
						newJurisdiction.RequestDate = newCopyMoveSubmissionOverWrite.SubmitDate;
						newJurisdiction.Status = JurisdictionStatus.RA.ToString();
						newJurisdiction.Active = JurisdictionActiveStatus.No.ToString();

						newJurisdiction.AddUserId = _userContext.User.Id;
						newJurisdiction.AddDate = DateTime.Now;

						//Add new Jur
						affectedJurs.AddRange(_jurisdictionalDataService.Add(newJurisdiction, true));
					}
				}
			}
			else if (copyMove == CopyMoveOption.Move)
			{
				_dbSubmission.JurisdictionalData.Where(x => x.SubmissionId == newSubmissionId && jurisdictionIdList.Contains(x.Id)).Update(x => new JurisdictionalData() { LabId = newCopyMoveSubmissionOverWrite.TestLabId });

				foreach (var jurisdictionId in jurisdictionIdList)
				{
					var updateJurisdiction = _dbSubmission.JurisdictionalData.Where(x => x.Id == jurisdictionId).SingleOrDefault();
					//Need to confirm while testing if jur data should be updated, it's only updating labID in old*************
					//updateJurisdiction.CompanyId = newCopyMoveSubmissionOverWrite.CompanyId;
					//updateJurisdiction.ContractTypeId = newCopyMoveSubmissionOverWrite.ContractTypeId;
					// updateJurisdiction.CurrencyId = newCopyMoveSubmissionOverWrite.CurrencyId;
					// updateJurisdiction.LabId = newCopyMoveSubmissionOverWrite.TestLabId;
					// updateJurisdiction.CertificationLabId = newCopyMoveSubmissionOverWrite.CertificationLabId;
					// updateJurisdiction.ReceiveDate = newCopyMoveSubmissionOverWrite.RecieveDate;
					// updateJurisdiction.RequestDate = newCopyMoveSubmissionOverWrite.SubmitDate;
					//updateJurisdiction.MasterBillingProject = newCopyMoveSubmissionOverWrite.WorkOrderNumber;
					//_jurisdictionalDataService.Update(updateJurisdiction);
					affectedJurs.Add(updateJurisdiction);
				}
			}

			//Check if Primary Jur exists
			if (!_dbSubmission.JurisdictionalData.Where(x => x.SubmissionId == newSubmissionId).Any(x => x.JurisdictionId == newPrimaryJur))
			{
				var newJurisdiction = new JurisdictionalData();
				newJurisdiction.SubmissionId = newSubmissionId;
				var primaryJurName = _dbSubmission.tbl_lst_fileJuris.Where(x => x.Id == newPrimaryJur).Select(x => x.Name).SingleOrDefault();

				var submissionInfoToCopy = _dbSubmission.Submissions.Where(x => x.Id == newSubmissionId).Select(x => new { ContractNumber = x.ContractNumber }).SingleOrDefault();
				newJurisdiction.JurisdictionName = primaryJurName;
				newJurisdiction.Txt = "Jurisdiction" + newPrimaryJur;
				newJurisdiction.JurisdictionId = newPrimaryJur;
				newJurisdiction.CompanyId = newCopyMoveSubmissionOverWrite.CompanyId;
				newJurisdiction.CurrencyId = newCopyMoveSubmissionOverWrite.CurrencyId;
				newJurisdiction.ContractTypeId = newCopyMoveSubmissionOverWrite.ContractTypeId;
				newJurisdiction.ContractNumber = submissionInfoToCopy.ContractNumber;
				newJurisdiction.LabId = newCopyMoveSubmissionOverWrite.TestLabId;
				newJurisdiction.ReceiveDate = newCopyMoveSubmissionOverWrite.RecieveDate;
				newJurisdiction.RequestDate = newCopyMoveSubmissionOverWrite.SubmitDate;
				newJurisdiction.Status = JurisdictionStatus.RA.ToString();
				newJurisdiction.Active = JurisdictionActiveStatus.No.ToString();
				newJurisdiction.TestingPerformedId = newCopyMoveSubmissionOverWrite.TestingPerformedId;
				newJurisdiction.MasterBillingProject = newCopyMoveSubmissionOverWrite.WorkOrderNumber;

				newJurisdiction.AddUserId = _userContext.User.Id;
				newJurisdiction.AddDate = DateTime.Now;

				//Add new Jur
				affectedJurs.AddRange(_jurisdictionalDataService.Add(newJurisdiction, true));
			}

			//protrack data
			if (copyMove == CopyMoveOption.Copy) //All new Added subs/jurs 
			{
				_jurisdictionalDataService.ProcessForProtrackAndAOtherDataForAddedJurisdictions(affectedJurs);

				//if submission Status was not changed to any pending, jur Trasfer flag will be on, make sure Jur belongs to Project
				var notBelongsToProjectsJurIds = affectedJurs.Where(x => x.JurisdictionalDataProjects == null).ToList();
				if (notBelongsToProjectsJurIds.Count > 0)
				{
					var projectJurisdictions = new List<trProjectJurisdictionalData>();
					foreach (var jd in notBelongsToProjectsJurIds)
					{
						projectJurisdictions.Add(new trProjectJurisdictionalData
						{
							AddDate = DateTime.Now,
							JurisdictionalDataId = jd.Id,
							ProjectId = newCopyMoveSubmissionOverWrite.AddJurisdictionsProjectId
						});
					}
					_projectService.AddProjectJurisdictions(projectJurisdictions);
				}
			}
			else if (copyMove == CopyMoveOption.Move)
			{
				//jurs belonging to project
				var alreadyBelongsToProjects = affectedJurs.Where(x => x.JurisdictionalDataProjects != null).ToList();
				var notBelongsToProjectsJurIds = affectedJurs.Where(x => x.JurisdictionalDataProjects == null).ToList();
				if (alreadyBelongsToProjects.Count > 0)
				{
					var affectedIdList = alreadyBelongsToProjects.Where(x => x.JurisdictionalDataProjects.Any(y => y.Project.IsTransfer == false && y.Project.BundleTypeId == 0)).Select(x => x.Id).ToList();
					_dbSubmission.trProjectJurisdictionalData.Where(x => affectedIdList.Contains(x.JurisdictionalDataId)).Update(x => new trProjectJurisdictionalData() { ProjectId = newCopyMoveSubmissionOverWrite.AddJurisdictionsProjectId });
				}

				if (notBelongsToProjectsJurIds.Count > 0)
				{
					var projectJurisdictions = new List<trProjectJurisdictionalData>();
					foreach (var jd in notBelongsToProjectsJurIds)
					{
						projectJurisdictions.Add(new trProjectJurisdictionalData
						{
							AddDate = DateTime.Now,
							JurisdictionalDataId = jd.Id,
							ProjectId = newCopyMoveSubmissionOverWrite.AddJurisdictionsProjectId
						});
					}
					_projectService.AddProjectJurisdictions(projectJurisdictions);
				}
			}
		}

		private void CopyMoveMemo(int submissionId, int newSubmissionId, bool copy = false)
		{
			_submissionService.CopyMoveMemo(submissionId, newSubmissionId, copy);
		}
		private void CopyMoveSignatures(int submissionId, int newSubmissionId, bool copy = false)
		{
			_submissionService.CopyMoveSignatures(submissionId, newSubmissionId, copy);
		}

		private void UpdateAristocratData(string newFileNumber)
		{
			//Add Aristocrat based on WON
			var newFileNumberInfo = _dbSubmission.Submissions.Where(x => x.FileNumber == newFileNumber).Select(x => new { x.JurisdictionId }).First();
			var won = _dbSubmission.JurisdictionalData.Where(x => x.Submission.FileNumber == newFileNumber && (x.MasterBillingProject != null && x.MasterBillingProject != "")).Select(x => x.MasterBillingProject).Distinct().ToList();
			if (won.Count() == 1)
			{
				var wonNumber = won[0];
				var masterFile = _dbSubmission.JurisdictionalData.Where(x => x.MasterBillingProject == wonNumber && x.Submission.FileNumber != newFileNumber).OrderByDescending(x => x.Submission.ReceiveDate).Select(x => x.Submission.FileNumber).FirstOrDefault();
				if (masterFile != null)
				{
					var aristocratData = _dbSubmission.AristocratData.Where(x => x.FileNumber == masterFile).ToList();
					foreach (var ariData in aristocratData)
					{
						if (!_dbSubmission.AristocratData.Where(x => x.FileNumber == newFileNumber).Any())
						{
							var newData = new AristocratData()
							{
								FileNumber = newFileNumber,
								JurisdictionId = null,
								AristocratType = ariData.AristocratType,
								AristocratComplexity = ariData.AristocratComplexity,
								ProjectType = "New Submissions",
								AddDate = DateTime.Now,
								AddUserId = _userContext.User.Id
							};
							_dbSubmission.AristocratData.Add(newData);
							_dbSubmission.SaveChanges();
							var aristocratDataId = newData.Id;

							foreach (var market in ariData.Markets)
							{
								var newMarket = new AristocratDataMarket()
								{
									AristocratDataId = aristocratDataId,
									IsPrime = market.IsPrime,
									Market = market.Market
								};
								_dbSubmission.AristocratDataMarket.Add(newMarket);
							}
							_dbSubmission.SaveChanges();
						}
					}
				}
			}
		}
		private void UpdateWONForOption(int oldSubmissionId, int newSubmissionId, bool useSelFileWon, bool useSelFileAsWon, bool useNewFileWon, bool useNewFileAsWon)
		{
			//Do Sql instead of loop not slow down
			var query = "";
			if (useSelFileWon)
			{
				query = "Update a Set MasterBillingProject = b.MasterBillingProject from Jurisdictionaldata a join (Select JurisdictionId, MasterBillingProject from Jurisdictionaldata where re_Keytbl=" + oldSubmissionId + " and (MasterBillingProject is not null And MasterBillingProject <> '')) b on a.JurisdictionId=b.JurisdictionId Where a.re_keytbl = " + newSubmissionId + " and (a.MasterBillingProject is null or a.MasterBillingProject = '')";
			}
			else if (useSelFileAsWon)
			{
				var newFileNumber = _dbSubmission.Submissions.Where(x => x.Id == oldSubmissionId).Select(x => x.FileNumber).SingleOrDefault();
				var filenumber = new FileNumber(newFileNumber);
				var newMasterBillingProject = filenumber.AccountingFormatNoDashes;
				query = "Update a Set MasterBillingProject = '" + newMasterBillingProject + "' from Jurisdictionaldata a Where a.re_keytbl = " + newSubmissionId + " and (a.MasterBillingProject is null or a.MasterBillingProject = '')";
			}
			else if (useNewFileWon)
			{
				query = "Update a Set MasterBillingProject = b.MasterBillingProject from Jurisdictionaldata a join (Select JurisdictionId, MasterBillingProject from Jurisdictionaldata where re_Keytbl=" + newSubmissionId + " and (MasterBillingProject is not null And MasterBillingProject <> '')) b on a.JurisdictionId=b.JurisdictionId Where a.re_keytbl = " + newSubmissionId + " and (a.MasterBillingProject is null or a.MasterBillingProject = '')";
			}
			else if (useNewFileAsWon)
			{
				var newFileNumber = _dbSubmission.Submissions.Where(x => x.Id == newSubmissionId).Select(x => x.FileNumber).SingleOrDefault();
				var filenumber = new FileNumber(newFileNumber);
				var newMasterBillingProject = filenumber.AccountingFormatNoDashes;
				query = "Update a Set MasterBillingProject = '" + newMasterBillingProject + "' from Jurisdictionaldata a Where a.re_keytbl = " + newSubmissionId + " and (a.MasterBillingProject is null or a.MasterBillingProject = '')";

			}
			if (!string.IsNullOrEmpty(query))
			{
				query = "exec devin_setcontext " + _userContext.User.Id + ";" + query + ";";
				_dbSubmission.Database.ExecuteSqlRaw(query);
			}

		}

		#endregion

	}
}