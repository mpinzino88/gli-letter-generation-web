﻿extern alias ZEFCore;
using AutoMapper.QueryableExtensions;
using GLI.EFCore.Submission;
using Submissions.Common;
using Submissions.Core;
using Submissions.Web.Areas.Submission.Models.SubmissionBulkUpdate;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using ZEFCore::Z.EntityFramework.Plus;

namespace Submissions.Web.Areas.Submission.Controllers
{
	public class SubmissionBulkUpdateController : Controller
	{
		private readonly SubmissionContext _dbSubmission;
		private readonly IUserContext _userContext;
		private readonly ISubmissionService _submissionService;
		private readonly IConfigContext _configContext;

		public SubmissionBulkUpdateController
		(
			SubmissionContext dbSubmission,
			ISubmissionService submissionService,
			IUserContext userContext,
			IConfigContext configContext
		)
		{
			_dbSubmission = dbSubmission;
			_submissionService = submissionService;
			_userContext = userContext;
			_configContext = configContext;
		}

		public ActionResult Index(int submissionHeaderId, int submissionId)
		{
			var vm = new IndexViewModel();

			var submissionHeader = _dbSubmission.SubmissionHeaders
				.Where(x => x.Id == submissionHeaderId)
				.Select(x => new { x.FileNumber })
				.Single();

			vm.SubmissionHeaderId = submissionHeaderId;
			vm.SubmissionId = submissionId;
			vm.FileNumber = submissionHeader.FileNumber;
			vm.Components = LoadComponents((int)submissionHeaderId);

			return View(vm);
		}

		public ActionResult Delete(int submissionHeaderId)
		{
			var vm = new DeleteViewModel();
			var submissionHeader = _dbSubmission.SubmissionHeaders
				.Where(x => x.Id == submissionHeaderId)
				.Select(x => new { x.FileNumber })
				.Single();

			vm.SubmissionHeaderId = submissionHeaderId;
			vm.FileNumber = submissionHeader.FileNumber;
			vm.Components = LoadComponents((int)submissionHeaderId);
			var tasks = _dbSubmission.trTasks.Where(x => x.Project.FileNumber == submissionHeader.FileNumber).ToList();
			vm.HasEngTasks = tasks.Count() != 0;
			return View(vm);
		}

		[HttpPost]
		[AuthorizeUser(Permission = Permission.Submission, HasAccessRight = AccessRight.Delete)]
		public ActionResult Delete(List<int> submissionIdsToDelete)
		{
			_submissionService.Delete(submissionIdsToDelete);
			return Json("ok");
		}

		[HttpPost]
		public ActionResult Process(IndexViewModel vm)
		{
			try
			{
				_dbSubmission.ChangeTracker.AutoDetectChangesEnabled = false;

				var now = DateTime.Now;
				var updateJira = false;
				var form = vm.UpdateFields;

				foreach (var submissionId in form.UpdateSubmissionIds)
				{
					var updateSubmission = new GLI.EFCore.Submission.Submission();
					updateSubmission.Id = submissionId;
					updateSubmission.DateCode = form.DateCode;
					updateSubmission.GameName = form.GameName;
					updateSubmission.Function = form.Function;
					updateSubmission.Position = form.Position;
					updateSubmission.ReceiveDate = form.ReceiveDate;
					updateSubmission.SubmitDate = form.SubmitDate;
					updateSubmission.Version = form.Version;
					updateSubmission.ChipType = form.ChipType;
					updateSubmission.ManufacturerBuildId = form.ManufacturerBuildId;
					updateSubmission.LetterNumber = form.LetterNumber;
					_dbSubmission.Submissions.Attach(updateSubmission);

					if (!string.IsNullOrEmpty(form.DateCode))
					{
						_dbSubmission.Entry(updateSubmission).Property(x => x.DateCode).IsModified = true;
						updateJira = true;
					}

					if (!string.IsNullOrEmpty(form.GameName))
					{
						_dbSubmission.Entry(updateSubmission).Property(x => x.GameName).IsModified = true;
						updateJira = true;
					}

					if (!string.IsNullOrEmpty(form.Function))
					{
						_dbSubmission.Entry(updateSubmission).Property(x => x.Function).IsModified = true;
						updateJira = true;
					}

					if (!string.IsNullOrEmpty(form.Position))
					{
						_dbSubmission.Entry(updateSubmission).Property(x => x.Position).IsModified = true;
					}

					if (form.ReceiveDate != null)
					{
						_dbSubmission.Entry(updateSubmission).Property(x => x.ReceiveDate).IsModified = true;
					}

					if (form.SubmitDate != null)
					{
						_dbSubmission.Entry(updateSubmission).Property(x => x.SubmitDate).IsModified = true;
					}

					if (!string.IsNullOrEmpty(form.Version))
					{
						_dbSubmission.Entry(updateSubmission).Property(x => x.Version).IsModified = true;
						updateJira = true;
					}

					if (!string.IsNullOrEmpty(form.ManufacturerBuildId))
					{
						_dbSubmission.Entry(updateSubmission).Property(x => x.ManufacturerBuildId).IsModified = true;
					}

					if (!string.IsNullOrEmpty(form.LetterNumber))
					{
						_dbSubmission.Entry(updateSubmission).Property(x => x.LetterNumber).IsModified = true;
					}

					if (!string.IsNullOrEmpty(form.ChipType))
					{
						_dbSubmission.Entry(updateSubmission).Property(x => x.ChipType).IsModified = true;
					}

					if (!string.IsNullOrEmpty(form.MemoReason) && form.AddMemoSubmissionIds.Contains(submissionId))
					{
						var dbMemo = new GLI.EFCore.Submission.tblMemo
						{
							AddDate = now,
							Memo = form.MemoReason,
							SubmissionId = submissionId,
							Username = _userContext.User.Name
						};

						_dbSubmission.tblMemos.Add(dbMemo);
					}
				}

				_dbSubmission.UserId = _userContext.User.Id;
				_dbSubmission.SaveChanges();

				// update the initial project's receive date to the min submission received date
				if (form.ReceiveDate != null)
				{
					var firstUpdateSubmissionId = form.UpdateSubmissionIds.First();
					var submissionHeader = _dbSubmission.Submissions
						.Where(x => x.Id == firstUpdateSubmissionId)
						.Select(x => new { Id = x.SubmissionHeaderId, x.FileNumber })
						.Single();

					var earliestReceiveDate = _dbSubmission.Submissions
						.Where(x => x.SubmissionHeaderId == submissionHeader.Id)
						.Min(x => x.ReceiveDate);

					var initialProject = _dbSubmission.trProjects
						.Where(x => x.FileNumber == submissionHeader.FileNumber && x.IsTransfer == false)
						.Select(x => new { x.Id, x.ReceiveDate })
						.Single();

					if (initialProject.ReceiveDate.Value.Date != earliestReceiveDate)
					{
						var project = new trProject { Id = initialProject.Id, ReceiveDate = earliestReceiveDate.Value.Date };
						_dbSubmission.trProjects.Attach(project);
						_dbSubmission.Entry(project).Property(x => x.ReceiveDate).IsModified = true;
						_dbSubmission.SaveChanges();
					}
				}

				if (updateJira && _userContext.User.Environment == Common.Environment.Production)
				{
					var updateSubmissionIdsString = string.Join(",", form.UpdateSubmissionIds);
					Task.Run(() =>
					{
						using (var client = new HttpClient())
						{
							client.BaseAddress = new Uri(_configContext.Config.ApiUrl);
							client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
							var response = client.PostAsync("api/jira/updatesubmissionsinjira?keys=" + HttpUtility.UrlEncode(updateSubmissionIdsString), null).Result;
						}
					});
				}
			}
			finally
			{
				_dbSubmission.UserId = null;
				_dbSubmission.ChangeTracker.AutoDetectChangesEnabled = true;
			}

			return Json("ok");
		}

		#region Helper Methods
		private IList<ComponentViewModel> LoadComponents(int submissionHeaderId)
		{
			return _dbSubmission.Submissions
				.Where(x => x.SubmissionHeaderId == submissionHeaderId)
				.OrderBy(x => x.Id)
				.ProjectTo<ComponentViewModel>()
				.ToList();
		}
		#endregion
	}
}