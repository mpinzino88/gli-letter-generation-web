extern alias ZEFCore;
using Aspose.Cells;
using Castle.Core.Internal;
using Dapper;
using GLI.EFCore.Submission;
using Microsoft.EntityFrameworkCore;
using Submissions.Common;
using Submissions.Common.Email;
using Submissions.Core.Interfaces;
using Submissions.Core.Models.DTO;
using Submissions.Core.Models.SubmissionService.IGT;
using Submissions.Core.Models.TransferService;
using Submissions.Core.Models.WithdrawRejectService;
using Submissions.Web.Areas.Submission.Models.Import;
using Submissions.Web.Utility.Email;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using ZEFCore::Z.EntityFramework.Plus;

namespace Submissions.Web.Areas.Submission.Controllers
{
	public class ImportController : Controller
	{
		#region Private Readonly data
		private readonly List<string> _EXCELMIMETYPES = new List<string>(new string[]
		{
			"application/vnd.ms-excel",
			"application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
		});

		private readonly string _UploadFileException = "ImportController.UploadFile: Failed to upload posted file to web server.";
		private readonly string _StreamConversionException = "ImportController.CreateDataTableFromStream: Failed to open / convert HTTP posted file worksheet to datatable.";

		private readonly List<string> _ALLSA = new List<string>() { "55", "62", "63", "82", "113", "130", "193", "200", "201" };
		private readonly List<string> _ALLSD = new List<string>() { "165", "166" };
		private readonly List<string> _ALLNM = new List<string>() { "121", "253" };
		private readonly List<string> _ALLSFBINGO = new List<string>() { "261", "402", "404", "409", "410", "498" };
		private readonly List<string> _ALLSFLPM = new List<string>() { "403", "405", "406", "407", "408", "412", "413", "414", "415" };
		private readonly List<string> _IGTFAM = new List<string>() { "ATA", "ATR", "GUK", "IGU", "LGT", "SPE", "IGT" };

		private readonly List<string> _southAfricaJurisdictions = new List<string>() { "49", "552", "553" };

		private readonly IReadOnlyDictionary<int, int> _landBasedJurisdictionPrecedence = new ReadOnlyDictionary<int, int>(
												new Dictionary<int, int>()
												{
															{22, 1},
															{73, 2},
															{50, 3},
															{66, 4},
															{394, 5},
															{85, 6},
															{251, 7},
															{122, 8},
															{124, 9},
															{146, 10},
															{113, 11},
															{140, 12},
															{135, 13},
															{134, 14},
															{148, 15},
															{308, 16},
															{311, 17},
															{155, 18 }
												});

		private readonly IReadOnlyDictionary<int, int> _iGaming200ProgramsJurisdictionPrecedence = new ReadOnlyDictionary<int, int>(
			new Dictionary<int, int>()
			{
				{337, 1},
				{246, 2}
			}
			);

		private readonly IReadOnlyDictionary<int, int> _iGaming300ProgramsJurisdictionPrecedence = new ReadOnlyDictionary<int, int>(
			new Dictionary<int, int>()
			{
				{246, 1},
				{337, 2}
			}
			);

		private readonly string _UPLOADPATH = ConfigurationManager.AppSettings["FlatfileUploadPath"].ToString();

		private readonly ILogger _logger;
		private readonly IUserContext _userContext;
		private readonly IUser _user;

		private readonly SubmissionContext _dbSubmission;
		private readonly ISubmissionIGTService _submissionIGTService;
		private readonly ISubmissionSCIService _submissionSCIService;

		private readonly ITransferIGTService _transferIGTService;
		private readonly IWithdrawRejectService _wdrjService;

		private readonly IElectronicSubmissionService _electronicSubmissionService;
		private readonly IEmailService _emailService;

		private string _errorMessage;
		#endregion

		public ImportController
		(
			SubmissionContext dbSubmission,
			IUserContext userContext,
			ITransferIGTService transferIGTService,
			IWithdrawRejectService WDRJService,
			ILogger logger,
			ISubmissionIGTService submissionIGTService,
			IUser user,
			IEmailService emailService,
			IElectronicSubmissionService electronicSubmissionService,
			ISubmissionSCIService submissionSCIService
		)
		{
			_dbSubmission = dbSubmission;
			_userContext = userContext;
			_logger = logger;
			_submissionIGTService = submissionIGTService;
			_transferIGTService = transferIGTService;
			_wdrjService = WDRJService;
			_user = user;
			_emailService = emailService;
			_electronicSubmissionService = electronicSubmissionService;
			_submissionSCIService = submissionSCIService;

			_errorMessage = "";
		}

		#region Public API
		#region MVC Enpoints
		public ActionResult Index(StageFilter filter) => View(BuildStageViewModel(filter));
		public ActionResult NewSubmissions() => RedirectToAction("Index");
		public ActionResult Transfers() => RedirectToAction("Index");
		public ActionResult WithdrawAndRejects() => RedirectToAction("Index");
		public ActionResult MapRequests() => RedirectToAction("Index");
		public ActionResult RequestsRequiringReview() => RedirectToAction("Index");
		public ActionResult CompletedSGISubmissions() => RedirectToAction("Index");
		#endregion

		#region Import API
		/// <summary>
		/// Controller action to accept flatfiles via HTTP Post, validate, upload, parse, and import to the database.
		/// </summary>
		/// <param name="payload">A JSON payload object containing a manufacturer short code.</param>
		/// <returns>HTTPResponseMsg indicating success or failure.</returns>
		[System.Web.Http.HttpPost]
		public JsonResult ImportFlatFile([FromBody] FlatFileMetaData metaData)
		{
			HttpStatusCode result;
			string HTTPResponseMsg = String.Empty;
			bool processResult = false;

			bool isIgtFam = _IGTFAM.Contains(metaData.Manufacturer);
			bool sciError = false;

			if (Request.Files.Count > 0 && !String.IsNullOrWhiteSpace(metaData.Manufacturer) && isIgtFam)
			{
				bool alreadyImported = false;

				for (var i = 0; i < Request.Files.Count; i++)
				{
					alreadyImported = SpreadSheetPreviouslyImported(Request.Files[i].FileName);

					if (_EXCELMIMETYPES.Contains(Request.Files[i].ContentType) && !alreadyImported)
					{
						var fileImport = new FileImport
						{
							FileStream = Request.Files[i].InputStream,
							Manufacturer = metaData.Manufacturer,
							NewFileName = UploadFile(Request.Files[i])
						};

						if (!String.IsNullOrWhiteSpace(fileImport.NewFileName))
						{
							processResult = ProcessFlatFile(fileImport);
						}
					}
				}

				if (processResult)
				{
					result = HttpStatusCode.OK;
					HTTPResponseMsg = "File(s) uploaded.";
				}
				else
				{
					if (alreadyImported)
					{
						result = HttpStatusCode.InternalServerError;
						HTTPResponseMsg = "The flat file you attempted to upload appears to have already been uploaded previously. Please check that you are importing the correct spreadsheet and not one that has already been imported. Changing the name of the spreadsheet to a name that has never been uploaded before will allow you to continue, if you believe  this check to be incorrect.";
					}
					else
					{
						result = HttpStatusCode.InternalServerError;
						HTTPResponseMsg = _errorMessage;
					}
				}
			}
			else
			{
				if (Request.Files.Count > 0)
				{
					FlatFileDefinition flatFileDefinition;
					var file = Request.Files[0];
					metaData.FileName = file.FileName;

					if (_EXCELMIMETYPES.Contains(file.ContentType))
					{
						flatFileDefinition = new SCIGamesFlatFileDefinition(_dbSubmission, _user, _electronicSubmissionService, metaData.Manufacturer);

						flatFileDefinition.File = file;
						flatFileDefinition.UploadFlatFile();
						flatFileDefinition.ReadFlatFileToDataTable();
						flatFileDefinition.ParseFlatFileDataTable();

						var errorList = ((SCIGamesFlatFileDefinition)flatFileDefinition).Errors.ToList();
						var studios = errorList
									.Where(x => x.Column == "Studio" && x.ErrorCodeDescription.Contains("Studio doesn't exist in our database."))
									.Select(x => x.ErrorMessage)
									.ToList();
						var vendors = errorList
									.Where(x => x.Column == "Vendor")
									.Select(x => x.ErrorMessage)
									.ToList();
						var platform = errorList
									.Where(x => x.Column == "Platform" && x.ErrorCodeDescription.Contains("Platform doesn't exist in our database."))
									.Select(x => x.ErrorMessage)
									.ToList();
						if (studios.Count > 0 || platform.Count > 0 || vendors.Count > 0)
						{
							var mailMsg = new MailMessage();
							mailMsg.From = new MailAddress("noreply@gaminglabs.com");
							if (_userContext.User.Environment == Common.Environment.Production)
								mailMsg.To.Add(new MailAddress("N.LaBrocca@gaminglabs.com"));
							else
								mailMsg.To.Add(new MailAddress(_userContext.User.Email));

							mailMsg.Subject = "SGI FlatFile: Invalid Fields";
							mailMsg.Body = _emailService.RenderTemplate("FlatFileInvalidField",
								new FlatFileInvalidFieldsEmail()
								{
									Filename = metaData.FileName,
									Studios = studios,
									Platforms = platform,
									Vendors = vendors
								});

							_emailService.Send(mailMsg);
						}
						errorList.RemoveAll(x => x.Column == "Studio" || x.Column == "Vendor");

						if (flatFileDefinition is SCIGamesFlatFileDefinition && errorList.Count > 0)
						{
							sciError = true;
						}

						if (!sciError)
						{
							flatFileDefinition.FlatDataSet.RemoveAll(x => x.JurisdictionStatus == JurisdictionStatus.AP.ToString() || x.JurisdictionStatus == JurisdictionStatus.OH.ToString() || x.JurisdictionStatus == JurisdictionStatus.RA.ToString());

							var mathRecords = flatFileDefinition.FlatDataSet.Where(x => x.Function == Function.MathAnalysis.GetEnumDescription()).ToList();
							var nonMathRecords = flatFileDefinition.FlatDataSet.Where(x => x.Function != Function.MathAnalysis.GetEnumDescription()).ToList();
							if (mathRecords.Count > 0)
							{
								for (int i = 0; i < mathRecords.Count(); i++)
								{
									flatFileDefinition.FlatDataSet.Clear();
									var tempList = new List<FlatFileDTO>();
									tempList.Add(mathRecords[i]);
									flatFileDefinition.FlatDataSet = tempList;
									var electronicNewSubmissionHeaderId = flatFileDefinition.BuildNewSubmissions();
								}
							}

							if (nonMathRecords.Count > 0)
							{
								flatFileDefinition.FlatDataSet.Clear();
								flatFileDefinition.FlatDataSet = nonMathRecords;

								var personalityPGMFunction = Function.PersonalityPGM.ToString().Replace(" ", "").ToLower().Trim();
								var hasPersonalityPGM = nonMathRecords.Any(x => x.Function.Replace(" ", "").ToLower().Trim() == personalityPGMFunction);
								var xldf = nonMathRecords.Where(x => x.GameName.Replace(" ", "").ToLower().Trim().Contains("xldf")).FirstOrDefault();
								if (hasPersonalityPGM && xldf != null)
								{
									nonMathRecords.RemoveAll(x => x.GameName.Replace(" ", "").ToLower().Trim().Contains("xldf"));
								}

								if (!hasPersonalityPGM && xldf != null)
								{
									nonMathRecords.RemoveAll(x => x.GameName.Replace(" ", "").ToLower().Trim().Contains("xldf"));
									nonMathRecords.Add(xldf);
								}
								var electronicNewSubmissionHeaderId = flatFileDefinition.BuildNewSubmissions();
							}
							processResult = true;
						}
						else
						{
							var htmlstring = "The Following Error has occurred with file: " + metaData.FileName + ". Please resolve the error and try again. <br/>";
							errorList.ForEach(x => { htmlstring += "\u2022 " + x.ErrorMessage + "<br/>"; });
							HTTPResponseMsg = htmlstring;
						}
					}

					if (processResult)
					{
						result = HttpStatusCode.OK;
						HTTPResponseMsg = "File(s) uploaded.";
					}
					else
					{
						if (sciError)
						{
							result = HttpStatusCode.InternalServerError;
						}
						else
						{
							result = HttpStatusCode.InternalServerError;
							HTTPResponseMsg = "Failed to parse flat file. Please review the file for formating errors and try again.";
						}
					}
				}
				else
				{
					result = HttpStatusCode.BadRequest;
					if (Request.Files.Count <= 0)
						HTTPResponseMsg = "No files provided for upload.";
					else if (String.IsNullOrWhiteSpace(metaData.Manufacturer))
						HTTPResponseMsg = "No manufacturer provided for upload.";
				}
			}

			Response.StatusCode = (int)result;
			return Json(HTTPResponseMsg);
		}


		#endregion

		#region Staged New Submission API
		[System.Web.Http.HttpPost]
		public ContentResult CreateNewSubmission(FlatFileClientDTO newSubmission)
		{
			if (_IGTFAM.Contains(newSubmission.ManufacturerShortCode))
			{
				return Content(ComUtil.JsonEncodeCamelCase(CreateNewSubmissionFromFlatFileIGT(newSubmission)), "application/json");
			}
			else
			{
				return Content(ComUtil.JsonEncodeCamelCase(CreateNewSubmissionFromFlatFile(newSubmission)), "application/json");
			}
		}

		[System.Web.Http.HttpPost]
		public void UpdateElectronicSubmissionComponent(FlatFileClientDTO component)
		{
			var jurisdictionalData = _dbSubmission.ElectronicSubmissionJurisdictionalDatas.Where(x => x.ElectronicSubmissionComponentId == component.Id).Single();
			jurisdictionalData.CompanyId = component.CompanyId;
			jurisdictionalData.ContractId = component.ContractTypeId;
			jurisdictionalData.CurrencyId = component.CurrencyId;
			jurisdictionalData.CertLabId = component.CertificationLabId;
			jurisdictionalData.LabId = component.TestLabId;
			jurisdictionalData.ElectronicSubmissionComponent.FunctionId = component.FunctionId;
			SaveChanges();
		}

		[System.Web.Http.HttpPost]
		public void UpdateElectronicSubmissionComponents(List<FlatFileClientDTO> components)
		{
			components.ForEach(x =>
			{
				var jurisdictionalData = new ElectronicSubmissionJurisdictionalData();
				jurisdictionalData.Id = x.ElectronicSubmissionJurisdictionalDataId;
				jurisdictionalData.CompanyId = x.CompanyId;
				jurisdictionalData.ContractId = x.ContractTypeId;
				jurisdictionalData.CurrencyId = x.CurrencyId;
				jurisdictionalData.CertLabId = x.CertificationLabId;
				jurisdictionalData.LabId = x.TestLabId;

				_dbSubmission.ElectronicSubmissionJurisdictionalDatas.Attach(jurisdictionalData);
				_dbSubmission.Entry(jurisdictionalData).Property(z => z.CompanyId).IsModified = true;
				_dbSubmission.Entry(jurisdictionalData).Property(z => z.ContractId).IsModified = true;
				_dbSubmission.Entry(jurisdictionalData).Property(z => z.CurrencyId).IsModified = true;
				_dbSubmission.Entry(jurisdictionalData).Property(z => z.CertLabId).IsModified = true;
				_dbSubmission.Entry(jurisdictionalData).Property(z => z.LabId).IsModified = true;


				var electronicSubmissionComponent = new ElectronicSubmissionComponent();
				electronicSubmissionComponent.Id = x.Id;
				electronicSubmissionComponent.FunctionId = x.FunctionId;

				_dbSubmission.ElectronicSubmissionComponents.Attach(electronicSubmissionComponent);
				_dbSubmission.Entry(electronicSubmissionComponent).Property(z => z.FunctionId).IsModified = true;
			});
			SaveChanges();
		}

		private SubmissionResultDTO CreateNewSubmissionFromFlatFileIGT(FlatFileClientDTO newSubmission)
		{
			var archiveLocation = GetArchiveLocation(Convert.ToInt32(newSubmission.TestLabId));

			var newSubmissionPackage = new SubmissionHeaderIGTDTO
			{
				ArchiveLocation = archiveLocation,
				CertificationLaboratoryId = newSubmission.CertificationLabId ?? 0,
				CompanyId = newSubmission.CompanyId ?? 0,
				ContractTypeId = newSubmission.ContractTypeId ?? 0,
				CurrencyId = newSubmission.CurrencyId ?? 0,
				LetterNumber = newSubmission.LetterNumber,
				ManufacturerCode = newSubmission.ManufacturerShortCode,
				JurisdictionId = newSubmission.PrimaryJurisdiction,
				ReceivedDate = newSubmission.ReceivedDate,
				RequestDate = newSubmission.RequestDate,
				Type = _dbSubmission.tbl_lst_SubmissionType.Where(x => x.Id == newSubmission.SubmissionTypeId).Single().Code,
				TestLaboratoryId = newSubmission.TestLabId ?? 0,
				Year = DateTime.Now.ToString("yy"),
				OverrideDuplicateGuard = newSubmission.OverRideDuplicateGuard,
				WorkOrderNumber = newSubmission.WorkOrderNumber,
				Source = SubmissionSource.FlatFile
			};
			return _submissionIGTService.Add(newSubmissionPackage);
		}

		private SubmissionResultDTO CreateNewSubmissionFromFlatFile(FlatFileClientDTO newSubmission)
		{
			var submitDate = newSubmission.Children.First().SubmitDate;
			var receiveDate = newSubmission.Children.First().ReceivedDate;
			SubmissionHeaderDTO submissionResult = null;
			if (submitDate != null && receiveDate != null)
			{
				submissionResult = _submissionSCIService.Add(newSubmission.Id, submitDate, receiveDate);
			}
			SubmissionResultDTO result = new SubmissionResultDTO();

			if (submissionResult != null)
			{
				result.FileNumber = submissionResult.FileNumber;
				result.ManufacturerCode = submissionResult.ManufacturerCode;
				result.OverallResult = true;
				result.ProjectId = (int)submissionResult.Project.Id;
				result.RequestType = ElectronicSubmissionJurisdictionalRequestAction.PND.GetEnumDescription();
				result.SubmissionIds = submissionResult.Submissions.Select(x => x.Id).ToList();
				result.ErrorMessage = null;
			}
			else
			{
				var ErrorMessage = "";
				if (submitDate == null)
				{
					ErrorMessage += "Submit Date is required. ";
				}
				if (receiveDate == null)
				{
					ErrorMessage += "Receive Date is required. ";
				}
				result.ErrorMessage = ErrorMessage;
			}

			return result;
		}

		[System.Web.Http.HttpPost]
		public ContentResult AddRecordToExistingSubmission(StagedSubmission newSubmission)
		{
			var archiveLocation = GetArchiveLocation(Convert.ToInt32(newSubmission.TestLabId));

			var newSubmissionPackage = new SubmissionHeaderIGTDTO
			{
				ArchiveLocation = archiveLocation,
				CertificationLaboratoryId = newSubmission.CertificationLabId ?? 0,
				CompanyId = newSubmission.CompanyId ?? 0,
				ContractTypeId = newSubmission.ContractTypeId ?? 0,
				CurrencyId = newSubmission.CurrencyId ?? 0,
				LetterNumber = newSubmission.LetterNumber,
				ManufacturerCode = newSubmission.ManufacturerShortCode,
				JurisdictionId = newSubmission.PrimaryJurisdiction,
				ReceivedDate = newSubmission.ReceivedDate,
				RequestDate = newSubmission.RequestDate,
				Type = _dbSubmission.tbl_lst_SubmissionType.Where(x => x.Id == newSubmission.SubmissionTypeId).Single().Code,
				TestLaboratoryId = newSubmission.TestLabId ?? 0,
				Year = DateTime.Now.ToString("yy"),
				OverrideDuplicateGuard = newSubmission.OverRideDuplicateGuard,
				WorkOrderNumber = newSubmission.WorkOrderNumber,
				Source = SubmissionSource.FlatFile,
				FileNumber = newSubmission.FileNumber
			};

			return Content(ComUtil.JsonEncodeCamelCase(_submissionIGTService.AddRecord(newSubmissionPackage)), "application/json");
		}
		[System.Web.Http.HttpPost]
		public ContentResult GetStagedNewSubmissions()
		{
			return Content(ComUtil.JsonEncodeCamelCase(BuildStagedNewSubmissionDataSet(new StageFilter())), "application/json");
		}
		[System.Web.Http.HttpPost]
		public ContentResult GetRecentlyCompletedNewSubmissions()
		{
			return Content(ComUtil.JsonEncodeCamelCase(BuildNewCompletedSubmissions()), "application/json");
		}
		[System.Web.Http.HttpPost]
		public ContentResult DeleteStagedNewSubmission(string id)
		{
			var forDeletion =
				_dbSubmission.trIGTTemporaryTables
				.Where(x => x.Letter_Number == id && new List<int> { 1, 2 }.Contains(x.Active) && x.Status == FlatFileStatus.PND.ToString())
				.ToList();

			var result = forDeletion.Select(x => x.Id).ToList();

			_dbSubmission.trIGTTemporaryTables.RemoveRange(forDeletion);
			SaveChanges();

			return Content(ComUtil.JsonEncodeCamelCase(result), "application/json");
		}
		#endregion

		#region Staged Transfer Request API
		[System.Web.Http.HttpPost]
		public ContentResult CreateTransferRequests(List<StagedFollowupRequest> requests)
		{
			var transferDTOs =
				requests
				.Where(x => x.Status == FlatFileStatus.XFR.ToString())
				.Select(y => new TransferDTO
				{
					ElectronicRequestId = y.Id,
					JurisdictionIdInternal = y.Jurisdiction,
					JurisdictionNameInternal = y.JurisdictionDescription,
					ManufacturerJurisdictionUniqueId = y.BillingReference,
					IdNumber = y.Material,
					SubmissionId = y.SubmissionId,
					ReceivedDate = y.ReceivedDate,
					RequestDate = y.RequestDate,
					ManufacturerSubmissionUniqueId = y.LetterNumber,
					ManufacturerProjectId = y.ProjectId,
					WorkOrderNumber = y.WorkOrderNumber,
					FileNumber = y.FileNumber,
					TestLaboratoryId = y.TestLabId,
					MappedSubmissionId = y.MappedSubmissionId,
					ManufacturerShortCode = y.ManufacturerCode,
					PurchaseOrder = y.PurchaseOrder
				}).ToList();

			var result = new TransferResultDTO();
			foreach (var transferHeader in _transferIGTService.Add(transferDTOs))
			{
				result.SuccessfulRequests.AddRange(transferHeader.TransferRequests.Where(x => x.Processed).ToList());
				result.FailedTransferRequests.AddRange(transferHeader.TransferRequests.Where(x => !x.Processed).ToList());
				if (result.FailedTransferRequests.Count > 0)
				{
					result.ErrorMessage = "One or more of the jurisdictions were not created becasue they already existed.";
				}
				if (transferHeader.GPSUploadFailed)
				{
					result.GPSUploadFailed = true;
				}
			}

			return Content(ComUtil.JsonEncodeCamelCase(result), "application/json");
		}
		[System.Web.Http.HttpPost]
		public ContentResult GetStagedTransferRequests()
		{
			return Content(ComUtil.JsonEncodeCamelCase(BuildStagedTransferRequestDataSet()), "application/json");
		}
		[System.Web.Http.HttpPost]
		public ContentResult GetRecentlyCompletedTransfers()
		{
			return Content(ComUtil.JsonEncodeCamelCase(BuildCompletedTransfers()), "application/json");
		}
		[System.Web.Http.HttpPost]
		public ContentResult DeleteStagedTransferRequest(int id)
		{
			var forDeletion =
				_dbSubmission.trIGTTemporaryTables
				.Where(x => x.Id == id && new List<int> { 1, 2 }.Contains(x.Active) && x.Status == FlatFileStatus.XFR.ToString())
				.ToList();

			var result = forDeletion.Select(x => x.Id).ToList();

			_dbSubmission.trIGTTemporaryTables.RemoveRange(forDeletion);
			SaveChanges();

			return Content(ComUtil.JsonEncodeCamelCase(result), "application/json");
		}
		[System.Web.Http.HttpPost]
		public ContentResult DeleteTransferPackageRequest(string id)
		{
			var forDeletion =
				_dbSubmission.trIGTTemporaryTables
				.Where(x => x.Letter_Number == id && new List<int> { 1, 2 }.Contains(x.Active) && x.Status == FlatFileStatus.XFR.ToString())
				.ToList();

			var result = forDeletion.Select(x => x.Id).ToList();

			_dbSubmission.trIGTTemporaryTables.RemoveRange(forDeletion);
			SaveChanges();

			return Content(ComUtil.JsonEncodeCamelCase(result), "application/json");
		}
		#endregion

		#region Staged WDRJ Request API
		[System.Web.Http.HttpPost]
		public ContentResult ProcessWDRJRequests(List<StagedFollowupRequest> requests)
		{
			var wdrjPackages =
				requests
				.Where(x => x.Status == FlatFileStatus.WDR.ToString())
				.Select(y => new WithdrawRejectRequest
				{
					WithdrawRejectRequestId = y.Id,
					InternalJurisdictionId = y.Jurisdiction,
					InternalJurisdictionName = y.JurisdictionDescription,
					ManufacturerJurisdictionUniqueId = y.BillingReference,
					IdNumber = y.Material,
					SubmissionId = y.SubmissionId,
					ReceivedDate = y.ReceivedDate,
					RequestDate = y.RequestDate,
					InsertDate = y.InsertDate,
					CloseDate = y.CloseDate,
					FileNumber = y.FileNumber,
					ManufacturerSubmissionUniqueId = y.LetterNumber,
					ManufacturerShortCode = y.ManufacturerCode,
					ManufacturerProjectId = y.ProjectId,
					MappedSubmissionId = y.MappedSubmissionId,
					SelectedStatus = y.SelectedStatus,
					WDRJReason = y.WDRJReason
				})
				.GroupBy(x => x.FileNumber)
				.Select(x => new WithdrawRejectPackage
				{
					FileNumber = x.Max(y => y.FileNumber),
					DataEntryOperatorId = _userContext.User.Id,
					DataEntryOperatorName = _userContext.User.FName + " " + _userContext.User.LName,
					IsElectronicSubmission = true,
					ManufacturerCode = x.Max(y => y.ManufacturerShortCode),
					WDRJRequests = x.ToList()
				})
				.ToList();

			foreach (var package in wdrjPackages)
			{
				package.WDRJSubmissionRecords =
					package.WDRJRequests
						.GroupBy(x => x.SubmissionId)
						.Select(y => new WithdrawRejectSubmissionRecord
						{
							Id = (int)y.Key,
							IdNumber = y.Max(z => z.IdNumber),
							FileNumber = y.Max(z => z.FileNumber),
							WithdrawRejectJurisdictionalRequests =
								y.Select(z => new WithdrawRejectRequest
								{
									WithdrawRejectRequestId = z.WithdrawRejectRequestId,
									InternalJurisdictionId = z.InternalJurisdictionId,
									InternalJurisdictionName = z.InternalJurisdictionName,
									ManufacturerJurisdictionUniqueId = z.ManufacturerJurisdictionUniqueId,
									IdNumber = z.IdNumber,
									SubmissionId = z.SubmissionId,
									ReceivedDate = z.ReceivedDate,
									RequestDate = z.RequestDate,
									InsertDate = z.InsertDate,
									CloseDate = z.CloseDate,
									FileNumber = z.FileNumber,
									ManufacturerSubmissionUniqueId = z.ManufacturerSubmissionUniqueId,
									ManufacturerShortCode = z.ManufacturerShortCode,
									ManufacturerProjectId = z.ManufacturerProjectId,
									MappedSubmissionId = z.MappedSubmissionId,
									SelectedStatus = z.SelectedStatus,
									WDRJReason = z.WDRJReason
								})
								.ToList()
						}).ToList();
			}

			var wdrjCreationResultSet = _wdrjService.ProcessWithdrawRejectRequests(wdrjPackages);
			var wdrjProcessResult = new WithdrawRejectResult();

			foreach (var wdRJResult in wdrjCreationResultSet)
			{
				if (!string.IsNullOrWhiteSpace(wdRJResult.ErrorMessage))
				{
					wdrjProcessResult.ErrorMessage = String.IsNullOrWhiteSpace(wdrjProcessResult.ErrorMessage) ? wdRJResult.ErrorMessage : wdrjProcessResult.ErrorMessage + "; " + wdRJResult.ErrorMessage;
				}

				if (wdRJResult.RedundantRequests != null)
				{
					foreach (var redundantRequest in wdRJResult.RedundantRequests)
					{
						wdrjProcessResult.ErrorMessage = String.IsNullOrWhiteSpace(wdrjProcessResult.ErrorMessage) ?
										redundantRequest.InternalJurisdictionName + " " + redundantRequest.InternalJurisdictionId + " for component " + redundantRequest.IdNumber + " for File Number " + redundantRequest.FileNumber + " was already withdrawn or rejected"
										: wdrjProcessResult.ErrorMessage + "; " + redundantRequest.InternalJurisdictionName + " " + redundantRequest.InternalJurisdictionId + " for component " + redundantRequest.IdNumber + " for File Number " + redundantRequest.FileNumber + " was already withdrawn or rejected";
						wdRJResult.OverallResult = false;
					}
				}

				wdrjProcessResult.SuccessfulRequests.AddRange(wdRJResult.SuccessfulRequests);
				wdrjProcessResult.FailedWDRJRequests.AddRange(wdRJResult.FailedWDRJRequests);
			}
			wdrjProcessResult.OverallResult = !wdrjCreationResultSet.Any(x => x.OverallResult == false);

			Response.StatusCode = wdrjProcessResult.OverallResult ? (int)HttpStatusCode.OK : (int)HttpStatusCode.InternalServerError;
			return Content(ComUtil.JsonEncodeCamelCase(wdrjProcessResult), "application/json");
		}
		[System.Web.Http.HttpPost]
		public ContentResult GetStagedWDRJRequests()
		{
			return Content(ComUtil.JsonEncodeCamelCase(BuildWDRJRequestDataSet(FollowupRequestStatus.STAGED)), "application/json");
		}
		[System.Web.Http.HttpPost]
		public ContentResult GetRecentlyCompletedWDRJs()
		{
			return Content(ComUtil.JsonEncodeCamelCase(BuildWDRJRequestDataSet(FollowupRequestStatus.COMPLETED)), "application/json");
		}
		public ContentResult DeleteStagedWDRJRequest(int id)
		{
			var forDeletion =
				_dbSubmission.trIGTTemporaryTables
				.Where(x => x.Id == id && new List<int> { 1, 2 }.Contains(x.Active) && x.Status == FlatFileStatus.WDR.ToString())
				.ToList();

			var result = forDeletion.Select(x => x.Id).ToList();

			_dbSubmission.trIGTTemporaryTables.RemoveRange(forDeletion);
			SaveChanges();

			return Content(ComUtil.JsonEncodeCamelCase(result), "application/json");
		}
		[System.Web.Http.HttpPost]
		public ContentResult DeleteWDRJPackageRequest(string id)
		{
			var forDeletion =
				_dbSubmission.trIGTTemporaryTables
				.Where(x => x.Letter_Number == id && new List<int> { 1, 2 }.Contains(x.Active) && x.Status == FlatFileStatus.WDR.ToString())
				.ToList();

			var result = forDeletion.Select(x => x.Id).ToList();

			_dbSubmission.trIGTTemporaryTables.RemoveRange(forDeletion);
			SaveChanges();

			return Content(ComUtil.JsonEncodeCamelCase(result), "application/json");
		}
		#endregion

		#region Map Ambiguous Request API
		[System.Web.Http.HttpPost]
		public ContentResult GetSubmissions(SubmissionsFilter submissionsFilter)
		{
			return Content(ComUtil.JsonEncodeCamelCase(BuildFilteredSubmissionsDataSet(submissionsFilter)), "application/json");
		}
		[System.Web.Http.HttpPost]
		public ContentResult MapFollowupRequest(FollowupMapPayload payload)
		{
			var ambiguousRequests = new List<trIGTTemporaryTable>();
			var status = payload.ElectronicSubmissionType.ToLower() == "transfer" ? FlatFileStatus.XFR.ToString() : FlatFileStatus.WDR.ToString();
			var activeList = new List<int> { 1, 2 };
			var ambiguousRequest = _dbSubmission.trIGTTemporaryTables.Where(x => x.Id == payload.ElectronicSubmissionId).Single();

			if (payload.MapToAll)
			{
				ambiguousRequests.AddRange(
					_dbSubmission.trIGTTemporaryTables
					.Where(x => x.Letter_Number == payload.LetterNumber && x.Status == status && activeList.Contains(x.Active) && x.Material == ambiguousRequest.Material)
					.ToList());
			}
			else
			{
				ambiguousRequests.Add(ambiguousRequest);
			}
			foreach (var request in ambiguousRequests)
			{
				request.MappedKeytbl = payload.SubmissionId;
				request.KeyTbl = payload.SubmissionId;
			}

			SaveChanges();

			return Content(ComUtil.JsonEncodeCamelCase(true), "application/json");
		}
		[System.Web.Http.HttpPost]
		public ContentResult UnMapFollowUpRequest(int id)
		{
			var request = _dbSubmission.trIGTTemporaryTables.Single(x => x.Id == id);
			request.KeyTbl = null;
			request.MappedKeytbl = null;

			SaveChanges();

			return Content(ComUtil.JsonEncodeCamelCase(true), "application/json");
		}
		#endregion

		[System.Web.Http.HttpPost]
		public ContentResult moveStagedRequest(MoveStagedRequest moveStagedRequest)
		{
			var stagedRequest =
				_dbSubmission.trIGTTemporaryTables
				.Where(x => x.Letter_Number == moveStagedRequest.LetterNumber && new List<int> { 1, 2 }.Contains(x.Active))
				.ToList();
			var result = stagedRequest.Select(x => x.Id).ToList();

			foreach (var item in stagedRequest)
			{
				switch (moveStagedRequest.DestinationQueue)
				{
					case "New Submission":
						item.Status = "PND";
						break;
					case "Transfer":
						item.Status = "XFR";
						break;
					case "Withdraw/Reject":
						item.Status = "WDR";
						break;
					default:
						break;
				}
			}
			SaveChanges();

			return Content(ComUtil.JsonEncodeCamelCase(result), "application/json");
		}

		#endregion

		#region Save
		/// <summary>
		/// Commit the flatfile entities to the submissions database.
		/// </summary>
		/// <param name="IGTTempTableEntities">Entities to commit.</param>
		private bool SaveFlatFile(List<trIGTTemporaryTable> IGTTempTableEntities)
		{
			UpdateExistingPendingRequests(IGTTempTableEntities);

			try
			{
				_dbSubmission
					.trIGTTemporaryTables
					.AddRange(IGTTempTableEntities);

				SaveChanges();
				return true;
			}
			catch (Exception ex)
			{
				_errorMessage = _errorMessage + "  There was an error saving the data to the database.";
				_logger.LogError(ex);
				return false;
			}
		}

		#endregion

		#region Processors
		/// <summary>
		/// Integration method for parsing, prcessing and commiting a flatfile to the database
		/// </summary>
		/// <param name="file">FileImport object representing a flatfile for processing.</param>
		private bool ProcessFlatFile(FileImport file)
		{
			var dataTable = CreateDataTableFromStream(file.FileStream);

			var spreadsheetDate = GetSpreadSheetDate(file.FileStream);

			var validColumns = ValidateColumns(dataTable);

			if (validColumns.Length == 0)
			{
				var oldTable = MapData(dataTable);

				if (oldTable.HasErrors)
				{
					return false;
				}

				var IGTTempTableEntities = ParseDataTableToIGTTempTableEntities(oldTable, file, spreadsheetDate);

				if (IGTTempTableEntities.Count > 0)
				{
					DateTime _insertDate = DateTime.Now;

					ProcessSpecialJurisdictionLogic(IGTTempTableEntities);

					foreach (var IGTTempTableEntity in IGTTempTableEntities)
					{
						IGTTempTableEntity.InsertDate = _insertDate;
						IGTTempTableEntity.SpreadSheetName = file.NewFileName;
					}

					SeekExistingSubmissions(IGTTempTableEntities);
					if (SaveFlatFile(IGTTempTableEntities))
					{
						return true;
					}
				}
			}
			return false;
		}

		#region NewFlatFileManipulation

		private DataTable CreateOldIGTDataTable()
		{
			DataTable OldTable = new DataTable();

			OldTable.Columns.Add("MATERIAL", typeof(string));
			OldTable.Columns.Add("MATERIAL DESCRIPTION", typeof(string));
			OldTable.Columns.Add("JURISDICTION", typeof(string));
			OldTable.Columns.Add("JURISDICTION DESCRIPTION", typeof(string));
			OldTable.Columns.Add("STATUS", typeof(string));
			OldTable.Columns.Add("HOT", typeof(string));
			OldTable.Columns.Add("LETTER NUMBER", typeof(string));
			OldTable.Columns.Add("EMPLOYEE NAME", typeof(string));
			OldTable.Columns.Add("Project", typeof(string));
			OldTable.Columns.Add("NETWORK", typeof(string));
			OldTable.Columns.Add("SUBTYPE", typeof(string));
			OldTable.Columns.Add("BUSINESS PRIORITY DATE", typeof(string));
			OldTable.Columns.Add("FOLLOW UP NOTES", typeof(string));
			OldTable.Columns.Add("Submission Notes", typeof(string));
			OldTable.Columns.Add("Component", typeof(string));
			OldTable.Columns.Add("Invoice To", typeof(string));
			OldTable.Columns.Add("OriginalIGTMaterial", typeof(string));
			OldTable.Columns.Add("Product Category", typeof(string));
			OldTable.Columns.Add("Cabinet(s)", typeof(string));

			return OldTable;
		}

		private DataTable MapData(DataTable newTable)
		{
			DataTable oldTable = CreateOldIGTDataTable();

			int numberRows = newTable.Rows.Count;

			var iGamingBillingParties = _dbSubmission.tbl_lst_BillingParties.Where(x => x.TestingTypeId == (int)TestingType.iGaming).Select(x => x.BillingParty).ToList();

			for (int i = 0; i < numberRows - 4; i++)
			{
				List<string> jurisdictions = newTable.Rows[i]["Jurisdictions"].ToString().Split(',').ToList();
				List<string> components = new List<string>();

				if (newTable.Rows[i]["Project Request Type"].ToString() == "Attestation")
				{
					jurisdictions.Clear();
					jurisdictions.Add("999 - Attestation");
				}

				string origIGTMaterial = "";

				if (newTable.Rows[i]["Primary Material #"].ToString().Trim() == "")
				{
					newTable.Rows[i].RowError = "For Key: " + newTable.Rows[i]["Key"].ToString().Trim() + " the Primary Material can not be null.";
					_errorMessage = _errorMessage + "  <p>For Key: " + newTable.Rows[i]["Key"].ToString().Trim() + " the Primary Material can not be null.";
				}

				if (iGamingBillingParties.Contains(newTable.Rows[i]["Invoice To"].ToString()) && newTable.Rows[i]["Other Submission Materials"].ToString().Length == 0)
				{
					origIGTMaterial = newTable.Rows[i]["Primary Material #"].ToString();
					components.Add(newTable.Rows[i]["Primary Material #"].ToString());
				}
				else if (iGamingBillingParties.Contains(newTable.Rows[i]["Invoice To"].ToString()) && newTable.Rows[i]["Other Submission Materials"].ToString().Length > 0)
				{
					origIGTMaterial = newTable.Rows[i]["Primary Material #"].ToString();
				}
				else
				{
					components.Add(newTable.Rows[i]["Primary Material #"].ToString());
				}

				var additionalComponents = newTable.Rows[i]["Other Submission Materials"].ToString().Split('\n').ToList();
				additionalComponents = additionalComponents.Where(s => !string.IsNullOrWhiteSpace(s)).Distinct().ToList();
				var duplicates = additionalComponents.Where(x => components.Any(y => x.Trim() == y.Trim()));
				if (!duplicates.IsNullOrEmpty())
				{
					newTable.Rows[i].RowError = "The material " + duplicates.First().ToString() + " is duplicated in a row.  It can only be entered once.";
					_errorMessage = _errorMessage + "The material " + duplicates.First().ToString() + " is duplicated in a row.  It can only be entered once.";
				}

				if (additionalComponents.Count > 0)
				{
					components.AddRange(additionalComponents);
				}

				AddRowsToOldTable(oldTable, newTable.Rows[i], jurisdictions, components, origIGTMaterial);
			}

			return oldTable;
		}

		private DataTable AddRowsToOldTable(DataTable oldTable, DataRow row, List<string> jurisdictions, List<string> components, string orgIGTMaterial)
		{
			try
			{
				foreach (var jurisdiction in jurisdictions)
				{
					var validJurisdiction = jurisdiction.Split(new char[] { '-' })[0].ToString().TrimStart(new Char[] { '0' });
					int i = 0;
					bool validNumber = int.TryParse(validJurisdiction, out i);

					if (validNumber)
					{
						foreach (var component in components)
						{
							oldTable.Rows.Add(
								component,
								row["Material Description"],
								GetJurisdictionNumber(jurisdiction),
								GetJurisdictionName(jurisdiction),
								ConvertStatus(row["Project Request Type"].ToString()),
								"",
								row["Key"],
								"",
								"",
								row["Billing Reference"],
								GetSubType(row["Project Request Type"].ToString()),
								row["Business Priority Date"].ToString() == "" ? "" : FormatDate(row["Business Priority Date"].ToString()),
								"",
								FormatSubmissionNotes("", row["Special Instructions"].ToString(), row["Math Clone Material Number"].ToString(), row["Precert ITL File Number"].ToString()),
								row["Invoice To"],
								row["Invoice To"],
								orgIGTMaterial == "" ? component : orgIGTMaterial,
								row["Product Category"],
								row["Cabinet(s)"]);
						}
					}
					else
					{
						oldTable.Rows.Add("For Key: " + row["Key"] + " the jurisdiction: " + jurisdiction + " does not contain a number at its beginning.");
						oldTable.Rows[oldTable.Rows.Count - 1].RowError = "For Key: " + row["Key"] + " the jurisdiction: " + jurisdiction + " does not contain a number at its beginning.";
						_errorMessage = _errorMessage + "  <p>For Key: " + row["Key"] + " the jurisdiction: " + jurisdiction + " does not contain a number at its beginning.";
					}
				}
			}
			catch (Exception ex)
			{
				Console.WriteLine(ex.Message);
			}

			return oldTable;
		}

		private string GetJurisdictionNumber(string jurisdiction)
		{
			return jurisdiction.Split(new char[] { '-' })[0].ToString().TrimStart(new Char[] { '0' });
		}

		private string GetJurisdictionName(string jurisdiction)
		{
			var splitJurName = jurisdiction.Split(new char[] { '-' });

			if (splitJurName.Count() > 2)
			{
				return splitJurName[1].ToString() + " - " + splitJurName[2].ToString();
			}
			else
			{
				return splitJurName[1].ToString();
			}
		}

		private string ConvertStatus(string status)
		{
			return status == "Certification" ? "PND" :
					status == "Precertification" ? "PND" :
					status == "Transfer" ? "XFR" :
					status == "Withdrawal" ? "WDR" :
					status == "Interoperability" ? "PND" :
					status == "Attestation" ? "PND"
					: _errorMessage = _errorMessage + " Project Request Type: '" + status + "' was not Certification, Precertification, Transfer, Withdrawal, Interoperability, or Attestation.<BR>";
		}

		private string GetSubType(string status)
		{
			return status == "Precertification" ? "PC" : "";
		}

		private string GetHot(string hot)
		{
			return hot == "" ? "" :
				hot == "no" ? "" :
				"X";
		}

		private string FormatDate(string oldDate)
		{
			var date = oldDate.Split(new char[] { '-' });

			return FormatMonth(date[1]) + "/" + date[0] + "/" + "20" + date[2];
		}

		private string FormatMonth(string month)
		{
			return month == "Jan" ? "1" :
					month == "Feb" ? "2" :
					month == "Mar" ? "3" :
					month == "Apr" ? "4" :
					month == "May" ? "5" :
					month == "Jun" ? "6" :
					month == "Jul" ? "7" :
					month == "Aug" ? "8" :
					month == "Sep" ? "9" :
					month == "Oct" ? "10" :
					month == "Nov" ? "11" :
					month == "Dec" ? "12" : throw new Exception();
		}

		private string FormatSubmissionNotes(string hotReason, string specialInstructions, string mathCloneMaterial, string preCertITL)
		{
			var hot = "";
			var splitHot = hotReason.Split(new char[] { '-' });
			if (splitHot.Count() > 1)
			{
				hot = hotReason.Split(new char[] { '-' })[0] == "No" ? "" : hotReason.Split(new char[] { '-' })[1];
			}

			return hot != "" ? "Hot Reason:" + hot : "" +
				specialInstructions != "" ? " Special Instructions:" + specialInstructions : "" +
				mathCloneMaterial != "" ? " Math Clone Material Number:" + mathCloneMaterial : "" +
				preCertITL != "" ? " Precert ITL File Number:" + preCertITL : "";
		}
		#endregion

		/// <summary>
		/// Integration method for handling IGT specific special jurisdiction rules.
		/// </summary>
		/// <param name="potentialRows">IGTTempTable Entities requiring special jurisdiction processing logic.</param>
		private void ProcessSpecialJurisdictionLogic(List<trIGTTemporaryTable> potentialRows)
		{
			try
			{
				var materialStatusGroups = potentialRows.GroupBy(x => new { x.Material, x.Status }).ToList();

				foreach (var materialStatusGroup in materialStatusGroups)
				{
					var SpecialJurisdictionCounts = new
					{
						AllSAExists = materialStatusGroup.Where(x => _ALLSA.Contains(x.Jurisdiction)).Select(x => x.Jurisdiction).Distinct().Count(),
						AllSDExists = materialStatusGroup.Where(x => _ALLSD.Contains(x.Jurisdiction)).Select(x => x.Jurisdiction).Distinct().Count(),
						AllNMExists = materialStatusGroup.Where(x => _ALLNM.Contains(x.Jurisdiction)).Select(x => x.Jurisdiction).Distinct().Count(),
						AllSFBingo = materialStatusGroup.Where(x => _ALLSFBINGO.Contains(x.Jurisdiction)).Select(x => x.Jurisdiction).Distinct().Count(),
						AllSFLPM = materialStatusGroup.Where(x => _ALLSFLPM.Contains(x.Jurisdiction)).Select(x => x.Jurisdiction).Distinct().Count(),
						RandomSFBingo = materialStatusGroup.Where(x => _ALLSFBINGO.Contains(x.Jurisdiction)).Select(x => x.Jurisdiction).Distinct().Max(),
						RandomSFLPM = materialStatusGroup.Where(x => _ALLSFLPM.Contains(x.Jurisdiction)).Select(x => x.Jurisdiction).Distinct().Max()
					};

					if (SpecialJurisdictionCounts.AllSAExists == 9)
					{
						foreach (var row in materialStatusGroup.Where(x => _ALLSA.Contains(x.Jurisdiction)))
						{
							HandleAllSAExistsUpdate(row);
						}
					}

					if (SpecialJurisdictionCounts.AllSDExists == 2)
					{
						foreach (var row in materialStatusGroup.Where(x => x.Jurisdiction == "166"))
						{
							HandleAllSDExistsUpdate(row);
						}
					}

					if (SpecialJurisdictionCounts.AllNMExists == 2)
					{
						foreach (var row in materialStatusGroup.Where(x => x.Jurisdiction == "121"))
						{
							HandleAllNMExistsUpdate(row);
						}
					}

					if (SpecialJurisdictionCounts.AllSFBingo > 0)
					{
						foreach (var row in materialStatusGroup.Where(x => _ALLSFBINGO.Contains(x.Jurisdiction) && x.Jurisdiction != "552"))
						{
							HandleAllSFBingoUpdate(row, SpecialJurisdictionCounts.RandomSFBingo);
						}
					}

					if (SpecialJurisdictionCounts.AllSFLPM > 0)
					{

						foreach (var row in materialStatusGroup.Where(x => _ALLSFLPM.Contains(x.Jurisdiction) && x.Jurisdiction != "553"))
						{
							HandleAllSFLPMUpdate(row, SpecialJurisdictionCounts.RandomSFLPM);
						}
					}
				}
			}
			catch (Exception ex)
			{
				_logger.LogError(ex);
			}
		}
		#endregion

		#region Handlers
		/// <summary>
		/// Handle Special Jurisdiction pre-processing for the trIGTTemporaaryTable for South Africa National (LPM)
		/// </summary>
		/// <param name="row">IGTTempTable Entity</param>
		/// <param name="targetJurisdiction">Highest Jurisdiction ID from AllSFLPM Jur List</param>
		private void HandleAllSFLPMUpdate(trIGTTemporaryTable row, string targetJurisdiction)
		{
			if (row.Jurisdiction == targetJurisdiction)
			{
				row.Jurisdiction = "553";
				row.Jurisdiction_Description = "South Africa National (LPM)";
			}
			else
			{
				row.Active = 2;
			}
		}

		/// <summary>
		/// Handle Special Jurisdiction pre-processing for the trIGTTemporaaryTable for South Africa National (Bingo)
		/// </summary>
		/// <param name="row">IGTTempTable Entity</param>
		/// <param name="targetJurisdiction">Highest Jurisdiction ID from AllSFBingo Jur List</param>
		private void HandleAllSFBingoUpdate(trIGTTemporaryTable row, string targetJurisdiction)
		{
			if (row.Jurisdiction == targetJurisdiction)
			{
				row.Jurisdiction = "552";
				row.Jurisdiction_Description = "South Africa National (Bingo)";
			}
			else
			{
				row.Active = 2;
			}
		}

		/// <summary>
		/// Handle Special Jurisdiction pre-processing for the trIGTTemporaaryTable for South Africa National
		/// </summary>
		/// <param name="row">IGTTempTable Entity</param>
		private void HandleAllSAExistsUpdate(trIGTTemporaryTable row)
		{
			if (row.Jurisdiction == "63")
			{
				row.Jurisdiction = "49";
				row.Jurisdiction_Description = "SOUTH AFRICA NATIONAL";
			}
			else
			{
				row.Active = 2;
			}
		}

		/// <summary>
		/// Handle Special Jurisdiction pre-processing for the trIGTTemporaaryTable for South Dakota
		/// </summary>
		/// <param name="row">IGTTempTable Entity</param>
		private void HandleAllSDExistsUpdate(trIGTTemporaryTable row)
		{
			row.Active = 2;
		}

		/// <summary>
		/// Handle Special Jurisdiction pre-processing for the trIGTTemporaaryTable for New Mexico
		/// </summary>
		/// <param name="row">IGTTempTable Entity</param>
		private void HandleAllNMExistsUpdate(trIGTTemporaryTable row)
		{
			row.Active = 2;
		}
		#endregion

		#region Model Builders
		private StageViewModel BuildStageViewModel(StageFilter filter)
		{
			var vm = new StageViewModel
			{
				ManufacturerList = BuildFlatFileManufacturerList(),
				WDRJReasonList = BuildWDRJReasonList(),
				SubmissionTemplates = BuildSubmissionTemplateSet(),
				StagedNewSubmissions = BuildStagedNewSubmissionDataSet(filter),
				StagedNewSubmissionColumnOptionSet = BuildStagedNewSubmissionColumnOptionSet(),
				CompletedNewSubmissions = BuildNewCompletedSubmissions(),
				CompletedNewSubmissionColumnOptionSet = BuildNewCompletedSubmissionColumnOptionSet(),
				StagedTransferRequests = BuildStagedTransferRequestQuery(),
				CompletedTransfers = BuildCompletedTransfers(),
				StagedWithdrawAndRejectRequests = BuildWDRJRequestDataSet(FollowupRequestStatus.STAGED),
				CompletedWithdrawAndRejectRequests = BuildWDRJRequestDataSet(FollowupRequestStatus.COMPLETED),
				ManufacturerGroupTileDefinitions = BuildManufacturerGroupTileDefinitions()
			};

			return vm;
		}

		private List<ManufacturerGroupTileDefinition> BuildManufacturerGroupTileDefinitions()
		{
			var flatFileCategoryId = _dbSubmission.ManufacturerGroupCategories.Where(x => x.Code.ToLower() == "flatfile").Single().Id;

			var tilesDefinitions = _dbSubmission.ManufacturerGroups.Where(x => x.ManufacturerGroupCategoryId == flatFileCategoryId)
				.Select(x => new ManufacturerGroupTileDefinition
				{
					Id = x.Id,
					Title = x.Description,
					Manufacturers = x.ManufacturerLinks.Select(y => new SelectListItem
					{
						Text = y.Manufacturer.Description + " (" + y.Manufacturer.Code + ")",
						Value = y.Manufacturer.Code
					})
					.ToList()
				})
				.ToList();

			foreach (var tile in tilesDefinitions)
			{
				if (tile.Title == "IGT")
				{
					tile.Color = "#FFA300";
					tile.TextColor = "#FFFFFF";
					tile.RequiresManufacturerSelection = false;
				}
				else if (tile.Title == "SGI")
				{
					tile.Color = "#005D99";
					tile.TextColor = "#FFFFFF";
				}
				else
				{
					tile.Color = "#FFFFFF";
					tile.TextColor = "#000000";
				}
			}

			return tilesDefinitions;
		}
		private List<StagedSubmission> BuildStagedNewSubmissionDataSet(StageFilter filter)
		{
			var result =
				BuildStagedNewSubmissionQuery(filter)
				.AsEnumerable()
				.GroupBy(trTempTable => trTempTable.Letter_Number)
				.Select(group => new StagedSubmission
				{
					LetterNumber = group.Key,
					InsertDate = group.Max(x => x.InsertDate),
					ManufacturerShortCode = group.Max(x => x.Manufacturer),
					PrimaryJurisdiction = group.Max(x => x.Primaryjur),
					PrimaryJurisdictionName = group.Max(x => x.PrimaryJurisdiction?.Name),
					Status = group.Max(x => x.Status),
					SubmissionType = group.Max(x => x.SubType),
					SubmissionTypeId =
						_dbSubmission.tbl_lst_SubmissionType.ToList().Where(x => x.Code == group.Max(y => y.SubType)).Select(x => x.Id).FirstOrDefault(),
					UserId = group.Max(x => x.LoginID),
					UserName = group.Max(x => x.AddUser.FName + " " + x.AddUser.LName),
					MissingBillingReference = group.Any(x => x.BillingReference == "" || x.BillingReference == null),
					ProductCategory = group.Max(x => x.ProductCategory),
					Children = group.Select(x => new StagedSubmissionChild { Material = x.Material, Jurisdiction = x.Jurisdiction_Description, JurisdictionId = int.Parse(x.Jurisdiction), BillingReference = x.BillingReference }).ToList()
				})
				.ToList();

			return result;
		}
		private List<StagedSubmission> BuildNewCompletedSubmissions()
		{
			var twoWeeksAgo = DateTime.Now.AddDays(-14);

			return
				_dbSubmission.trIGTTemporaryTables
				.Where(x => x.Active != 1 && x.Active != 2 && x.Status == FlatFileStatus.PND.ToString() && x.InsertDate > twoWeeksAgo)
				.AsEnumerable()
				.OrderByDescending(x => x.InsertDate)
				.GroupBy(x => new { x.Letter_Number, x.NewSubFileNumber, x.InsertDate, x.RecordAddedToExistingSubmission })
				.Take(10)
				.Select(group => new StagedSubmission
				{
					LetterNumber = group.Key.Letter_Number,
					InsertDate = group.Max(x => x.InsertDate),
					ManufacturerShortCode = group.Max(x => x.Manufacturer),
					FileNumber = group.Max(x => group.Key.NewSubFileNumber),
					AddToExistingSubmission = group.Max(x => x.RecordAddedToExistingSubmission),
					Children = group.Select(x => new StagedSubmissionChild { Material = x.Material, Jurisdiction = x.Jurisdiction_Description, JurisdictionId = int.Parse(x.Jurisdiction), BillingReference = x.BillingReference }).ToList()
				})
				.ToList();
		}
		private List<StagedFollowupRequest> BuildCompletedTransfers()
		{
			var result = new List<StagedFollowupRequest>();

			var transferRequestQuery = new StringBuilder();
			transferRequestQuery.Append(@"SELECT ");
			transferRequestQuery.Append(@"submission.Filenumberstr as FileNumber ");
			transferRequestQuery.Append(@",dataEntryOperator.fname + ' ' + dataEntryOperator.lname as UserName ");
			transferRequestQuery.Append(@",IGTTempTable.Material as Material ");
			transferRequestQuery.Append(@",jurisdictionList.Jurisdiction_ID as Jurisdiction ");
			transferRequestQuery.Append(@",jurisdictionList.Jurisdiction as JurisdictionDescription ");
			transferRequestQuery.Append(@",IGTTempTable.Letter_Number as LetterNumber ");
			transferRequestQuery.Append(@",IGTTempTable.InsertDate ");
			transferRequestQuery.Append(@"FROM ");
			transferRequestQuery.Append(@"trIGTTemporaryTable IGTTempTable with(nolock) ");
			transferRequestQuery.Append(@"LEFT JOIN Submissions submission with(nolock) on submission.keytbl = IGTTempTable.keytbl ");
			transferRequestQuery.Append(@"LEFT JOIN tbl_lst_JurisdictionMappings IGTJuriMap ON IGTTempTable.jurisdiction = IGTJuriMap.ExternalJurisdictionId AND IGTJuriMap.FlatFile = 1 AND IGTJuriMap.Active = 1 ");
			transferRequestQuery.Append(@"LEFT JOIN tbl_lst_fileJuris jurisdictionList ON jurisdictionList.jurisdiction_id = IGTJuriMap.Jurisdiction_ID ");
			transferRequestQuery.Append(@"INNER JOIN trLogin dataEntryOperator ON dataEntryOperator.loginId = IGTTempTable.loginid ");
			transferRequestQuery.Append(@"WHERE ");
			transferRequestQuery.Append(@"((IGTTempTable.status = 'XFR') AND IGTTempTable.Active = 0 AND IGTTempTable.InsertDate > GETDATE() - 2 And IGTTempTable.Primary_ IS NOT NULL AND IGTJuriMap.ManufacturerShort_Id = 'IGT')");
			transferRequestQuery.Append(@"OR ");
			transferRequestQuery.Append(@"((IGTTempTable.status = 'XFR') AND IGTTempTable.Active = 0 AND IGTTempTable.InsertDate > GETDATE() - 2 And IGTTempTable.Primary_ IS NOT NULL AND IGTTempTable.Jurisdiction in ('49', '552', '553')) ");

			var transferPackages = _dbSubmission.Database.GetDbConnection().Query<StagedFollowupRequest>(transferRequestQuery.ToString()).ToList().GroupBy(x => new { x.LetterNumber, x.InsertDate }).ToList();

			if (transferPackages.Count > 0)
			{
				foreach (var transferPackage in transferPackages)
				{
					var header = transferPackage.Select(x => new StagedFollowupRequest
					{
						LetterNumber = transferPackage.Key.LetterNumber,
						UserId = transferPackage.Max(y => y.UserId),
						UserName = transferPackage.Max(y => y.UserName),
						InsertDate = transferPackage.Key.InsertDate
					})
					.First();

					header.Children.AddRange(transferPackage.OrderBy(x => x.Material).ThenBy(x => x.Jurisdiction).ToList());

					result.Add(header);
				}
			}

			return result.OrderByDescending(x => x.InsertDate).Take(10).ToList();
		}
		private List<StagedFollowupRequest> BuildStagedTransferRequestDataSet()
		{
			return BuildStagedTransferRequestQuery();
		}
		private List<StagedSubmissionColumnOptions> BuildNewCompletedSubmissionColumnOptionSet()
		{
			var columnOptions = new List<StagedSubmissionColumnOptions>();

			#region Column Builders
			columnOptions.Add(new StagedSubmissionColumnOptions
			{
				Field = "letterNumber",
				Label = "Letter Number",
				Sortable = true,
				Type = ColumnType.text
			});

			columnOptions.Add(new StagedSubmissionColumnOptions
			{
				Field = "manufacturerShortCode",
				Label = "Manufacturer",
				Sortable = true,
				Type = ColumnType.text
			});

			columnOptions.Add(new StagedSubmissionColumnOptions
			{
				Field = "insertDate",
				Label = "Imported On",
				Sortable = true,
				Type = ColumnType.date
			});

			columnOptions.Add(new StagedSubmissionColumnOptions
			{
				Field = "fileNumber",
				Label = "Filenumber",
				Sortable = true,
				Type = ColumnType.text
			});

			columnOptions.Add(new StagedSubmissionColumnOptions
			{
				Field = "addToExistingSubmission",
				Label = "Appended",
				Sortable = true,
				Type = ColumnType.text
			});

			#endregion

			return columnOptions;
		}
		private List<StagedSubmissionColumnOptions> BuildNewCompletedTransferColumnOptionSet()
		{
			var columnOptions = new List<StagedSubmissionColumnOptions>();

			#region Column Builders
			columnOptions.Add(new StagedSubmissionColumnOptions
			{
				Field = "letterNumber",
				Label = "Letter Number",
				Sortable = true,
				Type = ColumnType.text
			});

			columnOptions.Add(new StagedSubmissionColumnOptions
			{
				Field = "manufacturerCode",
				Label = "Manufacturer",
				Sortable = true,
				Type = ColumnType.text
			});

			columnOptions.Add(new StagedSubmissionColumnOptions
			{
				Field = "jurisdiction",
				Label = "Jurisdiction Id",
				Sortable = true,
				Type = ColumnType.number
			});

			columnOptions.Add(new StagedSubmissionColumnOptions
			{
				Field = "jurisdictionDescription",
				Label = "Jurisdiction",
				Sortable = true,
				Type = ColumnType.text
			});

			#endregion

			return columnOptions;
		}
		private List<StagedSubmissionColumnOptions> BuildStagedNewSubmissionColumnOptionSet()
		{
			var columnOptions = new List<StagedSubmissionColumnOptions>();

			#region Column Builders
			columnOptions.Add(new StagedSubmissionColumnOptions
			{
				Field = "letterNumber",
				Label = "Letter Number",
				Sortable = true,
				Type = ColumnType.text
			});

			columnOptions.Add(new StagedSubmissionColumnOptions
			{
				Field = "manufacturerShortCode",
				Label = "Manufacturer",
				Sortable = true,
				Type = ColumnType.text
			});

			columnOptions.Add(new StagedSubmissionColumnOptions
			{
				Field = "userName",
				Label = "Imported By",
				Sortable = true,
				Type = ColumnType.text
			});

			columnOptions.Add(new StagedSubmissionColumnOptions
			{
				Field = "insertDate",
				Label = "Imported On",
				Sortable = true,
				Type = ColumnType.date
			});

			columnOptions.Add(new StagedSubmissionColumnOptions
			{
				Field = "missingBillingReference",
				Label = "Missing Billing Reference",
				Sortable = true,
				Type = ColumnType.boolean
			});

			columnOptions.Add(new StagedSubmissionColumnOptions
			{
				Field = "edit",
				Label = "",
				Sortable = false,
				Type = ColumnType.text
			});
			#endregion

			return columnOptions;
		}
		private List<Submissions.Web.Areas.Submission.Models.Import.SubmisionTemplate> BuildSubmissionTemplateSet()
		{
			var result = new List<Submissions.Web.Areas.Submission.Models.Import.SubmisionTemplate>
			{
				new SubmisionTemplate
				{
					Id = 0,
					CertificationLabId = 0,
					CertificationLab = string.Empty,
					CompanyId = 0,
					Company = string.Empty,
					ContractId = 0,
					Contract = string.Empty,
					CurrencyId = 0,
					Currency = string.Empty,
					Code = string.Empty,
					Description = string.Empty,
					SubmissionTypeId = 0,
					SubmissionType = string.Empty,
					TestLabId = 0,
					TestLab = string.Empty
				}
			};

			result.AddRange(_dbSubmission.SubmissionTemplates.Select(x => new Submissions.Web.Areas.Submission.Models.Import.SubmisionTemplate
			{
				Id = x.Id,
				CertificationLabId = x.CertificationLabId,
				CertificationLab = x.CertificationLab.Name,
				CompanyId = x.CompanyId,
				Company = x.Company.Name,
				ContractId = x.ContractId,
				Contract = x.Contract.Code + " - " + x.Contract.Description,
				CurrencyId = x.CurrencyId,
				Currency = x.Currency.Code + " - " + x.Currency.Description,
				Code = x.Code,
				Description = x.Description,
				SubmissionTypeId = x.SubmissionTypeId,
				SubmissionType = x.SubmissionType.Code + " - " + x.SubmissionType.Description,
				TestLabId = x.TestLabId,
				TestLab = x.TestLab.Name
			})
			.ToList());

			return result;
		}
		private List<SelectListItem> BuildFlatFileManufacturerList()
		{
			return
				_dbSubmission.ManufacturerGroups
				.Where(ManufacturerGroup => ManufacturerGroup.ManufacturerGroupCategoryId == (Int32)ManufacturerGroupCategory.Flatfile)
				.SelectMany(ManufacturerGroup => ManufacturerGroup.ManufacturerLinks)
				.Select(x => new SelectListItem
				{
					Text = x.Manufacturer.Description + " (" + x.Manufacturer.Code + ")",
					Value = x.Manufacturer.Code
				})
				.ToList();
		}
		private List<StagedSubmission> BuildFilteredSubmissionsDataSet(SubmissionsFilter submissionsFilter)
		{
			var IGTManufGroup = new List<string>() { "ATA", "ATR", "BSM", "GTE", "GUK", "IGU", "LGT", "SPE", "IGT" };
			var result = new List<StagedSubmission>();

			var query = _dbSubmission.Submissions.AsQueryable();
			query = query.Where(x => IGTManufGroup.Contains(x.ManufacturerCode) && x.Type != SubmissionType.EE.ToString());

			if (!String.IsNullOrWhiteSpace(submissionsFilter.FileNumber))
			{
				query = query.Where(x => x.FileNumber.StartsWith(submissionsFilter.FileNumber.Trim()));
			}

			if (!String.IsNullOrWhiteSpace(submissionsFilter.DateCode))
			{
				query = query.Where(x => x.DateCode.StartsWith(submissionsFilter.DateCode.Trim()));
			}

			if (!String.IsNullOrWhiteSpace(submissionsFilter.GameName))
			{
				query = query.Where(x => x.GameName.StartsWith(submissionsFilter.GameName.Trim()));
			}

			if (!String.IsNullOrWhiteSpace(submissionsFilter.IdNumber))
			{
				query = query.Where(x => x.IdNumber.StartsWith(submissionsFilter.IdNumber.Trim()));
			}

			if (!String.IsNullOrWhiteSpace(submissionsFilter.Status))
			{
				query = query.Where(x => x.Status == submissionsFilter.Status);
			}

			if (!String.IsNullOrWhiteSpace(submissionsFilter.Version))
			{
				query = query.Where(x => x.Version.StartsWith(submissionsFilter.Version.Trim()));
			}

			return query.Take(25).Select(x => new StagedSubmission
			{
				FileNumber = x.FileNumber,
				SubmissionRecordId = x.Id,
				Material = x.IdNumber,
				DateCode = x.DateCode,
				Version = x.Version,
				MaterialDescription = x.GameName,
			}).ToList();
		}
		private List<StagedFollowupRequest> BuildWDRJRequestDataSet(FollowupRequestStatus status)
		{
			var result = new List<StagedFollowupRequest>();
			var query = WDRJRequestQuery(status);
			var WDRJPackages = query.ToList().GroupBy(x => new { x.LetterNumber, x.InsertDate }).ToList();

			if (WDRJPackages.Count > 0)
			{
				foreach (var WDRJPackage in WDRJPackages)
				{
					var header = WDRJPackage.Select(x => new StagedFollowupRequest
					{
						LetterNumber = WDRJPackage.Key.LetterNumber,
						UserId = WDRJPackage.Max(y => y.UserId),
						UserName = WDRJPackage.Max(y => y.UserName),
						InsertDate = WDRJPackage.Key.InsertDate
					})
					.First();

					foreach (var wdrjRequest in WDRJPackage.Where(x => x.SubmissionMatchCount > 1).ToList())
					{
						wdrjRequest.PotentialSubmissions =
							_dbSubmission.JurisdictionalData
							.Where(x => x.JurisdictionId == wdrjRequest.Jurisdiction && x.Submission.OrgIGTMaterial == wdrjRequest.Material && x.Submission.Type != SubmissionType.EE.ToString())
							.Select(x =>
								new PotentialSubmission
								{
									Id = x.Submission.Id,
									FileNumber = x.Submission.FileNumber,
									IdNumber = x.Submission.IdNumber,
									GameName = x.Submission.GameName,
									Version = x.Submission.Version,
									DateCode = x.Submission.DateCode,
									JurisdictionStatus = x.Status
								})
							.ToList();
					}

					header.Children.AddRange(WDRJPackage.OrderBy(x => x.Material).ThenBy(x => x.Jurisdiction).ToList());

					result.Add(header);
				}
			}

			result = result.OrderByDescending(x => x.InsertDate).Take(10).ToList();

			return result;
		}
		private List<SelectListItem> BuildWDRJReasonList()
		{
			return
				_dbSubmission.tbl_lst_WDRJReason
				.Select(reason => new SelectListItem
				{
					Text = reason.Reason,
					Value = reason.Id.ToString()
				})
				.ToList();
		}
		#endregion

		#region Queries
		private IQueryable<trIGTTemporaryTable> BuildStagedNewSubmissionQuery(StageFilter filter)
		{
			var query =
					_dbSubmission.trIGTTemporaryTables
					.Where(x => x.Active == 1 && x.Status == FlatFileStatus.PND.ToString() || x.Status == null);

			if (filter.ViewMyPending)
			{
				query.Where(x => x.LoginID == _userContext.User.Id);
			}

			return query;
		}
		private List<StagedFollowupRequest> BuildStagedTransferRequestQuery()
		{
			var result = new List<StagedFollowupRequest>();

			var transferRequestQuery = new StringBuilder();
			transferRequestQuery.Append(@"SELECT ");
			transferRequestQuery.Append(@"submission.Filenumberstr as FileNumber ");
			transferRequestQuery.Append(@",submission.idnumber as IdNumber ");
			transferRequestQuery.Append(@",submission.version as Version ");
			transferRequestQuery.Append(@",submission.gamename as GameName ");
			transferRequestQuery.Append(@",submission.datecode as DateCode ");
			transferRequestQuery.Append(@",dataEntryOperator.fname + ' ' + dataEntryOperator.lname as UserName ");
			transferRequestQuery.Append(@",dataEntryOperator.loginId as UserId ");
			transferRequestQuery.Append(@",IGTTempTable.Id as Id ");
			transferRequestQuery.Append(@",IGTTempTable.Material as Material ");
			transferRequestQuery.Append(@",IGTTempTable.Material_Description as MaterialDescription ");
			transferRequestQuery.Append(@",jurisdictionList.Jurisdiction_ID as Jurisdiction ");
			transferRequestQuery.Append(@",jurisdictionList.Jurisdiction as JurisdictionDescription ");
			transferRequestQuery.Append(@",IGTTempTable.Status as Status ");
			transferRequestQuery.Append(@",IGTTempTable.Letter_Number as LetterNumber ");
			transferRequestQuery.Append(@",IGTTempTable.Project as ProjectId ");
			transferRequestQuery.Append(@",IGTTempTable.Network as BillingReference ");
			transferRequestQuery.Append(@",IGTTempTable.ReceivedDate ");
			transferRequestQuery.Append(@",IGTTempTable.RequestDate ");
			transferRequestQuery.Append(@",IGTTempTable.CloseDate ");
			transferRequestQuery.Append(@",IGTTempTable.InsertDate ");
			transferRequestQuery.Append(@",IGTTempTable.keytbl as SubmissionId ");
			transferRequestQuery.Append(@",IGTTempTable.RJWD_note as RJWDNote ");
			transferRequestQuery.Append(@",IGTTempTable.manufacturer as ManufacturerCode ");
			transferRequestQuery.Append(@",IGTTempTable.MappedKeytbl as MappedSubmissionId ");
			transferRequestQuery.Append(@",IGTTempTable.PurchaseOrder as PurchaseOrder ");
			transferRequestQuery.Append(@",CASE WHEN IGTTempTable.KeyTbl is null then 1 else 0 END AS AmbiguousFollowupRequest ");
			transferRequestQuery.Append(@",COALESCE(L.Location,submission.test_lab) TestLab ");
			transferRequestQuery.Append(@",(SELECT Count(keytbl) FROM Submissions WHERE Submissions.OrgIGTMaterial = IGTTempTable.Material AND Submissions.submission_type <> 'EE' AND Submissions.status <> 'RJ' AND Submissions.status <> 'WD' AND Submissions.status <> 'TC') as SubmissionMatchCount ");
			transferRequestQuery.Append(@",CASE WHEN IGTTempTable.Network is null then CONVERT(bit, 1) else CONVERT(bit, 0) END AS MissingBillingReference  ");
			transferRequestQuery.Append(@"FROM ");
			transferRequestQuery.Append(@"trIGTTemporaryTable IGTTempTable with(nolock) ");
			transferRequestQuery.Append(@"LEFT JOIN Submissions submission with(nolock) on submission.keytbl = IGTTempTable.keytbl ");
			transferRequestQuery.Append(@"LEFT JOIN tbl_lst_JurisdictionMappings IGTJuriMap ON IGTTempTable.jurisdiction = IGTJuriMap.ExternalJurisdictionId AND IGTJuriMap.FlatFile = 1 AND IGTJuriMap.Active = 1 ");
			transferRequestQuery.Append(@"LEFT JOIN tbl_lst_fileJuris jurisdictionList ON jurisdictionList.jurisdiction_id = IGTJuriMap.Jurisdiction_ID ");
			transferRequestQuery.Append(@"INNER JOIN trLogin dataEntryOperator ON dataEntryOperator.loginId = IGTTempTable.loginid ");
			transferRequestQuery.Append(@"LEFT JOIN tbl_lst_AccountingDefaults ad ON ad.MappingId = IGTJuriMap.MappingId and ad.ForTransfer = 0 ");
			transferRequestQuery.Append(@"LEFT JOIN trLaboratories L ON L.locationId = ad.TestLabId ");
			transferRequestQuery.Append(@"WHERE ");
			transferRequestQuery.Append(@"((IGTTempTable.status = 'XFR') AND IGTTempTable.Active = 1 AND IGTJuriMap.ManufacturerShort_Id = 'IGT' AND IGTTempTable.Jurisdiction_Description not like 'SOUTH AFRICA NATIONAL%') ");
			transferRequestQuery.Append(@"OR ");
			transferRequestQuery.Append(@"((IGTTempTable.status = 'XFR') AND IGTTempTable.Active = 1 AND IGTJuriMap.ManufacturerShort_Id <> 'IGT' AND IGTTempTable.Jurisdiction in ('49', '552', '553')) ");

			var transferPackages = _dbSubmission.Database.GetDbConnection().Query<StagedFollowupRequest>(transferRequestQuery.ToString()).ToList().GroupBy(x => x.LetterNumber).ToList();

			if (transferPackages.Count > 0)
			{
				foreach (var transferPackage in transferPackages)
				{
					var header = transferPackage.Select(x => new StagedFollowupRequest
					{
						LetterNumber = transferPackage.Key,
						UserId = transferPackage.Max(y => y.UserId),
						UserName = transferPackage.Max(y => y.UserName),
						InsertDate = transferPackage.Max(y => y.InsertDate)
					})
					.First();

					foreach (var transferRequest in transferPackage.Where(x => x.SubmissionMatchCount >= 1).ToList())
					{
						transferRequest.PotentialSubmissions =
							_dbSubmission.Submissions
							.Where(x => x.OrgIGTMaterial == transferRequest.Material && x.Type != SubmissionType.EE.ToString() && (x.Status != SubmissionStatus.RJ.ToString() && x.Status != SubmissionStatus.WD.ToString() && x.Status != SubmissionStatus.TC.ToString()))
							.Select(x =>
								new PotentialSubmission
								{
									Id = x.Id,
									FileNumber = x.FileNumber,
									IdNumber = x.IdNumber,
									GameName = x.GameName,
									Version = x.Version,
									DateCode = x.DateCode
								})
							.ToList();
					}

					header.Children.AddRange(transferPackage.OrderBy(x => x.Material).ThenBy(x => x.Jurisdiction).ToList());
					result.Add(header);
				}
			}

			return result.OrderBy(x => x.InsertDate).ToList();
		}
		private IEnumerable<StagedFollowupRequest> WDRJRequestQuery(FollowupRequestStatus status)
		{
			var WDRJRequestQuery = new StringBuilder();

			switch (status)
			{
				case FollowupRequestStatus.STAGED:
					WDRJRequestQuery.Append(@"SELECT ");
					WDRJRequestQuery.Append(@"submission.Filenumberstr as FileNumber ");
					WDRJRequestQuery.Append(@",submission.idnumber as IdNumber ");
					WDRJRequestQuery.Append(@",submission.version as Version ");
					WDRJRequestQuery.Append(@",submission.gamename as GameName ");
					WDRJRequestQuery.Append(@",submission.datecode as DateCode ");
					WDRJRequestQuery.Append(@",dataEntryOperator.fname + ' ' + dataEntryOperator.lname as UserName ");
					WDRJRequestQuery.Append(@",dataEntryOperator.loginId as UserId ");
					WDRJRequestQuery.Append(@",IGTTempTable.Id as Id ");
					WDRJRequestQuery.Append(@",IGTTempTable.Material as Material ");
					WDRJRequestQuery.Append(@",IGTTempTable.Material_Description as MaterialDescription ");
					WDRJRequestQuery.Append(@",jurisdictionList.Jurisdiction_ID as Jurisdiction ");
					WDRJRequestQuery.Append(@",jurisdictionList.Jurisdiction as JurisdictionDescription ");
					WDRJRequestQuery.Append(@",IGTTempTable.Status as Status ");
					WDRJRequestQuery.Append(@",IGTTempTable.Letter_Number as LetterNumber ");
					WDRJRequestQuery.Append(@",IGTTempTable.Project as ProjectId ");
					WDRJRequestQuery.Append(@",IGTTempTable.Network as BillingReference ");
					WDRJRequestQuery.Append(@",IGTTempTable.ReceivedDate ");
					WDRJRequestQuery.Append(@",IGTTempTable.RequestDate ");
					WDRJRequestQuery.Append(@",IGTTempTable.CloseDate ");
					WDRJRequestQuery.Append(@",IGTTempTable.InsertDate ");
					WDRJRequestQuery.Append(@",IGTTempTable.keytbl as SubmissionId ");
					WDRJRequestQuery.Append(@",IGTTempTable.RJWD_note as RJWDNote ");
					WDRJRequestQuery.Append(@",IGTTempTable.manufacturer as ManufacturerCode ");
					WDRJRequestQuery.Append(@",IGTTempTable.MappedKeytbl as MappedSubmissionId ");
					WDRJRequestQuery.Append(@",CASE WHEN IGTTempTable.KeyTbl is null then 1 else 0 END AS AmbiguousFollowupRequest ");
					WDRJRequestQuery.Append(@",COALESCE(L.Location,submission.test_lab) TestLab ");
					WDRJRequestQuery.Append(@",(SELECT Count(keytbl) FROM Submissions WHERE Submissions.OrgIGTMaterial = IGTTempTable.Material AND Submissions.submission_type <> 'EE') as SubmissionMatchCount ");
					WDRJRequestQuery.Append(@",CASE WHEN IGTTempTable.Network is null then CONVERT(bit, 1) else CONVERT(bit, 0) END AS MissingBillingReference  ");
					WDRJRequestQuery.Append(@"FROM ");
					WDRJRequestQuery.Append(@"trIGTTemporaryTable IGTTempTable with(nolock) ");
					WDRJRequestQuery.Append(@"LEFT JOIN Submissions submission with(nolock) on submission.keytbl = IGTTempTable.keytbl ");
					WDRJRequestQuery.Append(@"LEFT JOIN tbl_lst_JurisdictionMappings IGTJuriMap ON IGTTempTable.jurisdiction = IGTJuriMap.ExternalJurisdictionId AND IGTJuriMap.FlatFile = 1 AND IGTJuriMap.Active = 1 ");
					WDRJRequestQuery.Append(@"LEFT JOIN tbl_lst_fileJuris jurisdictionList ON jurisdictionList.jurisdiction_id = IGTJuriMap.Jurisdiction_ID ");
					WDRJRequestQuery.Append(@"INNER JOIN trLogin dataEntryOperator ON dataEntryOperator.loginId = IGTTempTable.loginid ");
					WDRJRequestQuery.Append(@"LEFT JOIN tbl_lst_AccountingDefaults ad ON ad.MappingId = IGTJuriMap.MappingId and ad.ForTransfer = 0 ");
					WDRJRequestQuery.Append(@"LEFT JOIN trLaboratories L ON L.locationId = ad.TestLabId ");
					WDRJRequestQuery.Append(@"WHERE ");
					WDRJRequestQuery.Append(@"((IGTTempTable.status = 'WDR') AND IGTTempTable.Active = 1 AND IGTJuriMap.ManufacturerShort_Id = 'IGT' AND IGTTempTable.Jurisdiction_Description not like 'SOUTH AFRICA NATIONAL%')");
					WDRJRequestQuery.Append(@"OR ");
					WDRJRequestQuery.Append(@"((IGTTempTable.status = 'WDR') AND IGTTempTable.Active = 1 AND IGTJuriMap.ManufacturerShort_Id <> 'IGT' AND IGTTempTable.Jurisdiction in ('49', '552', '553')) ");
					break;
				case FollowupRequestStatus.COMPLETED:
					WDRJRequestQuery.Append(@"SELECT ");
					WDRJRequestQuery.Append(@"IGTTempTable.Letter_Number as LetterNumber ");
					WDRJRequestQuery.Append(@",dataEntryOperator.fname + ' ' + dataEntryOperator.lname as UserName ");
					WDRJRequestQuery.Append(@",IGTTempTable.InsertDate ");
					WDRJRequestQuery.Append(@",IGTTempTable.Material as Material ");
					WDRJRequestQuery.Append(@",submission.Filenumberstr as FileNumber ");
					WDRJRequestQuery.Append(@",jurisdictionList.Jurisdiction_ID as Jurisdiction ");
					WDRJRequestQuery.Append(@",jurisdictionList.Jurisdiction as JurisdictionDescription ");
					WDRJRequestQuery.Append(@"FROM trIGTTemporaryTable IGTTempTable with(nolock) ");
					WDRJRequestQuery.Append(@"LEFT JOIN Submissions submission with(nolock) on submission.keytbl = IGTTempTable.keytbl ");
					WDRJRequestQuery.Append(@"LEFT JOIN tbl_lst_JurisdictionMappings IGTJuriMap ON IGTTempTable.jurisdiction = IGTJuriMap.ExternalJurisdictionId AND IGTJuriMap.FlatFile = 1 AND IGTJuriMap.Active = 1 ");
					WDRJRequestQuery.Append(@"LEFT JOIN tbl_lst_fileJuris jurisdictionList ON jurisdictionList.jurisdiction_id = IGTJuriMap.Jurisdiction_ID ");
					WDRJRequestQuery.Append(@"INNER JOIN trLogin dataEntryOperator ON dataEntryOperator.loginId = IGTTempTable.loginid ");
					WDRJRequestQuery.Append(@"WHERE(IGTTempTable.status = 'WDR') AND IGTTempTable.Active = 0 AND IGTTempTable.InsertDate > GETDATE() - 2 And IGTTempTable.Primary_ IS NOT NULL AND IGTJuriMap.ManufacturerShort_Id = 'IGT' ");
					break;
				default:
					break;
			}

			return _dbSubmission.Database.GetDbConnection().Query<StagedFollowupRequest>(WDRJRequestQuery.ToString()).AsEnumerable();
		}
		#endregion

		#region Parse
		/// <summary>
		/// Parse the standard columns of the Flat File to a trIGTTemporaryTable entity
		/// </summary>
		/// <param name="IGTTempTableRow">IGTTempTableRow Entity</param>
		/// <param name="row">Data table row</param>

		private void ProcessColumns(DataTable dt, trIGTTemporaryTable IGTTempTableRow, DataRow row)
		{
			IGTTempTableRow.Letter_Number = ProcessLetterNumber(row);
			IGTTempTableRow.Material = ProcessMaterial(row);

			try
			{
				IGTTempTableRow.Jurisdiction = ProcessJurisdiction(row);
			}
			catch (Exception ex)
			{
				_errorMessage += IGTTempTableRow.Letter_Number + ":" + ex.Message.ToString() + "<BR>";
			}

			IGTTempTableRow.Status = ProcessStatus(row);
			IGTTempTableRow.MasterBillingProject = ProcessMasterBillingProject(row);
			IGTTempTableRow.RJWD_note = ProcessRjWdNote(row);
			IGTTempTableRow.SubmissionNotes = ProcessSubmissionNotes(row);
			IGTTempTableRow.CloseDate = ProcessCloseDate(row);
			if (IGTTempTableRow.RequestDate == null)
			{
				IGTTempTableRow.RequestDate = ProcessRequestDate(row);
			}
			if (IGTTempTableRow.ReceivedDate == null)
			{
				IGTTempTableRow.ReceivedDate = ProcessReceiveDate(row);
			}
			IGTTempTableRow.BusinessPriorityDate = ProcessBusinessPrioityDate(row);
			IGTTempTableRow.Employee_Name = ProcessEmployeeName(row);
			IGTTempTableRow.FollowUpNotes = ProcessFollowUpNotes(row);
			IGTTempTableRow.Hot = ProcessHot(row);
			IGTTempTableRow.Jurisdiction_Description = ProcessJurisdictionDescription(row);
			IGTTempTableRow.Material_Description = ProcessMaterialDescription(row);
			IGTTempTableRow.BillingReference = ProcessNetwork(row);
			IGTTempTableRow.Project = ProcessProject(row);
			IGTTempTableRow.PurchaseOrder = ProcessSubmittedByDepartment(row);
			IGTTempTableRow.SubType = ProcessSubType(row);
			IGTTempTableRow.Primaryjur = ProcessPrimaryJurisdiction(IGTTempTableRow.PurchaseOrder, IGTTempTableRow.Material, IGTTempTableRow.Status, IGTTempTableRow.Letter_Number, dt).ToString();
			IGTTempTableRow.Primaryjur = IGTTempTableRow.Primaryjur.Length == 1 ? "0" + IGTTempTableRow.Primaryjur : IGTTempTableRow.Primaryjur;
			IGTTempTableRow.OrgIGTMaterial = ProcessOriginalIGTMaterial(row);
			IGTTempTableRow.ProductCategory = ProcessProductCategory(row);
		}

		private string ProcessOriginalIGTMaterial(DataRow row)
		{
			return row["OriginalIGTMaterial"].ToString();
		}

		private string ProcessProductCategory(DataRow row)
		{
			return row["Product Category"].ToString();
		}

		private int ProcessPrimaryJurisdiction(string billingParty, string idNumber, string status, string letterNumber, DataTable dt)
		{
			int primaryJurisdiction = 0;
			int current = 0;

			IReadOnlyDictionary<int, int> jurisdictionPrecedence = GetPrecedenceTable(billingParty, idNumber);
			Dictionary<string, int> jurisdictions = ExtractJurisdictions(idNumber, status, letterNumber, billingParty, dt);

			current = CheckForExplicitPrecedence(jurisdictionPrecedence, jurisdictions);

			if (current == 0)
			{
				primaryJurisdiction = jurisdictions.OrderBy(x => x.Key).First().Value;
			}
			else
			{
				primaryJurisdiction = jurisdictionPrecedence.Where(x => x.Value == current).First().Key;
			}

			return primaryJurisdiction;
		}

		private IReadOnlyDictionary<int, int> GetPrecedenceTable(string billingParty, string idNumber)
		{
			var iGamingBillingParties = _dbSubmission.tbl_lst_BillingParties.Where(x => x.TestingTypeId == (int)TestingType.iGaming).Select(x => x.BillingParty).ToList();

			int testingType = iGamingBillingParties.Contains(billingParty) ? 2 :
					billingParty == "Sports - Testing" ? 9 : 1;

			IReadOnlyDictionary<int, int> jurisdictionPrecedence = testingType == 2 && idNumber.StartsWith("200") ? _iGaming200ProgramsJurisdictionPrecedence :
																	testingType == 2 && idNumber.StartsWith("300") ? _iGaming300ProgramsJurisdictionPrecedence :
																	testingType == 9 ? _landBasedJurisdictionPrecedence : _landBasedJurisdictionPrecedence;

			return jurisdictionPrecedence;
		}

		private Dictionary<string, int> ExtractJurisdictions(string idNumber, string status, string letterNumber, string billingParty, DataTable dt)
		{
			Dictionary<string, int> jurisdictions = new Dictionary<string, int>();

			foreach (DataRow r in dt.Rows)
			{
				if (RemoveTerminalStars(r["MATERIAL"].ToString().Trim()) == idNumber && r["Status"].ToString().Trim() == status && r["LETTER NUMBER"].ToString() == letterNumber)
				{
					string gliJurisdictionId = MapJurisdictions(Convert.ToInt32(r["JURISDICTION"]), billingParty);

					jurisdictions.Add(r["JURISDICTION DESCRIPTION"].ToString().Trim(), Convert.ToInt32(gliJurisdictionId));
				}
			}

			return jurisdictions;
		}

		private string MapJurisdictions(int igtJurisdictionId, string billingParty)
		{
			if (billingParty == "Sports - Testing" || billingParty == "Sports - Transfers")
			{
				return _dbSubmission.tbl_lst_JurisdictionMappings
					.Where(x => x.ManufacturerCode == "IGT" && x.ExternalJurisdictionId == igtJurisdictionId.ToString()).Select(x => x.SportsJurisdictionId ?? x.JurisdictionId).FirstOrDefault();
			}
			else
			{
				return _dbSubmission.tbl_lst_JurisdictionMappings
					.Where(x => x.ManufacturerCode == "IGT" && x.ExternalJurisdictionId == igtJurisdictionId.ToString()).Select(x => x.JurisdictionId).FirstOrDefault();
			}
		}

		private int CheckForExplicitPrecedence(IReadOnlyDictionary<int, int> jurisdictionPrecedence, Dictionary<string, int> jurisdictions)
		{
			int value = 0;
			int current = 0;

			foreach (var j in jurisdictions)
			{
				if (jurisdictionPrecedence.TryGetValue(j.Value, out value))
				{
					current = current == 0 ? value : value < current ? value : current;
				}
			}
			return current;
		}

		private string ProcessSubType(DataRow row)
		{
			var iGamingBillingParties = _dbSubmission.tbl_lst_BillingParties.Where(x => x.TestingTypeId == (int)TestingType.iGaming).Select(x => x.BillingParty).ToList();

			if (!row["SubType"].ToString().IsNullOrEmpty())
			{
				return row["SubType"].ToString().Trim();
			}

			string submittingParty = row["Invoice To"].ToString();
			string material = row["MATERIAL"].ToString().Trim();
			string jurisdiction = row["JURISDICTION"].ToString().Trim().TrimStart('0');

			return (iGamingBillingParties.Contains(submittingParty) && (material.StartsWith("IGTiOn")) || material.StartsWith("IGTION")) ? "SY" :
					(material.Contains("Margin Maker") || material.Contains("QBMM")) ? "SY" :
					submittingParty == "Casino Management" ? "SY" :
					submittingParty == "Casino Mgmt Systems" ? "SY" :
					submittingParty == "Central Montoring" ? "SY" :
					submittingParty == "NAGI Transfers for Casino Systems" ? "SY" :
					submittingParty == "Sports - Testing" ? "SY" :
					submittingParty == "Lottery" ? "RN" :
					submittingParty == "Pre-Certification" ? "PC" :
					material.StartsWith("964") ? "PA" :
					jurisdiction == "108" ? "MA" : "MO";
		}

		private string ProcessSubmittedByDepartment(DataRow row)
		{
			if (row.Table.Columns.IndexOf("Invoice To") > 0)
			{
				return String.IsNullOrWhiteSpace(row["Invoice To"].ToString().Trim()) ? null : row["Invoice To"].ToString().Trim();
			}
			else
			{
				return "";
			}
		}

		private DateTime? ProcessBusinessPrioityDate(DataRow row)
		{
			if (row.Table.Columns.IndexOf("BUSINESS PRIORITY DATE") > 0)
			{
				DateTime.TryParse(row["BUSINESS PRIORITY DATE"].ToString().Trim(), out DateTime dateValue);

				if (dateValue != DateTime.MinValue)
				{
					return dateValue;
				}
				else
				{
					return null;
				}
			}
			else
			{
				return null;
			}
		}

		private string ProcessMaterial(DataRow row)
		{
			string material = row["MATERIAL"].ToString().Trim();

			return RemoveTerminalStars(material);
		}

		private string ProcessJurisdiction(DataRow row)
		{
			var jurisdiction = String.IsNullOrWhiteSpace(row["JURISDICTION"].ToString().Trim().TrimStart('0')) ? null : row["JURISDICTION"].ToString().Trim().TrimStart('0');

			if (CheckJurisidctionValidty(jurisdiction))
			{
				return jurisdiction;
			}
			else
			{
				throw new Exception("Jurisdiction mapping for jurisdiction " + jurisdiction + " was not found");
			}
		}

		private bool CheckJurisidctionValidty(string jurisdiction)
		{
			return _dbSubmission.tbl_lst_JurisdictionMappings.Select(x => x)
				.Where(x => x.ManufacturerCode == "IGT").ToList()
				.Where(x => x.ExternalJurisdictionId == jurisdiction).Any();
		}

		private string ProcessLetterNumber(DataRow row)
		{
			return String.IsNullOrWhiteSpace(row["LETTER NUMBER"].ToString().Trim()) &&
				!Enum.GetNames(typeof(FlatFileStatus)).Contains(row["STATUS"].ToString().ToUpper().Trim()) ? "MA_" + DateTime.Now.ToString().Replace(" ", "_") :
				row["LETTER NUMBER"].ToString().Trim();
		}

		private string ProcessStatus(DataRow row)
		{
			return String.IsNullOrWhiteSpace(row["LETTER NUMBER"].ToString().Trim()) &&
								!Enum.GetNames(typeof(FlatFileStatus)).Contains(row["STATUS"].ToString().ToUpper().Trim()) ? "PND" : row["STATUS"].ToString().ToUpper().Trim();
		}

		private string ProcessMasterBillingProject(DataRow row)
		{
			if (row.Table.Columns.IndexOf("MASTER BILLING PROJECT") > 0)
			{
				return String.IsNullOrWhiteSpace(row["MASTER BILLING PROJECT"].ToString().Trim()) ? null : row["MASTER BILLING PROJECT"].ToString().Trim();
			}
			else
			{
				return "";
			}
		}

		private string ProcessRjWdNote(DataRow row)
		{
			if (row.Table.Columns.IndexOf("RJWD_note") > 0)
			{
				return String.IsNullOrWhiteSpace(row["RJWD_NOTE"].ToString().Trim()) ? null : row["RJWD_NOTE"].ToString().Trim();
			}
			else
			{
				return "";
			}
		}

		private string ProcessSubmissionNotes(DataRow row)
		{
			if (row.Table.Columns.IndexOf("SUBMISSION NOTES") > 0)
			{
				return String.IsNullOrWhiteSpace(row["SUBMISSION NOTES"].ToString().Trim()) ? null : row["SUBMISSION NOTES"].ToString().Trim();
			}
			else
			{
				return "";
			}
		}

		private DateTime? ProcessCloseDate(DataRow row)
		{
			if (row.Table.Columns.IndexOf("CLOSEDATE") > 0)
			{
				DateTime.TryParse(row["CLOSEDATE"].ToString().Trim(), out DateTime dateValue);

				if (dateValue != DateTime.MinValue)
				{
					return dateValue;
				}
				else
				{
					return null;
				}
			}
			else
			{
				return null;
			}
		}

		private DateTime? ProcessRequestDate(DataRow row)
		{
			if (row.Table.Columns.IndexOf("REQUESTDATE") > 0)
			{
				DateTime.TryParse(row["REQUESTDATE"].ToString().Trim(), out DateTime dateValue);

				if (dateValue != DateTime.MinValue)
				{
					return dateValue;
				}
				else
				{
					return null;
				}
			}
			else
			{
				return null;
			}
		}

		private DateTime? ProcessReceiveDate(DataRow row)
		{
			if (row.Table.Columns.IndexOf("RECIEVEDDATE") > 0)
			{
				DateTime.TryParse(row["RECIEVEDDATE"].ToString().Trim(), out DateTime dateValue);

				if (dateValue != DateTime.MinValue)
				{
					return dateValue;
				}
				else
				{
					return null;
				}
			}
			else
			{
				return null;
			}
		}

		private string ProcessEmployeeName(DataRow row)
		{
			if (row.Table.Columns.IndexOf("EMPLOYEE NAME") > 0)
			{
				return String.IsNullOrWhiteSpace(row["EMPLOYEE NAME"].ToString().Trim()) ? null : row["EMPLOYEE NAME"].ToString().Trim();
			}
			else
			{
				return "";
			}
		}

		private string ProcessFollowUpNotes(DataRow row)
		{
			if (row.Table.Columns.IndexOf("FOLLOW UP NOTES") > 0)
			{
				return String.IsNullOrWhiteSpace(row["FOLLOW UP NOTES"].ToString().Trim()) ? null : row["FOLLOW UP NOTES"].ToString().Trim();
			}
			else
			{
				return "";
			}
		}

		private string ProcessHot(DataRow row)
		{
			if (row.Table.Columns.IndexOf("HOT") > 0)
			{
				return String.IsNullOrWhiteSpace(row["HOT"].ToString().Trim()) ? null : row["HOT"].ToString().Trim();
			}
			else
			{
				return "";
			}
		}

		private string ProcessJurisdictionDescription(DataRow row)
		{
			if (row.Table.Columns.IndexOf("JURISDICTION DESCRIPTION") > 0)
			{
				return String.IsNullOrWhiteSpace(row["JURISDICTION DESCRIPTION"].ToString().Trim()) ? null : row["JURISDICTION DESCRIPTION"].ToString().Trim();
			}
			else
			{
				return "";
			}
		}

		private string ProcessMaterialDescription(DataRow row)
		{
			if (row.Table.Columns.IndexOf("MATERIAL DESCRIPTION") > 0)
			{
				return String.IsNullOrWhiteSpace(row["MATERIAL DESCRIPTION"].ToString().Trim()) ? null : row["MATERIAL DESCRIPTION"].ToString().Trim();
			}
			else
			{
				return "";
			}
		}

		private string ProcessNetwork(DataRow row)
		{
			if (row.Table.Columns.IndexOf("NETWORK") > 0)
			{
				return String.IsNullOrWhiteSpace(row["NETWORK"].ToString().Trim()) ? null : row["NETWORK"].ToString().Trim();
			}
			else
			{
				return "";
			}
		}

		private string ProcessProject(DataRow row)
		{
			if (row.Table.Columns.IndexOf("PROJECT") > 0)
			{
				return String.IsNullOrWhiteSpace(row["PROJECT"].ToString().Trim()) ? null : row["PROJECT"].ToString().Trim();
			}
			else
			{
				return "";
			}
		}

		/// <summary>
		/// Parse the 'studio' column of the Flat File to a trIGTTemporaryTable entity. These columns may or may not be part of the flat file.
		/// </summary>
		/// <param name="IGTTempTableRow">IGTTempTableRow Entity</param>
		/// <param name="row">Data table row</param>
		/// <returns>If a studio doesn't exist in our database, return IGT studio. Otherwise, return empty string</returns>
		private string ParseStudio(trIGTTemporaryTable IGTTempTableRow, DataRow row, int? manufGroupStudioId)
		{
			var result = "";
			// -- IGT decided to call the column of the FF 'Component' but what they put in it is Studio.. so yeah theres that.
			if (row.Table.Columns.IndexOf("COMPONENT") > 0 && manufGroupStudioId != null)
			{
				var studio = String.IsNullOrWhiteSpace(row["COMPONENT"].ToString().Trim()) ? null : row["COMPONENT"].ToString().Trim();

				if (!String.IsNullOrWhiteSpace(studio))
				{
					var dbStudio = _dbSubmission.tbl_lst_Studios.SingleOrDefault(x => x.Name.ToLower() == studio.ToLower() && x.ManufacturerGroupId == manufGroupStudioId);

					if (dbStudio == null)
					{
						result = studio;
						IGTTempTableRow.StudioId = null;
					}
					else
					{
						IGTTempTableRow.StudioId = dbStudio.Id;
					}
				}
			}
			return result;
		}

		/// <summary>
		/// Parse the 'Billing Party' column of the Flat File to a trIGTTemporaryTable entity.
		/// </summary>
		/// <param name="IGTTempTableRow"></param>
		/// <param name="row"></param>
		/// <param name="manufGroupStudioId"></param>
		/// <returns>If a billing party doesn't exist in our database, return IGT billing party. Otherwise, return empty string</returns>
		private string ParseBillingParty(trIGTTemporaryTable IGTTempTableRow, DataRow row, int? manufGroupId)
		{
			var result = "";
			if (row.Table.Columns.IndexOf("Invoice To") > 0 && manufGroupId != null)
			{
				var billingParty = String.IsNullOrWhiteSpace(row["Invoice To"].ToString().Trim()) ? null : row["Invoice To"].ToString().Trim();

				if (!String.IsNullOrWhiteSpace(billingParty))
				{
					var dbBillingParty = _dbSubmission.tbl_lst_BillingParties.SingleOrDefault(x => x.BillingParty.ToLower() == billingParty.ToLower() && x.ManufacturerGroupId == manufGroupId);

					if (dbBillingParty == null)
					{
						result = billingParty;
						IGTTempTableRow.BillingPartyId = null;
					}
					else
					{
						IGTTempTableRow.BillingPartyId = dbBillingParty.Id;
					}
				}
			}
			return result;
		}

		/// <summary>
		/// Parse the 'Cabinet' column of the Flat File to a trIGTTemporaryTable entity.
		/// </summary>
		/// <param name="IGTTempTableRow"></param>
		/// <param name="row"></param>
		/// <param name="manufGroupStudioId"></param>
		/// <returns>If a cabinet doesn't exist in our database, return IGT cabinet. Otherwise, return empty string</returns>
		private string ParseCabinet(trIGTTemporaryTable IGTTempTableRow, DataRow row, int? manufGroupId)
		{
			var result = "";
			if (row.Table.Columns.IndexOf("Cabinet(s)") > 0 && manufGroupId != null)
			{
				var cabinet = String.IsNullOrWhiteSpace(row["Cabinet(s)"].ToString().Trim()) ? null : row["Cabinet(s)"].ToString().Trim();

				if (!String.IsNullOrWhiteSpace(cabinet))
				{
					var dbCabinet = _dbSubmission.tbl_lst_Cabinet.SingleOrDefault(x => x.Cabinet.ToLower() == cabinet.ToLower() && x.ManufacturerGroupId == manufGroupId);

					if (dbCabinet == null)
					{
						result = cabinet;
						IGTTempTableRow.CabinetId = null;
					}
					else
					{
						IGTTempTableRow.CabinetId = dbCabinet.Id;
					}
				}
			}
			return result;
		}

		private string ValidateColumns(DataTable dt)
		{
			List<string> IGTColumnNames = new List<string> { "KEY",
															"PRIMARY MATERIAL #",
															"MATERIAL DESCRIPTION",
															"OTHER SUBMISSION MATERIALS",
															"MATH CLONE MATERIAL NUMBER",
															"INVOICE TO",
															"BILLING REFERENCE",
															"PROJECT REQUEST TYPE",
															"PRIMARY JURISDICTION",
															"JURISDICTIONS",
															"HOT/REASON",
															"BUSINESS PRIORITY DATE",
															"PRECERT ITL FILE NUMBER",
															"SPECIAL INSTRUCTIONS",
															"PRODUCT CATEGORY",
															"CABINET(S)"};

			foreach (var s in dt.Columns)
			{
				if (!IGTColumnNames.Contains(s.ToString().ToUpper()))
				{
					_errorMessage = "Column names are incorrect.  Please change the name of this Column: " + s.ToString();
					return "Column names are incorrect";
				}
			}

			return "";
		}

		/// <summary>
		/// Parse the data table into trIGTTemporaryTable entities.
		/// </summary>
		/// <param name="dt">HTTP posted files datatable representation.</param>
		/// <param name="manufacturer">Manufacturer for the submissions.</param>
		/// <returns>A list of trIGTTemporaryTable Entities</returns>
		private List<trIGTTemporaryTable> ParseDataTableToIGTTempTableEntities(DataTable dt, FileImport file, DateTime? spreadSheetDate)
		{
			var result = new List<trIGTTemporaryTable>();
			List<string> badStudios = new List<string>();
			List<string> badBillingParty = new List<string>();
			List<string> badCabinet = new List<string>();

			var manufGroupStudioId = _dbSubmission.ManufacturerGroups
				.Where(x => x.Description.Contains(CustomManufacturer.IGT.ToString()) && x.ManufacturerGroupCategory.Code == ManufacturerGroupCategory.Studios.ToString())
				.Select(x => x.Id)
				.FirstOrDefault();

			var manufGroupId = _dbSubmission.ManufacturerGroups
				.Where(x => x.Description.Contains(file.Manufacturer) && x.ManufacturerGroupCategory.Code == ManufacturerGroupCategory.BillingParty.ToString())
				.Select(x => x.Id)
				.FirstOrDefault();

			var manufGroupCabinetId = _dbSubmission.ManufacturerGroups
				.Where(x => x.Description.Contains(file.Manufacturer) && x.ManufacturerGroupCategory.Code == ManufacturerGroupCategory.Cabinet.ToString())
				.Select(x => x.Id)
				.FirstOrDefault();

			if (_errorMessage == "")
			{
				foreach (DataRow row in dt.Rows)
				{
					var newTempTableRow = new trIGTTemporaryTable
					{
						Manufacturer = file.Manufacturer,
						sYear = DateTime.Now.ToString("yy"),
						Active = 1,
						LoginID = _userContext.User.Id,
						ReceivedDate = spreadSheetDate,
						RequestDate = spreadSheetDate
					};

					ProcessColumns(dt, newTempTableRow, row);

					var studio = ParseStudio(newTempTableRow, row, manufGroupStudioId);
					if (!string.IsNullOrEmpty(studio))
						badStudios.Add(studio);
					var billingParty = ParseBillingParty(newTempTableRow, row, manufGroupId);
					if (!string.IsNullOrEmpty(billingParty))
					{
						badBillingParty.Add(billingParty);
					}
					var cabinet = ParseCabinet(newTempTableRow, row, manufGroupCabinetId);
					if (!string.IsNullOrEmpty(cabinet))
					{
						badCabinet.Add(cabinet);
					}
					result.Add(newTempTableRow);
				}
			}

			if (badStudios.Count > 0 || badBillingParty.Count > 0)
				SendStudioEmail(badStudios.Distinct().ToList(), badBillingParty.Distinct().ToList(), file.NewFileName);

			if (_errorMessage != "")
			{
				result.Clear();
			}

			return result;
		}
		#endregion

		#region Helpers
		/// <summary>
		/// Given the HTTP Posted File stream construct a datatable form the spreadsheet.
		/// </summary>
		/// <param name="fileStream">HTTP posted file.</param>
		/// <returns>Datatable built from HTTP posted excel worksheet</returns>
		private DataTable CreateDataTableFromStream(Stream fileStream)
		{
			var dataTable = new DataTable();

			try
			{
				var workbook = new Workbook(fileStream);
				var worksheet = workbook.Worksheets[0];
				dataTable = worksheet.Cells.ExportDataTableAsString(3, 0, worksheet.Cells.MaxRow + 1, worksheet.Cells.MaxColumn + 1, true);
			}
			catch (Exception ex)
			{
				_logger.LogError(ex, _StreamConversionException);
				throw new Exception(_StreamConversionException);
			}

			return dataTable;
		}

		private DateTime? GetSpreadSheetDate(Stream fileStream)
		{
			var spreadSheetDate = new DateTime?();

			try
			{
				var workbook = new Workbook(fileStream);
				var worksheet = workbook.Worksheets[0];

				var dataCell = worksheet.Cells.ExportDataTableAsString(2, 0, 1, 1).Rows[0].ItemArray[0].ToString();
				spreadSheetDate = ComUtil.GetFirstDateFromString(dataCell);
			}
			catch (Exception ex)
			{
				_logger.LogError(ex, _StreamConversionException);
				throw new Exception(_StreamConversionException);
			}

			return spreadSheetDate;
		}

		/// <summary>
		/// Upload an HTTP posted file to the webserver.
		/// </summary>
		/// <param name="file">HTTP posted file</param>
		/// <returns>Returns the name of the file uploaded if successful, otherwise string.empty</returns>
		private string UploadFile(HttpPostedFileBase file)
		{
			var uploadedFileName = string.Empty;
			var newFile = Path.Combine(_UPLOADPATH, GenerateFlatFileName(Path.GetFileName(file.FileName), true));

			try
			{
				file.SaveAs(newFile);
				uploadedFileName = Path.GetFileName(newFile);
			}
			catch (Exception ex)
			{
				_logger.LogError(ex, _UploadFileException);
			}

			return uploadedFileName;
		}
		/// <summary>
		/// Generate unique filename for flatfile to upload to the web server.
		/// </summary>
		/// <param name="fileName">Current name fo the file to be uploaded.</param>
		/// <returns>New unique filename for file on web server.</returns>
		private string GenerateFlatFileName(string fileName)
		{
			return
				new StringBuilder()
				.Append(_userContext.User.Id)
				.Append("_")
				.Append(Guid.NewGuid())
				.Append("_")
				.Append(fileName)
				.ToString();
		}
		/// <summary>
		/// Generate legecy unique filename for flatfile to upload to the web server.
		/// ToDo: Depricate this in favor of a guid. Needed to maintain legecy support until entire process is moved to GLIInranet
		/// </summary>
		/// <param name="fileName">Current name fo the file to be uploaded.</param>
		/// <returns>New legecy unique filename for file on web server.</returns>
		private string GenerateFlatFileNameLegacy(string fileName)
		{
			var timeStamp = DateTime.Now;
			return
				new StringBuilder()
				.Append(_userContext.User.Id)
				.Append("_")
				.Append(timeStamp.Day)
				.Append("-")
				.Append(timeStamp.Month)
				.Append("-")
				.Append(timeStamp.Year)
				.Append("_")
				.Append(timeStamp.Hour)
				.Append("-")
				.Append(timeStamp.Minute)
				.Append("-")
				.Append(timeStamp.Second)
				.Append("_")
				.Append(fileName)
				.ToString();
		}
		/// <summary>
		/// Legacy router for filename convention.
		/// </summary>
		/// <param name="fileName"></param>
		/// <param name="useLegacyName"></param>
		/// <returns>New file name.</returns>
		private string GenerateFlatFileName(string fileName, bool useLegacyName)
		{
			// prob turn this off once fully moved to new sysyem
			if (useLegacyName)
			{
				return GenerateFlatFileNameLegacy(fileName);
			}
			else
			{
				return GenerateFlatFileName(fileName);
			}
		}

		private string RemoveTerminalStars(string material)
		{
			return String.IsNullOrWhiteSpace(material) ? null :
				material.EndsWith("**W") ? material.Remove(material.Length - 3, 3) + "xxW" :
				material.EndsWith("**") ? material.Remove(material.Length - 2, 2) + "xx" : material;
		}

		private bool SpreadSheetPreviouslyImported(string fileName)
		{
			var alreadyImported = false;

			var previousSpreadSheets = _dbSubmission.trIGTTemporaryTables.Where(x => x.SpreadSheetName.Contains(fileName)).Select(x => x.SpreadSheetName).ToList();
			if (previousSpreadSheets.Count > 0)
			{
				previousSpreadSheets = previousSpreadSheets.Select(x => x.Split('_')[3]).ToList();
				alreadyImported = previousSpreadSheets.Any(x => x == fileName);
			}

			return alreadyImported;
		}
		/// <summary>
		/// Set User context on db saves.
		/// </summary>
		private void SaveChanges()
		{
			_dbSubmission.UserId = _userContext.User.Id;
			_dbSubmission.SaveChanges();
			_dbSubmission.UserId = null;
		}
		private void SeekExistingSubmissions(List<trIGTTemporaryTable> IGTTempTableEntities)
		{
			var materialToSearchFor =
				IGTTempTableEntities
					.Where(x => x.Status == "XFR" || x.Status == "WDR")
					.GroupBy(x => x.Material)
					.ToList();

			foreach (var materialGroup in materialToSearchFor)
			{
				var material = materialGroup.Max(x => x.Material);

				var existingSubmission =
					_dbSubmission.Submissions
					.Where(x => x.OrgIGTMaterial == material && x.Type != "EE" && x.Status != SubmissionStatus.WD.ToString() && x.Status != SubmissionStatus.RJ.ToString())
					.ToList();

				if (existingSubmission.Count == 1)
				{
					materialGroup
					.ToList()
					.ForEach(x => x.KeyTbl = existingSubmission.First().Id);
				}
			}
		}
		private void UpdateExistingPendingRequest(trIGTTemporaryTable previous, trIGTTemporaryTable current)
		{
			previous.BusinessPriorityDate = current.BusinessPriorityDate;
			previous.CertLab = current.CertLab;
			previous.CloseDate = current.CloseDate;
			previous.Company = current.Company;
			previous.ContractType = current.ContractType;
			previous.Currency = current.Currency;
			previous.Employee_Name = current.Employee_Name;
			previous.FollowUpNotes = current.FollowUpNotes;
			previous.Hot = current.Hot;
			previous.InsertDate = current.InsertDate;
			previous.Jurisdiction = current.Jurisdiction;
			previous.Jurisdiction_Description = current.Jurisdiction_Description;
			previous.KeyTbl = current.KeyTbl;
			previous.Letter_Number = current.Letter_Number;
			previous.LoginID = current.LoginID;
			previous.Manufacturer = current.Manufacturer;
			previous.MappedKeytbl = current.MappedKeytbl;
			previous.MasterBillingProject = current.MasterBillingProject;
			previous.Material = current.Material;
			previous.Material_Description = current.Material_Description;
			previous.BillingReference = current.BillingReference;
			previous.NewIDnumber = current.NewIDnumber;
			previous.NewJurisdiction = current.NewJurisdiction;
			previous.NewSubFileNumber = current.NewSubFileNumber;
			previous.NewSubItemExist = current.NewSubItemExist;
			previous.Primaryjur = current.Primaryjur;
			previous.Primary_ = current.Primary_;
			previous.Project = current.Project;
			previous.PurchaseOrder = current.PurchaseOrder;
			previous.ReceivedDate = current.ReceivedDate;
			previous.RequestDate = current.RequestDate;
			previous.RJWD_note = current.RJWD_note;
			previous.SavedBy = current.SavedBy;
			previous.SpreadSheetName = current.SpreadSheetName;
			previous.Status = current.Status;
			previous.SubmissionNotes = current.SubmissionNotes;
			previous.SubType = current.SubType;
			previous.sYear = current.sYear;
			previous.TestLab = current.TestLab;
			previous.TXLab = current.TXLab;
		}
		private void UpdateExistingPendingRequests(List<trIGTTemporaryTable> currentRequests)
		{
			foreach (var letterNumberGroup in currentRequests.GroupBy(x => x.Letter_Number).ToList())
			{
				var activeTempTableRows = _dbSubmission.trIGTTemporaryTables.Where(x => x.Letter_Number == letterNumberGroup.Key && x.Active == 1).ToList();
				var transfers = activeTempTableRows.Where(x => x.Status == "XFR").ToList();
				var withdrawalsAndRejections = activeTempTableRows.Where(x => x.Status == "WDR").ToList();

				var newTransfers = currentRequests.Where(x => x.Status == "XFR").ToList();
				foreach (var newTransfer in newTransfers)
				{
					var match = transfers.Where(x => x.Jurisdiction == newTransfer.Jurisdiction && x.Material == newTransfer.Material).SingleOrDefault();

					if (null != match)
					{
						UpdateExistingPendingRequest(match, newTransfer);
						currentRequests.Remove(newTransfer);
					}
				}

				var newWithdrawalRejections = currentRequests.Where(x => x.Status == "WDR").ToList();
				foreach (var newWithdrawalRejection in newWithdrawalRejections)
				{
					var match = transfers.Where(x => x.Jurisdiction == newWithdrawalRejection.Jurisdiction && x.Material == newWithdrawalRejection.Material).SingleOrDefault();

					if (null != match)
					{
						UpdateExistingPendingRequest(match, newWithdrawalRejection);
						currentRequests.Remove(newWithdrawalRejection);
					}
				}
			}
		}

		private string GetArchiveLocation(int testLabId)
		{
			return _dbSubmission.trLaboratories
				.Where(x => x.Id == testLabId)
				.Select(x => x.ArchiveLocation)
				.SingleOrDefault();
		}

		private void SendStudioEmail(List<string> studios, List<string> billingParties, string filename) //TODO Update with billing parties
		{
			var mailMsg = new MailMessage();
			mailMsg.From = new MailAddress("noreply@gaminglabs.com");
			if (_userContext.User.Environment == Common.Environment.Production)
			{
				mailMsg.To.Add(new MailAddress("m.haugh@gaminglabs.com"));
			}
			else
			{
				mailMsg.To.Add(new MailAddress(_userContext.User.Email));
			}
			if (studios.Count > 0)
			{
				mailMsg.Subject = "FlatFile: Invalid IGT Studios ";
			}
			if (billingParties.Count > 0)
			{
				mailMsg.Subject += "FlatFile: Invalid Billing Party";
			}

			mailMsg.Body += _emailService.RenderTemplate("IGTFlatFileBadColumns", new IGTFlatFileBadColumnsEmail() { Filename = filename, Studios = studios, BillingParties = billingParties });

			_emailService.Send(mailMsg);
		}
		#endregion

		#region SGI gaming
		[System.Web.Http.HttpPost]
		public ContentResult GetRequestsRequiringReview()
		{
			return Content(ComUtil.JsonEncodeCamelCase(BuildRequestsRequiringReviewDataSet(false)), "application/json");
		}

		[System.Web.Http.HttpPost]
		public ContentResult GetCompletedSGISubmissions()
		{
			return Content(ComUtil.JsonEncodeCamelCase(BuildRequestsRequiringReviewDataSet(true)), "application/json");
		}

		public ActionResult GetRequestsRequiringReviewCount()
		{
			return Content(_dbSubmission.ElectronicSubmissionHeaders.Where(x => x.Active).Count().ToString());
		}

		public ActionResult GetSGICompletedCount()
		{
			var date = DateTime.Today.AddDays(-7);
			return Content(_dbSubmission.ElectronicSubmissionHeaders.Where(x => x.ProcessedDate > date).Count().ToString());
		}

		public List<FlatFileClientDTO> BuildRequestsRequiringReviewDataSet(bool completed)
		{
			var result = new List<FlatFileClientDTO>();
			var query = _dbSubmission.ElectronicSubmissionHeaders.AsQueryable();
			var labs = _dbSubmission.trLaboratories.ToList();

			if (completed)
			{
				var date = DateTime.Today.AddDays(-7);
				query = query.Where(x => x.ProcessedDate > date);
			}
			else
			{
				query = query.Where(x => x.Active && x.ProcessedDate == null);
			}

			foreach (var electronicSubmissionHeader in query.ToList())
			{
				var header = new FlatFileClientDTO
				{
					SupplierReferenceNumber = electronicSubmissionHeader.SupplierReferenceNumber,
					ManufacturerCode = electronicSubmissionHeader.ManufacturerCode,
					AddDate = electronicSubmissionHeader.AddDate,
					UserId = electronicSubmissionHeader.AddedById,
					UserName = electronicSubmissionHeader.AddedBy.FName + ' ' + electronicSubmissionHeader.AddedBy.LName,
					Status = electronicSubmissionHeader.ElectronicSubmissionAction.Code,
					Id = electronicSubmissionHeader.Id,
					SubmitterName = electronicSubmissionHeader.SubmitterName,
					SubmitterEmail = electronicSubmissionHeader.SubmitterEmail,
					FileNumber = electronicSubmissionHeader.FileNumber,
					ProcessedDate = electronicSubmissionHeader.ProcessedDate
				};

				foreach (var component in electronicSubmissionHeader.ElectronicSubmissionComponents)
				{
					var electronicSubmissionJurisdictionalDataRequests = component.ElectronicSubmissionJurisdictionalDatas
						.Select(x => new FlatFileClientDTO
						{
							Id = component.Id,
							ManufacturerCode = component.ManufacturerCode,
							IdNumber = component.ComponentIdentifier,
							FileNumber = component.SubmissionRecordId == null ? "" : header.FileNumber,
							GameName = component.ComponentName,
							Jurisdiction = x.InternalJurisdictionId.ToJurisdictionIdString(),
							JurisdictionDescription = x.InternalJurisdictionName,
							CertificationLabId = x.CertLabId,
							CertificationLab = x.CertLabId != null ? labs.Where(y => y.Id == x.CertLabId).Select(y => y.Name).FirstOrDefault() : null,
							CompanyId = x.CompanyId,
							Company = x.Company.Name,
							ContractTypeId = x.ContractId,
							ContractType = x.ContractType.Code,
							CurrencyId = x.CurrencyId,
							Currency = x.Currency.Code,
							TestLabId = x.LabId,
							TestLab = labs.Where(y => y.Id == x.LabId).Select(y => y.Name).FirstOrDefault(),
							Status = x.ElectronicSubmissionAction.Code,
							Function = component.Function == null ? null : component.Function.Code,
							FunctionId = component.FunctionId,
							ElectronicSubmissionJurisdictionalDataId = x.Id,
							SubmissionId = component.SubmissionRecordId == null ? "" : component.SubmissionRecordId.ToString()
						}).ToList();

					header.Children.AddRange(electronicSubmissionJurisdictionalDataRequests);
				}
				result.Add(header);
			}

			return completed ? result.OrderByDescending(x => x.ProcessedDate).ToList() : result.OrderBy(x => x.AddDate).ToList();
		}

		[System.Web.Http.HttpPost]
		public int DeleteComponent(int id)
		{
			_dbSubmission.ElectronicSubmissionJurisdictionalDatas.Where(x => x.ElectronicSubmissionComponentId == id).Delete();
			_dbSubmission.ElectronicSubmissionDocuments.Where(x => x.ElectronicSubmissionComponentId == id).Delete();
			_dbSubmission.ElectronicSubmissionNotes.Where(x => x.ElectronicSubmissionComponentId == id).Delete();
			_dbSubmission.ElectronicSubmissionSignatures.Where(x => x.ElectronicSubmissionComponentId == id).Delete();
			return _dbSubmission.ElectronicSubmissionComponents.Where(x => x.Id == id).Delete();
		}

		[System.Web.Http.HttpPost]
		public void DeactivateHeader(int id)
		{
			var header = _dbSubmission.ElectronicSubmissionHeaders.Find(id);
			header.Active = false;
			_dbSubmission.SaveChanges();
		}
		#endregion
	}

	#region Helper Classes
	/// <summary>
	/// The payload class is used to represent the client side payload sent via ajax for flat file imports.
	/// </summary>
	public class FlatFileMetaData
	{
		public string ManufacturerGroupId { get; set; }
		public string Manufacturer { get; set; }
		public string FileName { get; set; }
	}
	public class StageFilter
	{
		public bool ViewMyPending { get; set; }
	}
	public class SubmissionsFilter
	{
		public string FileNumber { get; set; }
		public string Status { get; set; }
		public string IdNumber { get; set; }
		public string Version { get; set; }
		public string GameName { get; set; }
		public string DateCode { get; set; }
	}
	public class FollowupMapPayload
	{
		public int ElectronicSubmissionId { get; set; }
		public string ElectronicSubmissionType { get; set; }
		public int SubmissionId { get; set; }
		public string LetterNumber { get; set; }
		public bool MapToAll { get; set; }
	}
	#endregion

	public enum FollowupRequestStatus
	{
		COMPLETED,
		STAGED
	}

	public class FileImport
	{
		public Stream FileStream { get; set; }
		public string Manufacturer { get; set; }
		public string NewFileName { get; set; }
	}
}