﻿extern alias ZEFCore;
using GLI.EFCore.Submission;
using Microsoft.EntityFrameworkCore;
using Submissions.Common;
using Submissions.Web.Areas.Submission.Models.RNGDataImport;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ZEFCore::Z.EntityFramework.Plus;

namespace Submissions.Web.Areas.Submission.Controllers
{
	[AuthorizeUser(Permission = Permission.RNGData, HasAccessRight = AccessRight.Read)]
	public class RNGDataImportController : Controller
	{
		private readonly IUserContext _userContext;
		private readonly SubmissionContext _dbSubmission;
		private RNG _RNGItem;
		private RNGConfLvl _ConfLevel;

		/// <summary>
		/// RNG should work based on SubmissionHeaderId
		/// RNG.re_keytbl is basically just selecting the 1st record on the file, but it's pretty meaningless
		/// SubmmissionId is passed around so that we can redirect back to the specific submission detail page
		/// </summary>
		public RNGDataImportController(IUserContext userContext, SubmissionContext dbSubmission)
		{
			_userContext = userContext;
			_dbSubmission = dbSubmission;
		}

		public ActionResult Index(string id, int? submissionId)
		{
			var vm = new RNGDataImportViewModel();
			if (Convert.ToInt32(submissionId) != 0)
			{
				vm.SubmissionId = (int)submissionId;
				id = _dbSubmission.Submissions.Where(x => x.Id == submissionId).Select(x => x.FileNumber).Single();
			}

			vm.SubmissionHeaderId = _dbSubmission.SubmissionHeaders.Where(x => x.FileNumber == id).Select(x => x.Id).Single();

			ConstructRNGDataSet(id, vm);

			return View(vm);
		}

		[AuthorizeUser(Permission = Permission.RNGData, HasAccessRight = AccessRight.Edit)]
		public ActionResult Edit(int Id, string FileNumber, int? submissionId, PageSource? source)
		{
			_RNGItem = _dbSubmission.RNGs.Find(Id);
			var vm = new RNGDataImportViewModel();

			bool nullableBitFixer = false;
			if (_RNGItem.Replacement == true)
			{
				nullableBitFixer = true;
			}
			var confidenceLevelList = new List<ConfidenceLevel>();
			foreach (var item in _RNGItem.ConfidenceLevels)
			{
				confidenceLevelList.Add(new ConfidenceLevel(item.ConfidenceLevel.Trim().Replace(" ", "").Replace("%", "")));
			}

			vm.RNGDataSet.RNGData.Add(new RNGData
			{
				Id = _RNGItem.Id,
				TestCase = _RNGItem.TestCase,
				Draws = _RNGItem.Draws,
				RangeStart = _RNGItem.RangeStart,
				RangeEnd = _RNGItem.RangeEnd,
				Replacement = nullableBitFixer,
				Note = _RNGItem.Notes,
				Selections = _RNGItem.Selections,
				ConfidenceLevels = confidenceLevelList
			});

			vm.Source = source;
			vm.RNGDataSet.FileNumber = FileNumber;
			vm.RNGDataSet.SubmissionHeaderId = _RNGItem.SubmissionHeaderId;
			vm.SubmissionId = Convert.ToInt32(submissionId);

			var cancelUrl = new UrlHelper(this.ControllerContext.RequestContext);
			vm.CancelButtonUrl = cancelUrl.Action("index", "rngdataimport", new { id = FileNumber, submissionId });
			if (source == PageSource.SubmissionDetailPartialView)
			{
				vm.CancelButtonUrl = cancelUrl.Action("detail", "submission", new { id = submissionId });
			}

			return View(vm);
		}

		[HttpPost]
		[AuthorizeUser(Permission = Permission.RNGData, HasAccessRight = AccessRight.Edit)]
		public ActionResult Edit(RNGDataImportViewModel vm, string ConfidenceLevelInfo)
		{
			if (ModelState.IsValid)
			{
				RNGData confidenceLevelsFromEdit = Newtonsoft.Json.JsonConvert.DeserializeObject<RNGData>(ConfidenceLevelInfo);

				foreach (var item in vm.RNGDataSet.RNGData)
				{
					_RNGItem = _dbSubmission.RNGs.Find(item.Id);
					_RNGItem.Draws = item.Draws;
					_RNGItem.TestCase = item.TestCase;
					_RNGItem.Notes = item.Note;
					_RNGItem.RangeStart = item.RangeStart;
					_RNGItem.RangeEnd = item.RangeEnd;
					_RNGItem.Selections = item.Selections;
					_RNGItem.Replacement = item.Replacement;

					_dbSubmission.UserId = _userContext.User.Id;
					_dbSubmission.RNGConfLvls.Where(x => x.RNGId == _RNGItem.Id).Delete();

					foreach (var conf in confidenceLevelsFromEdit.ConfidenceLevels)
					{
						if (conf.Level != null)
						{
							_ConfLevel = new RNGConfLvl();
							_ConfLevel.ConfidenceLevel = conf.Level;
							_RNGItem.ConfidenceLevels.Add(_ConfLevel);
						}
					}

					_dbSubmission.UserId = _userContext.User.Id;
					_dbSubmission.SaveChanges();
				}
			}

			if (vm.Source == PageSource.SubmissionDetailPartialView)
			{
				return RedirectToAction("detail", "submission", new { id = vm.SubmissionId });
			}

			return RedirectToAction("index", new { Id = vm.RNGDataSet.FileNumber, vm.SubmissionId });
		}

		[HttpPost]
		public ActionResult Delete(RNGDataImportViewModel vm)
		{
			var rng = vm.RNGDataSet.RNGData.Single();

			_dbSubmission.RNGConfLvls.Where(x => x.RNGId == rng.Id).Delete();

			var dbRng = new RNG { Id = (int)rng.Id };
			_dbSubmission.Entry(dbRng).State = EntityState.Deleted;
			_dbSubmission.UserId = _userContext.User.Id;
			_dbSubmission.SaveChanges();

			if (vm.Source == PageSource.SubmissionDetailPartialView)
			{
				return RedirectToAction("detail", "submission", new { id = vm.SubmissionId });
			}

			return RedirectToAction("index", new { vm.SubmissionId });
		}

		[HttpPost]
		public ActionResult DeleteBulk(int submissionHeaderId, int submissionId)
		{
			var confidenceLevelIds = _dbSubmission.RNGs
				.Where(x => x.SubmissionHeaderId == submissionHeaderId)
				.SelectMany(x => x.ConfidenceLevels.Select(y => y.Id))
				.ToList();

			var rngIds = _dbSubmission.RNGs
				.Where(x => x.SubmissionHeaderId == submissionHeaderId)
				.Select(x => x.Id)
				.ToList();

			_dbSubmission.RNGConfLvls.Where(x => confidenceLevelIds.Contains(x.Id)).Delete();
			_dbSubmission.RNGs.Where(x => rngIds.Contains(x.Id)).Delete();

			return RedirectToAction("index", new { submissionId });
		}

		#region Helper Methods
		public RNGDataImportViewModel AddDataSet(RNGDataImportViewModel vm)
		{
			if (vm.RNGDataSet.SubmissionHeaderId == 0)
			{
				vm.RNGDataSet.SubmissionHeaderId = _dbSubmission.Submissions.Where(x => x.FileNumber == vm.RNGDataSet.FileNumber).OrderBy(x => x.Id).Select(x => x.SubmissionHeaderId).FirstOrDefault().GetValueOrDefault();
			}
			var deafultKeytbl = _dbSubmission.RNGs
			   .Where(x => x.SubmissionHeaderId == vm.RNGDataSet.SubmissionHeaderId)
			   .OrderBy(x => x.SubmissionId)
			   .Select(x => x.SubmissionId).FirstOrDefault();

			if (deafultKeytbl == 0)
			{
				deafultKeytbl = _dbSubmission.Submissions.Where(x => x.FileNumber == vm.RNGDataSet.FileNumber).OrderBy(x => x.Id).Select(x => x.Id).FirstOrDefault();
			}

			foreach (var item in vm.RNGDataSet.RNGData)
			{
				_RNGItem = new RNG();
				_RNGItem.TestCase = item.TestCase;
				_RNGItem.Draws = item.Draws;
				_RNGItem.Selections = item.Selections;
				_RNGItem.RangeStart = item.RangeStart;
				_RNGItem.RangeEnd = item.RangeEnd;
				_RNGItem.Replacement = item.Replacement;
				_RNGItem.Notes = item.Note;
				foreach (var confidenceLevel in item.ConfidenceLevels)
				{
					_ConfLevel = new RNGConfLvl();
					_ConfLevel.ConfidenceLevel = confidenceLevel.Level;
					_RNGItem.ConfidenceLevels.Add(_ConfLevel);
				}
				_RNGItem.SubmissionHeaderId = vm.RNGDataSet.SubmissionHeaderId;
				_RNGItem.SubmissionId = deafultKeytbl;
				_dbSubmission.RNGs.Add(_RNGItem);
				_dbSubmission.UserId = _userContext.User.Id;
				_dbSubmission.SaveChanges();
				item.Id = _RNGItem.Id;
			}
			return vm;
		}

		public void ConstructRNGDataSet(string fileNumber, RNGDataImportViewModel vm)
		{
			var initialRNGData = _dbSubmission.RNGs.Where(x => x.SubmissionHeader.FileNumber == fileNumber).OrderBy(x => x.SubmissionId);

			var defaultSubmissionId = initialRNGData.Select(x => x.SubmissionId).FirstOrDefault();

			if (defaultSubmissionId == 0)
			{
				defaultSubmissionId = _dbSubmission.Submissions.Where(x => x.FileNumber == fileNumber).OrderBy(x => x.Id).Select(x => x.Id).FirstOrDefault();
			}

			if (vm.SubmissionId == 0)
			{
				vm.SubmissionId = defaultSubmissionId;
			}

			var rawRNGData = initialRNGData.Include(x => x.ConfidenceLevels).Where(x => x.SubmissionId == defaultSubmissionId).ToList();

			vm.RNGDataSet.FileNumber = fileNumber;

			if (rawRNGData.Count > 0)
			{
				vm.RNGDataSet.SubmissionHeaderId = rawRNGData.First().SubmissionHeaderId;
				foreach (var rawRNGItem in rawRNGData)
				{
					bool nullableBitFixer = false;
					if (rawRNGItem.Replacement == true)
					{
						nullableBitFixer = true;
					}

					vm.RNGDataSet.RNGData.Add(new RNGData
					{
						Id = rawRNGItem.Id,
						TestCase = rawRNGItem.TestCase,
						Draws = rawRNGItem.Draws,
						RangeStart = rawRNGItem.RangeStart,
						RangeEnd = rawRNGItem.RangeEnd,
						Replacement = nullableBitFixer,
						Note = rawRNGItem.Notes,
						Selections = rawRNGItem.Selections,
						ConfidenceLevels = rawRNGItem.ConfidenceLevels.Select(x => new ConfidenceLevel(x.ConfidenceLevel)).ToList()
					});
				}
			}
		}
		#endregion

		#region Ajax Posts
		[HttpPost]
		public JsonResult ImportDataSet(HttpPostedFileBase file, int submissionHeaderId, string fileNumber)
		{
			if (file != null && file.ContentLength > 0)
			{
				var applicationRoot = Server.MapPath("~");
				var Temp = "Temp";
				var fileName = Path.GetFileName(file.FileName);
				var pathToTemp = Path.Combine(applicationRoot, Temp);
				var fullPath = Path.Combine(pathToTemp, fileName);

				try
				{
					file.SaveAs(fullPath);
					RNGDataImportViewModel ImportedDataSet = Newtonsoft.Json.JsonConvert.DeserializeObject<RNGDataImportViewModel>(System.IO.File.ReadAllText(fullPath));
					ImportedDataSet.RNGDataSet.SubmissionHeaderId = submissionHeaderId;
					if (ImportedDataSet.RNGDataSet.SubmissionHeaderId == 0)
					{
						ImportedDataSet.RNGDataSet.SubmissionHeaderId = _dbSubmission.Submissions.Where(x => x.FileNumber == ImportedDataSet.RNGDataSet.FileNumber).Select(x => x.SubmissionHeaderId).FirstOrDefault().GetValueOrDefault();
					}

					System.IO.File.Delete(fullPath);
					if (String.IsNullOrWhiteSpace(fileNumber) || fileNumber != ImportedDataSet.RNGDataSet.FileNumber)
					{
						Response.StatusCode = (int)HttpStatusCode.BadRequest;
						return Json(new RNGErrorResult
						{
							ErrorCode = RNGErrorCodes.FileNumberMisMatch,
							Description = RNGErrorCodes.FileNumberMisMatch.GetEnumDescription()
						});
					}
					return Json(AddDataSet(ImportedDataSet));
				}
				catch (Exception)
				{
					Response.StatusCode = (int)HttpStatusCode.BadRequest;
					return Json(new RNGErrorResult
					{
						ErrorCode = RNGErrorCodes.UploadFail,
						Description = RNGErrorCodes.UploadFail.GetEnumDescription()
					});
				}
				finally
				{
					System.IO.File.Delete(fullPath);
				}
			}
			else
			{
				Response.StatusCode = (int)HttpStatusCode.BadRequest;
				return Json(new RNGErrorResult
				{
					ErrorCode = RNGErrorCodes.EmptyFile,
					Description = RNGErrorCodes.EmptyFile.GetEnumDescription()
				});
			}
		}

		public JsonResult AddSingleRNGItem(string RNGItem, string fileNumber)
		{
			RNGData SingleItem = Newtonsoft.Json.JsonConvert.DeserializeObject<RNGData>(RNGItem);
			var deafultKeytbl = _dbSubmission.RNGs
				.Where(x => x.SubmissionHeaderId == SingleItem.SubmissionHeaderId)
				.OrderBy(x => x.SubmissionId)
				.Select(x => x.SubmissionId).FirstOrDefault();

			if (deafultKeytbl == 0)
			{
				deafultKeytbl = _dbSubmission.Submissions.Where(x => x.FileNumber == fileNumber).OrderBy(x => x.Id).Select(x => x.Id).FirstOrDefault();
			}

			if (SingleItem.SubmissionHeaderId == 0)
			{
				SingleItem.SubmissionHeaderId = _dbSubmission.Submissions.Where(x => x.Id == deafultKeytbl).Select(x => x.SubmissionHeaderId).FirstOrDefault();
			}

			if (SingleItem.SubmissionHeaderId != 0)
			{
				_RNGItem = new RNG();
				_RNGItem.TestCase = SingleItem.TestCase;
				_RNGItem.Draws = SingleItem.Draws;
				_RNGItem.Selections = SingleItem.Selections;
				_RNGItem.RangeStart = SingleItem.RangeStart;
				_RNGItem.RangeEnd = SingleItem.RangeEnd;
				_RNGItem.Replacement = SingleItem.Replacement;
				_RNGItem.Notes = SingleItem.Note;

				foreach (var item in SingleItem.ConfidenceLevels)
				{
					_ConfLevel = new RNGConfLvl();
					_ConfLevel.ConfidenceLevel = item.Level;
					_RNGItem.ConfidenceLevels.Add(_ConfLevel);
				}

				_RNGItem.SubmissionHeaderId = SingleItem.SubmissionHeaderId.GetValueOrDefault();
				_RNGItem.SubmissionId = deafultKeytbl;
				_dbSubmission.RNGs.Add(_RNGItem);

				_dbSubmission.UserId = _userContext.User.Id;
				_dbSubmission.SaveChanges();


				var RNGItemForClient = new RNGData();
				RNGItemForClient.Id = _RNGItem.Id;
				RNGItemForClient.ConfidenceLevels = new List<ConfidenceLevel>();
				foreach (var item in _RNGItem.ConfidenceLevels)
				{
					RNGItemForClient.ConfidenceLevels.Add(new ConfidenceLevel(item.ConfidenceLevel));
				}
				RNGItemForClient.Draws = _RNGItem.Draws;
				RNGItemForClient.Note = _RNGItem.Notes;
				RNGItemForClient.RangeStart = _RNGItem.RangeStart;
				RNGItemForClient.RangeEnd = _RNGItem.RangeEnd;
				RNGItemForClient.Replacement = _RNGItem.Replacement.GetValueOrDefault();
				RNGItemForClient.Selections = _RNGItem.Selections;
				RNGItemForClient.SubmissionHeaderId = _RNGItem.SubmissionHeaderId;
				RNGItemForClient.TestCase = _RNGItem.TestCase;

				return Json(RNGItemForClient);
			}
			else
			{
				return Json("Error");
			}
		}
		#endregion
	}

	public enum RNGErrorCodes
	{
		[Description("File Number Mis-Match: Pleace Check that the File Number of the Json Data matches the File Number you are uploading too. Data set not added!")]
		FileNumberMisMatch,
		[Description("Upload Fail: Data set not added!")]
		UploadFail,
		[Description("Json File Empty: The Json File is empty.")]
		EmptyFile
	}

	public class RNGErrorResult
	{
		public RNGErrorCodes ErrorCode { get; set; }
		public string Description { get; set; }
	}
}