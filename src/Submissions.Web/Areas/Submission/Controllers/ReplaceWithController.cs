﻿using Dapper;
using GLI.EFCore.Submission;
using Microsoft.EntityFrameworkCore;
using Submissions.Web.Areas.Submission.Models.ReplaceWith;
using System;
using System.Linq;
using System.Web.Mvc;

namespace Submissions.Web.Areas.Submission.Controllers
{
	public class ReplaceWithController : Controller
	{
		private readonly SubmissionContext _dbSubmission;

		public ReplaceWithController
		(
			SubmissionContext dbSubmission
		)
		{
			_dbSubmission = dbSubmission;
		}

		public ActionResult History(int jurisdictionDataId)
		{
			var vm = new ReplaceWithHistory();
			vm.currentSelected = jurisdictionDataId;

			var replacingPrimary = _dbSubmission.SubmissionReplaceWiths.Where(x => x.ReplaceWithJurisdictionalDataId == jurisdictionDataId).Select(x => x.ReplaceWithJurisdictionalDataId).FirstOrDefault();
			if (replacingPrimary == null)
			{
				replacingPrimary = _dbSubmission.SubmissionReplaceWiths.Where(x => x.JurisdictionalDataId == jurisdictionDataId).Select(x => x.ReplaceWithJurisdictionalDataId).FirstOrDefault();
			}

			if (replacingPrimary != null)
			{
				//Get NEW software
				var result = (from srw in _dbSubmission.SubmissionReplaceWiths
							  where srw.ReplaceWithJurisdictionalDataId == replacingPrimary
							  select new ReplaceWithHistoryItem
							  {
								  ActionOrder = 1,
								  ActionBy = (srw.AddUser != null) ? srw.AddUser.FName + " " + srw.AddUser.LName : "",
								  Action = "Added",
								  FileNumber = srw.ReplaceWithJurisdictionalData.Submission.FileNumber,
								  Jurisdiction = srw.ReplaceWithJurisdictionalData.JurisdictionName,
								  JurisdictionalDataId = srw.ReplaceWithJurisdictionalData.Id,
								  IdNumber = srw.ReplaceWithJurisdictionalData.Submission.IdNumber,
								  DateCode = srw.ReplaceWithJurisdictionalData.Submission.DateCode,
								  Version = srw.ReplaceWithJurisdictionalData.Submission.Version,
								  RevokedDate = srw.ReplaceWithJurisdictionalData.RevokeDate,
								  UpgradeByDate = srw.ReplaceWithJurisdictionalData.UpgradeByDate,
								  ObsoleteDate = srw.ReplaceWithJurisdictionalData.ObsoleteDate,
								  ApprovedDate = srw.ReplaceWithJurisdictionalData.CloseDate,
								  ChangeDate = (DateTime)srw.AddDate
							  }).FirstOrDefault();

				//Get OLD items from NEW software
				var result1 = (from srw in _dbSubmission.SubmissionReplaceWiths
							   where srw.ReplaceWithJurisdictionalDataId == replacingPrimary
							   select new ReplaceWithHistoryItem
							   {
								   ActionOrder = 2,
								   ActionBy = (srw.AddUser != null) ? srw.AddUser.FName + " " + srw.AddUser.LName : "",
								   Action = "Replaced",
								   FileNumber = srw.JurisdictionalData.Submission.FileNumber,
								   Jurisdiction = srw.JurisdictionalData.JurisdictionName,
								   JurisdictionalDataId = (int)srw.JurisdictionalDataId,
								   IdNumber = srw.JurisdictionalData.Submission.IdNumber,
								   DateCode = srw.JurisdictionalData.Submission.DateCode,
								   Version = srw.JurisdictionalData.Submission.Version,
								   RevokedDate = srw.JurisdictionalData.RevokeDate,
								   UpgradeByDate = srw.JurisdictionalData.UpgradeByDate,
								   ObsoleteDate = srw.JurisdictionalData.ObsoleteDate,
								   ApprovedDate = srw.JurisdictionalData.CloseDate,
								   ChangeDate = (DateTime)srw.AddDate
							   }).Distinct().ToList();

				//Get history for new software
				var query = "SELECT DISTINCT 3 AS ActionOrder,";
				query += "l.fname + ' ' + l.lname AS ActionBy,";
				query += "s.filenumberstr As FileNumber,";
				query += "j.revokeddate As RevokedDate,";
				query += "j.upgradeby As UpgradeByDate,";
				query += "j.obsoleteddate As ObsoleteDate,";
				query += "j.jurisdictiondate As ApprovedDate,";
				query += "s.idnumber As IdNumber,";
				query += "s.datecode As DateCode,";
				query += "s.version As Version,";
				query += "j.jurisdictionname As Jurisdiction,";
				query += "j.primary_ As JurisdictionalDataId,";
				query += "CASE WHEN srw.ActionType = 'U' THEN 'Replaced' ELSE 'Deleted' END AS Action,";
				query += "srw.ChangeDate AS ChangeDate";
				query += " FROM GLI_SQL_HistoryLog.dbo.SubmissionReplaceWith_Audit srw JOIN JurisdictionalData j ON srw.re_Primary_ = j.Primary_ ";
				query += " JOIN Submissions s ON s.keytbl = j.re_keytbl LEFT JOIN trLogin l ON srw.CreatedByUserId = l.loginid WHERE srw.replace_with_Primary_ = " + replacingPrimary;

				var result3 = _dbSubmission.Database.GetDbConnection().Query<ReplaceWithHistoryItem>(query).Distinct().ToList();

				result1.Add(result);
				var finalResult = result1.Union(result3).OrderBy(x => x.ActionOrder).ToList();
				vm.ReplaceWithHistoryItems = finalResult;
			}

			return View("History", vm);
		}
	}
}