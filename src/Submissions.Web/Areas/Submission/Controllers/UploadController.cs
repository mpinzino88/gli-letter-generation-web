﻿using Aspose.Cells;
using AutoMapper.QueryableExtensions;
using Dapper;
using GLI.EFCore.Submission;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Submissions.Common;
using Submissions.Core;
using Submissions.Web.Areas.Submission.Models.Submission;
using Submissions.Web.Areas.Submission.Models.Upload;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;

namespace Submissions.Web.Areas.Submission.Controllers
{
	public class UploadController : Controller
	{
		private readonly List<string> _EXCELMIMETYPES = new List<string>(new string[]
		{
			"application/vnd.ms-excel",
			"application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
		});
		private readonly string _UPLOADPATH = ConfigurationManager.AppSettings["FlatfileUploadPath"].ToString();

		private readonly ILogger _logger;
		private readonly IUserContext _userContext;
		private readonly SubmissionContext _dbSubmission;
		private readonly IJurisdictionalDataService _jurisdictionalDataService;
		private readonly ISubmissionService _submissionService;

		public UploadController(IUserContext userContext, ILogger logger, SubmissionContext dbSubmission, IJurisdictionalDataService jurisdictionService, ISubmissionService submissionService)
		{
			_dbSubmission = dbSubmission;
			_logger = logger;
			_jurisdictionalDataService = jurisdictionService;
			_submissionService = submissionService;
			_userContext = userContext;
		}

		// GET: Submission/Upload
		public ActionResult Index(string fileNumber)
		{
			var vm = new IndexViewModel();
			vm.FileNumber = fileNumber;
			vm.Submissions = _dbSubmission.Submissions
				.AsNoTracking()
				.Where(x => x.FileNumber == vm.FileNumber)
				.ProjectTo<SubmissionSelection>()
				.OrderBy(x => x.Id)
				.ToList();

			var submission = _dbSubmission.Submissions
				.Where(x => x.FileNumber == fileNumber)
				.Select(x => new { x.ManufacturerCode, x.TestingTypeId, TestingType = x.TestingType.Description })
				.First();

			vm.ManufactureCode = submission.ManufacturerCode;
			vm.TestingTypeId = submission.TestingTypeId;

			foreach (var val in Enum.GetValues(typeof(UploadFromExcelOptions)))
			{
				vm.UploadExcelOptions.Add(new UploadExcelOption() { Name = val.ToString(), Description = val.GetEnumDescription() });
			}

			//get SigType/Default version data
			var SigTypeDefaults = _dbSubmission.tbl_lst_SignatureTypes
										.Select(x => new SignatureTypeInfo()
										{
											SignatureTypeId = x.Id,
											DefaultVersionId = x.DefaultVersionId,
											DefaultVersion = (x.DefaultVersion != null) ? x.DefaultVersion.Version : ""
										}).ToList();
			vm.SignatureDefaultVersions = SigTypeDefaults;
			vm.CanAdd = Util.HasAccess(Permission.Submission, AccessRight.Add);
			vm.CanEdit = Util.HasAccess(Permission.Submission, AccessRight.Edit);

			var projectStatus = _dbSubmission.trProjects.Where(x => x.ProjectName == fileNumber).Select(x => x.ENStatus).SingleOrDefault();

			var engineerEdit = projectStatus != ProjectStatus.QA.ToString() &&
				projectStatus != ProjectStatus.DN.ToString() &&
				submission.TestingType != TestingType.Sportsbook.ToString();

			vm.CanEditSignatures = vm.CanEdit || (Util.HasAccess(Permission.Signatures, AccessRight.Edit) && engineerEdit);
			vm.CanDeleteSignatures = vm.CanEdit || (Util.HasAccess(Permission.Signatures, AccessRight.Delete) && engineerEdit);

			return View(vm);
		}

		public ActionResult Templates(string fileNumber)
		{
			ViewBag.FileNumber = fileNumber;
			return View();
		}
		#region Post/get from Views for each step/validations

		[System.Web.Http.HttpPost]
		public JsonResult UploadFile([FromBody] string uploadOption)
		{
			var newFileName = "";

			if (Request.Files.Count > 0)
			{
				for (var i = 0; i < Request.Files.Count; i++)
				{
					if (_EXCELMIMETYPES.Contains(Request.Files[i].ContentType))
					{
						newFileName = SaveFile(Request.Files[i]);
					}
				}
			}
			return Json(newFileName);
		}

		public ActionResult ProcessForValidationMapping(string uploadOption, string uploadedFileName, string columnsMapping)
		{
			var uploadedColumnsMapping = JsonConvert.DeserializeObject<List<ExcelColInfo>>(columnsMapping);
			var fileNamePath = Path.Combine(_UPLOADPATH, uploadedFileName);
			var workSheetName = GetWorkSheetName(uploadOption);
			var fileSteam = System.IO.File.OpenRead(fileNamePath);
			var dt = CreateDataTableFromStream(fileSteam);
			fileSteam.Dispose();

			if (dt.Rows.Count > 0 && CheckFileFormat(uploadOption, fileNamePath, uploadedColumnsMapping))
			{
				var Columns = new List<ExcelColInfo>();
				if (uploadedColumnsMapping.Count > 0) // All Columns already validated and sigs mapped
				{
					Columns = uploadedColumnsMapping;
				}
				else
				{
					Columns = MapExcelColumnsAndValidate(dt, fileNamePath, workSheetName, uploadOption);
				}

				//Validate required Column
				var isFileValidated = CheckRequiredColumns(uploadOption, Columns);

				//if error delete file
				if (Columns.Any(x => x.hasInValidData) || isFileValidated == false)
				{
					DeleteFile(fileNamePath);
				}

				if (isFileValidated == false)
				{
					return Content("NoData");
				}
				else
				{
					return Content(ComUtil.JsonEncodeCamelCase(Columns), "application/json");
				}
			}
			else
			{
				return Content("NoData");
			}
		}

		[System.Web.Http.HttpPost]
		public ActionResult ProcessForSignatureValidations(string uploadOption, string uploadedFileName, List<ExcelColInfo> columnsMapping)
		{
			//Check Length
			var fileNamePath = Path.Combine(_UPLOADPATH, uploadedFileName);
			var workSheetName = GetWorkSheetName(uploadOption);
			var allowedSigLength = _dbSubmission.tbl_lst_SignatureTypes.Select(x => new { SignatureTypeId = x.Id, AllowedSigLength = x.Length }).ToList();

			if (uploadOption == UploadFromExcelOptions.ModifySignatures.ToString())
			{
				var notMatchedSigs = ProcessForSignatureValidationForModifications(fileNamePath);
				if (!string.IsNullOrEmpty(notMatchedSigs))
				{
					var sigColumn = columnsMapping.Where(x => x.ExcelColName.ToLower() == "signature").Single();
					sigColumn.IsSignatureColumn = true;
					sigColumn.SignatureMapped = true;
					sigColumn.hasInValidData = true;
					sigColumn.ErrorInfo = notMatchedSigs;
				}
			}
			else
			{
				var allSelectedSigColumns = columnsMapping.Where(x => x.IsSignatureColumn == true).ToList();
				var filePathExists = columnsMapping.Any(x => x.ExcelColName.ToLower() == "filepath");
				var sigErrorInfo = "";

				foreach (var sig in allSelectedSigColumns)
				{
					//check width
					sigErrorInfo = "";
					var allowedLength = allowedSigLength.Where(x => x.SignatureTypeId == sig.SignatureTypeId).Select(x => x.AllowedSigLength).SingleOrDefault();
					var arrAllowedLength = allowedLength.Split('|').ToList();
					var sColLengthWhere = "";
					foreach (var sigLength in arrAllowedLength)
					{
						if (!string.IsNullOrEmpty(sigLength))
						{
							sColLengthWhere = (string.IsNullOrEmpty(sColLengthWhere)) ? " len(a.[" + sig.ExcelColName + "]) <> " + sigLength : sColLengthWhere + " And len(a.[" + sig.ExcelColName + "]) <> " + sigLength;
							sigErrorInfo += (string.IsNullOrEmpty(sigErrorInfo)) ? sigLength : "," + sigLength;
						}
					}

					if (!string.IsNullOrEmpty(sColLengthWhere))
					{
						var query = "SELECT a.[" + sig.ExcelColName + "] As InValidData FROM OPENRowSET('Microsoft.ACE.OLEDB.12.0','Excel 12.0;Database=" + fileNamePath + "','Select * from [" + workSheetName + "$]') a Where len(a.[" + sig.ExcelColName + "]) <> 0 And (" + sColLengthWhere + ")";
						var notMatched = _dbSubmission.Database.GetDbConnection().Query<string>(query).ToList();

						if (notMatched.Count > 0)
						{
							sig.hasInValidData = true;
							sig.ErrorInfo = string.Join(", ", notMatched) + " (Signatures must be " + sigErrorInfo + " characters long)";
						}
					}

					//Check if signature already exists for modify records and Add signatures
					if (uploadOption == UploadFromExcelOptions.ModifySubmissions.ToString() || uploadOption == UploadFromExcelOptions.AddSignatures.ToString())
					{
						var existQuery = "";
						if (filePathExists)
						{
							existQuery = " SELECT distinct a.keytbl ";
							existQuery += " FROM ( Select * from OPENRowSET('Microsoft.ACE.OLEDB.12.0','Excel 12.0;Database=" + fileNamePath + ";IMEX=1','Select * from [" + workSheetName + "$]')) a ";
							existQuery += " join tblSignatures  b ";
							existQuery += " on a.[keytbl]=b.[re_keytbl] and isnull(b.TypeOfID,0)='" + sig.SignatureTypeId + "' and isnull(b.ScopeOFID,0)='" + sig.SignatureScopeId + "' and isnull(b.versionID,0)='" + sig.SignatureVersionId + "' and rtrim(isnull(b.signature,''))=a.[" + sig.ExcelColName + "] and isnull(a.FilePath,'')=isnull(b.sFilePath,'') ";
							existQuery += " Where a.[" + sig.ExcelColName + "] <> '' ";
						}
						else
						{
							existQuery = " SELECT distinct a.keytbl ";
							existQuery += " FROM ( Select * from OPENRowSET('Microsoft.ACE.OLEDB.12.0','Excel 12.0;Database=" + fileNamePath + ";IMEX=1','Select * from [" + workSheetName + "$]')) a ";
							existQuery += " join tblSignatures  b ";
							existQuery += " on a.[keytbl]=b.[re_keytbl] and isnull(b.TypeOfID,0)='" + sig.SignatureTypeId + "' and isnull(b.ScopeOFID,0)='" + sig.SignatureScopeId + "' and isnull(b.versionID,0)='" + sig.SignatureVersionId + "'";
							existQuery += " Where a.[" + sig.ExcelColName + "] <> '' ";
						}
						var alreadyExists = _dbSubmission.Database.GetDbConnection().Query<double>(existQuery).ToList();
						if (alreadyExists.Count > 0)
						{
							sig.SignatureExistsFor = "Signatures already exists for Ids: " + string.Join(", ", alreadyExists);
						}
					}

				}
			}

			//if error delete file
			if (columnsMapping.Any(x => x.hasInValidData))
			{
				DeleteFile(fileNamePath);
			}
			return Content(ComUtil.JsonEncodeCamelCase(columnsMapping), "application/json");
		}

		[System.Web.Http.HttpPost]
		private string ProcessForSignatureValidationForModifications(string uploadedFileName)
		{
			var fileNamePath = Path.Combine(_UPLOADPATH, uploadedFileName);
			var query = "Select convert(int,a.Keytbl) As SubmissionId , a.[Sig Type] As SignatureType, a.Signature As Signature, replace(left(c.SigAllowedLength,len(c.SigAllowedLength)-1),'|', ' or ' ) As ErrorInfo" +
						" from tblSignatures b join (Select * from OPENRowSET('Microsoft.ACE.OLEDB.12.0','Excel 12.0;Database=" + fileNamePath + ";IMEX=1','Select * from [MODSIG$]')) a  on a.keytbl=b.re_keytbl and isnull(a.[Sig Type],'')=isnull(b.TypeOf,'') and isnull(a.[Sig Scope],'')= isnull(b.ScopeOF,'') and isnull(a.[Sig Version],'')=isnull(b.Verifyversion,'') " +
						" join tbl_lst_SignatureTypes c on ltrim(rtrim(a.[Sig Type]))=c.SignatureType Where a.[Signature] <> '' and a.[Signature] <> b.[Signature] and  CHARINDEX(convert(varchar,len(rtrim(replace(a.[Signature],char(9),'')))) + '|',c.SigAllowedLength)= 0 ";

			var sigsLengthNotMatched = _dbSubmission.Database.GetDbConnection().Query<SignatureError>(query).ToList();

			var lstNotMatched = "";
			if (sigsLengthNotMatched.Count > 0)
			{
				lstNotMatched = "<Table border=1 cellspacing='1px' cellpadding='1px'><tr><td>Keytbl</td><td>Signature Type</td><td>Signature</td><td>Allowed Length</td></tr>";
				foreach (var lengthNotMatched in sigsLengthNotMatched)
				{
					lstNotMatched += "<tr><Td>&nbsp;" + lengthNotMatched.SubmissionId + "&nbsp;</Td>";
					lstNotMatched += "<Td>&nbsp;" + lengthNotMatched.SignatureType + "&nbsp;</Td>";
					lstNotMatched += "<Td>&nbsp;" + lengthNotMatched.Signature + "&nbsp;</Td>";
					lstNotMatched += "<Td>&nbsp;" + lengthNotMatched.ErrorInfo + "&nbsp;</Td></tr>";
				}
				lstNotMatched += "</Table>";
			}

			return lstNotMatched;
		}

		#endregion

		#region helper Column validatations Before DB updates
		private bool CheckFileFormat(string uploadOption, string fileNamePath, List<ExcelColInfo> uploadedColumns)
		{
			var isValid = false;

			//Check worksheet Name
			var workSheetName = GetWorkSheetName(uploadOption);
			var query = "SELECT top 1 1 FROM OPENRowSET('Microsoft.ACE.OLEDB.12.0','Excel 12.0;Database=" + fileNamePath + ";IMEX=1','Select * from [" + workSheetName + "$]') a ";
			try
			{
				var result = _dbSubmission.Database.GetDbConnection().ExecuteScalar<int>(query);
				isValid = true;
			}
			catch
			{
				isValid = false;
			}
			return isValid;
		}

		private bool CheckRequiredColumns(string uploadOption, List<ExcelColInfo> uploadedColumns)
		{
			var isValid = false;
			//Check Keytbl exists for options
			var requiredColumns = RequiredColumns(uploadOption);
			if (uploadOption != UploadFromExcelOptions.AddNewSubmissions.ToString())
			{
				if (uploadedColumns.Any(x => requiredColumns.Contains(x.ExcelColName.ToLower())))
				{
					isValid = true;
				}
			}
			else
			{
				isValid = true;
			}

			if (uploadOption == UploadFromExcelOptions.AddSignatures.ToString()) // if no signature columns
			{
				if (!uploadedColumns.Any(x => x.IsSignatureColumn == true))
				{
					isValid = false;
				}
			}

			return isValid;
		}

		private List<ExcelColInfo> MapExcelColumnsAndValidate(DataTable dt, string fileNamePath, string workSheetName, string uploadOption)
		{
			var Columns = new List<ExcelColInfo>();
			//Check All Columns 
			foreach (var column in dt.Columns)
			{
				if (!string.IsNullOrEmpty(column.ToString()))
				{
					if (Columns.Select(x => x.ExcelColName).ToList().Contains(column.ToString()))// if exists
					{
						Columns.Where(x => x.ExcelColName == column.ToString()).First().IsInvalidColumn = true;
						Columns.Where(x => x.ExcelColName == column.ToString()).First().ErrorInfo = "Column Already Exists. Please Remove";
					}
					else
					{
						Columns.Add(new ExcelColInfo() { ExcelColName = column.ToString() });
					}
				}
			}

			//Check if valid columns
			var allValidColumns = _dbSubmission.ExcelImportMaps.Where(x => (x.tfActive == true) && (x.TableName.ToLower() == "submissions" || x.TableName.ToLower() == "tblmemo")).ToList();
			var ignoreValidationsFor = IgnoreColumnValidationsFor(uploadOption).Select(x => x.ToLower()).ToList();
			var requiredColumns = RequiredColumns(uploadOption).Select(x => x.ToLower()).ToList();

			foreach (var column in Columns)
			{
				var validatedColumn = allValidColumns.Where(x => x.ExcelColName.ToLower() == column.ExcelColName.ToLower()).FirstOrDefault();
				if (validatedColumn == null)
				{
					//check if column needs to be ignored
					if (ignoreValidationsFor.Any(x => x.ToLower() == column.ExcelColName.ToLower()) ||
						requiredColumns.Any(x => x.ToLower() == column.ExcelColName.ToLower()))
					{
						column.IsInvalidColumn = false;
						column.TableColName = column.ExcelColName;
						column.TableName = null; // no need to update as this needs to be ignored
						column.ErrorInfo = "Ignore";
					}
					else
					{
						column.IsInvalidColumn = true;
						column.ErrorInfo = "No Mapping Found. Please Check Column Headings.";
					}
				}
				else
				{
					column.TableNameValidate = validatedColumn.CheckTableval;
					column.SpecialDataType = validatedColumn.FieldDataType;
					column.TableColName = validatedColumn.TableColName;
					column.TableName = validatedColumn.TableName;
				}
			}

			//Mark which ones are signatures
			var inValidColumns = Columns.Where(x => x.IsInvalidColumn == true).ToList();
			foreach (var column in inValidColumns)
			{
				if (column.ExcelColName.Contains("/") || column.ExcelColName.Contains("\\"))
				{
					//column.ExcelColName = column.ExcelColName.Replace("\\", "/");
					column.IsSignatureColumn = true;
					column.IsInvalidColumn = false;
				}
			}
			var signatureColumns = Columns.Where(x => x.IsSignatureColumn == true).ToList();
			foreach (var column in Columns)
			{
				if (signatureColumns.Contains(column))
				{
					var sigInfo = (column.ExcelColName).Split('/');
					if (sigInfo.Length == 1)
					{
						sigInfo = (column.ExcelColName).Split('\\');
					}

					//Set all sig default values
					var sigType = sigInfo[0].Trim();
					var sigScope = sigInfo[1].Trim();

					var signature = _dbSubmission.tbl_lst_SignatureTypes.Where(x => x.Type == sigType).Select(x => new { Id = x.Id, DefaultVersionId = x.DefaultVersionId, DefaultVersion = x.DefaultVersion.Version, Type = x.Type }).SingleOrDefault();
					column.SignatureTypeId = (signature != null) ? signature.Id : 0;
					column.SignatureType = (signature != null) ? signature.Type : "";
					column.SignatureVersionId = (signature != null) ? signature.DefaultVersionId : 0;
					column.SignatureVersion = (signature != null) ? signature.DefaultVersion : "";

					var scope = _dbSubmission.tbl_lst_SignatureScope.Where(x => x.Scope == sigScope).Select(x => new { Id = x.Id, Scope = x.Scope }).SingleOrDefault();
					column.SignatureScopeId = (scope != null) ? scope.Id : 0;
					column.SignatureScope = (scope != null) ? scope.Scope : "";

					//this is just while querying sql has # instead of "."
					column.ExcelColName = column.ExcelColName.Replace(".", "#");
				}
			}

			//Check validity of data
			ValidateColumnDataWithMappingTable(Columns, fileNamePath, workSheetName);

			return Columns;
		}

		private void ValidateColumnDataWithMappingTable(List<ExcelColInfo> excelCols, string fileNamePath, string workSheetName)
		{
			var query = "";
			var firstCol = excelCols[0].ExcelColName;
			foreach (var column in excelCols)
			{
				if (column.SpecialDataType != null || column.TableNameValidate != null)
				{
					if (column.SpecialDataType == "Date")
					{
						query = "SELECT distinct a.[" + column.ExcelColName + "] FROM OPENRowSET('Microsoft.ACE.OLEDB.12.0','Excel 12.0;Database=" + fileNamePath + ";IMEX=1','Select * from [" + workSheetName + "$]') a Where isDate(a.[" + column.ExcelColName + "]) = 0 and a.[" + firstCol + "] <> ''";
					}
					else
					{
						var tableName = column.TableNameValidate.Split('.')[0];
						var colName = column.TableNameValidate.Split('.')[1];
						query = "SELECT distinct convert(varchar(450),a.[" + column.ExcelColName + "]) FROM OPENRowSET('Microsoft.ACE.OLEDB.12.0','Excel 12.0;Database=" + fileNamePath + ";IMEX=1','Select * from [" + workSheetName + "$]') a left join " + tableName + " on convert(varchar(450),a.[" + column.ExcelColName + "])=" + column.TableNameValidate + " Where " + column.TableNameValidate + " is null and a.[" + firstCol + "] <> ''";
					}
					var notValidated = _dbSubmission.Database.GetDbConnection().Query<string>(query).ToList();

					if (notValidated.Count > 0)
					{
						var allEmptyValAllowed = false;
						if (column.TableColName.ToLower() == SubmissionColumn.ChipType.ToString().ToLower()
							|| column.TableColName.ToLower() == SubmissionColumn.Game_Type.ToString().ToLower()
							|| column.TableColName.ToLower() == SubmissionColumn.Function.ToString().ToLower())
						{
							allEmptyValAllowed = notValidated.All(x => (string.IsNullOrEmpty(x)));
						}

						if (!allEmptyValAllowed)
						{
							for (int i = 0; i < notValidated.Count; i++)
							{
								if (string.IsNullOrEmpty(notValidated[i]))
								{
									notValidated[i] = "(blank space)";
								}
							}
							column.hasInValidData = true;
							column.ErrorInfo = string.Join(",", notValidated);
						}
						else //allow blanks
						{
							column.hasInValidData = false;
							column.ErrorInfo = "";
						}

					}
					else
					{
						column.hasInValidData = false;
						column.ErrorInfo = "";
					}
				}
			}

		}

		#endregion

		#region Save Data
		[System.Web.Http.HttpPost]
		public ActionResult ProcessData(string vmStr)
		{
			var vm = JsonConvert.DeserializeObject<IndexViewModel>(vmStr);
			var fileNamePath = Path.Combine(_UPLOADPATH, vm.UploadedFileName);
			var workSheetName = GetWorkSheetName(vm.UploadOption);
			try
			{
				if (vm.UploadOption == UploadFromExcelOptions.AddNewSubmissions.ToString())
				{
					var cloneSubmissionId = vm.Submissions.Where(x => x.Selected == true).Select(x => x.Id).SingleOrDefault();
					//clean up tfImported for this user  
					UpdateImportFlag(vm.FileNumber, _userContext.User.Id);
					//Add submission data
					var uploadedSubmissionIds = AddUpdateSubmissionsData(vm.UploadOption, fileNamePath, workSheetName, vm.ColumnInfo, vm.FileNumber, cloneSubmissionId);
					//Add jurisdictions
					AddJurisdictions(uploadedSubmissionIds, cloneSubmissionId, vm.IncludeJurisdictions, vm.FileNumber, vm.JurisdictionIds);
					//Add Memo
					AddMemos(vm.UploadOption, uploadedSubmissionIds, cloneSubmissionId, vm.ColumnInfo, fileNamePath, workSheetName, vm.FileNumber);
					// Signatures
					AddSignatures(vm.UploadOption, uploadedSubmissionIds, cloneSubmissionId, vm.ColumnInfo, fileNamePath, workSheetName, vm.FileNumber);
					// tfImportedFromExcel flag
					UpdateImportFlag(vm.FileNumber, _userContext.User.Id);
				}
				else if (vm.UploadOption == UploadFromExcelOptions.ModifySubmissions.ToString())
				{
					//update Submission data
					var uploadedSubmissionIds = AddUpdateSubmissionsData(vm.UploadOption, fileNamePath, workSheetName, vm.ColumnInfo, vm.FileNumber, 0);
					//Add memo
					AddMemos(vm.UploadOption, uploadedSubmissionIds, null, vm.ColumnInfo, fileNamePath, workSheetName, vm.FileNumber);
					//Add Signatures
					AddSignatures(vm.UploadOption, uploadedSubmissionIds, null, vm.ColumnInfo, fileNamePath, workSheetName, vm.FileNumber);
				}
				else if (vm.UploadOption == UploadFromExcelOptions.AddSignatures.ToString())
				{
					var query = "SELECT convert(int,keytbl) As Keytbl FROM(Select * from OPENRowSET('Microsoft.ACE.OLEDB.12.0', 'Excel 12.0;Database=" + fileNamePath + ";IMEX=1', 'Select * from [" + workSheetName + "$]') Where Keytbl is not null ) b ";
					var uploadedSubmissionIds = _dbSubmission.Database.GetDbConnection().Query<int>(query).ToList();
					//Add Signatures
					AddSignatures(vm.UploadOption, uploadedSubmissionIds, null, vm.ColumnInfo, fileNamePath, workSheetName, vm.FileNumber);
				}
				else if (vm.UploadOption == UploadFromExcelOptions.ModifySignatures.ToString())
				{
					ModifySignatures(vm.ColumnInfo, fileNamePath);
				}
				else if (vm.UploadOption == UploadFromExcelOptions.DeleteSignatures.ToString())
				{
					DeleteSignatures(fileNamePath);
				}
				//Delete File
				DeleteFile(fileNamePath);
			}
			catch
			{
				if (vm.UploadOption == UploadFromExcelOptions.AddNewSubmissions.ToString())
				{
					//remove all inserted Data
					CleanUploadedData(vm.FileNumber);
				}
				throw;
			}
			return Content("OK");
		}

		private void CleanUploadedData(string fileNumber)
		{
			var query = "Delete from tblMemo Where re_keytbl in (Select keytbl from submissions Where FileNumberStr='" + fileNumber + "' and tfImportedFromExcel=" + _userContext.User.Id + ")";
			RunSQLCommand(query);
			query = "Delete from tbl_Standards Where re_keytbl in (Select keytbl from submissions Where FileNumberStr='" + fileNumber + "' and tfImportedFromExcel=" + _userContext.User.Id + ")";
			RunSQLCommand(query);
			query = "Delete from tbl_Standards Where re_keytbl in (Select keytbl from submissions Where FileNumberStr='" + fileNumber + "' and tfImportedFromExcel=" + _userContext.User.Id + ")";
			RunSQLCommand(query);
			query = "Delete from TblJurCheckOffVersion Where Re_Primary_ in (Select Primary_ from JurisdictionalData Where re_keytbl in (Select keytbl from submissions Where FileNumberStr = '" + fileNumber + "' and tfImportedFromExcel = " + _userContext.User.Id + "))";
			RunSQLCommand(query);
			query = "Delete from trProjectJurisdictionalData Where Re_Primary_ in (Select Primary_ from JurisdictionalData Where re_keytbl in (Select keytbl from submissions Where FileNumberStr ='" + fileNumber + "' and tfImportedFromExcel=" + _userContext.User.Id + "))";
			RunSQLCommand(query);
			query = "Delete from JurisdictionalData Where re_keytbl in (Select keytbl from submissions Where FileNumberStr = '" + fileNumber + "' and tfImportedFromExcel = " + _userContext.User.Id + ")";
			RunSQLCommand(query);
			query = "Delete from TrProjectJurisdictionaldata Where Re_Primary_ in (Select Primary_ from JurisdictionalData Where re_keytbl in (Select keytbl from submissions Where FileNUmberStr ='" + fileNumber + "' and tfImportedFromExcel=" + _userContext.User.Id + "))";
			RunSQLCommand(query);
			query = "Delete from tblSignatures Where re_keytbl in (Select keytbl from Submissions Where FileNumberStr = '" + fileNumber + "' and tfImportedFromExcel=" + _userContext.User.Id + ")";
			RunSQLCommand(query);
			query = "Delete from submissions Where FileNumberStr='" + fileNumber + "' and tfImportedFromExcel=" + _userContext.User.Id;
			RunSQLCommand(query);
			if (!_dbSubmission.Submissions.Where(x => x.FileNumber == fileNumber).Any())
			{
				query = "Delete from SubmissionHeaders Where Filenumber='" + fileNumber + "' and CreatedById=" + _userContext.User.Id;
				RunSQLCommand(query);
			}
		}
		#endregion

		#region Submissions DB updates
		private List<int> AddUpdateSubmissionsData(string uploadOption, string filePath, string workSheetName, List<ExcelColInfo> columns, string fileNumber, int? cloneSubmissionId)
		{
			var sqlQuery = new StringBuilder();
			var setClause = "";
			var selTableColName = "";
			var selExcelColName = "";
			var fromClause = "";
			var whereClause = "";

			var nonSignatureColumns = columns.Where(x => x.TableName == "Submissions").Select(x => new TableExcelMapping() { TableColName = x.TableColName, ExcelColName = x.ExcelColName }).ToList();
			if (uploadOption == UploadFromExcelOptions.AddNewSubmissions.ToString())
			{
				GetSubmissionColumnsForAddNew(ref nonSignatureColumns, fileNumber, cloneSubmissionId);
			}

			//Process special mapped columns
			foreach (var col in nonSignatureColumns)
			{
				if (col.ExcelColName != null)
				{
					if (col.TableColName.ToLower() == "testingtype")
					{
						col.MapFromTable = "isnull((Select top 1 TestingTypeID from tbl_lst_TestingType Where TestingType = a.[" + col.ExcelColName + "]),1)";
					}
					else if (col.TableColName.ToLower() == "company")
					{
						col.MapFromTable = "isnull((Select top 1 CompanyID from tbl_lst_Company Where Company = a.[" + col.ExcelColName + "]),1)";
					}
					else if (col.TableColName.ToLower() == "currency")
					{
						col.MapFromTable = "isnull((Select top 1 CurrencyId from tbl_lst_Currency Where Code = a.[" + col.ExcelColName + "]),1)";
					}
					else if (col.TableColName.ToLower() == "contracttype")
					{
						col.MapFromTable = "isnull((Select top 1 ContractTypeID from tbl_lst_ContractType Where Code = a.[" + col.ExcelColName + "]),1)";
					}
					else if (col.TableColName.ToLower() == "secondaryfunction")
					{
						col.MapFromTable = "isnull((Select top 1 Id from SubmissionsSecondaryFunctions Where Name = a.[" + col.ExcelColName + "]),0)";
					}
				}
			}

			foreach (var column in nonSignatureColumns)
			{
				if (uploadOption == UploadFromExcelOptions.AddNewSubmissions.ToString()) // Add Records
				{
					selTableColName += "[" + column.TableColName + "],";
					if (column.MapFromTable != null)
					{
						selExcelColName += "(" + column.MapFromTable + "),";
					}
					else if (string.IsNullOrEmpty(column.ExcelColName) && !(string.IsNullOrEmpty(column.DefaultValue)))
					{
						selExcelColName += "'" + column.DefaultValue.Replace("'", "''") + "',";
					}
					else
					{
						selExcelColName += "[" + column.ExcelColName.Replace("'", "''") + "],";
					}
				}
				else // Update Records
				{
					if (column.MapFromTable != null)
					{
						setClause += "[" + column.TableColName + "] = (" + column.MapFromTable + "),";
					}
					else if (column.TableColName.ToLower() == SubmissionColumn.IdNumber.ToString().ToLower()
							|| column.TableColName.ToLower() == SubmissionColumn.DateCode.ToString().ToLower()
							|| column.TableColName.ToLower() == SubmissionColumn.Version.ToString().ToLower()
							|| column.TableColName.ToLower() == SubmissionColumn.ChipType.ToString().ToLower()
							|| column.TableColName.ToLower() == SubmissionColumn.Position.ToString().ToLower()
							|| column.TableColName.ToLower() == SubmissionColumn.GameName.ToString().ToLower()
							|| column.TableColName.ToLower() == SubmissionColumn.MinPercent.ToString().ToLower()
							|| column.TableColName.ToLower() == SubmissionColumn.MaxPercent.ToString().ToLower())
					{
						setClause += "[" + column.TableColName + "] = Case when nullif(ltrim(rtrim(a.[" + column.ExcelColName + "])),'') is null then 'N/A' else a.[" + column.ExcelColName + "] end,";
					}
					else
					{
						setClause += "[" + column.TableColName + "] = a.[" + column.ExcelColName.Replace("'", "''") + "],";
					}

				}
			}

			if (uploadOption == UploadFromExcelOptions.AddNewSubmissions.ToString())
			{
				//some export default values needed
				if (nonSignatureColumns.Any(x => x.ExcelColName == "RowNum"))
				{
					selTableColName += "ImportRowNum,";
					selExcelColName += "RowNum,";
				}
				else
				{
					selTableColName += "ImportRowNum,";
					selExcelColName += "ROW_NUMBER() OVER(ORDER BY [ID Number] ASC) As Rownum,";
				}

				selTableColName += "fileNumberStr,tfImportedFromExcel, TS_entry, CreatedByLoginID";
				selExcelColName += "'" + fileNumber + "'," + _userContext.User.Id + ", GETDATE(), '" + _userContext.User.Id + "'";

				//from & where clause
				fromClause = " OPENRowSET('Microsoft.ACE.OLEDB.12.0','Excel 12.0;Database=" + filePath + ";IMEX=1','Select * from [" + workSheetName + "$]') a ";
				whereClause = " [ID Number] <> '' ";

				sqlQuery.AppendFormat("insert into {0} ({1})", "Submissions", selTableColName);
				sqlQuery.AppendFormat("select {0} from {1} where {2}", selExcelColName, fromClause, whereClause);

				//insert into submissions
				var insertQuery = sqlQuery.ToString();
				RunSQLCommand(insertQuery);

				//Run to make sure it is N/A
				setClause = " IDNumber = Case when IDNumber is null or rtrim(ltrim(IDNumber)) = '' then 'N/A' else rtrim(ltrim(IDNumber)) end ";
				setClause += ", DateCode = Case when DateCode is null or rtrim(ltrim(DateCode)) = '' then 'N/A' else rtrim(ltrim(DateCode)) end ";
				setClause += ", Version = Case when Version is null or rtrim(ltrim(Version)) = '' then 'N/A' else rtrim(ltrim(Version)) end ";
				setClause += ", chipType = Case when chipType is null or rtrim(ltrim(chipType)) = '' then 'N/A' else rtrim(ltrim(chipType)) end ";
				setClause += ", Position = Case when Position is null or rtrim(ltrim(Position)) = '' then 'N/A' else rtrim(ltrim(Position)) end ";
				setClause += ", GameName = Case when GameName is null or rtrim(ltrim(GameName)) = '' then 'N/A' else rtrim(ltrim(GameName)) end ";
				setClause += ", MInPercent = Case when MinPercent is null or rtrim(ltrim(MinPercent)) = '' then 'N/A' else rtrim(ltrim(MinPercent)) end ";
				setClause += ", MaxPercent = Case when MaxPercent is null or rtrim(ltrim(MaxPercent)) = '' then 'N/A' else rtrim(ltrim(MaxPercent)) end ";
				setClause += ", Status = Case when Status = 'OH' then 'PN' when Status is null or rtrim(ltrim(Status)) = '' then 'PN' else status end "; //Let them update through Manage OH

				var query = "Update Submissions Set" + setClause + " Where FileNumberSTr ='" + fileNumber + "' and tfImportedFromExcel=" + _userContext.User.Id;
				RunSQLCommand(query);

				var alluploadedSubmissionIds = _dbSubmission.Submissions.Where(x => x.tfImportedFromExcel == _userContext.User.Id && x.FileNumber == fileNumber).Select(x => x.Id).ToList();
				return alluploadedSubmissionIds;
			}
			else
			{
				setClause = setClause.Remove(setClause.Length - 1);
				sqlQuery.AppendFormat("UPDATE {0} set {1} ", "Submissions", setClause);
				sqlQuery.Append(" from Submissions c join (SELECT * FROM(Select * from OPENRowSET('Microsoft.ACE.OLEDB.12.0', 'Excel 12.0;Database=" + filePath + ";IMEX=1', 'Select * from [" + workSheetName + "$]') ) b ) a on a.Keytbl = c.Keytbl ");
				var query = sqlQuery.ToString();
				RunSQLCommand(query);

				query = "SELECT convert(int,keytbl) FROM(Select * from OPENRowSET('Microsoft.ACE.OLEDB.12.0', 'Excel 12.0;Database=" + filePath + ";IMEX=1', 'Select * from [" + workSheetName + "$]') ) b ";
				var alluploadedSubmissionIds = _dbSubmission.Database.GetDbConnection().Query<int>(query).ToList();
				return alluploadedSubmissionIds;
			}
		}

		private List<TableExcelMapping> GetSubmissionColumnsForAddNew(ref List<TableExcelMapping> finalColumns, string fileNumber, int? cloneSubmissionId)
		{
			var cloneSubmissiondataQuery = _dbSubmission.Submissions.Where(x => x.FileNumber == fileNumber).AsQueryable();
			if (cloneSubmissionId != null)
			{
				cloneSubmissiondataQuery = cloneSubmissiondataQuery.Where(x => x.Id == cloneSubmissionId).AsQueryable();
			}
			var cloneSubmissionData = cloneSubmissiondataQuery.OrderBy(x => x.Id).FirstOrDefault();

			//This will be used while clone
			if (!finalColumns.Any(x => x.TableColName.ToLower() == "cert_lab") && !string.IsNullOrEmpty(cloneSubmissionData.CertificationLab))
			{
				finalColumns.Add(new TableExcelMapping { TableColName = "cert_lab", DefaultValue = cloneSubmissionData.CertificationLab });
			}
			if (!finalColumns.Any(x => x.TableColName.ToLower() == "test_lab") && !string.IsNullOrEmpty(cloneSubmissionData.Lab))
			{
				finalColumns.Add(new TableExcelMapping { TableColName = "test_lab", DefaultValue = cloneSubmissionData.Lab });
			}
			if (!finalColumns.Any(x => x.TableColName.ToLower() == "submission_type") && !string.IsNullOrEmpty(cloneSubmissionData.Type))
			{
				finalColumns.Add(new TableExcelMapping { TableColName = "Submission_type", DefaultValue = cloneSubmissionData.Type });
			}
			if (!finalColumns.Any(x => x.TableColName.ToLower() == "jurisdiction") && !string.IsNullOrEmpty(cloneSubmissionData.JurisdictionId))
			{
				finalColumns.Add(new TableExcelMapping { TableColName = "jurisdiction", DefaultValue = cloneSubmissionData.JurisdictionId });
			}
			if (!finalColumns.Any(x => x.TableColName.ToLower() == "manufacturer") && !string.IsNullOrEmpty(cloneSubmissionData.ManufacturerCode))
			{
				finalColumns.Add(new TableExcelMapping { TableColName = "manufacturer", DefaultValue = cloneSubmissionData.ManufacturerCode });
			}
			if (!finalColumns.Any(x => x.TableColName.ToLower() == "year") && !string.IsNullOrEmpty(cloneSubmissionData.Year))
			{
				finalColumns.Add(new TableExcelMapping { TableColName = "year", DefaultValue = cloneSubmissionData.Year });
			}
			if (!finalColumns.Any(x => x.TableColName.ToLower() == "job_sequence") && !string.IsNullOrEmpty(cloneSubmissionData.Sequence))
			{
				finalColumns.Add(new TableExcelMapping { TableColName = "job_sequence", DefaultValue = cloneSubmissionData.Sequence });
			}
			if (!finalColumns.Any(x => x.TableColName.ToLower() == "recdate") && cloneSubmissionData.ReceiveDate != null)
			{
				finalColumns.Add(new TableExcelMapping { TableColName = "recDate", DefaultValue = cloneSubmissionData.ReceiveDate.ToString() });
			}
			if (!finalColumns.Any(x => x.TableColName.ToLower() == "subdate") && cloneSubmissionData.SubmitDate != null)
			{
				finalColumns.Add(new TableExcelMapping { TableColName = "subdate", DefaultValue = cloneSubmissionData.SubmitDate.ToString() });
			}
			if (!finalColumns.Any(x => x.TableColName.ToLower() == "company") && cloneSubmissionData.Company != null)
			{
				finalColumns.Add(new TableExcelMapping { TableColName = "company", DefaultValue = cloneSubmissionData.Company.Id.ToString() });
			}
			if (!finalColumns.Any(x => x.TableColName.ToLower() == "currency") && cloneSubmissionData.Currency != null)
			{
				finalColumns.Add(new TableExcelMapping { TableColName = "currency", DefaultValue = cloneSubmissionData.Currency.Id.ToString() });
			}
			if (!finalColumns.Any(x => x.TableColName.ToLower() == "contracttype") && cloneSubmissionData.ContractType != null)
			{
				finalColumns.Add(new TableExcelMapping { TableColName = "contracttype", DefaultValue = cloneSubmissionData.ContractType.Id.ToString() });
			}
			if (!finalColumns.Any(x => x.TableColName.ToLower() == "testingtype") && cloneSubmissionData.TestingType != null)
			{
				finalColumns.Add(new TableExcelMapping { TableColName = "testingtype", DefaultValue = cloneSubmissionData.TestingType.Id.ToString() });
			}
			if (!finalColumns.Any(x => x.TableColName.ToLower() == "status") && !string.IsNullOrEmpty(cloneSubmissionData.Status))
			{
				finalColumns.Add(new TableExcelMapping { TableColName = "status", DefaultValue = (cloneSubmissionData.Status == "OH") ? "PN" : cloneSubmissionData.Status.ToString() });
			}
			if (!finalColumns.Any(x => x.TableColName.ToLower() == "letter_number") && !string.IsNullOrEmpty(cloneSubmissionData.LetterNumber))
			{
				finalColumns.Add(new TableExcelMapping { TableColName = "letter_number", DefaultValue = cloneSubmissionData.LetterNumber.ToString() });
			}
			if (!finalColumns.Any(x => x.TableColName.ToLower() == "contractvalue") && cloneSubmissionData.ContractValue != null)
			{
				finalColumns.Add(new TableExcelMapping { TableColName = "contractvalue", DefaultValue = cloneSubmissionData.ContractValue.ToString() });
			}
			if (!finalColumns.Any(x => x.TableColName.ToLower() == "masterfilenumberstr") && !string.IsNullOrEmpty(cloneSubmissionData.MasterFileNumber))
			{
				finalColumns.Add(new TableExcelMapping { TableColName = "MasterFileNumberStr", DefaultValue = cloneSubmissionData.MasterFileNumber.ToString() });
			}
			if (!finalColumns.Any(x => x.TableColName.ToLower() == "secondaryfunction") && !string.IsNullOrEmpty(cloneSubmissionData.SecondaryFunction.Name))
			{
				finalColumns.Add(new TableExcelMapping { TableColName = "secondaryFunction", DefaultValue = cloneSubmissionData.SecondaryFunction.Id.ToString() });
			}
			if (!finalColumns.Any(x => x.TableColName.ToLower() == "studioid") && !string.IsNullOrEmpty(cloneSubmissionData.StudioId.ToString()))
			{
				finalColumns.Add(new TableExcelMapping { TableColName = "StudioId", DefaultValue = cloneSubmissionData.StudioId.ToString() });
			}
			if (!finalColumns.Any(x => x.TableColName.ToLower() == "archivelocation") && !string.IsNullOrEmpty(cloneSubmissionData.ArchiveLocation.ToString()))
			{
				finalColumns.Add(new TableExcelMapping { TableColName = "ArchiveLocation", DefaultValue = cloneSubmissionData.ArchiveLocation.ToString() });
			}
			if (!finalColumns.Any(x => x.TableColName.ToLower() == "contractnumber") && !string.IsNullOrEmpty(cloneSubmissionData.ContractNumber))
			{
				finalColumns.Add(new TableExcelMapping { TableColName = "ContractNumber", DefaultValue = cloneSubmissionData.ContractNumber.ToString() });
			}

			finalColumns.Add(new TableExcelMapping { TableColName = "manuf", DefaultValue = cloneSubmissionData.ManufacturerDescription.ToString() });
			finalColumns.Add(new TableExcelMapping { TableColName = "s_userid", DefaultValue = _userContext.User.Username });

			return finalColumns;
		}
		#endregion

		#region Jurisdictions Db updates

		private void AddJurisdictions(List<int> uploadedSubmissionIds, int? cloneSubmissionId, bool includeAllJurisdictions, string fileNumber, List<string> jurisdictionIds)
		{
			var query = "";
			if (includeAllJurisdictions)
			{
				query = "insert into JurisdictionalData(";
				query += "re_keyTbl";
				query += ",JurisdictionTxt";
				query += ",JurisdictionId";
				query += ",JurisdictionRequestDate";
				query += ",Active";
				query += ",JurisdictionType";
				query += ",JurisdictionName ";
				query += ",MasterBillingProject ";
				query += ",ProjectNo ";
				query += ",ContractNumber ";
				query += ",TXLab";
				query += ",JurisdictionReceivedDate";
				query += ",ClientNumber";
				query += ",Letter_Number";
				query += ",PurchaseOrder";
				query += ",Company";
				query += ",Currency";
				query += ",ContractType";
				query += ", re_TS_entry, CreatedByLoginID ";
				query += ")";
				query += " Select ";
				query += "keyTbl";
				query += ",JurisdictionTxt";
				query += ",JurisdictionId";
				query += ",JurisdictionRequestDate";
				query += ",Active";
				query += ",Case when JurisdictionType = 'OH' then 'RA' else JurisdictionType end ";
				query += ",JurisdictionName ";
				query += ",MasterBillingProject ";
				query += ",ProjectNo ";
				query += ",b.ContractNumber ";
				query += ",TXLab";
				query += ",JurisdictionReceivedDate";
				query += ",b.ClientNumber";
				query += ",b.Letter_Number";
				query += ",b.PurchaseOrder";
				query += ",b.Company";
				query += ",b.Currency";
				query += ",b.ContractType";
				query += ",getDate(), " + _userContext.User.Id;
				query += " from Submissions a with (nolock), JurisdictionalData b with (nolock)";
				//query += " Where a.FileNumberSTr ='" + fileNumber + "' and a.tfImportedFromExcel=" + _userContext.User.Id + " and b.re_keyTbl=" + cloneSubmissionId;
				query += " Where a.FileNumberSTr ='" + fileNumber + "' and a.keytbl in (" + string.Join(",", uploadedSubmissionIds) + ") and b.re_keyTbl=" + cloneSubmissionId;
				if (jurisdictionIds.Count > 0)
				{
					query += " and b.jurisdictionId in (" + string.Join(",", jurisdictionIds.Select(x => string.Format("'{0}'", x)).ToList()) + ")";
				}
				RunSQLCommand(query);

				//Test script
				query = "Insert into TblJurCheckOffVersion(re_primary_,checkOff_ID ) Select f.Primary_, checkOff_ID from (Select CheckOff_ID, b.JurisdictionTxt from TblJurCheckOffVersion a with (nolock) Join JurisdictionalData b with (nolock) on a.re_Primary_ =b.Primary_ Where b.re_keytbl=" + cloneSubmissionId + ") h Join (";
				query += " Select Primary_, JurisdictionTxt from JurisdictionalData with (nolock) Where re_Keytbl in (Select Keytbl from submissions with (nolock) Where FilenumberStr = '" + fileNumber + "' and keytbl in (" + string.Join(",", uploadedSubmissionIds) + "))) f on h.JurisdictionTxt=f.JurisdictionTxt ";
				RunSQLCommand(query);
			}
			else //primary jur
			{
				var primaryJur = _dbSubmission.SubmissionHeaders.Where(x => x.FileNumber == fileNumber).Select(x => x.JurisdictionId).Single();
				var jurName = _dbSubmission.tbl_lst_fileJuris.Where(x => x.Id == primaryJur).Select(x => x.Name).SingleOrDefault();

				query = "insert into JurisdictionalData(";
				query += "re_keyTbl";
				query += ",JurisdictionTxt";
				query += ",JurisdictionId";
				query += ",JurisdictionRequestDate";
				query += ",Active";
				query += ",JurisdictionType";
				query += ",JurisdictionName ";
				query += ",TXLab";
				query += ",JurisdictionReceivedDate";
				query += ",ClientNumber";
				query += ",Letter_Number";
				query += ",PurchaseOrder";
				query += ",MasterBillingProject";
				query += ",ProjectNo ";
				query += ",ContractNumber ";
				query += ",Company";
				query += ",Currency";
				query += ",ContractType";
				query += ", re_TS_entry, CreatedByLoginID ";
				query += ")";
				query += " Select ";
				query += "keyTbl";
				query += ",'Jurisdiction" + primaryJur + "'";
				query += ",'" + primaryJur + "'";
				query += ",a.subdate";
				query += ",'No'";
				query += ",'RA'";
				query += ",'" + jurName + "'";
				query += ",(SELECT locationId FROM trLaboratories WHERE location = a.cert_lab)";
				query += ",a.recDate";
				query += ",b.ClientNumber";
				query += ",b.Letter_Number";
				query += ",b.PurchaseOrder";
				query += ",b.MasterBillingProject";
				query += ",b.ProjectNo";
				query += ",b.ContractNumber ";
				query += ",a.Company";
				query += ",a.Currency";
				query += ",a.ContractType";
				query += ",getDate(), " + _userContext.User.Id;
				query += " from Submissions a with (nolock), JurisdictionalData b with (nolock)";
				query += " Where a.FileNumberSTr ='" + fileNumber + "' and a.keytbl in (" + string.Join(",", uploadedSubmissionIds) + ") and b.re_keyTbl=" + cloneSubmissionId + " and b.JurisdictionId='" + primaryJur + "'";

				RunSQLCommand(query);
			}

			//Add jur for new sub project 
			query = "insert into trProjectJurisdictionalData(ProjectID, re_Primary_)";
			query += " Select ProjectID,Primary_ ";
			query += " from jurisdictionalData a with (nolock) join Submissions b with (nolock) on a.re_Keytbl=b.keytbl ";
			query += " join trProject c with (nolock) on b.Filenumberstr=c.filenumber ";
			query += " Where ";
			query += "c.IsTransfer=0 and b.FileNumberStr ='" + fileNumber + "' and a.re_keytbl in (" + string.Join(",", uploadedSubmissionIds) + ")";
			query += " and TransferAvailable=0 and Not exists (Select 1 from trProjectJurisdictionalData with (nolock) where ProjectID = c.ProjectID and re_Primary_=a.Primary_)";
			RunSQLCommand(query);

			//Add Standards
			foreach (var submissionId in uploadedSubmissionIds)
			{
				var jurisdictionsForStandards = (from j in _dbSubmission.JurisdictionalData
												 join sa in _dbSubmission.tbl_lst_StandardsAll on j.JurisdictionId equals sa.JurisdictionId
												 where j.SubmissionId == submissionId && sa.Active == true
												 select j).ToList();
				foreach (var jur in jurisdictionsForStandards)
				{
					_jurisdictionalDataService.AddStandards(jur);
				}
			}
		}

		#endregion

		#region Memo
		private void AddMemos(string uploadOption, List<int> uploadedSubmissionIds, int? cloneSubmissionId, List<ExcelColInfo> columnMapping, string fileNamePath, string workSheetName, string fileNumber)
		{
			var query = "";
			if (uploadOption == UploadFromExcelOptions.AddNewSubmissions.ToString())
			{
				if (cloneSubmissionId != null)
				{
					query = "insert into tblmemo(re_keytbl,tstamp,s_UserID,note)";
					query += " Select a.KeyTbl,b.tstamp, b.s_userID,b.note from Submissions a , tblMemo b ";
					query += " Where a.keytbl in (" + string.Join(",", uploadedSubmissionIds) + ") and b.re_keyTbl=" + cloneSubmissionId;
					RunSQLCommand(query);
				}
			}

			if (columnMapping.Any(x => x.TableName == "tblMemo"))
			{
				var excelColMemo = columnMapping.Where(x => x.TableName == "tblMemo").Select(x => x.ExcelColName).SingleOrDefault();
				if (uploadOption == UploadFromExcelOptions.AddNewSubmissions.ToString())
				{
					query = "insert into tblMemo (re_KeyTbl, tStamp, s_UserID, note) ";
					query += " Select b.keytbl, GetDate(),'" + _userContext.User.Username + "',a." + excelColMemo;
					query += " FROM ( Select ROW_NUMBER() OVER(ORDER BY [ID Number] ASC) As Rownum,* from OPENRowSET('Microsoft.ACE.OLEDB.12.0','Excel 12.0;Database=" + fileNamePath + ";IMEX=1','Select * from [" + workSheetName + "$]') Where [ID Number] <> '') a ";
					query += " join (Select keytbl,ImportRownum from Submissions where filenumberStr='" + fileNumber + "' and keytbl in (" + string.Join(",", uploadedSubmissionIds) + ")) b ";
					query += " on a.[rownum]=b.[ImportRownum]  Where a.[" + excelColMemo + "] <> ''";
				}
				else
				{
					query = "insert into tblMemo (re_KeyTbl, tStamp, s_UserID, note) ";
					query += " Select b.keytbl, GetDate(),'" + _userContext.User.Username + "',a." + excelColMemo;
					query += " FROM ( Select * from OPENRowSET('Microsoft.ACE.OLEDB.12.0','Excel 12.0;Database=" + fileNamePath + ";IMEX=1','Select * from [" + workSheetName + "$]')) a ";
					query += " join (Select keytbl from Submissions where filenumberStr='" + fileNumber + "' and keytbl in (" + string.Join(",", uploadedSubmissionIds) + ")) b ";
					query += " on a.[keytbl]=b.[keytbl]  Where a.[" + excelColMemo + "] <> ''";
				}
				RunSQLCommand(query);
			}

		}
		#endregion

		#region Signatures
		private void AddSignatures(string uploadOption, List<int> uploadedSubmissionIds, int? cloneSubmissionId, List<ExcelColInfo> columnMapping, string fileNamePath, string workSheetName, string fileNumber)
		{
			var query = "";
			var allSignatures = columnMapping.Where(x => x.IsSignatureColumn == true).ToList();
			var filePathExists = columnMapping.Any(x => x.ExcelColName.ToLower() == "filepath");
			if (allSignatures.Count > 0)
			{
				var insertQuery = "insert into tblSignatures (TypeOfID,ScopeOFID,VersionID,TypeOf,ScopeOf,VerifyVersion,Signature,ComponentID,sFilePath, Sig_Entry,re_keytbl) ";
				foreach (var signature in allSignatures)
				{
					if (uploadOption == UploadFromExcelOptions.AddNewSubmissions.ToString())
					{
						if (string.IsNullOrEmpty(signature.SignatureFilePath) && filePathExists)
						{
							query = " SELECT '" + signature.SignatureTypeId + "','" + signature.SignatureScopeId + "','" + signature.SignatureVersionId + "','" + signature.SignatureType + "', '" + signature.SignatureScope + "','" + signature.SignatureVersion + "',[" + signature.ExcelColName + "],'" + signature.SignatureAlphaVersion + "',[FilePath], GetDate(),b.keytbl ";
							query += "FROM (Select ROW_NUMBER() OVER(ORDER BY [ID Number] ASC) As Rownum, * from OPENRowSET('Microsoft.ACE.OLEDB.12.0','Excel 12.0;Database=" + fileNamePath + ";IMEX=1','Select * from [" + workSheetName + "$]') Where [ID Number] <> '') a join (Select keytbl, ImportRowNum from Submissions where filenumberStr='" + fileNumber + "' and keytbl in (" + string.Join(",", uploadedSubmissionIds) + ")) b on a.[rownum]=b.[ImportRownum] Where a.[" + signature.ExcelColName + "] <> ''";
						}
						else
						{
							query = " SELECT '" + signature.SignatureTypeId + "','" + signature.SignatureScopeId + "','" + signature.SignatureVersionId + "','" + signature.SignatureType + "', '" + signature.SignatureScope + "','" + signature.SignatureVersion + "',[" + signature.ExcelColName + "],'" + signature.SignatureAlphaVersion + "','" + signature.SignatureFilePath + "', GetDate(),b.keytbl ";
							query += "FROM (Select ROW_NUMBER() OVER(ORDER BY [ID Number] ASC) As Rownum, * from OPENRowSET('Microsoft.ACE.OLEDB.12.0','Excel 12.0;Database=" + fileNamePath + ";IMEX=1','Select * from [" + workSheetName + "$]') Where [ID Number] <> '') a join (Select keytbl, ImportRowNum from Submissions where filenumberStr='" + fileNumber + "' and keytbl in (" + string.Join(",", uploadedSubmissionIds) + ")) b on a.[rownum]=b.[ImportRownum] Where a.[" + signature.ExcelColName + "] <> ''";
						}
					}
					else
					{
						if (filePathExists) //same(Type/scope/version) with same sig value can not exist
						{
							query = " SELECT '" + signature.SignatureTypeId + "','" + signature.SignatureScopeId + "','" + signature.SignatureVersionId + "','" + signature.SignatureType + "', '" + signature.SignatureScope + "','" + signature.SignatureVersion + "',[" + signature.ExcelColName + "],'" + signature.SignatureAlphaVersion + "',[FilePath], GetDate(),a.keytbl ";
							query += " FROM ( Select [" + signature.ExcelColName + "],[FilePath], keytbl from OPENRowSET('Microsoft.ACE.OLEDB.12.0','Excel 12.0;Database=" + fileNamePath + ";IMEX=1','Select * from [" + workSheetName + "$]')) a ";
							query += " left join (Select re_keytbl, TypeOfId, ScopeOfId, versionId, signature from tblSignatures where re_keytbl in (" + string.Join(",", uploadedSubmissionIds) + ")) b ";
							query += " on a.[keytbl]=b.[re_keytbl] and isnull(b.TypeOfID,0)='" + signature.SignatureTypeId + "' and isnull(b.ScopeOFID,0)='" + signature.SignatureScopeId + "' and isnull(b.versionID,0)='" + signature.SignatureVersionId + "' and rtrim(isnull(b.signature,''))=a.[" + signature.ExcelColName + "]";
							query += " Where a.[" + signature.ExcelColName + "] <> '' and b.re_keytbl is null";
						}
						else
						{
							if (signature.SignatureType.ToLower() == "checksum") // same sig allowed
							{
								query = " SELECT '" + signature.SignatureTypeId + "','" + signature.SignatureScopeId + "','" + signature.SignatureVersionId + "','" + signature.SignatureType + "', '" + signature.SignatureScope + "','" + signature.SignatureVersion + "',[" + signature.ExcelColName + "],'" + signature.SignatureAlphaVersion + "','" + signature.SignatureFilePath + "', GetDate(),a.keytbl ";
								query += " FROM ( Select [" + signature.ExcelColName + "], keytbl from OPENRowSET('Microsoft.ACE.OLEDB.12.0','Excel 12.0;Database=" + fileNamePath + ";IMEX=1','Select * from [" + workSheetName + "$]')) a ";
								query += " join (Select keytbl from submissions where keytbl in (" + string.Join(",", uploadedSubmissionIds) + ")) b ";
								query += " on a.[keytbl]=b.[keytbl] ";
								query += " Where a.[" + signature.ExcelColName + "] <> '' ";
							}
							else
							{
								query = " SELECT '" + signature.SignatureTypeId + "','" + signature.SignatureScopeId + "','" + signature.SignatureVersionId + "','" + signature.SignatureType + "', '" + signature.SignatureScope + "','" + signature.SignatureVersion + "',[" + signature.ExcelColName + "],'" + signature.SignatureAlphaVersion + "','" + signature.SignatureFilePath + "', GetDate(),a.keytbl ";
								query += " FROM ( Select [" + signature.ExcelColName + "], keytbl from OPENRowSET('Microsoft.ACE.OLEDB.12.0','Excel 12.0;Database=" + fileNamePath + ";IMEX=1','Select * from [" + workSheetName + "$]')) a ";
								query += " left join (Select  re_keytbl, TypeOfId, ScopeOfId, versionId from tblSignatures where re_keytbl in (" + string.Join(",", uploadedSubmissionIds) + ")) b ";
								query += " on a.[keytbl]=b.[re_keytbl] and isnull(b.TypeOfID,0)='" + signature.SignatureTypeId + "' and isnull(b.ScopeOFID,0)='" + signature.SignatureScopeId + "' and isnull(b.versionID,0)='" + signature.SignatureVersionId + "'";
								query += " Where a.[" + signature.ExcelColName + "] <> '' and b.re_keytbl is null";
							}
						}
					}
					RunSQLCommand(insertQuery + query);
				}
			}
		}

		private void ModifySignatures(List<ExcelColInfo> columnMapping, string fileNamePath)
		{
			var query = "";
			var filePathExists = columnMapping.Any(x => x.ExcelColName.ToLower() == "filepath");
			var seedExists = columnMapping.Any(x => x.ExcelColName.ToLower() == "seed");
			if (filePathExists)
			{
				query = "Update tblSignatures Set Signature=ltrim(rtrim(replace(a.[Signature],char(9),''))),sFilePath=a.FilePath,";
				if (seedExists)
				{
					query += "Seed =a.Seed,";
				}
				query += "Sig_Edit=GetDate() ";
				query += " from tblSignatures b join (Select * from OPENRowSET('Microsoft.ACE.OLEDB.12.0','Excel 12.0;Database=" + fileNamePath + ";IMEX=1','Select * from [MODSIG$]')) a  on a.keytbl=b.re_keytbl and isnull(a.[Sig Type],'')=isnull(b.TypeOf,'') and isnull(a.[Sig Scope],'')= isnull(b.ScopeOF,'') and isnull(a.[Sig Version],'')=isnull(b.Verifyversion,'') Where a.[Signature] <> '' and (a.[Signature] <> b.[Signature] or a.FilePath <> isnull(b.sFilePath,''))";
			}
			else
			{
				query = "Update tblSignatures Set Signature=ltrim(rtrim(replace(a.[Signature],char(9),''))), ";
				if (seedExists)
				{
					query += "Seed = a.Seed,";
				}
				query += "Sig_Edit=GetDate() ";
				query += " from tblSignatures b join (Select * from OPENRowSET('Microsoft.ACE.OLEDB.12.0','Excel 12.0;Database=" + fileNamePath + ";IMEX=1','Select * from [MODSIG$]')) a  on a.keytbl=b.re_keytbl and isnull(a.[Sig Type],'')=isnull(b.TypeOf,'') and isnull(a.[Sig Scope],'')= isnull(b.ScopeOF,'') and isnull(a.[Sig Version],'')=isnull(b.Verifyversion,'') Where a.[Signature] <> '' and a.[Signature] <> b.[Signature]";
			}
			RunSQLCommand(query);
		}

		private void DeleteSignatures(string fileNamePath)
		{
			var query = "Delete from tblSignatures Where SignatureID in ( ";
			query += "Select SignatureID from tblSignatures b join (Select * from OPENRowSET('Microsoft.ACE.OLEDB.12.0','Excel 12.0;Database=" + fileNamePath + ";IMEX=1','Select * from [DELSIG$]')) a  on a.keytbl=b.re_keytbl and isnull(a.[Sig Type],'')=isnull(b.TypeOf,'') and isnull(a.[Sig Scope],'')= isnull(b.ScopeOF,'') and isnull(a.[Sig Version],'')=isnull(b.Verifyversion,'') Where a.[Action] = 'D' )";
			RunSQLCommand(query);
		}

		#endregion

		#region SQL Common
		private DataTable getSQLData(string query)
		{
			var dt = new DataTable();
			var cmd = _dbSubmission.Database.GetDbConnection().CreateCommand();
			_dbSubmission.Database.GetDbConnection().Open();
#pragma warning disable CA2100 // Review SQL queries for security vulnerabilities
			cmd.CommandText = query;
#pragma warning restore CA2100 // Review SQL queries for security vulnerabilities
			var reader = cmd.ExecuteReader();
			dt.Load(reader);
			return dt;
		}
		private void RunSQLCommand(string query)
		{
			var finalQuery = "exec devin_setcontext " + _userContext.User.Id + ";" + query;
			_dbSubmission.Database.ExecuteSqlRaw(finalQuery);
		}

		private void UpdateImportFlag(string fileNumber, int userId)
		{
			var query = "exec devin_setcontext " + _userContext.User.Id + ";" + "Update Submissions Set tfImportedFromExcel = 1 Where FileNumberStr = '" + fileNumber + "' and tfImportedFromExcel = " + _userContext.User.Id + ";";
			_dbSubmission.Database.ExecuteSqlRaw(query);
		}
		#endregion

		#region Download template
		public void ExportDownloadFormat(string uploadOption, string fileNumber)
		{
			var dt = new DataTable();
			var fileName = GetWorkSheetName(uploadOption) + ".xls";
			if (uploadOption == UploadFromExcelOptions.AddNewSubmissions.ToString())
			{
				var columnsAllowed = _dbSubmission.ExcelImportMaps.Where(x => (x.TableName == "Submissions" || x.TableName == "tblMemo") && x.tfActive == true).Select(x => x.ExcelColName).Distinct().ToList();
				foreach (var column in columnsAllowed)
				{
					dt.Columns.Add(column);
				}
				dt.Rows.Add();
			}
			else if (uploadOption == UploadFromExcelOptions.ModifySubmissions.ToString())
			{
				var columnsAllowed = _dbSubmission.ExcelImportMaps.Where(x => (x.TableName == "Submissions" || x.TableName == "tblMemo") && x.tfActive == true).Select(x => new { ExcelColName = x.ExcelColName, TableColName = x.TableColName, TableName = x.TableName, CheckTableVal = x.CheckTableval }).Distinct().ToList();
				var selectColumns = "Keytbl";
				foreach (var column in columnsAllowed)
				{
					selectColumns += (selectColumns != "") ? "," : "";
					if (column.TableName == "Submissions")
					{
						if (column.TableColName.ToLower() == "testingtype")
						{
							selectColumns += "(Select top 1 TestingType from tbl_lst_TestingType Where TestingTypeId = a.[" + column.TableColName + "]) ";
						}
						else if (column.TableColName.ToLower() == "company")
						{
							selectColumns += "(Select top 1 Company from tbl_lst_Company Where CompanyId = a.[" + column.TableColName + "])";
						}
						else if (column.TableColName.ToLower() == "currency")
						{
							selectColumns += "(Select top 1 Code from tbl_lst_Currency Where CurrencyId = a.[" + column.TableColName + "])";
						}
						else if (column.TableColName.ToLower() == "contracttype")
						{
							selectColumns += "(Select top 1 Code from tbl_lst_ContractType Where ContractTypeID = a.[" + column.TableColName + "])";
						}
						else if (column.TableColName.ToLower() == "secondaryfunction")
						{
							selectColumns += "(Select top 1 Name from SubmissionsSecondaryFunctions Where Id = a.[" + column.TableColName + "])";
						}
						else
						{
							selectColumns += "[" + column.TableColName + "]";
						}
						selectColumns += " As [" + column.ExcelColName + "]";
					}
					else
					{
						selectColumns += "'' As [" + column.ExcelColName + "]";
					}
				}
				var query = "Select " + selectColumns + " from Submissions a With (nolock) Where filenumberStr ='" + fileNumber + "' order by keytbl";
				dt = getSQLData(query);
			}
			else if (uploadOption == UploadFromExcelOptions.AddSignatures.ToString())
			{
				var query = "Select Keytbl, fileNumberStr As [File Number], IdNumber As [ID Number], Version, GameName As [Game Name], DateCode As [Date Code], subDate As [Submitted Date], recDate As [Received Date] from Submissions a with (nolock) Where filenumberStr = '" + fileNumber + "' order by keytbl";
				dt = getSQLData(query);
			}
			else if (uploadOption == UploadFromExcelOptions.ModifySignatures.ToString())
			{
				var query = "Select keytbl, filenumberStr As [File Number], IdNumber As [ID Number], Version, GameName  As [Game Name], DateCode As [Date Code], subdate As [Submitted Date], recDate As [Received Date], rtrim(TypeOf) As [Sig Type], rtrim(ScopeOf) As [Sig Scope], rtrim(VerifyVersion) As [Sig Version], Signature  As [Signature], sFilePath As [FilePath], Seed  As [Seed] " +
							" from Submissions a join tblSignatures b on a.keytbl=b.re_keytbl Where FilenumberStr = '" + fileNumber + "' and Signature is not null";
				dt = getSQLData(query);
			}
			else if (uploadOption == UploadFromExcelOptions.DeleteSignatures.ToString())
			{
				var query = "Select keytbl, filenumberStr As [File Number], IdNumber As [ID Number], Version, GameName  As [Game Name], DateCode As [Date Code], subdate As [Submitted Date], recDate As [Received Date], rtrim(TypeOf) As [Sig Type], rtrim(ScopeOf) As [Sig Scope], rtrim(VerifyVersion) As [Sig Version], Signature As [Signature], '' As [Action] " +
							" from Submissions a join tblSignatures b on a.keytbl=b.re_keytbl Where FilenumberStr = '" + fileNumber + "' and Signature is not null";
				dt = getSQLData(query);
			}

			var sb = new StringBuilder();
			foreach (DataColumn column in dt.Columns)
			{
				sb.AppendFormat("{0}{1}", column.ColumnName, "\t");
			}

			sb.Append("\n");

			foreach (DataRow row in dt.Rows)
			{
				for (int i = 0; i < dt.Columns.Count; i++)
				{
					var cellValue = row[i].ToString();
					if (dt.Columns[i].ColumnName == "Signature")
					{
						sb.AppendFormat("{0}{1}", "=\"" + cellValue + "\"", "\t");
					}
					else
					{
						sb.AppendFormat("{0}{1}", "\"" + cellValue + "\"", "\t");
					}
				}

				sb.Append("\n");
			}

			Response.ClearContent();
			Response.AddHeader("content-disposition", "attachment; filename=" + fileName);
			Response.ContentType = "application/excel";
			Response.ContentEncoding = System.Text.Encoding.Default;
			Response.Clear();
			Response.Write(sb.ToString());
			Response.Flush();
			Response.SuppressContent = true;
		}
		#endregion

		#region Helper
		private DataTable CreateDataTableFromStream(Stream fileStream)
		{
			var dataTable = new DataTable();
			try
			{
				var workbook = new Workbook(fileStream);
				var worksheet = workbook.Worksheets[0];
				dataTable = worksheet.Cells.ExportDataTableAsString(0, 0, worksheet.Cells.MaxRow + 1, worksheet.Cells.MaxColumn + 1, true);
			}
			catch (Exception ex)
			{
				_logger.LogError(ex, "DataTable");
			}

			return dataTable;
		}

		private string SaveFile(HttpPostedFileBase file)
		{
			var uploadedFileName = string.Empty;
			var newFile = Path.Combine(_UPLOADPATH, GenerateName(Path.GetFileName(file.FileName)));

			try
			{
				file.SaveAs(newFile);
				uploadedFileName = Path.GetFileName(newFile);
			}
			catch (Exception ex)
			{
				_logger.LogError(ex, "Error While Saving to Excel");
			}
			return uploadedFileName;
		}

		private string GenerateName(string fileName)
		{
			return
				new StringBuilder()
				.Append("Records")
				.Append("_")
				.Append(_userContext.User.Id)
				.Append("_")
				.Append(Guid.NewGuid())
				.Append("_")
				.Append(fileName)
				.ToString();
		}

		private void DeleteFile(string fileNamePath)
		{
			if (System.IO.File.Exists(fileNamePath))
			{
				System.IO.File.Delete(fileNamePath);
			}
		}

		private string GetWorkSheetName(string uploadOption)
		{
			if (uploadOption == UploadFromExcelOptions.AddNewSubmissions.ToString())
			{
				return "NewRecords";
			}
			else if (uploadOption == UploadFromExcelOptions.ModifySubmissions.ToString())
			{
				return "ModRecords";
			}
			else if (uploadOption == UploadFromExcelOptions.AddSignatures.ToString())
			{
				return "AddSig";
			}
			else if (uploadOption == UploadFromExcelOptions.ModifySignatures.ToString())
			{
				return "ModSig";
			}
			else if (uploadOption == UploadFromExcelOptions.DeleteSignatures.ToString())
			{
				return "DelSig";
			}
			else
			{
				return "";
			}
		}

		private List<string> IgnoreColumnValidationsFor(string uploadOption)
		{
			var listOfColumns = new List<string>();
			if (uploadOption == UploadFromExcelOptions.AddSignatures.ToString())
			{
				listOfColumns.Add("File Number");
				listOfColumns.Add("ID Number");
				listOfColumns.Add("Version");
				listOfColumns.Add("Game Name");
				listOfColumns.Add("Date Code");
				listOfColumns.Add("Submitted Date");
				listOfColumns.Add("Received Date");
				listOfColumns.Add("FilePath"); //This will be inserted for each new signature
			}
			else if (uploadOption == UploadFromExcelOptions.ModifySignatures.ToString())
			{
				listOfColumns.Add("File Number");
				listOfColumns.Add("ID Number");
				listOfColumns.Add("Version");
				listOfColumns.Add("Game Name");
				listOfColumns.Add("Date Code");
				listOfColumns.Add("Submitted Date");
				listOfColumns.Add("Received Date");
				listOfColumns.Add("FilePath");
				listOfColumns.Add("Seed");
			}
			else if (uploadOption == UploadFromExcelOptions.DeleteSignatures.ToString())
			{
				listOfColumns.Add("File Number");
				listOfColumns.Add("ID Number");
				listOfColumns.Add("Version");
				listOfColumns.Add("Game Name");
				listOfColumns.Add("Date Code");
				listOfColumns.Add("Submitted Date");
				listOfColumns.Add("Received Date");
				listOfColumns.Add("Signature");
			}
			else if (uploadOption == UploadFromExcelOptions.AddNewSubmissions.ToString() || uploadOption == UploadFromExcelOptions.ModifySubmissions.ToString())
			{
				listOfColumns.Add("FilePath");
			}
			return listOfColumns;
		}

		private List<string> RequiredColumns(string uploadOption)
		{
			var listOfColumns = new List<string>();
			if (uploadOption == UploadFromExcelOptions.AddNewSubmissions.ToString())
			{

			}
			else if (uploadOption == UploadFromExcelOptions.ModifySubmissions.ToString())
			{
				listOfColumns.Add("Keytbl");
			}
			else if (uploadOption == UploadFromExcelOptions.AddSignatures.ToString())
			{
				listOfColumns.Add("Keytbl");
			}
			else if (uploadOption == UploadFromExcelOptions.ModifySignatures.ToString())
			{
				listOfColumns.Add("Keytbl");
				listOfColumns.Add("Sig Type");
				listOfColumns.Add("Sig Scope");
				listOfColumns.Add("Sig Version");
				listOfColumns.Add("Signature");
			}
			else if (uploadOption == UploadFromExcelOptions.DeleteSignatures.ToString())
			{
				listOfColumns.Add("Keytbl");
				listOfColumns.Add("Sig Type");
				listOfColumns.Add("Sig Scope");
				listOfColumns.Add("Sig Version");
				listOfColumns.Add("Action");
			}

			return listOfColumns.Select(x => x.ToLower()).ToList();
		}

		#endregion

		#region enums
		private enum SubmissionColumn
		{
			Function,
			Game_Type,
			ChipType,
			IdNumber,
			DateCode,
			Version,
			Position,
			GameName,
			MinPercent,
			MaxPercent
		}
		#endregion
	}
}