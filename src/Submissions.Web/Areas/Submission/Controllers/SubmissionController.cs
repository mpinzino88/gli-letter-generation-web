﻿extern alias ZEFCore;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using GLI.EFCore.Submission;
using JiraLibrary;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Submissions.Common;
using Submissions.Core;
using Submissions.Core.Models;
using Submissions.Core.Models.DTO;
using Submissions.Web.Areas.Submission.Models.Submission;
using Submissions.Web.Areas.Submission.Models.Submission.DetailPartialViews;
using Submissions.Web.Areas.Submission.Models.SubmissionBulkUpdate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using ZEFCore::Z.EntityFramework.Plus;

namespace Submissions.Web.Areas.Submission.Controllers
{
	public class SubmissionController : Controller
	{
		private readonly SubmissionContext _dbSubmission;
		private readonly IUserContext _userContext;
		private readonly IConfigContext _configContext;
		private readonly ISubmissionService _submissionService;
		private readonly IJurisdictionalDataService _jurisdictionalDataService;
		private readonly IMemoService _memoService;
		private readonly JiraMethods _jiraMethods;
		private readonly IDocumentService _documentService;

		public SubmissionController
		(
			SubmissionContext dbSubmission,
			IUserContext userContext,
			IConfigContext configContext,
			ISubmissionService submissionService,
			IMemoService memoService,
			JiraMethods jiraMethods,
			IJurisdictionalDataService jurisdictionDataService,
			IDocumentService documentService
		)
		{
			_dbSubmission = dbSubmission;
			_userContext = userContext;
			_configContext = configContext;
			_submissionService = submissionService;
			_memoService = memoService;
			_jiraMethods = jiraMethods;
			_jurisdictionalDataService = jurisdictionDataService;
			_documentService = documentService;
		}

		/// <summary>
		/// Loading by fileNumber or submissionHeaderId will navigate to the first record on file
		/// </summary>
		public ActionResult Detail(int? id, string fileNumber, int? submissionHeaderId)
		{
			SessionMgr.Current.SubmissionDetailPartialView = SessionMgr.Current.SubmissionDetailPartialView ?? "Jurisdictions";

			var vm = new DetailViewModel();

			if (!string.IsNullOrEmpty(fileNumber))
			{
				try
				{
					var validFileNumber = new FileNumber(fileNumber);
					id = _dbSubmission.Submissions
						.Where(x => x.ManufacturerCode == validFileNumber.ManufacturerCode && x.FileNumber == fileNumber)
						.Select(x => x.Id)
						.First();
				}
				catch
				{
					throw new Exception("Not a valid File Number");
				}
			}
			else if (submissionHeaderId != null)
			{
				id = _dbSubmission.Submissions
					.Where(x => x.SubmissionHeaderId == submissionHeaderId)
					.Select(x => x.Id)
					.First();
			}

			var submission = _dbSubmission.Submissions
				.Where(x => x.Id == id)
				.ProjectTo<SubmissionDetailViewModel>()
				.Single();

			var futureJurisdictionalData = _dbSubmission.JurisdictionalData
				.Where(x => x.SubmissionId == id)
				.Select(x => new { x.JurisdictionId })
				.Future();

			var futureProjectId = _dbSubmission.trProjects
				.Where(x => x.FileNumber == submission.FileNumber && x.IsTransfer == false)
				.Select(x => x.Id)
				.DeferredSingleOrDefault()
				.FutureValue();

			var futureSubmissionRecords = _dbSubmission.Submissions
				.Where(x => x.SubmissionHeaderId == submission.SubmissionHeaderId)
				.OrderBy(x => x.Id)
				.Select(x => new SubmissionRecordDetailViewModel
				{
					Id = x.Id,
					Selected = x.Id == id ? true : false,
					GameName = x.GameName,
					IdNumber = x.IdNumber
				})
				.Future();

			var jurisdictionalData = futureJurisdictionalData.ToList();
			var projectId = futureProjectId.Value;
			var submissionRecords = futureSubmissionRecords.ToList();

			if (String.IsNullOrWhiteSpace(submission.MasterFileNumber))
			{
				if (_dbSubmission.Submissions.Where(x => x.MasterFileNumber == submission.FileNumber).Any())
				{
					submission.MasterFileNumber = submission.FileNumber;
					submission.MasterFileLabel = "Master File";
					submission.MasterFileText = "View Summary";
				}
			}
			else
			{
				if (submission.MasterFileNumber == submission.FileNumber)
				{
					submission.MasterFileLabel = "Master File";
					submission.MasterFileText = "View Summary";
				}
				else
				{
					submission.MasterFileLabel = "Original File";
					submission.MasterFileText = submission.MasterFileNumber;
				}
			}

			if (CustomManufacturerGroups.Aristocrat.Contains(submission.ManufacturerCode))
			{
				var mainJurisdiction = _dbSubmission.SubmissionHeaders.Where(x => x.Id == submission.SubmissionHeaderId).Select(x => x.JurisdictionId).SingleOrDefault();
				var aristocratData = _dbSubmission.AristocratData.Where(x => (x.FileNumber == submission.FileNumber && (x.JurisdictionId == null || x.JurisdictionId == "")) || (x.FileNumber == submission.FileNumber && x.JurisdictionId == mainJurisdiction)).FirstOrDefault();
				if (aristocratData != null)
				{
					submission.ProductType = aristocratData.AristocratType;
					submission.Complexity = aristocratData.AristocratComplexity;
					submission.Market = _dbSubmission.AristocratDataMarket.Where(x => x.AristocratDataId == aristocratData.Id).Select(x => x.Market).SingleOrDefault();
				}
			}

			if (submission.Type == SubmissionType.OS.ToString())
			{
				var validMachineCertificateJurisdictionIds = ComUtil.MachineCertificateJurisdictions().Select(x => ((int)x).ToJurisdictionIdString()).ToList();
				if (jurisdictionalData.Any(x => validMachineCertificateJurisdictionIds.Contains(x.JurisdictionId)))
				{
					vm.ShowMachineCertificate = true;
				}
			}

			if (!vm.ShowMachineCertificate && SessionMgr.Current.SubmissionDetailPartialView == "MachineCertificates")
			{
				SessionMgr.Current.SubmissionDetailPartialView = "Jurisdictions";
			}

			submission.ProjectId = projectId;
			submission.IssueCount = GetIssueCount((int)id);
			submission.JurisdictionCount = jurisdictionalData.Count();

			// batch these counts
			var futureMachineCertCount = _dbSubmission.tbl_machine_cert.Where(x => x.SubmissionId == id).DeferredCount().FutureValue();

			var futureMemoCount = _dbSubmission.tblMemos
				.Where(x => x.SubmissionId == id)
				.Select(x => x.Id)
				.Concat(_dbSubmission.tblOHMemos.Where(x => x.SubmissionId == id).Select(x => x.Id))
				.DeferredCount()
				.FutureValue();

			var futureProductCount = _dbSubmission.ProductJurisdictionalData
						.Where(x => x.JurisdictionalData.SubmissionId == id)
						.Select(x => x.ProductId)
						.Distinct()
						.DeferredCount()
						.FutureValue();

			var futureRNGDataCount = _dbSubmission.RNGs.Where(x => x.SubmissionId == id).DeferredCount().FutureValue();
			var futureShipmentCount = _dbSubmission.ShippingData.Where(x => x.FileNumber == submission.FileNumber).DeferredCount().FutureValue();
			var futureSignatureCount = _dbSubmission.tblSignatures.Where(x => x.SubmissionId == id).DeferredCount().FutureValue();

			submission.MachineCertCount = futureMachineCertCount.Value;
			submission.MemoCount = futureMemoCount.Value;
			submission.ProductCount = futureProductCount;
			submission.RNGDataCount = futureRNGDataCount.Value;
			submission.ShipmentCount = futureShipmentCount.Value;
			submission.SignatureCount = futureSignatureCount.Value;

			vm.SubmissionRecords = ComUtil.JsonEncodeCamelCase(submissionRecords);
			vm.SubmissionRecordPaging = ConfigureSubmissionRecordPaging(submissionRecords);
			vm.JiraIssuesUrl = new Uri(new Uri(_configContext.Config.JiraBaseUrl), @"issues/?os_authType=basic&jql=Submissions~\\" + id + " AND Resolution = Unresolved").ToString();
			vm.NetworkDrive = PathUtil.NetworkDrive(submission.ArchiveLocation, new FileNumber(submission.FileNumber));
			vm.Submission = submission;
			vm.ShowOrgIGTMaterial = LoadManufacturerCodesByGroup(CustomManufacturerGroup.IGT, "FlatFile").Contains(submission.ManufacturerCode);
			vm.ShowCabinet = _dbSubmission.tbl_lst_fileManuManufacturerGroups.Where(x => x.ManufacturerGroup.ManufacturerGroupCategory.Code == "Cabinet").Select(x => x.ManufacturerCode).Contains(submission.ManufacturerCode);

			return View(vm);
		}

		public ActionResult Add(int submissionHeaderId, int submissionId)
		{
			var vm = new AddViewModel();
			vm.Source = PageSource.SubmissionDetail;
			vm.SubmissionHeaderId = submissionHeaderId;
			vm.SubmissionId = submissionId;

			var submissions = _dbSubmission.Submissions.AsNoTracking()
				.Where(x => x.SubmissionHeaderId == submissionHeaderId)
				.Select(x => new
				{
					x.FileNumber,
					SubmissionSelection = new SubmissionSelection
					{
						Id = x.Id,
						Status = x.Status,
						Version = x.Version,
						DateCode = x.DateCode,
						Position = x.Position,
						GameName = x.GameName,
						IdNumber = x.IdNumber,
						SubmitDate = x.SubmitDate,
						ReceiveDate = x.ReceiveDate
					}
				})
				.ToList();

			vm.FileNumber = submissions.Max(x => x.FileNumber);
			vm.Submissions = submissions.Select(x => x.SubmissionSelection).OrderBy(x => x.Id).ToList();
			vm.CanAdd = Util.HasAccess(Permission.Submission, AccessRight.Add);

			return View(vm);
		}

		public ActionResult Clone(int submissionHeaderId, int cloneSubmissionId, bool cloneJurisdictions, bool cloneSignatures, bool cloneMemos, string cloneJurisdictionIds, int sourceSubmissionId)
		{
			var now = DateTime.Now;
			var newSubmission = new SubmissionEditViewModel();

			// clone from selected submission
			if (cloneSubmissionId != 0)
			{
				var cloneSubmission = _dbSubmission.Submissions.Where(x => x.Id == cloneSubmissionId).Single();
				newSubmission = Mapper.Map<SubmissionEditViewModel>(cloneSubmission);
				newSubmission.Id = 0;

				var lookupData = new LookupData
				{
					CertificationLab = newSubmission.CertificationLab,
					Lab = newSubmission.Lab,
					Type = newSubmission.Type,
					ChipType = newSubmission.ChipType,
					Function = newSubmission.Function,
					GameType = newSubmission.GameType,
					Vendor = newSubmission.Vendor
				};

				lookupData = _submissionService.HydrateLookupDataByCode(lookupData);

				newSubmission.CertificationLabId = lookupData.CertificationLabId;
				newSubmission.CertificationLab = lookupData.CertificationLabText;
				newSubmission.LabId = lookupData.LabId;
				newSubmission.Lab = lookupData.LabText;
				newSubmission.TypeId = lookupData.TypeId;
				newSubmission.Type = lookupData.TypeText;
				newSubmission.ChipTypeId = lookupData.ChipTypeId;
				newSubmission.FunctionId = lookupData.FunctionId;
				newSubmission.GameTypeId = lookupData.GameTypeId;
				newSubmission.VendorId = lookupData.VendorId;

			}
			// get defaults from first submission
			else
			{
				var cloneSubmission = _dbSubmission.Submissions.Where(x => x.SubmissionHeaderId == submissionHeaderId).OrderBy(x => x.Id).First();

				newSubmission = new SubmissionEditViewModel()
				{
					ArchiveLocation = cloneSubmission.ArchiveLocation,
					CompanyId = (int)cloneSubmission.CompanyId,
					Company = cloneSubmission.Company.Name,
					ContractTypeId = (int)cloneSubmission.ContractTypeId,
					ContractType = cloneSubmission.ContractType.Code + " - " + cloneSubmission.ContractType.Description,
					CurrencyId = (int)cloneSubmission.CurrencyId,
					Currency = cloneSubmission.Currency.Code + " - " + cloneSubmission.Currency.Description,
					FileNumber = cloneSubmission.FileNumber,
					IsRegressionTesting = (cloneSubmission.IsRegressionTesting == true) ? true : false,
					JurisdictionId = cloneSubmission.JurisdictionId,
					LabId = _dbSubmission.trLaboratories.Where(x => x.Location == cloneSubmission.Lab).Select(x => x.Id).FirstOrDefault(),
					ManufacturerCode = cloneSubmission.ManufacturerCode,
					Manufacturer = cloneSubmission.Manufacturer.Description,
					ProjectDetail = cloneSubmission.ProjectDetail,
					TestingPerformedId = (cloneSubmission.TestingPerformedId != null) ? (int)cloneSubmission.TestingPerformedId : (int)TestingPerformed.FullTesting,
					TestingPerformed = (cloneSubmission.TestingPerformedId != null) ? cloneSubmission.TestingPerformed.Code : "",
					TestingTypeId = (int)cloneSubmission.TestingTypeId,
					TestingType = cloneSubmission.TestingType.Code,
					Year = Convert.ToInt32(cloneSubmission.Year)
				};

				var lookupData = new LookupData
				{
					CertificationLab = cloneSubmission.CertificationLab,
					Lab = cloneSubmission.Lab,
					Type = cloneSubmission.Type
				};

				lookupData = _submissionService.HydrateLookupDataByCode(lookupData);

				newSubmission.CertificationLabId = lookupData.CertificationLabId;
				newSubmission.CertificationLab = lookupData.CertificationLabText;
				newSubmission.LabId = lookupData.LabId;
				newSubmission.Lab = lookupData.LabText;
				newSubmission.TypeId = lookupData.TypeId;
				newSubmission.Type = lookupData.TypeText;

				var secondaryFunction = SecondaryFunction.Software.ToString();
				var secondaryFunctionId = _dbSubmission.SubmissionsSecondaryFunctions
					.Where(x => x.Name == secondaryFunction)
					.Select(x => x.Id)
					.Single();

				newSubmission.SecondaryFunction = secondaryFunction;
				newSubmission.SecondaryFunctionId = secondaryFunctionId;
			}

			newSubmission.Status = SubmissionStatus.PN.ToString();

			var vm = new EditViewModel();
			vm.SourceSubmissionId = sourceSubmissionId;
			vm.CanAdd = Util.HasAccess(Permission.Submission, AccessRight.Add);
			vm.Mode = Mode.Add;
			vm.Submission = newSubmission;
			vm.IsCloned = true;
			vm.CloneJurisdictionIds = JsonConvert.DeserializeObject<List<string>>(cloneJurisdictionIds);
			vm.CloneSubmissionId = cloneSubmissionId;
			vm.CloneJurisdictions = cloneJurisdictions;
			vm.CloneSignatures = cloneSignatures;
			vm.CloneMemos = cloneMemos;

			return View("Edit", vm);
		}

		public ActionResult Edit(int? id, Mode mode)
		{
			var now = DateTime.Now;
			var vm = new EditViewModel();
			var vmSubmission = new SubmissionEditViewModel();

			if (mode == Mode.Add)
			{
				var defaults = _dbSubmission.trLaboratories
					.Where(x => x.Id == _userContext.LocationId)
					.Select(x => new
					{
						x.ArchiveLocation,
						x.CompanyId,
						CompanyName = x.Company.Name,
						x.Company.CurrencyId,
						Currency = x.Company.Currency.Code + " - " + x.Company.Currency.Description,
						LabId = x.Id,
						LabLocation = x.Location,
						LabName = x.Name
					})
					.Single();

				var secondaryFunction = SecondaryFunction.Software.ToString();
				var secondaryFunctionId = _dbSubmission.SubmissionsSecondaryFunctions
					.Where(x => x.Name == secondaryFunction)
					.Select(x => x.Id)
					.Single();

				var testingTypeId = _dbSubmission.tbl_lst_TestingType
					.Where(x => x.Code == TestingType.Landbased.ToString())
					.Select(x => x.Id)
					.Single();

				var testingPerformed = TestingPerformed.FullTesting.GetEnumDescription();
				var testingPerformedId = _dbSubmission.tbl_lst_TestingPerformed
					.Where(x => x.Code == testingPerformed)
					.Select(x => x.Id)
					.Single();

				vmSubmission.ArchiveLocation = defaults.ArchiveLocation;
				vmSubmission.CompanyId = defaults.CompanyId;
				vmSubmission.Company = defaults.CompanyName;
				vmSubmission.CertificationLabId = defaults.LabId;
				vmSubmission.CertificationLab = defaults.LabLocation + " - " + defaults.LabName;
				vmSubmission.CurrencyId = defaults.CurrencyId;
				vmSubmission.Currency = defaults.Currency;
				vmSubmission.GameName = "N/A";
				vmSubmission.LabId = defaults.LabId;
				vmSubmission.Lab = defaults.LabLocation + " - " + defaults.LabName;
				vmSubmission.SecondaryFunction = secondaryFunction;
				vmSubmission.SecondaryFunctionId = secondaryFunctionId;
				vmSubmission.Status = SubmissionStatus.PN.ToString();
				vmSubmission.TestingPerformedId = testingPerformedId;
				vmSubmission.TestingPerformed = TestingPerformed.FullTesting.GetEnumDescription();
				vmSubmission.TestingTypeId = testingTypeId;
				vmSubmission.TestingType = TestingType.Landbased.ToString();
				vmSubmission.Year = now.Year;
			}
			else
			{
				var submission = _dbSubmission.Submissions.Where(x => x.Id == id).Single();
				vmSubmission = Mapper.Map<SubmissionEditViewModel>(submission);

				var lookupData = new LookupData
				{
					CertificationLab = vmSubmission.CertificationLab,
					Lab = vmSubmission.Lab,
					Type = vmSubmission.Type,
					ChipType = vmSubmission.ChipType,
					Function = vmSubmission.Function,
					GameType = vmSubmission.GameType,
					Vendor = vmSubmission.Vendor
				};

				lookupData = _submissionService.HydrateLookupDataByCode(lookupData);

				vmSubmission.CertificationLabId = lookupData.CertificationLabId;
				vmSubmission.CertificationLab = lookupData.CertificationLabText;
				vmSubmission.LabId = lookupData.LabId;
				vmSubmission.Lab = lookupData.LabText;
				vmSubmission.TypeId = lookupData.TypeId;
				vmSubmission.Type = lookupData.TypeText;
				vmSubmission.ChipTypeId = lookupData.ChipTypeId;
				vmSubmission.FunctionId = lookupData.FunctionId;
				vmSubmission.GameTypeId = lookupData.GameTypeId;
				vmSubmission.VendorId = lookupData.VendorId;
			}

			vm.Mode = mode;
			vm.Submission = vmSubmission;
			vm.CanAdd = Util.HasAccess(Permission.Submission, AccessRight.Add);
			vm.CanEdit = Util.HasAccess(Permission.Submission, AccessRight.Edit);
			vm.CanDelete = Util.HasAccess(Permission.Submission, AccessRight.Delete);
			vm.CanEditCabinet = _dbSubmission.tbl_lst_fileManuManufacturerGroups.Where(x => x.ManufacturerGroup.ManufacturerGroupCategory.Code == "Cabinet").Select(x => x.ManufacturerCode).Contains(vmSubmission.ManufacturerCode);

			return View(vm);
		}

		[HttpPost]
		public ActionResult Edit(EditViewModel vm)
		{
			var submissionId = vm.Submission.Id;
			var now = DateTime.Now;

			if (vm.Mode == Mode.Add)
			{
				if (!Util.HasAccess(Permission.Submission, AccessRight.Add))
				{
					return RedirectToAction("permissiondenied", "home", new { area = "" });
				}

				// cloning
				if (vm.IsCloned)
				{
					var submission = MapToSubmissionDTO(vm);
					var headerJurisdictionId = _dbSubmission.Submissions.Where(x => x.FileNumber == vm.Submission.FileNumber).Select(x => x.SubmissionHeader.JurisdictionId).FirstOrDefault();

					var query = _dbSubmission.JurisdictionalData.AsQueryable();
					if (vm.CloneSubmissionId != 0 && vm.CloneSubmissionId != null)
					{
						query = query.Where(x => x.SubmissionId == vm.CloneSubmissionId);
						query = query.Where(x => x.JurisdictionId == headerJurisdictionId);
					}
					else
					{
						var tempCloneId = _dbSubmission.Submissions.Where(x => x.FileNumber == vm.Submission.FileNumber).Select(x => x.Id).FirstOrDefault();
						query = query.Where(x => x.SubmissionId == tempCloneId);
						query = query.Where(x => x.JurisdictionId == headerJurisdictionId);
					}
					var mainJurisdiction = query.FirstOrDefault();
					if (mainJurisdiction == null)
					{
						var submissionIds = _dbSubmission.Submissions.Where(x => x.FileNumber == vm.Submission.FileNumber).Select(x => x.Id).ToList();
						mainJurisdiction = _dbSubmission.JurisdictionalData.Where(x => submissionIds.Contains((int)x.SubmissionId) && x.JurisdictionId == headerJurisdictionId).FirstOrDefault();
					}

					if (mainJurisdiction != null && !vm.CloneJurisdictions)
					{
						// only do primary for now
						submission.JurisdictionalData.Add(new JurisdictionalDataDTO
						{
							JurisdictionId = mainJurisdiction.JurisdictionId,
							RequestDate = (DateTime)mainJurisdiction.RequestDate,
							ReceiveDate = (DateTime)mainJurisdiction.ReceiveDate,
							CompanyId = (int)mainJurisdiction.CompanyId,
							CurrencyId = (int)mainJurisdiction.CurrencyId,
							ContractTypeId = (int)mainJurisdiction.ContractTypeId,
							TestingPerformedId = mainJurisdiction.TestingPerformedId,
							LabId = (int)mainJurisdiction.LabId,
							JurisdictionName = mainJurisdiction.JurisdictionName,
							ContractNumber = mainJurisdiction.ContractNumber,
							WON = mainJurisdiction.MasterBillingProject,
							ClientNumber = mainJurisdiction.ClientNumber,
							LetterNumber = mainJurisdiction.LetterNumber,
							PurchaseOrder = mainJurisdiction.PurchaseOrder,
							Active = JurisdictionActiveStatus.No.ToString()
						});
					}

					if (!string.IsNullOrEmpty(vm.Memo))
					{
						submission.Memos.Add(new MemoDTO() { Memo = vm.Memo });
					}

					var addedSubmission = _submissionService.AddRecord(submission);

					// clone here as associated jurisdictions are getting duplicated
					if (vm.CloneJurisdictions)
					{
						var affectedJurs = new List<JurisdictionalData>();
						if (vm.CloneSubmissionId != 0 && vm.CloneSubmissionId != null)
						{
							query = _dbSubmission.JurisdictionalData.AsQueryable();
							query = query.Where(x => x.SubmissionId == vm.CloneSubmissionId);

							if (vm.CloneJurisdictionIds.Count > 0)
							{
								query = query.Where(x => vm.CloneJurisdictionIds.Contains(x.JurisdictionId));
							}

							foreach (var jurisdiction in query.ToList())
							{
								if (!_dbSubmission.JurisdictionalData.Any(x => x.SubmissionId == addedSubmission.Id && x.JurisdictionId == jurisdiction.JurisdictionId))
								{
									var newJurisdiction = new JurisdictionalData
									{
										SubmissionId = addedSubmission.Id,
										JurisdictionId = jurisdiction.JurisdictionId,
										RequestDate = (DateTime)jurisdiction.RequestDate,
										ReceiveDate = (DateTime)jurisdiction.ReceiveDate,
										CompanyId = (int)jurisdiction.CompanyId,
										CurrencyId = (int)jurisdiction.CurrencyId,
										ContractTypeId = (int)jurisdiction.ContractTypeId,
										TestingPerformedId = jurisdiction.TestingPerformedId,
										LabId = (int)jurisdiction.LabId,
										JurisdictionName = jurisdiction.JurisdictionName,
										ContractNumber = jurisdiction.ContractNumber,
										Status = JurisdictionStatus.RA.ToString(),
										MasterBillingProject = jurisdiction.MasterBillingProject, // WON
										ClientNumber = jurisdiction.ClientNumber,
										LetterNumber = jurisdiction.LetterNumber,
										PurchaseOrder = jurisdiction.PurchaseOrder,
										Active = JurisdictionActiveStatus.No.ToString()
									};

									affectedJurs.AddRange(_jurisdictionalDataService.Add(newJurisdiction, true));
								}
							}
						}

						_jurisdictionalDataService.ProcessForProtrackAndAOtherDataForAddedJurisdictions(affectedJurs);
					}

					if (vm.CloneMemos)
					{
						_submissionService.CopyMoveMemo((int)vm.CloneSubmissionId, addedSubmission.Id, true);
					}

					if (vm.CloneSignatures)
					{
						_submissionService.CopyMoveSignatures((int)vm.CloneSubmissionId, addedSubmission.Id, true);
					}

					submissionId = addedSubmission.Id;
				}
				// new submission and project
				else
				{
					var contractTypeCode = (ContractType)Enum.Parse(typeof(ContractType), vm.Submission.ContractType.Split('-')[0].Trim());

					AristocratDataDTO aristocratData = null;
					if (CustomManufacturerGroups.Aristocrat.Contains(vm.Submission.ManufacturerCode) || vm.Submission.ManufacturerCode == CustomManufacturer.GAL.ToString())
					{
						if (contractTypeCode == ContractType.MSA || contractTypeCode == ContractType.IFP || contractTypeCode == ContractType.TME || contractTypeCode == ContractType.SBP)
						{
							if (!string.IsNullOrEmpty(vm.Submission.AristocratTypeId))
							{
								aristocratData = new AristocratDataDTO
								{
									ARPMarket = vm.Submission.AristocratMarketId,
									AristocratComplexity = vm.Submission.AristocratComplexityId,
									AristocratType = vm.Submission.AristocratTypeId
								};
							}
						}
					}

					var won = vm.Submission.WON;
					if (won != null && won.IndexOf("-") > 0)
					{
						won = new FileNumber(won).AccountingFormatNoDashes;
					}

					var jurisdiction = new JurisdictionalDataDTO
					{
						ARIAccountCode = vm.Submission.ARIAccountCode,
						ARIDeptCode = vm.Submission.ARIDeptCode,
						ARIProjectNo = vm.Submission.ARIProjectNo,
						ClientNumber = vm.Submission.ClientNumber,
						CompanyId = (int)vm.Submission.CompanyId,
						ContractNumber = vm.Submission.ContractNumber,
						CurrencyId = (int)vm.Submission.CurrencyId,
						ContractTypeId = (int)vm.Submission.ContractTypeId,
						ElectRequest = false,
						JurisdictionId = vm.Submission.JurisdictionId,
						JurisdictionName = vm.Submission.Jurisdiction,
						LabId = (int)vm.Submission.LabId,
						RequestDate = (DateTime)vm.Submission.SubmitDate,
						ReceiveDate = (DateTime)vm.Submission.ReceiveDate,
						WON = won
					};

					var submission = MapToSubmissionDTO(vm);

					if (!string.IsNullOrEmpty(vm.Memo))
					{
						submission.Memos.Add(new MemoDTO() { Memo = vm.Memo });
					}

					submission.JurisdictionalData = new List<JurisdictionalDataDTO> { jurisdiction };

					var submissionHeader = new SubmissionHeaderDTO
					{
						JurisdictionId = vm.Submission.JurisdictionId,
						ManufacturerCode = vm.Submission.ManufacturerCode,
						Source = SubmissionSource.AddPage,
						Type = vm.Submission.Type.Substring(0, 2),
						Year = vm.Submission.Year.ToString(),
						Submissions = new List<SubmissionDTO> { submission },
						AristocratData = aristocratData
					};

					var result = _submissionService.Add(submissionHeader);
					submissionId = result.Submissions.First().Id;

					// if Aristocrat and PC file
					if (aristocratData != null && vm.Submission.Type.Substring(0, 2) == SubmissionType.PC.ToString() && CustomManufacturerGroups.Aristocrat.Contains(vm.Submission.ManufacturerCode))
					{
						var associations = AssociatedJurisdiction.GetARIMarketAssociation(aristocratData.ARPMarket);
						var mainJurisdiction = _dbSubmission.JurisdictionalData.Where(x => x.SubmissionId == submission.Id && x.JurisdictionId == submissionHeader.JurisdictionId).SingleOrDefault();
						var addedAssociatedJurisdictions = new List<JurisdictionalData>();
						foreach (var juri in associations)
						{
							var jurisdictionId = ((int)juri).ToJurisdictionIdString();

							if (!_dbSubmission.JurisdictionalData.Where(x => x.SubmissionId == submission.Id && x.JurisdictionId == jurisdictionId).Any())
							{
								var newJurisdiction = new JurisdictionalData()
								{
									SubmissionId = submission.Id,
									ARIAccountCode = mainJurisdiction.ARIAccountCode,
									ARIDeptCode = mainJurisdiction.ARIDeptCode,
									ARIProjectNo = mainJurisdiction.ARIProjectNo,
									ClientNumber = mainJurisdiction.ClientNumber,
									PurchaseOrder = mainJurisdiction.PurchaseOrder,
									CompanyId = mainJurisdiction.CompanyId,
									ContractNumber = mainJurisdiction.ContractNumber,
									CurrencyId = mainJurisdiction.CurrencyId,
									ContractTypeId = mainJurisdiction.ContractTypeId,
									ElectRequest = false,
									JurisdictionId = jurisdictionId,
									JurisdictionName = juri.GetEnumDescription(),
									LabId = mainJurisdiction.LabId,
									RequestDate = mainJurisdiction.RequestDate,
									ReceiveDate = mainJurisdiction.ReceiveDate,
									Status = mainJurisdiction.Status,
									Active = JurisdictionActiveStatus.No.ToString(),
									MasterBillingProject = mainJurisdiction.MasterBillingProject
								};

								_jurisdictionalDataService.Add(newJurisdiction, true);
								addedAssociatedJurisdictions.Add(newJurisdiction);
							}
						}
						if (addedAssociatedJurisdictions.Count > 0)
						{
							_jurisdictionalDataService.ProcessForProtrackAndAOtherDataForAddedJurisdictions(addedAssociatedJurisdictions);
						}
					}
				}
			}
			else
			{
				if (!Util.HasAccess(Permission.Submission, AccessRight.Edit))
				{
					return RedirectToAction("permissiondenied", "home", new { area = "" });
				}

				var oldSubmission = _dbSubmission.Submissions
					.Where(x => x.Id == vm.Submission.Id)
					.Select(x => new { x.Status, x.IsRegressionTesting, x.ReceiveDate, x.SubmitDate })
					.SingleOrDefault();

				var submissionDTO = MapToSubmissionDTO(vm);
				_submissionService.Update(submissionDTO);

				if (Convert.ToBoolean(oldSubmission.IsRegressionTesting) != vm.Submission.IsRegressionTesting)
				{
					_dbSubmission.Submissions
						.Where(x => x.FileNumber == vm.Submission.FileNumber)
						.Update(x => new GLI.EFCore.Submission.Submission { IsRegressionTesting = vm.Submission.IsRegressionTesting });
				}

				if (!String.IsNullOrEmpty(vm.Submission.MemoReason) && (vm.Submission.ReceiveDate != oldSubmission.ReceiveDate || vm.Submission.SubmitDate != oldSubmission.SubmitDate))
				{
					var memo = new tblMemo
					{
						AddDate = now,
						Memo = vm.Submission.MemoReason,
						SubmissionId = submissionId,
						Username = _userContext.User.Name
					};

					_memoService.Add(memo);
				}
			}

			return RedirectToAction("detail", new { id = submissionId });
		}

		[AuthorizeUser(Permission = Permission.Submission, HasAccessRight = AccessRight.Delete)]
		public ActionResult Delete(int id)
		{
			var submissionHeaderId = _dbSubmission.Submissions.Where(x => x.Id == id).Select(x => x.SubmissionHeaderId).Single();

			var submissionRecords = _dbSubmission.Submissions
				.Where(x => x.SubmissionHeaderId == submissionHeaderId)
				.OrderBy(x => x.Id)
				.Select(x => new SubmissionRecordDetailViewModel { Id = x.Id, Selected = x.Id == id ? true : false })
				.ToList();

			var paging = ConfigureSubmissionRecordPaging(submissionRecords);

			int? redirectToSubmissionId = null;
			if (paging.NextSubmissionId != id)
			{
				redirectToSubmissionId = paging.NextSubmissionId;
			}
			else if (paging.PrevSubmissionId != id)
			{
				redirectToSubmissionId = paging.PrevSubmissionId;
			}

			_submissionService.Delete(id);

			if (redirectToSubmissionId != null)
			{
				return RedirectToAction("detail", new { id = redirectToSubmissionId });
			}

			return RedirectToAction("index", "home", new { area = "" });
		}

		public ActionResult DraftPortal(int projectId)
		{
			var bundleId = _dbSubmission.QABundles.Where(x => x.ProjectId == projectId).Select(x => x.Id).SingleOrDefault();
			return RedirectToAction("index", "createvue", new { area = "documents", bundleId });
		}

		public ActionResult Summary(int submissionHeaderId)
		{
			var vm = new SummaryViewModel();

			// load jurisdictionaldata, submission data
			var jurisdictionalData = _dbSubmission.JurisdictionalData
				.Where(x => x.Submission.SubmissionHeaderId == submissionHeaderId)
				.Select(x => new
				{
					FileNumber = x.Submission.FileNumber,
					SubmissionId = x.Submission.Id,
					SubmissionDateCode = x.Submission.DateCode,
					SubmissionGameName = x.Submission.GameName,
					SubmissionIdNumber = x.Submission.IdNumber,
					SubmissionPosition = x.Submission.Position,
					SubmissionTestingType = x.Submission.TestingType.Code,
					Id = x.Id,
					JurisdictionId = x.JurisdictionId,
					JurisdictionName = x.JurisdictionName,
					JurisdictionSubmitDate = x.RequestDate,
					JurisdictionReceiveDate = x.ReceiveDate,
					JurisdictionStatus = x.Status
				})
				.ToList();

			// load signature data
			var submissionIds = jurisdictionalData.Select(x => x.SubmissionId).Distinct().ToList();
			var signatures = _dbSubmission.tblSignatures
				.Where(x => submissionIds.Contains((int)x.SubmissionId))
				.Select(x => new SummarySignatureViewModel
				{
					Id = x.Id,
					SubmissionId = (int)x.SubmissionId,
					Scope = x.Scope,
					Signature = x.Signature,
					Type = x.SignatureType.Type,
					Version = x.VerifyVersion,
					FilePath = x.FilePath
				})
				.ToList();

			// format data
			var submissions = jurisdictionalData
				.GroupBy(x => x.SubmissionId)
				.OrderBy(x => x.Key)
				.Select(x => new SummarySubmissionViewModel
				{
					Id = x.Key,
					DateCode = x.Select(y => y.SubmissionDateCode).First(),
					IdNumber = x.Select(y => y.SubmissionIdNumber).First(),
					GameName = x.Select(y => y.SubmissionGameName).First(),
					Position = x.Select(y => y.SubmissionPosition).First(),
					TestingType = x.Select(y => y.SubmissionTestingType).First(),
					Jurisdictions = x
						.OrderBy(y => y.JurisdictionName)
						.Select(y => new SummaryJurisdictionViewModel
						{
							Id = y.Id,
							JurisdictionId = y.JurisdictionId,
							Name = y.JurisdictionName,
							SubmitDate = y.JurisdictionSubmitDate,
							ReceiveDate = y.JurisdictionReceiveDate,
							Status = y.JurisdictionStatus
						})
						.ToList()
				})
				.ToList();

			foreach (var s in submissions)
			{
				s.ShowSigFilePath = (s.TestingType == TestingType.iGaming.ToString() || s.TestingType == TestingType.Sportsbook.ToString());
				s.Signatures = signatures.Where(x => x.SubmissionId == s.Id).ToList();
			}

			vm.FileNumber = jurisdictionalData.Select(x => x.FileNumber).First();
			vm.Submissions = submissions;

			return View(vm);
		}

		#region Helper Methods
		private SubmissionRecordPagingDetailViewModel ConfigureSubmissionRecordPaging(List<SubmissionRecordDetailViewModel> submissionRecords)
		{
			var selected = submissionRecords.Where(x => x.Selected).Single();
			var page = submissionRecords.IndexOf(selected);

			return new SubmissionRecordPagingDetailViewModel
			{
				FirstSubmissionId = submissionRecords.Select(x => x.Id).First(),
				LastSubmissionId = submissionRecords.Select(x => x.Id).Last(),
				NextSubmissionId = (page + 1 < submissionRecords.Count) ? submissionRecords[page + 1].Id : selected.Id,
				PrevSubmissionId = (page - 1 > -1) ? submissionRecords[page - 1].Id : selected.Id,
				Page = page + 1,
				Total = submissionRecords.Count
			};
		}

		private string GetIssueCount(int submissionId)
		{
			var issueCount = "N/A";

			// try/catch to prevent page from breaking when Jira is down
			try
			{
				var filterList = new List<JiraLibrary.Filter>
				{
					new JiraLibrary.Filter
					{
						filterType = FilterTypes.Resolution,
						filterOperator = FilterOpterators.Equals,
						filterValue = "Unresolved",
						keyword = Keywords.NONE
					}
				};

				var additionalFilters = new List<string>();
				additionalFilters.Add(String.Format(@"Submissions~\\{0}", submissionId));

				issueCount = _jiraMethods.countIssues(filterList, additionalFilters).ToString();
			}
			catch { }

			return issueCount;
		}

		private SubmissionDTO MapToSubmissionDTO(EditViewModel vm)
		{
			return new SubmissionDTO
			{
				Id = vm.Submission.Id,
				ArchiveLocation = vm.Submission.ArchiveLocation,
				CabinetId = vm.Submission.CabinetId,
				CertificationLabId = (int)vm.Submission.CertificationLabId,
				ChipTypeId = vm.Submission.ChipTypeId,
				CompanyId = (int)vm.Submission.CompanyId,
				ContractNumber = vm.Submission.ContractNumber,
				ContractTypeId = (int)vm.Submission.ContractTypeId,
				ContractValue = vm.Submission.ContractValue,
				CurrencyId = (int)vm.Submission.CurrencyId,
				DateCode = vm.Submission.DateCode,
				FileNumber = vm.Submission.FileNumber,
				FunctionId = (int)vm.Submission.FunctionId,
				GameName = vm.Submission.GameName,
				GameTypeId = vm.Submission.GameTypeId,
				IdNumber = vm.Submission.IdNumber,
				IsRegressionTesting = vm.Submission.IsRegressionTesting,
				LabId = (int)vm.Submission.LabId,
				LetterNumber = vm.Submission.LetterNumber,
				ManufacturerBuildId = vm.Submission.ManufacturerBuildId,
				ManufacturerCode = vm.Submission.ManufacturerCode,
				Operator = vm.Submission.Operator,
				OrgIGTMaterial = vm.Submission.OrgIGTMaterial,
				Position = vm.Submission.Position,
				ProjectDetail = vm.Submission.ProjectDetail,
				PurchaseOrder = vm.Submission.PurchaseOrder == null ? null : vm.Submission.PurchaseOrder.ToString(),
				ReceiveDate = vm.Submission.ReceiveDate.Value,
				SecondaryFunctionId = (int)vm.Submission.SecondaryFunctionId,
				Status = vm.Submission.Status,
				System = vm.Submission.System,
				SubmitDate = vm.Submission.SubmitDate.Value,
				TestingPerformedId = vm.Submission.TestingPerformedId == null ? null : vm.Submission.TestingPerformedId,
				TestingTypeId = (int)vm.Submission.TestingTypeId,
				VendorId = vm.Submission.VendorId,
				Version = vm.Submission.Version,
				WorkPerformedId = vm.Submission.WorkPerformedId,
				Year = vm.Submission.Year.ToString(),
				ComponentTypeId = vm.Submission.ComponentTypeId
			};
		}

		private IList<string> LoadManufacturerCodesByGroup(CustomManufacturerGroup manufacturerGroup, string groupCategory)
		{
			return _dbSubmission.tbl_lst_fileManuManufacturerGroups
				.Where(x => x.ManufacturerGroup.Description == manufacturerGroup.ToString() && x.ManufacturerGroup.ManufacturerGroupCategory.Code == groupCategory)
				.Select(x => x.ManufacturerCode)
				.ToList();
		}
		#endregion

		#region Partial Views
		public ActionResult AssociatedSubmissions(int submissionId)
		{
			var submission = _dbSubmission.Submissions
			   .Where(x => x.Id == submissionId)
			   .Select(x => new { x.FileNumber, x.SubmissionHeaderId })
			   .Single();

			var submissions = _dbSubmission.Submissions
			   .Where(x => x.SubmissionHeaderId == submission.SubmissionHeaderId)
			   .OrderBy(x => x.Id)
			   .ProjectTo<AssociatedSubmissionViewModel>()
			   .ToList();

			var vm = new AssociatedSubmissionsViewModel
			{
				SubmissionId = submissionId,
				FileNumber = submission.FileNumber,
				AssociatedSubmissions = submissions
			};

			return PartialView("~/areas/submission/views/submission/detailpartialviews/_associatedsubmissions.cshtml", vm);
		}

		public ActionResult BugBoxes(int submissionId)
		{
			SessionMgr.Current.SubmissionDetailPartialView = "BugBoxes";

			var bugBoxes = _dbSubmission.trBugBoxes
			   .Where(x => x.SubmissionId == submissionId)
			   .OrderBy(x => x.Lab.Location)
			   .Select(x => new BugBoxViewModel
			   {
				   Id = x.Id,
				   BugBox = x.BugBox,
				   Location = x.Lab.Location
			   })
			   .ToList();

			var vm = new BugBoxesViewModel
			{
				SubmissionId = submissionId,
				BugBoxes = bugBoxes
			};

			return PartialView("~/areas/submission/views/submission/detailpartialviews/_bugboxes.cshtml", vm);
		}

		public ActionResult CertificationNumbers(int jurisdictionalDataId)
		{
			var certificationNumbers = _dbSubmission.RegCertId_JurisdictionalData
				.Where(x => x.JurisdictionalDataId == jurisdictionalDataId)
				.Select(x => new CertificationNumberViewModel
				{
					Id = x.Id,
					CertificationNumber = x.CertificationNumber.CertNumber,
					CertificationNumberId = x.CertificationNumberId,
					DocumentPath = x.CertificationNumber.Document.Path
				})
				.ToList();

			var jurisdictionalData = _dbSubmission.JurisdictionalData
				.Where(x => x.Id == jurisdictionalDataId)
				.Select(x => new
				{
					x.Submission.FileNumber,
					x.JurisdictionId,
					JurisidictionName = x.JurisdictionName
				})
				.Single();

			var vm = new CertificationNumbersViewModel
			{
				FileNumber = jurisdictionalData.FileNumber,
				JurisdictionId = jurisdictionalData.JurisdictionId,
				JurisdictionName = jurisdictionalData.JurisidictionName,
				CertificationNumbers = certificationNumbers
			};

			return PartialView("~/areas/submission/views/submission/detailpartialviews/_certificationnumbers.cshtml", vm);
		}

		public ActionResult CodeStorages(int submissionId)
		{
			SessionMgr.Current.SubmissionDetailPartialView = "CodeStorages";

			var codeStorages = _dbSubmission.SourceCodeStorage
			   .Where(x => x.SubmissionId == submissionId)
			   .OrderBy(x => x.Lab.Location)
			   .Select(x => new CodeStorageViewModel
			   {
				   Id = x.Id,
				   CodeStorage = x.CodeStorage,
				   Location = x.Lab.Location
			   })
			   .ToList();

			var vm = new CodeStoragesViewModel
			{
				SubmissionId = submissionId,
				CodeStorages = codeStorages
			};

			return PartialView("~/areas/submission/views/submission/detailpartialviews/_codestorages.cshtml", vm);
		}

		[HttpGet]
		public ActionResult DeleteJurisdictionSelection(int jurisdictionDataId, int submissionHeaderId, int submissionId)
		{
			var vm = new JurisdictionDeleteSubmissions();
			vm.SubmissionHeaderId = submissionHeaderId;
			vm.SubmissionId = submissionId;

			var submissionHeader = _dbSubmission.SubmissionHeaders
				.Where(x => x.Id == submissionHeaderId)
				.Select(x => new { x.FileNumber })
				.Single();

			var jurisdictionInfo = _dbSubmission.JurisdictionalData.Where(x => x.Id == jurisdictionDataId).Select(x => new { Name = x.JurisdictionName, Id = x.JurisdictionId }).FirstOrDefault();
			vm.JurisdictionId = jurisdictionInfo.Id;
			vm.JurisdictionName = jurisdictionInfo.Name;

			vm.SubmissionHeaderId = submissionHeaderId;
			vm.SubmissionId = submissionId;
			vm.FileNumber = submissionHeader.FileNumber;

			var components = _dbSubmission.Submissions
				.Where(x => x.SubmissionHeaderId == submissionHeaderId)
				.OrderBy(x => x.Id)
				.ProjectTo<ComponentViewModel>()
				.ToList();
			if (components.Count == 1)
			{
				ViewBag.Message = "Delete action is rejected, doing so will remove the only submission for the selected Jurisdiction.";
				return PartialView("~/areas/submission/views/submission/detailpartialviews/_rejectdeletesubmissions.cshtml");
			}
			components.Where(x => x.Id == submissionId).FirstOrDefault().IsChecked = true;

			vm.Components = components;
			return PartialView("~/areas/submission/views/submission/detailpartialviews/_jurisdictiondeletesubmissions.cshtml", vm);

		}

		[ChildActionOnly]
		public ActionResult CopyToSubmissions(int submissionId)
		{
			var submissionHeaderId = _dbSubmission.Submissions
				.Where(x => x.Id == submissionId)
				.Select(x => x.SubmissionHeaderId)
				.Single();

			var copyToSubmissions = _dbSubmission.Submissions
				.AsNoTracking()
				.Where(x => x.SubmissionHeaderId == submissionHeaderId)
				.ProjectTo<SubmissionSelection>()
				.OrderBy(x => x.Id)
				.ToList();

			var vm = new CopyToSubmissionsViewModel
			{
				SourceSubmissionId = submissionId,
				CopyToSubmissions = copyToSubmissions
			};

			return PartialView("~/areas/submission/views/submission/detailpartialviews/_copytosubmissions.cshtml", vm);
		}

		public ActionResult Jurisdictions(int submissionId)
		{
			SessionMgr.Current.SubmissionDetailPartialView = "Jurisdictions";

			var fileNumber = _dbSubmission.Submissions
				.Where(x => x.Id == submissionId)
				.Select(x => x.FileNumber)
				.Single();

			var jurisdictions = _dbSubmission.JurisdictionalData
				.Where(x => x.SubmissionId == submissionId)
				.OrderBy(x => x.JurisdictionName)
				.ProjectTo<JurisdictionViewModel>()
				.ToList();

			var standardJurisdictionIds = _dbSubmission.tbl_lst_StandardsAll.Where(x => x.Active).Select(x => x.JurisdictionId).Distinct().ToList();
			foreach (var j in jurisdictions)
			{
				if (standardJurisdictionIds.Contains(j.JurisdictionId))
				{
					j.ShowStandards = true;
				}
				if (j.Status == JurisdictionStatus.DR.ToString() || (j.Status == JurisdictionStatus.LQ.ToString() && j.CloseDate == null))
				{
					j.PdfDraftPath = _dbSubmission.SubmissionDrafts.Where(x => x.JurisdictionDataId == j.Id).Select(x => x.DraftLetter.FilePath).FirstOrDefault();
					var gpsId = _dbSubmission.SubmissionDrafts.Where(x => x.JurisdictionDataId == j.Id).Select(x => x.DraftLetter.PdfFileGpsId).FirstOrDefault() ?? default(int);
					j.PdfDraftGpsPath = _documentService.GetFilePath(gpsId);
				}
				var projectIds = _dbSubmission.trProjectJurisdictionalData.Where(x => x.JurisdictionalDataId == j.Id).Select(x => x.ProjectId).ToList();
				foreach (var projectId in projectIds)
				{
					j.IsLastJurisdiction = _dbSubmission.trProjects.Where(x => x.Id == projectId).Select(x => x.JurisdictionalData.Count()).FirstOrDefault() == 1;
					if (j.IsLastJurisdiction)
					{
						break;
					}
				}
			}

			var vm = new JurisdictionsViewModel
			{
				SubmissionId = submissionId,
				FileNumber = fileNumber,
				Jurisdictions = jurisdictions
			};

			return PartialView("~/areas/submission/views/submission/detailpartialviews/_jurisdictions.cshtml", vm);
		}

		public ActionResult JurisdictionsAlt(int submissionId)
		{
			var jurisdictions = _dbSubmission.JurisdictionalData
				.Where(x => x.SubmissionId == submissionId)
				.OrderBy(x => x.JurisdictionName)
				.ProjectTo<JurisdictionAltViewModel>()
				.ToList();

			var vm = new JurisdictionsAltViewModel
			{
				SubmissionId = submissionId,
				Jurisdictions = jurisdictions
			};

			return PartialView("~/areas/submission/views/submission/detailpartialviews/_jurisdictionsalt.cshtml", vm);
		}

		public ActionResult MachineCertificates(int submissionId)
		{
			SessionMgr.Current.SubmissionDetailPartialView = "MachineCertificates";

			var machineCertificates = _dbSubmission.tbl_machine_cert
				.Where(x => x.SubmissionId == submissionId)
				.OrderByDescending(x => x.CertificateDate)
				.Select(x => new MachineCertificateViewModel
				{
					Id = x.Id,
					CertificateDate = x.CertificateDate,
					CertificateNumber = x.CertificateNumber,
					DeviceType = x.DeviceType,
					Jurisdiction = x.Jurisdiction.Name,
					JurisdictionId = x.JurisdictionId,
					SealNumber = x.SealNumber,
					SerialNumber = x.SerialNumber,
					CertificatesThatWereReplaced = x.ReplacementCertificates.Select(y => y.CertificateNumber).ToList()
				})
				.ToList();

			var vm = new MachineCertificatesViewModel
			{
				SubmissionId = submissionId,
				MachineCertificates = machineCertificates
			};

			return PartialView("~/areas/submission/views/submission/detailpartialviews/_machinecertificates.cshtml", vm);
		}

		public ActionResult Memos(int submissionId)
		{
			var memoGNType = MemoType.GN.ToString();
			var memoOHType = MemoType.OH.ToString();
			var memoOHText = MemoType.OH.GetEnumDescription();

			SessionMgr.Current.SubmissionDetailPartialView = "Memos";

			var memos = _dbSubmission.tblMemos
				.Where(x => x.SubmissionId == submissionId)
				.Select(x => new MemoViewModel
				{
					Id = x.Id,
					AddDate = x.AddDate,
					Memo = x.Memo,
					Type = memoGNType,
					TypeText = string.Empty,
					Username = x.Username
				})
				.AsEnumerable()
				.Concat
				(
					_dbSubmission.tblOHMemos
					.Where(x => x.SubmissionId == submissionId)
					.Select(x => new MemoViewModel
					{
						Id = x.Id,
						AddDate = x.AddDate,
						Memo = x.Memo,
						Type = memoOHType,
						TypeText = memoOHText,
						Username = x.Username
					})
				)
				.OrderByDescending(x => x.AddDate)
				.ToList();

			var vm = new MemosViewModel
			{
				SubmissionId = submissionId,
				Memos = memos
			};

			return PartialView("~/areas/submission/views/submission/detailpartialviews/_memos.cshtml", vm);
		}

		public ActionResult Products(int submissionId)
		{

			var vm = new ProductsViewModel
			{
				SubmissionId = submissionId,
				Products =
					_dbSubmission.ProductJurisdictionalData
						.Where(x => x.JurisdictionalData.SubmissionId == submissionId)
						.DistinctBy(x => x.ProductId)
						.Select(x => new ProductViewModel { Id = x.ProductId, Name = x.Product.Name, Type = x.ProductJurisdictionalDataLinkType.Type, Version = x.Product.Version })
						.ToList()
			};

			return PartialView("~/areas/submission/views/submission/detailpartialviews/_products.cshtml", vm);
		}

		public ActionResult RNGs(int submissionId)
		{
			SessionMgr.Current.SubmissionDetailPartialView = "RNGs";

			var submissionHeader = _dbSubmission.Submissions
				.Where(x => x.Id == submissionId)
				.Select(x => new { Id = x.SubmissionHeaderId, x.FileNumber })
				.Single();

			var rngs = _dbSubmission.RNGs
				.Where(x => x.SubmissionHeaderId == submissionHeader.Id)
				.Select(x => new
				{
					x.Id,
					x.TestCase,
					x.RangeStart,
					x.RangeEnd,
					x.Draws,
					x.Replacement,
					x.Selections,
					x.Notes,
					ConfidenceLevels = x.ConfidenceLevels.Select(y => y.ConfidenceLevel).ToList()
				})
				.ToList()
				.Select(x => new RNGViewModel
				{
					Id = x.Id,
					TestCase = x.TestCase,
					RangeStart = x.RangeStart,
					RangeEnd = x.RangeEnd,
					Draws = x.Draws,
					Replacement = x.Replacement,
					Selections = x.Selections,
					Notes = x.Notes,
					ConfidenceLevels = String.Join(", ", x.ConfidenceLevels)
				})
				.ToList();

			var vm = new RNGsViewModel
			{
				SubmissionId = submissionId,
				FileNumber = submissionHeader.FileNumber,
				RNGs = rngs
			};

			return PartialView("~/areas/submission/views/submission/detailpartialviews/_rngs.cshtml", vm);
		}

		public ActionResult Shipments(int submissionId)
		{
			SessionMgr.Current.SubmissionDetailPartialView = "Shipments";

			var fileNumber = _dbSubmission.Submissions
				.Where(x => x.Id == submissionId)
				.Select(x => x.FileNumber)
				.Single();

			var shipments = _dbSubmission.ShippingData
				.Where(x => x.FileNumber == fileNumber)
				.OrderByDescending(x => x.Id)
				.Select(x => new
				{
					x.Id,
					x.Type,
					x.ShippingRecipientCode,
					x.ProcessDate,
					x.Courier,
					x.TrackingNumber,
					x.ChipCount,
					x.DiskCount,
					x.Memo,
					SoftwareCheckUser = x.SoftwareCheckUser.FName + " " + x.SoftwareCheckUser.LName,
					x.SoftwareCheckDate
				})
				.ToList();

			var vm = new ShipmentsViewModel
			{
				SubmissionId = submissionId,
				Shipments = Mapper.Map<List<ShipmentViewModel>>(shipments)
			};

			foreach (var item in vm.Shipments)
			{
				DateTime processDate;
				if (DateTime.TryParse(item.ProcessDate, out processDate))
				{
					item.ProcessDate = processDate.Date.ToShortDateString();
				}
			}

			return PartialView("~/areas/submission/views/submission/detailpartialviews/_shipments.cshtml", vm);
		}

		public ActionResult Signatures(int submissionId)
		{
			SessionMgr.Current.SubmissionDetailPartialView = "Signatures";

			var testingType = _dbSubmission.Submissions
				.Where(x => x.Id == submissionId)
				.Select(x => x.TestingType.Code)
				.SingleOrDefault();

			var signatures = _dbSubmission.tblSignatures
				.Where(x => x.SubmissionId == submissionId)
				.Select(x => new SignatureViewModel
				{
					Id = x.Id,
					Type = x.Type,
					Scope = x.Scope,
					Signature = x.Signature,
					Version = x.VerifyVersion,
					AlphaVersion = x.ComponentID,
					Seed = x.Seed,
					FilePath = x.FilePath
				})
				.ToList();

			var vm = new SignaturesViewModel
			{
				SubmissionId = submissionId,
				Signatures = signatures,
				ShowFilePath = (testingType == TestingType.iGaming.ToString() || testingType == TestingType.Sportsbook.ToString())
			};

			return PartialView("~/areas/submission/views/submission/detailpartialviews/_signatures.cshtml", vm);
		}

		public ActionResult ReplaceWith(int submissionId)
		{
			SessionMgr.Current.SubmissionDetailPartialView = "ReplaceWith";
			var vm = GetReplaceWithData(submissionId);
			return PartialView("~/areas/submission/views/submission/detailpartialviews/_ReplaceWith.cshtml", vm);

		}

		private ReplaceWithViewModel GetReplaceWithData(int submissionId)
		{
			var vm = new ReplaceWithViewModel();
			var results = (from sr in _dbSubmission.SubmissionReplaceWiths
						   where sr.SubmissionId == submissionId && sr.JurisdictionalDataId == null
						   select new SubmissionReplaceWithVM
						   {
							   IdNumber = sr.ReplaceWith
						   }).ToList();

			var jurResults = (from sr in _dbSubmission.SubmissionReplaceWiths
							  where sr.JurisdictionalData.SubmissionId == submissionId
							  select new JurisdictionReplaceWithVM
							  {
								  FileNumber = sr.ReplaceWithJurisdictionalData.Submission.FileNumber,
								  GameName = sr.ReplaceWithJurisdictionalData.Submission.GameName,
								  IdNumber = sr.ReplaceWithJurisdictionalData.Submission.IdNumber,
								  Version = sr.ReplaceWithJurisdictionalData.Submission.Version,
								  SubmissionId = (int)sr.ReplaceWithJurisdictionalData.SubmissionId,
								  JurisdictionName = sr.ReplaceWithJurisdictionalData.JurisdictionName,
								  ReplaceWith = sr.ReplaceWith
							  }).OrderBy(x => x.JurisdictionName).ToList();

			vm.SubmissionReplacements = results;
			vm.JurisdictionReplacements = jurResults;
			return vm;
		}
		#endregion

		#region Other Ajax Calls

		public ActionResult GetJurisdictionsForSelection(int submissionId)
		{
			var jurisdictions = _dbSubmission.JurisdictionalData.Where(x => x.SubmissionId == submissionId)
								.Select(x => new
								{
									x.Id,
									x.Submission.IdNumber,
									x.Submission.GameName,
									x.Submission.DateCode,
									x.Submission.Function,
									x.JurisdictionId,
									x.JurisdictionName,
									x.ReceiveDate,
									x.RequestDate,
									x.Status,
									Selected = true
								}).ToList();

			return Content(ComUtil.JsonEncodeCamelCase(jurisdictions), "application/json");
		}

		public ActionResult LoadArchiveLocation(int labId)
		{
			var archiveLocation = _dbSubmission.trLaboratories
				.Where(x => x.Id == labId)
				.Select(x => x.ArchiveLocation)
				.SingleOrDefault();

			return Content(ComUtil.JsonEncodeCamelCase(archiveLocation), "application/json");
		}

		public ActionResult LoadCompany(int companyId)
		{
			var currency = _dbSubmission.tbl_lst_Company
				.Where(x => x.Id == companyId)
				.Select(x => new
				{
					Id = x.CurrencyId,
					Code = x.Currency.Code + " - " + x.Currency.Description
				})
				.SingleOrDefault();

			var lab = _dbSubmission.trLaboratories
				.Where(x => x.CompanyId == companyId && x.Active == 1)
				.OrderBy(x => x.Id)
				.Select(x => new
				{
					x.Id,
					Code = x.Location + " - " + x.Name
				})
				.FirstOrDefault();

			var userDefaults = _dbSubmission.trLaboratories
				.Where(x => x.Id == _userContext.LocationId)
				.Select(x => new
				{
					x.Company.CurrencyId,
					Currency = x.Company.Currency.Code + " - " + x.Company.Currency.Description,
					LabId = x.Id,
					Lab = x.Name
				})
				.Single();

			var defaults = new
			{
				CurrencyId = currency == null ? userDefaults.CurrencyId : currency.Id,
				Currency = currency == null ? userDefaults.Currency : currency.Code,
				LabId = lab == null ? userDefaults.LabId : lab.Id,
				Lab = lab == null ? userDefaults.Lab : lab.Code
			};

			return Content(ComUtil.JsonEncodeCamelCase(defaults), "application/json");
		}

		public ActionResult LoadManufacturer(string manufacturerCode)
		{
			var manufacturer = _dbSubmission.tbl_lst_fileManu
				.Where(x => x.Code == manufacturerCode)
				.Select(x => x.AccountStatus.Status)
				.Single();

			manufacturer = manufacturer ?? "Not Found";

			return Content(ComUtil.JsonEncodeCamelCase(manufacturer), "application/json");
		}

		public ActionResult MoreInfo(int submissionId)
		{
			var bugBoxCount = _dbSubmission.trBugBoxes
				.Where(x => x.SubmissionId == submissionId)
				.Count();

			var machineCertificateCount = _dbSubmission.tbl_machine_cert
				.Where(x => x.SubmissionId == submissionId)
				.Count();

			var codeStorageCount = _dbSubmission.SourceCodeStorage
				.Where(x => x.SubmissionId == submissionId)
				.Count();

			var replaceWithCount = GetReplaceWithData(submissionId).SubmissionReplacements.Count() + GetReplaceWithData(submissionId).JurisdictionReplacements.Count();

			var moreInfo = new
			{
				BugBoxCount = bugBoxCount,
				MachineCertificateCount = machineCertificateCount,
				CodeStorageCount = codeStorageCount,
				ReplaceWithCount = replaceWithCount,
			};

			return Content(ComUtil.JsonEncodeCamelCase(moreInfo), "application/json");
		}
		#endregion
	}
}