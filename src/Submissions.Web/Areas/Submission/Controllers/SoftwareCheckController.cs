﻿using GLI.EFCore.Submission;
using Submissions.Common;
using Submissions.Web.Areas.Submission.Models.SoftwareCheck;
using System;
using System.Dynamic;
using System.Web.Mvc;

namespace Submissions.Web.Areas.Submission.Controllers
{
	public class SoftwareCheckController : Controller
	{
		private readonly SubmissionContext _dbSubmission;
		private readonly IUserContext _userContext;

		public SoftwareCheckController(SubmissionContext dbSubmission, IUserContext userContext)
		{
			_dbSubmission = dbSubmission;
			_userContext = userContext;
		}

		public ActionResult Index()
		{
			var vm = new IndexViewModel();
			vm.Filters.LocationId = _userContext.LocationId;
			vm.Filters.Location = _userContext.Location + " - " + _userContext.LocationName;

			return View(vm);
		}

		[HttpPost]
		public ActionResult Edit(int shipmentId, EditAction editAction)
		{
			dynamic result = new ExpandoObject();
			var shipment = new ShippingData { Id = shipmentId };

			if (editAction == EditAction.Accept)
			{
				shipment.SoftwareCheckUserId = _userContext.User.Id;
				_dbSubmission.ShippingData.Attach(shipment);
				_dbSubmission.Entry(shipment).Property(x => x.SoftwareCheckUserId).IsModified = true;
				_dbSubmission.SaveChanges();

				result.SoftwareCheckUser = _userContext.User.FName + " " + _userContext.User.LName;
			}
			else if (editAction == EditAction.Unaccept)
			{
				shipment.SoftwareCheckUserId = null;
				_dbSubmission.ShippingData.Attach(shipment);
				_dbSubmission.Entry(shipment).Property(x => x.SoftwareCheckUserId).IsModified = true;
				_dbSubmission.SaveChanges();

				result.SoftwareCheckUser = "";
			}
			else if (editAction == EditAction.Complete)
			{
				shipment.SoftwareCheckUserId = _userContext.User.Id;
				shipment.SoftwareCheckDate = DateTime.Now;
				shipment.SoftwareWithEngineering = false;
				_dbSubmission.ShippingData.Attach(shipment);
				_dbSubmission.Entry(shipment).Property(x => x.SoftwareCheckUserId).IsModified = true;
				_dbSubmission.Entry(shipment).Property(x => x.SoftwareCheckDate).IsModified = true;
				_dbSubmission.Entry(shipment).Property(x => x.SoftwareWithEngineering).IsModified = true;
				_dbSubmission.SaveChanges();
			}
			else if (editAction == EditAction.InEngYes)
			{
				shipment.SoftwareWithEngineering = true;
				_dbSubmission.ShippingData.Attach(shipment);
				_dbSubmission.Entry(shipment).Property(x => x.SoftwareWithEngineering).IsModified = true;
				_dbSubmission.SaveChanges();
			}
			else if (editAction == EditAction.InEngNo)
			{
				shipment.SoftwareWithEngineering = false;
				_dbSubmission.ShippingData.Attach(shipment);
				_dbSubmission.Entry(shipment).Property(x => x.SoftwareWithEngineering).IsModified = true;
				_dbSubmission.SaveChanges();
			}

			return Content(ComUtil.JsonEncodeCamelCase(result), "application/json");
		}
	}
}