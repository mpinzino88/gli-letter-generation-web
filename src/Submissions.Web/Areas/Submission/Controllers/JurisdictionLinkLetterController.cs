﻿using GLI.EFCore.Submission;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Submissions.Common;
using Submissions.Web.Areas.Submission.Models.JurisdictionLinkLetter;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace Submissions.Web.Areas.Submission.Controllers
{
	public class JurisdictionLinkLetterController : Controller
	{
		private readonly SubmissionContext _dbSubmission;

		public JurisdictionLinkLetterController(SubmissionContext dbSubmission)
		{
			_dbSubmission = dbSubmission;
		}

		/// <summary>
		/// This links Jurisdiction Certification and requires JurisdictionDataID.
		/// </summary>
		/// <param name="jurisdictionDataId">JurisdictionDataID</param>
		public ActionResult Index(int jurisdictionDataId)
		{
			var vm = new JurisdictionLinkLetterViewModel();
			var jurisdiction = _dbSubmission.JurisdictionalData
				.Where(x => x.Id == jurisdictionDataId)
				.Select(x => new SubmissionDataInfo()
				{
					Id = x.Id,
					FileNumber = x.Submission.FileNumber,
					GameName = x.Submission.GameName,
					IdNumber = x.Submission.IdNumber,
					Version = x.Submission.Version,
					JurisdictionName = x.JurisdictionName,
					JurisdictionId = x.JurisdictionId
				}
				).SingleOrDefault();

			vm.JurisdictionDataId = jurisdictionDataId;

			vm.CurrentSubmissionInfo = jurisdiction;
			vm.Search.JurisdictionId = jurisdiction.JurisdictionId; //Search for same jurisdiction
			vm.LinkOption = LinkOptions.Certification.ToString();
			return View("Index", vm);
		}

		/// <summary>
		/// This links Jurisdiction Drafts and requires JurisdictionDataID and current DraftId.
		/// </summary>
		/// <param name="jurisdictionDataId">JurisdictionDataID</param>
		/// <param name="draftId">Draft Id</param>
		public ActionResult LinkDrafts(int jurisdictionDataId, int draftId)
		{
			var vm = new JurisdictionLinkLetterViewModel();
			var jurisdiction = _dbSubmission.JurisdictionalData
				.Where(x => x.Id == jurisdictionDataId)
				.Select(x => new SubmissionDataInfo()
				{
					Id = x.Id,
					FileNumber = x.Submission.FileNumber,
					GameName = x.Submission.GameName,
					IdNumber = x.Submission.IdNumber,
					Version = x.Submission.Version,
					JurisdictionName = x.JurisdictionName,
					JurisdictionId = x.JurisdictionId
				}
				).SingleOrDefault();

			vm.JurisdictionDataId = jurisdictionDataId;
			vm.DraftId = draftId;

			vm.CurrentSubmissionInfo = jurisdiction;
			vm.Search.JurisdictionId = jurisdiction.JurisdictionId; //Search for same jurisdiction
			vm.LinkOption = LinkOptions.Draft.ToString();
			return View("Index", vm);
		}

		/// <summary>
		/// This links Jurisdiction/Submission record's supplementary letters and you can also apply or post to other records.
		/// </summary>
		/// <param name="jurisdictionDataId">JurisdictionDataID</param>
		/// <param name="submissionId">Submission Id</param>
		/// <param name="applyToSubmissionIds">Apply to Other Submission Ids</param>
		public ActionResult LinkSupplementLetters(int jurisdictionDataId, int submissionId, string applyToSubmissionIds = "")
		{
			var vm = new JurisdictionLinkLetterViewModel();
			vm.LinkOption = LinkOptions.Supplement.ToString();
			return View("Index", vm);
		}


		[HttpGet]
		public ActionResult SearchLetters(string jurisdictionSearchStr, string linkOption)
		{
			var jurisdictionSearch = JsonConvert.DeserializeObject<LetterSearch>(jurisdictionSearchStr);

			var results = new List<SearchDataInfo>();

			if (linkOption == LinkOptions.Certification.ToString())
			{
				results = SearchCertifications(jurisdictionSearch);
			}
			else if (linkOption == LinkOptions.Draft.ToString())
			{
				results = SearchDrafts(jurisdictionSearch);
			}

			return Content(ComUtil.JsonEncodeCamelCase(results), "application/json");

		}

		private List<SearchDataInfo> SearchCertifications(LetterSearch jurisdictionSearch)
		{
			var query = _dbSubmission.JurisdictionalData.AsNoTracking().AsQueryable();
			if (!string.IsNullOrEmpty(jurisdictionSearch.FileNumber))
			{
				query = query.Where(x => x.Submission.FileNumber == jurisdictionSearch.FileNumber).AsQueryable();
			}

			if (!string.IsNullOrEmpty(jurisdictionSearch.Manufacturer))
			{
				query = query.Where(x => x.Submission.ManufacturerCode == jurisdictionSearch.Manufacturer).AsQueryable();
			}

			if (!string.IsNullOrEmpty(jurisdictionSearch.JurisdictionId))
			{
				query = query.Where(x => x.JurisdictionId == jurisdictionSearch.JurisdictionId).AsQueryable();
			}

			if (!string.IsNullOrEmpty(jurisdictionSearch.IdNumber))
			{
				query = query.Where(x => x.Submission.IdNumber == jurisdictionSearch.IdNumber).AsQueryable();
			}

			if (!string.IsNullOrEmpty(jurisdictionSearch.Version))
			{
				query = query.Where(x => x.Submission.Version == jurisdictionSearch.Version).AsQueryable();
			}

			var results = query.Where(x => ((x.PdfPath != null && x.PdfPath != "") || (x.JurisdictionGPSPDF != null && x.JurisdictionGPSPDF != "")))
						 .Select(x => new SearchDataInfo()
						 {
							 FileNumber = x.Submission.FileNumber,
							 IdNumber = x.Submission.IdNumber,
							 GameName = x.Submission.GameName,
							 Status = x.Status,
							 JurisdictionName = x.JurisdictionName,
							 PdfPath = x.PdfPath,
							 GpsPdfPath = x.JurisdictionGPSPDF,
							 JurisdictionDataId = x.Id
						 }
						 ).ToList();

			return results;
		}

		private List<SearchDataInfo> SearchDrafts(LetterSearch jurisdictionSearch)
		{
			var query = _dbSubmission.SubmissionDrafts.AsNoTracking().AsQueryable();

			var allowedStatus = new List<string> { JurisdictionStatus.RA.ToString(), JurisdictionStatus.DR.ToString(), JurisdictionStatus.LQ.ToString(), JurisdictionStatus.AP.ToString() };
			query = query.Where(x => allowedStatus.Contains(x.JurisdictionalData.Status)).AsQueryable();
			if (!string.IsNullOrEmpty(jurisdictionSearch.FileNumber))
			{
				query = query.Where(x => x.Submission.FileNumber == jurisdictionSearch.FileNumber).AsQueryable();
			}

			if (!string.IsNullOrEmpty(jurisdictionSearch.Manufacturer))
			{
				query = query.Where(x => x.Submission.ManufacturerCode == jurisdictionSearch.Manufacturer).AsQueryable();
			}

			if (!string.IsNullOrEmpty(jurisdictionSearch.JurisdictionId))
			{
				query = query.Where(x => x.JurisdictionalData.JurisdictionId == jurisdictionSearch.JurisdictionId).AsQueryable();
			}

			if (!string.IsNullOrEmpty(jurisdictionSearch.IdNumber))
			{
				query = query.Where(x => x.Submission.IdNumber == jurisdictionSearch.IdNumber).AsQueryable();
			}

			if (!string.IsNullOrEmpty(jurisdictionSearch.Version))
			{
				query = query.Where(x => x.Submission.Version == jurisdictionSearch.Version).AsQueryable();
			}

			var results = query.Where(x => ((x.DraftLetter.FilePath != null && x.DraftLetter.FilePath != "") || (x.DraftLetter.GPSFilePath != null && x.DraftLetter.GPSFilePath != "")))
						 .Select(x => new SearchDataInfo()
						 {
							 FileNumber = x.Submission.FileNumber,
							 IdNumber = x.Submission.IdNumber,
							 GameName = x.Submission.GameName,
							 Status = x.JurisdictionalData.Status,
							 JurisdictionName = x.JurisdictionalData.JurisdictionName,
							 PdfPath = x.DraftLetter.FilePath,
							 GpsPdfPath = x.DraftLetter.GPSFilePath,
							 JurisdictionDataId = x.Id,
							 DraftId = x.DraftLetter.Id,
							 WordFilePath = x.DraftLetter.WordFilepath,
							 LetterheadPath = x.DraftLetter.LetterheadPath,
							 AddendumPath = x.DraftLetter.AddendumPath,
							 LetterheadPathAddendum = x.DraftLetter.LetterheadPathAddendum,
							 UseLetterhead = x.DraftLetter.UseLetterhead,
							 UseLetterheadAddendum = x.DraftLetter.UseLetterheadAddendum,
							 RepeatOverlay = x.DraftLetter.RepeatOverlay,
							 RepeatOverlayAddendum = x.DraftLetter.RepeatOverlayAddendum,
							 LetterheadSHA1 = x.DraftLetter.LetterheadSHA1,
							 LetterheadAddendumSHA1 = x.DraftLetter.LetterheadAddendumSHA1,
							 AttachCorrectionAddendum = x.DraftLetter.AttachCorrectionAddendum
						 }
						 ).Distinct().ToList();

			return results;
		}

		#region enums
		private enum LinkOptions
		{
			Certification,
			Supplement,
			Draft
		}

		#endregion
	}
}