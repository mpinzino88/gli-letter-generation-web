﻿extern alias ZEFCore;
using GLI.EFCore.Submission;
using Submissions.Common;
using Submissions.Web.Areas.Submission.Models.JurisdictionStandard;
using Submissions.Web.Models;
using System;
using System.Linq;
using System.Web.Mvc;
using ZEFCore::Z.EntityFramework.Plus;

namespace Submissions.Web.Areas.Submission.Controllers
{
	public class JurisdictionStandardController : Controller
	{
		private readonly SubmissionContext _dbSubmission;
		private readonly IUserContext _userContext;

		public JurisdictionStandardController
		(
			SubmissionContext dbSubmission,
			IUserContext userContext
		)
		{
			_dbSubmission = dbSubmission;
			_userContext = userContext;
		}

		public ActionResult Index(int submissionId, int jurisdictionDataId = 0)
		{
			var standards = _dbSubmission.tbl_Standards.Where(x => x.SubmissionId == submissionId)
							.Select(x => new StandardViewModel()
							{
								Id = x.Id,
								JurisdictionId = x.Standard.JurisdictionId,
								Jurisdiction = x.Standard.Jurisdiction.Name,
								Active = x.Active,
								AddDate = x.AddDate,
								CloseDate = x.CloseDate,
								Status = x.Status,
								Name = x.Standard.Standard,
							}).ToList();

			if (jurisdictionDataId != 0)
			{
				var currentJurisdictionId = _dbSubmission.JurisdictionalData.Where(y => y.Id == jurisdictionDataId).Select(y => y.JurisdictionId).SingleOrDefault();
				standards = standards.Where(x => x.JurisdictionId == currentJurisdictionId).ToList();
			}

			var submissionSubTitle = _dbSubmission.Submissions
				.Where(x => x.Id == submissionId)
				.Select(x => new SubmissionSubTitle
				{
					FileNumber = x.FileNumber,
					IdNumber = x.IdNumber,
					GameName = x.GameName,
					DateCode = x.DateCode,
					Function = x.Function
				})
				.Single();

			var vm = new IndexViewModel
			{
				FileNumber = submissionSubTitle.FileNumber,
				JurisdictionDataId = jurisdictionDataId,
				SubmissionId = submissionId,
				SubmissionSubTitle = submissionSubTitle,
				Standards = standards
			};
			return View(vm);
		}

		public ActionResult Edit(int? id, Mode mode, int submissionId, int jurisdictionDataId = 0)
		{
			var vm = new EditViewModel();

			if (mode == Mode.Edit)
			{
				vm = _dbSubmission.tbl_Standards.Where(x => x.Id == id)
							   .Select(x => new EditViewModel()
							   {
								   Id = x.Id,
								   JurisdictionId = x.Standard.JurisdictionId,
								   Jurisdiction = x.Standard.Jurisdiction.SubCode,
								   Active = x.Active,
								   AddDate = x.AddDate,
								   CloseDate = x.CloseDate,
								   Status = x.Status,
								   StandardName = x.Standard.Standard,
								   StandardId = (int)x.StandardsId,
								   OrgStandardId = (int)x.StandardsId
							   }).SingleOrDefault();
			}
			else
			{
				var subDate = _dbSubmission.Submissions.Where(x => x.Id == submissionId).Select(x => x.SubmitDate).SingleOrDefault();
				vm.AddDate = subDate;
			}

			vm.SubmissionId = submissionId;
			vm.JurisdictionDataId = jurisdictionDataId;
			vm.Mode = mode;

			return View(vm);
		}

		[HttpPost]
		public ActionResult Edit(EditViewModel vm)
		{
			var applyToJurisdictionalDataIds = _dbSubmission.JurisdictionalData.Where(x => x.SubmissionId == vm.SubmissionId && x.JurisdictionId == vm.JurisdictionId).Select(x => new { JurisdictionalDataId = x.Id, SubmissionId = x.SubmissionId }).ToList();
			if (vm.ApplyToAllRecords)
			{
				var fileNumber = _dbSubmission.Submissions.Where(x => x.Id == vm.SubmissionId).Select(x => x.FileNumber).SingleOrDefault();
				var applySubmissions = _dbSubmission.Submissions.Where(x => x.FileNumber == fileNumber).Select(x => x.Id).ToList();
				var applyTo = _dbSubmission.JurisdictionalData.Where(x => applySubmissions.Contains((int)x.SubmissionId) && x.JurisdictionId == vm.JurisdictionId).Select(x => new { JurisdictionalDataId = x.Id, SubmissionId = x.SubmissionId }).ToList();
				applyToJurisdictionalDataIds.AddRange(applyTo);
			}
			if (vm.Mode == Mode.Add)
			{
				foreach (var jurisdictionalData in applyToJurisdictionalDataIds)
				{
					if (!_dbSubmission.tbl_Standards.Where(x => x.JurisdictionalDataId == jurisdictionalData.JurisdictionalDataId && x.StandardsId == vm.StandardId).Any())
					{
						var standard = new tbl_Standards()
						{
							SubmissionId = jurisdictionalData.SubmissionId,
							JurisdictionalDataId = jurisdictionalData.JurisdictionalDataId,
							RequestDate = vm.AddDate,
							CloseDate = vm.CloseDate,
							Active = vm.Active,
							Status = vm.Status,
							StandardsId = vm.StandardId,
							Name = vm.StandardName,
							AddDate = DateTime.Now
						};
						_dbSubmission.UserId = _userContext.User.Id;
						_dbSubmission.tbl_Standards.Add(standard);
					}
				}
			}
			else
			{
				var currentStandard = _dbSubmission.tbl_Standards.Find(vm.Id);
				currentStandard.JurisdictionalData.JurisdictionId = vm.JurisdictionId;
				currentStandard.StandardsId = vm.StandardId;
				currentStandard.Name = vm.StandardName;
				currentStandard.RequestDate = vm.AddDate;
				currentStandard.CloseDate = vm.CloseDate;
				currentStandard.Active = vm.Active;
				currentStandard.Status = vm.Status;
				foreach (var jurisdictionalData in applyToJurisdictionalDataIds)
				{
					if (jurisdictionalData.JurisdictionalDataId != vm.JurisdictionDataId)
					{
						var updateStandard = _dbSubmission.tbl_Standards.Where(x => x.JurisdictionalDataId == jurisdictionalData.JurisdictionalDataId && x.StandardsId == vm.OrgStandardId).SingleOrDefault();
						if (updateStandard != null)
						{
							if (vm.ApplyToProps.Standard)
							{
								updateStandard.StandardsId = vm.StandardId;
								updateStandard.Name = vm.StandardName;
							}
							if (vm.ApplyToProps.AddDate)
							{
								updateStandard.RequestDate = vm.AddDate;
							}
							if (vm.ApplyToProps.CloseDate)
							{
								updateStandard.CloseDate = vm.CloseDate;
							}
							if (vm.ApplyToProps.Status)
							{
								updateStandard.Status = vm.Status;
							}
							if (vm.ApplyToProps.Active)
							{
								updateStandard.Active = vm.Active;
							}
						}
					}
				}

			}
			_dbSubmission.SaveChanges();
			return RedirectToAction("index", new { vm.SubmissionId, vm.JurisdictionDataId });
		}

		[HttpPost]
		public ActionResult Delete(int id, int submissionId, int jurisdictionDataId = 0)
		{
			_dbSubmission.tbl_Standards.Where(x => x.Id == id).Delete();
			return RedirectToAction("index", new { submissionId, jurisdictionDataId });
		}
	}
}