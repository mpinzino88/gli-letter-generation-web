﻿using GLI.EFCore.Submission;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Submissions.Common;
using Submissions.Core;
using Submissions.Web.Areas.Submission.Models.JurisdictionReplaceLink;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web.Mvc;

namespace Submissions.Web.Areas.Submission.Controllers
{
	public class JurisdictionReplaceLinkController : Controller
	{
		private readonly SubmissionContext _dbSubmission;
		private readonly IUserContext _userContext;
		private readonly IJurisdictionalDataService _jurisdictionalDataService;

		public JurisdictionReplaceLinkController(SubmissionContext dbSubmission, IUserContext userContext, IJurisdictionalDataService jurisdictionalDataService)
		{
			_dbSubmission = dbSubmission;
			_userContext = userContext;
			_jurisdictionalDataService = jurisdictionalDataService;
		}

		public ActionResult Index(int jurisdictionDataId, string linkMode = "NonDraft", string linkEditableMode = "Update")
		{

			var vm = new JurisdictionReplaceLinkViewModel();
			vm.JurisdictionDataId = jurisdictionDataId;
			vm.LinkMode = linkMode; //LinkMode is Draft & nonDraft, Draft is passed when when It's linked for Draft while setting draft
			vm.LinkEditableMode = linkEditableMode; // when Setting links from Draft Page, Jurisdiction status can't be updated so Readonly will be passed
			vm.FileNumber = _dbSubmission.JurisdictionalData.Where(x => x.Id == jurisdictionDataId).Select(x => x.Submission.FileNumber).SingleOrDefault();

			//get Jurisdiction to select
			var query = _dbSubmission.JurisdictionalData.AsNoTracking().AsQueryable();
			if (linkMode == ReplaceLinkMode.Draft.ToString())
			{
				var draftId = _dbSubmission.SubmissionDrafts.Where(x => x.JurisdictionDataId == jurisdictionDataId).Select(x => x.DraftLetterId).SingleOrDefault();
				//get all Jurs for Draft
				if (draftId != null)
				{
					var jurList = _dbSubmission.SubmissionDrafts.Where(x => x.DraftLetterId == draftId).Select(x => (int)x.JurisdictionDataId).ToList();
					query = query.Where(x => jurList.Contains(x.Id)).AsQueryable();
				}
				else
				{
					query = query.Where(x => x.Id == jurisdictionDataId).AsQueryable();
				}
			}
			else
			{
				var jurInfo = _dbSubmission.JurisdictionalData.Where(x => x.Id == jurisdictionDataId).Select(x => new { fileNumber = x.Submission.FileNumber, JurisdictionId = x.JurisdictionId }).SingleOrDefault();
				query = query.Where(x => x.Submission.FileNumber == jurInfo.fileNumber && x.JurisdictionId == jurInfo.JurisdictionId).AsQueryable();
			}

			var results = query
				.Select(x => new Jurisdiction()
				{
					Id = x.Id,
					FileNumber = x.Submission.FileNumber,
					IDNumber = x.Submission.IdNumber,
					Version = x.Submission.Version,
					DateCode = x.Submission.DateCode,
					GameName = x.Submission.GameName,
					JurisdictionName = x.JurisdictionName,
					JurisdictionId = x.JurisdictionId
				}).OrderBy(x => x.FileNumber).ToList();

			vm.JurisdictionSelection = results;

			//load jurs for current record
			var currentselectedInfo = _dbSubmission.JurisdictionalData.Where(x => x.Id == jurisdictionDataId).Select(x => new { SubmissionId = x.SubmissionId, JurId = x.JurisdictionId }).SingleOrDefault();
			//vm.LoadReplaceLinksOptions = _dbSubmission.JurisdictionalData
			//.Where(x => x.SubmissionId == currentselectedInfo.SubmissionId && x.JurisdictionId  != currentselectedInfo.JurId)
			//.OrderBy(x => x.JurisdictionName)
			//.Select(x => new SelectListItem { Value = x.Id.ToString(), Text = x.JurisdictionName + " - " + x.JurisdictionId })
			//.ToList();


			//Apply Available jurs for current record
			vm.ApplyReplaceLinksOptions = _dbSubmission.JurisdictionalData
			.Where(x => x.SubmissionId == currentselectedInfo.SubmissionId && x.JurisdictionId != currentselectedInfo.JurId)
			.OrderBy(x => x.JurisdictionName)
			.Select(x => new SelectListItem { Value = x.JurisdictionId, Text = x.JurisdictionName + " - " + x.JurisdictionId })
			.ToList();

			//get current primary to selected
			vm.JurisdictionSelection.Find(x => x.Id == jurisdictionDataId).Selected = true;
			//get replacement information for current Primary
			vm.SelJurisdictionReplaceLinks.JurisdictionDataId = jurisdictionDataId;
			vm.SelJurisdictionReplaceLinks.ReplaceLinks = GetReplacementLinks(jurisdictionDataId);

			var jurTypeSelection = new List<SelectListItem>();
			jurTypeSelection.Add(new SelectListItem() { Value = JurisdictionStatus.AP.ToString(), Text = JurisdictionStatus.AP.ToString() });
			jurTypeSelection.Add(new SelectListItem() { Value = JurisdictionStatus.DR.ToString(), Text = JurisdictionStatus.DR.ToString() });
			jurTypeSelection.Add(new SelectListItem() { Value = JurisdictionStatus.RV.ToString(), Text = JurisdictionStatus.RV.ToString() });
			jurTypeSelection.Add(new SelectListItem() { Value = JurisdictionStatus.NU.ToString(), Text = JurisdictionStatus.NU.ToString() });
			jurTypeSelection.Add(new SelectListItem() { Value = JurisdictionStatus.MO.ToString(), Text = JurisdictionStatus.MO.ToString() });

			vm.JurisdictionLetterTypes = jurTypeSelection;

			//Search
			vm.JurisdictionReplaceSearch.ReplaceOptions = LookupsStandard.ConvertEnum<ReplaceOptions>().ToList();
			vm.JurisdictionReplaceSearch.ReplaceSearch.JurisdictionId = vm.JurisdictionSelection.Find(x => x.Id == jurisdictionDataId).JurisdictionId;
			vm.JurisdictionReplaceSearch.ReplaceSearch.JurisdictionName = vm.JurisdictionSelection.Find(x => x.Id == jurisdictionDataId).JurisdictionName;

			//jira link
			vm.JIRAFullTextSearchURL = ConfigurationManager.AppSettings["jira.baseurl"] + @"/issues/?jql=text%20~%20%22" + vm.FileNumber + @"%22";

			return View("Index", vm);
		}

		[HttpGet]
		public ActionResult GetJurReplacements(int jurisdictionDataId)
		{
			var results = GetReplacementLinks(jurisdictionDataId);
			return Content(ComUtil.JsonEncodeCamelCase(results), "application/json");
			// return Json(ComUtil.JsonEncodeCamelCase(results));
		}
		private List<ReplaceWith> GetReplacementLinks(int jurisdictionDataId)
		{

			var links = _dbSubmission.SubmissionReplaceWiths.Where(x => x.ReplaceWithJurisdictionalDataId == jurisdictionDataId)
						.Select(x => new ReplaceWith()
						{
							ReplaceWithId = x.Id,
							Jurisdiction = new Jurisdiction()
							{
								Id = (int)x.JurisdictionalDataId,
								FileNumber = x.JurisdictionalData.Submission.FileNumber,
								IDNumber = x.JurisdictionalData.Submission.IdNumber,
								Version = x.JurisdictionalData.Submission.Version,
								DateCode = x.JurisdictionalData.Submission.DateCode,
								GameName = x.JurisdictionalData.Submission.GameName,
								JurisdictionName = x.JurisdictionalData.JurisdictionName,
								JurisdictionId = x.JurisdictionalData.JurisdictionId,
								CloseDate = x.JurisdictionalData.CloseDate,
								Status = x.JurisdictionalData.Status,
								RevokedDate = x.JurisdictionalData.RevokeDate,
								NUDate = x.JurisdictionalData.ObsoleteDate,
								UpgradeByDate = x.JurisdictionalData.UpgradeByDate,
								ReplaceWith = x.JurisdictionalData.ReplaceWith,
								ConditionalRevoke = (x.JurisdictionalData.ConditionalRevoke == true) ? true : false
							}
						}).ToList();

			return links;
		}

		[HttpPost]
		public ActionResult RemoveReplacementLinks(string replaceWithIds, string applyToJurisdictions)
		{
			var applyToJurIds = JsonConvert.DeserializeObject<List<String>>(applyToJurisdictions);
			try
			{
				var idList = JsonConvert.DeserializeObject<List<int>>(replaceWithIds);
				foreach (var id in idList)
				{
					var ReplaceInfo = _dbSubmission.SubmissionReplaceWiths.Where(x => x.Id == id).Select(x => new { ReplacedByJurId = x.JurisdictionalDataId, ReplaceWithJurId = x.ReplaceWithJurisdictionalDataId }).SingleOrDefault();
					_dbSubmission.SubmissionReplaceWiths.RemoveRange(_dbSubmission.SubmissionReplaceWiths.Where(x => x.Id == id));

					var jurReplaceById = ReplaceInfo.ReplacedByJurId;
					var jurReplaceWith = ReplaceInfo.ReplaceWithJurId;
					var submissionReplacebyId = _dbSubmission.JurisdictionalData.Where(x => x.Id == jurReplaceById).Select(x => x.SubmissionId).SingleOrDefault();
					var submissionReplaceWithId = _dbSubmission.JurisdictionalData.Where(x => x.Id == jurReplaceWith).Select(x => x.SubmissionId).SingleOrDefault();
					//remove it from other selected jurs
					foreach (var jurId in applyToJurIds)
					{
						DeleteLinkForSelectedJur((int)submissionReplacebyId, (int)submissionReplaceWithId, jurId);
					}

				}
				_dbSubmission.SaveChanges();
				return Content("OK");
			}
			catch (Exception ex)
			{
				return Content(ex.Message);
			}

		}

		[HttpPost]
		public ActionResult UpdateReplaceLinks(string replaceLinks, string applyToJurisdictions)
		{
			var applyToJurIds = JsonConvert.DeserializeObject<List<String>>(applyToJurisdictions);
			try
			{
				var processedJurs = new List<int>();
				var replaceLinkList = JsonConvert.DeserializeObject<List<ReplaceWith>>(replaceLinks);
				foreach (var replaceWithJurisdiction in replaceLinkList)
				{
					var jurisdictionReplaceBy = _dbSubmission.JurisdictionalData.Where(x => x.Id == replaceWithJurisdiction.Jurisdiction.Id && (x.Status != JurisdictionStatus.RA.ToString() && x.Status != JurisdictionStatus.CP.ToString() && x.Status != JurisdictionStatus.OH.ToString() && x.Status != JurisdictionStatus.LQ.ToString())).SingleOrDefault();
					if (jurisdictionReplaceBy != null)
					{
						var replacedBySubmission = jurisdictionReplaceBy.SubmissionId;

						jurisdictionReplaceBy.Status = replaceWithJurisdiction.Jurisdiction.Status;
						if (replaceWithJurisdiction.Jurisdiction.Status == JurisdictionStatus.RV.ToString())
						{
							jurisdictionReplaceBy.Active = JurisdictionActiveStatus.No.ToString();
						}
						else
						{
							jurisdictionReplaceBy.Active = JurisdictionActiveStatus.Yes.ToString();
						}
						jurisdictionReplaceBy.RevokeDate = replaceWithJurisdiction.Jurisdiction.RevokedDate;
						jurisdictionReplaceBy.ObsoleteDate = replaceWithJurisdiction.Jurisdiction.NUDate;
						jurisdictionReplaceBy.UpgradeByDate = replaceWithJurisdiction.Jurisdiction.UpgradeByDate;
						jurisdictionReplaceBy.ConditionalRevoke = replaceWithJurisdiction.Jurisdiction.ConditionalRevoke;

						_jurisdictionalDataService.Update(jurisdictionReplaceBy);
						processedJurs.Add(jurisdictionReplaceBy.Id);

						//remove it from other selected jurs
						foreach (var jurId in applyToJurIds)
						{
							UpdateLinkForSelectedJur((int)replacedBySubmission, jurId, replaceWithJurisdiction, ref processedJurs);
						}
					}

				}
				//update all processed Jurs Status, main selected and all applied
				_jurisdictionalDataService.ProcessForSubmissionStatus(processedJurs);

				return Content("OK");
			}
			catch (Exception ex)
			{
				return Content(ex.Message);
			}
		}

		#region Search and Add

		[HttpGet]
		public ActionResult SearchReplaceItems(string searchVM, int jurisdictionaldataId)
		{
			var vm = JsonConvert.DeserializeObject<JurisdictionReplaceSearch>(searchVM);
			var results = _dbSubmission.JurisdictionalData.Where(x => x.Id != jurisdictionaldataId).AsQueryable();
			//not in submission replace with
			results = results.Where(x => (x.Status != JurisdictionStatus.RA.ToString() && x.Status != JurisdictionStatus.CP.ToString() && x.Status != JurisdictionStatus.OH.ToString() && x.Status != JurisdictionStatus.LQ.ToString())).AsQueryable();

			if (!string.IsNullOrEmpty(vm.FileNumber))
			{
				results = results.Where(x => x.Submission.FileNumber == vm.FileNumber).AsQueryable();
			}
			if (!string.IsNullOrEmpty(vm.IDNumber))
			{
				results = results.Where(x => x.Submission.IdNumber.Contains(vm.IDNumber)).AsQueryable();
			}
			if (!string.IsNullOrEmpty(vm.GameName))
			{
				results = results.Where(x => x.Submission.GameName.Contains(vm.GameName)).AsQueryable();
			}
			if (!string.IsNullOrEmpty(vm.DateCode))
			{
				results = results.Where(x => x.Submission.DateCode == vm.DateCode).AsQueryable();
			}
			if (!string.IsNullOrEmpty(vm.Version))
			{
				results = results.Where(x => x.Submission.Version == vm.Version).AsQueryable();
			}
			if (!string.IsNullOrEmpty(vm.JurisdictionId))
			{
				results = results.Where(x => x.JurisdictionId == vm.JurisdictionId).AsQueryable();
			}

			var searchResult = results
				 .Select(x => new Jurisdiction()
				 {
					 Id = x.Id,
					 FileNumber = x.Submission.FileNumber,
					 IDNumber = x.Submission.IdNumber,
					 Version = x.Submission.Version,
					 GameName = x.Submission.GameName,
					 DateCode = x.Submission.DateCode,
					 JurisdictionName = x.JurisdictionName,
					 JurisdictionId = x.JurisdictionId,
					 CloseDate = x.CloseDate,
					 Status = x.Status,
					 RevokedDate = x.RevokeDate,
					 NUDate = x.ObsoleteDate,
					 UpgradeByDate = x.UpgradeByDate,
					 ReplaceColumn = ReplaceOptions.IDNumber.ToString(),
					 AlreadyReplaced = _dbSubmission.SubmissionReplaceWiths.Where(y => y.JurisdictionalDataId == x.Id).Select(y => y.ReplaceWithJurisdictionalData.Submission.FileNumber).FirstOrDefault()
				 }).ToList();

			return Content(ComUtil.JsonEncodeCamelCase(searchResult), "application/json");
		}

		[HttpPost]
		public ActionResult AddReplacementLinks(string newLinks, int jurisdictionalDataId, string applyToJurisdictions)
		{
			try
			{
				var newLinksList = JsonConvert.DeserializeObject<List<JurisdictionReplaceSelItems>>(newLinks);
				var applyToJurIds = JsonConvert.DeserializeObject<List<String>>(applyToJurisdictions);

				var submissionIdReplaceWith = _dbSubmission.JurisdictionalData.Where(x => x.Id == jurisdictionalDataId).Select(x => x.SubmissionId).SingleOrDefault();

				foreach (var jur in newLinksList)
				{
					var whichSubmissionReplaced = _dbSubmission.JurisdictionalData.Where(x => x.Id == jur.JurisdictionDataId).Select(x => x.SubmissionId).SingleOrDefault();
					if (!_dbSubmission.SubmissionReplaceWiths.Where(x => x.ReplaceWithJurisdictionalDataId == jurisdictionalDataId && x.JurisdictionalDataId == jur.JurisdictionDataId).Any())
					{
						//get replacement field info
						var replaceInfo = GetReplaceFieldInfo(jurisdictionalDataId, jur.ReplaceOption);
						var newReplacement = new SubmissionReplaceWith()
						{
							AddDate = DateTime.Now,
							AddUserId = _userContext.User.Id,
							JurisdictionalDataId = jur.JurisdictionDataId,
							ReplaceWithJurisdictionalDataId = jurisdictionalDataId,
							ReplaceWith = replaceInfo
						};
						_dbSubmission.SubmissionReplaceWiths.Add(newReplacement);

						//Update for selected jur
						var existingReplaceWiths = _dbSubmission.SubmissionReplaceWiths.Where(x => x.ReplaceWithJurisdictionalDataId == jur.JurisdictionDataId).ToList();
						if (existingReplaceWiths.Count > 0)
						{
							foreach (var existingReplace in existingReplaceWiths)
							{
								existingReplace.ReplaceWith = replaceInfo;
								existingReplace.ReplaceWithJurisdictionalDataId = jurisdictionalDataId;
								_dbSubmission.Entry(existingReplace).Property(x => x.ReplaceWith).IsModified = true;
								_dbSubmission.Entry(existingReplace).Property(x => x.ReplaceWithJurisdictionalDataId).IsModified = true;
							}
						}
						_dbSubmission.SaveChanges();
					}

					//apply
					foreach (var jurId in applyToJurIds)
					{
						AddLinkForSelectedJur((int)whichSubmissionReplaced, (int)submissionIdReplaceWith, jurId, jur.ReplaceOption);
					}

				}

				return Content("OK");
			}
			catch (Exception ex)
			{
				return Content(ex.Message);
			}

		}


		#region Apply to Other Jurs
		private void AddLinkForSelectedJur(int replacedSubmissionId, int replaceWithSubmissionId, string jurNum, string replaceWithOption)
		{
			var replaceWithJurisdictionalDataId = _dbSubmission.JurisdictionalData.Where(x => x.SubmissionId == replaceWithSubmissionId && x.JurisdictionId == jurNum).Select(x => x.Id).SingleOrDefault();
			var replacedJurisdictionalDataId = _dbSubmission.JurisdictionalData.Where(x => x.SubmissionId == replacedSubmissionId && x.JurisdictionId == jurNum).Select(x => x.Id).SingleOrDefault();

			if (replacedJurisdictionalDataId > 0 && replaceWithJurisdictionalDataId > 0)
			{
				//get replacement field info
				var replaceInfo = GetReplaceFieldInfo(replaceWithJurisdictionalDataId, replaceWithOption);

				if (!_dbSubmission.SubmissionReplaceWiths.Where(x => x.ReplaceWithJurisdictionalDataId == replaceWithJurisdictionalDataId && x.JurisdictionalDataId == replacedJurisdictionalDataId).Any())
				{
					var newReplacement = new SubmissionReplaceWith()
					{
						AddDate = DateTime.Now,
						AddUserId = _userContext.User.Id,
						JurisdictionalDataId = replacedJurisdictionalDataId,
						ReplaceWithJurisdictionalDataId = replaceWithJurisdictionalDataId,
						ReplaceWith = replaceInfo
					};
					_dbSubmission.SubmissionReplaceWiths.Add(newReplacement);

					//Update for selected jur
					var existingReplaceWiths = _dbSubmission.SubmissionReplaceWiths.Where(x => x.ReplaceWithJurisdictionalDataId == replacedJurisdictionalDataId).ToList();
					if (existingReplaceWiths.Count > 0)
					{
						foreach (var existingReplace in existingReplaceWiths)
						{
							existingReplace.ReplaceWith = replaceInfo;
							existingReplace.ReplaceWithJurisdictionalDataId = replacedJurisdictionalDataId;
							_dbSubmission.Entry(existingReplace).Property(x => x.ReplaceWith).IsModified = true;
							_dbSubmission.Entry(existingReplace).Property(x => x.ReplaceWithJurisdictionalDataId).IsModified = true;
						}
					}
					_dbSubmission.SaveChanges();
				}
			}
		}

		private void DeleteLinkForSelectedJur(int replacedSubmissionId, int replaceWithSubmissionId, string jurNum)
		{
			var replaceWithJurisdictionalDataId = _dbSubmission.JurisdictionalData.Where(x => x.SubmissionId == replaceWithSubmissionId && x.JurisdictionId == jurNum).Select(x => x.Id).SingleOrDefault();
			var replacedJurisdictionalDataId = _dbSubmission.JurisdictionalData.Where(x => x.SubmissionId == replacedSubmissionId && x.JurisdictionId == jurNum).Select(x => x.Id).SingleOrDefault();

			if (replacedJurisdictionalDataId > 0 && replaceWithJurisdictionalDataId > 0)
			{
				_dbSubmission.SubmissionReplaceWiths.RemoveRange(_dbSubmission.SubmissionReplaceWiths.Where(x => x.JurisdictionalDataId == replacedJurisdictionalDataId && x.ReplaceWithJurisdictionalDataId == replaceWithJurisdictionalDataId));
			}
		}

		private void UpdateLinkForSelectedJur(int replacedSubmissionId, string jurNum, ReplaceWith replaceWithJurInfo, ref List<int> processedJurs)
		{
			var jurisdictionReplaceBy = _dbSubmission.JurisdictionalData.Where(x => x.SubmissionId == replacedSubmissionId && x.JurisdictionId == jurNum && (x.Status != JurisdictionStatus.RA.ToString() && x.Status != JurisdictionStatus.DR.ToString() && x.Status != JurisdictionStatus.CP.ToString() && x.Status != JurisdictionStatus.OH.ToString() && x.Status != JurisdictionStatus.LQ.ToString())).SingleOrDefault();
			if (jurisdictionReplaceBy != null)
			{

				jurisdictionReplaceBy.Status = replaceWithJurInfo.Jurisdiction.Status;
				if (replaceWithJurInfo.Jurisdiction.Status == JurisdictionStatus.RV.ToString())
				{
					jurisdictionReplaceBy.Active = JurisdictionActiveStatus.No.ToString();
				}
				else
				{
					jurisdictionReplaceBy.Active = JurisdictionActiveStatus.Yes.ToString();
				}
				jurisdictionReplaceBy.RevokeDate = replaceWithJurInfo.Jurisdiction.RevokedDate;
				jurisdictionReplaceBy.ObsoleteDate = replaceWithJurInfo.Jurisdiction.NUDate;
				jurisdictionReplaceBy.UpgradeByDate = replaceWithJurInfo.Jurisdiction.UpgradeByDate;
				jurisdictionReplaceBy.ConditionalRevoke = replaceWithJurInfo.Jurisdiction.ConditionalRevoke;

				_jurisdictionalDataService.Update(jurisdictionReplaceBy);
				processedJurs.Add(jurisdictionReplaceBy.Id);
			}
		}

		#endregion
		private string GetReplaceFieldInfo(int jurisdictionalDataId, string replaceField)
		{
			var replaceInfo = "";
			var enumReplaceField = (ReplaceOptions)Enum.Parse(typeof(ReplaceOptions), replaceField, true);

			switch (enumReplaceField)
			{
				case ReplaceOptions.IDNumber:
					replaceInfo = _dbSubmission.JurisdictionalData.Where(x => x.Id == jurisdictionalDataId).Select(x => x.Submission.IdNumber).FirstOrDefault();
					break;
				case ReplaceOptions.DateCode:
					replaceInfo = _dbSubmission.JurisdictionalData.Where(x => x.Id == jurisdictionalDataId).Select(x => x.Submission.DateCode).FirstOrDefault();
					break;
				case ReplaceOptions.FileNumber:
					replaceInfo = _dbSubmission.JurisdictionalData.Where(x => x.Id == jurisdictionalDataId).Select(x => x.Submission.FileNumber).FirstOrDefault();
					break;
				case ReplaceOptions.Version:
					replaceInfo = _dbSubmission.JurisdictionalData.Where(x => x.Id == jurisdictionalDataId).Select(x => x.Submission.Version).FirstOrDefault();
					break;
				case ReplaceOptions.PartNumber:
					replaceInfo = _dbSubmission.JurisdictionalData.Where(x => x.Id == jurisdictionalDataId).Select(x => x.Submission.PartNumber).FirstOrDefault();
					break;
				default:
					break;
			}


			return replaceInfo;
		}
		#endregion

		#region enums
		private enum ReplaceOptions
		{
			IDNumber,
			DateCode,
			FileNumber,
			Version,
			PartNumber
		}
		private enum ReplaceLinkMode
		{
			Draft,
			NonDraft
		}

		private enum ReplaceLinkEditableMode
		{
			ReadOnly,
			Update
		}
		#endregion
	}
}