﻿using Dapper;
using GLI.EFCore.Submission;
using Microsoft.EntityFrameworkCore;
using Submissions.Common;
using Submissions.Web.Areas.Submission.Models.BugBox;
using Submissions.Web.Models;
using System;
using System.Linq;
using System.Web.Mvc;

namespace Submissions.Web.Areas.Submission.Controllers
{
	public class BugBoxController : Controller
	{
		private readonly SubmissionContext _dbSubmission;
		private readonly IUserContext _userContext;

		public BugBoxController(SubmissionContext dbSubmission, IUserContext userContext)
		{
			_dbSubmission = dbSubmission;
			_userContext = userContext;
		}

		public ActionResult Index(int submissionId)
		{
			var submissionSubTitle = _dbSubmission.Submissions
				.Where(x => x.Id == submissionId)
				.Select(x => new SubmissionSubTitle
				{
					FileNumber = x.FileNumber,
					IdNumber = x.IdNumber,
					GameName = x.GameName,
					DateCode = x.DateCode,
					Function = x.Function
				})
				.Single();

			var bugBoxes = _dbSubmission.trBugBoxes
				.Where(x => x.SubmissionId == submissionId)
				.OrderBy(x => x.Lab.Location)
				.Select(x => new BugBoxViewModel
				{
					Id = x.Id,
					BugBox = x.BugBox,
					Lab = x.Lab.Location
				})
				.ToList();

			var vm = new IndexViewModel
			{
				SubmissionId = submissionId,
				SubmissionSubTitle = submissionSubTitle,
				BugBoxes = bugBoxes
			};

			return View(vm);
		}

		public ActionResult Edit(int? id, Mode mode, int submissionId, PageSource? source)
		{
			var vm = new EditViewModel();
			if (mode == Mode.Edit)
			{
				vm = _dbSubmission.trBugBoxes
					.Where(x => x.Id == id)
					.Select(x => new EditViewModel
					{
						Id = x.Id,
						BugBox = x.BugBox,
						Lab = x.Lab.Location + " - " + x.Lab.Name,
						LabId = x.LabId
					})
					.Single();
			}

			vm.Mode = mode;
			vm.Source = source;
			vm.SubmissionId = submissionId;

			var cancelUrl = new UrlHelper(this.ControllerContext.RequestContext);
			vm.CancelButtonUrl = cancelUrl.Action("index", "bugbox", new { submissionId });
			if (source == PageSource.SubmissionDetailPartialView)
			{
				vm.CancelButtonUrl = cancelUrl.Action("detail", "submission", new { id = submissionId });
			}

			return View(vm);
		}

		[HttpPost]
		public ActionResult Edit(EditViewModel vm)
		{
			var submission = _dbSubmission.Submissions
				.Where(x => x.Id == vm.SubmissionId)
				.Select(x => new { x.FileNumber, x.ManufacturerCode })
				.Single();

			var bugBox = new trBugBox();
			if (vm.Mode == Mode.Edit)
			{
				bugBox = _dbSubmission.trBugBoxes.Find(vm.Id);
			}

			bugBox.Id = vm.Id ?? 0;
			bugBox.BugBox = vm.BugBox ?? bugBox.BugBox;
			bugBox.FileNumber = submission.FileNumber;
			bugBox.LabId = vm.LabId;
			bugBox.Manufacturer = submission.ManufacturerCode;
			bugBox.SubmissionId = vm.SubmissionId;

			if (vm.Mode == Mode.Add)
			{
				if (string.IsNullOrEmpty(vm.BugBox))
				{
					var sqlParams = new DynamicParameters();
					sqlParams.Add("@ManufacturerCode", submission.ManufacturerCode, System.Data.DbType.String, System.Data.ParameterDirection.Input, 5);
					sqlParams.Add("@LabId", vm.LabId, System.Data.DbType.Int32, System.Data.ParameterDirection.Input);

					var lastBugBox = _dbSubmission.Database.GetDbConnection().ExecuteScalar<int>(
						@"SELECT COALESCE(MAX(CONVERT(int, bugbox)), 0)
						FROM trBugBox
						WHERE Manufacturer_ShortID = @ManufacturerCode AND locationID = @LabId AND ISNUMERIC(bugbox) = 1",
						sqlParams);

					bugBox.BugBox = (Convert.ToInt32(lastBugBox) + 1).ToString();
				}

				_dbSubmission.trBugBoxes.Add(bugBox);
			}

			_dbSubmission.UserId = _userContext.User.Id;
			_dbSubmission.SaveChanges();

			if (vm.Source == PageSource.SubmissionDetailPartialView)
			{
				return RedirectToAction("detail", "submission", new { id = vm.SubmissionId });
			}

			return RedirectToAction("index", new { vm.SubmissionId });
		}

		[HttpPost]
		public ActionResult Delete(EditViewModel vm)
		{
			var bugBox = new trBugBox { Id = (int)vm.Id };
			_dbSubmission.Entry(bugBox).State = EntityState.Deleted;
			_dbSubmission.UserId = _userContext.User.Id;
			_dbSubmission.SaveChanges();

			if (vm.Source == PageSource.SubmissionDetailPartialView)
			{
				return RedirectToAction("detail", "submission", new { id = vm.SubmissionId });
			}

			return RedirectToAction("index", new { vm.SubmissionId });
		}

		public JsonResult CheckDuplicate(int? id, string bugBox, int submissionId, int? labId)
		{
			var validates = true;

			if (!string.IsNullOrEmpty(bugBox) && labId != null && _dbSubmission.trBugBoxes.Any(x => x.SubmissionId == submissionId && x.LabId == labId && x.BugBox == bugBox && x.Id != id))
			{
				validates = false;
			}

			return Json(validates, JsonRequestBehavior.AllowGet);
		}
	}
}