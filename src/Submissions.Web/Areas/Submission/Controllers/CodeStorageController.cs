﻿using GLI.EFCore.Submission;
using Microsoft.EntityFrameworkCore;
using Submissions.Common;
using Submissions.Web.Areas.Submission.Models.CodeStorage;
using Submissions.Web.Models;
using System;
using System.Linq;
using System.Web.Mvc;

namespace Submissions.Web.Areas.Submission.Controllers
{
	public class CodeStorageController : Controller
	{
		private readonly SubmissionContext _dbSubmission;
		private readonly IUserContext _userContext;

		public CodeStorageController(SubmissionContext dbSubmission, IUserContext userContext)
		{
			_dbSubmission = dbSubmission;
			_userContext = userContext;
		}

		public ActionResult Index(int submissionId)
		{
			var submissionSubTitle = _dbSubmission.Submissions
				.Where(x => x.Id == submissionId)
				.Select(x => new SubmissionSubTitle
				{
					FileNumber = x.FileNumber,
					IdNumber = x.IdNumber,
					GameName = x.GameName,
					DateCode = x.DateCode,
					Function = x.Function
				})
				.Single();

			var codeStorages = _dbSubmission.SourceCodeStorage
				.Where(x => x.SubmissionId == submissionId)
				.OrderBy(x => x.Lab.Location)
				.Select(x => new CodeStorageViewModel
				{
					Id = x.Id,
					CodeStorage = x.CodeStorage,
					Lab = x.Lab.Location
				})
				.ToList();

			var vm = new IndexViewModel
			{
				SubmissionId = submissionId,
				SubmissionSubTitle = submissionSubTitle,
				CodeStorages = codeStorages
			};

			return View(vm);
		}

		public ActionResult Edit(int? id, Mode mode, int submissionId, PageSource? source)
		{
			var vm = new EditViewModel();
			if (mode == Mode.Edit)
			{
				vm = _dbSubmission.SourceCodeStorage
					.Where(x => x.Id == id)
					.Select(x => new EditViewModel
					{
						Id = x.Id,
						CodeStorage = x.CodeStorage,
						Lab = x.Lab.Location + " - " + x.Lab.Name,
						LabId = x.LabId
					})
					.Single();
			}

			vm.Mode = mode;
			vm.Source = source;
			vm.SubmissionId = submissionId;

			var cancelUrl = new UrlHelper(this.ControllerContext.RequestContext);
			vm.CancelButtonUrl = cancelUrl.Action("index", "codestorage", new { submissionId });
			if (source == PageSource.SubmissionDetailPartialView)
			{
				vm.CancelButtonUrl = cancelUrl.Action("detail", "submission", new { id = submissionId });
			}

			return View(vm);
		}

		[HttpPost]
		public ActionResult Edit(EditViewModel vm)
		{
			var submission = _dbSubmission.Submissions
				.Where(x => x.Id == vm.SubmissionId)
				.Select(x => new { x.FileNumber, x.ManufacturerCode })
				.Single();

			var codeStorage = new SourceCodeStorage();
			if (vm.Mode == Mode.Edit)
			{
				codeStorage = _dbSubmission.SourceCodeStorage.Find(vm.Id);
			}

			codeStorage.Id = vm.Id ?? 0;
			codeStorage.CodeStorage = vm.CodeStorage ?? codeStorage.CodeStorage;
			codeStorage.LabId = (int)vm.LabId;
			codeStorage.SubmissionId = vm.SubmissionId;

			if (vm.Mode == Mode.Add)
			{
				if (vm.CodeStorage == null)
				{
					var lastCodeStorage = _dbSubmission.SourceCodeStorage
						.Where(x =>
							x.Submission.ManufacturerCode == submission.ManufacturerCode &&
							x.LabId == vm.LabId
						)
						.Max(x => x.CodeStorage);

					codeStorage.CodeStorage = Convert.ToInt32(lastCodeStorage) + 1;
				}

				_dbSubmission.SourceCodeStorage.Add(codeStorage);
			}

			_dbSubmission.UserId = _userContext.User.Id;
			_dbSubmission.SaveChanges();

			if (vm.Source == PageSource.SubmissionDetailPartialView)
			{
				return RedirectToAction("detail", "submission", new { id = vm.SubmissionId });
			}

			return RedirectToAction("index", new { vm.SubmissionId });
		}

		[HttpPost]
		public ActionResult Delete(EditViewModel vm)
		{
			var codeStorage = new SourceCodeStorage { Id = (int)vm.Id };
			_dbSubmission.Entry(codeStorage).State = EntityState.Deleted;
			_dbSubmission.UserId = _userContext.User.Id;
			_dbSubmission.SaveChanges();

			if (vm.Source == PageSource.SubmissionDetailPartialView)
			{
				return RedirectToAction("detail", "submission", new { id = vm.SubmissionId });
			}

			return RedirectToAction("index", new { vm.SubmissionId });
		}

		public JsonResult CheckDuplicate(int? id, int? codeStorage, int submissionId, int? labId)
		{
			var validates = true;

			if (codeStorage != null && labId != null && _dbSubmission.SourceCodeStorage.Any(x => x.SubmissionId == submissionId && x.LabId == labId && x.CodeStorage == codeStorage && x.Id != id))
			{
				validates = false;
			}

			return Json(validates, JsonRequestBehavior.AllowGet);
		}
	}
}