﻿extern alias ZEFCore;
using AutoMapper.QueryableExtensions;
using GLI.EFCore.Submission;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Submissions.Common;
using Submissions.Web.Areas.Submission.Models.MachineCertificate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using ZEFCore::Z.EntityFramework.Plus;

namespace Submissions.Web.Areas.Submission.Controllers
{
	public class MachineCertificateController : Controller
	{
		private readonly SubmissionContext _dbSubmission;
		private readonly IUserContext _userContext;

		public MachineCertificateController(SubmissionContext dbSubmission, IUserContext userContext)
		{
			_dbSubmission = dbSubmission;
			_userContext = userContext;
		}

		public ActionResult Index(int submissionId) => View(BuildIndexViewModel(submissionId));

		public ActionResult Edit(int id, int submissionRecordId, Mode mode, string jurisdictionId) => View(mode == Mode.Add ? new MachineCertificateEditViewModel() { Mode = Mode.Add, JurisdictionId = jurisdictionId, SubmissionRecordId = submissionRecordId } : BuildEditViewModel(id, submissionRecordId));

		[HttpPost]
		public ActionResult Edit(MachineCertificateEditViewModel viewModel, string replacementCertificateViewModel)
		{
			viewModel.MachineCertificate.ReplacedMachineCertificates.Clear();
			viewModel.MachineCertificate.ReplacedMachineCertificates = JsonConvert.DeserializeObject<List<ReplacedMachineCertificate>>(replacementCertificateViewModel);

			switch (viewModel.Mode)
			{
				case Mode.Add:
					viewModel.MachineCertificate.JurisdictionId = viewModel.JurisdictionId;
					CreateMachineCertificate(viewModel.MachineCertificate, viewModel.SubmissionRecordId);
					break;
				case Mode.Edit:
					UpdateMachineCertificate(viewModel.MachineCertificate);
					break;
				case Mode.Delete:
					DeleteMachineCertificate(viewModel.MachineCertificate.Id);
					break;
				default:
					break;
			}

			return View("Index", BuildIndexViewModel(viewModel.SubmissionRecordId));
		}

		#region Model Builders
		private MachineCertificateIndexViewModel BuildIndexViewModel(int submissionRecordId) => new MachineCertificateIndexViewModel
		{
			SubmissionRecordId = submissionRecordId,
			MachineCertificates = ReadMachineCertifications(submissionRecordId),
			Component = ReadComponent(submissionRecordId)
		};
		private MachineCertificateEditViewModel BuildEditViewModel(int id, int submissionRecordId)
		{
			var result = new MachineCertificateEditViewModel
			{
				MachineCertificate = ReadMachineCertificateModel(id),
				Component = ReadComponent(submissionRecordId),
				SubmissionRecordId = submissionRecordId,
			};
			result.JurisdictionId = result.Component.Jurisdiction;

			return result;
		}
		#endregion

		#region CRUD
		private void CreateMachineCertificate(MachineCertificateModel machineCertificate, int submissionRecordId)
		{
			var now = DateTime.Now;

			var queuedForCreation = new tbl_machine_cert
			{
				AddDate = now,
				CertificateDate = (DateTime)machineCertificate.CertificateDate,
				CertificateNumber = machineCertificate.CertificateNumber,
				DeviceType = machineCertificate.DeviceType,
				JurisdictionId = machineCertificate.JurisdictionId,
				SealNumber = machineCertificate.SealNumber,
				SerialNumber = machineCertificate.SerialNumber,
				SubmissionId = submissionRecordId
			};

			_dbSubmission.tbl_machine_cert.Add(queuedForCreation);
			SaveChanges();

			foreach (var replacment in machineCertificate.ReplacedMachineCertificates)
			{
				_dbSubmission.tbl_replacement_cert.Add(new tbl_replacement_cert
				{
					AddDate = now,
					CertificateNumber = replacment.CertificationNumber,
					MachineCertificateId = queuedForCreation.Id
				});
			}
			SaveChanges();
		}
		private tbl_machine_cert ReadTblMachineCert(int id) => _dbSubmission.tbl_machine_cert.Where(x => x.Id == id).Single();
		private IList<MachineCertificateModel> ReadMachineCertifications(int submissionRecordId)
		{
			return _dbSubmission.tbl_machine_cert
				.Where(x => x.SubmissionId == submissionRecordId)
				.Select(x => new MachineCertificateModel
				{
					CertificateDate = x.CertificateDate,
					CertificateNumber = x.CertificateNumber,
					DeviceType = x.DeviceType,
					Id = x.Id,
					JurisdictionId = x.JurisdictionId,
					SealNumber = x.SealNumber,
					SerialNumber = x.SerialNumber,
					ReplacedMachineCertificates = x.ReplacementCertificates.Select(y => new ReplacedMachineCertificate
					{
						Id = y.Id,
						CertificationNumber = y.CertificateNumber
					}).ToList()
				})
				.OrderByDescending(x => x.CertificateDate)
				.ToList();
		}
		private void UpdateMachineCertificate(MachineCertificateModel machineCertificate)
		{
			var now = DateTime.Now;
			var machineCertFromDB = ReadTblMachineCert(machineCertificate.Id);
			machineCertFromDB.CertificateDate = (DateTime)machineCertificate.CertificateDate;
			machineCertFromDB.CertificateNumber = machineCertificate.CertificateNumber;
			machineCertFromDB.SerialNumber = machineCertificate.SerialNumber;
			machineCertFromDB.SealNumber = machineCertificate.SealNumber;
			machineCertFromDB.DeviceType = machineCertificate.DeviceType;
			machineCertFromDB.EditDate = now;

			UpdateReplacementCertifications(machineCertificate, machineCertFromDB, now);
			SaveChanges();
		}
		private void UpdateReplacementCertifications(MachineCertificateModel machineCertificate, tbl_machine_cert machineCertFromDB, DateTime now)
		{
			var queuedForCreation = machineCertificate.ReplacedMachineCertificates.Where(x => x.Id == 0).ToList();
			var queuedForUpdate = machineCertificate.ReplacedMachineCertificates.Where(x => x.Id != 0).ToList();

			foreach (var item in machineCertFromDB.ReplacementCertificates)
			{
				var replacedMachineCertificate = queuedForUpdate.Where(x => x.Id == item.Id).SingleOrDefault();
				if (replacedMachineCertificate != null)
				{
					if (item.CertificateNumber != replacedMachineCertificate.CertificationNumber)
					{
						item.CertificateNumber = replacedMachineCertificate.CertificationNumber;
						item.EditDate = now;
					}
				}
				else
				{
					_dbSubmission.tbl_replacement_cert.Where(x => x.Id == item.Id).Delete();
				}
			}

			foreach (var item in queuedForCreation)
			{
				if (!String.IsNullOrWhiteSpace(item.CertificationNumber))
					_dbSubmission.tbl_replacement_cert.Add(new tbl_replacement_cert { AddDate = now, CertificateNumber = item.CertificationNumber, MachineCertificateId = machineCertificate.Id });
			}
		}
		private void DeleteMachineCertificate(int id)
		{
			var queuedForDeletion = new tbl_machine_cert { Id = id };
			_dbSubmission.tbl_machine_cert.Attach(queuedForDeletion);
			_dbSubmission.Entry(queuedForDeletion).State = EntityState.Deleted;
			_dbSubmission.tbl_replacement_cert.Where(x => x.MachineCertificateId == id).Delete();
			SaveChanges();
		}
		#endregion

		#region Helpers
		private MachineCertificateModel ReadMachineCertificateModel(int id)
		{
			var machineCertificate = ReadTblMachineCert(id);
			return new MachineCertificateModel
			{
				CertificateDate = machineCertificate.CertificateDate,
				CertificateNumber = machineCertificate.CertificateNumber,
				DeviceType = machineCertificate.DeviceType,
				Id = machineCertificate.Id,
				JurisdictionId = machineCertificate.JurisdictionId,
				SealNumber = machineCertificate.SealNumber,
				SerialNumber = machineCertificate.SerialNumber,
				ReplacedMachineCertificates = machineCertificate.ReplacementCertificates.Select(x => new ReplacedMachineCertificate
				{
					Id = x.Id,
					CertificationNumber = x.CertificateNumber
				})
				.ToList()
			};
		}
		private Component ReadComponent(int id) => _dbSubmission.Submissions.Where(x => x.Id == id).ProjectTo<Component>().SingleOrDefault();
		private void SaveChanges()
		{
			_dbSubmission.UserId = _userContext.User.Id;
			_dbSubmission.SaveChanges();
			_dbSubmission.UserId = null;
		}
		#endregion
	}
}