﻿extern alias ZEFCore;
using AutoMapper.QueryableExtensions;
using GLI.EFCore.Submission;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Submissions.Common;
using Submissions.Core;
using Submissions.Core.Interfaces;
using Submissions.Web.Areas.Submission.Models.JurisdictionalData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Web.Mvc;
using ZEFCore::Z.EntityFramework.Plus;

namespace Submissions.Web.Areas.Submission.Controllers
{
	using Submissions.Common.Email;
	using Submissions.Web.Utility.Email;

	public class JurisdictionalDataController : Controller
	{
		private readonly IEmailService _emailService;
		private readonly IJurisdictionalDataService _jurisdictionalDataService;
		private readonly ILogService _logService;
		private readonly IMemoService _memoService;
		private readonly IProjectService _projectService;
		private readonly ISharepointService _sharepointService;
		private readonly SubmissionContext _dbSubmission;
		private readonly IUserContext _userContext;

		private List<int> specialQuoateJurs = new List<int>()
		{
			((int) CustomJurisdiction.LotoQuebecCasino),
			((int) CustomJurisdiction.KansasRacingAndGaming),
			((int) CustomJurisdiction.LotoQuebecLottery)
		};

		public JurisdictionalDataController
		(
			SubmissionContext dbSubmission,
			IUserContext userContext,
			IProjectService projectService,
			IJurisdictionalDataService jurisdictionalDataService,
			IMemoService memoService,
			IEmailService emailService,
			ILogService logService,
			ISharepointService sharepointService
		)
		{
			_dbSubmission = dbSubmission;
			_emailService = emailService;
			_jurisdictionalDataService = jurisdictionalDataService;
			_logService = logService;
			_memoService = memoService;
			_projectService = projectService;
			_sharepointService = sharepointService;
			_userContext = userContext;

		}
		public ActionResult Index()
		{
			return View();
		}

		public ActionResult Edit(Mode mode, string fileNumber, int submissionId = 0)
		{
			var vm = new JurisdictionAddEditViewModel();

			if (mode == Mode.Edit && (submissionId != 0 && string.IsNullOrEmpty(fileNumber)))
			{
				vm.FileNumber = _dbSubmission.Submissions.Where(x => x.Id == submissionId).Select(x => x.FileNumber).First();
			}
			else
			{
				vm.FileNumber = fileNumber.ToString().ToUpper();
			}

			vm.Submissions = _dbSubmission.Submissions
				.AsNoTracking()
				.Where(x => x.FileNumber == vm.FileNumber)
				.ProjectTo<Models.Submission.SubmissionSelection>()
				.OrderBy(x => x.Id)
				.ToList();

			vm.SubmitDate = vm.Submissions.Min(x => x.SubmitDate);
			vm.ReceiveDate = vm.Submissions.Min(x => x.ReceiveDate);

			vm.IsFileOpen = vm.Submissions.Where(x => (x.Status == SubmissionStatus.PN.ToString() || x.Status == SubmissionStatus.CP.ToString() || x.Status == SubmissionStatus.OH.ToString() || x.Status == SubmissionStatus.NL.ToString())).Any();

			var submissionRecords = _dbSubmission.Submissions.Where(x => x.FileNumber == vm.FileNumber).OrderBy(x => x.AddDate)
							.Select(x => new
							{
								CompanyId = x.CompanyId,
								Company = x.Company.Name,
								CurrencyId = x.CurrencyId,
								Currency = x.Currency.Description,
								ContractTypeId = x.ContractTypeId,
								ContractType = x.ContractType.Description,
								TestLab = x.Lab,
								Manuf = x.ManufacturerCode,
								PrimaryJur = x.JurisdictionId,
								TestingType = x.TestingType.Code
							}).FirstOrDefault();

			vm.Manufacturer = submissionRecords.Manuf;
			vm.IsProductLineManuf =
				_dbSubmission.ManufacturerGroups
					.Where(x => x.ManufacturerGroupCategory.Code == ManufacturerGroupCategory.ProductLines.ToString())
					.SelectMany(x => x.ManufacturerLinks.Select(y => y.ManufacturerCode))
					.Any(x => x == vm.Manufacturer);


			vm.Testlab = submissionRecords.TestLab;
			vm.TestingType = submissionRecords.TestingType;
			vm.IsFlatFileManuf = IsFlatFileManuf(submissionRecords.Manuf);
			vm.IsAristocratManuf = CustomManufacturerGroups.Aristocrat.Contains(submissionRecords.Manuf);

			vm.DefaultJurisdiction.CompanyId = submissionRecords.CompanyId;
			vm.DefaultJurisdiction.Company = submissionRecords.Company;

			vm.DefaultJurisdiction.CurrencyId = submissionRecords.CurrencyId;
			vm.DefaultJurisdiction.Currency = _dbSubmission.tbl_lst_Currency.Where(x => x.Id == submissionRecords.CurrencyId).Select(x => x.Code + " - " + x.Description).SingleOrDefault();

			vm.DefaultJurisdiction.ContractTypeId = submissionRecords.ContractTypeId;
			vm.DefaultJurisdiction.ContractType = _dbSubmission.tbl_lst_ContractType.Where(x => x.Id == submissionRecords.ContractTypeId).Select(x => x.Code + " - " + x.Description).SingleOrDefault();

			vm.DefaultJurisdiction.Active = JurisdictionActiveStatus.No.ToString();

			var projectOnHold = _dbSubmission.trProjects.Where(x => x.ProjectName == vm.FileNumber).Select(x => x.OnHold).FirstOrDefault();
			vm.DefaultJurisdiction.Status = projectOnHold == 1 ? JurisdictionStatus.OH.ToString() : JurisdictionStatus.RA.ToString();


			var labInfo = _dbSubmission.trLaboratories.Where(x => x.Location == submissionRecords.TestLab).Select(x => new { Desc = x.Location + " - " + x.Name, Id = x.Id }).SingleOrDefault();
			if (labInfo != null)
			{
				vm.DefaultJurisdiction.lab = labInfo.Desc;
				vm.DefaultJurisdiction.LabId = labInfo.Id;
			}

			//Get Primary Jur info
			if (vm.Manufacturer == CustomManufacturer.APL.ToString())
			{
				var PrimaryJurInfo = _dbSubmission.JurisdictionalData.Where(x => x.Submission.FileNumber == vm.FileNumber && x.JurisdictionId == submissionRecords.PrimaryJur).Select(x => new { AriProjectNo = x.ARIProjectNo, ARIDeptCode = x.ARIDeptCode, ARIAccountCode = x.ARIAccountCode }).FirstOrDefault();
				if (PrimaryJurInfo != null)
				{
					vm.DefaultJurisdiction.ARIProjectNo = PrimaryJurInfo.AriProjectNo;
					vm.DefaultJurisdiction.ARIDeptCode = PrimaryJurInfo.ARIDeptCode;
					vm.DefaultJurisdiction.ARIAccountCode = PrimaryJurInfo.ARIAccountCode;
				}
			}

			if (mode == Mode.Edit)
			{
				vm.Jurisdictions = GetJurisdictionsInfo(submissionId);
				vm.Submissions.Find(x => x.Id == submissionId).Selected = true;

				vm.UserAndOtherInfo.CheckOffVersions = _dbSubmission.Tbl_lst_CheckOffVersion.Where(x => x.Active == true).Select(x => new CheckOffVersion() { Id = x.Id, Description = x.VersionDescription }).ToList();
				vm.JurisdictionUpdatedDateReasons = _dbSubmission.tbl_lst_UpdatedDateReason.Select(x => new SelectListItem() { Text = x.Reason, Value = x.Id.ToString() }).ToList();

			}

			//User and other info*******
			vm.UserAndOtherInfo.UserRole = _userContext.Role.ToString();
			vm.UserAndOtherInfo.UserlocationId = _userContext.LocationId;
			vm.UserAndOtherInfo.Userlocation = _userContext.Location + " - " + _userContext.LocationName;

			vm.UserAndOtherInfo.ClosedStatuses = _dbSubmission.tbl_lst_ProductJurisdictionStatus.Where(x => x.IsClosedStatus == true).Select(x => x.Status).ToList();

			//*******

			vm.SubmissionId = submissionId;
			vm.Mode = mode.ToString();
			vm.CanEdit = Util.HasAccess(Permission.Submission, AccessRight.Edit);

			return View(vm);
		}

		#region Add New Jurisdictions

		[HttpPost]
		public JsonResult AddJurisdictionalData(JurisdictionAddEditViewModel jurisdictionAddVm)
		{
			var addedJurisdictions = jurisdictionAddVm.Jurisdictions;
			var selectedSubmissions = jurisdictionAddVm.Submissions.Where(x => x.Selected == true).Select(x => x.Id).ToList();

			var testingPerformedJurs = ComUtil.TestingPerformedJurisdictions().Select(i => (int)i).ToList();

			var duplicateExists = new List<string>();
			var jurisdictionalDataList = new List<JurisdictionalData>();
			var jurWithPastDate = new List<int>();

			try
			{
				foreach (var submissionId in selectedSubmissions)
				{
					var submission = _dbSubmission.Submissions.Where(x => x.Id == submissionId).SingleOrDefault();
					foreach (var item in addedJurisdictions)
					{
						var jurisdiction = new JurisdictionalData();
						jurisdiction.CompanyId = item.CompanyId;
						jurisdiction.CurrencyId = item.CurrencyId;
						jurisdiction.ContractTypeId = item.ContractTypeId;
						jurisdiction.JurisdictionName = item.JurisdictionName;
						jurisdiction.Txt = "Jurisdiction" + item.JurisdictionId;
						jurisdiction.JurisdictionId = item.JurisdictionId;
						jurisdiction.LabId = item.LabId;
						jurisdiction.Active = item.Active;
						jurisdiction.ReceiveDate = item.ReceiveDate;
						jurisdiction.RequestDate = item.RequestDate;
						jurisdiction.Status = item.Status;

						jurisdiction.OrgIGTJurisdiction = item.OrgIGTJurisdiction;
						jurisdiction.ContractNumber = item.ContractNumber;
						jurisdiction.ClientNumber = item.ClientNumber;
						jurisdiction.LetterNumber = item.LetterNumber;
						jurisdiction.ProjectNumber = item.ProjectNumber;
						jurisdiction.PurchaseOrder = item.PurchaseOrder;

						jurisdiction.AddUserId = _userContext.User.Id;
						jurisdiction.AddDate = DateTime.Now;
						jurisdiction.ARIAccountCode = item.ARIAccountCode;
						jurisdiction.ARIDeptCode = item.ARIDeptCode;
						jurisdiction.ARIProjectNo = item.ARIProjectNo;

						jurisdiction.Restricted = (Convert.ToInt32(item.JurisdictionId) == (int)CustomJurisdiction.OkClassIIIPotawatomi) ? 1 : 0;

						//If not Partial testing jur
						if (!testingPerformedJurs.Contains(Convert.ToInt32(jurisdiction.JurisdictionId)))
						{
							jurisdiction.TestingPerformedId = 1;
						}

						//masterbillingProject
						if (!string.IsNullOrEmpty(item.MasterBillingProject))
						{
							var newMasterBillingProject = formatMasterBillingProject(item.MasterBillingProject);

							if (!string.IsNullOrEmpty(newMasterBillingProject))
							{
								jurisdiction.MasterBillingProject = newMasterBillingProject;
							}
						}

						jurisdiction.SubmissionId = submissionId;
						jurisdiction.Submission = submission;

						if (!_dbSubmission.JurisdictionalData.Where(x => x.JurisdictionId == jurisdiction.JurisdictionId && x.SubmissionId == submissionId).Any()) // if does not exists
						{
							DefaultProductLinesIfUniform(submissionId);
							jurisdictionalDataList.AddRange(_jurisdictionalDataService.Add(jurisdiction, true));
							//if past Date Memo needed
							if (!string.IsNullOrEmpty(item.PastDateMemo))
							{
								jurWithPastDate.Add(jurisdiction.Id);
							}
						}
						else
						{
							duplicateExists.Add(jurisdiction.JurisdictionName);
						}
					}
				}

				//extra processing for added data
				if (jurisdictionalDataList.Count > 0)
				{
					//Adds to new submission project and OH jurs
					_jurisdictionalDataService.ProcessForProtrackAndAOtherDataForAddedJurisdictions(jurisdictionalDataList);

					//set tbl_quoteAuthdate
					var quoteAuthJurs = jurisdictionalDataList.Where(x => specialQuoateJurs.Contains(Convert.ToInt32(x.JurisdictionId))).ToList();
					foreach (var jur in quoteAuthJurs)
					{
						_dbSubmission.tbl_quoteAuthDate.Add(new tbl_quoteAuthDate() { JurisdictionalDataId = jur.Id, QuoteDate = null, AuthorizeDate = null });
					}

					//On Hold
					var ohJurs = jurisdictionalDataList.Where(x => x.Status == JurisdictionStatus.OH.ToString()).Select(x => x.Id).ToList();
					_projectService.PutProjectOrItemsOnHold(new Core.Models.ProjectService.PutItemsOnHold()
					{
						JurisdictionalDataIds = ohJurs,
						OnHold = true,
						Reason = jurisdictionAddVm.MemoInfo.OHMemo,
						Description = "Other"
					});

					// All memo
					if (!string.IsNullOrEmpty(jurisdictionAddVm.MemoInfo.Memo))
					{
						var submissionsAffected = jurisdictionalDataList.Select(x => (int)x.SubmissionId).Distinct().ToList();
						AddMemos(submissionsAffected, jurisdictionAddVm.MemoInfo.Memo);

					}

					//memo for Past dates
					if (!string.IsNullOrEmpty(jurisdictionAddVm.MemoInfo.DatesMoreThanSixDaysPast))
					{
						var submissionsAffected = jurisdictionalDataList.Select(x => x.SubmissionId).Distinct().ToList();
						var memoList = new List<tblMemo>();
						foreach (var subId in submissionsAffected)
						{
							var memoText = " Submitted/Received dates for the file are 6 business days before current date(" + DateTime.Now + "): Reason: " + jurisdictionAddVm.MemoInfo.DatesMoreThanSixDaysPast + ". ";

							var jursWithPastDate = jurisdictionalDataList.Where(x => x.SubmissionId == subId && jurWithPastDate.Contains(x.Id)).Select(x => x.JurisdictionName).ToList();
							if (jursWithPastDate.Count > 0)
							{
								memoText += "Submitted/Received Date for the following jurisdiction(s): " + string.Join(",", jursWithPastDate);
								memoList.Add(new tblMemo()
								{
									SubmissionId = (int)subId,
									Memo = memoText,
									Username = _userContext.User.Username,
									AddDate = DateTime.Now
								});
							}
						}
						if (memoList.Count > 0)
						{
							_memoService.AddBulk(memoList);
						}
					}

					_dbSubmission.SaveChanges();

					//Send an email 
					SendEmailNotificationForAddedJuris(jurisdictionalDataList);
				}
				return Json(duplicateExists.Distinct(), JsonRequestBehavior.AllowGet);
			}
			catch (Exception ex)
			{
				Response.StatusCode = (int)HttpStatusCode.InternalServerError;
				return Json(JsonConvert.SerializeObject(ex));
			}

		}

		private void SendEmailNotificationForAddedJuris(List<JurisdictionalData> AddedJuris)
		{

			var allFiles = AddedJuris.Select(x => x.Submission.FileNumber).Distinct();

			foreach (var file in allFiles)
			{
				var projectInfo = _dbSubmission.trProjects.Where(x => x.ProjectName == file && x.ENStatus != "DN" && x.BundleTypeId == (int)BundleType.OriginalTesting).Select(x => new { projectId = x.Id, managerId = x.ENManagerId, leadId = x.ENProjectLeadId, status = x.ENStatus }).SingleOrDefault();
				var recepients = new List<int>();
				if (projectInfo != null)
				{
					if (projectInfo.managerId != null)
					{
						recepients.Add((int)projectInfo.managerId);
					}
					if (projectInfo.leadId != null)
					{
						recepients.Add((int)projectInfo.leadId);
					}

					if (projectInfo.status == ProjectStatus.IT.ToString() || projectInfo.status == ProjectStatus.IR.ToString() || projectInfo.status == ProjectStatus.RC.ToString())
					{
						var engList = _dbSubmission.trTasks.Where(x => x.ProjectId == projectInfo.projectId && x.UserId > 0).Select(x => (int)x.UserId).ToList();
						recepients.AddRange(engList);
					}

					var fileJurs = AddedJuris.Where(x => x.Submission.FileNumber == file).Select(x => new JurisdictionAdd()
					{
						IdNumber = x.Submission.IdNumber,
						GameName = x.Submission.GameName,
						DateCode = x.Submission.DateCode,
						Version = x.Submission.Version,
						JurisdictionId = x.JurisdictionId,
						JurisdictionName = x.JurisdictionName
					}).ToList();

					var emailForNewJur = new AddNewSubmissionJurisdictionsEmail();
					emailForNewJur.FileNumber = file;
					emailForNewJur.Jurisdictions = fileJurs;

					var emails = _dbSubmission.trLogins.Where(x => recepients.Contains(x.Id)).Select(x => x.Email).Distinct().ToList();
					var emailAddresses = string.Join(",", emails);

					if (!string.IsNullOrEmpty(emailAddresses))
					{
						var gliGlobe = Server.MapPath("~/Img/GLIGlobe.gif");
						_emailService.EmbeddedImages = new List<LinkedResource> { new LinkedResource(gliGlobe) { ContentId = "GLIGlobe" }, };

						var mailMsg = new MailMessage("noreply@gaminglabs.com", emailAddresses);
						mailMsg.Subject = "Jurisdiction(s) have been added to: " + file;
						mailMsg.Body = _emailService.RenderTemplate(Submissions.Common.Email.EmailType.AddSubmissionJurisdiction.ToString(), emailForNewJur);
						_emailService.Send(mailMsg);
					}
				}
			}

		}

		#endregion

		#region Delete Jurisdictions
		[AuthorizeUser(Permission = Permission.Submission, HasAccessRight = AccessRight.Delete)]
		public ActionResult Delete(int id)
		{
			_jurisdictionalDataService.Delete(id);

			var returnUrl = System.Web.HttpContext.Current.Request.UrlReferrer;

			return Redirect(returnUrl.ToString());
		}

		[AuthorizeUser(Permission = Permission.Submission, HasAccessRight = AccessRight.Delete)]
		public ActionResult DeleteAll(string fileNumber, string jurisdictionId)
		{
			var filenumber = new FileNumber(fileNumber);

			_jurisdictionalDataService.Delete(filenumber, jurisdictionId);

			var returnUrl = System.Web.HttpContext.Current.Request.UrlReferrer;

			return Redirect(returnUrl.ToString());
		}

		[AuthorizeUser(Permission = Permission.Submission, HasAccessRight = AccessRight.Delete)]
		public ActionResult DeleteFromSubmissions(string submissionIds, string jurisdictionId)
		{
			var lstSubmissionIds = JsonConvert.DeserializeObject<List<int>>(submissionIds);
			_jurisdictionalDataService.Delete(lstSubmissionIds, jurisdictionId);

			var returnUrl = System.Web.HttpContext.Current.Request.UrlReferrer;

			return Redirect(returnUrl.ToString());
		}
		#endregion

		#region Edit Jurisdictions
		[HttpPost]
		public JsonResult SaveJurisdictionalData(JurisdictionAddEditViewModel jurisdictionEditVm)
		{

			//update Jurisdictions
			var modifiedJurisdictionIds = UpdateJurisdictions(jurisdictionEditVm);

			CheckSubmissionForStatus(modifiedJurisdictionIds);

			//Protrack checks
			var updatedJurisdictionalDataIdList = _dbSubmission.JurisdictionalData.Where(x => modifiedJurisdictionIds.Contains(x.Id)).ToList();
			_jurisdictionalDataService.ProcessForProtrackDataAfterModifiedJurisdictions(updatedJurisdictionalDataIdList);

			return Json("OK", JsonRequestBehavior.AllowGet);

		}

		private List<int> UpdateJurisdictions(JurisdictionAddEditViewModel jurisdictionEditVm)
		{
			var affectedJurIdList = new List<int>();

			var selectedSubmissions = jurisdictionEditVm.Submissions.Where(x => x.Selected == true).Select(x => x.Id).ToList();
			//var jursToUpdate = jurisdictionEditVm.Jurisdictions.Where(x => x.IsDirty == true && x.SelToUpdate).ToList();
			var jursToUpdate = jurisdictionEditVm.Jurisdictions.Where(x => x.SelToUpdate).ToList();

			//update submission level conditional Revoke
			if (jurisdictionEditVm.ConditionalRevokeSubmission && selectedSubmissions.Count > 0)
			{
				UpdateSubmissionConditionalRevoke(selectedSubmissions);
			}

			//Add memo
			if (!string.IsNullOrEmpty(jurisdictionEditVm.MemoInfo.Memo) && selectedSubmissions.Count > 0)
			{
				AddMemos(selectedSubmissions, jurisdictionEditVm.MemoInfo.Memo);
			}

			//memo for Past dates
			if (!string.IsNullOrEmpty(jurisdictionEditVm.MemoInfo.DatesMoreThanSixDaysPast) && selectedSubmissions.Count > 0)
			{

				var memoList = new List<tblMemo>();
				foreach (var subId in selectedSubmissions)
				{
					var memoText = " Submitted/Received dates for the file are 6 business days before current date(" + DateTime.Now + "): Reason: " + jurisdictionEditVm.MemoInfo.DatesMoreThanSixDaysPast + ". ";
					var jurWithPastDate = jurisdictionEditVm.Jurisdictions.Where(x => x.PastDateMemo != null && x.PastDateMemo != "").Select(x => x.JurisdictionId).ToList();
					var jursWithPastDate = _dbSubmission.JurisdictionalData.Where(x => x.SubmissionId == subId && jurWithPastDate.Contains(x.JurisdictionId)).Select(x => x.JurisdictionName).ToList();
					if (jursWithPastDate.Count > 0)
					{
						memoText += "Submitted/Received Date for the following jurisdiction(s): " + string.Join(",", jursWithPastDate);
						memoList.Add(new tblMemo()
						{
							SubmissionId = (int)subId,
							Memo = memoText,
							Username = _userContext.User.Username,
							AddDate = DateTime.Now
						});
					}
				}
				if (memoList.Count > 0)
				{
					_memoService.AddBulk(memoList);
				}
			}

			var dBColumnMapping = SetTableColumnMapping("JurisdictionalData");
			//Process jurisdictions
			foreach (var jur in jursToUpdate)
			{
				//What Jurisdictions are getting affected
				var jurisdictionIdList = new List<int>();
				var billingJurisdictionIdList = new List<int>();
				var whichJurNum = _dbSubmission.JurisdictionalData.Where(x => x.Id == jur.Id).Select(x => x.JurisdictionId).SingleOrDefault();

				jurisdictionIdList = _dbSubmission.JurisdictionalData.Where(x => x.Id == jur.Id || (selectedSubmissions.Contains((int)x.SubmissionId) && x.JurisdictionId == whichJurNum)).Select(x => x.Id).ToList();
				if (jur.ColumnChanges.CurrencyId || jur.ColumnChanges.CompanyId || jur.ColumnChanges.ContractTypeId)
				{
					var submissionRecords = jurisdictionEditVm.Submissions.Select(x => x.Id).ToList();
					billingJurisdictionIdList = _dbSubmission.JurisdictionalData.Where(x => (submissionRecords.Contains((int)x.SubmissionId) && x.JurisdictionId == whichJurNum)).Select(x => x.Id).ToList();
				}

				//***On Hold
				if (jur.Status == JurisdictionStatus.OH.ToString() && jur.OrigStatus != JurisdictionStatus.OH.ToString())
				{
					ProcessOnHold(jurisdictionIdList, true, jurisdictionEditVm.MemoInfo.OHMemo);
				}
				else if (jur.Status != JurisdictionStatus.OH.ToString() && jur.OrigStatus == JurisdictionStatus.OH.ToString())
				{
					ProcessOnHold(jurisdictionIdList, false, jurisdictionEditVm.MemoInfo.OHMemo);
				}
				//******************************************

				//check MasterBillingProject format
				if (jur.ColumnChanges.MasterBillingProject == true)
				{
					if (!string.IsNullOrEmpty(jur.MasterBillingProject))
					{
						jur.MasterBillingProject = formatMasterBillingProject(jur.MasterBillingProject);
					}
				}
				//******************************

				var currentJurisdictionData = _dbSubmission.JurisdictionalData.Where(x => x.Id == jur.Id).SingleOrDefault();
				var oldDynamicsCompanyId = currentJurisdictionData.Company.DynamicsCompanyId;
				var oldCurrency = currentJurisdictionData.Currency.Code;
				var oldContractType = currentJurisdictionData.ContractType.Code;
				var oldJurId = Convert.ToInt32(currentJurisdictionData.JurisdictionId);

				//Updates to DB
				if (billingJurisdictionIdList.Count > 0)
				{
					UpdateToDB(jur, currentJurisdictionData, jurisdictionIdList, jurisdictionEditVm.CopyOnlyModified, dBColumnMapping, billingJurisdictionIdList);
				}
				else
				{
					UpdateToDB(jur, currentJurisdictionData, jurisdictionIdList, jurisdictionEditVm.CopyOnlyModified, dBColumnMapping);
				}

				//Add standards for new jurs
				if (jur.ColumnChanges.JurisdictionId == true)
				{
					//Delete standards
					DeleteStandards(jur.JurisdictionId, jurisdictionIdList);
					//Add standards
					_jurisdictionalDataService.AddStandards(currentJurisdictionData);

					//Change Transfer Bundle Name
					var fileNumber = _dbSubmission.JurisdictionalData.Where(x => x.Id == jur.Id).Select(x => x.Submission.FileNumber).SingleOrDefault();
					var project = _dbSubmission.trProjects.Where(x => x.FileNumber == fileNumber && x.Suffix == oldJurId).FirstOrDefault();
					if (project != null)
					{
						_logService.AddProjectHistory(new trHistory()
						{
							AddDate = DateTime.Now,
							UserId = _userContext.User.Id,
							ProjectId = project.Id,
							Notes = "Bundle name was changed from " + project.ProjectName
						});

						project.Suffix = Convert.ToInt32(jur.JurisdictionId);
						_dbSubmission.Entry(project).Property(x => x.Suffix).IsModified = true;
						_dbSubmission.SaveChanges();
					}
				}

				if (jur.ColumnChanges.Active == true || jur.ColumnChanges.Status == true || jur.ColumnChanges.CloseDate == true || jur.ColumnChanges.JurisdictionId == true)
				{
					UpdateStandards(jur.JurisdictionId, jurisdictionIdList, jur.Active, jur.Status, jur.CloseDate);
				}
				//Jur Updated to RA from WD,RJ
				if (jur.Status == JurisdictionStatus.RA.ToString() && (jur.OrigStatus == JurisdictionStatus.RJ.ToString() || jur.OrigStatus == JurisdictionStatus.WD.ToString()))
				{
					var belongstoClosedProjectList = _dbSubmission.trProjectJurisdictionalData.Where(x => jurisdictionIdList.Contains(x.JurisdictionalDataId) && x.Project.ENStatus == "DN").Select(x => x.JurisdictionalDataId).ToList();
					if (belongstoClosedProjectList.Count > 0)
					{
						_dbSubmission.trTargetDate.Where(x => belongstoClosedProjectList.Contains((int)x.JurisdictionalDataId)).Update(x => new trTargetDate { IsDeleted = true });
						_dbSubmission.JurisdictionalData.Where(x => belongstoClosedProjectList.Contains(x.Id)).Update(x => new JurisdictionalData { TransferAvailable = (x.TransferCreated == true) ? true : false, TransferCreated = false, TransferAdded = DateTime.Now, TargetDate = null, ProposedTargetDate = null });
						_dbSubmission.trProjectJurisdictionalData.Where(x => belongstoClosedProjectList.Contains(x.JurisdictionalDataId)).Delete();
					}
				}

				//Jur updated to RJ/WD from DR
				if ((jur.Status == JurisdictionStatus.RJ.ToString() || jur.Status == JurisdictionStatus.WD.ToString()) && (jur.OrigStatus == JurisdictionStatus.DR.ToString()))
				{
					var draftIdList = _dbSubmission.SubmissionDrafts.Where(x => jurisdictionIdList.Contains((int)x.JurisdictionDataId)).Select(x => new { DraftId = x.DraftLetterId, JurisdictionalDataId = x.JurisdictionDataId }).ToList();
					foreach (var draftJurId in draftIdList)
					{
						_dbSubmission.SubmissionDrafts.Where(x => x.DraftLetterId == draftJurId.DraftId && x.JurisdictionDataId == draftJurId.JurisdictionalDataId).Delete();

						if (!_dbSubmission.SubmissionDrafts.Where(x => x.DraftLetterId == draftJurId.DraftId).Any())
						{
							_dbSubmission.DraftLetters.Where(x => x.Id == draftJurId.DraftId).Delete();
						}
					}
				}

				//Jur Status AP, update cert Lab
				if (jur.Status == JurisdictionStatus.AP.ToString())
				{
					var projectIdList = _dbSubmission.trProjectJurisdictionalData.Where(x => jurisdictionIdList.Contains(x.JurisdictionalDataId) && x.Project.BundleTypeId == (int)BundleType.OriginalTesting).Select(x => x.ProjectId).ToList();
					if (projectIdList.Count > 0)
					{
						var lab = _dbSubmission.trLaboratories.Where(x => x.Id == jur.CertificationLabId).Select(x => x.Location).SingleOrDefault();
						if (lab != null)
						{
							_dbSubmission.trProjects.Where(x => projectIdList.Contains(x.Id) && x.CertificationLab != lab).Update(x => new trProject { CertificationLab = lab });
						}
					}
				}

				//Jur Status AP, update to Submission side AP as well
				if (jur.ColumnChanges.Status == true && jur.Status == JurisdictionStatus.AP.ToString())
				{
					CheckSubmissionForAP(jurisdictionIdList);
				}

				//NU/Rv send an email
				if ((jur.Status == JurisdictionStatus.NU.ToString() && jur.OrigStatus != JurisdictionStatus.NU.ToString()) || (jur.Status == JurisdictionStatus.RV.ToString() && jur.OrigStatus != JurisdictionStatus.RV.ToString()))
				{
					_jurisdictionalDataService.SendMailForNURV(jur.Id, jur.OrigStatus);
				}

				if (jur.ColumnChanges.CurrencyId || jur.ColumnChanges.ContractTypeId || jur.ColumnChanges.PurchaseOrder || jur.ColumnChanges.CompanyId)
				{
					var jurisdictionalData = _dbSubmission.JurisdictionalData.Where(x => x.Id == jur.Id && x.JurisdictionId == currentJurisdictionData.JurisdictionId).Single();
					jurisdictionalData.SentToAcumatica = 0;
					_dbSubmission.SaveChanges();

					// Posting to sharepoint needs to be taken off after confirming with Robert Cox - Jayati
					var newDynamicsCompnayId = _dbSubmission.tbl_lst_Company.Where(x => x.Id == jur.CompanyId).Select(x => x.DynamicsCompanyId).SingleOrDefault();
					var newCurrency = _dbSubmission.tbl_lst_Currency.Where(x => x.Id == jur.CurrencyId).Select(x => x.Code).SingleOrDefault();
					var newContractType = _dbSubmission.tbl_lst_ContractType.Where(x => x.Id == jur.ContractTypeId).Select(x => x.Code).SingleOrDefault();
					var fullFileNumber = new FileNumber(currentJurisdictionData.Submission.Type, currentJurisdictionData.Submission.JurisdictionId, currentJurisdictionData.Submission.ManufacturerCode, currentJurisdictionData.Submission.Year, currentJurisdictionData.Submission.Sequence, currentJurisdictionData.JurisdictionId);
					_sharepointService.UpdateDynamicsUpdateJurisdictionCompanyCurrencyContractTypeChange(fullFileNumber, oldDynamicsCompanyId, newDynamicsCompnayId, oldCurrency, newCurrency, oldContractType, newContractType);
				}

				affectedJurIdList.AddRange(jurisdictionIdList);
			}

			return (affectedJurIdList);
		}

		private void CheckSubmissionForStatus(List<int> jurisdictionDataIdList)
		{
			_jurisdictionalDataService.ProcessForSubmissionStatus(jurisdictionDataIdList);
		}
		private void CheckSubmissionForAP(List<int> jurisdictionDataIdList)
		{
			var submissionNotAPList = _dbSubmission.JurisdictionalData.Where(x => jurisdictionDataIdList.Contains(x.Id) && x.Submission.Status != SubmissionStatus.AP.ToString()).Select(x => x.SubmissionId).ToList();
			if (submissionNotAPList.Count > 0)
			{
				foreach (var submissionId in submissionNotAPList)
				{
					var _submission = _dbSubmission.Submissions.Where(x => x.Id == submissionId).SingleOrDefault();
					_submission.Status = SubmissionStatus.AP.ToString();
					_dbSubmission.Entry(_submission).Property(x => x.Status).IsModified = true;
					_dbSubmission.UserId = _userContext.User.Id;
					_dbSubmission.SaveChanges();
				}
			}
		}

		private void UpdateToDB(Jurisdiction updatedJurisdiction, JurisdictionalData currentJurisdictionData, List<int> applyToJurisdictionDataIds, bool copyOnlyModified, List<PropertyDBColumnMapping> dBColumnMapping, List<int> billingJurisdictionIdList = null)
		{
			var allColumns = updatedJurisdiction.ColumnChanges.GetType().GetProperties().OrderBy(x => x.Name).Select(x => x.Name).ToList();
			var sqlUpdateQuery = new StringBuilder();
			var sqlUpdateQueryBilling = new StringBuilder();
			var setClause = "";
			var setBillingClause = "";
			sqlUpdateQuery.AppendFormat("exec devin_setcontext {0};", _userContext.User.Id);
			sqlUpdateQuery.AppendFormat("UPDATE {0}", "JurisdictionalData");
			sqlUpdateQueryBilling.AppendFormat("exec devin_setcontext {0};", _userContext.User.Id);
			sqlUpdateQueryBilling.AppendFormat("UPDATE {0}", "JurisdictionalData");

			if (updatedJurisdiction.SelToUpdate)
			{
				foreach (var column in allColumns)
				{
					var IsValChanged = updatedJurisdiction.ColumnChanges.GetType().GetProperty(column).GetValue(updatedJurisdiction.ColumnChanges, null);
					if (copyOnlyModified == true) //update only modified
					{
						if ((bool)IsValChanged == true)
						{
							UpdateJurisdictionItem(column, updatedJurisdiction, currentJurisdictionData, applyToJurisdictionDataIds, dBColumnMapping, ref setClause, ref setBillingClause);
						}
					}
					else //update all fields
					{
						UpdateJurisdictionItem(column, updatedJurisdiction, currentJurisdictionData, applyToJurisdictionDataIds, dBColumnMapping, ref setClause, ref setBillingClause);
					}

				}
			}
			if (setClause != "")
			{
				sqlUpdateQuery.AppendFormat(" SET {0}", setClause);
				var sWhere = string.Join(",", applyToJurisdictionDataIds);
				sqlUpdateQuery.AppendFormat(" Where primary_ in ({0})", sWhere);
				var query = sqlUpdateQuery.ToString();
				_dbSubmission.Database.ExecuteSqlRaw(query);

			}
			if(setBillingClause != "")
			{
				sqlUpdateQueryBilling.AppendFormat(" SET {0}", setBillingClause);
				var sWhere = string.Join(",", billingJurisdictionIdList);
				sqlUpdateQueryBilling.AppendFormat(" Where primary_ in ({0})", sWhere);
				var query = sqlUpdateQueryBilling.ToString();
				_dbSubmission.Database.ExecuteSqlRaw(query);
			}
		}

		private void UpdateJurisdictionItem(string column, Jurisdiction newUpdatedJurisdiction, JurisdictionalData currentJurisdiction, List<int> applyToJurisdictionDataIds, List<PropertyDBColumnMapping> dBColumnMappings, ref string setClause, ref string setBillingClause)
		{

			var setClauseBuilder = new StringBuilder();
			var setBillingClauseBuilder = new StringBuilder();

			var dbColumnName = dBColumnMappings.Where(x => x.PropertyName == column).Select(x => x.DBColumnName).SingleOrDefault();
			var newVal = newUpdatedJurisdiction.GetType().GetProperty(column).GetValue(newUpdatedJurisdiction, null);

			if (column == TrackedJurisdictionColumns.CertNumber.ToString()) //cert number 
			{
				if (!string.IsNullOrEmpty(newUpdatedJurisdiction.CertNumber))
				{
					foreach (var jurisdictionDataId in applyToJurisdictionDataIds)
					{
						_dbSubmission.Database.ExecuteSqlRaw("Exec sp_UpdateLabNumber " + jurisdictionDataId + ",'" + newUpdatedJurisdiction.CertNumber + "'," + _userContext.User.Id);
					}
				}
			}
			else if (column == TrackedJurisdictionColumns.AuthorizeDate.ToString() || column == TrackedJurisdictionColumns.QuoteDate.ToString()) //Quote or Auth Date
			{
				foreach (var jurisdictionDataId in applyToJurisdictionDataIds)
				{
					UpdateQuoteAuthDate(jurisdictionDataId, newUpdatedJurisdiction.JurisdictionId, newUpdatedJurisdiction.QuoteDate, newUpdatedJurisdiction.AuthorizeDate);
				}
			}
			else if (column == TrackedJurisdictionColumns.TestScriptVersions.ToString()) //Test script version
			{
				// insert into tblJurCheckOffversion
				_dbSubmission.TblJurCheckOffVersions.Where(x => applyToJurisdictionDataIds.Contains(x.JurisdictionalDataId)).Delete();
				if (newUpdatedJurisdiction.TestScriptVersionIds != null)
				{
					foreach (var versionId in newUpdatedJurisdiction.TestScriptVersionIds)
					{
						foreach (var jurisdictionDataId in applyToJurisdictionDataIds)
						{
							_dbSubmission.TblJurCheckOffVersions.Add(new TblJurCheckOffVersion()
							{
								JurisdictionalDataId = jurisdictionDataId,
								CheckOffId = versionId
							});
						}
					}
				}
				_dbSubmission.SaveChanges();

			}
			else if (column == TrackedJurisdictionColumns.ProductLines.ToString())
			{
				_dbSubmission.tbl_lst_ProductLinesJurisdictionalData.Where(x => applyToJurisdictionDataIds.Contains(x.JurisdictionaldataId)).Delete();
				foreach (var productLine in newUpdatedJurisdiction.ProductLines)
				{
					foreach (var jurisdictionDataId in applyToJurisdictionDataIds)
					{
						_dbSubmission.tbl_lst_ProductLinesJurisdictionalData.Add(new tbl_lst_ProductLinesJurisdictionalData { JurisdictionaldataId = jurisdictionDataId, ProductLineId = productLine.Id });
					}

				}
				_dbSubmission.SaveChanges();
			}
			else if (column == TrackedJurisdictionColumns.CurrencyId.ToString() || column == TrackedJurisdictionColumns.CompanyId.ToString() || column == TrackedJurisdictionColumns.ContractTypeId.ToString())
			{
				if (newVal is null)
				{
					setBillingClauseBuilder.AppendFormat("{0} = NULL", dbColumnName);
				}
				else
				{
					setBillingClauseBuilder.AppendFormat("{0} = '{1}'", dbColumnName, newVal);
				}

				if (string.IsNullOrEmpty(setBillingClause))
				{
					setBillingClause = setBillingClauseBuilder.ToString();
				}
				else
				{
					setBillingClause = setBillingClause + "," + setBillingClauseBuilder.ToString();
				}
			}
			else
			{
				if (column == TrackedJurisdictionColumns.JurisdictionId.ToString() && newUpdatedJurisdiction.ColumnChanges.JurisdictionId == true)
				{
					//Delete CheckOffversion
					_dbSubmission.TblJurCheckOffVersions.Where(x => applyToJurisdictionDataIds.Contains(x.JurisdictionalDataId)).Delete();

					//******************************
					//update jurId, jurName, jurtxt
					setClauseBuilder.AppendFormat("{0} = '{1}'", "JurisdictionId", newUpdatedJurisdiction.JurisdictionId);
					setClauseBuilder.Append(",");
					setClauseBuilder.AppendFormat("{0} = '{1}'", "JurisdictionName", newUpdatedJurisdiction.JurisdictionName);
					setClauseBuilder.Append(",");
					setClauseBuilder.AppendFormat("{0} = '{1}'", "JurisdictionTxt", "Jurisdiction" + newUpdatedJurisdiction.JurisdictionId);

				}
				else
				{
					if (newVal is null)
					{
						setClauseBuilder.AppendFormat("{0} = NULL", dbColumnName);
					}
					else
					{
						setClauseBuilder.AppendFormat("{0} = '{1}'", dbColumnName, newVal);
					}
				}

				//jurisdictionData.GetType().GetProperty(column).SetValue(jurisdictionData, newVal, null);
				if (string.IsNullOrEmpty(setClause))
				{
					setClause = setClauseBuilder.ToString();
				}
				else
				{
					setClause = setClause + "," + setClauseBuilder.ToString();
				}

			}


		}

		[HttpGet]
		public ContentResult GetJurisdictionInfo(int jurisdictionalDataId)
		{
			var jurisdiction = _dbSubmission.JurisdictionalData.Where(x => x.Id == jurisdictionalDataId).Select(x => new Jurisdiction()
			{
				Id = x.Id,
				JurisdictionId = x.JurisdictionId,
				JurisdictionName = x.JurisdictionName,
				CompanyId = x.CompanyId,
				Company = x.Company.Name,
				CurrencyId = x.CurrencyId,
				Currency = x.Currency.Code + " - " + x.Currency.Description,
				ContractTypeId = x.ContractTypeId,
				ContractType = x.ContractType.Code + " - " + x.ContractType.Description,
				ReceiveDate = x.ReceiveDate,
				RequestDate = x.RequestDate,
				Active = x.Active,
				Status = x.Status,
				OrigStatus = x.Status,
				LabId = x.LabId,
				lab = x.Lab.Location + " -" + x.Lab.Name,
				MasterBillingProject = x.MasterBillingProject,
				ContractNumber = x.ContractNumber,
				LetterNumber = x.LetterNumber,
				ClientNumber = x.ClientNumber,
				ARIAccountCode = x.ARIAccountCode,
				ARIDeptCode = x.ARIDeptCode,
				ARIProjectNo = x.ARIProjectNo,
				ProjectNumber = x.ProjectNumber,
				CertNumber = x.RegulatorRefNum,
				CertificationLabId = x.CertificationLabId,
				CertificationLab = (x.CertificationLab != null) ? _dbSubmission.trLaboratories.Where(y => y.Id == x.CertificationLabId).Select(y => y.Location + " - " + y.Name).FirstOrDefault() : null,
				DraftDate = x.DraftDate,
				CloseDate = x.CloseDate,
				ObsoleteDate = x.ObsoleteDate,
				RevokeDate = x.RevokeDate,
				UpgradeByDate = x.UpgradeByDate,
				UpdateDate = x.UpdateDate,
				UpdateReasonId = x.UpdateReasonId,
				//UpdateReason = (x.UpdateReasonId != null)?((UpdateDateReason) x.UpdateReasonId).ToString() : null,
				RJWDReason = x.RJWDReason,
				ConditionalRevoke = (x.ConditionalRevoke == true) ? true : false,
				PriorityDraft = (x.PriorityDraft == true) ? true : false,
				PdfPath = x.PdfPath,
				JurisdictionGPSPDF = x.JurisdictionGPSPDF,
				ReplaceCount = x.PreviousVersions.Count(),
				RegulatorSpecNum = x.RegulatorSpecNum,
				RegulatorRefNum = x.RegulatorRefNum,
				SpecialNote = (x.SpecialNote == true) ? true : false,
				QuoteDate = x.QuoteAuthDate.QuoteDate,
				AuthorizeDate = x.QuoteAuthDate.AuthorizeDate,
				NationCode = x.Jurisdiction.NationCode,
				JurType = x.Jurisdiction.JurisdictionType,
				TestScriptVersions = x.CheckOffVersion,
				TestScriptVersionIds = _dbSubmission.TblJurCheckOffVersions.Where(y => y.JurisdictionalDataId == x.Id).Select(y => y.CheckOffVersion.Id).ToList(),
				ProductLines = x.ProductLineJurisdictionalData.Select(y => new ProductLine { Id = y.ProductLine.Id, Name = y.ProductLine.Name }).ToList()
			}).SingleOrDefault();
			return Content(ComUtil.JsonEncodeCamelCase(jurisdiction), "application/json");
		}

		[HttpGet]
		public JsonResult BelongsToBundleInfo(List<int> submissionIds, string jurNum)
		{
			//all projects
			var result = _dbSubmission.trProjectJurisdictionalData.Where(x => submissionIds.Contains((int)x.JurisdictionalData.SubmissionId) && x.JurisdictionalData.JurisdictionId == jurNum)
						.Select(x => new
						{
							ProjectId = x.ProjectId,
							BundleId = _dbSubmission.QABundles.Where(y => y.ProjectId == x.ProjectId).Select(y => y.Id).FirstOrDefault(),
							ProjectName = x.Project.ProjectName,
							CreateDate = x.Project.AddDate,
							Completeddate = x.Project.ENCompleteDate,
						}).ToList();

			//only latest projects
			var results1 = (from c in result
							group c by c.ProjectId into g
							select new
							{
								ProjectId = g.Key,
								MaxCreateDate = g.Max(q => q.CreateDate)
							}).ToList();

			var finalResult = (from x in result
							   join y in results1
							   on new { X1 = x.ProjectId, X2 = x.CreateDate } equals new { X1 = y.ProjectId, X2 = y.MaxCreateDate }
							   select x).ToList();

			return Json(finalResult, JsonRequestBehavior.AllowGet);
		}

		[HttpGet]
		public JsonResult GetJurisdictionInfoForSubmissionIds(string submissionIds, string jurNum)
		{
			var lstSubmissionIds = JsonConvert.DeserializeObject<List<int>>(submissionIds);
			var result = _dbSubmission.JurisdictionalData.Where(x => lstSubmissionIds.Contains((int)x.SubmissionId) && x.JurisdictionId == jurNum)
						.Select(x => new
						{
							SubmissionId = x.SubmissionId,
							Name = x.JurisdictionName,
							Status = x.Status,
							SubmitDate = x.RequestDate,
							ReceiveDate = x.ReceiveDate
						}).ToList();
			return Json(result, JsonRequestBehavior.AllowGet);
		}
		#endregion

		#region Add/Update misc DB updates
		private void DeleteStandards(string oldJurId, List<int> jurisdictionDataId)
		{
			//Delete all
			var standardAll = _dbSubmission.tbl_lst_StandardsAll.Where(x => x.JurisdictionId == oldJurId).Select(x => x.Id).ToList();
			foreach (var standardId in standardAll)
			{
				_dbSubmission.tbl_Standards.Where(x => x.StandardsId == standardId && jurisdictionDataId.Contains((int)x.JurisdictionalDataId)).Delete();
			}
		}

		private void UpdateStandards(string JurId, List<int> jurisdictionDataId, string Active, string status, DateTime? closeDate)
		{
			//Delete all
			var standardAll = _dbSubmission.tbl_lst_StandardsAll.Where(x => x.JurisdictionId == JurId).Select(x => x.Id).ToList();
			foreach (var standardId in standardAll)
			{
				var standardList = _dbSubmission.tbl_Standards.Where(x => x.StandardsId == standardId && jurisdictionDataId.Contains((int)x.JurisdictionalDataId)).ToList();
				foreach (var standard in standardList)
				{
					standard.Active = Active;
					if (closeDate != null)
					{
						standard.CloseDate = closeDate;
					}
					standard.Status = status;
					standard.EditDate = DateTime.Now;

					_dbSubmission.Entry(standard).Property(x => x.Active).IsModified = true;
					_dbSubmission.Entry(standard).Property(x => x.CloseDate).IsModified = true;
					_dbSubmission.Entry(standard).Property(x => x.Status).IsModified = true;
					_dbSubmission.Entry(standard).Property(x => x.EditDate).IsModified = true;
				}

				_dbSubmission.SaveChanges();
			}

		}

		private void UpdateQuoteAuthDate(int jurisdictionDataId, string jurisdictionId, DateTime? quoteDate, DateTime? authDate)
		{

			if (!_dbSubmission.tbl_quoteAuthDate.Where(x => x.JurisdictionalDataId == jurisdictionDataId).Any() && specialQuoateJurs.Contains(Convert.ToInt32(jurisdictionId)))
			{
				_dbSubmission.tbl_quoteAuthDate.Add(
						new tbl_quoteAuthDate()
						{
							AuthorizeDate = null,
							QuoteDate = null,
							JurisdictionalDataId = jurisdictionDataId
						}
				 );
			}

			var quoteAuthDate = _dbSubmission.tbl_quoteAuthDate.Where(x => x.JurisdictionalDataId == jurisdictionDataId).SingleOrDefault();
			if (quoteAuthDate != null)
			{
				if (quoteAuthDate.AuthorizeDate != authDate)
				{
					quoteAuthDate.AuthorizeDate = authDate;
					_dbSubmission.Entry(quoteAuthDate).Property(x => x.AuthorizeDate).IsModified = true;
				}
				if (quoteAuthDate.QuoteDate != quoteDate)
				{
					quoteAuthDate.QuoteDate = quoteDate;
					_dbSubmission.Entry(quoteAuthDate).Property(x => x.QuoteDate).IsModified = true;
				}
				_dbSubmission.SaveChanges();
			}
		}

		private void ProcessOnHold(List<int> JurIds, bool onHold, string memo)
		{
			//****On hold********************
			_projectService.PutProjectOrItemsOnHold(new Core.Models.ProjectService.PutItemsOnHold()
			{
				JurisdictionalDataIds = JurIds,
				OnHold = onHold,
				Reason = memo,
				Description = "Other"
			});

		}

		private void UpdateSubmissionConditionalRevoke(List<int> submissionIds)
		{
			var updateSubInfo = _dbSubmission.Submissions.Where(x => submissionIds.Contains(x.Id)).Where(x => x.ConditionalRevoke == null || x.ConditionalRevoke != true).ToList();

			foreach (var submission in updateSubInfo)
			{
				submission.ConditionalRevoke = true;
				_dbSubmission.Entry(submission).State = EntityState.Modified;
				_dbSubmission.UserId = _userContext.User.Id;
				_dbSubmission.SaveChanges();
				_dbSubmission.UserId = null;
			}
		}

		private void AddMemos(List<int> submissionIds, string memo)
		{
			var memoList = new List<tblMemo>();
			foreach (var subId in submissionIds)
			{
				memoList.Add(new tblMemo()
				{
					SubmissionId = (int)subId,
					Memo = memo,
					Username = _userContext.User.Username,
					AddDate = DateTime.Now
				});
			}
			_memoService.AddBulk(memoList);
		}



		#endregion

		#region Helper misc methods

		private bool IsFlatFileManuf(string manufCode)
		{
			var isFlatFileManuf =
			 (_dbSubmission.ManufacturerGroups
			 .Where(ManufacturerGroup => ManufacturerGroup.ManufacturerGroupCategoryId == (Int32)ManufacturerGroupCategory.Flatfile && ManufacturerGroup.Description == "IGT")
			 .SelectMany(ManufacturerGroup => ManufacturerGroup.ManufacturerLinks)
			 .ToList()).Where(x => x.ManufacturerCode == manufCode).Any();

			return isFlatFileManuf;

		}

		private List<Jurisdiction> GetJurisdictionsInfo(int submissionId)
		{
			var jurisdictions = _dbSubmission.JurisdictionalData.Where(x => x.SubmissionId == submissionId).Select(x => new Jurisdiction()
			{
				Id = x.Id,
				JurisdictionId = x.JurisdictionId,
				JurisdictionName = x.JurisdictionName,
				CompanyId = x.CompanyId,
				Company = x.Company.Name,
				CurrencyId = x.CurrencyId,
				Currency = x.Currency.Code + " - " + x.Currency.Description,
				ContractTypeId = x.ContractTypeId,
				ContractType = x.ContractType.Code + " - " + x.ContractType.Description,
				ReceiveDate = x.ReceiveDate.Value.Date,
				RequestDate = x.RequestDate.Value.Date,
				Active = x.Active,
				Status = x.Status,
				OrigStatus = x.Status,
				LabId = x.LabId,
				lab = x.Lab.Location + " -" + x.Lab.Name,
				MasterBillingProject = x.MasterBillingProject,
				ContractNumber = x.ContractNumber,
				LetterNumber = x.LetterNumber,
				ClientNumber = x.ClientNumber,
				ARIAccountCode = x.ARIAccountCode,
				ARIDeptCode = x.ARIDeptCode,
				ARIProjectNo = x.ARIProjectNo,
				ProjectNumber = x.ProjectNumber,
				CertNumber = x.RegulatorRefNum,
				CertificationLabId = x.CertificationLabId,
				CertificationLab = (x.CertificationLab != null) ? _dbSubmission.trLaboratories.Where(y => y.Id == x.CertificationLabId).Select(y => y.Location + " - " + y.Name).FirstOrDefault() : null,
				DraftDate = x.DraftDate,
				CloseDate = x.CloseDate.HasValue ? x.CloseDate.Value.Date : x.CloseDate,
				ObsoleteDate = x.ObsoleteDate,
				RevokeDate = x.RevokeDate,
				UpgradeByDate = x.UpgradeByDate,
				UpdateDate = x.UpdateDate,
				UpdateReasonId = x.UpdateReasonId,
				//UpdateReason = (x.UpdateReasonId != null)?((UpdateDateReason) x.UpdateReasonId).ToString() : null,
				RJWDReason = x.RJWDReason,
				ConditionalRevoke = (x.ConditionalRevoke == true) ? true : false,
				PriorityDraft = (x.PriorityDraft == true) ? true : false,
				PdfPath = x.PdfPath,
				JurisdictionGPSPDF = x.JurisdictionGPSPDF,
				ReplaceCount = x.PreviousVersions.Count(),
				RegulatorSpecNum = x.RegulatorSpecNum,
				RegulatorRefNum = x.RegulatorRefNum,
				SpecialNote = (x.SpecialNote == true) ? true : false,
				QuoteDate = x.QuoteAuthDate.QuoteDate,
				AuthorizeDate = x.QuoteAuthDate.AuthorizeDate,
				NationCode = x.Jurisdiction.NationCode,
				JurType = x.Jurisdiction.JurisdictionType,
				TestScriptVersions = x.CheckOffVersion,
				TestScriptVersionIds = _dbSubmission.TblJurCheckOffVersions.Where(y => y.JurisdictionalDataId == x.Id).Select(y => y.CheckOffVersion.Id).ToList(),
				DraftGPSPDFPath = (x.Status == JurisdictionStatus.DR.ToString()) ? _dbSubmission.SubmissionDrafts.Where(y => y.JurisdictionDataId == x.Id).Select(y => y.DraftLetter.GPSFilePath).FirstOrDefault() : "",
				DraftWebPDFPath = (x.Status == JurisdictionStatus.DR.ToString()) ? _dbSubmission.SubmissionDrafts.Where(y => y.JurisdictionDataId == x.Id).Select(y => y.DraftLetter.FilePath).FirstOrDefault() : "",
				ProductLines = x.ProductLineJurisdictionalData.Select(y => new ProductLine { Id = y.ProductLine.Id, Name = y.ProductLine.Name }).ToList(),
				PurchaseOrder = x.PurchaseOrder
			}).OrderBy(x => x.JurisdictionName).ToList();

			return jurisdictions;
		}

		private string formatMasterBillingProject(string MasterBillingProject)
		{

			var newMasterBillingProject = MasterBillingProject;
			//workorder/MasterBillingProject number logic
			if ((newMasterBillingProject.Length - newMasterBillingProject.Replace("-", "").Length) == 4 // new submission
																										// || (newMasterBillingProject.Length - newMasterBillingProject.Replace("-", "").Length) == 0
				)
			{
				//query = "select dbo.fn_Format_FileToDynamicsProject('" + newMasterBillingProject + "', NULL)";
				var filenumber = new FileNumber(newMasterBillingProject);
				newMasterBillingProject = filenumber.AccountingFormatNoDashes;
			}
			else if ((newMasterBillingProject.Length - newMasterBillingProject.Replace("-", "").Length) == 5) //Transfer project
			{
				//var filenumber = newMasterBillingProject.Substring(0, newMasterBillingProject.LastIndexOf("-"));
				//var jurNumber = newMasterBillingProject.Substring(newMasterBillingProject.LastIndexOf("-") + 1, newMasterBillingProject.Length);

				var transferProject = new FileNumber(newMasterBillingProject);
				newMasterBillingProject = transferProject.AccountingFormatNoDashes;
			}

			return newMasterBillingProject;
		}

		private List<PropertyDBColumnMapping> SetTableColumnMapping(string tableName)
		{
			var mappings = new List<PropertyDBColumnMapping>();
			var entity = Type.GetType($"GLI.EFCore.Submission.{tableName},GLI.EFCore.Submission");
			var entityType = _dbSubmission.Model.FindEntityType(entity);

			foreach (var property in entityType.GetProperties())
			{
				mappings.Add(new PropertyDBColumnMapping
				{
					DBColumnName = property.GetColumnName(),
					PropertyName = property.Name
				});
			};

			return mappings;
		}

		private void DefaultProductLinesIfUniform(int id)
		{
			var productLineIds =
				_dbSubmission.tbl_lst_ProductLinesJurisdictionalData
				.Where(x => x.JurisdictionalData.SubmissionId == id)
				.Select(x => x.ProductLineId)
				.Distinct()
				.ToList();

			if (productLineIds.Count == 1)
			{
				_jurisdictionalDataService.ProductLines.Add(productLineIds.First());
			}
		}
		#endregion
	}
}