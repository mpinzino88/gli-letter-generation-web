﻿using GLI.EFCore.Submission;
using Submissions.Web.Areas.Submission.Models.AristocratData;
using System;
using System.Linq;
using System.Web.Mvc;

namespace Submissions.Web.Areas.Submission.Controllers
{
	public class AristocratDataController : Controller
	{
		private readonly SubmissionContext _dbSubmission;
		private readonly IUserContext _userContext;

		public AristocratDataController(SubmissionContext dbSubmission, IUserContext userContext)
		{
			_dbSubmission = dbSubmission;
			_userContext = userContext;
		}

		public ActionResult Index(int submissionHeaderId)
		{
			var vm = new IndexViewModel();

			var fileInfo = _dbSubmission.SubmissionHeaders.Where(x => x.Id == submissionHeaderId).Select(x => new { FileNumber = x.FileNumber, MainJurisdiction = x.JurisdictionId }).SingleOrDefault();
			vm.FileNumber = fileInfo.FileNumber;
			vm.SubmissionHeaderId = submissionHeaderId;
			var items = _dbSubmission.JurisdictionalData.Where(x => x.Submission.FileNumber == fileInfo.FileNumber)
						.Select(x => new AristocratDataItem
						{
							Jurisdiction = x.Jurisdiction.Name,
							JurisdictionId = x.JurisdictionId,
							IsMainJurisdiction = (x.JurisdictionId == fileInfo.MainJurisdiction) ? true : false
						}).Distinct().ToList();

			var aristocratData = _dbSubmission.AristocratData.Where(x => x.FileNumber == fileInfo.FileNumber).ToList();
			foreach (var item in items)
			{
				var aristocratDataItem = (item.IsMainJurisdiction) ? aristocratData.Where(x => x.JurisdictionId == item.JurisdictionId || (x.JurisdictionId == null || x.JurisdictionId == "")).FirstOrDefault() : aristocratData.Where(x => x.JurisdictionId == item.JurisdictionId).FirstOrDefault();
				if (aristocratDataItem != null)
				{
					item.AristocratType = aristocratDataItem.AristocratType;
					item.AristocratTypeId = aristocratDataItem.AristocratType;
					item.AristocratComplexityId = aristocratDataItem.AristocratComplexity;
					item.AristocratComplexity = aristocratDataItem.AristocratComplexity;
					item.AristocratMarket = aristocratDataItem.Markets.Where(x => x.AristocratDataId == aristocratDataItem.Id && x.IsPrime == true).Select(x => x.Market).FirstOrDefault();
					item.AristocratMarketId = item.AristocratMarket;
					item.SecondaryAristocratMarketIds = aristocratDataItem.Markets.Where(x => x.AristocratDataId == aristocratDataItem.Id && !x.IsPrime).Select(x => x.Market).ToList();
					item.SecondaryAristocratMarkets = item.SecondaryAristocratMarketIds.Select(x => new SelectListItem { Value = x, Text = x, Selected = true }).ToList();
				}
			}

			vm.AristocratItems = items.OrderBy(x => x.AristocratType).OrderBy(x => x.Jurisdiction).ToList();
			return View(vm);
		}

		[HttpPost]
		public ActionResult Save(IndexViewModel vm)
		{
			foreach (var item in vm.AristocratItems)
			{
				var selItem = (item.IsMainJurisdiction) ? _dbSubmission.AristocratData.Where(x => x.FileNumber == vm.FileNumber && (x.JurisdictionId == item.JurisdictionId || (x.JurisdictionId == null || x.JurisdictionId == ""))).FirstOrDefault() : _dbSubmission.AristocratData.Where(x => x.FileNumber == vm.FileNumber && x.JurisdictionId == item.JurisdictionId).FirstOrDefault();
				var itemId = 0;
				if (selItem != null)
				{
					itemId = selItem.Id;
					selItem.AristocratType = item.AristocratTypeId;
					selItem.AristocratComplexity = item.AristocratComplexityId;
					selItem.EditDate = DateTime.Now;
					selItem.EditUserId = _userContext.User.Id;

					_dbSubmission.Entry(selItem).Property(x => x.AristocratType).IsModified = true;
					_dbSubmission.Entry(selItem).Property(x => x.AristocratComplexity).IsModified = true;
					_dbSubmission.Entry(selItem).Property(x => x.EditDate).IsModified = true;
					_dbSubmission.Entry(selItem).Property(x => x.EditUserId).IsModified = true;

					_dbSubmission.AristocratDataMarket.RemoveRange(_dbSubmission.AristocratDataMarket.Where(x => x.AristocratDataId == itemId));
					_dbSubmission.SaveChanges();
				}
				else
				{
					//Add
					if (!string.IsNullOrEmpty(item.AristocratTypeId) || !string.IsNullOrEmpty(item.AristocratComplexityId) || !string.IsNullOrEmpty(item.AristocratMarketId) || item.SecondaryAristocratMarketIds.Count > 0)
					{
						var newItem = new AristocratData()
						{
							FileNumber = vm.FileNumber,
							JurisdictionId = (!item.IsMainJurisdiction) ? item.JurisdictionId : null,
							ProjectType = (item.IsMainJurisdiction) ? "New Submissions" : "",
							AristocratType = item.AristocratTypeId,
							AristocratComplexity = item.AristocratComplexityId,
							AddDate = DateTime.Now,
							AddUserId = _userContext.User.Id
						};
						_dbSubmission.AristocratData.Add(newItem);
						_dbSubmission.SaveChanges();
						itemId = newItem.Id;
					}
				}

				if (itemId != 0)
				{
					if (!string.IsNullOrEmpty(item.AristocratMarketId))
					{
						_dbSubmission.AristocratDataMarket.Add(new AristocratDataMarket()
						{
							AristocratDataId = itemId,
							IsPrime = true,
							Market = item.AristocratMarketId,
						});
					}

					if (item.SecondaryAristocratMarketIds.Count > 0)
					{
						foreach (var market in item.SecondaryAristocratMarketIds)
						{
							_dbSubmission.AristocratDataMarket.Add(new AristocratDataMarket()
							{
								AristocratDataId = itemId,
								Market = market,
							});
						}
					}
					_dbSubmission.SaveChanges();
				}
			}
			return RedirectToAction("Index", new { submissionHeaderId = vm.SubmissionHeaderId });
		}
	}
}