﻿using AutoMapper;
using GLI.EFCore.Submission;
using Microsoft.EntityFrameworkCore;
using Submissions.Common;
using Submissions.Web.Areas.Submission.Models.Shipment;
using Submissions.Web.Models;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace Submissions.Web.Areas.Submission.Controllers
{
	public class ShipmentController : Controller
	{
		private readonly SubmissionContext _dbSubmission;
		private readonly IUserContext _userContext;

		public ShipmentController(SubmissionContext dbSubmission, IUserContext userContext)
		{
			_dbSubmission = dbSubmission;
			_userContext = userContext;
		}

		public ActionResult Index(int submissionId)
		{
			var submissionSubTitle = _dbSubmission.Submissions
				.Where(x => x.Id == submissionId)
				.Select(x => new SubmissionSubTitle
				{
					FileNumber = x.FileNumber,
					IdNumber = x.IdNumber,
					GameName = x.GameName,
					DateCode = x.DateCode,
					Function = x.Function
				})
				.Single();

			var shipments = _dbSubmission.ShippingData
				.Where(x => x.FileNumber == submissionSubTitle.FileNumber)
				.OrderByDescending(x => x.Id)
				.Select(x => new
				{
					x.Id,
					x.Type,
					x.ShippingRecipientCode,
					x.ProcessDate,
					x.Courier,
					x.TrackingNumber,
					x.ChipCount,
					x.DiskCount,
					x.Memo,
					SoftwareCheckUser = x.SoftwareCheckUser.FName + " " + x.SoftwareCheckUser.LName,
					x.SoftwareCheckDate
				})
				.ToList();

			var vm = new IndexViewModel
			{
				SubmissionId = submissionId,
				SubmissionSubTitle = submissionSubTitle,
				Shipments = Mapper.Map<List<ShipmentViewModel>>(shipments)
			};

			return View(vm);
		}

		public ActionResult Edit(int? id, Mode mode, int submissionId, PageSource? source)
		{
			var vm = new EditViewModel();
			if (mode == Mode.Edit)
			{
				var shipment = _dbSubmission.ShippingData.Find(id);
				vm = Mapper.Map<EditViewModel>(shipment);

				vm.CourierId = _dbSubmission.tbl_lst_Shippers.Where(x => x.Code == vm.Courier).Select(x => x.Id).Single();
				vm.ShipmentRecipientId = _dbSubmission.tbl_lst_ShippingRecipients.Where(x => x.ShippedTo == vm.ShipmentRecipient).Select(x => x.Id).Single();
			}

			vm.Mode = mode;
			vm.Source = source;
			vm.SubmissionId = submissionId;

			var cancelUrl = new UrlHelper(this.ControllerContext.RequestContext);
			vm.CancelButtonUrl = cancelUrl.Action("index", "shipment", new { submissionId });
			if (source == PageSource.SubmissionDetailPartialView)
			{
				vm.CancelButtonUrl = cancelUrl.Action("detail", "submission", new { id = submissionId });
			}

			return View(vm);
		}

		[HttpPost]
		public ActionResult Edit(EditViewModel vm)
		{
			var fileNumber = _dbSubmission.Submissions.Where(x => x.Id == vm.SubmissionId).Select(x => x.FileNumber).Single();

			var shipment = new ShippingData();
			if (vm.Mode == Mode.Edit)
			{
				shipment = _dbSubmission.ShippingData.Find(vm.Id);
			}

			shipment.Id = vm.Id ?? 0;
			shipment.ChipCount = vm.ChipCount.ToString();
			shipment.Courier = vm.Courier;
			shipment.DiskCount = vm.DiskCount.ToString();
			shipment.FileNumber = fileNumber;
			shipment.Memo = vm.Memo;
			shipment.ProcessDate = vm.ProcessDate;
			shipment.ShippingRecipientCode = vm.ShipmentRecipient;
			shipment.TrackingNumber = vm.TrackingNumber;
			shipment.Type = vm.Type;

			if (vm.Mode == Mode.Add)
			{
				_dbSubmission.ShippingData.Add(shipment);
			}

			_dbSubmission.SaveChanges();

			if (vm.Source == PageSource.SubmissionDetailPartialView)
			{
				return RedirectToAction("detail", "submission", new { id = vm.SubmissionId });
			}

			return RedirectToAction("index", new { vm.SubmissionId });
		}

		[HttpPost]
		public ActionResult Delete(EditViewModel vm)
		{
			var shipment = new ShippingData { Id = (int)vm.Id };
			_dbSubmission.Entry(shipment).State = EntityState.Deleted;
			_dbSubmission.UserId = _userContext.User.Id;
			_dbSubmission.SaveChanges();

			if (vm.Source == PageSource.SubmissionDetailPartialView)
			{
				return RedirectToAction("detail", "submission", new { id = vm.SubmissionId });
			}

			return RedirectToAction("index", new { vm.SubmissionId });
		}
	}
}