﻿extern alias ZEFCore;
using GLI.EFCore.Submission;
using Submissions.Common;
using Submissions.Web.Areas.Submission.Models.Memo;
using Submissions.Web.Models;
using System;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ZEFCore::Z.EntityFramework.Plus;

namespace Submissions.Web.Areas.Submission.Controllers
{
	public class MemoController : Controller
	{
		private readonly SubmissionContext _dbSubmission;
		private readonly IUserContext _userContext;

		public MemoController(SubmissionContext dbSubmission, IUserContext userContext)
		{
			_dbSubmission = dbSubmission;
			_userContext = userContext;
		}

		public ActionResult Index(int submissionId)
		{
			var memoGNType = MemoType.GN.ToString();
			var memoOHType = MemoType.OH.ToString();
			var memoOHText = MemoType.OH.GetEnumDescription();

			var submissionSubTitle = _dbSubmission.Submissions
					.Where(x => x.Id == submissionId)
					.Select(x => new SubmissionSubTitle
					{
						FileNumber = x.FileNumber,
						IdNumber = x.IdNumber,
						GameName = x.GameName,
						DateCode = x.DateCode,
						Function = x.Function
					})
					.Single();

			var memos = _dbSubmission.tblMemos
				.Where(x => x.SubmissionId == submissionId)
				.Select(x => new MemoViewModel
				{
					Id = x.Id,
					AddDate = x.AddDate,
					Memo = x.Memo,
					Type = memoGNType,
					TypeText = "",
					Username = x.Username
				})
				.AsEnumerable()
				.Concat
				(
					_dbSubmission.tblOHMemos
					.Where(x => x.SubmissionId == submissionId)
					.Select(x => new MemoViewModel
					{
						Id = x.Id,
						AddDate = x.AddDate,
						Memo = x.Memo,
						Type = memoOHType,
						TypeText = memoOHText,
						Username = x.Username
					})
				)
				.OrderByDescending(x => x.AddDate)
				.ToList();

			var vm = new IndexViewModel
			{
				SubmissionId = submissionId,
				SubmissionSubTitle = submissionSubTitle,
				Memos = memos
			};

			return View(vm);
		}

		public ActionResult Edit(int? id, MemoType? type, Mode mode, int submissionId, PageSource? source)
		{
			var vm = new EditViewModel();

			if (mode == Mode.Edit)
			{
				if (type == MemoType.OH)
				{
					vm = _dbSubmission.tblOHMemos
						.Where(x => x.Id == id)
						.Select(x => new EditViewModel { Id = x.Id, Username = x.Username, AddDate = x.AddDate, Memo = x.Memo })
						.Single();

					vm.Type = MemoType.OH;
				}
				else
				{
					vm = _dbSubmission.tblMemos
						.Where(x => x.Id == id)
						.Select(x => new EditViewModel { Id = x.Id, Username = x.Username, AddDate = x.AddDate, Memo = x.Memo })
						.Single();

					vm.Type = MemoType.GN;
				}
			}

			vm.Mode = mode;
			vm.Source = source;
			vm.SubmissionId = submissionId;
			vm.Memo = HttpUtility.HtmlDecode(vm.Memo?.Replace(System.Environment.NewLine, "<br />"));

			var cancelUrl = new UrlHelper(this.ControllerContext.RequestContext);
			vm.CancelButtonUrl = cancelUrl.Action("index", "memo", new { submissionId });
			if (source == PageSource.SubmissionDetailPartialView)
			{
				vm.CancelButtonUrl = cancelUrl.Action("detail", "submission", new { id = submissionId });
			}

			return View(vm);
		}

		[HttpPost]
		public ActionResult Edit(EditViewModel vm, Models.Submission.DetailPartialViews.CopyToSubmissionsViewModel copyTo)
		{
			var now = DateTime.Now;

			if (vm.Mode == Mode.Add)
			{
				vm.CopyToSubmissionIds.Add(vm.SubmissionId);

				if (copyTo.CopyToSubmissions != null)
				{
					var copyToSubmissionIds = copyTo.CopyToSubmissions.Select(x => x.Id).ToList();
					if (copyToSubmissionIds.Count > 0)
					{
						vm.CopyToSubmissionIds = copyToSubmissionIds;
					}
				}
			}

			if (vm.Type == MemoType.OH)
			{
				if (vm.Mode == Mode.Add)
				{
					foreach (var submissionId in vm.CopyToSubmissionIds)
					{
						var memo = new tblOHMemo();
						memo.SubmissionId = submissionId;
						memo.Memo = vm.Memo;
						memo.AddDate = now;
						memo.Username = _userContext.User.Username;
						_dbSubmission.tblOHMemos.Add(memo);
					}
				}
				else
				{
					var memo = _dbSubmission.tblOHMemos.Find((int)vm.Id);
					memo.Memo = vm.Memo;
					memo.AddDate = now;
					memo.Username = _userContext.User.Username;
				}
			}
			else
			{
				if (vm.Mode == Mode.Add)
				{
					foreach (var submissionId in vm.CopyToSubmissionIds)
					{
						var memo = new tblMemo();
						memo.SubmissionId = submissionId;
						memo.Memo = vm.Memo;
						memo.AddDate = now;
						memo.Username = _userContext.User.Username;
						_dbSubmission.tblMemos.Add(memo);
					}
				}
				else
				{
					var memo = _dbSubmission.tblMemos.Find((int)vm.Id);
					memo.Memo = vm.Memo;
					memo.AddDate = now;
					memo.Username = _userContext.User.Username;
				}
			}

			_dbSubmission.SaveChanges();

			if (vm.Source == PageSource.SubmissionDetailPartialView)
			{
				return RedirectToAction("detail", "submission", new { id = vm.SubmissionId });
			}

			return RedirectToAction("index", new { vm.SubmissionId });
		}

		[HttpPost]
		public ActionResult Delete(EditViewModel vm)
		{
			if (vm.Type == MemoType.OH)
			{
				_dbSubmission.tblOHMemos.Where(x => x.Id == vm.Id).Delete();
			}
			else
			{
				_dbSubmission.tblMemos.Where(x => x.Id == vm.Id).Delete();
			}

			if (vm.Source == PageSource.SubmissionDetailPartialView)
			{
				return RedirectToAction("detail", "submission", new { id = vm.SubmissionId });
			}

			return RedirectToAction("index", new { vm.SubmissionId });
		}
	}
}