﻿using GLI.EFCore.Submission;
using Microsoft.EntityFrameworkCore;
using Submissions.Common;
using Submissions.Core;
using Submissions.Web.Areas.Submission.Models.Signature;
using Submissions.Web.Models;
using System;
using System.Linq;
using System.Web.Mvc;

namespace Submissions.Web.Areas.Submission.Controllers
{
	public class SignatureController : Controller
	{
		private readonly SubmissionContext _dbSubmission;
		private readonly IUserContext _userContext;
		private readonly ISignatureService _signatureService;

		public SignatureController(SubmissionContext dbSubmission, IUserContext userContext, ISignatureService signatureService)
		{
			_dbSubmission = dbSubmission;
			_userContext = userContext;
			_signatureService = signatureService;
		}

		public ActionResult Index(int submissionId)
		{
			var signatures = _dbSubmission.tblSignatures
				.Where(x => x.SubmissionId == submissionId)
				.Select(x => new EditViewModel
				{
					Id = x.Id,
					Type = x.Type,
					Scope = x.Scope,
					Signature = x.Signature,
					Version = x.VerifyVersion,
					AlphaVersion = x.ComponentID,
					Seed = x.Seed,
					FilePath = x.FilePath,
					SubmissionId = submissionId
				})
				.OrderBy(x => x.Type)
				.ThenByDescending(x => x.Version)
				.ToList();

			var submissionSubTitle = _dbSubmission.Submissions
				.Where(x => x.Id == submissionId)
				.Select(x => new SubmissionSubTitle
				{
					FileNumber = x.FileNumber,
					IdNumber = x.IdNumber,
					GameName = x.GameName,
					DateCode = x.DateCode,
					Function = x.Function
				})
				.Single();

			var vm = new IndexViewModel
			{
				Signatures = signatures,
				SubmissionId = submissionId,
				HasFilePath = SignatureFilePathVisibility(submissionId),
				SubmissionSubTitle = submissionSubTitle
			};

			return View(vm);
		}

		public ActionResult Edit(int? id, Mode mode, int submissionId, PageSource? source)
		{
			var submission = _dbSubmission.Submissions.Where(x => x.Id == submissionId).Select(x => new { x.FileNumber, TestingType = x.TestingType.Description }).Single();
			var projectStatus = _dbSubmission.trProjects.Where(x => x.ProjectName == submission.FileNumber).Select(x => x.ENStatus).SingleOrDefault();

			var engineerPermission = projectStatus != ProjectStatus.QA.ToString() &&
				projectStatus != ProjectStatus.DN.ToString() &&
				submission.TestingType != TestingType.Sportsbook.ToString();

			var engineerEdit = Util.HasAccess(Permission.Signatures, AccessRight.Edit) && engineerPermission;
			var engineerDelete = Util.HasAccess(Permission.Signatures, AccessRight.Delete) && engineerPermission;

			var vm = new EditViewModel();

			if (mode == Mode.Add)
			{
				vm.ShowSaveButton = Util.HasAccess(Permission.Submission, AccessRight.Add) || engineerEdit;
			}
			else
			{
				var signature = _dbSubmission.tblSignatures.Find(id);
				vm.ShowSaveButton = (Util.HasAccess(Permission.Submission, AccessRight.Edit) || engineerEdit) && signature.Type != SignatureType.GAT.GetEnumDescription();
				vm.ShowDeleteButton = (Util.HasAccess(Permission.Submission, AccessRight.Edit) || engineerDelete) && signature.Type != SignatureType.GAT.GetEnumDescription();
				vm.Id = signature.Id;
				vm.Signature = signature.Signature;
				vm.Type = signature.Type;
				vm.TypeId = signature.TypeOfId;
				vm.Scope = signature.Scope;
				vm.ScopeId = signature.ScopeOfId;
				vm.VersionId = signature.VersionId;
				vm.Version = signature.VerifyVersion;
				vm.Seed = signature.Seed;
				vm.AlphaVersion = signature.ComponentID;
				vm.FilePath = signature.FilePath;
			}

			vm.Mode = mode;
			vm.Source = source;
			vm.SubmissionId = submissionId;
			vm.HasFilePath = SignatureFilePathVisibility(submissionId);

			if (!CheckCustomManufacturer(submissionId))
			{
				vm.TypeLogic.ExcludedTypes.Clear();
			}

			var cancelUrl = new UrlHelper(this.ControllerContext.RequestContext);
			vm.CancelButtonUrl = cancelUrl.Action("index", "signature", new { submissionId });
			if (source == PageSource.SubmissionDetailPartialView)
			{
				vm.CancelButtonUrl = cancelUrl.Action("detail", "submission", new { id = submissionId });
			}

			return View(vm);
		}

		[HttpPost]
		public ActionResult Edit(EditViewModel vm)
		{
			var signature = new tblSignature();
			if (vm.Mode == Mode.Edit)
			{
				signature = _dbSubmission.tblSignatures.Find(vm.Id);
				signature.EditDate = DateTime.Now;
			}
			signature.Id = vm.Id ?? 0;
			signature.SubmissionId = vm.SubmissionId;
			signature.Signature = vm.Signature;
			signature.Type = vm.Type;
			signature.TypeOfId = vm.TypeId;
			signature.Scope = vm.Scope;
			signature.ScopeOfId = vm.ScopeId;
			signature.VersionId = vm.VersionId;
			signature.VerifyVersion = vm.Version;
			signature.Seed = vm.Seed;
			signature.ComponentID = vm.AlphaVersion;
			signature.FilePath = vm.FilePath;

			if (vm.Mode == Mode.Add)
			{
				signature.AddDate = DateTime.Now;
				_dbSubmission.tblSignatures.Add(signature);
			}

			_dbSubmission.UserId = _userContext.User.Id;
			_dbSubmission.SaveChanges();

			if (vm.Source == PageSource.SubmissionDetailPartialView)
			{
				return RedirectToAction("detail", "submission", new { id = vm.SubmissionId });
			}

			return RedirectToAction("index", new { vm.SubmissionId });
		}

		public JsonResult ValidateSignatureLength(string typeId, string signature)
		{
			var signatureValidation = _signatureService.ValidateSignatureLength(typeId, signature);
			return signatureValidation.HasError ? Json(signatureValidation.ErrorMessage, JsonRequestBehavior.AllowGet) : Json(true, JsonRequestBehavior.AllowGet);
		}

		[HttpGet]
		public JsonResult GetDefaultVersion(string signatureTypeId)
		{
			var sigTypeId = int.Parse(signatureTypeId);
			var result = _dbSubmission.tbl_lst_SignatureTypes
				.Where(x => x.Id == sigTypeId && x.DefaultVersion != null)
				.Select(x => new Select2Result { id = (x.DefaultVersionId).ToString(), text = x.DefaultVersion.Version })
				.SingleOrDefault();

			return Json(result, JsonRequestBehavior.AllowGet);
		}

		public bool SignatureFilePathVisibility(int submissionId)
		{
			var testingType = _dbSubmission.Submissions
				.Where(x => x.Id == submissionId && x.TestingTypeId != null)
				.Select(x => x.TestingType.Code)
				.FirstOrDefault();

			return testingType != null && (testingType == TestingType.iGaming.ToString() || testingType == TestingType.Sportsbook.ToString());
		}

		public bool CheckCustomManufacturer(int submissionId)
		{
			var manufacturerCode = _dbSubmission.Submissions
				.Where(x => x.Id == submissionId)
				.Select(x => x.ManufacturerCode)
				.FirstOrDefault();

			return manufacturerCode != null && !new TypeLogic().Manufacturers.Contains(manufacturerCode);
		}

		[HttpPost]
		public JsonResult CheckSignatureExist(EditViewModel vm)
		{
			var signatures = _dbSubmission.tblSignatures
				.Where(x => x.SubmissionId == vm.SubmissionId &&
				x.Signature == vm.Signature &&
				(x.Seed == vm.Seed || x.Seed == (vm.Seed == null ? "" : vm.Seed)) &&
				x.TypeOfId == vm.TypeId &&
				x.ScopeOfId == vm.ScopeId &&
				x.VersionId == vm.VersionId &&
				x.Id != vm.Id)
				.FirstOrDefault();

			return Json(signatures == null ? false : true);
		}

		[HttpPost]
		public ActionResult Delete(EditViewModel vm)
		{
			var signature = new tblSignature { Id = (int)vm.Id };
			_dbSubmission.Entry(signature).State = EntityState.Deleted;
			_dbSubmission.UserId = _userContext.User.Id;
			_dbSubmission.SaveChanges();

			if (vm.Source == PageSource.SubmissionDetailPartialView)
			{
				return RedirectToAction("detail", "submission", new { id = vm.SubmissionId });
			}

			return RedirectToAction("index", new { vm.SubmissionId });
		}
	}
}