﻿using System.Web.Mvc;

namespace Submissions.Web.Areas.Tools
{
	public class ToolsAreaRegistration : AreaRegistration
	{
		public override string AreaName
		{
			get
			{
				return "Tools";
			}
		}

		public override void RegisterArea(AreaRegistrationContext context)
		{
			context.MapRoute(
				"Tools_default",
				"Tools/{controller}/{action}/{id}",
				new { controller = "Tools", action = "Index", id = UrlParameter.Optional }
			);
		}
	}
}