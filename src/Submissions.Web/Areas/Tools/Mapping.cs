﻿using AutoMapper;
using EF.Submission;
using Submissions.Web.Areas.Tools.Models;
using System;

namespace Submissions.Web.Mappings
{
	public class ToolsProfile : Profile
	{
		public ToolsProfile()
		{
			CreateMap<PendingEmail, PendingEmailVM>()
				.ForMember(dest => dest.Jurisdiction, src => src.MapFrom(x => x.Jurisdiction.Name));
			CreateMap<tbl_CertJobNumber, RegulatorCertificationNumberViewModel>()
				.ForMember(dest => dest.Mode, opt => opt.Ignore())
				.ForMember(dest => dest.ArchiveLocation, opt => opt.Ignore())
				.ForMember(dest => dest.PreCertFile, opt => opt.Ignore())
				.ForMember(dest => dest.DisplaySelectedGridModel, opt => opt.Ignore())
				.ForMember(dest => dest.CheckJurisdictionIds, opt => opt.Ignore())
				.ForMember(dest => dest.JurisdictionIdsPost, opt => opt.Ignore())
				.ForMember(dest => dest.JurisdictionDataList, opt => opt.Ignore())
				.ForMember(dest => dest.JurisdictionIndexGrid, opt => opt.Ignore())
				.ForMember(dest => dest.JurisdictionGridData, opt => opt.Ignore())
				.ForMember(dest => dest.DisplayCertificationIndexGrid, opt => opt.Ignore())
				.ForMember(dest => dest.DisplayCertificationIndexGridRW, opt => opt.Ignore())
				.ForMember(dest => dest.CertificationIndexGrid, opt => opt.Ignore())
				.ForMember(dest => dest.CertificationGridData, opt => opt.Ignore());
			CreateMap<JurisdictionalData, JurisdictionIndexGridRecord>()
				.ForMember(dest => dest.Jurisdiction, src => src.MapFrom(x => x.Jurisdiction.Name))
				.ForMember(dest => dest.PrimaryID, src => src.MapFrom(x => x.Id));
			CreateMap<RegCertDocs, RegulatorDocumentViewModel>()
				.ForMember(dest => dest.Mode, opt => opt.Ignore())
				.ForMember(dest => dest.ManfacturerName, opt => opt.MapFrom(src => src.ManufacturerCode))
				.ForMember(dest => dest.ManufacturerName, opt => opt.MapFrom(src => src.ManufacturerCode))
				.ForMember(dest => dest.Jurisdiction, opt => opt.MapFrom(src => src.JurisdictionId))
				.ForMember(dest => dest.PreCertFile, opt => opt.Ignore())
				.ForMember(dest => dest.CertNumber, opt => opt.Ignore())
				.ForMember(dest => dest.GameName, opt => opt.Ignore())
				.ForMember(dest => dest.GameType, opt => opt.Ignore())
				.ForMember(dest => dest.GameId, opt => opt.Ignore())
				.ForMember(dest => dest.GLIEntryDate, opt => opt.Ignore())
				.ForMember(dest => dest.DisplayDocumentIndexGrid, opt => opt.Ignore())
				.ForMember(dest => dest.DocumentIndexGrid, opt => opt.Ignore())
				.ForMember(dest => dest.DocumentGridData, opt => opt.Ignore())
				.ForMember(dest => dest.DisplayDocumentIndexGridRW, opt => opt.Ignore())
				.ForMember(dest => dest.mainView, opt => opt.Ignore())
				.ForMember(dest => dest.CertNumDataList, opt => opt.Ignore())
				.ForMember(dest => dest.certNums, opt => opt.Ignore());
			CreateMap<RegCertId_JurisdictionalData, JurisdictionIndexGridRecord>()
				.ForMember(dest => dest.PrimaryID, opt => opt.Ignore())
				.ForMember(dest => dest.SubmissionFileNumber, opt => opt.Ignore())
				.ForMember(dest => dest.SubmissionIdNumber, opt => opt.Ignore())
				.ForMember(dest => dest.SubmissionGameName, opt => opt.Ignore())
				.ForMember(dest => dest.SubmissionVersion, opt => opt.Ignore())
				.ForMember(dest => dest.Jurisdiction, opt => opt.Ignore());
			CreateMap<RegCertDocs, DocumentIndexGridModel>()
				.ForMember(dest => dest.Grid, opt => opt.Ignore());
			CreateMap<RegCertDocs, DocumentIndexGridRecord>()
				.ForMember(dest => dest.Jurisdiction, opt => opt.MapFrom(src => src.JurisdictionId))
				.ForMember(dest => dest.Edit, opt => opt.Ignore())
				.ForMember(dest => dest.Delete, opt => opt.Ignore());
			CreateMap<tbl_CertJobNumber, CertificationIndexGridModel>()
				.ForMember(dest => dest.CertGrid, opt => opt.Ignore());
			CreateMap<tbl_CertJobNumber, CertificationIndexGridRecord>()
				.ForMember(dest => dest.Edit, opt => opt.Ignore())
				.ForMember(dest => dest.Delete, opt => opt.Ignore());
			CreateMap<tbl_lst_ProtocolDeficiencytbl_lst_Platform, ProtocolPlatform>()
				.ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.tbl_lst_PlatformId))
				.ForMember(dest => dest.ManufacturerGroupId, opt => opt.MapFrom(src => src.tbl_lst_Platform.ManufacturerGroupId))
				.ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.tbl_lst_Platform.Name));
			CreateMap<tbl_lst_ProtocolDeficiency, ProtocolDeficiency>()
				.ForMember(dest => dest.Platforms, opt => opt.MapFrom(src => src.tbl_lst_Platforms))
				.ForMember(dest => dest.AddUserName, opt => opt.MapFrom(src => src.AddUser.FName + " " + src.AddUser.LName));
			CreateMap<tbl_lst_ProtocolDeficiency, SASDeficiency>()
				.ForMember(dest => dest.PollNameId, opt => opt.MapFrom(src => src.tbl_lst_SASPollNameId))
				.ForMember(dest => dest.PollName, opt => opt.MapFrom(src => src.tbl_lst_SASPollName.PollName))
				.ForMember(dest => dest.Platforms, opt => opt.MapFrom(src => src.tbl_lst_Platforms))
				.ForMember(dest => dest.AddUserName, opt => opt.MapFrom(src => src.AddUser.FName + " " + src.AddUser.LName));
			CreateMap<tbl_lst_ProtocolDeficiency, GATDeficiency>()
				.ForMember(dest => dest.Platforms, opt => opt.MapFrom(src => src.tbl_lst_Platforms))
				.ForMember(dest => dest.AddUserName, opt => opt.MapFrom(src => src.AddUser.FName + " " + src.AddUser.LName));
		}
	}
}