using EF.eResultsManaged;
using AutoMapper.QueryableExtensions;
using EF.Submission;
using Kendo.Mvc.Extensions;
using Newtonsoft.Json;
using Submissions.Common;
using Submissions.Core.Models;
using Submissions.Web.Areas.Tools.Models;
using Submissions.Web.Models.Grids.Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using System.Data.Entity;
using GLI.Notification.Monitor.Models;
using Submissions.Core;
using Submissions.Core.Models.ProjectService;
using System.Net;
using Submissions.Common.Email;
using System.Net.Mail;

namespace Submissions.Web.Areas.Tools.Controllers
{
	[AuthorizeUser(Permission = Permission.LetterGeneration, HasAccessRight = AccessRight.Edit)]
	public class ProtocolDeficienciesController : Controller
	{
		private readonly ISubmissionContext _dbSubmission;
		private readonly IUserContext _userContext;
		private readonly IEmailService _emailService;
		public ProtocolDeficienciesController
		(
			IEmailService emailService,
			ISubmissionContext dbSubmission,
			IUserContext userContext
		)
		{
			_dbSubmission = dbSubmission;
			_userContext = userContext;
			_emailService = emailService;
		}
		public ActionResult Index(string mode)
		{
			var query = _dbSubmission.tbl_lst_ProtocolDeficiencies
				.Include(def => def.tbl_lst_Platforms)
				.Include(def => def.tbl_lst_SASPollName)
				.Where(def => mode == null ? def.Lifecycle == "Active" : def.Lifecycle == "Submitted" )
				.OrderBy(def => def.OriginalId);
			//This could be done in one query if Mapping.cs wasn't so limited
			var sasDefs = query.Where(def => def.Protocol == "SAS")
				.ProjectTo<SASDeficiency>()
				.ToList();
			var gatDefs = query.Where(def => def.Protocol == "GAT")
				.ProjectTo<GATDeficiency>()
				.ToList();
			ProtocolDeficienciesIndexViewModel vm = new ProtocolDeficienciesIndexViewModel();
			vm.ReviewMode = mode == "Submitted";
			vm.Deficiencies = new List<ProtocolDeficiency>();
			vm.Deficiencies.AddRange(sasDefs);
			vm.Deficiencies.AddRange(gatDefs);
			return View(vm);
		}
	
		public ActionResult Add(string protocol)
		{
			var vmEdit = new ProtocolDeficienciesEditViewModel();
			vmEdit.Mode = Mode.Add;
			Protocol p = (Protocol)Enum.Parse(typeof(Protocol), protocol, true);//(Protocol)id.GetValueOrDefault();//
			vmEdit.Deficiency = BuildNewDeficiency(p);
			return View("Edit", vmEdit);
		}
		public ActionResult Edit(int deficiencyId, string protocol)
		{
			var vmEdit = new ProtocolDeficienciesEditViewModel();
			vmEdit.Mode = Mode.Edit;
			vmEdit.Deficiency = GetDeficiency(deficiencyId, protocol);
			return View("Edit", vmEdit);
		}
		public ActionResult Review(int deficiencyId, string protocol)
		{
			var vmEdit = new ProtocolDeficienciesEditViewModel();
			vmEdit.Mode = Mode.Edit;
			vmEdit.ReviewMode = true;
			vmEdit.Deficiency = GetDeficiency(deficiencyId, protocol);
			return View("Edit", vmEdit);
		}
		[HttpPost]
		public ActionResult Reject(int deficiencyId, string reason)
		{
			var def = _dbSubmission.tbl_lst_ProtocolDeficiencies.Find(deficiencyId);
			RejectDeficiency(def);
			NotifyUserRejection(def, reason);
			return RedirectToAction("Index");
		}

		private void NotifyUserRejection(tbl_lst_ProtocolDeficiency def, string reason)
		{
			var emailAddress = def.AddUser.Email;
			var mailMsg = new MailMessage("noreply@gaminglabs.com", emailAddress);
			mailMsg.Subject = "Your deficiency has been rejected.";
			mailMsg.Body = BuildRejectionEmailBody(def, reason);
			_emailService.Send(mailMsg);

		}

		private string BuildRejectionEmailBody(tbl_lst_ProtocolDeficiency def, string reason)
		{
			var urlTag = "";//http://submissions/gliintranet/tools/emailsmanagement
			if (_userContext.User.Environment == Common.Environment.Development)
				urlTag = "<a href=\"http://localhost:63298/tools/protocoldeficiencies/edit?deficiencyId=" + def.Id + "&protocol=" + def.Protocol + "\">here</a>";
			else
				urlTag = "<a href=\"http://submissions/gliintranet/tools/protocoldeficiencies/edit?deficiencyId=" + def.Id + "&protocol=" + def.Protocol + "\">here</a>";
			return string.Format("{0},<br/><br/>Your deficiency has been rejected by {1} on {2}. The given explanation: <br/><br/>{3}<br/><br/>" +
				"You can revise this deficiency via Letter Gen or by clicking {4}. The description of the rejected deficiency:<br/><br/>{5}", 
				def.AddUser.FName, 
				_userContext.User.Name, 
				def.DeactivateDate.Value.ToLongDateString(),
				reason,
				urlTag,
				def.Description);
		}

		[HttpPost]
		public ActionResult Save(string deficiencyString, string protocol)
		{
			ProtocolDeficiency deficiencyToSave = null;
			if(protocol == "SAS")
				deficiencyToSave = JsonConvert.DeserializeObject<SASDeficiency>(deficiencyString);
			else deficiencyToSave = JsonConvert.DeserializeObject<GATDeficiency>(deficiencyString);

			tbl_lst_ProtocolDeficiency deficiency = BuildEFDeficiency(deficiencyToSave);
			_dbSubmission.tbl_lst_ProtocolDeficiencies.Add(deficiency);
			_dbSubmission.SaveChanges();
			if (deficiencyToSave.OriginalId == 0)
			{
				deficiency.OriginalId = deficiency.Id;
				_dbSubmission.SaveChanges();
			}
			else if(deficiencyToSave.Lifecycle == "Submitted")
			{
				ApproveDeficiency(deficiencyToSave.Id);
			}
			else
			{
				DeactivateDeficiency(deficiencyToSave.Id);
			}
			return RedirectToAction("Index");
		}
		/// <summary>
		/// This is only used for testing
		/// </summary>
		/// <param name="deficiencyString"></param>
		/// <param name="protocol"></param>
		/// <returns></returns>
		[HttpPost]
		public ActionResult SubmitForReview(string deficiencyString, string protocol)
		{
			ProtocolDeficiency deficiencyToSave = null;
			if (protocol == "SAS")
				deficiencyToSave = JsonConvert.DeserializeObject<SASDeficiency>(deficiencyString);
			else deficiencyToSave = JsonConvert.DeserializeObject<GATDeficiency>(deficiencyString);

			tbl_lst_ProtocolDeficiency deficiency = BuildEFDeficiency(deficiencyToSave);
			deficiency.Lifecycle = "Submitted";
			_dbSubmission.tbl_lst_ProtocolDeficiencies.Add(deficiency);
			_dbSubmission.SaveChanges();
			if (deficiencyToSave.OriginalId == 0)
			{
				deficiency.OriginalId = deficiency.Id;
				_dbSubmission.SaveChanges();
			}
			else if (deficiencyToSave.Lifecycle == "Submitted")
			{
				ApproveDeficiency(deficiencyToSave.Id);
			}
			else
			{
				DeactivateDeficiency(deficiencyToSave.Id);
			}
			return RedirectToAction("Index");
		}

		[HttpGet]
		public ActionResult Delete(int deactivateId)
		{
			DeactivateDeficiency(deactivateId);
			return Content(ComUtil.JsonEncodeCamelCase(0), "application/json");
		}

		#region Helper Methods
		private tbl_lst_ProtocolDeficiency BuildEFDeficiency(ProtocolDeficiency deficiencyToSave)
		{
			var ret = new tbl_lst_ProtocolDeficiency()
			{
				AddUserId = _userContext.User.Id,
				AddDate = DateTime.UtcNow,
				Critical = deficiencyToSave.Critical,
				IncludeOnLetter = deficiencyToSave.IncludeOnLetter,
				IncludeOnWVLetter = deficiencyToSave.IncludeOnWVLetter,
				Description = deficiencyToSave.Description,
				Lifecycle = "Active",
				OriginalId = deficiencyToSave.OriginalId,
				Protocol = deficiencyToSave.Protocol,
				tbl_lst_Platforms = deficiencyToSave.Platforms.Select(plat => new tbl_lst_ProtocolDeficiencytbl_lst_Platform
				{
					tbl_lst_PlatformId = plat.Id
				}).ToList()
			};
			if (deficiencyToSave is SASDeficiency)
			{
				var saveSAS = deficiencyToSave as SASDeficiency;
				ret.Exception = saveSAS.Exception;
				ret.Section = saveSAS.Section;
				ret.LP = saveSAS.LP;
				if (saveSAS.PollNameId == 0)
				{
					ret.tbl_lst_SASPollName = new tbl_lst_SASPollName
					{
						PollName = saveSAS.PollName
					};
				}
				else
				{
					ret.tbl_lst_SASPollNameId = saveSAS.PollNameId;
				}

			}
			else
			{
				var saveGAT = deficiencyToSave as GATDeficiency;
				ret.Command = saveGAT.Command;
				ret.CommandName = saveGAT.CommandName;
				ret.Requirement = saveGAT.Requirement;
			}
			return ret;
		}
		private ProtocolDeficiency BuildNewDeficiency(Protocol protocol)
		{
			switch (protocol)
			{
				case Protocol.SAS:
					return new SASDeficiency()
					{
						Protocol = "SAS",
						PollName = "",
						PollNameId = 0
					};
				case Protocol.GAT:
					return new GATDeficiency()
					{
						Protocol = "GAT"
					};
				default:
					return new ProtocolDeficiency()
					{
						Protocol = "SAS"
					};
			}
		}
		private ProtocolDeficiency GetDeficiency(int deficiencyId, string protocol)
		{
			Protocol p = (Protocol)Enum.Parse(typeof(Protocol), protocol, true);//(Protocol)id.GetValueOrDefault();//
			var query = _dbSubmission.tbl_lst_ProtocolDeficiencies
															.Include(def => def.tbl_lst_Platforms)
															.Include(def => def.tbl_lst_SASPollName)
															.Include(def => def.AddUser)
															.Where(def => def.Id == deficiencyId);
			switch (p)
			{
				case Protocol.SAS:
					return query.ProjectTo<SASDeficiency>().FirstOrDefault();
				case Protocol.GAT:
					return query.ProjectTo<GATDeficiency>().FirstOrDefault();
				default:
					return new ProtocolDeficiency();
			}
		}
		private void RejectDeficiency(tbl_lst_ProtocolDeficiency def)
		{
			def.Lifecycle = "Rejected";
			def.DeactivateUserId = _userContext.User.Id;
			def.DeactivateDate = DateTime.UtcNow;
			_dbSubmission.SaveChanges();
		}
		private void ApproveDeficiency(int id)
		{
			var def = _dbSubmission.tbl_lst_ProtocolDeficiencies.Find(id);
			def.Lifecycle = "Approved";
			def.DeactivateUserId = _userContext.User.Id;
			def.DeactivateDate = DateTime.UtcNow;
			_dbSubmission.SaveChanges();

		}
		private void DeactivateDeficiency(int id)
		{
			var def = _dbSubmission.tbl_lst_ProtocolDeficiencies.Find(id);
			def.Lifecycle = "Deactivated";
			def.DeactivateUserId = _userContext.User.Id;
			def.DeactivateDate = DateTime.UtcNow;
			_dbSubmission.SaveChanges();
		}
		#endregion
	}
}
