using EF.eResultsManaged;
using GLI.EFCore.Submission;
using Kendo.Mvc.Extensions;
using Newtonsoft.Json;
using Submissions.Common;
using Submissions.Core.Models;
using Submissions.Web.Areas.Tools.Models;
using Submissions.Web.Models.Grids.Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace Submissions.Web.Areas.Tools.Controllers
{
	[AuthorizeUser(Permission = Permission.LetterGeneration, HasAccessRight = AccessRight.Edit)]
	public class ClauseTemplateManagementController : Controller
	{
		private readonly SubmissionContext _dbSubmission;
		private readonly IeResultsManagedContext _dbeResultsManaged;
		private readonly IUserContext _userContext;

		public ClauseTemplateManagementController
		(
			SubmissionContext dbSubmission,
			IeResultsManagedContext dbeResultsManaged,
			IUserContext userContext
		)
		{
			_dbSubmission = dbSubmission;
			_dbeResultsManaged = dbeResultsManaged;
			_userContext = userContext;
		}

		public ActionResult Edit(int id, object Mode)
		{
			var vm = new ClauseTemplateManagementEditViewModel();

			var template = _dbeResultsManaged.ClauseTemplates.First(temp => temp.Id == id);
			vm.Id = id;
			vm.ManufacturerId = template.ManufacturerId;
			vm.JurisdictionId = template.JurisdictionId;
			vm.Label = template.Label;

			return View(vm);
		}

		public ActionResult Index(int? deactivate)
		{
			if (deactivate.HasValue)
				DeactivateTemplate(deactivate.Value);
			var vm = new ClauseTemplateManagementIndexViewModel();
			return View(vm);
		}

		[HttpGet]
		public ActionResult LoadClauses(int jurisdictionId, int templateId)
		{
			//var componentId = new Guid(componentGroupId);
			//var result = _evolutionService.GetComponentGroupJurisdictions(componentId);
			var clauses = _dbeResultsManaged.Clause.Join(_dbeResultsManaged.ClauseJuri,
				clause => clause.Id,
				clauseJuri => clauseJuri.ClauseId,
				(clause, clauseJuri) => new
				{
					clause.Id,
					clause.Section,
					clause.SectionName,
					clause.Memo,
					clauseJuri.JuriID
				})
				.Where(joint => joint.JuriID == jurisdictionId)
				//.Take(10)
				.ToList();

			List<ClauseReportModel> ret = new List<ClauseReportModel>();

			//If a templateId is provided, populate the template here
			if (templateId > 0)
			{
				var templates = _dbeResultsManaged.ClauseTemplateExplanations
					.Where(template => template.ClauseTemplateId == templateId && !template.Inactive)
					.Select(template => new
					{
						Id = template.ExplanationLanguage == (int)ClauseTemplateExplanationLanguage.English ? (int?)template.Id : null,
						NativeId = template.ExplanationLanguage == (int)ClauseTemplateExplanationLanguage.Native ? (int?)template.Id : null,
						OriginalId = template.ExplanationLanguage == (int)ClauseTemplateExplanationLanguage.English ? (int?)template.OriginalId : null,
						NativeOriginalId = template.ExplanationLanguage == (int)ClauseTemplateExplanationLanguage.Native ? (int?)template.OriginalId : null,
						template.ClauseId,
						template.FailExplanation,
						template.PassExplanation,
						template.NAExplanation,
						template.NAStarExplanation,
						template.ExplanationLanguage
					})
					.ToList();

				ret = clauses.Select(clause => new ClauseReportModel
				{
					ClauseID = clause.Id,
					Section = clause.Section,
					SectionName = clause.SectionName,
					Memo = clause.Memo,
					JurisdictionId = clause.JuriID,
					Template = new ClauseReportExplanationModel(),
					NativeTemplate = new ClauseReportExplanationModel()
				}).ToList();
				foreach (var clause in ret)
				{
					var englishTemplate = templates.FirstOrDefault(temp => temp.ClauseId == clause.ClauseID && temp.ExplanationLanguage == (int)ClauseTemplateExplanationLanguage.English);
					if (englishTemplate != null)
					{
						clause.Template.Id = englishTemplate?.Id ?? 0;
						clause.Template.OriginalId = englishTemplate?.OriginalId ?? 0;
						clause.Template.PassExplanation = englishTemplate?.PassExplanation ?? null;
						clause.Template.FailExplanation = englishTemplate?.FailExplanation ?? null;
						clause.Template.NAExplanation = englishTemplate?.NAExplanation ?? null;
						clause.Template.NAStarExplanation = englishTemplate?.NAStarExplanation ?? null;
					}
					var nativeTemplate = templates.FirstOrDefault(temp => temp.ClauseId == clause.ClauseID && temp.ExplanationLanguage == (int)ClauseTemplateExplanationLanguage.Native);
					if (nativeTemplate != null)
					{
						clause.NativeTemplate.Id = nativeTemplate?.NativeId ?? 0;
						clause.NativeTemplate.OriginalId = nativeTemplate?.NativeOriginalId ?? 0;
						clause.NativeTemplate.PassExplanation = nativeTemplate?.PassExplanation ?? null;
						clause.NativeTemplate.FailExplanation = nativeTemplate?.FailExplanation ?? null;
						clause.NativeTemplate.NAExplanation = nativeTemplate?.NAExplanation ?? null;
						clause.NativeTemplate.NAStarExplanation = nativeTemplate?.NAStarExplanation ?? null;
					}
				}
				foreach (var clause in ret.Where(clause => (clause.Template.Id != 0 && clause.Template.OriginalId == 0) || (clause.NativeTemplate.Id != 0 && clause.NativeTemplate.OriginalId == 0)))
				{
					if (clause.Template.Id != 0 && clause.Template.OriginalId == 0)
					{
						clause.Template.OriginalId = clause.Template.Id;
					}
					if (clause.NativeTemplate.Id != 0 && clause.NativeTemplate.OriginalId == 0)
					{
						clause.NativeTemplate.OriginalId = clause.NativeTemplate.Id;
					}
				}
			}
			else
			{
				ret = clauses.Select(clause => new ClauseReportModel
				{
					ClauseID = clause.Id,
					Section = clause.Section,
					SectionName = clause.SectionName,
					Memo = clause.Memo,
					JurisdictionId = clause.JuriID,
					Template = new ClauseReportExplanationModel(),
					NativeTemplate = new ClauseReportExplanationModel()
				}).ToList();
			}

			return Content(ComUtil.JsonEncodeCamelCase(ret), "application/json");
		}

		[HttpGet]
		public ActionResult LoadApplicableTemplates()//int jurisdictionId, string manufacturerId)
		{

			var existing = _dbeResultsManaged.ClauseTemplates
				.Where(template => !template.Inactive)
				.Select(template => new ClauseTemplateModel { Id = template.Id, Label = template.Label, JurisdictionId = template.JurisdictionId, ManufacturerId = template.ManufacturerId, Inactive = template.Inactive })
				.ToList();
			var jurIds = existing.Select(template => template.JurisdictionId).Distinct().ToList();
			var mfgIds = existing.Select(template => template.ManufacturerId).Distinct().ToList();
			var juris = _dbSubmission.tbl_lst_fileJuris
				.Where(juri => jurIds.Contains(juri.FixId))
				.Select(juri => new { juri.Name, juri.FormattedJurisdictionName, juri.FixId })
				.ToList();
			var mfgs = _dbSubmission.tbl_lst_fileManu
				.Where(mfg => mfgIds.Contains(mfg.Code))
				.Select(mfg => new { mfg.Description, mfg.Code })
				//.Take(10)
				.ToList();
			foreach (var template in existing)
			{
				var juri = juris.FirstOrDefault(jur => jur.FixId == template.JurisdictionId);
				template.Jurisdiction = string.Format("{0} ({1})",
										string.IsNullOrEmpty(juri.FormattedJurisdictionName) ? juri.Name : juri.FormattedJurisdictionName,
										juri.FixId.ToString());

				var manufacturer = mfgs.FirstOrDefault(mfg => mfg.Code == template.ManufacturerId);
				template.Manufacturer = string.Format("{0} ({1})", manufacturer.Description, manufacturer.Code);
			}
			return Content(ComUtil.JsonEncodeCamelCase(existing), "application/json");
		}

		[HttpGet]
		public ActionResult DeactivateTemplate(int templateId)
		{
			try
			{
				var target = _dbeResultsManaged.ClauseTemplates.First(template => template.Id == templateId);
				target.Inactive = true;
				_dbeResultsManaged.SaveChanges();
			}
			catch (Exception ex)
			{
				return Content(ComUtil.JsonEncodeCamelCase(ex), "application/json");
			}
			return Content(ComUtil.JsonEncodeCamelCase(0), "application/json");

		}

		[HttpPost, ValidateInput(false)]
		public ActionResult SaveNewTemplatePost(string formTemplate, int jurisdictionId, string manufacturerId, string label)
		{
			//int newTemplateId = -1;
			ClauseTemplate dbTemplate = null;
			try
			{
				var clauses = JsonConvert.DeserializeObject<ClauseReportModel[]>(formTemplate);
				var dbClauseExplanations = BuildDBExplanations(clauses, _userContext.User.Id);
				dbTemplate = BuildDBTemplate(jurisdictionId, manufacturerId, label, _userContext.User.Id);
				dbTemplate.ClauseTemplateExplanations.AddRange(dbClauseExplanations);
#if DEBUG
				var errorTesting = clauses.FirstOrDefault(clause => !string.IsNullOrEmpty(clause.Template?.FailExplanation) && clause.Template.FailExplanation.ToLower().Contains("throw"));
				if (errorTesting != null) throw new Exception("Error Testing");
#endif
				_dbeResultsManaged.ClauseTemplates.Add(dbTemplate);
				//TODO: Add error checking
				_dbeResultsManaged.SaveChanges();
				//newTemplateId = dbTemplate.Id;
			}
			catch (Exception ex)
			{
				return Content(ComUtil.JsonEncodeCamelCase(ex), "application/json");
			}
			ClauseReportTemplateModel ret = new ClauseReportTemplateModel
			{
				Id = dbTemplate.Id,
				ManufacturerId = dbTemplate.ManufacturerId,
				JurisdictionId = dbTemplate.JurisdictionId,
				Label = dbTemplate.Label,
				Inactive = dbTemplate.Inactive,
				AddDate = dbTemplate.AddDate,
				UserId = dbTemplate.UserId
			};
			return Content(ComUtil.JsonEncodeCamelCase(ret), "application/json");

		}

		[HttpPost, ValidateInput(false)]
		public ActionResult UpdateTemplatePost(string formTemplate, int templateId)
		{
			using (var ts = _dbeResultsManaged.Database.BeginTransaction())
			{

				try
				{
					var clauses = JsonConvert.DeserializeObject<ClauseReportModel[]>(formTemplate);

					var existingTemplate = _dbeResultsManaged.ClauseTemplates
											.Where(template => template.Id == templateId)
											.Select(template => new { template.Id, template.JurisdictionId, template.Label, template.ManufacturerId })
											.First();
					//TODO: Add error checking
					var jurisdictionId = existingTemplate.JurisdictionId;
					var label = existingTemplate.Label;
					var manufacturerId = existingTemplate.ManufacturerId;
					var dbClauseTemplates = BuildDBExplanations(clauses, _userContext.User.Id);
					dbClauseTemplates.ForEach(clause => clause.ClauseTemplateId = existingTemplate.Id);

#if DEBUG
					var errorTesting = clauses.FirstOrDefault(clause => !string.IsNullOrEmpty(clause.Template?.FailExplanation) && clause.Template.FailExplanation.ToLower().Contains("throw"));
					if (errorTesting != null) throw new Exception("Error Testing");
#endif

					var updatedIds = clauses.Where(clause => clause.Template != null && clause.Template.Modified).Select(clause => clause.Template.Id).ToList();
					updatedIds.AddRange(clauses.Where(clause => clause.NativeTemplate != null && clause.NativeTemplate.Modified).Select(clause => clause.NativeTemplate.Id).ToList());
					//Deactivate the previous versions of these
					//The first time it is saved, it won't have a value for OriginalId - the Id value holds the original originalId
					var oldTemplates = _dbeResultsManaged.ClauseTemplateExplanations
														.Where(explanation =>
														updatedIds.Contains(explanation.Id)
														)
														.ToList();
					foreach (var template in oldTemplates)
						template.Inactive = true;

					_dbeResultsManaged.ClauseTemplateExplanations.AddRange(dbClauseTemplates);
					_dbeResultsManaged.SaveChanges();
				}
				catch (Exception ex)
				{
					ts.Rollback();
					return Content(ComUtil.JsonEncodeCamelCase(ex), "application/json");
				}
				ts.Commit();
			}

			return Content(ComUtil.JsonEncodeCamelCase(0), "application/json");
		}

		[HttpGet]
		public ActionResult GetJurisdictions()
		{
			var jurs = _dbSubmission.tbl_lst_fileJuris
							.Where(jur => jur.Active && jur.FixId != 0)
							.Select(jur => new { jur.Name, jur.FormattedJurisdictionName, jur.FixId })
							//.Take(10)
							.ToList();
			var ret = jurs.OrderBy(jur => jur.FixId).Select(jur =>
							new SelectListItem()
							{
								Text = string.Format("{0} ({1})",
										string.IsNullOrEmpty(jur.FormattedJurisdictionName) ? jur.Name : jur.FormattedJurisdictionName,
										jur.FixId.ToString()),
								Value = jur.FixId.ToString()
							}).ToList();
			return Content(ComUtil.JsonEncodeCamelCase(ret), "application/json");

		}

		[HttpGet]
		public ActionResult GetManufacturers()
		{
			var manufacturers = _dbSubmission.tbl_lst_fileManu
				.Where(mfg => mfg.Active)
				.Select(mfg => new { mfg.Description, mfg.Code })
				//.Take(10)
				.ToList();


			var ret = manufacturers.OrderBy(mfg => mfg.Code).Select(mfg =>
			new SelectListItem()
			{
				Text = string.Format("{0} ({1})", mfg.Description, mfg.Code),
				Value = mfg.Code
			}).ToList();
			return Content(ComUtil.JsonEncodeCamelCase(ret), "application/json");

		}

		#region Helper Methods
		private ClauseTemplate BuildDBTemplate(int jurisdictionId, string manufacturerId, string label, int userId)
		{
			return new ClauseTemplate
			{
				JurisdictionId = jurisdictionId,
				ManufacturerId = manufacturerId,
				Label = label,
				UserId = userId,
				Inactive = false,
				ClauseTemplateExplanations = new List<ClauseTemplateExplanation>()
			};
		}

		private List<ClauseTemplateExplanation> BuildDBExplanations(ClauseReportModel[] clauses, int userId)
		{
			var ret = clauses
				.Where(clause => //Where at least one explanation has a value
				clause.Template != null &&
				clause.Template.Modified)
				//(!string.IsNullOrEmpty(clause.Template.NAStarExplanation) ||
				//!string.IsNullOrEmpty(clause.Template.NAExplanation) ||
				//!string.IsNullOrEmpty(clause.Template.PassExplanation) ||
				//!string.IsNullOrEmpty(clause.Template.FailExplanation)))
				.Select(clause => new ClauseTemplateExplanation
				{
					ClauseId = clause.ClauseID,
					PassExplanation = clause.Template.PassExplanation,
					FailExplanation = clause.Template.FailExplanation,
					NAExplanation = clause.Template.NAExplanation,
					NAStarExplanation = clause.Template.NAStarExplanation,
					OriginalId = clause.Template.OriginalId,
					UserId = userId,
					Inactive = false,
					ExplanationLanguage = (int)ClauseTemplateExplanationLanguage.English
				}).ToList();
			ret.AddRange(clauses
				.Where(clause => //Where at least one explanation has a value
				clause.NativeTemplate != null &&
				clause.NativeTemplate.Modified)
				.Select(clause => new ClauseTemplateExplanation
				{
					ClauseId = clause.ClauseID,
					PassExplanation = clause.NativeTemplate.PassExplanation,
					FailExplanation = clause.NativeTemplate.FailExplanation,
					NAExplanation = clause.NativeTemplate.NAExplanation,
					NAStarExplanation = clause.NativeTemplate.NAStarExplanation,
					OriginalId = clause.NativeTemplate.OriginalId,
					UserId = userId,
					Inactive = false,
					ExplanationLanguage = (int)ClauseTemplateExplanationLanguage.Native
				}).ToList());
			foreach (var clause in ret.Where(clause => clause.PassExplanation == "" || clause.FailExplanation == "" || clause.NAExplanation == "" || clause.NAStarExplanation == ""))
			{
				if (clause.PassExplanation != null && clause.PassExplanation.Trim() == "")
					clause.PassExplanation = null;
				if (clause.FailExplanation != null && clause.FailExplanation.Trim() == "")
					clause.FailExplanation = null;
				if (clause.NAExplanation != null && clause.NAExplanation.Trim() == "")
					clause.NAExplanation = null;
				if (clause.NAStarExplanation != null && clause.NAStarExplanation.Trim() == "")
					clause.NAStarExplanation = null;
			}
			return ret;
		}
		#endregion
	}
}
