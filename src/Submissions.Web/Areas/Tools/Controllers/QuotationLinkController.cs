﻿using GLI.EFCore.Submission;
using Submissions.Web.Areas.Tools.Models;
using System.Linq;
using System.Web.Mvc;

namespace Submissions.Web.Areas.Tools.Controllers
{
	public class QuotationLinkController : Controller
	{
		private readonly SubmissionContext _dbSubmission;

		public QuotationLinkController(SubmissionContext dbSubmission)
		{
			_dbSubmission = dbSubmission;
		}

		#region Index
		public ActionResult Index(int? key, int? primary)
		{
			if (key != null)
			{
				var quoteNumber = _dbSubmission.Submissions.Where(x => x.Id == key).Select(x => x.ContractNumber).Single();
				return View(new QuotationLinkViewModel(quoteNumber));
			}
			else if (primary != null)
			{
				var quoteNumber = _dbSubmission.JurisdictionalData.Where(x => x.Id == primary).Select(x => x.ContractNumber).Single();
				return View(new QuotationLinkViewModel(quoteNumber));
			}

			else
			{
				return View(new QuotationLinkViewModel());
			}
		}

		public ActionResult JurisdictionIndex(int? key, int? primary)
		{
			if (key != null)
			{
				var quoteNumber = _dbSubmission.Submissions.Where(x => x.Id == key).Select(x => x.ContractNumber).Single();
				return View(new QuotationLinkViewModel(quoteNumber));
			}
			else if (primary != null)
			{
				var quoteNumber = _dbSubmission.JurisdictionalData.Where(x => x.Id == primary).Select(x => x.ContractNumber).Single();
				return View(new QuotationLinkViewModel(quoteNumber));
			}
			else
			{
				return View(new QuotationLinkViewModel());
			}
		}
		#endregion

		#region Search
		[HttpPost]
		public JsonResult searchForFile(string QuoteNumber)
		{
			var payload = _dbSubmission.Submissions
			 .Where(x => x.ContractNumber == QuoteNumber)
			 .GroupBy(x => x.FileNumber)
			 .Select(x => new FileNumberResult
			 {
				 FileNumber = x.Key
			 }).ToList();

			return (payload.Count == 0) ? null : Json(payload);
		}

		[HttpPost]
		public JsonResult searchForJurisdiction(string QuoteNumber)
		{
			var payload = _dbSubmission.JurisdictionalData
				//.Include(x => x.Submission)
				.Where(x => x.ContractNumber == QuoteNumber)
				.GroupBy(x => new { x.Submission.FileNumber, x.JurisdictionName })
				.Select(x => new JurisdictionResult
				{
					FileNumber = x.Key.FileNumber,
					Jurisdiction = x.Key.JurisdictionName
				}).ToList();

			return (payload.Count == 0) ? null : Json(payload);
		}
		#endregion

		#region Load Submission Records
		[HttpPost]
		public JsonResult LoadSubmissionRecords(string fileNumber)
		{
			var SubmissionRecordSet = _dbSubmission.Submissions
										.Where(x => x.FileNumber == fileNumber)
										.Select(x => new FileNumberResult
										{
											FileNumber = x.FileNumber,
											IDNumber = x.IdNumber,
											GameName = x.GameName
										}).ToList();
			return (SubmissionRecordSet.Count > 0) ? Json(SubmissionRecordSet) : null;
		}
		#endregion

		#region Load Jurisdiction Records
		[HttpPost]
		public JsonResult LoadJurisdictionRecords(string fileNumber)
		{
			var JurisdictionRecordSet = _dbSubmission.JurisdictionalData
											.Where(x => x.Submission.FileNumber == fileNumber)
											.Select(x => new JurisdictionResult
											{
												FileNumber = x.Submission.FileNumber,
												IDNumber = x.Submission.IdNumber,
												GameName = x.Submission.GameName,
												Jurisdiction = x.JurisdictionName
											}).ToList();

			return (JurisdictionRecordSet.Count > 0) ? Json(JurisdictionRecordSet) : null;
		}
		#endregion
	}

	#region FileNumber Result Object
	public class FileNumberResult
	{
		public string FileNumber { get; set; }
		public string IDNumber { get; set; }
		public string GameName { get; set; }
		public string QuotationLinkFileNumber { get; set; }
		public bool InAnotherChain { get; set; }
	}

	public class JurisdictionResult
	{
		public string FileNumber { get; set; }
		public string IDNumber { get; set; }
		public string GameName { get; set; }
		public string Jurisdiction { get; set; }
		public string QuotationLinkFileNumber { get; set; }
		public bool InAnotherChain { get; set; }
	}
	#endregion
}