﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using EF.Submission;
using Newtonsoft.Json;
using Submissions.Common;
using Submissions.Web.Areas.Tools.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web.Mvc;
using Z.EntityFramework.Plus;

namespace Submissions.Web.Areas.Tools.Controllers
{
	public class RegulatorDocumentController : Controller
	{
		private readonly EF.Submission.ISubmissionContext _dbSubmission;
		private RegCertDocs _item;
		private tbl_CertJobNumber _ArItem;

		public RegulatorDocumentController(ISubmissionContext dbSubmission)
		{
			_dbSubmission = dbSubmission;
		}

		public ActionResult Index()
		{
			var vm = new RegulatorDocumentViewModel();
			vm.DisplayDocumentIndexGrid = new DocumentIndexGridModel();
			vm.DisplayDocumentIndexGridRW = new DocumentIndexGridModelRW();

			return View(vm);
		}

		#region Edit
		public ActionResult Edit(int? id, Mode? mode)
		{
			List<tbl_CertJobNumber> tbl_CertJobNumberItems = new List<tbl_CertJobNumber>();
			List<CertNumData> certNumData_ForModel = new List<CertNumData>();

			if (mode == null)
			{
				mode = Mode.Edit;
			}

			if (mode == Mode.Add)
			{
				_item = new RegCertDocs();
			}
			else
			{
				_item = _dbSubmission.RegCertDocs.Find(id);
				tbl_CertJobNumberItems = _dbSubmission.tbl_CertJobNumber.Where(x => x.DocumentId == id).ToList();
			}

			for (int i = 0; i < tbl_CertJobNumberItems.Count; i++)
			{
				certNumData_ForModel.Add(
						new CertNumData(
							tbl_CertJobNumberItems[i].CertNumber,
							tbl_CertJobNumberItems[i].GameName,
							tbl_CertJobNumberItems[i].GameType,
							tbl_CertJobNumberItems[i].GameId,
							tbl_CertJobNumberItems[i].GLIEntryDate.ToString(),
							tbl_CertJobNumberItems[i].Id));
			}

			foreach (var certItem in certNumData_ForModel)
			{
				if (certItem.GameType != null)
				{
					certItem.GameType = Regex.Replace(certItem.GameType, "([a-z])([A-Z])", "$1 $2");
				}
			}

			var result = Mapper.Map<RegulatorDocumentViewModel>(_item);

			result.Mode = (Mode)mode;
			result.CertNumDataList = certNumData_ForModel;

			return View(result);
		}

		[HttpPost]
		public ActionResult Edit(int? id, string sMode, RegulatorDocumentViewModel selObj, string CertData)
		{
			var deSerializedCertData = JsonConvert.DeserializeObject<List<CertNumData>>(CertData);
			switch (selObj.Mode)
			{
				case Mode.Add:
					_item = new RegCertDocs();
					_item.JurisdictionId = selObj.Jurisdiction;
					_item.Date = selObj.Date;
					_item.ManufacturerCode = selObj.ManufacturerName;

					if (selObj.PreCertFile == null)
					{
						selObj.PreCertFile = selObj.PreCertFile;
					}
					else
					{
						var locationOnServer = System.Configuration.ConfigurationManager.AppSettings["PreCertUploadPath"];
						_item.Path = Path.Combine(locationOnServer, Path.GetFileName(selObj.PreCertFile.FileName));
					}

					_dbSubmission.RegCertDocs.Add(_item);
					_dbSubmission.SaveChanges();
					CertificationNumberSaveChanges(deSerializedCertData, selObj, _item.Id, selObj.Mode);
					break;
				case Mode.Edit:
					_item = _dbSubmission.RegCertDocs.Where(x => x.Id == id).Single();
					_item.JurisdictionId = selObj.Jurisdiction;
					_item.Date = selObj.Date;
					_item.ManufacturerCode = selObj.ManufacturerName;

					if (selObj.PreCertFile == null)
					{
						selObj.PreCertFile = selObj.PreCertFile;
					}
					else
					{
						var locationOnServer = System.Configuration.ConfigurationManager.AppSettings["PreCertUploadPath"];
						_item.Path = Path.Combine(locationOnServer, Path.GetFileName(selObj.PreCertFile.FileName));
					}

					CertificationNumberSaveChanges(deSerializedCertData, selObj, _item.Id, selObj.Mode);
					_dbSubmission.SaveChanges();
					break;
				default:
					throw new Exception("Invalid Edit Mode");
			}

			if (selObj.PreCertFile != null)
			{
				selObj.PreCertFile.SaveAs(_item.Path);
			}

			_dbSubmission.SaveChanges();
			return RedirectToAction("Edit", new { id = _item.Id });
		}
		#endregion

		public ActionResult Delete(int id)
		{
			_item = new RegCertDocs { Id = id };
			_dbSubmission.Entry(_item).State = EntityState.Deleted;

			//remove document link
			var certNumbers = _dbSubmission.tbl_CertJobNumber.Where(x => x.DocumentId == id);

			foreach (var certNumber in certNumbers)
			{
				var certNumberItem = _dbSubmission.tbl_CertJobNumber.Find(certNumber.Id);
				certNumberItem.DocumentId = null;
			}
			_dbSubmission.SaveChanges();

			return RedirectToAction("Index");
		}

		#region Load Document Index Page Grid Data
		[OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]

		public JsonResult LoadDocumentIndexGrid()
		{
			var query = _dbSubmission.RegCertDocs.AsNoTracking().AsQueryable();
			query = query.OrderByDescending(x => x.Date);
			var gridData = query.ProjectTo<DocumentIndexGridRecord>();

			var gridModel = new DocumentIndexGridModel();
			var dataGrid = gridModel.Grid.DataBind(gridData);

			return dataGrid;
		}
		#endregion

		#region Helper functions
		private void CertificationNumberSaveChanges(List<CertNumData> CertData, RegulatorDocumentViewModel selObj, int? id, Mode mode)
		{
			if (CertData.Count == 0 && mode == Mode.Edit)
			{
				_dbSubmission.tbl_CertJobNumber.Where(x => x.DocumentId == id).Delete();
			}
			else
			{
				List<tbl_CertJobNumber> tbl_CertJobNumberItems = _dbSubmission.tbl_CertJobNumber.Where(x => x.DocumentId == id).ToList();

				List<int?> existingCertIdsInDb = tbl_CertJobNumberItems.Select(x => x.Id).ToList();
				List<CertNumData> addCertData = CertData.Where(x => !existingCertIdsInDb.Contains(x.CertId)).ToList();

				UpdateCertificationNumbers(CertData, tbl_CertJobNumberItems, selObj);
				AddCertificationNumbers(addCertData, selObj, id);

				List<int?> existingCertIdsInForm = CertData.Select(x => x.CertId).ToList();
				List<tbl_CertJobNumber> deleteCertData = tbl_CertJobNumberItems.Where(x => !existingCertIdsInForm.Contains(x.Id)).ToList();
				foreach (var x in deleteCertData)
				{
					_dbSubmission.Entry(x).State = EntityState.Deleted;
				}
			}
		}

		private void UpdateCertificationNumbers(List<CertNumData> CertData, List<tbl_CertJobNumber> tbl_CertJobNumberItems, RegulatorDocumentViewModel selObj)
		{
			foreach (var item in tbl_CertJobNumberItems)
			{
				var CertDataItemsMatchingDocumentId = CertData.Where(x => x.CertId == item.Id).FirstOrDefault();

				if (CertDataItemsMatchingDocumentId != null)
				{
					item.Jurisdiction = selObj.Jurisdiction;
					item.PreCertPath = selObj.Path;
					item.CertDate = selObj.Date;
					item.ManufacturerName = selObj.ManufacturerName;
					item.CertNumber = CertDataItemsMatchingDocumentId.CertNumber;
					item.GameName = CertDataItemsMatchingDocumentId.GameName;
					item.GameId = CertDataItemsMatchingDocumentId.GameId;
					item.GameType = CertDataItemsMatchingDocumentId.GameType;
					if (!string.IsNullOrEmpty(CertDataItemsMatchingDocumentId.GameType))
					{
						item.GameType = CertDataItemsMatchingDocumentId.GameType;
					}
					else
					{
						item.GameType = "";
					}
					item.GLIEntryDate = Convert.ToDateTime(CertDataItemsMatchingDocumentId.GLIEntryDate).Date;
				}
			}
		}

		private void AddCertificationNumbers(List<CertNumData> CertData, RegulatorDocumentViewModel selObj, int? id)
		{
			foreach (var item in CertData)
			{
				_ArItem = new tbl_CertJobNumber();
				_ArItem.Jurisdiction = selObj.Jurisdiction;
				_ArItem.ManufacturerName = selObj.ManufacturerName;

				if (selObj.PreCertFile == null)
				{
					_ArItem.PreCertPath = selObj.Path;
				}
				else
				{
					var locationOnServer = System.Configuration.ConfigurationManager.AppSettings["PreCertUploadPath"];
					_ArItem.PreCertPath = Path.Combine(locationOnServer, Path.GetFileName(selObj.PreCertFile.FileName));
				}

				_ArItem.CertDate = selObj.Date;
				_ArItem.CertNumber = item.CertNumber;
				_ArItem.GameName = item.GameName;
				//_ArItem.Id = item.Id;
				_ArItem.GameId = item.GameId;
				if (!string.IsNullOrEmpty(item.GameType))
				{
					_ArItem.GameType = item.GameType;
				}
				else
				{
					item.GameType = "";

				}
				_ArItem.GameType = item.GameType;
				_ArItem.GLIEntryDate = Convert.ToDateTime(item.GLIEntryDate).Date;
				_ArItem.DocumentId = id;
				_ArItem.DataType = "CertNum";
				_dbSubmission.tbl_CertJobNumber.Add(_ArItem);
			}
		}
	}
}
#endregion