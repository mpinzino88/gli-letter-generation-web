﻿using GLI.EFCore.Submission;
using Submissions.Core;
using Submissions.Web.Areas.Tools.Models;
using System.Linq;
using System.Web.Mvc;

namespace Submissions.Web.Areas.Tools.Controllers
{
	public class ImpersonateController : Controller
	{
		private readonly SubmissionContext _dbSubmission;
		private readonly IUserContext _userContext;
		private readonly IActiveDirectoryService _activeDirectoryService;

		public ImpersonateController(SubmissionContext dbSubmission, IUserContext userContext, IActiveDirectoryService activeDirectoryService)
		{
			_dbSubmission = dbSubmission;
			_userContext = userContext;
			_activeDirectoryService = activeDirectoryService;
		}

		public ActionResult Index()
		{
			var vm = new ImpersonateIndexViewModel();
			return View(vm);
		}

		[HttpPost]
		public ActionResult Index(ImpersonateIndexViewModel vm)
		{
			var ldap_sid = _dbSubmission.trLogins.Where(x => x.Id == vm.UserId).Select(x => x.LDAP_SID).Single();
			var user = _activeDirectoryService.GetUserBySID(ldap_sid);

			var principal = new ImpersonatePrincipal("GAMINGLABS\\" + user.sAMAccountName);

			HttpContext.User = principal;
			SessionConfig.Register();

			_userContext.User.Impersonating = true;

			return RedirectToAction("Index", "Home", new { area = "" });
		}

		public ActionResult LogOff()
		{
			SessionConfig.Register();

			return RedirectToAction("Index", "Home", new { area = "" });
		}
	}
}