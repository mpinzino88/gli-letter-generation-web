﻿extern alias ZEFCore;
using AutoMapper.QueryableExtensions;
using EF.eResultsManaged;
using EF.Submission;
using GLI.EF.LetterContent;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
//using System.Data.Entity;
using Submissions.Common;
using Submissions.Core.Models.DTO;
using Submissions.Core.Models.LetterContent;
using Submissions.Web.Areas.Documents.Models.Generate;
using Submissions.Web.Areas.Lookups.Models;
using Submissions.Web.Areas.Tools.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using ZEFCore::Z.EntityFramework.Plus;

namespace Submissions.Web.Areas.Tools.Controllers
{
	public class OCUITController : Controller
	{
		private readonly ISubmissionContext _dbSubmission;
		private readonly IeResultsManagedContext _dbeResultsManaged;
		private readonly ILetterContentContext _dbLetterContent;
		private readonly IUserContext _userContext;

		public OCUITController(ISubmissionContext dbSubmission,
			IeResultsManagedContext dbeResultsManaged,
			ILetterContentContext dbLetterContent,
			IUserContext userContext)
		{
			_dbSubmission = dbSubmission;
			_dbeResultsManaged = dbeResultsManaged;
			_dbLetterContent = dbLetterContent;
			_userContext = userContext;
		}

		public ActionResult Index(int bundleId)//
		{
			var bundleInfo =
			_dbSubmission.QABundles.Where(x => x.Id == bundleId)
			.Include(b => b.Project.JurisdictionalData)
			.Select(x => new GenerateQABundleModel
			{
				BundleId = x.Id,
				ProjectFileNumber = x.Project.FileNumber,
				ProjectId = x.ProjectId,
				ProjectIsTransfer = x.IsTransfer,
				ProjectName = x.ProjectName,
				ProjectENStatus = x.Project.ENStatus,
				QAStatus = x.QAStatus,
				JurisdictionalData = x.Project.JurisdictionalData.Select(y => new GenerateJurisdictionalDataModel
				{
					Primarykey = y.JurisdictionalData.Id,
					JurisdictionFixId = y.JurisdictionalData.Jurisdiction.FixId,
					CertificationLab = y.JurisdictionalData.CertificationLab.Location ?? "NJ",
					JurisdictionId = y.JurisdictionalData.Jurisdiction.Id,
					JurisdictionName = y.JurisdictionalData.Jurisdiction.Name,
					RequestDate = y.JurisdictionalData.RequestDate
				}).ToList(),
				Manufacturer = new ManufacturerDTO
				{
					ManufacturerId = x.Project.JurisdictionalData.FirstOrDefault().JurisdictionalData.Submission.Manufacturer.Code,
				}
			}).Single();

			Models.OCUIT ocuit = LoadOCUIT(bundleInfo.ProjectId);

			var vm = new OCUITIndexView();
			SetProjectInfo(vm, bundleInfo);
			SetJurisdictions(vm, bundleInfo);

			SetPlatformPieceTemplates(vm, ocuit);

			SetOcuitComponentList(vm, ocuit);

			return View("Index", vm);
		}

		private void SetPlatformPieceTemplates(OCUITIndexView vm, Models.OCUIT ocuit)
		{
			if (ocuit?.OCUITComponents == null || ocuit.OCUITComponents.Count == 0)
				return;

			var pieceIds = ocuit.OCUITComponents.Select(sw => sw.PlatformPieceId).Distinct().ToList();
			var templateIds = _dbSubmission.PlatformPieces
				.Where(pp => pieceIds.Contains(pp.Id))
				.Select(pp => pp.PlatformPiecesTemplateId)
				.Distinct()
				.ToList();

			vm.PlatformPiecesTemplates = _dbSubmission.PlatformPiecesTemplates
				.Include(ppt => ppt.PlatformPieces)
				.Where(ppt => templateIds.Contains(ppt.Id))
				.ProjectTo<PlatformPiecesTemplateDTO>()
				.ToList();
		}

		public Models.OCUIT LoadOCUIT(int projectId)
		{

			var projectJurisdictions = _dbSubmission.trProjectJurisdictionalData.Where(x => x.ProjectId == projectId).Select(x => x.JurisdictionalDataId).ToList();
			//get all OCUIIds
			var ocuitId = (from ocj in _dbSubmission.OCUITJurisdictionalDatas
						   join occ in _dbSubmission.OCUITComponents on ocj.OCUITComponentId equals occ.Id
						   join oc in _dbSubmission.OCUITs on occ.OCUITId equals oc.Id
						   where projectJurisdictions.Contains(ocj.JurisdictionalDataId) && oc.Lifecycle == "Active"
						   select oc.Id).FirstOrDefault();
			var projectName = _dbSubmission.trProjects.Where(x => x.Id == projectId).Select(x => x.ProjectName).SingleOrDefault();
			Models.OCUIT ocuit = _dbSubmission.OCUITs
				.Where(o => o.Id == ocuitId)
				.Select(x => new Models.OCUIT
				{
					Id = x.Id,
					OriginalId = x.OriginalId,
					Type = x.Type,

					OCUITComponents = x.OCUITComponents.Select(comp => new Models.OCUITComponent
					{
						Id = comp.Id,
						OrderId = comp.OrderId,
						PlatformPieceId = comp.PlatformPieceId.HasValue ? comp.PlatformPieceId.Value : 0,
						SubmissionId = comp.SubmissionId,
						Continuity = comp.Continuity,
						JurisdictionChip = comp.JurisdictionChip,
						TestedInParallel = comp.TestedInParallel,
						Description = comp.Description,
						Submission = new SubmissionDTO
						{
							Id = comp.SubmissionId,
							FileNumber = comp.Submission.FileNumber,
							IdNumber = comp.Submission.IdNumber,
						},

						AssociatedSoftwareTemplates = comp.AssociatedSoftwareTemplates.Select(temp => new AssociatedSoftwareTemplatesInfo
						{
							AssociatedSoftwareTemplateId = temp.AssociatedSoftwareTemplateId,
							AssociatedSoftwareTemplate = temp.AssociatedSoftwareTemplate.Name,
						}).ToList(),

						Jurisdictions = comp.OCUITJurisdictionalDatas.Select(juri => new JurisdictionInfo
						{
							JurisdictionId = _dbSubmission.tbl_lst_fileJuris.Where(j => j.Id == juri.JurisdictionalData.JurisdictionId).Select(j => j.FixId).FirstOrDefault(),
							Jurisdiction = juri.JurisdictionalData.JurisdictionName,
						}).Distinct().ToList(),

					}).ToList(),

					Platforms = x.Platforms.Select(plat => new ManufacturerPlatform
					{
						Id = plat.PlatformId,
						Name = plat.Platform.Name,
						ManufacturerGroupId = plat.Platform.ManufacturerGroupId,

					}).ToList(),

					Project = new GenerateQABundleModel
					{
						ProjectName = projectName,
					},
				}).SingleOrDefault();

			return ocuit;
		}
		[HttpGet]
		public ActionResult LoadAssociatedSoftwaresTemplatesComponents(string associatedSoftwaresTemplateIds)
		{
			List<int> deserializedIds = new System.Web.Script.Serialization.JavaScriptSerializer().Deserialize<List<int>>(associatedSoftwaresTemplateIds);
			var ret = new TemplateComponentPlatformPiecesDTO();

			ret.Components = _dbSubmission.AssociatedSoftwares
				.Include(sof => sof.AssociatedSoftwareTemplate)
				.ThenInclude(ass => ass.Jurisdictions)
				.ThenInclude(jur => jur.Jurisdiction)

				//.Include(sof => sof.Footnotes)
				.Include(sof => sof.Submission)
				//.ThenInclude(sub => sub.JurisdictionalData)
				.Where(sof => deserializedIds.Contains(sof.AssociatedSoftwareTemplateId))
				.Select(sof => new TemplateComponent
				{
					Id = sof.Id,
					OrderId = sof.OrderId,
					Description = sof.Description,
					ComponentId = sof.SubmissionId,
					PlatformPieceId = sof.PlatformPieceId.HasValue ? sof.PlatformPieceId.Value : 0,
					Component = new SubmissionDTO()
					{
						Id = sof.Id,
						FileNumber = sof.Submission.FileNumber,
						Status = sof.Submission.Status,
						IdNumber = sof.Submission.IdNumber,
						JurisdictionalData = sof.Submission.JurisdictionalData
						.Where(Juri => Juri.Status != "RJ" && Juri.Status != "WD" && Juri.Status != "RV")
						.Select(juri => new JurisdictionalDataDTO
						{
							JurisdictionName = juri.JurisdictionName,
							JurisdictionId = juri.JurisdictionId,
							Active = juri.Status,       //Need to update to correct JurisdictionalDataDTO property 

						})
						.OrderBy(juri => juri.JurisdictionId)
						.ToList(),

					},
					AssociatedSoftwareTemplate = new Lookups.Models.AssociatedSoftwareTemplate
					{
						Name = sof.AssociatedSoftwareTemplate.Name,
						Id = sof.AssociatedSoftwareTemplate.Id,
						Jurisdictions = sof.AssociatedSoftwareTemplate.Jurisdictions.Select(j => new JurisdictionInfo { Jurisdiction = j.Jurisdiction.Name, JurisdictionId = j.Jurisdiction.FixId }).ToList()
					},
					//FootNotes = sof.Footnotes.Select(fn => new TemplateComponentFootnote
					//{
					//	Id = fn.Id,
					//	Memo = fn.Memo

					//})
					//.OrderBy(note => note.Id)
					//.ToList(),
				})
				.OrderBy(comp => comp.ComponentId)
				.ToList();
			var pieceIds = ret.Components.Where(c => c.PlatformPieceId != 0).Select(c => c.PlatformPieceId).Distinct().ToList();
			var templateIds = _dbSubmission.PlatformPieces
				.Where(pp => pieceIds.Contains(pp.Id))
				.Select(pp => pp.PlatformPiecesTemplateId)
				.Distinct()
				.ToList();

			ret.PlatformPiecesTemplates = _dbSubmission.PlatformPiecesTemplates
				.Include(ppt => ppt.PlatformPieces)
				.Where(ppt => templateIds.Contains(ppt.Id))
				.ProjectTo<PlatformPiecesTemplateDTO>()
				.ToList();


			return Content(ComUtil.JsonEncodeCamelCase(ret), "application/json");
		}

		[HttpGet]
		public ActionResult LoadOCUITForCompare(int projectId, string type)
		{
			//type will be used at a later date.
			Models.OCUIT ocuit = LoadOCUIT(projectId);
			return Content(ComUtil.JsonEncodeCamelCase(ocuit), "application/json");

		}
		private void SetProjectInfo(OCUITIndexView vm, GenerateQABundleModel bundle)
		{
			vm.ProjectInfo = new GenerateLetterProjectInfo()
			{
				BundleId = bundle.BundleId,
				ProjectId = bundle.ProjectId,
				ProjectStatus = bundle.ProjectENStatus,
				QAStatus = bundle.QAStatus,
				BundleName = bundle.ProjectName,
				FileNumber = bundle.ProjectFileNumber, //bundle.ProjectName ?? "Project Not Found",
				ManufacturerId = bundle.FileNumber.ManufacturerCode,
			};

			//get Evalution Period from Tasks for new submission bundle, Need to confirm for Transfers 
			//if (bundle.ProjectIsTransfer == false)
			//{

			//}
		}
		private void SetJurisdictions(OCUITIndexView vm, GenerateQABundleModel bundle)
		{
			int trash;

			//var jurIds = bundle.JurisdictionalData.Select(jur => jur.JurisdictionId)
			//			.Distinct()
			//			.Where(juris => int.TryParse(juris, out trash))
			//			.Select(juris => int.Parse(juris))
			//			.ToList();

			var jurisdictionList = bundle.JurisdictionalData
						.Where(juris => int.TryParse(juris.JurisdictionId, out trash))
						.Select(jur => new JurisdictionInfo() { JurisdictionId = int.Parse(jur.JurisdictionId), Jurisdiction = jur.JurisdictionName })
						.DistinctBy(j => j.JurisdictionId)
						.OrderBy(juris => juris.Jurisdiction)
						.ToList();

			var jurIds = jurisdictionList.Select(juris => juris.JurisdictionId)
						.ToList();

			vm.ProjectInfo.JurisdictionIds = jurIds;
			vm.JurisdictionList = jurisdictionList;


		}
		private void SetOcuitComponentList(OCUITIndexView vm, Models.OCUIT ocuit)
		{
			if (ocuit == null)
				return;
			vm.CompatibilityOCUIT = ocuit;
		}

		[HttpPost]
		public JsonResult EditValidate(string compatibilityComponents)
		{
			var errorMsg = new List<string>();
			var components = JsonConvert.DeserializeObject<Models.OCUIT>(compatibilityComponents);
			foreach (var component in components.OCUITComponents)
			{
				var jurisdictionIds = component.Jurisdictions.Select(x => x.JurisdictionId).ToList();
				var jurisdictionIdStr = _dbSubmission.tbl_lst_fileJuris.Where(x => jurisdictionIds.Contains(x.FixId)).Select(x => x.Id).ToList();
				var jurisdictionalData = _dbSubmission.JurisdictionalData.Where(x => x.SubmissionId == component.SubmissionId && jurisdictionIdStr.Contains(x.JurisdictionId)).Select(x => new { SubmissionId = x.Submission.Id, FileNumber = x.Submission.FileNumber, IdNumber = x.Submission.IdNumber, Status = x.Status, JurisdictionName = x.JurisdictionName + "(" + x.JurisdictionId + ")" }).ToList();
				var notApproved = jurisdictionalData.Where(x => (x.Status.ToUpper() != JurisdictionStatus.AP.ToString() && x.Status.ToUpper() != JurisdictionStatus.DR.ToString() && x.Status.ToUpper() != JurisdictionStatus.TC.ToString()))
					.Select(x => "<b>" + x.FileNumber + "</b> / " + x.IdNumber + " - " + x.JurisdictionName).ToList();
				errorMsg.AddRange(notApproved);
			}
			return Json(new { ErrorMsg = errorMsg }, JsonRequestBehavior.AllowGet);

		}

		[HttpPost]
		public ActionResult Save(string compatibilityComponents)
		{

			Models.OCUIT componentsToSave = JsonConvert.DeserializeObject<Models.OCUIT>(compatibilityComponents);

			EF.Submission.OCUIT ocuit = BuildEFOCUIT(componentsToSave);

			_dbSubmission.OCUITs.Add(ocuit);
			_dbSubmission.SaveChanges();

			if (ocuit.OriginalId == 0)
			{
				ocuit.OriginalId = ocuit.Id;
				_dbSubmission.SaveChanges();
			}
			else
			{
				DeactiveOCUIT(componentsToSave.Id);
			}

			return RedirectToAction("Index");
		}

		private void DeactiveOCUIT(int id)
		{
			var ocuit = _dbSubmission.OCUITs.Find(id);
			ocuit.Lifecycle = "Deactivated";
			ocuit.DeactivateUserId = _userContext.User.Id;
			ocuit.DeactivateDate = DateTime.UtcNow;
			_dbSubmission.SaveChanges();
		}

		[HttpGet]
		public ContentResult ValidateComponents(string compatibilityComponents)
		{
			Dictionary<int, List<int>> invalidComponents = new Dictionary<int, List<int>>();
			//List<OCUITComponent> componentsToValidate = compatibilityComponents;
			List<Models.OCUITComponent> componentsToValidate = JsonConvert.DeserializeObject<List<Models.OCUITComponent>>(compatibilityComponents);


			foreach (Models.OCUITComponent component in componentsToValidate)
			{
				int componentId = component.SubmissionId;
				List<int> selectedJuridcitionIds = component.Jurisdictions.Select(juri => juri.JurisdictionId).ToList();

				//EFCore Code
				//List<int> approvedJuridcitionIds = _dbSubmission.JurisdictionalData
				//	.Where(juri => juri.SubmissionId == componentId && juri.Status.ToUpper() != "RJ" && juri.Status.ToUpper() != "WD" && juri.Status.ToUpper() != "RV")
				//	.Select(juri => int.Parse(juri.JurisdictionId)).ToList();

				//EF Code
				//.Where(Juri => ) 
				List<int> approvedJuridcitionIds = _dbSubmission.JurisdictionalData
					.Where(juri => juri.SubmissionId == componentId && juri.Status.ToUpper() != "RJ" && juri.Status.ToUpper() != "WD" && juri.Status.ToUpper() != "RV")
					.Select(juri => juri.JurisdictionId).ToList().Select(id => int.Parse(id)).ToList();

				List<int> invalidJuridcitionIds = selectedJuridcitionIds.Where(id => !approvedJuridcitionIds.Contains(id)).ToList();

				if (invalidJuridcitionIds.Any())
					invalidComponents.Add(componentId, invalidJuridcitionIds);
			}

			return Content(ComUtil.JsonEncodeCamelCase(invalidComponents), "application/json"); ;
		}

		private EF.Submission.OCUIT BuildEFOCUIT(Models.OCUIT componentsToSave)
		{
			var projectJurisdictions = _dbSubmission.trProjectJurisdictionalData.Where(x => x.ProjectId == componentsToSave.ProjectId).Select(x => new { x.JurisdictionalDataId, x.JurisdictionalData.JurisdictionId }).ToList();

			//new to change the return object as database object
			var ret = new EF.Submission.OCUIT()
			{
				Lifecycle = "Active",
				OriginalId = componentsToSave.OriginalId,
				AddUserId = _userContext.User.Id,
				AddDate = DateTime.UtcNow,
				Type = componentsToSave.Type,

				OCUITComponents = componentsToSave.OCUITComponents.Where(comp => comp.PlatformPieceId > 0).Select(comp => new EF.Submission.OCUITComponent
				{
					//update this object to databe object, when it is created
					OrderId = comp.OrderId,
					SubmissionId = comp.SubmissionId,
					PlatformPieceId = comp.PlatformPieceId,
					Continuity = comp.Continuity,
					TestedInParallel = comp.TestedInParallel,
					JurisdictionChip = comp.JurisdictionChip,
					Description = comp.Description,
					AssociatedSoftwareTemplates = comp.AssociatedSoftwareTemplates.Select(TemplateInfo => new OCUITComponentAssociatedSoftwareTemplate
					{
						AssociatedSoftwareTemplateId = TemplateInfo.AssociatedSoftwareTemplateId,
					}).ToList(),

					OCUITJurisdictionalDatas = projectJurisdictions.Where(x => comp.Jurisdictions.Select(jur => jur.JurisdictionId.ToJurisdictionIdString()).Contains(x.JurisdictionId))
											.Select(x => new OCUITJurisdictionalData
											{
												JurisdictionalDataId = x.JurisdictionalDataId
											}).ToList()

				}).ToList(),


				Platforms = componentsToSave.Platforms.Select(plat => new OCUITtbl_lst_Platform
				{
					PlatformId = plat.Id
				}).ToList()
			};



			return ret;
		}
	}
}