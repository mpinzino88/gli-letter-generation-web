﻿extern alias ZEFCore;
using Aspose.Cells;
using AutoMapper.QueryableExtensions;
using GLI.EFCore.Submission;
using Microsoft.EntityFrameworkCore;
using Submissions.Web.Areas.Tools.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.Mvc;
using ZEFCore::Z.EntityFramework.Plus;

namespace Submissions.Web.Areas.Tools.Controllers
{
	public class TKNumberController : Controller
	{
		private readonly SubmissionContext _dbSubmission;
		private readonly IUserContext _userContext;

		public TKNumberController(SubmissionContext dbSubmission, IUserContext userContext)
		{
			_dbSubmission = dbSubmission;
			_userContext = userContext;
		}
		public ActionResult Index(int? primary, string fileNumber, string jurisdiction)
		{
			var vm = new KSANumberIndexViewModel();
			ViewBag.Title = "KSA Numbers for: ";
			ViewBag.fileNumber = fileNumber;
			ViewBag.Jurisdiction = jurisdiction;

			var KSANumbers = _dbSubmission.RegCertId_JurisdictionalData
				.Include(x => x.CertificationNumber)
				.AsNoTracking()
				.Where(x => x.CertificationNumberId == x.CertificationNumber.Id && x.JurisdictionalDataId == primary)
				.OrderByDescending(x => x.CertificationNumber.Id)
				.ThenBy(x => x.CertificationNumber.CertNumber)
				.ProjectTo<KSANumberIndexViewModel>()
				.ToList();

			foreach (var ksaNum in KSANumbers)
			{
				ksaNum.Primary = primary;
				ksaNum.FileNumber = fileNumber;
				ksaNum.Jurisdiction = jurisdiction;
				vm.Id = ksaNum.Id;
			}
			return View(KSANumbers);
		}

		#region Edit
		public ActionResult Edit()
		{
			var vm = new TKNumberEditViewModel();
			return View(vm);
		}

		[HttpPost]
		public ActionResult Edit(int? id, TKNumberEditViewModel vm)
		{
			string fileNumber = vm.FileNumber;
			var user = new trLogin();
			if (vm.saveRecord)
			{
				var submissionsRecords = vm.SubmissionsRecord;
				bool hasSelectedRecords = submissionsRecords.Any(x => x.Selected);
				bool hasTKNumbers = !string.IsNullOrEmpty(vm.TKNumbers);
				if (hasSelectedRecords && hasTKNumbers)
				{
					SaveTkNumbers(vm);
					vm.SubmissionsRecord = submissionsRecords;
					ModelState.Clear();
					vm.saveRecord = false;
					return View(vm);
				}
				else
				{
					ModelState.Clear();
					vm.saveRecord = false;
					return View(vm);
				}
			}
			else
			{
				ModelState.Clear();
				SetTkNumbersFromFile(vm);
				SetSubmissionRecord(vm, fileNumber);
			}
			return View(vm);
		}

		[HttpPost]
		public ActionResult Delete(int? id, int? primary, string fileNumber, string jurisdiction)
		{

			_dbSubmission.tbl_CertJobNumber.Where(x => x.Id == id).Delete();
			var certNumbers = _dbSubmission.RegCertId_JurisdictionalData.Where(x => x.CertificationNumberId == id);

			foreach (var certNumber in certNumbers)
			{
				_dbSubmission.Entry(certNumber).State = Microsoft.EntityFrameworkCore.EntityState.Deleted;
			}
			_dbSubmission.SaveChanges();

			return RedirectToAction("Index", new { primary = primary, fileNumber = fileNumber, jurisdiction = jurisdiction });
		}

		private void SetSubmissionRecord(TKNumberEditViewModel vm, string fileNumber)
		{
			if (!String.IsNullOrEmpty(fileNumber))
			{
				vm.SubmissionsRecord = ((from s in _dbSubmission.Submissions
										 join j in _dbSubmission.JurisdictionalData on s.Id equals j.SubmissionId
										 where (s.FileNumber == fileNumber)
										 orderby s.Id
										 select (new SubmissionsRecord
										 {
											 FileNumber = s.FileNumber,
											 SubmissionKey = s.Id.ToString(),
											 IdNumber = s.IdNumber,
											 GameName = s.GameName,
											 Version = s.Version,
											 DateCode = s.DateCode,
											 Jurisdiction = j.JurisdictionName,
											 JurisdictionType = j.Status,
											 SubmissionsReceivedDate = s.ReceiveDate
										 }))
										   ).ToList();

			}
			if (vm.Jurisdictions.Count > 0 && vm.SubmissionsRecord != null)
			{
				var submissionKeys = vm.SubmissionsRecord.Select(x => (int?)int.Parse(x.SubmissionKey)).ToList();
				var jurSubIds = _dbSubmission.JurisdictionalData.Where(x => submissionKeys.Contains(x.SubmissionId) && vm.Jurisdictions.Contains(x.JurisdictionId)).Select(x => x.Id).ToList();

				vm.SubmissionsRecord = ((from s in _dbSubmission.Submissions
										 join j in _dbSubmission.JurisdictionalData on s.Id equals j.SubmissionId
										 where (jurSubIds.Contains(j.Id))
										 orderby s.Id
										 select (new SubmissionsRecord
										 {
											 FileNumber = s.FileNumber,
											 SubmissionKey = s.Id.ToString(),
											 IdNumber = s.IdNumber,
											 GameName = s.GameName,
											 Version = s.Version,
											 DateCode = s.DateCode,
											 Jurisdiction = j.JurisdictionName,
											 JurisdictionType = j.Status,
											 SubmissionsReceivedDate = s.ReceiveDate
										 }))).ToList();
			}
		}

		private void SetTkNumbersFromFile(TKNumberEditViewModel vm)
		{
			if (vm.TKFileUpload != null && vm.TKFileUpload.Count > 0)
			{
				var file = vm.TKFileUpload.First();
				var tkNumbers = new List<string>();

				if (file.ContentLength > 0)
				{
					var workbook = new Workbook(file.InputStream);
					var worksheet = workbook.Worksheets[0];
					var dataTable = worksheet.Cells.ExportDataTableAsString(0, 0, worksheet.Cells.MaxRow + 1, worksheet.Cells.MaxColumn + 1, true);
					foreach (DataRow r in dataTable.Rows)
					{
						foreach (DataColumn c in dataTable.Columns)
						{
							string value = r.Field<string>(c);
							if (!String.IsNullOrEmpty(value))
							{
								tkNumbers.Add(value);
							}
						}
					}

					vm.TKNumbers = string.Join(",", tkNumbers);
				}
			}
		}
		#endregion

		private void SaveTkNumbers(TKNumberEditViewModel vm)
		{

			var submissionKeys = vm.SubmissionsRecord.Where(x => x.Selected).Select(x => (int?)int.Parse(x.SubmissionKey)).ToList();
			var jurisditionsSelected = vm.SubmissionsRecord.Where(x => x.Selected).Select(x => x.Jurisdiction).ToList();
			var jurPrimaries = _dbSubmission.JurisdictionalData
					.Where(x => submissionKeys.Contains(x.SubmissionId)).Where(y => jurisditionsSelected.Contains(y.JurisdictionName))
					.Select(x => x.Id)
					.ToList();

			foreach (var tkNum in vm.TKNumbers.Split(','))
			{
				var _item = new tbl_CertJobNumber();
				_item.GLIFileNumber = vm.FileNumber;
				_item.CertNumber = tkNum.Trim().Replace(Environment.NewLine, " ");
				_item.LastModifiedLoginId = _userContext.User.Id;
				_item.GLIEntryDate = DateTime.Now;
				_item.DataType = "KSANum";
				_dbSubmission.tbl_CertJobNumber.Add(_item);
				_dbSubmission.SaveChanges();
				var newId = _item.Id;
				foreach (var juridictionPrimary in jurPrimaries)
				{
					_dbSubmission.RegCertId_JurisdictionalData.Add(new RegCertId_JurisdictionalData
					{
						CertificationNumberId = (int)newId,
						JurisdictionalDataId = juridictionPrimary
					});
					_dbSubmission.SaveChanges();
				}
			}
			_dbSubmission.UserId = _userContext.User.Id;
			_dbSubmission.SaveChanges();
			_dbSubmission.UserId = null;
		}

	}
}