﻿extern alias ZEFCore;
using GLI.EFCore.Submission;
using Submissions.Common;
using Submissions.Common.Kendo;
using Submissions.Web.Areas.Tools.Models;
using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ZEFCore::Z.EntityFramework.Plus;

namespace Submissions.Web.Areas.Tools.Controllers
{
	public class ExternalDocumentsController : Controller
	{
		private readonly SubmissionContext _dbSubmission;
		private readonly IUserContext _userContext;

		private string RootPath
		{
			get
			{
				if (_userContext.User.Environment == Common.Environment.Production)
				{
					return PathUtil.FileServer(@"Submissions\Invoices");
				}
				else
				{
					return PathUtil.Temp(@"Submissions\Invoices");
				}
			}
		}

		public ExternalDocumentsController(SubmissionContext dbSubmission, IUserContext userContext)
		{
			_dbSubmission = dbSubmission;
			_userContext = userContext;
		}

		public ActionResult Index()
		{
			var vm = new ExternalDocumentsIndexViewModel();
			vm.RootPathUrl = PathUtil.FileServerUrl(@"Submissions/Invoices");

			return View(vm);
		}

		public ActionResult Edit(int? id, Mode mode)
		{
			var vm = new ExternalDocumentsEditViewModel();

			if (mode == Mode.Add)
			{
				vm.DocumentDate = DateTime.Now;
			}
			else
			{
				var document = _dbSubmission.tbl_ExternalDocuments
					.Include(x => x.Manufacturer)
					.Where(x => x.Id == id)
					.Single();

				vm = new ExternalDocumentsEditViewModel
				{
					Id = document.Id,
					Description = document.Description,
					DocumentDate = document.DocumentDate,
					FileName = document.FileName,
					ManufacturerCode = document.ManufacturerCode,
					Manufacturer = document.Manufacturer.Description,
					Type = document.Type
				};

				if (!string.IsNullOrEmpty(document.FileName))
				{
					var filePath = Path.Combine(this.RootPath, document.DocumentDate.Year.ToString(), document.ManufacturerCode, document.FileName);
					var file = new FileInfo(filePath);

					if (file.Exists)
					{
						vm.ExternalDocuments.Add(new KendoFile { Name = file.Name, Extension = file.Extension, Size = file.Length });
					}
				}
			}

			vm.RootPathUrl = PathUtil.FileServerUrl(@"Submissions/Invoices");
			vm.Mode = mode;

			return View(vm);
		}

		[HttpPost]
		public ActionResult Edit(ExternalDocumentsEditViewModel vm)
		{
			var document = new tbl_ExternalDocuments();

			if (vm.Mode == Mode.Edit)
			{
				document = _dbSubmission.tbl_ExternalDocuments.Where(x => x.Id == vm.Id).Single();
				document.Description = vm.Description;
				_dbSubmission.SaveChanges();
			}
			else
			{
				document.AddDate = DateTime.Now;
				document.AddUserId = _userContext.User.Id;
				document.DocumentDate = vm.DocumentDate;
				document.Description = vm.Description;
				document.FileName = vm.ExternalDocumentsUpload.First().FileName;
				document.ManufacturerCode = vm.ManufacturerCode;
				document.Type = "Invoice";

				_dbSubmission.tbl_ExternalDocuments.Add(document);
				_dbSubmission.SaveChanges();

				AddDocuments(document.Id, vm.ExternalDocumentsUpload);
			}

			return RedirectToAction("Index");
		}

		[HttpPost]
		public ActionResult Delete(ExternalDocumentsEditViewModel vm)
		{
			DeleteDocument((int)vm.Id);
			_dbSubmission.tbl_ExternalDocuments.Where(x => x.Id == vm.Id).Delete();

			return RedirectToAction("Index");
		}

		public ActionResult AddDocuments(int id, IList<HttpPostedFileBase> externalDocumentsUpload)
		{
			var document = _dbSubmission.tbl_ExternalDocuments
				.Where(x => x.Id == id)
				.Single();

			var file = externalDocumentsUpload.First();
			var fileName = file.FileName;

			var path = Path.Combine(this.RootPath, document.DocumentDate.Year.ToString(), document.ManufacturerCode, fileName);

			(new FileInfo(path)).Directory.Create();
			file.SaveAs(path);

			document.FileName = fileName;
			_dbSubmission.SaveChanges();

			return Content("");
		}

		public ActionResult DeleteDocuments(int id)
		{
			DeleteDocument(id);
			return Content("");
		}

		public ActionResult OpenDoc(int? id, string manufacturerCode, DateTime? documentDate, string fileName)
		{
			if (id != null)
			{
				var document = _dbSubmission.tbl_ExternalDocuments
						.Where(x => x.Id == id)
						.Single();

				manufacturerCode = document.ManufacturerCode;
				documentDate = document.DocumentDate;
				fileName = document.FileName;
			}

			TempData["File"] = Path.Combine(this.RootPath, Convert.ToDateTime(documentDate).Year.ToString(), manufacturerCode, fileName);

			return RedirectToAction("Index", "DocumentViewer", new { area = "" });
		}

		private void DeleteDocument(int id)
		{
			var document = _dbSubmission.tbl_ExternalDocuments
				.Where(x => x.Id == id)
				.Select(x => new
				{
					x.DocumentDate,
					x.FileName,
					x.ManufacturerCode,
					x.Type
				})
				.Single();

			var fileName = Path.Combine(this.RootPath, document.DocumentDate.Year.ToString(), document.ManufacturerCode, document.FileName);

			if (System.IO.File.Exists(fileName))
			{
				System.IO.File.Delete(fileName);
			}
		}
	}
}