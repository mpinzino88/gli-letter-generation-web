﻿using AutoMapper.QueryableExtensions;
using EF.Submission;
using Submissions.Common;
using Submissions.Common.Email;
using Submissions.Common.JQGrid;
using Submissions.Web.Areas.Tools.Models;
using System;
using System.Linq;
using System.Net.Mail;
using System.Web.Mvc;

namespace Submissions.Web.Areas.Tools.Controllers
{
	public class PendingQueueController : Controller
	{
		private readonly ISubmissionContext _dbSubmission;
		private readonly IEmailService _emailService;
		private readonly IUserContext _userContext;

		public PendingQueueController(ISubmissionContext dbSubmission, IEmailService emailService, IUserContext userContext)
		{
			_dbSubmission = dbSubmission;
			_emailService = emailService;
			_userContext = userContext;
		}

		public ActionResult Index()
		{
			var results = new PendingQueueViewModel();

			var PendingQueueSearch = new QueueSearchVM();

			PendingQueueSearch.IsPending = true;
			Session["FilterVM"] = PendingQueueSearch;

			results.QueueSearch = PendingQueueSearch;
			results.PendingEmailGrid = new PendingGridModel();

			return View("Index", results);
		}

		public ActionResult SendEmail(int emailId)
		{
			//get file size
			var selItem = _dbSubmission.PendingEmails.Find(emailId);
			string sPDFPath = selItem.AttachPath.ToString();

			/* var impersonateUsername = ConfigurationManager.AppSettings["MonitorAccountUsername"];
			 var impersonatePassword = ConfigurationManager.AppSettings["MonitorAccountPassword"];
			 using (var impersonatedUser = new ImpersonateUser(Domain.GamingLabs, impersonateUsername, impersonatePassword))
			 {
				 FileInfo file = new FileInfo(sPDFPath);
				 sFileSize = Convert.ToInt32(file.Length);
			   
			 }

			 int iSuccess = _dbSubmission.Database.ExecuteSqlCommand("exec SendAutomatedEmails " + emailId + "," + sFileSize);
			 * */

			selItem.IsEnforceRun = true;
			_dbSubmission.SaveChanges();

			return RedirectToAction("Index");
		}

		[AuthorizeUser(Permission = Permission.PDFEmailQueue, HasAccessRight = AccessRight.Edit)]
		public ActionResult SaveGrid(string emailId, string oper, PendingGridModel item, FormCollection formCollection)
		{
			var selItem = _dbSubmission.PendingEmails.Find(Convert.ToInt32(emailId));
			string emailBody;
			string emailSubject;
			if (oper == "edit")
			{
				selItem.To = formCollection.Get("To").ToString();

				emailSubject = "PDf queue was modified for " + selItem.FileNumber;
				emailBody = "PDF queue was modified for " + selItem.FileNumber + " and " + selItem.Jurisdiction.Name + ".";
				_dbSubmission.SaveChanges();
			}
			else
			{
				emailSubject = "PDf email removed for " + selItem.FileNumber;
				emailBody = "PDF email was removed for " + selItem.FileNumber + " and " + selItem.Jurisdiction.Name + ".";
				//_dbSubmission.Entry(selItem).State = EntityState.Deleted;
				selItem.IsDeleted = true; //as Per track it-74976
				_dbSubmission.SaveChanges();
			}

			//sends an email
			var mailMsg = new MailMessage();
			var emailToAddr = (from x in _dbSubmission.EmailSubscriptions
							   where x.EmailType.Code == "PendingPDFEmail"
							   select x.User.Email).ToList();

			foreach (var recipient in emailToAddr) { mailMsg.To.Add(new MailAddress(recipient)); }
			var FromId = (from x in _dbSubmission.trLogins
						  where x.Id == _userContext.User.Id
						  select x.Email).Single();
			mailMsg.From = new MailAddress(FromId.ToString());
			mailMsg.Subject = emailSubject;
			mailMsg.Body = emailBody;
			_emailService.Send(mailMsg);

			return RedirectToAction("Index");
		}

		[HttpPost]
		public ActionResult Search(PendingQueueViewModel indexView)
		{
			Session["FilterVM"] = indexView.QueueSearch;
			var vm = new PendingQueueViewModel
			{
				PendingEmailGrid = new PendingGridModel(),
				QueueSearch = indexView.QueueSearch
			};

			return View("Index", vm);
		}

		public void Export(PendingQueueViewModel indexView)
		{
			var searchFilters = indexView.QueueSearch;

			var query = _dbSubmission.PendingEmails.AsQueryable();

			if (searchFilters.JurisdictionId != null)
			{
				query = query.Where(x => x.JurisdictionID == searchFilters.JurisdictionId);
			}

			if (searchFilters.ManufacturerCode != null)
			{
				var manuCode = searchFilters.ManufacturerCode;//searchFilters.Manufacturer.Substring(0, searchFilters.Manufacturer.IndexOf('-')).Trim();
				query = query.Where(x => x.FileNumber.Contains(manuCode));
			}

			if (searchFilters.FileNumber != null)
			{
				query = query.Where(x => x.FileNumber.Contains(searchFilters.FileNumber));
			}

			if (searchFilters.SentFrom != null)
			{
				query = query.Where(x => x.SentTS >= searchFilters.SentFrom);
			}

			if (searchFilters.SentTo != null)
			{
				query = query.Where(x => x.SentTS <= searchFilters.SentTo);
			}

			if (searchFilters.IsDeleted == true)
			{
				query = query.Where(x => x.IsDeleted == true);
			}
			else
			{
				query = query.Where(x => x.IsDeleted == false || x.IsDeleted == null);
			}

			if (searchFilters.IsPending == true)
			{
				query = query.Where(x => x.SentTS == null).Where(x => x.IsEnforceRun == false || x.IsEnforceRun == null).Where(x => x.IsFailed == null || x.IsFailed == 0);
			}

			query = query.OrderByDescending(x => x.CreateTS).AsQueryable();
			var emailGrid = new PendingGridModel();
			var export = new Export
			{
				ExportType = ExportType.Csv,
				FiltersToolbar = "",
				SavedColumns = "",
				Title = "PDF Emails"
			};
			emailGrid.Export(export, query);
		}

		#region Load Grid Data
		[OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
		public JsonResult LoadPendingListGrid()
		{
			var searchFilters = (QueueSearchVM)Session["FilterVM"];

			var query = _dbSubmission.PendingEmails.AsQueryable();

			if (searchFilters.JurisdictionId != null)
			{
				query = query.Where(x => x.JurisdictionID == searchFilters.JurisdictionId);
			}

			if (searchFilters.ManufacturerCode != null)
			{
				var manuCode = searchFilters.ManufacturerCode;//searchFilters.Manufacturer.Substring(0, searchFilters.Manufacturer.IndexOf('-')).Trim();
				query = query.Where(x => x.FileNumber.Contains(manuCode));
			}

			if (searchFilters.FileNumber != null)
			{
				query = query.Where(x => x.FileNumber.Contains(searchFilters.FileNumber));
			}

			if (searchFilters.SentFrom != null)
			{
				query = query.Where(x => x.SentTS >= searchFilters.SentFrom);
			}

			if (searchFilters.SentTo != null)
			{
				query = query.Where(x => x.SentTS <= searchFilters.SentTo);
			}

			if (searchFilters.IsDeleted == true)
			{
				query = query.Where(x => x.IsDeleted == true);
			}
			else
			{
				query = query.Where(x => x.IsDeleted == false || x.IsDeleted == null);
			}

			if (searchFilters.IsPending == true)
			{
				query = query.Where(x => x.SentTS == null).Where(x => x.IsEnforceRun == false || x.IsEnforceRun == null).Where(x => x.IsFailed == null || x.IsFailed == 0);
			}

			var emailGrid = new PendingGridModel();

			query = query.OrderByDescending(x => x.CreateTS).AsQueryable();
			var results = query.ProjectTo<PendingEmailVM>();

			return emailGrid.Grid.DataBind(results);
		}
		#endregion

	}
}