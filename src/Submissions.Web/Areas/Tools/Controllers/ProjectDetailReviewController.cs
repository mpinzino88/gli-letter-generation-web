﻿extern alias ZEFCore;
using GLI.EFCore.Submission;
using Submissions.Common;
using Submissions.Web.Areas.Tools.Models;
using System;
using System.Linq;
using System.Web.Mvc;
using ZEFCore::Z.EntityFramework.Plus;

namespace Submissions.Web.Areas.Tools.Controllers
{
	[AuthorizeUser(Permission = Permission.Submission, HasAccessRight = AccessRight.Edit)]
	public class ProjectDetailReviewController : Controller
	{
		private readonly SubmissionContext _dbSubmission;
		private readonly IUserContext _userContext;

		public ProjectDetailReviewController
		(
			SubmissionContext dbSubmission,
			IUserContext userContext
		)
		{
			_dbSubmission = dbSubmission;
			_userContext = userContext;
		}

		public ActionResult Index(DateTime? startDate, DateTime? endDate, string mode)
		{
			var vm = new ProjectDetailReviewIndexViewModel();
			vm.SessionUser = _userContext.User.FName + " " + _userContext.User.LName;
			vm.StartDate = mode == "Search" ? (DateTime)startDate : DateTime.Now.AddDays(-1).Date;
			vm.EndDate = endDate;

			var query = _dbSubmission.Submissions
				.Where(x => x.SubmitDate >= vm.StartDate &&
					(x.Type == SubmissionType.CS.ToString() ||
					x.Type == SubmissionType.OS.ToString() ||
					x.Type == SubmissionType.PS.ToString()));

			if (vm.EndDate != null)
			{
				query = query.Where(x => x.SubmitDate <= vm.EndDate);
			}

			var submissions = query
				.GroupBy(x => new { x.FileNumber, x.ProjectDetail })
				.Select(x => new ProjectDetailReviewSubmissionViewModel
				{
					SubmissionHeaderId = x.Max(y => (int)y.SubmissionHeaderId),
					FileNumber = x.Key.FileNumber,
					ProjectDetail = x.Key.ProjectDetail,
					SubmitDate = x.Max(y => y.SubmitDate),
					EditUser = _dbSubmission.HistoryLogs
						.Where(y => y.Table == "Submissions" && y.Column == "projDetail" && y.PrimaryKey == x.Max(z => z.Id))
						.Select(y => y.User.FName + " " + y.User.LName)
						.FirstOrDefault()
				})
				.OrderBy(x => x.SubmitDate)
				.ThenBy(x => x.FileNumber)
				.ToList();

			vm.Submissions = submissions;

			return View(vm);
		}

		[HttpPost]
		public ActionResult Process(int submissionHeaderId, string projectDetail)
		{
			_dbSubmission.UserId = _userContext.User.Id;
			_dbSubmission.Submissions
				.Where(x => x.SubmissionHeaderId == submissionHeaderId)
				.Update(x => new GLI.EFCore.Submission.Submission { ProjectDetail = projectDetail });

			return Json("ok");
		}
	}
}