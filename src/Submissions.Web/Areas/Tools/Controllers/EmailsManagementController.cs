﻿extern alias ZEFCore;
using GLI.EFCore.Submission;
using Submissions.Common;
using Submissions.Web.Areas.Tools.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Mail;
using System.Web.Mvc;
using ZEFCore::Z.EntityFramework.Plus;

namespace Submissions.Web.Areas.Tools.Controllers
{
	using Submissions.Common.Email;

	public class EmailsManagementController : Controller
	{
		private readonly SubmissionContext _dbSubmission;
		private readonly IEmailService _emailService;
		private readonly IUserContext _userContext;

		public EmailsManagementController(SubmissionContext submissionContext, IUserContext userContext, IEmailService emailService)
		{
			_dbSubmission = submissionContext;
			_userContext = userContext;
			_emailService = emailService;
		}

		public ActionResult Index(bool showManufactureData = false, bool showJurisdictionData = false, bool showPDFRules = false)
		{
			var vm = new EmailsManagementViewModel();
			vm.ViewJurisdictionData = (!showManufactureData && !showJurisdictionData && !showPDFRules) ? true : showJurisdictionData;
			vm.ViewManufactureData = showManufactureData;
			vm.ViewPDFRules = showPDFRules;

			if (vm.ViewManufactureData)
			{
				vm.ManufactureData = GetManufacturesEmailInfo();
			}

			if (vm.ViewJurisdictionData)
			{
				vm.JurisdictionData = GetJurisdictionsEmailInfo();
			}

			//if show pdfrules data is getting through grid load
			vm.CanEditData = (Util.HasAccess(Permission.EmailsManagement, AccessRight.Edit)) ? true : false;

			return View("Index", vm);
		}

		#region Manufacturer

		private List<ManufEmailData> GetManufacturesEmailInfo()
		{
			var manufEmails = (from m in _dbSubmission.tbl_lst_fileManu
							   where m.Active == true
							   select (new ManufEmailData()
							   {
								   Description = m.Description,
								   Code = m.Code,
								   IsDirtyDaily = false,
								   IsDirtyStatus = false,
								   IsVisible = true
							   })).OrderBy(m => m.Code).ToList();

			var manufDaily = (from m in manufEmails
							  join eu in _dbSubmission.EmailDistributionUsers on m.Code equals eu.ManufacturerCode
							  join e in _dbSubmission.EmailDistributions on eu.EmailDistributionId equals e.Id
							  where e.Code == EmailDistributionType.ManufDailySummary.ToString()
							  group eu by eu.ManufacturerCode into g
							  select new
							  {
								  Code = g.Key,
								  //Internalusers = g.ToList().Where(x => x.UserId != null).Select(x => x.User.Email).ToList(),
								  //ExternalEmails = g.ToList().Where(x => x.ExternalEmail != null).Select(x => x.ExternalEmail).ToList()
								  Users = g.ToList()
							  }).ToList();

			var manufStatus = (from m in manufEmails
							   join eu in _dbSubmission.EmailDistributionUsers on m.Code equals eu.ManufacturerCode
							   join e in _dbSubmission.EmailDistributions on eu.EmailDistributionId equals e.Id
							   where e.Code == EmailDistributionType.ManufStatusEmail.ToString()
							   group eu by eu.ManufacturerCode into g
							   select new
							   {
								   Code = g.Key,
								   //Internalusers = g.ToList().Where(x => x.UserId != null).Select(x => x.User.Email).ToList(),
								   //ExternalEmails = g.ToList().Where(x => x.ExternalEmail != null).Select(x => x.ExternalEmail).ToList(),
								   Users = g.ToList()
							   }).ToList();


			foreach (var m in manufEmails)
			{
				if (manufDaily.Find(x => x.Code == m.Code) != null)
				{
					var dailyEmailInternal = (manufDaily.Find(x => x.Code == m.Code).Users).Where(x => x.User != null).Select(x => x.User.Email).ToList();
					var dailyEmail = (manufDaily.Find(x => x.Code == m.Code).Users).Where(x => x.ExternalEmail != null).Select(x => x.ExternalEmail).ToList();
					m.DailySummary = string.Join(",", dailyEmail);
					if (dailyEmailInternal.Count() > 0)
					{
						m.DailySummary += (m.DailySummary != "") ? "," + string.Join(",", dailyEmailInternal) : string.Join(",", dailyEmailInternal);
					}
				}
				if (manufStatus.Find(x => x.Code == m.Code) != null)
				{
					var statusEmailInternal = (manufStatus.Find(x => x.Code == m.Code).Users).Where(x => x.User != null).Select(x => x.User.Email).ToList();
					var statusEmail = (manufStatus.Find(x => x.Code == m.Code).Users).Where(x => x.ExternalEmail != null).Select(x => x.ExternalEmail).ToList();
					m.StatusChange = string.Join(",", statusEmail);
					if (statusEmailInternal.Count() > 0)
					{
						m.StatusChange += (m.StatusChange != "") ? "," + string.Join(",", statusEmailInternal) : string.Join(",", statusEmailInternal);
					}
				}

			}

			return manufEmails;
		}

		[HttpPost]
		public ContentResult SaveManufactureData(List<ManufEmailData> vm)
		{
			if (vm.Any(x => x.IsDirtyDaily))
			{
				var dailyEmails = vm.Where(x => x.IsDirtyDaily).Select(x => x.Code).ToList();
				foreach (var manufCode in dailyEmails)
				{
					var manufEmails = vm.Where(x => x.Code == manufCode).Select(x => x.DailySummary).SingleOrDefault();
					var lstEmails = (manufEmails != null) ? manufEmails.Split(',').ToList() : new List<string>();
					SaveManufData(EmailDistributionType.ManufDailySummary, manufCode, lstEmails);
				}
			}

			if (vm.Any(x => x.IsDirtyStatus))
			{
				var statusEmails = vm.Where(x => x.IsDirtyStatus).Select(x => x.Code).ToList();
				foreach (var manufCode in statusEmails)
				{
					var manufEmails = vm.Where(x => x.Code == manufCode).Select(x => x.StatusChange).SingleOrDefault();
					var lstEmails = (manufEmails != null) ? manufEmails.Split(',').ToList() : new List<string>();
					SaveManufData(EmailDistributionType.ManufStatusEmail, manufCode, lstEmails);
				}
			}

			//Add Reason
			if (vm.Any(x => x.IsDirtyStatus || x.IsDirtyDaily))
			{
				var allChanged = vm.Where(x => x.IsDirtyStatus || x.IsDirtyDaily).ToList();
				foreach (var manuf in allChanged)
				{
					AddManufJurChangeHistory(manuf.EditReason, manuf.Code, "");
				}
			}
			return Content("OK");
		}

		private void SaveManufData(EmailDistributionType type, string code, List<string> emails)
		{
			var internalEmails = emails.Where(x => x.ToLower().Contains("@gaminglabs.com")).Select(x => x).ToList();
			var externalEmails = emails.Where(x => !x.ToLower().Contains("@gaminglabs.com")).Select(x => x).ToList();

			//get id
			var distId = _dbSubmission.EmailDistributions.Where(x => x.Code == type.ToString()).Select(x => x.Id).SingleOrDefault();

			//InternalUsers
			var listUsers = _dbSubmission.trLogins.Where(x => internalEmails.Contains(x.Email)).GroupBy(x => x.Email).Select(group => group.Min(x => x.Id)).ToList();

			var existingUsers = (from eu in _dbSubmission.EmailDistributionUsers
								 join e in _dbSubmission.EmailDistributions on eu.EmailDistributionId equals e.Id
								 where e.Code == type.ToString() && eu.ManufacturerCode == code && eu.UserId > 0
								 select eu.UserId).ToList();
			//items to Add
			var newUsers = listUsers.Where(x => !existingUsers.Contains(x)).Select(x => x).ToList();
			//items to Delete
			var deleteUsers = (listUsers.Count > 0) ? existingUsers.Where(x => !listUsers.Contains((int)x)).Select(x => x).ToList() : existingUsers;

			foreach (var user in newUsers)
			{
				var newUser = new EmailDistributionUser()
				{
					EmailDistributionId = distId,
					ManufacturerCode = code,
					UserId = user
				};
				_dbSubmission.EmailDistributionUsers.Add(newUser);
			}

			foreach (var user in deleteUsers)
			{
				_dbSubmission.RemoveRange(_dbSubmission.EmailDistributionUsers.Where(x => x.EmailDistributionId == distId && x.ManufacturerCode == code && x.UserId == user));
			}

			//Internal emails that r distribution 
			var gliUsers = _dbSubmission.trLogins.Select(x => x.Email).ToList();
			var internalNotUsers = internalEmails.Where(x => !gliUsers.Contains(x)).ToList();

			//Add as external
			externalEmails.AddRange(internalNotUsers);

			//End InternalUsers

			//ExternalEmail
			var existingExternal = (from eu in _dbSubmission.EmailDistributionUsers
									join e in _dbSubmission.EmailDistributions on eu.EmailDistributionId equals e.Id
									where e.Code == type.ToString() && eu.ManufacturerCode == code && eu.ExternalEmail != null
									select eu.ExternalEmail).ToList();

			//items to Add
			var newExternal = externalEmails.Where(x => !existingExternal.Contains(x)).Select(x => x).Distinct().ToList();
			//items to Delete
			var deleteExternal = (externalEmails.Count > 0) ? existingExternal.Where(x => !externalEmails.Contains(x)).Select(x => x).ToList() : existingExternal;

			foreach (var email in newExternal)
			{
				var newUser = new EmailDistributionUser()
				{
					EmailDistributionId = distId,
					ManufacturerCode = code,
					ExternalEmail = email.Trim()
				};
				_dbSubmission.EmailDistributionUsers.Add(newUser);
			}

			foreach (var email in deleteExternal)
			{
				_dbSubmission.RemoveRange(_dbSubmission.EmailDistributionUsers.Where(x => x.EmailDistributionId == distId && x.ManufacturerCode == code && x.ExternalEmail == email));
			}
			_dbSubmission.UserId = _userContext.User.Id;
			_dbSubmission.SaveChanges();
			//End ExternalEmail
		}
		#endregion

		#region ManufJur Change History
		public ContentResult GetManufJurChangeHistoryData(string jurId, string manufId)
		{
			var result = new List<ChangeHistory>();

			var query = _dbSubmission.EmailsSummaryChangeHistories.AsQueryable();
			if (!string.IsNullOrEmpty(manufId))
			{
				query = query.Where(x => x.ManufactureId == manufId).AsQueryable();
			}
			if (!string.IsNullOrEmpty(jurId))
			{
				query = query.Where(x => x.JurisdictionId == jurId).AsQueryable();
			}
			result = query.Select(x => new ChangeHistory()
			{
				ManufId = x.ManufactureId,
				JurId = x.JurisdictionId,
				ChangeDate = x.ChangeDate,
				EditedBy = x.ChangedBy.FName + " " + x.ChangedBy.LName,
				Reason = x.Reason
			}).OrderByDescending(x => x.ChangeDate).ToList();

			return Content(ComUtil.JsonEncodeCamelCase(result), "application/json");
		}

		private void AddManufJurChangeHistory(string editReason, string manufCode, string jurCode)
		{
			if (!string.IsNullOrEmpty(editReason))
			{
				var newChangeHistory = new EmailsSummaryChangeHistory()
				{
					ManufactureId = (!string.IsNullOrEmpty(manufCode)) ? manufCode : null,
					JurisdictionId = (!string.IsNullOrEmpty(jurCode)) ? jurCode : null,
					Reason = editReason,
					ChangeDate = DateTime.Now,
					ChangedById = _userContext.User.Id
				};
				_dbSubmission.EmailsSummaryChangeHistories.Add(newChangeHistory);
				_dbSubmission.UserId = _userContext.User.Id;
				_dbSubmission.SaveChanges();
			}
		}
		#endregion

		#region Jurisdictions
		private List<JurEmailData> GetJurisdictionsEmailInfo()
		{
			var jursEmails = (from m in _dbSubmission.tbl_lst_fileJuris
							  where m.Active == true
							  select (new JurEmailData()
							  {
								  Id = m.Id,
								  Name = m.Name,
								  isDirtyDaily = false,
								  isDirtyWeekly = false,
								  isDirtyStatus = false,
								  isVisible = true
							  })).OrderBy(m => m.Name).ToList();

			var jurDaily = (from m in jursEmails
							join eu in _dbSubmission.EmailDistributionUsers on m.Id equals eu.JurisdictionId
							join e in _dbSubmission.EmailDistributions on eu.EmailDistributionId equals e.Id
							where e.Code == EmailDistributionType.JurDailySummary.ToString()
							group eu by eu.JurisdictionId into g
							select new
							{
								Code = g.Key,
								Users = g.ToList()
							}).ToList();

			var jurStatus = (from m in jursEmails
							 join eu in _dbSubmission.EmailDistributionUsers on m.Id equals eu.JurisdictionId
							 join e in _dbSubmission.EmailDistributions on eu.EmailDistributionId equals e.Id
							 where e.Code == EmailDistributionType.JurStatusEmail.ToString()
							 group eu by eu.JurisdictionId into g
							 select new
							 {
								 Code = g.Key,
								 Users = g.ToList()
							 }).ToList();

			var jurWeekly = (from m in jursEmails
							 join eu in _dbSubmission.EmailDistributionUsers on m.Id equals eu.JurisdictionId
							 join e in _dbSubmission.EmailDistributions on eu.EmailDistributionId equals e.Id
							 where e.Code == EmailDistributionType.JurWeeklySummary.ToString()
							 group eu by eu.JurisdictionId into g
							 select new
							 {
								 Code = g.Key,
								 Users = g.ToList()
							 }).ToList();

			foreach (var j in jursEmails)
			{
				if (jurDaily.Find(x => x.Code == j.Id) != null)
				{
					var dailyEmailInternal = (jurDaily.Find(x => x.Code == j.Id).Users).Where(x => x.User != null).Select(x => x.User.Email).ToList();
					var dailyEmail = (jurDaily.Find(x => x.Code == j.Id).Users).Where(x => x.ExternalEmail != null).Select(x => x.ExternalEmail).ToList();
					j.DailySummary = string.Join(",", dailyEmail);
					if (dailyEmailInternal.Count() > 0)
					{
						j.DailySummary += (j.DailySummary != "") ? "," + string.Join(",", dailyEmailInternal) : string.Join(",", dailyEmailInternal);
					}
				}
				if (jurStatus.Find(x => x.Code == j.Id) != null)
				{
					var statusEmailInternal = (jurStatus.Find(x => x.Code == j.Id).Users).Where(x => x.User != null).Select(x => x.User.Email).ToList();
					var statusEmail = (jurStatus.Find(x => x.Code == j.Id).Users).Where(x => x.ExternalEmail != null).Select(x => x.ExternalEmail).ToList();
					j.StatusChange = string.Join(",", statusEmail);
					if (statusEmailInternal.Count() > 0)
					{
						j.StatusChange += (j.StatusChange != "") ? "," + string.Join(",", statusEmailInternal) : string.Join(",", statusEmailInternal);
					}
				}
				if (jurWeekly.Find(x => x.Code == j.Id) != null)
				{
					var weeklyEmailInternal = (jurWeekly.Find(x => x.Code == j.Id).Users).Where(x => x.User != null).Select(x => x.User.Email).ToList();
					var weeklyEmail = (jurWeekly.Find(x => x.Code == j.Id).Users).Where(x => x.ExternalEmail != null).Select(x => x.ExternalEmail).ToList();
					j.WeeklySummary = string.Join(",", weeklyEmail);
					if (weeklyEmailInternal.Count() > 0)
					{
						j.WeeklySummary += (j.WeeklySummary != "") ? "," + string.Join(",", weeklyEmailInternal) : string.Join(",", weeklyEmailInternal);
					}
				}
			}
			return jursEmails;
		}

		[HttpPost]
		public ContentResult SaveJurisdictionalData(List<JurEmailData> vm)
		{
			if (vm.Any(x => x.isDirtyDaily))
			{
				var dailyEmails = vm.Where(x => x.isDirtyDaily).Select(x => x.Id).ToList();
				foreach (var jurId in dailyEmails)
				{
					var jurEmails = vm.Where(x => x.Id == jurId).Select(x => x.DailySummary).SingleOrDefault();
					var lstEmails = (jurEmails != null) ? jurEmails.Split(',').ToList() : new List<string>();
					SaveJurData(EmailDistributionType.JurDailySummary, jurId, lstEmails);
				}
			}

			if (vm.Any(x => x.isDirtyWeekly))
			{
				var weeklyEmails = vm.Where(x => x.isDirtyWeekly).Select(x => x.Id).ToList();
				foreach (var jurId in weeklyEmails)
				{
					var jurEmails = vm.Where(x => x.Id == jurId).Select(x => x.WeeklySummary).SingleOrDefault();
					var lstEmails = (jurEmails != null) ? jurEmails.Split(',').ToList() : new List<string>();
					SaveJurData(EmailDistributionType.JurWeeklySummary, jurId, lstEmails);
				}
			}

			if (vm.Any(x => x.isDirtyStatus))
			{
				var statusEmails = vm.Where(x => x.isDirtyStatus).Select(x => x.Id).ToList();
				foreach (var jurId in statusEmails)
				{
					var jurEmails = vm.Where(x => x.Id == jurId).Select(x => x.StatusChange).SingleOrDefault();
					var lstEmails = (jurEmails != null) ? jurEmails.Split(',').ToList() : new List<string>();
					SaveJurData(EmailDistributionType.JurStatusEmail, jurId, lstEmails);
				}
			}

			//Add Reason
			if (vm.Any(x => x.isDirtyStatus || x.isDirtyDaily || x.isDirtyWeekly))
			{
				var allChanged = vm.Where(x => x.isDirtyStatus || x.isDirtyDaily || x.isDirtyWeekly).ToList();
				foreach (var jur in allChanged)
				{
					AddManufJurChangeHistory(jur.EditReason, "", jur.Id);
				}
			}
			return Content("OK");
		}

		private void SaveJurData(EmailDistributionType type, string jurId, List<string> Emails)
		{
			var internalEmails = Emails.Where(x => x.ToLower().Contains("@gaminglabs.com")).Select(x => x).ToList();
			var externalEmails = Emails.Where(x => !x.ToLower().Contains("@gaminglabs.com")).Select(x => x).ToList();

			//get id
			var distId = _dbSubmission.EmailDistributions.Where(x => x.Code == type.ToString()).Select(x => x.Id).SingleOrDefault();

			//InternalUsers
			var listUsers = _dbSubmission.trLogins.Where(x => internalEmails.Contains(x.Email)).GroupBy(x => x.Email).Select(group => group.Min(x => x.Id)).ToList();

			var existingUsers = (from eu in _dbSubmission.EmailDistributionUsers
								 join e in _dbSubmission.EmailDistributions on eu.EmailDistributionId equals e.Id
								 where e.Code == type.ToString() && eu.JurisdictionId == jurId && eu.UserId > 0
								 select eu.UserId).ToList();
			//items to Add
			var newUsers = listUsers.Where(x => !existingUsers.Contains(x)).Select(x => x).ToList();
			//items to Delete
			var deleteUsers = (listUsers.Count > 0) ? existingUsers.Where(x => !listUsers.Contains((int)x)).Select(x => x).ToList() : existingUsers;

			foreach (var user in newUsers)
			{
				var newUser = new EmailDistributionUser()
				{
					EmailDistributionId = distId,
					JurisdictionId = jurId,
					UserId = user
				};
				_dbSubmission.EmailDistributionUsers.Add(newUser);
			}

			foreach (var user in deleteUsers)
			{
				_dbSubmission.RemoveRange(_dbSubmission.EmailDistributionUsers.Where(x => x.EmailDistributionId == distId && x.JurisdictionId == jurId && x.UserId == user));
			}

			//Internal emails that r distribution 
			var gliUsers = _dbSubmission.trLogins.Select(x => x.Email).ToList();
			var internalNotUsers = internalEmails.Where(x => !gliUsers.Contains(x)).ToList();

			//Add as external
			externalEmails.AddRange(internalNotUsers);

			//End IntrenalUsers

			//ExternalEmail
			var existingExternal = (from eu in _dbSubmission.EmailDistributionUsers
									join e in _dbSubmission.EmailDistributions on eu.EmailDistributionId equals e.Id
									where e.Code == type.ToString() && eu.JurisdictionId == jurId && eu.ExternalEmail != null
									select eu.ExternalEmail).ToList();

			//items to Add
			var newExternal = externalEmails.Where(x => !existingExternal.Contains(x)).Select(x => x).Distinct().ToList();
			//items to Delete
			var deleteExternal = (existingExternal.Count > 0) ? existingExternal.Where(x => !externalEmails.Contains(x)).Select(x => x).ToList() : existingExternal;

			foreach (var email in newExternal)
			{
				var newUser = new EmailDistributionUser()
				{
					EmailDistributionId = distId,
					JurisdictionId = jurId,
					ExternalEmail = email.Trim()
				};
				_dbSubmission.EmailDistributionUsers.Add(newUser);
			}

			foreach (var email in deleteExternal)
			{
				_dbSubmission.RemoveRange(_dbSubmission.EmailDistributionUsers.Where(x => x.EmailDistributionId == distId && x.JurisdictionId == jurId && x.ExternalEmail == email));
			}
			_dbSubmission.UserId = _userContext.User.Id;
			_dbSubmission.SaveChanges();
			//End ExternalEmail
		}
		#endregion

		#region PDF Rules
		public ActionResult DeletePDFRule(int ruleId)
		{
			_dbSubmission.RuleParameters.Where(x => x.RuleId == ruleId).Delete();
			_dbSubmission.RuleAddresses.Where(x => x.RuleId == ruleId).Delete();
			_dbSubmission.RemoveRange(_dbSubmission.EmailDistributionUsers.Where(x => x.RuleId == ruleId));
			_dbSubmission.Rules.Where(x => x.Id == ruleId).Delete();
			return Content("OK");
		}

		public ActionResult EditPDFRule(int id, Mode mode = Mode.Edit)
		{
			var vm = new PDFEmailRule();
			vm.Mode = mode.ToString();
			vm.CanEditData = (Util.HasAccess(Permission.EmailsManagement, AccessRight.Edit)) ? true : false;
			vm.CanActivateRule = (Util.HasAccess(Permission.PDFRuleActivate, AccessRight.Edit)) ? true : false;

			if (mode == Mode.Edit)
			{
				var rule = _dbSubmission.Rules.Where(x => x.Id == id).Single();
				vm.Id = id;
				vm.Name = rule.Description;
				vm.IsScheduled = rule.Schedule;
				vm.PdfAttachment = rule.PDFAttachment;
				vm.LastEditDate = rule.EditTS;
				vm.ChangeHistories = rule.ChangeHistories.Select(
										x => new ChangeHistory
										{
											ChangeDate = x.ChangeDate,
											EditedBy = x.ChangedBy.FName + " " + x.ChangedBy.LName,
											Reason = x.Reason,
											RuleId = x.Id
										}
									).OrderByDescending(x => x.ChangeDate).ToList();

				var ruleParameters = rule.Parameters.ToList();

				if (ruleParameters.Count > 0)
				{
					vm.ManufIds = ruleParameters.Where(x => x.TableName.ToLower() == PDFRuleTableNames.tbl_lst_FileManu.ToString().ToLower() && x.ColumnName.ToLower() == PDFRuleColumnNames.Manufacturer_ShortID.ToString().ToLower()).Select(x => x.Value).ToList();
					vm.JurIds = ruleParameters.Where(x => x.TableName.ToLower() == PDFRuleTableNames.tbl_lst_FileJuris.ToString().ToLower() && x.ColumnName.ToLower() == PDFRuleColumnNames.Jurisdiction_ID.ToString().ToLower()).Select(x => x.Value).ToList();
					var selFilesTypes = ruleParameters.Where(x => x.TableName.ToLower() == PDFRuleTableNames.Submissions.ToString().ToLower() && x.ColumnName.ToLower() == PDFRuleColumnNames.Submission_Type.ToString().ToLower()).Select(x => x.Value).ToList();
					vm.FileTypes = _dbSubmission.tbl_lst_SubmissionType.Where(x => selFilesTypes.Contains(x.Code)).Select(x => x.Id.ToString()).ToList();
					vm.JurisdictionStatuses = ruleParameters.Where(x => x.TableName.ToLower() == PDFRuleTableNames.JurisdictionalData.ToString().ToLower() && x.ColumnName.ToLower() == PDFRuleColumnNames.JurisdictionType.ToString().ToLower()).Select(x => x.Value).ToList();

					//Selection
					vm.ManufNames = _dbSubmission.tbl_lst_fileManu.Where(x => vm.ManufIds.Contains(x.Code)).Select(x => new SelectListItem() { Text = x.Description, Value = x.Code, Selected = true }).ToList();
					vm.JurNames = _dbSubmission.tbl_lst_fileJuris.Where(x => vm.JurIds.Contains(x.Id)).Select(x => new SelectListItem() { Text = x.Name, Value = x.Id, Selected = true }).ToList();
					vm.FileTypesNames = _dbSubmission.tbl_lst_SubmissionType.Where(x => vm.FileTypes.Contains(x.Id.ToString())).Select(x => new SelectListItem() { Text = x.Code + " - " + x.Description, Value = x.Id.ToString(), Selected = true }).ToList();

					vm.JurisdictionStatusesNames = vm.JurisdictionStatuses.Select(x => new SelectListItem() { Text = x + " - " + x, Value = x, Selected = true }).ToList();

					//other selection
					vm.HasSpecialNote = ruleParameters.Where(x => x.ColumnName.ToLower() == PDFRuleColumnNames.SpecialNote.ToString().ToLower() && x.Value == "1").Any();
					vm.IncludesRVItems = ruleParameters.Where(x => x.ColumnName.ToLower() == PDFRuleColumnNames.IncludesRV.ToString().ToLower() && x.Value == "1").Any();
					vm.RushOnly = ruleParameters.Where(x => x.ColumnName.ToLower() == PDFRuleColumnNames.Rushes.ToString().ToLower() && x.Value == "1").Any();
					vm.IsCorrected = ruleParameters.Where(x => x.ColumnName.ToLower() == PDFRuleColumnNames.Corrected.ToString().ToLower() && x.Value == "1").Any();
				}
				var ruleAddresses = _dbSubmission.RuleAddresses.Where(x => x.RuleId == id).SingleOrDefault();
				if (ruleAddresses != null)
				{
					vm.EmailsTo = ruleAddresses.EmailAddress;
					vm.EmailsCC = ruleAddresses.CCEmailAddress;
					vm.AppendMsg = ruleAddresses.EmailAppendMsg;
					vm.CcENManager = ruleAddresses.CcENManager;
					vm.CcENProjectLead = ruleAddresses.CcENProjectLead;
					vm.CcCSR = ruleAddresses.CcCSR;
				}

			}

			//Status Options
			vm.StatusOptions.Add(new SelectListItem() { Text = JurisdictionStatus.AP.ToString(), Value = JurisdictionStatus.AP.ToString() });
			vm.StatusOptions.Add(new SelectListItem() { Text = JurisdictionStatus.RJ.ToString(), Value = JurisdictionStatus.RJ.ToString() });
			vm.StatusOptions.Add(new SelectListItem() { Text = JurisdictionStatus.TC.ToString(), Value = JurisdictionStatus.TC.ToString() });
			vm.StatusOptions.Add(new SelectListItem() { Text = JurisdictionStatus.DR.ToString(), Value = JurisdictionStatus.DR.ToString() });
			vm.StatusOptions.Add(new SelectListItem() { Text = JurisdictionStatus.RV.ToString(), Value = JurisdictionStatus.RV.ToString() });

			//Jur Options
			var jurData = _dbSubmission.tbl_lst_fileJuris
				.Where(x => x.Active)
				.OrderBy(x => x.Name)
				.Select(x => new SelectListItem { Value = x.Id.Trim(), Text = x.Name + " - " + x.Id })
				.ToList();
			vm.JurOptions = jurData;

			//Manuf Options
			//  var manufData = _dbSubmission.tbl_lst_fileManu
			//	.Where(x => x.Active)
			//	.OrderBy(x => x.Code)
			//	.Select(x => new SelectListItem() { Value = x.Code, Text = x.Description + " - " + x.Code })
			//	.ToList();
			//vm.ManufOptions = manufData;

			return View("PDFEmailRules/Edit", vm);
		}

		public ActionResult SavePDFRule(PDFEmailRule vm)
		{
			var ruleId = vm.Id;
			if (vm.Mode == "Add")
			{
				var newRule = new Rule()
				{
					Description = vm.Name,
					CreatedById = _userContext.User.Id,
					CreateTS = DateTime.Now,
					Schedule = false,
					PDFAttachment = vm.PdfAttachment,
				};
				_dbSubmission.Rules.Add(newRule);
				_dbSubmission.UserId = _userContext.User.Id;
				_dbSubmission.SaveChanges();
				ruleId = newRule.Id;

				//sends an email
				var mailMsg = new MailMessage();
				var emailToAddr = new List<string>();
				if (_userContext.Location == Laboratory.EU.ToString())
				{
					emailToAddr = (from x in _dbSubmission.EmailSubscriptions
								   where x.EmailType.Code == EmailType.NewPDFRuleEU.ToString()
								   select x.User.Email).ToList();
				}
				else
				{
					emailToAddr = (from x in _dbSubmission.EmailSubscriptions
								   where x.EmailType.Code == EmailType.NewPDFRule.ToString()
								   select x.User.Email).ToList();
				}

				foreach (var recipient in emailToAddr) { mailMsg.To.Add(new MailAddress(recipient)); }
				var FromId = (from x in _dbSubmission.trLogins
							  where x.Id == _userContext.User.Id
							  select x.Email).Single();
				mailMsg.To.Add(new MailAddress(FromId));

				var emailSubject = "New PDF Rule has been created - " + vm.Name;
				var emailBody = "Please review the new Rule - <b><A href='" + ConfigurationManager.AppSettings["submissions.baseurl"] + "/gliintranet/tools/emailsmanagement/editpdfrule/" + ruleId + "'>" + vm.Name + "</A></b> and activate to schedule.";
				mailMsg.Subject = emailSubject;
				mailMsg.Body = emailBody;
				_emailService.Send(mailMsg);
			}
			else
			{
				var rule = _dbSubmission.Rules.Where(x => x.Id == ruleId).Single();
				rule.Description = vm.Name;
				rule.EditTS = DateTime.Now;
				rule.Schedule = vm.IsScheduled;
				rule.PDFAttachment = vm.PdfAttachment;
				rule.LastEditedById = _userContext.User.Id;
			}
			_dbSubmission.UserId = _userContext.User.Id;
			_dbSubmission.SaveChanges();

			//Add remove parameters
			SaveRuleParameters(ruleId, vm);

			//email address
			SaveRuleAddresses(ruleId, vm);

			//Add Change history
			if (!string.IsNullOrEmpty(vm.EditReason))
			{
				var newChangeHistory = new RuleChangeHistory()
				{
					RuleId = ruleId,
					Reason = vm.EditReason,
					ChangeDate = DateTime.Now,
					ChangedById = _userContext.User.Id
				};
				_dbSubmission.RuleChangeHistories.Add(newChangeHistory);
				_dbSubmission.UserId = _userContext.User.Id;
				_dbSubmission.SaveChanges();
			}

			return Content("OK");
		}

		private void SaveRuleAddresses(int ruleId, PDFEmailRule vm)
		{
			var ruleAddress = _dbSubmission.RuleAddresses.Where(x => x.RuleId == ruleId).SingleOrDefault();
			if (ruleAddress == null)
			{
				var newAddress = new RuleAddress()
				{
					RuleId = ruleId,
					EmailAppendMsg = vm.AppendMsg,
					CreatedBy = _userContext.User.Id,
					CreateTS = DateTime.Now,
					CcENManager = vm.CcENManager,
					CcENProjectLead = vm.CcENProjectLead,
					CcCSR = vm.CcCSR
				};
				_dbSubmission.RuleAddresses.Add(newAddress);
			}
			else
			{
				ruleAddress.EmailAppendMsg = vm.AppendMsg;
				ruleAddress.EditTS = DateTime.Now;
				ruleAddress.CcENManager = vm.CcENManager;
				ruleAddress.CcENProjectLead = vm.CcENProjectLead;
				ruleAddress.CcCSR = vm.CcCSR;
			}
			_dbSubmission.UserId = _userContext.User.Id;
			_dbSubmission.SaveChanges();

			var Emails = (string.IsNullOrEmpty(vm.EmailsTo)) ? new List<string>() : vm.EmailsTo.Split(';').ToList();
			SaveRuleEmails(ruleId, Emails, AddressType.To, EmailDistributionType.PDFEmails);

			var EmailsCc = (string.IsNullOrEmpty(vm.EmailsCC)) ? new List<string>() : vm.EmailsCC.Split(';').ToList();
			SaveRuleEmails(ruleId, EmailsCc, AddressType.Cc, EmailDistributionType.PDFEmails);
		}

		private void SaveRuleEmails(int ruleId, List<string> emails, AddressType addressType, EmailDistributionType type)
		{
			var internalEmails = emails.Where(x => x.ToLower().Contains("@gaminglabs.com")).Select(x => x).ToList();
			var externalEmails = emails.Where(x => !x.ToLower().Contains("@gaminglabs.com")).Select(x => x).ToList();

			//get id
			var distId = _dbSubmission.EmailDistributions.Where(x => x.Code == type.ToString()).Select(x => x.Id).SingleOrDefault();

			//InternalUsers
			var listUsers = _dbSubmission.trLogins.Where(x => internalEmails.Contains(x.Email)).GroupBy(x => x.Email).Select(group => group.Min(x => x.Id)).ToList();

			var existingUsers = (from eu in _dbSubmission.EmailDistributionUsers
								 join e in _dbSubmission.EmailDistributions on eu.EmailDistributionId equals e.Id
								 where e.Code == type.ToString() && eu.RuleId == ruleId && eu.UserId > 0 && eu.AddressType == addressType.ToString()
								 select eu.UserId).ToList();
			//items to Add
			var newUsers = listUsers.Where(x => !existingUsers.Contains(x)).Select(x => x).ToList();
			//items to Delete
			var deleteUsers = (listUsers.Count > 0) ? existingUsers.Where(x => !listUsers.Contains((int)x)).Select(x => x).ToList() : existingUsers;

			foreach (var user in newUsers)
			{
				var newUser = new EmailDistributionUser()
				{
					EmailDistributionId = distId,
					RuleId = ruleId,
					UserId = user,
					AddressType = addressType.ToString()
				};
				_dbSubmission.EmailDistributionUsers.Add(newUser);
			}

			foreach (var user in deleteUsers)
			{
				_dbSubmission.RemoveRange(_dbSubmission.EmailDistributionUsers.Where(x => x.EmailDistributionId == distId && x.RuleId == ruleId && x.AddressType == addressType.ToString() && x.UserId == user));
			}

			//Internal emails that r distribution 
			var gliUsers = _dbSubmission.trLogins.Select(x => x.Email.ToLower()).ToList();
			var internalNotUsers = internalEmails.Where(x => !gliUsers.Contains(x.ToLower())).ToList();

			//Add as external
			externalEmails.AddRange(internalNotUsers);

			//End InternalUsers

			//ExternalEmail
			var existingExternal = (from eu in _dbSubmission.EmailDistributionUsers
									join e in _dbSubmission.EmailDistributions on eu.EmailDistributionId equals e.Id
									where e.Code == type.ToString() && eu.RuleId == ruleId && eu.AddressType == addressType.ToString() && eu.ExternalEmail != null
									select eu.ExternalEmail).ToList();

			//items to Add
			var newExternal = externalEmails.Where(x => !existingExternal.Contains(x)).Select(x => x).Distinct().ToList();
			//items to Delete
			var deleteExternal = (existingExternal.Count > 0) ? existingExternal.Where(x => !externalEmails.Contains(x)).Select(x => x).ToList() : existingExternal;

			foreach (var email in newExternal)
			{
				var newUser = new EmailDistributionUser()
				{
					EmailDistributionId = distId,
					RuleId = ruleId,
					ExternalEmail = email.Trim(),
					AddressType = addressType.ToString()
				};
				_dbSubmission.EmailDistributionUsers.Add(newUser);
			}

			foreach (var email in deleteExternal)
			{
				_dbSubmission.RemoveRange(_dbSubmission.EmailDistributionUsers.Where(x => x.EmailDistributionId == distId && x.RuleId == ruleId && x.AddressType == addressType.ToString() && x.ExternalEmail == email));
			}
			_dbSubmission.UserId = _userContext.User.Id;
			_dbSubmission.SaveChanges();
			//End ExternalEmail
		}

		private void AddRemoveRuleParameter(int ruleId, List<string> newItems, List<string> deleteItems, PDFRuleTableNames tableName, PDFRuleColumnNames columnName)
		{
			foreach (var item in newItems)
			{
				var newItem = new RuleParameter()
				{
					RuleId = ruleId,
					TableName = tableName.ToString(),
					ColumnName = columnName.ToString(),
					Value = item,
					CreateTS = DateTime.Now,
					CreatedBy = _userContext.User.Id
				};
				_dbSubmission.RuleParameters.Add(newItem);
			}

			foreach (var item in deleteItems)
			{
				_dbSubmission.RuleParameters.Where(x => x.RuleId == ruleId && x.TableName == tableName.ToString() && x.ColumnName == columnName.ToString() && x.Value == item).Delete();
			}
			_dbSubmission.UserId = _userContext.User.Id;
			_dbSubmission.SaveChanges();
		}
		private void SaveRuleParameters(int ruleId, PDFEmailRule vm)
		{
			var ruleParameters = _dbSubmission.RuleParameters.Where(x => x.RuleId == ruleId).ToList();

			//Manuf
			var existingManufs = ruleParameters.Where(x => x.TableName.ToLower() == PDFRuleTableNames.tbl_lst_FileManu.ToString().ToLower() && x.ColumnName.ToLower() == PDFRuleColumnNames.Manufacturer_ShortID.ToString().ToLower()).Select(x => x.Value).ToList();
			//items to Add
			var newManufs = vm.ManufIds.Where(x => !existingManufs.Contains(x)).Select(x => x).ToList();
			//items to Delete
			var deleteManufs = (vm.ManufIds.Count >= 0) ? existingManufs.Where(x => !vm.ManufIds.Contains(x)).Select(x => x).ToList() : vm.ManufIds;
			AddRemoveRuleParameter(ruleId, newManufs, deleteManufs, PDFRuleTableNames.tbl_lst_FileManu, PDFRuleColumnNames.Manufacturer_ShortID);

			//Jurs
			var existingJurs = ruleParameters.Where(x => x.TableName.ToLower() == PDFRuleTableNames.tbl_lst_FileJuris.ToString().ToLower() && x.ColumnName.ToLower() == PDFRuleColumnNames.Jurisdiction_ID.ToString().ToLower()).Select(x => x.Value).ToList();
			//items to Add
			var newJurs = vm.JurIds.Where(x => !existingJurs.Contains(x)).Select(x => x).ToList();
			//items to Delete
			var deleteJurs = (vm.JurIds.Count >= 0) ? existingJurs.Where(x => !vm.JurIds.Contains(x)).Select(x => x).ToList() : vm.JurIds;
			AddRemoveRuleParameter(ruleId, newJurs, deleteJurs, PDFRuleTableNames.tbl_lst_FileJuris, PDFRuleColumnNames.Jurisdiction_ID);

			//file types
			var existngFileTypes = ruleParameters.Where(x => x.TableName.ToLower() == PDFRuleTableNames.Submissions.ToString().ToLower() && x.ColumnName.ToLower() == PDFRuleColumnNames.Submission_Type.ToString().ToLower()).Select(x => x.Value).ToList();
			//items to Add
			var FileTypesList = _dbSubmission.tbl_lst_SubmissionType.Where(x => vm.FileTypes.Contains(x.Id.ToString())).Select(x => x.Code).ToList();
			var newFileTypes = FileTypesList.Where(x => !existngFileTypes.Contains(x)).Select(x => x).ToList();
			//items to Delete
			var deleteFileTypes = (FileTypesList.Count >= 0) ? existngFileTypes.Where(x => !FileTypesList.Contains(x)).Select(x => x).ToList() : FileTypesList;
			AddRemoveRuleParameter(ruleId, newFileTypes, deleteFileTypes, PDFRuleTableNames.Submissions, PDFRuleColumnNames.Submission_Type);

			//file Stauses
			var existingStatus = ruleParameters.Where(x => x.TableName.ToLower() == PDFRuleTableNames.JurisdictionalData.ToString().ToLower() && x.ColumnName.ToLower() == PDFRuleColumnNames.JurisdictionType.ToString().ToLower()).Select(x => x.Value).ToList();
			//items to Add
			var newFileStatus = vm.JurisdictionStatuses.Where(x => !existingStatus.Contains(x)).Select(x => x).ToList();
			//items to Delete
			var deleteFileStatus = (vm.JurisdictionStatuses.Count >= 0) ? existingStatus.Where(x => !vm.JurisdictionStatuses.Contains(x)).Select(x => x).ToList() : vm.JurisdictionStatuses;
			AddRemoveRuleParameter(ruleId, newFileStatus, deleteFileStatus, PDFRuleTableNames.JurisdictionalData, PDFRuleColumnNames.JurisdictionType);

			//Misc
			var isRushOnlyExists = _dbSubmission.RuleParameters.Where(x => x.RuleId == ruleId && x.ColumnName == PDFRuleColumnNames.Rushes.ToString() && x.Value == "1").Any();
			var newParm = new List<string>();
			var deleteParm = new List<string>();
			if (!vm.RushOnly && isRushOnlyExists)
			{
				deleteParm.Add("1");
			}
			else if (!isRushOnlyExists && vm.RushOnly)
			{
				newParm.Add("1");
			}
			AddRemoveRuleParameter(ruleId, newParm, deleteParm, PDFRuleTableNames.Misc, PDFRuleColumnNames.Rushes);

			var isCorrectionExists = _dbSubmission.RuleParameters.Where(x => x.RuleId == ruleId && x.ColumnName == PDFRuleColumnNames.Corrected.ToString() && x.Value == "1").Any();
			var newParm1 = new List<string>();
			var deleteParm1 = new List<string>();
			if (!vm.IsCorrected && isCorrectionExists)
			{
				deleteParm1.Add("1");
			}
			else if (!isCorrectionExists && vm.IsCorrected)
			{
				newParm1.Add("1");
			}
			AddRemoveRuleParameter(ruleId, newParm1, deleteParm1, PDFRuleTableNames.Misc, PDFRuleColumnNames.Corrected);

			var isSpecialNoteExists = _dbSubmission.RuleParameters.Where(x => x.RuleId == ruleId && x.ColumnName == PDFRuleColumnNames.SpecialNote.ToString() && x.Value == "1").Any();
			var newParm2 = new List<string>();
			var deleteParm2 = new List<string>();
			if (!vm.HasSpecialNote && isSpecialNoteExists)
			{
				deleteParm2.Add("1");
			}
			else if (!isSpecialNoteExists && vm.HasSpecialNote)
			{
				newParm2.Add("1");
			}
			AddRemoveRuleParameter(ruleId, newParm2, deleteParm2, PDFRuleTableNames.Misc, PDFRuleColumnNames.SpecialNote);

			var isIncludesRVExists = _dbSubmission.RuleParameters.Where(x => x.RuleId == ruleId && x.ColumnName == PDFRuleColumnNames.IncludesRV.ToString() && x.Value == "1").Any();
			var newParm3 = new List<string>();
			var deleteParm3 = new List<string>();
			if (!vm.IncludesRVItems && isIncludesRVExists)
			{
				deleteParm3.Add("1");
			}
			else if (!isIncludesRVExists && vm.IncludesRVItems)
			{
				newParm3.Add("1");
			}
			AddRemoveRuleParameter(ruleId, newParm3, deleteParm3, PDFRuleTableNames.Misc, PDFRuleColumnNames.IncludesRV);
		}
		#endregion

		#region Enum
		private enum PDFRuleColumnNames
		{
			Submission_Type,
			JurisdictionType,
			Manufacturer_ShortID,
			Jurisdiction_ID,
			IncludesRV,
			Corrected,
			SpecialNote,
			Rushes
		}
		private enum PDFRuleTableNames
		{
			tbl_lst_FileManu,
			tbl_lst_FileJuris,
			Submissions,
			JurisdictionalData,
			Misc
		}

		#endregion
	}
}