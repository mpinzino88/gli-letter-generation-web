﻿extern alias ZEFCore;
using GLI.EFCore.Submission;
using Microsoft.EntityFrameworkCore;
using Submissions.Web.Areas.Tools.Models.MasterBilling;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web.Mvc;
using ZEFCore::Z.EntityFramework.Plus;

namespace Submissions.Web.Areas.Tools.Controllers
{
	using Newtonsoft.Json;
	using Submissions.Common;
	using Submissions.Web.Areas.Tools.Models.MasterBilling.KO_Models;
	using System.Net;

	[AuthorizeUser(Permission = Permission.MasterBilling, HasAccessRight = AccessRight.Read, Roles = new Role[] { Role.ENS, Role.MG })]
	public class MasterBillingController : Controller
	{
		private readonly IUserContext _userContext;
		private readonly SubmissionContext _dbSubmission;

		public MasterBillingController(IUserContext userContext, SubmissionContext dbSubmission)
		{
			_userContext = userContext;
			_dbSubmission = dbSubmission;
		}

		#region Index
		public ActionResult Index(string term, int? jurisdictionId)
		{
			var vm = new IndexViewModel();
			if (!String.IsNullOrWhiteSpace(term))
			{
				var filters = new SearchModel { Term = term, JurisdictionId = jurisdictionId };
				vm = vmLoadViewModel(filters);

				vm.JurisdictionFilterId = jurisdictionId;
				if (jurisdictionId != null)
				{
					vm.JurisdictionFilterName = _dbSubmission.tbl_lst_fileJuris.SingleOrDefault(x => x.FixId == jurisdictionId).Name;
				}
			}

			return View(vm);
		}
		#endregion

		#region Edit
		[AuthorizeUser(Permission = Permission.MasterBilling, HasAccessRight = AccessRight.Edit)]
		public ActionResult Edit(int? id, string MBID)
		{
			if (id == null)
			{
				return RedirectToAction("Index");
			}

			var vm = new EditViewModel();

			vm.SubmissionRecord = _dbSubmission.Submissions.Where(x => x.Id == id)
				.Select(x => new Submission
				{
					Id = x.Id,
					FileNumber = x.FileNumber,
					IdNumber = x.IdNumber,
					GameName = x.GameName,
					Version = x.Version,
					DateCode = x.DateCode
				})
				.SingleOrDefault();

			vm.MasterBillingProject.AssociatedJurisdictions =
					_dbSubmission.JurisdictionalData
					.Where(
						x => x.SubmissionId == id &&
						x.MasterBillingProject == MBID &&
						x.MasterBillingProject != "" &&
						x.MasterBillingProject != null)
					.Select(x => new Jurisdiction
					{
						_destroy = false,
						Marked = false,
						Id = x.Id,
						JurisdictionId = x.JurisdictionId,
						Name = x.JurisdictionName,
						MasterBillingId = x.MasterBillingProject,
						SubmissionId = x.Submission.Id
					})
					.ToList();

			vm.MasterBillingProject.UnAssociatedJurisdictions =
					_dbSubmission.JurisdictionalData
					.Where(
						x => x.SubmissionId == id &&
						x.MasterBillingProject != MBID &&
						x.MasterBillingProject != ""
					)
					.Select(x => new Jurisdiction
					{
						_destroy = false,
						Marked = false,
						Id = x.Id,
						JurisdictionId = x.JurisdictionId,
						Name = x.JurisdictionName,
						MasterBillingId = x.MasterBillingProject,
						SubmissionId = x.Submission.Id
					})
					.ToList();

			vm.MBID = MBID;
			return View(vm);
		}

		[HttpPost]
		[AuthorizeUser(Permission = Permission.MasterBilling, HasAccessRight = AccessRight.Edit)]
		public ActionResult Edit(string EditViewModel)
		{
			var vm = JsonConvert.DeserializeObject<EditViewModel>(EditViewModel);

			ApplyToAll(vm);

			return RedirectToAction("Index", new { term = vm.MBID });
		}
		#endregion

		#region Add Association
		[AuthorizeUser(Permission = Permission.MasterBilling, HasAccessRight = AccessRight.Add)]
		public ActionResult AddAssociation(string id)
		{
			if (String.IsNullOrEmpty(id))
			{
				return RedirectToAction("Index");
			}
			Regex submissionFileNumberPattern = new Regex("([a-zA-Z][a-zA-Z])-([1-9]?[0-9][0-9])-([a-zA-Z][a-zA-Z][a-zA-Z])-([0-9][0-9])-([1-9]?[0-9][0-9])-?([0-9]?[0-9][0-9])?", RegexOptions.IgnoreCase);
			Regex dynamicsFileNumber = new Regex("^([a-zA-Z][a-zA-Z][0-9][0-9][0-9][a-zA-Z][a-zA-Z][a-zA-Z][0-9][0-9][0-9][0-9][0-9])$", RegexOptions.IgnoreCase);
			Regex dynamicsTransferFileNumber = new Regex("([a-zA-Z][a-zA-Z])([0-9][0-9][0-9])([a-zA-Z][a-zA-Z][a-zA-Z])([0-9][0-9])([0-9][0-9][0-9])([0-9][0-9][0-9])", RegexOptions.IgnoreCase);
			Match isSubFile = submissionFileNumberPattern.Match(id);
			Match isDynamicsFile = dynamicsFileNumber.Match(id);
			Match isDynamicsTransferFile = dynamicsTransferFileNumber.Match(id);
			if ((!(isSubFile.Success) && !(isDynamicsFile.Success) && !(isDynamicsTransferFile.Success)))
			{
				return RedirectToAction("Index");
			}

			var vm = new AddAssociationViewModel();
			vm.MBID = id;
			return View(vm);
		}

		[HttpPost]
		[AuthorizeUser(Permission = Permission.MasterBilling, HasAccessRight = AccessRight.Add)]
		public ActionResult AddAssociationSave(string AddAssociationViewModel, string MBID)
		{
			var vm = JsonConvert.DeserializeObject<AddAssociationViewModel>(AddAssociationViewModel);

			var jurisdictionsForAssoc = new List<string>();
			jurisdictionsForAssoc.AddRange(vm.MasterBillingAssociation.JurisdictionList.Where(x => x.Marked == true).Select(x => x.JurisdictionId));

			var jurisdictionPrimariesForUpdate = new List<int>();

			foreach (var record in vm.MasterBillingAssociation.SubmissionRecords)
			{
				jurisdictionPrimariesForUpdate.AddRange(record.Jurisdictions.Where(x => jurisdictionsForAssoc.Contains(x.JurisdictionId)).Select(x => x.Id).ToList());
			}

			_dbSubmission.UserId = _userContext.User.Id;
			_dbSubmission.JurisdictionalData
					.Where(x => jurisdictionPrimariesForUpdate.Contains(x.Id))
					.Update(x => new JurisdictionalData { MasterBillingProject = MBID });

			return RedirectToAction("Index", new { term = MBID });
		}
		#endregion

		#region Bulk Update
		public ActionResult BulkUpdate(string BulkUpdateModel)
		{
			var model = JsonConvert.DeserializeObject<BulkUpdateModel>(BulkUpdateModel);


			var jurisdictionalDataSyncSet = _dbSubmission.JurisdictionalData
				.Where(x => model.Primaries.Contains(x.Id))
				.Select(x => new
				{
					SubmissionHeader = x.Submission.SubmissionHeader,
					JurisdictionId = x.Jurisdiction.FixId
				}).ToList();

			var jurisdictions = jurisdictionalDataSyncSet.Select(x => x.JurisdictionId).ToList();
			var headers = jurisdictionalDataSyncSet.Select(x => x.SubmissionHeader.Id).ToList();

			var Primaries = _dbSubmission.JurisdictionalData
				.Where(x =>
					headers.Contains(x.Submission.SubmissionHeader.Id) &&
					jurisdictions.Contains(x.Jurisdiction.FixId)
				)
				.Select(x => x.Id)
				.ToList();

			if (model != null && !String.IsNullOrWhiteSpace(model.Destination) && model.Primaries.Count > 0)
			{
				_dbSubmission.UserId = _userContext.User.Id;
				_dbSubmission.JurisdictionalData
					.Where(x => Primaries.Contains(x.Id))
					.Update(x => new JurisdictionalData
					{
						MasterBillingProject = model.Destination
					});

			}
			return RedirectToAction("Index");
		}
		#endregion

		#region Model Loads
		[HttpPost]
		public JsonResult Search(SearchModel model)
		{
			var vm = new IndexViewModel();
			vm.MasterBillingProject.Submissions = GetMasterBillingProjects(model).ToList();
			vm.Results = vm.MasterBillingProject.Submissions.Count == 0 ? false : true;

			if (vm.Results)
			{
				return (Json(vm));
			}
			else
			{
				Response.StatusCode = (int)HttpStatusCode.BadRequest;
				return Json(Response.StatusCode);
			}
		}

		public IndexViewModel vmLoadViewModel(SearchModel filters)
		{
			var vm = new IndexViewModel();
			vm.MasterBillingProject.Submissions = GetMasterBillingProjects(filters).ToList();
			return (vm);
		}

		[HttpPost]
		public JsonResult loadSubmissionSearchResults(string term)
		{
			var vm = new AddAssociationViewModel();

			vm.MasterBillingAssociation.SubmissionRecords =
				_dbSubmission.Submissions
					.Where(
						x => x.FileNumber == term
					)
					.Select(x => new Submission
					{
						_destroy = false,
						DateCode = x.DateCode,
						FileNumber = x.FileNumber,
						GameName = x.GameName,
						Id = x.Id,
						Marked = false,
						IdNumber = x.IdNumber,
						MBID = null,
						Version = x.Version,
						Jurisdictions = x.JurisdictionalData
							.Select(y => new Jurisdiction
							{
								_destroy = false,
								Id = y.Id,
								JurisdictionId = y.JurisdictionId,
								Marked = false,
								MasterBillingId = y.MasterBillingProject,
								Name = y.JurisdictionName,
								SubmissionId = y.Submission.Id
							})
							.OrderBy(y => y.Name)
							.ToList()
					}).ToList();

			var allJurisdictions = new List<Jurisdiction>();
			foreach (var record in vm.MasterBillingAssociation.SubmissionRecords)
			{
				allJurisdictions.AddRange(record.Jurisdictions);
			}
			vm.MasterBillingAssociation.JurisdictionList = allJurisdictions
				.GroupBy(x => x.JurisdictionId)
				.Select(g => g.FirstOrDefault())
				.ToList();


			vm.MasterBillingAssociation.ContextIdNumber = "";

			vm.Results = vm.MasterBillingAssociation.SubmissionRecords.Count == 0 ? false : true;


			return (Json(vm));
		}
		#endregion

		#region Helper Methods
		private void UserContextSave()
		{
			_dbSubmission.UserId = _userContext.User.Id;
			_dbSubmission.SaveChanges();
		}

		public void ApplyToAll(EditViewModel vm)
		{
			var submissionRecords = _dbSubmission.Submissions.Where(x => x.FileNumber == vm.SubmissionRecord.FileNumber).ToList();
			var jursForAssoc = vm.MasterBillingProject.AssociatedJurisdictions.Select(x => x.JurisdictionId).ToList();
			var jursForDisAssoc = vm.MasterBillingProject.UnAssociatedJurisdictions.Select(x => x.JurisdictionId).ToList();

			foreach (var record in submissionRecords)
			{
				if (jursForAssoc.Count > 0)
				{
					var jurisdictionalDataIds = _dbSubmission.JurisdictionalData.Where(x => x.Submission.FileNumber == vm.SubmissionRecord.FileNumber && jursForAssoc.Contains(x.JurisdictionId)).Select(x => x.Id).ToList();

					if (jurisdictionalDataIds.Count > 0)
					{
						_dbSubmission.UserId = _userContext.User.Id;
						_dbSubmission.JurisdictionalData
							.Where(x => jurisdictionalDataIds.Contains(x.Id))
							.Update(x => new JurisdictionalData { MasterBillingProject = vm.MBID });

					}
				}

				if (jursForDisAssoc.Count > 0)
				{

					var jurisdictionalDataIds = _dbSubmission.JurisdictionalData.Where(x => x.Submission.FileNumber == vm.SubmissionRecord.FileNumber && jursForDisAssoc.Contains(x.JurisdictionId)).Select(x => x.Id).ToList();
					if (jurisdictionalDataIds.Count > 0)
					{
						_dbSubmission.UserId = _userContext.User.Id;
						_dbSubmission.JurisdictionalData
							.Where(x => jurisdictionalDataIds.Contains(x.Id))
							.Update(x => new JurisdictionalData { MasterBillingProject = null });

					}
				}
			}
		}

		private IEnumerable<Models.MasterBilling.KO_Models.Submission> GetMasterBillingProjects(SearchModel Filters)
		{
			#region Query
			var query = _dbSubmission.JurisdictionalData
					.Include(x => x.Submission)
					.Include(x => x.Jurisdiction)
					.AsNoTracking();

			if (!String.IsNullOrWhiteSpace(Filters.Term))
			{
				query = query.Where(x => x.MasterBillingProject == Filters.Term);
			}
			if (null != Filters.JurisdictionId)
			{
				query = query.Where(x => x.Jurisdiction.FixId == Filters.JurisdictionId);
			}
			#endregion

			#region Grouping
			var rawData = query.Select(x => new
			{
				DateCode = x.Submission.DateCode,
				FileNumber = x.Submission.FileNumber,
				GameName = x.Submission.GameName,
				Id = x.Submission.Id,
				IdNumber = x.Submission.IdNumber,
				Version = x.Submission.Version,

				jdId = x.Id,
				jdJurisdictionId = x.JurisdictionId,
				jdMasterBillingId = x.MasterBillingProject,
				jdName = x.JurisdictionName,
			})
			.ToList();

			var result = rawData.GroupBy(x => x.Id, (key, SubmissionGroup) => new Models.MasterBilling.KO_Models.Submission
			{
				_destroy = false,
				DateCode = SubmissionGroup.Max(s => s.DateCode),
				FileNumber = SubmissionGroup.Max(s => s.FileNumber),
				GameName = SubmissionGroup.Max(s => s.GameName),
				Id = SubmissionGroup.Max(s => s.Id),
				IdNumber = SubmissionGroup.Max(s => s.IdNumber),
				Version = SubmissionGroup.Max(s => s.Version),
				MBID = Filters.Term,
				Jurisdictions = SubmissionGroup.Select(j => new Models.MasterBilling.KO_Models.Jurisdiction
				{
					_destroy = false,
					Id = j.jdId,
					JurisdictionId = j.jdJurisdictionId,
					Marked = false,
					MasterBillingId = j.jdMasterBillingId,
					Name = j.jdName,
					SubmissionId = j.Id
				})
				.OrderBy(x => x.Name)
				.ToList()
			})
			.OrderBy(x => x.FileNumber)
			.ThenBy(x => x.IdNumber);
			#endregion

			return result;
		}

		#endregion
	}

	#region Helper Models
	public class SearchModel
	{
		public string Term { get; set; }
		public int? JurisdictionId { get; set; }

	}

	public class BulkUpdateModel
	{
		public string Destination { get; set; }
		public List<int> Primaries { get; set; }
	}
	#endregion
}