using AutoMapper;
using AutoMapper.QueryableExtensions;
using EF.GPS;
using EF.Submission;
using Submissions.Common;
using Submissions.Web.Areas.Tools.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Web.Mvc;

namespace Submissions.Web.Areas.Tools.Controllers
{
	public class RegulatorCertificationNumberController : Controller
	{
		private readonly ISubmissionContext _dbSubmission;
		private readonly IUserContext _userContext;
		private FUWUtil.FUWResult _uploadResult;
		private tbl_CertJobNumber _item;
		private RegCertDocs _item2;
		public RegulatorCertificationNumberController(ISubmissionContext dbSubmission, IUserContext userContext)
		{
			_dbSubmission = dbSubmission;
			_uploadResult = new FUWUtil.FUWResult();
			_userContext = userContext;
		}

		#region Index
		public ActionResult Index()
		{
			var results = new RegulatorCertificationNumberViewModel();
			results.DisplayCertificationIndexGrid = new CertificationIndexGridModel();
			results.DisplayCertificationIndexGridRW = new CertificationIndexGridModelRW();
			return View("Index", results);
		}
		#endregion

		#region Edit
		public ActionResult Edit(int? id, Mode? mode)
		{
			if (mode == null)
			{
				mode = Mode.Edit;
			}

			if (mode == Mode.Add)
			{
				_item = new tbl_CertJobNumber();

			}
			else
			{

				_item = _dbSubmission.tbl_CertJobNumber.Find(id);


			}

			var result = Mapper.Map<RegulatorCertificationNumberViewModel>(_item);

			result.Mode = (Mode)mode;

			if (_item != null)
			{
				var document = _dbSubmission.RegCertDocs.Find(_item.DocumentId);
				if (document != null)
				{
					result.PreCertPath = document.Path;
					result.Jurisdiction = document.JurisdictionId;
					result.CertDate = document.Date;
				}

			}

			Session["Id"] = id;
			Session["SkipGridQuery"] = true;

			result.DisplaySelectedGridModel = new JurisdictionIndexGridModel(true);

			result.JurisdictionDataList = new List<JurisdictionalData>().ToList();

			var NewSearch = new JurisdictionIndexGridRecord();
			NewSearch.SubmissionFileNumber = _item.GLIFileNumber;
			NewSearch.SubmissionGameName = _item.GameName;
			NewSearch.Jurisdiction = "";
			Session["FilterVM"] = NewSearch;

			result.JurisdictionIndexGrid = new JurisdictionIndexGridModel(false);

			TempData["RegCertNum Id"] = id;

			result.GameType = result.GameType == null ? null : result.GameType.Trim();
			result.Jurisdiction = result.Jurisdiction == null ? null : result.Jurisdiction.Trim();

			return View(result);
		}
		#endregion

		#region Edit2
		[HttpPost]
		public ActionResult Edit(int? id, string sMode, RegulatorCertificationNumberViewModel selObj)
		{
			switch (selObj.Mode)
			{
				case Mode.Add:
					_item = new tbl_CertJobNumber();
					BuildRegCertNumber(selObj);

					_dbSubmission.tbl_CertJobNumber.Add(_item);

					break;
				case Mode.Edit:
					_item = _dbSubmission.tbl_CertJobNumber.Find(id);

					if (_item.Id != null)
					{
						if (_item.DocumentId != null)
						{
							_item2 = _dbSubmission.RegCertDocs.Find(_item.DocumentId);
							_item2.JurisdictionId = selObj.Jurisdiction;
							_item2.ManufacturerCode = selObj.ManufacturerName;
							_item2.Path = selObj.PreCertPath;
							_item2.Date = selObj.CertDate;
						}
						_item.Jurisdiction = selObj.Jurisdiction;
						_item.ManufacturerName = selObj.ManufacturerName;
						_item.GameName = selObj.GameName;
						_item.GameId = selObj.GameId;
						_item.GameType = selObj.GameType;
						_item.CertNumber = selObj.CertNumber;
						_item.CertDate = selObj.CertDate;
						_item.GLIEntryDate = selObj.GLIEntryDate;
						_item.Comments = selObj.Comments;
						_item.PreCertPath = selObj.PreCertPath;

					}

					break;

				default:
					throw new Exception("Invalid Edit Mode");
			}
			_dbSubmission.UserId = _userContext.User.Id;
			_dbSubmission.SaveChanges();
			_dbSubmission.UserId = null;

			if (selObj.CheckJurisdictionIds != null) //unlink button clicked
			{
				var jurIdList = selObj.CheckJurisdictionIds.Split(',').Select(Int32.Parse).ToList();
				UpdateJurisdictionalDataUnCheck(_item.Id, jurIdList);
			}
			else if (selObj.JurisdictionIdsPost != null) // Link button clicked
			{
				var jurIdList = selObj.JurisdictionIdsPost.Split(',').Select(Int32.Parse).ToList();
				UpdateJurisdictionalData(_item.Id, jurIdList);
			}

			if (_item.Id != null)
			{
				var jurisdictionDatas = _dbSubmission.RegCertId_JurisdictionalData.Where(x => x.CertificationNumberId == _item.Id).ToList();
				foreach (var jurisdictionData in jurisdictionDatas)
				{
					var jurData = _dbSubmission.JurisdictionalData.Where(x => x.Id == jurisdictionData.JurisdictionalDataId).First();
					if (jurData.JurisdictionId.Equals("14"))
					{
						if (jurData.Submission.JurisdictionId.Equals("14"))
						{
							_dbSubmission.Database.ExecuteSqlCommand("exec dbo.Dynamics_UpdateProjectDesc_ByFilenumber '" + jurData.Submission.FileNumber + "',null,'" + _item.CertNumber + "'");
						}
						else
						{
							_dbSubmission.Database.ExecuteSqlCommand("exec dbo.Dynamics_UpdateProjectDesc_ByFilenumber '" + jurData.Submission.FileNumber + "','" + jurData.JurisdictionId + "','" + _item.CertNumber + "'");
						}
					}

					if (_item.PreCertPath != null)
					{
						UploadToGPS(_item.PreCertPath, jurData.Submission.FileNumber, jurData.JurisdictionId);
					}

				}

			}
			return RedirectToAction("Edit", new { id = _item.Id });

		}

		private void UpdateJurisdictionalDataUnCheck(int? id, List<int> jurisdictionIds)
		{
			if (jurisdictionIds != null)
			{
				foreach (var jurisdictionId in jurisdictionIds)
				{
					var certNumbers = _dbSubmission.RegCertId_JurisdictionalData.Where(x => (x.CertificationNumberId == (int)id
						&& x.JurisdictionalDataId == jurisdictionId));

					foreach (var certNumber in certNumbers)
					{
						_dbSubmission.Entry(certNumber).State = EntityState.Deleted;
					}
				}
				_dbSubmission.SaveChanges();
			}
		}

		private void UpdateJurisdictionalData(int? id, List<int> jurisdictionIds)
		{
			if (jurisdictionIds != null)
			{
				foreach (var jurisdictionId in jurisdictionIds)
				{
					var certInfos = _dbSubmission.RegCertId_JurisdictionalData.Where(x => (x.CertificationNumberId == (int)id
					&& x.JurisdictionalDataId == jurisdictionId));

					if (certInfos != null && certInfos.Any())
					{
						continue; //already exists -- skip
					}

					_dbSubmission.RegCertId_JurisdictionalData.Add(new RegCertId_JurisdictionalData
					{
						CertificationNumberId = (int)id,
						JurisdictionalDataId = jurisdictionId
					});
					_dbSubmission.SaveChanges();
				}
			}
		}
		#endregion

		#region Search/Filter
		[HttpPost]
		public ActionResult Search(RegulatorCertificationNumberViewModel editView)
		{

			_item = _dbSubmission.tbl_CertJobNumber.Find(editView.Id);
			editView.JurisdictionGridData.Jurisdiction = editView.Jurisdiction;
			Session["FilterVM"] = editView.JurisdictionGridData;

			var vm = new RegulatorCertificationNumberViewModel
			{
				JurisdictionIndexGrid = new JurisdictionIndexGridModel(),
				JurisdictionGridData = editView.JurisdictionGridData,
				DisplaySelectedGridModel = new JurisdictionIndexGridModel(true),
				GLIFileNumber = _item.GLIFileNumber,
				ManufacturerName = _item.ManufacturerName,
				GameName = _item.GameName,
				GameType = _item.GameType,
				GameId = _item.GameId,
				DocumentId = _item.DocumentId,
				Id = _item.Id,
				Jurisdiction = _item.Jurisdiction,
				CertNumber = _item.CertNumber,
				CertDate = _item.CertDate,
				GLIEntryDate = _item.GLIEntryDate,
				Comments = _item.Comments,
				Comments2 = _item.Comments2,
				PreCertPath = _item.PreCertPath

			};

			return View("Edit", vm);
		}
		#endregion

		#region Load Certification Number Index Page Grid Data

		[OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]

		public JsonResult LoadCertificationIndexGrid()
		{
			var query = _dbSubmission.tbl_CertJobNumber.Where(x => x.DataType == "CertNum").AsNoTracking().AsQueryable(); // need to update code
			query = query.OrderByDescending(x => x.ManufacturerName);
			var gridData = query.ProjectTo<CertificationIndexGridRecord>();
			var gridModel = new CertificationIndexGridModel();
			var dataGrid = gridModel.CertGrid.DataBind(gridData);

			return dataGrid;

		}
		#endregion

		public JsonResult LoadJurisdictionIndexGrid()
		{
			var searchFilters = (JurisdictionIndexGridRecord)Session["FilterVM"];

			string fileNumber;
			string gameName;
			string jurisdiction;

			fileNumber = "";
			gameName = "";
			jurisdiction = "";

			if (searchFilters != null)
			{
				fileNumber = searchFilters.SubmissionFileNumber;
				gameName = searchFilters.SubmissionGameName;
				jurisdiction = searchFilters.Jurisdiction;
			}

			var query = _dbSubmission.JurisdictionalData
			.Include(x => x.Submission)
			.AsNoTracking()
			.AsQueryable();

			if ((bool)Session["SkipGridQuery"])
			{
				query = query.Where(x => x.Id == -1);
			}

			else
			{
				if (!String.IsNullOrEmpty(fileNumber))
				{
					query = query.Where(x => x.Submission.FileNumber == fileNumber);
				}

				if (!String.IsNullOrEmpty(gameName))
				{
					query = query.Where(x => x.Submission.GameName == gameName);
				}

				if (!String.IsNullOrEmpty(jurisdiction))
				{
					query = query.Where(x => x.JurisdictionId == jurisdiction);
				}

			}

			var gridModel = new JurisdictionIndexGridModel();
			query = query.OrderByDescending(x => x.Submission.FileNumber).AsQueryable();
			var results = query.ProjectTo<JurisdictionIndexGridRecord>();
			var dataGrid = gridModel.Grid.DataBind(results);

			Session["SkipGridQuery"] = false;

			return dataGrid;
		}

		[OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
		public JsonResult LoadDisplaySelectedGrid()
		{
			int? id = (int?)Session["Id"];
			var regCertId = id != null ? id : -1;
			var primaryIds = _dbSubmission.RegCertId_JurisdictionalData.Where(x => (x.CertificationNumberId == regCertId)).Select(y => y.JurisdictionalDataId).ToList();
			var jurisdictionData = _dbSubmission.JurisdictionalData.Where(x => primaryIds.Contains(x.Id));
			if (jurisdictionData != null)
			{
				var juristictionResults = jurisdictionData.ProjectTo<JurisdictionIndexGridRecord>();
				var grid = new JurisdictionIndexGridModel().Grid;
				var dataGrid = grid.DataBind(juristictionResults);

				return dataGrid;

			}

			return null;
		}

		#region Delete
		public ActionResult Delete(int id)
		{
			_item = new tbl_CertJobNumber { Id = id };
			_dbSubmission.Entry(_item).State = EntityState.Deleted;

			var certNumbers = _dbSubmission.RegCertId_JurisdictionalData.Where(x => (x.CertificationNumberId == id));

			foreach (var certNumber in certNumbers)
			{
				_dbSubmission.Entry(certNumber).State = EntityState.Deleted;

			}
			_dbSubmission.SaveChanges();

			return RedirectToAction("Index");
		}
		#endregion

		private void BuildRegCertNumber(RegulatorCertificationNumberViewModel objCertNumber)
		{
			_item.Jurisdiction = objCertNumber.Jurisdiction;
			_item.GLIFileNumber = objCertNumber.GLIFileNumber;
			_item.ManufacturerName = objCertNumber.ManufacturerName;
			_item.GameName = objCertNumber.GameName;
			_item.GameId = objCertNumber.GameId;
			_item.GameType = objCertNumber.GameType;
			_item.CertNumber = objCertNumber.CertNumber;
			_item.CertDate = (DateTime?)objCertNumber.CertDate;
			_item.GLIEntryDate = objCertNumber.GLIEntryDate;
			_item.Comments = objCertNumber.Comments;
			_item.Comments2 = objCertNumber.Comments2;
			_item.DataType = "CertNum";
			_item.LastModifiedLoginId = _userContext.User.Id;


			if (objCertNumber.PreCertFile == null)
			{
				_item.PreCertPath = objCertNumber.PreCertPath;
			}
			else
			{
				var locationOnServer = System.Configuration.ConfigurationManager.AppSettings["PreCertUploadPath"];
				_item.PreCertPath = Path.Combine(locationOnServer, Path.GetFileName(objCertNumber.PreCertFile.FileName));
			}
		}

		private void UploadToGPS(string FilePath, string FileNumber, string jurisdiction)//Pre-Cert Upload to GPS
		{
			var archiveVersion = FUWUtil.FUWUtil.GetArchiveDirTreeversionId(FileNumber);
			var archiveLocation = "";
			var fileName = Path.GetFileName(FilePath);
			var FUWFileNumber = new FUWUtil.FileNumber(FileNumber);
			var Root = FUWUtil.FUWDirTree.GenerateFUWTree(FUWFileNumber, true, ref archiveLocation, false, false, true, true, true, false);

			//If this is the old version, this old method will still work.
			if (archiveVersion == 0)
			{
				var jurisdictionName = "";
				if (jurisdiction.Equals("42"))
				{
					jurisdictionName = "NC";
				}
				else if (jurisdiction.Equals("214"))
				{
					jurisdictionName = "AK";
				}
				else if (jurisdiction.Equals("14"))
				{
					jurisdictionName = "Oregon";
				}
				else if (jurisdiction.Equals("145"))
				{
					jurisdictionName = "NC-Raffle";
				}
				else if (jurisdiction.Equals("414"))
				{
					jurisdictionName = "NC-Gaming";
				}
				else if (jurisdiction.Equals("100"))
				{
					jurisdictionName = "Gauteng-100";
				}
				else if (jurisdiction.Equals("101"))
				{
					jurisdictionName = "Mpumalanga-101";
				}
				else if (jurisdiction.Equals("102"))
				{
					jurisdictionName = "Western Cape-102";
				}
				else if (jurisdiction.Equals("103"))
				{
					jurisdictionName = "Eastern Cape-103";
				}
				else if (jurisdiction.Equals("104"))
				{
					jurisdictionName = "Northern Cape-104";
				}
				else if (jurisdiction.Equals("105"))
				{
					jurisdictionName = "Free State-105";
				}
				else if (jurisdiction.Equals("106"))
				{
					jurisdictionName = "North West-106";
				}
				else if (jurisdiction.Equals("108"))
				{
					jurisdictionName = "KwaZulu-Natal-108";
				}
				else if (jurisdiction.Equals("315"))
				{
					jurisdictionName = "Limpopo-315";
				}
				else if (jurisdiction.Equals("49"))
				{
					jurisdictionName = "South Africa-49";
				}
				else
					return;

				var Dir = Root.DescendantOrSelf(x => x.Name == jurisdictionName).Children.Where(x => x.FilePathPart == "PreCerts").First();
				if (!Dir.Items.Exists(item => item.Name == fileName) && !FUWUtil.GPSFileInfo.GPSFileExists(Dir.FullName + "\\" + fileName))
				{
					var gpsFileInfo = new FUWUtil.GPSFileInfo(FilePath)
					{
						Action = FUWUtil.ItemAction.UPLOADNEW
					};
					Dir.EntriesToProcess.Add(gpsFileInfo);
					var gpsLabId = FUWUtil.FUWUtil.Laboratories.FirstOrDefault(lab => lab.Location == archiveLocation || lab.ArchiveLocation == archiveLocation).ID;
					_uploadResult = Dir.ProcessAll(gpsLabId);
				}
			}
			else
			{
				var Dir = Root.DescendantOrSelf(x => x.Name == "PreCerts");

				if (!Dir.Items.Exists(item => item.Name == fileName) && !FUWUtil.GPSFileInfo.GPSFileExists(Dir.FullName + "\\" + fileName))
				{
					var gpsFileInfo = new FUWUtil.GPSFileInfo(FilePath)
					{
						Action = FUWUtil.ItemAction.UPLOADNEW
					};

					var jurId = Convert.ToInt32(jurisdiction);
					var jurName = _dbSubmission.tbl_lst_fileJuris.Where(x => x.Id == jurisdiction).Select(y => y.Name).FirstOrDefault();

					FUWUtil.UploadableExtensions.AddJurisdictionalData(gpsFileInfo, jurId, jurName);

					Dir.EntriesToProcess.Add(gpsFileInfo);
					var gpsLabId = FUWUtil.FUWUtil.Laboratories.FirstOrDefault(lab => lab.Location == archiveLocation || lab.ArchiveLocation == archiveLocation).ID;
					_uploadResult = Dir.ProcessAll(gpsLabId);
				}

			}
		}
	}

	#region PreCertDocument Object
	public class PreCertDocument
	{

		public PreCertDocument(string FileNumber, string OriginFilePath, string DestinationPath, string ArchiveLocation = "NJ")
		{
			this.FileNumber = FileNumber;
			this.OriginFilePath = OriginFilePath;
			this.ArchiveLocation = ArchiveLocation;

			this.FUWFileNumber = new FUWUtil.FileNumber(FileNumber);
		}

		public string FileNumber { get; set; }
		public string OriginFilePath { get; set; }
		public string ArchiveLocation { get; set; }
		public FUWUtil.FileNumber FUWFileNumber { get; set; }
	}
	#endregion

	public class ResultObject
	{
		public ResultObject(string FileNumber, string PreCert)
		{
			this.FileNumber = FileNumber;
			this.PreCert = PreCert;
		}

		public string FileNumber { get; set; }
		public string PreCert { get; set; }
	}
}