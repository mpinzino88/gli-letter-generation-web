﻿using Submissions.Common;
using Submissions.Common.Kendo;
using Submissions.Web.Models.Grids.Tools;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web;

namespace Submissions.Web.Areas.Tools.Models
{
	public class ExternalDocumentsIndexViewModel
	{
		public ExternalDocumentsIndexViewModel()
		{
			this.ExternalDocumentsGrid = new ExternalDocumentsGrid();
		}

		public string RootPathUrl { get; set; }
		public ExternalDocumentsGrid ExternalDocumentsGrid;
	}

	public class ExternalDocumentsEditViewModel
	{
		public ExternalDocumentsEditViewModel()
		{
			this.ExternalDocuments = new List<KendoFile>();
			this.ExternalDocumentsUpload = new List<HttpPostedFileBase>();
		}

		public int? Id { get; set; }
		[Display(Name = "File Name"), Required]
		public string FileName { get; set; }
		[Required]
		public string Description { get; set; }
		[Display(Name = "Manufacturer"), Required]
		public string ManufacturerCode { get; set; }
		public string Manufacturer { get; set; }
		[Display(Name = "Date")]
		public DateTime DocumentDate { get; set; }
		public string Type { get; set; }
		public Mode Mode { get; set; }
		public string RootPathUrl { get; set; }
		public IList<KendoFile> ExternalDocuments { get; set; }
		public IList<HttpPostedFileBase> ExternalDocumentsUpload { get; set; }
	}
}