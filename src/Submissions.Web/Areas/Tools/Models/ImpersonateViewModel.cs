﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Security.Principal;

namespace Submissions.Web.Areas.Tools.Models
{
	public class ImpersonateIndexViewModel
	{
		[Display(Name = "User")]
		public int? UserId { get; set; }
		public string User { get; set; }
	}

	public class ImpersonatePrincipal : IPrincipal
	{
		private readonly string _name;

		public ImpersonatePrincipal(string name)
		{
			_name = name;
		}

		public IIdentity Identity
		{
			get
			{
				return new GenericIdentity(_name);
			}
		}

		public bool IsInRole(string role)
		{
			throw new NotImplementedException();
		}
	}
}