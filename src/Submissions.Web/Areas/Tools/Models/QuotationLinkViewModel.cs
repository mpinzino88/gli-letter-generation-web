﻿namespace Submissions.Web.Areas.Tools.Models
{
	public class QuotationLinkViewModel
	{
		public QuotationLinkViewModel()
		{

		}
		public QuotationLinkViewModel(string QuoteNumber)
		{
			this.QuoteNumber = QuoteNumber;
		}
		public string QuoteNumber { get; set; }
	}
}