﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace Submissions.Web.Areas.Tools.Models
{
    public class EmailsManagementViewModel
    {
        public EmailsManagementViewModel()
        {
            ManufactureData = new List<ManufEmailData>();
            JurisdictionData = new List<JurEmailData>();
            //SearchLinks = new List<string>();
        }
        public bool ViewManufactureData { get; set; }
        public bool ViewJurisdictionData { get; set; }
        public bool ViewPDFRules { get; set; }
        public List<ManufEmailData> ManufactureData { get; set; }
        public List<JurEmailData> JurisdictionData { get; set; }
        //public List<string> SearchLinks { get; set; }
        public bool HideBlankData { get; set; }
        public bool CanEditData { get; set; }
    }

    public class ManufEmailData {
        public ManufEmailData()
        {
            ChangeHistories = new List<ChangeHistory>();
        }
        public string Code { get; set; }
        public string Description { get; set; }
        public string DailySummary { get; set; }
        public string StatusChange { get; set; }
        public bool IsDirtyDaily { get; set; }
        public bool IsDirtyStatus { get; set; }
        public List<ChangeHistory> ChangeHistories { get; set; }
        public bool IsVisible { get; set; }
        public string EditReason { get; set; }

    }
    public class JurEmailData
    {
        public JurEmailData()
        {
            ChangeHistories = new List<ChangeHistory>();
        }
        public string Id { get; set; }
        public string Name { get; set; }
        public string DailySummary { get; set; }
        public string WeeklySummary { get; set; }
        public string StatusChange { get; set; }
        public bool isDirtyDaily { get; set; }
        public bool isDirtyWeekly { get; set; }
        public bool isDirtyStatus { get; set; }
        public List<ChangeHistory> ChangeHistories { get; set; }
        public bool isVisible { get; set; }
        public string EditReason { get; set; }
    }

    public class PDFEmailRule
    {
        public PDFEmailRule()
        {
            JurIds = new List<string>();
            JurNames = new List<SelectListItem>();
            FileTypes = new List<string>();
            FileTypesNames = new List<SelectListItem>();
            JurisdictionStatuses = new List<string>();
            JurisdictionStatusesNames = new List<SelectListItem>();
            ManufIds = new List<string>();
            ManufNames = new List<SelectListItem>();
            StatusOptions = new List<SelectListItem>();
            ChangeHistories = new List<ChangeHistory>();
            JurOptions = new List<SelectListItem>();
            ManufOptions = new List<SelectListItem>();
        }
        public int Id { get; set; }
        public string Name { get; set; }
        public bool IsScheduled { get; set; } 
        public bool PdfAttachment { get; set; }
        public bool CanEditData { get; set; }
        public DateTime? LastEditDate { get; set; }
        public List<string> JurIds { get; set; }
        public List<SelectListItem> JurNames { get; set; }
        public List<string> ManufIds { get; set; }
        public List<SelectListItem> ManufNames { get; set; }
        public List<string> FileTypes { get; set; }
        public List<SelectListItem> FileTypesNames { get; set; }
        public List<string> JurisdictionStatuses { get; set; }
        public List<SelectListItem> JurisdictionStatusesNames { get; set; }
        public bool IsCorrected { get; set; }
        public bool HasSpecialNote { get; set; }
        public bool RushOnly { get; set; }
        public bool IncludesRVItems { get; set; }
        public string EmailsTo { get; set; }
        public string EmailsCC { get; set; }
        public bool CcENManager { get; set; }
        public bool CcENProjectLead { get; set; }
        public bool CcCSR { get; set; }
        public string AppendMsg { get; set; }
        public bool IsDirty { get; set; }
        public string EditReason { get; set; }
        public List<ChangeHistory> ChangeHistories { get; set; }
        public string Mode { get; set; }
        public bool CanActivateRule { get; set; }

        public List<SelectListItem> StatusOptions { get; set; }
        public List<SelectListItem> JurOptions { get; set; }
        public List<SelectListItem> ManufOptions { get; set; }
    }

    public class ChangeHistory
    {
        public string JurId { get; set; }
        public string ManufId { get; set; }
        public int RuleId { get; set; }
        public DateTime ChangeDate { get; set; }
        public string EditedBy { get; set; }
        public string Reason { get; set; }
        public string OldVal { get; set; }
        public string NewVal { get; set; }
    }
}