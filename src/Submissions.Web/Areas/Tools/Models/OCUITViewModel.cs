﻿using Submissions.Core.Models.DTO;
using Submissions.Web.Areas.Documents.Models.Generate;
using Submissions.Web.Areas.Lookups.Models;
using System;
using System.Collections.Generic;

namespace Submissions.Web.Areas.Tools.Models
{
	public class OCUITIndexView : Lookup
	{
		public OCUITIndexView()
		{
			AssociatedSoftwaresTemplate = "";
			JurisdictionList = new List<JurisdictionInfo>();
			PlatformPiecesTemplates = new List<PlatformPiecesTemplateDTO>();
			//Components only used for this bundle
			JChipOCUIT = new OCUIT("jurisdiction chip");
			CompatibilityOCUIT = new OCUIT("compatibility");
			ContinuityOCUIT = new OCUIT("continuity");
		}

		public string AssociatedSoftwaresTemplate { get; set; }
		public int AssociatedSoftwaresTemplateId { get; set; }
		public OCUIT JChipOCUIT { get; set; }
		public OCUIT CompatibilityOCUIT { get; set; }
		public OCUIT ContinuityOCUIT { get; set; }


		public GenerateLetterProjectInfo ProjectInfo { get; set; }
		public IList<JurisdictionInfo> JurisdictionList { get; set; }
		public IList<PlatformPiecesTemplateDTO> PlatformPiecesTemplates { get; set; }


	}
	public class JurisdictionInfo
	{
		public int JurisdictionId { get; set; }
		public string Jurisdiction { get; set; }
	}
	public class AssociatedSoftwareTemplatesInfo
	{
		public int AssociatedSoftwareTemplateId { get; set; }
		public string AssociatedSoftwareTemplate { get; set; }
		public int Id { get; set; }
		public string Name { get; set; }
		public string Description { get; set; }
		public IList<JurisdictionInfo> Jurisdictions { get; set; }

	}

	public class OCUIT
	{
		public OCUIT(string type)
		{
			OCUITComponents = new List<OCUITComponent>();
			Type = type;
			Platforms = new List<ManufacturerPlatform>();
		}
		public OCUIT()
		{
			OCUITComponents = new List<OCUITComponent>();
			Platforms = new List<ManufacturerPlatform>();
		}

		public int Id { get; set; }
		public string Lifecycle { get; set; }
		public int OriginalId { get; set; }
		public int AddUserId { get; set; }
		public int DeactivateUserId { get; set; }
		public DateTime AddDate { get; set; }
		public DateTime DeactivateDate { get; set; }
		public IList<OCUITComponent> OCUITComponents { get; set; }
		public string Type { get; set; } //compatibility, continuity or jurisdiction chip
		public int ProjectId { get; set; }

		public GenerateQABundleModel Project { get; set; }

		public IList<ManufacturerPlatform> Platforms { get; set; }
		public IList<PlatformPiecesTemplateDTO> PlatformPiecesTemplates { get; set; }
	}

	public class OCUITComponent
	{
		public OCUITComponent()
		{
			AssociatedSoftwareTemplates = new List<AssociatedSoftwareTemplatesInfo>();
			Jurisdictions = new List<JurisdictionInfo>();
		}

		public int Id { get; set; }
		public int OrderId { get; set; }
		public bool Continuity { get; set; }
		public bool TestedInParallel { get; set; }
		public bool JurisdictionChip { get; set; }
		public int SubmissionId { get; set; }
		public int PlatformPieceId { get; set; }
		public string Description { get; set; }
		public SubmissionDTO Submission { get; set; }
		public IList<AssociatedSoftwareTemplatesInfo> AssociatedSoftwareTemplates { get; set; }
		public IList<JurisdictionInfo> Jurisdictions { get; set; }
	}
	public class ManufacturerPlatform
	{
		public int Id { get; set; }
		public string Name { get; set; }
		public int ManufacturerGroupId { get; set; }
	}
}