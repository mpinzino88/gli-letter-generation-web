﻿using EF.Submission;
using Submissions.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;
using Trirand.Web.Mvc;

namespace Submissions.Web.Areas.Tools.Models
{
	public class RegulatorCertificationNumberViewModel
	{
		public Mode Mode { get; set; }
		public int? Id { get; set; }
		public int? DocumentId { get; set; }
		[Editable(false)]
		public string GLIFileNumber { get; set; }
		[Display(Name = "Manufacturer")]
		public string ManufacturerName { get; set; }
		[Display(Name = "Game Name")]
		public string GameName { get; set; }
		[Display(Name = "Game Id")]
		public string GameId { get; set; }
		[Display(Name = "Game Type")]
		public string GameType { get; set; }
		[Required(ErrorMessage = "Certification/Job Number is required.")]
		[DisplayName("Certification Number")]
		public string CertNumber { get; set; }
		[Display(Name = "Certification Date"), DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy}")]
		public DateTime? CertDate { get; set; }
		[Display(Name = "Entry Date"), DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy}")]
		public DateTime? GLIEntryDate { get; set; }
		public string Jurisdiction { get; set; }
		public string ArchiveLocation { get; set; }
		public string Comments { get; set; }
		public string Comments2 { get; set; }
		[DisplayName("PreCert Path")]
		public string PreCertPath { get; set; }
		[DisplayName("PreCert File")]
		public HttpPostedFileBase PreCertFile { get; set; }

		//Juristiction
		public JurisdictionIndexGridModel DisplaySelectedGridModel { get; set; }
		public string CheckJurisdictionIds { get; set; }
		public string JurisdictionIdsPost { get; set; }
		public List<JurisdictionalData> JurisdictionDataList { get; set; }
		public JurisdictionIndexGridModel JurisdictionIndexGrid { get; set; }
		public JurisdictionIndexGridRecord JurisdictionGridData { get; set; }
		public CertificationIndexGridModel DisplayCertificationIndexGrid { get; set; }
		public CertificationIndexGridModelRW DisplayCertificationIndexGridRW { get; set; }
		public CertificationIndexGridModel CertificationIndexGrid { get; set; }
		public CertificationIndexGridRecord CertificationGridData { get; set; }
	}

	[Serializable]
	public class JurisdictionIndexGridRecord
	{
		[DisplayName("Primary")]
		public int PrimaryID { get; set; }
		[DisplayName("File Number")]
		public string SubmissionFileNumber { get; set; }
		public string SubmissionIdNumber { get; set; }
		[DisplayName("Game Name")]
		public string SubmissionGameName { get; set; }
		[DisplayName("Version")]
		public string SubmissionVersion { get; set; }
		[DisplayName("Jurisdictions")]
		public string Jurisdiction { get; set; }
	}

	public class JurisdictionIndexGridModel
	{
		public JQGrid Grid { get; set; }

		public JurisdictionIndexGridModel(bool displayModel = false)
		{
			Grid = new JQGrid
			{
				Columns = new List<JQGridColumn>
				{
					new JQGridColumn
					{
						DataField = "PrimaryID",
						PrimaryKey = true,
						Editable = false,
						Visible = false
					},
					new JQGridColumn
					{
						DataField = "SubmissionFileNumber",
						HeaderText = "File Number",
						DataType = typeof(string),
						Width =100
					},
					new JQGridColumn
					{
						DataField = "SubmissionIdNumber",
						HeaderText = "ID Number",
						DataType = typeof(string),
						Width =75
					},
					new JQGridColumn
					{
						DataField = "SubmissionGameName",
						HeaderText = "Game Name",
						DataType = typeof(string),
						Width =325
					},
					new JQGridColumn
					{
						DataField = "SubmissionVersion",
						HeaderText = "Version",
						DataType = typeof(string),
						Width =150
					},
					new JQGridColumn
					{
						DataField = "Jurisdiction",
						HeaderText = "Jurisdiction",
						DataType = typeof(string),
						Width =150
					}
				}
			};

			if (displayModel)
			{
				Grid.ID = "DisplaySelectedGrid";
				Grid.DataUrl = new UrlHelper(((MvcHandler)HttpContext.Current.Handler).RequestContext).Action("LoadDisplaySelectedGrid");
			}
			else
			{
				Grid.ID = "JurisdictionIndexGrid";
				Grid.DataUrl = new UrlHelper(((MvcHandler)HttpContext.Current.Handler).RequestContext).Action("LoadJurisdictionIndexGrid");
			}

			Grid.AutoWidth = true;
			Grid.Width = Unit.Percentage(50);
			Grid.Height = Unit.Percentage(100);
			Grid.MultiSelect = true;
			Grid.ToolBarSettings = new ToolBarSettings
			{
				ShowRefreshButton = true,
				ToolBarPosition = ToolBarPosition.Top
			};
			Grid.PagerSettings = new Trirand.Web.Mvc.PagerSettings
			{
				PageSize = 100,
				PageSizeOptions = "[]"
			};
			Grid.AppearanceSettings = new AppearanceSettings
			{
				AlternateRowBackground = true,
				HighlightRowsOnHover = true,
				ShowFooter = false
			};
		}
	}

	//JQGrid for Certification Number Index page
	[Serializable]
	public class CertificationIndexGridRecord
	{
		public int? Id { get; set; }
		[DisplayName("Manufacturer Name")]
		public string ManufacturerName { get; set; }
		[DisplayName("Game Name")]
		public string Gamename { get; set; }
		[DisplayName("Game ID")]
		public string GameId { get; set; }
		[DisplayName("Game Type")]
		public string Gametype { get; set; }
		[DisplayName("Cert Number")]
		public string CertNumber { get; set; }
		public DateTime? CertDate { get; set; }
		public string PreCertPath { get; set; }
		[DisplayName("GLI EntryDate")]
		[DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy}")]
		public DateTime? GLIEntryDate { get; set; }
		public string Jurisdiction { get; set; }
		[DisplayName("Comments")]
		public string Comments { get; set; }
		public string Edit { get; set; }
		public string Delete { get; set; }
	}

	public class CertificationIndexGridModel
	{
		public JQGrid CertGrid { get; set; }

		public CertificationIndexGridModel()
		{
			CertGrid = new JQGrid
			{
				Columns = new List<JQGridColumn>
				{
					new JQGridColumn
					{
						DataField = "Id",
						PrimaryKey = true,
						Editable = false,
						Visible = false
					},

					new JQGridColumn
					{
						DataField = "ManufacturerName",
						HeaderText = "Manufacturer Name",
						DataType = typeof(string),
						Width =150,
					},

					new JQGridColumn
					{
						DataField = "Gamename",
						HeaderText = "Game Name",
						DataType = typeof(string),
						Width = 150
					},

					new JQGridColumn
					{
						DataField = "GameId",
						HeaderText = "Game Id",
						DataType = typeof(string),
						Width = 150
					},

					new JQGridColumn
					{
						DataField = "GameType",
						HeaderText = "Game Type",
						DataType = typeof(string),
						Searchable = true,
						SearchType = SearchType.DropDown,
						SearchToolBarOperation = SearchOperation.IsEqualTo,
						SearchList = new List<SelectListItem>
						{
							new SelectListItem { Text = "", Value = ""},
							new SelectListItem { Text = "Skill Game", Value = "skillGame" },
							new SelectListItem { Text = "Raffle Game", Value = "raffleGame" },
							new SelectListItem { Text = "Gaming Machine", Value = "gamingMachine" },
							new SelectListItem { Text = "Non Skill", Value = "nonSkill" }
						},
						Width =200,
					},

					new JQGridColumn
					{
						DataField = "CertNumber",
						HeaderText = "CertNumber",
						DataType = typeof(string),
						SearchToolBarOperation = SearchOperation.IsEqualTo,
						Searchable = true,
						Width =100
					},

					new JQGridColumn
					{
						DataField = "CertDate",
						HeaderText = "CertDate",
						DataFormatString =  "{0:MM/dd/yyyy}",
						DataType = typeof(DateTime),
						Searchable = true,
						SearchToolBarOperation = SearchOperation.IsEqualTo,
						Width =100
					},

					new JQGridColumn
					{
						DataField = "GLIEntryDate",
						HeaderText = "GLIEntryDate",
						DataFormatString = "{0:MM/dd/yyyy}",
						DataType = typeof(DateTime),
						Searchable = true,
						SearchToolBarOperation = SearchOperation.IsEqualTo,
						Width =100
					},

					new JQGridColumn
					{
						DataField = "Jurisdiction",
						HeaderText = "Jurisdiction",
						DataType = typeof(string),
						Searchable = true,
						SearchType = SearchType.DropDown,
						SearchToolBarOperation = SearchOperation.IsEqualTo,
						SearchList = new List<SelectListItem>
						{
							new SelectListItem { Text = "", Value = ""},
							new SelectListItem { Text = "Arkansas - 214", Value = "214" },
							new SelectListItem { Text = "No Carolina - 42", Value = "42" },
							new SelectListItem { Text = "Oregon Lottery - 14", Value = "14" },
							new SelectListItem { Text = "No Carolina Raffle - 145", Value = "145" },
							new SelectListItem { Text = "North Carolina - Gaming Machines - 414", Value = "414" },
							new SelectListItem { Text = "Gauteng - 100", Value = "100" },
							new SelectListItem { Text = "Mpumalanga - 101", Value = "101" },
							new SelectListItem { Text = "Ohio Lottery - 129", Value = "129" },
							new SelectListItem { Text = "Western Cape - 102", Value = "102" },
							new SelectListItem { Text = "Eastern Cape - 103", Value = "103" },
							new SelectListItem { Text = "Northern Cape - 104", Value = "104" },
							new SelectListItem { Text = "Free State - 105", Value = "105" },
							new SelectListItem { Text = "North West Prov - 106", Value = "106" },
							new SelectListItem { Text = "KwaZulu-Natal - 108", Value = "108" },
							new SelectListItem { Text = "Limpopo - 315", Value = "315" },
							new SelectListItem { Text = "South Africa - 49", Value = "49" }
						},
						Width =150,
					},

					new JQGridColumn
					{
						DataField = "Comments",
						HeaderText = "Comments",
						DataType = typeof(string),
						Searchable = false,
						Width =200
					},

					new JQGridColumn
					{
						DataField = "Edit",
						HeaderText = "Edit",
						Width = 50,
						Sortable = false,
						Searchable = false,
						Formatter = new CustomFormatter{FormatFunction = "EditLink"}
					}
				}
			};

			CertGrid.ID = "DisplayCertificationIndexGrid";
			CertGrid.DataUrl = new UrlHelper(((MvcHandler)HttpContext.Current.Handler).RequestContext).Action("LoadCertificationIndexGrid");
			CertGrid.AutoWidth = true;
			CertGrid.Width = Unit.Percentage(100);
			CertGrid.Height = Unit.Percentage(100);
			CertGrid.ToolBarSettings = new ToolBarSettings
			{
				ShowRefreshButton = true,
				ShowSearchToolBar = true,
				ToolBarPosition = ToolBarPosition.Bottom
			};
			CertGrid.PagerSettings = new Trirand.Web.Mvc.PagerSettings
			{
				PageSize = 25,
				PageSizeOptions = "[]"
			};
			CertGrid.AppearanceSettings = new AppearanceSettings
			{
				AlternateRowBackground = true,
				HighlightRowsOnHover = true,
				ShowFooter = false
			};
		}
	}

	public class CertificationIndexGridModelRW : CertificationIndexGridModel //to be displayed in Index for users with Write permission only
	{
		public CertificationIndexGridModelRW() : base()
		{
			CertGrid.Columns.Add(
					new JQGridColumn
					{
						DataField = "Delete",
						HeaderText = "Delete",
						Width = 80,
						Sortable = false,
						Searchable = false,
						Formatter = new CustomFormatter { FormatFunction = "DeleteLink" }
					}
				);
		}
	}
}