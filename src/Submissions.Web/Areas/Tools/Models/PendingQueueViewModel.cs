﻿using EF.Submission;
using Submissions.Common;
using Submissions.Common.JQGrid;
using Submissions.Common.Linq.Dynamic;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;
using Trirand.Web.Mvc;

namespace Submissions.Web.Areas.Tools.Models
{
	public class PendingQueueViewModel
	{
		public PendingGridModel PendingEmailGrid { get; set; }
		public QueueSearchVM QueueSearch { get; set; }
	}

	#region Model


	[Serializable]
	public class QueueSearchVM
	{
		[DisplayName("Email Sent From")]
		public DateTime? SentFrom { get; set; }
		[DisplayName("Email Sent To")]
		public DateTime? SentTo { get; set; }
		[DisplayName("File Number")]
		public string FileNumber { get; set; }

		[DisplayName("Jurisdiction")]
		public string JurisdictionId { get; set; }
		public string Jurisdiction { get; set; }

		[DisplayName("Manufacturer")]
		public string ManufacturerCode { get; set; }
		public string Manufacturer { get; set; }

		[DisplayName("Only Pending")]
		public bool IsPending { get; set; }
		[DisplayName("Only Deleted")]
		public bool IsDeleted { get; set; }
	}


	public class PendingEmailVM
	{
		[DisplayName("Email Id")]
		public int EmailID { get; set; }

		[DisplayName("Email To")]
		public string To { get; set; }

		[DisplayName("From")]
		public string From { get; set; }

		[DisplayName("Subject")]
		public string Subject { get; set; }

		[DisplayName("PDF Path")]
		public string AttachPath { get; set; }

		[DisplayName("FileNumber")]
		public string FileNumber { get; set; }

		[DisplayName("Jurisdiction")]
		public string Jurisdiction { get; set; }
		public string JurisdictionId { get; set; }

		[DisplayName("Queued Time")]
		public DateTime? CreateTS { get; set; }

		[DisplayName("Sent")]
		public DateTime? SentTS { get; set; }

		[DisplayName("Deleted")]
		public bool? IsDeleted { get; set; }

		[DisplayName("Enforced Run")]
		public bool? IsEnforceRun { get; set; }

	}
	#endregion

	#region gridModel
	public class PendingGridModel
	{
		public JQGrid Grid { get; set; }
		public PendingGridModel()
		{
		   
			Grid = new JQGrid
			{
				EditUrl = new UrlHelper(((MvcHandler)HttpContext.Current.Handler).RequestContext).Action("SaveGrid"),
				Columns = new List<JQGridColumn>
				{
					new JQGridColumn
					{
						HeaderText = "Actions",
						EditActionIconsColumn = true,
						Width =25
					},
					new JQGridColumn
					{
						DataField = "EmailID",
						PrimaryKey = true,
						Editable = false,
						Visible = false
					},
					new JQGridColumn
					{
						DataField = "FileNumber",
						HeaderText = "File Number",
						Width = 40,
						Formatter = new CustomFormatter{FormatFunction = "fileNumberLink"}
					},
					new JQGridColumn
					{
						DataField = "Jurisdiction",
						HeaderText = "Jurisdiction",
						Width = 40
					},
					new JQGridColumn
					{
						DataField = "JurisdictionId",
						HeaderText = "Jur Id",
						Width = 40
					},
					new JQGridColumn
					{
						DataField = "To",
						HeaderText = "Email To",
						Width = 110,
						Editable = true,
						EditType = EditType.TextArea
					},
					new JQGridColumn
					{
						DataField = "AttachPath",
						HeaderText = "PDF Path",
						Width = 150,
						Formatter = new CustomFormatter{FormatFunction = "formatterPdfPath"},
					},
					new JQGridColumn
					{
						DataField = "CreateTS",
						HeaderText = "Queued Date",
						DataFormatString = "{0:g}",
						DataType = typeof(DateTime),
						Width = 45
					},
					new JQGridColumn
					{
						DataField = "SentTS",
						HeaderText = "Sent Date",
						DataFormatString = "{0:g}",
						DataType = typeof(DateTime),
						Width = 45
					},
					new JQGridColumn
					{
						DataField = "IsDeleted",
						Visible = false
					},
					new JQGridColumn
					{
						DataField = "IsEnforceRun",
						Visible = false
					},
					new JQGridColumn
					{
						HeaderText = "Send Email",
						Formatter = new CustomFormatter{FormatFunction = "SendEmailButton"},
						Width = 30,

					}
				}
			};

			Grid.ID = "PendingEmailGrid";
			Grid.DataUrl = new UrlHelper(((MvcHandler)HttpContext.Current.Handler).RequestContext).Action("LoadPendingListGrid");
			Grid.AutoWidth = true;
			Grid.Width = Unit.Percentage(100);
			Grid.Height = Unit.Percentage(100);
			Grid.ToolBarSettings = new ToolBarSettings
			{
				ShowRefreshButton = true,
				ToolBarPosition = ToolBarPosition.Top
			};
			Grid.PagerSettings = new Trirand.Web.Mvc.PagerSettings
			{
				PageSize = 100,
				PageSizeOptions = "[]"
			};
			Grid.AppearanceSettings = new AppearanceSettings
			{
				AlternateRowBackground = true,
				HighlightRowsOnHover = true,
				ShowFooter = false
			};
					   
		}

		//private IList<string> DownloadableColumns()
		//{
		//    return new List<string>
		//            {
		//                "EmailID",
		//                "FileNumber",
		//                "Jurisdiction",
		//                "To",
		//                "AttachPath",
		//                "CreateTS",
		//                "SentTS",
		//                "IsDeleted"
		//            };
		//}
		public string GetColumnsDynamicLinqFormat()
		{
			var selectFields = this.Grid.Columns;
			var selectStatement = new StringBuilder();
			selectStatement.Append("new (");

			foreach (var col in selectFields)
			{
				if (! string.IsNullOrEmpty(col.DataField))
				{
					if (col.DataField == "Jurisdiction")
					{
						selectStatement.AppendFormat("{0} As {1},", "Jurisdiction.Name", col.DataField);
					}
					else
					{
						selectStatement.AppendFormat("{0} As {1},", col.DataField, col.DataField);
					}
					
				}
			}

			selectStatement.Length--;
			selectStatement.Append(")");

			return selectStatement.ToString();
		}
		public void Export(Export export, IQueryable<PendingEmail> query)
		{
			var selectStatement = this.GetColumnsDynamicLinqFormat();
			var results = query.Select(selectStatement);

			var fileName = string.Format(export.Title + " - {0:yyyy-MM-dd_hh-mm-ss-tt}", DateTime.Now);
			var jqGridHelper = new JQGridHelper();
			var jqGridState = new JQGridState();
			jqGridState.QueryString = HttpUtility.ParseQueryString(export.FiltersToolbar ?? "");

			if (export.ExportType == ExportType.Xls)
			{
				fileName += ".xls";
				this.Grid.ExportToExcel(results, fileName, jqGridState);
			}
			else if (export.ExportType == ExportType.Csv)
			{
				fileName += ".csv";
				this.Grid.ExportSettings.ExportDataRange = ExportDataRange.All;
				var exportData = this.Grid.GetExportData(results, jqGridState);
				jqGridHelper.ExportToCSV(exportData, fileName);
			}
			else if (export.ExportType == ExportType.Pdf)
			{
				fileName += ".pdf";
				this.Grid.ExportSettings.ExportDataRange = ExportDataRange.All;
				var exportData = this.Grid.GetExportData(results, jqGridState);
				jqGridHelper.ExportToPDF(exportData, fileName);
			}
		}

	}


}
#endregion
