﻿namespace Submissions.Web.Areas.Tools.Models
{
	public class ClauseTemplateManagementIndexViewModel
	{
		public ClauseTemplateManagementIndexViewModel()
		{

		}
	}

	public class ClauseTemplateManagementEditViewModel
	{
		public ClauseTemplateManagementEditViewModel()
		{

		}
		public int Id { get; set; }
		public string ManufacturerId { get; set; }
		public int JurisdictionId { get; set; }
		public string Label { get; set; }

	}

}