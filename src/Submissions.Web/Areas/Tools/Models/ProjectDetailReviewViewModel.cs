﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Submissions.Web.Areas.Tools.Models
{
	public class ProjectDetailReviewIndexViewModel
	{
		public ProjectDetailReviewIndexViewModel()
		{
			this.Submissions = new List<ProjectDetailReviewSubmissionViewModel>();
		}

		[Display(Name = "Start Date"), Required]
		public DateTime StartDate { get; set; }
		[Display(Name = "End Date")]
		public DateTime? EndDate { get; set; }
		public IList<ProjectDetailReviewSubmissionViewModel> Submissions { get; set; }
		public string SessionUser { get; set; }
	}

	public class ProjectDetailReviewSubmissionViewModel
	{
		public int SubmissionHeaderId { get; set; }
		public string FileNumber { get; set; }
		public DateTime? SubmitDate { get; set; }
		public string EditUser { get; set; }
		public string ProjectDetail { get; set; }
	}
}