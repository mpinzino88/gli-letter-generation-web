﻿using Aspose.Cells;
using Submissions.Common;
using Submissions.Common.Kendo;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web;
using System.Web.Mvc;
namespace Submissions.Web.Areas.Tools.Models
{
	public class TKNumberEditViewModel
	{
		public TKNumberEditViewModel()
		{
			this.SubmissionsRecord = new List<SubmissionsRecord>();
			this.TKFile = new List<KendoFile>();
			this.workbook = new Workbook();
			this.TKFileUpload = new List<HttpPostedFileBase>();
			this.Jurisdictions = new List<string>();
			this.JurisdictionDesc = new List<SelectListItem>();
			this.RegCertIds = new List<string>();
			this.keyTbls = new List<int>();
			this.HollandJurisdictions = hollandJurisdictions;

		}

		private IEnumerable<SelectListItem> hollandJurisdictions = new List<SelectListItem>()
		{
			new SelectListItem { Text = "", Value = "" },
			new SelectListItem { Text = CustomJurisdiction.HollandArcade.GetEnumDescription(), Value = ((int) CustomJurisdiction.HollandArcade).ToString() },
			new SelectListItem { Text = CustomJurisdiction.HollandCasinos.GetEnumDescription(), Value = ((int) CustomJurisdiction.HollandCasinos).ToString() },
			new SelectListItem { Text = CustomJurisdiction.HollandLottery.GetEnumDescription(), Value = ((int) CustomJurisdiction.HollandLottery).ToString() },
			new SelectListItem { Text = CustomJurisdiction.HollandSkillAmusementGamey.GetEnumDescription(), Value = ((int) CustomJurisdiction.HollandSkillAmusementGamey).ToString() },
			new SelectListItem { Text = CustomJurisdiction.HollandStreet.GetEnumDescription(), Value = ((int) CustomJurisdiction.HollandStreet).ToString() }
		};
		public List<SubmissionsRecord> SubmissionsRecord { get; set; }
		public KSANumberIndexViewModel IndexViewModel { get; set; }
		[Display(Name = "Jurisdiction")]
		public IEnumerable<SelectListItem> HollandJurisdictions { get; set; }
		[Display(Name = "File Number")]
		public string FileNumber { get; set; }
		[Display(Name = "KSA Numbers")]
		public string TKNumbers { get; set; }
		public string ProjectName { get; set; }
		public string IdNumber { get; set; }
		public string GameName { get; set; }
		public string Jurisdiction { get; set; }
		public List<string> Jurisdictions { get; set; }
		public List<SelectListItem> JurisdictionDesc { get; set; }
		public string selIds { get; set; }
		public IList<KendoFile> TKFile { get; set; }
		public List<string> RegCertIds { get; set; }
		public List<int> keyTbls { get; set; }
		public List<HttpPostedFileBase> TKFileUpload { get; set; }
		public Workbook workbook { get; set; }
		public bool saveRecord { get; set; }
		public int? primary { get; set; }

	}

	public class KSANumberIndexViewModel
	{
		public int? Id { get; set; }
		[Display(Name = "KSA Number")]
		public string CertNumber { get; set; }
		public int? Primary { get; set; }
		public string FileNumber { get; set; }
		public string Jurisdiction { get; set; }

	}
	public class SubmissionsRecord
	{
		public int Id { get; set; }
		public int ProjectId { get; set; }
		public string SubmissionKey { get; set; }
		[Display(Name = "File Number")]
		public string FileNumber { get; set; }
		public string IdNumber { get; set; }
		public string GameName { get; set; }
		public string Jurisdiction { get; set; }
		public string JurisdictionType { get; set; }
		public string Version { get; set; }
		public int JurisdictionId { get; set; }
		public List<string> RegCertIds { get; set; }
		public bool Selected { get; set; }
		public string DateCode { get; set; }
		[DisplayFormat(DataFormatString = "{0:d}")]
		public DateTime? SubmissionsReceivedDate { get; set; }

	}


}





