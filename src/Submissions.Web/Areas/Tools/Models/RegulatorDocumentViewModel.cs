﻿using System;
using System.Collections.Generic;
using System.Linq;
using Submissions.Common;
using System.Web.Mvc;
using EF.Submission;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web;
using System.Web.UI.WebControls;
using Trirand.Web.Mvc;
using Newtonsoft.Json;

namespace Submissions.Web.Areas.Tools.Models
{
	public class RegulatorDocumentViewModel
	{
		public Mode Mode { get; set; }
		public int? Id { get; set; }
		[Required(ErrorMessage = "Date is required.")]
		[DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy}")]
		public DateTime? Date { get; set; }
		public string Path { get; set; }
		public string ManfacturerName { get; set; }
		[DisplayName("PreCert Document")]
		public HttpPostedFileBase PreCertFile { get; set; }
		#region Job Number Variables
		[DisplayName("Cert Number")]
		public string CertNumber { get; set; }
		public string GameName { get; set; }
		[DisplayName("Manufacturer Name")]
		public string ManufacturerName { get; set; }
		public string Jurisdiction { get; set; }
		[DisplayName("Game Type")]
		public string GameType { get; set; }
		[DisplayName("Game ID")]
		public string GameId { get; set; }
		[DisplayName("GLI Entry Date")]
		[DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy}")]
		public DateTime? GLIEntryDate { get; set; }
		#endregion

		//Document Index page JQGrid
		public DocumentIndexGridModel DisplayDocumentIndexGrid { get; set; }
		public DocumentIndexGridModel DocumentIndexGrid { get; set; }
		public DocumentIndexGridRecord DocumentGridData { get; set; }
		public DocumentIndexGridModelRW DisplayDocumentIndexGridRW { get; set; }
		public RegulatorCertificationNumberViewModel mainView { get; set; }
		public List<CertNumData> CertNumDataList { get; set; }
		public string certNums { get; set; }
		
	}

	[Serializable]
	public class DocumentIndexGridRecord
	{
		public int? Id { get; set; }
		[DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:MM/dd/yyyy}")]
		public DateTime? Date { get; set; }
		[DisplayName("Document Path")]
		public string Path { get; set; }
		public string Jurisdiction { get; set; }
		public string Edit { get; set; }
		public string Delete { get; set; }
	
	}

	public class DocumentIndexGridModel
	{
		public JQGrid Grid { get; set; }

		public DocumentIndexGridModel()
		{
			Grid = new JQGrid
			{
				Columns = new List<JQGridColumn>
				{
					new JQGridColumn
					{
						DataField = "Id",
						PrimaryKey = true,
						Editable = false,
						Visible = false
			   
					},

						new JQGridColumn
					{
						DataField = "Date",
						HeaderText = "Document Date",
						DataFormatString = "{0:MM/dd/yyyy}", 
						DataType = typeof(DateTime),
						Searchable = true,
						SearchToolBarOperation = SearchOperation.IsEqualTo,
						Width = 60 
					},

					new JQGridColumn
					{
						DataField = "Path",
						HeaderText = "Document Path",
						DataType = typeof(string),
						Width = 200 
						
					},

					new JQGridColumn
					{
						DataField = "Jurisdiction",
						HeaderText = "Jurisdiction",
						DataType = typeof(string),
						Searchable = true,
						SearchType = SearchType.DropDown,
						SearchToolBarOperation = SearchOperation.IsEqualTo,
						SearchList = new List<SelectListItem>
						{
							new SelectListItem { Text = "", Value = ""},
							new SelectListItem { Text = "Arkansas - 214", Value = "214" },
							new SelectListItem { Text = "No Carolina - 42", Value = "42" },
							new SelectListItem { Text = "Oregon Lottery - 14", Value = "14" },
							new SelectListItem { Text = "No Carolina Raffle - 145", Value = "145" },
							new SelectListItem { Text = "North Carolina - Gaming Machines - 414", Value = "414" },
							new SelectListItem { Text = "Gauteng - 100", Value = "100" },
							new SelectListItem { Text = "Gauteng - 100", Value = "100" },
							new SelectListItem { Text = "Mpumalanga - 101", Value = "101" },
							new SelectListItem { Text = "Ohio Lottery - 129", Value = "129" },
							new SelectListItem { Text = "Western Cape - 102", Value = "102" },
							new SelectListItem { Text = "Eastern Cape - 103", Value = "103" },
							new SelectListItem { Text = "Northern Cape - 104", Value = "104" },
							new SelectListItem { Text = "Free State - 105", Value = "105" },
							new SelectListItem { Text = "North West Prov - 106", Value = "106" },
							new SelectListItem { Text = "KwaZulu-Natal - 108", Value = "108" },
							new SelectListItem { Text = "Limpopo - 315", Value = "315" },
                            new SelectListItem { Text = "South Africa - 49", Value = "49" }
						
						},

						Width = 50 
						
					},

					new JQGridColumn
					{

						DataField = "Edit",
						HeaderText = "Edit",
						Width = 25,
						Sortable = false,
						Searchable = false,
						Formatter = new CustomFormatter{FormatFunction = "EditLink"}

					}
													
				}

			};

			Grid.ID = "DisplayDocumentIndexGrid";
			Grid.DataUrl = new UrlHelper(((MvcHandler)HttpContext.Current.Handler).RequestContext).Action("LoadDocumentIndexGrid");
			Grid.AutoWidth = true;
			Grid.Width = Unit.Percentage(100);
			Grid.Height = Unit.Percentage(100);
			Grid.ToolBarSettings = new ToolBarSettings
			{
				ShowRefreshButton = true,
				ShowSearchToolBar = true,
				ToolBarPosition = ToolBarPosition.Bottom
			};
			Grid.PagerSettings = new Trirand.Web.Mvc.PagerSettings
			{
				PageSize = 25,
				PageSizeOptions = "[]"
			};
			Grid.AppearanceSettings = new AppearanceSettings
			{

				AlternateRowBackground = true,
				HighlightRowsOnHover = true,
				ShowFooter = false
			};

		}
		
	}

	public class CertNumData
	{

		public string CertNumber { get; set; }
		public string GameName { get; set; }
		public string GameType { get; set; }
		public string GameId { get; set; }
		public string GLIEntryDate { get; set; }
		public int? CertId { get; set; }

		public CertNumData(string certNum, string gameName, GameTypeShortListCertificationNumber gameType, string gameId, string gliEntryDate, int? CertId)
		{

			CertNumber = certNum;
			GameName = gameName;
			GameType = gameType.ToString();
			GameId = gameId;
			GLIEntryDate = gliEntryDate;
			this.CertId = CertId;

		}

		[JsonConstructor]
		public CertNumData(string certNum, string gameName, string gameType, string gameId, string gliEntryDate, int? CertId)
		{
			CertNumber = certNum;
			GameName = gameName;
			if(gameType != null)
			{
			GameType = gameType.Replace(" ", "");			
			}
			GameId = gameId;
			GLIEntryDate = gliEntryDate;
			this.CertId = CertId;

		}
	}


	public class DocumentIndexGridModelRW : DocumentIndexGridModel //to be displayed in Index for users with Write permission only
	{
		public DocumentIndexGridModelRW()
			: base()
		{
			Grid.Columns.Add(
					new JQGridColumn
					{
						DataField = "Delete",
						HeaderText = "Delete",
						Width = 80,
						Sortable = false,
						Searchable = false,
						Formatter = new CustomFormatter { FormatFunction = "DeleteLink" }

					}
				);

		}
	}


}