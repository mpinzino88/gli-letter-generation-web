﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Submissions.Web.Areas.Tools.Models.MasterBilling
{
	using Submissions.Web.Areas.Tools.Models.MasterBilling.KO_Models;

	public class EditViewModel
	{
		public EditViewModel()
		{
			this.MasterBillingProject = new MasterBillingProjectViewModel();
			this.DefaultJurisdiction = new Jurisdiction();
			this.ApplyToAll = false;
		}

		public string MBID { get; set; }
		public bool ApplyToAll { get; set; }
		public Submission SubmissionRecord { get; set; }
		public MasterBillingProjectViewModel MasterBillingProject { get; set; }

		//Default objects
		public Jurisdiction DefaultJurisdiction { get; set; }
	}

	public class MasterBillingProjectViewModel
	{
		public MasterBillingProjectViewModel()
		{
			this.AssociatedJurisdictions = new List<Jurisdiction>();
			this.UnAssociatedJurisdictions = new List<Jurisdiction>();
			this.markedForAssociation = new List<Jurisdiction>();
			this.markedForDisAssociation = new List<Jurisdiction>();
		}

		public IList<Jurisdiction> AssociatedJurisdictions { get; set; }
		public IList<Jurisdiction> UnAssociatedJurisdictions { get; set; }
		public IList<Jurisdiction> markedForAssociation { get; set; }
		public IList<Jurisdiction> markedForDisAssociation { get; set; }
	}
}