﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Submissions.Web.Areas.Tools.Models.MasterBilling.KO_Models;

namespace Submissions.Web.Areas.Tools.Models.MasterBilling
{
	using Submissions.Web.Areas.Tools.Models.MasterBilling.KO_Models;

	public class IndexViewModel
	{
		public IndexViewModel()
		{
			this.DefaultJurisdiction = new Jurisdiction();
			this.DefaultSubmission = new Submission();
			this.MasterBillingProject = new MasterBillingProjectIndexViewModel();
		}

		public MasterBillingProjectIndexViewModel MasterBillingProject { get; set; }
		public bool Results { get; set; }
		public string BulkUpdateModel { get; set; }

		// Default Objects
		public Jurisdiction DefaultJurisdiction { get; set; }
		public Submission DefaultSubmission { get; set; }
		public int? JurisdictionFilterId { get; set; }
		public string JurisdictionFilterName { get; set; }
	}

	public class MasterBillingProjectIndexViewModel
	{
		public MasterBillingProjectIndexViewModel()
		{
			this.Submissions = new List<Submission>();
		}
		public IList<Submission> Submissions { get; set; }
	}
}