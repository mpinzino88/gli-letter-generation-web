﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Submissions.Web.Areas.Tools.Models.MasterBilling.KO_Models
{
	public class Jurisdiction
	{
		public bool _destroy { get; set; }
		public bool Marked { get; set; }
		public int Id { get; set; }
		public string Name { get; set; }
		public string JurisdictionId { get; set; }
		public string MasterBillingId { get; set; }
		public int SubmissionId { get; set; }
	}
}