﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Submissions.Web.Areas.Tools.Models.MasterBilling.KO_Models
{
	public class Submission
	{
		public Submission()
		{
			this.Jurisdictions = new List<Jurisdiction>();
		}

		public bool _destroy { get; set; }
		public int Id { get; set; }
		public bool Marked { get; set; }
		public string FileNumber { get; set; }
		public string IdNumber { get; set; }
		public string GameName { get; set; }
		public string Version { get; set; }
		public string DateCode { get; set; }
		public string MBID { get; set; }

		public IList<Jurisdiction> Jurisdictions { get; set; }
	}
}