﻿using System.Collections.Generic;

namespace Submissions.Web.Areas.Tools.Models.MasterBilling
{
	using Submissions.Web.Areas.Tools.Models.MasterBilling.KO_Models;

	public class AddAssociationViewModel
	{
		public AddAssociationViewModel()
		{
			this.DefaultJurisdiction = new Jurisdiction();
			this.DefaultSubmission = new Submission();


			this.MasterBillingAssociation = new MasterBillingAddAssocViewModel();
		}

		public string MBID { get; set; }
		public int? ProjectId { get; set; }
		public string ProjectName { get; set; }
		public bool Results { get; set; }
		public MasterBillingAddAssocViewModel MasterBillingAssociation { get; set; }

		// Default Objects
		public Jurisdiction DefaultJurisdiction { get; set; }
		public Submission DefaultSubmission { get; set; }


	}

	public class MasterBillingAddAssocViewModel
	{
		public MasterBillingAddAssocViewModel()
		{
			this.SubmissionRecords = new List<Submission>();
			this.JurisdictionList = new List<Jurisdiction>();
		}

		public string ContextIdNumber { get; set; }
		public IList<Submission> SubmissionRecords { get; set; }
		public IList<Jurisdiction> JurisdictionList { get; set; }
	}
}