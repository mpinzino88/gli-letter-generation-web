﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Submissions.Web.Areas.Tools.Models
{
	public class ProtocolDeficienciesIndexViewModel
	{
		public List<ProtocolDeficiency> Deficiencies { get; set; }
		public string ProtocolMode { get; set; }
		public IList<string> AllProtocols { get; set; }
		public bool ReviewMode { get; set; } = false;
		public ProtocolDeficienciesIndexViewModel()
		{
			Deficiencies = new List<ProtocolDeficiency>();
			var protocols = Enum.GetValues(typeof(Protocol));
			AllProtocols = new List<string>();
			//ret.Add(BuildTab(LetterGenerationSource.LetterDetail));
			foreach (Protocol protocol in protocols)
			{
				AllProtocols.Add(protocol.ToString());
			}

		}
	}

	public class ProtocolDeficienciesEditViewModel
	{
		public Submissions.Common.Mode Mode { get; set; }
		public ProtocolDeficiency Deficiency { get; set; }
		public bool ReviewMode { get; set; } = false;
		public ProtocolDeficienciesEditViewModel()
		{

		}
	

	}

	public class ProtocolDeficiency
	{
		public ProtocolDeficiency()
		{
			Platforms = new List<ProtocolPlatform>();
		}
		public int Id { get; set; }
		public string Protocol { get; set; }
		public bool Critical { get; set; }
		public string Description { get; set; }
		public string Lifecycle { get; set; }
		public int OriginalId { get; set; }
		public string AddUserName { get; set; }
		public DateTime AddDate { get; set; }
		public IList<ProtocolPlatform> Platforms { get; set; }

		public bool IncludeOnLetter { get; set; }	
		public bool IncludeOnWVLetter { get; set; } 
	}
	public class GATDeficiency : ProtocolDeficiency
	{
		public string Command { get; set; }
		public string CommandName { get; set; }
		public string Requirement { get; set; }
	}

	public class SASDeficiency : ProtocolDeficiency
	{
		public int PollNameId { get; set; }
		public string PollName { get; set; }
		public string Section { get; set; }
		public string LP { get; set; }
		public string Exception { get; set; }

	}

	public class ProtocolPlatform
	{
		public int Id { get; set; }
		public string Name { get; set; }
		public int ManufacturerGroupId { get; set; }
	}

	public enum Protocol
	{
		SAS,
		GAT
	}
}