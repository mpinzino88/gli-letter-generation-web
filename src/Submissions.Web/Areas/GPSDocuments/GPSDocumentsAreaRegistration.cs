﻿using System.Web.Mvc;

namespace Submissions.Web.Areas.GPSDocuments
{
	public class GPSDocumentsAreaRegistration : AreaRegistration
	{
		public override string AreaName
		{
			get
			{
				return "GPSDocuments";
			}
		}

		public override void RegisterArea(AreaRegistrationContext context)
		{
			context.MapRoute(
				"GPSDocuments_default",
				"GPSDocuments/{controller}/{action}/{id}",
				new { action = "Index", id = UrlParameter.Optional }
			);
		}
	}
}