﻿using Submissions.Core;
using Submissions.Core.Models.GPSFileTreeService;
using Submissions.Web.Areas.GPSDocuments.Models;
using System;
using System.Web.Mvc;

namespace Submissions.Web.Areas.GPSDocuments.Controllers
{
	public class GPSDocumentsController : Controller
	{
		private readonly IGPSFileTreeService _gpsFileTreeService;
		private readonly IConfigContext _configContext;

		public GPSDocumentsController(IGPSFileTreeService gpsFileTreeService, IConfigContext configContext)
		{
			_gpsFileTreeService = gpsFileTreeService;
			_configContext = configContext;
		}

		public ActionResult Index(string fileNumber, string callLocation, int? jurisId)
		{
			var gpsFileTreeLoad = new GPSFileTreeLoad
			{
				Context = Url,
				FileNumber = fileNumber,
				JurisdictionId = Convert.ToInt32(jurisId),
				LoadRegulator = true,
				LoadSupplier = true,
				TemplatePath = "",
				WebApiPath = _configContext.Config.ApiUrl
			};

			var tree = _gpsFileTreeService.Load(gpsFileTreeLoad);

			var gpsViewModel = new GPSDocumentsViewModel
			{
				FileTree = tree,
				FileNumber = fileNumber
			};

			return View(gpsViewModel);
		}

		public JsonResult GetFolderInfo(string callLocation, string path)
		{
			var gpsFileSubDirectory = new GPSFileSubDirectoryLoad
			{
				Context = Url,
				SubDirectoryPath = path,
				WebApiPath = _configContext.Config.ApiUrl
			};

			var tree = _gpsFileTreeService.LoadSubDirectory(gpsFileSubDirectory);
			var gpsViewModel = new GPSDocumentsViewModel
			{
				FileTree = tree
			};
			JsonResult json = new JsonResult();
			json.Data = new { Value = gpsViewModel.FileTree, ID = path };
			return json;
		}

		public JsonResult GetTemplateFolderInfo(string templatePath, string fileNumber)
		{
			var gpsFileTreeLoad = new GPSFileTreeLoad
			{
				CallLocation = "Internal",
				Context = Url,
				FileNumber = fileNumber,
				JurisdictionId = -2,
				LoadRegulator = true,
				LoadSupplier = true,
				TemplatePath = templatePath,
				WebApiPath = _configContext.Config.ApiUrl
			};

			var tree = _gpsFileTreeService.Load(gpsFileTreeLoad);

			JsonResult json = new JsonResult();
			json.Data = new { Value = tree, ID = "GPSDocumentFileTree" };
			return json;
		}
	}
}