﻿using EF.GPS;
using FUWUtil;
using GLI.EFCore.Submission;
using Submissions.Common;
using Submissions.Web.Areas.GPSDocuments.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Http.Description;
using System.Web.Mvc;

namespace Submissions.Web.Areas.GPSDocuments.Controllers
{
	using System.Web.Http;

	public class MoveFileToGPSController : Controller
	{
		private static string FlatfileUploadPath = System.Configuration.ConfigurationManager.AppSettings["FlatfileUploadPath"];

		private readonly SubmissionContext _dbSubmission;

		private FUWUtil.FUWResult _uploadResult;
		private MetaData RequestMetaData;

		public MoveFileToGPSController(SubmissionContext dbSubmission)
		{
			_dbSubmission = dbSubmission;
			_uploadResult = new FUWUtil.FUWResult();
		}

		#region HTTP Post Request handler
		[HttpPost]
		[ResponseType(typeof(ActionResult))]
		public ActionResult UploadFlatFileToGPS([FromBody] FlatFileUploadRequest Request)
		{
			if (ModelState.IsValid)
			{
				RequestMetaData = Request.MetaData;
				var flatFiles = getFlatFiles(Request.Ids).ToList();
				var uploads = constructUploadList(flatFiles).Distinct().ToList();
				var results = processAllUploads(uploads);

				if (results.Where(x => x.Res != Result.SUCCESS).ToList().Count == 0)
				{
					return new HttpStatusCodeResult(HttpStatusCode.OK, "Success");
				}
				else
				{
					return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "FUW Failure.");
				}
			}
			else
			{
				return new HttpStatusCodeResult(HttpStatusCode.BadRequest, "Invalid JSON Model.");
			}
		}
		#endregion

		#region Data Access and Model Manipulation
		private List<FlatFileUpload> getFlatFiles(List<int> ids)
		{
			return _dbSubmission.trIGTTemporaryTables
				.Where(x => ids.Contains(x.Id))
				.Select(x => new FlatFileUpload
				{
					FileNumber = x.Submission.FileNumber,
					MappedFileNumber = x.MappedSubmission.FileNumber,
					NewSubmissionFileNumber = x.NewSubFileNumber,
					SpreadSheetName = x.SpreadSheetName,
					Status = x.Status
				})
				.ToList();
		}

		private List<Upload> constructUploadList(List<FlatFileUpload> flatFilesForUpload)
		{
			var Uploads = new List<Upload>();

			foreach (var flatFileUpload in flatFilesForUpload)
			{
				if (flatFileUpload.Status == "PND")
				{
					Uploads.Add(new Upload { FileNumber = flatFileUpload.NewSubmissionFileNumber, SpreadSheetName = flatFileUpload.SpreadSheetName });
				}
				else
				{
					if (!String.IsNullOrWhiteSpace(flatFileUpload.MappedFileNumber))
					{
						Uploads.Add(new Upload { FileNumber = flatFileUpload.MappedFileNumber, SpreadSheetName = flatFileUpload.SpreadSheetName });
					}
					else
					{
						Uploads.Add(new Upload { FileNumber = flatFileUpload.FileNumber, SpreadSheetName = flatFileUpload.SpreadSheetName });
					}
				}
			}
			return Uploads;
		}
		#endregion

		#region FUW Upload
		private List<FUWResult> processAllUploads(List<Upload> uploads)
		{
			var Result = new List<FUWResult>();
			foreach (var spreadsheet in uploads)
			{
				Result.Add(upload(spreadsheet));
			}
			return Result;
		}

		private FUWResult upload(Upload spreadsheet)
		{
			string archiveLocation = null;
			var root = FUWDirTree.GenerateFUWTree(new FUWUtil.FileNumber(spreadsheet.FileNumber), true, ref archiveLocation, false, false, true, true, true, false);
			var dir = root.DescendantOrSelf(node => node.Name.Contains(GPSDirName.OfficialRequests.GetEnumDescription()));
			var gpsFileInfo = new GPSFileInfo(FlatfileUploadPath + "\\" + spreadsheet.SpreadSheetName);
			var uploadableGPSFileInfo = gpsFileInfo as Uploadable;
			uploadableGPSFileInfo.Action = ItemAction.UPLOADNEW;
			dir.EntriesToProcess.Add(uploadableGPSFileInfo);
			var gpsLabId = FUWUtil.FUWUtil.Laboratories.FirstOrDefault(lab => lab.Location == archiveLocation || lab.ArchiveLocation == archiveLocation).ID;
			return _uploadResult = dir.ProcessAll(gpsLabId);
		}
		#endregion
	}
}