﻿namespace Submissions.Web.Areas.GPSDocuments.Models
{
	public class Upload
	{
		public string FileNumber { get; set; }
		public string SpreadSheetName { get; set; }

		public override bool Equals(object obj)
		{
			if (obj == null || GetType() != obj.GetType())
				return false;


			Upload p = (Upload)obj;
			return (this.FileNumber == p.FileNumber) && (this.SpreadSheetName == p.SpreadSheetName);
		}

		public override int GetHashCode()
		{
			return FileNumber.GetHashCode() ^ SpreadSheetName.GetHashCode();
		}
	}
}