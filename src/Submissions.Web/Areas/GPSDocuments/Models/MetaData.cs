﻿using System;

namespace Submissions.Web.Areas.GPSDocuments.Models
{
	public class MetaData
	{
		public MetaData()
		{

		}

		public MetaData(int UserId, DateTime TimeStamp)
		{
			this.UserId = UserId;
			this.TimeStamp = TimeStamp;
		}
		public int UserId { get; set; }
		public DateTime TimeStamp { get; set; }
	}
}