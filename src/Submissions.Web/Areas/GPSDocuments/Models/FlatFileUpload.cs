﻿namespace Submissions.Web.Areas.GPSDocuments.Models
{
	public class FlatFileUpload
	{
		public string Status { get; set; }
		public string FileNumber { get; set; }
		public string MappedFileNumber { get; set; }
		public string NewSubmissionFileNumber { get; set; }
		public string SpreadSheetName { get; set; }
	}
}
