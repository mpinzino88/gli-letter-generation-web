﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Submissions.Web.Areas.GPSDocuments.Models
{
	public class GPSDocumentsViewModel
	{
		public string FileTree { get; set; }
        public string FileNumber { get; set; }
        public IEnumerable<GPSTemplate> OrderedTemplateList { get; set; }

        private List<GPSTemplate> TemplateList { get; set; }

        public GPSDocumentsViewModel() {
            TemplateList = new List<GPSTemplate>();


            if( Directory.Exists( FUWUtil.FUWUtil.LOC_TEMPLATE_DIR ) ) {
                string[] AllGPSTemplates = Directory.GetFiles( FUWUtil.FUWUtil.LOC_TEMPLATE_DIR, "*.xml", SearchOption.AllDirectories );

                foreach( string s in AllGPSTemplates ) {
                    TemplateList.Add( new GPSTemplate() {
                        TemplatePath = s,
                        TemplateName = Path.GetFileName( s )
                    } );
                }
                OrderedTemplateList = TemplateList.OrderBy( tl => tl.TemplateName );
            }

        }
	}

    public class GPSTemplate 
    {
        public string TemplatePath { get; set; }
        public string TemplateName { get; set; }
    }
}