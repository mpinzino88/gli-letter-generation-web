﻿using System.Collections.Generic;

namespace Submissions.Web.Areas.GPSDocuments.Models
{
	public class FlatFileUploadRequest
	{
		public MetaData MetaData { get; set; }
		public List<int> Ids { get; set; }
	}
}