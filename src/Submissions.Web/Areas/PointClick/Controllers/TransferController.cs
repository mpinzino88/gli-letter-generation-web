﻿using AutoMapper;
using EF.GLIAccess;
using GLI.EFCore.Submission;
using Microsoft.EntityFrameworkCore;
using Submissions.Common;
using Submissions.Core.Models;
using Submissions.Web.Areas.PointClick.Models.Transfer;
using Submissions.Web.Areas.PointClick.Services;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace Submissions.Web.Areas.PointClick.Controllers
{
	public class TransferController : Controller
	{
		private readonly SubmissionContext _dbSubmission;
		private readonly IGLIAccessContext _dbGLIAccess;
		private readonly ITransferRequest _transferRequest;
		private readonly IUserContext _userContext;
		private readonly IConfigContext _configContext;

		public TransferController
		(
			SubmissionContext dbSubmission,
			IGLIAccessContext dbGLIAccess,
			ITransferRequest transferRequest,
			IUserContext userContext,
			IConfigContext configContext
		)
		{
			_dbSubmission = dbSubmission;
			_dbGLIAccess = dbGLIAccess;
			_transferRequest = transferRequest;
			_userContext = userContext;
			_configContext = configContext;
		}

		public ActionResult TestMoveFiles(string transferRequestId)
		{
			var transferRequestIdGuid = new Guid(transferRequestId);

			var transferRequest = _dbGLIAccess.TransferRequestHeaders
				.Where(x => x.Id == transferRequestIdGuid)
				.Single();

			var fileNumbersAffected = _dbSubmission.JurisdictionalData
				.Where(x => x.TransferRequestId == transferRequestIdGuid)
				.Select(x => x.Submission.FileNumber)
				.ToHashSet();

			_transferRequest.MoveFiles(transferRequest.Id, (int)transferRequest.SubmissionId, fileNumbersAffected, false, "NJ");

			return new EmptyResult();
		}

		public ActionResult Index()
		{
			var vm = new IndexViewModel();

			if (Directory.Exists(_configContext.PCTUploadPath))
			{
				var filePath = Path.Combine("file://", _configContext.PCTUploadPath);
				vm.AttachmentsLink = new HtmlString(String.Format("<a href='" + filePath + "'>{0}</a>", "Attachments"));
			}

			return View(vm);
		}

		public ActionResult Detail(Guid id)
		{
			SessionMgr.Current.TransferRequestDetailId = id;

			var vm = LoadTransferRequest(id);
			return View(vm);
		}

		public ActionResult DetailEdit()
		{
			return PartialView();
		}

		[HttpPost]
		public JsonResult DetailEdit(DetailEditModel model)
		{
			var transferRequestDetailId = SessionMgr.Current.TransferRequestDetailId;
			var response = _transferRequest.ProcessRequests(transferRequestDetailId, model);

			return Json(response);
		}

		[HttpPost]
		public ActionResult Edit(Guid transferRequestHeaderId, string internalMemo)
		{
			var transferRequestHeader = new TransferRequestHeader
			{
				Id = transferRequestHeaderId,
				InternalMemo = internalMemo,
				InternalEditDate = DateTime.Now,
				InternalEditUserId = _userContext.User.Id
			};

			_dbGLIAccess.TransferRequestHeaders.Attach(transferRequestHeader);
			_dbGLIAccess.Entry(transferRequestHeader).Property(x => x.InternalMemo).IsModified = true;
			_dbGLIAccess.Entry(transferRequestHeader).Property(x => x.InternalEditDate).IsModified = true;
			_dbGLIAccess.Entry(transferRequestHeader).Property(x => x.InternalEditUserId).IsModified = true;
			_dbGLIAccess.SaveChanges();

			return Json("ok");
		}

		[HttpPost]
		public ActionResult Approve(Guid transferRequestHeaderId)
		{
			var transferRequestHeader = _dbGLIAccess.TransferRequestHeaders.Find(transferRequestHeaderId);
			transferRequestHeader.IsConfirmed = true;
			transferRequestHeader.ConfirmDate = DateTime.Now;

			_dbGLIAccess.SaveChanges();

			return Content("ok");
		}

		[HttpPost]
		public JsonResult GetMasterBillingInfo(TransfersToUpdate TransfersToUpdate)
		{
			var rows = new List<string>();
			foreach (var transferRequest in TransfersToUpdate.TransferIds)
			{
				var item = _dbGLIAccess.TransferRequestItems
					.Where(x => x.Id == transferRequest)
					.Include(x => x.Submission)
					.Include(x => x.Jurisdiction)
					.Select(x => new MBIDResultRow
					{
						DateCode = x.Submission.DateCode,
						IdNumber = x.Submission.IdNumber,
						JurisdictionName = x.Jurisdiction.Name,
						TransferId = transferRequest,
						Version = x.Submission.Version,
						JurisdictionId = x.Jurisdiction.Id,
						FileNumber = x.Submission.FileNumber
					})
					.FirstOrDefault();

				var manufacturerCode = _dbSubmission.Submissions.Where(x => x.FileNumber == item.FileNumber).Select(x => x.ManufacturerCode).FirstOrDefault();

				var associationedJurisdictionIds = AssociatedJurisdiction.GetAssociations(item.JurisdictionId, manufacturerCode)
					.Select(x => x.Id)
					.ToList();

				item.MBID =
					_dbSubmission.JurisdictionalData
					.Where(x =>
						((x.JurisdictionId == item.JurisdictionId) || (associationedJurisdictionIds.Contains(x.JurisdictionId))) &&
						x.Submission.FileNumber == item.FileNumber &&
						x.MasterBillingProject != null &&
						x.MasterBillingProject != "")
					.Select(x => x.MasterBillingProject)
					.FirstOrDefault();

				var row = new StringBuilder();
				row.Append("<tr>");
				row.Append("<td name = 'jurisdictionName'>" + item.JurisdictionName + "</td>");
				row.Append("<td name = 'jurisdictionId'>" + item.JurisdictionId + "</td>");
				row.Append("<td name = 'fileNumber'>" + item.FileNumber + "</td>");
				row.Append("<td name = 'idNumber'>" + item.IdNumber + "</td>");
				row.Append("<td name = 'dateCode'>" + item.DateCode + "</td>");
				row.Append("<td name = 'version'>" + item.Version + "</td>");
				row.Append("<td name = 'letterNumber'><input type='text' id='letterNumber." + item.TransferId + "' class='form-control' /></td>");
				row.Append("<td name = 'billingReference'><input type='text' id='billingReference." + item.TransferId + "' class='form-control' /></td>");
				row.Append("<td name = 'contractNumber'><input type='text' id='contractNumber." + item.TransferId + "' class='form-control' /></td>");
				if (!String.IsNullOrWhiteSpace(item.MBID))
				{
					row.Append("<td name = 'MBID'><input type='text' id='MBID." + item.TransferId + "' value='" + item.MBID + "' readonly='readonly' class='form-control' /></td>");
					row.Append("<td><b> * </b></td>");
				}
				else
				{
					row.Append("<td name = 'MBID'><input type='text' id='MBID." + item.TransferId + "' value='" + item.FileNumber + "' class='form-control' /></td>");
					row.Append("<td>&nbsp;</td>");
				}
				row.Append("</tr>");
				rows.Add(row.ToString());
				row.Clear();
			}
			return Json(rows);
		}

		#region Helper Methods
		public DetailViewModel LoadTransferRequest(Guid id)
		{
			var transferRequest = _dbGLIAccess.TransferRequestHeaders.Where(x => x.Id == id).Single();

			var submission = _dbSubmission.Submissions
				.Include(x => x.Company)
				.Include(x => x.ContractType)
				.Include(x => x.Currency)
				.Where(x => x.Id == transferRequest.SubmissionId)
				.Select(x => new
				{
					CompanyId = x.CompanyId,
					Company = x.Company.Name,
					ContractTypeId = x.ContractTypeId,
					ContractType = x.ContractType.Code + " - " + x.ContractType.Description,
					CurrencyId = x.CurrencyId,
					Currency = x.Currency.Code + " - " + x.Currency.Description,
					Lab = x.Lab,
					ManufacturerCode = x.ManufacturerCode
				})
				.Single();

			var defaultLab = _dbSubmission.trLaboratories
				.Where(x => x.Location == submission.Lab)
				.Select(x => new { x.Id, x.Location, x.Name })
				.Single();

			var headerModel = Mapper.Map<DetailHeaderModel>(transferRequest);
			headerModel.ManufacturerCode = submission.ManufacturerCode;
			var status = (TransferRequestHeaderStatus)Enum.Parse(typeof(TransferRequestHeaderStatus), headerModel.Status);
			headerModel.AssociatedJurisdictions = BuildAssociatedJurisdictions(id, status);

			if (transferRequest.InternalEditUserId != null)
			{
				headerModel.InternalEditUser = _dbSubmission.trLogins.Where(x => x.Id == transferRequest.InternalEditUserId).Select(x => x.FName + " " + x.LName).SingleOrDefault();
			}

			var fileCount = _dbGLIAccess.TransferRequestAttachments.Where(x => x.TransferRequestHeaderId == id).Count();
			if (fileCount > 0)
			{
				headerModel.AttachmentsLink = new HtmlString(String.Format("<span class='badge badge-attachment'>{0}</span>", fileCount));
			}
			else
			{
				headerModel.AttachmentsLink = new HtmlString(String.Format("<span class='badge' title='No Attachments Found'>{0}</span>", 0));
			}

			var vm = new DetailViewModel
			{
				DetailHeaderModel = headerModel,
				DetailEditModel = new DetailEditModel
				{
					CompanyId = submission.CompanyId,
					Company = submission.Company,
					LabId = defaultLab.Id,
					Lab = defaultLab.Location + " - " + defaultLab.Name,
					ContractTypeId = submission.ContractTypeId,
					ContractType = submission.ContractType,
					CurrencyId = submission.CurrencyId,
					Currency = submission.Currency,
					StatementAck = Convert.ToBoolean(transferRequest.StatementAck),
					SoftwareAck = Convert.ToBoolean(transferRequest.SoftwareAck)
				}
			};

			vm.TransferRequestDetailGrid.ShowRDAPPins(CustomManufacturerGroups.Aristocrat.Contains(submission.ManufacturerCode));

			return vm;
		}

		private IList<AssociatedJurisdiction> BuildAssociatedJurisdictions(Guid transferRequestHeaderId, TransferRequestHeaderStatus status)
		{
			var associationsFound = new List<AssociatedJurisdiction>();

			if (status == TransferRequestHeaderStatus.Processed)
			{
				return associationsFound;
			}

			var items = _dbGLIAccess.TransferRequestItems
				.Where(x => x.TransferRequestHeaderId == transferRequestHeaderId)
				.Include(x => x.Submission)
				.Include(x => x.Jurisdiction)
				.Select(x => new { JurisdictionId = x.Jurisdiction.Id, ManufacturerCode = x.Submission.Manufacturer })
				.ToList();

			var jurisdictionIds = items.Select(x => x.JurisdictionId).ToList();
			var manufacturerCode = items.Select(x => x.ManufacturerCode).First();

			return AssociatedJurisdiction.GetDistinctAssociations(jurisdictionIds, manufacturerCode);
		}
		#endregion
	}

	public class TransfersToUpdate
	{
		public List<Guid> TransferIds { get; set; }
	}
}