﻿using EF.GLIAccess;
using Submissions.Common;
using Submissions.Web.Areas.PointClick.Models.Withdraw;
using Submissions.Web.Areas.PointClick.Services;
using Submissions.Web.Models.Query;
using System;
using System.Linq;
using System.Web.Mvc;
using Z.EntityFramework.Plus;

namespace Submissions.Web.Areas.PointClick.Controllers
{
	public class WithdrawController : Controller
	{
		private readonly IGLIAccessContext _dbGLIAccess;
		private readonly IWithdrawRequest _withdrawRequest;
		private readonly IUserContext _userContext;

		public WithdrawController
		(
			IGLIAccessContext dbGLIAccess,
			IWithdrawRequest withdrawRequest,
			IUserContext userContext
		)
		{
			_dbGLIAccess = dbGLIAccess;
			_withdrawRequest = withdrawRequest;
			_userContext = userContext;
		}

		public ActionResult Index()
		{
			var vm = new IndexViewModel { };
			return View(vm);
		}

		public ActionResult Detail(Guid id)
		{
			SessionMgr.Current.WithdrawRequestDetailId = id;

			var items = _dbGLIAccess.WithdrawalRequests
				.Where(x => x.RequestId == id)
				.Select(x => x);

			var openItems = items.Where(x => x.ProcessDate == null).Select(x => x);

			var headerInfo = items
				.Select(x => new DetailHeaderModel
				{
					RequestId = id,
					Reason = x.Memo,
					RequestDate = (DateTime)x.RequestDate,
					RequestorName = x.ContactName,
					QAMemo = x.QAMemo != null ? x.QAMemo : "",
					Status = openItems.Count() > 0 ? WithdrawStatus.Pending.ToString() : WithdrawStatus.Processed.ToString()
				})
				.First();

			var vm = new DetailViewModel
			{
				HeaderInfo = headerInfo,
				JurisdictionFilters = new WithdrawRequestSearch { RequestId = id },
				RecordFilters = new WithdrawRequestSearch { RequestId = id },
				DetailEditModel = new DetailEditModel { RequestorMemo = headerInfo.Reason }
			};

			return View(vm);
		}

		public ActionResult DetailEdit()
		{
			return PartialView();
		}

		[HttpPost]
		public JsonResult DetailEdit(DetailEditModel model)
		{
			var withdrawRequestDetailId = SessionMgr.Current.WithdrawRequestDetailId;
			if (model.WithdrawRequestEditAction != null)
			{
				_withdrawRequest.ProcessRequests(withdrawRequestDetailId, model);
				return Json("RequestProcessed");
			}
			else
			{
				if (!String.IsNullOrWhiteSpace(model.QAMemo))
				{
					_dbGLIAccess.WithdrawalRequests
						.Where(x => x.RequestId == withdrawRequestDetailId)
						.Update(x => new WithdrawalRequest
						{
							QAMemo = model.QAMemo,
							QAUserId = _userContext.User.Id
						});
				}
				return Json("MemoAdded");
			}
		}

	}
}