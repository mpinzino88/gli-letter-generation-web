﻿using AutoMapper.QueryableExtensions;
using EF.GLIAccess;
using EF.GPS;
using GLI.EFCore.Submission;
using Submissions.Common;
using Submissions.Common.Email;
using Submissions.Core;
using Submissions.Core.Models.DTO;
using Submissions.Web.Areas.PointClick.Models.Submit;
using Submissions.Web.Utility.Email;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Web.Mvc;
using Z.EntityFramework.Plus;
using GPS = FUWUtil;

namespace Submissions.Web.Areas.PointClick.Controllers
{
	public class SubmitController : Controller
	{
		private readonly SubmissionContext _dbSubmission;
		private readonly IGLIAccessContext _dbGLIAccess;
		private readonly IUserContext _userContext;
		private readonly IConfigContext _configContext;
		private readonly ISubmissionService _submissionService;
		private readonly IEmailService _emailService;
		private readonly INotificationService _notifcationService;

		public SubmitController
		(
			SubmissionContext dbSubmission,
			IGLIAccessContext dbGLIAccess,
			IUserContext userContext,
			IConfigContext configContext,
			ISubmissionService submissionService,
			IEmailService emailService,
			INotificationService notificationService
		)
		{
			_dbSubmission = dbSubmission;
			_dbGLIAccess = dbGLIAccess;
			_userContext = userContext;
			_configContext = configContext;
			_submissionService = submissionService;
			_emailService = emailService;
			_notifcationService = notificationService;
		}

		public ActionResult TestMoveFiles(string confirmId)
		{
			var submitRequest = _dbGLIAccess.electronicSubmissionMasters
				.Where(x => x.ConfirmId == confirmId)
				.Single();

			MoveFiles(submitRequest, submitRequest.FileNumber, "NJ");

			return new EmptyResult();
		}

		public ActionResult Index(int? newSubmissionId)
		{
			var vm = new IndexViewModel
			{
				NewSubmissionId = newSubmissionId
			};

			return View(vm);
		}

		public ActionResult Detail(int id)
		{
			var vm = new DetailViewModel();

			vm.ContractTypeIdsRequireMemo = _dbSubmission.tbl_lst_ContractType
				.Where(x => x.Code == ContractType.FP.ToString() || x.Code == ContractType.FPE.ToString() || x.Code == ContractType.SBP.ToString())
				.Select(x => x.Id)
				.ToList();

			var submitRequest = _dbGLIAccess.electronicSubmissionMasters
				.Where(x => x.Id == id)
				.ProjectTo<SubmitRequestViewModel>()
				.Single();

			var translationHouses = _dbGLIAccess.electronicSubmissionTranslationHouses
				.Where(x => x.ElectronicSubmissionMasterId == id)
				.OrderBy(x => x.TranslationHouse.Description)
				.Select(x => new SelectListItem { Value = x.TranslationHouseId.ToString(), Text = x.TranslationHouse.Description })
				.ToList();

			var shipments = _dbGLIAccess.electronicSubmissionTrackingNums
				.Where(x => x.ConfirmId == submitRequest.ConfirmId)
				.ProjectTo<ShipmentViewModel>()
				.ToList();

			var attachments = _dbGLIAccess.electronicSubmissionAttachments
				.Where(x => x.ConfirmId == submitRequest.ConfirmId && x.ItemNumber == null)
				.Select(x => new AttachmentViewModel
				{
					Id = x.Id,
					AttachmentType = x.AttachmentType,
					FileName = x.FileName
				})
				.ToList();

			if (submitRequest.InternalEditUserId != null)
			{
				submitRequest.InternalEditUser = _dbSubmission.trLogins.Where(x => x.Id == submitRequest.InternalEditUserId).Select(x => x.FName + " " + x.LName).SingleOrDefault();
			}

			var components = _dbGLIAccess.electronicSubmissionItems
				.Where(x => x.ConfirmId == submitRequest.ConfirmId)
				.Select(x => new ComponentViewModel
				{
					Id = x.Id,
					GameName = x.GameName,
					IdNumber = x.IdNumber,
					ItemNumber = (int)x.ItemNumber,
					OrgIGTMaterial = x.OrgIGTMaterial,
					Position = x.Position,
					Signature = x.Signature,
					SignatureScope = x.SignatureScope,
					SignatureType = x.SignatureType,
					Version = x.Version,
					Jurisdictions = _dbGLIAccess.ElectronicSubmissionJurs
						.Join(_dbGLIAccess.tbl_lst_fileJuris, j => j.JurisdictionId, fj => (int)fj.Fix_Id, (j, fj) => new { Jurisdiction = j, FileJurisdiction = fj })
						.Where(y => y.Jurisdiction.ConfirmId == submitRequest.ConfirmId && y.Jurisdiction.ItemNumber == x.ItemNumber)
						.Select(y => new JurisdictionViewModel
						{
							Id = y.Jurisdiction.Id,
							JurisdictionId = y.FileJurisdiction.Id,
							Name = y.FileJurisdiction.Name
						})
						.ToList(),
					Attachments = _dbGLIAccess.electronicSubmissionAttachments
						.Where(a => a.ConfirmId == submitRequest.ConfirmId && a.IdNumber == x.IdNumber && x.ItemNumber != null)
						.Select(a => new AttachmentViewModel
						{
							Id = a.Id,
							AttachmentType = a.AttachmentType,
							FileName = a.FileName
						})
						.ToList()
				})
				.ToList();

			if (submitRequest.Status == SubmitRequestHeaderStatus.Pending.ToString())
			{
				var idNumbers = components.Select(x => x.IdNumber).ToList();
				var submissionsWithIdNumbers = _dbSubmission.Submissions
					.Where(x => x.ManufacturerCode == submitRequest.ManufacturerCode && x.IdNumber != null && idNumbers.Contains(x.IdNumber))
					.Select(x => new { x.Id, x.IdNumber })
					.Distinct()
					.ToList();

				if (submissionsWithIdNumbers.Count > 0)
				{
					foreach (var item in components)
					{
						var possibleDuplicateSubmissionId = submissionsWithIdNumbers.Where(x => x.IdNumber == item.IdNumber).Select(x => x.Id).FirstOrDefault();
						item.PossibleDuplicateSubmissionId = possibleDuplicateSubmissionId == 0 ? (int?)null : possibleDuplicateSubmissionId;
					}
				}
			}

			submitRequest.TranslationHouses = translationHouses.Select(x => x.Text).ToList();
			submitRequest.Shipments = shipments;
			submitRequest.Attachments = attachments;
			submitRequest.Components = components;

			vm.SubmitRequest = submitRequest;

			// accept defaults
			var defaults = _dbSubmission.trLaboratories
				.Where(x => x.Id == _userContext.LocationId)
				.Select(x => new
				{
					x.CompanyId,
					CompanyName = x.Company.Name,
					x.Company.CurrencyId,
					Currency = x.Company.Currency.Code + " - " + x.Company.Currency.Description,
					LabId = x.Id,
					LabLocation = x.Location,
					LabName = x.Name
				})
				.Single();

			vm.Accept.CompanyId = defaults.CompanyId;
			vm.Accept.Company = defaults.CompanyName;
			vm.Accept.CertificationLabId = defaults.LabId;
			vm.Accept.CertificationLab = defaults.LabLocation + " - " + defaults.LabName;
			vm.Accept.CurrencyId = defaults.CurrencyId;
			vm.Accept.Currency = defaults.Currency;
			vm.Accept.LabId = defaults.LabId;
			vm.Accept.Lab = defaults.LabLocation + " - " + defaults.LabName;
			vm.Accept.JurisdictionFilter.JurisdictionIds = components.SelectMany(x => x.Jurisdictions).Select(x => x.JurisdictionId).Distinct().ToList();
			vm.Accept.TranslationHouseIds = translationHouses.Select(x => x.Value).ToArray();
			vm.Accept.TranslationHouses = translationHouses;

			if (vm.SubmitRequest.PreCertified)
			{
				var submissionType = _dbSubmission.tbl_lst_SubmissionType
					.Where(x => x.Code == SubmissionType.PC.ToString())
					.Select(x => new { x.Id, Type = x.Code + " - " + x.Description })
					.Single();

				vm.Accept.TypeId = submissionType.Id;
				vm.Accept.Type = submissionType.Type;
			}

			CustomManufacturer customManufacturer;
			if (Enum.TryParse(submitRequest.ManufacturerCode, out customManufacturer))
			{
				vm.ShowGameSubmissionType = ComUtil.GameSubmissionTypeManufacturers().Any(x => x == (CustomManufacturer)Enum.Parse(typeof(CustomManufacturer), submitRequest.ManufacturerCode));
			}

			return View(vm);
		}

		[HttpPost]
		public ActionResult Edit(int submitRequestId, string internalMemo)
		{
			var submitRequest = new electronicSubmissionMaster
			{
				Id = submitRequestId,
				InternalMemo = internalMemo,
				InternalEditDate = DateTime.Now,
				InternalEditUserId = _userContext.User.Id
			};

			_dbGLIAccess.electronicSubmissionMasters.Attach(submitRequest);
			_dbGLIAccess.Entry(submitRequest).Property(x => x.InternalMemo).IsModified = true;
			_dbGLIAccess.Entry(submitRequest).Property(x => x.InternalEditDate).IsModified = true;
			_dbGLIAccess.Entry(submitRequest).Property(x => x.InternalEditUserId).IsModified = true;
			_dbGLIAccess.Configuration.ValidateOnSaveEnabled = false;
			_dbGLIAccess.SaveChanges();

			return Json("ok");
		}

		[HttpPost]
		public ActionResult Accept(int submitRequestId, [Bind(Prefix = "Accept")] AcceptViewModel vm)
		{
			// defaults
			var today = DateTime.Now.Date;

			var archiveLocation = _dbSubmission.trLaboratories
				.Where(x => x.Id == vm.LabId)
				.Select(x => x.ArchiveLocation)
				.Single();

			var testingPerformed = TestingPerformed.FullTesting.GetEnumDescription();
			var testingPerformedId = _dbSubmission.tbl_lst_TestingPerformed
				.Where(x => x.Code == testingPerformed)
				.Select(x => x.Id)
				.Single();

			var won = vm.WON;
			if (won != null && won.IndexOf("-") > 0)
			{
				won = new FileNumber(won).AccountingFormatNoDashes;
			}

			// add new submission and project
			var submitRequest = _dbGLIAccess.electronicSubmissionMasters.Find(submitRequestId);
			var translationHouseIds = _dbGLIAccess.electronicSubmissionTranslationHouses
				.Where(x => x.ElectronicSubmissionMasterId == submitRequest.Id)
				.Select(x => x.TranslationHouseId)
				.ToList();

			var functionId = 0;
			var functionCode = "";
			if (submitRequest.TestingType.Description == TestingType.iGaming.ToString())
			{
				functionCode = Function.PersonalityPGM.GetEnumDescription();
			}
			else if (submitRequest.TestingType.Description == TestingType.Sportsbook.ToString())
			{
				functionCode = Function.Other.GetEnumDescription();
			}

			if (!string.IsNullOrEmpty(functionCode))
			{
				functionId = _dbSubmission.tbl_lst_EPROMFunction
					.Where(x => x.Code == functionCode)
					.Select(x => x.Id)
					.Single();
			}

			var isIGT = LoadManufacturerCodesByGroup(CustomManufacturerGroup.IGT, "FlatFile").Contains(submitRequest.ManufacturerCode);

			var memos = new List<MemoDTO>();
			memos.Add(new MemoDTO { Memo = BuildMemo(submitRequest) });

			if (!string.IsNullOrEmpty(vm.Memo))
			{
				memos.Add(new MemoDTO { Memo = vm.Memo });
			}

			var components = _dbGLIAccess.electronicSubmissionItems
				.Where(x => x.ConfirmId == submitRequest.ConfirmId)
				.Select(x => new
				{
					x.Id,
					x.GameName,
					x.IdNumber,
					ItemNumber = (int)x.ItemNumber,
					x.OrgIGTMaterial,
					x.Position,
					x.Signature,
					x.SignatureScope,
					x.SignatureType,
					x.Version,
					Jurisdictions = _dbGLIAccess.ElectronicSubmissionJurs
						.Join(_dbGLIAccess.tbl_lst_fileJuris, j => j.JurisdictionId, fj => (int)fj.Fix_Id, (j, fj) => new { Jurisdiction = j, FileJurisdiction = fj })
						.Where(y => y.Jurisdiction.ConfirmId == submitRequest.ConfirmId && y.Jurisdiction.ItemNumber == x.ItemNumber)
						.Select(y => new
						{
							y.Jurisdiction.Id,
							JurisdictionId = y.FileJurisdiction.Id,
							y.FileJurisdiction.Name
						})
						.ToList()
				})
				.ToList();

			var submissions = components
				.Select(x => new SubmissionDTO
				{
					CertificationLabId = (int)vm.CertificationLabId,
					CompanyId = (int)vm.CompanyId,
					ContractNumber = vm.ContractNumber,
					ContractTypeId = (int)vm.ContractTypeId,
					ContractValue = vm.ContractValue,
					CurrencyId = (int)vm.CurrencyId,
					FunctionId = functionId,
					GameName = x.GameName,
					JurisdictionalData = x.Jurisdictions.Select(y => new JurisdictionalDataDTO
					{
						CompanyId = (int)vm.CompanyId,
						ContractNumber = vm.ContractNumber,
						CurrencyId = (int)vm.CurrencyId,
						ContractTypeId = (int)vm.ContractTypeId,
						ElectRequest = true,
						JurisdictionId = y.JurisdictionId,
						JurisdictionName = y.Name,
						LabId = (int)vm.LabId,
						RequestDate = submitRequest.SubmitDate,
						ReceiveDate = today,
						SubmitRequestId = submitRequestId,
						WON = won
					}).ToList(),
					IdNumber = x.IdNumber,
					LabId = (int)vm.LabId,
					ManufacturerBuildId = submitRequest.ManufacturerBuildId,
					ManufacturerCode = submitRequest.ManufacturerCode,
					Memos = memos,
					OrgIGTMaterial = x.OrgIGTMaterial,
					Position = x.Position,
					ReceiveDate = today,
					StudioId = submitRequest.StudioId,
					SubmitDate = submitRequest.SubmitDate,
					TestingPerformedId = testingPerformedId,
					TestingTypeId = submitRequest.TestingTypeId,
					Version = x.Version,
					Year = today.Year.ToString()
				})
				.ToList();

			var project = new ProjectDTO
			{
				FTPLocation = submitRequest.FTPLocation,
				GameSubmissionTypeId = submitRequest.GameSubmissionTypeId,
				GameSubmissionTypeInfo = submitRequest.GameSubmissionTypeInfo,
				GoLiveDate = submitRequest.GoLiveDate,
				TestingEnvironment = submitRequest.TestingEnvironment,
				TranslationHouseIds = vm.TranslationHouseIds == null ? new List<int>() : vm.TranslationHouseIds.Select(int.Parse).ToList()
			};

			var submissionHeader = new SubmissionHeaderDTO
			{
				ManufacturerCode = submitRequest.ManufacturerCode,
				JurisdictionId = vm.JurisdictionId,
				Project = project,
				Source = SubmissionSource.PCS,
				Type = vm.Type.Substring(0, 2),
				Year = today.Year.ToString(),
				Submissions = submissions
			};

			var result = _submissionService.Add(submissionHeader);
			var submissionId = result.Submissions.First().Id;
			var newFileNumber = result.Submissions.First().FileNumber;

			// add shipments
			var shipments = _dbGLIAccess.electronicSubmissionTrackingNums
				.Where(x => x.ConfirmId == submitRequest.ConfirmId)
				.ToList();

			if (shipments.Count > 0)
			{
				try
				{
					_dbSubmission.ChangeTracker.AutoDetectChangesEnabled = false;

					foreach (var shipment in shipments)
					{
						var newShipment = new ShippingData();
						newShipment.FileNumber = newFileNumber;
						newShipment.Type = ShipmentType.Shipping.ToString();
						newShipment.ShippingRecipientCode = ShipmentRecipientCode.Manuf_NJ.ToString();
						newShipment.Courier = shipment.Shipper;
						newShipment.TrackingNumber = shipment.TrackingNumber;
						newShipment.ProcessDate = today.ToString();

						_dbSubmission.ShippingData.Add(newShipment);
					}

					_dbSubmission.SaveChanges();

				}
				finally
				{
					_dbSubmission.ChangeTracker.AutoDetectChangesEnabled = true;
				}
			}

			// update submit request jurisdictions
			_dbGLIAccess.ElectronicSubmissionJurs
				.Where(x => x.ConfirmId == submitRequest.ConfirmId)
				.Update(x => new ElectronicSubmissionJur { submissionStatus = SubmitRequestHeaderStatus.Accepted.ToString(), statusLastUpdated = today });

			// update submit request
			submitRequest.InternalEditUserId = _userContext.User.Id;
			submitRequest.InternalEditDate = DateTime.Now;
			submitRequest.FileNumber = newFileNumber;
			submitRequest.QAMemo = vm.Memo;
			submitRequest.ProcessDate = today;
			submitRequest.Status = SubmitRequestHeaderStatus.Accepted.ToString();
			_dbGLIAccess.SaveChanges();

			// email accounting for fixed price contract types
			var contractTypeCode = _dbSubmission.tbl_lst_ContractType.Where(x => x.Id == vm.ContractTypeId).Select(x => x.Code).Single();
			if (contractTypeCode == ContractType.FP.ToString() || contractTypeCode == ContractType.FPE.ToString())
			{
				var email = new AccountingNewProjectEmail
				{
					FileNumber = newFileNumber,
					Company = vm.Company,
					ContractType = vm.ContractType,
					Currency = vm.Currency,
					WON = vm.WON,
					ContractNumber = vm.ContractNumber,
					Memo = vm.Memo ?? ""
				};

				EmailAccounting(email);
			}

			// move files
			MoveFiles(submitRequest, newFileNumber, archiveLocation);

			return RedirectToAction("index", new { newSubmissionId = submissionId });
		}

		[HttpPost]
		public ActionResult Reject(int submitRequestId, string confirmId, [Bind(Prefix = "Reject")] RejectViewModel vm)
		{
			var today = DateTime.Now.Date;

			// update submit request jurisdictions
			_dbGLIAccess.ElectronicSubmissionJurs
				.Where(x => x.ConfirmId == confirmId)
				.Update(x => new ElectronicSubmissionJur { submissionStatus = SubmitRequestHeaderStatus.Rejected.ToString(), statusLastUpdated = today });

			// update submit request
			var dbSubmitRequest = new electronicSubmissionMaster
			{
				Id = submitRequestId,
				InternalEditUserId = _userContext.User.Id,
				InternalEditDate = DateTime.Now,
				Status = SubmitRequestHeaderStatus.Rejected.ToString(),
				QAMemo = vm.Reason,
				ProcessDate = DateTime.Now
			};

			_dbGLIAccess.electronicSubmissionMasters.Attach(dbSubmitRequest);
			_dbGLIAccess.Entry(dbSubmitRequest).Property(x => x.InternalEditUserId).IsModified = true;
			_dbGLIAccess.Entry(dbSubmitRequest).Property(x => x.InternalEditDate).IsModified = true;
			_dbGLIAccess.Entry(dbSubmitRequest).Property(x => x.Status).IsModified = true;
			_dbGLIAccess.Entry(dbSubmitRequest).Property(x => x.QAMemo).IsModified = true;
			_dbGLIAccess.Entry(dbSubmitRequest).Property(x => x.ProcessDate).IsModified = true;
			_dbGLIAccess.Configuration.ValidateOnSaveEnabled = false;
			_dbGLIAccess.SaveChanges();

			return RedirectToAction("index");
		}

		#region Helper Methods
		private string BuildMemo(electronicSubmissionMaster submitRequest)
		{
			var sb = new StringBuilder();
			sb.Append("PCS<br />");
			sb.Append("Confirmation ID: " + submitRequest.ConfirmId + "<br />");
			sb.Append("Request Date: " + submitRequest.SubmitDate.ToShortDateString() + "<br />");

			if (!String.IsNullOrWhiteSpace(submitRequest.ReferenceNumber))
			{
				sb.Append("Reference Number: " + submitRequest.ReferenceNumber + "<br />");
			}

			sb.Append("Manufacturer Memo: " + submitRequest.Memo + "<br /><br />");
			sb.Append("Contact Information:<br />");
			sb.Append(submitRequest.ContactName + "<br /");
			sb.Append(submitRequest.ContactEmail + "<br />");

			return sb.ToString();
		}

		private void EmailAccounting(AccountingNewProjectEmail email)
		{
			var mailMsg = new MailMessage();
			mailMsg.From = new MailAddress("noreply@gaminglabs.com");
			mailMsg.Subject = "Submission Request - Accounting - " + email.FileNumber;
			mailMsg.Body = _emailService.RenderTemplate(Common.Email.EmailType.AccountingNewProject.ToString(), email);

			// TODO: email distribution lists need to account for company so that we can tie this to EmailType.AccountingNewProject
			if (email.Company == Company.GLIUK.GetEnumDescription())
			{
				mailMsg.To.Add("o.jones@gaminglabs.com");
			}
			else
			{
				var emailAddresses = _notifcationService.GetEmailToAddresses(Common.Email.EmailType.AccountingNewProject).ToList();
				foreach (var emailAddress in emailAddresses)
				{
					mailMsg.To.Add(emailAddress);
				}
			}

			_emailService.Send(mailMsg);
		}

		private IList<string> LoadManufacturerCodesByGroup(CustomManufacturerGroup manufacturerGroup, string groupCategory)
		{
			return _dbSubmission.tbl_lst_fileManuManufacturerGroups
				.Where(x => x.ManufacturerGroup.Description == manufacturerGroup.ToString() && x.ManufacturerGroup.ManufacturerGroupCategory.Code == groupCategory)
				.Select(x => x.ManufacturerCode)
				.ToList();
		}

		private void MoveFiles(electronicSubmissionMaster submitRequest, string newFileNumber, string archiveLocation)
		{
			if (_userContext.User.Environment == Common.Environment.Production)
			{
				var attachments = _dbGLIAccess.electronicSubmissionAttachments
					.Where(x => x.ConfirmId == submitRequest.ConfirmId)
					.Select(x => new { x.FileName, x.AttachmentType })
					.ToList();

				//source example: \\nj_esquarantine\submissionUploads\IGT\97B7D30BF1
				//dest example: \\gaminglabs\glimain\Public\NJ\FTP Submissions\fromWebsite\IGT\MO-166-IGT-18-04
				//gps example: \\gaminglabs.net\glimain\Public\NJ\Submissions\2019\INT\MO-073-INT-19-014\Manufacturer Submitted\00 Entire Submission Contents
				var sourcePath = Path.Combine(_configContext.PCSUploadPath, submitRequest.ManufacturerCode, submitRequest.ConfirmId);
				var destPath = Path.Combine(_configContext.PCSAttachmentPath, submitRequest.ManufacturerCode, newFileNumber);

				var impersonateUsername = _configContext.MonitorAccountUsername;
				var impersonatePassword = _configContext.MonitorAccountPassword;

				FileInfo[] files;

				//Move Network Share Files
				using (var impersonatedUser = new ImpersonateUser(Domain.GamingLabs, impersonateUsername, impersonatePassword))
				{
					if (!Directory.Exists(sourcePath)) { return; }
					if (!Directory.Exists(destPath)) { Directory.CreateDirectory(destPath); }

					files = new DirectoryInfo(sourcePath).GetFiles();

					foreach (var file in files)
					{
						System.IO.File.Copy(file.FullName, Path.Combine(destPath, file.Name), true);
					}
				}

				//GPS the Files
				var gpsFileNum = new GPS.FileNumber(newFileNumber);
				var root = GPS.FUWDirTree.GenerateFUWTree(gpsFileNum, true, ref archiveLocation, false, false, true, true, true, false);
				var dirOfficialRequests = root.DescendantOrSelf(x => x.Name.Contains(GPSDirName.OfficialRequests.GetEnumDescription()));
				var dirEntireSubmissionContents = root.DescendantOrSelf(x => x.Name == GPSDirName.EntireSubmissionContents.GetEnumDescription());

				foreach (var file in files)
				{
					var gpsFileInfo = new GPS.GPSFileInfo(file.FullName) { Action = GPS.ItemAction.UPLOADNEW };
					var documentType = attachments.Where(x => x.FileName == file.Name).Select(x => x.AttachmentType).SingleOrDefault();
					if (string.Equals(documentType, FileUploadType.Letter.ToString(), StringComparison.OrdinalIgnoreCase))
					{
						dirOfficialRequests.EntriesToProcess.Add(gpsFileInfo);
					}
					else
					{
						dirEntireSubmissionContents.EntriesToProcess.Add(gpsFileInfo);
					}
				}

				var gpsLabId = FUWUtil.FUWUtil.Laboratories.FirstOrDefault(lab => lab.Location == archiveLocation || lab.ArchiveLocation == archiveLocation).ID;
				dirOfficialRequests.ProcessAll(gpsLabId);
				dirEntireSubmissionContents.ProcessAll(gpsLabId);

				//Delete Source Folder once all of the Requests have been Processed
				using (var impersonatedUser = new ImpersonateUser(Domain.GamingLabs, impersonateUsername, impersonatePassword))
				{
					try
					{
						Directory.Delete(sourcePath, true);
					}
					catch (IOException)
					{
						//Ignore this exception.  Possibly not released properly in GPS.
						//The process cannot access the file 'fileName' because it is being used by another process.
					}
				}
			}
		}
		#endregion
	}
}