﻿extern alias ZEFCore;
using EF.GLIAccess;
using EF.GPS;
using GLI.EFCore.Submission;
using Submissions.Core;
using Submissions.Web.Areas.PointClick.Models.Transfer;
using Submissions.Web.Utility.Email;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Text;
using ZEFCore::Z.EntityFramework.Plus;
using GPS = FUWUtil;

namespace Submissions.Web.Areas.PointClick.Services
{
	using Submissions.Common;
	using Submissions.Common.Email;
	using Submissions.Core.Models;

	public class TransferRequest : ITransferRequest
	{
		private readonly SubmissionContext _dbSubmission;
		private readonly IGLIAccessContext _dbGLIAccess;
		private readonly IEmailService _emailService;
		private readonly IUserContext _userContext;
		private readonly IJurisdictionalDataService _jurisdictionalDataService;
		private readonly IConfigContext _configContext;
		private readonly IProjectService _projectService;

		private DetailEditModel _editModel;
		private bool _batchHasAcceptedRequests = false, _batchHasPendingRequests = false;
		private EF.GLIAccess.TransferRequestHeader _transferRequestHeader;
		private List<TransferRequestItem> _transferRequestItemsToUpdate;
		private List<KeyValuePair<int, string>> _excludeAssociatedJurisdictions;
		private HashSet<string> _fileNumbersAffected = new HashSet<string>();
		private List<PointClickTransferEmail> _allEmailJurisdictions = new List<PointClickTransferEmail>();
		private StringBuilder _responseMsg = new StringBuilder();
		private List<GLI.EFCore.Submission.JurisdictionalData> _processedJurisdictionalData = new List<GLI.EFCore.Submission.JurisdictionalData>();

		public TransferRequest
		(
			SubmissionContext dbSubmission,
			IGLIAccessContext dbGLIAccess,
			IUserContext userContext,
			IJurisdictionalDataService jurisdictionalDataService,
			IConfigContext configContext,
			IEmailService emailService,
			IProjectService projectService
		)
		{
			_dbSubmission = dbSubmission;
			_dbGLIAccess = dbGLIAccess;
			_userContext = userContext;
			_jurisdictionalDataService = jurisdictionalDataService;
			_configContext = configContext;
			_emailService = emailService;
			_projectService = projectService;
		}

		public ProcessResponse ProcessRequests(Guid id, DetailEditModel editModel)
		{
			_editModel = editModel;
			LoadTransferRequest(id);

			if (_editModel.TransferRequestStatus == TransferRequestStatus.Accepted.ToString())
			{
				CreateJurisdictions();
			}
			else
			{
				DeclineTransferRequestItems();
			}

			var archiveLocation = _dbSubmission.trLaboratories
				.Where(x => x.Id == editModel.LabId)
				.Select(x => x.ArchiveLocation)
				.Single();

			var pendingStatus = TransferRequestStatus.Pending.ToString();
			_batchHasPendingRequests = (from x in _dbGLIAccess.TransferRequestItems
										where x.TransferRequestHeaderId == id && x.Status == pendingStatus
										select x).Any();

			var acceptedStatus = TransferRequestStatus.Accepted.ToString();
			_batchHasAcceptedRequests = (from x in _dbGLIAccess.TransferRequestItems
										 where x.TransferRequestHeaderId == id && x.Status == acceptedStatus
										 select x).Any();

			AddSubmissionMemos();
			UpdateTransferRequestHeader();
			SendEmails();
			MoveFiles(_transferRequestHeader.Id, (int)_transferRequestHeader.SubmissionId, _fileNumbersAffected, _batchHasPendingRequests, archiveLocation);

			var response = BuildResponse();

			return response;
		}

		private void LoadTransferRequest(Guid transferRequestHeaderId)
		{
			_transferRequestHeader = _dbGLIAccess.TransferRequestHeaders.Find(transferRequestHeaderId);
			_excludeAssociatedJurisdictions = _transferRequestHeader.TransferRequestItems
				.Select(x => new KeyValuePair<int, string>((int)x.SubmissionId, x.JurisdictionId))
				.ToList();

			var transfersToUpdateIds = _editModel.TransfersToUpdate.Select(x => x.Id).ToList();
			_transferRequestItemsToUpdate = (from x in _dbGLIAccess.TransferRequestItems
											 where transfersToUpdateIds.Contains(x.Id)
											 select x).ToList();
		}

		#region JurisdictionalData Creation
		private void CreateJurisdictions()
		{
			//Add or Update Jurisdiction
			_jurisdictionalDataService.ExcludeAssociatedJurisdictions = _excludeAssociatedJurisdictions;

			var juriIdModified = _editModel.JurisdictionId.ToJurisdictionIdString();
			var juriTxtModified = "Jurisdiction" + juriIdModified;
			var juriNameModified = _editModel.Jurisdiction;
			string juriId, juriTxt, juriName;
			var processedProjectJuriData = new List<Tuple<int, IList<GLI.EFCore.Submission.JurisdictionalData>>>();

			foreach (var x in _transferRequestItemsToUpdate)
			{
				var submission = _dbSubmission.Submissions.Find(x.SubmissionId);

				// auto-fill RDAP Pin for Aristocrat
				string clientNumber = null;
				if (CustomManufacturerGroups.Aristocrat.Contains(submission.ManufacturerCode))
				{
					clientNumber = ComUtil.GetRDAP(submission.Function) ?? x.ClientNumber;

					if (clientNumber == null)
					{
						clientNumber = _dbSubmission.JurisdictionalData
							.Where(y => y.Submission.SubmissionHeaderId == submission.SubmissionHeaderId && y.ClientNumber != null)
							.Select(y => y.ClientNumber)
							.FirstOrDefault();
					}
				}

				//Non-transfers will not be added to a transfer project
				bool isTransfer = !(
					submission.Status == SubmissionStatus.CP.ToString() ||
					submission.Status == SubmissionStatus.OH.ToString() ||
					submission.Status == SubmissionStatus.PN.ToString());

				if (string.IsNullOrEmpty(juriNameModified))
				{
					juriId = x.JurisdictionId;
					juriTxt = "Jurisdiction" + x.JurisdictionId;
					juriName = x.Jurisdiction.Name;
				}
				else
				{
					juriId = juriIdModified;
					juriTxt = juriTxtModified;
					juriName = juriNameModified;
				}

				var jurisdiction = (from s in _dbSubmission.JurisdictionalData
									where s.SubmissionId == x.SubmissionId && s.Txt == juriTxt
									select s).SingleOrDefault();

				bool juriExists = false, processJuri = true;

				if (jurisdiction == null)
				{
					jurisdiction = new GLI.EFCore.Submission.JurisdictionalData();
				}
				else
				{
					//If Jurisdiction already exists, it should have a withdrawn status in which case we'll update it
					//and set the values as if it was newly created, otherwise don't process the record
					juriExists = true;
					if (jurisdiction.Status != JurisdictionStatus.WD.ToString() && jurisdiction.Status != JurisdictionStatus.RJ.ToString())
					{
						processJuri = false;
						_responseMsg.AppendLine("File Number: " + submission.FileNumber + " - " + juriName + " already exists and was not processed.<br>");
					}
				}

				if (processJuri)
				{
					jurisdiction.SubmissionId = x.SubmissionId;
					jurisdiction.JurisdictionId = juriId;
					jurisdiction.Txt = juriTxt;
					jurisdiction.JurisdictionName = juriName;
					jurisdiction.RequestDate = Convert.ToDateTime(x.TransferRequestHeader.RequestDate).Date;
					jurisdiction.ReceiveDate = DateTime.Now.Date;
					jurisdiction.Status = JurisdictionStatus.RA.ToString();
					jurisdiction.LabId = _editModel.LabId;
					jurisdiction.CompanyId = _editModel.CompanyId;
					jurisdiction.ContractTypeId = _editModel.ContractTypeId;
					jurisdiction.CurrencyId = _editModel.CurrencyId;
					jurisdiction.TestingPerformedId = 1;
					jurisdiction.Restricted = 0;
					jurisdiction.Active = "No";
					jurisdiction.ElectRequest = true;
					jurisdiction.AddDate = DateTime.Now;
					jurisdiction.PDFSent = 0;
					jurisdiction.PDFSentSpanish = 0;
					jurisdiction.SpecialNote = false;
					jurisdiction.PriorityDraft = false;
					jurisdiction.Submission = submission;
					jurisdiction.ClientNumber = clientNumber;
					jurisdiction.CloseDate = null;
					jurisdiction.RJWDReason = null;
					jurisdiction.ProposedTargetDate = null;
					jurisdiction.TargetDate = null;
					jurisdiction.TargetDateNote = null;
					jurisdiction.TransferAdded = DateTime.Now;
					jurisdiction.TransferAvailable = isTransfer;
					jurisdiction.TransferCreated = false;
					jurisdiction.TransferRequestId = _transferRequestHeader.Id;

					if (_editModel.MBIDResultRows != null)
					{
						var mbRow = _editModel.MBIDResultRows
							.Where(y => y.TransferId == x.Id)
							.Select(y => new { y.LetterNumber, y.BillingReference, y.ContractNumber, y.MBID })
							.FirstOrDefault();

						if (mbRow != null)
						{
							jurisdiction.LetterNumber = mbRow.LetterNumber;
							jurisdiction.ClientNumber = clientNumber ?? mbRow.BillingReference;
							jurisdiction.ContractNumber = mbRow.ContractNumber;
							jurisdiction.MasterBillingProject = mbRow.MBID;
						}

						CascadeMasterBillingId(jurisdiction, x.Submission);
					}

					var lastProcessedJurisdictionalData = new List<GLI.EFCore.Submission.JurisdictionalData>();

					if (juriExists)
					{
						_jurisdictionalDataService.Update(jurisdiction);
						_processedJurisdictionalData.Add(jurisdiction);
						lastProcessedJurisdictionalData.Add(jurisdiction);

						// remove any pre-existing transferProjectJur link records
						_dbSubmission.trProjectJurisdictionalData.Where(u => u.JurisdictionalDataId == jurisdiction.Id).Delete();
					}
					else
					{
						var affectedJurisdictionalData = _jurisdictionalDataService.Add(jurisdiction);
						_processedJurisdictionalData.AddRange(affectedJurisdictionalData);
						lastProcessedJurisdictionalData.AddRange(affectedJurisdictionalData);
					}

					var emailJurisdictions = lastProcessedJurisdictionalData
						.Select(j => new PointClickTransferEmail()
						{
							FileNumber = j.Submission.FileNumber,
							Jurisdiction = j.JurisdictionName + "(" + j.JurisdictionId + ")",
							Manufacturer = j.Submission.Manufacturer.Code,
							Status = j.Status,
							Processed = (DateTime)j.TransferAdded,
							Requested = j.RequestDate
						})
						.ToList();

					_allEmailJurisdictions.AddRange(emailJurisdictions);


					var projectId = _dbSubmission.trProjects
							.Where(y => y.FileNumber == submission.FileNumber && y.IsTransfer == false)
							.Select(y => y.Id)
							.SingleOrDefault();

					// add project jurisdictional data records for non-transfers
					if (!isTransfer)
					{


						foreach (var jurisdictionalData in lastProcessedJurisdictionalData)
						{
							var projectJurisdictionalData = new GLI.EFCore.Submission.trProjectJurisdictionalData
							{
								JurisdictionalDataId = jurisdictionalData.Id,
								ProjectId = projectId
							};

							_projectService.AddProjectJurisdictions(new List<GLI.EFCore.Submission.trProjectJurisdictionalData> { projectJurisdictionalData });
						}

						_dbSubmission.SaveChanges();

						//Send an email to Engineers
						var mailMsg = new MailMessage();
						mailMsg.From = new MailAddress("noreply@gaminglabs.com");
						mailMsg.Subject = "Point Click Transfer Processed for " + submission.FileNumber;
						mailMsg.Body = _emailService.RenderTemplate(Submissions.Common.Email.EmailType.PointClickTransfer.ToString(), emailJurisdictions);

						var taskUsers = _dbSubmission.trTasks
							.Where(t => t.ProjectId == projectId && t.User != null)
							.Select(t => t.User.Email)
							.Distinct()
							.ToList();

						if (taskUsers.Count > 0)
						{
							var recipients = String.Join(",", taskUsers);
							mailMsg.To.Add(recipients);
						}

						var manager = _dbSubmission.trProjects.Where(t => t.Id == projectId).Select(t => t.ENManager.Email).SingleOrDefault();
						if (manager != null)
						{
							mailMsg.To.Add(manager);
						}

						if (mailMsg.To.Count > 0)
						{
							_emailService.Send(mailMsg);
						}
					}
					//processedProjectJuriData.Add(Tuple.Create<int, IList<GLI.EFCore.Submission.JurisdictionalData>>(projectId, lastProcessedJurisdictionalData));

					UpdateTransferRequestItem(x);

					_fileNumbersAffected.Add(submission.FileNumber);
				}
			}
			//_projectService.TransferProcessedEmails(processedProjectJuriData);
		}
		#endregion

		#region Transfer Request Updates
		private void DeclineTransferRequestItems()
		{
			foreach (var item in _transferRequestItemsToUpdate)
			{
				UpdateTransferRequestItem(item);
			}
		}

		private void UpdateTransferRequestItem(TransferRequestItem item)
		{
			item.Status = Enum.Parse(typeof(TransferRequestStatus), _editModel.TransferRequestStatusId).ToString();
			item.TfSendSupEmail = true;
			_dbGLIAccess.Entry(item).Property(x => x.Status).IsModified = true;
			_dbGLIAccess.Entry(item).Property(x => x.TfSendSupEmail).IsModified = true;
			_dbGLIAccess.SaveChanges();
		}

		private void UpdateTransferRequestHeader()
		{
			var trh = _dbGLIAccess.TransferRequestHeaders.Find(_transferRequestHeader.Id);
			trh.InternalEditUserId = _userContext.User.Id;
			trh.InternalEditDate = DateTime.Now;

			if (!_batchHasPendingRequests)
			{
				trh.Status = TransferRequestHeaderStatus.Processed.ToString();
			}

			_dbGLIAccess.SaveChanges();
		}
		#endregion

		#region Update Submissions
		private void AddSubmissionMemos()
		{
			// Exit if there are no notes to be added
			if (String.IsNullOrWhiteSpace(_transferRequestHeader.Memo) && String.IsNullOrWhiteSpace(_editModel.Memo))
			{
				return;
			}

			// Accepted Status
			if (_editModel.TransferRequestStatus == TransferRequestStatus.Accepted.ToString())
			{
				// Group Jurisdictions by Submission
				var submissions = (from x in _processedJurisdictionalData
								   group x by x.SubmissionId into submissionGrp
								   select new
								   {
									   SubmissionId = submissionGrp.Key,
									   Jurisdictions = submissionGrp.Select(y => new { Jurisdiction = y }).ToList()
								   }).ToList();

				// Create Memos per Submission
				foreach (var submission in submissions)
				{
					// Add Manufacturer Memo
					if (!String.IsNullOrWhiteSpace(_transferRequestHeader.Memo))
					{
						var jurisdictionNames = submission.Jurisdictions.Select(x => x.Jurisdiction.JurisdictionName).ToList();
						jurisdictionNames.Sort();
						var jurisdictionString = String.Join(", ", jurisdictionNames);

						var manufMemoBuilder = new StringBuilder();
						manufMemoBuilder.Append("PCT<br>");
						manufMemoBuilder.Append("Order Number: " + _transferRequestHeader.OrderNumber + "<br>");
						manufMemoBuilder.Append("Request Date: " + _transferRequestHeader.RequestDate.ToString("g") + "<br>");

						if (!String.IsNullOrWhiteSpace(_transferRequestHeader.RefNum))
						{
							manufMemoBuilder.Append("Reference Number: " + _transferRequestHeader.RefNum + "<br>");
						}

						manufMemoBuilder.Append("Jurisdiction: " + jurisdictionString + "<br>");
						manufMemoBuilder.Append("Manufacturer Memo: " + _transferRequestHeader.Memo + "<br><br>");
						manufMemoBuilder.Append("Contact Information:" + "<br>");
						manufMemoBuilder.Append(_transferRequestHeader.ContactName + "<br>");
						manufMemoBuilder.Append(_transferRequestHeader.ContactEmail);

						var manufMemo = new GLI.EFCore.Submission.tblMemo
						{
							Memo = manufMemoBuilder.ToString(),
							SubmissionId = (int)submission.SubmissionId,
							AddDate = DateTime.Now,
							Username = _userContext.User.Username
						};

						_dbSubmission.tblMemos.Add(manufMemo);
						_dbSubmission.SaveChanges();
					}

					if (!String.IsNullOrWhiteSpace(_editModel.Memo))
					{
						AddCustomSubmissionMemo((int)submission.SubmissionId);
					}
				}
			}
			// Declined Status
			else
			{
				if (!String.IsNullOrWhiteSpace(_editModel.Memo))
				{
					var submissionIds = (from x in _transferRequestItemsToUpdate
										 select x.SubmissionId).ToList();

					foreach (var submissionId in submissionIds)
					{
						AddCustomSubmissionMemo((int)submissionId);
					}
				}
			}
		}

		// Optional user entered memo
		private void AddCustomSubmissionMemo(int submissionId)
		{
			var userMemo = new GLI.EFCore.Submission.tblMemo
			{
				Memo = _editModel.Memo,
				SubmissionId = submissionId,
				AddDate = DateTime.Now,
				Username = _userContext.User.Username
			};

			_dbSubmission.tblMemos.Add(userMemo);
			_dbSubmission.SaveChanges();
		}
		#endregion

		#region File Handling
		public void MoveFiles(Guid id, int submissionId, HashSet<string> fileNumbersAffected, bool batchHasPendingRequests, string archiveLocation)
		{
			if (_userContext.User.Environment == Environment.Production)
			{
				var attachments = _dbGLIAccess.TransferRequestAttachments
					.Where(x => x.TransferRequestHeaderId == id)
					.Select(x => new { x.FileName, x.AttachmentType, x.UploadFolder })
					.ToList();

				var sourceFolderGuid = attachments.Select(x => x.UploadFolder).FirstOrDefault() ?? id.ToString();

				//source example: \\nj_esquarantine\pctUploads\18DCA42A-5529-4A05-8F64-00024035C834
				//dest example: \\gaminglabs.net\glimain\Public\NJ\FTP Submissions\PCT Attachments\MO-73-BAL-13-85_18DCA42A-5529-4A05-8F64-00024035C834
				//gps example: \\gaminglabs.net\glimain\Public\NJ\Submissions\2019\INT\MO-073-INT-19-014\Manufacturer Submitted\00 Entire Submission Contents
				var fileNumber = _dbSubmission.Submissions.Find(submissionId).FileNumber;
				var sourcePath = Path.Combine(_configContext.PCTUploadPath, sourceFolderGuid);
				var destPath = Path.Combine(_configContext.PCTAttachmentPath, fileNumber.ToString() + "_" + id.ToString());

				var impersonateUsername = _configContext.MonitorAccountUsername;
				var impersonatePassword = _configContext.MonitorAccountPassword;

				FileInfo[] files;

				//Move Network Share Files
				using (var impersonatedUser = new ImpersonateUser(Domain.GamingLabs, impersonateUsername, impersonatePassword))
				{
					if (!Directory.Exists(sourcePath)) { return; }
					if (!Directory.Exists(destPath)) { Directory.CreateDirectory(destPath); }

					files = new DirectoryInfo(sourcePath).GetFiles();

					foreach (var file in files)
					{
						File.Copy(file.FullName, Path.Combine(destPath, file.Name), true);
					}
				}

				//GPS the Files
				foreach (var fileNum in fileNumbersAffected)
				{
					var gpsFileNum = new GPS.FileNumber(fileNum);
					var root = GPS.FUWDirTree.GenerateFUWTree(gpsFileNum, true, ref archiveLocation, false, false, true, true, true, false);
					var dirOfficialRequests = root.DescendantOrSelf(x => x.Name.Contains(GPSDirName.OfficialRequests.GetEnumDescription()));
					var dirEntireSubmissionContents = root.DescendantOrSelf(x => x.Name == GPSDirName.EntireSubmissionContents.GetEnumDescription());

					foreach (var file in files)
					{
						var gpsFileInfo = new GPS.GPSFileInfo(file.FullName) { Action = GPS.ItemAction.UPLOADNEW };
						var documentType = attachments.Where(x => x.FileName == file.Name).Select(x => x.AttachmentType).SingleOrDefault();
						if (string.Equals(documentType, FileUploadType.Letter.ToString(), StringComparison.OrdinalIgnoreCase))
						{
							dirOfficialRequests.EntriesToProcess.Add(gpsFileInfo);
						}
						else
						{
							dirEntireSubmissionContents.EntriesToProcess.Add(gpsFileInfo);
						}
					}

					var gpsLabId = FUWUtil.FUWUtil.Laboratories.FirstOrDefault(lab => lab.Location == archiveLocation || lab.ArchiveLocation == archiveLocation).ID;
					dirOfficialRequests.ProcessAll(gpsLabId);
					dirEntireSubmissionContents.ProcessAll(gpsLabId);
				}

				//Delete Source Folder once all of the Requests have been Processed
				if (!batchHasPendingRequests)
				{
					using (var impersonatedUser = new ImpersonateUser(Domain.GamingLabs, impersonateUsername, impersonatePassword))
					{
						try
						{
							Directory.Delete(sourcePath, true);
						}
						catch (IOException)
						{
							//Ignore this exception.  Possibly not released properly in GPS.
							//The process cannot access the file 'fileName' because it is being used by another process.
						}
					}
				}
			}
		}
		#endregion

		private ProcessResponse BuildResponse()
		{
			var response = new ProcessResponse();

			if (!_batchHasPendingRequests)
			{
				response.Result = "AllRequestsCompleted";
			}
			else
			{
				response.Result = "PendingRequests";
			}

			response.Messages = _responseMsg.ToString();

			return response;
		}

		// -- Cascades MasterBilling Project Id to other records of submission containing given jurisdiction and any associated jurisdictions.
		private void CascadeMasterBillingId(GLI.EFCore.Submission.JurisdictionalData jurisdiction, EF.GLIAccess.Submission submission)
		{
			var associatedJurisdictionIds = AssociatedJurisdiction.GetAssociations(jurisdiction.JurisdictionId, submission.Manufacturer)
				.Select(x => x.Id)
				.ToList();

			_dbSubmission.JurisdictionalData
				.Where(x => x.Submission.FileNumber == submission.FileNumber && (x.JurisdictionName == jurisdiction.JurisdictionName || associatedJurisdictionIds.Contains(x.JurisdictionId)))
				.Update(x => new GLI.EFCore.Submission.JurisdictionalData { MasterBillingProject = jurisdiction.MasterBillingProject });
		}

		private void SendEmails()
		{
			var manufacturerCodes = _allEmailJurisdictions.Select(x => x.Manufacturer).Distinct().ToList();

			var novomacTestedByUK = new List<string>
			{
				CustomManufacturer.ANU.ToString(),
				CustomManufacturer.NMI.ToString(),
				CustomManufacturer.NMY.ToString(),
				CustomManufacturer.NSU.ToString(),
				CustomManufacturer.NOJ.ToString()
			};

			if (!manufacturerCodes.Any(x => novomacTestedByUK.Contains(x)))
			{
				return;
			}

			var emailViewModel = _allEmailJurisdictions.OrderBy(x => x.FileNumber).ThenBy(x => x.Jurisdiction).ToList();

			var mailMsg = new MailMessage();
			mailMsg.From = new MailAddress("noreply@gaminglabs.com");
			mailMsg.Subject = "Point Click Transfer Processed";
			mailMsg.Body = _emailService.RenderTemplate(EmailType.PointClickTransfer.ToString(), emailViewModel);
			mailMsg.To.Add("submissions.zuluaga@gaminglabs.com");
			_emailService.Send(mailMsg);
		}
	}

	public class ProcessResponse
	{
		public string Result { get; set; }
		public string Messages { get; set; }
	}
}