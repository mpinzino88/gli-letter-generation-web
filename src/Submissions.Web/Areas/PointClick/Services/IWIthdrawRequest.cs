﻿using Submissions.Web.Areas.PointClick.Models.Withdraw;
using System;

namespace Submissions.Web.Areas.PointClick.Services
{
	public interface IWithdrawRequest
	{
		void ProcessRequests(Guid id, DetailEditModel editModel);
	}
}
