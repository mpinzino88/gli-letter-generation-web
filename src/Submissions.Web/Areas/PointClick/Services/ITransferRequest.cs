﻿using Submissions.Web.Areas.PointClick.Models.Transfer;
using System;
using System.Collections.Generic;

namespace Submissions.Web.Areas.PointClick.Services
{
	public interface ITransferRequest
	{
		void MoveFiles(Guid id, int submissionId, HashSet<string> fileNumbersAffected, bool batchHasPendingRequests, string archiveLocation);
		ProcessResponse ProcessRequests(Guid id, DetailEditModel editModel);
	}
}
