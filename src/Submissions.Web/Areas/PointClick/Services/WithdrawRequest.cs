﻿using EF.GLIAccess;
using Submissions.Core;
using Submissions.Web.Areas.PointClick.Models.Withdraw;
using System;
using System.Linq;
using Z.EntityFramework.Plus;

namespace Submissions.Web.Areas.PointClick.Services
{
	using Submissions.Common;
	using Submissions.Common.Email;
	using Submissions.Web.Utility.Email;
	using System.Net.Mail;

	public class WithdrawRequest : IWithdrawRequest
	{
		private readonly IGLIAccessContext _dbGLIAccess;
		private readonly IUserContext _userContext;
		private readonly IJurisdictionalDataService _jurisdictionalDataService;
		private readonly ISubmissionService _submissionService;
		private readonly IEmailService _emailService;
		private readonly IMemoService _memoService;

		public WithdrawRequest
		(
			IGLIAccessContext dbGLIAccess,
			IUserContext userContext,
			IJurisdictionalDataService jurisdictionalDataService,
			ISubmissionService submissionService,
			IEmailService emailService,
			IMemoService memoService
		)
		{
			_dbGLIAccess = dbGLIAccess;
			_userContext = userContext;
			_jurisdictionalDataService = jurisdictionalDataService;
			_submissionService = submissionService;
			_emailService = emailService;
			_memoService = memoService;
		}

		public void ProcessRequests(Guid id, DetailEditModel editModel)
		{
			var requestIds = editModel.WithdrawalsToUpdate.Select(x => x.Id).Distinct().ToList();
			var requests = _dbGLIAccess.WithdrawalRequests
				.Where(x => requestIds.Contains(x.Id));

			var requestorEmail = requests.Select(x => x.ContactEmail).First();
			var requestorName = requests.Select(x => x.ContactName).First();

			var records = requests
					.Where(x => x.SubmissionId != null && x.JurisdictionalDataId == null);

			var jurisdictions = requests
					.Where(x => x.JurisdictionalDataId != null);

			if (editModel.WithdrawRequestEditAction == WithdrawRequestEditAction.RJ ||
				editModel.WithdrawRequestEditAction == WithdrawRequestEditAction.WD)
			{
				SubmissionStatus closeStatusSub = (SubmissionStatus)Enum.Parse(typeof(SubmissionStatus), editModel.WithdrawRequestEditAction.ToString());
				JurisdictionStatus closeStatusJur = (JurisdictionStatus)Enum.Parse(typeof(JurisdictionStatus), editModel.WithdrawRequestEditAction.ToString());


				//update selected records
				var recordIds = records.Select(x => (int)x.SubmissionId).ToList();
				if (recordIds.Count > 0)
					_submissionService.Close(recordIds, closeStatusSub);
				//add record memos
				var recordMemo = new GLI.EFCore.Submission.tblMemo
				{
					AddDate = DateTime.Now,
					Username = _userContext.User.Username,
					Memo = "Web request for Withdrawal submitted by supplier " + DateTime.Now.ToString("g") + ": " + editModel.RequestorMemo
				};
				_memoService.AddBulk(recordMemo, recordIds);

				//update selected jurisdictions
				var jurisdictionIds = jurisdictions.Select(x => (int)x.JurisdictionalDataId).ToList();
				if (jurisdictionIds.Count > 0)
					_jurisdictionalDataService.CloseBulk(jurisdictionIds, closeStatusJur, editModel.WithdrawReason);
				//add jur specific memos
				var jurDate = DateTime.Now.ToString("g");
				var jurMemos = jurisdictions
					.Select(x => new GLI.EFCore.Submission.tblMemo
					{
						AddDate = DateTime.Now,
						SubmissionId = (int)x.JurisdictionalData.SubmissionId,
						Username = _userContext.User.Username,
						Memo = "Web request for Withdrawal on jurisdiction " + x.JurisdictionalData.JurisdictionName + " submitted by supplier " + jurDate + ": " + editModel.RequestorMemo
					})
					.ToList();
				_memoService.AddBulk(jurMemos);

				//process for sub status
				_jurisdictionalDataService.ProcessForSubmissionStatus(jurisdictionIds);
			}

			//close request
			requests.Update(x => new WithdrawalRequest
			{
				QAMemo = editModel.QAMemo != null ? editModel.QAMemo : x.QAMemo,
				ProcessDate = DateTime.Now,
				QAUserId = _userContext.User.Id
			});

			//send email notification
			var emailItems = records
				.Select(x => new PointClickWithdrawEmailItem
				{
					FileNumber = x.Submission.FileNumber,
					Manufacturer = x.Submission.ManufacturerDescription,
					IDNumber = x.Submission.IdNumber,
					GameName = x.Submission.GameName,
					Version = x.Submission.Version,
					Function = x.Submission.Function,
					Position = x.Submission.Position,
					Jurisdiction = "All Jurisdictions",
				})
				.Union(jurisdictions
				.Select(x => new PointClickWithdrawEmailItem
				{
					FileNumber = x.JurisdictionalData.Submission.FileNumber,
					Manufacturer = x.JurisdictionalData.Submission.ManufacturerDescription,
					IDNumber = x.JurisdictionalData.Submission.IdNumber,
					GameName = x.JurisdictionalData.Submission.GameName,
					Version = x.JurisdictionalData.Submission.Version,
					Function = x.JurisdictionalData.Submission.Function,
					Position = x.JurisdictionalData.Submission.Position,
					Jurisdiction = x.JurisdictionalData.JurisdictionName,
				}))
				.ToList();

			var emailModel = new PointClickWithdrawEmail
			{
				ActionName = editModel.WithdrawRequestEditAction.ToString(),
				ProcessorEmail = _userContext.User.Email,
				ProccessorName = _userContext.User.FName + " " + _userContext.User.LName,
				RequestorEmail = requestorEmail,
				RequestiorName = requestorName,
				RequestItems = emailItems
			};

			SendEmail(emailModel);
		}

		#region Emails
		private EmailResponse SendEmail(PointClickWithdrawEmail emailModel)
		{
			//var gliGlobe = Server.MapPath("~/Img/GLIGlobe.gif");
			//_emailService.EmbeddedImages = new List<LinkedResource> { new LinkedResource(gliGlobe) { ContentId = "GLIGlobe" }, };

			var mailMsg = new MailMessage();
			mailMsg.From = new MailAddress("QACertQuestions@gaminglabs.com");
			mailMsg.To.Add(new MailAddress(emailModel.RequestorEmail));
			mailMsg.CC.Add(new MailAddress(emailModel.ProcessorEmail));

			mailMsg.Subject = "GLI Notification: Withdrawal Request[s] has been processed.";
			mailMsg.Body = _emailService.RenderTemplate(EmailType.PointClickWithdraw.ToString(), emailModel);

			return _emailService.Send(mailMsg);
		}
		#endregion
	}
}