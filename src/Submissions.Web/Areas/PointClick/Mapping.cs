﻿using AutoMapper;
using EF.GLIAccess;
using Submissions.Web.Areas.PointClick.Models.Submit;
using Submissions.Web.Areas.PointClick.Models.Transfer;
using Submissions.Web.Models.Grids.PointClick;

namespace Submissions.Web.Areas.PointClick
{
	public class PointClickProfile : Profile
	{
		public PointClickProfile()
		{
			// PCS
			CreateMap<electronicSubmissionMaster, SubmitRequestGridRecord>()
				.ForMember(dest => dest.EditLink, opt => opt.Ignore());
			CreateMap<electronicSubmissionMaster, SubmitRequestViewModel>()
				.ForMember(dest => dest.AddUsername, opt => opt.Ignore())
				.ForMember(dest => dest.Attachments, opt => opt.Ignore())
				.ForMember(dest => dest.Components, opt => opt.Ignore())
				.ForMember(dest => dest.InternalEditUser, opt => opt.Ignore())
				.ForMember(dest => dest.Shipments, opt => opt.Ignore())
				.ForMember(dest => dest.Studio, opt => opt.MapFrom(src => src.Studio.Name))
				.ForMember(dest => dest.GameSubmissionType, opt => opt.MapFrom(src => src.GameSubmissionType.Description))
				.ForMember(dest => dest.TestingType, opt => opt.MapFrom(src => src.TestingType.Description))
				.ForMember(dest => dest.TranslationHouses, opt => opt.Ignore());
			CreateMap<electronicSubmissionTrackingNum, ShipmentViewModel>();

			// PCT
			CreateMap<TransferRequestHeader, TransferRequestGridRecord>()
				.ForMember(dest => dest.ViewDetails, opt => opt.Ignore());
			CreateMap<TransferRequestHeader, DetailHeaderModel>()
				.ForMember(dest => dest.AssociatedJurisdictions, opt => opt.Ignore())
				.ForMember(dest => dest.AttachmentsLink, opt => opt.Ignore())
				.ForMember(dest => dest.InternalEditUser, opt => opt.Ignore())
				.ForMember(dest => dest.ManufacturerCode, opt => opt.Ignore());
			CreateMap<TransferRequestItem, TransferRequestDetailGridRecord>()
				.ForMember(dest => dest.Add, opt => opt.Ignore());

			// PCW
			CreateMap<WithdrawalRequest, WithdrawRequestRecordGridRecord>()
				.ForMember(dest => dest.SubmissionStatus, opt => opt.Ignore());
			CreateMap<WithdrawalRequest, WithdrawRequestJurisdictionGridRecord>()
				.ForMember(dest => dest.JurisdictionalDataStatus, opt => opt.Ignore());
		}
	}
}