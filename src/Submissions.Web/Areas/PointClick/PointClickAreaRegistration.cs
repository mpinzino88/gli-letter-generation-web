﻿using System.Web.Mvc;

namespace Submissions.Web.Areas.PointClick
{
	public class PointClickAreaRegistration : AreaRegistration
	{
		public override string AreaName
		{
			get
			{
				return "PointClick";
			}
		}

		public override void RegisterArea(AreaRegistrationContext context)
		{
			context.MapRoute(
				"PointClick_default",
				"PointClick/{controller}/{action}/{id}",
				new { action = "Index", id = UrlParameter.Optional }
			);
		}
	}
}