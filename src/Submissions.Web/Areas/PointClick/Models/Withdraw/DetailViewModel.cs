﻿using Submissions.Common;
using Submissions.Web.Models.Grids.PointClick;
using Submissions.Web.Models.Query;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Submissions.Web.Areas.PointClick.Models.Withdraw
{
	public class DetailViewModel
	{
		public DetailViewModel()
		{
			JurisdictionFilters = new WithdrawRequestSearch { IsJurisdiction = true };
			RecordFilters = new WithdrawRequestSearch { IsJurisdiction = false };
			HeaderInfo = new DetailHeaderModel();
			WithdrawRequestRecordGrid = new WithdrawRequestRecordGrid();
			WithdrawRequestJurisdictionGrid = new WithdrawRequestJurisdictionGrid();
		}
		public DetailHeaderModel HeaderInfo { get; set; }
		public WithdrawRequestJurisdictionGrid WithdrawRequestJurisdictionGrid { get; set; }
		public WithdrawRequestRecordGrid WithdrawRequestRecordGrid { get; set; }
		public WithdrawRequestSearch JurisdictionFilters { get; set; }
		public WithdrawRequestSearch RecordFilters { get; set; }
		public DetailEditModel DetailEditModel { get; set; }
	}

	public class DetailHeaderModel
	{
		[Display(Name = "Request Id")]
		public Guid RequestId { get; set; }
		[Display(Name = "Requestor Name")]
		public string RequestorName { get; set; }
		[Display(Name = "Request Date")]
		public DateTime RequestDate { get; set; }
		[Display(Name = "Reason for Withdrawal")]
		public string Reason { get; set; }
		public string Status { get; set; }
		[Display(Name = "QA Memo")]
		public string QAMemo { get; set; }
	}

	#region Modal Dialog
	public class DetailEditModel
	{
		public List<WithdrawalsToUpdate> WithdrawalsToUpdate { get; set; }
		[Display(Name = "Edit Action"), Required]
		public WithdrawRequestEditAction? WithdrawRequestEditAction { get; set; }
		public string QAMemo { get; set; }
		public string RequestorMemo { get; set; }
		[DisplayName("Withdraw/Reject Reason ")]
		public int? WithdrawReasonId { get; set; }
		public string WithdrawReason { get; set; }
	}

	public class WithdrawalsToUpdate
	{
		public int Id { get; set; }
	}
	#endregion
}