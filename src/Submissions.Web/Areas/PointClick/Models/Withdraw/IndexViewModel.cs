﻿using Submissions.Web.Models.Grids.PointClick;
using Submissions.Web.Models.Query;

namespace Submissions.Web.Areas.PointClick.Models.Withdraw
{
	public class IndexViewModel
	{
		public IndexViewModel()
		{
			this.WithdrawRequestGrid = new WithdrawRequestGrid();
			this.Filters = new WithdrawRequestSearch { Status = "Pending" };
		}

		public WithdrawRequestGrid WithdrawRequestGrid { get; set; }
		public WithdrawRequestSearch Filters { get; set; }
	}
}