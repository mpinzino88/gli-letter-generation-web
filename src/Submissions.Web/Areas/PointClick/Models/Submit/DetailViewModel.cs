﻿using Submissions.Common.Validation;
using Submissions.Web.Models.LookupsAjax;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Submissions.Web.Areas.PointClick.Models.Submit
{
	public class DetailViewModel
	{
		public DetailViewModel()
		{
			this.Accept = new AcceptViewModel();
			this.Reject = new RejectViewModel();
			this.SubmitRequest = new SubmitRequestViewModel();
			this.ContractTypeIdsRequireMemo = new List<int>();
		}

		public AcceptViewModel Accept { get; set; }
		public RejectViewModel Reject { get; set; }
		public SubmitRequestViewModel SubmitRequest { get; set; }
		public IList<int> ContractTypeIdsRequireMemo { get; set; }
		public bool ShowGameSubmissionType { get; set; }
	}

	public class SubmitRequestViewModel
	{
		public SubmitRequestViewModel()
		{
			this.Shipments = new List<ShipmentViewModel>();
			this.Components = new List<ComponentViewModel>();
			this.Attachments = new List<AttachmentViewModel>();
			this.TranslationHouses = new List<string>();
		}

		public int Id { get; set; }
		[Display(Name = "Confirmation ID")]
		public string ConfirmId { get; set; }
		[Display(Name = "File Number")]
		public string FileNumber { get; set; }
		public string Status { get; set; }
		public int? AddUserId { get; set; }
		public string AddUsername { get; set; }
		[Display(Name = "Name")]
		public string ContactName { get; set; }
		[Display(Name = "Email")]
		public string ContactEmail { get; set; }
		[Display(Name = "Phone")]
		public string ContactPhone { get; set; }
		[Display(Name = "Manufacturer")]
		public string ManufacturerCode { get; set; }
		[Display(Name = "Testing Type")]
		public string TestingType { get; set; }
		[Display(Name = "Reference Number")]
		public string ReferenceNumber { get; set; }
		[Display(Name = "Request Date")]
		public DateTime? SubmitDate { get; set; }
		public DateTime? EditDate { get; set; }
		public string Memo { get; set; }
		public string QAMemo { get; set; }
		[Display(Name = "Precertification")]
		public bool PreCertified { get; set; }
		[Display(Name = "Internal Memo")]
		public string InternalMemo { get; set; }
		public int? InternalEditUserId { get; set; }
		[Display(Name = "Edit User")]
		public string InternalEditUser { get; set; }
		[Display(Name = "Edit Date")]
		public DateTime? InternalEditDate { get; set; }
		public IList<ShipmentViewModel> Shipments { get; set; }
		public IList<ComponentViewModel> Components { get; set; }
		public IList<AttachmentViewModel> Attachments { get; set; }

		// iGaming Fields
		public string Studio { get; set; }
		[Display(Name = "Target Date")]
		public DateTime? TargetDate { get; set; }
		[Display(Name = "Go-Live Date")]
		public DateTime? GoLiveDate { get; set; }
		[Display(Name = "FTP Location")]
		public string FTPLocation { get; set; }
		[Display(Name = "Build Tag")]
		public string ManufacturerBuildId { get; set; }
		[Display(Name = "Testing Environment")]
		public string TestingEnvironment { get; set; }
		[Display(Name = "Translation Houses")]
		public IList<string> TranslationHouses { get; set; }
		[Display(Name = "Game Submission Type")]
		public string GameSubmissionType { get; set; }
		public string GameSubmissionTypeInfo { get; set; }
	}

	public class ShipmentViewModel
	{
		public int Id { get; set; }
		[Display(Name = "Carrier")]
		public string Shipper { get; set; }
		[Display(Name = "Tracking Number")]
		public string TrackingNumber { get; set; }
	}

	public class ComponentViewModel
	{
		public ComponentViewModel()
		{
			this.Jurisdictions = new List<JurisdictionViewModel>();
			this.Attachments = new List<AttachmentViewModel>();
		}

		public int Id { get; set; }
		public int ItemNumber { get; set; }
		public int? PossibleDuplicateSubmissionId { get; set; }
		[Display(Name = "ID Number")]
		public string IdNumber { get; set; }
		[Display(Name = "Version")]
		public string Version { get; set; }
		[Display(Name = "Game Name")]
		public string GameName { get; set; }
		[Display(Name = "IGT Material #")]
		public string OrgIGTMaterial { get; set; }
		public string Position { get; set; }
		[Display(Name = "Signature Type")]
		public string SignatureType { get; set; }
		[Display(Name = "Signature Scope")]
		public string SignatureScope { get; set; }
		[Display(Name = "Signature")]
		public string Signature { get; set; }
		public IList<JurisdictionViewModel> Jurisdictions { get; set; }
		public IList<AttachmentViewModel> Attachments { get; set; }
	}

	public class JurisdictionViewModel
	{
		public int Id { get; set; }
		public string JurisdictionId { get; set; }
		public string Name { get; set; }
	}

	public class AttachmentViewModel
	{
		public int Id { get; set; }
		public string AttachmentType { get; set; }
		public string FileName { get; set; }
	}

	public class AcceptViewModel
	{
		public AcceptViewModel()
		{
			this.JurisdictionFilter = new JurisdictionsFilter();
			this.TranslationHouses = new List<SelectListItem>();
		}

		[Display(Name = "Company"), Required]
		public int? CompanyId { get; set; }
		public string Company { get; set; }
		[Display(Name = "Contract Type"), Required]
		public int? ContractTypeId { get; set; }
		public string ContractType { get; set; }
		[Display(Name = "Currency"), Required]
		public int? CurrencyId { get; set; }
		public string Currency { get; set; }
		[Display(Name = "Type"), Required]
		public int? TypeId { get; set; }
		public string Type { get; set; }
		[Display(Name = "Test Lab"), Required]
		public int? LabId { get; set; }
		public string Lab { get; set; }
		[Display(Name = "Certification Lab"), Required]
		public int? CertificationLabId { get; set; }
		public string CertificationLab { get; set; }
		[Display(Name = "Work Order Number (WON)"), RegularExpression(RegularExpressionPatterns.AccountingOrFileNumber, ErrorMessage = "Please enter a valid format")]
		public string WON { get; set; }
		[Display(Name = "Project/Contract Number"), StringLength(150)]
		public string ContractNumber { get; set; }
		[Display(Name = "Contract Value"), Range(0, 99999999.99)]
		public decimal? ContractValue { get; set; }
		public string Memo { get; set; }
		[Display(Name = "Primary Jurisdiction")]
		public string JurisdictionId { get; set; }
		public string Jurisdiction { get; set; }
		public JurisdictionsFilter JurisdictionFilter { get; set; }
		[Display(Name = "Translation Houses")]
		public string[] TranslationHouseIds { get; set; }
		public IList<SelectListItem> TranslationHouses { get; set; }
	}

	public class RejectViewModel
	{
		[Required]
		public string Reason { get; set; }
	}
}