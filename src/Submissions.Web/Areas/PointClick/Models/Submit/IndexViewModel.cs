﻿using Submissions.Web.Models.Grids.PointClick;
using Submissions.Web.Models.Query;

namespace Submissions.Web.Areas.PointClick.Models.Submit
{
	public class IndexViewModel
	{
		public IndexViewModel()
		{
			this.SubmitRequestGrid = new SubmitRequestGrid();
			this.Filters = new SubmitRequestSearch();
		}

		public SubmitRequestGrid SubmitRequestGrid { get; set; }
		public SubmitRequestSearch Filters { get; set; }
		public int? NewSubmissionId { get; set; }
	}
}