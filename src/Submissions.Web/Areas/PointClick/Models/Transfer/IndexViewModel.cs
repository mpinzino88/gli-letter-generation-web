﻿using Submissions.Web.Models.Grids.PointClick;
using Submissions.Web.Models.Query;
using System.Web;

namespace Submissions.Web.Areas.PointClick.Models.Transfer
{
	public class IndexViewModel
	{
		public IndexViewModel()
		{
			this.TransferRequestGrid = new TransferRequestGrid();
			this.Filters = new TransferRequestSearch();
		}

		public IHtmlString AttachmentsLink { get; set; }
		public TransferRequestGrid TransferRequestGrid { get; set; }
		public TransferRequestSearch Filters { get; set; }
	}
}