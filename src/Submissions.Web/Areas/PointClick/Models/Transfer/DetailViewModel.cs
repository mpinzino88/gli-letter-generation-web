﻿using Submissions.Common;
using Submissions.Core.Models;
using Submissions.Web.Models.Grids.PointClick;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web;
using System.Web.Mvc;

namespace Submissions.Web.Areas.PointClick.Models.Transfer
{
	public class DetailViewModel
	{
		public DetailViewModel()
		{
			this.TransferRequestDetailGrid = new TransferRequestDetailGrid();
		}

		public DetailHeaderModel DetailHeaderModel { get; set; }
		public DetailEditModel DetailEditModel { get; set; }
		public TransferRequestDetailGrid TransferRequestDetailGrid { get; set; }
	}

	public class DetailHeaderModel
	{
		[Display(Name = "Request Id")]
		public Guid Id { get; set; }
		[Display(Name = "Order Number")]
		public int OrderNumber { get; set; }
		[Display(Name = "Request Date")]
		public DateTime RequestDate { get; set; }
		[Display(Name = "Manufacturer")]
		public string ManufacturerCode { get; set; }
		[Display(Name = "Name")]
		public string ContactName { get; set; }
		[Display(Name = "Email")]
		public string ContactEmail { get; set; }
		[Display(Name = "Phone")]
		public string ContactPhone { get; set; }
		[Display(Name = "Project Reference")]
		public string RefNum { get; set; }
		public string Memo { get; set; }
		public bool IsConfirmed { get; set; }
		public string Status { get; set; }
		public IList<AssociatedJurisdiction> AssociatedJurisdictions { get; set; }
		public bool TransferAdditionalItems { get; set; }
		[Display(Name = "Internal Memo")]
		public string InternalMemo { get; set; }
		[Display(Name = "Edit User")]
		public string InternalEditUser { get; set; }
		[Display(Name = "Edit Date")]
		public DateTime? InternalEditDate { get; set; }
		public IHtmlString AttachmentsLink { get; set; }
	}

	#region Modal Dialog
	public class DetailEditModel
	{
		public DetailEditModel()
		{
			TransferRequestStatusId = null;
			TransferRequestStatus = "";
			TransferRequestStatusList = LookupsStandard.ConvertEnum(showBlank: true, enumValues: new Common.TransferRequestStatus[] { Common.TransferRequestStatus.Accepted, Common.TransferRequestStatus.Declined });
		}

		public List<TransfersToUpdate> TransfersToUpdate { get; set; }
		[Display(Name = "Jurisdiction")]
		public int? JurisdictionId { get; set; }
		public string Jurisdiction { get; set; }
		[Display(Name = "Transfer Status"), Required]
		public string TransferRequestStatusId { get; set; }
		public string TransferRequestStatus { get; set; }
		public IEnumerable<SelectListItem> TransferRequestStatusList { get; set; }
		[Display(Name = "Company"), Required]
		public int? CompanyId { get; set; }
		public string Company { get; set; }
		[Display(Name = "TX Lab"), Required]
		public int? LabId { get; set; }
		public string Lab { get; set; }
		[Display(Name = "Contract Type"), Required]
		public int? ContractTypeId { get; set; }
		public string ContractType { get; set; }
		[Display(Name = "Currency"), Required]
		public int? CurrencyId { get; set; }
		public string Currency { get; set; }
		public string Memo { get; set; }
		[Display(Name = "Subliminal Message")]
		public bool StatementAck { get; set; }
		[Display(Name = "Software Copy")]
		public bool SoftwareAck { get; set; }
		public List<MBIDResultRow> MBIDResultRows { get; set; }
	}

	public class TransfersToUpdate
	{
		public Guid Id { get; set; }
	}

	public class MBIDResultRow
	{
		public Guid TransferId { get; set; }
		public string JurisdictionId { get; set; }
		public string JurisdictionName { get; set; }
		public string FileNumber { get; set; }
		public string IdNumber { get; set; }
		public string Version { get; set; }
		public string DateCode { get; set; }
		public string LetterNumber { get; set; }
		public string BillingReference { get; set; }
		public string ContractNumber { get; set; }
		public string MBID { get; set; }
		public string MBIDRow { get; set; }
	}
	#endregion
}