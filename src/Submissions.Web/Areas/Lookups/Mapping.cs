using System.Linq;
using AutoMapper;
using EF.Submission;
using Submissions.Common.Linq.Dynamic;
using Submissions.Web.Areas.Lookups.Models;
using Submissions.Web.Areas.Tools.Models;
using Submissions.Web.Models.Grids.Lookups;

namespace Submissions.Web.Mappings
{
	public class LookupsProfile : Profile
	{
		public LookupsProfile()
		{
			CreateMap<GLI.EFCore.Submission.EmailDistribution, EmailDistributionsIndexViewModel>();
			CreateMap<GLI.EFCore.Submission.EmailDistribution, EmailDistributionInfo>()
				.ForMember(dest => dest.JurisdictionUsers, opt => opt.Ignore())
				.ForMember(dest => dest.ManufacturerGroupUsers, opt => opt.Ignore())
				.ForMember(dest => dest.ManufacturerUsers, opt => opt.Ignore())
				.ForMember(dest => dest.UserIds, opt => opt.Ignore())
				.ForMember(dest => dest.Users, opt => opt.Ignore());
			CreateMap<GLI.EFCore.Submission.EmailType, EmailTypesEditViewModel>()
				.ForMember(dest => dest.Mode, opt => opt.Ignore())
				.ForMember(dest => dest.Active, opt => opt.Ignore())
				.ForMember(dest => dest.UserIds, opt => opt.Ignore())
				.ForMember(dest => dest.Users, opt => opt.Ignore())
				.ForMember(dest => dest.ExternalEmailAddress, opt => opt.Ignore())
				.ForMember(dest => dest.EmailDistributionIds, opt => opt.Ignore())
				.ForMember(dest => dest.EmailDistributions, opt => opt.Ignore());
			CreateMap<GLI.EFCore.Submission.EmailType, EmailTypesIndexViewModel>();
			CreateMap<tbl_lst_AcumaticaBillwithAndCustomerExceptions, AcumaticaBillWithAndCustomerExceptionsGridRecord>()
				.ForMember(dest => dest.Edit, opt => opt.Ignore())
				.ForMember(dest => dest.CompanyCode, opt => opt.MapFrom(src => src.Company.DynamicsCustomerCode))
				.ForMember(dest => dest.ExceptionTypeName, opt => opt.MapFrom(src => src.BillingExceptionType.Name));
			CreateMap<GLI.EFCore.Submission.tbl_lst_AcumaticaBillwithAndCustomerExceptions, AcumaticaBillWithAndCustomerExceptionEditViewModel>()
				.ForMember(dest => dest.Mode, opt => opt.Ignore())
				.ForMember(dest => dest.CompanyCode, opt => opt.MapFrom(src => src.Company.DynamicsCustomerCode))
				.ForMember(dest => dest.CompanyName, opt => opt.MapFrom(src => src.Company.Name))
				.ForMember(dest => dest.Manufacturer, opt => opt.MapFrom(src => src.Manufacturer.Description))
				.ForMember(dest => dest.Customer, opt => opt.MapFrom(src => src.Customer.Description))
				.ForMember(dest => dest.ExceptionTypeName, opt => opt.MapFrom(src => src.BillingExceptionType.Name));
			CreateMap<tbl_lst_AcumaticaTenant, AcumaticaTenantsGridRecord>()
				.ForMember(dest => dest.Edit, opt => opt.Ignore())
				.ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.TenantName));
			CreateMap<GLI.EFCore.Submission.tbl_lst_AcumaticaTenant, AcumaticaTenantsEditViewModel>()
				.ForMember(dest => dest.Mode, opt => opt.Ignore())
				.ForMember(dest => dest.Active, opt => opt.Ignore())
				.ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.TenantName))
				.ForMember(dest => dest.GLICompanyCode, opt => opt.MapFrom(src => src.Company.Name))
				.ForMember(dest => dest.GLICompanyID, opt => opt.MapFrom(src => src.Company.Id));
			CreateMap<GLI.EFCore.Submission.tbl_lst_fileJuris, JurisdictionsEditViewModel>()
				.ForMember(dest => dest.Mode, opt => opt.Ignore())
				.ForMember(dest => dest.SendEmail, opt => opt.Ignore())
				.ForMember(dest => dest.NationCodeOptions, opt => opt.Ignore())
				.ForMember(dest => dest.Country, opt => opt.Ignore())
				.ForMember(dest => dest.LiaisonUserIds, opt => opt.Ignore())
				.ForMember(dest => dest.LiaisonUsers, opt => opt.Ignore())
				.ForMember(dest => dest.QMSLink, opt => opt.Ignore())
				.ForMember(dest => dest.ManufacturerCodes, opt => opt.Ignore())
				.ForMember(dest => dest.Manufacturers, opt => opt.Ignore())
				.ForMember(dest => dest.BillingLabIds, opt => opt.Ignore())
				.ForMember(dest => dest.BillingLabs, opt => opt.Ignore())
				.ForMember(dest => dest.A1DataField4TypOptions, opt => opt.Ignore())
				.ForMember(dest => dest.A1Data, opt => opt.MapFrom(src => src.A1Data))
				.ForMember(dest => dest.DefaultA1Data, opt => opt.Ignore())
				.ForMember(dest => dest.SignatureScopes, opt => opt.Ignore())
				.ForMember(dest => dest.QAOfficeDefaultId, opt => opt.Ignore())
				.ForMember(dest => dest.QAOfficeDefaults, opt => opt.Ignore())
				.ForMember(dest => dest.DefaultSignatureScope, opt => opt.Ignore());
			CreateMap<GLI.EFCore.Submission.A1FindJurisdictionNamefrom_Let, A1Data>()
				.ForMember(dest => dest._destroy, opt => opt.Ignore())
				.ForMember(dest => dest.Field4Typ, opt => opt.MapFrom(src => src.Field4Typ))
				.ForMember(dest => dest.Field5Dir, opt => opt.MapFrom(src => src.Field5Dir))
				.ForMember(dest => dest.Field6Juris, opt => opt.MapFrom(src => src.Field6Juris));
			CreateMap<tbl_lst_BusinessOwner, BusinessOwnersGridRecord>()
				.ForMember(dest => dest.Active, opt => opt.MapFrom(src => src.Active))
				.ForMember(dest => dest.Code, opt => opt.MapFrom(src => src.Code))
				.ForMember(dest => dest.Description, opt => opt.MapFrom(src => src.Description))
				.ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
				.ForMember(dest => dest.CreatedBy, opt => opt.MapFrom(src => src.CreatedBy))
				.ForMember(dest => dest.CreatedOn, opt => opt.MapFrom(src => src.CreatedOn))
				.ForMember(dest => dest.EditedBy, opt => opt.MapFrom(src => src.EditedBy))
				.ForMember(dest => dest.EditedOn, opt => opt.Ignore())
				.ForMember(dest => dest.Edit, opt => opt.Ignore());
			CreateMap<tbl_lst_fileJuris, JurisdictionsGridRecord>()
				.ForMember(dest => dest.Edit, opt => opt.Ignore());
			CreateMap<GLI.EFCore.Submission.tbl_lst_fileManu, ManufacturersEditViewModel>()
				.ForMember(dest => dest.Mode, opt => opt.Ignore())
				.ForMember(dest => dest.CountryId, opt => opt.Ignore())
				.ForMember(dest => dest.Country, opt => opt.Ignore())
				.ForMember(dest => dest.ManufacturerGroupIds, opt => opt.Ignore())
				.ForMember(dest => dest.ManufacturerGroups, opt => opt.Ignore())
				.ForMember(dest => dest.SendEmail, opt => opt.Ignore())
				.ForMember(dest => dest.AccountStatus, opt => opt.MapFrom(src => src.AccountStatus.Status))
				.ForMember(dest => dest.LiaisonUserIds, opt => opt.Ignore())
				.ForMember(dest => dest.LiaisonUsers, opt => opt.Ignore())
				.ForMember(dest => dest.TestingTypeIds, opt => opt.Ignore())
				.ForMember(dest => dest.TestingTypes, opt => opt.Ignore())
				.ForMember(dest => dest.JurisdictionIds, opt => opt.Ignore())
				.ForMember(dest => dest.Jurisdictions, opt => opt.Ignore())
				.ForMember(dest => dest.BillingCompanyIds, opt => opt.Ignore())
				.ForMember(dest => dest.BillingCompanies, opt => opt.Ignore())
				.ForMember(dest => dest.Expedite, opt => opt.Ignore())
				.ForMember(dest => dest.SendForApprovalJurisdictionIds, opt => opt.Ignore())
				.ForMember(dest => dest.SendForApprovalJurisdictions, opt => opt.Ignore())
				.ForMember(dest => dest.QAOfficeDefault, opt => opt.Ignore())
				.ForMember(dest => dest.QAOfficeId, opt => opt.Ignore());
			CreateMap<tbl_lst_fileManu, ManufacturerGridRecord>()
				.ForMember(dest => dest.Edit, opt => opt.Ignore());
			CreateMap<tbl_lst_Juris_TargetOffSet, TargetOffSetGridRecord>()
				.ForMember(dest => dest.JurisdictionId, opt => opt.MapFrom(src => src.JurId))
				.ForMember(dest => dest.JurisdictionName, opt => opt.MapFrom(src => src.Jurisdiction.Name))
				.ForMember(dest => dest.Manufacturer, opt => opt.MapFrom(src => src.Manufacturer))
				.ForMember(dest => dest.Reason, opt => opt.MapFrom(src => src.Reason))
				.ForMember(dest => dest.OffSetDays, opt => opt.MapFrom(src => src.TargetDayOffSet));
			CreateMap<tbl_lst_Teams, TeamsGridRecord>()
				.ForMember(dest => dest.Edit, opt => opt.Ignore());
			CreateMap<GLI.EFCore.Submission.tbl_lst_Teams, TeamsEditViewModel>()
				.ForMember(dest => dest.TeamUsersGrid, opt => opt.Ignore())
				.ForMember(dest => dest.ModalTeamsGrid, opt => opt.Ignore())
				.ForMember(dest => dest.Mode, opt => opt.Ignore());
			CreateMap<trLogin, TeamUsersGridRecord>()
				.ForMember(dest => dest.Location, opt => opt.MapFrom(src => src.Location.Location + " - " + src.Location.Name))
				.ForMember(dest => dest.Department, opt => opt.MapFrom(src => src.Department.Code + " - " + src.Department.Description))
				.ForMember(dest => dest.SupplierFocus, opt => opt.MapFrom(src => src.SupplierFocus.Description))
				.ForMember(dest => dest.FirstName, opt => opt.MapFrom(src => src.FName))
				.ForMember(dest => dest.LastName, opt => opt.MapFrom(src => src.LName))
				.ForMember(dest => dest.Edit, opt => opt.Ignore());
			CreateMap<trLogin, SupplierFocusUsersGridRecord>()
				.ForMember(dest => dest.Location, opt => opt.MapFrom(src => src.Location.Location + " - " + src.Location.Name))
				.ForMember(dest => dest.Department, opt => opt.MapFrom(src => src.Department.Code + " - " + src.Department.Description))
				.ForMember(dest => dest.Team, opt => opt.MapFrom(src => src.Team.Description))
				.ForMember(dest => dest.FirstName, opt => opt.MapFrom(src => src.FName))
				.ForMember(dest => dest.LastName, opt => opt.MapFrom(src => src.LName))
				.ForMember(dest => dest.Edit, opt => opt.Ignore());
			CreateMap<trLogin, UserGridRecord>()
				.ForMember(dest => dest.CurrentOffice, opt => opt.MapFrom(src => src.Location.Location))
				.ForMember(dest => dest.HomeOffice, opt => opt.MapFrom(src => src.HomeOffice.Location))
				.ForMember(dest => dest.StringId, opt => opt.MapFrom(src => src.Id.ToString()))
				.ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.FName + " " + src.LName))
				.ForMember(dest => dest.Edit, opt => opt.Ignore());
			CreateMap<GLI.EFCore.Submission.trLogin, UsersEditViewModel>()
				.ForMember(dest => dest.Location, opt => opt.MapFrom(src => src.Location.Location + " - " + src.Location.Name))
				.ForMember(dest => dest.HomeOffice, opt => opt.MapFrom(src => src.HomeOffice.Location + " - " + src.HomeOffice.Name))
				.ForMember(dest => dest.Manager, opt => opt.MapFrom(src => src.Manager.FName + " " + src.Manager.LName))
				.ForMember(dest => dest.Department, opt => opt.MapFrom(src => src.Department.Code + " - " + src.Department.Description))
				.ForMember(dest => dest.Team, opt => opt.MapFrom(src => src.Team.Code + " - " + src.Team.Description))
				.ForMember(dest => dest.SupplierFocus, opt => opt.MapFrom(src => src.SupplierFocus.Description))
				.ForMember(dest => dest.BusinessOwner, opt => opt.MapFrom(src => src.BussinessOwner.Description))
				.ForMember(dest => dest.Role, opt => opt.MapFrom(src => src.Role.Description))
				.ForMember(dest => dest.Status, opt => opt.MapFrom(src => src.Status == "A" ? true : false))
				.ForMember(dest => dest.AccessRights, opt => opt.Ignore())
				.ForMember(dest => dest.PermissionId, opt => opt.Ignore())
				.ForMember(dest => dest.Active, opt => opt.Ignore())
				.ForMember(dest => dest.Mode, opt => opt.Ignore())
				.ForMember(dest => dest.ProfilePictures, opt => opt.Ignore())
				.ForMember(dest => dest.ProfilePicturesUpload, opt => opt.Ignore())
				.ForMember(dest => dest.WorkdaySchedule, opt => opt.Ignore())
				.ForMember(dest => dest.OldPermissions, opt => opt.Ignore())
				.ForMember(dest => dest.EngineeringDeptId, opt => opt.Ignore());
			CreateMap<GLI.EFCore.Submission.trPermission, OldPermissions>();
			CreateMap<GLI.EFCore.Submission.tbl_lst_SignatureScope, SignatureScopesViewModel>()
				.ForMember(dest => dest.Mode, opt => opt.Ignore());
			CreateMap<GLI.EFCore.Submission.tbl_lst_SignatureTypes, Areas.Lookups.Models.SignatureType>()
				.ForMember(dest => dest.DefaultVersion, opt => opt.MapFrom(src => src.DefaultVersion.Version));
			CreateMap<GLI.EFCore.Submission.tbl_lst_SignatureTypes, SignatureTypesEditViewModel>()
				.ForMember(dest => dest.AvailableVersionIds, opt => opt.Ignore())
				.ForMember(dest => dest.AvailableVersions, opt => opt.Ignore())
				.ForMember(dest => dest.Mode, opt => opt.Ignore())
				.ForMember(dest => dest.DefaultVersion, opt => opt.MapFrom(src => src.DefaultVersion.Version));
			CreateMap<GLI.EFCore.Submission.tbl_lst_SignatureVersions, SignatureVersionsViewModel>()
				.ForMember(dest => dest.Mode, opt => opt.Ignore());
			CreateMap<GLI.EFCore.Submission.Roles, RolesViewModel>()
				.ForMember(dest => dest.AccessRights, opt => opt.Ignore())
				.ForMember(dest => dest.DepartmentName, opt => opt.Ignore())
				.ForMember(dest => dest.Mode, opt => opt.Ignore());
			CreateMap<GLI.EFCore.Submission.ManufacturerGroupCategories, Areas.Lookups.Models.ManufacturerGroupCategory>();
			CreateMap<tbl_lst_JurisdictionMappings, ManufacturerJurisdictionCodeMappingRecord>()
				.ForMember(dest => dest.MappedJurisdictionId, opt => opt.MapFrom(src => src.ExternalJurisdictionId))
				.ForMember(dest => dest.MappedJurisdictionCode, opt => opt.MapFrom(src => src.ExternalJurisdictionCode))
				.ForMember(dest => dest.MappedJurisdictionName, opt => opt.MapFrom(src => src.ExternalJurisdictionDescription))
				.ForMember(dest => dest.ManufacturerShortID, opt => opt.MapFrom(src => src.ManufacturerCode))
				.ForMember(dest => dest.JurisdictionName, opt => opt.MapFrom(src => src.Jurisdiction.Name))
				.ForMember(dest => dest.Edit, opt => opt.Ignore());
			CreateMap<tbl_lst_AccountingDefaults, AccountingDefaults>();
			CreateMap<GLI.EFCore.Submission.tbl_lst_JurisdictionMappings, JurisdictionMappingEditViewModel>()
				.ForMember(dest => dest.Manufacturer, opt => opt.MapFrom(src => src.Manufacturer.Description))
				.ForMember(dest => dest.Jurisdiction, opt => opt.MapFrom(src => src.Jurisdiction.Name))
				.ForMember(dest => dest.SportsJurisdiction, opt => opt.MapFrom(src => src.SportsJurisdiction.Name))
				.ForMember(dest => dest.AccountingDefaults, opt => opt.MapFrom(src => src.AccountingDefaults))
				.ForMember(dest => dest.Mode, opt => opt.Ignore());
			CreateMap<tbl_lst_MobileDevice, MobileDeviceGridRecord>()
				.ForMember(dest => dest.Location, opt => opt.MapFrom(src => src.Location.Location))
				.ForMember(dest => dest.Edit, opt => opt.Ignore());
			CreateMap<tbl_lst_MobileDevice, MobileDevicesEditViewModel>()
				.ForMember(dest => dest.Location, opt => opt.MapFrom(src => src.Location.Location))
				.ForMember(dest => dest.LocationId, opt => opt.MapFrom(src => src.Location.Id))
				.ForMember(dest => dest.Active, opt => opt.Ignore())
				.ForMember(dest => dest.DeviceType, opt => opt.MapFrom(src => src.Type))
				.ForMember(dest => dest.Mode, opt => opt.Ignore());
			CreateMap<trTaskDef, TaskDefinitionsGridRecord>()
				.ForMember(dest => dest.Active, opt => opt.MapFrom(src => src.Disabled == 0))
				.ForMember(dest => dest.Edit, opt => opt.Ignore());
			CreateMap<GLI.EFCore.Submission.trTaskDef, TaskDefinitionsEditViewModel>()
				.ForMember(dest => dest.Department, opt => opt.Ignore())
				.ForMember(dest => dest.DefaultDocument, opt => opt.Ignore())
				.ForMember(dest => dest.Documents, opt => opt.Ignore())
				.ForMember(dest => dest.EstimateHours, opt => opt.MapFrom(src => src.EstimateHours == 0 ? null : src.EstimateHours))
				.ForMember(dest => dest.EstimateInterval, opt => opt.MapFrom(src => src.EstimateInterval == 0 ? null : src.EstimateInterval))
				.ForMember(dest => dest.Active, opt => opt.MapFrom(src => src.Disabled == 1 ? false : true))
				.ForMember(dest => dest.IsAcumaticaTask, opt => opt.MapFrom(src => src.IsAcumaticaSet))
				.ForMember(dest => dest.Mode, opt => opt.Ignore());
			CreateMap<GLI.EFCore.Submission.trTaskDoc, TaskDocumentsEditViewModel>()
				.ForMember(dest => dest.Active, opt => opt.Ignore())
				.ForMember(dest => dest.Mode, opt => opt.Ignore());
			CreateMap<trTaskDoc, TaskDocumentsGridRecord>()
				.ForMember(dest => dest.Edit, opt => opt.Ignore());
			CreateMap<JurisdictionGroups, JurisdictionGroupGridRecord>()
				.ForMember(dest => dest.Edit, opt => opt.Ignore())
				.ForMember(dest => dest.Delete, opt => opt.Ignore());
			CreateMap<JurisdictionGroupsEditViewModel, GLI.EFCore.Submission.JurisdictionGroups>()
				.ForMember(dest => dest.JurisdictionLinks, opt => opt.Ignore());
			CreateMap<JurisdictionGroupCategories, JurisdictionGroupCategoriesGridRecord>()
				.ForMember(dest => dest.Edit, opt => opt.Ignore())
				.ForMember(dest => dest.Delete, opt => opt.Ignore());
			CreateMap<GLI.EFCore.Submission.JurisdictionGroupCategories, JurisdictionGroupCategoriesEditViewModel>()
				.ForMember(dest => dest.Mode, opt => opt.Ignore());
			CreateMap<GLI.EFCore.Submission.tbl_lst_StandardsAll, StandardViewModel>()
				.ForMember(dest => dest.Jurisdiction, opt => opt.MapFrom(src => src.Jurisdiction.Name));
			CreateMap<GLI.EFCore.Submission.tbl_lst_StandardsAll, StandardsEditViewModel>()
				.ForMember(dest => dest.Jurisdiction, opt => opt.MapFrom(src => src.Jurisdiction.Name))
				.ForMember(dest => dest.Mode, opt => opt.Ignore());
			CreateMap<GLI.EFCore.Submission.trTaskTemplate, TaskTemplatesViewModel>()
				.ForMember(dest => dest.Mode, opt => opt.Ignore())
				.ForMember(dest => dest.Active, opt => opt.Ignore())
				.ForMember(dest => dest.UserName, opt => opt.MapFrom(src => src.User.FName + " " + src.User.LName))
				.ForMember(dest => dest.Tasks, opt => opt.Ignore())
				.ForMember(dest => dest.CanSave, opt => opt.Ignore());
			CreateMap<trTaskTemplate, TaskTemplateGridRecord>()
				.ForMember(dest => dest.Edit, opt => opt.Ignore())
				.ForMember(dest => dest.UserName, opt => opt.MapFrom(src => src.User.FName + " " + src.User.LName))
				.ForMember(dest => dest.Template, opt => opt.MapFrom(src => src.Code));
			CreateMap<GLI.EFCore.Submission.tbl_lst_Letterheads, Letterhead>();
			CreateMap<GLI.EFCore.Submission.tbl_lst_Letterheads, LetterheadsEditViewModel>()
				.ForMember(dest => dest.LaboratoryIds, opt => opt.Ignore())
				.ForMember(dest => dest.Laboratories, opt => opt.Ignore())
				.ForMember(dest => dest.Mode, opt => opt.Ignore())
				.ForMember(dest => dest.GPSFilePath, opt => opt.Ignore())
				.ForMember(dest => dest.GPSFileName, opt => opt.Ignore())
				.ForMember(dest => dest.LetterheadFile, opt => opt.Ignore())
				.ForMember(dest => dest.LetterheadFileUpload, opt => opt.Ignore());
			CreateMap<GLI.EFCore.Submission.trLaboratory, Areas.Lookups.Models.Laboratory>();
			CreateMap<GLI.EFCore.Submission.tbl_lst_EPROMsize, ChipTypeViewModel>()
				.ForMember(dest => dest.Mode, opt => opt.Ignore());
			CreateMap<GLI.EFCore.Submission.RegCertId_JurisdictionalData, KSANumberIndexViewModel>()
			 .ForMember(dest => dest.CertNumber, opt => opt.MapFrom(src => src.CertificationNumber.CertNumber))
			 .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.CertificationNumber.Id))
			 .ForMember(dest => dest.Primary, opt => opt.Ignore())
			 .ForMember(dest => dest.FileNumber, opt => opt.Ignore())
			 .ForMember(dest => dest.Jurisdiction, opt => opt.Ignore());
			CreateMap<GLI.EFCore.Submission.trDept, DepartmentViewModel>()
			 .ForMember(dest => dest.Mode, opt => opt.Ignore());
			CreateMap<GLI.EFCore.Submission.tbl_lst_Classifications, ClassificationsViewModel>()
				.ForMember(dest => dest.Mode, opt => opt.Ignore())
				.ForMember(dest => dest.ClassificationTypeName, opt => opt.MapFrom(src => src.ClassificationTypes.Name))
				.ForMember(dest => dest.ManufacturerName, opt => opt.MapFrom(src => src.Manufacturers.Description));
			CreateMap<GLI.EFCore.Submission.tbl_lst_ClassificationTypes, ClassificationTypesViewModel>()
				.ForMember(dest => dest.Mode, opt => opt.Ignore());
			CreateMap<GLI.EFCore.Submission.tbl_lst_Country, CountryViewModel>()
				.ForMember(dest => dest.Mode, opt => opt.Ignore());
			CreateMap<GLI.EFCore.Submission.tbl_lst_CompilationMethod, CompilationMethodViewModel>()
				.ForMember(dest => dest.Mode, opt => opt.Ignore());
			CreateMap<GLI.EFCore.Submission.tbl_lst_CPReviewTasks, CPReviewTasksViewModel>()
				.ForMember(dest => dest.Jurisdiction, opt => opt.MapFrom(src => src.Jurisdiction.Name))
				.ForMember(dest => dest.Mode, opt => opt.Ignore());
			CreateMap<GLI.EFCore.Submission.tbl_Help, HelpItem>();
			CreateMap<GLI.EFCore.Submission.tbl_lst_GamingGuidelineAttributes, tbl_lst_GamingGuidelineAttributeViewModel>()
				.ForMember(dest => dest.Mode, opt => opt.Ignore());
			CreateMap<GLI.EFCore.Submission.tbl_lst_GamingGuidelineGameTypes, tbl_lst_GamingGuidelineGameTypesViewModel>()
				.ForMember(dest => dest.Mode, opt => opt.Ignore());
			CreateMap<GLI.EFCore.Submission.tbl_lst_GamingGuidelineLimitType, GamingGuidelineLimitsViewModel>()
				.ForMember(dest => dest.Mode, opt => opt.Ignore());
			CreateMap<GLI.EFCore.Submission.tbl_lst_GamingGuidelineProtocolType, GamingGuidelineProtocolsViewModel>()
				.ForMember(dest => dest.Mode, opt => opt.Ignore());
			CreateMap<tbl_lst_GamingGuidelines, GamingGuidelineViewModel>()
				.ForMember(dest => dest.Mode, opt => opt.Ignore())
				.ForMember(dest => dest.DefaultGamingGuidelineLimit, opt => opt.Ignore())
				.ForMember(dest => dest.DefaultGamingGuidelineAttribute, opt => opt.Ignore())
				.ForMember(dest => dest.GamingGuidelineAttributeStatus, opt => opt.Ignore())
				.ForMember(dest => dest.GamingGuidelineAwardTypes, opt => opt.Ignore())
				.ForMember(dest => dest.GamingGuidelinePrizeTypes, opt => opt.Ignore())
				.ForMember(dest => dest.GamingGuidelineProgressiveTypes, opt => opt.Ignore())
				.ForMember(dest => dest.DefaultGamingGuidelineOddsRequirement, opt => opt.Ignore())
				.ForMember(dest => dest.DefaultGamingGuidelineProhibitedGameType, opt => opt.Ignore())
				.ForMember(dest => dest.GamingGuidelineProhibitedGameTypeStatus, opt => opt.Ignore())
				.ForMember(dest => dest.DefaultGamingGuidelineProtocolType, opt => opt.Ignore())
				.ForMember(dest => dest.GamingGuidelineTechTypeStatus, opt => opt.Ignore())
				.ForMember(dest => dest.DefaultGamingGuidelineTechType, opt => opt.Ignore())
				.ForMember(dest => dest.GamingGuidelineRTPType, opt => opt.Ignore())
				.ForMember(dest => dest.DefaultGamingGuidelineRTPRequirement, opt => opt.Ignore())
				.ForMember(dest => dest.JurisdictionOptions, opt => opt.Ignore())
				.ForMember(dest => dest.SelectedJurisdiction, opt => opt.Ignore())
				.ForMember(dest => dest.JurisdictionName, opt => opt.Ignore())
				.ForMember(dest => dest.NonCompliantPaytableHandlingOptions, opt => opt.Ignore())
				.ForMember(dest => dest.WirelessTechnologyOptions, opt => opt.Ignore())
				.ForMember(dest => dest.SkillGamesOptions, opt => opt.Ignore())
				.ForMember(dest => dest.BonusPotOptions, opt => opt.Ignore())
				.ForMember(dest => dest.RandomNumberGeneratorOptions, opt => opt.Ignore())
				.ForMember(dest => dest.GeographicalTestingLocationOnlyOptions, opt => opt.Ignore())
				.ForMember(dest => dest.PhysicalTestingLocationOptions, opt => opt.Ignore())
				.ForMember(dest => dest.Country, opt => opt.Ignore())
				.ForMember(dest => dest.UserComments, opt => opt.Ignore())
				.ForMember(dest => dest.DefaultUserComment, opt => opt.Ignore())
				.ForMember(dest => dest.CompilationMethodIds, opt => opt.Ignore())
				.ForMember(dest => dest.CompilationMethods, opt => opt.Ignore())
				.ForMember(dest => dest.NonCompliantPaytableHandling,
				opt => opt.MapFrom(source => (Common.GamingGuidelineNonCompliantPaytableHandling)source.NonCompliantPaytableHandling))
				.ForMember(dest => dest.WirelessTechnology,
				opt => opt.MapFrom(source => (Common.GamingGuidelineAllowedProhibited)source.WirelessTechnology))
				.ForMember(dest => dest.SkillGames,
				opt => opt.MapFrom(source => (Common.GamingGuidelineAllowedProhibited)source.SkillGames))
				.ForMember(dest => dest.BonusPot,
				opt => opt.MapFrom(source => (Common.GamingGuidelineBonusPot)source.BonusPot))
				.ForMember(dest => dest.RandomNumberGenerator,
				opt => opt.MapFrom(source => (Common.GamingGuidelineRandomNumberGenerator)source.RandomNumberGenerator))
				.ForMember(dest => dest.GeographicalTestingLocationOnly,
				opt => opt.MapFrom(source => (Common.GamingGuidelineGeographicalTestingLocationOnly)source.GeographicalTestingLocationOnly))
				.ForMember(dest => dest.PhysicalTestingLocation,
				opt => opt.MapFrom(source => (Common.GamingGuidelinePhysicalTestingLocation)source.PhysicalTestingLocation))
				.ForMember(dest => dest.Owner, opt => opt.Ignore())
				.ForMember(dest => dest.SecondaryOwner, opt => opt.Ignore())
				.ForMember(dest => dest.editAllowed, opt => opt.Ignore())
				.ForMember(dest => dest.CommentCount, opt => opt.Ignore());
			CreateMap<GLI.EFCore.Submission.tbl_lst_GamingGuidelineTechType, GamingGuidelineTechTypeViewModel>()
				.ForMember(dest => dest.Mode, opt => opt.Ignore());
			CreateMap<tbl_lst_GameDescriptionStatuses, GameDescriptionStatusViewModel>()
				.ForMember(dest => dest.Mode, opt => opt.Ignore())
				.ForMember(dest => dest.Active, opt => opt.Ignore());
			CreateMap<tbl_lst_GameDescriptionTypes, GameDescriptionTypeViewModel>()
				.ForMember(dest => dest.Mode, opt => opt.Ignore())
				.ForMember(dest => dest.Active, opt => opt.Ignore());
			CreateMap<GLI.EFCore.Submission.tbl_lst_DocumentTypes, DocumentTypesViewModel>()
				.ForMember(dest => dest.Mode, opt => opt.Ignore())
				.ForMember(dest => dest.Active, opt => opt.Ignore());
			CreateMap<GLI.EFCore.Submission.tbl_lst_ReviewDetailCategories, ProjectReviewDetailCategoriesViewModel>()
				.ForMember(dest => dest.Department, opt => opt.MapFrom(src => src.Department.Code))
				.ForMember(dest => dest.Mode, opt => opt.Ignore());
			CreateMap<GLI.EFCore.Submission.tbl_lst_Vendors, VendorsViewModel>()
				.ForMember(dest => dest.Mode, opt => opt.Ignore());
			CreateMap<GLI.EFCore.Submission.tbl_WorkdaySchedule, WorkdayScheduleViewModel>();
			CreateMap<GLI.EFCore.Submission.GamingGuidelineSportsBetting, GamingGuidelinesSportsBettingEditViewModel>()
				.ForMember(dest => dest.Mode, opt => opt.Ignore())
				.ForMember(dest => dest.JurisdictionName, opt => opt.Ignore())
				.ForMember(dest => dest.DropdownOptions, opt => opt.Ignore())
				.ForMember(dest => dest.DefaultUserComment, opt => opt.Ignore())
				.ForMember(dest => dest.CommentCount, opt => opt.Ignore())
				.ForMember(dest => dest.UserComments, opt => opt.Ignore())
				.ForMember(dest => dest.Location, opt => opt.Ignore());
			CreateMap<GamingGuidelineUserTemplate, GamingGuidelineUserTemplateViewModel>();
			CreateMap<GamingGuidelineUserTemplateViewModel, GamingGuidelineUserTemplate>();
			CreateMap<GLI.EFCore.Submission.Subscription, Areas.Lookups.Models.Subscription>()
				.ForMember(dest => dest.SubscriptionGroup, opt => opt.MapFrom(src => src.SubscriptionGroup.Name));
			CreateMap<GLI.EFCore.Submission.Subscription, SubscriptionEditViewModel>()
				.ForMember(dest => dest.Mode, opt => opt.Ignore())
				.ForMember(dest => dest.CategoryList, opt => opt.Ignore())
				.ForMember(dest => dest.FilterField, opt => opt.Ignore())
				.ForMember(dest => dest.FilterFieldList, opt => opt.Ignore())
				.ForMember(dest => dest.SelectedFilterField, opt => opt.Ignore())
				.ForMember(dest => dest.SubscriptionGroup, opt => opt.MapFrom(src => src.SubscriptionGroup.Name));
			CreateMap<GLI.EFCore.Submission.trTaskEvolutionGroups, TaskEvolutionGroupsViewModel>()
				.ForMember(dest => dest.Mode, opt => opt.Ignore())
				.ForMember(dest => dest.EvolutionTC, opt => opt.Ignore())
				.ForMember(dest => dest.TaskDefinitions, opt => opt.Ignore())
				.ForMember(dest => dest.TaskDefinitionIds, opt => opt.Ignore())
				.ForMember(dest => dest.Active, opt => opt.MapFrom(src => src.Status))
				.ForMember(dest => dest.TaskDefinitionId, opt => opt.MapFrom(src => src.TaskDef.Id))
				.ForMember(dest => dest.TaskDefinition, opt => opt.MapFrom(src => src.TaskDef.Code + (src.TaskDef.DynamicsId == null ? "" : " - " + src.TaskDef.DynamicsId)));
			CreateMap<GLI.EFCore.Submission.trLaboratory, LaboratoryLocations>()
				.ForMember(dest => dest.Company, opt => opt.MapFrom(src => src.Company.Name))
				.ForMember(dest => dest.Country, opt => opt.MapFrom(src => src.Country.Code));
			CreateMap<GLI.EFCore.Submission.trLaboratory, LaboratoriesEditViewModel>()
				.ForMember(dest => dest.Company, opt => opt.MapFrom(src => src.Company.Name))
				.ForMember(dest => dest.Country, opt => opt.MapFrom(src => src.Country.Code))
				.ForMember(dest => dest.AccreditationIds, opt => opt.Ignore())
				.ForMember(dest => dest.Accreditations, opt => opt.Ignore())
				.ForMember(dest => dest.QAOfficeDefault, opt => opt.MapFrom(src => src.Location))
				.ForMember(dest => dest.QAOfficeDefaultId, opt => opt.Ignore())
				.ForMember(dest => dest.Mode, opt => opt.Ignore());
			CreateMap<GLI.EFCore.Submission.tbl_lst_TestEnvironment, TestEnvironment>()
				.ForMember(dest => dest.ManufacturerGroup, opt => opt.MapFrom(src => src.ManufacturerGroup.Description))
				.ForMember(dest => dest.Jurisdiction, opt => opt.MapFrom(src => src.Jurisdiction.Name))
				.ForMember(dest => dest.Platform, opt => opt.MapFrom(src => src.Platform.Name));
			CreateMap<GLI.EFCore.Submission.tbl_lst_TestEnvironment, TestEnvironmentEditViewModel>()
				.ForMember(dest => dest.Mode, opt => opt.Ignore())
				.ForMember(dest => dest.ManufacturerGroup, opt => opt.MapFrom(src => src.ManufacturerGroup.Description))
				.ForMember(dest => dest.Jurisdiction, opt => opt.MapFrom(src => src.Jurisdiction.Name))
				.ForMember(dest => dest.Platform, opt => opt.MapFrom(src => src.Platform.Name));
			CreateMap<GLI.EFCore.Submission.tbl_lst_BillingParty, BillingPartyViewModel>()
				.ForMember(dest => dest.ManufacturerGroup, opt => opt.MapFrom(src => src.ManufacturerGroup.Description))
				.ForMember(dest => dest.TestingType, opt => opt.MapFrom(src => src.TestingType.Description))
				.ForMember(dest => dest.Mode, opt => opt.Ignore());
			CreateMap<GLI.EFCore.Submission.tbl_lst_Cabinet, CabinetViewModel>()
				.ForMember(dest => dest.ManufacturerGroup, opt => opt.MapFrom(src => src.ManufacturerGroup.Description))
				.ForMember(dest => dest.Mode, opt => opt.Ignore());
			CreateMap<GLI.EFCore.Submission.AssociatedSoftwareFootnote, TemplateComponentFootnote>();
			CreateMap<GLI.EFCore.Submission.AssociatedSoftware, TemplateComponent>()
				.ForMember(dest => dest.Component, opt => opt.MapFrom(src => src.Submission))
				.ForMember(dest => dest.ComponentId, opt => opt.MapFrom(src => src.SubmissionId))
				.ForMember(dest => dest.AssociatedSoftwareTemplate, opt => opt.Ignore());
			CreateMap<GLI.EFCore.Submission.AssociatedSoftwareTemplate, Areas.Lookups.Models.AssociatedSoftwareTemplate>()
				.ForMember(dest => dest.Manufacturer, opt => opt.MapFrom(src => src.Manufacturer.Description))
				.ForMember(dest => dest.Jurisdictions, opt => opt.MapFrom(src => src.Jurisdictions.Select(j => j.Jurisdiction)))
				.ForMember(dest => dest.AssociatedSoftwares, opt => opt.Ignore())
				.ForMember(dest => dest.Platform, opt => opt.MapFrom(src => src.Platform.Name));
			//EF
			CreateMap<EF.Submission.AssociatedSoftwareFootnote, TemplateComponentFootnote>();
			CreateMap<EF.Submission.AssociatedSoftware, TemplateComponent>()
				.ForMember(dest => dest.Component, opt => opt.MapFrom(src => src.Submission))
				.ForMember(dest => dest.ComponentId, opt => opt.MapFrom(src => src.SubmissionId))
				.ForMember(dest => dest.AssociatedSoftwareTemplate, opt => opt.Ignore());
			CreateMap<EF.Submission.AssociatedSoftwareTemplate, Areas.Lookups.Models.AssociatedSoftwareTemplate>()
				.ForMember(dest => dest.Manufacturer, opt => opt.MapFrom(src => src.Manufacturer.Description))
				.ForMember(dest => dest.Jurisdictions, opt => opt.MapFrom(src => src.Jurisdictions.Select(j => j.Jurisdiction)))
				.ForMember(dest => dest.AssociatedSoftwares, opt => opt.Ignore())
				.ForMember(dest => dest.Platform, opt => opt.MapFrom(src => src.Platform.Name));

			CreateMap<tbl_lst_fileJuris, JurisdictionInfo>()
				.ForMember(dest => dest.Jurisdiction, opt => opt.MapFrom(src => src.Name))
				.ForMember(dest => dest.JurisdictionId, opt => opt.MapFrom(src => (int)src.FixId));
			CreateMap<tbl_lst_Platform, ManufacturerPlatform>();
			CreateMap<PlatformPiece, PlatformPieceDTO>()
				.ForMember(dest => dest.Deleted, opt => opt.Ignore())
				.ForMember(dest => dest.Modified, opt => opt.Ignore());
			CreateMap<PlatformPiecesTemplate, PlatformPiecesTemplateDTO>()
				.ForMember(dest => dest.Manufacturer, opt => opt.MapFrom(src => src.Manufacturer.Description))
				.ForMember(dest => dest.Jurisdictions, opt => opt.MapFrom(src => src.Jurisdictions.Select(j => j.Jurisdiction)));
			CreateMap<PlatformPiecesTemplate, PlatformPiecesEditViewModel>()
				.ForMember(dest => dest.Mode, opt => opt.Ignore())
				.ForMember(dest => dest.Manufacturer, opt => opt.MapFrom(src => src.Manufacturer.Description))
				.ForMember(dest => dest.Jurisdictions, opt => opt.MapFrom(src => src.Jurisdictions.Select(j => j.Jurisdiction)));
			//EF Core
			CreateMap<GLI.EFCore.Submission.tbl_lst_fileJuris, JurisdictionInfo>()
				.ForMember(dest => dest.Jurisdiction, opt => opt.MapFrom(src => src.Name))
				.ForMember(dest => dest.JurisdictionId, opt => opt.MapFrom(src => (int)src.FixId));
			CreateMap<GLI.EFCore.Submission.tbl_lst_Platform, ManufacturerPlatform>();
			CreateMap<GLI.EFCore.Submission.PlatformPiece, PlatformPieceDTO>()
				.ForMember(dest => dest.Deleted, opt => opt.Ignore())
				.ForMember(dest => dest.Modified, opt => opt.Ignore());
			CreateMap<GLI.EFCore.Submission.PlatformPiecesTemplate, PlatformPiecesTemplateDTO>()
				.ForMember(dest => dest.Manufacturer, opt => opt.MapFrom(src => src.Manufacturer.Description))
				.ForMember(dest => dest.Jurisdictions, opt => opt.MapFrom(src => src.Jurisdictions.Select(j => j.Jurisdiction)));
			CreateMap<GLI.EFCore.Submission.PlatformPiecesTemplate, PlatformPiecesEditViewModel>()
				.ForMember(dest => dest.Mode, opt => opt.Ignore())
				.ForMember(dest => dest.Manufacturer, opt => opt.MapFrom(src => src.Manufacturer.Description))
				.ForMember(dest => dest.Jurisdictions, opt => opt.MapFrom(src => src.Jurisdictions.Select(j => j.Jurisdiction)));
			CreateMap<trLogin, BusinessOwnerUsersGridRecord>()
				.ForMember(dest => dest.Location, opt => opt.MapFrom(src => src.Location.Location + " - " + src.Location.Name))
				.ForMember(dest => dest.Department, opt => opt.MapFrom(src => src.Department.Code + " - " + src.Department.Description))
				.ForMember(dest => dest.FirstName, opt => opt.MapFrom(src => src.FName))
				.ForMember(dest => dest.LastName, opt => opt.MapFrom(src => src.LName))
				.ForMember(dest => dest.Edit, opt => opt.Ignore());
		}
	}
}