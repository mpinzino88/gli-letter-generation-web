﻿using Submissions.Core.Models.DTO;
using Submissions.Web.Areas.Tools.Models;
using System.Collections.Generic;

namespace Submissions.Web.Areas.Lookups.Models
{
	public class AssociatedSoftwareTemplate
	{
		public AssociatedSoftwareTemplate()
		{
			AssociatedSoftwares = new List<TemplateComponent>();
			Jurisdictions = new List<JurisdictionInfo>();
		}
		public int Id { get; set; }
		public int OriginalId { get; set; }
		public string Name { get; set; }
		public string Description { get; set; }
		public string Manufacturer { get; set; }
		public string ManufacturerCode { get; set; }
		public IList<JurisdictionInfo> Jurisdictions { get; set; }
		public string Platform { get; set; }
		public int PlatformId { get; set; }
		//public List<string> PlatformId { get; set; }
		public IList<TemplateComponent> AssociatedSoftwares { get; set; }
	}

	public class TemplateComponent
	{
		public TemplateComponent()
		{
			this.FootNotes = new List<TemplateComponentFootnote>();
		}

		public int Id { get; set; }
		public string Description { get; set; }
		public int OrderId { get; set; }
		public int ComponentId { get; set; }
		public SubmissionDTO Component { get; set; }
		public IList<TemplateComponentFootnote> FootNotes { get; set; }
		public AssociatedSoftwareTemplate AssociatedSoftwareTemplate { get; set; }
		public int PlatformPieceId { get; set; }
	}

	public class TemplateComponentFootnote
	{
		public int Id { get; set; }
		public string Memo { get; set; }
	}

	public class TemplateComponentPlatformPiecesDTO
	{
		public IList<TemplateComponent> Components { get; set; }
		public IList<PlatformPiecesTemplateDTO> PlatformPiecesTemplates { get; set; }
		public TemplateComponentPlatformPiecesDTO()
		{
			Components = new List<TemplateComponent>();
			PlatformPiecesTemplates = new List<PlatformPiecesTemplateDTO>();
		}
	}
}