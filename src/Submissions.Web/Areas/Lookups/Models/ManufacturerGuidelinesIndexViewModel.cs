﻿using System.Collections.Generic;
using System.Web.Mvc;

namespace Submissions.Web.Areas.Lookups.Models
{
	public class ManufacturerGuidelinesIndexViewModel
	{
		public ManufacturerGuidelinesIndexViewModel()
		{
			this.Rows = new List<ManufacturerGuidelinesRow>();
		}
		public List<ManufacturerGuidelinesRow> Rows { get; set; }
	}

	public class ManufacturerGuidelinesRow
	{
		public ManufacturerGuidelinesRow()
		{
			this.GamingGuidelineOptions = new List<SelectListItem>();
			this.ManufacturerOptions = new List<SelectListItem>();
		}
		public int Id { get; set; }
		public string Jurisdiction { get; set; } // From GamingGuideline.Jurisdiction.Name
		public int GamingGuidelineId { get; set; }
		public string ManufacturerId { get; set; }
		public int MaxRTP { get; set; }
		public int MinRTP { get; set; }
		public List<SelectListItem> GamingGuidelineOptions { get; set; }
		public List<SelectListItem> ManufacturerOptions { get; set; }
	}
}