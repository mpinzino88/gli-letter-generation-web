﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Submissions.Web.Areas.Lookups.Models
{
	public class SignatureTypesIndexViewModel
	{
		public IList<SignatureType> SignatureTypes { get; set; }
	}

	public class SignatureTypesEditViewModel : Lookup
	{
		public SignatureTypesEditViewModel()
		{
			this.AvailableVersions = new List<SelectListItem>();
		}

		public int? Id { get; set; }
		[Required, StringLength(50), Remote("ValidateType", "SignatureTypes", AdditionalFields = "Id, Mode", ErrorMessage = "{0} already exists.")]
		public string Type { get; set; }
		[Required, StringLength(10)]
		public string Length { get; set; }
		[Display(Name = "Default Version")]
		public int? DefaultVersionId { get; set; }
		public string DefaultVersion { get; set; }
		[Display(Name = "Available Versions")]
		public string[] AvailableVersionIds { get; set; }
		public IList<SelectListItem> AvailableVersions { get; set; }
	}

	public class SignatureType
	{
		public int Id { get; set; }
		public string Type { get; set; }
		[Display(Name = "Default Version")]
		public string DefaultVersion { get; set; }
		public string Length { get; set; }
		public bool Active { get; set; }
	}
}