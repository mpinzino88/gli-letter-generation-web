﻿using Submissions.Web.Models.Grids.Lookups;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Submissions.Web.Areas.Lookups.Models
{
	public class ManufacturersIndexViewModel
	{
		public ManufacturersIndexViewModel()
		{
			this.ManufacturerGrid = new ManufacturersGrid();
		}

		public ManufacturersGrid ManufacturerGrid;
	}

	public class ManufacturersEditViewModel : Lookup
	{
		private string _code;

		public ManufacturersEditViewModel()
		{
			this.ManufacturerGroups = new List<SelectListItem>();
			this.LiaisonUserIds = new string[] { };
			this.LiaisonUsers = new List<SelectListItem>();
			this.TestingTypeIds = new string[] { };
			this.TestingTypes = new List<SelectListItem>();
			this.Jurisdictions = new List<SelectListItem>();
			this.BillingCompanies = new List<SelectListItem>();
			this.SendForApprovalJurisdictionIds = new string[] { };
			this.SendForApprovalJurisdictions = new List<SelectListItem>();
		}

		public Guid? Id { get; set; }
		[Required, StringLength(3), Remote("ValidateCode", "Manufacturers", AdditionalFields = "Id, Mode", ErrorMessage = "{0} already exists.")]
		[RegularExpression("^[a-zA-Z0-9]*$", ErrorMessage = "Please enter a alphanumeric code only")]
		public string Code
		{
			get
			{
				return _code;
			}
			set
			{
				_code = value.ToUpper();
			}
		}
		[Required, StringLength(120), Remote("ValidateDescription", "Manufacturers", AdditionalFields = "Id, Mode", ErrorMessage = "{0} already exists.")]
		public string Description { get; set; }
		[StringLength(30)]
		public string City { get; set; }
		public string State { get; set; }
		[Display(Name = "Country")]
		public int? CountryId { get; set; }
		public string Country { get; set; }
		[StringLength(3)]
		public string Territory { get; set; }
		[Display(Name = "Account Status"), Required]
		public int? AccountStatusId { get; set; }
		public string AccountStatus { get; set; }
		public bool PendingApproval { get; set; }
		[Display(Name = "Manufacturer Groups")]
		public string[] ManufacturerGroupIds { get; set; }
		public IList<SelectListItem> ManufacturerGroups { get; set; }
		public bool SendEmail { get; set; }
		public string Email { get; set; }
		[Display(Name = "Binary Request Group Emails")]
		public string BinaryRequestGroup { get; set; }
		[Display(Name = "Liaisons")]
		public string[] LiaisonUserIds { get; set; }
		public IList<SelectListItem> LiaisonUsers { get; set; }
		[Display(Name = "Default QA Office")]
		public int? QAOfficeId { get; set; }
		public string QAOfficeDefault { get; set; }

		// options
		[Display(Name = "For Dynamics Only")]
		public bool ForDynamicsUseOnly { get; set; }
		[Display(Name = "Draft Supplier")]
		public bool IsDraftSupplier { get; set; }
		// the jurisdictions listed here (Illinois currently) should be regulators that require draft approval/PCR
		// by default, the letter will automatically be available to the regulator via the normal PCR process
		// for any jurisdictions selected here, the manufacturer will have to log into GLIAccess and manually send the letter to the regulator for approval (ie. send it to the regulator PCR queue)
		// this option will set the letter/jurisdictionaldata status to PA (pending approval which is sort of like DR) for the given jurisdictions
		// only GLIAccess manufacturer users can see PA jurisdictionaldata
		[Display(Name = "Manually Send Letter for Approval Jurisdictions")]
		public string[] SendForApprovalJurisdictionIds { get; set; }
		public IList<SelectListItem> SendForApprovalJurisdictions { get; set; }
		[Display(Name = "Use Delayed Certifications")]
		public bool UseDelayedCertifications { get; set; }
		[Display(Name = "Testing Types")]
		public string[] TestingTypeIds { get; set; }
		public IList<SelectListItem> TestingTypes { get; set; }

		// accounting
		[Display(Name = "Product Submitter List Id")]
		public int? ProductSubmitterListId { get; set; }
		[Display(Name = "Jurisdiction")]
		public string[] JurisdictionIds { get; set; }
		public IList<SelectListItem> Jurisdictions { get; set; }
		[Display(Name = "Billing Companies")]
		public string[] BillingCompanyIds { get; set; }
		public IList<SelectListItem> BillingCompanies { get; set; }
		public bool Expedite { get; set; }
	}
}