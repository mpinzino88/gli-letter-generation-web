﻿namespace Submissions.Web.Areas.Lookups.Models
{
	public class FormatFilterViewModel : Lookup
	{
		public int Id { get; set; }
		public string Name { get; set; }
		public string Description { get; set; }
		public string FilterText { get; set; }
	}
}