﻿using Submissions.Web.Models.Grids.Lookups;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Submissions.Web.Areas.Lookups.Models
{
	public class TaskDocumentsIndexViewModel
	{
		public TaskDocumentsIndexViewModel()
		{
			this.TaskDocumentsGrid = new TaskDocumentsGrid();
		}

		public TaskDocumentsGrid TaskDocumentsGrid { get; set; }
	}

	public class TaskDocumentsEditViewModel : Lookup
	{
		public int? Id { get; set; }
		[Required, StringLength(100), Remote("ValidateName", "TaskDocuments", AdditionalFields = "Id, Mode", ErrorMessage = "{0} already exists.")]
		public string Name { get; set; }
		[Required, StringLength(1024)]
		public string Location { get; set; }
	}
}