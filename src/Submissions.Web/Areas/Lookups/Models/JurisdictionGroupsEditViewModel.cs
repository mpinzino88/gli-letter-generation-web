﻿using EF.Submission;
using Submissions.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Submissions.Web.Areas.Lookups.Models
{
	public class JurisdictionGroupsEditViewModel
	{
		public int Id { get; set; }

		[DisplayName("Group Name"), Required, StringLength(150)]
		public string Code { get; set; }

		[DisplayName("Description"), Required, StringLength(300)]
		public string Description { get; set; }

		[DisplayName("Category")]
		public JurisdictionGroupCategories Category { get; set; }

		public int? JurisdictionGroupCategoryId { get; set; }

		[DisplayName("Jurisdictions")]
		public string JurisdictionsString { get; set; }

		public Mode Mode { get; set; }
	}
}