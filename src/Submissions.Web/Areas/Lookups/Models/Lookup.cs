﻿using Submissions.Common;

namespace Submissions.Web.Areas.Lookups.Models
{
	public class Lookup
	{
		public bool Active { get; set; }
		public Mode Mode { get; set; }
	}
}