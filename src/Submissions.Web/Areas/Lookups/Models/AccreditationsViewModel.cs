﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Submissions.Web.Areas.Lookups.Models
{
	public class AccreditationsViewModel : Lookup
	{
		public int? Id { get; set; }
		[Required, StringLength(20), Remote("ValidateAccreditation", "Accreditations", AdditionalFields = "Id, Mode", ErrorMessage = "{0} already exists.")]
		public string Name { get; set; }
		[Required, StringLength(50)]
		public string Description { get; set; }
		[Display(Name = "Issue Date"), Required, DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}")]
		public DateTime? IssueDate { get; set; }
		[Display(Name = "Expiration Date"), Required, DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}")]
		public DateTime? ExpirationDate { get; set; }
		[Display(Name = "Laboratories")]
		public string[] LaboratoryIds { get; set; }
		public IList<SelectListItem> Laboratories { get; set; }
	}
}