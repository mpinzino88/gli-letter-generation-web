﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Submissions.Web.Areas.Lookups.Models
{
	public class EmailDistributionsIndexViewModel
	{
		public int Id { get; set; }
		public string Code { get; set; }
		public string Description { get; set; }
	}

	public class EmailDistributionsEditViewModel : Lookup
	{
		public EmailDistributionsEditViewModel()
		{
			this.EmailDistributionInfo = new EmailDistributionInfo();
			this.DefaultManufacturerFilter = new ManufacturerFilter();
			this.DefaultManufacturerGroupFilter = new ManufacturerGroupFilter();
			this.DefaultJurisdictionFilter = new JurisdictionFilter();
		}

		public EmailDistributionInfo EmailDistributionInfo { get; set; }

		// default objects are used when adding knockout items
		public ManufacturerFilter DefaultManufacturerFilter { get; set; }
		public ManufacturerGroupFilter DefaultManufacturerGroupFilter { get; set; }
		public JurisdictionFilter DefaultJurisdictionFilter { get; set; }
	}

	public class EmailDistributionInfo
	{
		public EmailDistributionInfo()
		{
			this.Users = new List<SelectListItem>();
			this.ManufacturerUsers = new List<ManufacturerFilter>();
			this.ManufacturerGroupUsers = new List<ManufacturerGroupFilter>();
			this.JurisdictionUsers = new List<JurisdictionFilter>();
		}

		public int? Id { get; set; }
		[Remote("ValidateCode", "EmailDistributions", AdditionalFields = "Id", ErrorMessage = "{0} already exists.")]
		public string Code { get; set; }
		public string Description { get; set; }

		[Display(Name = "Global Notifications")]
		public string[] UserIds { get; set; }
		public IList<SelectListItem> Users { get; set; }

		public IList<ManufacturerFilter> ManufacturerUsers { get; set; }
		public IList<ManufacturerGroupFilter> ManufacturerGroupUsers { get; set; }
		public IList<JurisdictionFilter> JurisdictionUsers { get; set; }
	}

	public class ManufacturerFilter
	{
		public ManufacturerFilter()
		{
			this.Manufacturer = new SelectListItem();
			this.Users = new List<SelectListItem>();
		}

		public bool _destroy { get; set; }
		public string ManufacturerCode { get; set; }
		public SelectListItem Manufacturer { get; set; }
		public string[] UserIds { get; set; }
		public IList<SelectListItem> Users { get; set; }
	}

	public class ManufacturerGroupFilter
	{
		public ManufacturerGroupFilter()
		{
			this.ManufacturerGroup = new SelectListItem();
			this.Users = new List<SelectListItem>();
		}

		public bool _destroy { get; set; }
		public int? ManufacturerGroupId { get; set; }
		public SelectListItem ManufacturerGroup { get; set; }
		public string[] UserIds { get; set; }
		public IList<SelectListItem> Users { get; set; }
	}

	public class JurisdictionFilter
	{
		public JurisdictionFilter()
		{
			this.Jurisdiction = new SelectListItem();
			this.Users = new List<SelectListItem>();
		}

		public bool _destroy { get; set; }
		public string JurisdictionId { get; set; }
		public SelectListItem Jurisdiction { get; set; }
		public string[] UserIds { get; set; }
		public IList<SelectListItem> Users { get; set; }
	}
}