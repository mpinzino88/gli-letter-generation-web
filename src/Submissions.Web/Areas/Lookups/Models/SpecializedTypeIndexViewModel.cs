﻿using System.Collections.Generic;

namespace Submissions.Web.Areas.Lookups.Models
{
	public class SpecializedTypeIndexViewModel
	{
		public SpecializedTypeIndexViewModel()
		{
			SpecializedTypes = new List<SpecializedType>();
		}
		public List<SpecializedType> SpecializedTypes { get; set; }
	}

	public class SpecializedType
	{
		public int Id { get; set; }
		public string Name { get; set; }
		public string Description { get; set; }
		public string BillingSheet { get; set; }
		public bool Active { get; set; }
	}
}