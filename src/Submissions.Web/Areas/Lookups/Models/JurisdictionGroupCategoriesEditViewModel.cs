﻿using Submissions.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Submissions.Web.Areas.Lookups.Models
{
	public class JurisdictionGroupCategoriesEditViewModel
	{
		public int Id { get; set; }

		[DisplayName("Category Tag"), StringLength(150), Required]
		public string Code { get; set; }

		[DisplayName("Category Description"), StringLength(300), Required]
		public string Description { get; set; }

		public Mode Mode { get; set; }
	}
}