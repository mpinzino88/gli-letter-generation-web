﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Submissions.Web.Areas.Lookups.Models
{
	public class RolesViewModel : Lookup
	{
		public int? Id { get; set; }
		[Display(Name = "Code"), Required, StringLength(4), Remote("ValidateCode", "Roles", AdditionalFields = "Id, Mode", ErrorMessage = "{0} already exists.")]
		public string Code { get; set; }
		[Display(Name = "Description"), Required]
		public string Description { get; set; }
		[Display(Name = "Department"), Required]
		public int? DepartmentId { get; set; }
		public string DepartmentName { get; set; }
		public List<AccessRightCheckbox> AccessRights { get; set; }
	}
}