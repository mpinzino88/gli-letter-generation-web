﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Submissions.Web.Areas.Lookups.Models
{
	public class AcumaticaBillWithAndCustomerExceptionEditViewModel : Lookup
	{
		public int Id { get; set; }
		[Required]
		[DisplayName("Company")]
		public int? CompanyId { get; set; }
		[DisplayName("Company")]
		public string CompanyCode { get; set; }
		[DisplayName("Company")]
		public string CompanyName { get; set; }
		[Required]
		[DisplayName("Manufacturer")]
		public string ManufacturerCode { get; set; }
		public string Manufacturer { get; set; }
		[RegularExpression(@"^$|[0-9]{3}")]
		[DisplayName("Jurisdiction")]
		public string Jurisdiction { get; set; }
		[RegularExpression(@"^$|[0-9]{3}")]
		[DisplayName("Transfer Jurisdiction")]
		public string TransferJurisdiction { get; set; }
		[DisplayName("Self Bill")]
		public bool SelfBill { get; set; }
		[Required]
		[DisplayName("Project Customer")]
		public string ProjectCustomer { get; set; }
		public string Customer { get; set; }
		[Required]
		[DisplayName("Exception Type")]
		public int? ExceptionTypeId { get; set; }
		[DisplayName("Exception Type")]
		public string ExceptionTypeName { get; set; }

		[RegularExpression(@"^$|[a-zA-Z0-9]{2}")]
		[DisplayName("Segment 01")]
		public string Segment01 { get; set; }
		[RegularExpression(@"^$|[0-9]{3}")]
		[DisplayName("Segment 02")]
		public string Segment02 { get; set; }
		[RegularExpression(@"^$|[a-zA-Z0-9]{3}")]
		[DisplayName("Segment 03")]
		public string Segment03 { get; set; }
		[RegularExpression(@"^$|[0-9]{2}")]
		[DisplayName("Segment 04")]
		public string Segment04 { get; set; }
		[RegularExpression(@"^$|[0-9]{3}")]
		[DisplayName("Segment 05")]
		public string Segment05 { get; set; }
		[RegularExpression(@"^$|[0-9]{3}")]
		[DisplayName("Segment 06")]
		public string Segment06 { get; set; }
	}
}