﻿using Submissions.Web.Models.Grids.Lookups;

namespace Submissions.Web.Areas.Lookups.Models
{
	public class JurisdictionGroupsIndexViewModel
	{
		public JurisdictionGroupsIndexViewModel()
		{
			this.JurisdictionGroupGrid = new JurisdictionGroupsGrid();
		}

		public JurisdictionGroupsGrid JurisdictionGroupGrid { get; set; }
	}
}