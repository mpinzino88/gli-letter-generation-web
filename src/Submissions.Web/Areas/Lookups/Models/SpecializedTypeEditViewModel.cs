﻿namespace Submissions.Web.Areas.Lookups.Models
{
	public class SpecializedTypeEditViewModel
	{
		public int Id { get; set; }
		public string Name { get; set; }
		public string Description { get; set; }
		public int? BillingSheetId { get; set; }
		public string BillingSheet { get; set; }
		public bool Active { get; set; }

		public string Mode { get; set; }
	}
}