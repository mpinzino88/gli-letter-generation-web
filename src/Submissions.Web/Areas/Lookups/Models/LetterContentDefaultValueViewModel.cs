﻿using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Submissions.Web.Areas.Lookups.Models
{
	public class LetterContentDefaultValueViewModel : Lookup
	{
		public int? Id { get; set; }
		[Display(Name = "Jurisdiction"), Required, Remote("ValidateJurisdiction", "LetterContentDefaultValues", AdditionalFields = "Id, Mode", ErrorMessage = "{0} already exists.")]
		public int? JurisdictionId { get; set; }
		public string Jurisdiction { get; set; }
		[Display(Name = "Report Type"), Required]
		public int? ReportTypeId { get; set; }
		public string ReportType { get; set; }
		[Display(Name = "Testing Result"), Required]
		public int? TestingResultId { get; set; }
		public string TestingResult { get; set; }
		[Display(Name = "Letter Jurisdiction"), StringLength(50)]
		public string LetterJurisdiction { get; set; }
	}
}