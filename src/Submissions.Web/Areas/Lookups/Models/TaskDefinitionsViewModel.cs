﻿using Submissions.Web.Models.Grids.Lookups;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Submissions.Web.Areas.Lookups.Models
{
	public class TaskDefinitionsIndexViewModel
	{
		public TaskDefinitionsIndexViewModel()
		{
			this.TaskDefinitionsGrid = new TaskDefinitionsGrid();
		}

		public TaskDefinitionsGrid TaskDefinitionsGrid;
	}

	public class TaskDefinitionsEditViewModel : Lookup
	{
		public TaskDefinitionsEditViewModel()
		{
			this.Department = new SelectListItem();
			this.DefaultDocument = new TaskDefinitionDocument();
			this.Documents = new List<TaskDefinitionDocument>();
		}

		public int? Id { get; set; }
		[Remote("ValidateCode", "TaskDefinitions", AdditionalFields = "Id, Mode", ErrorMessage = "{0} already exists.")]
		public string Code { get; set; }
		public string Description { get; set; }
		[Display(Name = "Department")]
		public int? DepartmentId { get; set; }
		public SelectListItem Department { get; set; }
		[Display(Name = "Default Estimate (hours)")]
		public decimal? EstimateHours { get; set; }
		[Display(Name = "Default Estimate Interval (days)")]
		public decimal? EstimateInterval { get; set; }
		[Display(Name = "Reviewable")]
		public bool IsReview { get; set; }
		[Display(Name = "ETA Task")]
		public bool IsETATask { get; set; }

		[Display(Name = "Acumatica Task")]
		public bool IsAcumaticaTask { get; set; }

		[Display(Name = "Task Id")]
		public string DynamicsId { get; set; }
		public IList<TaskDefinitionDocument> Documents { get; set; }

		// default objects are used when adding knockout items
		public TaskDefinitionDocument DefaultDocument { get; set; }
	}

	public class TaskDefinitionDocument
	{
		public TaskDefinitionDocument()
		{
			this.TaskDocument = new SelectListItem();
		}

		public bool _destroy { get; set; }
		public int? Id { get; set; }
		public int? TaskDocumentId { get; set; }
		public SelectListItem TaskDocument { get; set; }
		public string Note { get; set; }
	}
}