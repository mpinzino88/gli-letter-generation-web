﻿using System.Collections.Generic;

namespace Submissions.Web.Areas.Lookups.Models
{
	public class ManufacturerGuidelinesEditViewModel
	{
		public ManufacturerGuidelinesEditViewModel()
		{
			Rows = new List<ManufacturerGuidelinesRow>();
			NewRow = new ManufacturerGuidelinesRow();
		}
		public string ManufacturerId { get; set; }
		public string ManufacturerName { get; set; }
		public ManufacturerGuidelinesRow NewRow { get; set; }
		public List<ManufacturerGuidelinesRow> Rows { get; set; }

	}
}