﻿using System.ComponentModel.DataAnnotations;

namespace Submissions.Web.Areas.Lookups.Models
{
	public class AcumaticaTenantsEditViewModel : Lookup
	{
		public AcumaticaTenantsEditViewModel()
		{
			Name = string.Empty;
			GLICompanyCode = string.Empty;
		}
		public int Id { get; set; }
		[Display(Name = "Tenant Name"), Required, StringLength(100)]
		public string Name { get; set; }
		[Display(Name = "For Testing")]
		public bool IsTestTenant { get; set; }
		[Display(Name = "Associated GLI Company")]
		public int GLICompanyID { get; set; }
		[Display(Name = "Associated GLI Company")]
		public string GLICompanyCode { get; set; }
	}
}