﻿
using System.ComponentModel.DataAnnotations;

namespace Submissions.Web.Areas.Lookups.Models
{
	public class DocumentTypesViewModel : Lookup
	{
		public int Id { get; set; }
		[Display(Name = "Document Type")]
		public string DocumentType { get; set; }
		public string Description { get; set; }
	}
}