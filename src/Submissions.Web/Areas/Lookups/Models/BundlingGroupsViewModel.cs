﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Submissions.Web.Areas.Lookups.Models
{
	public class BundlingGroupsViewModel : Lookup
	{
		public BundlingGroupsViewModel()
		{
			this.Jurisdictions = new List<SelectListItem>();
			this.ExcludedJurisdictionIds = new List<string>();
		}
		public int? Id { get; set; }
		[Required, StringLength(50), Remote("ValidateBundlingGroupName", "BundlingGroups", AdditionalFields = "Id", ErrorMessage = "{0} already exists.")]
		public string Name { get; set; }
		[Required, StringLength(300)]
		public string Description { get; set; }
		[Display(Name = "Available Jurisdictions"), Required]
		public string[] JurisdictionIds { get; set; }
		[Required]
		public IList<SelectListItem> Jurisdictions { get; set; }
		[Display(Name = "Jurisdictions")]
		public string JurisdictionsIndexDisplay { get; set; }
		public List<string> ExcludedJurisdictionIds { get; set; }
	}
}