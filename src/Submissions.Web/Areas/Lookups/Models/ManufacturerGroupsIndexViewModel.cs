﻿using System.Collections.Generic;

namespace Submissions.Web.Areas.Lookups.Models
{
	public class ManufacturerGroupsIndexViewModel
	{
		public string Type { get; set; }

		public List<string> Manufacturers { get; set; }

		public List<string> ManufacturerCodes { get; set; }

		public List<ManufacturerGroupsIndexListItemModel> ListItems { get; set; }
		public string Heading { get; set; }
	}

	public class ManufacturerGroupsIndexListItemModel
	{
		public string Name { get; set; }
		public int Id { get; set; }
		public string Category { get; internal set; }
	}

}