﻿using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Submissions.Web.Areas.Lookups.Models
{
	public class TranslationHousesViewModel : Lookup
	{
		public int? Id { get; set; }
		[Required, StringLength(20), Display(Name = "Code"), Remote("ValidateCode", "TranslationHouses", AdditionalFields = "Id, Mode", ErrorMessage = "{0} already exists.")]
		public string Code { get; set; }
		[Required, StringLength(200), Display(Name = "Description")]
		public string Description { get; set; }
	}
}