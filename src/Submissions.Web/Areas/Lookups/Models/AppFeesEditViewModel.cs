﻿namespace Submissions.Web.Areas.Lookups.Models
{
	public class AppFeesEditViewModel
	{
		public int Id { get; set; }
		public string Name { get; set; }
		public string Description { get; set; }
		public int Amount { get; set; }
		public bool Active { get; set; }

		public string Mode { get; set; }
	}
}