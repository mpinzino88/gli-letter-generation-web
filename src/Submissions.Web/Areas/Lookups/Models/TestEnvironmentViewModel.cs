﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;


namespace Submissions.Web.Areas.Lookups.Models
{
	public class TestEnvironmentIndexViewModel
	{
		public IList<TestEnvironment> TestEnvironments { get; set; }
	}

	public class TestEnvironmentEditViewModel : Lookup
	{
		public int? Id { get; set; }
		[Display(Name = "Manufacturer Group")]
		public int? ManufacturerGroupId { get; set; }
		public string ManufacturerGroup { get; set; }
		[Display(Name = "Jurisdiction")]
		public string JurisdictionId { get; set; }
		public string Jurisdiction { get; set; }
		[Display(Name = "Platform")]
		public int? PlatformId { get; set; }
		public string Platform { get; set; }
		[StringLength(1000)]
		public string Jackpot { get; set; }
		[Display(Name = "Game Engine"), StringLength(20)]
		public string GameEngine { get; set; }
		[Display(Name = "Server Location"), StringLength(100)]
		public string ServerLocation { get; set; }
		[Display(Name = "Game Url"), StringLength(100)]
		public string GameUrl { get; set; }
		[Display(Name = "Back Office Url"), StringLength(100)]
		public string BackOfficeUrl { get; set; }
		[Display(Name = "Other Url"), StringLength(100)]
		public string OtherUrl { get; set; }
		[Display(Name = "Test Environment System"), StringLength(20)]
		public string TestEnvironmentSystem { get; set; }
		public string Simulator { get; set; }
		[Display(Name = "Help Rules"), StringLength(1000)]
		public string HelpRules { get; set; }
		[Display(Name = "Force Tool"), StringLength(1000)]
		public string ForceTool { get; set; }
		[Display(Name = "PDF Added To Letter"), StringLength(1000)]
		public string PDFAddedToLetter { get; set; }
	}

	public class TestEnvironment
	{
		public int Id { get; set; }
		public bool Active { get; set; }
		[Display(Name = "Manufacturer Group")]
		public string ManufacturerGroup { get; set; }
		public string Jurisdiction { get; set; }
		public string Platform { get; set; }
		public string Jackpot { get; set; }
		[Display(Name = "Game Engine")]
		public string GameEngine { get; set; }
		[Display(Name = "Server Location")]
		public string ServerLocation { get; set; }
		[Display(Name = "Game Url")]
		public string GameUrl { get; set; }
		[Display(Name = "Back Office Url")]
		public string BackOfficeUrl { get; set; }
		[Display(Name = "Other Url")]
		public string OtherUrl { get; set; }
		[Display(Name = "Test Environment System")]
		public string TestEnvironmentSystem { get; set; }
		public string Simulator { get; set; }
		[Display(Name = "Help Rules")]
		public string HelpRules { get; set; }
		[Display(Name = "Force Tool")]
		public string ForceTool { get; set; }
		[Display(Name = "PDF Added To Letter")]
		public string PDFAddedToLetter { get; set; }
	}
}