﻿using System.Collections.Generic;
using System.Web.Mvc;

namespace Submissions.Web.Areas.Lookups.Models
{
	public class BillingSheetEditViewModel
	{
		public BillingSheetEditViewModel()
		{
			this.LabIds = new List<int>();
			this.Labs = new List<SelectListItem>();
		}
		public int Id { get; set; }
		public string Name { get; set; }
		public string Description { get; set; }
		public bool Active { get; set; }
		public bool GetsGatChargeIfApplicable { get; set; }
		public bool RequiredSpecializedType { get; set; }

		public List<int> LabIds { get; set; }
		public List<SelectListItem> Labs { get; set; }

		public int? AppFeeId { get; set; }
		public string AppFee { get; set; }

		public int? BilledItem1AppFeeId { get; set; }
		public string BilledItem1AppFee { get; set; }

		public int? BilledItem2AppFeeId { get; set; }
		public string BilledItem2AppFee { get; set; }

		public int? BilledItem3AppFeeId { get; set; }
		public string BilledItem3AppFee { get; set; }

		public string Mode { get; set; }
		public bool CanNameBeUpdated { get; set; }
	}
}