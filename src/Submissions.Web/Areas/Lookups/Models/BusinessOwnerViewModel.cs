﻿using Submissions.Common;
using Submissions.Web.Models.Grids.Lookups;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Trirand.Web.Mvc;

namespace Submissions.Web.Areas.Lookups.Models
{
	public class BusinessOwnerViewModel
	{
	}

	public class BusinessOwnerIndexViewModel
	{
		public BusinessOwnerIndexViewModel()
		{
			BusinessOwners = new BusinessOwnersGrid();
		}

		public BusinessOwnersGrid BusinessOwners { get; set; }
	}

	public class BusinessOwnerEditViewModel : Lookup
	{
		public BusinessOwnerEditViewModel()
		{
			BusinessOwnerUsersGrid = new BusinessOwnerUsersGrid();
			BusinessOwnerUsersGrid.Grid.ID = "BusinessOwnerUsersGrid";
			BusinessOwnerUsersGrid.Grid.ClientSideEvents = new ClientSideEvents
			{
				BeforeAjaxRequest = "gridBeforeAjaxRequest"
			};
			BusinessOwnerUsersGrid.Grid.Columns.Add(
				new JQGridColumn
				{
					DataField = "Remove",
					HeaderText = " ",
					Formatter = new CustomFormatter { FormatFunction = "removeUserLink" },
					Searchable = false,
					Sortable = false,
					TextAlign = Trirand.Web.Mvc.TextAlign.Right,
					CssClass = "padright"
				});

			ModalBusinessOwnerGrid = new BusinessOwnerUsersGrid();
			ModalBusinessOwnerGrid.Grid.ID = "ModalBusinessOwnerGrid";
			ModalBusinessOwnerGrid.Grid.ClientSideEvents = new ClientSideEvents
			{
				BeforeAjaxRequest = "modalGridBeforeAjaxRequest"
			};
			ModalBusinessOwnerGrid.Grid.MultiSelect = true;
		}

		public int? Id { get; set; }
		[Required(ErrorMessage = "An abbreviation is required.")]
		[StringLength(2)]
		[DisplayName("Abbreviation")]
		public string Code { get; set; }
		[Required(ErrorMessage = "A description is required.")]
		[StringLength(20)]
		[DisplayName("Description")]
		public string Description { get; set; }

		public BusinessOwnerUsersGrid BusinessOwnerUsersGrid;
		public BusinessOwnerUsersGrid ModalBusinessOwnerGrid;
	}

	public class UsersBusinessOwnerPayload
	{
		public List<int> UserIds { get; set; }
		public int BusinessOwnerId { get; set; }
	}
}