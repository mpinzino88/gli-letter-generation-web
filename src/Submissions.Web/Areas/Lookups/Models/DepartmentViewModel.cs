﻿using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Submissions.Web.Areas.Lookups.Models
{
	public class DepartmentViewModel : Lookup
	{
		public int? Id { get; set; }
		[Required, StringLength(3), Remote("ValidateCode", "Departments", AdditionalFields = "Id, Mode", ErrorMessage = "{0} already exists.")]
		public string Code { get; set; }
		[Required, StringLength(64), Remote("ValidateCode", "Departments", AdditionalFields = "Id, Mode", ErrorMessage = "{0} already exists.")]
		public string Description { get; set; }
	}
}