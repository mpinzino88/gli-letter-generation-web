﻿using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Submissions.Web.Areas.Lookups.Models
{
	public class CompilationMethodViewModel : Lookup
	{
		public int? Id { get; set; }
		[Required, StringLength(50), Remote("ValidateName", "CompilationMethod", AdditionalFields = "Id, Mode", ErrorMessage = "{0} already exists.")]
		public string Name { get; set; }
	}
}