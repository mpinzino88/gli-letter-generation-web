﻿using Submissions.Web.Models.Grids.Lookups;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Trirand.Web.Mvc;

namespace Submissions.Web.Areas.Lookups.Models
{
	public class SupplierFocusIndexViewModel
	{
		public int Id { get; set; }
		public string Description { get; set; }
		public bool Active { get; set; }
	}

	public class SupplierFocusEditViewModel : Lookup
	{
		public SupplierFocusEditViewModel()
		{
			SupplierFocusUsersGrid = new SupplierFocusUsersGrid();
			SupplierFocusUsersGrid.Grid.ID = "SupplierFocusUsersGrid";
			SupplierFocusUsersGrid.Grid.ClientSideEvents = new ClientSideEvents
			{
				BeforeAjaxRequest = "gridBeforeAjaxRequest"
			};
			SupplierFocusUsersGrid.Grid.Columns.Add(
				new JQGridColumn
				{
					DataField = "Remove",
					HeaderText = " ",
					Formatter = new CustomFormatter { FormatFunction = "removeUserLink" },
					Searchable = false,
					Sortable = false,
					TextAlign = Trirand.Web.Mvc.TextAlign.Right,
					CssClass = "padright"
				});

			ModalSupplierFocusGrid = new SupplierFocusUsersGrid();
			ModalSupplierFocusGrid.Grid.ID = "ModalSupplierFocusGrid";
			ModalSupplierFocusGrid.Grid.ClientSideEvents = new ClientSideEvents
			{
				BeforeAjaxRequest = "modalGridBeforeAjaxRequest"
			};
			ModalSupplierFocusGrid.Grid.MultiSelect = true;
		}

		public int Id { get; set; }
		[Required]
		public string Description { get; set; }

		public SupplierFocusUsersGrid SupplierFocusUsersGrid;
		public SupplierFocusUsersGrid ModalSupplierFocusGrid;
	}

	public class UsersSupplierFocusPayload
	{
		public List<int> UserIds { get; set; }
		public int SupplierFocusId { get; set; }
	}
}