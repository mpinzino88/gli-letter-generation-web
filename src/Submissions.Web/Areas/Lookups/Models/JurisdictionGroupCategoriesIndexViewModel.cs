﻿using Submissions.Web.Models.Grids.Lookups;

namespace Submissions.Web.Areas.Lookups.Models
{
	public class JurisdictionGroupCategoriesIndexViewModel
	{
		public JurisdictionGroupCategoriesIndexViewModel()
		{
			this.JurisdictionGroupCategoriesGrid = new JurisdictionGroupCategoriesGrid();
		}

		public JurisdictionGroupCategoriesGrid JurisdictionGroupCategoriesGrid { get; set; }
	}
}