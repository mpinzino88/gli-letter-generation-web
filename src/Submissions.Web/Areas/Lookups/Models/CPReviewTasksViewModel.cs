﻿using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Submissions.Web.Areas.Lookups.Models
{
	public class CPReviewTasksViewModel : Lookup
	{
		public int? Id { get; set; }
		[Display(Name = "Jurisdiction ID"), Required, Remote("ValidateCPReviewTask", "CPReviewTasks", AdditionalFields = "Id, Mode", ErrorMessage = "{0} already exists.")]
		public string JurisdictionId { get; set; }
		public string Jurisdiction { get; set; }
		[Display(Name = "Estimated Hours"), Required]
		public decimal? EstimatedHours { get; set; }

	}
}