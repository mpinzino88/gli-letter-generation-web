﻿using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Submissions.Web.Areas.Lookups.Models
{
	public class tbl_lst_GamingGuidelineGameTypesViewModel : Lookup
	{

		public int? Id { get; set; }
		[Required, StringLength(50), Remote("ValidateGameType", "GamingGuidelineGameTypes", AdditionalFields = "Id, Mode", ErrorMessage = "{0} already exists.")]
		public string Name { get; set; }
		[StringLength(500)]
		public string Note { get; set; }
	}
}