﻿using Submissions.Web.Areas.Tools.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Submissions.Web.Areas.Lookups.Models
{
	public class PlatformPiecesIndexViewModel : Lookup
	{
		public List<PlatformPiecesTemplateDTO> PlatformPiecesTemplates { get; set; }
		public PlatformPiecesIndexViewModel()
		{
			PlatformPiecesTemplates = new List<PlatformPiecesTemplateDTO>();
		}
	}

	public class PlatformPiecesEditViewModel : Lookup
	{
		public PlatformPiecesEditViewModel()
		{
			Platform = new ManufacturerPlatform();
			PlatformPieces = new List<PlatformPieceDTO>();
			Jurisdictions = new List<JurisdictionInfo>();
		}
		public int Id { get; set; }
		public string Name { get; set; }
		public string Description { get; set; }
		public int OriginalId { get; set; }
		public DateTime AddDate { get; set; }
		public int AddUserId { get; set; }
		public string Manufacturer { get; set; }
		public string ManufacturerCode { get; set; }

		public ManufacturerPlatform Platform { get; set; }

		public List<PlatformPieceDTO> PlatformPieces { get; set; }
		public IList<JurisdictionInfo> Jurisdictions { get; set; }
	}


}