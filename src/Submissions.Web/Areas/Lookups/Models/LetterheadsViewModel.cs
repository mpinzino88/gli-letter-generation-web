﻿using Submissions.Common.Kendo;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web;
using System.Web.Mvc;

namespace Submissions.Web.Areas.Lookups.Models
{
	public class LetterheadsIndexViewModel
	{
		public IList<Letterhead> Letterheads { get; set; }
	}

	public class LetterheadsEditViewModel : Lookup
	{
		public LetterheadsEditViewModel()
		{
			this.LetterheadFile = new List<KendoFile>();
			this.LetterheadFileUpload = new List<HttpPostedFileBase>();
			this.Laboratories = new List<SelectListItem>();
		}

		public int Id { get; set; }
		[Required, StringLength(25), Remote("ValidateCode", "Letterheads", AdditionalFields = "Id, Mode", ErrorMessage = "{0} already exists.")]
		public string Code { get; set; }
		[Required, StringLength(300)]
		public string Description { get; set; }

		public int GPSItemId { get; set; }
		public string GPSFilePath { get; set; }
		public string GPSFileName { get; set; }
		public IList<KendoFile> LetterheadFile { get; set; }
		public IList<HttpPostedFileBase> LetterheadFileUpload { get; set; }

		[Display(Name = "Laboratories"), Required]
		public string[] LaboratoryIds { get; set; }
		public IList<SelectListItem> Laboratories { get; set; }
	}

	public class Letterhead
	{
		public int Id { get; set; }
		public string Code { get; set; }
		public string Description { get; set; }
		public bool Active { get; set; }
	}

	public class Laboratory
	{
		public int Id { get; set; }
		public string Location { get; set; }
		public string Name { get; set; }
		public Nullable<int> Active { get; set; }
	}
}