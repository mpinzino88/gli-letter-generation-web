﻿using Submissions.Common;
using Submissions.Web.Models.Grids.Lookups;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web.Mvc;

namespace Submissions.Web.Areas.Lookups.Models
{
	public class JurisdictionsIndexViewModel
	{
		public JurisdictionsIndexViewModel()
		{
			this.JurisdictionsGridModel = new JurisdictionsGrid();
		}

		public JurisdictionsGrid JurisdictionsGridModel { get; set; }
	}

	public class JurisdictionsEditViewModel : Lookup
	{
		public JurisdictionsEditViewModel()
		{
			this.Country = new List<SelectListItem>();
			this.A1Data = new List<A1Data>();
			this.SignatureScopes = new List<SignatureScope>();
			this.BillingLabs = new List<SelectListItem>();
			this.Manufacturers = new List<SelectListItem>();
			this.LiaisonUsers = new List<SelectListItem>();
			this.A1DataField4TypOptions = LookupsStandard.ConvertEnum<PdfFolderDesignation>(showBlank: true).ToList();
			this.NationCodeOptions = LookupsStandard.Nations;
			this.DefaultA1Data = new A1Data();
			this.DefaultSignatureScope = new SignatureScope();
			this.SendEmail = false;
			this.QAOfficeDefaults = new List<SelectListItem>();
		}

		// general
		[Display(Name = "ID")]
		public string Id { get; set; }
		public bool PendingApproval { get; set; }
		public bool SendEmail { get; set; }
		[Display(Name = "Name"), Required, StringLength(100)]
		public string Name { get; set; }
		[Display(Name = "Formatted Name"), StringLength(100)]
		public string FormattedJurisdictionName { get; set; }
		[Display(Name = "Jurisdiction Type"), StringLength(120)]
		public string JurisdictionType { get; set; }
		[Display(Name = "Nation")]
		public string NationCode { get; set; }
		[Display(Name = "Country")]
		public int? CountryId { get; set; }
		public IList<SelectListItem> Country { get; set; }
		[Display(Name = "Sub Code"), StringLength(5)]
		public string SubCode { get; set; }
		[StringLength(256)]
		public string Comments { get; set; }
		[Display(Name = "Liaisons")]
		public string[] LiaisonUserIds { get; set; }
		public IList<SelectListItem> LiaisonUsers { get; set; }
		[Display(Name = "Default QA Office")]
		public int? QAOfficeDefaultId { get; set; }
		public IList<SelectListItem> QAOfficeDefaults { get; set; }

		// pct
		[Display(Name = "Electronic Requests Available")]
		public bool AvailForElectReq { get; set; }
		[Display(Name = "Alternate Name"), StringLength(240)]
		public string AltName { get; set; }

		// emails
		public string Email { get; set; }
		[Display(Name = "Emails for Daily Updates")]

		// pdf and sig scopes
		public IList<A1Data> A1Data { get; set; }
		public IList<SignatureScope> SignatureScopes { get; set; }

		// options
		[Display(Name = "ARI Contract")]
		public bool ARIScheduleC { get; set; }
		[Display(Name = "Restriced Use")]
		public bool RestrictedUse { get; set; }
		[Display(Name = "Use Draft Letters")]
		public bool UseDraftLetters { get; set; }
		[Display(Name = "GAT Jurisdiction")]
		public bool IsGAT { get; set; }
		[Display(Name = "Testing Performed Available")]
		public bool TestingPerformedAvailable { get; set; }

		// accounting
		[Display(Name = "QMS Link")]
		public string QMSLink { get; set; }
		[Display(Name = "Manufacturer")]
		public string[] ManufacturerCodes { get; set; }
		public IList<SelectListItem> Manufacturers { get; set; }
		[Display(Name = "Billing Labs")]
		public string[] BillingLabIds { get; set; }
		public IList<SelectListItem> BillingLabs { get; set; }

		// default objects are used when adding knockout items
		public IList<SelectListItem> A1DataField4TypOptions { get; set; }
		public IList<SelectListItem> NationCodeOptions { get; set; }
		public A1Data DefaultA1Data { get; set; }
		public SignatureScope DefaultSignatureScope { get; set; }
	}

	public class A1Data
	{
		public bool _destroy { get; set; }
		public int? Id { get; set; }
		public string Field4Typ { get; set; }
		public string Field5Dir { get; set; }
		public string Field6Juris { get; set; }
	}

	public class SignatureScope
	{
		public SignatureScope()
		{
			this.Scope = new List<SelectListItem>();
			this.Type = new List<SelectListItem>();
		}

		public bool _destroy { get; set; }
		public int? Id { get; set; }
		public int ScopeId { get; set; }
		public IList<SelectListItem> Scope { get; set; }
		public int TypeId { get; set; }
		public IList<SelectListItem> Type { get; set; }
	}
}