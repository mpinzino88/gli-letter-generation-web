﻿using Submissions.Web.Models.Grids.Lookups;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Trirand.Web.Mvc;

namespace Submissions.Web.Areas.Lookups.Models
{
	public class TeamsIndexViewModel
	{
		public TeamsIndexViewModel()
		{
			TeamsGrid = new TeamsGrid();
		}

		public TeamsGrid TeamsGrid;
	}

	public class TeamsEditViewModel : Lookup
	{
		public TeamsEditViewModel()
		{
			TeamUsersGrid = new TeamUsersGrid();
			TeamUsersGrid.Grid.ID = "TeamUsersGrid";
			TeamUsersGrid.Grid.ClientSideEvents = new ClientSideEvents
			{
				BeforeAjaxRequest = "gridBeforeAjaxRequest"
			};
			TeamUsersGrid.Grid.Columns.Add(
				new JQGridColumn
				{
					DataField = "Remove",
					HeaderText = " ",
					Formatter = new CustomFormatter { FormatFunction = "removeUserLink" },
					Searchable = false,
					Sortable = false,
					TextAlign = Trirand.Web.Mvc.TextAlign.Right,
					CssClass = "padright"
				});

			ModalTeamsGrid = new TeamUsersGrid();
			ModalTeamsGrid.Grid.ID = "ModalTeamsGrid";
			ModalTeamsGrid.Grid.ClientSideEvents = new ClientSideEvents
			{
				BeforeAjaxRequest = "modalGridBeforeAjaxRequest"
			};
			ModalTeamsGrid.Grid.MultiSelect = true;
		}
		public int Id { get; set; }

		[Required(ErrorMessage = "An abbreviation is required.")]
		[StringLength(2)]
		[DisplayName("Abbreviation")]
		public string Code { get; set; }

		[Required(ErrorMessage = "A description is required.")]
		[StringLength(20)]
		[DisplayName("Description")]
		public string Description { get; set; }

		public TeamUsersGrid TeamUsersGrid;
		public TeamUsersGrid ModalTeamsGrid;
	}

	public class UsersTeamPayload
	{
		public List<int> UserIds { get; set; }
		public int TeamId { get; set; }
	}
}