﻿using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Submissions.Web.Areas.Lookups.Models
{
	public class GamingGuidelineTechTypeViewModel : Lookup
	{
		public int? Id { get; set; }
		[Required, StringLength(50), Remote("ValidateTechType", "GamingGuidelineTechTypes", AdditionalFields = "Id, Mode", ErrorMessage = "{0} already exists.")]
		public string Name { get; set; }
		[StringLength(500)]
		public string Note { get; set; }
	}
}