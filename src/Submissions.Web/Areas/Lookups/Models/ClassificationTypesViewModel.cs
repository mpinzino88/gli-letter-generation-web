﻿using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Submissions.Web.Areas.Lookups.Models
{
	public class ClassificationTypesViewModel : Lookup
	{
		public int? Id { get; set; }
		[Required, StringLength(100), Remote("ValidateClassificationTypes", "ClassificationTypes", AdditionalFields = "Id, Mode", ErrorMessage = "{0} already exists.")]
		public string Name { get; set; }
	}
}