﻿using Submissions.Common;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Submissions.Web.Areas.Lookups.Models
{
	public class SubscriptionIndexViewModel
	{
		public IList<Subscription> Subscriptions { get; set; }
	}

	public class SubscriptionEditViewModel : Lookup
	{
		public SubscriptionEditViewModel()
		{
			this.CategoryList = LookupsStandard.ConvertEnum<SubscriptionCategory>(useDescriptionAsValue: true, showBlank: true);
			this.FilterFieldList = LookupsStandard.ConvertEnum<SubscriptionFilterField>(useDescriptionAsValue: true, showBlank: true);
			this.SelectedFilterField = new List<string>();
		}

		public int? Id { get; set; }
		[Required, StringLength(4000), Remote("ValidateName", "Subscription", AdditionalFields = "Id, Mode", ErrorMessage = "{0} already exists.")]
		public string Name { get; set; }
		[Required, StringLength(4000)]
		public string Category { get; set; }
		public IEnumerable<SelectListItem> CategoryList { get; set; }
		[Required, StringLength(30)]
		public string Description { get; set; }
		[Display(Name = "Subscription Group")]
		public int? SubscriptionGroupsId { get; set; }
		public string SubscriptionGroup { get; set; }
		[Required, Display(Name = "Message Expiry (Hrs)")]
		public int MessageExpiry { get; set; }
		public string FilterField { get; set; }
		public IEnumerable<string> SelectedFilterField { get; set; }
		[Display(Name = "Filter Field")]
		public IEnumerable<SelectListItem> FilterFieldList { get; set; }
	}

	public class Subscription
	{
		public int Id { get; set; }
		public string Name { get; set; }
		public string Category { get; set; }
		public string Description { get; set; }
		[Display(Name = "Subscription Group")]
		public string SubscriptionGroup { get; set; }
		public bool Active { get; set; }
	}

	public class Filters
	{
		public string TableName { get; set; }
		public string ColumnName { get; set; }
		public string DataType { get; set; }
	}
}