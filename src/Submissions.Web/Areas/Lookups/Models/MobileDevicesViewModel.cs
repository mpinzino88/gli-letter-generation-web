﻿using System.ComponentModel.DataAnnotations;
using Submissions.Common;
using Submissions.Web.Models.Grids.Lookups;

namespace Submissions.Web.Areas.Lookups.Models
{
	public class MobileDevicesIndexViewModel
	{
		public readonly string Title;

		public MobileDevicesGrid MobielDevicesGrid { get; set; }

		public MobileDevicesIndexViewModel()
		{
			Title = "Mobile Devices";
			MobielDevicesGrid = new MobileDevicesGrid();
		}
	}

	public class MobileDevicesEditViewModel : Lookup
	{
		public int Id { get; set; }

		[Display(Name = "Device"), Required, StringLength(100)]
		public string Device { get; set; }

		[Display(Name = "Model"), StringLength(100)]
		public string Model { get; set; }

		[Display(Name = "OS"), StringLength(50)]
		public string OperatingSystem { get; set; }

		[Display(Name = "Browser"), StringLength(100)]
		public string Browser { get; set; }

		[Display(Name = "Device Type"), Required]
		public MobileDeviceType DeviceType { get; set; }

		[Display(Name = "Device Id")]
		public string DeviceId { get; set; }

		[Display(Name = "Location"), Required]
		public int? LocationId { get; set; }
		public string Location { get; set; }

		//public IEnumerable<SelectListItem> DeviceTypeList { get; set; }

		//public MobileDevicesEditViewModel()
		//{
		//	DeviceTypeList = LookupsStandard
		//		.ConvertEnum(showBlank: true,
		//			enumValues: new Common.MobileDeviceType[] {
		//				Common.MobileDeviceType.Mobile,
		//				Common.MobileDeviceType.Tablet }
		//			);
		//}
	}
}