﻿using System.ComponentModel.DataAnnotations;

namespace Submissions.Web.Areas.Lookups.Models
{
	public class GameDescriptionStatusViewModel : Lookup
	{
		public int? Id { get; set; }
		[Required, StringLength(100)]
		public string Name { get; set; }
	}
}