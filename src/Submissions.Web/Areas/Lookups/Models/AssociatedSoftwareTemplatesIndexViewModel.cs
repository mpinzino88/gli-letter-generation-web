﻿using Submissions.Web.Areas.Tools.Models;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Submissions.Web.Areas.Lookups.Models
{
	public class AssociatedSoftwareTemplatesIndexView : Lookup
	{
		public AssociatedSoftwareTemplatesIndexView()
		{
			Templates = new List<AssociatedSoftwareTemplate>();
		}
		public IList<AssociatedSoftwareTemplate> Templates { get; set; }
	}

	public class AssociatedSoftwareTemplateEditViewModel : Lookup
	{
		public AssociatedSoftwareTemplateEditViewModel()
		{
			this.AssociatedSoftwares = new List<TemplateComponent>();
			PlatformPiecesTemplates = new List<PlatformPiecesTemplateDTO>();
			Jurisdictions = new List<JurisdictionInfo>();
		}
		public int Id { get; set; }
		public int OriginalId { get; set; }
		public string Name { get; set; }
		public string Description { get; set; }
		public string Manufacturer { get; set; }
		public string ManufacturerCode { get; set; }
		public IList<JurisdictionInfo> Jurisdictions { get; set; }
		public string Platform { get; set; }
		public int PlatformId { get; set; }
		public IList<TemplateComponent> AssociatedSoftwares { get; set; }
		public IList<PlatformPiecesTemplateDTO> PlatformPiecesTemplates { get; set; }
	}
}