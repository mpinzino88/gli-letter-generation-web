﻿namespace Submissions.Web.Areas.Lookups.Models
{
	public class BillingPartyViewModel : Lookup
	{
		public int Id { get; set; }
		public string BillingParty { get; set; }
		public int? ManufacturerGroupId { get; set; }
		public string ManufacturerGroup { get; set; }
		public int? TestingTypeId { get; set; }
		public string TestingType { get; set; }
		public int? BillingPartyCode { get; set; }
	}
}