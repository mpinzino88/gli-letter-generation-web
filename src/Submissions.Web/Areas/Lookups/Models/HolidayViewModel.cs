﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace Submissions.Web.Areas.Lookups.Models
{
	public class HolidayEditViewModel : Lookup
	{
		public HolidayEditViewModel()
		{
			this.DefaultHolidayViewModel = new HolidayViewModel();
			this.Holidays = new List<HolidayViewModel>();
		}
		public int Id { get; set; }
		public int LocationId { get; set; }
		public string LocationName { get; set; }
		public SelectListItem Location { get; set; }
		public IList<HolidayViewModel> Holidays { get; set; }
		public HolidayViewModel DefaultHolidayViewModel { get; set; }
		public int Year { get; set; }
		public string ModeName { get; set; }
	}

	public class HolidayViewModel
	{
		public string Name { get; set; }
		public DateTime? Date { get; set; }
	}
}