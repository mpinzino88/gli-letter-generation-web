﻿using Submissions.Web.Models.Grids.Lookups;

namespace Submissions.Web.Areas.Lookups.Models
{
	public class AcumaticaTenantsIndexViewModel
	{
		public AcumaticaTenantsIndexViewModel()
		{
			AcumaticaTenantsGridModel = new AcumaticaTenantsGrid();
		}

		public AcumaticaTenantsGrid AcumaticaTenantsGridModel { get; set; }
	}
}