﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Submissions.Web.Areas.Lookups.Models
{
	public class LaboratoriesIndexViewModel
	{
		public IList<LaboratoryLocations> Laboratories { get; set; }
	}

	public class LaboratoriesEditViewModel : Lookup
	{
		public LaboratoriesEditViewModel()
		{
			this.Accreditations = new List<SelectListItem>();
		}

		public int? Id { get; set; }
		[Required, StringLength(2), Remote("ValidateLocation", "Laboratories", AdditionalFields = "Id, Mode", ErrorMessage = "{0} already exists.")]
		public string Location { get; set; }
		[Required, StringLength(255)]
		public string Name { get; set; }
		[Display(Name = "Archive Location"), Required, StringLength(2)]
		public string ArchiveLocation { get; set; }
		[Display(Name = "Company")]
		public int? CompanyId { get; set; }
		public string Company { get; set; }
		[Required, StringLength(75)]
		public string Address1 { get; set; }
		[StringLength(75)]
		public string Address2 { get; set; }
		[Required, StringLength(50)]
		public string City { get; set; }
		[Required, StringLength(50)]
		public string State { get; set; }
		[Required, StringLength(10), Display(Name = "Postal Code")]
		public string PostalCode { get; set; }
		[Required, Display(Name = "Country")]
		public int? CountryId { get; set; }
		public string Country { get; set; }
		public string[] AccreditationIds { get; set; }
		public IList<SelectListItem> Accreditations { get; set; }
		public int? QAOfficeDefaultId { get; set; }
		public string QAOfficeDefault { get; set; }
		public string Abbreviation { get; set; }
	}

	public class LaboratoryLocations
	{
		public int Id { get; set; }
		public int? Active { get; set; }
		public string Location { get; set; }
		public string Name { get; set; }
		[Display(Name = "Archive Location")]
		public string ArchiveLocation { get; set; }
		public string Company { get; set; }
		public string City { get; set; }
		public string State { get; set; }
		public string Country { get; set; }
	}
}