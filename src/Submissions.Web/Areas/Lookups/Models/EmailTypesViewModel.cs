﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Submissions.Web.Areas.Lookups.Models
{
	public class EmailTypesIndexViewModel
	{
		public int Id { get; set; }
		[Required]
		public string Code { get; set; }
		[Required]
		public string Description { get; set; }
	}

	public class EmailTypesEditViewModel : Lookup
	{
		public EmailTypesEditViewModel()
		{
			this.Users = new List<SelectListItem>();
			this.EmailDistributions = new List<SelectListItem>();
		}

		public int Id { get; set; }
		[Required, Remote("ValidateCode", "EmailTypes", AdditionalFields = "Id, Mode", ErrorMessage = "{0} already exists.")]
		public string Code { get; set; }
		[Required]
		public string Description { get; set; }
		[Display(Name = "Users")]
		public string[] UserIds { get; set; }
		public IList<SelectListItem> Users { get; set; }
		[Display(Name = "Email Distributions")]
		public string[] EmailDistributionIds { get; set; }
		public IList<SelectListItem> EmailDistributions { get; set; }
		[Display(Name = "External Email Address")]
		public string ExternalEmailAddress { get; set; }
	}
}