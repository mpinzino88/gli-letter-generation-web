﻿using Submissions.Common;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace Submissions.Web.Areas.Lookups.Models
{
	public class LBDefaultingOptionsEditViewModel
	{
		public LBDefaultingOptionsEditViewModel()
		{
			this.StatusOptions = LookupsStandard.ConvertEnum<JurisdictionStatus>(showValueInDescription: true).ToList();
			this.JurisdictionIds = new List<string>();
			this.Jurisdictions = new List<SelectListItem>();
			this.ManufacturerCodes = new List<string>();
			this.Manufacturers = new List<SelectListItem>();
			this.SubmissionTypeIds = new List<int>();
			this.SubmissionTypes = new List<SelectListItem>();
			this.DocumentTypeIds = new List<int>();
			this.DocumentTypes = new List<SelectListItem>();
			this.ContractTypeIds = new List<int>();
			this.ContractTypes = new List<SelectListItem>();
			this.BillingPartyIds = new List<int>();
			this.BillingParties = new List<SelectListItem>();
			this.AvailableBillingSheetIds = new List<int>();
			this.AvailableBillingSheets = new List<SelectListItem>();
			this.LocationIds = new List<int>();
			this.Locations = new List<SelectListItem>();
			this.CompanyIds = new List<int>();
			this.Companies = new List<SelectListItem>();

			this.ReportTypeOptions = LookupsStandard.ConvertEnum<ReportTypes>(showBlank: true, useDescriptionAsValue: true);
			this.EUReportTypeOptions = LookupsStandard.ConvertEnum<EUReport>(showBlank: true).ToList();
			this.SpecializedTypeOptions = LookupsStandard.ConvertEnum<SpecializedTypes>(useDescriptionAsValue: true);
			this.BillingSheetOptions = LookupsStandard.ConvertEnum<LetterBookBillingSheet>(showBlank: true).ToList();
			this.BallyTypeOptions = LookupsStandard.ConvertEnum<LetterBookBallyType>(showBlank: true).ToList();
		}

		public int Id { get; set; }
		public List<string> JurisdictionIds { get; set; }
		public List<SelectListItem> Jurisdictions { get; set; }

		public List<string> ManufacturerCodes { get; set; }
		public List<SelectListItem> Manufacturers { get; set; }

		public List<int> SubmissionTypeIds { get; set; }
		public List<SelectListItem> SubmissionTypes { get; set; }

		public List<int> DocumentTypeIds { get; set; }
		public List<SelectListItem> DocumentTypes { get; set; }

		public List<int> ContractTypeIds { get; set; }
		public List<SelectListItem> ContractTypes { get; set; }

		public List<int> BillingPartyIds { get; set; }
		public List<SelectListItem> BillingParties { get; set; }

		public List<int> LocationIds { get; set; }
		public List<SelectListItem> Locations { get; set; }

		public List<int> CompanyIds { get; set; }
		public List<SelectListItem> Companies { get; set; }

		public int? LetterheadId { get; set; }
		public int? CorrectionLetterheadId { get; set; }
		public string LetterheadName { get; set; }
		public string CorrectionLetterheadName { get; set; }
		public bool RepeatLetterhead { get; set; }
		public bool RepeatCorrectionLetterhead { get; set; }
		public string ApprovalStatus { get; set; }
		public bool Billable { get; set; }
		public int? BillingSheetId { get; set; } //Default BillingSheet
		public string BillingSheet { get; set; }
		public List<int> AvailableBillingSheetIds { get; set; } //if not Defaut set, these are preferred options
		public List<SelectListItem> AvailableBillingSheets { get; set; }

		public string SpecializedType { get; set; }
		public string ReportType { get; set; }
		public string BillingLab { get; set; }
		public int? BillingLabId { get; set; }
		public bool IsFixedPrice { get; set; }
		public string BillingNote { get; set; }

		public string Mode { get; set; }

		public IEnumerable<SelectListItem> StatusOptions { get; set; }
		public IEnumerable<SelectListItem> BillingSheetOptions { get; set; }
		public IEnumerable<SelectListItem> SpecializedTypeOptions { get; set; }
		public IEnumerable<SelectListItem> ReportTypeOptions { get; set; }
		public IEnumerable<SelectListItem> EUReportTypeOptions { get; set; }
		public IEnumerable<SelectListItem> BallyTypeOptions { get; set; }

	}
}