﻿using Submissions.Web.Areas.Tools.Models;
using System;
using System.Collections.Generic;

namespace Submissions.Web.Areas.Lookups.Models
{
	public class PlatformPiecesTemplateDTO
	{
		public PlatformPiecesTemplateDTO()
		{
			Platform = new ManufacturerPlatform();
			PlatformPieces = new List<PlatformPieceDTO>();
			Jurisdictions = new List<JurisdictionInfo>();
		}
		public int Id { get; set; }
		public string Name { get; set; }
		public string Description { get; set; }
		public int OriginalId { get; set; }
		public DateTime AddDate { get; set; }
		public int AddUserId { get; set; }
		public string Manufacturer { get; set; }
		public string ManufacturerCode { get; set; }

		public ManufacturerPlatform Platform { get; set; }

		public List<PlatformPieceDTO> PlatformPieces { get; set; }
		public IList<JurisdictionInfo> Jurisdictions { get; set; }
	}
	public class PlatformPieceDTO
	{
		public int Id { get; set; }
		public int OrderId { get; set; }
		public string Name { get; set; }
		public bool Required { get; set; }
		public string Filter { get; set; }
		public bool Deleted { get; set; }
		public bool Modified { get; set; }
	}
}