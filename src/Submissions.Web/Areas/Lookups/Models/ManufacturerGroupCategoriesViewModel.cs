﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Submissions.Web.Areas.Lookups.Models
{
	public class ManufacturerGroupCategoriesIndexViewModel
	{
		public ManufacturerGroupCategoriesIndexViewModel()
		{
			this.ManufacturerGroupCategories = new List<ManufacturerGroupCategory>();
		}

		public IList<ManufacturerGroupCategory> ManufacturerGroupCategories { get; set; }
	}

	public class ManufacturerGroupCategoriesEditViewModel : Lookup
	{
		public int? Id { get; set; }
		[Required, StringLength(25), Remote("ValidateCode", "ManufacturerGroupCategories", AdditionalFields = "Id, Mode", ErrorMessage = "{0} already exists.")]
		public string Code { get; set; }
		[Required, StringLength(100)]
		public string Description { get; set; }
	}

	public class ManufacturerGroupCategory
	{
		public int Id { get; set; }
		public string Code { get; set; }
		public string Description { get; set; }
	}
}
