﻿using Submissions.Web.Models.Grids.Lookups;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Submissions.Web.Areas.Lookups.Models
{
	public class LetterContactsIndexViewModel
	{
		public LetterContactsIndexViewModel() { }

		public LetterContactsGrid LetterContactsGrid => new LetterContactsGrid();
		public List<SelectListItem> ContactTypeOptions { get; set; }
	}

	public class LetterContactsEditViewModel : Lookup
	{
		public LetterContactsEditViewModel()
		{
			this.ManufacturerCodes = new List<string>();
			this.JurisdictionCodes = new List<string>();
		}
		public int? Id { get; set; }
		[Display(Name = "Contact Type"), Required]
		public List<int> ContactTypeIds { get; set; }
		public List<SelectListItem> ContactTypeOptions { get; set; }
		[Display(Name = "Name"), Required, StringLength(100)]
		public string Name { get; set; }
		[Display(Name = "Address Line 1"), Required, StringLength(500)]
		public string AddressLine1 { get; set; }
		[Display(Name = "Address Line 2"), StringLength(500)]
		public string AddressLine2 { get; set; }
		[Display(Name = "Address Line 3"), StringLength(500)]
		public string AddressLine3 { get; set; }
		[Display(Name = "Address Line 4"), StringLength(500)]
		public string AddressLine4 { get; set; }
		[Display(Name = "Address Line 5"), StringLength(500)]
		public string AddressLine5 { get; set; }
		[Display(Name = "Address Line 6"), StringLength(500)]
		public string AddressLine6 { get; set; }
		[Display(Name = "Manufacturers")]
		public List<string> ManufacturerCodes { get; set; }
		public IList<SelectListItem> Manufacturers { get; set; }
		[Display(Name = "Jurisdictions")]
		public List<string> JurisdictionCodes { get; set; }
		public IList<SelectListItem> Jurisdictions { get; set; }
		public bool ApplyAllJur { get; set; }
		public bool ApplyAllManuf { get; set; }

	}
}