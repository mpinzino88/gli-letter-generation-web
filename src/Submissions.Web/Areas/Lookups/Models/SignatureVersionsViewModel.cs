﻿using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Submissions.Web.Areas.Lookups.Models
{
	public class SignatureVersionsViewModel : Lookup
	{
		public int? Id { get; set; }
		[Required, StringLength(50), Remote("ValidateVersion", "SignatureVersions", AdditionalFields = "Id, Mode", ErrorMessage = "{0} already exists.")]
		public string Version { get; set; }
	}
}