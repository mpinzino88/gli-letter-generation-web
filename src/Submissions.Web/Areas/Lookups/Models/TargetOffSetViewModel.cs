﻿using Submissions.Common;
using Submissions.Web.Models.Grids.Lookups;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Submissions.Web.Areas.Lookups.Models
{
	public class TargetOffSetViewModel
	{
		public TargetOffSetViewModel()
		{
			TargetOffSetGrid = new TargetOffSetGrid();
		}
		public TargetOffSetGrid TargetOffSetGrid { get; set; }
	}

	public class TargetOffSetEditViewModel
	{
		public int Id { get; set; }

		public string JurisdictionID { get; set; }

		[Display(Name = "Jurisdiction"), Required, Remote("ValidateJurisdiction", "TargetOffSet", AdditionalFields = "Id, Mode", ErrorMessage = "{0} already exists.")]
		public string Jurisdiction { get; set; }

		[Display(Name = "OffSet Days"), Required]
		public int TargetOffSet { get; set; }

		[Display(Name = "Reason"), StringLength(50), Required]
		public string Reason { get; set; }

		public Mode Mode { get; set; }
	}
}