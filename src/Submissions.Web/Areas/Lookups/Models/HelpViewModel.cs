﻿using Submissions.Common;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Submissions.Web.Areas.Lookups.Models
{
	public class HelpEditViewModel
	{
		public HelpEditViewModel()
		{
			this.Help = new HelpItem();
		}

		public Mode Mode { get; set; }
		public HelpItem Help { get; set; }
	}

	public class HelpIndexViewModel
	{
		public HelpIndexViewModel()
		{
			this.HelpData = new List<HelpItem>();
		}

		public IList<HelpItem> HelpData { get; set; }
	}

	public class HelpItem
	{
		public int? Id { get; set; }
		[Required, StringLength(20)]
		public string Code { get; set; }
		[Required, StringLength(200)]
		public string Description { get; set; }
		[AllowHtml]
		public string Html { get; set; }
	}
}