﻿using Submissions.Common.Kendo;
using Submissions.Web.Models.Grids.Lookups;
using Submissions.Web.Models.Query;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web;

namespace Submissions.Web.Areas.Lookups.Models
{
	public class UsersIndexViewModel
	{
		public UsersIndexViewModel()
		{
			this.UserGrid = new UsersGrid();
			this.Filters = new UserSearch();
		}

		public UsersGrid UserGrid { get; set; }
		public UserSearch Filters { get; set; }

	}

	public class UsersEditViewModel : Lookup
	{
		public UsersEditViewModel()
		{
			this.ProfilePictures = new List<KendoFile>();
			this.ProfilePicturesUpload = new List<HttpPostedFileBase>();
			this.WorkdaySchedule = new List<WorkdayScheduleViewModel>();
			this.OldPermissions = new OldPermissions();
		}

		public int? Id { get; set; }
		[Required]
		public string Username { get; set; }
		[Display(Name = "First Name"), Required]
		public string FName { get; set; }
		[Display(Name = "Last Name"), Required]
		public string LName { get; set; }
		public string Name { get { return this.FName + " " + this.LName; } }
		[Required]
		public string Email { get; set; }
		[Display(Name = "Office Phone")]
		public string Phone { get; set; }
		[Display(Name = "Mobile Phone")]
		public string MobilePhone { get; set; }
		[Display(Name = "Liaison")]
		public bool IsLiaison { get; set; }
		[Display(Name = "Active")]
		public bool Status { get; set; }
		[Display(Name = "Home Office"), Required]
		public int? HomeOfficeId { get; set; }
		public string HomeOffice { get; set; }
		[Display(Name = "Current Office")]
		public int? LocationId { get; set; }
		public string Location { get; set; }
		[Display(Name = "Manager")]
		public int? ManagerId { get; set; }
		public string Manager { get; set; }
		[Display(Name = "Department"), Required]
		public int? DepartmentId { get; set; }
		public string Department { get; set; }
		[Display(Name = "Role"), Required]
		public int? RoleId { get; set; }
		public string Role { get; set; }
		public List<AccessRightCheckbox> AccessRights { get; set; }
		public DateTime? AddDate { get; set; }
		public int? PermissionId { get; set; }
		public string sAMAccountName { get; set; }
		[Display(Name = "SID")]
		public string LDAP_SID { get; set; }
		[Display(Name = "ADsPath")]
		public string ADUserPath { get; set; }
		[Display(Name = "Ceridian Employee Number"), StringLength(255)]
		public string CeridianEmployeeNum { get; set; }
		public IList<KendoFile> ProfilePictures { get; set; }
		public IList<HttpPostedFileBase> ProfilePicturesUpload { get; set; }
		[Display(Name = "Workday Hours"), DisplayFormat(DataFormatString = "{0:0.#}", ApplyFormatInEditMode = true), Range(0, 10)]
		public decimal WorkdayHours { get; set; }
		[Display(Name = "Workday Schedule")]
		public IList<WorkdayScheduleViewModel> WorkdaySchedule { get; set; }
		public OldPermissions OldPermissions { get; set; }
		[Display(Name = "Team Owner")]
		public int? TeamId { get; set; }
		public string Team { get; set; }
		[Display(Name = "Supplier Focus")]
		public int? SupplierFocusId { get; set; }
		public string SupplierFocus { get; set; }
		public int EngineeringDeptId { get; set; }
		[Display(Name = "Public Knowledge User ID")]
		public int? PkInternalMetricsEmployeeId { get; set; }
		[Display(Name = "Business Owner")]
		public int? BusinessOwnerId { get; set; }
		public string BusinessOwner { get; set; }
	}

	public class OldPermissions
	{
		public int? Id { get; set; }
		[Display(Name = "Create Submission Record")]
		public bool CreateSubmission { get; set; }
		[Display(Name = "Edit Submission Record")]
		public bool EditSubmission { get; set; }
		[Display(Name = "Delete Submission Record")]
		public bool DeleteSubmission { get; set; }
		[Display(Name = "Create Jurisdiction")]
		public bool CreateJurisdiction { get; set; }
		[Display(Name = "Edit Jurisdiction")]
		public bool EditJurisdiction { get; set; }
		[Display(Name = "Delete Jurisdiction")]
		public bool DeleteJurisdiction { get; set; }

		[Display(Name = "Process PCT/PCR/PCS Requests")]
		public bool QAElecTransf { get; set; }
		[Display(Name = "Can Set Status to AP or TC")]
		public bool CanSetSubStatusAP { get; set; }
		[Display(Name = "Can Only Set Status to/from OH")]
		public bool CanSetSubStatusOH { get; set; }
		[Display(Name = "Cannot Set Status to AP or NL")]
		public bool CannotChangeAPNL { get; set; }
		[Display(Name = "Act as Data Entry Updater")]
		public bool DEUpdaterRole { get; set; }
		[Display(Name = "Act as Data Entry Scanner")]
		public bool DEScannerRole { get; set; }
		[Display(Name = "Set Status to CP")]
		public bool CanSetStatusCP { get; set; }
		[Display(Name = "Can Create Draft Letters")]
		public bool CanCreateDraftLetters { get; set; }
		[Display(Name = "Can Process COS")]
		public bool CanProcessCos { get; set; }
		[Display(Name = "Can Open the Detail Billing Report Page")]
		public bool CanRunDetailBillReport { get; set; }
		[Display(Name = "Can Delete File Number")]
		public bool DeleteFileNumber { get; set; }

		[Display(Name = "Can Unlock COS Form")]
		public bool CanUnlockCOSForm { get; set; }
	}

	public class WorkdayScheduleViewModel
	{
		public int? Id { get; set; }
		public int UserId { get; set; }
		public DayOfWeek DayOfWeek { get; set; }
		[DisplayFormat(DataFormatString = "{0:0.##}", ApplyFormatInEditMode = true), Range(0, 10)]
		public decimal Hours { get; set; }
	}
}