﻿using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Submissions.Web.Areas.Lookups.Models
{
	public class CountryViewModel : Lookup
	{
		public int? Id { get; set; }
		[Required, StringLength(50), Remote("ValidateCountry", "Countries", AdditionalFields = "Id, Mode", ErrorMessage = "{0} already exists.")]
		public string Code { get; set; }
		[Required, StringLength(255), Remote("ValidateCountry", "Countries", AdditionalFields = "Id, Mode", ErrorMessage = "{0} already exists.")]
		public string Description { get; set; }
	}
}