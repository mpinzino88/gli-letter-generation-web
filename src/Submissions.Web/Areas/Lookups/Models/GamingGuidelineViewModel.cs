﻿using EF.Submission;
using Newtonsoft.Json;
using Submissions.Common;
using Submissions.Web.Models.Grids.GamingGuidelines;
using Submissions.Web.Models.Query;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace Submissions.Web.Areas.Lookups.Models
{
	public class GamingGuidelineIndexViewModel
	{
		public GamingGuidelineIndexViewModel()
		{
			this.Filters = new GamingGuidelineSearch();
		}
		public GamingGuidelineSearch Filters { get; set; }
		public GamingGuidelineGrid GamingGuidelineGrid { get; set; }
		public string NonCompPaytableList
		{
			get
			{
				var valList = new List<string>();
				foreach (GamingGuidelineNonCompliantPaytableHandling item in Enum.GetValues(typeof(GamingGuidelineNonCompliantPaytableHandling)))
				{
					valList.Add(item.GetEnumDescription());
				}

				return JsonConvert.SerializeObject(valList);
			}
		}
		public GamingGuidelineUserTemplateViewModel UserTemplate { get; set; }

		public List<char> Alphabets
		{
			get
			{
				List<char> result = new List<char>();
				for (char c = 'A'; c <= 'Z'; c++)
				{
					result.Add(c);
				}
				return result;
			}
		}
	}
	public class GamingGuidelineViewModel : Lookup
	{
		public GamingGuidelineViewModel()
		{
			this.DefaultGamingGuidelineLimit = new GamingGuidelineLimitViewModel();

			this.DefaultGamingGuidelineAttribute = new GamingGuidelineAttributeViewModel();

			this.DefaultGamingGuidelineOddsRequirement = new GamingGuidelineOddsRequirementsViewModel();

			this.DefaultGamingGuidelineProhibitedGameType = new GamingGuidelineProhibitedGameTypeViewModel();

			this.DefaultGamingGuidelineProtocolType = new GamingGuidelineProtocolTypeViewModel();

			this.DefaultGamingGuidelineRTPRequirement = new GamingGuidelineRTPRequirementsViewModel();

			this.DefaultGamingGuidelineTechType = new GamingGuidelineTechTypesViewModel();
			//this.NewUserComments = new List<GamingGuidelineCommentViewModel>();
			CompilationMethods = new List<SelectListItem>();
		}

		public int Id { get; set; }
		[Required]
		public string JurisdictionId { get; set; }
		[Display(Name = "Jurisdiction Name: ")]
		public string JurisdictionName { get; set; }
		public int SelectedJurisdiction { get; set; }
		public IList<SelectListItem> JurisdictionOptions { get; set; }
		[Display(Name = "Special Handling: ")]
		public bool SpecialHandling { get; set; }
		[Display(Name = "Compilation of Sourcecode: ")]
		public bool SourceCodeCompilation { get; set; }
		[Display(Name = "Forensic: ")]
		public bool Forensic { get; set; }
		[Display(Name = "Progressive Reconciliation: ")]
		public bool ProgressiveReconciliation { get; set; }
		[Display(Name = "Offensive Material Evaluation: ")]
		public bool OffensiveMaterialEvaluation { get; set; }
		[Display(Name = "Underage Advertising Evaluation: ")]
		public bool UnderageAdvertisingEvaluation { get; set; }
		[Display(Name = "Responsible Gaming: ")]
		public bool ResponsibleGaming { get; set; }
		[Display(Name = "Remote Access ITL: ")]
		public bool RemoteAccessITL { get; set; }
		[Display(Name = "Remote Access: ")]
		public bool RemoteAccess { get; set; }
		[Display(Name = "Point Click Review: ")]
		public bool Portal { get; set; }
		[Required]
		[Display(Name = "Non-Compliant Paytable Handling: ")]
		public GamingGuidelineNonCompliantPaytableHandling NonCompliantPaytableHandling { get; set; }
		[Display(Name = "Wireless Technology: ")]
		public GamingGuidelineAllowedProhibited WirelessTechnology { get; set; }
		[Display(Name = "SkillGames: ")]
		public GamingGuidelineAllowedProhibited SkillGames { get; set; }
		[Display(Name = "Bonus Pots: ")]
		public GamingGuidelineBonusPot BonusPot { get; set; }
		[Display(Name = "Random Number Generator: ")]
		public GamingGuidelineRandomNumberGenerator RandomNumberGenerator { get; set; }
		[Display(Name = "Geographical Testing Location Only: ")]
		public GamingGuidelineGeographicalTestingLocationOnly GeographicalTestingLocationOnly { get; set; }
		[Display(Name = "Physical Testing Location: ")]
		public GamingGuidelinePhysicalTestingLocation PhysicalTestingLocation { get; set; }
		public IList<SelectListItem> NonCompliantPaytableHandlingOptions { get; set; }
		public IList<SelectListItem> WirelessTechnologyOptions { get; set; }
		public IList<SelectListItem> SkillGamesOptions { get; set; }
		public IList<SelectListItem> BonusPotOptions { get; set; }
		public IList<SelectListItem> RandomNumberGeneratorOptions { get; set; }
		public IList<SelectListItem> GeographicalTestingLocationOnlyOptions { get; set; }
		public IList<SelectListItem> PhysicalTestingLocationOptions { get; set; }
		[StringLength(4000)]
		[Display(Name = "Special Math Instructions: ")]
		public string SpecialMathInstructions { get; set; }
		[StringLength(4000)]
		[Display(Name = "General Notes: ")]
		public string Comments { get; set; }
		[Display(Name = "New Technology Review Needed: ")]
		public bool NewTechnologyReviewNeeded { get; set; }
		[Display(Name = "Documented Restrictions on Skill: ")]
		public string DocumentedRestrictionsOnSkill { get; set; }
		[Display(Name = "Cryptographic RNG: ")]
		public bool CryptographicRNG { get; set; }
		public string SpecialName { get; set; }
		public string Country { get; set; }
		[Display(Name = "Primary Owner: ")]
		public int? OwnerId { get; set; }
		public string Owner { get; set; }
		[Display(Name = "Secondary Owner: ")]
		public int? SecondaryOwnerId { get; set; }
		public string SecondaryOwner { get; set; }
		[Display(Name = "Website Link: ")]
		public string WebsiteLink { get; set; }
		public int CommentCount { get; set; }
		public bool editAllowed { get; set; }
		[Display(Name = "Acceptable Ways of Compilation: ")]
		public string[] CompilationMethodIds { get; set; }
		public IList<SelectListItem> CompilationMethods { get; set; }
		public tbl_lst_fileJuris Jurisdiction { get; set; }
		[ScriptIgnore] //Keeps the JSONification from trying to serialize/deserialize this.
		public IList<GamingGuidelineLimits> GamingGuidelineLimits { get; set; }
		public IList<GamingGuidelineLimitViewModel> GamingGuidelineLimitViewModels
		{
			get
			{
				var gamingGuidelineLimitViewModels = GamingGuidelineLimits
				.Select(x => new GamingGuidelineLimitViewModel
				{
					Id = x.Id,
					Limit = x.Limit,
					xBet = x.xBet,
					Note = x.Note,
					LimitTypeId = (int)x.LimitTypeId,
					LimitType = new SelectListItem { Value = x.LimitTypeId.ToString(), Text = x.GamingGuidelineLimit.Name },
					GameTypeId = (int)x.GameTypeId,
					GameType = new SelectListItem { Value = x.GameTypeId.ToString(), Text = x.GameType.Name },
					GameTypeName = x.GameType.Name
				})
				.OrderBy(x => x.Id)
				.ToList();

				return gamingGuidelineLimitViewModels;
			}
		}

		[ScriptIgnore]
		public IList<GamingGuidelineAttributes> GamingGuidelineAttributes { get; set; }
		public List<SelectListItem> GamingGuidelineAttributeStatus { get; set; }
		public IList<GamingGuidelineAttributeViewModel> GamingGuidelineAttributeViewModels
		{
			get
			{
				var gamingGuidelineAttributeViewModels = GamingGuidelineAttributes
					.Select(x => new GamingGuidelineAttributeViewModel
					{
						Id = x.Id,
						Name = x.GamingGuidelineAttribute.Name,
						AttributeStatusId = x.AttributeStatusId,
						AttributeStatus = new SelectListItem { Value = x.AttributeStatusId.ToString(), Text = ((GamingGuidelineAttributeStatus)x.AttributeStatusId).ToString() },
						AttributeId = (int)x.AttributeId,
						Attribute = new List<SelectListItem> { new SelectListItem { Value = x.AttributeId.ToString(), Text = x.GamingGuidelineAttribute.Name } },
						Note = x.Note
					})
					.OrderBy(x => x.Id)
					.ToList();
				return gamingGuidelineAttributeViewModels;
			}
		}

		[ScriptIgnore]
		public IList<GamingGuidelineProhibitedGameTypes> GamingGuidelineProhibitedGameTypes { get; set; }
		public List<SelectListItem> GamingGuidelineProhibitedGameTypeStatus { get; set; }
		public IList<GamingGuidelineProhibitedGameTypeViewModel> GamingGuidelineProhibitedGameTypeViewModels
		{
			get
			{
				var gamingGuidelineProhibitedGameTypeViewModels = GamingGuidelineProhibitedGameTypes
					.Select(x => new GamingGuidelineProhibitedGameTypeViewModel
					{
						Id = x.Id,
						Note = x.Notes,
						ProhibitedStatusId = x.ProhibitedGameTypeStatusId,
						ProhibitedGameTypeStatus = new SelectListItem { Value = x.ProhibitedGameTypeStatusId.ToString(), Text = ((GamingGuidelineProhibitedGameTypeStatus)x.ProhibitedGameTypeStatusId).ToString() },
						GameTypeId = (int)x.GameTypeId,
						GameType = new SelectListItem { Value = x.GameTypeId.ToString(), Text = x.GameType.Name },
						GameTypeName = x.GameType.Name
					})
					.OrderBy(x => x.Id)
					.ToList();
				return gamingGuidelineProhibitedGameTypeViewModels;
			}
		}

		[ScriptIgnore]
		public IList<GamingGuidelineProtocolTypes> GamingGuidelineProtocolTypes { get; set; }
		public IList<GamingGuidelineProtocolTypeViewModel> GamingGuidelineProtocolTypeViewModels
		{
			get
			{
				var gamingGuidelineProtocolsViewModels = GamingGuidelineProtocolTypes
					.Select(x => new GamingGuidelineProtocolTypeViewModel
					{
						Id = x.Id,
						Note = x.Note,
						Version = x.Version,
						ProtocolTypeId = (int)x.ProtocolTypeId,
						ProtocolType = new SelectListItem { Value = x.ProtocolTypeId.ToString(), Text = x.ProtocolType.Name },
						ProtocolTypeName = x.ProtocolType.Name
					})
				.OrderBy(x => x.Id)
				.ToList();
				return gamingGuidelineProtocolsViewModels;
			}
		}


		[ScriptIgnore]
		public IList<GamingGuidelineOddsRequirements> GamingGuidelineOddsRequirements { get; set; }
		public IList<GamingGuidelineOddsRequirementsViewModel> GamingGuidelineOddsRequirementViewModels
		{
			get
			{
				var gamingGuidelineOddsRequirementViewModels = GamingGuidelineOddsRequirements
					.Select(x => new GamingGuidelineOddsRequirementsViewModel
					{
						Id = x.Id,
						Note = x.Note,
						Odds = x.Odds.ToString(),
						AwardTypeId = (int)x.AwardTypeId,
						AwardType = new SelectListItem() { Value = x.AwardTypeId.ToString(), Text = ((GamingGuidelineAwardType)x.AwardTypeId).ToString() },
						PrizeTypeId = (int)x.PrizeTypeId,
						PrizeType = new SelectListItem() { Value = x.PrizeTypeId.ToString(), Text = ((GamingGuidelinePrizeType)x.PrizeTypeId).ToString() },
						ProgressiveTypeId = (int)x.ProgressiveTypeId,
						ProgressiveType = new SelectListItem() { Value = x.ProgressiveTypeId.ToString(), Text = ((GamingGuidelineProgressiveType)x.ProgressiveTypeId).ToString() }
					})
					.OrderBy(x => x.Id)
					.ToList();
				return gamingGuidelineOddsRequirementViewModels;
			}
		}
		public IList<SelectListItem> GamingGuidelineAwardTypes { get; set; }
		public IList<SelectListItem> GamingGuidelinePrizeTypes { get; set; }
		public IList<SelectListItem> GamingGuidelineProgressiveTypes { get; set; }

		[ScriptIgnore]
		public IList<GamingGuidelineTechTypes> GamingGuidelineTechTypes { get; set; }
		public IList<GamingGuidelineTechTypesViewModel> GamingGuidelineTechTypesViewModels
		{
			get
			{
				var gamingGuidelineTechTypesViewModels = GamingGuidelineTechTypes
					.Select(x => new GamingGuidelineTechTypesViewModel
					{
						Id = x.Id,
						Note = x.Notes,
						TechTypeId = (int)x.TechTypeId,
						TechType = new List<SelectListItem> { new SelectListItem { Value = x.TechTypeId.ToString(), Text = x.GamingGuidelineTechType.Name } },
						TechTypeStatusId = (int)x.TechTypeStatusId,
						TechTypeStatus = new SelectListItem { Value = x.TechTypeStatusId.ToString(), Text = ((GamingGuidelineTechTypeStatus)x.TechTypeStatusId).ToString() },
						TechTypeName = x.GamingGuidelineTechType.Name
					})
					.OrderBy(x => x.Id)
					.ToList();
				return gamingGuidelineTechTypesViewModels;
			}
		}
		public IList<SelectListItem> GamingGuidelineTechTypeStatus { get; set; }



		[ScriptIgnore]
		public IList<GamingGuidelineRTPRequirements> GamingGuidelineRTPRequirements { get; set; }
		public IList<GamingGuidelineRTPRequirementsViewModel> GamingGuidelineRTPRequirementsViewModels
		{
			get
			{
				var gamingGuidelineRTPRequirementsViewModels = GamingGuidelineRTPRequirements
					.Select(x => new GamingGuidelineRTPRequirementsViewModel
					{
						Id = x.Id,
						GameTypeId = (int)x.GameTypeId,
						GameType = new SelectListItem { Value = x.GameTypeId.ToString(), Text = x.GameType.Name },
						GameTypeName = x.GameType.Name,
						Note = x.Note,
						RTP = x.RTP,
						RTPTypeId = (int)x.RTPType,
						RTPType = new SelectListItem { Value = x.RTPType.ToString(), Text = ((GamingGuidelineRTPType)x.RTPType).ToString() },
						DenomSymbol = x.DenomSymbol
					})
					.OrderBy(x => x.Id)
					.ToList();
				return gamingGuidelineRTPRequirementsViewModels;
			}
		}
		public IList<SelectListItem> GamingGuidelineRTPType { get; set; }

		public GamingGuidelineRTPRequirementsViewModel DefaultGamingGuidelineRTPRequirement { get; set; }
		public GamingGuidelineLimitViewModel DefaultGamingGuidelineLimit { get; set; }
		public GamingGuidelineAttributeViewModel DefaultGamingGuidelineAttribute { get; set; }
		public GamingGuidelineOddsRequirementsViewModel DefaultGamingGuidelineOddsRequirement { get; set; }
		public GamingGuidelineProhibitedGameTypeViewModel DefaultGamingGuidelineProhibitedGameType { get; set; }
		public GamingGuidelineProtocolTypeViewModel DefaultGamingGuidelineProtocolType { get; set; }
		public GamingGuidelineTechTypesViewModel DefaultGamingGuidelineTechType { get; set; }
		public GamingGuidelineCommentViewModel DefaultUserComment { get; set; }
		public IList<GamingGuidelineCommentViewModel> UserComments { get; set; }
		//public IList<GamingGuidelineCommentViewModel> NewUserComments { get; set; }
	}

	public class GamingGuidelineLimitViewModel
	{
		public GamingGuidelineLimitViewModel()
		{
			this.LimitType = new SelectListItem();
			this.GameType = new SelectListItem();
		}

		public string GameTypeName { get; set; }

		public bool _destroy { get; set; }
		public int? Id { get; set; }
		public decimal Limit { get; set; }
		public bool xBet { get; set; }
		public string Note { get; set; }

		public int? LimitTypeId { get; set; }
		public int? GameTypeId { get; set; }

		public SelectListItem LimitType { get; set; }
		public SelectListItem GameType { get; set; }

		public string LimitTypeName { get { return LimitType.Text; } }
		public string LimitTypeNameWithLimit { get { return LimitTypeName + " - " + Limit; } }
		public string LimitTypeStringWithXBet { get { return xBet ? LimitTypeNameWithLimit + " - xBet" : LimitTypeNameWithLimit; } }

	}

	public class GamingGuidelineAttributeViewModel
	{
		public GamingGuidelineAttributeViewModel()
		{
			this.Attribute = new List<SelectListItem>();
			this.AttributeStatus = new SelectListItem();
		}

		public string Name { get; set; }

		public bool _destroy { get; set; }
		public int? Id { get; set; }
		public string Note { get; set; }

		public int AttributeStatusId { get; set; }
		public int AttributeId { get; set; }

		public IList<SelectListItem> Attribute { get; set; }
		public SelectListItem AttributeStatus { get; set; }

		public string getAttributeValues
		{
			get { return (Name + " - " + AttributeStatus.Text); }
		}
	}

	public class GamingGuidelineOddsRequirementsViewModel
	{
		public GamingGuidelineOddsRequirementsViewModel()
		{
			this.AwardType = new SelectListItem();
			this.PrizeType = new SelectListItem();
			this.ProgressiveType = new SelectListItem();
		}

		public bool _destroy { get; set; }
		public int? Id { get; set; }
		public string Note { get; set; }

		public int AwardTypeId { get; set; }
		public int PrizeTypeId { get; set; }
		public int ProgressiveTypeId { get; set; }
		public string Odds { get; set; }

		public SelectListItem AwardType { get; set; }
		public SelectListItem PrizeType { get; set; }
		public SelectListItem ProgressiveType { get; set; }

		public string OddsRequirementDisplayString
		{
			get { return (PrizeType.Text + " - " + ProgressiveType.Text + " Progressive Type - 1 in " + string.Format("{0:n0}", Int64.Parse(!string.IsNullOrEmpty(Odds) ? Odds : "0"))); }
		}
	}

	public class GamingGuidelineProhibitedGameTypeViewModel
	{
		public GamingGuidelineProhibitedGameTypeViewModel()
		{
			this.GameType = new SelectListItem();
			this.ProhibitedGameTypeStatus = new SelectListItem();
		}

		public string GameTypeName { get; set; }

		public bool _destroy { get; set; }
		public int? Id { get; set; }
		public string Note { get; set; }

		public int ProhibitedStatusId { get; set; }
		public int? GameTypeId { get; set; }

		public SelectListItem GameType { get; set; }
		public SelectListItem ProhibitedGameTypeStatus { get; set; }

	}

	public class GamingGuidelineProtocolTypeViewModel
	{
		public GamingGuidelineProtocolTypeViewModel()
		{
			this.ProtocolType = new SelectListItem();
		}

		public string ProtocolTypeName { get; set; }

		public bool _destroy { get; set; }
		public int? Id { get; set; }
		public string Version { get; set; }
		public string Note { get; set; }

		public int? ProtocolTypeId { get; set; }

		public SelectListItem ProtocolType { get; set; }

		public string ProtocolNameAndVersion
		{
			get { return (ProtocolTypeName + " (" + Version + ")"); }
		}
	}

	public class GamingGuidelineRTPRequirementsViewModel
	{
		public GamingGuidelineRTPRequirementsViewModel()
		{
			this.GameType = new SelectListItem();
			this.RTPType = new SelectListItem();
		}

		public string GameTypeName { get; set; }

		public bool _destroy { get; set; }
		public int? Id { get; set; }
		public double RTP { get; set; }
		public string Note { get; set; }

		public int RTPTypeId { get; set; }
		public int? GameTypeId { get; set; }
		public double? Denom { get; set; }
		public string DenomSymbol { get; set; }

		public SelectListItem RTPType { get; set; }
		public SelectListItem GameType { get; set; }

		public string RtpRequirementNameAndValueString
		{
			get { return (RTPType.Text + " - " + RTP + "%"); }
		}
	}

	public class GamingGuidelineTechTypesViewModel
	{
		public GamingGuidelineTechTypesViewModel()
		{
			this.TechTypeStatus = new SelectListItem();
			this.TechType = new List<SelectListItem>();
		}

		public string TechTypeName { get; set; }

		public bool _destroy { get; set; }
		public int? Id { get; set; }
		public string Note { get; set; }

		public int TechTypeStatusId { get; set; }
		public int TechTypeId { get; set; }

		public SelectListItem TechTypeStatus { get; set; }
		public IList<SelectListItem> TechType { get; set; }

		public string getTechTypeValues
		{
			get { return (TechTypeName + " - " + TechTypeStatus.Text); }
		}

	}

	//Used for posting Gaming Guidelines back to Edit for saving.
	public class GamingGuidelineViewModelThin : Lookup
	{
		public GamingGuidelineViewModelThin()
		{
			this.CompilationMethods = new List<SelectListItem>();
		}

		public int Id { get; set; }

		[Required]
		public string JurisdictionId { get; set; }

		[Display(Name = "Jurisdiction Name")]
		public string JurisdictionName { get; set; }

		[Display(Name = "Special Handling")]
		public bool SpecialHandling { get; set; }
		public bool Portal { get; set; }
		[Display(Name = "Compilation of Sourcecode")]
		public bool SourceCodeCompilation { get; set; }
		[Display(Name = "Forensic")]
		public bool Forensic { get; set; }
		[Display(Name = "Progressive Reconciliation")]
		public bool ProgressiveReconciliation { get; set; }
		[Display(Name = "Offensive Material Evaluation")]
		public bool OffensiveMaterialEvaluation { get; set; }
		[Display(Name = "Underage Advertising Evaluation")]
		public bool UnderageAdvertisingEvaluation { get; set; }
		[Display(Name = "Responsible Gaming")]
		public bool ResponsibleGaming { get; set; }
		[Display(Name = "Remote Access ITL")]
		public bool RemoteAccessITL { get; set; }
		[Display(Name = "Remote Access")]
		public bool RemoteAccess { get; set; }
		public string[] CompilationMethodIds { get; set; }
		public IList<SelectListItem> CompilationMethods { get; set; }
		[Required]
		[Display(Name = "Non-Compliant Paytable Handling")]
		public GamingGuidelineNonCompliantPaytableHandling NonCompliantPaytableHandling { get; set; }
		[Required]
		[Display(Name = "Wireless Technology")]
		public GamingGuidelineAllowedProhibited WirelessTechnology { get; set; }
		[Required]
		[Display(Name = "Skill Games")]
		public GamingGuidelineAllowedProhibited SkillGames { get; set; }
		[Required]
		[Display(Name = "Bonus Pots")]
		public GamingGuidelineBonusPot BonusPot { get; set; }
		[Required]
		[Display(Name = "Random Number Generator")]
		public GamingGuidelineRandomNumberGenerator RandomNumberGenerator { get; set; }
		[Required]
		[Display(Name = "Geographical Testing Location Only")]
		public GamingGuidelineGeographicalTestingLocationOnly GeographicalTestingLocationOnly { get; set; }
		[Required]
		[Display(Name = "Physical Testing Location")]
		public GamingGuidelinePhysicalTestingLocation PhysicalTestingLocation { get; set; }
		[StringLength(4000)]
		[Display(Name = "Special Math Instructions")]
		public string SpecialMathInstructions { get; set; }

		[StringLength(4000)]
		public string Comments { get; set; }

		public bool NewTechnologyReviewNeeded { get; set; }
		public bool CryptographicRNG { get; set; }
		[StringLength(1000)]
		public string DocumentedRestrictionsOnSkill { get; set; }
		public int? OwnerId { get; set; }
		public int? SecondaryOwnerId { get; set; }
		public int CommentCount { get; set; }
		public string WebsiteLink { get; set; }
		public bool editAllowed { get; set; }
		public tbl_lst_fileJuris Jurisdiction { get; set; }
		public GamingGuidelineSportsBetting GamingGuidelineSportsBetting { get; set; }

		public IList<GamingGuidelineLimitViewModel> GamingGuidelineLimitViewModels { get; set; }

		public IList<GamingGuidelineAttributeViewModel> GamingGuidelineAttributeViewModels { get; set; }

		public IList<GamingGuidelineProhibitedGameTypeViewModel> GamingGuidelineProhibitedGameTypeViewModels { get; set; }

		[Display(Name = "GamingGuidelineOddsRequirements")]
		public IList<GamingGuidelineOddsRequirementsViewModel> GamingGuidelineOddsRequirementViewModels { get; set; }

		[Display(Name = "GamingGuidelineTechTypes")]
		public IList<GamingGuidelineTechTypesViewModel> GamingGuidelineTechTypesViewModels { get; set; }

		public IList<GamingGuidelineProtocolTypeViewModel> GamingGuidelineProtocolTypeViewModels { get; set; }

		[Display(Name = "RTP Types")]
		public IList<GamingGuidelineRTPRequirementsViewModel> GamingGuidelineRTPRequirementsViewModels { get; set; }
		public IList<GamingGuidelineCommentViewModel> UserComments { get; set; }
		public IList<GamingGuidelineCommentViewModel> NewUserComments { get; set; }

	}

	public class GamingGuidelineUserTemplateViewModel
	{
		public GamingGuidelineUserTemplateViewModel()
		{
			this.NotAllowedGames = true;
			this.AnyAwardOR = true;
			this.AllAwardOR = true;
			this.TopAwardOR = true;
			this.NotAllowedTech = true;
			this.ProtocolValue = true;
			this.MaxRTP = true;
			this.MinRTP = true;
			this.NUStatus = true;
			this.AverageRTP = true;
			this.RequiredSafetyCertifications = true;
			this.Country = true;
			this.SpecialHandling = true;
			this.SourceCodeCompilation = true;
			this.Forensic = true;
			this.ProgressiveReconciliation = true;
			this.OffensiveMaterialEvaluation = true;
			this.UnderageAdvertisingEvaluation = true;
			this.ResponsibleGaming = true;
			this.RemoteAccessITL = true;
			this.RemoteAccess = true;
			this.Portal = true;
			this.NonCompliantPaytableHandling = true;
			this.WirelessTechnology = true;
			this.SkillGames = true;
			this.BonusPot = true;
			this.RandomNumberGenerator = true;
			this.GeographicalTestingLocationOnly = true;
			this.PhysicalTestingLocation = true;
			this.CompilationMethod = true;
			this.NewTechnologyReviewNeeded = true;
			this.SpecialMathInstructions = true;
			this.Comments = true;
			this.DocumentedRestrictionsOnSkill = true;
			this.ConditionalApproval = true;
			this.ConditionalRevocation = true;
			this.Interop = true;
			this.ProtocolTesting = true;
			this.WeightedRTP = true;
			this.GAT = true;
			this.CreditLimit = true;
			this.LinkMaxWin = true;
			this.LinkedSmallEstablishments = true;
			this.MaxBet = true;
			this.MaxWin = true;
			this.MinBet = true;
			this.MTGMMaxBet = true;
			this.MTGMMaxWin = true;
			this.ProgressiveCap = true;
			this.ProgressiveMaxWin = true;
			this.SAPMaxWin = true;
			this.WAPMaxWin = true;
			this.WinCap = true;
		}
		[Display(Name = "Prohibited Game Types")]
		public bool NotAllowedGames { get; set; }
		[Display(Name = "Any Award Odds Requirements")]
		public bool AnyAwardOR { get; set; }
		[Display(Name = "All Award Odds Requirements")]
		public bool AllAwardOR { get; set; }
		[Display(Name = "Top Award Odds Requirements")]
		public bool TopAwardOR { get; set; }
		[Display(Name = "Prohibited Technology")]
		public bool NotAllowedTech { get; set; }
		[Display(Name = "Protocols")]
		public bool ProtocolValue { get; set; }
		[Display(Name = "Maximum RTP")]
		public bool MaxRTP { get; set; }
		[Display(Name = "Minimum RTP")]
		public bool MinRTP { get; set; }
		[Display(Name = "Non-Mandatory Update")]
		public bool NUStatus { get; set; }
		[Display(Name = "Average RTP")]
		public bool AverageRTP { get; set; }
		[Display(Name = "Safety Certification")]
		public bool RequiredSafetyCertifications { get; set; }
		public int Id { get; set; }
		public int UserId { get; set; }
		[Display(Name = "Country")]
		public bool Country { get; set; }
		[Display(Name = "Special Handling Jurisdiction")]
		public bool SpecialHandling { get; set; }
		[Display(Name = "Compilation of Sourcecode")]
		public bool SourceCodeCompilation { get; set; }
		[Display(Name = "Forensic")]
		public bool Forensic { get; set; }
		[Display(Name = "Progressive Reconciliation")]
		public bool ProgressiveReconciliation { get; set; }
		[Display(Name = "Offensive Material Evaluation")]
		public bool OffensiveMaterialEvaluation { get; set; }
		[Display(Name = "Underage Advertising Evaluation")]
		public bool UnderageAdvertisingEvaluation { get; set; }
		[Display(Name = "Responsible Gaming")]
		public bool ResponsibleGaming { get; set; }
		[Display(Name = "Remote Access ITL")]
		public bool RemoteAccessITL { get; set; }
		[Display(Name = "Remote Access")]
		public bool RemoteAccess { get; set; }
		[Display(Name = "Point Click Review")]
		public bool Portal { get; set; }
		[Display(Name = "Non-Compliant Paytable Handling")]
		public bool NonCompliantPaytableHandling { get; set; }
		[Display(Name = "Wireless Technology")]
		public bool WirelessTechnology { get; set; }
		[Display(Name = "Skill Games")]
		public bool SkillGames { get; set; }
		[Display(Name = "Bonus Pots")]
		public bool BonusPot { get; set; }
		[Display(Name = "Random Number Generator")]
		public bool RandomNumberGenerator { get; set; }
		[Display(Name = "Geographical Testing Location Only")]
		public bool GeographicalTestingLocationOnly { get; set; }
		[Display(Name = "Physical Testing Location")]
		public bool PhysicalTestingLocation { get; set; }
		[Display(Name = "Acceptable Ways of Compilation: ")]
		public bool CompilationMethod { get; set; }
		[Display(Name = "New Technology Review")]
		public bool NewTechnologyReviewNeeded { get; set; }
		[Display(Name = "Special Math Instructions")]
		public bool SpecialMathInstructions { get; set; }
		[Display(Name = "Comment")]
		public bool Comments { get; set; }
		[Display(Name = "Documented Skill Restrictions")]
		public bool DocumentedRestrictionsOnSkill { get; set; }
		[Display(Name = "Conditional Approval")]
		public bool ConditionalApproval { get; set; }
		[Display(Name = "Conditional Revocation")]
		public bool ConditionalRevocation { get; set; }
		[Display(Name = "Inter-Operability Testing Required")]
		public bool Interop { get; set; }
		[Display(Name = "Protocol Testing Required")]
		public bool ProtocolTesting { get; set; }
		[Display(Name = "Weighted RTP")]
		public bool WeightedRTP { get; set; }
		[Display(Name = "Game Authentication Terminal Verification Required")]
		public bool GAT { get; set; }
		[Display(Name = "Credit Limit")]
		public bool CreditLimit { get; set; }
		[Display(Name = "Link Maximum Win")]
		public bool LinkMaxWin { get; set; }
		[Display(Name = "Linked Small Establishments")]
		public bool LinkedSmallEstablishments { get; set; }
		[Display(Name = "Maximum Bet")]
		public bool MaxBet { get; set; }
		[Display(Name = "Maximum Win")]
		public bool MaxWin { get; set; }
		[Display(Name = "Minimum Bet")]
		public bool MinBet { get; set; }
		[Display(Name = "Multi-Game Maximum Bet")]
		public bool MTGMMaxBet { get; set; }
		[Display(Name = "Multi-Game Maximum Win")]
		public bool MTGMMaxWin { get; set; }
		[Display(Name = "Progressive Limit")]
		public bool ProgressiveCap { get; set; }
		[Display(Name = "Progressive Maximum Win")]
		public bool ProgressiveMaxWin { get; set; }
		[Display(Name = "Stand Alone Progressive Maximum Win")]
		public bool SAPMaxWin { get; set; }
		[Display(Name = "Wide Area Progressive Maximum Win")]
		public bool WAPMaxWin { get; set; }
		[Display(Name = "Win Limit")]
		public bool WinCap { get; set; }
	}
	public class GamingGuidelineCommentViewModel
	{
		public int Id { get; set; }
		public int UserId { get; set; }
		public string UserName { get; set; }
		public string CommentString { get; set; }
		public DateTime AddedTime { get; set; }
		public string SimpleDateTime { get { return this.AddedTime.ToString("MM/dd/yyyy"); } }
		public bool IsEditable { get; set; }
		public bool _destroy { get; set; }
		public bool _dirty { get; set; }
	}
}