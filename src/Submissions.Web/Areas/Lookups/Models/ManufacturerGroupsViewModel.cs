﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Submissions.Web.Areas.Lookups.Models
{
	public class ManufacturerGroupsEditViewModel : Lookup
	{
		public ManufacturerGroupsEditViewModel()
		{
			this.DefaultStudioFilter = new Studio();
			this.Manufacturers = new List<SelectListItem>();
			this.Studios = new List<Studio>();
			this.DefaultStudioFilter = new Studio();
			this.ProductLines = new List<ProductLine>();
			this.DefaultProductLine = new ProductLine();
			this.Platforms = new List<Platform>();
			this.DefaultPlatform = new Platform();
		}

		public int? Id { get; set; }
		[Display(Name = "Group Name"), Required, StringLength(50)]
		public string Description { get; set; }
		[Display(Name = "Manufacturers"), Required]
		public string[] ManufacturerCodes { get; set; }
		public IList<SelectListItem> Manufacturers { get; set; }
		[Display(Name = "Category"), Required]
		public int? ManufacturerGroupCategoryId { get; set; }
		public string ManufacturerGroupCategory { get; set; }
		public int? ManufacturerGroupId { get; set; }
		[Display(Name = "Visibility"), Required]
		public bool isVisible { get; set; }
		[Display(Name = "Required"), Required]
		public bool isRequired { get; set; }

		public IList<Studio> Studios { get; set; }
		public Studio DefaultStudioFilter { get; set; }
		public IList<ProductLine> ProductLines { get; set; }
		public ProductLine DefaultProductLine { get; set; }
		public IList<Platform> Platforms { get; set; }
		public Platform DefaultPlatform { get; set; }
		public string Heading { get; internal set; }
	}

	public class Studio
	{
		public Studio()
		{
			this.StudioName = new SelectListItem();
			this.TestingTypeName = new SelectListItem();
		}
		public bool _destroy { get; set; }
		[Required]
		public int? StudioId { get; set; }

		[Display(Name = "Studio Name"), Required]
		public SelectListItem StudioName { get; set; }
		public int? TestingTypeId { get; set; }
		[Display(Name = "Testing Type"), Required]
		public SelectListItem TestingTypeName { get; set; }
	}

	public class ProductLine
	{
		public ProductLine()
		{
			this.ProductLineName = new SelectListItem();
			this.TestingTypeName = new SelectListItem();
		}
		public bool _destroy { get; set; }
		[Required]
		public int? ProductLineId { get; set; }

		[Display(Name = "Product Line Name"), Required(ErrorMessage = "{0} is required.")]
		public SelectListItem ProductLineName { get; set; }
		[Required(ErrorMessage = "{0} is required.")]
		public int? TestingTypeId { get; set; }
		[Display(Name = "Testing Type"), Required]
		public SelectListItem TestingTypeName { get; set; }
	}

	public class Platform
	{
		public Platform()
		{
			this.PlatformName = new SelectListItem();
		}
		public bool _destroy { get; set; }
		[Required]
		public int? PlatformId { get; set; }

		[Display(Name = "Platform Name"), Required(ErrorMessage = "{0} is required.")]
		public SelectListItem PlatformName { get; set; }
	}

}