﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using Submissions.Common;
using Submissions.Web.Models.Grids.GamingGuidelines;
using Submissions.Web.Models.Query;

namespace Submissions.Web.Areas.Lookups.Models
{
	public class GamingGuidelinesSportsBettingViewModel
	{
		public GamingGuidelinesSportsBettingViewModel()
		{
			this.Filters = new GamingGuidelinesSportsBettingSearch();
		}
		public GamingGuidelinesSportsBettingSearch Filters { get; set; }
		public GamingGuidelinesSportsBettingGrid SportsBettingGrid { get; set; }
	}

	public class GamingGuidelinesSportsBettingEditViewModel
	{
		public GamingGuidelinesSportsBettingEditViewModel()
		{
			UserComments = new List<GamingGuidelineCommentSportsBettingViewModel>();
		}
		public int Id { get; set; }
		public Mode Mode { get; set; }
		public IList<SelectListItem> DropdownOptions { get; set; }
		public int JurisdictionId { get; set; }
		public string JurisdictionName { get; set; }
		[Display(Name = "Location")]
		public int? LocationId { get; set; }
		public string Location { get; set; }
		[Display(Name = "ITL Required")]
		public bool ITLRequired { get; set; }
		[Display(Name = "Audit Required")]
		public int AuditRequired { get; set; }
		[Display(Name = "Audit Note")]
		public string AuditNote { get; set; }
		[Display(Name = "Land Based")]
		public bool LandBased { get; set; }
		[Display(Name = "Online")]
		public bool Online { get; set; }
		[Display(Name = "Limit on Event Type Allowed")]
		public string LimitOnEventTypeAllowed { get; set; }
		[Display(Name = "Max Caps")]
		public string MaxCaps { get; set; }
		[Display(Name = "Change Management System")]
		public int ChangeManagementSystem { get; set; }
		[Display(Name = "Data Vaults")]
		public int DataVaults { get; set; }
		[Display(Name = "Data Retention Requirement")]
		public int DataRetentionRequirement { get; set; }
		[Display(Name = "Retention Period")]
		public string RetentionPeriod { get; set; }
		[Display(Name = "Tax Percentage")]
		public double? TaxPercentage { get; set; }
		[Display(Name = "Tax Notes")]
		public string TaxNotes { get; set; }
		[Display(Name = "Server Required to be Located in Jur")]
		public int ServerLocationRequirement { get; set; }
		[Display(Name = "Liability Amount Required")]
		public int LiabilityAmountRequired { get; set; }
		[Display(Name = "Liability Note")]
		public string LiabilityNote { get; set; }
		[Display(Name = "Anonymous Play Allowed")]
		public int AnonymousPlayAllowed { get; set; }
		[Display(Name = "General Notes")]
		public string GeneralNotes { get; set; }
		[Display(Name = "Comments")]
		public string Comments { get; set; }
		public int CommentCount { get; set; }
		public IList<GamingGuidelineCommentSportsBettingViewModel> UserComments { get; set; }
		public GamingGuidelineCommentSportsBettingViewModel DefaultUserComment { get; set; }
	}

	public class GamingGuidelineCommentSportsBettingViewModel
	{
		public int Id { get; set; }
		public int UserId { get; set; }
		public string UserName { get; set; }
		public string CommentString { get; set; }
		public DateTime AddedTime { get; set; }
		public string SimpleDateTime { get { return this.AddedTime.ToString("MM/dd/yyyy"); } }
		public bool IsEditable { get; set; }
		public bool _destroy { get; set; }
		public bool _dirty { get; set; }
	}
}