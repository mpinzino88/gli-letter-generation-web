﻿using System.ComponentModel.DataAnnotations;

namespace Submissions.Web.Areas.Lookups.Models
{
	public class ClassificationsViewModel : Lookup
	{
		public int? Id { get; set; }
		[Required, StringLength(5)]

		[Display(Name = "Manufacturer")]
		public string ManufacturerId { get; set; }
		public string ManufacturerName { get; set; }
		[Display(Name = "Product Line"), Required]
		public int ProductLine { get; set; }
		[Required, StringLength(500)]
		public string Name { get; set; }
		[Display(Name = "Group Id"), Required]
		public int GroupId { get; set; }
		[Display(Name = "Classification Type"), Required]
		public int ClassificationTypeId { get; set; }
		[Display(Name = "Classification Type")]
		public string ClassificationTypeName { get; set; }
	}
}