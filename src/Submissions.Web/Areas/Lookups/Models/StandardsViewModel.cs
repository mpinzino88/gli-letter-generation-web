﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Submissions.Web.Areas.Lookups.Models
{
	public class StandardsIndexViewModel
	{
		public IList<StandardViewModel> Standards { get; set; }
	}

	public class StandardsEditViewModel : Lookup
	{
		public int? Id { get; set; }
		[Required, Display(Name = "Jurisdiction")]
		public string JurisdictionId { get; set; }
		public string Jurisdiction { get; set; }
		[Required, StringLength(50), Remote("ValidateStandard", "Standards", AdditionalFields = "Id, Mode, JurisdictionId", ErrorMessage = "{0} already exists.")]
		public string Standard { get; set; }
		[Display(Name = "Add By Default")]
		public bool AddByDefault { get; set; }
	}

	public class StandardViewModel
	{
		public int Id { get; set; }
		[Display(Name = "Jurisdiction ID")]
		public string JurisdictionId { get; set; }
		public string Jurisdiction { get; set; }
		public string Standard { get; set; }
		public bool Active { get; set; }
	}
}