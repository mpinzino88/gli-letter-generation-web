﻿using Submissions.Web.Areas.Tools.Models;
using Submissions.Web.Models.Grids.Lookups;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Submissions.Web.Areas.Lookups.Models
{
	public class TechnicalStandardViewModel : Lookup
	{
		public TechnicalStandardViewModel()
		{
			TechnicalStandardsGrid = new TechnicalStandardsGrid();
			Translations = new List<TechnicalStandardTranslation>();
			Functions = new List<Function>();
			FunctionalRoles = new List<FunctionalRole>();
			Jurisdictions = new List<JurisdictionInfo>();
			EvolutionDocuments = new List<EvolutionDocument>();
		}
		public TechnicalStandardsGrid TechnicalStandardsGrid { get; set; }

		public int Id { get; set; }
		public int RegulatoryBodyId { get; set; }
		[Required, StringLength(1000)]
		public string Description { get; set; }
		public int OriginalId { get; set; }
		public string TechLogic { get; set; }
		public int OrderId { get; set; }
		public bool Mandatory { get; set; }
		public string Result { get; set; }
		public IList<TechnicalStandardTranslation> Translations { get; set; }
		public IList<Function> Functions { get; set; }
		public bool AllFunctions { get; set; }
		public IList<FunctionalRole> FunctionalRoles { get; set; }
		public bool AllFunctionalRoles { get; set; }
		public IList<JurisdictionInfo> Jurisdictions { get; set; }
		public bool AllJurisdictions { get; set; }
		public IList<EvolutionDocument> EvolutionDocuments { get; set; }
	}

	public class Function
	{
		public int Id { get; set; }
		public string Code { get; set; }
	}
	public class FunctionalRole
	{
		public int Id { get; set; }
		public string Name { get; set; }
		public string Description { get; set; }
	}

	public class TechnicalStandardTranslation
	{
		public int Id { get; set; }
		public string Language { get; set; }
		public string Description { get; set; }
		public string Result { get; set; }
	}

	public class EvolutionDocument
	{
		public int Id { get; set; }
		public string Text { get; set; }
	}

}