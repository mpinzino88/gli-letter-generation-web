﻿using Submissions.Web.Models.Grids.Lookups;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Submissions.Web.Areas.Lookups.Models
{
	public class JurisdictionMappingIndexViewModel
	{
		public JurisdictionMappingIndexViewModel()
		{
			this.ManufacturerJurisdictionCodeMappingsGridModel = new ManufacturerJurisdictionCodeMappingsGrid();
		}
		public ManufacturerJurisdictionCodeMappingsGrid ManufacturerJurisdictionCodeMappingsGridModel { get; set; }
	}

	public class JurisdictionMappingEditViewModel : Lookup
	{
		public JurisdictionMappingEditViewModel()
		{
			AccountingDefaults = new List<AccountingDefaults>();
		}
		public int Id { get; set; }
		public string Manufacturer { get; set; }
		[Display(Name = "Manufacturer"), Required]
		public string ManufacturerCode { get; set; }
		[Display(Name = "Manufacturer Letter Code ID"), StringLength(8)]
		public string ExternalJurisdictionCode { get; set; }
		[Display(Name = "Manufacturer Jurisdiction ID"), Required, StringLength(4)]
		public string ExternalJurisdictionId { get; set; }
		[Display(Name = "Manufacturer Jurisdiction Name"), StringLength(50)]
		public string ExternalJurisdictionDescription { get; set; }
		[Display(Name = "GLI Jurisdiction"), Required]
		public string JurisdictionId { get; set; }
		public string Jurisdiction { get; set; }
		public string SportsJurisdictionId { get; set; }
		public string SportsJurisdiction { get; set; }
		[Remote("CheckFFMappingExists", "JurisdictionMapping", HttpMethod = "POST", ErrorMessage = "A FlatFile mapping for this manufacturer and GLI jurisdiction is already established.", AdditionalFields = "ManufacturerCode, JurisdictionId, Mode, Id")]
		public bool Flatfile { get; set; }
		public bool Billing { get; set; }
		public List<AccountingDefaults> AccountingDefaults { get; set; }
		[Display(Name = "Seperate Project Batch Number"), Required]
		public int SeperateProjectBatchNumber { get; set; }
	}

	public class AccountingDefaults
	{
		public int Id { get; set; }
		public int MappingId { get; set; }
		public bool ForTransfer { get; set; }
		[Display(Name = "Company")]
		public int? CompanyId { get; set; }
		public string Company { get; set; }
		[Display(Name = "Currency")]
		public int? CurrencyId { get; set; }
		public string Currency { get; set; }
		[Display(Name = "Contract Type")]
		public int? ContractTypeId { get; set; }
		public string ContractType { get; set; }
		[Display(Name = "Test Lab")]
		public int? LabId { get; set; }
		public string Lab { get; set; }

	}
}