﻿using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Submissions.Web.Areas.Lookups.Models
{
	public class VendorsViewModel : Lookup
	{
		public int? Id { get; set; }
		[Display(Name = "Name"), Required, StringLength(120), Remote("ValidateVendor", "Vendors", AdditionalFields = "Id, Mode", ErrorMessage = "{0} already exists.")]
		public string Description { get; set; }
	}
}