﻿using System.Collections.Generic;

namespace Submissions.Web.Areas.Lookups.Models
{
	public class LBDefaultingOptionsIndexViewModel
	{
		public List<LBDefaultingOptionsEditViewModel> Options { get; set; }
	}
}