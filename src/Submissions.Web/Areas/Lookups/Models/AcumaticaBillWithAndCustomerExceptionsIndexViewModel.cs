﻿using Submissions.Web.Models.Grids.Lookups;

namespace Submissions.Web.Areas.Lookups.Models
{
	public class AcumaticaBillWithAndCustomerExceptionsIndexViewModel
	{
		public AcumaticaBillWithAndCustomerExceptionsIndexViewModel()
		{
			AcumaticaBillWithAndCustomerExceptionsGrid = new AcumaticaBillWithAndCustomerExceptionsGrid();
		}

		public AcumaticaBillWithAndCustomerExceptionsGrid AcumaticaBillWithAndCustomerExceptionsGrid { get; set; }
	}

}