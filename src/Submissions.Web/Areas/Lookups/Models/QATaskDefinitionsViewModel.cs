﻿using Submissions.Common;
using System;
using System.Collections.Generic;
using System.Web.Mvc;


namespace Submissions.Web.Areas.Lookups.Models
{
    public class QATaskDefinitionsIndexViewModel
    {
        public List<QATaskDefinitionsViewModel> QATaskDefinitions { get; set; }
    }
    public class QATaskDefinitionsViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool IsTaskForOriginalTesting { get; set; }
        public bool? IsTaskForClosedJurisdictionsTesting { get; set; }
    }

    public class QATaskDefinitionsEditViewModel
    {
        public QATaskDefinitionsEditViewModel()
        {
            TaskDependedOnIds = new List<String>();
        }
    
        public int Id { get; set; }
        public string Name { get; set; }
        public bool IsTaskForOriginalTesting { get; set; }
        public bool? IsTaskForClosedJurisdictionsTesting { get; set; }
        public string TaskDependedOn { get; set; }
        public List<String> TaskDependedOnIds { get; set; }
        public List<SelectListItem> TaskDependedOnName { get; set; }
        public string ShortName { get; set; }
        public bool IsReviewTask { get; set; }
        public string Mode { get; set; }
        public int? FinalizationPriority { get; set; }
        public bool IsFinalizationTask { get; set; }
        public List<SelectListItem> FinalizationPriorityOptions { get; set; }
      
    }
}