﻿using Submissions.Web.Models.Grids.Lookups;
using System.Collections.Generic;

namespace Submissions.Web.Areas.Lookups.Models
{
	public class TaskTemplateIndexViewModel
	{
		public TaskTemplateIndexViewModel()
		{
			this.TaskTemplateGrid = new TaskTemplateGrid();
		}

		public TaskTemplateGrid TaskTemplateGrid { get; set; }
	}
	public class TaskTemplatesViewModel : Lookup
	{
		public TaskTemplatesViewModel()
		{
			this.Tasks = new List<Task>();
		}
		public int? Id { get; set; }
		public string Code { get; set; }
		public bool CanSave { get; set; }
		public string Description { get; set; }
		public string UserName { get; set; }
		public IList<Task> Tasks { get; set; }
	}

	public class Task
	{
		public int Id { get; set; }
		public bool IsETATask { get; set; }
		public bool IsETAEligible { get; set; }
		public string Description { get; set; }
		public int? Position { get; set; }
		public decimal? EstimateHours { get; set; }
		public int TaskDefinitionId { get; set; }
		public string TaskDefinition { get; set; }
	}
}