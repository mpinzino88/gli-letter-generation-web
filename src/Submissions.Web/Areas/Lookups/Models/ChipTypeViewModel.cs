﻿using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Submissions.Web.Areas.Lookups.Models
{
	public class ChipTypeViewModel : Lookup
	{
		public int? Id { get; set; }
		[Required, StringLength(25), Remote("ValidateCode", "ChipTypes", AdditionalFields = "Id, Mode", ErrorMessage = "{0} already exists.")]
		public string Code { get; set; }
	}
}