﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Submissions.Web.Areas.Lookups.Models
{
	public class TaskEvolutionGroupsViewModel : Lookup
	{
		public TaskEvolutionGroupsViewModel()
		{
			TaskDefinitions = new List<SelectListItem>();
			TaskDefinitionIds = new List<int>();
		}

		public int? Id { get; set; }
		public string EvolutionTC { get; set; }
		[Display(Name = "Evolution Test Case"), Required]
		public Guid EvolutionTCId { get; set; }
		[Display(Name = "Task Definition"), Required]
		public string TaskDefinition { get; set; }
		public int TaskDefinitionId { get; set; }
		public IList<SelectListItem> TaskDefinitions { get; set; }
		public List<int> TaskDefinitionIds { get; set; }

	}
}