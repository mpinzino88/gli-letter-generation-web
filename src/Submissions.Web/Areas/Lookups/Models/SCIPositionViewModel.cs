﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Submissions.Web.Areas.Lookups.Models
{
	public class SCIPositionIndexViewModel
	{
		public int Id { get; set; }
		public string Position { get; set; }
		[Display(Name = "Chip Type")]
		public string ChipType { get; set; }
		public IList<string> SignatureScopes { get; set; }
		[Display(Name = "Signature Scopes")]
		public string CommaSeperatedSignatureScopes { get; set; }
		public bool Active { get; set; }
	}

	public class SCIPositionEditViewModel : Lookup
	{
		public SCIPositionEditViewModel()
		{
			SignatureScopeIds = new List<int>();
			SignatureScopes = new List<SelectListItem>();
		}
		public int Id { get; set; }
		[Required, StringLength(100), Remote("ValidatePosition", "SCIPositions", AdditionalFields = "Id, Mode", ErrorMessage = "{0} already exists.")]
		public string Position { get; set; }
		public string ChipType { get; set; }
		[Display(Name = "Chip Type"), Required]
		public int? ChipTypeId { get; set; }
		[Display(Name = "Signature Scopes"), Required]
		public IList<int> SignatureScopeIds { get; set; }
		public IList<SelectListItem> SignatureScopes { get; set; }
	}
}