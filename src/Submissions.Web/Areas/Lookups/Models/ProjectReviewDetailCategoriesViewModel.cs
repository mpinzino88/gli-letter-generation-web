﻿using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Submissions.Web.Areas.Lookups.Models
{
	public class ProjectReviewDetailCategoriesViewModel : Lookup
	{
		public int? Id { get; set; }
		[Display(Name = "Department")]
		public int? DepartmentId { get; set; }
		public string Department { get; set; }
		public string Code { get; set; }
		[Display(Name = "Name"), Required, StringLength(300), Remote("ValidateReviewDetail", "ProjectReviewDetailCategories", AdditionalFields = "Id, Mode", ErrorMessage = "{0} already exists.")]
		public string Description { get; set; }
	}

}