﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Submissions.Web.Areas.Lookups.Models
{
    public class MultipleNameModel
    {
        public int JurId { get; set; }
        public List<string> GuidelineNames { get; set; }
        public bool ShowList { get; set; }
        
    }
}