﻿using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Submissions.Web.Areas.Lookups.Models
{
	public class SignatureScopesViewModel : Lookup
	{
		public int Id { get; set; }
		[Display(Name = "Signature Scope"), Required, StringLength(30), Remote("ValidateCode", "SignatureScopes", AdditionalFields = "Id, Mode", ErrorMessage = "{0} already exists.")]
		public string Scope { get; set; }
	}
}