﻿namespace Submissions.Web.Areas.Lookups.Models
{
	public class CabinetViewModel : Lookup
	{
		public int Id { get; set; }
		public string Cabinet { get; set; }
		public int? ManufacturerGroupId { get; set; }
		public string ManufacturerGroup { get; set; }
	}
}