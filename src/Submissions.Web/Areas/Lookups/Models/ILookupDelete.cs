﻿using System.Web.Mvc;

namespace Submissions.Web.Areas.Lookups.Models
{
	public interface ILookupDelete<T>
	{
		JsonResult CheckDelete(T id);
		ActionResult Delete(T id);
	}
}