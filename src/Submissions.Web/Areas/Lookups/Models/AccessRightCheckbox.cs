﻿
namespace Submissions.Web.Areas.Lookups.Models
{
	public class AccessRightCheckbox
	{
		public int PermissionId { get; set; }
		public string Code { get; set; }
		public string Description { get; set; }
		public bool Read { get; set; }
		public bool Add { get; set; }
		public bool Edit { get; set; }
		public bool Delete { get; set; }
	}
}