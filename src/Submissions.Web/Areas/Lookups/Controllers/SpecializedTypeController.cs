﻿extern alias ZEFCore;
using GLI.EFCore.Submission;
using Submissions.Common;
using Submissions.Web.Areas.Lookups.Models;
using System.Linq;
using System.Web.Mvc;
using ZEFCore::Z.EntityFramework.Plus;

namespace Submissions.Web.Areas.Lookups.Controllers
{
	public class SpecializedTypeController : Controller
	{

		private readonly SubmissionContext _dbSubmission;

		public SpecializedTypeController(SubmissionContext submissionContext)
		{
			_dbSubmission = submissionContext;
		}

		// GET: Lookups/SpecializedType
		public ActionResult Index()
		{
			var vm = new SpecializedTypeIndexViewModel();
			var types = _dbSubmission.tbl_lst_SpecializedTypes.Select(x => new SpecializedType()
			{
				Id = x.Id,
				Name = x.Name,
				Description = x.Description,
				Active = x.Active
			}).ToList();
			vm.SpecializedTypes = types;
			return View(vm);
		}

		public ActionResult Edit(int? id)
		{
			var vm = new SpecializedTypeEditViewModel();
			vm.Mode = Mode.Add.ToString();
			vm.Active = true;

			if (id != null && id != 0)
			{
				var existingOptions = _dbSubmission.tbl_lst_SpecializedTypes.Where(x => x.Id == id).SingleOrDefault();
				vm.Id = existingOptions.Id;
				vm.Name = existingOptions.Name;
				vm.Description = existingOptions.Description;
				vm.Active = existingOptions.Active;

				vm.Mode = Mode.Edit.ToString();
			}
			return View(vm);
		}

		[HttpPost]
		public ActionResult Edit(SpecializedTypeEditViewModel vm)
		{
			tbl_lst_SpecializedTypes type;
			if (vm.Id == 0)
			{
				type = new tbl_lst_SpecializedTypes();
			}
			else
			{
				type = _dbSubmission.tbl_lst_SpecializedTypes.Find(vm.Id);
			}

			type.Name = vm.Name;
			type.Description = vm.Description;
			type.Active = vm.Active;

			if (type.Id == 0)
			{
				_dbSubmission.tbl_lst_SpecializedTypes.Add(type);
			}
			_dbSubmission.SaveChanges();
			return RedirectToAction("Index");
		}

		[HttpPost]
		public ActionResult CheckIfExists(SpecializedTypeEditViewModel vm)
		{
			var exists = _dbSubmission.tbl_lst_SpecializedTypes.Any(
				x => x.Id != vm.Id && x.Name == vm.Name);

			if (exists)
			{
				return Content("NOOK");
			}
			else
			{
				return Content("OK");
			}
		}

		public ContentResult Delete(int id)
		{
			var typeName = _dbSubmission.tbl_lst_SpecializedTypes.Where(x => x.Id == id).Select(x => x.Name).SingleOrDefault();
			var anyBillingExists = _dbSubmission.tbl_LetterBook.Where(x => x.SpecializedType == typeName).Any();
			if (!anyBillingExists)
			{
				_dbSubmission.tbl_lst_SpecializedTypes.Where(opt => opt.Id == id).Delete();
				_dbSubmission.SaveChanges();

				return Content("OK");
			}
			else
			{
				return Content("NOOK");
			}
		}
	}
}