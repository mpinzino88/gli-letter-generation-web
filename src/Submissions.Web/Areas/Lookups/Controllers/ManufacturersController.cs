﻿extern alias ZEFCore;
using AutoMapper;
using EF.GLIExtranet;
using GLI.EFCore.Submission;
using Microsoft.EntityFrameworkCore;
using Submissions.Core.Interfaces;
using Submissions.Web.Areas.Lookups.Models;
using Submissions.Web.Utility.Email;
using Submissions.Web.Utility.Email.Templates.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Web.Mvc;
using ZEFCore::Z.EntityFramework.Plus;

namespace Submissions.Web.Areas.Lookups.Controllers
{
	using Submissions.Common;
	using Submissions.Common.Email;

	[AuthorizeUser(Permission = Permission.Admin, HasAccessRight = AccessRight.Edit)]
	public class ManufacturersController : Controller, ILookupDelete<Guid>
	{
		private readonly SubmissionContext _dbSubmission;
		private readonly IGLIExtranetContext _dbExtranet;
		private readonly IUserContext _userContext;
		private readonly IEmailService _emailService;
		private readonly ISharepointService _sharePointService;
		private readonly IConfigContext _configContext;

		public ManufacturersController
		(
			SubmissionContext dbSubmission,
			IGLIExtranetContext dbExtranet,
			IUserContext userContext,
			IEmailService emailService,
			ISharepointService sharePointService,
			IConfigContext configContext
		)
		{
			_dbSubmission = dbSubmission;
			_dbExtranet = dbExtranet;
			_userContext = userContext;
			_emailService = emailService;
			_sharePointService = sharePointService;
			_configContext = configContext;
		}

		#region Main
		public ActionResult Index()
		{
			var vm = new ManufacturersIndexViewModel();
			return View(vm);
		}

		public ActionResult Edit(Guid? id, Mode mode)
		{
			var vm = new ManufacturersEditViewModel();

			if (mode == Mode.Add)
			{
				var accountStatus = _dbSubmission.tbl_lst_ManufAcctStatus
					.Where(x => x.Status == ManufacturerAccountStatus.Active.ToString())
					.Single();

				vm.AccountStatusId = accountStatus.Id;
				vm.AccountStatus = accountStatus.Status;
			}
			else
			{
				var manufacturer = _dbSubmission.tbl_lst_fileManu.Where(x => x.Id == id).Single();

				vm = Mapper.Map<ManufacturersEditViewModel>(manufacturer);

				if (!string.IsNullOrEmpty(manufacturer.Country))
				{
					var country = _dbSubmission.tbl_lst_Country.Where(x => x.Code == manufacturer.Country).Single();
					vm.CountryId = country.Id;
					vm.Country = country.Code + " - " + country.Description;
				}

				var liaisonUsers = LoadLiaisons(manufacturer.Code);
				vm.LiaisonUserIds = liaisonUsers.Select(x => x.Value).ToArray();
				vm.LiaisonUsers = liaisonUsers;

				var testingTypes = LoadTestingTypes(manufacturer.Code);
				vm.TestingTypeIds = testingTypes.Select(x => x.Value).ToArray();
				vm.TestingTypes = testingTypes;

				var manufacturerGroups = LoadManufacturerGroups(manufacturer.Code);
				vm.ManufacturerGroupIds = manufacturerGroups.Select(x => x.Value).ToArray();
				vm.ManufacturerGroups = manufacturerGroups;

				var jurisdictions = LoadJurisdictions(manufacturer.Jurisdictions);
				vm.JurisdictionIds = jurisdictions.Count > 0 ? jurisdictions.Select(x => x.Value).ToArray() : null;
				vm.Jurisdictions = jurisdictions;

				var sendForApprovalJurisdictions = LoadSendForApprovalJurisdictions(manufacturer.Code);
				vm.SendForApprovalJurisdictionIds = sendForApprovalJurisdictions.Select(x => x.Value).ToArray();
				vm.SendForApprovalJurisdictions = sendForApprovalJurisdictions;

				var qaOffice = _dbSubmission.QAOfficeDefaults.Where(x => x.ManufacturerId == manufacturer.Code).SingleOrDefault();
				vm.QAOfficeId = qaOffice?.QAOfficeId;
				vm.QAOfficeDefault = qaOffice?.QAOffice.Name;
			}

			vm.Mode = mode;

			return View(vm);
		}

		[HttpPost]
		public ActionResult Edit(Guid? id, ManufacturersEditViewModel vm)
		{
			// update submissions
			var manufacturer = new tbl_lst_fileManu();
			var originalDescription = "";

			if (vm.Mode == Mode.Edit)
			{
				manufacturer = _dbSubmission.tbl_lst_fileManu.Where(x => x.Id == id).Single();
				originalDescription = manufacturer.Description;
			}

			manufacturer = ApplyChanges(manufacturer, vm);

			if (vm.Mode == Mode.Add)
			{
				manufacturer.AddDate = DateTime.Now;
				manufacturer.AddUserId = _userContext.User.Id;
				manufacturer.PendingApproval = true;
				_dbSubmission.tbl_lst_fileManu.Add(manufacturer);

				if (vm.ForDynamicsUseOnly)
				{
					if (manufacturer.ProductSubmitterListId != null && !String.IsNullOrWhiteSpace(manufacturer.Code))
					{
						_sharePointService.UpdateProductSubmitterListItem((Int32)manufacturer.ProductSubmitterListId, manufacturer.Code);
					}
				}
			}
			manufacturer.IsDraftSupplier = vm.IsDraftSupplier;

			_dbSubmission.UserId = _userContext.User.Id;
			_dbSubmission.SaveChanges();

			UpdateDefaultQAOffice(vm, manufacturer);
			UpdateLiaisons(vm);
			UpdateTestingTypes(vm);
			UpdateSendForApprovalJurisdictions(vm);

			// update access
			var accessManufacturer = _dbExtranet.tblManufacturers.Where(x => x.Code == vm.Code).SingleOrDefault() ?? new tblManufacturers();
			accessManufacturer.Code = vm.Code;
			accessManufacturer.Name = vm.Description;

			if (vm.Mode == Mode.Add || accessManufacturer.Id == 0)
			{
				_dbExtranet.tblManufacturers.Add(accessManufacturer);
			}

			_dbExtranet.SaveChanges();

			if (!string.IsNullOrEmpty(originalDescription) && originalDescription != vm.Description)
			{
				UpdateHistoricalData(originalDescription, vm.Description);
			}

			CacheManager.Clear(LookupAjax.Manufacturers.ToString());

			if (_userContext.User.Environment == Environment.Production)
			{
				CacheManager.AccessUpdateCache(LookupAjaxAccess.Manufacturers);
			}

			if (vm.SendEmail)
			{
				EmailAccounting((Guid)manufacturer.Id, vm);
			}

			return RedirectToAction("Index");
		}

		private void UpdateDefaultQAOffice(ManufacturersEditViewModel vm, tbl_lst_fileManu manufacturer)
		{
			var qaOffice = _dbSubmission.QAOfficeDefaults.Where(x => x.ManufacturerId == manufacturer.Code).SingleOrDefault();
			if (qaOffice == null && vm.QAOfficeId != null)
			{
				qaOffice = new QAOfficeDefaults
				{
					ManufacturerId = manufacturer.Code,
					QAOfficeId = (int)vm.QAOfficeId
				};
				_dbSubmission.QAOfficeDefaults.Add(qaOffice);
			}
			else
			{
				if (vm.QAOfficeId != null)
				{
					qaOffice.QAOfficeId = (int)vm.QAOfficeId;
				}
				else if (qaOffice != null)
				{
					_dbSubmission.QAOfficeDefaults.Remove(qaOffice);
				}
			}
			_dbSubmission.SaveChanges();
		}

		public JsonResult CheckDelete(Guid id)
		{
			var manufacturerCode = _dbSubmission.tbl_lst_fileManu.Where(x => x.Id == id).Select(x => x.Code).Single();
			var isInUse = _dbSubmission.Submissions.Where(x => x.ManufacturerCode == manufacturerCode).Any() || _dbExtranet.tblUserManufacturers.Where(x => x.Manufacturer.Code == manufacturerCode).Any();

			return Json(isInUse);
		}

		public ActionResult Delete(Guid id)
		{
			var dbManufacturer = _dbSubmission.tbl_lst_fileManu
				.Where(x => x.Id == id)
				.Select(x => new { x.Code })
				.Single();

			// TODO: ZEFCore Delete
			_dbExtranet.tblManufacturers.Remove(_dbExtranet.tblManufacturers.Where(x => x.Code == dbManufacturer.Code).SingleOrDefault());
			_dbExtranet.SaveChanges();

			_dbSubmission.tbl_lst_fileManuTestingTypes.Where(x => x.ManufacturerCode == dbManufacturer.Code).Delete();
			_dbSubmission.tbl_lst_fileManuManufacturerGroups.Where(x => x.ManufacturerCode == dbManufacturer.Code).Delete();
			_dbSubmission.tbl_SendForApprovalJurisdictions.Where(x => x.ManufacturerCode == dbManufacturer.Code).Delete();

			var manufacturer = new tbl_lst_fileManu { Code = dbManufacturer.Code };
			_dbSubmission.UserId = _userContext.User.Id;
			_dbSubmission.Entry(manufacturer).State = EntityState.Deleted;
			_dbSubmission.SaveChanges();

			CacheManager.Clear(LookupAjax.Manufacturers.ToString());

			if (_userContext.User.Environment == Environment.Production)
			{
				CacheManager.AccessUpdateCache(LookupAjaxAccess.Manufacturers);
			}

			return RedirectToAction("Index");
		}

		public JsonResult GetRecordCountForUpdate(string originalManufacturerDescription)
		{
			return Json(_dbSubmission.Submissions.Where(x => x.ManufacturerDescription == originalManufacturerDescription).Count());
		}

		public JsonResult ValidateCode(Guid? id, string code, Mode mode)
		{
			var validates = true;

			if (!string.IsNullOrEmpty(code) && _dbSubmission.tbl_lst_fileManu.Any(x => x.Id != id && x.Code == code))
			{
				validates = false;
			}

			return Json(validates, JsonRequestBehavior.AllowGet);
		}

		public JsonResult ValidateDescription(Guid? id, string description, Mode mode)
		{
			var validates = true;

			if (!string.IsNullOrEmpty(description) && _dbSubmission.tbl_lst_fileManu.Any(x => x.Id != id && x.Description == description))
			{
				validates = false;
			}

			return Json(validates, JsonRequestBehavior.AllowGet);
		}

		public ActionResult EmailManufacturerActivation(Guid id)
		{
			var manufacturer = _dbSubmission.tbl_lst_fileManu.Where(x => x.Id == id).SingleOrDefault();
			if (manufacturer.PendingApproval)
			{
				manufacturer.PendingApproval = false;
				manufacturer.Active = true;
				_dbSubmission.UserId = _userContext.User.Id;
				_dbSubmission.SaveChanges();

				if (manufacturer.ProductSubmitterListId != null && !String.IsNullOrWhiteSpace(manufacturer.Code))
				{
					_sharePointService.UpdateProductSubmitterListItem((Int32)manufacturer.ProductSubmitterListId, manufacturer.Code);
				}

				CacheManager.Clear(LookupAjax.Manufacturers.ToString());

				if (_userContext.User.Environment == Environment.Production)
				{
					CacheManager.AccessUpdateCache(LookupAjaxAccess.Manufacturers);
				}

				EmailActivationSuccess(id);
				_sharePointService.CreateManufacturerTermSet(manufacturer.Code, manufacturer.Description);
			}

			return RedirectToAction("Index");
		}

		public ActionResult SetActive(Guid id, Mode mode)
		{
			var active = mode == Mode.Activate;

			var manufacturer = _dbSubmission.tbl_lst_fileManu
				.Where(x => x.Id == id)
				.Single();
			manufacturer.Active = active;
			_dbSubmission.SaveChanges();

			CacheManager.Clear(LookupAjax.Manufacturers.ToString());

			return RedirectToAction("Index");
		}
		#endregion

		#region Emails
		private void EmailActivationSuccess(Guid? id)
		{
			var manufacturer = _dbSubmission.tbl_lst_fileManu
				.Where(x => x.Id == id)
				.Select(x => new { x.AddUserId, x.Description, x.Code, ProductSubmitterListItemId = x.ProductSubmitterListId })
				.Single();

			var addUserEmail = _dbSubmission.trLogins.Where(x => x.Id == manufacturer.AddUserId).Select(x => x.Email).Single();

			var To = _dbSubmission.EmailSubscriptions
				.Where(x => x.EmailType.Code == EmailType.DynamicsManufacturerActivation.ToString())
				.Select(x => x.User.Email)
				.ToList();

			To.Add(addUserEmail);

			var email = new ManufacturerSuccessfulActivationEmail
			{
				ManufacturerDescription = manufacturer.Description,
				ManufacturerCode = manufacturer.Code,
				ProductSubmitterListItemId = manufacturer.ProductSubmitterListItemId,
				ProductSubmittersListItemUri = new Uri(new Uri(_configContext.Config.SharepointBaseUrl), "sites/legal/Lists/prodsub3/DispForm.aspx?ID=" + manufacturer.ProductSubmitterListItemId)
			};

			var subject = "Activation successful for " + manufacturer.Description;

			var mailMsg = new MailMessage();
			mailMsg.From = new MailAddress(addUserEmail);
			mailMsg.To.Add(string.Join(",", To));

			mailMsg.Subject = subject;
			mailMsg.Body = _emailService.RenderTemplate(EmailType.ManufacturerSuccessfulActivation.ToString(), email);

			var gliGlobe = Server.MapPath("~/Img/GLIGlobe.gif");
			_emailService.EmbeddedImages = new List<LinkedResource> { new LinkedResource(gliGlobe) { ContentId = "GLIGlobe" } };
			_emailService.Send(mailMsg);
		}

		private void EmailAccounting(Guid id, ManufacturersEditViewModel vm)
		{
			var email = GetEmail(id, vm);

			var subject = "New Manufacturer Request for Dynamics : " + email.Code + "-" + email.Description + " Requested By: " + email.AddedBy;

			var To = _dbSubmission.EmailSubscriptions
				.Where(x => x.EmailType.Code == EmailType.AddManufacturerDynamics.ToString())
				.Select(x => x.User.Email)
				.ToList();

			var CC = _dbSubmission.EmailSubscriptions
				.Where(x => x.EmailType.Code == EmailType.AddManufacturerToDynamicsCC.ToString())
				.Select(x => x.User.Email)
				.ToList();

			var mailMsg = new MailMessage();

			mailMsg.To.Add(string.Join(",", To));
			mailMsg.CC.Add(string.Join(",", CC));
			mailMsg.From = new MailAddress(_userContext.User.Email);
			if (vm.Expedite)
			{
				mailMsg.Subject = "Expedite: " + subject;
			}
			else
			{
				mailMsg.Subject = subject;
			}
			mailMsg.Body = _emailService.RenderTemplate(EmailType.AddManufacturerDynamics.ToString(), email);

			if (vm.Expedite)
			{
				mailMsg.Priority = MailPriority.High;
			}

			var gliGlobe = Server.MapPath("~/Img/GLIGlobe.gif");
			_emailService.EmbeddedImages = new List<LinkedResource> { new LinkedResource(gliGlobe) { ContentId = "GLIGlobe" }, };
			_emailService.Send(mailMsg);
		}

		private AddManufacturerEmail GetEmail(Guid id, ManufacturersEditViewModel vm)
		{
			var companies = new List<string>();
			var jurisdictions = new List<string>();

			if (vm.BillingCompanyIds != null)
			{
				var billingCompanyId = vm.BillingCompanyIds.Select(y => Int32.Parse(y)).ToList();

				companies = _dbSubmission.tbl_lst_Company
								.Where(x => billingCompanyId.Contains(x.Id))
								.Select(x => x.Name).ToList();
			}

			if (vm.JurisdictionIds != null)
			{
				jurisdictions = vm.JurisdictionIds.ToList();
			}

			return new AddManufacturerEmail
			{
				Id = id,
				AddedBy = _userContext.User.FName + ' ' + _userContext.User.LName,
				Description = vm.Description,
				Code = vm.Code,
				ActivateManufacturerUri = new Uri(new Uri(ComUtil.GetBaseUrl()), "Lookups/Manufacturers/EmailManufacturerActivation/" + vm.Id.ToString()),
				ProductSubmittersListItemUri = new Uri(new Uri(_configContext.Config.SharepointBaseUrl), "sites/legal/Lists/prodsub3/DispForm.aspx?ID=" + vm.ProductSubmitterListId),
				ForDynamicsOnly = vm.ForDynamicsUseOnly,
				pendingJurisdictionRequests = jurisdictions,
				pendingCompaniesForBilling = companies,
				Expedite = vm.Expedite
			};
		}
		#endregion

		#region Helper Methods
		private tbl_lst_fileManu ApplyChanges(tbl_lst_fileManu manufacturer, ManufacturersEditViewModel vm)
		{
			var countryAbbreviation = "";
			if (vm.CountryId != null)
			{
				countryAbbreviation = _dbSubmission.tbl_lst_Country
					.Where(x => x.Id == vm.CountryId)
					.Select(x => x.Code)
					.Single();
			}

			manufacturer.Id = vm.Id;
			manufacturer.Code = vm.Code;
			manufacturer.Description = vm.Description;
			manufacturer.Active = vm.Active;
			manufacturer.AccountStatusId = vm.AccountStatusId;
			manufacturer.City = vm.City;
			manufacturer.State = vm.State;
			manufacturer.Country = countryAbbreviation;
			manufacturer.Territory = vm.Territory;
			manufacturer.Email = vm.Email;
			manufacturer.BinaryRequestGroup = vm.BinaryRequestGroup;
			manufacturer.EditDate = DateTime.Now;
			manufacturer.EditUserId = _userContext.User.Id;
			manufacturer.ForDynamicsUseOnly = vm.ForDynamicsUseOnly;
			manufacturer.UseDelayedCertifications = vm.UseDelayedCertifications;
			manufacturer.ProductSubmitterListId = vm.ProductSubmitterListId;
			manufacturer.Jurisdictions = vm.JurisdictionIds == null ? null : string.Join(",", vm.JurisdictionIds);

			return manufacturer;
		}

		private List<SelectListItem> LoadLiaisons(string manufacturerCode)
		{
			return _dbSubmission.tbl_Liaisons.Include(x => x.User)
				.Where(x => x.ManufacturerCode == manufacturerCode)
				.Select(x => new SelectListItem { Value = x.User.Id.ToString(), Text = x.User.FName + " " + x.User.LName })
				.OrderBy(x => x.Text)
				.ToList();
		}

		private List<SelectListItem> LoadTestingTypes(string manufacturerCode)
		{
			return _dbSubmission.tbl_lst_fileManuTestingTypes.Include(x => x.TestingType)
				.Where(x => x.ManufacturerCode == manufacturerCode)
				.Select(x => new SelectListItem { Value = x.TestingType.Id.ToString(), Text = x.TestingType.Code })
				.OrderBy(x => x.Text)
				.ToList();
		}

		private List<SelectListItem> LoadJurisdictions(string jurisdictionIds)
		{
			if (jurisdictionIds == null)
			{
				return new List<SelectListItem>();
			}

			var jurIds = jurisdictionIds.Split(',').ToList();

			return _dbSubmission.tbl_lst_fileJuris
				.Where(x => jurIds.Contains(x.Id))
				.Select(x => new SelectListItem { Value = x.Id, Text = x.Name })
				.OrderBy(x => x.Text)
				.ToList();
		}

		private List<SelectListItem> LoadManufacturerGroups(string manufacturerCode)
		{
			return _dbSubmission.tbl_lst_fileManuManufacturerGroups.Include(x => x.ManufacturerGroup.ManufacturerGroupCategory)
				.Where(x => x.ManufacturerCode == manufacturerCode)
				.Select(x => new SelectListItem { Value = x.ManufacturerGroupId.ToString(), Text = x.ManufacturerGroup.Description + ": " + x.ManufacturerGroup.ManufacturerGroupCategory.Code })
				.OrderBy(x => x.Text)
				.ToList();
		}

		private List<SelectListItem> LoadSendForApprovalJurisdictions(string manufacturerCode)
		{
			return _dbSubmission.tbl_SendForApprovalJurisdictions.Include(x => x.Jurisdiction)
				.Where(x => x.ManufacturerCode == manufacturerCode)
				.Select(x => new SelectListItem { Value = x.Jurisdiction.Id.ToString(), Text = x.Jurisdiction.Name })
				.OrderBy(x => x.Text)
				.ToList();
		}

		public void UpdateHistoricalData(string oldDescription, string newDescription)
		{
			_dbSubmission.Submissions.Where(x => x.ManufacturerDescription == oldDescription)
				.Update(x => new GLI.EFCore.Submission.Submission
				{
					ManufacturerDescription = newDescription,
					EditDate = DateTime.Now,
					EditUserId = _userContext.User.Id
				});
		}

		private void UpdateLiaisons(ManufacturersEditViewModel vm)
		{
			_dbSubmission.tbl_Liaisons.Where(x => x.ManufacturerCode == vm.Code).Delete();
			if (vm.LiaisonUserIds.Length > 0)
			{
				foreach (var userId in vm.LiaisonUserIds)
				{
					if (!string.IsNullOrEmpty(userId))
					{
						var liaison = new tbl_Liaison { ManufacturerCode = vm.Code, UserId = Convert.ToInt32(userId) };
						_dbSubmission.tbl_Liaisons.Add(liaison);
					}
				}

				_dbSubmission.SaveChanges();
			}
		}

		private void UpdateTestingTypes(ManufacturersEditViewModel vm)
		{
			_dbSubmission.tbl_lst_fileManuTestingTypes.Where(x => x.ManufacturerCode == vm.Code).Delete();
			if (vm.TestingTypeIds.Length > 0)
			{
				foreach (var testingTypeId in vm.TestingTypeIds)
				{
					if (!string.IsNullOrEmpty(testingTypeId))
					{
						var testingType = new tbl_lst_fileManuTestingType { ManufacturerCode = vm.Code, TestingTypeId = Convert.ToInt32(testingTypeId) };
						_dbSubmission.tbl_lst_fileManuTestingTypes.Add(testingType);
					}
				}

				_dbSubmission.SaveChanges();
			}
		}

		private void UpdateSendForApprovalJurisdictions(ManufacturersEditViewModel vm)
		{
			_dbSubmission.tbl_SendForApprovalJurisdictions.Where(x => x.ManufacturerCode == vm.Code).Delete();
			if (vm.SendForApprovalJurisdictionIds.Length > 0)
			{
				foreach (var jurisdictionId in vm.SendForApprovalJurisdictionIds)
				{
					if (!string.IsNullOrEmpty(jurisdictionId))
					{
						var sendForApprovalJurisdiction = new tbl_SendForApprovalJurisdiction { ManufacturerCode = vm.Code, JurisdictionId = jurisdictionId };
						_dbSubmission.tbl_SendForApprovalJurisdictions.Add(sendForApprovalJurisdiction);
					}
				}

				_dbSubmission.SaveChanges();
			}
		}
		#endregion
	}
}