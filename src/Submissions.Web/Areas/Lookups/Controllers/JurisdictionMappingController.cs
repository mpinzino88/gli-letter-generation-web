﻿extern alias ZEFCore;
using Aspose.Cells;
using AutoMapper;
using GLI.EFCore.Submission;
using Submissions.Common;
using Submissions.Core;
using Submissions.Web.Areas.Lookups.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Web.Mvc;
using ZEFCore::Z.EntityFramework.Plus;

namespace Submissions.Web.Areas.Lookups.Controllers
{
	using Newtonsoft.Json;
	using Submissions.Common.Email;
	using Submissions.Web.Utility.Email;
	using System.Data;

	[AuthorizeUser(Permission = Permission.JurisdictionMapping, HasAccessRight = AccessRight.Read)]
	public class JurisdictionMappingController : Controller
	{
		private readonly SubmissionContext _dbSubmission;
		private readonly IEmailService _emailService;
		private readonly INotificationService _notificationService;
		private readonly IUser _user;

		public JurisdictionMappingController
		(
			SubmissionContext dbSubmission,
			IEmailService emailService,
			INotificationService notificationService,
			IUser user
		)
		{
			_dbSubmission = dbSubmission;
			_emailService = emailService;
			_notificationService = notificationService;
			_user = user;
		}

		#region Public API
		public ActionResult Index() => View(new JurisdictionMappingIndexViewModel());
		public ActionResult Edit(int? id, Mode mode)
		{
			var vm = new JurisdictionMappingEditViewModel();
			if (mode == Mode.Edit)
			{
				vm = Mapper.Map<JurisdictionMappingEditViewModel>(_dbSubmission.tbl_lst_JurisdictionMappings.Find(id));
				foreach (var accoutingDefault in vm.AccountingDefaults)
				{
					accoutingDefault.Company = _dbSubmission.tbl_lst_Company.Find(accoutingDefault.CompanyId)?.Name;
					accoutingDefault.Currency = _dbSubmission.tbl_lst_Currency.Find(accoutingDefault.CurrencyId)?.Description;
					accoutingDefault.Lab = _dbSubmission.trLaboratories.Find(accoutingDefault.LabId)?.Location;
					accoutingDefault.ContractType = _dbSubmission.tbl_lst_ContractType.Find(accoutingDefault.ContractTypeId)?.Description;
				}
			}
			vm.Mode = mode;

			return View(vm);
		}
		[HttpPost]
		public ActionResult Edit(int? id, JurisdictionMappingEditViewModel vm, string AccountingDefaults)
		{
			vm.AccountingDefaults.Clear();
			vm.AccountingDefaults = JsonConvert.DeserializeObject<List<AccountingDefaults>>(AccountingDefaults);

			switch (vm.Mode)
			{
				case Mode.Add:
					var addJurisdictionMapping = new tbl_lst_JurisdictionMappings()
					{
						Active = vm.Active,
						Billing = vm.Billing,
						ExternalJurisdictionCode = vm.ExternalJurisdictionCode ?? "",
						ExternalJurisdictionDescription = vm.ExternalJurisdictionDescription ?? "",
						ExternalJurisdictionId = vm.ExternalJurisdictionId,
						FlatFile = vm.Flatfile,
						JurisdictionId = vm.JurisdictionId,
						SportsJurisdictionId = vm.SportsJurisdiction,
						ManufacturerCode = vm.ManufacturerCode,
						SeperateProjectBatchNumber = vm.SeperateProjectBatchNumber
					};

					_dbSubmission.tbl_lst_JurisdictionMappings.Add(addJurisdictionMapping);
					SaveChanges();

					var accountingDefaults =
						vm.AccountingDefaults
							.Select(x => new tbl_lst_AccountingDefaults
							{
								CertificationLabId = null,
								CompanyId = x.CompanyId,
								ContractTypeId = x.ContractTypeId,
								CurrencyId = x.CurrencyId,
								ForTransfer = x.ForTransfer,
								LabId = x.LabId,
								MappingId = addJurisdictionMapping.Id
							})
							.ToList();

					_dbSubmission.tbl_lst_AccountingDefaults.AddRange(accountingDefaults);
					SaveChanges();

					break;
				case Mode.Edit:
					var editJurisdictionMapping = _dbSubmission.tbl_lst_JurisdictionMappings.Find(id);
					editJurisdictionMapping.Active = vm.Active;
					editJurisdictionMapping.Billing = vm.Billing;
					editJurisdictionMapping.ExternalJurisdictionCode = vm.ExternalJurisdictionCode;
					editJurisdictionMapping.ExternalJurisdictionDescription = vm.ExternalJurisdictionDescription;
					editJurisdictionMapping.ExternalJurisdictionId = vm.ExternalJurisdictionId;
					editJurisdictionMapping.FlatFile = vm.Flatfile;
					editJurisdictionMapping.JurisdictionId = vm.JurisdictionId;
					editJurisdictionMapping.SportsJurisdictionId = vm.SportsJurisdictionId;
					editJurisdictionMapping.ManufacturerCode = vm.ManufacturerCode;
					editJurisdictionMapping.SeperateProjectBatchNumber = vm.SeperateProjectBatchNumber;

					var accountingDefaultsIds = editJurisdictionMapping.AccountingDefaults.Select(x => x.Id).ToList();
					_dbSubmission.tbl_lst_AccountingDefaults.Where(x => accountingDefaultsIds.Contains(x.Id)).Delete();

					var editAccountingDefaults =
						vm.AccountingDefaults
							.Select(x => new tbl_lst_AccountingDefaults
							{
								CertificationLabId = null,
								CompanyId = x.CompanyId,
								ContractTypeId = x.ContractTypeId,
								CurrencyId = x.CurrencyId,
								ForTransfer = x.ForTransfer,
								LabId = x.LabId,
								MappingId = editJurisdictionMapping.Id
							})
							.ToList();

					_dbSubmission.tbl_lst_AccountingDefaults.AddRange(editAccountingDefaults);
					SaveChanges();

					break;
				default:
					throw new Exception("Invalid Edit Mode");
			}

			HandleEmail(vm);

			return RedirectToAction("Index");
		}
		[HttpPost]
		public JsonResult CheckDelete(int id)
		{
			return Json(_dbSubmission.tbl_lst_JurisdictionMappings.Find(id).Active);
		}
		[HttpPost]
		public ActionResult Delete(int id)
		{
			_dbSubmission.tbl_lst_JurisdictionMappings.Where(x => x.Id == id).Delete();
			return RedirectToAction("Index");
		}
		[HttpPost]
		public ActionResult SetActive(int id, Mode mode)
		{
			bool active = (mode == Mode.Activate ? true : false);

			var jurisdictionMapping = _dbSubmission.tbl_lst_JurisdictionMappings.Find(id);
			jurisdictionMapping.Active = active;
			SaveChanges();

			return RedirectToAction("Index");
		}
		public JsonResult ValidateBilling(int? id, string manufacturerShortID, string jurisdictionId, string billing)
		{
			var validates = true;

			if (!string.IsNullOrEmpty(manufacturerShortID) && !string.IsNullOrEmpty(jurisdictionId) && !string.IsNullOrEmpty(billing) && (billing.ToLower() == "true")
				&& _dbSubmission.tbl_lst_JurisdictionMappings.Any(x => x.Id != id && x.Billing && x.ManufacturerCode == manufacturerShortID && x.JurisdictionId == jurisdictionId))
			{
				validates = false;
			}

			return Json(validates, JsonRequestBehavior.AllowGet);
		}

		//[HttpPost]
		public ActionResult Export()
		{
			MemoryStream spreadSheetStream = new MemoryStream();
			Workbook workbook = new Workbook();
			Worksheet worksheet = workbook.Worksheets[0];
			var jurisdictionMappings = _dbSubmission.tbl_lst_JurisdictionMappings
										.Where(x => x.Active)
										.Select(x => new
										{
											x.ManufacturerCode,
											x.JurisdictionId,
											x.Jurisdiction.Name,
											x.ExternalJurisdictionCode,
											x.ExternalJurisdictionId,
											x.ExternalJurisdictionDescription,
											x.Billing,
											x.FlatFile
										})
										.OrderBy(x => x.ManufacturerCode)
										.ThenBy(x => x.JurisdictionId).ToList();
			worksheet.Cells.ImportDataTable(jurisdictionMappings.ToDataTable(), true, "A1");
			workbook.Save(spreadSheetStream, SaveFormat.CSV);
			spreadSheetStream.Seek(0, SeekOrigin.Begin);

			return new FileStreamResult(spreadSheetStream, "application/csv") { FileDownloadName = "ManufacturerJurisdictionMappings.csv" };
		}

		public JsonResult CheckFFMappingExists(string ManufacturerCode, string JurisdictionId, bool Flatfile, Mode Mode, int Id)
		{
			return Json(FFMappingExists(ManufacturerCode, JurisdictionId, Flatfile, Mode, Id), JsonRequestBehavior.AllowGet);
		}

		public bool FFMappingExists(string ManufacturerCode, string JurisdictionId, bool Flatfile, Mode Mode, int Id)
		{
			bool validates = false;
			var mappings = GetFFMapping(JurisdictionId, ManufacturerCode);

			if (Mode == Mode.Edit && mappings.Count == 0)
			{
				validates = true;
			}
			else if (Mode == Mode.Edit && mappings.Count == 1)
			{
				if (mappings.First().Id == Id)
					validates = true;
			}
			else if (Mode == Mode.Add && mappings.Count == 0)
			{
				validates = true;
			}

			return validates;
		}

		public List<tbl_lst_JurisdictionMappings> GetFFMapping(string JurisdictionId, string ManufacturerCode)
		{
			return _dbSubmission.tbl_lst_JurisdictionMappings.Where(x => x.JurisdictionId == JurisdictionId && x.ManufacturerCode == ManufacturerCode && x.FlatFile).ToList();
		}

		#endregion

		#region Helpers
		private void SaveChanges()
		{
			_dbSubmission.UserId = _user.Id;
			_dbSubmission.SaveChanges();
			_dbSubmission.UserId = null;
		}
		private void HandleEmail(JurisdictionMappingEditViewModel vm)
		{
			var _email = Mapper.Map<EditJurisdictionMappingEmail>(vm);

			var gliGlobe = Server.MapPath("~/Img/GLIGlobe.gif");
			_emailService.EmbeddedImages = new List<LinkedResource> { new LinkedResource(gliGlobe) { ContentId = "GLIGlobe" }, };


			var mailMsg = new MailMessage("noreply@gaminglabs.com", string.Join(",", _notificationService.GetEmailToAddresses(EmailType.EditJurisdictionMapping)))
			{
				Subject = "Manufacturer Jurisdiction Code Mapping Update",
				Body = _emailService.RenderTemplate(EmailType.EditJurisdictionMapping.ToString(), _email)
			};

			_emailService.Send(mailMsg);
		}
		#endregion
	}
}