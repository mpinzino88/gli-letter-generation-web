﻿using AutoMapper;
using GLI.EFCore.Submission;
using Submissions.Common;
using Submissions.Web.Areas.Lookups.Models;
using System.Web.Mvc;

namespace Submissions.Web.Areas.Lookups.Controllers
{
	public class AcumaticaTenantsController : Controller
	{
		private readonly SubmissionContext _dbSubmission;
		private readonly IUserContext _userContext;

		public AcumaticaTenantsController(SubmissionContext dbSubmission, IUserContext userContext)
		{
			_dbSubmission = dbSubmission;
			_userContext = userContext;
		}

		public ActionResult Index() => View(new AcumaticaTenantsIndexViewModel());
		public ActionResult Edit(Mode mode, int id = 0)
		{
			var vm = new AcumaticaTenantsEditViewModel();
			if (Mode.Edit == mode)
			{
				vm = Mapper.Map<AcumaticaTenantsEditViewModel>(_dbSubmission.tbl_lst_AcumaticaTenants.Find(id));
			}
			vm.Mode = mode;
			return View(vm);
		}

		[HttpPost]
		public ActionResult Edit(AcumaticaTenantsEditViewModel vm)
		{
			var acumaticaTenant = Mode.Add == vm.Mode ? new tbl_lst_AcumaticaTenant() : _dbSubmission.tbl_lst_AcumaticaTenants.Find(vm.Id);
			acumaticaTenant.CompanyId = vm.GLICompanyID;
			acumaticaTenant.IsTestTenant = vm.IsTestTenant;
			acumaticaTenant.TenantName = vm.Name;
			if (vm.Mode == Mode.Add)
				_dbSubmission.tbl_lst_AcumaticaTenants.Add(acumaticaTenant);

			SaveChanges();
			return RedirectToAction("Index");
		}

		private void SaveChanges()
		{
			_dbSubmission.UserId = _userContext.User.Id;
			_dbSubmission.SaveChanges();
			_dbSubmission.UserId = null;
		}
	}
}