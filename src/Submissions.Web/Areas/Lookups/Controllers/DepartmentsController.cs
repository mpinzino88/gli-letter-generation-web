﻿extern alias ZEFCore;
using AutoMapper.QueryableExtensions;
using GLI.EFCore.Submission;
using Submissions.Common;
using Submissions.Web.Areas.Lookups.Models;
using System.Linq;
using System.Web.Mvc;
using ZEFCore::Z.EntityFramework.Plus;

namespace Submissions.Web.Areas.Lookups.Controllers
{
	[AuthorizeUser(Permission = Permission.Admin, HasAccessRight = AccessRight.Edit)]
	public class DepartmentsController : Controller, ILookupDelete<int>
	{
		private readonly SubmissionContext _dbSubmission;

		public DepartmentsController(SubmissionContext dbSubmission)
		{
			_dbSubmission = dbSubmission;
		}

		public ActionResult Index()
		{
			var departments = _dbSubmission.trDepts
				.OrderByDescending(x => x.Active)
				.ThenBy(x => x.Code)
				.ProjectTo<DepartmentViewModel>();
			return View(departments);
		}

		public ActionResult Edit(int? id, Mode mode)
		{
			var vm = new DepartmentViewModel();
			vm.Active = true;

			if (mode == Mode.Edit)
			{
				var department = _dbSubmission.trDepts.Find(id);
				vm.Id = department.Id;
				vm.Code = department.Code;
				vm.Description = department.Description;
			}

			vm.Mode = mode;

			return View(vm);
		}

		[HttpPost]
		public ActionResult Edit(int? id, DepartmentViewModel vm)
		{
			var department = new trDept();
			if (vm.Mode == Mode.Edit)
			{
				department = _dbSubmission.trDepts.Find(vm.Id);
			}

			department.Id = vm.Id ?? 0;
			department.Description = vm.Description;
			department.Code = vm.Code.ToUpper();
			department.Active = vm.Active;

			if (vm.Mode == Mode.Add)
			{
				_dbSubmission.trDepts.Add(department);
			}

			_dbSubmission.SaveChanges();

			CacheManager.Clear(LookupAjax.Departments.ToString());

			return RedirectToAction("Index");
		}

		[HttpPost]
		public JsonResult CheckDelete(int id)
		{
			var isInUse = _dbSubmission.trLogins.Any(x => x.DepartmentId == id);

			return Json(isInUse);
		}

		[HttpPost]
		public ActionResult Delete(int id)
		{
			_dbSubmission.trDepts.Where(x => x.Id == id).Delete();

			CacheManager.Clear(LookupAjax.Departments.ToString());

			return RedirectToAction("Index");
		}

		[HttpPost]
		public ActionResult SetActive(int id, Mode mode)
		{
			bool active = (mode == Mode.Deactivate ? false : true);

			var department = _dbSubmission.trDepts.Find(id);
			department.Active = active;

			_dbSubmission.SaveChanges();

			CacheManager.Clear(LookupAjax.Departments.ToString());

			return RedirectToAction("Index");
		}

		public JsonResult ValidateCode(int? id, string code, string description, Mode mode)
		{
			var validates = true;

			if (!string.IsNullOrEmpty(code) && _dbSubmission.trDepts.Any(x => x.Id != id && x.Code == code))
			{
				validates = false;
			}
			if (!string.IsNullOrEmpty(description) && _dbSubmission.trDepts.Any(x => x.Id != id && x.Description == description))
			{
				validates = false;
			}

			return Json(validates, JsonRequestBehavior.AllowGet);
		}
	}
}
