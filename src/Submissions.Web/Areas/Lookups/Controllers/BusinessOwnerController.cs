﻿extern alias ZEFCore;
using GLI.EFCore.Submission;
using Submissions.Common;
using Submissions.Web.Areas.Lookups.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ZEFCore::Z.EntityFramework.Plus;

namespace Submissions.Web.Areas.Lookups.Controllers
{
	[AuthorizeUser(Permission = Permission.UserFields, HasAccessRight = AccessRight.Edit)]
	public class BusinessOwnerController : Controller, ILookupDelete<int>
	{
		private readonly SubmissionContext _dbSubmission;
		private readonly IUserContext _userContext;
		public BusinessOwnerController(SubmissionContext dbSubmission, IUserContext userContext)
		{
			_dbSubmission = dbSubmission;
			_userContext = userContext;
		}

		public ActionResult Index()
		{
			var vm = new BusinessOwnerIndexViewModel();
			return View(vm);
		}

		public ActionResult Edit(int? id, Mode mode)
		{
			var vm = new BusinessOwnerEditViewModel();
			vm.Mode = Mode.Add;
			if (mode == Mode.Edit)
			{
				vm = _dbSubmission.tbl_lst_BusinessOwners.Where(x => x.Id == id).Select(x => new BusinessOwnerEditViewModel
				{
					Id = x.Id,
					Code = x.Code,
					Description = x.Description,
					Active = x.Active,
					Mode = Mode.Edit
				}).Single();
			}
			return View(vm);
		}

		[HttpPost]
		public ActionResult Edit(BusinessOwnerEditViewModel vm)
		{
			var businessOwner = new tbl_lst_BusinessOwner();

			if (vm.Mode == Mode.Edit)
			{
				businessOwner = _dbSubmission.tbl_lst_BusinessOwners.Find(vm.Id);
				businessOwner.EditedOn = DateTime.Now;
				businessOwner.EditedBy = _userContext.User.Id;
			}

			businessOwner.Code = vm.Code.Trim().ToUpper();
			businessOwner.Description = vm.Description;
			businessOwner.Active = vm.Active;

			if (vm.Mode == Mode.Add)
			{
				businessOwner.CreatedBy = _userContext.User.Id;
				businessOwner.CreatedOn = DateTime.Now;
				_dbSubmission.tbl_lst_BusinessOwners.Add(businessOwner);
			}

			_dbSubmission.SaveChanges();
			CacheManager.Clear(LookupAjax.BusinessOwners.ToString());
			return RedirectToAction("Index");
		}

		public JsonResult CheckDelete(int id)
		{
			var result = _dbSubmission.trLogins.Any(x => x.BusinessOwnerId == id);
			return Json(result);
		}

		public ActionResult Delete(int id)
		{
			_dbSubmission.tbl_lst_BusinessOwners.Where(x => x.Id == id).Delete();
			CacheManager.Clear(LookupAjax.BusinessOwners.ToString());

			return RedirectToAction("Index");
		}

		public ActionResult SetActive(BusinessOwnerEditViewModel vm)
		{
			var businessOwner = _dbSubmission.tbl_lst_BusinessOwners.Find(vm.Id);

			if (vm.Mode == Mode.Deactivate)
			{
				businessOwner.Active = false;
			}
			else if (vm.Mode == Mode.Activate)
			{
				businessOwner.Active = true;
			}

			_dbSubmission.SaveChanges();
			CacheManager.Clear(LookupAjax.BusinessOwners.ToString());

			return RedirectToAction("Index");
		}

		[HttpPost]
		public ContentResult AddUsers(UsersBusinessOwnerPayload payload)
		{
			try
			{
				_dbSubmission.ChangeTracker.AutoDetectChangesEnabled = false;
				foreach (var userId in payload.UserIds)
				{
					var updatedUser = new trLogin
					{
						Id = userId,
						BusinessOwnerId = payload.BusinessOwnerId
					};

					_dbSubmission.trLogins.Attach(updatedUser);
					_dbSubmission.Entry(updatedUser).Property(x => x.BusinessOwnerId).IsModified = true;
				}

				var updatedBusinessOwner = new tbl_lst_BusinessOwner
				{
					Id = payload.BusinessOwnerId,
					EditedBy = _userContext.User.Id,
					EditedOn = DateTime.Now
				};

				_dbSubmission.tbl_lst_BusinessOwners.Attach(updatedBusinessOwner);
				_dbSubmission.Entry(updatedBusinessOwner).Property(x => x.EditedBy).IsModified = true;
				_dbSubmission.Entry(updatedBusinessOwner).Property(x => x.EditedOn).IsModified = true;
				_dbSubmission.SaveChanges();
			}
			finally
			{
				_dbSubmission.ChangeTracker.AutoDetectChangesEnabled = true;
			}



			return Content(ComUtil.JsonEncodeCamelCase(""), "application/json");
		}



		[HttpPost]
		public ContentResult RemoveUsers(UsersBusinessOwnerPayload payload)
		{
			try
			{
				_dbSubmission.ChangeTracker.AutoDetectChangesEnabled = false;
				foreach (var userId in payload.UserIds)
				{
					var updatedUser = new trLogin
					{
						Id = userId,
						BusinessOwnerId = null
					};

					_dbSubmission.trLogins.Attach(updatedUser);
					_dbSubmission.Entry(updatedUser).Property(x => x.BusinessOwnerId).IsModified = true;
				}

				var updatedBusinessOwner = new tbl_lst_BusinessOwner
				{
					Id = payload.BusinessOwnerId,
					EditedBy = _userContext.User.Id,
					EditedOn = DateTime.Now
				};

				_dbSubmission.tbl_lst_BusinessOwners.Attach(updatedBusinessOwner);
				_dbSubmission.Entry(updatedBusinessOwner).Property(x => x.EditedBy).IsModified = true;
				_dbSubmission.Entry(updatedBusinessOwner).Property(x => x.EditedOn).IsModified = true;
				_dbSubmission.SaveChanges();
			}
			finally
			{
				_dbSubmission.ChangeTracker.AutoDetectChangesEnabled = true;
			}

			return Content(ComUtil.JsonEncodeCamelCase(""), "application/json");
		}
	}
}