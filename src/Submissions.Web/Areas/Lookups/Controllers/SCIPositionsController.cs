﻿extern alias ZEFCore;
using GLI.EFCore.Submission;
using Submissions.Common;
using Submissions.Web.Areas.Lookups.Models;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using ZEFCore::Z.EntityFramework.Plus;

namespace Submissions.Web.Areas.Lookups.Controllers
{
	[AuthorizeUser(Permission = Permission.Admin, HasAccessRight = AccessRight.Edit)]
	public class SCIPositionsController : Controller, ILookupDelete<int>
	{
		private readonly SubmissionContext _dbSubmission;

		public SCIPositionsController(SubmissionContext dbSubmission)
		{
			_dbSubmission = dbSubmission;
		}

		public ActionResult Index()
		{
			var sciPositions = _dbSubmission.SCItbl_lst_Positions
				.Select(x => new SCIPositionIndexViewModel
				{
					Id = x.Id,
					Position = x.Position,
					ChipType = x.ChipType.Code,
					SignatureScopes = x.PositionSignatureScopes.Select(y => y.SignatureScope.Scope).ToList(),
					Active = x.Active
				})
				.OrderByDescending(x => x.Active)
				.ThenBy(x => x.Position)
				.ToList();
			sciPositions.ForEach(x => x.CommaSeperatedSignatureScopes = string.Join(", ", x.SignatureScopes));
			return View(sciPositions);
		}

		public ActionResult Edit(int? id, Mode mode)
		{
			var vm = new SCIPositionEditViewModel();
			vm.Active = true;

			if (mode == Mode.Edit)
			{
				var SCIposition = _dbSubmission.SCItbl_lst_Positions.SingleOrDefault(x => x.Id == id);
				vm.Position = SCIposition.Position;
				vm.ChipTypeId = SCIposition.ChipTypeId;
				vm.ChipType = SCIposition.ChipType.Code;
				vm.SignatureScopeIds = SCIposition.PositionSignatureScopes.Select(x => x.SignatureScopeId).ToList();
				vm.SignatureScopes = SCIposition.PositionSignatureScopes
					.Select(x => new SelectListItem
					{
						Value = x.SignatureScope.Id.ToString(),
						Text = x.SignatureScope.Scope
					})
					.ToList();
				vm.Active = SCIposition.Active;
			}
			vm.Mode = mode;

			return View(vm);
		}

		[HttpPost]
		public ActionResult Edit(int? id, SCIPositionEditViewModel vm)
		{
			var sciPosition = new SCItbl_lst_Positions();

			if (vm.Mode == Mode.Edit)
			{
				sciPosition = _dbSubmission.SCItbl_lst_Positions.Find(id);
			}

			sciPosition.Position = vm.Position;
			sciPosition.ChipTypeId = (int)vm.ChipTypeId;
			sciPosition.Active = vm.Active;

			if (vm.Mode == Mode.Add)
			{
				_dbSubmission.SCItbl_lst_Positions.Add(sciPosition);
			}

			_dbSubmission.SaveChanges();

			UpdateSignatureScopes(sciPosition.Id, vm.SignatureScopeIds);
			return RedirectToAction("Index");
		}

		private void UpdateSignatureScopes(int positionId, IList<int> signatureScopeIds)
		{
			_dbSubmission.SCIPositionSignatureScope.Where(x => x.PositionId == positionId).Delete();

			foreach (var scopeId in signatureScopeIds)
			{
				_dbSubmission.SCIPositionSignatureScope.Add(new SCIPositionSignatureScope()
				{
					PositionId = positionId,
					SignatureScopeId = scopeId
				});
			}
			_dbSubmission.SaveChanges();
		}
		public JsonResult CheckDelete(int id)
		{
			var sciPosition = _dbSubmission.SCItbl_lst_Positions
				.Where(x => x.Id == id)
				.Select(x => x.Position)
				.First();
			var isInUse = _dbSubmission.ElectronicSubmissionComponents.Any(x => x.Position == sciPosition);

			return Json(isInUse);
		}

		public ActionResult Delete(int id)
		{
			_dbSubmission.SCIPositionSignatureScope.Where(x => x.PositionId == id).Delete();
			_dbSubmission.SCItbl_lst_Positions.Where(x => x.Id == id).Delete();

			return RedirectToAction("Index");
		}

		[HttpPost]
		public ActionResult SetActive(int id, Mode mode)
		{
			bool active = mode == Mode.Activate;

			var sciPosition = _dbSubmission.SCItbl_lst_Positions.Find(id);
			sciPosition.Active = active;
			_dbSubmission.SaveChanges();

			return RedirectToAction("Index");
		}

		public JsonResult ValidatePosition(int? id, string position, Mode mode)
		{
			var validates = true;
			if (!string.IsNullOrEmpty(position) && _dbSubmission.SCItbl_lst_Positions.Any(x => x.Id != id && x.Position == position))
			{
				validates = false;
			}
			return Json(validates, JsonRequestBehavior.AllowGet);
		}
	}
}