﻿extern alias ZEFCore;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using GLI.EFCore.Submission;
using Microsoft.EntityFrameworkCore;
using Submissions.Common;
using Submissions.Web.Areas.Lookups.Models;
using System.Linq;
using System.Web.Mvc;
using ZEFCore::Z.EntityFramework.Plus;

namespace Submissions.Web.Areas.Lookups.Controllers
{
	[AuthorizeUser(Permission = Permission.Admin, HasAccessRight = AccessRight.Edit)]
	public class ClassificationsController : Controller, ILookupDelete<int>
	{
		private readonly SubmissionContext _dbSubmission;

		public ClassificationsController(SubmissionContext dbSubmission)
		{
			_dbSubmission = dbSubmission;
		}

		public ActionResult Index()
		{
			var classifications = _dbSubmission.tbl_lst_Classifications
				.Include(x => x.ClassificationTypes)
				.AsNoTracking()
				.OrderByDescending(x => x.Active)
				.ThenBy(x => x.ManufacturerId)
				.ThenBy(x => x.Name)
				.ProjectTo<ClassificationsViewModel>();

			return View(classifications);
		}

		public ActionResult Edit(int? id, Mode mode)
		{
			var vm = new ClassificationsViewModel();
			vm.Active = true;

			if (mode == Mode.Edit)
			{
				var classifications = _dbSubmission.tbl_lst_Classifications.Find(id);
				vm = Mapper.Map<ClassificationsViewModel>(classifications);
			}
			if (mode == Mode.Add)
			{
				vm.ClassificationTypeName = "";
				vm.ManufacturerName = "";
			}

			vm.Mode = mode;

			return View(vm);
		}

		[HttpPost]
		public ActionResult Edit(int? id, ClassificationsViewModel vm)
		{
			var classifications = new tbl_lst_Classifications();
			if (vm.Mode == Mode.Edit)
			{
				classifications = _dbSubmission.tbl_lst_Classifications.Find(id);
			}

			classifications.Id = vm.Id ?? 0;
			classifications.ManufacturerId = vm.ManufacturerId;
			classifications.ProductLine = vm.ProductLine;
			classifications.Name = vm.Name;
			classifications.GroupId = vm.GroupId;
			classifications.ClassificationTypeId = vm.ClassificationTypeId;
			classifications.Active = vm.Active;


			if (vm.Mode == Mode.Add)
			{
				_dbSubmission.tbl_lst_Classifications.Add(classifications);
			}

			_dbSubmission.SaveChanges();

			CacheManager.Clear(LookupAjax.Classifications.ToString());

			return RedirectToAction("Index");
		}

		[HttpPost]
		public JsonResult CheckDelete(int id)
		{
			var classifications = _dbSubmission.tbl_lst_Classifications.Find(id);
			var isInUse = _dbSubmission.tbl_lst_ProductClassifications.Any(x => x.ClassificationId == id) && classifications.Active;

			return Json(isInUse);
		}

		[HttpPost]
		public ActionResult Delete(int id)
		{
			_dbSubmission.tbl_lst_Classifications.Where(x => x.Id == id).Delete();
			_dbSubmission.tbl_lst_ProductClassifications.Where(x => x.ClassificationId == id).Delete();

			CacheManager.Clear(LookupAjax.Classifications.ToString());

			return RedirectToAction("Index");
		}

		[HttpPost]
		public ActionResult SetActive(int id, Mode mode)
		{
			var active = (mode == Mode.Deactivate) ? false : true;

			var classifications = _dbSubmission.tbl_lst_Classifications.Find(id);
			classifications.Active = active;

			_dbSubmission.SaveChanges();

			CacheManager.Clear(LookupAjax.Classifications.ToString());

			return RedirectToAction("Index");

		}
	}
}