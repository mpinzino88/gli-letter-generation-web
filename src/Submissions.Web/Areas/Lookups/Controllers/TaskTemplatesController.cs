﻿extern alias ZEFCore;
using AutoMapper;
using GLI.EFCore.Submission;
using Submissions.Common;
using Submissions.Web.Areas.Lookups.Models;
using System.Linq;
using System.Web.Mvc;
using ZEFCore::Z.EntityFramework.Plus;

namespace Submissions.Web.Areas.Lookups.Controllers
{
	public class TaskTemplatesController : Controller
	{
		private readonly SubmissionContext _dbSubmission;
		private readonly IUserContext _userContext;

		public TaskTemplatesController(SubmissionContext dbSubmission, IUserContext userContext)
		{
			_dbSubmission = dbSubmission;
			_userContext = userContext;
		}

		public ActionResult Index()
		{
			var vm = new TaskTemplateIndexViewModel();
			return View(vm);
		}

		public ActionResult Edit(int? id, Mode mode)
		{
			var vm = new TaskTemplatesViewModel();
			vm.Mode = mode;
			vm.CanSave = vm.Mode == Mode.Add;

			if (mode == Mode.Edit || (mode == Mode.Add && id != null))
			{
				var taskTemplate = _dbSubmission.trTaskTemplates.Find(id);
				vm = Mapper.Map<TaskTemplatesViewModel>(taskTemplate);
				vm.Mode = mode;
				vm.CanSave = taskTemplate.UserId == _userContext.User.Id || mode == Mode.Add || Util.HasAccess(Permission.Admin, AccessRight.Edit);

				vm.Tasks = _dbSubmission.trTaskLinks
					.Where(x => x.TaskTemplateId == id)
					.OrderBy(x => x.Position)
					.Select(x => new Task
					{
						Id = x.Id,
						Description = x.Description,
						EstimateHours = x.EstimateHours,
						TaskDefinitionId = x.TaskDefinitionId,
						TaskDefinition = x.TaskDefinition.Code + " - " + x.TaskDefinition.DynamicsId,
						Position = x.Position,
						IsETATask = x.IsETA,
						IsETAEligible = x.TaskDefinition.IsETATask
					})
					.ToList();
			}

			if (mode == Mode.Add)
			{
				vm.Id = null;
				vm.UserName = _userContext.User.Name;
			}
			return View(vm);
		}

		[HttpPost]
		public ActionResult Edit(int? id, TaskTemplatesViewModel vm)
		{
			var taskTemplate = new trTaskTemplate();
			if (vm.Mode == Mode.Edit)
			{
				taskTemplate = _dbSubmission.trTaskTemplates.Find(id);
				_dbSubmission.trTaskLinks
					.Where(x => x.TaskTemplateId == vm.Id)
					.Delete();
			}

			if (vm.Mode == Mode.Add)
			{
				taskTemplate.UserId = _userContext.User.Id;
				taskTemplate.IsGlobal = 0;
				_dbSubmission.trTaskTemplates.Add(taskTemplate);
			}
			taskTemplate.Id = vm.Id ?? 0;
			taskTemplate.Code = vm.Code;
			taskTemplate.Description = vm.Description;
			_dbSubmission.SaveChanges();

			foreach (var item in vm.Tasks)
			{
				var task = new trTaskLink();
				task.TaskTemplateId = taskTemplate.Id;
				task.Position = item.Position;
				task.Description = item.Description;
				task.TaskDefinitionId = item.TaskDefinitionId;
				task.EstimateHours = item.EstimateHours;
				task.IsETA = item.IsETATask;
				_dbSubmission.trTaskLinks.Add(task);
			}

			_dbSubmission.SaveChanges();

			CacheManager.Clear(LookupAjax.TaskTemplates.ToString());

			return RedirectToAction("Index");
		}

		[HttpGet]
		public ActionResult LoadTaskDefinition(int id)
		{
			var taskDefinition = _dbSubmission.trTaskDefs
				.Where(x => x.Id == id)
				.Select(x => new
				{
					x.Description,
					x.EstimateHours,
					x.IsETATask
				})
				.Single();

			return Content(ComUtil.JsonEncodeCamelCase(taskDefinition), "application/json");
		}

		public ActionResult Delete(int id)
		{
			_dbSubmission.trTaskTemplates.Where(x => x.Id == id).Delete();
			_dbSubmission.trTaskLinks.Where(x => x.TaskTemplateId == id).Delete();

			CacheManager.Clear(LookupAjax.TaskTemplates.ToString());

			return RedirectToAction("Index");
		}

		public JsonResult ValidateCode(int? id, string code, Mode mode)
		{
			var validates = true;
			if (mode == Mode.Add && _dbSubmission.trTaskTemplates.Any(x => x.Id != id && x.Code == code && x.UserId == _userContext.User.Id))
			{
				validates = false;
			}

			return Json(validates, JsonRequestBehavior.AllowGet);
		}

		[HttpPost]
		public JsonResult CheckDelete(int id)
		{
			return Json(false);
		}
	}
}
