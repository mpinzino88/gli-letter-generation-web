﻿extern alias ZEFCore;
using AutoMapper.QueryableExtensions;
using GLI.EFCore.Submission;
using Microsoft.EntityFrameworkCore;
using Submissions.Common;
using Submissions.Web.Areas.Lookups.Models;
using System.Linq;
using System.Web.Mvc;
using ZEFCore::Z.EntityFramework.Plus;

namespace Submissions.Web.Areas.Lookups.Controllers
{
	[AuthorizeUser(Permission = Permission.Admin, HasAccessRight = AccessRight.Edit)]
	public class GameDescriptionTypesController : Controller, ILookupDelete<int>
	{
		private readonly SubmissionContext _dbSubmission;

		public GameDescriptionTypesController(SubmissionContext dbSubmission)
		{
			_dbSubmission = dbSubmission;
		}

		public ActionResult Index()
		{
			var descrtiptionTypes = _dbSubmission.tbl_lst_GameDescriptionTypes
				.AsNoTracking()
				.OrderBy(x => x.Id)
				.ProjectTo<GameDescriptionTypeViewModel>();

			return View(descrtiptionTypes);
		}

		public ActionResult Edit(int? id, Mode mode)
		{
			var vm = new GameDescriptionTypeViewModel();

			if (mode == Mode.Edit)
			{
				var type = _dbSubmission.tbl_lst_GameDescriptionTypes.Find(id);

				vm.Id = type.Id;
				vm.Name = type.Name;
			}
			vm.Mode = mode;

			return View(vm);
		}

		[HttpPost]
		public ActionResult Edit(int? id, GameDescriptionTypeViewModel vm)
		{
			var type = new tbl_lst_GameDescriptionTypes();
			if (vm.Mode == Mode.Edit)
			{
				type = _dbSubmission.tbl_lst_GameDescriptionTypes.Find(id);
			}

			type.Id = vm.Id ?? 0;
			type.Name = vm.Name;

			if (vm.Mode == Mode.Add)
			{
				_dbSubmission.tbl_lst_GameDescriptionTypes.Add(type);
			}

			_dbSubmission.SaveChanges();

			CacheManager.Clear(LookupAjax.GameDescriptionTypes.ToString());

			return RedirectToAction("Index");
		}

		[HttpPost]
		public JsonResult CheckDelete(int id)
		{
			// This may need to be expanded
			return Json(false);
		}

		[HttpPost]
		public ActionResult Delete(int id)
		{
			_dbSubmission.tbl_lst_GameDescriptionTypes.Where(x => x.Id == id).Delete();
			_dbSubmission.SaveChanges();

			CacheManager.Clear(LookupAjax.GameDescriptionTypes.ToString());

			return RedirectToAction("Index");
		}
	}
}