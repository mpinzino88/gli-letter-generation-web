﻿using AutoMapper;
using GLI.EFCore.Submission;
using Submissions.Common;
using Submissions.Web.Areas.Lookups.Models;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace Submissions.Web.Areas.Lookups.Controllers
{
	public class CabinetsController : Controller
	{
		private readonly SubmissionContext _dbSubmission;

		public CabinetsController(SubmissionContext submissionContext)
		{
			_dbSubmission = submissionContext;
		}

		public ActionResult Index()
		{
			var cabinets = _dbSubmission.tbl_lst_Cabinet
				.Select(x => x)
				.OrderByDescending(x => x.Active)
				.ThenBy(x => x.ManufacturerGroup.Description)
				.ThenBy(x => x.Cabinet)
				.ToList();

			var vm = Mapper.Map<List<CabinetViewModel>>(cabinets);

			return View(vm);
		}

		public ActionResult Edit(int? id)
		{
			var vm = new CabinetViewModel();
			vm.Mode = Mode.Add;
			if (id != null)
			{
				var cabinet = _dbSubmission.tbl_lst_Cabinet.Find(id);
				vm.Cabinet = cabinet.Cabinet;
				vm.Id = cabinet.Id;
				vm.ManufacturerGroup = cabinet.ManufacturerGroup.Description;
				vm.ManufacturerGroupId = cabinet.ManufacturerGroupId;
				vm.Active = cabinet.Active;
				vm.Mode = Mode.Edit;
			}
			else
			{
				vm.Active = true;
			}

			return View(vm);
		}

		[HttpPost]
		public ActionResult Edit(CabinetViewModel vm)
		{
			var cabinet = _dbSubmission.tbl_lst_Cabinet.Find(vm.Id);
			if (cabinet == null)
			{
				cabinet = new tbl_lst_Cabinet();
			}
			cabinet.Cabinet = vm.Cabinet;
			cabinet.ManufacturerGroupId = (int)vm.ManufacturerGroupId;
			cabinet.Active = vm.Active;
			if (cabinet.Id == 0)
			{
				_dbSubmission.tbl_lst_Cabinet.Add(cabinet);
			}
			_dbSubmission.SaveChanges();
			return RedirectToAction("Index");
		}

		public ActionResult Delete(int id)
		{
			var cabinet = _dbSubmission.tbl_lst_Cabinet.Find(id);
			_dbSubmission.tbl_lst_Cabinet.Remove(cabinet);
			_dbSubmission.SaveChanges();

			return RedirectToAction("Index");
		}

		public ActionResult ToggleActive(int id)
		{
			var cabinet = _dbSubmission.tbl_lst_Cabinet.Find(id);
			cabinet.Active = !cabinet.Active;
			_dbSubmission.SaveChanges();

			return RedirectToAction("Index");
		}

		public JsonResult CheckTempTable(int id)
		{
			var exists = _dbSubmission.trIGTTemporaryTables.Any(x => x.CabinetId == id);

			return Json(exists, JsonRequestBehavior.AllowGet);
		}
	}
}