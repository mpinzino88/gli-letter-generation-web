﻿extern alias ZEFCore;
using GLI.EFCore.Submission;
using Microsoft.EntityFrameworkCore;
using Submissions.Common;
using Submissions.Web.Areas.Lookups.Models;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using ZEFCore::Z.EntityFramework.Plus;

namespace Submissions.Web.Areas.Lookups.Controllers
{
	public class QATaskDefinitionsController : Controller
	{
		private readonly SubmissionContext _dbSubmission;

		public QATaskDefinitionsController(SubmissionContext submissionContext)
		{
			_dbSubmission = submissionContext;
		}

		public ActionResult Index()
		{
			var vm = new QATaskDefinitionsIndexViewModel();
			var qaTaskDefs = _dbSubmission.QABundles_TaskDef
				.OrderBy(x => x.TaskDefDesc)
				.Select(x => new QATaskDefinitionsViewModel()
				{
					Name = x.TaskDefDesc,
					Id = x.Id,
					IsTaskForOriginalTesting = x.TaskForBundles,
					IsTaskForClosedJurisdictionsTesting = x.TaskForBundlesDN
				})
				.ToList();

			vm.QATaskDefinitions = qaTaskDefs;

			return View(vm);
		}

		public ActionResult Edit(int id, Mode Mode)
		{
			var vm = new QATaskDefinitionsEditViewModel();
			vm.Mode = Mode.ToString();
			vm.TaskDependedOnName = new List<SelectListItem>();
			vm.FinalizationPriorityOptions = new List<SelectListItem>();
			vm.FinalizationPriorityOptions.Add(new SelectListItem { Text = "", Value = "" });
			vm.FinalizationPriorityOptions.Add(new SelectListItem { Text = "Low", Value = "1" });
			vm.FinalizationPriorityOptions.Add(new SelectListItem { Text = "Medium", Value = "2" });
			vm.FinalizationPriorityOptions.Add(new SelectListItem { Text = "High", Value = "3" });
			vm.FinalizationPriorityOptions.Add(new SelectListItem { Text = "Highest", Value = "4" });

			if (Mode == Mode.Edit)
			{
				var currentTask = _dbSubmission.QABundles_TaskDef.Where(x => x.Id == id).SingleOrDefault();
				if (currentTask != null)
				{
					vm.Id = currentTask.Id;
					vm.Name = currentTask.TaskDefDesc;
					vm.ShortName = currentTask.TaskShortDesc;
					vm.TaskDependedOn = currentTask.TaskDependedOn;
					vm.IsReviewTask = currentTask.IsReviewTask;
					vm.IsTaskForOriginalTesting = currentTask.TaskForBundles;
					vm.IsTaskForClosedJurisdictionsTesting = currentTask.TaskForBundlesDN;
					vm.FinalizationPriority = currentTask.TaskFinalizationPriority;
					vm.IsFinalizationTask = (currentTask.TaskFinalizationPriority > 0) ? true : false;
				}

				if (!string.IsNullOrEmpty(vm.TaskDependedOn))
				{
					var selDependedTasksIds = vm.TaskDependedOn.Split(',').Select(int.Parse).ToList();
					var selDependedTasks = _dbSubmission.QABundles_TaskDef.Where(x => selDependedTasksIds.Contains(x.Id)).Select(x => new SelectListItem() { Text = x.TaskDefDesc, Value = x.Id.ToString(), Selected = true }).ToList();

					vm.TaskDependedOnIds = selDependedTasksIds.Select(i => i.ToString()).ToList();
					vm.TaskDependedOnName = selDependedTasks;
				}

			}
			return View(vm);
		}

		[HttpPost]
		public ContentResult Edit(QATaskDefinitionsEditViewModel taskEditVm)
		{
			if (taskEditVm.Mode == Mode.Add.ToString())
			{
				var newTask = new QABundles_TaskDef()
				{
					TaskDefDesc = taskEditVm.Name,
					IsReviewTask = taskEditVm.IsReviewTask,
					TaskFinalizationPriority = (byte?)(taskEditVm.FinalizationPriority),
					TaskForBundles = taskEditVm.IsTaskForOriginalTesting,
					TaskForBundlesDN = taskEditVm.IsTaskForClosedJurisdictionsTesting,
					TaskShortDesc = taskEditVm.ShortName,
					TaskDependedOn = string.Join(",", taskEditVm.TaskDependedOnIds)
				};
				_dbSubmission.QABundles_TaskDef.Add(newTask);
				_dbSubmission.SaveChanges();
			}
			else
			{
				var qaTask = _dbSubmission.QABundles_TaskDef.Where(x => x.Id == taskEditVm.Id).SingleOrDefault();
				if (qaTask != null)
				{
					qaTask.TaskDefDesc = taskEditVm.Name;
					qaTask.IsReviewTask = taskEditVm.IsReviewTask;
					qaTask.TaskFinalizationPriority = (byte?)(taskEditVm.FinalizationPriority);
					qaTask.TaskForBundles = taskEditVm.IsTaskForOriginalTesting;
					qaTask.TaskForBundlesDN = taskEditVm.IsTaskForClosedJurisdictionsTesting;
					qaTask.TaskShortDesc = taskEditVm.ShortName;
					qaTask.TaskDependedOn = string.Join(",", taskEditVm.TaskDependedOnIds);

					_dbSubmission.Entry(qaTask).State = EntityState.Modified;
					_dbSubmission.SaveChanges();
				}
			}
			return Content("OK");
		}

		[HttpPost]
		public ActionResult Delete(int id)
		{
			var currentTask = _dbSubmission.QABundles_TaskDef.Where(x => x.Id == id).Delete();
			return Content("OK");
		}
		[HttpGet]
		public ContentResult CheckIfNameExists(string name, int qaTaskDefId)
		{
			var isExists = _dbSubmission.QABundles_TaskDef.Any(x => x.TaskDefDesc == name && x.Id != qaTaskDefId);
			return Content((isExists) ? "Yes" : "No", "text/plain");
		}

	}
}