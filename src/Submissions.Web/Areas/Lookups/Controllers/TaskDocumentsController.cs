﻿extern alias ZEFCore;
using AutoMapper;
using GLI.EFCore.Submission;
using Submissions.Common;
using Submissions.Web.Areas.Lookups.Models;
using System.Linq;
using System.Web.Mvc;
using ZEFCore::Z.EntityFramework.Plus;

namespace Submissions.Web.Areas.Lookups.Controllers
{
	public class TaskDocumentsController : Controller, ILookupDelete<int>
	{
		private readonly SubmissionContext _dbSubmission;

		public TaskDocumentsController(SubmissionContext dbSubmission)
		{
			_dbSubmission = dbSubmission;
		}

		public ActionResult Index()
		{
			var vm = new TaskDocumentsIndexViewModel();
			return View(vm);
		}

		public ActionResult Edit(int? id, Mode mode)
		{
			var vm = new TaskDocumentsEditViewModel();

			if (mode == Mode.Edit)
			{
				var taskDocument = _dbSubmission.trTaskDocs.Find(id);
				vm = Mapper.Map<TaskDocumentsEditViewModel>(taskDocument);
			}

			vm.Mode = mode;

			return View(vm);
		}

		[HttpPost]
		public ActionResult Edit(int? id, TaskDocumentsEditViewModel vm)
		{
			var taskDocument = new trTaskDoc();
			if (vm.Mode == Mode.Edit)
			{
				taskDocument = _dbSubmission.trTaskDocs.Find(id);
			}

			taskDocument.Name = vm.Name;
			taskDocument.Location = vm.Location;

			if (vm.Mode == Mode.Add)
			{
				_dbSubmission.trTaskDocs.Add(taskDocument);
			}

			_dbSubmission.SaveChanges();

			CacheManager.Clear(LookupAjax.TaskDocuments.ToString());

			return RedirectToAction("Index");
		}

		public JsonResult CheckDelete(int id)
		{
			var isInUse = _dbSubmission.trTaskDefDocs.Any(x => x.TaskDocumentId == id);

			return Json(isInUse);
		}

		public ActionResult Delete(int id)
		{
			_dbSubmission.trTaskDocs.Where(x => x.Id == id).Delete();

			CacheManager.Clear(LookupAjax.TaskDocuments.ToString());

			return RedirectToAction("Index");
		}

		public JsonResult ValidateName(int? id, string name, Mode mode)
		{
			var validates = true;

			if (!string.IsNullOrEmpty(name) && _dbSubmission.trTaskDocs.Any(x => x.Id != id && x.Name == name))
			{
				validates = false;
			}

			return Json(validates, JsonRequestBehavior.AllowGet);
		}
	}
}