﻿extern alias ZEFCore;
using AutoMapper.QueryableExtensions;
using GLI.EF.LetterContent;
using GLI.EFCore.Submission;
using Submissions.Common;
using Submissions.Web.Areas.Lookups.Models;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using ZEFCore::Z.EntityFramework.Plus;

namespace Submissions.Web.Areas.Lookups.Controllers
{
	[AuthorizeUser(Permissions = new Permission[] { Permission.LetterGeneration }, HasAccessRight = AccessRight.Edit)]
	public class LetterContentDefaultValuesController : Controller, ILookupDelete<int>
	{
		private readonly ILetterContentContext _dbLetterContent;
		private readonly SubmissionContext _dbSubmission;

		public LetterContentDefaultValuesController(ILetterContentContext dbLetterContent, SubmissionContext dbSubmission)
		{
			_dbLetterContent = dbLetterContent;
			_dbSubmission = dbSubmission;
		}

		public ActionResult Index()
		{
			var defaultValues = _dbLetterContent.DefaultValue
				.OrderByDescending(x => x.Active)
				.ThenBy(x => x.JurisdictionId)
				.ProjectTo<LetterContentDefaultValueViewModel>()
				.ToList();

			return View(HydrateDefaultValuesViewModel(defaultValues));
		}

		public ActionResult Edit(int? id, Mode mode)
		{
			var vm = new LetterContentDefaultValueViewModel();
			vm.Active = true;

			if (mode == Mode.Edit)
			{
				var defaultValue = _dbLetterContent.DefaultValue.Find(id);
				vm.Id = defaultValue.Id;
				vm.JurisdictionId = defaultValue.JurisdictionId;
				vm.ReportTypeId = defaultValue.ReportTypeId;
				vm.TestingResultId = defaultValue.TestingResultId;
				vm.LetterJurisdiction = defaultValue.LetterJurisdiction;
				HydrateDefaultValuesViewModel(new List<LetterContentDefaultValueViewModel> { vm });
			}

			vm.Mode = mode;

			return View(vm);
		}

		[HttpPost]
		public ActionResult Edit(int? id, LetterContentDefaultValueViewModel vm)
		{
			var defaultValue = new DefaultValue();
			if (vm.Mode == Mode.Edit)
			{
				defaultValue = _dbLetterContent.DefaultValue.Find(id);
			}

			defaultValue.Id = vm.Id ?? 0;
			defaultValue.JurisdictionId = (int)vm.JurisdictionId;
			defaultValue.ReportTypeId = (int)vm.ReportTypeId;
			defaultValue.TestingResultId = (int)vm.TestingResultId;
			defaultValue.LetterJurisdiction = vm.LetterJurisdiction;
			defaultValue.Active = vm.Active;

			if (vm.Mode == Mode.Add)
			{
				_dbLetterContent.DefaultValue.Add(defaultValue);
			}

			_dbLetterContent.SaveChanges();

			CacheManager.Clear(LookupAjax.LetterContentDefaultValues.ToString());

			return RedirectToAction("Index");
		}

		[HttpPost]
		public JsonResult CheckDelete(int id)
		{
			//For now returning false. Eventually a check needs to be made if id is being used in an another table
			return Json(false);
		}

		[HttpPost]
		public ActionResult Delete(int id)
		{
			_dbLetterContent.DefaultValue.Where(x => x.Id == id).Delete();

			CacheManager.Clear(LookupAjax.LetterContentDefaultValues.ToString());

			return RedirectToAction("Index");
		}

		[HttpPost]
		public ActionResult SetActive(int id, Mode mode)
		{
			bool active = (mode == Mode.Deactivate ? false : true);

			var defaultValue = _dbLetterContent.DefaultValue.Find(id);
			defaultValue.Active = active;

			_dbLetterContent.SaveChanges();

			CacheManager.Clear(LookupAjax.LetterContentDefaultValues.ToString());

			return RedirectToAction("Index");
		}

		public JsonResult ValidateJurisdiction(int? id, int? jurisdictionId, int? reportTypeId, int? testingTypeId, Mode mode)
		{
			var validates = true;

			if (jurisdictionId != null && _dbLetterContent.DefaultValue.Any(x => x.Id != id && x.JurisdictionId == jurisdictionId))
			{
				validates = false;
			}
			return Json(validates, JsonRequestBehavior.AllowGet);
		}

		private List<LetterContentDefaultValueViewModel> HydrateDefaultValuesViewModel(List<LetterContentDefaultValueViewModel> defaultValues)
		{
			var jurisdictions = _dbSubmission.tbl_lst_fileJuris
				.Select(x => new
				{
					JurisdictionId = x.FixId,
					x.Name
				}).ToList();

			var reportTypes = _dbSubmission.tbl_lst_ReportType
				.Select(x => new
				{
					x.Id,
					x.Type
				}).ToList();

			var testingResults = _dbSubmission.tbl_lst_TestingResult
				.Select(x => new
				{
					x.Id,
					x.Result
				}).ToList();

			foreach (var defaultValue in defaultValues)
			{
				defaultValue.Jurisdiction = jurisdictions
					.Where(x => x.JurisdictionId == defaultValue.JurisdictionId)
					.Select(x => x.Name)
					.SingleOrDefault();

				defaultValue.ReportType = reportTypes
					.Where(x => x.Id == defaultValue.ReportTypeId)
					.Select(x => x.Type)
					.SingleOrDefault();

				defaultValue.TestingResult = testingResults
					.Where(x => x.Id == defaultValue.TestingResultId)
					.Select(x => x.Result)
					.SingleOrDefault();
			}

			return defaultValues;
		}
	}
}
