﻿using AutoMapper.QueryableExtensions;
using EF.Submission;
using Newtonsoft.Json;
using Submissions.Common;
using Submissions.Web.Areas.Lookups.Models;
using Submissions.Web.Areas.Tools.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;
using DBModels = EF.Submission;

namespace Submissions.Web.Areas.Lookups.Controllers
{
	[AuthorizeUser(Permission = Permission.LetterGeneration, HasAccessRight = AccessRight.Edit)]
	public class PlatformPiecesController : Controller, ILookupDelete<int>
	{
		private readonly ISubmissionContext _dbSubmission;
		private readonly IUserContext _userContext;

		public PlatformPiecesController(
			ISubmissionContext dbSubmission,
			IUserContext userContext
			)
		{
			_dbSubmission = dbSubmission;
			_userContext = userContext;
		}

		public ActionResult Index()
		{
			var vm = new PlatformPiecesIndexViewModel();
			vm.PlatformPiecesTemplates = _dbSubmission.PlatformPiecesTemplates
				.Include(ppt => ppt.Manufacturer)
				.Include(ppt => ppt.Jurisdictions)
				.Include(ppt => ppt.Jurisdictions.Select(j => j.Jurisdiction))
				.Include(ppt => ppt.Platform)
				.Where(ppt => ppt.Active)
				.Select(ppt => new PlatformPiecesTemplateDTO
				{
					Manufacturer = ppt.Manufacturer.Description,
					ManufacturerCode = ppt.ManufacturerCode,
					Jurisdictions = ppt.Jurisdictions.Select(j => j.Jurisdiction).Select(j => new JurisdictionInfo { JurisdictionId = j.FixId, Jurisdiction = j.Name }).ToList(),
					Name = ppt.Name,
					Description = ppt.Description,
					Id = ppt.Id,
					OriginalId = ppt.OriginalId,
					Platform = new Tools.Models.ManufacturerPlatform
					{
						Id = ppt.PlatformId,
						Name = ppt.Platform.Name,
						ManufacturerGroupId = ppt.Platform.ManufacturerGroupId
					}
				})
				.OrderBy(ppt => ppt.OriginalId)
				.ToList();
			return View(vm);
		}

		public ActionResult Add()
		{
			var vmEdit = new PlatformPiecesEditViewModel();
			vmEdit.PlatformPieces.Add(new PlatformPieceDTO());
			vmEdit.Platform.Name = "";
			vmEdit.Mode = Mode.Add;
			return View("Edit", vmEdit);
		}

		public ActionResult Edit(int? id)
		{
			var vmEdit = new PlatformPiecesEditViewModel();
			if (!id.HasValue)
			{
				vmEdit.PlatformPieces.Add(new PlatformPieceDTO());
				vmEdit.Mode = Mode.Add;
			}
			else
			{
				vmEdit = _dbSubmission.PlatformPiecesTemplates
									.Include(ppt => ppt.Manufacturer)
									.Include(ppt => ppt.Jurisdictions)
									.Include(ppt => ppt.Jurisdictions.Select(j => j.Jurisdiction))
									.Include(ppt => ppt.Platform)
									.Where(ppt => ppt.Id == id.Value)
									.ProjectTo<PlatformPiecesEditViewModel>()
									.Single();
				//vmEdit.PlatformPieces.ForEach(pp => pp.Deleted = false);
				vmEdit.Mode = Mode.Edit;
			}
			return View("Edit", vmEdit);

		}

		[HttpPost]
		public ActionResult Edit(string editViewModelString)
		{
			var savedTemplate = JsonConvert.DeserializeObject<PlatformPiecesEditViewModel>(editViewModelString);

			if (savedTemplate.Mode == Mode.Add)
			{
				SaveNewTemplate(savedTemplate);
			}
			else
			{
				DeactivateTemplate(savedTemplate.Id);
				SaveNewTemplate(savedTemplate);

			}
			return RedirectToAction("Index");
		}
		/// <summary>
		/// 
		/// </summary>
		/// <param name="savedTemplate"></param>
		private void UpdateAssociatedSoftwareTemplates(PlatformPiecesTemplate newTemplate, PlatformPiecesEditViewModel oldTemplate, List<ModifiedPlatfromPiece> modifiedPieces)
		{
			var pieceIds = modifiedPieces.Select(mp => mp.ModifiedPiece.Id).ToList();
			var affectedTemplateIds = GetLinkedTemplateIds(pieceIds);
			if (affectedTemplateIds.Count == 0) return;

			var affectedTemplates = _dbSubmission.AssociatedSoftwareTemplates
				.Include(ast => ast.Platform)
				.Include(ast => ast.Jurisdictions)
				.Include(ast => ast.Manufacturer)
				.Include(ast => ast.AssociatedSoftwares)
				.Where(ast => ast.Active && affectedTemplateIds.Contains(ast.Id))
				.OrderBy(ast => ast.OriginalId)
				.ToList();
			if (affectedTemplates.Count == 0) return;

			var modOrDeletePieceIds = modifiedPieces.Where(mp => mp.ModifiedPiece.Deleted || mp.ModifiedPiece.Modified).Select(mp => mp.ModifiedPiece.Id).ToList();

			List<DBModels.AssociatedSoftwareTemplate> modifiedTemplates = new List<DBModels.AssociatedSoftwareTemplate>();
			var saveDate = DateTime.UtcNow;
			foreach (var template in affectedTemplates)
			{
				var newASTemplate = new DBModels.AssociatedSoftwareTemplate
				{
					Active = true,
					AddDate = saveDate,
					AddUserId = _userContext.User.Id,
					Description = template.Description,
					ManufacturerCode = template.ManufacturerCode,
					Name = template.Name,
					OriginalId = template.OriginalId,
					PlatformId = template.PlatformId,
					//this column needs to be updated to allow nulls
					JurisdictionId = "00",
					Jurisdictions = template.Jurisdictions.Select(j => new AssociatedSoftwareTemplatetbl_lst_fileJuris
					{
						JurisdictionId = j.JurisdictionId.ToJurisdictionIdString()
					}).ToList(),
				};
				//only copy over the components that aren't tied to modified/deleted pieces
				newASTemplate.AssociatedSoftwares = template.AssociatedSoftwares
					.Where(asw => asw.PlatformPieceId.HasValue && !modOrDeletePieceIds.Contains(asw.PlatformPieceId.Value))
					.Select(asw => new AssociatedSoftware
					{
						OrderId = asw.OrderId,
						SubmissionId = asw.SubmissionId,
						Description = asw.Description,
						Footnotes = asw.Footnotes.Select(fn => new AssociatedSoftwareFootnote
						{
							Memo = fn.Memo,
						}).ToList(),
						PlatformPieceId = modifiedPieces.FirstOrDefault(mp => mp.ModifiedPiece.Id == asw.PlatformPieceId).NewlySavedPiece.Id
					}).ToList();


				modifiedTemplates.Add(newASTemplate);
			}
			DeactivateAssociatedSoftwareTemplate(affectedTemplateIds, saveDate);
			_dbSubmission.AssociatedSoftwareTemplates.AddRange(modifiedTemplates);
			_dbSubmission.SaveChanges();

		}

		private void DeactivateAssociatedSoftwareTemplate(List<int> ids, DateTime deactivateDate)
		{
			foreach (var deactivateTemplate in _dbSubmission.AssociatedSoftwareTemplates.Where(asw => ids.Contains(asw.Id)))
			{
				deactivateTemplate.DeactivateDate = deactivateDate;
				deactivateTemplate.DeactivateUserId = _userContext.User.Id;
				deactivateTemplate.Active = false;
			}
			_dbSubmission.SaveChanges();
		}
		private void DeactivateTemplate(int id)
		{
			var deactivateTemplate = _dbSubmission.PlatformPiecesTemplates.First(ast => ast.Id == id);
			deactivateTemplate.DeactivateDate = DateTime.UtcNow;
			deactivateTemplate.DeactivateUserId = _userContext.User.Id;
			deactivateTemplate.Active = false;
			_dbSubmission.SaveChanges();
		}


		private void SaveNewTemplate(PlatformPiecesEditViewModel template)
		{
			var newTemplate = new PlatformPiecesTemplate()
			{
				Active = true,
				OriginalId = template.OriginalId,
				Name = template.Name,
				Description = template.Description,
				ManufacturerCode = template.ManufacturerCode,
				PlatformId = template.Platform.Id,
				AddDate = DateTime.UtcNow,
				AddUserId = _userContext.User.Id,
				Jurisdictions = template.Jurisdictions.Select(jur => new PlatformPiecesTemplatetbl_lst_fileJuris
				{
					JurisdictionId = jur.JurisdictionId.ToJurisdictionIdString()
				}).ToList(),

			};

			List<ModifiedPlatfromPiece> pieceCombos = new List<ModifiedPlatfromPiece>();
			template.PlatformPieces.ForEach(pp =>
			{
				var dbPiece = new PlatformPiece
				{
					Name = pp.Name,
					OrderId = pp.OrderId,
					Required = pp.Required,
					Filter = pp.Filter,
				};
				pieceCombos.Add(new ModifiedPlatfromPiece { ModifiedPiece = pp, NewlySavedPiece = dbPiece });

			});
			newTemplate.PlatformPieces = pieceCombos.Where(tup => !tup.ModifiedPiece.Deleted).Select(tup => tup.NewlySavedPiece).ToList();
			_dbSubmission.PlatformPiecesTemplates.Add(newTemplate);
			_dbSubmission.SaveChanges();
			if (newTemplate.OriginalId == 0)
			{
				newTemplate.OriginalId = newTemplate.Id;
				_dbSubmission.SaveChanges();
			}
			UpdateAssociatedSoftwareTemplates(newTemplate, template, pieceCombos);


		}

		internal class ModifiedPlatfromPiece
		{
			public PlatformPieceDTO ModifiedPiece { get; set; }
			public PlatformPiece NewlySavedPiece { get; set; }
		}

		[HttpPost]
		public JsonResult CheckDelete(int id)
		{
			return Json(false);
		}

		[HttpPost]
		public ActionResult Delete(int id)
		{
			DeactivateTemplate(id);
			return RedirectToAction("Index");
		}
		[HttpGet]
		public ContentResult CheckTemplateUsage(string rawPieceIds)
		{
			//var savedTemplate = JsonConvert.DeserializeObject<PlatformPiecesEditViewModel>(editViewModelString);
			//var pieceIds = savedTemplate.PlatformPieces.Select(pp => pp.Id).ToList();
			var pieceIds = JsonConvert.DeserializeObject<List<int>>(rawPieceIds);
			List<int> linkedTemplateIds = GetLinkedTemplateIds(pieceIds);
			var linkedTemplates = _dbSubmission.AssociatedSoftwareTemplates
									.Where(ast => ast.Active && linkedTemplateIds.Contains(ast.Id))
									.Select(ast => new Models.AssociatedSoftwareTemplate
									{
										Id = ast.Id,
										OriginalId = ast.Id,
										Name = ast.Name,
										Description = ast.Description
									})
									.ToList();


			return Content(ComUtil.JsonEncodeCamelCase(linkedTemplates), "application/json");

		}

		private List<int> GetLinkedTemplateIds(List<int> pieceIds)
		{
			return _dbSubmission.AssociatedSoftwares
											.Where(assSoc => assSoc.PlatformPieceId.HasValue && pieceIds.Contains(assSoc.PlatformPieceId.Value))
											.Select(assSoc => assSoc.AssociatedSoftwareTemplateId)
											.Distinct()
											.ToList();
		}
	}
}