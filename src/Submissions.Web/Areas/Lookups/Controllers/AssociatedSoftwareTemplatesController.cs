﻿using AutoMapper.QueryableExtensions;
//using EFCore.Submission;
using GLI.EFCore.Submission;
using Newtonsoft.Json;
using Submissions.Common;
using Submissions.Web.Areas.Lookups.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;
using EFModels = GLI.EFCore.Submission;

namespace Submissions.Web.Areas.Lookups.Controllers
{
	[AuthorizeUser(Permission = Permission.LetterGeneration, HasAccessRight = AccessRight.Edit)]
	public class AssociatedSoftwareTemplatesController : Controller, ILookupDelete<int>
	{
		private readonly SubmissionContext _dbSubmission;
		private readonly IUserContext _userContext;
		private static AssociatedSoftwareTemplatesIndexView vm = new AssociatedSoftwareTemplatesIndexView();
		//
		public AssociatedSoftwareTemplatesController(
			SubmissionContext dbSubmission,
			IUserContext userContext
			)
		{
			_dbSubmission = dbSubmission;
			_userContext = userContext;
		}

		private AssociatedSoftwareTemplatesIndexView InitializeIndexViewMode()
		{
			RefreshTemplateList();
			vm.Active = true;
			vm.Mode = Mode.Activate;
			return vm;

		}
		private void RefreshTemplateList()
		{
			vm.Templates = _dbSubmission.AssociatedSoftwareTemplates
				.Include(ast => ast.Platform)
				.Include(ast => ast.Jurisdiction)
				.Include(ast => ast.Manufacturer)
				.Where(ast => ast.Active)
				.OrderBy(ast => ast.OriginalId)
				.ProjectTo<Models.AssociatedSoftwareTemplate>()
				.ToList();
		}

		// GET: Lookups/ComponentCabinets
		public ActionResult Index()
		{
			InitializeIndexViewMode();

			return View(vm);
		}

		public ActionResult Add()
		{
			return EditView(null, Mode.Add);
		}

		public ActionResult Edit(int? id)
		{
			return EditView(id, Mode.Edit);
		}

		public ActionResult EditView(int? id, Mode mode)
		{
			var vmEdit = new AssociatedSoftwareTemplateEditViewModel();
			if (id.HasValue)
			{
				var editingId = id.Value;
				var targetTemplate = vm.Templates.FirstOrDefault(tt => tt.Id == editingId);
				if (TemplateUpdated(editingId))
				{
					var targetId = targetTemplate.OriginalId;
					RefreshTemplateList();
					targetTemplate = vm.Templates.OrderByDescending(template => template.Id).FirstOrDefault(template => template.OriginalId == targetId);
					editingId = targetTemplate.Id;
				}
				vmEdit.Id = targetTemplate.Id;
				vmEdit.OriginalId = targetTemplate.OriginalId;
				vmEdit.Name = targetTemplate.Name;
				vmEdit.Description = targetTemplate.Description;
				vmEdit.Jurisdictions = targetTemplate.Jurisdictions;
				vmEdit.ManufacturerCode = targetTemplate.ManufacturerCode;
				vmEdit.Manufacturer = targetTemplate.Manufacturer;
				vmEdit.PlatformId = targetTemplate.PlatformId;
				vmEdit.Platform = targetTemplate.Platform;
				vmEdit.AssociatedSoftwares = _dbSubmission.AssociatedSoftwares
					.Include(sw => sw.Footnotes)
					.Include(sw => sw.Submission)
					.Where(sw => sw.AssociatedSoftwareTemplateId == editingId)
					.Select(sw => new TemplateComponent
					{
						Component = new Core.Models.DTO.SubmissionDTO
						{
							FileNumber = sw.Submission.FileNumber,
							IdNumber = sw.Submission.IdNumber,
							Id = sw.Submission.Id
						},
						ComponentId = sw.SubmissionId,
						Description = sw.Description,
						OrderId = sw.OrderId,
						Id = sw.Id,
						PlatformPieceId = sw.PlatformPieceId.HasValue ? sw.PlatformPieceId.Value : 0,
						FootNotes = sw.Footnotes.Select(fn => new TemplateComponentFootnote
						{
							Id = fn.Id,
							Memo = fn.Memo
						}).ToList()
					})
					.ToList();

				var pieceIds = vmEdit.AssociatedSoftwares.Select(sw => sw.PlatformPieceId).Distinct().ToList();
				var templateIds = _dbSubmission.PlatformPieces
					.Where(pp => pieceIds.Contains(pp.Id))
					.Select(pp => pp.PlatformPiecesTemplateId)
					.Distinct()
					.ToList();

				vmEdit.PlatformPiecesTemplates = _dbSubmission.PlatformPiecesTemplates
					.Include(ppt => ppt.PlatformPieces)
					.Include(ppt => ppt.Jurisdictions)
					.Include(ppt => ppt.Jurisdictions.Select(jur => jur.Jurisdiction))
					.Where(ppt => templateIds.Contains(ppt.Id))
					.ProjectTo<PlatformPiecesTemplateDTO>()
					.ToList();

			}

			vmEdit.Mode = mode;
			return View("Edit", vmEdit);
		}

		private bool TemplateUpdated(int templateId)
		{
			var targetTemplate = _dbSubmission.AssociatedSoftwareTemplates.Find(templateId);
			return !targetTemplate.Active;
		}

		[HttpPost]
		public JsonResult EditValidate(string editViewModelString)
		{
			var savedTemplate = JsonConvert.DeserializeObject<AssociatedSoftwareTemplateEditViewModel>(editViewModelString);
			var components = savedTemplate.AssociatedSoftwares.Where(sw => sw.ComponentId != 0 && sw.PlatformPieceId != 0).Select(m => new { m.ComponentId, m.Component.IdNumber, m.Component.FileNumber }).ToList();
			var jurisdictions = savedTemplate.Jurisdictions.Select(x => x.JurisdictionId).ToList();
			var jurisdictionIds = _dbSubmission.tbl_lst_fileJuris.Where(x => jurisdictions.Contains(x.FixId)).Select(x => x.Id).ToList();

			var jurisdictionalData = _dbSubmission.JurisdictionalData.Where(x => components.Select(c => c.ComponentId).ToList().Contains((int)x.SubmissionId) && jurisdictionIds.Contains(x.JurisdictionId)).Select(x => new { SubmissionId = x.Submission.Id, FileNumber = x.Submission.FileNumber, IdNumber = x.Submission.IdNumber, Status = x.Status, JurisdictionName = x.JurisdictionName + "(" + x.JurisdictionId + ")" }).ToList();

			var allJurs = (from submission in components
						   join jurisdiction in jurisdictionalData on submission.ComponentId equals jurisdiction.SubmissionId into jc
						   from jurisdiction in jc.DefaultIfEmpty()
						   select (new
						   {
							   submission.FileNumber,
							   submission.IdNumber,
							   JurisdictionName = (jurisdiction != null) ? jurisdiction.JurisdictionName : "Not Found",
							   Status = (jurisdiction != null) ? jurisdiction.Status : "Not Found"
						   })).ToList();

			var notApproved = allJurs.Where(x => (x.Status.ToUpper() != JurisdictionStatus.AP.ToString() && x.Status.ToUpper() != JurisdictionStatus.DR.ToString() && x.Status.ToUpper() != JurisdictionStatus.TC.ToString())).Select(x => new { FileNumber = x.FileNumber, IdNumber = x.IdNumber, JurisdictionName = x.JurisdictionName }).ToList();
			return Json(new { NotApproved = notApproved }, JsonRequestBehavior.AllowGet);
		}

		[HttpPost]
		public ActionResult Edit(string editViewModelString)
		{
			var savedTemplate = JsonConvert.DeserializeObject<AssociatedSoftwareTemplateEditViewModel>(editViewModelString);
			savedTemplate = SetupEditViewModel(savedTemplate);


			if (savedTemplate.Mode == Mode.Add)
			{
				SaveNewTemplate(savedTemplate);
			}
			else
			{
				DeactivateTemplate(savedTemplate.Id);
				SaveNewTemplate(savedTemplate);
			}
			return RedirectToAction("Index");
		}

		private void DeactivateTemplate(int id)
		{
			var deactivateTemplate = _dbSubmission.AssociatedSoftwareTemplates.First(ast => ast.Id == id);
			deactivateTemplate.DeactivateDate = DateTime.UtcNow;
			deactivateTemplate.DeactivateUserId = _userContext.User.Id;
			deactivateTemplate.Active = false;
			_dbSubmission.SaveChanges();
		}

		private void SaveNewTemplate(AssociatedSoftwareTemplateEditViewModel template)
		{
			var newTemplate = new EFModels.AssociatedSoftwareTemplate()
			{
				OriginalId = template.OriginalId,
				Name = template.Name,
				Description = template.Description,
				ManufacturerCode = template.ManufacturerCode,
				//this column needs to be updated to allow nulls
				JurisdictionId = "00",
				Jurisdictions = template.Jurisdictions.Select(j => new AssociatedSoftwareTemplatetbl_lst_fileJuris { JurisdictionId = j.JurisdictionId.ToJurisdictionIdString() }).ToList(),
				PlatformId = template.PlatformId,
				Active = true,
				AddDate = DateTime.UtcNow,
				AddUserId = _userContext.User.Id,
				AssociatedSoftwares = template.AssociatedSoftwares.Where(sw => sw.ComponentId != 0 && sw.PlatformPieceId != 0).Select(mas => new AssociatedSoftware()
				{
					Description = mas.Description,
					OrderId = mas.OrderId,
					SubmissionId = mas.ComponentId,
					PlatformPieceId = mas.PlatformPieceId,
					Footnotes = mas.FootNotes.Where(fn => !string.IsNullOrEmpty(fn.Memo)).Select(fn => new AssociatedSoftwareFootnote()
					{
						Memo = fn.Memo
					}).ToList()
				}).ToList()
			};
			_dbSubmission.AssociatedSoftwareTemplates.Add(newTemplate);
			_dbSubmission.SaveChanges();
			if (newTemplate.OriginalId == 0)
			{
				newTemplate.OriginalId = newTemplate.Id;
				_dbSubmission.SaveChanges();
			}
		}

		private AssociatedSoftwareTemplateEditViewModel SetupEditViewModel(AssociatedSoftwareTemplateEditViewModel vm)
		{
			List<SelectListItem> manufacturers;
			manufacturers = _dbSubmission.tbl_lst_fileManu
					.Where(x => vm.ManufacturerCode == x.Code)
					.Select(x => new SelectListItem { Value = x.Code, Text = x.Description, Selected = true })
					.ToList();
			vm.ManufacturerCode = manufacturers[0].Value;
			vm.Manufacturer = manufacturers[0].Text;


			var jurIds = vm.Jurisdictions.Select(j => j.JurisdictionId.ToJurisdictionIdString()).Distinct().ToList();
			var fileJurs = _dbSubmission.tbl_lst_fileJuris
					.Where(x => jurIds.Contains(x.Id))
					.Select(x => new { x.FixId, x.Name })
					.ToList();
			vm.Jurisdictions = fileJurs.Select(fj => new Tools.Models.JurisdictionInfo { JurisdictionId = fj.FixId, Jurisdiction = fj.Name }).ToList();

			var selectedPlat = _dbSubmission.tbl_lst_Platforms
				.FirstOrDefault(x => vm.PlatformId == x.Id);

			List<SelectListItem> platformList = new List<SelectListItem>()
			{
				new SelectListItem()
				{
					Value = selectedPlat.Id.ToString(),
					Text = selectedPlat.Name,
					Selected = true
				}
			};

			vm.PlatformId = selectedPlat.Id;
			vm.Platform = platformList[0].Text;
			return vm;
		}


		[HttpPost]
		public JsonResult CheckDelete(int id)
		{
			return Json(false);
		}

		[HttpPost]
		public ActionResult Delete(int id)
		{
			DeactivateTemplate(id);
			vm.Mode = Mode.Delete;
			return RedirectToAction("Index");
		}

		[HttpGet]
		public ActionResult GetPlatformPieces(string platformPiecesTemplateIdsJson)
		{
			var Ids = JsonConvert.DeserializeObject<int[]>(platformPiecesTemplateIdsJson);
			var ret = _dbSubmission.PlatformPiecesTemplates
						.Include(ppt => ppt.Jurisdictions)
						.Include(ppt => ppt.Jurisdictions.Select(jur => jur.Jurisdiction))
						.Where(ppt => Ids.Contains(ppt.Id))
						.ProjectTo<PlatformPiecesTemplateDTO>()
						.ToList();
			return Content(ComUtil.JsonEncodeCamelCase(ret), "application/json");
		}

	}
}