﻿extern alias ZEFCore;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using GLI.EFCore.Submission;
using Microsoft.EntityFrameworkCore;
using Submissions.Common;
using Submissions.Web.Areas.Lookups.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using ZEFCore::Z.EntityFramework.Plus;

namespace Submissions.Web.Areas.Lookups.Controllers
{
	public class LaboratoriesController : Controller, ILookupDelete<int>
	{
		private readonly SubmissionContext _dbSubmission;

		public LaboratoriesController(SubmissionContext dbSubmission)
		{
			_dbSubmission = dbSubmission;
		}

		public ActionResult Index()
		{
			var vm = new LaboratoriesIndexViewModel();
			vm.Laboratories = _dbSubmission.trLaboratories
				.AsNoTracking()
				.OrderByDescending(x => x.Active)
				.ThenBy(x => x.Name)
				.ProjectTo<LaboratoryLocations>()
				.ToList();

			return View(vm);
		}

		public ActionResult Edit(int? id, Mode mode)
		{
			var vm = new LaboratoriesEditViewModel();
			vm.Active = true;

			if (mode == Mode.Edit)
			{
				var laboratory = _dbSubmission.trLaboratories.Find(id);

				vm = Mapper.Map<LaboratoriesEditViewModel>(laboratory);

				var accreditations = _dbSubmission.LaboratoryAccreditationJoin.Include(x => x.Accreditation)
					.Where(x => x.LaboratoryId == vm.Id)
					.Select(x => new SelectListItem { Value = x.AccreditationId.ToString(), Text = x.Accreditation.Name, Selected = true })
					.ToList();

				vm.AccreditationIds = accreditations.Select(x => x.Value).ToArray();
				vm.Accreditations = accreditations;

				var qaOffice = _dbSubmission.QAOfficeDefaults.Where(x => x.TestLabId == vm.Id).Select(x => new { x.QAOfficeId, QAOffice = x.QAOffice.Location + " - " + x.QAOffice.Name }).SingleOrDefault();

				if (qaOffice != null)
				{
					vm.QAOfficeDefaultId = qaOffice.QAOfficeId;
					vm.QAOfficeDefault = qaOffice.QAOffice;
				}
			}

			vm.Mode = mode;

			return View(vm);
		}

		[HttpPost]
		public ActionResult Edit(int? id, LaboratoriesEditViewModel vm)
		{
			var laboratory = new trLaboratory();
			if (vm.Mode == Mode.Edit)
			{
				laboratory = _dbSubmission.trLaboratories.Find(vm.Id);
			}

			laboratory.Id = vm.Id ?? 0;
			laboratory.Active = vm.Active ? 1 : 0;
			laboratory.Name = vm.Name.Trim();
			laboratory.ArchiveLocation = vm.ArchiveLocation.Trim();
			laboratory.Abbreviation = vm.Abbreviation;
			laboratory.Location = vm.Location.Trim();
			laboratory.CompanyId = vm.CompanyId;
			laboratory.Address1 = vm.Address1.Trim();
			laboratory.Address2 = (String.IsNullOrEmpty(vm.Address2)) ? null : vm.Address2.Trim();
			laboratory.City = vm.City.Trim();
			laboratory.State = vm.State.Trim();
			laboratory.PostalCode = vm.PostalCode.Trim();
			laboratory.CountryId = vm.CountryId ?? 1;

			if (vm.Mode == Mode.Add)
			{
				_dbSubmission.trLaboratories.Add(laboratory);
			}

			var accreditations = vm.AccreditationIds == null ? null : vm.AccreditationIds.Select(x => Convert.ToInt32(x)).ToList();

			_dbSubmission.SaveChanges();
			vm.Id = laboratory.Id;
			UpdateQADefaultRule(vm);
			UpdateAccreditations(laboratory.Id, accreditations);

			CacheManager.Clear(LookupAjax.Laboratories.ToString());

			return RedirectToAction("Index");
		}

		[HttpPost]
		public JsonResult CheckDelete(int id)
		{
			var isInUse = _dbSubmission.JurisdictionalData.Any(x => x.CertificationLabId == id);

			return Json(isInUse);
		}

		[HttpPost]
		public ActionResult Delete(int id)
		{
			_dbSubmission.LaboratoryAccreditationJoin.Where(x => x.LaboratoryId == id).Delete();
			var qaOfficeDefault = _dbSubmission.QAOfficeDefaults.Where(x => x.TestLabId == id).SingleOrDefault();
			if (qaOfficeDefault != null)
			{
				_dbSubmission.QAOfficeDefaults.Remove(qaOfficeDefault);
			}
			_dbSubmission.SaveChanges();
			_dbSubmission.trLaboratories.Where(x => x.Id == id).Delete();

			CacheManager.Clear(LookupAjax.Laboratories.ToString());

			return RedirectToAction("Index");
		}

		[HttpPost]
		public ActionResult SetActive(int id, Mode mode)
		{
			int active = (mode == Mode.Activate ? 1 : 0);

			var laboratory = _dbSubmission.trLaboratories.Find(id);
			laboratory.Active = active;
			_dbSubmission.SaveChanges();

			CacheManager.Clear(LookupAjax.Laboratories.ToString());

			return RedirectToAction("Index");
		}

		public JsonResult ValidateLocation(int? id, string location, Mode mode)
		{
			var validates = true;

			if (!string.IsNullOrEmpty(location) && _dbSubmission.trLaboratories.Any(x => x.Id != id && x.Location == location))
			{
				validates = false;
			}

			return Json(validates, JsonRequestBehavior.AllowGet);
		}

		#region Helper Methods
		private void UpdateAccreditations(int laboratoryId, List<int> accreditationIds)
		{
			_dbSubmission.LaboratoryAccreditationJoin.Where(x => x.LaboratoryId == laboratoryId).Delete();

			if (accreditationIds != null && accreditationIds.Count > 0)
			{
				foreach (var accreditationId in accreditationIds)
				{
					var laboratoryAccreditation = new LaboratoryAccreditationJoin { LaboratoryId = laboratoryId, AccreditationId = accreditationId };
					_dbSubmission.LaboratoryAccreditationJoin.Add(laboratoryAccreditation);
				}

				_dbSubmission.SaveChanges();
			}
		}
		private void UpdateQADefaultRule(LaboratoriesEditViewModel vm)
		{
			var qaOfficeDefaultRule = _dbSubmission.QAOfficeDefaults.Where(x => x.TestLabId == vm.Id).SingleOrDefault();
			if (qaOfficeDefaultRule == null)
			{
				if (vm.QAOfficeDefaultId != null)
				{
					qaOfficeDefaultRule = new QAOfficeDefaults
					{
						TestLabId = vm.Id,
						QAOfficeId = (int)vm.QAOfficeDefaultId
					};
					_dbSubmission.QAOfficeDefaults.Add(qaOfficeDefaultRule);
				}
			}
			else
			{
				if (vm.QAOfficeDefaultId != null)
				{
					qaOfficeDefaultRule.QAOfficeId = (int)vm.QAOfficeDefaultId;
				}
				else
				{
					_dbSubmission.QAOfficeDefaults.Remove(qaOfficeDefaultRule);
				}

			}
			_dbSubmission.SaveChanges();
		}
		#endregion
	}
}