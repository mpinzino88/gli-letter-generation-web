﻿extern alias ZEFCore;
using AutoMapper;
using GLI.EFCore.Submission;
using Newtonsoft.Json;
using Submissions.Common;
using Submissions.Web.Areas.Lookups.Models;
using System;
using System.Linq;
using System.Web.Mvc;
using ZEFCore::Z.EntityFramework.Plus;

namespace Submissions.Web.Areas.Lookups.Controllers
{
	public class TaskDefinitionsController : Controller, ILookupDelete<int>
	{
		private readonly SubmissionContext _dbSubmission;
		private readonly IUserContext _userContext;

		public TaskDefinitionsController(SubmissionContext dbSubmission, IUserContext userContext)
		{
			_dbSubmission = dbSubmission;
			_userContext = userContext;
		}

		public ActionResult Index()
		{
			var vm = new TaskDefinitionsIndexViewModel();
			return View(vm);
		}

		public ActionResult Edit(int? id, Mode mode)
		{
			var vm = new TaskDefinitionsEditViewModel();
			vm.Active = true;

			if (mode == Mode.Edit)
			{
				var taskDefinition = _dbSubmission.trTaskDefs.Find(id);
				var taskDocuments = _dbSubmission.trTaskDefDocs
					.Where(x => x.TaskDefinitionId == id)
					.OrderBy(x => x.TaskDocument.Name)
					.Select(x => new TaskDefinitionDocument
					{
						Id = x.Id,
						TaskDocumentId = x.TaskDocumentId,
						TaskDocument = new SelectListItem { Value = x.TaskDocumentId.ToString(), Text = x.TaskDocument.Name },
						Note = x.Note
					})
					.ToList();

				vm = Mapper.Map<TaskDefinitionsEditViewModel>(taskDefinition);
				vm.Department = new SelectListItem { Value = taskDefinition.DepartmentId.ToString(), Text = taskDefinition.Department.Code + " - " + taskDefinition.Department.Description };
				vm.Documents = taskDocuments;
			}
			vm.Mode = mode;

			return View(vm);
		}

		[HttpPost]
		public ActionResult Edit(string editViewModel)
		{
			var vm = JsonConvert.DeserializeObject<TaskDefinitionsEditViewModel>(editViewModel);
			var taskDefinition = new trTaskDef();

			if (vm.Mode == Mode.Edit)
			{
				taskDefinition = _dbSubmission.trTaskDefs.Find(vm.Id);
			}

			taskDefinition = ApplyChanges(taskDefinition, vm);

			if (vm.Mode == Mode.Add)
			{
				_dbSubmission.trTaskDefs.Add(taskDefinition);
			}

			_dbSubmission.SaveChanges();
			vm.Id = taskDefinition.Id;

			UpdateDocuments(vm);

			CacheManager.Clear(LookupAjax.TaskDefinitions.ToString());

			return RedirectToAction("Index");
		}

		[HttpPost]
		public JsonResult CheckDelete(int id)
		{
			var isInUse = _dbSubmission.trTasks.Where(x => x.TaskDefinitionId == id).Any();
			return Json(isInUse);
		}

		[HttpPost]
		public ActionResult Delete(int id)
		{
			_dbSubmission.trTaskDefDocs.Where(x => x.TaskDefinitionId == id).Delete();
			_dbSubmission.trTaskDefs.Where(x => x.Id == id).Delete();

			CacheManager.Clear(LookupAjax.TaskDefinitions.ToString());

			return RedirectToAction("Index");
		}

		[HttpPost]
		public ActionResult SetActive(int id, Mode mode)
		{
			var disabled = (mode == Mode.Deactivate) ? 1 : 0;

			var taskDefinition = _dbSubmission.trTaskDefs.Find(id);
			taskDefinition.Disabled = Convert.ToInt16(disabled);

			_dbSubmission.SaveChanges();

			CacheManager.Clear(LookupAjax.TaskDefinitions.ToString());

			return RedirectToAction("Index");
		}

		public JsonResult ValidateCode(int? id, string code, Mode mode)
		{
			var validates = true;

			if (!string.IsNullOrEmpty(code) && _dbSubmission.trTaskDefs.Any(x => x.Id != id && x.Code == code))
			{
				validates = false;
			}

			return Json(validates, JsonRequestBehavior.AllowGet);
		}

		#region Helper Methods
		private trTaskDef ApplyChanges(trTaskDef taskDefinition, TaskDefinitionsEditViewModel vm)
		{
			taskDefinition.Id = vm.Id ?? 0;
			taskDefinition.Code = vm.Code;
			taskDefinition.Description = vm.Description;
			taskDefinition.DepartmentId = vm.DepartmentId;
			taskDefinition.Disabled = Convert.ToInt16(vm.Active ? 0 : 1);
			taskDefinition.DynamicsId = vm.DynamicsId;
			taskDefinition.EstimateHours = vm.EstimateHours;
			taskDefinition.EstimateInterval = vm.EstimateInterval;
			taskDefinition.IsReview = Convert.ToInt16(vm.IsReview);
			taskDefinition.IsETATask = vm.IsETATask;
			taskDefinition.IsAcumaticaSet = vm.IsAcumaticaTask;

			return taskDefinition;
		}

		private void UpdateDocuments(TaskDefinitionsEditViewModel vm)
		{
			var preExistingDocumentIds = _dbSubmission.trTaskDefDocs.Where(x => x.TaskDefinitionId == vm.Id).Select(x => x.Id).ToList();

			// Add
			var documentsToAdd = vm.Documents.Where(x => x.Id == null && !x._destroy).ToList();
			foreach (var item in documentsToAdd)
			{
				var addDocument = new trTaskDefDoc { TaskDefinitionId = vm.Id, TaskDocumentId = item.TaskDocumentId, Note = item.Note };
				_dbSubmission.trTaskDefDocs.Add(addDocument);
			}

			if (vm.Mode == Mode.Edit)
			{
				// Delete
				var documentIdsToDelete = vm.Documents.Where(x => x._destroy).Select(x => x.Id).Cast<int>().ToList();
				if (documentIdsToDelete.Count > 0)
				{
					_dbSubmission.trTaskDefDocs.Where(x => documentIdsToDelete.Contains(x.Id)).Delete();
				}

				// Edit
				var documentIdsToUpdate = preExistingDocumentIds.Except(documentIdsToDelete).ToList();

				foreach (var id in documentIdsToUpdate)
				{
					var documentToEdit = vm.Documents.Where(x => x.Id == id).Single();

					var editDocument = new trTaskDefDoc { Id = id, TaskDocumentId = documentToEdit.TaskDocumentId, Note = documentToEdit.Note };
					_dbSubmission.trTaskDefDocs.Attach(editDocument);
					_dbSubmission.Entry(editDocument).Property(x => x.TaskDocumentId).IsModified = true;
					_dbSubmission.Entry(editDocument).Property(x => x.Note).IsModified = true;
				}
			}

			_dbSubmission.SaveChanges();
		}
		#endregion
	}
}