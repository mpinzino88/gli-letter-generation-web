﻿extern alias ZEFCore;
using GLI.EFCore.Submission;
using Submissions.Common;
using Submissions.Web.Areas.Lookups.Models;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using ZEFCore::Z.EntityFramework.Plus;

namespace Submissions.Web.Areas.Lookups.Controllers
{
	public class LetterbookDefaultingOptionsController : Controller
	{
		private readonly SubmissionContext _dbSubmission;

		public LetterbookDefaultingOptionsController(SubmissionContext submission)
		{
			_dbSubmission = submission;
		}

		public ActionResult Index()
		{
			//var optionsList = _dbSubmission.LetterbookDefaultingOptions.ToList();
			var vm = new LBDefaultingOptionsIndexViewModel();

			return View(vm);
		}

		public ActionResult Edit(int? id)
		{
			var vm = new LBDefaultingOptionsEditViewModel();
			vm.Mode = Mode.Add.ToString();

			if (id != null && id != 0)
			{
				var existingOptions = _dbSubmission.LetterbookDefaultingOptions.Where(x => x.Id == id).SingleOrDefault();
				vm.Id = existingOptions.Id;

				vm.LetterheadId = existingOptions.LetterheadId;
				vm.LetterheadName = (existingOptions.LetterheadId != null) ? existingOptions.Letterhead.Code : "";
				vm.CorrectionLetterheadId = existingOptions.CorrectionLetterheadId;
				vm.CorrectionLetterheadName = (existingOptions.CorrectionLetterheadId != null) ? existingOptions.CorrectionLetterhead.Code : "";
				vm.RepeatLetterhead = existingOptions.RepeatLetterhead;
				vm.RepeatCorrectionLetterhead = existingOptions.RepeatCorrectionLetterhead;
				vm.ApprovalStatus = existingOptions.ApprovalStatus;

				vm.Billable = existingOptions.Billable;
				vm.BillingLabId = existingOptions.BillingLabId;
				vm.BillingLab = (existingOptions.BillingLabId != null) ? existingOptions.BillingLab.Name : "";
				vm.BillingNote = existingOptions.BillingNote;

				vm.BillingSheetId = existingOptions.BillingSheetId;   //_dbSubmission.tbl_lst_BillingSheets.Where(x => x.Name == existingOptions.BillingSheet).Select(x => x.Id).SingleOrDefault();
				vm.BillingSheet = (existingOptions.BillingSheetId != null && existingOptions.BillingSheetId != 0) ? existingOptions.DefaultBillingSheet.Name : "";

				vm.AvailableBillingSheetIds = existingOptions.AvailableBillingSheets.Select(x => x.BillingSheetId).ToList();
				vm.AvailableBillingSheets = existingOptions.AvailableBillingSheets.Select(x => new SelectListItem() { Value = x.BillingSheet.Id.ToString(), Text = x.BillingSheet.Name, Selected = true }).ToList();

				vm.ManufacturerCodes = existingOptions.Manufacturers.Select(x => x.ManufacturerCode).ToList();
				vm.Manufacturers = existingOptions.Manufacturers.Select(x => new SelectListItem() { Value = x.ManufacturerCode.ToString(), Text = x.Manufacturer.Description, Selected = true }).ToList();

				vm.JurisdictionIds = existingOptions.Jurisdictions.Select(x => x.JurisdictionId).ToList();
				vm.Jurisdictions = existingOptions.Jurisdictions.Select(x => new SelectListItem() { Value = x.JurisdictionId.ToString(), Text = x.Jurisdiction.Name, Selected = true }).ToList();

				vm.SubmissionTypeIds = existingOptions.SubTypes.Select(x => x.SubmissionTypeId).ToList();
				vm.SubmissionTypes = existingOptions.SubTypes.Select(x => new SelectListItem() { Value = x.SubmissionType.Id.ToString(), Text = x.SubmissionType.Code + " - " + x.SubmissionType.Description, Selected = true }).ToList();

				vm.DocumentTypeIds = existingOptions.DocumentTypes.Select(x => x.DocumentTypeId).ToList();
				vm.DocumentTypes = existingOptions.DocumentTypes.Select(x => new SelectListItem() { Value = x.DocumentTypeId.ToString(), Text = x.DocumentType.Description, Selected = true }).ToList();

				vm.ContractTypeIds = existingOptions.ContractTypes.Select(x => x.ContractTypeId).ToList();
				vm.ContractTypes = existingOptions.ContractTypes.Select(x => new SelectListItem() { Value = x.ContractTypeId.ToString(), Text = x.ContractType.Code + " - " + x.ContractType.Description, Selected = true }).ToList();

				vm.BillingPartyIds = existingOptions.BillingParties.Select(x => x.BillingPartyId).ToList();
				vm.BillingParties = existingOptions.BillingParties.Select(x => new SelectListItem() { Value = x.BillingPartyId.ToString(), Text = x.BillingParty.BillingParty, Selected = true }).ToList();

				vm.LocationIds = existingOptions.Locations.Select(x => x.LocationId).ToList();
				vm.Locations = existingOptions.Locations.Select(x => new SelectListItem() { Value = x.LocationId.ToString(), Text = x.Location.Location + " - " + x.Location.Name, Selected = true }).ToList();

				vm.CompanyIds = existingOptions.Companies.Select(x => x.CompanyId).ToList();
				vm.Companies = existingOptions.Companies.Select(x => new SelectListItem() { Value = x.CompanyId.ToString(), Text = x.Company.Name, Selected = true }).ToList();

				vm.Mode = Mode.Edit.ToString();
			}

			return View(vm);
		}

		[HttpPost]
		public ActionResult CheckIfExistsDefaults(LBDefaultingOptionsEditViewModel vm)
		{
			//Debug query
			//var all = _dbSubmission.LetterbookDefaultingOptions.Where(
			//	x => x.Id != vm.Id
			//).Select(x => new
			//{
			//	Manufs = x.Manufacturers.Select(y => y.ManufacturerCode).ToList(),
			//	jurs = x.Jurisdictions.Select(y => y.JurisdictionId).ToList(),
			//	docTypes = x.DocumentTypes.Select(y => y.DocumentTypeId).ToList(),
			//	subTypes = x.SubTypes.Select(y => y.SubmissionTypeId).ToList(),
			//	isManufSame = (x.Manufacturers.Select(y => y.ManufacturerCode).Intersect(vm.ManufacturerCodes).Any() || (x.Manufacturers.Select(y => y.ManufacturerCode).ToList().Count == 0 && vm.ManufacturerCodes.Count == 0)),
			//	isJurSame = (x.Jurisdictions.Select(y => y.JurisdictionId).Intersect(vm.JurisdictionIds).Any() || (x.Jurisdictions.Select(y => y.JurisdictionId).ToList().Count == 0 && vm.JurisdictionIds.Count == 0)),
			//	isDocSame = (x.DocumentTypes.Select(y => y.DocumentTypeId).Intersect(vm.DocumentTypeIds).Any() || (x.DocumentTypes.Select(y => y.DocumentTypeId).ToList().Count == 0 && vm.DocumentTypeIds.Count == 0)),
			//	isSubTypeSame = (x.SubTypes.Select(y => y.SubmissionTypeId).Intersect(vm.SubmissionTypeIds).Any() || (x.SubTypes.Select(y => y.SubmissionTypeId).ToList().Count == 0 && vm.SubmissionTypeIds.Count == 0))
			//}
			//).ToList();

			var exists = _dbSubmission.LetterbookDefaultingOptions.Any(
				x => x.Id != vm.Id &&
				(x.Manufacturers.Any(y => vm.ManufacturerCodes.Contains(y.ManufacturerCode)) || (!x.Manufacturers.Any() && !vm.ManufacturerCodes.Any())) &&
				(x.Jurisdictions.Any(y => vm.JurisdictionIds.Contains(y.JurisdictionId)) || (!x.Jurisdictions.Any() && !vm.JurisdictionIds.Any())) &&
				(x.DocumentTypes.Any(y => vm.DocumentTypeIds.Contains(y.DocumentTypeId)) || (!x.DocumentTypes.Any() && !vm.DocumentTypeIds.Any())) &&
				(x.SubTypes.Any(y => vm.SubmissionTypeIds.Contains(y.SubmissionTypeId)) || (!x.SubTypes.Any() && !vm.SubmissionTypeIds.Any())) &&
				(x.ContractTypes.Any(y => vm.ContractTypeIds.Contains(y.ContractTypeId)) || (!x.ContractTypes.Any() && !vm.ContractTypeIds.Any())) &&
				(x.Locations.Any(y => vm.LocationIds.Contains(y.LocationId)) || (!x.Locations.Any() && !vm.LocationIds.Any())) &&
				(x.Companies.Any(y => vm.CompanyIds.Contains(y.CompanyId)) || (!x.Companies.Any() && !vm.CompanyIds.Any())) &&
				(x.BillingParties.Any(y => vm.BillingPartyIds.Contains(y.BillingPartyId)) || (!x.BillingParties.Any() && !vm.BillingPartyIds.Any()))
				);

			if (exists)
			{
				return Content("NOOK");
			}
			else
			{
				return Content("OK");
			}

		}

		[HttpPost]
		public ActionResult Edit(LBDefaultingOptionsEditViewModel vm)
		{

			LetterbookDefaultingOption options;
			if (vm.Id == 0)
			{
				options = new LetterbookDefaultingOption();
			}
			else
			{
				options = _dbSubmission.LetterbookDefaultingOptions.Find(vm.Id);
			}

			options.ApprovalStatus = vm.ApprovalStatus;
			options.Billable = vm.Billable;
			options.CorrectionLetterheadId = vm.CorrectionLetterheadId;
			options.LetterheadId = vm.LetterheadId;
			options.RepeatCorrectionLetterhead = vm.RepeatCorrectionLetterhead;
			options.RepeatLetterhead = vm.RepeatLetterhead;
			//options.BillingSheet = vm.BillingSheet; 
			options.BillingSheetId = vm.BillingSheetId;
			options.BillingLabId = vm.BillingLabId;
			options.BillingNote = vm.BillingNote;

			if (options.Id == 0)
			{
				_dbSubmission.LetterbookDefaultingOptions.Add(options);
			}
			_dbSubmission.SaveChanges();

			//Save all Association
			AddJurisdictionAssociation(options.Id, vm.Mode, vm.JurisdictionIds);
			AddManufacturerAssociation(options.Id, vm.Mode, vm.ManufacturerCodes);
			AddSubTypeAssociation(options.Id, vm.Mode, vm.SubmissionTypeIds);
			AddDocumentTypeAssociation(options.Id, vm.Mode, vm.DocumentTypeIds);
			AddContractTypeAssociation(options.Id, vm.Mode, vm.ContractTypeIds);
			AddBillingPartyAssociation(options.Id, vm.Mode, vm.BillingPartyIds);
			AddAvailBillingSheetOptionsAssociation(options.Id, vm.Mode, vm.AvailableBillingSheetIds);
			AddLocationAssociation(options.Id, vm.Mode, vm.LocationIds);
			AddCompanyAssociation(options.Id, vm.Mode, vm.CompanyIds);

			return RedirectToAction("Index");
		}

		public ActionResult Delete(int id)
		{
			_dbSubmission.LetterbookDefaultingOptionsJur
					.Where(x => x.LetterbookDefaultingOptionId == id)
					.Delete();

			_dbSubmission.LetterbookDefaultingOptionsManu
					.Where(x => x.LetterbookDefaultingOptionId == id)
					.Delete();

			_dbSubmission.LetterbookDefaultingOptionsSubType
					.Where(x => x.LetterbookDefaultingOptionId == id)
					.Delete();

			_dbSubmission.LetterbookDefaultingOptionsDocumentType
					.Where(x => x.LetterbookDefaultingOptionId == id)
					.Delete();

			_dbSubmission.LetterbookDefaultingOptionsContractType
					.Where(x => x.LetterbookDefaultingOptionId == id)
					.Delete();

			_dbSubmission.LetterbookDefaultingOptionsBillingParty
					.Where(x => x.LetterbookDefaultingOptionId == id)
					.Delete();

			_dbSubmission.LetterbookDefaultingOptionsAvailBillingSheets
				.Where(x => x.LetterbookDefaultingOptionId == id)
				.Delete();

			_dbSubmission.LetterbookDefaultingOptionsLocations
				.Where(x => x.LetterbookDefaultingOptionId == id)
				.Delete();

			_dbSubmission.LetterbookDefaultingOptionsCompany
				.Where(x => x.LetterbookDefaultingOptionId == id)
				.Delete();

			_dbSubmission.LetterbookDefaultingOptions.Where(opt => opt.Id == id).Delete();
			_dbSubmission.SaveChanges();

			return RedirectToAction("Index");
		}

		private void AddCompanyAssociation(int defaultingOptionId, string mode, List<int> companyIds)
		{
			if (mode == Mode.Edit.ToString())
			{
				_dbSubmission.LetterbookDefaultingOptionsCompany
					.Where(x => x.LetterbookDefaultingOptionId == defaultingOptionId)
					.Delete();
			}

			if (companyIds.Count > 0)
			{
				var newCompanies = companyIds
										.Select(x => new LetterbookDefaultingOptionsCompany
										{
											LetterbookDefaultingOptionId = defaultingOptionId,
											CompanyId = x
										}).ToList();

				_dbSubmission.LetterbookDefaultingOptionsCompany.AddRange(newCompanies);
				_dbSubmission.SaveChanges();
			}
		}

		private void AddLocationAssociation(int defaultingOptionId, string mode, List<int> locationIds)
		{
			if (mode == Mode.Edit.ToString())
			{
				_dbSubmission.LetterbookDefaultingOptionsLocations
					.Where(x => x.LetterbookDefaultingOptionId == defaultingOptionId)
					.Delete();
			}

			if (locationIds.Count > 0)
			{
				var newLocations = locationIds
										.Select(x => new LetterbookDefaultingOptionsLocations
										{
											LetterbookDefaultingOptionId = defaultingOptionId,
											LocationId = x
										}).ToList();

				_dbSubmission.LetterbookDefaultingOptionsLocations.AddRange(newLocations);
				_dbSubmission.SaveChanges();
			}
		}


		private void AddJurisdictionAssociation(int defaultingOptionId, string mode, List<string> jurisdictions)
		{
			if (mode == Mode.Edit.ToString())
			{
				_dbSubmission.LetterbookDefaultingOptionsJur
					.Where(x => x.LetterbookDefaultingOptionId == defaultingOptionId)
					.Delete();
			}

			if (jurisdictions.Count > 0)
			{
				var newJurisdictions = jurisdictions
										.Select(x => new LetterbookDefaultingOptionsJur
										{
											LetterbookDefaultingOptionId = defaultingOptionId,
											JurisdictionId = x.ToString()
										}).ToList();

				_dbSubmission.LetterbookDefaultingOptionsJur.AddRange(newJurisdictions);
				_dbSubmission.SaveChanges();
			}
		}

		private void AddManufacturerAssociation(int defaultingOptionId, string mode, List<string> manufacturers)
		{
			if (mode == Mode.Edit.ToString())
			{
				_dbSubmission.LetterbookDefaultingOptionsManu
					.Where(x => x.LetterbookDefaultingOptionId == defaultingOptionId)
					.Delete();
			}

			if (manufacturers.Count > 0)
			{
				var newManufs = manufacturers
								.Select(x => new LetterbookDefaultingOptionsManu
								{
									LetterbookDefaultingOptionId = defaultingOptionId,
									ManufacturerCode = x.ToString()
								}).ToList();

				_dbSubmission.LetterbookDefaultingOptionsManu.AddRange(newManufs);
				_dbSubmission.SaveChanges();
			}
		}

		private void AddSubTypeAssociation(int defaultingOptionId, string mode, List<int> subTypeIds)
		{
			if (mode == Mode.Edit.ToString())
			{
				_dbSubmission.LetterbookDefaultingOptionsSubType
					.Where(x => x.LetterbookDefaultingOptionId == defaultingOptionId)
					.Delete();
			}

			if (subTypeIds.Count > 0)
			{
				var newSubTypes = subTypeIds
								.Select(x => new LetterbookDefaultingOptionsSubType
								{
									LetterbookDefaultingOptionId = defaultingOptionId,
									SubmissionTypeId = x
								}).ToList();

				_dbSubmission.LetterbookDefaultingOptionsSubType.AddRange(newSubTypes);
				_dbSubmission.SaveChanges();
			}
		}

		private void AddDocumentTypeAssociation(int defaultingOptionId, string mode, List<int> documentTypeIds)
		{
			if (mode == Mode.Edit.ToString())
			{
				_dbSubmission.LetterbookDefaultingOptionsDocumentType
					.Where(x => x.LetterbookDefaultingOptionId == defaultingOptionId)
					.Delete();
			}

			if (documentTypeIds.Count > 0)
			{
				var newDocTypes = documentTypeIds
								.Select(x => new LetterbookDefaultingOptionsDocumentType
								{
									LetterbookDefaultingOptionId = defaultingOptionId,
									DocumentTypeId = x
								}).ToList();

				_dbSubmission.LetterbookDefaultingOptionsDocumentType.AddRange(newDocTypes);
				_dbSubmission.SaveChanges();
			}
		}


		private void AddContractTypeAssociation(int defaultingOptionId, string mode, List<int> contractTypeIds)
		{
			if (mode == Mode.Edit.ToString())
			{
				_dbSubmission.LetterbookDefaultingOptionsContractType
					.Where(x => x.LetterbookDefaultingOptionId == defaultingOptionId)
					.Delete();
			}

			if (contractTypeIds.Count > 0)
			{
				var newDocTypes = contractTypeIds
								.Select(x => new LetterbookDefaultingOptionsContractType
								{
									LetterbookDefaultingOptionId = defaultingOptionId,
									ContractTypeId = x
								}).ToList();

				_dbSubmission.LetterbookDefaultingOptionsContractType.AddRange(newDocTypes);
				_dbSubmission.SaveChanges();
			}
		}

		private void AddBillingPartyAssociation(int defaultingOptionId, string mode, List<int> billingParties)
		{
			if (mode == Mode.Edit.ToString())
			{
				_dbSubmission.LetterbookDefaultingOptionsBillingParty
					.Where(x => x.LetterbookDefaultingOptionId == defaultingOptionId)
					.Delete();
			}

			if (billingParties.Count > 0)
			{
				var newDocTypes = billingParties
								.Select(x => new LetterbookDefaultingOptionsBillingParty
								{
									LetterbookDefaultingOptionId = defaultingOptionId,
									BillingPartyId = x
								}).ToList();

				_dbSubmission.LetterbookDefaultingOptionsBillingParty.AddRange(newDocTypes);
				_dbSubmission.SaveChanges();
			}
		}

		private void AddAvailBillingSheetOptionsAssociation(int defaultingOptionId, string mode, List<int> availBillingSheets)
		{
			if (mode == Mode.Edit.ToString())
			{
				_dbSubmission.LetterbookDefaultingOptionsAvailBillingSheets
					.Where(x => x.LetterbookDefaultingOptionId == defaultingOptionId)
					.Delete();
			}

			if (availBillingSheets.Count > 0)
			{
				var newDocTypes = availBillingSheets
								.Select(x => new LetterbookDefaultingOptionsAvailBillingSheets
								{
									LetterbookDefaultingOptionId = defaultingOptionId,
									BillingSheetId = x
								}).ToList();

				_dbSubmission.LetterbookDefaultingOptionsAvailBillingSheets.AddRange(newDocTypes);
				_dbSubmission.SaveChanges();
			}
		}

		[HttpPost]
		public JsonResult Clone(List<int> jurIds, int sourceId)
		{
			var sourceModel = _dbSubmission.LetterbookDefaultingOptions.Find(sourceId);

			foreach (var id in jurIds)
			{
				_dbSubmission.LetterbookDefaultingOptions.Add(new LetterbookDefaultingOption
				{
					Billable = sourceModel.Billable,
					//JurisdictionId = id.ToJurisdictionIdString(),
					LetterheadId = sourceModel.LetterheadId,
					RepeatLetterhead = sourceModel.RepeatLetterhead,
					CorrectionLetterheadId = sourceModel.CorrectionLetterheadId,
					ApprovalStatus = sourceModel.ApprovalStatus,
					RepeatCorrectionLetterhead = sourceModel.RepeatCorrectionLetterhead
				});
			}

			_dbSubmission.SaveChanges();
			return Json(new { Msg = "Successfully copied for selected jurisdictions" }, JsonRequestBehavior.AllowGet);
		}
	}
}