﻿extern alias ZEFCore;
using GLI.EFCore.Submission;
using Submissions.Common;
using Submissions.Web.Areas.Lookups.Models;
using System.Linq;
using System.Web.Mvc;
using ZEFCore::Z.EntityFramework.Plus;

namespace Submissions.Web.Areas.Lookups.Controllers
{
	public class AppFeesController : Controller
	{
		private readonly SubmissionContext _dbSubmission;
		public AppFeesController(SubmissionContext submission)
		{
			_dbSubmission = submission;
		}


		// GET: Lookups/AppFees
		public ActionResult Index()
		{
			var vm = new AppFeesIndexViewModel();
			return View(vm);
		}

		public ActionResult Edit(int? id)
		{
			var vm = new AppFeesEditViewModel();
			vm.Mode = Mode.Add.ToString();
			vm.Active = true;

			if (id != null && id != 0)
			{
				var existingOptions = _dbSubmission.tbl_lst_AppFees.Where(x => x.Id == id).SingleOrDefault();
				vm.Id = existingOptions.Id;
				vm.Name = existingOptions.Name;
				vm.Description = existingOptions.Description;
				vm.Amount = existingOptions.Amount;
				vm.Active = existingOptions.Active;

				vm.Mode = Mode.Edit.ToString();
			}
			return View(vm);
		}

		[HttpPost]
		public ActionResult Edit(AppFeesEditViewModel vm)
		{
			tbl_lst_AppFees appFee;
			if (vm.Id == 0)
			{
				appFee = new tbl_lst_AppFees();
			}
			else
			{
				appFee = _dbSubmission.tbl_lst_AppFees.Find(vm.Id);
			}

			appFee.Name = vm.Name;
			appFee.Description = vm.Description;
			appFee.Amount = vm.Amount;
			appFee.Active = vm.Active;

			if (appFee.Id == 0)
			{
				_dbSubmission.tbl_lst_AppFees.Add(appFee);
			}
			_dbSubmission.SaveChanges();
			return RedirectToAction("Index");
		}

		[HttpPost]
		public ActionResult CheckIfExists(AppFeesEditViewModel vm)
		{
			var exists = _dbSubmission.tbl_lst_AppFees.Any(
				x => x.Id != vm.Id && x.Name == vm.Name);

			if (exists)
			{
				return Content("NOOK");
			}
			else
			{
				return Content("OK");
			}
		}

		public ContentResult Delete(int id)
		{
			var anyBillingSheetExists = _dbSubmission.tbl_lst_BillingSheets.Where(x => x.AppFeeId == id || x.BilledItem1AppFeeId == id || x.BilledItem2AppFeeId == id || x.BilledItem3AppFeeId == id).Any();
			if (!anyBillingSheetExists)
			{
				_dbSubmission.tbl_lst_AppFees.Where(opt => opt.Id == id).Delete();
				_dbSubmission.SaveChanges();

				return Content("OK");
			}
			else
			{
				return Content("NOOK");
			}
		}

	}
}