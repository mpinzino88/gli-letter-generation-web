﻿extern alias ZEFCore;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using GLI.EFCore.Submission;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Submissions.Common;
using Submissions.Web.Areas.Lookups.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using ZEFCore::Z.EntityFramework.Plus;

namespace Submissions.Web.Areas.Lookups.Controllers
{
	[AuthorizeUser(Permission = Permission.Admin, HasAccessRight = AccessRight.Edit)]
	public class EmailDistributionsController : Controller, ILookupDelete<int>
	{
		private readonly SubmissionContext _dbSubmission;
		private readonly IUserContext _userContext;
		private readonly int[] _emailManagementDistributions = new int[] { 6, 7, 8, 9, 10, 11 };

		public EmailDistributionsController(
			SubmissionContext dbSubmission,
			IUserContext userContext)
		{
			_dbSubmission = dbSubmission;
			_userContext = userContext;
		}

		public ActionResult Index()
		{
			var emailDistributions = _dbSubmission.EmailDistributions
				.Where(x => !_emailManagementDistributions.Contains(x.Id))
				.AsNoTracking()
				.OrderBy(x => x.Code)
				.ProjectTo<EmailDistributionsIndexViewModel>();

			return View(emailDistributions);
		}

		public ActionResult Edit(int? id, Mode mode)
		{
			var vm = new EmailDistributionsEditViewModel();

			if (mode == Mode.Edit)
			{
				var emailDistribution = _dbSubmission.EmailDistributions.Find(id);
				var emailDistributionInfo = Mapper.Map<EmailDistributionInfo>(emailDistribution);
				vm.EmailDistributionInfo = emailDistributionInfo;

				var users = LoadUsers((int)id);
				vm.EmailDistributionInfo.UserIds = users.Select(x => x.Value).ToArray();
				vm.EmailDistributionInfo.Users = users;

				vm.EmailDistributionInfo.ManufacturerUsers = LoadManufacturerUsers((int)id);
				vm.EmailDistributionInfo.ManufacturerGroupUsers = LoadManufacturerGroupUsers((int)id);
				vm.EmailDistributionInfo.JurisdictionUsers = LoadJurisdictionUsers((int)id);
			}
			vm.Mode = mode;

			return View(vm);
		}

		[HttpPost]
		public ActionResult Edit(string editViewModel)
		{
			// update general
			var vm = JsonConvert.DeserializeObject<EmailDistributionsEditViewModel>(editViewModel);
			var emailDistribution = new EmailDistribution();

			if (vm.Mode == Mode.Edit)
			{
				emailDistribution = _dbSubmission.EmailDistributions.Find(vm.EmailDistributionInfo.Id);
			}

			emailDistribution = ApplyChanges(emailDistribution, vm);

			if (vm.Mode == Mode.Add)
			{
				_dbSubmission.EmailDistributions.Add(emailDistribution);
			}

			_dbSubmission.UserId = _userContext.User.Id;
			_dbSubmission.SaveChanges();
			vm.EmailDistributionInfo.Id = emailDistribution.Id;

			// update notifications
			_dbSubmission.RemoveRange(_dbSubmission.EmailDistributionUsers.Where(x => x.EmailDistributionId == vm.EmailDistributionInfo.Id));
			UpdateUsers(vm);
			UpdateManufacturerUsers(vm);
			UpdateManufacturerGroupUsers(vm);
			UpdateJurisdictionUsers(vm);

			CacheManager.Clear(LookupAjax.EmailDistributions.ToString());

			return RedirectToAction("Index");
		}

		[HttpPost]
		public JsonResult CheckDelete(int id)
		{
			var isInUse = _dbSubmission.EmailSubscriptions.Any(x => x.EmailDistributionId == id);

			return Json(isInUse);
		}

		[HttpPost]
		public ActionResult Delete(int id)
		{
			_dbSubmission.UserId = _userContext.User.Id;
			_dbSubmission.RemoveRange(_dbSubmission.EmailDistributionUsers.Where(x => x.EmailDistributionId == id));
			_dbSubmission.RemoveRange(_dbSubmission.EmailDistributions.Where(x => x.Id == id));

			CacheManager.Clear(LookupAjax.EmailDistributions.ToString());

			return RedirectToAction("Index");
		}

		[HttpPost]
		public ActionResult SetActive(int id, Mode mode)
		{
			throw new NotImplementedException();
		}

		public JsonResult ValidateCode([Bind(Prefix = "EmailDistributionInfo.Id")] int? id, [Bind(Prefix = "EmailDistributionInfo.Code")] string code)
		{
			var validates = true;

			if (!string.IsNullOrEmpty(code) && _dbSubmission.EmailDistributions.Any(x => x.Id != id && x.Code == code))
			{
				validates = false;
			}

			return Json(validates, JsonRequestBehavior.AllowGet);
		}

		#region Helper Methods
		private EmailDistribution ApplyChanges(EmailDistribution emailDistribution, EmailDistributionsEditViewModel vm)
		{
			emailDistribution.Id = vm.EmailDistributionInfo.Id ?? 0;
			emailDistribution.Code = vm.EmailDistributionInfo.Code;
			emailDistribution.Description = vm.EmailDistributionInfo.Description;

			return emailDistribution;
		}

		private List<SelectListItem> LoadUsers(int id)
		{
			return _dbSubmission.EmailDistributionUsers.Include(x => x.User)
				.Where(x => x.EmailDistributionId == id && x.ManufacturerCode == null && x.JurisdictionId == null && x.ManufacturerGroupId == null)
				.Select(x => new SelectListItem { Value = x.User.Id.ToString(), Text = x.User.FName + " " + x.User.LName })
				.OrderBy(x => x.Text)
				.ToList();
		}

		private IList<ManufacturerFilter> LoadManufacturerUsers(int id)
		{
			var filters = new List<ManufacturerFilter>();

			var manufacturerUsers = _dbSubmission.EmailDistributionUsers
				.Include(x => x.Manufacturer)
				.Include(x => x.User)
				.Where(x => x.EmailDistributionId == id && x.ManufacturerCode != null && x.UserId != null)
				.AsEnumerable()
				.GroupBy(x => x.ManufacturerCode)
				.Select(x => new
				{
					ManufacturerCode = x.Key,
					Manufacturer = x.Max(y => y.Manufacturer.Description),
					Users = x.Select(y => new { UserId = y.UserId, FName = y.User.FName, LName = y.User.LName }).ToList()
				})
				.OrderBy(x => x.ManufacturerCode)
				.ToList();

			foreach (var item in manufacturerUsers)
			{
				var filter = new ManufacturerFilter
				{
					ManufacturerCode = item.ManufacturerCode,
					Manufacturer = new SelectListItem { Value = item.ManufacturerCode, Text = item.Manufacturer },
					UserIds = item.Users.Select(x => x.UserId.ToString()).ToArray(),
					Users = item.Users.Select(x => new SelectListItem { Value = x.UserId.ToString(), Text = x.FName + " " + x.LName }).OrderBy(x => x.Text).ToList()
				};

				filters.Add(filter);
			}

			return filters;
		}

		private IList<ManufacturerGroupFilter> LoadManufacturerGroupUsers(int id)
		{
			var filters = new List<ManufacturerGroupFilter>();

			var manufacturerGroupUsers = _dbSubmission.EmailDistributionUsers
				.Include(x => x.ManufacturerGroup.ManufacturerGroupCategory)
				.Include(x => x.User)
				.Where(x => x.EmailDistributionId == id && x.ManufacturerGroupId != null && x.UserId != null)
				.AsEnumerable()
				.GroupBy(x => x.ManufacturerGroupId)
				.Select(x => new
				{
					ManufacturerGroupId = x.Key,
					ManufacturerGroup = x.Max(y => y.ManufacturerGroup.Description + ": " + y.ManufacturerGroup.ManufacturerGroupCategory.Code),
					Users = x.Select(y => new { UserId = y.UserId, FName = y.User.FName, LName = y.User.LName }).ToList()
				})
				.OrderBy(x => x.ManufacturerGroup)
				.ToList();

			foreach (var item in manufacturerGroupUsers)
			{
				var filter = new ManufacturerGroupFilter
				{
					ManufacturerGroupId = (int)item.ManufacturerGroupId,
					ManufacturerGroup = new SelectListItem { Value = item.ManufacturerGroupId.ToString(), Text = item.ManufacturerGroup },
					UserIds = item.Users.Select(x => x.UserId.ToString()).ToArray(),
					Users = item.Users.Select(x => new SelectListItem { Value = x.UserId.ToString(), Text = x.FName + " " + x.LName }).OrderBy(x => x.Text).ToList()
				};

				filters.Add(filter);
			}

			return filters;
		}

		private IList<JurisdictionFilter> LoadJurisdictionUsers(int id)
		{
			var filters = new List<JurisdictionFilter>();

			var jurisdictionUsers = _dbSubmission.EmailDistributionUsers
				.Where(x => x.EmailDistributionId == id && x.JurisdictionId != null && x.UserId != null)
				.AsEnumerable()
				.GroupBy(x => x.JurisdictionId)
				.Select(x => new
				{
					JurisdictionId = x.Key,
					Jurisdiction = x.Max(y => y.Jurisdiction.Name),
					Users = x.Select(y => new { UserId = y.UserId, FName = y.User?.FName, LName = y.User?.LName }).ToList()
				})
				.OrderBy(x => x.Jurisdiction)
				.ToList();

			foreach (var item in jurisdictionUsers)
			{
				var filter = new JurisdictionFilter
				{
					JurisdictionId = item.JurisdictionId,
					Jurisdiction = new SelectListItem { Value = item.JurisdictionId, Text = item.Jurisdiction },
					UserIds = item.Users.Select(x => x.UserId.ToString()).ToArray(),
					Users = item.Users.Select(x => new SelectListItem { Value = x.UserId.ToString(), Text = x.FName + " " + x.LName }).OrderBy(x => x.Text).ToList()
				};

				filters.Add(filter);
			}

			return filters;
		}

		private void UpdateUsers(EmailDistributionsEditViewModel vm)
		{
			var userIdsToAdd = vm.EmailDistributionInfo.UserIds;

			if (userIdsToAdd != null)
			{
				foreach (var userId in userIdsToAdd)
				{
					var emailDistributionUser = new EmailDistributionUser { EmailDistributionId = (int)vm.EmailDistributionInfo.Id, UserId = Convert.ToInt32(userId) };
					_dbSubmission.EmailDistributionUsers.Add(emailDistributionUser);
				}
				_dbSubmission.UserId = _userContext.User.Id;
				_dbSubmission.SaveChanges();
			}
		}

		private void UpdateManufacturerUsers(EmailDistributionsEditViewModel vm)
		{
			var emailDistributionUsersToAdd = vm.EmailDistributionInfo.ManufacturerUsers.Where(x => !x._destroy).ToList();

			if (emailDistributionUsersToAdd.Count > 0)
			{
				foreach (var item in emailDistributionUsersToAdd)
				{
					foreach (var userId in item.UserIds)
					{
						var emailDistributionUser = new EmailDistributionUser { EmailDistributionId = (int)vm.EmailDistributionInfo.Id, ManufacturerCode = item.ManufacturerCode, UserId = Convert.ToInt32(userId) };
						_dbSubmission.EmailDistributionUsers.Add(emailDistributionUser);
					}
				}
				_dbSubmission.UserId = _userContext.User.Id;
				_dbSubmission.SaveChanges();
			}
		}

		private void UpdateManufacturerGroupUsers(EmailDistributionsEditViewModel vm)
		{
			var emailDistributionUsersToAdd = vm.EmailDistributionInfo.ManufacturerGroupUsers.Where(x => !x._destroy).ToList();

			if (emailDistributionUsersToAdd.Count > 0)
			{
				foreach (var item in emailDistributionUsersToAdd)
				{
					foreach (var userId in item.UserIds)
					{
						var emailDistributionUser = new EmailDistributionUser { EmailDistributionId = (int)vm.EmailDistributionInfo.Id, ManufacturerGroupId = item.ManufacturerGroupId, UserId = Convert.ToInt32(userId) };
						_dbSubmission.EmailDistributionUsers.Add(emailDistributionUser);
					}
				}
				_dbSubmission.UserId = _userContext.User.Id;
				_dbSubmission.SaveChanges();
			}
		}

		private void UpdateJurisdictionUsers(EmailDistributionsEditViewModel vm)
		{
			var emailDistributionUsersToAdd = vm.EmailDistributionInfo.JurisdictionUsers.Where(x => !x._destroy).ToList();

			if (emailDistributionUsersToAdd.Count > 0)
			{
				foreach (var item in emailDistributionUsersToAdd)
				{
					foreach (var userId in item.UserIds)
					{
						var emailDistributionUser = new EmailDistributionUser { EmailDistributionId = (int)vm.EmailDistributionInfo.Id, JurisdictionId = item.JurisdictionId, UserId = Convert.ToInt32(userId) };
						_dbSubmission.EmailDistributionUsers.Add(emailDistributionUser);
					}
				}

				_dbSubmission.UserId = _userContext.User.Id;
				_dbSubmission.SaveChanges();
			}
		}
		#endregion
	}
}