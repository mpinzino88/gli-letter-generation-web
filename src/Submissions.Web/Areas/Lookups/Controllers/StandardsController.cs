﻿extern alias ZEFCore;
using AutoMapper.QueryableExtensions;
using GLI.EFCore.Submission;
using Microsoft.EntityFrameworkCore;
using Submissions.Common;
using Submissions.Web.Areas.Lookups.Models;
using System;
using System.Linq;
using System.Web.Mvc;
using ZEFCore::Z.EntityFramework.Plus;

namespace Submissions.Web.Areas.Lookups.Controllers
{
	[AuthorizeUser(Permission = Permission.Admin, HasAccessRight = AccessRight.Edit)]
	public class StandardsController : Controller
	{
		private readonly SubmissionContext _dbSubmission;

		public StandardsController(SubmissionContext dbSubmission)
		{
			_dbSubmission = dbSubmission;
		}

		public ActionResult Index()
		{
			var vm = new StandardsIndexViewModel();
			vm.Standards = _dbSubmission.tbl_lst_StandardsAll
				.AsNoTracking()
				.ProjectTo<StandardViewModel>()
				.ToList()
				.OrderByDescending(x => x.Active)
				.ThenBy(x => Convert.ToInt32(x.JurisdictionId))
				.ThenBy(x => x.Standard)
				.ToList();

			return View(vm);
		}

		public ActionResult Edit(int? id, Mode mode)
		{
			var vm = new StandardsEditViewModel();

			if (mode == Mode.Add)
			{
				vm.Active = true;
				vm.AddByDefault = true;
			}
			else if (mode == Mode.Edit)
			{
				vm = _dbSubmission.tbl_lst_StandardsAll
					.Where(x => x.Id == id)
					.ProjectTo<StandardsEditViewModel>()
					.Single();
			}

			vm.Mode = mode;

			return View(vm);
		}

		[HttpPost]
		public ActionResult Edit(int? id, StandardsEditViewModel vm)
		{
			var standard = new tbl_lst_StandardsAll();
			if (vm.Mode == Mode.Edit)
			{
				standard = _dbSubmission.tbl_lst_StandardsAll.Find(vm.Id);
			}

			standard.Id = vm.Id ?? 0;
			standard.JurisdictionId = vm.JurisdictionId;
			standard.Standard = vm.Standard;
			standard.Active = vm.Active;
			standard.AddByDefault = vm.AddByDefault;

			if (vm.Mode == Mode.Add)
			{
				_dbSubmission.tbl_lst_StandardsAll.Add(standard);
			}

			_dbSubmission.SaveChanges();

			CacheManager.Clear(LookupAjax.Standards.ToString());

			return RedirectToAction("Index");
		}

		public JsonResult ValidateStandard(int? id, string standard, string jurisdictionId, Mode mode)
		{
			var validates = true;

			if (!string.IsNullOrEmpty(standard) && _dbSubmission.tbl_lst_StandardsAll.Any(x => x.Id != id && x.JurisdictionId == jurisdictionId && x.Standard == standard))
			{
				validates = false;
			}

			return Json(validates, JsonRequestBehavior.AllowGet);
		}

		[HttpPost]
		public JsonResult CheckDelete(int id)
		{
			var isInUse = _dbSubmission.tbl_Standards.Where(x => x.StandardsId == id).Any();
			return Json(isInUse);
		}

		[HttpPost]
		public ActionResult Delete(int id)
		{
			_dbSubmission.tbl_lst_StandardsAll.Where(x => x.Id == id).Delete();

			CacheManager.Clear(LookupAjax.Standards.ToString());

			return RedirectToAction("Index");
		}

		[HttpPost]
		public ActionResult SetActive(int id, Mode mode)
		{
			var active = (mode == Mode.Deactivate) ? false : true;

			var standard = _dbSubmission.tbl_lst_StandardsAll.Find(id);
			standard.Active = active;

			_dbSubmission.SaveChanges();

			CacheManager.Clear(LookupAjax.Standards.ToString());

			return RedirectToAction("Index");
		}
	}
}