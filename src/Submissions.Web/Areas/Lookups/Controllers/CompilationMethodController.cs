﻿extern alias ZEFCore;
using AutoMapper.QueryableExtensions;
using GLI.EFCore.Submission;
using Microsoft.EntityFrameworkCore;
using Submissions.Common;
using Submissions.Web.Areas.Lookups.Models;
using System.Linq;
using System.Web.Mvc;
using ZEFCore::Z.EntityFramework.Plus;

namespace Submissions.Web.Areas.Lookups.Controllers
{
	[AuthorizeUser(Permission = Permission.Admin, HasAccessRight = AccessRight.Edit)]
	public class CompilationMethodController : Controller, ILookupDelete<int>
	{
		private readonly SubmissionContext _dbSubmission;

		public CompilationMethodController(SubmissionContext dbSubmission)
		{
			_dbSubmission = dbSubmission;
		}

		public ActionResult Index()
		{
			var compilationMethods = _dbSubmission.tbl_lst_CompilationMethod
				.AsNoTracking()
				.OrderByDescending(x => x.Active)
				.ThenBy(x => x.Name)
				.ProjectTo<CompilationMethodViewModel>();

			return View(compilationMethods);
		}

		public ActionResult Edit(int? id, Mode mode)
		{
			var vm = new CompilationMethodViewModel();
			vm.Active = true;
			if (mode == Mode.Edit)
			{
				var compilationMethod = _dbSubmission.tbl_lst_CompilationMethod.Find(id);
				vm.Id = compilationMethod.Id;
				vm.Name = compilationMethod.Name;
			}
			vm.Mode = mode;

			return View(vm);
		}

		[HttpPost]
		public ActionResult Edit(int? id, CompilationMethodViewModel vm)
		{
			var compilationMethod = new tbl_lst_CompilationMethod();
			if (vm.Mode == Mode.Edit)
			{
				compilationMethod = _dbSubmission.tbl_lst_CompilationMethod.Find(id);
			}
			compilationMethod.Id = vm.Id ?? 0;
			compilationMethod.Name = vm.Name;
			compilationMethod.Active = vm.Active;
			if (vm.Mode == Mode.Add)
			{
				_dbSubmission.tbl_lst_CompilationMethod.Add(compilationMethod);
			}
			_dbSubmission.SaveChanges();

			CacheManager.Clear(LookupAjax.CompilationMethod.ToString());

			return RedirectToAction("Index");
		}

		[HttpPost]
		public JsonResult CheckDelete(int id)
		{
			var isInUse = _dbSubmission.GamingGuidelineCompilationMethod.Any(x => x.CompilationMethodId == id);
			return Json(isInUse);
		}

		[HttpPost]
		public ActionResult Delete(int id)
		{
			_dbSubmission.tbl_lst_CompilationMethod.Where(x => x.Id == id).Delete();

			CacheManager.Clear(LookupAjax.CompilationMethod.ToString());

			return RedirectToAction("Index");
		}

		public JsonResult ValidateName(int? id, string name, Mode mode)
		{
			var validates = true;
			if (!string.IsNullOrEmpty(name) && _dbSubmission.tbl_lst_CompilationMethod.Any(x => x.Id != id && x.Name == name))
			{
				validates = false;
			}
			return Json(validates, JsonRequestBehavior.AllowGet);
		}

		[HttpPost]
		public ActionResult SetActive(int id, Mode mode)
		{
			var active = (mode == Mode.Deactivate) ? false : true;
			var compilationMethod = _dbSubmission.tbl_lst_CompilationMethod.Find(id);
			compilationMethod.Active = active;
			_dbSubmission.SaveChanges();

			CacheManager.Clear(LookupAjax.CompilationMethod.ToString());

			return RedirectToAction("Index");
		}
	}
}