﻿extern alias ZEFCore;
using AutoMapper.QueryableExtensions;
using GLI.EFCore.Submission;
using Microsoft.EntityFrameworkCore;
using Submissions.Common;
using Submissions.Web.Areas.Lookups.Models;
using System.Linq;
using System.Web.Mvc;
using ZEFCore::Z.EntityFramework.Plus;

namespace Submissions.Web.Areas.Lookups.Controllers
{
	public class GamingGuidelineGameTypesController : Controller, ILookupDelete<int>
	{
		private readonly SubmissionContext _dbSubmission;

		public GamingGuidelineGameTypesController(SubmissionContext dbSubmission)
		{
			_dbSubmission = dbSubmission;
		}

		public ActionResult Index()
		{
			var gameTypes = _dbSubmission.tbl_lst_GamingGuidelineGameTypes
				.AsNoTracking()
				.OrderBy(x => x.Active)
				.ThenBy(x => x.Name)
				.ProjectTo<tbl_lst_GamingGuidelineGameTypesViewModel>();

			return View(gameTypes);
		}

		public ActionResult Edit(int? id, Mode mode)
		{
			var vm = new tbl_lst_GamingGuidelineGameTypesViewModel();
			vm.Active = true;

			if (mode == Mode.Edit)
			{
				var gameType = _dbSubmission.tbl_lst_GamingGuidelineGameTypes.Find(id);

				vm.Id = gameType.Id;
				vm.Name = gameType.Name;
				vm.Note = gameType.Note;
			}
			vm.Mode = mode;

			return View(vm);
		}

		[HttpPost]
		public ActionResult Edit(int? id, tbl_lst_GamingGuidelineGameTypesViewModel vm)
		{
			var gameType = new tbl_lst_GamingGuidelineGameTypes();
			if (vm.Mode == Mode.Edit)
			{
				gameType = _dbSubmission.tbl_lst_GamingGuidelineGameTypes.Find(id);
			}

			gameType.Id = vm.Id ?? 0;
			gameType.Name = vm.Name;
			gameType.Note = vm.Note;
			gameType.Active = vm.Active;

			if (vm.Mode == Mode.Add)
			{
				_dbSubmission.tbl_lst_GamingGuidelineGameTypes.Add(gameType);
			}

			_dbSubmission.SaveChanges();

			CacheManager.Clear(LookupAjax.GamingGuidelinesGameTypes.ToString());

			return RedirectToAction("Index");
		}

		[HttpPost]
		public JsonResult CheckDelete(int id)
		{
			var gameType = _dbSubmission.tbl_lst_GamingGuidelineGameTypes.Find(id);
			var inInUse = _dbSubmission.GamingGuidelineRTPRequirements.Any(x => x.GameTypeId == id) && gameType.Active ||
						_dbSubmission.GamingGuidelineLimits.Any(x => x.GameTypeId == id) && gameType.Active ||
						_dbSubmission.GamingGuidelineProhibitedGameTypes.Any(x => x.GameTypeId == id) && gameType.Active ? true : false;
			return Json(inInUse);
		}

		[HttpPost]
		public ActionResult Delete(int id)
		{
			_dbSubmission.tbl_lst_GamingGuidelineGameTypes.Where(x => x.Id == id).Delete();

			CacheManager.Clear(LookupAjax.GamingGuidelinesGameTypes.ToString());

			return RedirectToAction("Index");
		}

		public JsonResult ValidateGameType(int? id, string name)
		{
			var validates = true;

			if (!string.IsNullOrEmpty(name) && _dbSubmission.tbl_lst_GamingGuidelineGameTypes.Any(x => x.Id != id && x.Name == name))
			{
				validates = false;
			}
			return Json(validates, JsonRequestBehavior.AllowGet);
		}

		[HttpPost]
		public ActionResult SetActive(int id, Mode mode)
		{
			var active = (mode == Mode.Deactivate) ? false : true;

			var gameType = _dbSubmission.tbl_lst_GamingGuidelineGameTypes.Find(id);
			gameType.Active = active;

			_dbSubmission.SaveChanges();

			CacheManager.Clear(LookupAjax.GamingGuidelinesGameTypes.ToString());

			return RedirectToAction("Index");

		}
	}
}