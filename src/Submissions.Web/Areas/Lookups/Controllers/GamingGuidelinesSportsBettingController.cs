﻿extern alias ZEFCore;
using AutoMapper;
using GLI.EFCore.Submission;
using Newtonsoft.Json;
using Submissions.Common;
using Submissions.Web.Areas.Lookups.Models;
using Submissions.Web.Models.Grids.GamingGuidelines;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using ZEFCore::Z.EntityFramework.Plus;

namespace Submissions.Web.Areas.Lookups.Controllers
{
	public class GamingGuidelinesSportsBettingController : Controller, ILookupDelete<int>
	{
		private readonly SubmissionContext _dbSubmission;
		private readonly IUserContext _userContext;

		public GamingGuidelinesSportsBettingController(SubmissionContext dbSubmission, IUserContext userContext)
		{
			_dbSubmission = dbSubmission;
			_userContext = userContext;
		}

		public ActionResult Index()
		{
			var vm = new GamingGuidelinesSportsBettingViewModel();
			var grid = new GamingGuidelinesSportsBettingGrid();
			vm.SportsBettingGrid = grid;
			return View(vm);
		}

		public ActionResult Edit(int id, Mode mode, int jurId = 0)
		{
			var vm = new GamingGuidelinesSportsBettingEditViewModel();

			if (mode == Mode.Add)
			{
				if (_dbSubmission.GamingGuidelineSportsBetting.Where(x => x.JurisdictionId == jurId).Any())
				{
					TempData["ErrorMessage"] = "Can not create new Guideline for selected jurisdiction";
					return RedirectToAction("Index");
				}
			}
			vm = CreateVm(id, jurId, mode);

			vm.DropdownOptions = ProjectEnumIntoSelectList(new SportsBettingSelectOptions());

			return View(vm);
		}

		[HttpPost]
		public ActionResult Edit(int? id, string GamingGuidelinesSportsBettingEditViewModel)
		{
			var vm = JsonConvert.DeserializeObject<GamingGuidelinesSportsBettingEditViewModel>(GamingGuidelinesSportsBettingEditViewModel);

			var ggSportsBetting = new GamingGuidelineSportsBetting();
			if (vm.Mode == Mode.Edit)
			{
				ggSportsBetting = _dbSubmission.GamingGuidelineSportsBetting.Find(id);
			}

			ggSportsBetting.AnonymousPlayAllowed = vm.AnonymousPlayAllowed;
			ggSportsBetting.AuditNote = vm.AuditNote;
			ggSportsBetting.AuditRequired = vm.AuditRequired;
			ggSportsBetting.ChangeManagementSystem = vm.ChangeManagementSystem;
			ggSportsBetting.DataRetentionRequirement = vm.DataRetentionRequirement;
			ggSportsBetting.DataVaults = vm.DataVaults;
			ggSportsBetting.GeneralNotes = vm.GeneralNotes;
			ggSportsBetting.Comments = vm.Comments;
			ggSportsBetting.ITLRequired = vm.ITLRequired;
			ggSportsBetting.JurisdictionId = vm.JurisdictionId;
			ggSportsBetting.LandBased = vm.LandBased;
			ggSportsBetting.LiabilityAmountRequired = vm.LiabilityAmountRequired;
			ggSportsBetting.LiabilityNote = vm.LiabilityNote;
			ggSportsBetting.LimitOnEventTypeAllowed = vm.LimitOnEventTypeAllowed;
			ggSportsBetting.LocationId = vm.LocationId ?? 0;
			ggSportsBetting.MaxCaps = vm.MaxCaps;
			ggSportsBetting.Online = vm.Online;
			ggSportsBetting.RetentionPeriod = vm.RetentionPeriod;
			ggSportsBetting.ServerLocationRequirement = vm.ServerLocationRequirement;
			ggSportsBetting.TaxNotes = vm.TaxNotes;
			ggSportsBetting.TaxPercentage = vm.TaxPercentage;

			if (vm.Mode == Mode.Add)
			{
				_dbSubmission.GamingGuidelineSportsBetting.Add(ggSportsBetting);
			}
			UpdateUserComments(vm);
			_dbSubmission.SaveChanges();

			CacheManager.Clear(LookupAjax.SportsBettingJurisdictions.ToString());

			return RedirectToAction("Index");
		}

		[HttpPost]
		public JsonResult CheckDelete(int id)
		{
			return Json(false);
		}

		[HttpPost]
		public ActionResult Delete(int id)
		{
			_dbSubmission.GamingGuidelineSportsBetting.Where(x => x.Id == id).Delete();

			CacheManager.Clear(LookupAjax.SportsBettingJurisdictions.ToString());

			return RedirectToAction("Index");
		}

		private List<SelectListItem> ProjectEnumIntoSelectList(Enum enumToConvert)
		{
			var enumType = enumToConvert.GetType();
			var enumValues = Enum.GetValues(enumType);
			return (from object enumValue in enumValues select new SelectListItem() { Value = ((int)enumValue).ToString(), Text = enumValue.GetEnumDescription() }).ToList();
		}

		private GamingGuidelinesSportsBettingEditViewModel CreateVm(int id, int jurId, Mode mode)
		{
			var vm = new GamingGuidelinesSportsBettingEditViewModel();

			var ggSportsBetting = _dbSubmission.GamingGuidelineSportsBetting.Find(id);

			if (ggSportsBetting != null)
			{
				vm = Mapper.Map<GamingGuidelinesSportsBettingEditViewModel>(ggSportsBetting);
			}
			if (vm.LocationId != null && vm.LocationId != 0)
			{
				vm.Location = _dbSubmission.tbl_lst_Country
					.SingleOrDefault(x => x.Id == vm.LocationId)
					.Code;
			}
			else
			{
				vm.Location = "";
			}

			vm.JurisdictionId = vm.JurisdictionId == 0 ? jurId : vm.JurisdictionId;
			var jurisdictionIdString = vm.JurisdictionId.ToJurisdictionIdString();
			vm.JurisdictionName = _dbSubmission.tbl_lst_fileJuris
				.SingleOrDefault(x => x.Id == jurisdictionIdString)
				.Name;
			vm.UserComments = _dbSubmission.GamingGuidelineSportsBettingComment
				.Where(x => x.SportsBettingId == vm.Id)
				.Select(x => new GamingGuidelineCommentSportsBettingViewModel
				{
					Id = x.Id,
					AddedTime = x.AddedDate,
					CommentString = x.Comment,
					UserId = x.UserId,
					UserName = x.User.FName + " " + x.User.LName,
					IsEditable = x.UserId == _userContext.User.Id,
					_dirty = false
				}).ToList();
			vm.CommentCount = vm.UserComments.Count;
			vm.DefaultUserComment = new GamingGuidelineCommentSportsBettingViewModel
			{
				AddedTime = DateTime.Now,
				UserId = _userContext.User.Id,
				UserName = _userContext.User.FName + " " + _userContext.User.LName,
				CommentString = "",
				IsEditable = true
			};
			vm.Mode = mode;
			return vm;
		}

		private void UpdateUserComments(GamingGuidelinesSportsBettingEditViewModel vm)
		{
			CommentsToCreate(vm);
			CommentsToUpdate(vm);
			CommentsToDelete(vm);
		}

		private void CommentsToCreate(GamingGuidelinesSportsBettingEditViewModel vm)
		{
			var userCommentsToAdd = vm.UserComments.Where(x => !x._destroy && x.Id == 0 && x.IsEditable).ToList();
			foreach (var comments in userCommentsToAdd)
			{
				var newComment = new GamingGuidelineSportsBettingComment
				{
					AddedDate = DateTime.Now,
					Comment = comments.CommentString,
					SportsBettingId = vm.Id,
					UserId = comments.UserId
				};
				_dbSubmission.GamingGuidelineSportsBettingComment.Add(newComment);
			}
		}

		private void CommentsToUpdate(GamingGuidelinesSportsBettingEditViewModel vm)
		{
			var userCommentsToModify = vm.UserComments.Where(x => x._dirty && x.Id != 0).ToList();
			foreach (var modifyComments in userCommentsToModify)
			{
				var comment = new GamingGuidelineSportsBettingComment();
				comment = _dbSubmission.GamingGuidelineSportsBettingComment.Find(modifyComments.Id);
				comment.Comment = modifyComments.CommentString;
				comment.AddedDate = DateTime.Now;
			}
		}

		private void CommentsToDelete(GamingGuidelinesSportsBettingEditViewModel vm)
		{
			var userCommentsToDelete = vm.UserComments.Where(x => x._destroy && x.Id != 0).Select(x => x.Id).ToList();
			foreach (var toDelete in userCommentsToDelete)
			{
				_dbSubmission.GamingGuidelineSportsBettingComment.Where(x => x.Id == toDelete).Delete();
			}
		}

	}
}