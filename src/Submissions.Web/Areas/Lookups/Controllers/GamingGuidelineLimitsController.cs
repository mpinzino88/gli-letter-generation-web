﻿extern alias ZEFCore;
using AutoMapper.QueryableExtensions;
using GLI.EFCore.Submission;
using Microsoft.EntityFrameworkCore;
using Submissions.Common;
using Submissions.Web.Areas.Lookups.Models;
using System.Linq;
using System.Web.Mvc;
using ZEFCore::Z.EntityFramework.Plus;

namespace Submissions.Web.Areas.Lookups.Controllers
{
	public class GamingGuidelineLimitsController : Controller, ILookupDelete<int>
	{
		private readonly SubmissionContext _dbSubmission;

		public GamingGuidelineLimitsController(SubmissionContext dbSubmission)
		{
			_dbSubmission = dbSubmission;
		}

		public ActionResult Index()
		{
			var limitType = _dbSubmission.tbl_lst_GamingGuidelineLimitType
				.AsNoTracking()
				.OrderBy(x => x.Active)
				.ThenBy(x => x.Name)
				.ProjectTo<GamingGuidelineLimitsViewModel>();

			return View(limitType);
		}

		public ActionResult Edit(int? id, Mode mode)
		{
			var vm = new GamingGuidelineLimitsViewModel();
			vm.Active = true;

			if (mode == Mode.Edit)
			{
				var limitType = _dbSubmission.tbl_lst_GamingGuidelineLimitType.Find(id);

				vm.Id = limitType.Id;
				vm.Name = limitType.Name;
				vm.Note = limitType.Note;
			}
			vm.Mode = mode;
			return View(vm);
		}

		[HttpPost]
		public ActionResult Edit(int? id, GamingGuidelineLimitsViewModel vm)
		{
			var limitType = new tbl_lst_GamingGuidelineLimitType();
			if (vm.Mode == Mode.Edit)
			{
				limitType = _dbSubmission.tbl_lst_GamingGuidelineLimitType.Find(id);
			}

			limitType.Id = vm.Id ?? 0;
			limitType.Name = vm.Name;
			limitType.Note = vm.Note;
			limitType.Active = vm.Active;

			if (vm.Mode == Mode.Add)
			{
				_dbSubmission.tbl_lst_GamingGuidelineLimitType.Add(limitType);
			}

			_dbSubmission.SaveChanges();

			CacheManager.Clear(LookupAjax.GamingGuidelineLimits.ToString());

			return RedirectToAction("Index");
		}

		[HttpPost]
		public JsonResult CheckDelete(int id)
		{
			var limitType = _dbSubmission.tbl_lst_GamingGuidelineLimitType.Find(id);
			var isInUse = _dbSubmission.GamingGuidelineLimits.Any(x => x.GamingGuidelineId == id) && limitType.Active;

			return Json(isInUse);
		}

		[HttpPost]
		public ActionResult Delete(int id)
		{
			_dbSubmission.tbl_lst_GamingGuidelineLimitType.Where(x => x.Id == id).Delete();
			_dbSubmission.GamingGuidelineLimits.Where(x => x.LimitTypeId == id).Delete();

			CacheManager.Clear(LookupAjax.GamingGuidelineLimits.ToString());

			return RedirectToAction("Index");
		}

		public JsonResult ValidateLimitType(int? id, string name)
		{
			var validates = true;

			if (!string.IsNullOrEmpty(name) && _dbSubmission.tbl_lst_GamingGuidelineLimitType.Any(x => x.Id != id && x.Name == name))
			{
				validates = false;
			}
			return Json(validates, JsonRequestBehavior.AllowGet);
		}

		[HttpPost]
		public ActionResult setActive(int id, Mode mode)
		{
			var active = (mode == Mode.Deactivate) ? false : true;

			var limitType = _dbSubmission.tbl_lst_GamingGuidelineLimitType.Find(id);
			limitType.Active = active;

			_dbSubmission.SaveChanges();

			CacheManager.Clear(LookupAjax.GamingGuidelineLimits.ToString());

			return RedirectToAction("Index");

		}
	}
}