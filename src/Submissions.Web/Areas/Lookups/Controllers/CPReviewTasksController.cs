﻿extern alias ZEFCore;
using AutoMapper.QueryableExtensions;
using GLI.EFCore.Submission;
using Microsoft.EntityFrameworkCore;
using Submissions.Common;
using Submissions.Web.Areas.Lookups.Models;
using System.Linq;
using System.Web.Mvc;
using ZEFCore::Z.EntityFramework.Plus;

namespace Submissions.Web.Areas.Lookups.Controllers
{
	[AuthorizeUser(Permission = Permission.Admin, HasAccessRight = AccessRight.Edit)]
	public class CPReviewTasksController : Controller
	{
		private readonly SubmissionContext _dbSubmission;

		public CPReviewTasksController(SubmissionContext dbSubmission)
		{
			_dbSubmission = dbSubmission;
		}

		public ActionResult Index()
		{
			var cpReviewTasks = _dbSubmission.tbl_lst_CPReviewTasks
				.AsNoTracking()
				.OrderByDescending(x => x.Active)
				.ThenBy(x => x.Jurisdiction.FormattedJurisdictionName)
				.ProjectTo<CPReviewTasksViewModel>();

			return View(cpReviewTasks);
		}

		public ActionResult Edit(int? id, Mode mode)
		{
			var vm = new CPReviewTasksViewModel();
			vm.Active = true;

			if (mode == Mode.Edit)
			{
				var cpReviewTask = _dbSubmission.tbl_lst_CPReviewTasks.Find(id);
				vm.Id = cpReviewTask.Id;
				vm.JurisdictionId = cpReviewTask.JurisdictionId;
				vm.Jurisdiction = cpReviewTask.Jurisdiction.FormattedJurisdictionName;
				vm.EstimatedHours = cpReviewTask.EstimatedHours;
			}

			vm.Mode = mode;

			return View(vm);
		}

		[HttpPost]
		public ActionResult Edit(int? id, CPReviewTasksViewModel vm)
		{
			var cpReviewTask = new tbl_lst_CPReviewTasks();
			if (vm.Mode == Mode.Edit)
			{
				cpReviewTask = _dbSubmission.tbl_lst_CPReviewTasks.Find(vm.Id);
			}

			cpReviewTask.EstimatedHours = vm.EstimatedHours;
			cpReviewTask.JurisdictionId = vm.JurisdictionId;
			cpReviewTask.Active = vm.Active;
			cpReviewTask.EstimatedHours = vm.EstimatedHours;

			if (vm.Mode == Mode.Add)
			{
				_dbSubmission.tbl_lst_CPReviewTasks.Add(cpReviewTask);
			}
			_dbSubmission.SaveChanges();

			CacheManager.Clear(LookupAjax.CPReviewTasks.ToString());

			return RedirectToAction("Index");
		}

		[HttpPost]
		public JsonResult CheckDelete(int id)
		{
			return Json(false);
		}

		[HttpPost]
		public ActionResult Delete(int id)
		{
			_dbSubmission.tbl_lst_CPReviewTasks.Where(x => x.Id == id).Delete();

			CacheManager.Clear(LookupAjax.CPReviewTasks.ToString());

			return RedirectToAction("Index");
		}

		public JsonResult ValidateCPReviewTask(int? id, string jurisdictionId, Mode mode)
		{
			var validates = true;

			if (!string.IsNullOrEmpty(jurisdictionId) && _dbSubmission.tbl_lst_CPReviewTasks.Any(x => x.Id != id && x.JurisdictionId == jurisdictionId))
			{
				validates = false;
			}

			return Json(validates, JsonRequestBehavior.AllowGet);
		}

		[HttpPost]
		public ActionResult SetActive(int id, Mode mode)
		{
			var active = (mode == Mode.Activate ? true : false);
			var cpReviewTask = _dbSubmission.tbl_lst_CPReviewTasks.Find(id);
			cpReviewTask.Active = active;
			_dbSubmission.SaveChanges();

			CacheManager.Clear(LookupAjax.CPReviewTasks.ToString());

			return RedirectToAction("Index");
		}
	}
}