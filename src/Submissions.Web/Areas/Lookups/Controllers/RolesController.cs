﻿extern alias ZEFCore;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using GLI.EFCore.Submission;
using Microsoft.EntityFrameworkCore;
using Submissions.Common;
using Submissions.Web.Areas.Lookups.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using ZEFCore::Z.EntityFramework.Plus;

namespace Submissions.Web.Areas.Lookups.Controllers
{
	[AuthorizeUser(Permission = Permission.Admin, HasAccessRight = AccessRight.Edit)]
	public class RolesController : Controller, ILookupDelete<int>
	{
		private readonly SubmissionContext _dbSubmission;

		public RolesController(SubmissionContext dbSubmission)
		{
			_dbSubmission = dbSubmission;
		}

		public ActionResult Index()
		{
			var roles = _dbSubmission.Roles
				.OrderByDescending(x => x.Active)
				.ThenBy(x => x.Code)
				.ProjectTo<RolesViewModel>();

			return View(roles);
		}

		public ActionResult Edit(int? id, Mode mode)
		{
			var defaultAccessRights = LoadDefaultAccessRights();

			var vm = new RolesViewModel();
			vm.Active = true;

			if (mode == Mode.Edit)
			{
				var role = _dbSubmission.Roles.Find(id);
				var rolePermissions = _dbSubmission.RolePermissions.Where(x => x.RoleId == id).ToList();

				// populate default access rights from saved data
				foreach (var right in defaultAccessRights)
				{
					var dbPermission = rolePermissions.Where(x => x.PermissionId == right.PermissionId).Select(x => x.PermissionValue).SingleOrDefault();
					var permission = (AccessRight)Enum.ToObject(typeof(AccessRight), dbPermission);
					right.Read = permission.HasFlag(AccessRight.Read);
					right.Add = permission.HasFlag(AccessRight.Add);
					right.Edit = permission.HasFlag(AccessRight.Edit);
					right.Delete = permission.HasFlag(AccessRight.Delete);
				}

				vm = Mapper.Map<RolesViewModel>(role);

				if (role.DepartmentId != null)
				{
					vm.DepartmentName = role.Department.Code + " - " + role.Department.Description;
				}
			}

			vm.Mode = mode;
			vm.AccessRights = defaultAccessRights;

			return View(vm);
		}

		[HttpPost]
		public ActionResult Edit(RolesViewModel vm)
		{
			var role = new Roles();

			if (vm.Mode == Mode.Edit)
			{
				role = _dbSubmission.Roles.Find(vm.Id);
			}

			role = ApplyChanges(role, vm);

			if (vm.Mode == Mode.Add)
			{
				_dbSubmission.Roles.Add(role);
			}

			_dbSubmission.SaveChanges();

			if (vm.Mode == Mode.Add)
			{
				foreach (var item in vm.AccessRights)
				{
					var rolePermission = ApplyRolePermissionChanges(role.Id, item);
					_dbSubmission.RolePermissions.Add(rolePermission);
				}
			}
			else
			{
				var existingRolePermissionIds = _dbSubmission.RolePermissions
					.Where(x => x.RoleId == role.Id)
					.Select(x => x.PermissionId)
					.ToList();

				var newRolePermissions = vm.AccessRights.Where(x => !existingRolePermissionIds.Contains(x.PermissionId)).ToList();
				var existingRolePermissions = vm.AccessRights.Where(x => existingRolePermissionIds.Contains(x.PermissionId)).ToList();

				foreach (var item in existingRolePermissions)
				{
					var existingRolePermission = ApplyRolePermissionChanges(role.Id, item);
					_dbSubmission.Entry(existingRolePermission).State = EntityState.Modified;
				}

				foreach (var item in newRolePermissions)
				{
					var newRolePermission = ApplyRolePermissionChanges(role.Id, item);
					_dbSubmission.RolePermissions.Add(newRolePermission);
				}
			}

			_dbSubmission.SaveChanges();

			CacheManager.Clear(LookupAjax.Roles.ToString());

			return RedirectToAction("Index");
		}

		[HttpPost]
		public JsonResult CheckDelete(int id)
		{
			var isInUse = _dbSubmission.trLogins.Where(x => x.RoleId == id).Any();
			return Json(isInUse);
		}

		[HttpPost]
		public ActionResult Delete(int id)
		{
			_dbSubmission.Roles.Where(x => x.Id == id).Delete();

			CacheManager.Clear(LookupAjax.Roles.ToString());

			return RedirectToAction("Index");
		}

		[HttpPost]
		public ActionResult SetActive(int id, Mode mode)
		{
			var active = (mode == Mode.Deactivate) ? false : true;

			var role = _dbSubmission.Roles.Find(id);
			role.Active = active;

			_dbSubmission.SaveChanges();

			CacheManager.Clear(LookupAjax.Roles.ToString());

			return RedirectToAction("Index");
		}

		public JsonResult ValidateCode(int? id, string code, Mode mode)
		{
			var validates = true;

			if (!string.IsNullOrEmpty(code) && _dbSubmission.Roles.Any(x => x.Id != id && x.Code == code))
			{
				validates = false;
			}

			return Json(validates, JsonRequestBehavior.AllowGet);
		}

		#region Helper Methods
		private Roles ApplyChanges(Roles role, RolesViewModel vm)
		{
			role.Id = vm.Id ?? 0;
			role.Code = vm.Code;
			role.DepartmentId = vm.DepartmentId;
			role.Description = vm.Description;
			role.Active = vm.Active;

			return role;
		}

		private RolePermission ApplyRolePermissionChanges(int roleId, AccessRightCheckbox accessRight)
		{
			var permission = accessRight.Read ? AccessRight.Read : 0;
			permission |= accessRight.Add ? AccessRight.Add : 0;
			permission |= accessRight.Edit ? AccessRight.Edit : 0;
			permission |= accessRight.Delete ? AccessRight.Delete : 0;

			var permissionDb = Convert.ToInt32(permission);

			var existingRolePermission = new RolePermission
			{
				RoleId = roleId,
				PermissionId = accessRight.PermissionId,
				PermissionValue = permissionDb
			};
			return existingRolePermission;
		}

		private List<AccessRightCheckbox> LoadDefaultAccessRights()
		{
			return _dbSubmission.Permissions
				.Select(x => new AccessRightCheckbox { PermissionId = x.Id, Code = x.Code, Description = x.Description })
				.OrderBy(x => x.Description)
				.ToList();
		}
		#endregion
	}
}