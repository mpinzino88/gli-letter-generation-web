﻿extern alias ZEFCore;
using AutoMapper.QueryableExtensions;
using GLI.EFCore.Submission;
using Microsoft.EntityFrameworkCore;
using Submissions.Common;
using Submissions.Web.Areas.Lookups.Models;
using System.Linq;
using System.Web.Mvc;
using ZEFCore::Z.EntityFramework.Plus;

namespace Submissions.Web.Areas.Lookups.Controllers
{
	public class ChipTypesController : Controller
	{
		private readonly SubmissionContext _dbSubmission;

		public ChipTypesController(SubmissionContext dbSubmission)
		{
			_dbSubmission = dbSubmission;
		}

		public ActionResult Index()
		{
			var chiptypes = _dbSubmission.tbl_lst_EPROMsize
				.AsNoTracking()
				.OrderByDescending(x => x.Active)
				.ThenBy(x => x.Code)
				.ProjectTo<ChipTypeViewModel>();

			return View(chiptypes);
		}

		public ActionResult Edit(int? id, Mode mode)
		{
			var vm = new ChipTypeViewModel();
			vm.Active = true;

			if (mode == Mode.Edit)
			{
				var chipType = _dbSubmission.tbl_lst_EPROMsize.Find(id);

				vm.Id = chipType.Id;
				vm.Code = chipType.Code;
			}

			vm.Mode = mode;

			return View(vm);
		}

		[HttpPost]
		public ActionResult Edit(int? id, ChipTypeViewModel vm)
		{
			var chipType = new tbl_lst_EPROMsize();
			if (vm.Mode == Mode.Edit)
			{
				chipType = _dbSubmission.tbl_lst_EPROMsize.Find(vm.Id);
			}

			chipType.Id = vm.Id ?? 0;
			chipType.Active = vm.Active;
			chipType.Code = vm.Code;

			if (vm.Mode == Mode.Add)
			{
				_dbSubmission.tbl_lst_EPROMsize.Add(chipType);
			}

			_dbSubmission.SaveChanges();

			CacheManager.Clear(LookupAjax.ChipTypes.ToString());

			return RedirectToAction("Index");
		}

		[HttpPost]
		public JsonResult CheckDelete(int id)
		{
			var chiptype = _dbSubmission.tbl_lst_EPROMsize.Find(id);
			var isInUse = _dbSubmission.Submissions.Any(x => x.ChipType == chiptype.Code);

			return Json(isInUse);
		}

		[HttpPost]
		public ActionResult Delete(int id)
		{
			_dbSubmission.tbl_lst_EPROMsize.Where(x => x.Id == id).Delete();

			CacheManager.Clear(LookupAjax.ChipTypes.ToString());

			return RedirectToAction("Index");
		}

		[HttpPost]
		public ActionResult SetActive(int id, Mode mode)
		{
			bool active = (mode == Mode.Activate ? true : false);

			var version = _dbSubmission.tbl_lst_EPROMsize.Find(id);
			version.Active = active;
			_dbSubmission.SaveChanges();

			CacheManager.Clear(LookupAjax.ChipTypes.ToString());

			return RedirectToAction("Index");
		}

		public JsonResult ValidateCode(int? id, string Code, Mode mode)
		{
			var validates = true;

			if (!string.IsNullOrEmpty(Code) && _dbSubmission.tbl_lst_EPROMsize.Any(x => x.Id != id && x.Code == Code))
			{
				validates = false;
			}

			return Json(validates, JsonRequestBehavior.AllowGet);
		}
	}
}