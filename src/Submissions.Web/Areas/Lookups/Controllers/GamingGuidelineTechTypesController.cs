﻿extern alias ZEFCore;
using AutoMapper.QueryableExtensions;
using GLI.EFCore.Submission;
using Microsoft.EntityFrameworkCore;
using Submissions.Common;
using Submissions.Web.Areas.Lookups.Models;
using System.Linq;
using System.Web.Mvc;
using ZEFCore::Z.EntityFramework.Plus;

namespace Submissions.Web.Areas.Lookups.Controllers
{
	public class GamingGuidelineTechTypesController : Controller, ILookupDelete<int>
	{
		private readonly SubmissionContext _dbSubmission;

		public GamingGuidelineTechTypesController(SubmissionContext dbSubmission)
		{
			_dbSubmission = dbSubmission;
		}

		public ActionResult Index()
		{
			var techType = _dbSubmission.tbl_lst_GamingGuidelineTechType
				.AsNoTracking()
				.OrderBy(x => x.Active)
				.ThenBy(x => x.Name)
				.ProjectTo<GamingGuidelineTechTypeViewModel>();

			return View(techType);
		}

		public ActionResult Edit(int? id, Mode mode)
		{
			var vm = new GamingGuidelineTechTypeViewModel();
			vm.Active = true;

			if (mode == Mode.Edit)
			{
				var techType = _dbSubmission.tbl_lst_GamingGuidelineTechType.Find(id);

				vm.Id = techType.Id;
				vm.Name = techType.Name;
				vm.Note = techType.Note;
			}
			vm.Mode = mode;

			return View(vm);
		}

		[HttpPost]
		public ActionResult Edit(int? id, GamingGuidelineTechTypeViewModel vm)
		{
			var techType = new tbl_lst_GamingGuidelineTechType();
			if (vm.Mode == Mode.Edit)
			{
				techType = _dbSubmission.tbl_lst_GamingGuidelineTechType.Find(id);
			}

			techType.Id = vm.Id ?? 0;
			techType.Name = vm.Name;
			techType.Note = vm.Note;
			techType.Active = vm.Active;

			if (vm.Mode == Mode.Add)
			{
				_dbSubmission.tbl_lst_GamingGuidelineTechType.Add(techType);
			}

			_dbSubmission.SaveChanges();

			CacheManager.Clear(LookupAjax.GamingGuidelineTechTypes.ToString());

			return RedirectToAction("Index");
		}

		[HttpPost]
		public JsonResult CheckDelete(int id)
		{
			var techType = _dbSubmission.tbl_lst_GamingGuidelineTechType.Find(id);
			var isInUse = _dbSubmission.GamingGuidelineTechType.Any(x => x.GamingGuidelineId == id) && techType.Active;

			return Json(isInUse);

		}

		[HttpPost]
		public ActionResult Delete(int id)
		{
			_dbSubmission.tbl_lst_GamingGuidelineTechType.Where(x => x.Id == id).Delete();
			_dbSubmission.GamingGuidelineTechType.Where(x => x.TechTypeId == id).Delete();

			CacheManager.Clear(LookupAjax.GamingGuidelineTechTypes.ToString());

			return RedirectToAction("Index");
		}

		public JsonResult ValidateTechType(int? id, string name)
		{
			var validates = true;

			if (!string.IsNullOrEmpty(name) && _dbSubmission.tbl_lst_GamingGuidelineTechType.Any(x => x.Id != id && x.Name == name))
			{
				validates = false;
			}

			return Json(validates, JsonRequestBehavior.AllowGet);
		}

		[HttpPost]
		public ActionResult SetActive(int id, Mode mode)
		{
			var active = (mode == Mode.Deactivate) ? false : true;

			var techType = _dbSubmission.tbl_lst_GamingGuidelineTechType.Find(id);
			techType.Active = active;

			_dbSubmission.SaveChanges();

			CacheManager.Clear(LookupAjax.GamingGuidelineTechTypes.ToString());

			return RedirectToAction("Index");

		}
	}
}