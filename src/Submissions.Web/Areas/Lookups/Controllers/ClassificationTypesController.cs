﻿extern alias ZEFCore;
using AutoMapper.QueryableExtensions;
using GLI.EFCore.Submission;
using Microsoft.EntityFrameworkCore;
using Submissions.Common;
using Submissions.Web.Areas.Lookups.Models;
using System.Linq;
using System.Web.Mvc;
using ZEFCore::Z.EntityFramework.Plus;

namespace Submissions.Web.Areas.Lookups.Controllers
{
	[AuthorizeUser(Permission = Permission.Admin, HasAccessRight = AccessRight.Edit)]
	public class ClassificationTypesController : Controller, ILookupDelete<int>
	{
		private readonly SubmissionContext _dbSubmission;

		public ClassificationTypesController(SubmissionContext dbSubmission)
		{
			_dbSubmission = dbSubmission;
		}

		public ActionResult Index()
		{
			var classificationTypes = _dbSubmission.tbl_lst_ClassificationTypes
				.AsNoTracking()
				.OrderBy(x => x.Active)
				.ThenBy(x => x.Name)
				.ProjectTo<ClassificationTypesViewModel>();

			return View(classificationTypes);
		}

		public ActionResult Edit(int? id, Mode mode)
		{
			var vm = new ClassificationTypesViewModel();
			vm.Active = true;

			if (mode == Mode.Edit)
			{
				var classificationTypes = _dbSubmission.tbl_lst_ClassificationTypes.Find(id);

				vm.Id = classificationTypes.Id;
				vm.Name = classificationTypes.Name;
			}
			vm.Mode = mode;

			return View(vm);
		}

		[HttpPost]
		public ActionResult Edit(int? id, ClassificationTypesViewModel vm)
		{
			var classificationTypes = new tbl_lst_ClassificationTypes();
			if (vm.Mode == Mode.Edit)
			{
				classificationTypes = _dbSubmission.tbl_lst_ClassificationTypes.Find(id);
			}

			classificationTypes.Id = vm.Id ?? 0;
			classificationTypes.Name = vm.Name;
			classificationTypes.Active = vm.Active;

			if (vm.Mode == Mode.Add)
			{
				_dbSubmission.tbl_lst_ClassificationTypes.Add(classificationTypes);
			}

			_dbSubmission.SaveChanges();
			CacheManager.Clear(LookupAjax.ClassificationTypes.ToString());

			return RedirectToAction("Index");
		}

		[HttpPost]
		public JsonResult CheckDelete(int id)
		{
			var classificationTypes = _dbSubmission.tbl_lst_ClassificationTypes.Find(id);
			var isInUse = _dbSubmission.tbl_lst_Classifications.Any(x => x.ClassificationTypeId == id) && classificationTypes.Active;

			return Json(isInUse);
		}

		[HttpPost]
		public ActionResult Delete(int id)
		{
			_dbSubmission.tbl_lst_ClassificationTypes.Where(x => x.Id == id).Delete();
			_dbSubmission.tbl_lst_Classifications.Where(x => x.ClassificationTypeId == id).Delete();

			CacheManager.Clear(LookupAjax.ClassificationTypes.ToString());

			return RedirectToAction("Index");
		}

		public JsonResult ValidateClassificationTypes(int? id, string name)
		{
			var validates = true;

			if (!string.IsNullOrEmpty(name) && _dbSubmission.tbl_lst_ClassificationTypes.Any(x => x.Id != id && x.Name == name))
			{
				validates = false;
			}
			return Json(validates, JsonRequestBehavior.AllowGet);
		}

		[HttpPost]
		public ActionResult SetActive(int id, Mode mode)
		{
			var active = (mode == Mode.Deactivate) ? false : true;

			var classificationTypes = _dbSubmission.tbl_lst_ClassificationTypes.Find(id);
			classificationTypes.Active = active;

			_dbSubmission.SaveChanges();

			CacheManager.Clear(LookupAjax.ClassificationTypes.ToString());

			return RedirectToAction("Index");

		}
	}
}