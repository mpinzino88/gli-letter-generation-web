﻿extern alias ZEFCore;
using GLI.EFCore.Submission;
using Microsoft.EntityFrameworkCore;
using Submissions.Common;
using Submissions.Web.Areas.Lookups.Models;
using System;
using System.Linq;
using System.Web.Mvc;
using ZEFCore::Z.EntityFramework.Plus;

namespace Submissions.Web.Areas.Lookups.Controllers
{
	public class BundlingGroupsController : Controller
	{
		private readonly SubmissionContext _dbSubmission;

		public BundlingGroupsController(SubmissionContext dbSubmission)
		{
			_dbSubmission = dbSubmission;
		}
		public ActionResult Index()
		{
			var bundlingGroups = _dbSubmission.tbl_lst_BundlingGroups
				.AsNoTracking()
				.Select(x => new
				{
					Id = x.Id,
					Name = x.Name,
					Description = x.Description,
					Jurisdictions = x.Jurisdictions.Select(y => new SelectListItem
					{
						Value = y.Id,
						Text = y.Name
					}).ToList(),
					JurisdictionsIndexDisplay = x.Jurisdictions.Select(y => y.Name + " (" + y.Id + ")").ToList()
				})
				.OrderBy(x => x.Name)
				.ToList()
				.Select(x => new BundlingGroupsViewModel
				{
					Id = x.Id,
					Name = x.Name,
					Description = x.Description,
					Jurisdictions = x.Jurisdictions,
					JurisdictionsIndexDisplay = String.Join(",<br/>", x.JurisdictionsIndexDisplay)
				})
				.ToList();

			return View(bundlingGroups);
		}

		public ActionResult Edit(int? id, Mode mode)
		{
			var vm = new BundlingGroupsViewModel();

			if (mode == Mode.Edit)
			{
				var bundlingGroup = _dbSubmission.tbl_lst_BundlingGroups
					.Where(x => x.Id == id)
					.SingleOrDefault();
				vm.Id = bundlingGroup.Id;
				vm.Name = bundlingGroup.Name;
				vm.Description = bundlingGroup.Description;
				vm.Jurisdictions = bundlingGroup.Jurisdictions
					.Select(x => new SelectListItem
					{
						Value = x.Id,
						Text = x.Name
					}).ToList();
				vm.JurisdictionIds = vm.Jurisdictions.Count > 0 ? vm.Jurisdictions.Select(x => x.Value).ToArray() : null;
			}
			vm.ExcludedJurisdictionIds = _dbSubmission.tbl_lst_fileJuris
					.Where(x => x.BundlingGroupId != null)
					.Select(x => x.Id)
					.ToList();

			vm.Mode = mode;

			return View(vm);
		}

		[HttpPost, AuthorizeUser(Permission = Permission.BundlingGroups, HasAccessRight = AccessRight.Edit)]
		public ActionResult Edit(BundlingGroupsViewModel vm)
		{
			var bundlingGroup = new tbl_lst_BundlingGroups();
			if (vm.Mode == Mode.Edit)
			{
				bundlingGroup = _dbSubmission.tbl_lst_BundlingGroups
					.Where(x => x.Id == vm.Id)
					.SingleOrDefault();
			}
			bundlingGroup.Id = vm.Id ?? 0;
			bundlingGroup.Name = vm.Name;
			bundlingGroup.Description = vm.Description;

			if (vm.Mode == Mode.Add)
			{
				_dbSubmission.tbl_lst_BundlingGroups.Add(bundlingGroup);
			}

			_dbSubmission.SaveChanges();

			SaveJurisdictions(bundlingGroup.Id, vm.JurisdictionIds);

			return RedirectToAction("Index");
		}

		private void SaveJurisdictions(int bundlingGroupId, string[] jurisdictionIds)
		{
			if (jurisdictionIds == null)
			{
				jurisdictionIds = new string[] { };
			}
			var jurisdictions = _dbSubmission.tbl_lst_fileJuris
				.Where(x => x.BundlingGroupId == bundlingGroupId || jurisdictionIds.Contains(x.Id))
				.ToList();

			foreach (var item in jurisdictions)
			{
				if (jurisdictionIds.Contains(item.Id))
				{
					item.BundlingGroupId = bundlingGroupId;
				}
				else
				{
					item.BundlingGroupId = null;
				}
			}

			_dbSubmission.SaveChanges();
		}

		[HttpPost, AuthorizeUser(Permission = Permission.BundlingGroups, HasAccessRight = AccessRight.Edit)]
		public JsonResult CheckDelete(int id)
		{
			return Json(false);
		}

		[HttpPost, AuthorizeUser(Permission = Permission.BundlingGroups, HasAccessRight = AccessRight.Edit)]
		public ActionResult Delete(int id)
		{
			SaveJurisdictions(id, null);
			_dbSubmission.tbl_lst_BundlingGroups.Where(x => x.Id == id).Delete();

			return RedirectToAction("Index");
		}

		public JsonResult ValidateBundlingGroupName(int? id, string name)
		{
			var validates = true;
			name = name.ToUpper().Trim();

			if (!string.IsNullOrEmpty(name) && _dbSubmission.tbl_lst_BundlingGroups.Any(x => x.Id != id && x.Name.ToUpper().Trim() == name))
			{
				validates = false;
			}

			return Json(validates, JsonRequestBehavior.AllowGet);
		}
	}
}