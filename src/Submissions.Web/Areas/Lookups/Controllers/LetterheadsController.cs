﻿extern alias ZEFCore;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using EF.GPS;
using GLI.EFCore.Submission;
using Microsoft.EntityFrameworkCore;
using Submissions.Common;
using Submissions.Web.Areas.Lookups.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ZEFCore::Z.EntityFramework.Plus;
using GPS = FUWUtil;

namespace Submissions.Web.Areas.Lookups.Controllers
{
	[AuthorizeUser(Permission = Permission.Admin, HasAccessRight = AccessRight.Read)]
	public class LetterheadsController : Controller
	{
		private readonly SubmissionContext _dbSubmission;

		private FUWUtil.FUWResult _uploadResult;

		public LetterheadsController(SubmissionContext dbSubmission)
		{
			_dbSubmission = dbSubmission;
			_uploadResult = new FUWUtil.FUWResult();
		}

		public ActionResult Index()
		{
			var letterheads = _dbSubmission.tbl_lst_Letterheads
				.AsNoTracking()
				.OrderByDescending(x => x.Active)
				.ThenBy(x => x.Code)
				.ProjectTo<Letterhead>()
				.ToList();

			var vm = new LetterheadsIndexViewModel { Letterheads = letterheads };

			return View(vm);
		}

		public ActionResult Edit(int? id, Mode mode)
		{
			var vm = new LetterheadsEditViewModel();
			vm.Active = true;

			if (mode == Mode.Edit)
			{
				var letterhead = _dbSubmission.tbl_lst_Letterheads.Find(id);

				vm = Mapper.Map<LetterheadsEditViewModel>(letterhead);

				var laboratories = _dbSubmission.tbl_lst_LetterheadLaboratories
					.Where(x => x.LetterheadId == vm.Id)
					.Select(x => new SelectListItem { Value = x.LabId.ToString(), Text = x.Lab.Location })
					.ToList();

				vm.LaboratoryIds = laboratories.Select(x => x.Value).ToArray();
				vm.Laboratories = laboratories;

				if (letterhead.GPSItemId == 0)
				{
					vm.GPSFilePath = "\\\\gaminglabs.net\\glimain\\Public\\";
					vm.GPSFileName = "No File in GPS";
				}
				else
				{
					var gpsItem = FUWUtil.FUWUtil.GetItemById(letterhead.GPSItemId);

					vm.GPSFilePath = gpsItem.EnsureNotUNCName(gpsItem.FullName);
					vm.GPSFileName = gpsItem.Name;
				}
			}
			vm.Mode = mode;

			return View(vm);
		}

		[HttpPost]
		public ActionResult Edit(int? id, LetterheadsEditViewModel vm)
		{
			var letterhead = new tbl_lst_Letterheads();
			if (vm.Mode == Mode.Edit)
			{
				letterhead = _dbSubmission.tbl_lst_Letterheads.Find(vm.Id);
			}

			letterhead.Id = vm.Id;
			letterhead.Active = vm.Active;
			letterhead.Code = vm.Code.Trim();
			letterhead.Description = vm.Description.Trim();

			if (vm.Mode == Mode.Add)
			{
				_dbSubmission.tbl_lst_Letterheads.Add(letterhead);
			}

			var laboratoryIds = vm.LaboratoryIds.Select(x => Convert.ToInt32(x)).ToList();

			_dbSubmission.SaveChanges();

			UpdateLaboratories(letterhead.Id, laboratoryIds);

			CacheManager.Clear(LookupAjax.LetterHeads.ToString());

			return RedirectToAction("Index");
		}

		[HttpPost]
		public JsonResult CheckDelete(int id)
		{
			var letterhead = _dbSubmission.tbl_lst_Letterheads.Find(id);

			var archiveLocation = "NJ";
			var gpsFileNum = new GPS.FileNumber(ComGlobals.GPSLetterheadsFileNumber);
			var gpsRoot = GPS.FUWDirTree.GenerateFUWTree(gpsFileNum, true, ref archiveLocation, false, false, true, true, true, false);
			var dir = gpsRoot.DescendantOrSelf(x => x.Name == ComGlobals.GPSLetterheadsDirectory);
			var isInUse = dir.AllItems.Any(x => x.Id == letterhead.GPSItemId);

			return Json(isInUse);
		}

		[HttpPost]
		public ActionResult Delete(int id)
		{
			_dbSubmission.tbl_lst_LetterheadLaboratories.Where(x => x.LetterheadId == id).Delete();
			_dbSubmission.tbl_lst_Letterheads.Where(x => x.Id == id).Delete();

			CacheManager.Clear(LookupAjax.LetterHeads.ToString());

			return RedirectToAction("Index");
		}

		[HttpPost]
		public ActionResult SetActive(int id, Mode mode)
		{
			bool active = (mode == Mode.Activate ? true : false);

			var letterhead = _dbSubmission.tbl_lst_Letterheads.Find(id);
			letterhead.Active = active;
			_dbSubmission.SaveChanges();

			CacheManager.Clear(LookupAjax.LetterHeads.ToString());

			return RedirectToAction("Index");
		}

		public JsonResult ValidateCode(int? id, string code, Mode mode)
		{
			var validates = true;

			if (!string.IsNullOrEmpty(code) && _dbSubmission.tbl_lst_Letterheads.Any(x => x.Id != id && x.Code == code))
			{
				validates = false;
			}

			return Json(validates, JsonRequestBehavior.AllowGet);
		}

		#region Helper Methods
		private void UpdateLaboratories(int letterheadId, List<int> laboratoryIds)
		{
			_dbSubmission.tbl_lst_LetterheadLaboratories.Where(x => x.LetterheadId == letterheadId).Delete();

			if (laboratoryIds.Count > 0)
			{
				foreach (var laboratoryId in laboratoryIds)
				{
					var letterheadLaboratory = new tbl_lst_LetterheadLaboratories { LetterheadId = letterheadId, LabId = laboratoryId };
					_dbSubmission.tbl_lst_LetterheadLaboratories.Add(letterheadLaboratory);
				}

				_dbSubmission.SaveChanges();
			}
		}

		public ActionResult AddLetterheadFile(int letterheadId, IList<HttpPostedFileBase> letterheadFileUpload)
		{
			if (letterheadId > 0)
			{
				var letterhead = _dbSubmission.tbl_lst_Letterheads.Find(letterheadId);
				var file = letterheadFileUpload.First();
				var fileName = letterhead.Code.Trim() + letterheadId + Path.GetExtension(file.FileName);

				var path = Path.Combine(PathUtil.Temp("Letterheads"), fileName);

				(new FileInfo(path)).Directory.Create();
				file.SaveAs(path);

				var archiveLocation = "NJ";
				var gpsFileNum = new GPS.FileNumber(ComGlobals.GPSLetterheadsFileNumber);
				var root = GPS.FUWDirTree.GenerateFUWTree(gpsFileNum, true, ref archiveLocation, false, false, true, true, true, false);
				var dir = root.DescendantOrSelf(x => x.Name == ComGlobals.GPSLetterheadsDirectory);

				var gpsFileInfo = new GPS.GPSFileInfo(path)
				{
					Action = GPS.ItemAction.UPLOADNEW
				};
				dir.EntriesToProcess.Add(gpsFileInfo);

				var gpsLabId = FUWUtil.FUWUtil.Laboratories.FirstOrDefault(lab => lab.Location == archiveLocation || lab.ArchiveLocation == archiveLocation).ID;
				_uploadResult = dir.ProcessAll(gpsLabId);

				var gpsItemId = 0;

				foreach (var subFile in _uploadResult.SubFiles)
				{
					foreach (var item in subFile.SubFiles)
					{
						foreach (var foo in item.UploadDir.Items)
						{
							if (foo.Name == fileName)
							{
								gpsItemId = foo.Id;
							}
						}
					}
				}

				letterhead.GPSItemId = gpsItemId;
				_dbSubmission.SaveChanges();
			}

			return Content("");
		}

		public ActionResult DeprecateLetterheadFile(int letterheadId, string file)
		{
			if (letterheadId > 0)
			{
				var letterhead = _dbSubmission.tbl_lst_Letterheads.Find(letterheadId);
				letterhead.Active = false;
				letterhead.GPSItemId = 0;
				_dbSubmission.SaveChanges();

				CacheManager.Clear(LookupAjax.LetterHeads.ToString());
			}

			return Content("");
		}
		#endregion
	}
}