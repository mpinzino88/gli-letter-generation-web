﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using EF.Submission;
using Newtonsoft.Json;
using Submissions.Common;
using Submissions.Core;
using Submissions.Web.Areas.Lookups.Models;
using Submissions.Web.Models.Grids.GamingGuidelines;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web.Mvc;
using Z.EntityFramework.Plus;

namespace Submissions.Web.Areas.Lookups.Controllers
{
	[AuthorizeUser(Permissions = new Permission[] { Permission.Admin, Permission.GamingGuideline }, HasAccessRight = AccessRight.Read)]
	public class GamingGuidelinesController : Controller, ILookupDelete<int>
	{
		private readonly ISubmissionContext _dbSubmission;
		private readonly IGamingGuidelineService _gamingGuidelineService;
		private readonly IUserContext _userContext;
		private readonly IQueryService _queryService;

		public GamingGuidelinesController(ISubmissionContext dbSubmission, IGamingGuidelineService gamingGuidelineService, IUserContext userContext, IQueryService queryService)
		{
			_dbSubmission = dbSubmission;
			_gamingGuidelineService = gamingGuidelineService;
			_userContext = userContext;
			_queryService = queryService;
		}
		public ActionResult Index()
		{
			var vm = new GamingGuidelineIndexViewModel();
			var userTemplate = _dbSubmission.GamingGuidelineUserTemplate.Where(x => x.UserId == _userContext.User.Id).SingleOrDefault() ?? new GamingGuidelineUserTemplate();
			vm.UserTemplate = userTemplate.Id == 0 ? new GamingGuidelineUserTemplateViewModel() : Mapper.Map<GamingGuidelineUserTemplateViewModel>(userTemplate);
			var grid = new GamingGuidelineGrid(userTemplate);
			vm.GamingGuidelineGrid = grid;
			ViewBag.ErrorMessage = TempData["ErrorMessage"];
			return View(vm);
		}
		[HttpPost]
		public ActionResult UpdateUserTemplate(GamingGuidelineUserTemplateViewModel userTemplate)
		{
			var existingTemplate = _dbSubmission.GamingGuidelineUserTemplate.SingleOrDefault(x => x.UserId == _userContext.User.Id);
			var mode = Mode.Edit;
			if (existingTemplate == null)
			{
				existingTemplate = new GamingGuidelineUserTemplate();
				mode = Mode.Add;
			}
			var templateId = mode == Mode.Edit ? existingTemplate.Id : 0;

			existingTemplate.UserId = _userContext.User.Id;
			UpdateTemplateModel(existingTemplate, userTemplate);

			if (mode == Mode.Add)
			{
				_dbSubmission.GamingGuidelineUserTemplate.Add(existingTemplate);
			}

			_dbSubmission.SaveChanges();

			return RedirectToAction("Index");
		}
		public ActionResult Edit(int id, Mode mode, int jurId = 0)
		{
			if (mode == Mode.Add)
			{
				if (_dbSubmission.tbl_lst_GamingGuidelines.Where(x => x.JurisdictionId == jurId.ToString()).Count() > 0)
				{
					TempData["ErrorMessage"] = "Can not create new Guideline for selected jurisdiction";
					return RedirectToAction("Index");
				}
				var gamingGuideline = new tbl_lst_GamingGuidelines
				{
					JurisdictionId = jurId.ToJurisdictionIdString()
				};
				_dbSubmission.tbl_lst_GamingGuidelines.Add(gamingGuideline);
				_dbSubmission.SaveChanges();
				var parsedJurId = jurId.ToJurisdictionIdString();
				id = _dbSubmission.tbl_lst_GamingGuidelines
					.Single(x => x.JurisdictionId == parsedJurId).Id;
			}

			GamingGuidelineViewModel vm;
			vm = _dbSubmission.tbl_lst_GamingGuidelines
				.Include(x => x.Jurisdiction)
				.Include("GamingGuidelineAttributes.GamingGuidelineAttribute")
				.Include("GamingGuidelineLimits.GamingGuidelineLimit")
				.Include("GamingGuidelineProhibitedGameTypes.GameType")
				.Include("GamingGuidelineProtocolTypes.ProtocolType")
				.Include("GamingGuidelineRTPRequirements.GameType")
				.Include("GamingGuidelineTechTypes.GamingGuidelineTechType")
				.ProjectTo<GamingGuidelineViewModel>()
				.SingleOrDefault(gg => gg.Id == id);

			if (vm == null)
			{
				TempData["ErrorMessage"] = string.Format("Could not find gaming guideline with id of {0}", id);
				return RedirectToAction("Index");
			}

			//The below is a bit hacky. Hydrating a whole Jurisdiction object causes circular-ref error on serialization, so just pull what we need out of it and destroy it:
			vm.JurisdictionId = vm.Jurisdiction.Id;
			vm.JurisdictionName = vm.Jurisdiction.Name;
			vm.Country = vm.Jurisdiction.Country.Description;
			vm.Jurisdiction = null;
			vm.UserComments = _dbSubmission.GamingGuidelineComment
				.Where(comment => comment.GamingGuidelineId == vm.Id)
				.Select(x => new GamingGuidelineCommentViewModel
				{
					Id = x.Id,
					AddedTime = x.AddedDate,
					CommentString = x.Comment,
					UserId = x.UserId,
					UserName = x.User.FName + " " + x.User.LName,
					IsEditable = x.UserId == _userContext.User.Id,
					_dirty = false
				}).ToList();
			vm.CommentCount = vm.UserComments.Count;
			vm.DefaultUserComment = new GamingGuidelineCommentViewModel
			{
				AddedTime = DateTime.Now,
				UserId = _userContext.User.Id,
				UserName = _userContext.User.FName + " " + _userContext.User.LName,
				CommentString = "",
				IsEditable = true
			};
			LoadEnumLists(vm);
			vm.Owner = _dbSubmission.trLogins
				.Where(x => x.Id == vm.OwnerId)
				.Select(x => x.FName + " " + x.LName)
				.SingleOrDefault();
			vm.SecondaryOwner = _dbSubmission.trLogins
				.Where(x => x.Id == vm.SecondaryOwnerId)
				.Select(x => x.FName + " " + x.LName)
				.SingleOrDefault();
			vm.Mode = mode;
			var compilationMethods = _gamingGuidelineService.LoadCompilationMethods(vm.Id);
			vm.CompilationMethodIds = compilationMethods.Count > 0 ? compilationMethods.Select(x => x.Value).ToArray() : null;
			vm.CompilationMethods = compilationMethods;
			vm.editAllowed = _gamingGuidelineService.EditAllowed(vm.OwnerId, vm.SecondaryOwnerId, _userContext.User.Id);
			return View(vm);
		}
		[HttpPost]
		public ActionResult Edit(int? id, string GamingGuidelineViewModel)
		{
			var vm = JsonConvert.DeserializeObject<GamingGuidelineViewModelThin>(GamingGuidelineViewModel);

			var gamingGuideline = new tbl_lst_GamingGuidelines();
			if (vm.Mode == Mode.Edit)
			{
				gamingGuideline = _dbSubmission.tbl_lst_GamingGuidelines.Find(id);
			}
			if (vm.Mode == Mode.Add)
			{
				gamingGuideline = _dbSubmission.tbl_lst_GamingGuidelines.SingleOrDefault(x => x.JurisdictionId == vm.JurisdictionId);
			}

			gamingGuideline.Id = vm.Id;
			if (gamingGuideline.JurisdictionId == null)
			{
				gamingGuideline.JurisdictionId = "0";
			}

			gamingGuideline.NonCompliantPaytableHandling = (int)vm.NonCompliantPaytableHandling;
			gamingGuideline.BonusPot = (int)vm.BonusPot;
			gamingGuideline.WirelessTechnology = (int)vm.WirelessTechnology;
			gamingGuideline.SkillGames = (int)vm.SkillGames;
			gamingGuideline.RandomNumberGenerator = (int)vm.RandomNumberGenerator;
			gamingGuideline.GeographicalTestingLocationOnly = (int)vm.GeographicalTestingLocationOnly;
			gamingGuideline.PhysicalTestingLocation = (int)vm.PhysicalTestingLocation;
			gamingGuideline.Portal = vm.Portal;
			gamingGuideline.SpecialHandling = vm.SpecialHandling;
			gamingGuideline.SourceCodeCompilation = vm.SourceCodeCompilation;
			gamingGuideline.Forensic = vm.Forensic;
			gamingGuideline.ProgressiveReconciliation = vm.ProgressiveReconciliation;
			gamingGuideline.OffensiveMaterialEvaluation = vm.OffensiveMaterialEvaluation;
			gamingGuideline.UnderageAdvertisingEvaluation = vm.UnderageAdvertisingEvaluation;
			gamingGuideline.ResponsibleGaming = vm.ResponsibleGaming;
			gamingGuideline.RemoteAccessITL = vm.RemoteAccessITL;
			gamingGuideline.RemoteAccess = vm.RemoteAccess;
			gamingGuideline.SpecialMathInstructions = vm.SpecialMathInstructions;
			gamingGuideline.Comments = vm.Comments;
			gamingGuideline.Active = vm.Active;
			gamingGuideline.NewTechnologyReviewNeeded = vm.NewTechnologyReviewNeeded;
			gamingGuideline.DocumentedRestrictionsOnSkill = vm.DocumentedRestrictionsOnSkill;
			gamingGuideline.CryptographicRNG = vm.CryptographicRNG;
			gamingGuideline.OwnerId = vm.OwnerId;
			gamingGuideline.SecondaryOwnerId = vm.SecondaryOwnerId;
			gamingGuideline.WebsiteLink = vm.WebsiteLink;
			UpdateGamingGuidelineCompilationMethod(vm);
			UpdateGamingGuidelineAttributes(vm);
			UpdateGamingGuidelineLimitTypes(vm);
			UpdateGamingGuidelineOddsRequirements(vm);
			UpdateGamingGuidelineProhibitedGameTypes(vm);
			UpdateGamingGuidelineProtocolTypes(vm);
			UpdateGamingGuidelineTechType(vm);
			UpdateGamingGuidelineRTPRequirements(vm);
			UpdateUserComments(vm);

			_dbSubmission.SaveChanges();

			CacheManager.Clear(LookupAjax.GamingGuidelines.ToString());
			CacheManager.Clear(LookupAjax.GamingGuidelineJurisdictions.ToString());
			CacheManager.Clear(LookupAjax.GamingGuidelineJurisdictionsSelected.ToString());

			return RedirectToAction("Index");
		}
		[HttpPost]
		public JsonResult InitializeMultiple(MultipleNameModel model)
		{
			foreach (var name in model.GuidelineNames)
			{
				CreateNewGuideline(model.JurId, name);
			}
			return Json(true);
		}
		[HttpPost]
		public JsonResult CheckDelete(int id)
		{
			var isInUse = _dbSubmission.tbl_lst_GamingGuidelines.Find(id).Active;
			return Json(isInUse);
		}
		[HttpPost]
		public ActionResult Delete(int id)
		{
			_dbSubmission.tbl_lst_GamingGuidelines.Where(x => x.Id == id).Delete();
			_dbSubmission.GamingGuidelineAttributes.Where(x => x.GamingGuidelineId == id).Delete();
			_dbSubmission.GamingGuidelineLimits.Where(x => x.GamingGuidelineId == id).Delete();
			_dbSubmission.GamingGuidelineOddsRequirement.Where(x => x.GamingGuidelineId == id).Delete();
			_dbSubmission.GamingGuidelineProhibitedGameTypes.Where(x => x.GamingGuidelineId == id).Delete();
			_dbSubmission.GamingGuidelineProtocolType.Where(x => x.GamingGuidelineId == id).Delete();
			_dbSubmission.GamingGuidelineRTPRequirements.Where(x => x.GamingGuidelineId == id).Delete();
			_dbSubmission.GamingGuidelineTechType.Where(x => x.GamingGuidelineId == id).Delete();

			if (_dbSubmission.tbl_lst_GamingGuidelines.Where(x => x.JurisdictionId == "-1").Count() > 0)
			{
				_dbSubmission.tbl_lst_GamingGuidelines.Where(x => x.JurisdictionId == "-1").Delete();
			}

			CacheManager.Clear(LookupAjax.GamingGuidelines.ToString());
			CacheManager.Clear(LookupAjax.GamingGuidelineJurisdictions.ToString());
			CacheManager.Clear(LookupAjax.GamingGuidelineJurisdictionsSelected.ToString());

			return RedirectToAction("Index");
		}
		public JsonResult ValidateGuideline(int? id, string jurisId)
		{
			var validated = true;

			if (!string.IsNullOrEmpty(jurisId) && _dbSubmission.tbl_lst_GamingGuidelines.Any(x => x.JurisdictionId == jurisId && x.Id != id))
			{
				validated = false;
			}

			return Json(validated, JsonRequestBehavior.AllowGet);
		}
		[HttpPost]
		public ActionResult SetActive(int? id, Mode mode)
		{
			var active = (mode == Mode.Deactivate) ? false : true;

			var gamingGuideline = _dbSubmission.tbl_lst_GamingGuidelines.Find(id);
			gamingGuideline.Active = active;

			_dbSubmission.SaveChanges();

			CacheManager.Clear(LookupAjax.GamingGuidelines.ToString());
			CacheManager.Clear(LookupAjax.GamingGuidelineJurisdictions.ToString());
			CacheManager.Clear(LookupAjax.GamingGuidelineJurisdictionsSelected.ToString());

			return RedirectToAction("Index");
		}
		public void Export()
		{
			var data = GetFormattedGridData();

			HttpContext.Response.Clear();
			HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename={0}", "GamingGuidelines.csv"));
			HttpContext.Response.ContentType = "application/vnd.ms-excel";

			var newLine = System.Environment.NewLine;
			var t = data[0].GetType();

			using (StringWriter sw = new StringWriter())
			{
				PropertyInfo[] props = t.GetProperties();
				var userTemplate = _dbSubmission.GamingGuidelineUserTemplate.SingleOrDefault(x => x.UserId == _userContext.User.Id);
				if (userTemplate == null)
				{
					userTemplate = Mapper.Map<GamingGuidelineUserTemplate>(new GamingGuidelineUserTemplateViewModel());
				}
				List<PropertyInfo> customProperty = RearrangeProps(props, userTemplate);


				foreach (var propInfo in customProperty)
				{
					var name = propInfo.Name;
					sw.Write(name.Replace("GamingGuideline", "") + ",");
				}
				sw.Write(newLine);

				foreach (var item in data)
				{
					foreach (var col in customProperty)
					{
						var type = item.GetType();
						var property = type.GetProperty(col.Name);
						var value = "";
						if (col.Name == "Jurisdiction")
						{
							value = _gamingGuidelineService.GetJurisPropValue(item.JurId);
						}
						else
						{
							try
							{
								value = property.GetValue(item).ToString();
							}
							catch (Exception)
							{
								value = "";
							}
						}
						if (value.Contains(","))
						{
							value = "\"" + value + "\"";
						}
						sw.Write(value + ",");
					}
					sw.Write(newLine);
				}
				HttpContext.Response.Write(sw.ToString());
				HttpContext.Response.End();
			}
		}
		#region Helper Methods
		private void UpdateGamingGuidelineCompilationMethod(GamingGuidelineViewModelThin vm)
		{
			_dbSubmission.GamingGuidelineCompilationMethod.Where(x => x.GamingGuidelineId == vm.Id).Delete();
			if (vm.CompilationMethodIds == null || vm.CompilationMethodIds.Count() == 0)
				return;
			foreach (var item in vm.CompilationMethodIds)
			{
				var gamingGuidelineCompilationMethod = new GamingGuidelineCompilationMethod
				{
					CompilationMethodId = Convert.ToInt32(item),
					GamingGuidelineId = vm.Id
				};
				_dbSubmission.GamingGuidelineCompilationMethod.Add(gamingGuidelineCompilationMethod);
			}
		}

		private void UpdateGamingGuidelineAttributes(GamingGuidelineViewModelThin vm)
		{
			_dbSubmission.GamingGuidelineAttributes.Where(x => x.GamingGuidelineId == vm.Id).Delete();

			if (vm.GamingGuidelineAttributeViewModels.Count == 0)
			{
				return;
			}
			foreach (var item in vm.GamingGuidelineAttributeViewModels.Where(ggavm => !ggavm._destroy && ggavm.AttributeId > 0))
			{
				var gamingGuidelineAttributeLink = new GamingGuidelineAttributes
				{
					AttributeId = item.AttributeId,
					AttributeStatusId = item.AttributeStatusId,
					GamingGuidelineId = vm.Id,
					Note = item.Note
				};
				_dbSubmission.GamingGuidelineAttributes.Add(gamingGuidelineAttributeLink);
			}
		}
		private void UpdateGamingGuidelineLimitTypes(GamingGuidelineViewModelThin vm)
		{
			_dbSubmission.GamingGuidelineLimits.Where(x => x.GamingGuidelineId == vm.Id).Delete();

			if (vm.GamingGuidelineLimitViewModels.Count == 0)
			{
				return;
			}

			foreach (var item in vm.GamingGuidelineLimitViewModels.Where(gglvm => !gglvm._destroy && gglvm.LimitTypeId > 0).ToList())
			{
				var newGamingGuidelineLimit = new GamingGuidelineLimits
				{
					GamingGuidelineId = vm.Id,
					Limit = item.Limit,
					GameTypeId = item.GameTypeId,
					LimitTypeId = item.LimitTypeId,
					xBet = item.xBet,
					Note = item.Note
				};
				_dbSubmission.GamingGuidelineLimits.Add(newGamingGuidelineLimit);
			}
		}
		private void UpdateGamingGuidelineOddsRequirements(GamingGuidelineViewModelThin vm)
		{
			_dbSubmission.GamingGuidelineOddsRequirement.Where(x => x.GamingGuidelineId == vm.Id).Delete();

			if (vm.GamingGuidelineOddsRequirementViewModels.Count == 0)
			{
				return;
			}

			foreach (var item in vm.GamingGuidelineOddsRequirementViewModels.Where(ggorvm => !ggorvm._destroy))
			{
				if (Int64.Parse(item.Odds, System.Globalization.NumberStyles.AllowThousands) != 0)
				{
					var newOddsRequirement = new GamingGuidelineOddsRequirements
					{
						GamingGuidelineId = (int)vm.Id,
						AwardTypeId = item.AwardTypeId,
						Note = item.Note,
						Odds = Int32.Parse(item.Odds, System.Globalization.NumberStyles.AllowThousands),
						PrizeTypeId = item.PrizeTypeId,
						ProgressiveTypeId = (item.AwardTypeId != 2) ? item.ProgressiveTypeId : 0
					};
					_dbSubmission.GamingGuidelineOddsRequirement.Add(newOddsRequirement);
				}

			}
		}
		private void UpdateGamingGuidelineProhibitedGameTypes(GamingGuidelineViewModelThin vm)
		{
			_dbSubmission.GamingGuidelineProhibitedGameTypes.Where(x => x.GamingGuidelineId == vm.Id).Delete();
			if (vm.GamingGuidelineProhibitedGameTypeViewModels.Count == 0)
			{
				return;
			}
			foreach (var item in vm.GamingGuidelineProhibitedGameTypeViewModels.Where(ggprovm => !ggprovm._destroy && ggprovm.GameTypeId > 0))
			{
				var gamingGuidelineProhibitedGameTypeLink = new GamingGuidelineProhibitedGameTypes
				{
					GamingGuidelineId = vm.Id,
					Notes = item.Note,
					ProhibitedGameTypeStatusId = item.ProhibitedStatusId,
					GameTypeId = item.GameTypeId
				};
				_dbSubmission.GamingGuidelineProhibitedGameTypes.Add(gamingGuidelineProhibitedGameTypeLink);
			}
		}
		private void UpdateGamingGuidelineTechType(GamingGuidelineViewModelThin vm)
		{
			_dbSubmission.GamingGuidelineTechType.Where(x => x.GamingGuidelineId == vm.Id).Delete();
			if (vm.GamingGuidelineTechTypesViewModels.Count == 0)
			{
				return;
			}
			foreach (var item in vm.GamingGuidelineTechTypesViewModels.Where(ggttvm => !ggttvm._destroy && ggttvm.TechTypeId > 0))
			{
				var gamingGuidelineTechTypeLink = new GamingGuidelineTechTypes
				{
					GamingGuidelineId = vm.Id,
					Notes = item.Note,
					TechTypeStatusId = item.TechTypeStatusId,
					TechTypeId = item.TechTypeId
				};
				_dbSubmission.GamingGuidelineTechType.Add(gamingGuidelineTechTypeLink);
			}
		}
		private void UpdateGamingGuidelineProtocolTypes(GamingGuidelineViewModelThin vm)
		{
			_dbSubmission.GamingGuidelineProtocolType.Where(x => x.GamingGuidelineId == vm.Id).Delete();

			if (vm.GamingGuidelineProtocolTypeViewModels.Count == 0)
			{
				return;
			}

			foreach (var item in vm.GamingGuidelineProtocolTypeViewModels.Where(ggpvm => !ggpvm._destroy && ggpvm.ProtocolTypeId > 0))
			{
				var gamingGuidelineProtocolTypeLink = new GamingGuidelineProtocolTypes
				{
					Version = item.Version,
					Note = item.Note,
					GamingGuidelineId = vm.Id,
					ProtocolTypeId = item.ProtocolTypeId
				};
				_dbSubmission.GamingGuidelineProtocolType.Add(gamingGuidelineProtocolTypeLink);
			}
		}
		private void UpdateGamingGuidelineRTPRequirements(GamingGuidelineViewModelThin vm)
		{
			_dbSubmission.GamingGuidelineRTPRequirements.Where(x => x.GamingGuidelineId == vm.Id).Delete();

			if (vm.GamingGuidelineRTPRequirementsViewModels.Count == 0)
			{
				return;
			}

			foreach (var item in vm.GamingGuidelineRTPRequirementsViewModels.Where(ggrtprvm => !ggrtprvm._destroy && ggrtprvm.GameTypeId > 0))
			{
				var gamingGuidelineRTPRequirementLink = new GamingGuidelineRTPRequirements
				{
					RTP = item.RTP,
					Note = item.Note,
					RTPType = item.RTPTypeId,
					GameTypeId = item.GameTypeId,
					GamingGuidelineId = vm.Id,
					DenomSymbol = item.DenomSymbol
				};
				_dbSubmission.GamingGuidelineRTPRequirements.Add(gamingGuidelineRTPRequirementLink);
			}
		}
		private void UpdateUserComments(GamingGuidelineViewModelThin vm)
		{
			CommentsToCreate(vm);
			CommentsToUpdate(vm);
			CommentsToDelete(vm);
		}
		/// <summary>
		/// Converts an Enum into a List of SelectListItems. Hydrates the "Value" field with the enum's int value and the "Text" field with it's Description.
		/// </summary>
		private List<SelectListItem> ProjectEnumIntoSelectList(Enum enumToConvert)
		{
			var enumType = enumToConvert.GetType();
			var enumValues = Enum.GetValues(enumType);
			return (from object enumValue in enumValues select new SelectListItem() { Value = ((int)enumValue).ToString(), Text = enumValue.GetEnumDescription() }).ToList();
		}
		private void LoadEnumLists(GamingGuidelineViewModel vm)
		{
			vm.NonCompliantPaytableHandlingOptions = ProjectEnumIntoSelectList(new GamingGuidelineNonCompliantPaytableHandling());
			vm.WirelessTechnologyOptions = ProjectEnumIntoSelectList(new GamingGuidelineAllowedProhibited());
			vm.SkillGamesOptions = ProjectEnumIntoSelectList(new GamingGuidelineAllowedProhibited());
			vm.BonusPotOptions = ProjectEnumIntoSelectList(new GamingGuidelineBonusPot());
			vm.RandomNumberGeneratorOptions = ProjectEnumIntoSelectList(new GamingGuidelineRandomNumberGenerator());
			vm.GeographicalTestingLocationOnlyOptions = ProjectEnumIntoSelectList(new GamingGuidelineGeographicalTestingLocationOnly());
			vm.PhysicalTestingLocationOptions = ProjectEnumIntoSelectList(new GamingGuidelinePhysicalTestingLocation());
			vm.GamingGuidelineAttributeStatus = ProjectEnumIntoSelectList(new GamingGuidelineAttributeStatus());
			vm.GamingGuidelineAwardTypes = ProjectEnumIntoSelectList(new GamingGuidelineAwardType());
			vm.GamingGuidelinePrizeTypes = ProjectEnumIntoSelectList(new GamingGuidelinePrizeType());
			vm.GamingGuidelineProgressiveTypes = ProjectEnumIntoSelectList(new GamingGuidelineProgressiveType());
			vm.GamingGuidelineProhibitedGameTypeStatus = ProjectEnumIntoSelectList(new GamingGuidelineProhibitedGameTypeStatus());
			vm.GamingGuidelineRTPType = ProjectEnumIntoSelectList(new GamingGuidelineRTPType());
			vm.GamingGuidelineTechTypeStatus = ProjectEnumIntoSelectList(new GamingGuidelineTechTypeStatus());
		}
		private void CreateNewGuideline(int JurisdictionId, string GuidelineName)
		{
			_gamingGuidelineService.CreateNewGuideline(JurisdictionId, GuidelineName);

			CacheManager.Clear(LookupAjax.GamingGuidelines.ToString());
			CacheManager.Clear(LookupAjax.GamingGuidelineJurisdictions.ToString());
			CacheManager.Clear(LookupAjax.GamingGuidelineJurisdictionsSelected.ToString());
		}
		private void UpdateTemplateModel(GamingGuidelineUserTemplate existingTemplate, GamingGuidelineUserTemplateViewModel userTemplate)
		{
			existingTemplate.AllAwardOR = userTemplate.AllAwardOR;
			existingTemplate.AnyAwardOR = userTemplate.AnyAwardOR;
			existingTemplate.AverageRTP = userTemplate.AverageRTP;
			existingTemplate.Comments = userTemplate.Comments;
			existingTemplate.ConditionalApproval = userTemplate.ConditionalApproval;
			existingTemplate.ConditionalRevocation = userTemplate.ConditionalRevocation;
			existingTemplate.Country = userTemplate.Country;
			existingTemplate.CreditLimit = userTemplate.CreditLimit;
			existingTemplate.DocumentedRestrictionsOnSkill = userTemplate.DocumentedRestrictionsOnSkill;
			existingTemplate.GAT = userTemplate.GAT;
			existingTemplate.Interop = userTemplate.Interop;
			existingTemplate.LinkedSmallEstablishments = userTemplate.LinkedSmallEstablishments;
			existingTemplate.LinkMaxWin = userTemplate.LinkMaxWin;
			existingTemplate.MaxBet = userTemplate.MaxBet;
			existingTemplate.MaxRTP = userTemplate.MaxRTP;
			existingTemplate.MaxWin = userTemplate.MaxWin;
			existingTemplate.MinBet = userTemplate.MinBet;
			existingTemplate.MinRTP = userTemplate.MinRTP;
			existingTemplate.MTGMMaxBet = userTemplate.MTGMMaxBet;
			existingTemplate.MTGMMaxWin = userTemplate.MTGMMaxWin;
			existingTemplate.NewTechnologyReviewNeeded = userTemplate.NewTechnologyReviewNeeded;
			existingTemplate.NonCompliantPaytableHandling = userTemplate.NonCompliantPaytableHandling;
			existingTemplate.BonusPot = userTemplate.BonusPot;
			existingTemplate.WirelessTechnology = userTemplate.WirelessTechnology;
			existingTemplate.SkillGames = userTemplate.SkillGames;
			existingTemplate.RandomNumberGenerator = userTemplate.RandomNumberGenerator;
			existingTemplate.GeographicalTestingLocationOnly = userTemplate.GeographicalTestingLocationOnly;
			existingTemplate.PhysicalTestingLocation = userTemplate.PhysicalTestingLocation;
			existingTemplate.CompilationMethod = userTemplate.CompilationMethod;
			existingTemplate.NotAllowedGames = userTemplate.NotAllowedGames;
			existingTemplate.NUStatus = userTemplate.NUStatus;
			existingTemplate.Portal = userTemplate.Portal;
			existingTemplate.ProgressiveCap = userTemplate.ProgressiveCap;
			existingTemplate.ProgressiveMaxWin = userTemplate.ProgressiveMaxWin;
			existingTemplate.ProtocolTesting = userTemplate.ProtocolTesting;
			existingTemplate.ProtocolValue = userTemplate.ProtocolValue;
			existingTemplate.RequiredSafetyCertifications = userTemplate.RequiredSafetyCertifications;
			existingTemplate.SAPMaxWin = userTemplate.SAPMaxWin;
			existingTemplate.SpecialHandling = userTemplate.SpecialHandling;
			existingTemplate.SourceCodeCompilation = userTemplate.SourceCodeCompilation;
			existingTemplate.Forensic = userTemplate.Forensic;
			existingTemplate.ProgressiveReconciliation = userTemplate.ProgressiveReconciliation;
			existingTemplate.OffensiveMaterialEvaluation = userTemplate.OffensiveMaterialEvaluation;
			existingTemplate.UnderageAdvertisingEvaluation = userTemplate.UnderageAdvertisingEvaluation;
			existingTemplate.ResponsibleGaming = userTemplate.ResponsibleGaming;
			existingTemplate.RemoteAccessITL = userTemplate.RemoteAccessITL;
			existingTemplate.RemoteAccess = userTemplate.RemoteAccess;
			existingTemplate.SpecialMathInstructions = userTemplate.SpecialMathInstructions;
			existingTemplate.TopAwardOR = userTemplate.TopAwardOR;
			existingTemplate.WAPMaxWin = userTemplate.WAPMaxWin;
			existingTemplate.WeightedRTP = userTemplate.WeightedRTP;
			existingTemplate.WinCap = userTemplate.WinCap;
			existingTemplate.NotAllowedTech = userTemplate.NotAllowedTech;
		}
		private List<PropertyInfo> RearrangeProps(PropertyInfo[] props, GamingGuidelineUserTemplate userTemplate)
		{
			List<PropertyInfo> customProperty = new List<PropertyInfo>();
			Type userTemplateType = userTemplate.GetType();
			customProperty.Add(props.Where(x => x.Name == "Jurisdiction").FirstOrDefault());
			foreach (var item in userTemplateType.GetProperties())
			{
				if (item.CanRead && item.PropertyType.Name == "Boolean")
				{
					var propToAdd = props.Where(x => x.Name == item.Name && (bool)item.GetValue(userTemplate)).FirstOrDefault();
					if (propToAdd != null)
					{
						customProperty.Add(propToAdd);
					}
				}
			}
			return customProperty;
		}
		public List<GamingGuidelineGridRecord> GetFormattedGridData()
		{
			var filter = new Web.Models.Query.GamingGuidelineSearch()
			{
				JurisdictionIds = new string[0]
			};
			var results = _queryService.GamingGuidelineSearch(filter)
				.Select(x => new GamingGuidelineGridRecord()
				{
					Id = x.Id,
					Jurisdiction = new
					{
						x.Id,
						x.Jurisdiction.Name,
						x.JurisdictionId,
						x.NonCompliantPaytableHandling,
						x.WirelessTechnology,
						x.SkillGames,
						x.BonusPot,
						x.RandomNumberGenerator,
						x.GeographicalTestingLocationOnly,
						x.PhysicalTestingLocation,
						x.SpecialMathInstructions,
						x.NewTechnologyReviewNeeded,
						x.CryptographicRNG
					},
					GamingGuidelineId = x.Id,
					SpecialHandling = x.SpecialHandling ? "Yes" : "No",
					SourceCodeCompilation = x.SourceCodeCompilation ? "Yes" : "No",
					Forensic = x.Forensic ? "Yes" : "No",
					ProgressiveReconciliation = x.ProgressiveReconciliation ? "Yes" : "No",
					OffensiveMaterialEvaluation = x.OffensiveMaterialEvaluation ? "Yes" : "No",
					UnderageAdvertisingEvaluation = x.UnderageAdvertisingEvaluation ? "Yes" : "No",
					ResponsibleGaming = x.ResponsibleGaming ? "Yes" : "No",
					RemoteAccessITL = x.RemoteAccessITL ? "Yes" : "No",
					RemoteAccess = x.RemoteAccess ? "Yes" : "No",
					Portal = x.Portal ? "Yes" : "No",
					NewTechnologyReviewNeeded = x.NewTechnologyReviewNeeded ? "Yes" : "No",
					SpecialMathInstructions = x.SpecialMathInstructions,
					Comments = x.Comments,
					DocumentedRestrictionsOnSkill = x.DocumentedRestrictionsOnSkill,
					NonCompliantPaytableHandling = ((GamingGuidelineNonCompliantPaytableHandling)x.NonCompliantPaytableHandling).ToString(),
					WirelessTechnology = ((GamingGuidelineAllowedProhibited)x.WirelessTechnology).ToString(),
					SkillGames = ((GamingGuidelineAllowedProhibited)x.SkillGames).ToString(),
					BonusPot = ((GamingGuidelineBonusPot)x.BonusPot).ToString(),
					RandomNumberGenerator = ((GamingGuidelineRandomNumberGenerator)x.RandomNumberGenerator).ToString(),
					GeographicalTestingLocationOnly = ((GamingGuidelineGeographicalTestingLocationOnly)x.GeographicalTestingLocationOnly).ToString(),
					PhysicalTestingLocation = ((GamingGuidelinePhysicalTestingLocation)x.PhysicalTestingLocation).ToString(),
					JurId = x.JurisdictionId,
					Country = x.Jurisdiction.Country.Description,
					Attributes = x.GamingGuidelineAttributes.ToList(),
					Limits = x.GamingGuidelineLimits.ToList(),
					OddsRequirements = x.GamingGuidelineOddsRequirements.ToList(),
					ProhibitedGameTypes = x.GamingGuidelineProhibitedGameTypes.ToList(),
					Protocols = x.GamingGuidelineProtocolTypes.ToList(),
					RTPRequirements = x.GamingGuidelineRTPRequirements.ToList(),
					TechnologyTypes = x.GamingGuidelineTechTypes.ToList()
				})
				.ToList();

			foreach (var item in results)
			{
				var jurid = int.Parse(item.JurId);
				item.SportsBettingId = _dbSubmission.GamingGuidelineSportsBetting
					.Where(x => x.JurisdictionId == jurid)
					.Select(x => x.Id)
					.FirstOrDefault();

				item.Jurisdiction = JsonConvert.SerializeObject(new
				{
					jur = item.Jurisdiction,
					sportbetting = item.SportsBettingId
				});
				//Attributes TODODD: replace strings with enum.GetDescription / whatever
				item.ConditionalApproval = _gamingGuidelineService.GetAttrValue(item.Attributes, "Conditional Approval");
				item.ConditionalRevocation = _gamingGuidelineService.GetAttrValue(item.Attributes, "Conditional Revocation");
				item.GAT = _gamingGuidelineService.GetAttrValue(item.Attributes, "GAT");
				item.Interop = _gamingGuidelineService.GetAttrValue(item.Attributes, "Interop");
				item.ProtocolTesting = _gamingGuidelineService.GetAttrValue(item.Attributes, "Protocol Testing");
				item.RequiredSafetyCertifications = _gamingGuidelineService.GetAttrValue(item.Attributes, "Required Safety Certifications (ex UL, CSA, etc)");
				item.NUStatus = _gamingGuidelineService.GetAttrValue(item.Attributes, "NU status");
				// limits
				item.CreditLimit = _gamingGuidelineService.GetLimValue(item.Limits, "Credit Limit");
				item.LinkMaxWin = _gamingGuidelineService.GetLimValue(item.Limits, "Link Max Win");
				item.LinkedSmallEstablishments = _gamingGuidelineService.GetLimValue(item.Limits, "Linked Small Establishments");
				item.MaxBet = _gamingGuidelineService.GetLimValue(item.Limits, "Max Bet");
				item.MaxWin = _gamingGuidelineService.GetLimValue(item.Limits, "Max Win");
				item.MinBet = _gamingGuidelineService.GetLimValue(item.Limits, "Min Bet");
				item.MTGMMaxBet = _gamingGuidelineService.GetLimValue(item.Limits, "MTGM Max Bet");
				item.MTGMMaxWin = _gamingGuidelineService.GetLimValue(item.Limits, "MTGM Max Win");
				item.ProgressiveCap = _gamingGuidelineService.GetLimValue(item.Limits, "Progressive Cap");
				item.ProgressiveMaxWin = _gamingGuidelineService.GetLimValue(item.Limits, "Progressive Max Win");
				item.SAPMaxWin = _gamingGuidelineService.GetLimValue(item.Limits, "SAP Max Win");
				item.WAPMaxWin = _gamingGuidelineService.GetLimValue(item.Limits, "WAP Max Win");
				item.WinCap = _gamingGuidelineService.GetLimValue(item.Limits, "Win Cap");
				//Odds
				item.AllAwardOR = _gamingGuidelineService.GetOdds(item.OddsRequirements, GamingGuidelinePrizeType.AllAward);
				item.AnyAwardOR = _gamingGuidelineService.GetOdds(item.OddsRequirements, GamingGuidelinePrizeType.AnyAward);
				item.TopAwardOR = _gamingGuidelineService.GetOdds(item.OddsRequirements, GamingGuidelinePrizeType.TopAward);
				//Prohibited gametypes
				item.NotAllowedGames = _gamingGuidelineService.GetProhibitedGames(item.ProhibitedGameTypes, GamingGuidelineProhibitedGameTypeStatus.NotAllowed);
				//RTP
				item.MaxRTP = _gamingGuidelineService.GetRTP(item.RTPRequirements, GamingGuidelineRTPType.Max);
				item.MinRTP = _gamingGuidelineService.GetRTP(item.RTPRequirements, GamingGuidelineRTPType.Min);
				item.AverageRTP = _gamingGuidelineService.GetRTP(item.RTPRequirements, GamingGuidelineRTPType.Avg);
				item.WeightedRTP = _gamingGuidelineService.GetRTP(item.RTPRequirements, GamingGuidelineRTPType.WtAvg);
				//Tech types
				item.NotAllowedTech = _gamingGuidelineService.GetTechType(item.TechnologyTypes, GamingGuidelineTechTypeStatus.NotAllowed);
				//Protocols
				item.ProtocolValue = string.Join(", <br/>", item.Protocols.Select(x => x.ProtocolType.Name + " (" + x.Version + ") "));
				//CompilationMethod
				item.CompilationMethod = _gamingGuidelineService.LoadAllCompilationMethods(item.GamingGuidelineId);
			}
			return results;
		}

		private void CommentsToCreate(GamingGuidelineViewModelThin vm)
		{
			var userCommentsToAdd = vm.UserComments.Where(x => !x._destroy && x.Id == 0 && x.IsEditable).ToList();
			foreach (var comments in userCommentsToAdd)
			{
				var newComment = new GamingGuidelineComment
				{
					AddedDate = DateTime.Now,
					Comment = comments.CommentString,
					GamingGuidelineId = vm.Id,
					UserId = comments.UserId
				};
				_dbSubmission.GamingGuidelineComment.Add(newComment);
			}
		}

		private void CommentsToUpdate(GamingGuidelineViewModelThin vm)
		{
			var userCommentsToModify = vm.UserComments.Where(x => x._dirty && x.Id != 0).ToList();
			foreach (var modifyComments in userCommentsToModify)
			{

				var comment = new GamingGuidelineComment();
				comment = _dbSubmission.GamingGuidelineComment.Find(modifyComments.Id);
				comment.Comment = modifyComments.CommentString;
				comment.AddedDate = DateTime.Now;
			}
		}

		private void CommentsToDelete(GamingGuidelineViewModelThin vm)
		{
			var userCommentsToDelete = vm.UserComments.Where(x => x._destroy && x.Id != 0).Select(x => x.Id).ToList();
			foreach (var toDelete in userCommentsToDelete)
			{
				_dbSubmission.GamingGuidelineComment.Where(x => x.Id == toDelete).Delete();
			}
		}
		#endregion
	}
}