﻿extern alias ZEFCore;
using GLI.EFCore.Submission;
using Submissions.Common;
using Submissions.Web.Areas.Lookups.Models;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using ZEFCore::Z.EntityFramework.Plus;

namespace Submissions.Web.Areas.Lookups.Controllers
{
	public class BillingSheetController : Controller
	{
		private readonly SubmissionContext _dbSubmission;

		public BillingSheetController(SubmissionContext submission)
		{
			_dbSubmission = submission;
		}

		// GET: Lookups/BillingSheet
		public ActionResult Index()
		{
			var vm = new BillingSheetIndexViewModel();
			return View(vm);
		}

		public ActionResult Edit(int? id)
		{
			var vm = new BillingSheetEditViewModel();
			vm.Mode = Mode.Add.ToString();
			vm.Active = true;

			if (id != null && id != 0)
			{
				var existingOptions = _dbSubmission.tbl_lst_BillingSheets.Where(x => x.Id == id).SingleOrDefault();
				vm.Id = existingOptions.Id;
				vm.Name = existingOptions.Name;
				vm.Description = existingOptions.Description;
				vm.RequiredSpecializedType = existingOptions.RequiredSpecializedType;
				vm.GetsGatChargeIfApplicable = existingOptions.GetsGatChargeIfApplicable;
				vm.RequiredSpecializedType = existingOptions.RequiredSpecializedType;
				vm.AppFeeId = existingOptions.AppFeeId;
				vm.AppFee = (existingOptions.AppFee != null) ? existingOptions.AppFee.Name : "";
				vm.BilledItem1AppFeeId = existingOptions.BilledItem1AppFeeId;
				vm.BilledItem1AppFee = (existingOptions.BilledItem1AppFee != null) ? existingOptions.BilledItem1AppFee.Name : "";
				vm.BilledItem2AppFeeId = existingOptions.BilledItem2AppFeeId;
				vm.BilledItem2AppFee = (existingOptions.BilledItem2AppFee != null) ? existingOptions.BilledItem2AppFee.Name : "";
				vm.BilledItem3AppFeeId = existingOptions.BilledItem3AppFeeId;
				vm.BilledItem3AppFee = (existingOptions.BilledItem3AppFee != null) ? existingOptions.BilledItem3AppFee.Name : "";
				vm.LabIds = existingOptions.Labs.Select(x => x.LabId).ToList();
				vm.Labs = existingOptions.Labs.Select(x => new SelectListItem() { Value = x.LabId.ToString(), Text = x.Lab.Location + " - " + x.Lab.Name, Selected = true }).ToList();

				vm.Active = existingOptions.Active;

				vm.Mode = Mode.Edit.ToString();
				vm.CanNameBeUpdated = !AnyLetterBookExists((int)id);
			}
			return View(vm);
		}

		[HttpPost]
		public ActionResult Edit(BillingSheetEditViewModel vm)
		{
			tbl_lst_BillingSheets billingSheet;
			if (vm.Id == 0)
			{
				billingSheet = new tbl_lst_BillingSheets();
			}
			else
			{
				billingSheet = _dbSubmission.tbl_lst_BillingSheets.Find(vm.Id);
			}

			billingSheet.Name = vm.Name;
			billingSheet.Description = vm.Description;
			billingSheet.GetsGatChargeIfApplicable = vm.GetsGatChargeIfApplicable;
			billingSheet.RequiredSpecializedType = vm.RequiredSpecializedType;
			billingSheet.AppFeeId = vm.AppFeeId;
			billingSheet.BilledItem1AppFeeId = vm.BilledItem1AppFeeId;
			billingSheet.BilledItem2AppFeeId = vm.BilledItem2AppFeeId;
			billingSheet.BilledItem3AppFeeId = vm.BilledItem3AppFeeId;
			billingSheet.Active = vm.Active;

			if (billingSheet.Id == 0)
			{
				_dbSubmission.tbl_lst_BillingSheets.Add(billingSheet);
			}
			_dbSubmission.SaveChanges();

			AddLabAssociation(billingSheet.Id, vm.Mode, vm.LabIds);

			return RedirectToAction("Index");
		}

		private void AddLabAssociation(int billingSheetId, string mode, List<int> labs)
		{
			if (mode == Mode.Edit.ToString())
			{
				_dbSubmission.tbl_BillingSheetsLabs
					.Where(x => x.BillingSheetId == billingSheetId)
					.Delete();
			}

			if (labs.Count > 0)
			{
				var newLabs = labs
							.Select(x => new tbl_BillingSheetsLab
							{
								BillingSheetId = billingSheetId,
								LabId = (int)x
							}).ToList();

				_dbSubmission.tbl_BillingSheetsLabs.AddRange(newLabs);
				_dbSubmission.SaveChanges();
			}
		}

		[HttpPost]
		public ActionResult CheckIfExists(BillingSheetEditViewModel vm)
		{
			var exists = _dbSubmission.tbl_lst_BillingSheets.Any(
				x => x.Id != vm.Id && x.Name == vm.Name);

			if (exists)
			{
				return Content("NOOK");
			}
			else
			{
				return Content("OK");
			}
		}

		public ContentResult Delete(int id)
		{
			var anyLetterBookExists = AnyLetterBookExists(id);
			if (!anyLetterBookExists)
			{
				_dbSubmission.LetterbookDefaultingOptions.Where(x => x.BillingSheetId == id).Update(x => new LetterbookDefaultingOption { BillingSheetId = null });
				_dbSubmission.tbl_lst_BillingSheets.Where(opt => opt.Id == id).Delete();

				_dbSubmission.tbl_BillingSheetsLabs
						.Where(x => x.BillingSheetId == id)
						.Delete();
				_dbSubmission.SaveChanges();

				return Content("OK");
			}
			else
			{
				return Content("NOOK");
			}
		}

		private bool AnyLetterBookExists(int billingSheetId)
		{
			var billingSheetName = _dbSubmission.tbl_lst_BillingSheets.Where(opt => opt.Id == billingSheetId).Select(x => x.Name).SingleOrDefault();
			var anyLetterBookExists = _dbSubmission.tbl_LetterBook.Where(x => x.BillingSheet == billingSheetName).Any();
			return (anyLetterBookExists) ? true : false;
		}

		public ContentResult BatchUpdateQARules(int oldBillingSheetId, int newBillingSheetId)
		{
			_dbSubmission.LetterbookDefaultingOptions.Where(x => x.BillingSheetId == oldBillingSheetId).Update(x => new LetterbookDefaultingOption() { BillingSheetId = newBillingSheetId });
			_dbSubmission.LetterbookDefaultingOptionsAvailBillingSheets.Where(x => x.BillingSheetId == oldBillingSheetId).Update(x => new LetterbookDefaultingOptionsAvailBillingSheets() { BillingSheetId = newBillingSheetId });
			return Content("ok");
		}


	}
}