﻿extern alias ZEFCore;
using AutoMapper.QueryableExtensions;
using GLI.EFCore.Submission;
using Microsoft.EntityFrameworkCore;
using Submissions.Common;
using Submissions.Web.Areas.Lookups.Models;
using System.Linq;
using System.Web.Mvc;
using ZEFCore::Z.EntityFramework.Plus;

namespace Submissions.Web.Areas.Lookups.Controllers
{
	[AuthorizeUser(Permission = Permission.Admin, HasAccessRight = AccessRight.Edit)]
	public class GameDescriptionStatusesController : Controller, ILookupDelete<int>
	{
		private readonly SubmissionContext _dbSubmission;

		public GameDescriptionStatusesController(SubmissionContext dbSubmission)
		{
			_dbSubmission = dbSubmission;
		}

		public ActionResult Index()
		{
			var descrtiptionStatuses = _dbSubmission.tbl_lst_GameDescriptionStatuses
				.AsNoTracking()
				.OrderBy(x => x.Id)
				.ProjectTo<GameDescriptionStatusViewModel>();

			return View(descrtiptionStatuses);
		}

		public ActionResult Edit(int? id, Mode mode)
		{
			var vm = new GameDescriptionStatusViewModel();

			if (mode == Mode.Edit)
			{
				var status = _dbSubmission.tbl_lst_GameDescriptionStatuses.Find(id);

				vm.Id = status.Id;
				vm.Name = status.Name;
			}
			vm.Mode = mode;

			return View(vm);
		}

		[HttpPost]
		public ActionResult Edit(int? id, GameDescriptionStatusViewModel vm)
		{
			var status = new tbl_lst_GameDescriptionStatuses();
			if (vm.Mode == Mode.Edit)
			{
				status = _dbSubmission.tbl_lst_GameDescriptionStatuses.Find(id);
			}

			status.Id = vm.Id ?? 0;
			status.Name = vm.Name;

			if (vm.Mode == Mode.Add)
			{
				_dbSubmission.tbl_lst_GameDescriptionStatuses.Add(status);
			}

			_dbSubmission.SaveChanges();

			CacheManager.Clear(LookupAjax.GameDescriptionStatuses.ToString());

			return RedirectToAction("Index");
		}

		[HttpPost]
		public JsonResult CheckDelete(int id)
		{
			// This may need to be expanded
			return Json(false);
		}

		[HttpPost]
		public ActionResult Delete(int id)
		{
			_dbSubmission.tbl_lst_GameDescriptionStatuses.Where(x => x.Id == id).Delete();
			_dbSubmission.SaveChanges();

			CacheManager.Clear(LookupAjax.GameDescriptionStatuses.ToString());

			return RedirectToAction("Index");
		}
	}
}