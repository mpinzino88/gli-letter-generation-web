﻿extern alias ZEFCore;
using AutoMapper.QueryableExtensions;
using GLI.EFCore.Submission;
using Microsoft.EntityFrameworkCore;
using Submissions.Common;
using Submissions.Web.Areas.Lookups.Models;
using System.Linq;
using System.Web.Mvc;
using ZEFCore::Z.EntityFramework.Plus;

namespace Submissions.Web.Areas.Lookups.Controllers
{
	[AuthorizeUser(Permission = Permission.Admin, HasAccessRight = AccessRight.Edit)]
	public class TranslationHousesController : Controller, ILookupDelete<int>
	{
		private readonly SubmissionContext _dbSubmission;
		private readonly IUserContext _userContext;

		public TranslationHousesController(SubmissionContext dbSubmission, IUserContext userContext)
		{
			_dbSubmission = dbSubmission;
			_userContext = userContext;
		}

		public ActionResult Index()
		{
			var translationHouses = _dbSubmission.tbl_lst_TranslationHouses
				.AsNoTracking()
				.OrderByDescending(x => x.Active)
				.ThenBy(x => x.Code)
				.ProjectTo<TranslationHousesViewModel>();

			return View(translationHouses);
		}

		public ActionResult Edit(int? id, Mode mode)
		{
			var vm = new TranslationHousesViewModel();
			vm.Active = true;

			if (mode == Mode.Edit)
			{
				var translationHouse = _dbSubmission.tbl_lst_TranslationHouses.Find(id);

				vm.Id = translationHouse.Id;
				vm.Code = translationHouse.Code;
				vm.Description = translationHouse.Description;
			}

			vm.Mode = mode;

			return View(vm);
		}

		[HttpPost]
		public ActionResult Edit(int? id, TranslationHousesViewModel vm)
		{
			var translationHouse = new tbl_lst_TranslationHouse();
			if (vm.Mode == Mode.Edit)
			{
				translationHouse = _dbSubmission.tbl_lst_TranslationHouses.Find(vm.Id);
			}

			translationHouse.Id = vm.Id ?? 0;
			translationHouse.Active = vm.Active;
			translationHouse.Code = vm.Code;
			translationHouse.Description = vm.Description;

			if (vm.Mode == Mode.Add)
			{
				_dbSubmission.tbl_lst_TranslationHouses.Add(translationHouse);
			}

			_dbSubmission.SaveChanges();

			CacheManager.Clear(LookupAjax.TranslationHouses.ToString());

			if (_userContext.User.Environment == Environment.Production)
			{
				CacheManager.AccessUpdateCache(LookupAjaxAccess.TranslationHouses);
			}

			return RedirectToAction("Index");
		}

		[HttpPost]
		public JsonResult CheckDelete(int id)
		{
			var isInUse = _dbSubmission.trProjectTranslationHouses.Any(x => x.TranslationHouseId == id);

			return Json(isInUse);
		}

		[HttpPost]
		public ActionResult Delete(int id)
		{
			_dbSubmission.tbl_lst_TranslationHouses.Where(x => x.Id == id).Delete();

			CacheManager.Clear(LookupAjax.TranslationHouses.ToString());

			if (_userContext.User.Environment == Environment.Production)
			{
				CacheManager.AccessUpdateCache(LookupAjaxAccess.TranslationHouses);
			}

			return RedirectToAction("Index");
		}

		[HttpPost]
		public ActionResult SetActive(int id, Mode mode)
		{
			bool active = mode == Mode.Activate ? true : false;

			var translationHouse = _dbSubmission.tbl_lst_TranslationHouses.Find(id);
			translationHouse.Active = active;
			_dbSubmission.SaveChanges();

			CacheManager.Clear(LookupAjax.TranslationHouses.ToString());

			if (_userContext.User.Environment == Environment.Production)
			{
				CacheManager.AccessUpdateCache(LookupAjaxAccess.TranslationHouses);
			}

			return RedirectToAction("Index");
		}

		public JsonResult ValidateCode(int? id, string code, Mode mode)
		{
			var validates = true;

			if (!string.IsNullOrEmpty(code) && _dbSubmission.tbl_lst_TranslationHouses.Any(x => x.Id != id && x.Code == code))
			{
				validates = false;
			}

			return Json(validates, JsonRequestBehavior.AllowGet);
		}
	}
}