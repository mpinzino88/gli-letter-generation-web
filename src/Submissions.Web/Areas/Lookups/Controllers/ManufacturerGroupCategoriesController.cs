﻿extern alias ZEFCore;
using AutoMapper.QueryableExtensions;
using GLI.EFCore.Submission;
using Submissions.Common;
using Submissions.Web.Areas.Lookups.Models;
using System;
using System.Linq;
using System.Web.Mvc;
using ZEFCore::Z.EntityFramework.Plus;

namespace Submissions.Web.Areas.Lookups.Controllers
{
	[AuthorizeUser(Permission = Permission.Admin, HasAccessRight = AccessRight.Edit)]
	public class ManufacturerGroupCategoriesController : Controller, ILookupDelete<int>
	{
		private readonly SubmissionContext _dbSubmission;

		public ManufacturerGroupCategoriesController(SubmissionContext dbSubmission)
		{
			_dbSubmission = dbSubmission;
		}

		public ActionResult Index()
		{
			var vm = new ManufacturerGroupCategoriesIndexViewModel();
			vm.ManufacturerGroupCategories = _dbSubmission.ManufacturerGroupCategories
				.OrderBy(x => x.Code)
				.ProjectTo<Models.ManufacturerGroupCategory>()
				.ToList();

			return View(vm);
		}

		public ActionResult Edit(int? id, Mode mode)
		{
			var vm = new ManufacturerGroupCategoriesEditViewModel();

			if (mode == Mode.Edit)
			{
				var category = _dbSubmission.ManufacturerGroupCategories.Find(id);
				vm.Id = category.Id;
				vm.Code = category.Code;
				vm.Description = category.Description;
			}

			vm.Mode = mode;

			return View(vm);
		}

		[HttpPost]
		public ActionResult Edit(ManufacturerGroupCategoriesEditViewModel vm)
		{
			var category = new ManufacturerGroupCategories();
			if (vm.Mode == Mode.Edit)
			{
				category = _dbSubmission.ManufacturerGroupCategories.Find(vm.Id);
			}

			category.Id = vm.Id ?? 0;
			category.Description = vm.Description;

			if (vm.Mode == Mode.Add)
			{
				category.Code = vm.Code;
				_dbSubmission.ManufacturerGroupCategories.Add(category);
			}

			_dbSubmission.SaveChanges();

			CacheManager.Clear(LookupAjax.ManufacturerGroupCategories.ToString());

			return RedirectToAction("Index");
		}

		[HttpPost]
		public JsonResult CheckDelete(int id)
		{
			var isInUse = _dbSubmission.ManufacturerGroups.Any(x => x.ManufacturerGroupCategoryId == id);

			return Json(isInUse);
		}

		public ActionResult Delete(int id)
		{
			_dbSubmission.ManufacturerGroupCategories.Where(x => x.Id == id).Delete();

			CacheManager.Clear(LookupAjax.ManufacturerGroupCategories.ToString());

			return RedirectToAction("Index");
		}

		public ActionResult SetActive(int id, Mode mode)
		{
			throw new NotSupportedException();
		}

		public JsonResult ValidateCode(int? id, string code, Mode mode)
		{
			var validates = true;

			if (!string.IsNullOrEmpty(code) && _dbSubmission.ManufacturerGroupCategories.Any(x => x.Id != id && x.Code == code))
			{
				validates = false;
			}

			return Json(validates, JsonRequestBehavior.AllowGet);
		}
	}
}
