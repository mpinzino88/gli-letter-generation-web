﻿extern alias ZEFCore;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using GLI.EFCore.Submission;
using Microsoft.EntityFrameworkCore;
using Submissions.Common;
using Submissions.Web.Areas.Lookups.Models;
using System.Linq;
using System.Web.Mvc;
using ZEFCore::Z.EntityFramework.Plus;

namespace Submissions.Web.Areas.Lookups.Controllers
{
	[AuthorizeUser(Permission = Permission.LetterGeneration, HasAccessRight = AccessRight.Edit)]
	public class TestEnvironmentController : Controller, ILookupDelete<int>
	{
		private readonly SubmissionContext _dbSubmission;

		public TestEnvironmentController(SubmissionContext dbSubmission)
		{
			_dbSubmission = dbSubmission;
		}

		public ActionResult Index()
		{
			var vm = new TestEnvironmentIndexViewModel();
			vm.TestEnvironments = _dbSubmission.tbl_lst_TestEnvironment
				.AsNoTracking()
				.OrderByDescending(x => x.Active)
				.ThenBy(x => x.GameEngine)
				.ProjectTo<TestEnvironment>()
				.ToList();

			return View(vm);
		}

		public ActionResult Edit(int? id, Mode mode)
		{
			var vm = new TestEnvironmentEditViewModel();
			vm.Active = true;

			if (mode == Mode.Edit)
			{
				var testEnvironment = _dbSubmission.tbl_lst_TestEnvironment.Find(id);

				vm = Mapper.Map<TestEnvironmentEditViewModel>(testEnvironment);
			}

			vm.Mode = mode;

			return View(vm);
		}

		[HttpPost]
		public ActionResult Edit(int? id, TestEnvironmentEditViewModel vm)
		{
			var testEnvironment = new tbl_lst_TestEnvironment();
			if (vm.Mode == Mode.Edit)
			{
				testEnvironment = _dbSubmission.tbl_lst_TestEnvironment.Find(vm.Id);
			}

			testEnvironment.Id = vm.Id ?? 0;
			testEnvironment.Active = vm.Active;
			testEnvironment.ManufacturerGroupId = vm.ManufacturerGroupId ?? 0;
			testEnvironment.JurisdictionId = vm.JurisdictionId;
			testEnvironment.PlatformId = vm.PlatformId ?? 0;
			testEnvironment.Jackpot = vm.Jackpot;
			testEnvironment.GameEngine = vm.GameEngine;
			testEnvironment.ServerLocation = vm.ServerLocation;
			testEnvironment.GameUrl = vm.GameUrl;
			testEnvironment.BackOfficeUrl = vm.BackOfficeUrl;
			testEnvironment.OtherUrl = vm.OtherUrl;
			testEnvironment.TestEnvironmentSystem = vm.TestEnvironmentSystem;
			testEnvironment.Simulator = vm.Simulator;
			testEnvironment.HelpRules = vm.HelpRules;
			testEnvironment.ForceTool = vm.ForceTool;
			testEnvironment.PDFAddedToLetter = vm.PDFAddedToLetter;

			if (vm.Mode == Mode.Add)
			{
				_dbSubmission.tbl_lst_TestEnvironment.Add(testEnvironment);
			}

			_dbSubmission.SaveChanges();

			return RedirectToAction("Index");
		}

		[HttpPost]
		public JsonResult CheckDelete(int id)
		{
			//will need to be changed once table is actually used
			//var isInUse = _dbSubmission.tbl_lst_TestEnvironment.Any(x => x.Id == id);

			return Json(false);
		}

		[HttpPost]
		public ActionResult Delete(int id)
		{
			_dbSubmission.tbl_lst_TestEnvironment.Where(x => x.Id == id).Delete();

			return RedirectToAction("Index");
		}

		[HttpPost]
		public ActionResult SetActive(int id, Mode mode)
		{
			bool active = (mode == Mode.Activate ? true : false);

			var testEnvironment = _dbSubmission.tbl_lst_TestEnvironment.Find(id);
			testEnvironment.Active = active;
			_dbSubmission.SaveChanges();

			return RedirectToAction("Index");
		}
	}
}