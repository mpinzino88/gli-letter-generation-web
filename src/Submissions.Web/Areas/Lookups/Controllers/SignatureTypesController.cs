﻿extern alias ZEFCore;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using GLI.EFCore.Submission;
using Microsoft.EntityFrameworkCore;
using Submissions.Common;
using Submissions.Web.Areas.Lookups.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using ZEFCore::Z.EntityFramework.Plus;
using SignatureType = Submissions.Web.Areas.Lookups.Models.SignatureType;

namespace Submissions.Web.Areas.Lookups.Controllers
{
	[AuthorizeUser(Permission = Permission.Admin, HasAccessRight = AccessRight.Edit)]
	public class SignatureTypesController : Controller, ILookupDelete<int>
	{
		private readonly SubmissionContext _dbSubmission;

		public SignatureTypesController(SubmissionContext dbSubmission)
		{
			_dbSubmission = dbSubmission;
		}

		public ActionResult Index()
		{
			var vm = new SignatureTypesIndexViewModel();
			vm.SignatureTypes = _dbSubmission.tbl_lst_SignatureTypes.Include(x => x.DefaultVersion)
				.AsNoTracking()
				.OrderByDescending(x => x.Active)
				.ThenBy(x => x.Type)
				.ProjectTo<SignatureType>()
				.ToList();

			return View(vm);
		}

		public ActionResult Edit(int? id, Mode mode)
		{
			var vm = new SignatureTypesEditViewModel();
			vm.Active = true;

			if (mode == Mode.Edit)
			{
				var signatureType = _dbSubmission.tbl_lst_SignatureTypes.Find(id);

				vm = Mapper.Map<SignatureTypesEditViewModel>(signatureType);

				var versions = _dbSubmission.tbl_SignatureType_Versions.Include(x => x.Version)
					.Where(x => x.SignatureTypeId == vm.Id)
					.Select(x => new SelectListItem { Value = x.SignatureVersionId.ToString(), Text = x.Version.Version })
					.ToList();

				vm.AvailableVersionIds = versions.Select(x => x.Value).ToArray();
				vm.AvailableVersions = versions;
			}

			vm.Mode = mode;

			return View(vm);
		}

		[HttpPost]
		public ActionResult Edit(int? id, SignatureTypesEditViewModel vm)
		{
			var signatureType = new tbl_lst_SignatureTypes();
			if (vm.Mode == Mode.Edit)
			{
				signatureType = _dbSubmission.tbl_lst_SignatureTypes.Find(vm.Id);
			}

			signatureType.Id = vm.Id ?? 0;
			signatureType.Active = vm.Active;
			signatureType.DefaultVersionId = vm.DefaultVersionId;
			signatureType.Length = vm.Length;
			signatureType.Type = vm.Type;

			if (vm.Mode == Mode.Add)
			{
				signatureType.SignatureGUID = Guid.NewGuid();
				_dbSubmission.tbl_lst_SignatureTypes.Add(signatureType);
			}

			var availableVersionIds = vm.AvailableVersionIds == null ? null : vm.AvailableVersionIds.Select(x => Convert.ToInt32(x)).ToList();
			if (availableVersionIds != null && !availableVersionIds.Contains((int)vm.DefaultVersionId))
			{
				availableVersionIds.Add((int)vm.DefaultVersionId);
			}

			_dbSubmission.SaveChanges();


			UpdateAvailableVersions(signatureType.Id, availableVersionIds);

			CacheManager.Clear(LookupAjax.SignatureTypes.ToString());

			return RedirectToAction("Index");
		}

		[HttpPost]
		public JsonResult CheckDelete(int id)
		{
			var isInUse = _dbSubmission.tblSignatures.Any(x => x.TypeOfId == id);

			return Json(isInUse);
		}

		[HttpPost]
		public ActionResult Delete(int id)
		{
			_dbSubmission.tbl_SignatureType_Versions.Where(x => x.SignatureTypeId == id).Delete();
			_dbSubmission.tbl_lst_SignatureTypes.Where(x => x.Id == id).Delete();

			CacheManager.Clear(LookupAjax.SignatureTypes.ToString());

			return RedirectToAction("Index");
		}

		[HttpPost]
		public ActionResult SetActive(int id, Mode mode)
		{
			bool active = (mode == Mode.Activate ? true : false);

			var signatureType = _dbSubmission.tbl_lst_SignatureTypes.Find(id);
			signatureType.Active = active;
			_dbSubmission.SaveChanges();

			CacheManager.Clear(LookupAjax.SignatureTypes.ToString());

			return RedirectToAction("Index");
		}

		public JsonResult ValidateType(int? id, string type, Mode mode)
		{
			var validates = true;

			if (!string.IsNullOrEmpty(type) && _dbSubmission.tbl_lst_SignatureTypes.Any(x => x.Id != id && x.Type == type))
			{
				validates = false;
			}

			return Json(validates, JsonRequestBehavior.AllowGet);
		}

		#region Helper Methods
		private void UpdateAvailableVersions(int signatureTypeId, List<int> availableVersionIds)
		{
			//TODO: Bulk Delete
			//_dbSubmission.tbl_SignatureType_Versions.Where(x => x.SignatureTypeId == signatureTypeId).Delete();
			_dbSubmission.RemoveRange(_dbSubmission.tbl_SignatureType_Versions.Where(x => x.SignatureTypeId == signatureTypeId));
			_dbSubmission.SaveChanges();

			if (availableVersionIds != null && availableVersionIds.Count > 0)
			{
				foreach (var versionId in availableVersionIds)
				{
					var signatureVersion = new tbl_SignatureType_Versions { SignatureTypeId = signatureTypeId, SignatureVersionId = versionId };
					_dbSubmission.tbl_SignatureType_Versions.Add(signatureVersion);
				}

				_dbSubmission.SaveChanges();
			}
		}
		#endregion
	}
}
