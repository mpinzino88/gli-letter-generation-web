﻿using AutoMapper;
using EF.Submission;
using Submissions.Common;
using Submissions.Core.Interfaces;
using Submissions.Web.Areas.Lookups.Models;
using System.Web.Mvc;

namespace Submissions.Web.Areas.Lookups.Controllers
{
	[AuthorizeUser(Permission = Permission.LetterGeneration, HasAccessRight = AccessRight.Edit)]
	public class MobileDevicesController : Controller, ILookupDelete<int>
	{
		private readonly IMobileDeviceService _mobileDeviceService;

		public MobileDevicesController(IMobileDeviceService mobileServiceDevice)
		{
			_mobileDeviceService = mobileServiceDevice;
		}

		[HttpGet]
		public ActionResult Index()
		{
			var vm = new MobileDevicesIndexViewModel();
			ViewBag.Title = vm.Title;
			return View(vm);
		}

		[HttpGet]
		public ActionResult Edit(int? id, Mode mode)
		{
			MobileDevicesEditViewModel vm;
			if (mode == Mode.Add)
			{
				vm = new MobileDevicesEditViewModel();
			}
			else if (mode == Mode.Edit)
			{
				var entity = _mobileDeviceService.GetMobileDevice((int)id);
				vm = Mapper.Map<MobileDevicesEditViewModel>(entity);
			}
			else
			{
				return RedirectToAction("Index");
			}
			vm.Mode = mode;

			return View(vm);
		}

		[HttpPost]
		public ActionResult Edit(MobileDevicesEditViewModel vm)
		{

			var mobileDevice = new tbl_lst_MobileDevice();

			mobileDevice = ApplyChanges(mobileDevice, vm);

			if (vm.Mode == Mode.Add)
			{
				_mobileDeviceService.SaveMobileDevice(mobileDevice);
			}
			else if (vm.Mode == Mode.Edit)
			{
				_mobileDeviceService.UpdateMobileDevice(mobileDevice);
			}

			CacheManager.Clear(LookupAjax.MobileDevices.ToString());

			return RedirectToAction("Index");
		}

		[HttpPost]
		public JsonResult CheckDelete(int id)
		{
			return Json(false);
		}

		[HttpPost]
		public ActionResult Delete(int id)
		{
			_mobileDeviceService.DeleteMobileDevice(id);

			CacheManager.Clear(LookupAjax.MobileDevices.ToString());

			return RedirectToAction("Index");
		}

		#region Helper Methods
		private tbl_lst_MobileDevice ApplyChanges(tbl_lst_MobileDevice mobileDevice, MobileDevicesEditViewModel vm)
		{
			mobileDevice.Id = vm.Id;
			mobileDevice.Device = vm.Device;
			mobileDevice.Browser = vm.Browser;
			mobileDevice.Model = vm.Model;
			mobileDevice.OperatingSystem = vm.OperatingSystem;
			mobileDevice.Type = vm.DeviceType.ToString();
			mobileDevice.DeviceId = vm.DeviceId;
			mobileDevice.LocationId = vm.LocationId;
			return mobileDevice;
		}
		#endregion
	}
}