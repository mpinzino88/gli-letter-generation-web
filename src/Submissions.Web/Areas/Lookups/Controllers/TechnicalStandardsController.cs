﻿extern alias ZEFCore;
using EF.eResultsManaged;
using GLI.EFCore.Submission;
using Newtonsoft.Json;
using Submissions.Common;
using Submissions.Web.Areas.Lookups.Models;
using Submissions.Web.Areas.Tools.Models;
using System;
using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;
using ZEFCore::Z.EntityFramework.Plus;

namespace Submissions.Web.Areas.Lookups.Controllers
{
	public class TechnicalStandardsController : Controller
	{
		private readonly SubmissionContext _dbSubmission;
		private readonly IeResultsManagedContext _dbEResultsManaged;
		private readonly IUserContext _userContext;

		public TechnicalStandardsController
		(
			SubmissionContext dbSubmission,
			IeResultsManagedContext dbEResultsManaged,
			IUserContext userContext
		)
		{
			_dbSubmission = dbSubmission;
			_userContext = userContext;
			_dbEResultsManaged = dbEResultsManaged;
		}

		public ActionResult Index()
		{
			var vm = new TechnicalStandardViewModel();
			return View(vm);
		}

		[AuthorizeUser(Permission = Permission.LetterGeneration, HasAccessRight = AccessRight.Edit)]
		public ActionResult Edit(int? id, Mode mode)
		{
			var vm = new TechnicalStandardViewModel();
			vm.Active = true;

			if (mode == Mode.Edit)
			{
				var standard = _dbSubmission.tbl_lst_TechnicalStandards
					.Include(tech => tech.Jurisdictions)
					.Include(tech => tech.Jurisdictions.Select(j => j.Jurisdiction))
					.Include(tech => tech.tbl_lst_EPROMFunctions)
					.Include(tech => tech.tbl_lst_EPROMFunctions.Select(f => f.tbl_lst_EPROMFunction))
					.Include(tech => tech.FunctionalRoles)
					.Include(tech => tech.Translations)
					.Include(tech => tech.Documents)
					.First(tech => tech.Id == id.Value);
				vm.Id = standard.Id;
				vm.OriginalId = standard.OriginalId;
				vm.Mandatory = standard.Mandatory;
				vm.OrderId = standard.OrderId;
				vm.Result = standard.Result;
				vm.Description = standard.Description;
				vm.Jurisdictions = standard.Jurisdictions.Select(juri => new JurisdictionInfo
				{
					Jurisdiction = juri.Jurisdiction.Name,
					JurisdictionId = juri.Jurisdiction.FixId
				}).ToList();
				var funcRoleIds = standard.FunctionalRoles.Select(fr => fr.FunctionalRoleId).ToList();
				vm.FunctionalRoles = _dbEResultsManaged.FunctionalRole.Where(fr => funcRoleIds.Contains(fr.Id)).Select(fr =>
				new Models.FunctionalRole
				{
					Id = fr.Id,
					Name = fr.Name,
					Description = fr.Description
				}).ToList();
				vm.Functions = standard.tbl_lst_EPROMFunctions.Select(f => new Models.Function
				{
					Id = f.tbl_lst_EPROMFunction.Id,
					Code = f.tbl_lst_EPROMFunction.Code
				}).ToList();
				vm.AllFunctions = vm.Functions.Count == 0;
				vm.AllJurisdictions = vm.Jurisdictions.Count == 0;
				vm.Translations = standard.Translations.Select(t => new TechnicalStandardTranslation
				{
					Id = t.Id,
					Description = t.Description,
					Language = t.Language,
					Result = t.Result
				}).ToList();
				var docIds = standard.Documents.Select(x => x.DocumentId).ToList();
				vm.EvolutionDocuments = _dbEResultsManaged.Document.Where(x => docIds.Contains(x.Id)).Select(x => new Models.EvolutionDocument
				{
					Id = x.Id,
					Text = x.Name
				}).ToList();
				vm.AllFunctionalRoles = vm.FunctionalRoles.Count == 0 && vm.EvolutionDocuments.Count == 0;
			}
			vm.Mode = mode;

			return View(vm);
		}

		[HttpPost, AuthorizeUser(Permission = Permission.LetterGeneration, HasAccessRight = AccessRight.Edit)]
		public ActionResult Edit(string editViewModelString)
		{
			var savedTemplate = JsonConvert.DeserializeObject<TechnicalStandardViewModel>(editViewModelString);

			tbl_lst_TechnicalStandards saveNew = BuildTechStandard(savedTemplate);
			_dbSubmission.tbl_lst_TechnicalStandards.Add(saveNew);
			_dbSubmission.SaveChanges();

			if (savedTemplate.Mode == Mode.Add)
			{
				saveNew.OriginalId = saveNew.Id;
				_dbSubmission.SaveChanges();
			}
			else
			{
				DeactivateStandard(savedTemplate.Id);
			}
			return RedirectToAction("Index");

		}

		private void DeactivateStandard(int id)
		{
			var deativateStandard = _dbSubmission.tbl_lst_TechnicalStandards.Find(id);
			deativateStandard.DeactivateDate = DateTime.UtcNow;
			deativateStandard.DeactivateUserId = _userContext.User.Id;
			deativateStandard.Lifecycle = "DEACTIVATED";
			_dbSubmission.SaveChanges();
		}

		private tbl_lst_TechnicalStandards BuildTechStandard(TechnicalStandardViewModel viewModel)
		{
			return new tbl_lst_TechnicalStandards
			{
				AddDate = DateTime.UtcNow,
				AddUserId = _userContext.User.Id,
				Description = viewModel.Description,
				Lifecycle = "ACTIVE",
				Mandatory = viewModel.Mandatory,
				OrderId = viewModel.OrderId,
				OriginalId = viewModel.OriginalId,
				Result = viewModel.Result,
				FunctionalRoles = viewModel.FunctionalRoles.Select(fr => new tbl_lst_TechnicalStandardsFunctionalRole
				{
					FunctionalRoleId = fr.Id
				}).ToList(),
				Jurisdictions = viewModel.Jurisdictions.Select(fr => new tbl_lst_TechnicalStandardstbl_lst_fileJuris
				{
					JurisdictionId = fr.JurisdictionId.ToJurisdictionIdString()
				}).ToList(),
				tbl_lst_EPROMFunctions = viewModel.Functions.Select(f => new tbl_lst_TechnicalStandardstbl_lst_EPROMFunction
				{
					tbl_lst_EPROMFunctionId = f.Id
				}).ToList(),
				Translations = viewModel.Translations.Select(t => new tbl_lst_TechnicalStandardsTranslation
				{
					Description = t.Description,
					Language = t.Language,
					Result = t.Result
				}).ToList(),
				Documents = viewModel.EvolutionDocuments.Select(t => new tbl_lst_TechnicalStandardsDocument
				{
					DocumentId = t.Id
				}).ToList()
			};
		}

		[HttpPost, AuthorizeUser(Permission = Permission.LetterGeneration, HasAccessRight = AccessRight.Edit)]
		public ActionResult SetActive(int id, Mode mode)
		{
			DeactivateStandard(id);

			return RedirectToAction("Index");
		}
	}
}