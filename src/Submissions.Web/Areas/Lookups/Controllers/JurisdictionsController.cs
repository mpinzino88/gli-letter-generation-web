extern alias ZEFCore;
using AutoMapper;
using EF.GLIExtranet;
using GLI.EFCore.Submission;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Submissions.Core.Interfaces;
using Submissions.Web.Areas.Lookups.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Web.Mvc;
using ZEFCore::Z.EntityFramework.Plus;

namespace Submissions.Web.Areas.Lookups.Controllers
{
	using Submissions.Common;
	using Submissions.Common.Email;
	using Submissions.Web.Utility.Email;

	[AuthorizeUser(Permission = Permission.Admin, HasAccessRight = AccessRight.Edit)]
	public class JurisdictionsController : Controller, ILookupDelete<string>
	{
		private readonly SubmissionContext _dbSubmission;
		private readonly IGLIExtranetContext _dbGLIExtranet;
		private readonly IEmailService _emailService;
		private readonly IUserContext _userContext;
		private readonly ISharepointService _sharePointService;
		private readonly IGLIExtranetContext _dbExtranet;

		public JurisdictionsController
		(
			SubmissionContext dbSubmission,
			IGLIExtranetContext dbGLIExtranet,
			IEmailService emailService,
			IUserContext userContext,
			ISharepointService sharePointService,
			IGLIExtranetContext dbExtranet
		)
		{
			_dbSubmission = dbSubmission;
			_dbGLIExtranet = dbGLIExtranet;
			_emailService = emailService;
			_userContext = userContext;
			_sharePointService = sharePointService;
			_dbExtranet = dbExtranet;
		}

		#region Main
		public ActionResult Index()
		{
			var vm = new JurisdictionsIndexViewModel();
			return View(vm);
		}

		public ActionResult Edit(string id, Mode mode)
		{
			var vm = new JurisdictionsEditViewModel();

			if (mode == Mode.Add)
			{
				vm.Id = (_dbSubmission.tbl_lst_fileJuris.Max(juris => juris.FixId) + 1).ToString();
			}
			else
			{
				var jurisdiction = _dbSubmission.tbl_lst_fileJuris.Find(id);
				vm = Mapper.Map<JurisdictionsEditViewModel>(jurisdiction);

				if (vm.CountryId != null)
				{
					vm.Country.Add(new SelectListItem { Value = jurisdiction.CountryId.ToString(), Text = jurisdiction.Country.Code + " - " + jurisdiction.Country.Description });
				}

				var liaisonUsers = LoadLiaisons(id);
				vm.LiaisonUserIds = liaisonUsers.Count > 0 ? liaisonUsers.Select(x => x.Value).ToArray() : null;
				vm.LiaisonUsers = liaisonUsers;

				vm.SignatureScopes = LoadSignatureScopes(id);

				var billingLabs = LoadBillingLabs(jurisdiction.LabsForBilling);
				vm.BillingLabIds = billingLabs.Count > 0 ? billingLabs.Select(x => x.Value).ToArray() : null;
				vm.BillingLabs = billingLabs;

				var manufacturers = LoadManufacturers(jurisdiction.Manufacturer);
				vm.ManufacturerCodes = manufacturers.Count > 0 ? manufacturers.Select(x => x.Value).ToArray() : null;
				vm.Manufacturers = manufacturers;


				var qaOffice = _dbSubmission.QAOfficeDefaults.Where(x => x.JurisdictionId == vm.Id).Select(x => new { x.QAOfficeId, QAOffice = x.QAOffice.Location }).SingleOrDefault();

				if (qaOffice != null)
				{
					vm.QAOfficeDefaultId = qaOffice.QAOfficeId;
					vm.QAOfficeDefaults.Add(new SelectListItem { Value = qaOffice.QAOfficeId.ToString(), Text = qaOffice.QAOffice, Selected = true });
				}
			}

			vm.Mode = mode;

			return View(vm);
		}

		[HttpPost]
		public ActionResult Edit(string id, string JurisdictionsEditViewModel)
		{
			var vm = JsonConvert.DeserializeObject<JurisdictionsEditViewModel>(JurisdictionsEditViewModel);

			var jurisdiction = new tbl_lst_fileJuris();
			var oldJurisdictionName = "";

			if (vm.Mode == Mode.Edit)
			{
				jurisdiction = _dbSubmission.tbl_lst_fileJuris.Find(id);
				oldJurisdictionName = jurisdiction.Name;
			}

			jurisdiction = ApplyChanges(jurisdiction, vm);

			if (vm.Mode == Mode.Add)
			{
				_dbSubmission.tbl_lst_fileJuris.Add(jurisdiction);
			}

			_dbSubmission.UserId = _userContext.User.Id;
			_dbSubmission.SaveChanges();

			UpdateQADefaultRule(vm);

			UpdateLiaisons(vm);
			UpdateA1Data(vm);
			UpdateSignatureScopes(vm);
			UpdateAccess(vm);

			if (!string.IsNullOrEmpty(oldJurisdictionName) && (jurisdiction.Name != oldJurisdictionName))
			{
				_dbSubmission.JurisdictionalData
					.Where(x => x.JurisdictionName == oldJurisdictionName)
					.Update(x => new JurisdictionalData { JurisdictionName = jurisdiction.Name });
			}

			if (vm.SendEmail)
			{
				EmailAccounting(vm.Id, vm.QMSLink);
			}

			CacheManager.Clear(LookupAjax.Jurisdictions.ToString());

			if (_userContext.User.Environment == Environment.Production)
			{
				CacheManager.AccessUpdateCache(LookupAjaxAccess.ExtranetJurisdictions);
				CacheManager.AccessUpdateCache(LookupAjaxAccess.UserJurisdictions);
			}

			return RedirectToAction("Index");
		}

		public ActionResult SetActive(string id, Mode mode)
		{
			bool active = (mode == Mode.Activate) ? true : false;

			var jurisdiction = _dbSubmission.tbl_lst_fileJuris.Find(id);

			if (active && !jurisdiction.PendingApproval)
			{
				// Inactive -- > Active
				jurisdiction.Active = active;
			}
			else if (active && jurisdiction.PendingApproval)
			{
				// Pending -- > Active
				jurisdiction.Active = active;
				jurisdiction.PendingApproval = false;
			}
			else if (!active && !jurisdiction.PendingApproval)
			{
				// Active -- > Inactive
				jurisdiction.Active = false;
			}

			_dbSubmission.SaveChanges();

			CacheManager.Clear(LookupAjax.Jurisdictions.ToString());

			if (_userContext.User.Environment == Environment.Production)
			{
				CacheManager.AccessUpdateCache(LookupAjaxAccess.ExtranetJurisdictions);
				CacheManager.AccessUpdateCache(LookupAjaxAccess.UserJurisdictions);
			}

			return RedirectToAction("Index");
		}

		[HttpPost]
		public ActionResult Delete(string id)
		{
			var isInUse = _dbSubmission.JurisdictionalData.Any(x => x.JurisdictionId == id);
			if (!isInUse)
			{
				var jurisdictionName = _dbSubmission.tbl_lst_fileJuris.Where(x => x.Id == id).Select(x => x.Name).Single();
				_dbSubmission.UserId = _userContext.User.Id;
				_dbSubmission.tbl_lst_fileJuris.Where(x => x.Id == id).Delete();
				_dbSubmission.A1FindJurisdictionNamefrom_Let.Where(x => x.JurisdictionId == id).Delete();
				_dbSubmission.tbl_lst_Sig_Scope_Juris.Where(x => x.JurisdictionId == id).Delete();
				_dbSubmission.tbl_Liaisons.Where(x => x.JurisdictionId == id).Delete();

				// TODO: ZEFCore Delete
				var extranetJurisdiction = new tblJurisdiction { Id = id };
				_dbGLIExtranet.Entry(extranetJurisdiction).State = System.Data.Entity.EntityState.Deleted;
				_dbGLIExtranet.SaveChanges();

				CacheManager.Clear(LookupAjax.Jurisdictions.ToString());

				if (_userContext.User.Environment == Environment.Production)
				{
					CacheManager.AccessUpdateCache(LookupAjaxAccess.ExtranetJurisdictions);
					CacheManager.AccessUpdateCache(LookupAjaxAccess.UserJurisdictions);
				}
			}
			return RedirectToAction("Index");
		}

		[HttpPost]
		public JsonResult CheckDelete(string id)
		{
			var intId = Int32.Parse(id);
			var isInUse = _dbSubmission.JurisdictionalData.Any(x => x.JurisdictionId == id) || _dbExtranet.tblUserJurisdictions.Any(x => x.ExtranetJurisdictionId == intId);
			return Json(isInUse);
		}

		public JsonResult GetRowCountForUpdate(string originalJurisdictionName)
		{
			return Json(_dbSubmission.JurisdictionalData.Where(x => x.JurisdictionName == originalJurisdictionName).Count());
		}

		public ActionResult EmailJurisdictionActivation(string id)
		{
			var jurisdiction = _dbSubmission.tbl_lst_fileJuris.Where(x => x.Id == id).Single();
			if (jurisdiction.PendingApproval)
			{
				jurisdiction.PendingApproval = false;
				jurisdiction.Active = true;
				_dbSubmission.UserId = _userContext.User.Id;
				_dbSubmission.SaveChanges();
				EmailJuriActivatedNotificationToIT(jurisdiction);
				_sharePointService.CreateJurisdictionTermSet(jurisdiction.Id, jurisdiction.Name);
			}

			return RedirectToAction("Index");
		}
		#endregion

		#region Emails

		private void EmailJuriActivatedNotificationToIT(tbl_lst_fileJuris jurisdiction)
		{
			var emailInfo = new AddJurisdictionEmail
			{
				Id = jurisdiction.Id,
				Jurisdiction = jurisdiction.Name,
				jurisdictionType = jurisdiction.JurisdictionType
			};

			var subject = "New Jurisdiction Activated in Dynamics: " + emailInfo.Id + "-" + emailInfo.Jurisdiction;
			SendEmail(EmailType.JurisdictionActivatedNotification, subject, emailInfo);
		}

		private void EmailAccounting(string id, string QMSLink)
		{
			var jurisdiction = _dbSubmission.tbl_lst_fileJuris
				.Where(x => x.Id == id)
				.Select(x => new
				{
					x.Manufacturer,
					x.LabsForBilling,
					x.Name,
					Type = x.JurisdictionType
				})
				.Single();

			var emailInfo = new AddJurisdictionEmail
			{
				Id = id,
				Jurisdiction = jurisdiction.Name,
				jurisdictionType = jurisdiction.Type,
				QMSLink = QMSLink
			};

			if (!String.IsNullOrWhiteSpace(jurisdiction.Manufacturer))
			{
				emailInfo.pendingManufacturerRequests = jurisdiction.Manufacturer.Split(',').ToList();
			}

			if (!String.IsNullOrWhiteSpace(jurisdiction.LabsForBilling))
			{
				var billingLabIds = jurisdiction.LabsForBilling.Split(',').Select(int.Parse).ToList();

				var labNames = _dbSubmission.trLaboratories
					.Where(x => billingLabIds.Contains(x.Id))
					.Select(x => x.Name)
					.ToList();

				emailInfo.pendingLabsForBilling = labNames;
			}

			var subject = "New Jurisdiction Request for Dynamics: " + emailInfo.Id + "-" + emailInfo.Jurisdiction;
			SendEmail(EmailType.AddJurisdictionDynamics, subject, emailInfo);
		}

		private EmailResponse SendEmail(EmailType email, string subject, AddJurisdictionEmail emailInfo)
		{
			var gliGlobe = Server.MapPath("~/Img/GLIGlobe.gif");
			_emailService.EmbeddedImages = new List<LinkedResource> { new LinkedResource(gliGlobe) { ContentId = "GLIGlobe" }, };
			var emailType = email.ToString();
			var emailTypeId = (from x in _dbSubmission.EmailTypes
							   where x.Code == email.ToString()
							   select x.Id).SingleOrDefault();

			var recipients = new List<string>();

			if (email == EmailType.AddJurisdictionDynamics)
			{
				recipients = _dbSubmission
					.EmailSubscriptions
					.Where(x => x.EmailTypeId == emailTypeId)
					.Select(x => x.User.Email)
					.ToList();
			}
			else if (email == EmailType.AddJurisdictionProcurement)
			{
				recipients.Add("procurement@gaminglabs.com");
			}
			else if (email == EmailType.JurisdictionActivatedNotification)
			{
				recipients = _dbSubmission
					.EmailSubscriptions
					.Where(x => x.EmailTypeId == emailTypeId)
					.Select(x => x.User.Email)
					.ToList();
				recipients.Add("NJProgrammers@gaminglabs.com");
			}

			var mailMsg = new MailMessage();
			var fromId = _userContext.User.Id;
			mailMsg.From = new MailAddress((from x in _dbSubmission.trLogins
											where x.Id == fromId
											select x.Email).Single());
			mailMsg.CC.Add(new MailAddress("NJProgrammers@gaminglabs.com"));
			foreach (var recipient in recipients) { mailMsg.To.Add(new MailAddress(recipient)); }
			mailMsg.Subject = subject;
			mailMsg.Body = _emailService.RenderTemplate(email.ToString(), emailInfo);

			return _emailService.Send(mailMsg);
		}
		#endregion

		#region Helper Methods
		private tbl_lst_fileJuris ApplyChanges(tbl_lst_fileJuris jurisdiction, JurisdictionsEditViewModel vm)
		{
			jurisdiction.Id = vm.Id;
			jurisdiction.Name = vm.Name;
			jurisdiction.FixId = Convert.ToInt16(vm.Id);
			jurisdiction.AvailForElectReq = vm.AvailForElectReq;
			jurisdiction.AltName = vm.AltName;
			jurisdiction.CountryId = vm.CountryId;
			jurisdiction.SubCode = vm.SubCode;
			jurisdiction.Email = vm.Email;
			jurisdiction.UseDraftLetters = vm.UseDraftLetters;
			jurisdiction.Comments = vm.Comments;
			jurisdiction.FormattedJurisdictionName = vm.FormattedJurisdictionName;
			jurisdiction.JurisdictionType = vm.JurisdictionType;
			jurisdiction.Manufacturer = vm.ManufacturerCodes == null ? null : string.Join(",", vm.ManufacturerCodes);
			jurisdiction.LabsForBilling = vm.BillingLabIds == null ? null : string.Join(",", vm.BillingLabIds);
			jurisdiction.ARIScheduleC = vm.ARIScheduleC;
			jurisdiction.RestrictedUse = Convert.ToInt32(vm.RestrictedUse);
			jurisdiction.NationCode = vm.NationCode == null ? null : TruncateNation(vm.NationCode);
			jurisdiction.IsGAT = vm.IsGAT;
			jurisdiction.TestingPerformedAvailable = vm.TestingPerformedAvailable;

			if (vm.Mode == Mode.Add)
			{
				jurisdiction.AddDate = DateTime.Now;
				jurisdiction.AddUserId = _userContext.User.Id;
				jurisdiction.Active = false;
				jurisdiction.PendingApproval = true;
			}
			else
			{
				jurisdiction.EditDate = DateTime.Now;
				jurisdiction.EditUserId = _userContext.User.Id;
				jurisdiction.Active = vm.Active;

				if (vm.Active)
				{
					jurisdiction.PendingApproval = false;
				}
			}

			return jurisdiction;
		}

		private string TruncateNation(string nation)
		{
			var doesStringContain = (nation == null) ? false : nation.ToLowerInvariant().Contains('-');
			return ((doesStringContain) ? nation.Split('-')[1] : nation);
		}

		private List<SelectListItem> LoadBillingLabs(string labratoryIds)
		{
			if (labratoryIds == null)
			{
				return new List<SelectListItem>();
			}

			var labIds = labratoryIds.Split(',').Select(int.Parse).ToList();

			return _dbSubmission.trLaboratories
				.Where(x => labIds.Contains(x.Id))
				.Select(x => new SelectListItem { Value = x.Id.ToString(), Text = x.Name })
				.OrderBy(x => x.Text)
				.ToList();
		}

		private List<SelectListItem> LoadLiaisons(string jurisdictionId)
		{
			return _dbSubmission.tbl_Liaisons.Include(x => x.User)
				.Where(x => x.JurisdictionId == jurisdictionId)
				.Select(x => new SelectListItem { Value = x.UserId.ToString(), Text = x.User.FName + " " + x.User.LName })
				.OrderBy(x => x.Text)
				.ToList();
		}

		private List<SelectListItem> LoadManufacturers(string manufacturerCodes)
		{
			if (manufacturerCodes == null)
			{
				return new List<SelectListItem>();
			}

			var manufCodes = manufacturerCodes.Split(',').ToList();

			return _dbSubmission.tbl_lst_fileManu
				.Where(x => manufCodes.Contains(x.Code))
				.Select(x => new SelectListItem { Value = x.Code, Text = x.Description })
				.OrderBy(x => x.Text)
				.ToList();
		}

		private IList<SignatureScope> LoadSignatureScopes(string id)
		{
			var signatureScopes = _dbSubmission.tbl_lst_Sig_Scope_Juris
				.Where(x => x.JurisdictionId == id)
				.Select(x => new
				{
					x.Id,
					ScopeId = x.SigScopeId,
					x.Scope.Scope,
					TypeId = x.SigTypeId,
					x.Type.Type
				})
				.ToList()
				.Select(x => new SignatureScope
				{
					Id = x.Id,
					ScopeId = x.ScopeId,
					Scope = new List<SelectListItem> { new SelectListItem { Value = x.ScopeId.ToString(), Text = x.Scope } },
					TypeId = x.TypeId,
					Type = new List<SelectListItem> { new SelectListItem { Value = x.TypeId.ToString(), Text = x.Type } }
				})
				.OrderBy(x => x.Type.First().Text)
				.ToList();

			return signatureScopes;
		}

		private void UpdateA1Data(JurisdictionsEditViewModel vm)
		{
			if (vm.A1Data.Count == 0)
			{
				return;
			}

			var A1DataIdsToDelete = vm.A1Data.Where(x => x._destroy == true && x.Id != null).Select(x => x.Id).ToList();
			if (A1DataIdsToDelete.Count > 0)
			{
				_dbSubmission.A1FindJurisdictionNamefrom_Let.Where(x => A1DataIdsToDelete.Contains(x.Id)).Delete();
			}

			foreach (var item in vm.A1Data.Where(x => x._destroy == false).ToList())
			{
				var dbA1Data = _dbSubmission.A1FindJurisdictionNamefrom_Let
					.Where(x => x.Id == item.Id)
					.SingleOrDefault();

				if (dbA1Data == null)
				{
					var newA1Data = new A1FindJurisdictionNamefrom_Let
					{
						Id = 0,
						JurisdictionId = vm.Id,
						Field4Typ = item.Field4Typ,
						Field5Dir = item.Field5Dir,
						Field6Juris = item.Field6Juris
					};

					_dbSubmission.A1FindJurisdictionNamefrom_Let.Add(newA1Data);
				}
				else
				{
					dbA1Data.Field4Typ = item.Field4Typ;
					dbA1Data.Field5Dir = item.Field5Dir;
					dbA1Data.Field6Juris = item.Field6Juris;
				}

				_dbSubmission.SaveChanges();
			}
		}

		private void UpdateSignatureScopes(JurisdictionsEditViewModel vm)
		{
			if (vm.SignatureScopes.Count == 0)
			{
				return;
			}

			var signatureScopeIdsToDelete = vm.SignatureScopes.Where(x => x._destroy == true).Select(x => x.Id).ToList();
			if (signatureScopeIdsToDelete.Count > 0)
			{
				_dbSubmission.tbl_lst_Sig_Scope_Juris.Where(x => signatureScopeIdsToDelete.Contains(x.Id)).Delete();
			}

			foreach (var item in vm.SignatureScopes.Where(x => x._destroy == false).ToList())
			{
				var dbSignatureScope = _dbSubmission.tbl_lst_Sig_Scope_Juris
					.Where(x => x.Id == item.Id)
					.SingleOrDefault();

				if (dbSignatureScope == null)
				{
					var newSignatureScope = new tbl_lst_Sig_Scope_Juris
					{
						JurisdictionId = vm.Id,
						SigScopeId = item.ScopeId,
						SigTypeId = item.TypeId
					};

					_dbSubmission.tbl_lst_Sig_Scope_Juris.Add(newSignatureScope);
				}
				else
				{
					dbSignatureScope.SigScopeId = item.ScopeId;
					dbSignatureScope.SigTypeId = item.TypeId;
				}

				_dbSubmission.SaveChanges();
			}
		}

		private void UpdateLiaisons(JurisdictionsEditViewModel vm)
		{
			_dbSubmission.tbl_Liaisons.Where(x => x.JurisdictionId == vm.Id).Delete();
			if (vm.LiaisonUserIds != null)
			{
				foreach (var userId in vm.LiaisonUserIds)
				{
					if (!string.IsNullOrEmpty(userId))
					{
						var liaison = new tbl_Liaison { JurisdictionId = vm.Id, UserId = Convert.ToInt32(userId), };
						_dbSubmission.tbl_Liaisons.Add(liaison);
					}
				}

				_dbSubmission.SaveChanges();
			}
		}

		private void UpdateAccess(JurisdictionsEditViewModel vm)
		{
			var accessJurisdiction = _dbGLIExtranet.tblJurisdictions.Where(x => x.Id == vm.Id).SingleOrDefault();

			if (accessJurisdiction == null)
			{
				accessJurisdiction = new tblJurisdiction { Id = vm.Id, Name = vm.Name };
				_dbGLIExtranet.tblJurisdictions.Add(accessJurisdiction);
			}
			else
			{
				accessJurisdiction.Name = vm.Name;
			}

			_dbGLIExtranet.SaveChanges();
		}
		private void UpdateQADefaultRule(JurisdictionsEditViewModel vm)
		{
			var qaOfficeDefaultRule = _dbSubmission.QAOfficeDefaults.Where(x => x.JurisdictionId == vm.Id).SingleOrDefault();
			if (qaOfficeDefaultRule == null)
			{
				if (vm.QAOfficeDefaultId != null)
				{
					qaOfficeDefaultRule = new QAOfficeDefaults
					{
						JurisdictionId = vm.Id,
						QAOfficeId = (int)vm.QAOfficeDefaultId
					};
					_dbSubmission.QAOfficeDefaults.Add(qaOfficeDefaultRule);
				}
			}
			else
			{
				if (vm.QAOfficeDefaultId != null)
				{
					qaOfficeDefaultRule.QAOfficeId = (int)vm.QAOfficeDefaultId;
				}
				else
				{
					_dbSubmission.QAOfficeDefaults.Remove(qaOfficeDefaultRule);
				}

			}
			_dbSubmission.SaveChanges();
		}
		#endregion
	}
}