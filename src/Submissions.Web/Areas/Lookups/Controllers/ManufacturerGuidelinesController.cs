﻿using GLI.EFCore.Submission;
using Submissions.Web.Areas.Lookups.Models;
using System.Linq;
using System.Web.Mvc;

namespace Submissions.Web.Areas.Lookups.Controllers
{
	public class ManufacturerGuidelinesController : Controller
	{
		private readonly SubmissionContext _dbSubmission;

		public ManufacturerGuidelinesController(SubmissionContext dbSubmission)
		{
			_dbSubmission = dbSubmission;
		}
		public ActionResult Index()
		{
			var vm = new ManufacturerGuidelinesIndexViewModel();
			return View(vm);
		}

		[HttpGet]
		public JsonResult GetRows(string manufacturerId)
		{
			var results = _dbSubmission.ManufacturerGuidelines.Where(x => x.ManufacturerId == manufacturerId)
				.Select(x => new ManufacturerGuidelinesRow
				{
					Id = x.Id,
					Jurisdiction = x.GamingGuideline.Jurisdiction.Name,
					ManufacturerId = x.ManufacturerId,
					MaxRTP = x.MaxRTP,
					MinRTP = x.MinRTP
				}).ToList();

			return Json(results, JsonRequestBehavior.AllowGet);
		}
		public ActionResult Edit(string manufacturerId)
		{
			var manufacturerName = _dbSubmission.tbl_lst_fileManu.Find(manufacturerId).Description;
			var vm = new ManufacturerGuidelinesEditViewModel
			{
				Rows = _dbSubmission.ManufacturerGuidelines.Where(x => x.ManufacturerId == manufacturerId)
					.Select(x => new ManufacturerGuidelinesRow
					{
						GamingGuidelineId = (int)x.GamingGuidelineId,
						Jurisdiction = x.GamingGuideline.Jurisdiction.Name,
						Id = x.Id,
						ManufacturerId = x.ManufacturerId,
						MaxRTP = x.MaxRTP,
						MinRTP = x.MinRTP
					}).ToList(),
				ManufacturerName = manufacturerName,
				ManufacturerId = manufacturerId,

			};


			return View(vm);
		}

		[HttpPost]
		public JsonResult Edit(ManufacturerGuidelinesRow vm)
		{
			ManufacturerGuidelines manufacturerGuidelines;

			if (vm.Id != 0)
			{
				manufacturerGuidelines = _dbSubmission.ManufacturerGuidelines.Find(vm.Id);
			}
			else
			{
				manufacturerGuidelines = new ManufacturerGuidelines();
			}

			manufacturerGuidelines.GamingGuidelineId = vm.GamingGuidelineId;
			manufacturerGuidelines.ManufacturerId = vm.ManufacturerId;
			manufacturerGuidelines.MaxRTP = vm.MaxRTP;
			manufacturerGuidelines.MinRTP = vm.MinRTP;

			if (manufacturerGuidelines.Id == 0)
			{
				_dbSubmission.ManufacturerGuidelines.Add(manufacturerGuidelines);
			}

			_dbSubmission.SaveChanges();

			return Json(new { Id = manufacturerGuidelines.Id }, JsonRequestBehavior.AllowGet);
		}

		[HttpPost]
		public void Delete(int id)
		{
			var entityToRemove = _dbSubmission.ManufacturerGuidelines.Find(id);
			if (entityToRemove != null)
			{
				_dbSubmission.ManufacturerGuidelines.Remove(entityToRemove);
				_dbSubmission.SaveChanges();
			}
		}
	}
}