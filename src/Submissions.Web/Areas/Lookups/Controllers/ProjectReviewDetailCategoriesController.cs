﻿extern alias ZEFCore;
using AutoMapper.QueryableExtensions;
using GLI.EFCore.Submission;
using Microsoft.EntityFrameworkCore;
using Submissions.Common;
using Submissions.Web.Areas.Lookups.Models;
using System.Linq;
using System.Web.Mvc;
using ZEFCore::Z.EntityFramework.Plus;

namespace Submissions.Web.Areas.Lookups.Controllers
{
	public class ProjectReviewDetailCategoriesController : Controller
	{
		private readonly SubmissionContext _dbSubmission;

		public ProjectReviewDetailCategoriesController(SubmissionContext dbSubmission)
		{
			_dbSubmission = dbSubmission;
		}

		public ActionResult Index()
		{
			var projectReviews = _dbSubmission.tbl_lst_ReviewDetailCategories
				.AsNoTracking()
				.OrderByDescending(x => x.Active)
				.ThenBy(x => x.Description)
				.ProjectTo<ProjectReviewDetailCategoriesViewModel>();

			return View(projectReviews);
		}

		public ActionResult Edit(int? id, Mode mode)
		{
			var vm = new ProjectReviewDetailCategoriesViewModel();
			vm.Active = true; //verify to see if t his is needed

			if (mode == Mode.Edit)
			{
				var detailCategories = _dbSubmission.tbl_lst_ReviewDetailCategories.Find(id);
				vm.Id = detailCategories.Id;
				vm.Description = detailCategories.Description;
				vm.Code = detailCategories.Code;
				vm.DepartmentId = detailCategories.DeptId;
				if (detailCategories.DeptId != null)
				{
					vm.Department = detailCategories.Department.Code;
				}

			}

			vm.Mode = mode;

			return View(vm);
		}

		[HttpPost]
		public ActionResult Edit(ProjectReviewDetailCategoriesViewModel vm)
		{
			var detailCategories = new tbl_lst_ReviewDetailCategories();
			if (vm.Mode == Mode.Edit)
			{
				detailCategories = _dbSubmission.tbl_lst_ReviewDetailCategories.Find(vm.Id);
			}
			detailCategories.Id = vm.Id ?? 0;
			detailCategories.Code = vm.Code.ToUpper();
			detailCategories.Description = vm.Description;
			detailCategories.DeptId = vm.DepartmentId;
			detailCategories.Active = vm.Active;

			if (vm.Mode == Mode.Add)
			{
				_dbSubmission.tbl_lst_ReviewDetailCategories.Add(detailCategories);
			}

			_dbSubmission.SaveChanges();

			CacheManager.Clear(LookupAjax.ProjectReviews.ToString());

			return RedirectToAction("Index");
		}

		[HttpPost]
		public JsonResult CheckDelete(int id)
		{
			return Json(false);
		}

		[HttpPost]
		public ActionResult Delete(int id)
		{
			_dbSubmission.tbl_lst_ReviewDetailCategories.Where(x => x.Id == id).Delete();

			CacheManager.Clear(LookupAjax.ProjectReviews.ToString());

			return RedirectToAction("Index");
		}

		[HttpPost]
		public ActionResult SetActive(int id, Mode mode)
		{
			var active = (mode == Mode.Activate ? true : false);
			var reviewDetail = _dbSubmission.tbl_lst_ReviewDetailCategories.Find(id);
			reviewDetail.Active = active;
			_dbSubmission.SaveChanges();

			CacheManager.Clear(LookupAjax.ProjectReviews.ToString());

			return RedirectToAction("Index");
		}

		public JsonResult ValidateReviewDetail(int? id, string description, Mode mode)
		{
			var validates = true;

			if (!string.IsNullOrEmpty(description) && _dbSubmission.tbl_lst_ReviewDetailCategories.Any(x => x.Id != id && x.Description == description))
			{
				validates = false;
			}

			return Json(validates, JsonRequestBehavior.AllowGet);
		}
	}
}