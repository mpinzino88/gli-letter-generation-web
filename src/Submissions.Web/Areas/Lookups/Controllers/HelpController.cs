﻿extern alias ZEFCore;
using AutoMapper.QueryableExtensions;
using GLI.EFCore.Submission;
using Submissions.Common;
using Submissions.Web.Areas.Lookups.Models;
using System.Linq;
using System.Web.Mvc;
using ZEFCore::Z.EntityFramework.Plus;

namespace Submissions.Web.Areas.Lookups.Controllers
{
	[AuthorizeUser(Permission = Permission.Admin, HasAccessRight = AccessRight.Edit)]
	public class HelpController : Controller
	{
		private readonly SubmissionContext _dbSubmission;

		public HelpController(SubmissionContext dbsubmission)
		{
			_dbSubmission = dbsubmission;
		}

		public ActionResult Index()
		{
			var vm = new HelpIndexViewModel();
			vm.HelpData = _dbSubmission.tbl_Help
				.OrderBy(x => x.Code)
				.ProjectTo<HelpItem>()
				.ToList();
			return View(vm);
		}

		[HttpPost]
		public ActionResult Delete(HelpEditViewModel vm)
		{
			_dbSubmission.tbl_Help.Where(x => x.Id == vm.Help.Id).Delete();

			return RedirectToAction("Index");
		}

		public ActionResult Edit(int? id, Mode mode)
		{
			var vm = new HelpEditViewModel();
			vm.Mode = mode;

			if (mode == Mode.Edit)
			{
				vm.Help = _dbSubmission.tbl_Help.Where(x => x.Id == id).ProjectTo<HelpItem>().Single();
			}
			else
			{
				vm.Help.Html = "<p /><i>Insert Help Content Here</i><p /><p />";
			}

			return View(vm);
		}

		[HttpPost]
		public ActionResult Edit(HelpEditViewModel vm)
		{
			if (vm.Mode == Mode.Add)
			{
				var help = MapFields(new tbl_Help(), vm);
				_dbSubmission.tbl_Help.Add(help);
			}
			else
			{
				var dbHelp = _dbSubmission.tbl_Help.Find(vm.Help.Id);
				MapFields(dbHelp, vm);
			}

			_dbSubmission.SaveChanges();

			return RedirectToAction("Index");
		}

		[HttpGet]
		public JsonResult GetHelpContentByCode(string code)
		{
			return Json(new { content = _dbSubmission.tbl_Help.Where(x => x.Code == code).SingleOrDefault().Html }, JsonRequestBehavior.AllowGet);
		}

		#region Helper Methods
		private tbl_Help MapFields(tbl_Help dbHelp, HelpEditViewModel vm)
		{
			dbHelp.Code = vm.Help.Code;
			dbHelp.Description = vm.Help.Description;
			dbHelp.Html = vm.Help.Html;

			return dbHelp;
		}
		#endregion
	}
}