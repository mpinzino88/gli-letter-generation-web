﻿extern alias ZEFCore;
using AutoMapper;
using GLI.EFCore.Submission;
using Newtonsoft.Json;
using Submissions.Common;
using Submissions.Web.Areas.Lookups.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using ZEFCore::Z.EntityFramework.Plus;

namespace Submissions.Web.Areas.Lookups.Controllers
{
	[AuthorizeUser(Permission = Permission.Admin, HasAccessRight = AccessRight.Edit)]
	public class JurisdictionGroupsController : Controller
	{
		private readonly SubmissionContext _dbSubmission;
		private readonly IUserContext _userContext;

		private JurisdictionGroups _jurisdictionGroup;
		private tbl_lst_fileJurisJurisdictionGroups _associationItem;
		private tbl_lst_fileJuris _jurisdiction;

		public JurisdictionGroupsController(IUserContext userContext, SubmissionContext submissionContext)
		{
			_dbSubmission = submissionContext;
			_userContext = userContext;
		}

		public ActionResult Index()
		{
			ViewBag.Title = "Jurisdiction Groups";
			var vm = new JurisdictionGroupsIndexViewModel();
			return View(vm);
		}

		public ActionResult Edit(int? id, Mode mode)
		{
			if (mode == Mode.Add)
			{
				_jurisdictionGroup = new JurisdictionGroups();
			}
			else if (mode == Mode.Edit)
			{
				_jurisdictionGroup = _dbSubmission.JurisdictionGroups.Find(id);
			}
			else
			{
				return RedirectToAction("Index");
			}

			var resultModel = Mapper.Map<JurisdictionGroupsEditViewModel>(_jurisdictionGroup);

			var jurisdictions = _dbSubmission.tbl_lst_fileJurisJurisdictionGroups
				.Where(x => x.JurisdictionGroupId == resultModel.Id)
				.Select(x => x.Jurisdiction)
				.ToList();

			resultModel.JurisdictionsString = LoadJurisdictions(jurisdictions);

			if (_jurisdictionGroup != null)
			{
				ViewBag.Title = mode.ToString();
				return View(resultModel);
			}
			else
			{
				return RedirectToAction("Index");
			}
		}

		[HttpPost]
		public ActionResult Edit(JurisdictionGroupsEditViewModel viewModel)
		{
			var exists = _dbSubmission.JurisdictionGroups
				.Any(x => x.Code == viewModel.Code
					&& x.Category.Id == viewModel.JurisdictionGroupCategoryId
					&& x.Id != viewModel.Id);

			if (exists)
			{
				ModelState.AddModelError("Code", "A group with this name and category combination already exists.");
				viewModel.JurisdictionsString = viewModel.JurisdictionsString == null ? null : LoadJurisdictions(viewModel.JurisdictionsString.Split(','));
				return View(viewModel);
			}

			if (!ModelState.IsValid)
			{
				return View(viewModel);
			}

			switch (viewModel.Mode)
			{
				case Mode.Add:
					ApplyChanges(viewModel);
					_dbSubmission.JurisdictionGroups.Add(_jurisdictionGroup);
					break;
				case Mode.Edit:
					_jurisdictionGroup = _dbSubmission.JurisdictionGroups.Find(viewModel.Id);
					ApplyChanges(viewModel);
					break;
				default:
					throw new Exception("Invalid Edit Mode");
			}

			UserContextSave();

			if (viewModel.Mode == Mode.Add)
			{
				viewModel.Id = _dbSubmission.JurisdictionGroups.SingleOrDefault(x => x.Code == viewModel.Code && x.Category.Id == viewModel.JurisdictionGroupCategoryId).Id;
			}

			UpdateJurisdictionAssociations(viewModel.JurisdictionsString, viewModel.Id);
			UserContextSave();

			CacheManager.Clear(LookupAjax.JurisdictionGroups.ToString());

			return RedirectToAction("Index");
		}

		[HttpPost]
		public JsonResult CheckDelete(int id)
		{
			var payLoad = new
			{
				inUse = false,
				canDeActivate = false
			};
			return Json(payLoad);
		}

		[HttpPost]
		public ActionResult Delete(int id)
		{
			RemoveJurisdictionAssociations(id);
			_dbSubmission.JurisdictionGroups.Where(x => x.Id == id).Delete();

			CacheManager.Clear(LookupAjax.JurisdictionGroups.ToString());

			return RedirectToAction("Index");
		}

		#region Helper Methods
		private void ApplyChanges(JurisdictionGroupsEditViewModel viewModel)
		{
			if (_jurisdictionGroup == null)
			{
				_jurisdictionGroup = new JurisdictionGroups();
			}

			_jurisdictionGroup.Id = viewModel.Id;
			_jurisdictionGroup.Code = viewModel.Code;
			_jurisdictionGroup.Description = viewModel.Description;
			_jurisdictionGroup.JurisdictionGroupCategoryId = viewModel.JurisdictionGroupCategoryId;
			RemoveJurisdictionAssociations(viewModel.Id);
		}

		private void UserContextSave()
		{
			_dbSubmission.UserId = _userContext.User.Id;
			_dbSubmission.SaveChanges();
		}

		private void UpdateJurisdictionAssociations(string jurisdictionsList, int? id)
		{
			var jurisdictions = jurisdictionsList != null ? jurisdictionsList.Split(',') : null;

			if (id != null && jurisdictions != null)
			{
				foreach (var jurisdiction in jurisdictions)
				{
					_associationItem = new tbl_lst_fileJurisJurisdictionGroups();
					_associationItem.JurisdictionGroupId = (int)id;
					_associationItem.JurisdictionId = jurisdiction;
					_dbSubmission.tbl_lst_fileJurisJurisdictionGroups.Add(_associationItem);
				}
			}
		}

		private string LoadJurisdictions(List<tbl_lst_fileJuris> jurisdictions)
		{
			var select2Result = jurisdictions.Select(x => new Select2Result { id = x.Id, text = x.Name }).ToList();
			return (JsonConvert.SerializeObject(select2Result));
		}

		private string LoadJurisdictions(Array jurisdictions)
		{
			var select2Result = new List<Select2Result>();
			foreach (var jurisdiction in jurisdictions)
			{
				_jurisdiction = _dbSubmission.tbl_lst_fileJuris.Find(jurisdiction.ToString());
				select2Result.Add(new Select2Result { id = _jurisdiction.Id, text = _jurisdiction.Name });
			}
			return (JsonConvert.SerializeObject(select2Result));
		}

		private void RemoveJurisdictionAssociations(int? id)
		{
			if (id != null)
			{
				_dbSubmission.tbl_lst_fileJurisJurisdictionGroups.Where(x => x.JurisdictionGroupId == id).Delete();
			}
		}
		#endregion
	}
}