﻿extern alias ZEFCore;
using AutoMapper;
using EF.GPS;
using GLI.EFCore.Submission;
using Microsoft.EntityFrameworkCore;
using Submissions.Common;
using Submissions.Common.Kendo;
using Submissions.Core;
using Submissions.Core.Models;
using Submissions.Web.Areas.Lookups.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ZEFCore::Z.EntityFramework.Plus;

namespace Submissions.Web.Areas.Lookups.Controllers
{
	[AuthorizeUser(Permissions = new Permission[] { Permission.Admin, Permission.AdminManager }, HasAccessRight = AccessRight.Edit)]
	public class UsersController : Controller, ILookupDelete<int>
	{
		private readonly SubmissionContext _dbSubmission;
		private readonly IActiveDirectoryService _activeDirectoryService;
		private readonly IUserContext _userContext;
		private readonly IGPSContext _gpsContext;

		const decimal _defaultDayHours = 8;

		public UsersController(SubmissionContext dbSubmission, IActiveDirectoryService activeDirectoryService, IUserContext userContext, IGPSContext gpsContext)
		{
			_dbSubmission = dbSubmission;
			_activeDirectoryService = activeDirectoryService;
			_userContext = userContext;
			_gpsContext = gpsContext;
		}

		public ActionResult Index()
		{
			var vm = new UsersIndexViewModel();

			// non-admins can only see their employees
			if (!Util.HasAccess(Permission.Admin, AccessRight.Read))
			{
				vm.Filters.ManagerId = _userContext.User.Id;
			}

			return View(vm);
		}

		public ActionResult Edit(int? id, Mode mode)
		{
			var defaultAccessRights = LoadDefaultAccessRights();

			var vm = new UsersEditViewModel();
			vm.Active = true;
			vm.Status = true;
			vm.EngineeringDeptId = _dbSubmission.trDepts.Where(x => x.Code == Department.ENG.ToString()).Select(x => x.Id).Single();

			if (mode == Mode.Add)
			{
				var workdaySchedule = new List<WorkdayScheduleViewModel>();
				workdaySchedule.Add(new WorkdayScheduleViewModel { DayOfWeek = DayOfWeek.Sunday, Hours = 0 });
				workdaySchedule.Add(new WorkdayScheduleViewModel { DayOfWeek = DayOfWeek.Monday, Hours = _defaultDayHours });
				workdaySchedule.Add(new WorkdayScheduleViewModel { DayOfWeek = DayOfWeek.Tuesday, Hours = _defaultDayHours });
				workdaySchedule.Add(new WorkdayScheduleViewModel { DayOfWeek = DayOfWeek.Wednesday, Hours = _defaultDayHours });
				workdaySchedule.Add(new WorkdayScheduleViewModel { DayOfWeek = DayOfWeek.Thursday, Hours = _defaultDayHours });
				workdaySchedule.Add(new WorkdayScheduleViewModel { DayOfWeek = DayOfWeek.Friday, Hours = _defaultDayHours });
				workdaySchedule.Add(new WorkdayScheduleViewModel { DayOfWeek = DayOfWeek.Saturday, Hours = 0 });

				vm.WorkdayHours = _defaultDayHours;
				vm.WorkdaySchedule = workdaySchedule;
			}
			else
			{
				var user = _dbSubmission.trLogins.Find(id);
				vm = Mapper.Map<UsersEditViewModel>(user);

				var workdaySchedule = _dbSubmission.tbl_WorkdaySchedules.Where(x => x.UserId == id).ToList();
				vm.WorkdaySchedule = Mapper.Map<List<WorkdayScheduleViewModel>>(workdaySchedule).OrderBy(x => x.DayOfWeek).ToList();

				var userPermissions = _dbSubmission.UserPermissions.Where(x => x.UserId == id).ToList();

				// populate default access rights from saved data
				foreach (var right in defaultAccessRights)
				{
					var dbPermission = userPermissions.Where(x => x.PermissionId == right.PermissionId).Select(x => x.PermissionValue).SingleOrDefault();
					var permission = (AccessRight)Enum.ToObject(typeof(AccessRight), dbPermission);
					right.Read = permission.HasFlag(AccessRight.Read);
					right.Add = permission.HasFlag(AccessRight.Add);
					right.Edit = permission.HasFlag(AccessRight.Edit);
					right.Delete = permission.HasFlag(AccessRight.Delete);
				}

				var oldPermissions = _dbSubmission.trPermissions.Where(x => x.UserId == id).SingleOrDefault();
				if (oldPermissions != null)
				{
					vm.OldPermissions = Mapper.Map<OldPermissions>(oldPermissions);
				}

				// get profile pics
				if (!string.IsNullOrEmpty(user.ProfilePictureFile))
				{
					var fileName = Path.Combine(PathUtil.UserProfilePicture(), user.ProfilePictureFile);
					var file = new FileInfo(fileName);

					if (file.Exists)
					{
						vm.ProfilePictures.Add(new KendoFile { Name = file.Name, Extension = file.Extension, Size = file.Length });
					}
				}
			}

			vm.AccessRights = defaultAccessRights;
			vm.Mode = mode;

			return View(vm);
		}

		[HttpPost]
		public ActionResult Edit(int? id, UsersEditViewModel vm)
		{
			if (_dbSubmission.trLogins.Any(x => x.Id != vm.Id && x.Status == "A" && (x.Username == vm.Username && (x.FName == vm.FName && x.LName == vm.LName))))
			{
				ModelState.AddModelError("Username", "User already exists");
			}

			if (!ModelState.IsValid)
			{
				vm.AccessRights = LoadDefaultAccessRights();
				return View(vm);
			}

			var user = new trLogin();

			if (vm.Mode == Mode.Edit)
			{
				user = _dbSubmission.trLogins.Find(vm.Id);
			}

			user = ApplyChanges(user, vm);

			if (vm.Mode == Mode.Add)
			{
				user.AddDate = DateTime.Now;
				_dbSubmission.trLogins.Add(user);
			}

			if (!vm.Status)
			{
				_dbSubmission.UserSubscriptions.Where(x => x.UserId == vm.Id).Delete();
			}

			_dbSubmission.UserId = _userContext.User.Id;
			_dbSubmission.SaveChanges();

			if (vm.Mode == Mode.Add)
			{
				AddGpsUser(user.Id);
			}

			vm.Id = user.Id;
			UpdateWorkdaySchedule(vm);
			UpdatePermissions(vm);
			UpdateOldPermissions(vm);

			if (vm.Mode == Mode.Add)
			{
				AddProfilePicture(user, vm.ProfilePicturesUpload);
			}

			CacheManager.Clear(LookupAjax.Users.ToString());
			CacheManager.Clear(LookupAjax.Liaisons.ToString());

			return RedirectToAction("Index");
		}

		private void AddGpsUser(int userId) //SUB-2539
		{
			var allLabIds = _dbSubmission.trLaboratories.Select(lab => lab.Id).ToList();
			foreach (var labId in allLabIds)
			{
				var newGpsUploadMethodUser = new GPSUserUploadMethod()
				{
					UserId = userId,
					UploadMethodId = 1,
					LocationId = labId
				};
				_gpsContext.GPSUserUploadMethods.Add(newGpsUploadMethodUser);
			}
			_gpsContext.SaveChanges();
		}

		[HttpPost]
		public ActionResult LoadDefaultRolePermissions(int roleId)
		{
			var templateAccessRights = LoadDefaultAccessRights();
			var rolePermissions = _dbSubmission.RolePermissions.Where(x => x.RoleId == roleId).ToList();

			// populate default access rights from saved data
			foreach (var right in templateAccessRights)
			{
				var rolePermission = rolePermissions.Where(x => x.PermissionId == right.PermissionId).Select(x => x.PermissionValue).SingleOrDefault();
				var accessRight = (AccessRight)Enum.ToObject(typeof(AccessRight), rolePermission);

				right.Read = accessRight.HasFlag(AccessRight.Read);
				right.Add = accessRight.HasFlag(AccessRight.Add);
				right.Edit = accessRight.HasFlag(AccessRight.Edit);
				right.Delete = accessRight.HasFlag(AccessRight.Delete);
			}

			return Json(templateAccessRights);
		}

		[HttpPost]
		public ActionResult SyncActiveDirectoryUser(UsersEditViewModel vm)
		{
			ActiveDirectoryUser user;

			if (!vm.Email.Contains("pubknow.com"))
			{
				if (!string.IsNullOrEmpty(vm.LDAP_SID))
				{
					user = _activeDirectoryService.GetUserBySID(vm.LDAP_SID);
				}
				else if (!string.IsNullOrEmpty(vm.Username))
				{
					user = _activeDirectoryService.GetUserByUsername(vm.Username);
				}
				else
				{
					user = _activeDirectoryService.GetUserByName(vm.FName, vm.LName);
				}
			}
			else
			{
				user = _activeDirectoryService.GetPublicKnowledgeUserByUsername(vm.Username);
			}


			var results = new UsersEditViewModel();
			results.LDAP_SID = user.SID;
			results.ADUserPath = user.ADsPath;
			results.sAMAccountName = user.sAMAccountName;
			results.Username = user.sAMAccountName;
			results.FName = user.FName;
			results.LName = user.LName;
			results.Phone = user.Phone;
			results.MobilePhone = user.MobilePhone;
			results.Email = user.Email;

			if (!string.IsNullOrEmpty(user.HomeOffice))
			{
				var homeOffice = _dbSubmission.trLaboratories
					.Where(x => x.Name.Contains(user.HomeOffice) && x.Active == 1)
					.Select(x => new { Id = x.Id, Text = x.Location + " - " + x.Name })
					.FirstOrDefault();

				if (homeOffice != null)
				{
					results.HomeOfficeId = homeOffice.Id;
					results.HomeOffice = homeOffice.Text;
				}
			}

			if (!string.IsNullOrEmpty(user.ManagerName))
			{
				var manager = user.ManagerName.Split(' ');
				var managerFName = manager[0];
				var managerLName = manager[1];

				var dbManager = _dbSubmission.trLogins
					.Where(x => x.FName == managerFName && x.LName == managerLName && x.Status == "A")
					.Select(x => new { Id = x.Id, Text = x.FName + " " + x.LName })
					.FirstOrDefault();

				if (dbManager != null)
				{
					results.ManagerId = dbManager.Id;
					results.Manager = dbManager.Text;
				}
			}

			return Json(results);
		}

		/// <summary>
		/// Allow deletion if the user was added in the past week.
		/// This gives the ability to delete a user added by mistake.
		/// </summary>
		[HttpPost]
		public JsonResult CheckDelete(int id)
		{
			var addDate = _dbSubmission.trLogins.Find(id).AddDate ?? DateTime.Today;
			var hasSubordinates = _dbSubmission.trLogins.Any(x => x.ManagerId == id);
			var userCreatedMoreThanSevenDaysAgo = (DateTime.Today - (DateTime)addDate).TotalDays > 7;

			return Json(hasSubordinates || userCreatedMoreThanSevenDaysAgo);
		}

		[HttpPost]
		public ActionResult Delete(int id)
		{
			_dbSubmission.tbl_Liaisons.Where(x => x.UserId == id).Delete();
			_dbSubmission.tbl_WorkdaySchedules.Where(x => x.UserId == id).Delete();
			_dbSubmission.UserPermissions.Where(x => x.UserId == id).Delete();
			_dbSubmission.trPermissions.Where(x => x.UserId == id).Delete();
			_dbSubmission.UserSubscriptions.Where(x => x.UserId == id).Delete();
			_dbSubmission.trLogins.Where(x => x.Id == id).Delete();

			CacheManager.Clear(LookupAjax.Users.ToString());
			CacheManager.Clear(LookupAjax.Liaisons.ToString());

			return RedirectToAction("Index");
		}

		[HttpPost]
		public ActionResult SetActive(int id, Mode mode)
		{
			string active = (mode == Mode.Activate ? "A" : "I");

			var user = _dbSubmission.trLogins.Find(id);
			user.Status = active;
			_dbSubmission.SaveChanges();

			CacheManager.Clear(LookupAjax.Users.ToString());
			CacheManager.Clear(LookupAjax.Liaisons.ToString());

			return RedirectToAction("Index");
		}

		#region File Upload
		private void AddProfilePicture(trLogin user, IList<HttpPostedFileBase> files)
		{
			if (files.Count == 0)
			{
				return;
			}

			var file = files.First();
			var fileName = user.LName + user.Id.ToString() + Path.GetExtension(file.FileName);
			var path = Path.Combine(PathUtil.UserProfilePicture(), fileName);

			(new FileInfo(path)).Directory.Create();
			file.SaveAs(path);

			user.ProfilePictureFile = fileName;
			_dbSubmission.SaveChanges();
		}

		public ActionResult AddProfilePicture(int userId, IList<HttpPostedFileBase> profilePicturesUpload)
		{
			if (userId > 0)
			{
				var user = _dbSubmission.trLogins.Find(userId);
				var file = profilePicturesUpload.First();
				var fileName = user.LName + userId + Path.GetExtension(file.FileName);

				var path = Path.Combine(PathUtil.UserProfilePicture(), fileName);

				(new FileInfo(path)).Directory.Create();
				file.SaveAs(path);

				user.ProfilePictureFile = fileName;
				_dbSubmission.SaveChanges();
			}

			return Content("");
		}

		public ActionResult DeleteProfilePicture(int userId, string[] fileNames)
		{
			if (userId > 0)
			{
				var lName = _dbSubmission.trLogins.Where(x => x.Id == userId).Select(x => x.LName).Single();

				var fileName = lName + userId + Path.GetExtension(fileNames[0]);
				var path = Path.Combine(PathUtil.UserProfilePicture(), fileName);
				System.IO.File.Delete(path);

				var user = _dbSubmission.trLogins.Find(userId);
				user.ProfilePictureFile = null;
				_dbSubmission.SaveChanges();
			}

			return Content("");
		}
		#endregion

		#region Helper Methods
		private trLogin ApplyChanges(trLogin user, UsersEditViewModel vm)
		{
			user.Id = vm.Id ?? 0;
			user.Username = vm.Username;
			user.FName = vm.FName;
			user.LName = vm.LName;
			user.Phone = vm.Phone;
			user.Email = vm.Email;
			user.Status = vm.Status ? "A" : "I";
			user.DepartmentId = vm.DepartmentId;
			user.LocationId = vm.LocationId;
			user.HomeOfficeId = vm.HomeOfficeId;
			user.RoleId = (int)vm.RoleId;
			user.ManagerId = vm.ManagerId;
			user.sAMAccountName = vm.sAMAccountName;
			user.ADUserPath = vm.ADUserPath;
			user.LDAP_SID = vm.LDAP_SID;
			user.IsLiaison = vm.IsLiaison;
			user.MobilePhone = vm.MobilePhone;
			user.CeridianEmployeeNum = vm.CeridianEmployeeNum;
			user.WorkdayHours = vm.WorkdayHours;
			user.TeamId = vm.TeamId;
			user.SupplierFocusId = vm.SupplierFocusId;
			user.PkInternalMetricsEmployeeId = vm.PkInternalMetricsEmployeeId;
			user.BusinessOwnerId = vm.BusinessOwnerId;
			return user;
		}

		private UserPermission ApplyUserPermissionChanges(int userId, AccessRightCheckbox accessRight)
		{
			var permission = accessRight.Read ? AccessRight.Read : 0;
			permission |= accessRight.Add ? AccessRight.Add : 0;
			permission |= accessRight.Edit ? AccessRight.Edit : 0;
			permission |= accessRight.Delete ? AccessRight.Delete : 0;

			var permissionDb = Convert.ToInt32(permission);

			var existingUserPermission = new UserPermission
			{
				UserId = userId,
				PermissionId = accessRight.PermissionId,
				PermissionValue = permissionDb
			};

			return existingUserPermission;
		}

		private trPermission ApplyOldPermissionChanges(trPermission permissions, UsersEditViewModel vm)
		{
			permissions.Id = vm.OldPermissions.Id ?? 0;
			permissions.UserId = vm.Id ?? 0;
			permissions.CreateSubmission = Convert.ToInt32(vm.OldPermissions.CreateSubmission);
			permissions.EditSubmission = Convert.ToInt32(vm.OldPermissions.EditSubmission);
			permissions.DeleteSubmission = Convert.ToInt32(vm.OldPermissions.DeleteSubmission);
			permissions.CreateJurisdiction = Convert.ToInt32(vm.OldPermissions.CreateJurisdiction);
			permissions.EditJurisdiction = Convert.ToInt32(vm.OldPermissions.EditJurisdiction);
			permissions.DeleteJurisdiction = Convert.ToInt32(vm.OldPermissions.DeleteJurisdiction);
			permissions.QAElecTransf = Convert.ToByte(vm.OldPermissions.QAElecTransf);
			permissions.CanSetSubStatusAP = Convert.ToByte(vm.OldPermissions.CanSetSubStatusAP);
			permissions.CanSetSubStatusOH = Convert.ToByte(vm.OldPermissions.CanSetSubStatusOH);
			permissions.CannotChangeAPNL = Convert.ToByte(vm.OldPermissions.CannotChangeAPNL);
			permissions.DEUpdaterRole = Convert.ToByte(vm.OldPermissions.DEUpdaterRole);
			permissions.DEScannerRole = Convert.ToByte(vm.OldPermissions.DEScannerRole);
			permissions.CanSetStatusCP = Convert.ToInt32(vm.OldPermissions.CanSetStatusCP);
			permissions.CanCreateDraftLetters = Convert.ToByte(vm.OldPermissions.CanCreateDraftLetters);
			permissions.CanProcessCos = Convert.ToByte(vm.OldPermissions.CanProcessCos);
			permissions.CanRunDetailBillReport = vm.OldPermissions.CanRunDetailBillReport;
			permissions.DeleteFileNumber = vm.OldPermissions.DeleteFileNumber;
			permissions.CanUnlockCOSForm = vm.OldPermissions.CanUnlockCOSForm;

			return permissions;
		}

		private List<AccessRightCheckbox> LoadDefaultAccessRights()
		{
			var permissions = _dbSubmission.Permissions.AsQueryable();

			// exclude Admin permission if user doesn't have Admin rights
			if (!Util.HasAccess(Permission.Admin, AccessRight.Edit))
			{
				permissions = permissions.Where(x =>
					x.Code != Permission.Admin.ToString() &&
					(x.DepartmentId == _userContext.DepartmentId || x.DepartmentId == null));
			}

			return permissions.Select(x => new AccessRightCheckbox { PermissionId = x.Id, Code = x.Code, Description = x.Description })
				.OrderBy(x => x.Description)
				.ToList();
		}

		private void UpdateWorkdaySchedule(UsersEditViewModel vm)
		{
			foreach (var item in vm.WorkdaySchedule)
			{
				var workdaySchedule = new tbl_WorkdaySchedule
				{
					Id = item.Id ?? 0,
					UserId = (int)vm.Id,
					DayOfWeek = (int)item.DayOfWeek,
					Hours = item.Hours
				};

				_dbSubmission.Entry(workdaySchedule).State = workdaySchedule.Id == 0 ? EntityState.Added : EntityState.Modified;
			}

			_dbSubmission.SaveChanges();
		}

		private void UpdatePermissions(UsersEditViewModel vm)
		{
			if (vm.Mode == Mode.Add)
			{
				foreach (var accessRight in vm.AccessRights)
				{
					var userPermission = ApplyUserPermissionChanges((int)vm.Id, accessRight);
					_dbSubmission.UserPermissions.Add(userPermission);
				}
			}
			else
			{
				var existingUserPermissionIds = _dbSubmission.UserPermissions
						.Where(x => x.UserId == vm.Id)
						.Select(x => x.PermissionId)
						.ToList();

				var newUserPermissions = vm.AccessRights.Where(x => !existingUserPermissionIds.Contains(x.PermissionId)).ToList();
				var existingUserPermissions = vm.AccessRights.Where(x => existingUserPermissionIds.Contains(x.PermissionId)).ToList();

				foreach (var permission in existingUserPermissions)
				{
					var existingUserPermission = ApplyUserPermissionChanges((int)vm.Id, permission);
					_dbSubmission.Entry(existingUserPermission).State = EntityState.Modified;
				}

				foreach (var permission in newUserPermissions)
				{
					var newUserPermission = ApplyUserPermissionChanges((int)vm.Id, permission);
					_dbSubmission.UserPermissions.Add(newUserPermission);
				}
			}

			_dbSubmission.SaveChanges();
		}

		private void UpdateOldPermissions(UsersEditViewModel vm)
		{
			var oldPermissions = new trPermission();

			if (vm.Mode == Mode.Add)
			{
				oldPermissions = ApplyOldPermissionChanges(oldPermissions, vm);
				_dbSubmission.trPermissions.Add(oldPermissions);
			}
			else
			{
				oldPermissions = _dbSubmission.trPermissions.Find(vm.OldPermissions.Id);

				if (oldPermissions == null)
				{
					oldPermissions = new trPermission();
					oldPermissions = ApplyOldPermissionChanges(oldPermissions, vm);
					_dbSubmission.trPermissions.Add(oldPermissions);
				}
				else
				{
					oldPermissions = ApplyOldPermissionChanges(oldPermissions, vm);
				}
			}

			_dbSubmission.SaveChanges();
		}
		#endregion
	}
}