﻿extern alias ZEFCore;
using AutoMapper;
using GLI.EFCore.Submission;
using Submissions.Common;
using Submissions.Web.Areas.Lookups.Models;
using System;
using System.Linq;
using System.Web.Mvc;
using ZEFCore::Z.EntityFramework.Plus;

namespace Submissions.Web.Areas.Lookups.Controllers
{
	[AuthorizeUser(Permission = Permission.Admin, HasAccessRight = AccessRight.Edit)]
	public class JurisdictionGroupCategoriesController : Controller
	{
		private readonly SubmissionContext _dbSubmission;
		private readonly IUserContext _userContext;
		private JurisdictionGroupCategories _item;

		public JurisdictionGroupCategoriesController(SubmissionContext dbSubmission, IUserContext userContext)
		{
			_dbSubmission = dbSubmission;
			_userContext = userContext;
		}

		public ActionResult Index()
		{
			ViewBag.Title = "Jurisdiction Group Categories";
			var vm = new JurisdictionGroupCategoriesIndexViewModel();
			return View(vm);
		}

		public ActionResult Edit(int? id, Mode mode)
		{
			_item = mode == Mode.Add ? new JurisdictionGroupCategories() : _dbSubmission.JurisdictionGroupCategories.Find(id);

			if (_item != null)
			{
				ViewBag.Title = mode.ToString();
				return View(Mapper.Map<JurisdictionGroupCategoriesEditViewModel>(_item));
			}
			else
			{
				return RedirectToAction("Index");
			}
		}

		[HttpPost]
		public ActionResult Edit(JurisdictionGroupCategoriesEditViewModel vm)
		{
			var exists = _dbSubmission.JurisdictionGroupCategories.Any(x => x.Code == vm.Code);
			if (ModelState.IsValid && !exists)
			{
				ApplyChanges(vm);

				CacheManager.Clear(LookupAjax.JurisdictionGroupCategories.ToString());

				return RedirectToAction("Index");
			}
			else
			{
				if (exists)
				{
					ModelState.AddModelError("Code", "Category Tag already exists please choose a new one.");
				}
				return View(vm);
			}
		}

		[HttpPost]
		public JsonResult CheckDelete(int id)
		{
			var payLoad = new
			{
				inUse = _dbSubmission.JurisdictionGroups.Any(x => x.JurisdictionGroupCategoryId == id),
				canDeActivate = false
			};
			return Json(payLoad);
		}

		public ActionResult Delete(int id)
		{
			_dbSubmission.JurisdictionGroupCategories.Where(x => x.Id == id).Delete();

			CacheManager.Clear(LookupAjax.JurisdictionGroupCategories.ToString());

			return RedirectToAction("Index");
		}

		#region Helper Methods
		private void ApplyChanges(JurisdictionGroupCategoriesEditViewModel vm)
		{
			_item = _dbSubmission.JurisdictionGroupCategories.Find(vm.Id);
			switch (vm.Mode)
			{
				case Mode.Add:
					_item = new JurisdictionGroupCategories();
					_item = Mapper.Map<JurisdictionGroupCategories>(vm);
					_dbSubmission.JurisdictionGroupCategories.Add(_item);
					break;
				case Mode.Edit:
					if (_item == null)
					{
						throw new Exception("Jurisdiction Category NOT found.");
					}
					_item.Description = vm.Description;
					_item.Code = vm.Code;
					break;
				default:
					throw new Exception("Invalid Edit Mode");
			}
			UserContextSave();
		}

		private void UserContextSave()
		{
			_dbSubmission.UserId = _userContext.User.Id;
			_dbSubmission.SaveChanges();
		}
		#endregion
	}
}