﻿extern alias ZEFCore;
using AutoMapper.QueryableExtensions;
using GLI.EFCore.Submission;
using Microsoft.EntityFrameworkCore;
using Submissions.Common;
using Submissions.Web.Areas.Lookups.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using ZEFCore::Z.EntityFramework.Plus;

namespace Submissions.Web.Areas.Lookups.Controllers
{
	[AuthorizeUser(Permission = Permission.LetterGeneration, HasAccessRight = AccessRight.Edit)]
	public class AccreditationsController : Controller, ILookupDelete<int>
	{
		private readonly SubmissionContext _dbSubmission;

		public AccreditationsController(SubmissionContext dbSubmission)
		{
			_dbSubmission = dbSubmission;
		}

		public ActionResult Index()
		{
			var accreditations = _dbSubmission.tbl_lst_Accreditation
				.AsNoTracking()
				.OrderByDescending(x => x.Active)
				.ThenBy(x => x.Name)
				.ProjectTo<AccreditationsViewModel>();

			return View(accreditations);
		}

		public ActionResult Edit(int? id, Mode mode)
		{
			var vm = new AccreditationsViewModel();
			vm.Active = true;

			if (mode == Mode.Edit)
			{
				var accreditation = _dbSubmission.tbl_lst_Accreditation.Find(id);

				vm.Id = accreditation.Id;
				vm.Name = accreditation.Name;
				vm.Description = accreditation.Description;
				vm.IssueDate = accreditation.IssueDate;
				vm.ExpirationDate = accreditation.ExpirationDate.Date;

				var laboratories = _dbSubmission.LaboratoryAccreditationJoin.Include(x => x.Laboratory)
					.Where(x => x.AccreditationId == vm.Id)
					.Select(x => new SelectListItem { Value = x.LaboratoryId.ToString(), Text = x.Laboratory.Name, Selected = true })
					.ToList();

				vm.LaboratoryIds = laboratories.Select(x => x.Value).ToArray();
				vm.Laboratories = laboratories;
			}

			vm.Mode = mode;

			return View(vm);
		}

		[HttpPost]
		public ActionResult Edit(int? id, AccreditationsViewModel vm)
		{
			var accreditation = new tbl_lst_Accreditation();
			if (vm.Mode == Mode.Edit)
			{
				accreditation = _dbSubmission.tbl_lst_Accreditation.Find(vm.Id);
			}

			accreditation.Id = vm.Id ?? 0;
			accreditation.Active = vm.Active;
			accreditation.Name = vm.Name;
			accreditation.Description = vm.Description;
			accreditation.IssueDate = Convert.ToDateTime(vm.IssueDate).Date;
			accreditation.ExpirationDate = Convert.ToDateTime(vm.ExpirationDate).Date;

			if (vm.Mode == Mode.Add)
			{
				_dbSubmission.tbl_lst_Accreditation.Add(accreditation);
			}

			var laboratories = vm.LaboratoryIds == null ? null : vm.LaboratoryIds.Select(x => Convert.ToInt32(x)).ToList();

			_dbSubmission.SaveChanges();

			UpdateLaboratories(accreditation.Id, laboratories);

			CacheManager.Clear(LookupAjax.Accreditations.ToString());

			return RedirectToAction("Index");
		}

		[HttpPost]
		public JsonResult CheckDelete(int id)
		{
			var isInUse = _dbSubmission.LaboratoryAccreditationJoin.Any(x => x.AccreditationId == id);

			return Json(isInUse);
		}

		[HttpPost]
		public ActionResult Delete(int id)
		{
			_dbSubmission.LaboratoryAccreditationJoin.Where(x => x.AccreditationId == id).Delete();
			_dbSubmission.tbl_lst_Accreditation.Where(x => x.Id == id).Delete();

			CacheManager.Clear(LookupAjax.Accreditations.ToString());

			return RedirectToAction("Index");
		}

		[HttpPost]
		public ActionResult SetActive(int id, Mode mode)
		{
			bool active = (mode == Mode.Activate ? true : false);

			var version = _dbSubmission.tbl_lst_Accreditation.Find(id);
			version.Active = active;
			_dbSubmission.SaveChanges();

			CacheManager.Clear(LookupAjax.Accreditations.ToString());

			return RedirectToAction("Index");
		}

		public JsonResult ValidateAccreditation(int? id, string name, Mode mode)
		{
			var validates = true;

			if (!string.IsNullOrEmpty(name) && _dbSubmission.tbl_lst_Accreditation.Any(x => x.Id != id && x.Name == name))
			{
				validates = false;
			}

			return Json(validates, JsonRequestBehavior.AllowGet);
		}

		#region Helper Methods
		private void UpdateLaboratories(int accreditationId, List<int> laboratoryIds)
		{
			_dbSubmission.LaboratoryAccreditationJoin.Where(x => x.AccreditationId == accreditationId).Delete();

			if (laboratoryIds != null && laboratoryIds.Count > 0)
			{
				foreach (var laboratoryId in laboratoryIds)
				{
					var laboratoryAccreditation = new LaboratoryAccreditationJoin { LaboratoryId = laboratoryId, AccreditationId = accreditationId };
					_dbSubmission.LaboratoryAccreditationJoin.Add(laboratoryAccreditation);
				}

				_dbSubmission.SaveChanges();
			}
		}
		#endregion
	}
}