﻿using AutoMapper;
using GLI.EFCore.Submission;
using Submissions.Common;
using Submissions.Web.Areas.Lookups.Models;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace Submissions.Web.Areas.Lookups.Controllers
{
	public class BillingPartiesController : Controller
	{
		private readonly SubmissionContext _dbSubmission;

		public BillingPartiesController(SubmissionContext submissionContext)
		{
			_dbSubmission = submissionContext;
		}

		public ActionResult Index()
		{
			var billingParties = _dbSubmission.tbl_lst_BillingParties
				.Select(x => x)
				.OrderByDescending(x => x.Active)
				.ThenBy(x => x.ManufacturerGroup.Description)
				.ThenBy(x => x.BillingParty)
				.ToList();

			var vm = Mapper.Map<List<BillingPartyViewModel>>(billingParties);

			return View(vm);
		}

		public ActionResult Edit(int? id)
		{
			var vm = new BillingPartyViewModel();
			vm.Mode = Mode.Add;
			if (id != null)
			{
				var billingParty = _dbSubmission.tbl_lst_BillingParties.Find(id);
				vm.BillingParty = billingParty.BillingParty;
				vm.Id = billingParty.Id;
				vm.ManufacturerGroup = billingParty.ManufacturerGroup.Description;
				vm.ManufacturerGroupId = billingParty.ManufacturerGroupId;
				vm.TestingType = billingParty.TestingType != null ? billingParty.TestingType.Description : "";
				vm.TestingTypeId = billingParty.TestingTypeId;
				vm.Active = billingParty.Active;
				vm.BillingPartyCode = billingParty.BillingPartyCode;
				vm.Mode = Mode.Edit;
			}

			return View(vm);
		}

		[HttpPost]
		public ActionResult Edit(BillingPartyViewModel vm)
		{
			var billingParty = _dbSubmission.tbl_lst_BillingParties.Find(vm.Id);
			if (billingParty == null)
			{
				billingParty = new tbl_lst_BillingParty();
			}
			billingParty.BillingParty = vm.BillingParty;
			billingParty.ManufacturerGroupId = (int)vm.ManufacturerGroupId;
			billingParty.TestingTypeId = (int?)vm.TestingTypeId;
			billingParty.BillingPartyCode = vm.BillingPartyCode;
			billingParty.Active = vm.Active;
			if (billingParty.Id == 0)
			{
				_dbSubmission.tbl_lst_BillingParties.Add(billingParty);
			}
			_dbSubmission.SaveChanges();
			return RedirectToAction("Index");
		}

		public ActionResult Delete(int id)
		{
			var party = _dbSubmission.tbl_lst_BillingParties.Find(id);
			_dbSubmission.tbl_lst_BillingParties.Remove(party);
			_dbSubmission.SaveChanges();

			return RedirectToAction("Index");
		}

		public ActionResult ToggleActive(int id)
		{
			var party = _dbSubmission.tbl_lst_BillingParties.Find(id);
			party.Active = !party.Active;
			_dbSubmission.SaveChanges();

			return RedirectToAction("Index");
		}
	}
}