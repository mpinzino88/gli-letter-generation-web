﻿extern alias ZEFCore;
using AutoMapper.QueryableExtensions;
using GLI.EFCore.Submission;
using Microsoft.EntityFrameworkCore;
using Submissions.Common;
using Submissions.Web.Areas.Lookups.Models;
using System.Linq;
using System.Web.Mvc;
using ZEFCore::Z.EntityFramework.Plus;

namespace Submissions.Web.Areas.Lookups.Controllers
{
	[AuthorizeUser(Permission = Permission.Admin, HasAccessRight = AccessRight.Edit)]
	public class SignatureVersionsController : Controller, ILookupDelete<int>
	{
		private readonly SubmissionContext _dbSubmission;

		public SignatureVersionsController(SubmissionContext dbSubmission)
		{
			_dbSubmission = dbSubmission;
		}

		public ActionResult Index()
		{
			var versions = _dbSubmission.tbl_lst_SignatureVersions
				.AsNoTracking()
				.OrderByDescending(x => x.Active)
				.ThenBy(x => x.Version)
				.ProjectTo<SignatureVersionsViewModel>();

			return View(versions);
		}

		public ActionResult Edit(int? id, Mode mode)
		{
			var vm = new SignatureVersionsViewModel();
			vm.Active = true;

			if (mode == Mode.Edit)
			{
				var version = _dbSubmission.tbl_lst_SignatureVersions.Find(id);

				vm.Id = version.Id;
				vm.Version = version.Version;
			}

			vm.Mode = mode;

			return View(vm);
		}

		[HttpPost]
		public ActionResult Edit(int? id, SignatureVersionsViewModel vm)
		{
			var version = new tbl_lst_SignatureVersions();
			if (vm.Mode == Mode.Edit)
			{
				version = _dbSubmission.tbl_lst_SignatureVersions.Find(vm.Id);
			}

			version.Id = vm.Id ?? 0;
			version.Active = vm.Active;
			version.Version = vm.Version;

			if (vm.Mode == Mode.Add)
			{
				_dbSubmission.tbl_lst_SignatureVersions.Add(version);
			}

			_dbSubmission.SaveChanges();

			CacheManager.Clear(LookupAjax.SignatureVersions.ToString());

			return RedirectToAction("Index");
		}

		[HttpPost]
		public JsonResult CheckDelete(int id)
		{
			var isInUse = _dbSubmission.tblSignatures.Any(x => x.VersionId == id);

			return Json(isInUse);
		}

		[HttpPost]
		public ActionResult Delete(int id)
		{
			_dbSubmission.tbl_lst_SignatureVersions.Where(x => x.Id == id).Delete();

			CacheManager.Clear(LookupAjax.SignatureVersions.ToString());

			return RedirectToAction("Index");
		}

		[HttpPost]
		public ActionResult SetActive(int id, Mode mode)
		{
			bool active = (mode == Mode.Activate ? true : false);

			var version = _dbSubmission.tbl_lst_SignatureVersions.Find(id);
			version.Active = active;
			_dbSubmission.SaveChanges();

			CacheManager.Clear(LookupAjax.SignatureVersions.ToString());

			return RedirectToAction("Index");
		}

		public JsonResult ValidateVersion(int? id, string version, Mode mode)
		{
			var validates = true;

			if (!string.IsNullOrEmpty(version) && _dbSubmission.tbl_lst_SignatureVersions.Any(x => x.Id != id && x.Version == version))
			{
				validates = false;
			}

			return Json(validates, JsonRequestBehavior.AllowGet);
		}
	}
}