﻿extern alias ZEFCore;
using AutoMapper.QueryableExtensions;
using GLI.EFCore.Submission;
using Microsoft.EntityFrameworkCore;
using Submissions.Common;
using Submissions.Web.Areas.Lookups.Models;
using System.Linq;
using System.Web.Mvc;
using ZEFCore::Z.EntityFramework.Plus;

namespace Submissions.Web.Areas.Lookups.Controllers
{
	public class DocumentTypesController : Controller
	{
		private readonly SubmissionContext _dbSubmission;

		public DocumentTypesController(SubmissionContext dbSubmission)
		{
			_dbSubmission = dbSubmission;
		}

		public ActionResult Index()
		{
			var types = _dbSubmission.tbl_lst_DocumentTypes
				.AsNoTracking()
				.OrderBy(x => x.DocumentType)
				.ProjectTo<DocumentTypesViewModel>()
				.ToList();

			return View(types);
		}

		public ActionResult Edit(int? id, Mode mode)
		{
			var vm = new DocumentTypesViewModel();
			vm.Active = true;

			if (mode == Mode.Edit)
			{
				var docType = _dbSubmission.tbl_lst_DocumentTypes.Find(id);
				vm.Id = docType.Id;
				vm.DocumentType = docType.DocumentType;
				vm.Description = docType.Description;
			}
			vm.Mode = mode;

			return View(vm);
		}

		[HttpPost]
		public ActionResult Edit(int? id, DocumentTypesViewModel vm)
		{
			var docType = new tbl_lst_DocumentTypes();
			if (vm.Mode == Mode.Edit)
			{
				docType = _dbSubmission.tbl_lst_DocumentTypes.Find(vm.Id);
			}

			docType.Description = vm.Description;
			docType.DocumentType = vm.DocumentType;

			if (vm.Mode == Mode.Add)
			{
				_dbSubmission.tbl_lst_DocumentTypes.Add(docType);
			}
			_dbSubmission.SaveChanges();

			CacheManager.Clear(LookupAjax.DocumentType.ToString());

			return RedirectToAction("Index");
		}

		[HttpPost]
		public JsonResult CheckDelete(int id)
		{
			var isInUse = _dbSubmission.Documents.Any(x => x.DocumentTypeId == id);
			return Json(isInUse);
		}

		[HttpPost]
		public ActionResult Delete(int id)
		{
			_dbSubmission.tbl_lst_DocumentTypes.Where(x => x.Id == id).Delete();

			CacheManager.Clear(LookupAjax.DocumentType.ToString());

			return RedirectToAction("Index");
		}
	}
}