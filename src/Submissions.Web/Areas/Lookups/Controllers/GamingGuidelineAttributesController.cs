﻿extern alias ZEFCore;
using AutoMapper.QueryableExtensions;
using GLI.EFCore.Submission;
using Microsoft.EntityFrameworkCore;
using Submissions.Common;
using Submissions.Web.Areas.Lookups.Models;
using System.Linq;
using System.Web.Mvc;
using ZEFCore::Z.EntityFramework.Plus;

namespace Submissions.Web.Areas.Lookups.Controllers
{
	public class GamingGuidelineAttributesController : Controller, ILookupDelete<int>
	{
		private readonly SubmissionContext _dbSubmission;

		public GamingGuidelineAttributesController(SubmissionContext dbSubmission)
		{
			_dbSubmission = dbSubmission;
		}

		public ActionResult Index()
		{
			var attributes = _dbSubmission.tbl_lst_GamingGuidelineAttributes
				.AsNoTracking()
				.OrderBy(x => x.Active)
				.ThenBy(x => x.Name)
				.ProjectTo<tbl_lst_GamingGuidelineAttributeViewModel>();

			return View(attributes);
		}

		public ActionResult Edit(int? id, Mode mode)
		{
			var vm = new tbl_lst_GamingGuidelineAttributeViewModel();
			vm.Active = true;

			if (mode == Mode.Edit)
			{
				var attribute = _dbSubmission.tbl_lst_GamingGuidelineAttributes.Find(id);

				vm.Id = attribute.Id;
				vm.Name = attribute.Name;
				vm.Note = attribute.Note;
			}
			vm.Mode = mode;

			return View(vm);
		}

		[HttpPost]
		public ActionResult Edit(int? id, tbl_lst_GamingGuidelineAttributeViewModel vm)
		{
			var attribute = new tbl_lst_GamingGuidelineAttributes();
			if (vm.Mode == Mode.Edit)
			{
				attribute = _dbSubmission.tbl_lst_GamingGuidelineAttributes.Find(id);
			}

			attribute.Id = vm.Id ?? 0;
			attribute.Name = vm.Name;
			attribute.Note = vm.Note;
			attribute.Active = vm.Active;

			if (vm.Mode == Mode.Add)
			{
				_dbSubmission.tbl_lst_GamingGuidelineAttributes.Add(attribute);
			}

			_dbSubmission.SaveChanges();

			CacheManager.Clear(LookupAjax.GamingGuidelinesAttributes.ToString());

			return RedirectToAction("Index");
		}

		[HttpPost]
		public JsonResult CheckDelete(int id)
		{
			var attribute = _dbSubmission.tbl_lst_GamingGuidelineAttributes.Find(id);
			var isInUse = _dbSubmission.GamingGuidelineAttributes.Any(x => x.AttributeId == id) && attribute.Active;

			return Json(isInUse);
		}

		[HttpPost]
		public ActionResult Delete(int id)
		{
			_dbSubmission.tbl_lst_GamingGuidelineAttributes.Where(x => x.Id == id).Delete();
			_dbSubmission.GamingGuidelineAttributes.Where(x => x.AttributeId == id).Delete();

			CacheManager.Clear(LookupAjax.GamingGuidelinesAttributes.ToString());

			return RedirectToAction("Index");
		}

		public JsonResult ValidateAttribute(int? id, string name)
		{
			var validates = true;

			if (!string.IsNullOrEmpty(name) && _dbSubmission.tbl_lst_GamingGuidelineAttributes.Any(x => x.Id != id && x.Name == name))
			{
				validates = false;
			}
			return Json(validates, JsonRequestBehavior.AllowGet);
		}

		[HttpPost]
		public ActionResult SetActive(int id, Mode mode)
		{
			var active = (mode == Mode.Deactivate) ? false : true;

			var attribute = _dbSubmission.tbl_lst_GamingGuidelineAttributes.Find(id);
			attribute.Active = active;

			_dbSubmission.SaveChanges();

			CacheManager.Clear(LookupAjax.GamingGuidelinesAttributes.ToString());

			return RedirectToAction("Index");

		}
	}
}