﻿extern alias ZEFCore;
using AutoMapper.QueryableExtensions;
using GLI.EFCore.Submission;
using Microsoft.EntityFrameworkCore;
using Submissions.Common;
using Submissions.Common.JQGrid;
using Submissions.Web.Areas.Lookups.Models;
using Submissions.Web.Models.Grids.Lookups;
using System.Linq;
using System.Web.Mvc;
using ZEFCore::Z.EntityFramework.Plus;

namespace Submissions.Web.Areas.Lookups.Controllers
{
	public class TargetOffSetController : Controller
	{
		private readonly SubmissionContext _dbSubmission;

		public TargetOffSetController(SubmissionContext dbSubmission)
		{
			_dbSubmission = dbSubmission;
		}

		public ActionResult Index()
		{
			var vm = new TargetOffSetViewModel();
			return View(vm);
		}

		public ActionResult Edit(int? id, Mode mode)
		{
			var vm = new TargetOffSetEditViewModel();

			if (mode == Mode.Edit)
			{
				var targetOffSet = _dbSubmission.tbl_lst_Juris_TargetOffSet.Find(id);
				vm.JurisdictionID = targetOffSet.Jurisdiction.Id;
				vm.Jurisdiction = targetOffSet.Jurisdiction.Name;
				vm.TargetOffSet = targetOffSet.TargetDayOffSet;
				vm.Reason = targetOffSet.Reason;
			}
			vm.Mode = mode;

			return View(vm);
		}

		[HttpPost]
		public ActionResult Edit(TargetOffSetEditViewModel vm)
		{
			var targetOffSet = new tbl_lst_Juris_TargetOffSet();

			if (vm.Mode == Mode.Edit)
			{
				targetOffSet = _dbSubmission.tbl_lst_Juris_TargetOffSet.Find(vm.Id);
			}

			targetOffSet = ApplyChanges(targetOffSet, vm);

			if (vm.Mode == Mode.Add)
			{
				_dbSubmission.tbl_lst_Juris_TargetOffSet.Add(targetOffSet);
			}
			_dbSubmission.SaveChanges();
			vm.Id = targetOffSet.Id;
			return RedirectToAction("Index");
		}

		public ActionResult Delete(int? id)
		{
			_dbSubmission.tbl_lst_Juris_TargetOffSet.Where(x => x.Id == id).Delete();
			return RedirectToAction("Index");
		}

		public JsonResult ValidateJurisdiction(int? id, string Jurisdiction, Mode mode)
		{
			var validates = true;
			if (!string.IsNullOrEmpty(Jurisdiction) && _dbSubmission.tbl_lst_Juris_TargetOffSet.Any(x => x.Id != id && x.JurisdictionId == Jurisdiction))
			{
				validates = false;
			}
			return Json(validates, JsonRequestBehavior.AllowGet);
		}

		public void Export()
		{
			var query = _dbSubmission.tbl_lst_Juris_TargetOffSet.AsNoTracking().AsQueryable();
			var results = query.ProjectTo<TargetOffSetGridRecord>();
			var targetOffSetGrid = new TargetOffSetGrid();
			var export = new Export
			{
				ExportType = ExportType.Csv,
				FiltersToolbar = "",
				SavedColumns = "",
				Title = "Target OffSet"
			};
			targetOffSetGrid.Export(export, results);
		}

		#region Helper Methods
		private tbl_lst_Juris_TargetOffSet ApplyChanges(tbl_lst_Juris_TargetOffSet targetOffSet, TargetOffSetEditViewModel vm)
		{
			targetOffSet.JurisdictionId = vm.JurisdictionID;
			targetOffSet.TargetDayOffSet = vm.TargetOffSet;
			targetOffSet.Reason = vm.Reason;
			return targetOffSet;
		}
		#endregion
	}
}