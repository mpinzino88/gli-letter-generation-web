﻿using GLI.EF.LetterContent;
using Submissions.Common;
using Submissions.Web.Areas.Lookups.Models;
using System.Linq;
using System.Web.Mvc;

namespace Submissions.Web.Areas.Lookups.Controllers
{
	public class FormatFiltersController : Controller
	{
		private readonly ILetterContentContext _letterContentContext;

		public FormatFiltersController(ILetterContentContext letterContentContext)
		{
			_letterContentContext = letterContentContext;
		}

		public ActionResult Index()
		{
			var vm = _letterContentContext.FormatFilters.Select(x => new FormatFilterViewModel
			{
				Id = x.Id,
				Description = x.Description,
				FilterText = x.FilterText,
				Name = x.Name,
				Active = x.Active
			}).ToList();
			return View(vm);
		}

		public ActionResult Edit(int? id)
		{
			var vm = new FormatFilterViewModel
			{
				Mode = Mode.Add
			};
			if (id != null)
			{
				var formatFilter = _letterContentContext.FormatFilters.Where(x => x.Id == id).SingleOrDefault();
				vm.Description = formatFilter.Description;
				vm.FilterText = formatFilter.FilterText;
				vm.Name = formatFilter.Name;
				vm.Active = formatFilter.Active;
				vm.Mode = Mode.Edit;
			}

			return View(vm);
		}

		[HttpPost]
		public ActionResult Edit(FormatFilterViewModel vm)
		{
			FormatFilter formatFilter;
			if (vm.Mode == Mode.Edit)
			{
				formatFilter = _letterContentContext.FormatFilters.Where(x => x.Id == vm.Id).SingleOrDefault();
			}
			else
			{
				formatFilter = new FormatFilter();
			}

			formatFilter.Description = vm.Description;
			formatFilter.Name = vm.Name;
			formatFilter.FilterText = vm.FilterText;
			formatFilter.Active = vm.Active;

			if (vm.Mode == Mode.Add)
			{
				_letterContentContext.FormatFilters.Add(formatFilter);
			}
			_letterContentContext.SaveChanges();
			return RedirectToAction("Index");
		}


		public ActionResult Delete(int id)
		{
			var filter = _letterContentContext.FormatFilters.Find(id);
			_letterContentContext.FormatFilters.Remove(filter);
			_letterContentContext.SaveChanges();
			return RedirectToAction("Index");
		}

		public ActionResult ToggleActive(int id)
		{
			var filter = _letterContentContext.FormatFilters.Find(id);
			filter.Active = !filter.Active;
			_letterContentContext.SaveChanges();

			return RedirectToAction("Index");
		}
	}
}