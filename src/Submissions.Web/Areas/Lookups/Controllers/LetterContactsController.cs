﻿extern alias ZEFCore;
using GLI.EFCore.Submission;
using Submissions.Common;
using Submissions.Web.Areas.Lookups.Models;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using ZEFCore::Z.EntityFramework.Plus;

namespace Submissions.Web.Areas.Lookups.Controllers
{
	[AuthorizeUser(Permission = Permission.LetterGeneration, HasAccessRight = AccessRight.Edit)]
	public class LetterContactsController : Controller, ILookupDelete<int>
	{
		private readonly SubmissionContext _dbSubmission;

		public LetterContactsController
		(
			SubmissionContext dbSubmission
		)
		{
			_dbSubmission = dbSubmission;
		}

		public ActionResult Index()
		{
			var vm = new LetterContactsIndexViewModel();
			vm.ContactTypeOptions = _dbSubmission.tbl_lst_LetterContactType
					.Select(x => new SelectListItem
					{
						Value = x.Id.ToString(),
						Text = x.ContactType
					})
					.ToList();
			return View(vm);
		}

		public ActionResult Edit(int? id, Mode mode)
		{
			var vm = new LetterContactsEditViewModel();
			vm.ContactTypeOptions = _dbSubmission.tbl_lst_LetterContactType
					.Select(x => new SelectListItem
					{
						Value = x.Id.ToString(),
						Text = x.ContactType
					})
					.ToList();
			if (mode == Mode.Edit)
			{
				var letterContact = _dbSubmission.tbl_lst_LetterContacts.Find(id);
				vm.Id = letterContact.Id;
				vm.Name = letterContact.Name;
				vm.AddressLine1 = letterContact.AddressLine1;
				vm.AddressLine2 = letterContact.AddressLine2;
				vm.AddressLine3 = letterContact.AddressLine3;
				vm.AddressLine4 = letterContact.AddressLine4;
				vm.AddressLine5 = letterContact.AddressLine5;
				vm.ApplyAllManuf = letterContact.ApplyAllManuf;
				vm.ApplyAllJur = letterContact.ApplyAllJur;
				vm.ContactTypeIds = letterContact.ContactTypes.Select(x => x.ContactTypeId).ToList();
				vm.ManufacturerCodes = letterContact.Manufacturers.Select(x => x.ManufacturerId).ToList();
				vm.Manufacturers = letterContact.Manufacturers
					.Select(x => new SelectListItem
					{
						Value = x.ManufacturerId.ToString(),
						Text = x.Manufacturer.Description
					})
					.ToList();
				vm.JurisdictionCodes = letterContact.Jurisdictions.Select(x => x.JurisdictionId).ToList();
				vm.Jurisdictions = letterContact.Jurisdictions
					.Select(x => new SelectListItem
					{
						Value = x.JurisdictionId.ToString(),
						Text = x.Jurisdiction.Name
					})
					.ToList();
			}
			vm.Mode = mode;
			return View(vm);
		}

		[HttpPost]
		public ActionResult Edit(LetterContactsEditViewModel vm)
		{
			var exists = _dbSubmission.tbl_lst_LetterContacts.Any(
				x => x.Id != vm.Id &&
				x.ApplyAllJur == vm.ApplyAllJur &&
				x.ApplyAllManuf == vm.ApplyAllManuf &&
				x.ContactTypes.Any(y => vm.ContactTypeIds.Contains(y.ContactTypeId)) &&
				x.Jurisdictions.Any(y => vm.JurisdictionCodes.Contains(y.JurisdictionId)) &&
				x.Manufacturers.Any(y => vm.ManufacturerCodes.Contains(y.ManufacturerId))
			);

			if (exists)
			{
				ModelState.AddModelError("Name", "A contact with same Letter Type, Jurisdiction and Manufacturer already exists.");
				vm = SetupEditViewModel(vm);
				return View(vm);
			}

			//update Letter Contact
			var letterContact = new tbl_lst_LetterContacts();

			if (vm.Mode == Mode.Edit)
			{
				letterContact = _dbSubmission.tbl_lst_LetterContacts.Find(vm.Id);
			}

			// letterContact.Id = vm.Id ?? 0;
			letterContact.Name = vm.Name;
			letterContact.AddressLine1 = vm.AddressLine1;
			letterContact.AddressLine2 = vm.AddressLine2;
			letterContact.AddressLine3 = vm.AddressLine3;
			letterContact.AddressLine4 = vm.AddressLine4;
			letterContact.AddressLine5 = vm.AddressLine5;
			letterContact.ApplyAllJur = vm.ApplyAllJur;
			letterContact.ApplyAllManuf = vm.ApplyAllManuf;
			if (vm.Mode == Mode.Add)
			{
				_dbSubmission.tbl_lst_LetterContacts.Add(letterContact);
			}
			_dbSubmission.SaveChanges();
			vm.Id = letterContact.Id;

			UpdateManufacturers((int)vm.Id, vm.ManufacturerCodes);
			UpdateJurisdictions((int)vm.Id, vm.JurisdictionCodes);
			UpdateContactTypes((int)vm.Id, vm.ContactTypeIds);

			return RedirectToAction("Index");
		}

		public JsonResult CheckDelete(int id)
		{
			var isInUse = false;
			//Need to check if this is needed
			return Json(isInUse);
		}

		public ActionResult Delete(int id)
		{
			_dbSubmission.LetterContacts_JurisdictionJoin.Where(x => x.LetterContactsId == id).Delete();
			_dbSubmission.LetterContacts_ManufacturerJoin.Where(x => x.LetterContactsId == id).Delete();
			_dbSubmission.tbl_lst_LetterContacts.Where(x => x.Id == id).Delete();
			return RedirectToAction("Index");
		}

		#region Helper Methods
		private LetterContactsEditViewModel SetupEditViewModel(LetterContactsEditViewModel vm)
		{
			vm.ContactTypeOptions = _dbSubmission.tbl_lst_LetterContactType
					.Select(x => new SelectListItem
					{
						Value = x.Id.ToString(),
						Text = x.ContactType
					})
					.ToList();

			if (vm.Mode == Mode.Edit)
			{
				vm.Manufacturers = _dbSubmission.LetterContacts_ManufacturerJoin
					.Where(x => x.LetterContactsId == vm.Id)
					.Select(x => new SelectListItem { Value = x.ManufacturerId, Text = x.Manufacturer.Description })
					.ToList();

				vm.Jurisdictions = _dbSubmission.LetterContacts_JurisdictionJoin
					.Where(x => x.LetterContactsId == vm.Id)
					.Select(x => new SelectListItem { Value = x.JurisdictionId, Text = x.Jurisdiction.Name })
					.ToList();
			}
			else
			{
				vm.Manufacturers = _dbSubmission.tbl_lst_fileManu
					.Where(x => vm.ManufacturerCodes.Contains(x.Code))
					.Select(x => new SelectListItem { Value = x.Code, Text = x.Description })
					.ToList();

				vm.Jurisdictions = _dbSubmission.tbl_lst_fileJuris
					.Where(x => vm.JurisdictionCodes.Contains(x.Id))
					.Select(x => new SelectListItem { Value = x.Id, Text = x.Name })
					.ToList();
			}

			return vm;
		}

		private void UpdateManufacturers(int letterContactId, List<string> manufacturerIds)
		{
			_dbSubmission.LetterContacts_ManufacturerJoin.Where(x => x.LetterContactsId == letterContactId).Delete();
			if (manufacturerIds != null && manufacturerIds.Count > 0)
			{
				foreach (var manufacturerId in manufacturerIds)
				{
					var manufacturer = new LetterContacts_ManufacturerJoin { LetterContactsId = letterContactId, ManufacturerId = manufacturerId };
					_dbSubmission.LetterContacts_ManufacturerJoin.Add(manufacturer);
				}
				_dbSubmission.SaveChanges();
			}
		}

		private void UpdateJurisdictions(int letterContactId, List<string> jurisdictionIds)
		{
			_dbSubmission.LetterContacts_JurisdictionJoin.Where(x => x.LetterContactsId == letterContactId).Delete();
			if (jurisdictionIds != null && jurisdictionIds.Count > 0)
			{
				foreach (var jurisdictionId in jurisdictionIds)
				{
					var jurisdiction = new LetterContacts_JurisdictionJoin { LetterContactsId = letterContactId, JurisdictionId = jurisdictionId };
					_dbSubmission.LetterContacts_JurisdictionJoin.Add(jurisdiction);
				}
				_dbSubmission.SaveChanges();
			}
		}

		private void UpdateContactTypes(int letterContactId, List<int> contactTypes)
		{
			_dbSubmission.LetterContacts_ContactTypeJoin.Where(x => x.LetterContactsId == letterContactId).Delete();
			if (contactTypes != null && contactTypes.Count > 0)
			{
				foreach (var contactType in contactTypes)
				{
					var contactTypeJoin = new LetterContacts_ContactTypeJoin { LetterContactsId = letterContactId, ContactTypeId = contactType };
					_dbSubmission.LetterContacts_ContactTypeJoin.Add(contactTypeJoin);
				}
				_dbSubmission.SaveChanges();
			}
		}
		#endregion
	}
}