﻿extern alias ZEFCore;
using GLI.EFCore.Submission;
using Submissions.Common;
using Submissions.Web.Areas.Lookups.Models;
using System.Linq;
using System.Web.Mvc;
using ZEFCore::Z.EntityFramework.Plus;

namespace Submissions.Web.Areas.Lookups.Controllers
{
	[AuthorizeUser(Permission = Permission.UserFields, HasAccessRight = AccessRight.Edit)]

	public class SupplierFocusController : Controller, ILookupDelete<int>
	{
		private readonly SubmissionContext _dbSubmission;

		public SupplierFocusController(SubmissionContext dbSubmission)
		{
			_dbSubmission = dbSubmission;
		}

		public ActionResult Index()
		{
			var supplierFocus = _dbSubmission.tbl_lst_SupplierFocus
				.Select(x => new SupplierFocusIndexViewModel
				{
					Id = x.Id,
					Description = x.Description,
					Active = x.Active
				})
				.OrderBy(x => x.Description)
				.ToList();

			return View(supplierFocus);
		}

		public ActionResult Edit(int? id, Mode mode)
		{
			var vm = new SupplierFocusEditViewModel();

			if (mode == Mode.Edit)
			{
				var supplierFocus = _dbSubmission.tbl_lst_SupplierFocus.Find(id);
				vm.Description = supplierFocus.Description;
				vm.Active = supplierFocus.Active;
			}
			vm.Mode = mode;

			return View(vm);
		}

		[HttpPost]
		public ActionResult Edit(int? id, SupplierFocusEditViewModel vm)
		{
			var supplierFocus = new tbl_lst_SupplierFocus();

			if (vm.Mode == Mode.Edit)
			{
				supplierFocus = _dbSubmission.tbl_lst_SupplierFocus.Find(id);
			}

			supplierFocus.Description = vm.Description;
			supplierFocus.Active = vm.Active;

			if (vm.Mode == Mode.Add)
			{
				_dbSubmission.tbl_lst_SupplierFocus.Add(supplierFocus);
			}

			_dbSubmission.SaveChanges();

			CacheManager.Clear(LookupAjax.SupplierFocus.ToString());

			return RedirectToAction("Index");
		}

		public JsonResult CheckDelete(int id)
		{
			var result = _dbSubmission.trLogins.Any(x => x.SupplierFocusId == id);
			return Json(result);
		}

		public ActionResult SetActive(int id, Mode mode)
		{
			bool active = (mode == Mode.Activate ? true : false);

			var supplierFocus = _dbSubmission.tbl_lst_SupplierFocus.Find(id);
			supplierFocus.Active = active;
			_dbSubmission.SaveChanges();

			CacheManager.Clear(LookupAjax.SupplierFocus.ToString());

			return RedirectToAction("Index");
		}

		public ActionResult Delete(int id)
		{
			_dbSubmission.tbl_lst_SupplierFocus.Where(x => x.Id == id).Delete();

			CacheManager.Clear(LookupAjax.SupplierFocus.ToString());

			return RedirectToAction("Index");
		}

		[HttpPost]
		public ContentResult AddUsers(UsersSupplierFocusPayload payload)
		{
			try
			{
				_dbSubmission.ChangeTracker.AutoDetectChangesEnabled = false;
				foreach (var userId in payload.UserIds)
				{
					var updatedUser = new trLogin
					{
						Id = userId,
						SupplierFocusId = payload.SupplierFocusId
					};

					_dbSubmission.trLogins.Attach(updatedUser);
					_dbSubmission.Entry(updatedUser).Property(x => x.SupplierFocusId).IsModified = true;
				}

				_dbSubmission.SaveChanges();
			}
			finally
			{
				_dbSubmission.ChangeTracker.AutoDetectChangesEnabled = true;
			}

			return Content(ComUtil.JsonEncodeCamelCase(""), "application/json");
		}

		[HttpPost]
		public ContentResult RemoveUsers(UsersSupplierFocusPayload payload)
		{
			try
			{
				_dbSubmission.ChangeTracker.AutoDetectChangesEnabled = false;
				foreach (var userId in payload.UserIds)
				{
					var updatedUser = new trLogin
					{
						Id = userId,
						SupplierFocusId = null
					};

					_dbSubmission.trLogins.Attach(updatedUser);
					_dbSubmission.Entry(updatedUser).Property(x => x.SupplierFocusId).IsModified = true;
				}

				_dbSubmission.SaveChanges();
			}
			finally
			{
				_dbSubmission.ChangeTracker.AutoDetectChangesEnabled = true;
			}

			return Content(ComUtil.JsonEncodeCamelCase(""), "application/json");
		}
	}
}