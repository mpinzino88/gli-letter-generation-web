
extern alias ZEFCore;
using GLI.EFCore.Submission;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Submissions.Common;
using Submissions.Web.Areas.Lookups.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using ZEFCore::Z.EntityFramework.Plus;

namespace Submissions.Web.Areas.Lookups.Controllers
{
	[AuthorizeUser(Permission = Permission.HRHolidays, HasAccessRight = AccessRight.Edit)]
	public class HolidaysController : Controller
	{
		private readonly SubmissionContext _dbSubmission;
		private readonly IUserContext _userContext;

		public HolidaysController(SubmissionContext dbSubmission, IUserContext userContext)
		{
			_dbSubmission = dbSubmission;
			_userContext = userContext;
		}

		public ActionResult Index()
		{
			var query = _dbSubmission.trLaboratories
				.Where(x => x.Active == 1)
				.Select(x => new HolidayEditViewModel
				{
					Id = x.Id,
					LocationName = x.Name
				})
				.OrderBy(x => x.LocationName)
				.ToList();

			return View(query);
		}

		[HttpPost]
		public ActionResult UpdateHolidays(int? id, int year)
		{
			var holidays = GetHolidays(id, (int)year);

			var json = JsonConvert.SerializeObject(holidays, Formatting.Indented, new JsonSerializerSettings { ContractResolver = new CamelCasePropertyNamesContractResolver() });
			return Content(json, "application/json");
		}
		public ActionResult Edit(int? id)
		{
			var year = DateTime.Now.Year;
			var location = _dbSubmission.trLaboratories.Where(x => x.Id == id).Single();

			var vm = new HolidayEditViewModel();
			vm.LocationId = location.Id;
			vm.LocationName = location.Name;
			vm.Holidays = GetHolidays(id, (int)year);
			vm.Year = (int)year;

			return View(vm);
		}

		[HttpPost]
		public ActionResult Edit(HolidayEditViewModel holidayViewModel)
		{
			_dbSubmission.tbl_lst_Holidays
				.Where(x => x.LocationId == holidayViewModel.LocationId && x.HolidayDate.Year == holidayViewModel.Year)
				.Delete();

			foreach (var item in holidayViewModel.Holidays)
			{
				var holiday = new tbl_lst_Holidays();
				holiday.LocationId = holidayViewModel.LocationId;
				holiday.HolidayName = item.Name;
				holiday.HolidayDate = item.Date ?? DateTime.MinValue;
				holiday.EnteredTime = DateTime.Now;
				holiday.UserId = _userContext.User.Id;
				_dbSubmission.tbl_lst_Holidays.Add(holiday);
			}

			_dbSubmission.SaveChanges();

			CacheManager.Clear(LookupAjax.HolidayYears.ToString());

			return RedirectToAction("Index");
		}

		[HttpPost]
		public JsonResult Clone(int existingId, int newLocation)
		{
			var existingHolidays = _dbSubmission.tbl_lst_Holidays
				.Where(x => x.LocationId == existingId)
				.ToList();

			foreach (var item in existingHolidays)
			{
				var newHolidays = new tbl_lst_Holidays();
				newHolidays.LocationId = newLocation;
				newHolidays.HolidayName = item.HolidayName;
				newHolidays.HolidayDate = item.HolidayDate;

				_dbSubmission.tbl_lst_Holidays.Add(newHolidays);
			}
			_dbSubmission.SaveChanges();

			return Json(true);
		}

		private List<HolidayViewModel> GetHolidays(int? id, int year)
		{
			var holidays = _dbSubmission.tbl_lst_Holidays
				.Where(x => x.LocationId == id && x.HolidayDate.Year == year)
				.Select(x => new HolidayViewModel
				{
					Name = x.HolidayName,
					Date = x.HolidayDate
				})
				.ToList();

			return holidays;
		}
	}
}