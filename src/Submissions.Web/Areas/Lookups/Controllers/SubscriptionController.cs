﻿extern alias ZEFCore;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using GLI.EFCore.Submission;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Submissions.Common;
using Submissions.Web.Areas.Lookups.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using ZEFCore::Z.EntityFramework.Plus;

namespace Submissions.Web.Areas.Lookups.Controllers
{
	[AuthorizeUser(Permission = Permission.Admin, HasAccessRight = AccessRight.Edit)]
	public class SubscriptionController : Controller, ILookupDelete<int>
	{
		private readonly SubmissionContext _dbSubmission;
		private readonly IUserContext _userContext;

		public SubscriptionController(SubmissionContext dbSubmission, IUserContext userContext)
		{
			_dbSubmission = dbSubmission;
			_userContext = userContext;
		}

		public ActionResult Index()
		{
			var vm = new SubscriptionIndexViewModel();
			vm.Subscriptions = _dbSubmission.Subscriptions
				.AsNoTracking()
				.OrderByDescending(x => x.Active)
				.ThenBy(x => x.Category)
				.ThenBy(x => x.Name)
				.ProjectTo<Models.Subscription>()
				.ToList();

			return View(vm);
		}

		public ActionResult Edit(int? id, Mode mode)
		{
			var selectedFields = new List<string>();
			var vm = new SubscriptionEditViewModel();
			vm.Active = true;

			if (mode == Mode.Edit)
			{
				var subscription = _dbSubmission.Subscriptions.Find(id);

				vm = Mapper.Map<SubscriptionEditViewModel>(subscription);

				if (subscription.AdditionalFilters != null)
				{
					var jsAdditionalFilters = JsonConvert.DeserializeObject<List<Filters>>(subscription.AdditionalFilters);
					foreach (var field in vm.FilterFieldList)
					{
						foreach (var filter in jsAdditionalFilters)
						{
							if (field.Value == filter.ColumnName.ToString())
							{
								field.Selected = true;
								selectedFields.Add(filter.ColumnName.ToString());
							}
						}
					}
				}

				vm.FilterField = subscription.AdditionalFilters;
			}

			vm.Mode = mode;
			vm.SelectedFilterField = selectedFields;

			return View(vm);
		}

		[HttpPost]
		public ActionResult Edit(int? id, SubscriptionEditViewModel vm)
		{
			var subscription = new GLI.EFCore.Submission.Subscription();
			if (vm.Mode == Mode.Edit)
			{
				subscription = _dbSubmission.Subscriptions.Find(vm.Id);
			}

			subscription.Id = vm.Id ?? 0;
			subscription.Active = vm.Active;
			subscription.Name = vm.Name;
			subscription.SubscriptionGroupsId = vm.SubscriptionGroupsId ?? 0;
			subscription.Description = vm.Description;
			subscription.Category = vm.Category;
			subscription.MessageExpiry = vm.MessageExpiry;
			subscription.EditDate = DateTime.Now;
			subscription.EditUserId = _userContext.User.Id;

			var selectedFields = new List<Filters>();

			foreach (var field in vm.SelectedFilterField)
			{
				if (field == "Jurisdiction")
				{
					selectedFields.Add(new Filters
					{
						TableName = "tbl_lst_fileJuris",
						ColumnName = field.ToString(),
						DataType = "nvarchar"
					});
				}

				if (field == "Manager")
				{
					selectedFields.Add(new Filters
					{
						TableName = "trLogin",
						ColumnName = field.ToString(),
						DataType = "int"
					});
				}

				if (field == "Manufacturer")
				{
					selectedFields.Add(new Filters
					{
						TableName = "tbl_lst_fileManu",
						ColumnName = field.ToString(),
						DataType = "nvarchar"
					});
				}

				if (field == "SequenceNumber")
				{
					selectedFields.Add(new Filters
					{
						TableName = "Submissions",
						ColumnName = field.ToString(),
						DataType = "nvarchar"
					});
				}

				if (field == "SubmissionType")
				{
					selectedFields.Add(new Filters
					{
						TableName = "tbl_lst_SubmissionType",
						ColumnName = field.ToString(),
						DataType = "nvarchar"
					});
				}

				if (field == "Year")
				{
					selectedFields.Add(new Filters
					{
						TableName = "Submissions",
						ColumnName = field.ToString(),
						DataType = "int"
					});
				}
			}

			subscription.AdditionalFilters = JsonConvert.SerializeObject(selectedFields);

			subscription.DatabaseName = "";
			subscription.TableName = "";
			subscription.TriggerName = "";
			subscription.MailTemplate = "";
			subscription.Communication = JsonConvert.SerializeObject(String.Empty);
			subscription.Delimiter = "|";

			if (vm.Mode == Mode.Add)
			{
				subscription.AddDate = DateTime.Now;
				subscription.AddUserId = _userContext.User.Id;
				_dbSubmission.Subscriptions.Add(subscription);
			}

			_dbSubmission.SaveChanges();

			CacheManager.Clear(LookupAjax.Subscriptions.ToString());

			return RedirectToAction("Index");
		}

		[HttpPost]
		public JsonResult CheckDelete(int id)
		{
			var isInUse = _dbSubmission.UserSubscriptions.Any(x => x.SubscriptionId == id);

			return Json(isInUse);
		}

		[HttpPost]
		public ActionResult Delete(int id)
		{
			_dbSubmission.UserSubscriptions.Where(x => x.SubscriptionId == id).Delete();
			_dbSubmission.Subscriptions.Where(x => x.Id == id).Delete();

			CacheManager.Clear(LookupAjax.Subscriptions.ToString());

			return RedirectToAction("Index");
		}

		[HttpPost]
		public ActionResult SetActive(int id, Mode mode)
		{
			bool active = (mode == Mode.Activate ? true : false);

			var subscription = _dbSubmission.Subscriptions.Find(id);
			subscription.Active = active;
			_dbSubmission.SaveChanges();

			CacheManager.Clear(LookupAjax.Subscriptions.ToString());

			return RedirectToAction("Index");
		}

		public JsonResult ValidateName(int? id, string name, Mode mode)
		{
			var validates = true;

			if (!string.IsNullOrEmpty(name) && _dbSubmission.Subscriptions.Any(x => x.Id != id && x.Name == name))
			{
				validates = false;
			}

			return Json(validates, JsonRequestBehavior.AllowGet);
		}
	}
}