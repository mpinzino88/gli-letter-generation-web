﻿using AutoMapper;
using GLI.EFCore.Submission;
using Microsoft.EntityFrameworkCore;
using Submissions.Common;
using Submissions.Web.Areas.Lookups.Models;
using System.Web.Mvc;

namespace Submissions.Web.Areas.Lookups.Controllers
{
	public class AcumaticaBillWithAndCustomerExceptionsController : Controller
	{
		private readonly SubmissionContext _dbSubmission;
		private readonly IUserContext _userContext;

		public AcumaticaBillWithAndCustomerExceptionsController(SubmissionContext dbSubmission, IUserContext userContext)
		{
			_dbSubmission = dbSubmission;
			_userContext = userContext;
		}

		#region Action Results
		public ActionResult Index() => View(BuildIndexViewModel());
		public ActionResult Edit(Mode mode, int id = 0) => View(BuildEditViewModel(id, mode));
		[HttpPost]
		public ActionResult Edit(AcumaticaBillWithAndCustomerExceptionEditViewModel vm)
		{
			var exception = Mode.Add == vm.Mode ? new tbl_lst_AcumaticaBillwithAndCustomerExceptions() : _dbSubmission.tbl_lst_AcumaticaBillwithAndCustomerExceptions.Find(vm.Id);

			exception.CompanyId = (int)vm.CompanyId;
			exception.ExceptionTypeId = (int)vm.ExceptionTypeId;
			exception.Jurisdiction = string.IsNullOrWhiteSpace(vm.Jurisdiction) ? null : vm.Jurisdiction;
			exception.TransferJurisdiction = string.IsNullOrWhiteSpace(vm.TransferJurisdiction) ? null : vm.TransferJurisdiction;
			exception.ManufacturerCode = vm.ManufacturerCode;
			exception.ProjectCustomer = vm.ProjectCustomer;
			exception.Segment01 = string.IsNullOrWhiteSpace(vm.Segment01) ? null : vm.Segment01;
			exception.Segment02 = string.IsNullOrWhiteSpace(vm.Segment02) ? null : vm.Segment02;
			exception.Segment03 = string.IsNullOrWhiteSpace(vm.Segment03) ? null : vm.Segment03;
			exception.Segment04 = string.IsNullOrWhiteSpace(vm.Segment04) ? null : vm.Segment04;
			exception.Segment05 = string.IsNullOrWhiteSpace(vm.Segment05) ? null : vm.Segment05;
			exception.Segment06 = string.IsNullOrWhiteSpace(vm.Segment06) ? null : vm.Segment06;
			exception.SelfBill = vm.SelfBill;
			exception.Active = vm.Active;

			if (vm.Mode == Mode.Add)
				_dbSubmission.tbl_lst_AcumaticaBillwithAndCustomerExceptions.Add(exception);

			SaveChanges();

			return RedirectToAction("Index");
		}
		[HttpPost]
		public ActionResult Delete(AcumaticaBillWithAndCustomerExceptionEditViewModel vm)
		{
			var exception = new tbl_lst_AcumaticaBillwithAndCustomerExceptions { Id = vm.Id };
			_dbSubmission.Entry(exception).State = EntityState.Deleted;
			SaveChanges();
			return RedirectToAction("Index");
		}
		#endregion

		#region Private Helpers
		private AcumaticaBillWithAndCustomerExceptionEditViewModel BuildEditViewModel(int id, Mode mode)
		{
			var vm = new AcumaticaBillWithAndCustomerExceptionEditViewModel();
			if (mode == Mode.Edit)
			{
				vm = Mapper.Map<AcumaticaBillWithAndCustomerExceptionEditViewModel>(_dbSubmission.tbl_lst_AcumaticaBillwithAndCustomerExceptions.Find(id));
			}
			vm.Mode = mode;
			vm.Active = true;

			return vm;
		}
		private AcumaticaBillWithAndCustomerExceptionsIndexViewModel BuildIndexViewModel() => new AcumaticaBillWithAndCustomerExceptionsIndexViewModel();
		private void SaveChanges()
		{
			_dbSubmission.UserId = _userContext.User.Id;
			_dbSubmission.SaveChanges();
			_dbSubmission.UserId = null;
		}
		#endregion
	}
}