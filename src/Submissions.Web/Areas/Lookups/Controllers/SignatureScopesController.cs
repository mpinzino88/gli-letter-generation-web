﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using GLI.EFCore.Submission;
using Microsoft.EntityFrameworkCore;
using Submissions.Common;
using Submissions.Web.Areas.Lookups.Models;
using System.Linq;
using System.Web.Mvc;

namespace Submissions.Web.Areas.Lookups.Controllers
{
	[AuthorizeUser(Permission = Permission.Admin, HasAccessRight = AccessRight.Edit)]
	public class SignatureScopesController : Controller, ILookupDelete<int>
	{
		private readonly SubmissionContext _dbSubmission;

		public SignatureScopesController(SubmissionContext dbSubmission)
		{
			_dbSubmission = dbSubmission;
		}

		public ActionResult Index()
		{
			var vm = _dbSubmission.tbl_lst_SignatureScope
				.AsNoTracking()
				.OrderByDescending(x => x.Active)
				.ThenBy(x => x.Scope)
				.ProjectTo<SignatureScopesViewModel>();

			return View(vm);
		}

		public ActionResult Edit(int? id, Mode mode)
		{
			var vm = new SignatureScopesViewModel();
			vm.Active = true;

			if (mode == Mode.Edit)
			{
				var signatureScope = _dbSubmission.tbl_lst_SignatureScope.Find(id);
				vm = Mapper.Map<SignatureScopesViewModel>(signatureScope);
			}

			vm.Mode = mode;

			return View(vm);
		}

		[HttpPost]
		public ActionResult Edit(int? id, SignatureScopesViewModel vm)
		{
			var signatureScope = new tbl_lst_SignatureScope();

			if (vm.Mode == Mode.Edit)
			{
				signatureScope = _dbSubmission.tbl_lst_SignatureScope.Find(id);
			}

			signatureScope.Active = vm.Active;
			signatureScope.Scope = vm.Scope;

			if (vm.Mode == Mode.Add)
			{
				_dbSubmission.tbl_lst_SignatureScope.Add(signatureScope);
			}

			_dbSubmission.SaveChanges();

			CacheManager.Clear(LookupAjax.SignatureScopes.ToString());

			return RedirectToAction("Index");
		}

		[HttpPost]
		public JsonResult CheckDelete(int id)
		{
			var isInUse = _dbSubmission.tblSignatures.Any(x => x.ScopeOfId == id);

			return Json(isInUse);
		}

		[HttpPost]
		public ActionResult Delete(int id)
		{
			var signatureScope = new tbl_lst_SignatureScope { Id = id };
			_dbSubmission.Entry(signatureScope).State = EntityState.Deleted;
			_dbSubmission.SaveChanges();

			CacheManager.Clear(LookupAjax.SignatureScopes.ToString());

			return RedirectToAction("Index");
		}

		[HttpPost]
		public ActionResult SetActive(int id, Mode mode)
		{
			bool active = (mode == Mode.Activate ? true : false);

			var signatureScope = _dbSubmission.tbl_lst_SignatureScope.Find(id);
			signatureScope.Active = active;
			_dbSubmission.SaveChanges();

			CacheManager.Clear(LookupAjax.SignatureScopes.ToString());

			return RedirectToAction("Index");
		}

		public JsonResult ValidateCode(int? id, string scope, Mode mode)
		{
			var validates = true;

			if (!string.IsNullOrEmpty(scope) && _dbSubmission.tbl_lst_SignatureScope.Any(x => x.Id != id && x.Scope == scope))
			{
				validates = false;
			}

			return Json(validates, JsonRequestBehavior.AllowGet);
		}
	}
}