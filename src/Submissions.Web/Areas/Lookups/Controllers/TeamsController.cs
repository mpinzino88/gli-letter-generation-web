﻿extern alias ZEFCore;
using AutoMapper;
using GLI.EFCore.Submission;
using Submissions.Common;
using Submissions.Web.Areas.Lookups.Models;
using System.Linq;
using System.Web.Mvc;
using ZEFCore::Z.EntityFramework.Plus;

namespace Submissions.Web.Areas.Lookups.Controllers
{
	[AuthorizeUser(Permission = Permission.UserFields, HasAccessRight = AccessRight.Edit)]
	public class TeamsController : Controller, ILookupDelete<int>
	{
		private readonly SubmissionContext _dbSubmission;

		public TeamsController(SubmissionContext dbSubmission)
		{
			_dbSubmission = dbSubmission;
		}

		public ActionResult Index() => View(new TeamsIndexViewModel());

		public ActionResult Edit(int? id, Mode mode)
		{
			var vm = new TeamsEditViewModel();

			if (mode == Mode.Edit)
			{
				vm = Mapper.Map<TeamsEditViewModel>(_dbSubmission.tbl_lst_Teams.Find(id));
			}

			vm.Mode = mode;

			return View(vm);
		}

		[HttpPost]
		public ActionResult Edit(TeamsEditViewModel vm)
		{
			var team = new tbl_lst_Teams();

			if (vm.Mode == Mode.Edit)
				team = _dbSubmission.tbl_lst_Teams.Find(vm.Id);

			team.Code = vm.Code.Trim().ToUpper();
			team.Description = vm.Description;
			team.Active = vm.Active;

			if (vm.Mode == Mode.Add)
				_dbSubmission.tbl_lst_Teams.Add(team);

			_dbSubmission.SaveChanges();
			CacheManager.Clear(LookupAjax.Teams.ToString());
			return RedirectToAction("Index");
		}

		public JsonResult CheckDelete(int id)
		{
			var result = _dbSubmission.trLogins.Any(x => x.TeamId == id);
			return Json(result);
		}

		public ActionResult Delete(int id)
		{
			_dbSubmission.tbl_lst_Teams.Where(x => x.Id == id).Delete();
			CacheManager.Clear(LookupAjax.Teams.ToString());

			return RedirectToAction("Index");
		}

		public ActionResult SetActive(TeamsEditViewModel vm)
		{
			var team = _dbSubmission.tbl_lst_Teams.Find(vm.Id);

			if (vm.Mode == Mode.Deactivate)
			{
				team.Active = false;
			}
			else if (vm.Mode == Mode.Activate)
			{
				team.Active = true;
			}

			_dbSubmission.SaveChanges();
			CacheManager.Clear(LookupAjax.Teams.ToString());

			return RedirectToAction("Index");
		}

		[HttpPost]
		public ContentResult AddUsers(UsersTeamPayload payload)
		{
			try
			{
				_dbSubmission.ChangeTracker.AutoDetectChangesEnabled = false;
				foreach (var userId in payload.UserIds)
				{
					var updatedUser = new trLogin
					{
						Id = userId,
						TeamId = payload.TeamId
					};

					_dbSubmission.trLogins.Attach(updatedUser);
					_dbSubmission.Entry(updatedUser).Property(x => x.TeamId).IsModified = true;
				}

				_dbSubmission.SaveChanges();
			}
			finally
			{
				_dbSubmission.ChangeTracker.AutoDetectChangesEnabled = true;
			}

			return Content(ComUtil.JsonEncodeCamelCase(""), "application/json");
		}

		[HttpPost]
		public ContentResult RemoveUsers(UsersTeamPayload payload)
		{
			try
			{
				_dbSubmission.ChangeTracker.AutoDetectChangesEnabled = false;
				foreach (var userId in payload.UserIds)
				{
					var updatedUser = new trLogin
					{
						Id = userId,
						TeamId = null
					};

					_dbSubmission.trLogins.Attach(updatedUser);
					_dbSubmission.Entry(updatedUser).Property(x => x.TeamId).IsModified = true;
				}

				_dbSubmission.SaveChanges();
			}
			finally
			{
				_dbSubmission.ChangeTracker.AutoDetectChangesEnabled = true;
			}

			return Content(ComUtil.JsonEncodeCamelCase(""), "application/json");
		}
	}
}