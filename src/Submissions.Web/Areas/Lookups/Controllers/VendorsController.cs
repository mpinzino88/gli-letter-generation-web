﻿extern alias ZEFCore;
using AutoMapper.QueryableExtensions;
using GLI.EFCore.Submission;
using Microsoft.EntityFrameworkCore;
using Submissions.Common;
using Submissions.Web.Areas.Lookups.Models;
using System.Linq;
using System.Web.Mvc;
using ZEFCore::Z.EntityFramework.Plus;

namespace Submissions.Web.Areas.Lookups.Controllers
{
	public class VendorsController : Controller
	{
		private readonly SubmissionContext _dbSubmission;

		public VendorsController(SubmissionContext dbSubmission)
		{
			_dbSubmission = dbSubmission;
		}

		public ActionResult Index()
		{
			var vendors = _dbSubmission.tbl_lst_Vendors
				.AsNoTracking()
				.OrderByDescending(x => x.Active)
				.ThenBy(x => x.Description)
				.ProjectTo<VendorsViewModel>();

			return View(vendors);
		}

		public ActionResult Edit(int? id, Mode mode)
		{
			var vm = new VendorsViewModel();
			vm.Active = true;

			if (mode == Mode.Edit)
			{
				var vendor = _dbSubmission.tbl_lst_Vendors.Find(id);
				vm.Id = vendor.Id;
				vm.Description = vendor.Description;
			}

			vm.Mode = mode;

			return View(vm);
		}

		[HttpPost]
		public ActionResult Edit(int? id, VendorsViewModel vm)
		{
			var vendor = new tbl_lst_Vendors();
			if (vm.Mode == Mode.Edit)
			{
				vendor = _dbSubmission.tbl_lst_Vendors.Find(vm.Id);
			}

			vendor.Description = vm.Description;
			vendor.Active = vm.Active;

			if (vm.Mode == Mode.Add)
			{
				_dbSubmission.tbl_lst_Vendors.Add(vendor);
			}
			_dbSubmission.SaveChanges();

			CacheManager.Clear(LookupAjax.Vendors.ToString());

			return RedirectToAction("Index");
		}

		[HttpPost]
		public JsonResult CheckDelete(int id)
		{
			return Json(false);
		}

		[HttpPost]
		public ActionResult Delete(int id)
		{
			_dbSubmission.tbl_lst_Vendors.Where(x => x.Id == id).Delete();

			CacheManager.Clear(LookupAjax.Vendors.ToString());

			return RedirectToAction("Index");
		}

		public JsonResult ValidateVendor(int? id, string description, Mode mode)
		{
			var validates = true;

			if (!string.IsNullOrEmpty(description) && _dbSubmission.tbl_lst_Vendors.Any(x => x.Id != id && x.Description == description))
			{
				validates = false;
			}

			return Json(validates, JsonRequestBehavior.AllowGet);
		}

		[HttpPost]
		public ActionResult SetActive(int id, Mode mode)
		{
			var active = (mode == Mode.Activate ? true : false);
			var vendor = _dbSubmission.tbl_lst_Vendors.Find(id);
			vendor.Active = active;
			_dbSubmission.SaveChanges();

			CacheManager.Clear(LookupAjax.Vendors.ToString());

			return RedirectToAction("Index");
		}
	}
}