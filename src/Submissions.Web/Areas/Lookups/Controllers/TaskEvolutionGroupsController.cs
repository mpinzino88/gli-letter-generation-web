﻿extern alias ZEFCore;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using EF.eResultsManaged;
using GLI.EFCore.Submission;
using Microsoft.EntityFrameworkCore;
using Submissions.Common;
using Submissions.Web.Areas.Lookups.Models;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using ZEFCore::Z.EntityFramework.Plus;

namespace Submissions.Web.Areas.Lookups.Controllers
{
	[AuthorizeUser(Permission = Permission.Admin, HasAccessRight = AccessRight.Edit)]
	public class TaskEvolutionGroupsController : Controller
	{
		private readonly SubmissionContext _dbSubmission;
		private readonly IeResultsManagedContext _dbEResultsManaged;

		public TaskEvolutionGroupsController(SubmissionContext dbSubmission, IeResultsManagedContext dbEResultsManaged)
		{
			_dbSubmission = dbSubmission;
			_dbEResultsManaged = dbEResultsManaged;
		}

		public ActionResult Index()
		{
			var taskEvolutionGroups = _dbSubmission.trTaskEvolutionGroups
				.AsNoTracking()
				.OrderByDescending(x => x.Status)
				.ProjectTo<TaskEvolutionGroupsViewModel>()
				.ToList();

			var evoTCIds = taskEvolutionGroups.Select(y => y.EvolutionTCId).ToList();
			var initialData = _dbEResultsManaged.QuestionBase
				.Where(x => evoTCIds.Contains(x.Id))
				.ToList();

			foreach (var taskEvoGroups in taskEvolutionGroups)
			{
				taskEvoGroups.EvolutionTC = initialData
					.Where(x => x.Id == taskEvoGroups.EvolutionTCId)
					.Select(x => x.Name)
					.FirstOrDefault();
			}
			var test = taskEvolutionGroups.ToList();

			return View(taskEvolutionGroups);
		}

		public ActionResult Edit(int? id, Mode mode)
		{
			var vm = new TaskEvolutionGroupsViewModel();
			vm.Active = true;

			if (mode == Mode.Edit)
			{
				var taskEvolutionGroups = _dbSubmission.trTaskEvolutionGroups.Find(id);
				vm = Mapper.Map<TaskEvolutionGroupsViewModel>(taskEvolutionGroups);
				vm.EvolutionTC = _dbEResultsManaged.QuestionBase
					.Where(x => x.Id == vm.EvolutionTCId)
					.Select(x => x.Name)
					.FirstOrDefault();
			}

			if (mode == Mode.Add)
			{
				vm.EvolutionTC = "";
				vm.TaskDefinition = "";
			}

			vm.Mode = mode;
			return View(vm);
		}

		[HttpPost]
		public ActionResult Edit(int? id, TaskEvolutionGroupsViewModel vm)
		{
			var taskEvolutionGroups = new trTaskEvolutionGroups();
			if (vm.Mode == Mode.Edit)
			{
				taskEvolutionGroups = _dbSubmission.trTaskEvolutionGroups.Find(id);
			}

			if (vm.TaskDefinitionIds.Count() > 0 && vm.Mode == Mode.Add)
			{
				var trEvoGroups = new List<trTaskEvolutionGroups>();
				foreach (var taskDefs in vm.TaskDefinitionIds)
				{
					trEvoGroups.Add(new trTaskEvolutionGroups
					{
						Id = vm.Id ?? 0,
						EvolutionTCId = vm.EvolutionTCId,
						Status = vm.Active,
						TaskDefId = taskDefs
					});
				}
				_dbSubmission.trTaskEvolutionGroups.AddRange(trEvoGroups);
			}

			taskEvolutionGroups.Id = vm.Id ?? 0;
			taskEvolutionGroups.EvolutionTCId = vm.EvolutionTCId;
			taskEvolutionGroups.Status = vm.Active;
			taskEvolutionGroups.TaskDefId = vm.TaskDefinitionId;

			_dbSubmission.SaveChanges();

			CacheManager.Clear(LookupAjax.TestCase.ToString());

			return RedirectToAction("Index");
		}


		[HttpPost]
		public JsonResult CheckDelete(int id)
		{
			var isInUse = _dbSubmission.trTaskEvolutionGroups.Any(x => x.Id == id);
			return Json(isInUse);
		}

		[HttpPost]
		public ActionResult Delete(int id)
		{
			_dbSubmission.trTaskEvolutionGroups.Where(x => x.Id == id).Delete();

			CacheManager.Clear(LookupAjax.TestCase.ToString());

			return RedirectToAction("Index");
		}

		[HttpPost]
		public ActionResult SetActive(int id, Mode mode)
		{
			var active = (mode == Mode.Deactivate) ? false : true;

			var taskEvolutionGroups = _dbSubmission.trTaskEvolutionGroups.Find(id);
			taskEvolutionGroups.Status = active;

			_dbSubmission.SaveChanges();

			CacheManager.Clear(LookupAjax.TestCase.ToString());

			return RedirectToAction("Index");

		}
	}
}