﻿extern alias ZEFCore;
using AutoMapper.QueryableExtensions;
using GLI.EFCore.Submission;
using Microsoft.EntityFrameworkCore;
using Submissions.Common;
using Submissions.Web.Areas.Lookups.Models;
using System.Linq;
using System.Web.Mvc;
using ZEFCore::Z.EntityFramework.Plus;

namespace Submissions.Web.Areas.Lookups.Controllers
{
	[AuthorizeUser(Permission = Permission.Admin, HasAccessRight = AccessRight.Edit)]
	public class CountriesController : Controller, ILookupDelete<int>
	{
		private readonly SubmissionContext _dbSubmission;

		public CountriesController(SubmissionContext dbSubmission)
		{
			_dbSubmission = dbSubmission;
		}

		public ActionResult Index()
		{
			var countries = _dbSubmission.tbl_lst_Country
				.AsNoTracking()
				.OrderByDescending(x => x.Active)
				.ThenBy(x => x.Code)
				.ProjectTo<CountryViewModel>();

			return View(countries);
		}

		public ActionResult Edit(int? id, Mode mode)
		{
			var vm = new CountryViewModel();
			vm.Active = true;

			if (mode == Mode.Edit)
			{
				var country = _dbSubmission.tbl_lst_Country.Find(id);

				vm.Id = country.Id;
				vm.Description = country.Description;
				vm.Code = country.Code;
			}
			vm.Mode = mode;

			return View(vm);
		}

		[HttpPost]
		public ActionResult Edit(int? id, CountryViewModel vm)
		{
			var country = new tbl_lst_Country();
			if (vm.Mode == Mode.Edit)
			{
				country = _dbSubmission.tbl_lst_Country.Find(id);
			}

			country.Id = vm.Id ?? 0;
			country.Code = vm.Code.ToUpper();
			country.Description = vm.Description;
			country.Active = vm.Active;

			if (vm.Mode == Mode.Add)
			{
				_dbSubmission.tbl_lst_Country.Add(country);
			}

			_dbSubmission.SaveChanges();

			CacheManager.Clear(LookupAjax.Countries.ToString());

			return RedirectToAction("Index");
		}

		[HttpPost]
		public JsonResult CheckDelete(int id)
		{
			var country = _dbSubmission.tbl_lst_Country.Find(id);

			var isInUseJuris = _dbSubmission.tbl_lst_fileJuris.Any(x => x.CountryId == id);
			var isInUseManu = _dbSubmission.tbl_lst_fileManu.Any(x => x.Country == country.Code);

			var isInUse = isInUseJuris || isInUseManu;

			return Json(isInUse);
		}

		[HttpPost]
		public ActionResult Delete(int id)
		{
			_dbSubmission.tbl_lst_Country.Where(x => x.Id == id).Delete();

			CacheManager.Clear(LookupAjax.Countries.ToString());

			return RedirectToAction("Index");
		}

		public JsonResult ValidateCountry(int? id, string description, string code, Mode mode)
		{
			var validates = true;

			if (!string.IsNullOrEmpty(description) && _dbSubmission.tbl_lst_Country.Any(x => x.Id != id && x.Description == description))
			{
				validates = false;
			}
			if (!string.IsNullOrEmpty(code) && _dbSubmission.tbl_lst_Country.Any(x => x.Id != id && x.Code == code))
			{
				validates = false;
			}

			return Json(validates, JsonRequestBehavior.AllowGet);
		}

		[HttpPost]
		public ActionResult SetActive(int id, Mode mode)
		{
			var active = (mode == Mode.Deactivate) ? false : true;

			var country = _dbSubmission.tbl_lst_Country.Find(id);
			country.Active = active;

			_dbSubmission.SaveChanges();

			CacheManager.Clear(LookupAjax.Countries.ToString());

			return RedirectToAction("Index");
		}
	}
}