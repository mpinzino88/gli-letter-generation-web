﻿extern alias ZEFCore;
using AutoMapper.QueryableExtensions;
using GLI.EFCore.Submission;
using Microsoft.EntityFrameworkCore;
using Submissions.Common;
using Submissions.Web.Areas.Lookups.Models;
using System.Linq;
using System.Web.Mvc;
using ZEFCore::Z.EntityFramework.Plus;

namespace Submissions.Web.Areas.Lookups.Controllers
{
	public class GamingGuidelineProtocolsController : Controller, ILookupDelete<int>
	{
		private readonly SubmissionContext _dbSubmission;

		public GamingGuidelineProtocolsController(SubmissionContext dbSubmission)
		{
			_dbSubmission = dbSubmission;
		}

		public ActionResult Index()
		{
			var protocols = _dbSubmission.tbl_lst_GamingGuidelineProtocolType
				.AsNoTracking()
				.OrderBy(x => x.Active)
				.ThenBy(x => x.Name)
				.ProjectTo<GamingGuidelineProtocolsViewModel>();

			return View(protocols);
		}

		public ActionResult Edit(int? id, Mode mode)
		{
			var vm = new GamingGuidelineProtocolsViewModel();
			vm.Active = true;

			if (mode == Mode.Edit)
			{
				var protocols = _dbSubmission.tbl_lst_GamingGuidelineProtocolType.Find(id);

				vm.Id = protocols.Id;
				vm.Name = protocols.Name;
				vm.Note = protocols.Note;
			}
			vm.Mode = mode;

			return View(vm);
		}

		[HttpPost]
		public ActionResult Edit(int? id, GamingGuidelineProtocolsViewModel vm)
		{
			var protocols = new tbl_lst_GamingGuidelineProtocolType();
			if (vm.Mode == Mode.Edit)
			{
				protocols = _dbSubmission.tbl_lst_GamingGuidelineProtocolType.Find(id);
			}
			protocols.Id = vm.Id ?? 0;
			protocols.Name = vm.Name;
			protocols.Note = vm.Note;
			protocols.Active = vm.Active;

			if (vm.Mode == Mode.Add)
			{
				_dbSubmission.tbl_lst_GamingGuidelineProtocolType.Add(protocols);
			}

			_dbSubmission.SaveChanges();

			CacheManager.Clear(LookupAjax.GamingGuidelineProtocols.ToString());

			return RedirectToAction("Index");
		}

		[HttpPost]
		public JsonResult CheckDelete(int id)
		{
			var protocols = _dbSubmission.tbl_lst_GamingGuidelineProtocolType.Find(id);
			var isInUse = _dbSubmission.GamingGuidelineProtocolType.Any(x => x.GamingGuidelineId == id) && protocols.Active;

			return Json(isInUse);
		}

		[HttpPost]
		public ActionResult Delete(int id)
		{
			_dbSubmission.tbl_lst_GamingGuidelineProtocolType.Where(x => x.Id == id).Delete();
			_dbSubmission.GamingGuidelineProtocolType.Where(x => x.ProtocolTypeId == id).Delete();

			CacheManager.Clear(LookupAjax.GamingGuidelineProtocols.ToString());

			return RedirectToAction("Index");
		}

		public JsonResult ValidateProtocolType(int? id, string name)
		{
			var validates = true;

			if (!string.IsNullOrEmpty(name) && _dbSubmission.tbl_lst_GamingGuidelineProtocolType.Any(x => x.Id != id && x.Name == name))
			{
				validates = false;
			}
			return Json(validates, JsonRequestBehavior.AllowGet);
		}

		[HttpPost]
		public ActionResult SetActive(int id, Mode mode)
		{
			var active = (mode == Mode.Deactivate) ? false : true;

			var protocol = _dbSubmission.tbl_lst_GamingGuidelineProtocolType.Find(id);
			protocol.Active = active;

			_dbSubmission.SaveChanges();

			CacheManager.Clear(LookupAjax.GamingGuidelineProtocols.ToString());

			return RedirectToAction("Index");
		}
	}
}