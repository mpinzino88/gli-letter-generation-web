﻿extern alias ZEFCore;
using GLI.EFCore.Submission;
using Microsoft.EntityFrameworkCore;
using Submissions.Common;
using Submissions.Web.Areas.Lookups.Models;
using Submissions.Web.Areas.Tools.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using ZEFCore::Z.EntityFramework.Plus;


namespace Submissions.Web.Areas.Lookups.Controllers
{
	[AuthorizeUser(Permissions = new Permission[] { Permission.Admin, Permission.AdminManager }, HasAccessRight = AccessRight.Edit)]
	public class ManufacturerGroupsController : Controller, ILookupDelete<int>
	{
		private readonly SubmissionContext _dbSubmission;

		public ManufacturerGroupsController(SubmissionContext dbSubmission)
		{
			_dbSubmission = dbSubmission;
		}

		public ActionResult Index(string type)
		{
			var vm = new ManufacturerGroupsIndexViewModel
			{
				Type = type,
				Heading = GetHeading(type)
			};
			vm.ListItems = GetManufacturerGroups(vm);
			return View(vm);
		}

		[HttpPost]
		public JsonResult Index(ManufacturerGroupsIndexViewModel vm)
		{
			return Json(ComUtil.JsonEncodeCamelCase(GetManufacturerGroups(vm)), JsonRequestBehavior.AllowGet);
		}

		public ActionResult Add(string type = null)
		{
			return EditView(null, Mode.Add, type);
		}

		public ActionResult Edit(int? id)
		{
			return EditView(id, Mode.Edit);
		}

		private ActionResult EditView(int? id, Mode mode, string type = null)
		{
			var vm = new ManufacturerGroupsEditViewModel();

			ManufacturerGroupCategories cat = null;

			if (mode == Mode.Edit)
			{
				var manufacturerGroup = _dbSubmission.ManufacturerGroups.Find(id);
				vm.Id = manufacturerGroup.Id;
				vm.Description = manufacturerGroup.Description;
				vm.Studios = LoadStudios((int)id);
				vm.ProductLines = LoadProductLines((int)id);
				vm.Platforms = LoadPlatforms((int)id);
				vm = SetupManufGroupIncomingSettings(vm);
				vm = SetupEditViewModel(vm);

				cat = manufacturerGroup.ManufacturerGroupCategory;
			}
			else if (!string.IsNullOrWhiteSpace(type))
			{
				cat = _dbSubmission.ManufacturerGroupCategories.FirstOrDefault(c => c.Code == type);
			}

			vm.Mode = mode;
			vm.ManufacturerGroupCategory = cat?.Code;
			vm.ManufacturerGroupCategoryId = cat?.Id;
			vm.Heading = GetHeading(cat?.Code);

			return View("Edit", vm);
		}

		[HttpPost]
		public ActionResult Edit(ManufacturerGroupsEditViewModel vm)
		{
			var exists = _dbSubmission.ManufacturerGroups.Any(x =>
				x.Id != vm.Id &&
				x.Description.ToLower() == vm.Description.ToLower() &&
				x.ManufacturerGroupCategoryId == vm.ManufacturerGroupCategoryId
				);

			if (exists)
			{
				ModelState.AddModelError("Description", "A manufacturer group with this name and category already exists.");
				vm = SetupEditViewModel(vm);
				return View(vm);
			}

			// update manufacturer group
			var manufacturerGroup = new ManufacturerGroups();
			var studio = new tbl_lst_Studios();
			var productLine = new tbl_lst_ProductLines();
			var platform = new tbl_lst_Platform();

			if (vm.Mode == Mode.Edit)
			{
				manufacturerGroup = _dbSubmission.ManufacturerGroups.Find(vm.Id);

				foreach (var item in vm.Studios.Where(x => x.StudioId != null && !x._destroy))
				{
					studio = _dbSubmission.tbl_lst_Studios.Find(item.StudioId);
					studio.Name = item.StudioName.Text;
					studio.TestingTypeId = item.TestingTypeId;
					_dbSubmission.SaveChanges();
				}

				foreach (var item in vm.ProductLines.Where(x => x.ProductLineId != null && !x._destroy))
				{
					productLine = _dbSubmission.tbl_lst_ProductLines.Find(item.ProductLineId);
					productLine.Name = item.ProductLineName.Text;
					productLine.TestingTypeId = item.TestingTypeId;
					_dbSubmission.SaveChanges();
				}

				vm.Platforms.Where(x => x.PlatformId != null && !x._destroy).ToList()
					.ForEach(x =>
					{
						var platformToEdit = _dbSubmission.tbl_lst_Platforms.Find(x.PlatformId);
						if (!platformToEdit.Name.Equals(x.PlatformName.Text))
						{
							platformToEdit.Name = x.PlatformName.Text;
							_dbSubmission.tbl_lst_Platforms.Attach(platformToEdit);
							_dbSubmission.Entry(platformToEdit).Property(y => y.Name).IsModified = true;
							_dbSubmission.SaveChanges();
						}
					});
			}

			manufacturerGroup.Id = vm.Id ?? 0;
			manufacturerGroup.Description = vm.Description;
			manufacturerGroup.ManufacturerGroupCategoryId = vm.ManufacturerGroupCategoryId;


			if (vm.Mode == Mode.Add)
			{
				_dbSubmission.ManufacturerGroups.Add(manufacturerGroup);
				_dbSubmission.SaveChanges();
			}

			vm.Platforms.Where(x => x.PlatformId == null && x.PlatformName.Text != null).ToList()
				.ForEach(x => _dbSubmission.tbl_lst_Platforms.Add(new tbl_lst_Platform() { Name = x.PlatformName.Text, ManufacturerGroupId = manufacturerGroup.Id }));

			var productLinesToAdd = vm.ProductLines.Where(x => x.ProductLineId == null && x.ProductLineName.Text != null).ToList();
			foreach (var item in productLinesToAdd)
			{

				var productLineGroup = new tbl_lst_ProductLines
				{
					Name = item.ProductLineName.Text,
					TestingTypeId = item.TestingTypeId,
					ManufacturerGroupId = manufacturerGroup.Id
				};
				_dbSubmission.tbl_lst_ProductLines.Add(productLineGroup);
			}

			var studiosToAdd = vm.Studios.Where(x => x.StudioId == null && x.StudioName.Text != null).ToList();
			foreach (var item in studiosToAdd)
			{

				var studioGroup = new tbl_lst_Studios
				{
					Name = item.StudioName.Text,
					TestingTypeId = item.TestingTypeId,
					ManufacturerGroupId = manufacturerGroup.Id
				};
				_dbSubmission.tbl_lst_Studios.Add(studioGroup);
			}

			var studiosToDelete = vm.Studios.Where(x => x._destroy == true).ToList();
			foreach (var item in studiosToDelete)
			{
				_dbSubmission.tbl_lst_Studios.Where(x => x.Id == item.StudioId).Delete();
			}

			var productLinesToDelete = vm.ProductLines.Where(x => x._destroy == true).ToList();
			foreach (var item in productLinesToDelete)
			{
				_dbSubmission.tbl_lst_ProductLines.Where(x => x.Id == item.ProductLineId).Delete();
			}



			//Check if Platforms are in use
			var PlatformIds = _dbSubmission.tbl_lst_Platforms.Where(x => x.ManufacturerGroupId == manufacturerGroup.Id).Select(x => x.Id).ToList();
			if (!_dbSubmission.SubmissionPlatforms.Where(x => PlatformIds.Contains(x.PlatformId)).Any())
			{
				var destoryPlatformId = vm.Platforms.Where(x => x._destroy == true).Select(plat => plat.PlatformId).ToList();
				//Remove prtocol platform dependancy
				_dbSubmission.tbl_lst_ProtocolDeficiencytbl_lst_Platform.Where(x => destoryPlatformId.Contains(x.PlatformId)).Delete();

				/* Reduce query to database
				 vm.Platforms.Where(x => x._destroy == true).ToList()
				.ForEach(x => { _dbSubmission.tbl_lst_Platforms.Where(y => y.Id == x.PlatformId).Delete(); });
				*/
				_dbSubmission.tbl_lst_Platforms.Where(x => destoryPlatformId.Contains(x.Id)).Delete();
			}
			_dbSubmission.SaveChanges();

			// update manufacturers
			if (vm.ManufacturerCodes.Length > 0)
			{
				_dbSubmission.tbl_lst_fileManuManufacturerGroups.Where(x => x.ManufacturerGroupId == manufacturerGroup.Id).Delete();

				foreach (var item in vm.ManufacturerCodes)
				{
					var manufacturerGroupLink = new tbl_lst_fileManuManufacturerGroups
					{
						ManufacturerGroupId = manufacturerGroup.Id,
						ManufacturerCode = item
					};

					_dbSubmission.tbl_lst_fileManuManufacturerGroups.Add(manufacturerGroupLink);
				}

				_dbSubmission.SaveChanges();
			}

			AddModifyManufGroupIncomingSettings(vm, manufacturerGroup);

			CacheManager.Clear(LookupAjax.ManufacturerGroups.ToString());

			return RedirectToAction("Index");
		}

		[HttpPost]
		public ActionResult SetActive(int id, Mode mode)
		{
			throw new NotSupportedException();
		}

		[HttpPost]
		public ActionResult Delete(int id)
		{
			string type = null;
			var group = _dbSubmission.ManufacturerGroups.FirstOrDefault(x => x.Id == id);
			if (group != null)
			{
				type = group.ManufacturerGroupCategory.Code;
			}

			var platformIds = _dbSubmission.tbl_lst_Platforms.Where(x => x.ManufacturerGroupId == id && x.ProtocolDeficiencies.Any()).Select(platform => platform.Id).ToList();
			_dbSubmission.tbl_lst_ProtocolDeficiencytbl_lst_Platform.Where(x => platformIds.Contains(x.PlatformId)).Delete();

			_dbSubmission.tbl_lst_fileManuManufacturerGroups.Where(x => x.ManufacturerGroupId == id).Delete();
			_dbSubmission.tbl_lst_Platforms.Where(x => x.ManufacturerGroupId == id).Delete();
			_dbSubmission.tbl_lst_Studios.Where(x => x.ManufacturerGroupId == id).Delete();
			_dbSubmission.tbl_lst_ProductLines.Where(x => x.ManufacturerGroupId == id).Delete();
			_dbSubmission.ManufGroupIncomingSettings.Where(x => x.ManufacturerGroupId == id).Delete();
			_dbSubmission.ManufacturerGroups.Where(x => x.Id == id).Delete();

			CacheManager.Clear(LookupAjax.ManufacturerGroups.ToString());

			return RedirectToAction("Index", new { type });
		}

		[HttpPost]
		public JsonResult CheckDelete(int id)
		{
			//Check if studios are in use
			var studioIds = _dbSubmission.tbl_lst_Studios.Where(x => x.ManufacturerGroupId == id).Select(x => x.Id).ToList();
			if (_dbSubmission.Submissions.Where(x => studioIds.Contains(x.StudioId ?? 0)).Any())
			{
				return Json(true);
			}
			//Check if Productline are in use
			var productlineIds = _dbSubmission.tbl_lst_ProductLines.Where(x => x.ManufacturerGroupId == id).Select(x => x.Id).ToList();
			if (_dbSubmission.tbl_lst_ProductLinesJurisdictionalData.Where(x => productlineIds.Contains(x.ProductLineId)).Any())
			{
				return Json(true);
			}
			//Check if Platforms are in use
			var PlatformIds = _dbSubmission.tbl_lst_Platforms.Where(x => x.ManufacturerGroupId == id).Select(x => x.Id).ToList();
			if (_dbSubmission.SubmissionPlatforms.Where(x => PlatformIds.Contains(x.PlatformId)).Any())
			{
				return Json(true);
			}

			return Json(false);
		}

		private string GetHeading(string type)
		{
			return _dbSubmission.ManufacturerGroupCategories.SingleOrDefault(c => c.Code == type)?.Description ?? "Manufacturer Groups";
		}

		private List<ManufacturerGroupsIndexListItemModel> GetManufacturerGroups(ManufacturerGroupsIndexViewModel vm)
		{
			var groups = _dbSubmission.ManufacturerGroups.Include(x => x.ManufacturerLinks).Include(x => x.ManufacturerGroupCategory).AsQueryable();
			if (vm.ManufacturerCodes != null && vm.ManufacturerCodes.Any())
			{
				groups = groups
					.Where(mg => mg.ManufacturerLinks.Any(ml => vm.ManufacturerCodes.Contains(ml.ManufacturerCode)))
					.AsQueryable();
			}
			if (!string.IsNullOrWhiteSpace(vm.Type))
			{
				var type = _dbSubmission.ManufacturerGroupCategories.FirstOrDefault(c => c.Code == vm.Type);
				if (type != null)
				{
					groups = groups.Where(mg => mg.ManufacturerGroupCategoryId == type.Id);
				}
			}
			var result = groups.Select(mg => new ManufacturerGroupsIndexListItemModel
			{
				Name = mg.Description,
				Id = mg.Id,
				Category = mg.ManufacturerGroupCategory.Description
			}
			);
			return result.ToList();
		}

		private IList<Studio> LoadStudios(int id)
		{
			var filters = new List<Studio>();
			var studios = _dbSubmission.tbl_lst_Studios
				.Where(x => x.ManufacturerGroupId == id)
				.Select(x => new
				{
					StudioId = x.Id,
					StudioName = x.Name,
					TestingTypeId = x.TestingType.Id,
					TestingTypeName = x.TestingType.Code
				})
				.OrderBy(x => x.StudioId)
				.ToList();

			foreach (var studio in studios)
			{
				var filter = new Studio
				{
					StudioId = studio.StudioId,
					StudioName = new SelectListItem { Value = studio.StudioId.ToString(), Text = studio.StudioName },
					TestingTypeId = studio.TestingTypeId,
					TestingTypeName = new SelectListItem { Value = studio.TestingTypeId.ToString(), Text = studio.TestingTypeName }
				};

				filters.Add(filter);
			}

			return filters.OrderBy(x => x.TestingTypeName.Text).ThenBy(x => x.StudioName.Text).ToList();
		}

		private IList<ProductLine> LoadProductLines(int id)
		{
			var filters = new List<ProductLine>();
			var productLines = _dbSubmission.tbl_lst_ProductLines
				.Where(x => x.ManufacturerGroupId == id)
				.Select(x => new
				{
					ProductLineId = x.Id,
					ProductLineName = x.Name,
					TestingTypeId = x.TestingType.Id,
					TestingTypeName = x.TestingType.Code
				})
				.OrderBy(x => x.ProductLineId)
				.ToList();

			foreach (var productLine in productLines)
			{
				var filter = new ProductLine
				{
					ProductLineId = productLine.ProductLineId,
					ProductLineName = new SelectListItem { Value = productLine.ProductLineId.ToString(), Text = productLine.ProductLineName },
					TestingTypeId = productLine.TestingTypeId,
					TestingTypeName = new SelectListItem { Value = productLine.TestingTypeId.ToString(), Text = productLine.TestingTypeName }
				};

				filters.Add(filter);
			}

			return filters.OrderBy(x => x.TestingTypeName.Text).ThenBy(x => x.ProductLineName.Text).ToList();
		}

		private IList<Models.Platform> LoadPlatforms(int id)
		{
			return _dbSubmission.tbl_lst_Platforms
				.Where(x => x.ManufacturerGroupId == id)
				.OrderBy(x => x.Name)
				.Select(x => new Models.Platform()
				{
					PlatformId = x.Id,
					PlatformName = new SelectListItem { Value = x.Id.ToString(), Text = x.Name }
				})
				.ToList();
		}


		#region Helper Methods
		private ManufacturerGroupsEditViewModel SetupEditViewModel(ManufacturerGroupsEditViewModel vm)
		{
			List<SelectListItem> manufacturers;

			if (vm.Mode == Mode.Add)
			{
				manufacturers = _dbSubmission.tbl_lst_fileManu
					.Where(x => vm.ManufacturerCodes.Contains(x.Code))
					.Select(x => new SelectListItem { Value = x.Code, Text = x.Description, Selected = true })
					.ToList();
			}
			else
			{
				manufacturers = _dbSubmission.tbl_lst_fileManuManufacturerGroups
					.Where(x => x.ManufacturerGroupId == vm.Id)
					.Select(x => new SelectListItem { Value = x.ManufacturerCode, Text = x.Manufacturer.Description, Selected = true })
					.ToList();
			}

			vm.ManufacturerCodes = manufacturers.Select(x => x.Value).ToArray();
			vm.Manufacturers = manufacturers;
			return vm;
		}

		private ManufacturerGroupsEditViewModel SetupManufGroupIncomingSettings(ManufacturerGroupsEditViewModel vm)
		{
			var manufGroupIncomingSetting = _dbSubmission.ManufGroupIncomingSettings
				.Where(x => x.ManufacturerGroupId == vm.Id)
				.FirstOrDefault();
			if (manufGroupIncomingSetting != null)
			{
				vm.isRequired = manufGroupIncomingSetting.IsRequired;
				vm.isVisible = manufGroupIncomingSetting.IsVisible;
			}
			return vm;
		}

		private void AddModifyManufGroupIncomingSettings(ManufacturerGroupsEditViewModel vm, ManufacturerGroups mg)
		{
			var manufCategoryList = new List<int>() { (int)Common.ManufacturerGroupCategory.Studios, (int)Common.ManufacturerGroupCategory.ProductLines, (int)Common.ManufacturerGroupCategory.Platform };

			if (vm.Mode == Mode.Add && manufCategoryList.Contains((int)vm.ManufacturerGroupCategoryId))
			{
				AddVisibilityRequired(vm, mg);
			}

			if (vm.Mode == Mode.Edit)
			{
				ManufGroupIncomingSettings manufGroupIncomingSettings = _dbSubmission.ManufGroupIncomingSettings
					.Where(x => x.ManufacturerGroupId == vm.Id)
					.FirstOrDefault();
				if (manufGroupIncomingSettings == null && manufCategoryList.Contains((int)vm.ManufacturerGroupCategoryId))
				{
					AddVisibilityRequired(vm, mg);
					return;
				}

				if (manufGroupIncomingSettings != null && manufCategoryList.Contains((int)vm.ManufacturerGroupCategoryId))
				{
					manufGroupIncomingSettings.IsRequired = vm.isRequired;
					manufGroupIncomingSettings.IsVisible = vm.isVisible;
					_dbSubmission.SaveChanges();
				}
				else
				{
					_dbSubmission.ManufGroupIncomingSettings.Where(x => x.ManufacturerGroupId == vm.Id).Delete();
				}
			}
		}

		private void AddVisibilityRequired(ManufacturerGroupsEditViewModel vm, ManufacturerGroups mg)
		{
			var manufGroupIncomingSettings = new ManufGroupIncomingSettings();
			manufGroupIncomingSettings.Id = 0;
			manufGroupIncomingSettings.ManufacturerGroupId = mg.Id;
			manufGroupIncomingSettings.IsVisible = vm.isVisible;
			manufGroupIncomingSettings.IsRequired = vm.isRequired;
			_dbSubmission.ManufGroupIncomingSettings.Add(manufGroupIncomingSettings);
			_dbSubmission.SaveChanges();
		}

		[HttpGet]
		public ActionResult CheckManuGroupDependancy(int manufacturerGroupId)
		{
			var deficiencies = _dbSubmission.tbl_lst_ProtocolDeficiencies
			.Include(def => def.ProtocolDeficiencyPlatforms)
					.ThenInclude(defPlat => defPlat.Platform)
					.Where(def =>
						!def.DeactivateDate.HasValue &&
						def.ProtocolDeficiencyPlatforms.Select(defPlat => defPlat.Platform)
														.Any(platform => platform.ManufacturerGroupId == manufacturerGroupId)
					)
					.Select(def => new ProtocolDeficiency
					{
						Protocol = def.Protocol,
						Critical = def.Critical,
						IncludeOnLetter = def.IncludeOnLetter,
						IncludeOnWVLetter = def.IncludeOnWVLetter,
						Description = def.Description,
						Id = def.Id,
						OriginalId = def.OriginalId,
						Platforms = def.ProtocolDeficiencyPlatforms.Where(defPlat => defPlat.Platform.ManufacturerGroupId == manufacturerGroupId)
						.Select(defPlat => new ProtocolPlatform
						{
							Id = defPlat.Platform.Id,
							Name = defPlat.Platform.Name
						}).ToList()
					}).ToList();

			List<ProtocolDeficiency> relevantDeficiencies = deficiencies;

			return Content(ComUtil.JsonEncodeCamelCase(relevantDeficiencies), "application/json");
		}

		[HttpGet]
		public ActionResult CheckPlatformDependancy(string destroyPlatformId_s)
		{

			List<int> destroyPlatformId = new System.Web.Script.Serialization.JavaScriptSerializer().Deserialize<int[]>(destroyPlatformId_s).ToList();

			List<ProtocolDeficiency> deficiencies = _dbSubmission.tbl_lst_ProtocolDeficiencies
				.Include(def => def.ProtocolDeficiencyPlatforms)
					.ThenInclude(defPlat => defPlat.Platform)
					.Where(def => !def.DeactivateDate.HasValue && def.ProtocolDeficiencyPlatforms.Any(platform => destroyPlatformId.Contains(platform.PlatformId) ))
					.Select(def => new ProtocolDeficiency
					{
						Protocol = def.Protocol,
						Critical = def.Critical,
						IncludeOnLetter = def.IncludeOnLetter,
						IncludeOnWVLetter = def.IncludeOnWVLetter,
						Description = def.Description,
						Id = def.Id,
						OriginalId = def.OriginalId,
						Platforms = def.ProtocolDeficiencyPlatforms.Where(platform => destroyPlatformId.Contains(platform.PlatformId))
						.Select(defPlat => new ProtocolPlatform
						{
							Id = defPlat.Platform.Id,
							Name = defPlat.Platform.Name
						}).ToList()
					}).ToList();

			return Content(ComUtil.JsonEncodeCamelCase(deficiencies), "application/json");

		}
			#endregion
		}
}