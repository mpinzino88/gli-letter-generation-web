﻿extern alias ZEFCore;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using GLI.EFCore.Submission;
using Microsoft.EntityFrameworkCore;
using Submissions.Common;
using Submissions.Web.Areas.Lookups.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using ZEFCore::Z.EntityFramework.Plus;

namespace Submissions.Web.Areas.Lookups.Controllers
{
	[AuthorizeUser(Permission = Permission.Admin, HasAccessRight = AccessRight.Edit)]
	public class EmailTypesController : Controller, ILookupDelete<int>
	{
		private readonly SubmissionContext _dbSubmission;

		public EmailTypesController(SubmissionContext dbSubmission)
		{
			_dbSubmission = dbSubmission;
		}

		public ActionResult Index()
		{
			var emailTypes = _dbSubmission.EmailTypes
				.AsNoTracking()
				.OrderBy(x => x.Code)
				.ProjectTo<EmailTypesIndexViewModel>();

			return View(emailTypes);
		}

		public ActionResult Edit(int? id, Mode mode)
		{
			var vm = new EmailTypesEditViewModel();

			if (mode == Mode.Edit)
			{
				var emailType = _dbSubmission.EmailTypes.Find(id);
				vm = Mapper.Map<EmailTypesEditViewModel>(emailType);
				var externalEmailAddress = _dbSubmission.EmailSubscriptions
					.Where(x => x.EmailTypeId == id && x.ExternalEmailAddresses != null)
					.Distinct()
					.Select(x => x.ExternalEmailAddresses).ToList();
				vm.ExternalEmailAddress = string.Join(";", externalEmailAddress);

				var users = LoadUsers(emailType.Id);
				vm.UserIds = users.Select(x => x.Value).ToArray();
				vm.Users = users;

				var emailDistributions = LoadEmailDistributions(emailType.Id);
				vm.EmailDistributionIds = emailDistributions.Select(x => x.Value).ToArray();
				vm.EmailDistributions = emailDistributions;
			}

			vm.Mode = mode;

			return View(vm);
		}

		[HttpPost]
		public ActionResult Edit(int? id, EmailTypesEditViewModel vm)
		{
			var emailType = new EmailType();
			if (vm.Mode == Mode.Edit)
			{
				emailType = _dbSubmission.EmailTypes.Find(id);
			}

			emailType.Code = vm.Code;
			emailType.Description = vm.Description;

			if (vm.Mode == Mode.Add)
			{
				_dbSubmission.EmailTypes.Add(emailType);
			}

			_dbSubmission.SaveChanges();

			UpdateUsers(emailType.Id, vm.UserIds, vm.ExternalEmailAddress);
			UpdateEmailDistributions(emailType.Id, vm.EmailDistributionIds);

			CacheManager.Clear(LookupAjax.EmailTypes.ToString());

			return RedirectToAction("Index");
		}

		[HttpPost]
		public JsonResult CheckDelete(int id)
		{
			return Json(false);
		}

		[HttpPost]
		public ActionResult Delete(int id)
		{
			_dbSubmission.EmailSubscriptions.Where(x => x.EmailTypeId == id).Delete();

			var emailType = new EmailType { Id = id };
			_dbSubmission.Entry(emailType).State = EntityState.Deleted;
			_dbSubmission.SaveChanges();

			CacheManager.Clear(LookupAjax.EmailTypes.ToString());

			return RedirectToAction("Index");
		}

		[HttpPost]
		public ActionResult SetActive(int id, Mode mode)
		{
			throw new NotImplementedException();
		}

		public JsonResult ValidateCode(int? id, string code, Mode mode)
		{
			var validates = true;

			if (!string.IsNullOrEmpty(code) && _dbSubmission.EmailTypes.Any(x => x.Id != id && x.Code == code))
			{
				validates = false;
			}

			return Json(validates, JsonRequestBehavior.AllowGet);
		}

		#region Helper Methods
		private List<SelectListItem> LoadUsers(int emailTypeId)
		{
			return _dbSubmission.EmailSubscriptions.Include(x => x.User)
				.Where(x => x.EmailTypeId == emailTypeId && x.UserId != null)
				.Select(x => new SelectListItem { Value = x.User.Id.ToString(), Text = x.User.FName + " " + x.User.LName })
				.ToList();
		}

		private IList<SelectListItem> LoadEmailDistributions(int emailTypeId)
		{
			return _dbSubmission.EmailSubscriptions.Include(x => x.EmailDistribution)
				.Where(x => x.EmailTypeId == emailTypeId && x.EmailDistributionId != null)
				.Select(x => new SelectListItem { Value = x.EmailDistribution.Id.ToString(), Text = x.EmailDistribution.Code })
				.ToList();
		}

		private void UpdateUsers(int emailTypeId, string[] userIds, string externalAddresses)
		{
			_dbSubmission.EmailSubscriptions.Where(x => x.EmailTypeId == emailTypeId && x.UserId != null).Delete();

			if (userIds != null)
			{
				foreach (var userId in userIds)
				{
					var emailSubscriptionUser = new EmailSubscription { UserId = Convert.ToInt32(userId), EmailTypeId = emailTypeId };
					_dbSubmission.EmailSubscriptions.Add(emailSubscriptionUser);
				}

				_dbSubmission.SaveChanges();
			}

			_dbSubmission.EmailSubscriptions.Where(x => x.EmailTypeId == emailTypeId && x.ExternalEmailAddresses != null).Delete();
			if (externalAddresses != null)
			{
				var addressList = externalAddresses.Split(';');
				foreach (var address in addressList)
				{
					var emailExternalAddress = new EmailSubscription { EmailTypeId = emailTypeId, ExternalEmailAddresses = address };
					_dbSubmission.EmailSubscriptions.Add(emailExternalAddress);
				}
				_dbSubmission.SaveChanges();
			}
		}

		private void UpdateEmailDistributions(int emailTypeId, string[] emailDistributionIds)
		{
			_dbSubmission.EmailSubscriptions.Where(x => x.EmailTypeId == emailTypeId && x.EmailDistributionId != null).Delete();

			if (emailDistributionIds != null)
			{
				foreach (var emailDistributionId in emailDistributionIds)
				{
					var emailSubscriptionDistribution = new EmailSubscription { EmailDistributionId = Convert.ToInt32(emailDistributionId), EmailTypeId = emailTypeId };
					_dbSubmission.EmailSubscriptions.Add(emailSubscriptionDistribution);
				}

				_dbSubmission.SaveChanges();
			}
		}
		#endregion
	}
}