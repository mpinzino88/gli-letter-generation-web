﻿using System;
using System.IO;

namespace Submissions.Web.Areas.Help.Models.About
{
	public class AboutViewModel
	{
		public AboutViewModel()
		{
			ChangeLog = new ChangeLog("");
		}
		public AboutViewModel(string version, DateTime buildDate, string path)
		{
			ChangeLog = new ChangeLog(path);
			BuildVersion = version;
			BuildDate = buildDate;
		}
		public ChangeLog ChangeLog { get; set; }
		public string BuildVersion { get; }
		public DateTime? BuildDate { get; }
	}

	public class ChangeLog
	{
		public ChangeLog(string path)
		{
			Path = path;
			if (File.Exists(Path))
			{
				Raw = File.ReadAllText(Path);
				ParseChangeLog();
			}
		}

		public string Path { get; set; }
		public string Raw { get; set; }
		public string Full { get; internal set; }
		public string Latest { get; internal set; }

		private void ParseChangeLog()
		{
			var parts = Raw.Split(new string[] { @"%Latest%" }, 3, StringSplitOptions.None);

			Latest = parts[1];
			Full = parts.Length > 0 ? $"{ parts[0] + parts[1] + parts[2]}" : Raw;
		}
	}
}