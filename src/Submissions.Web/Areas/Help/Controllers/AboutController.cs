﻿using Submissions.Web.Areas.Help.Models.About;
using Submissions.Web.Helpers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Submissions.Web.Areas.Help.Controllers
{
	public class AboutController : Controller
	{
		public ActionResult Index() => View(new AboutViewModel(ApplicationInformation.ExecutingAssemblyVersion.ToString(), ApplicationInformation.CompileDate, Path.Combine(Server.MapPath("~"), @"CHANGELOG.md")));
	}
}