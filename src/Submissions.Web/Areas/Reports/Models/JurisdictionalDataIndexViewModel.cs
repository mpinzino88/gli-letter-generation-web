﻿using Submissions.Web.Models.Grids.Reports;
using Submissions.Web.Models.Query;
using System.Web;
using System.Web.Mvc;

namespace Submissions.Web.Areas.Reports.Models
{
	public class JurisdictionalDataIndexViewModel
	{
		public JurisdictionalDataIndexViewModel()
		{
			this.JurisdictionalDataSearchPartial = new JurisdictionalDataSearchPartialViewModel();
		}

		public JurisdictionalDataSearchPartialViewModel JurisdictionalDataSearchPartial { get; set; }
	}

	public class JurisdictionalDataDisplayViewModel
	{
		public JurisdictionalDataDisplayViewModel()
		{
			this.Title = "Jurisdictions Report";
			this.JurisdictionalDataGrid = new JurisdictionalDataReportGrid();
			this.Filters = new JurisdictionalDataSearch();
		}

		public string Title { get; set; }
		public int? WidgetId { get; set; }
		public string ExportUrl
		{
			get
			{
				var url = new UrlHelper(HttpContext.Current.Request.RequestContext);

				if (this.WidgetId == null)
				{
					return url.Action("Export", "JurisdictionalData");
				}
				else
				{
					return url.Action("Export", "Widget");
				}
			}
		}
		public JurisdictionalDataReportGrid JurisdictionalDataGrid { get; set; }
		public JurisdictionalDataSearch Filters { get; set; }
	}
}