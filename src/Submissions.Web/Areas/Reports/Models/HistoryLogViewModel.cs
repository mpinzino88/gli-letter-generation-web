﻿using Submissions.Web.Models.Grids.Reports;
using Submissions.Web.Models.Query;
using System.Collections.Generic;
using System.ComponentModel;
using System.Web.Mvc;

namespace Submissions.Web.Areas.Reports.Models
{
	public class HistoryLogIndexViewModel
	{
		public HistoryLogIndexViewModel()
		{
			this.Filters = new HistoryLogSearch();
			this.AvailableTableOptions = new List<SelectListItem>();
			this.SubmissionSubTitle = new SubmissionData();
		}
		public HistoryLogSearch Filters { get; set; }
		public List<SelectListItem> AvailableTableOptions { get; set; }
		public HistoryView? CurrentView { get; set; }
		public SubmissionData SubmissionSubTitle { get; set; }

	}

	public class HistoryLogDisplayViewModel
	{
		public HistoryLogDisplayViewModel()
		{
			this.HistoryLogGrid = new HistoryLogReportGrid();
			this.Filters = new HistoryLogSearch();
		}

		public HistoryLogReportGrid HistoryLogGrid { get; set; }
		public HistoryLogSearch Filters { get; set; }
	}

	public class OptionValue
	{
		public string Val { get; set; }
		public string Text { get; set; }
	}

	public class SubmissionData
	{
		public string FileNumber { get; set; }
		public string IdNumber { get; set; }
		public string GameName { get; set; }
		public string DateCode { get; set; }
		public string Function { get; set; }
		public string JurisdictionName { get; set; }
		public string JurisdictionId { get; set; }
	}
	#region Enums
	public enum HistoryView
	{
		BugBoxHistory,
		EmailDistributionUserHistory,
		GeneralReport,
		JurisdictionsHistory,
		ShippingHistory,
		SignaturesHistory,
		SourceCodeStorageHistory,
		SubmissionsHistory
	}
	public enum DatabaseOperations
	{
		[Description("Insert")]
		I,
		[Description("Update")]
		U,
		[Description("Delete")]
		D,
	}

	public enum tbl_lst_fileJuris_Columns
	{
		Active,
		AltName,
		AvailForElectReq,
		CountryId,
		email,
		//emailDaily,
		//emailPDF,
		Name,
		FormattedJurisdictionName,
		jurisdictionType,
		Nation,
		SubCode
	}

	public enum tbl_lst_fileManu_Columns
	{
		email,
		//emailDaily,
		//emailPDF,
		manufacturer
	}

	public enum BugBox_Columns
	{
		[Description("BugBox")]
		BugBox
	}
	public enum DraftLetters_Columns
	{
		[Description("Draft Letter - Document Path")]
		FilePath,
		[Description("Draft Letter - Request Timeline")]
		TimelineRequested,
		[Description("Draft Letter - RV/NU")]
		TimelineType,
		[Description("Draft Letter - Special Note")]
		SpecialNote,
		[Description("Draft Letter - Status")]
		Status,
	}

	public enum EmailDistributionUser_Columns
	{
		[Description("Email Distribution")]
		EmailDistributionId,
		[Description("External Email")]
		ExternalEmail,
		[Description("Rule Id")]
		RuleId,
		[Description("Login Id")]
		LoginId,
		[Description("Jurisdiction Id")]
		JurisdictionId,
		[Description("Manufacturer Code")]
		ManufacturerCode,
		[Description("Manufacturer Group")]
		ManufacturerGroupId
	}

	public enum JurisdictionalData_Columns
	{
		[Description("Active")]
		Active,
		[Description("Billing Party")]
		PurchaseOrder,
		[Description("Company")]
		Company,
		[Description("Conditional Revoke")]
		ConditionalRevoke,
		[Description("Contract Number")]
		ContractNumber,
		[Description("Contract Type")]
		ContractType,
		[Description("Currency")]
		Currency,
		[Description("Draft Letter Date")]
		DraftLetterDate,
		[Description("External Test Lab")]
		ExternalTestLab,
		[Description("Jurisdiction Close Date")]
		JurisdictionDate,
		[Description("Jurisdiction Letter Type")]
		JurisdictionType,
		[Description("Jurisdiction Name")]
		JurisdictionName,
		[Description("Jurisdiction PDF")]
		JurisdictionPDF,
		[Description("Jurisdiction GPS PDF")]
		JurisdictionGPSPDF,
		[Description("Jurisdiction Received Date")]
		JurisdictionReceivedDate,
		[Description("Jurisdiction Submitted Date")]
		JurisdictionRequestDate,
		[Description("Updated Date")]
		UpdatedDate,
		[Description("Letter Number")]
		Letter_Number,
		[Description("WON")]
		MasterBillingProject,
		[Description("Billing Reference/Clarity Number")]
		ClientNumber,
		[Description("NU Date")]
		ObsoletedDate,
		[Description("PDFSent")]
		PDFSent,
		[Description("Priority Draft")]
		PriorityDraft,
		[Description("Regulatory Status")]
		RegulatoryStatus,
		[Description("RJWD Reason")]
		RJWD_note,
		[Description("RV Date")]
		RevokedDate,
		[Description("TX Lab")]
		TXLab,
		[Description("Upgrade By")]
		UpgradeBy,
		[Description("Issue Lab")]
		CertLab
	}

	public enum Shipping_Columns
	{
		[Description("Courier")]
		Shipper,
		[Description("From/To")]
		From_To,
		[Description("Num Chips")]
		num_chips,
		[Description("Num Disks")]
		num_disks,
		[Description("Received Date")]
		date_processed,
		[Description("Shipping Memo")]
		Ship_memo,
		[Description("Tracking Number")]
		Shipper_id,
		[Description("Type")]
		Type
	}

	public enum Signtures_Columns
	{
		[Description("Signature")]
		Signature
	}
	public enum Submissions_Columns
	{
		[Description("Archive Location")]
		ArchiveLocation,
		[Description("Cert Lab")]
		Cert_lab,
		[Description("Chip Type")]
		ChipType,
		[Description("Company")]
		Company,
		[Description("Conditional Revoke")]
		ConditionalRevoke,
		[Description("Contract Type")]
		ContractType,
		[Description("Contract Value")]
		ContractValue,
		[Description("Currency")]
		Currency,
		[Description("Datecode")]
		Datecode,
		[Description("Function")]
		Function,
		[Description("Game Name")]
		GameName,
		[Description("Game Type")]
		GameType,
		[Description("Homologation ID")]
		HomologationID,
		[Description("Idnumber")]
		Idnumber,
		[Description("Letter Number")]
		Letter_Number,
		[Description("Manufacturer Build ID")]
		manufacturerBuildId,
		[Description("Master Filenumber")]
		MasterFileNumberStr,
		[Description("Maximum")]
		Maxpercent,
		[Description("Minimum")]
		Minimum,
		[Description("Billing Reference")]
		Network,
		[Description("Operator")]
		Operator,
		[Description("OrgIGTMaterial")]
		OrgIGTMaterial,
		[Description("Position")]
		Position,
		[Description("Project/Contract Number")]
		ContractNumber,
		[Description("Received Date")]
		recDate,
		[Description("Regression Testing")]
		IsRegressionTesting,
		[Description("Replace With")]
		SubmissionReplaceWith,
		[Description("Status")]
		Status,
		[Description("Submission Date")]
		subDate,
		[Description("Submissions PDF")]
		SubmissionPDF,
		[Description("Submissions GPS PDF")]
		SubmissionGPSPDF,
		[Description("System")]
		SystemProvider,
		[Description("Test Lab")]
		test_lab,
		[Description("Testing Performed")]
		TestingPerformed,
		[Description("Testing Type")]
		TestingType,
		[Description("Version")]
		Version
	}

	public enum SourceCodeStorage_Columns
	{
		[Description("CodeStorage")]
		CodeStorage
	}
	public enum SubmissionLetters_Columns
	{
		[Description("Supplemental FilePath")]
		FilePath,
		[Description("Supplemental GPS FilePath")]
		GPSFilePath,
		[Description("Supplemental LetterDate")]
		LetterDate,
		[Description("Supplemental Langugae")]
		Language
	}
	public enum QuoteAuth_Columns
	{
		[Description("Date of Authorization")]
		authDate,
		[Description("Quote Date")]
		quoteDate
	}

	#endregion
}