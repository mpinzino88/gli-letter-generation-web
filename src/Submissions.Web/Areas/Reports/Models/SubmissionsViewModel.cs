﻿using Submissions.Web.Models.Grids.Reports;
using Submissions.Web.Models.Query;
using System.Web;
using System.Web.Mvc;

namespace Submissions.Web.Areas.Reports.Models
{
	public class SubmissionsIndexViewModel
	{
		public SubmissionsIndexViewModel()
		{
			this.SubmissionSearchPartial = new SubmissionSearchPartialViewModel();
		}

		public SubmissionSearchPartialViewModel SubmissionSearchPartial { get; set; }
	}

	public class SubmissionsDisplayViewModel
	{
		public SubmissionsDisplayViewModel()
		{
			this.Title = "Submissions Report";
			this.SubmissionGrid = new SubmissionsReportGrid();
			this.Filters = new SubmissionSearch();
		}

		public string Title { get; set; }
		public int? WidgetId { get; set; }
		public string ExportUrl
		{
			get
			{
				var url = new UrlHelper(HttpContext.Current.Request.RequestContext);

				if (this.WidgetId == null)
				{
					return url.Action("Export", "Submissions");
				}
				else
				{
					return url.Action("Export", "Widget");
				}
			}
		}
		public SubmissionsReportGrid SubmissionGrid { get; set; }
		public SubmissionSearch Filters { get; set; }
	}
}