﻿using Submissions.Web.Models.Grids.Reports;
using Submissions.Web.Models.Query;
using System.Web;
using System.Web.Mvc;

namespace Submissions.Web.Areas.Reports.Models
{
	public class ProjectsIndexViewModel
	{
		public ProjectsIndexViewModel()
		{
			this.ProjectSearchPartial = new ProjectSearchPartialViewModel();
		}

		public ProjectSearchPartialViewModel ProjectSearchPartial { get; set; }
	}

	public class ProjectsDisplayViewModel
	{
		public ProjectsDisplayViewModel()
		{
			this.Title = "Projects Report";
			this.ProjectGrid = new ProjectsReportGrid();
			this.Filters = new ProjectSearch();
		}

		public string Title { get; set; }
		public int? WidgetId { get; set; }
		public string ExportUrl
		{
			get
			{
				var url = new UrlHelper(HttpContext.Current.Request.RequestContext);

				if (this.WidgetId == null)
				{
					return url.Action("Export", "Projects");
				}
				else
				{
					return url.Action("Export", "Widget");
				}
			}
		}
		public ProjectsReportGrid ProjectGrid { get; set; }
		public ProjectSearch Filters { get; set; }
	}
}