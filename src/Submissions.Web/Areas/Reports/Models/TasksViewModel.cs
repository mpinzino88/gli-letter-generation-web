﻿using Submissions.Web.Models.Grids.Reports;
using Submissions.Web.Models.Query;
using System.Web;
using System.Web.Mvc;

namespace Submissions.Web.Areas.Reports.Models
{
	public class TasksIndexViewModel
	{
		public TasksIndexViewModel()
		{
			this.TaskSearchPartial = new TaskSearchPartialViewModel();
		}

		public TaskSearchPartialViewModel TaskSearchPartial { get; set; }
	}

	public class TasksDisplayViewModel
	{
		public TasksDisplayViewModel()
		{
			this.Title = "Tasks Report";
			this.TasksGrid = new TasksReportGrid();
			this.Filters = new TaskSearch();
		}

		public string Title { get; set; }
		public int? WidgetId { get; set; }
		public string ExportUrl
		{
			get
			{
				var url = new UrlHelper(HttpContext.Current.Request.RequestContext);

				if (this.WidgetId == null)
				{
					return url.Action("Export", "Tasks");
				}
				else
				{
					return url.Action("Export", "Widget");
				}
			}
		}
		public TasksReportGrid TasksGrid { get; set; }
		public TaskSearch Filters { get; set; }
	}
}