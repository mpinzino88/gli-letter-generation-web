﻿using Submissions.Web.Models.Query.Reports;
using Submissions.Web.Models.Grids.Reports;
using System.ComponentModel;

namespace Submissions.Web.Areas.Reports.Models
{
	public class QAAdvancedSearchViewModel
	{
		public QAAdvancedSearchViewModel()
		{
			this.filter = new QAAdvancedSearch();
		}
		public QAAdvancedSearch filter { get; set; }
	}

	public class QAAdvancedSearchDisplayViewModel
	{
		public QAAdvancedSearchDisplayViewModel()
		{
			this.filter = new QAAdvancedSearch();
			this.AdvancedSearchGrid = new QAAdvancedSearchGrid();
		}
		public QAAdvancedSearch filter { get; set; }
		public QAAdvancedSearchGrid AdvancedSearchGrid { get; set; }
		public string DetailReportUrl { get; set; }
	}
		
}