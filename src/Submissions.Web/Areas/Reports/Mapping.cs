﻿using AutoMapper;
using EF.Submission;
using Submissions.Web.Models.Grids;
using Submissions.Web.Models.Grids.Reports;

namespace Submissions.Web.Areas.Reports
{
	public class ReportsProfile : Profile
	{
		public ReportsProfile()
		{
			//CreateMap<HistoryLog, HistoryLogReportGridRecord>()
			//	.ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.User.FName + " " + src.User.LName));
			CreateMap<trTask, TasksGridRecord>()
				.ForMember(dest => dest.ProjectName, opt => opt.MapFrom(src => src.Project.FileNumber))
				.ForMember(dest => dest.QADeliveryDate, opt => opt.MapFrom(src => src.Project.QADeliveryDate))
				.ForMember(dest => dest.FName, opt => opt.MapFrom(src => src.User.FName))
				.ForMember(dest => dest.LName, opt => opt.MapFrom(src => src.User.LName))
				.ForMember(dest => dest.User, opt => opt.MapFrom(src => src.User.FName + " " + src.User.LName))
				.ForMember(dest => dest.Department, opt => opt.MapFrom(src => src.Department.Code))
				.ForMember(dest => dest.ActualHours, opt => opt.Ignore())
				.ForMember(dest => dest.DynamicsID, opt => opt.Ignore())
				.ForMember(dest => dest.IsRush, opt => opt.Ignore())
				.ForMember(dest => dest.EstMathCompletionDate, opt => opt.Ignore())
				.ForMember(dest => dest.EstCompleteDate, opt => opt.Ignore())
				.ForMember(dest => dest.ProjectStatus, opt => opt.Ignore())
				.ForMember(dest => dest.GameName, opt => opt.Ignore())
				.ForMember(dest => dest.IdNumber, opt => opt.Ignore())
				.ForMember(dest => dest.NoteDate, opt => opt.Ignore())
				.ForMember(dest => dest.Manager, opt => opt.Ignore())
				.ForMember(dest => dest.QAReviewer, opt => opt.Ignore())
				.ForMember(dest => dest.MathSupervisor, opt => opt.Ignore())
				.ForMember(dest => dest.Status, opt => opt.Ignore())
				.ForMember(dest => dest.BundleStatus, opt => opt.Ignore())
				.ForMember(dest => dest.ProjectIndicator, opt => opt.Ignore())
				.ForMember(dest => dest.ProjectTargetDiff, opt => opt.Ignore())
				.ForMember(dest => dest.IsFinalizationTask, opt => opt.Ignore())
				.ForMember(dest => dest.BundleType, opt => opt.Ignore())
				.ForMember(dest => dest.QATaskStatus, opt => opt.Ignore())
				.ForMember(dest => dest.EngTaskOrder, opt => opt.Ignore())
				.ForMember(dest => dest.QATaskOrder, opt => opt.Ignore())
				.ForMember(dest => dest.IDVersion, opt => opt.Ignore())
				.ForMember(dest => dest.AssociatedJur, opt => opt.Ignore())
				.ForMember(dest => dest.AssociatedComponent, opt => opt.Ignore())
				.ForMember(dest => dest.OtherEngTasks, opt => opt.Ignore())
				.ForMember(dest => dest.IsAssigned, opt => opt.Ignore())
				.ForMember(dest => dest.IsDeleted, opt => opt.Ignore())
				.ForMember(dest => dest.OrigEstHours, opt => opt.Ignore())
				.ForMember(dest => dest.BillType, opt => opt.Ignore())
				.ForMember(dest => dest.BundleId, opt => opt.Ignore())
				.ForMember(dest => dest.QAAccepted, opt => opt.Ignore())
				.ForMember(dest => dest.AssignedBy, opt => opt.Ignore())
				.ForMember(dest => dest.ProjectJurCount, opt => opt.Ignore())
				.ForMember(dest => dest.JiraOpen, opt => opt.Ignore())
				.ForMember(dest => dest.JurisdictionId, opt => opt.Ignore())
				.ForMember(dest => dest.JurisdictionName, opt => opt.Ignore())
				.ForMember(dest => dest.IsBundleResubmission, opt => opt.Ignore())
				.ForMember(dest => dest.ENInvolvementWhileFL, opt => opt.Ignore());
		}
	}
}