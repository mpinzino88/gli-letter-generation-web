using EF.Submission;
using Newtonsoft.Json;
using Submissions.Common;
using Submissions.Common.JQGrid;
using Submissions.Web.Areas.Reports.Models;
using Submissions.Web.Models.Grids.Reports;
using Submissions.Web.Models.Query.Reports;
using System.Linq;
using System.Web.Mvc;

namespace Submissions.Web.Areas.Reports.Controllers
{
	public class QAAdvancedSearchController : Controller
	{
		private readonly ISubmissionContext _dbSubmission;

		public QAAdvancedSearchController(ISubmissionContext dbSubmission)
		{
			_dbSubmission = dbSubmission;
		}
		//
		// GET: /Reports/QAAdvancedSearch/
		public ActionResult Index()
		{
			var vm = new QAAdvancedSearchViewModel();
			vm.filter.IsTransfer = ProjectType.All;
			vm.filter.Expedite = false;
			return View("Index", vm);
		}

		public ActionResult Display(QAAdvancedSearchViewModel model)
		{
			var vm = new QAAdvancedSearchDisplayViewModel();
			vm.filter = model.filter;
			vm.DetailReportUrl = GetDetailedReportUrl(model.filter);
			return View(vm);
		}

		private string GetDetailedReportUrl(QAAdvancedSearch filters)
		{
			var url = "";

			var sStatus = ""; // "0|";
			if (filters.CustomSearchStatus != null)
			{
				if (filters.CustomSearchStatus.Count > 0)
				{
					for (int i = 0; i < filters.CustomSearchStatus.Count; i++)
					{
						if (filters.CustomSearchStatus[i] == "EN")
						{
							filters.CustomSearchStatus[i] = "";
						}
					}
					sStatus = string.Join("|", filters.CustomSearchStatus);
				}
				sStatus = sStatus + "|";
			}

			var isExpedite = (filters.Expedite) ? 1 : 0;
			var qaAcceptFrom = filters.QAAcceptedFrom;
			var qaAcceptTo = filters.QAAcceptedTo;
			var rcvdFrom = filters.RecievedFrom;
			var rcvdTo = filters.RecievedTo;
			var reportType = 0;
			if (filters.IsTransfer == ProjectType.Projects)
			{
				reportType = 1;
			}
			else if (filters.IsTransfer == ProjectType.Transfers)
			{
				reportType = 2;
			}

			var qaRcvdFrom = filters.QAReceivedFrom;
			var qaRcvdTo = filters.QAReceivedTo;
			var engCompletedFrom = filters.ENCompletedFrom;
			var engCompletedTo = filters.ENCompletedTo;
			var qaCompleteFrom = filters.QACompletedFrom;
			var qaCompleteTo = filters.QACompletedTo;
			var manufacturer = (filters.ManufacturerShort == null) ? "-1" : filters.ManufacturerShort;
			var jurisdiction = (filters.JurisdictionId == null) ? -1 : filters.JurisdictionId;
			var bundleTypeId = (filters.CustomSearchBundleType != null) ? (int)(filters.CustomSearchBundleType) : 0;
			var locations = "";
			var workedByLocations = "";
			var acceptedByLocations = "";
			if (filters.LaboratoryIds != null)
			{
				if (filters.LaboratoryIds.Count > 0)
				{
					var locationsCode = _dbSubmission.trLaboratories.Where(x => filters.LaboratoryIds.Contains(x.Id)).Select(x => x.Location).ToList();
					locations = string.Join("|", locationsCode);
				}
			}
			if (filters.WorkedByLocationIds != null)
			{
				if (filters.WorkedByLocationIds.Count > 0)
				{
					var locationsCode = _dbSubmission.trLaboratories.Where(x => filters.WorkedByLocationIds.Contains(x.Id)).Select(x => x.Location).ToList();
					workedByLocations = string.Join("|", locationsCode);
				}
			}
			if (filters.AcceptedByLocationIds != null)
			{
				if (filters.AcceptedByLocationIds.Count > 0)
				{
					var locationsCode = _dbSubmission.trLaboratories.Where(x => filters.AcceptedByLocationIds.Contains(x.Id)).Select(x => x.Location).ToList();
					acceptedByLocations = string.Join("|", locationsCode);
				}
			}

			var taskDesc = filters.TaskDefs;
			var taskTarget = filters.TaskTarget;
			var qFINeeded = (filters.QFINeeded == true) ? 1 : 0;
			var neededADMWork = (filters.ADMWorkNeeded == true) ? 1 : 0;
			var noCertNeeded = (filters.NoCertNeeded) ? 1 : 0;
			var enInvolvementNeeded = (filters.ENInvolvementWhileFL == true) ? 1 : 0;
			var qaLocation = filters.QALocationId;
			url = "https://reports.gaminglabs.net/Reports/report/Protrack/QAprotrack?sStatus=" + sStatus + "&IsRush=" + isExpedite + "&AcceptFrom=" + qaAcceptFrom + "&AcceptTo=" + qaAcceptTo + "&RcvdFrom=" + rcvdFrom + "&RcvdTo=" + rcvdTo + "&QARcvdFrom=" + qaRcvdFrom + "&QARcvdTo=" + qaRcvdTo + "&EngCompleteFrom=" + engCompletedFrom + "&EngCompleteTo=" + engCompletedTo + "&QACompleteFrom=" + qaCompleteFrom + "&QACompleteTo=" + qaCompleteTo + "&OnlyBundles=0&iReportType=" + reportType + "&sManuf=" + manufacturer + "&sJuris=" + jurisdiction + "&BundleType=" + bundleTypeId + "&IssueExists=0&sLoc=" + locations + "&taskDescription=" + taskDesc + "&taskTargetFilter=" + taskTarget + "&NCRReq=" + qFINeeded + "&ADMWorkNeeded=" + neededADMWork + "&ENInvolvementWhileFL=" + enInvolvementNeeded + "&NoCertNeeded=" + noCertNeeded + "&WorkedBy=" + workedByLocations + "&AcceptedBy=" + acceptedByLocations + "&QALocation=" + qaLocation;


			return url;
		}

		[HttpPost]
		public void Export(ExportType exportType, string filter, string filtersToolbar, string title, string subTitle)
		{
			var filters = JsonConvert.DeserializeObject<QAAdvancedSearch>(filter);

			var sStatus = ""; // "0|";
			if (filters.CustomSearchStatus != null)
			{
				if (filters.CustomSearchStatus.Count > 0)
				{
					for (int i = 0; i < filters.CustomSearchStatus.Count; i++)
					{
						if (filters.CustomSearchStatus[i] == "EN")
						{
							filters.CustomSearchStatus[i] = "";
						}
					}
					sStatus = string.Join("|", filters.CustomSearchStatus);
				}
				sStatus = sStatus + "|";
			}

			var isExpedite = (filters.Expedite) ? 1 : 0;
			var qaAcceptFrom = filters.QAAcceptedFrom;
			var qaAcceptTo = filters.QAAcceptedTo;
			var rcvdFrom = filters.RecievedFrom;
			var rcvdTo = filters.RecievedTo;
			var reportType = 0;
			if (filters.IsTransfer == ProjectType.Projects)
			{
				reportType = 1;
			}
			else if (filters.IsTransfer == ProjectType.Transfers)
			{
				reportType = 2;
			}
			var SummaryReport = 1;
			var qaRcvdFrom = filters.QAReceivedFrom;
			var qaRcvdTo = filters.QAReceivedTo;
			var engCompletedFrom = filters.ENCompletedFrom;
			var engCompletedTo = filters.ENCompletedTo;
			var qaCompleteFrom = filters.QACompletedFrom;
			var qaCompleteTo = filters.QACompletedTo;
			var manufacturer = (filters.ManufacturerShort == null) ? "-1" : filters.ManufacturerShort;
			var jurisdiction = (filters.JurisdictionId == null) ? -1 : filters.JurisdictionId;
			var bundleTypeId = (filters.CustomSearchBundleType != null) ? (int)(filters.CustomSearchBundleType) : 0;
			var locations = "";
			var workedByLocations = "";
			var acceptedByLocations = "";
			if (filters.LaboratoryIds != null)
			{
				if (filters.LaboratoryIds.Count > 0)
				{
					var locationsCode = _dbSubmission.trLaboratories.Where(x => filters.LaboratoryIds.Contains(x.Id)).Select(x => x.Location).ToList();
					locations = string.Join("|", locationsCode);
				}
			}
			if (filters.WorkedByLocationIds != null)
			{
				if (filters.WorkedByLocationIds.Count > 0)
				{
					var locationsCode = _dbSubmission.trLaboratories.Where(x => filters.WorkedByLocationIds.Contains(x.Id)).Select(x => x.Location).ToList();
					workedByLocations = string.Join("|", locationsCode);
				}
			}

			if (filters.AcceptedByLocationIds != null)
			{
				if (filters.AcceptedByLocationIds.Count > 0)
				{
					var locationsCode = _dbSubmission.trLaboratories.Where(x => filters.AcceptedByLocationIds.Contains(x.Id)).Select(x => x.Location).ToList();
					acceptedByLocations = string.Join("|", locationsCode);
				}
			}

			var taskDesc = (filters.TaskDefIds != null ? string.Join(",", filters.TaskDefIds) : null);
			var taskTarget = filters.TaskTarget.ToString("d");
			var qFINeeded = (filters.QFINeeded == true) ? 1 : 0;
			var enInvolvementNeeded = (filters.ENInvolvementWhileFL) ? 1 : 0;
			var noCertNeeded = (filters.NoCertNeeded) ? 1 : 0;
			var admWorkNeeded = (filters.ADMWorkNeeded) ? 1 : 0;
			var qaLocation = filters.QALocationId != null ? filters.QALocationId.ToString() : "null";
			var complianceRequired = (filters.ComplianceRequired == true) ? 1 : 0;
			var sqlQuery = "exec [dbo].[GetQAReportData] '" + sStatus + "','" + isExpedite + "','" + qaAcceptFrom + "','" + qaAcceptTo + "','" + rcvdFrom + "','" + rcvdTo + "','" + reportType + "'," + SummaryReport + ",'" + qaRcvdFrom + "','" + qaRcvdTo + "','" + engCompletedFrom + "','" + engCompletedTo + "','" + qaCompleteFrom + "','" + qaCompleteTo + "','" + manufacturer + "','" + jurisdiction + "','" + bundleTypeId + "',0,'0',0,'" + locations + "', '" + taskDesc + "', '" + taskTarget + "'," + admWorkNeeded + "," + enInvolvementNeeded + "," + noCertNeeded + ",'" + workedByLocations + "','" + acceptedByLocations + "'," + qaLocation + "," + complianceRequired;
			var query = _dbSubmission.Database.SqlQuery<QAAdvancedSearchGridRecord>(sqlQuery).AsQueryable();

			var grid = new QAAdvancedSearchGrid();
			var export = new Export
			{
				ExportType = exportType,
				FiltersToolbar = filtersToolbar,
				Title = (title + " " + subTitle).TrimEnd()
			};
			grid.Export(export, query);
		}

	}
}