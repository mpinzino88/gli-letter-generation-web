﻿using Newtonsoft.Json;
using Submissions.Common;
using Submissions.Common.JQGrid;
using Submissions.Web.Areas.Reports.Models;
using Submissions.Web.Models;
using Submissions.Web.Models.Grids.Reports;
using Submissions.Web.Models.Query;
using System.Web.Mvc;

namespace Submissions.Web.Areas.Reports.Controllers
{
	public class TasksController : Controller
	{
		private readonly IQueryService _queryService;
		private readonly IColumnChooserService _columnChooserService;

		public TasksController(IQueryService queryService, IColumnChooserService columnChooserService)
		{
			_queryService = queryService;
			_columnChooserService = columnChooserService;
		}

		public ActionResult Index()
		{
			var vm = new TasksIndexViewModel();
			vm.TaskSearchPartial.Filters.Sort1 = "ProjectName";

			return View(vm);
		}

		public ActionResult ColumnChooser()
		{
			var savedColumns = _columnChooserService.LoadColumns(ColumnChooserGrid.TasksReport);

			var grid = new TasksReportGrid();
			grid.InitializeDynamicGrid(savedColumns);

			var vm = new ColumnChooserViewModel();
			vm.SubTitle = "Tasks Report";
			vm.AvailableColumns = grid.ColumnChooserAvailableColumns;
			vm.SelectedColumns = grid.ColumnChooserSelectedColumns;

			return PartialView("_ColumnChooser", vm);
		}

		[HttpPost]
		public ActionResult ColumnChooser(string savedColumns, string filters)
		{
			_columnChooserService.SaveColumns(ColumnChooserGrid.TasksReport, savedColumns);

			TempData["TaskSearchFilters"] = JsonConvert.DeserializeObject<TaskSearch>(filters);
			return RedirectToAction("Display");
		}

		public ActionResult Display(TasksIndexViewModel model)
		{
			var filters = (TaskSearch)(TempData["TaskSearchFilters"] ?? model.TaskSearchPartial.Filters);
			var savedColumns = _columnChooserService.LoadColumns(ColumnChooserGrid.TasksReport);

			var vm = new TasksDisplayViewModel();
			vm.TasksGrid.InitializeDynamicGrid(savedColumns);
			vm.Filters = filters;

			return View(vm);
		}

		[HttpPost]
		public void Export(ExportType exportType, string filters, string filtersToolbar, string title, string subTitle)
		{
			var searchFilters = JsonConvert.DeserializeObject<TaskSearch>(filters);
			var savedColumns = _columnChooserService.LoadColumns(ColumnChooserGrid.TasksReport);
			var query = _queryService.TaskSearch(searchFilters).GenericSort(searchFilters);

			var grid = new TasksReportGrid();
			var export = new Export
			{
				ExportType = exportType,
				FiltersToolbar = filtersToolbar,
				SavedColumns = savedColumns,
				Title = (title + " " + subTitle).TrimEnd()
			};
			grid.Export(export, query);
		}
	}
}