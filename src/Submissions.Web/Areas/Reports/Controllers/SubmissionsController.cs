﻿using GLI.EFCore.Submission;
using Newtonsoft.Json;
using Submissions.Common;
using Submissions.Common.JQGrid;
using Submissions.Web.Areas.Reports.Models;
using Submissions.Web.Models;
using Submissions.Web.Models.Grids.Reports;
using Submissions.Web.Models.Query;
using System;
using System.Linq;
using System.Web.Mvc;

namespace Submissions.Web.Areas.Reports.Controllers
{
	public class SubmissionsController : Controller
	{
		private readonly SubmissionContext _dbSubmission;
		private readonly IQueryService _queryService;
		private readonly IColumnChooserService _columnChooserService;

		public SubmissionsController(SubmissionContext dbSubmission, IQueryService queryService, IColumnChooserService columnChooserService)
		{
			_dbSubmission = dbSubmission;
			_queryService = queryService;
			_columnChooserService = columnChooserService;
		}

		public ActionResult Index()
		{
			var vm = new SubmissionsIndexViewModel();
			vm.SubmissionSearchPartial.Filters.Sort1 = "ReceiveDate";
			vm.SubmissionSearchPartial.Filters.Sort1Order = SortOrder.Desc;

			return View(vm);
		}

		public ActionResult ColumnChooser()
		{
			var savedColumns = _columnChooserService.LoadColumns(ColumnChooserGrid.SubmissionsReport);

			var grid = new SubmissionsReportGrid();
			grid.InitializeDynamicGrid(savedColumns);

			var vm = new ColumnChooserViewModel();
			vm.SubTitle = "Submissions Report";
			vm.AvailableColumns = grid.ColumnChooserAvailableColumns;
			vm.SelectedColumns = grid.ColumnChooserSelectedColumns;

			return PartialView("_ColumnChooser", vm);
		}

		[HttpPost]
		public ActionResult ColumnChooser(string savedColumns, string filters)
		{
			_columnChooserService.SaveColumns(ColumnChooserGrid.SubmissionsReport, savedColumns);

			TempData["SubmissionSearchFilters"] = JsonConvert.DeserializeObject<SubmissionSearch>(filters);
			return RedirectToAction("Display");
		}

		public ActionResult Display(SubmissionsIndexViewModel model)
		{
			var filters = (SubmissionSearch)(TempData["SubmissionSearchFilters"] ?? model.SubmissionSearchPartial.Filters);
			var savedColumns = _columnChooserService.LoadColumns(ColumnChooserGrid.SubmissionsReport);

			var vm = new SubmissionsDisplayViewModel();
			vm.SubmissionGrid.InitializeDynamicGrid(savedColumns);
			vm.Filters = filters;

			return View(vm);
		}

		[HttpPost]
		public void Export(ExportType exportType, string filters, string filtersToolbar, string title, string subTitle)
		{
			var searchFilters = JsonConvert.DeserializeObject<SubmissionSearch>(filters);
			var savedColumns = _columnChooserService.LoadColumns(ColumnChooserGrid.SubmissionsReport);
			var query = _queryService.SubmissionSearch(searchFilters).GenericSort(searchFilters);

			var grid = new SubmissionsReportGrid();
			var export = new Export
			{
				ExportType = exportType,
				FiltersToolbar = filtersToolbar,
				SavedColumns = savedColumns,
				Title = (title + " " + subTitle).TrimEnd()
			};
			grid.Export(export, query);
		}

		public ActionResult SingleInputSearch(string singleInputSearch)
		{
			singleInputSearch = singleInputSearch.Trim();

			try
			{
				if (singleInputSearch.Count(x => x == '-') < 4)
				{
					throw new Exception("Not a valid File Number so we can't search with Manufacturer Code");
				}

				var validFileNumber = new FileNumber(singleInputSearch);
				var fileNumber = validFileNumber.ToString();
				var manufacturerCode = validFileNumber.ManufacturerCode;

				var submissionId = _dbSubmission.Submissions
					.Where(x => x.ManufacturerCode == manufacturerCode && x.FileNumber == fileNumber)
					.Select(x => x.Id)
					.FirstOrDefault();

				if (submissionId != 0)
				{
					return RedirectToAction("detail", "submission", new { area = "submission", id = submissionId });
				}
			}
			catch { }

			TempData["SubmissionSearchFilters"] = new SubmissionSearch { SingleInputSearch = singleInputSearch };

			return RedirectToAction("Display");
		}
	}
}