﻿using Newtonsoft.Json;
using Submissions.Common;
using Submissions.Common.JQGrid;
using Submissions.Web.Areas.Reports.Models;
using Submissions.Web.Models;
using Submissions.Web.Models.Grids.Reports;
using Submissions.Web.Models.Query;
using System.Web.Mvc;

namespace Submissions.Web.Areas.Reports.Controllers
{
	public class JurisdictionalDataController : Controller
	{
		private readonly IQueryService _queryService;
		private readonly IColumnChooserService _columnChooserService;

		public JurisdictionalDataController(IQueryService queryService, IColumnChooserService columnChooserService)
		{
			_queryService = queryService;
			_columnChooserService = columnChooserService;
		}

		public ActionResult Index()
		{
			var vm = new JurisdictionalDataIndexViewModel();
			return View(vm);
		}

		public ActionResult ColumnChooser()
		{
			var savedColumns = _columnChooserService.LoadColumns(ColumnChooserGrid.JurisdictionalDataReport);

			var grid = new JurisdictionalDataReportGrid();
			grid.InitializeDynamicGrid(savedColumns);

			var vm = new ColumnChooserViewModel();
			vm.SubTitle = "Jurisdictions Report";
			vm.AvailableColumns = grid.ColumnChooserAvailableColumns;
			vm.SelectedColumns = grid.ColumnChooserSelectedColumns;

			return PartialView("_ColumnChooser", vm);
		}

		[HttpPost]
		public ActionResult ColumnChooser(string savedColumns, string filters)
		{
			_columnChooserService.SaveColumns(ColumnChooserGrid.JurisdictionalDataReport, savedColumns);

			TempData["JurisdictionalDataSearchFilters"] = JsonConvert.DeserializeObject<JurisdictionalDataSearch>(filters);
			return RedirectToAction("Display");
		}

		public ActionResult Display(JurisdictionalDataIndexViewModel model)
		{
			var filters = (JurisdictionalDataSearch)(TempData["JurisdictionalDataSearchFilters"] ?? model.JurisdictionalDataSearchPartial.Filters);
			var savedColumns = _columnChooserService.LoadColumns(ColumnChooserGrid.JurisdictionalDataReport);

			var vm = new JurisdictionalDataDisplayViewModel();
			vm.JurisdictionalDataGrid.InitializeDynamicGrid(savedColumns);
			vm.Filters = filters;

			return View(vm);
		}

		[HttpPost]
		public void Export(ExportType exportType, string filters, string filtersToolbar, string title, string subTitle)
		{
			var searchFilters = JsonConvert.DeserializeObject<JurisdictionalDataSearch>(filters);
			var savedColumns = _columnChooserService.LoadColumns(ColumnChooserGrid.JurisdictionalDataReport);
			var query = _queryService.JurisdictionalDataSearch(searchFilters).GenericSort(searchFilters);

			var grid = new JurisdictionalDataReportGrid();
			var export = new Export
			{
				ExportType = exportType,
				FiltersToolbar = filtersToolbar,
				SavedColumns = savedColumns,
				Title = (title + " " + subTitle).TrimEnd()
			};
			grid.Export(export, query);
		}
	}
}