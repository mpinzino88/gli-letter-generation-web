﻿using GLI.EFCore.Submission;
using Newtonsoft.Json;
using Submissions.Common;
using Submissions.Web.Areas.Reports.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace Submissions.Web.Areas.Reports.Controllers
{
	public class HistoryLogController : Controller
	{
		private readonly SubmissionContext _dbSubmission;

		public HistoryLogController(SubmissionContext dbSubmission)
		{
			_dbSubmission = dbSubmission;
		}
		public ActionResult Index()
		{
			var vm = new HistoryLogIndexViewModel();
			vm.Filters.StartDate = DateTime.Today.AddDays(-14).Date;
			vm.CurrentView = HistoryView.GeneralReport;

			//This is configurable based on what history is viewed for so pass from here
			vm.AvailableTableOptions.Add(new SelectListItem() { Text = "", Value = "" });
			vm.AvailableTableOptions.Add(new SelectListItem() { Text = HistoryLogTableName.Submissions.GetEnumDescription() + " (" + HistoryLogTableName.Submissions.ToString() + ")", Value = HistoryLogTableName.Submissions.ToString(), Selected = true });
			vm.AvailableTableOptions.Add(new SelectListItem() { Text = HistoryLogTableName.JurisdictionalData.GetEnumDescription() + " (" + HistoryLogTableName.JurisdictionalData.ToString() + ")", Value = HistoryLogTableName.JurisdictionalData.ToString() });
			vm.AvailableTableOptions.Add(new SelectListItem() { Text = HistoryLogTableName.tbl_lst_fileManu.GetEnumDescription() + " (" + HistoryLogTableName.tbl_lst_fileManu.ToString() + ")", Value = HistoryLogTableName.tbl_lst_fileManu.ToString() });
			vm.AvailableTableOptions.Add(new SelectListItem() { Text = HistoryLogTableName.tbl_lst_fileJuris.GetEnumDescription() + " (" + HistoryLogTableName.tbl_lst_fileJuris.ToString() + ")", Value = HistoryLogTableName.tbl_lst_fileJuris.ToString() });
			vm.AvailableTableOptions.Add(new SelectListItem() { Text = HistoryLogTableName.EmailDistributionUsers.GetEnumDescription() + " (" + HistoryLogTableName.EmailDistributionUsers.ToString() + ")", Value = HistoryLogTableName.EmailDistributionUsers.ToString() });

			return View(vm);
		}

		public ActionResult Display(HistoryLogIndexViewModel model)
		{
			var vm = new HistoryLogDisplayViewModel();

			vm.Filters = model.Filters;

			return View(vm);
		}

		public ActionResult ViewSubmissionHistory(int submissionId)
		{
			var vm = new HistoryLogIndexViewModel();
			vm.CurrentView = HistoryView.SubmissionsHistory;
			vm.Filters.SubmissionId = submissionId;
			vm.SubmissionSubTitle = GetSubmissionInfo(submissionId, null);
			//This is configurable based on what history is viewed for so pass from here
			vm.AvailableTableOptions.Add(new SelectListItem() { Text = "", Value = "" });
			vm.AvailableTableOptions.Add(new SelectListItem() { Text = HistoryLogTableName.Submissions.GetEnumDescription() + " (" + HistoryLogTableName.Submissions.ToString() + ")", Value = HistoryLogTableName.Submissions.ToString(), Selected = true });

			return View("Index", vm);
		}

		public ActionResult ViewJurisdictionDataHistory(int jurisdictionalDataId)
		{
			var vm = new HistoryLogIndexViewModel();
			vm.CurrentView = HistoryView.JurisdictionsHistory;
			vm.Filters.JurisdictionalDataId = jurisdictionalDataId;
			vm.SubmissionSubTitle = GetSubmissionInfo(null, jurisdictionalDataId);
			//This is configurable based on what history is viewed for so pass from here
			vm.AvailableTableOptions.Add(new SelectListItem() { Text = "", Value = "" });
			vm.AvailableTableOptions.Add(new SelectListItem() { Text = HistoryLogTableName.JurisdictionalData.GetEnumDescription() + " (" + HistoryLogTableName.JurisdictionalData.ToString() + ")", Value = HistoryLogTableName.JurisdictionalData.ToString(), Selected = true });
			vm.AvailableTableOptions.Add(new SelectListItem() { Text = HistoryLogTableName.Letters.GetEnumDescription() + " (" + HistoryLogTableName.Letters.ToString() + ")", Value = HistoryLogTableName.Letters.ToString() });
			vm.AvailableTableOptions.Add(new SelectListItem() { Text = HistoryLogTableName.DraftLetters.GetEnumDescription() + " (" + HistoryLogTableName.DraftLetters.ToString() + ")", Value = HistoryLogTableName.DraftLetters.ToString() });
			vm.AvailableTableOptions.Add(new SelectListItem() { Text = HistoryLogTableName.tbl_QuoteAuthDate.GetEnumDescription() + " (" + HistoryLogTableName.tbl_QuoteAuthDate.ToString() + ")", Value = HistoryLogTableName.tbl_QuoteAuthDate.ToString() });

			return View("Index", vm);
		}

		public ActionResult ViewSignatureHistory(int signatureId)
		{
			var vm = new HistoryLogDisplayViewModel();
			vm.Filters.SignatureId = signatureId;
			vm.Filters.Table = HistoryLogTableName.tblSignatures.ToString();
			vm.Filters.Column = Signtures_Columns.Signature.ToString();

			return View("Display", vm);
		}

		public ActionResult ViewShippingHistory(int shippingDataId)
		{
			var vm = new HistoryLogIndexViewModel();
			vm.CurrentView = HistoryView.ShippingHistory;
			vm.Filters.ShippingDataId = shippingDataId;

			//This is configurable based on what history is viewed for so pass from here
			vm.AvailableTableOptions.Add(new SelectListItem() { Text = "", Value = "" });
			vm.AvailableTableOptions.Add(new SelectListItem() { Text = HistoryLogTableName.File_Details.GetEnumDescription() + " (" + HistoryLogTableName.File_Details.ToString() + ")", Value = HistoryLogTableName.File_Details.ToString(), Selected = true });

			return View("Index", vm);
		}

		public ActionResult ViewBugBoxHistory(int bugBoxId)
		{
			var vm = new HistoryLogDisplayViewModel();
			vm.Filters.BugBoxId = bugBoxId;
			vm.Filters.Table = HistoryLogTableName.trBugBox.ToString();
			vm.Filters.Column = BugBox_Columns.BugBox.ToString();

			return View("Display", vm);
		}

		public ActionResult ViewSourceCodeStorageHistory(int sourceCodeStorageId)
		{
			var vm = new HistoryLogDisplayViewModel();
			vm.Filters.SourceCodeStorageId = sourceCodeStorageId;
			vm.Filters.Table = HistoryLogTableName.SourceCodeStorage.ToString();
			vm.Filters.Column = SourceCodeStorage_Columns.CodeStorage.ToString();

			return View("Display", vm);
		}

		public ActionResult ViewEmailDistributionUserHistory(int emailDistributionId)
		{
			var vm = new HistoryLogDisplayViewModel();
			vm.Filters.EmailDistributionId = emailDistributionId;
			vm.Filters.Table = HistoryLogTableName.EmailDistributionUsers.ToString();
			vm.Filters.Column = EmailDistributionUser_Columns.EmailDistributionId.ToString();

			return View("Display", vm);
		}

		[HttpPost]
		public ActionResult CascadeColumnChoice(string tableName)
		{
			return Content(JsonConvert.SerializeObject(PopulateColumns(tableName)));
		}

		#region Helper Methods
		private SubmissionData GetSubmissionInfo(int? submissionId, int? jurisdictionalDataId)
		{
			var submissionData = new SubmissionData();
			if (submissionId != null)
			{
				submissionData = _dbSubmission.Submissions.Where(x => x.Id == submissionId)
								.Select(x => new SubmissionData()
								{
									IdNumber = x.IdNumber,
									FileNumber = x.FileNumber,
									DateCode = x.DateCode,
									GameName = x.GameName,
									Function = x.Function
								}).SingleOrDefault();
			}
			else
			{
				submissionData = _dbSubmission.JurisdictionalData.Where(x => x.Id == jurisdictionalDataId)
								.Select(x => new SubmissionData()
								{
									IdNumber = x.Submission.IdNumber,
									FileNumber = x.Submission.FileNumber,
									DateCode = x.Submission.DateCode,
									GameName = x.Submission.GameName,
									Function = x.Submission.Function,
									JurisdictionId = x.JurisdictionId,
									JurisdictionName = x.JurisdictionName
								}).SingleOrDefault();
			}
			return submissionData;
		}

		// -- This function is used to populate the column Drop down on the index page.
		//	  Add additional conditionals as more tables are required
		//    Add additional columns to tables as needed.
		private List<OptionValue> PopulateColumns(string tableName)
		{
			var columns = new List<OptionValue>();

			if (tableName == HistoryLogTableName.DraftLetters.ToString())
			{
				foreach (var val in Enum.GetValues(typeof(DraftLetters_Columns)))
				{
					columns.Add(new OptionValue() { Val = val.ToString(), Text = val.GetEnumDescription() });
				}
			}
			else if (tableName == HistoryLogTableName.EmailDistributionUsers.ToString())
			{
				foreach (var val in Enum.GetValues(typeof(EmailDistributionUser_Columns)))
				{
					columns.Add(new OptionValue() { Val = val.ToString(), Text = val.GetEnumDescription() });
				}
			}
			else if (tableName == HistoryLogTableName.File_Details.ToString())
			{
				foreach (var val in Enum.GetValues(typeof(Shipping_Columns)))
				{
					columns.Add(new OptionValue() { Val = val.ToString(), Text = val.GetEnumDescription() });
				}
			}
			else if (tableName == HistoryLogTableName.JurisdictionalData.ToString())
			{
				foreach (var val in Enum.GetValues(typeof(JurisdictionalData_Columns)))
				{
					columns.Add(new OptionValue() { Val = val.ToString(), Text = val.GetEnumDescription() });
				}
			}
			else if (tableName == HistoryLogTableName.Letters.ToString())
			{
				foreach (var val in Enum.GetValues(typeof(SubmissionLetters_Columns)))
				{
					columns.Add(new OptionValue() { Val = val.ToString(), Text = val.GetEnumDescription() });
				}
			}
			else if (tableName == HistoryLogTableName.Submissions.ToString())
			{
				foreach (var val in Enum.GetValues(typeof(Submissions_Columns)))
				{
					columns.Add(new OptionValue() { Val = val.ToString(), Text = val.GetEnumDescription() });
				}
			}
			else if (tableName == HistoryLogTableName.tbl_lst_fileJuris.ToString())
			{
				columns.Add(new OptionValue() { Val = tbl_lst_fileJuris_Columns.email.ToString(), Text = tbl_lst_fileJuris_Columns.email.ToString() });
			}
			else if (tableName == HistoryLogTableName.tbl_lst_fileManu.ToString())
			{
				columns.Add(new OptionValue() { Val = tbl_lst_fileManu_Columns.email.ToString(), Text = tbl_lst_fileManu_Columns.email.ToString() });
				columns.Add(new OptionValue() { Val = tbl_lst_fileManu_Columns.manufacturer.ToString(), Text = tbl_lst_fileManu_Columns.manufacturer.ToString() });
			}
			else if (tableName == HistoryLogTableName.tbl_QuoteAuthDate.ToString())
			{
				foreach (var val in Enum.GetValues(typeof(QuoteAuth_Columns)))
				{
					columns.Add(new OptionValue() { Val = val.ToString(), Text = val.GetEnumDescription() });
				}
			}

			return columns;
		}
		#endregion
	}
}