﻿using GLI.EFCore.Submission;
using Newtonsoft.Json;
using Submissions.Common;
using Submissions.Common.JQGrid;
using Submissions.Web.Areas.Reports.Models;
using Submissions.Web.Models;
using Submissions.Web.Models.Grids.Reports;
using Submissions.Web.Models.Query;
using System;
using System.Linq;
using System.Web.Mvc;

namespace Submissions.Web.Areas.Reports.Controllers
{
	public class ProjectsController : Controller
	{
		private readonly SubmissionContext _dbSubmission;
		private readonly IQueryService _queryService;
		private readonly IColumnChooserService _columnChooserService;

		public ProjectsController(SubmissionContext dbSubmission, IQueryService queryService, IColumnChooserService columnChooserService)
		{
			_dbSubmission = dbSubmission;
			_queryService = queryService;
			_columnChooserService = columnChooserService;
		}

		public ActionResult Index()
		{
			var vm = new ProjectsIndexViewModel();
			vm.ProjectSearchPartial.Filters.Sort1 = "ReceiveDate";
			vm.ProjectSearchPartial.Filters.Sort1Order = SortOrder.Desc;

			return View(vm);
		}

		public ActionResult ColumnChooser()
		{
			var savedColumns = _columnChooserService.LoadColumns(ColumnChooserGrid.ProjectsReport);

			var grid = new ProjectsReportGrid();
			grid.InitializeDynamicGrid(savedColumns);

			var vm = new ColumnChooserViewModel();
			vm.SubTitle = "Projects Report";
			vm.AvailableColumns = grid.ColumnChooserAvailableColumns;
			vm.SelectedColumns = grid.ColumnChooserSelectedColumns;

			return PartialView("_ColumnChooser", vm);
		}

		[HttpPost]
		public ActionResult ColumnChooser(string savedColumns, string filters)
		{
			_columnChooserService.SaveColumns(ColumnChooserGrid.ProjectsReport, savedColumns);

			TempData["ProjectSearchFilters"] = JsonConvert.DeserializeObject<ProjectSearch>(filters);
			return RedirectToAction("Display");
		}

		public ActionResult Display(ProjectsIndexViewModel model)
		{
			var filters = (ProjectSearch)(TempData["ProjectSearchFilters"] ?? model.ProjectSearchPartial.Filters);
			var savedColumns = _columnChooserService.LoadColumns(ColumnChooserGrid.ProjectsReport);

			var vm = new ProjectsDisplayViewModel();
			vm.ProjectGrid.InitializeDynamicGrid(savedColumns);
			vm.Filters = filters;

			return View(vm);
		}

		[HttpPost]
		public void Export(ExportType exportType, string filters, string filtersToolbar, string title, string subTitle)
		{
			var searchFilters = JsonConvert.DeserializeObject<ProjectSearch>(filters);
			searchFilters.IsTransfer = ProjectType.All;
			var savedColumns = _columnChooserService.LoadColumns(ColumnChooserGrid.ProjectsReport);
			var query = _queryService.ProjectSearch(searchFilters).GenericSort(searchFilters);

			var grid = new ProjectsReportGrid();
			var export = new Export
			{
				ExportType = exportType,
				FiltersToolbar = filtersToolbar,
				SavedColumns = savedColumns,
				Title = (title + " " + subTitle).TrimEnd()
			};
			grid.Export(export, query);
		}

		public ActionResult SingleInputSearch(string singleInputSearch, bool showList)
		{
			singleInputSearch = singleInputSearch.Trim();

			try
			{
				if (singleInputSearch.Count(x => x == '-') < 4)
				{
					throw new Exception("Not a valid File Number so we can't search with Manufacturer Code");
				}

				var fileNumber = new FileNumber(singleInputSearch).ToString();

				var projectId = _dbSubmission.trProjects
					.Where(x => x.ProjectName == fileNumber)
					.Select(x => x.Id)
					.FirstOrDefault();

				if (projectId > 0 && !showList)
				{
					return RedirectToAction("Detail", "Project", new { area = "Protrack", id = projectId });
				}
			}
			catch { }

			TempData["ProjectSearchFilters"] = new ProjectSearch { SingleInputSearch = singleInputSearch, SingleInputSearchProjectOnly = (showList) ? true : false };

			return RedirectToAction("Display");
		}
	}
}