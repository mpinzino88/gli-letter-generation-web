﻿using System.Web.Mvc;

namespace Submissions.Web.Areas.Reports
{
	public class ReportsAreaRegistration : AreaRegistration
	{
		public override string AreaName
		{
			get
			{
				return "Reports";
			}
		}

		public override void RegisterArea(AreaRegistrationContext context)
		{
			context.MapRoute(
				"Reports_default",
				"Reports/{controller}/{action}/{id}",
				new { action = "Index", id = UrlParameter.Optional }
			);

			context.MapRoute(
				"AuditTrail_default",
				"Reports/{controller}/{action}/{entity}/{entityId}",
				new { action = "Index", entity = UrlParameter.Optional, entityId = UrlParameter.Optional }
			);
		}
	}
}