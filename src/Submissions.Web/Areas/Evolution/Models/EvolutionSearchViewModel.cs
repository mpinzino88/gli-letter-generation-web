﻿using EF.eResultsManaged;
using EF.GLIAccess;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Submissions.Web.Areas.Evolution.Models
{
	[Serializable]
	public class EvolutionSearchViewModel
	{
		public EvolutionSearchViewModel()
		{
			var dc_ga = new GLIAccessContext();
			var dc_em = new eResultsManagedContext();

			//JuriData
			JuriData = new JurisdictionData();
			var fileJurisDictionary = (
				from fj in dc_ga.tbl_lst_fileJuris
				orderby fj.Name
				select new FileJuris
				{
					Id = fj.Id ?? "",
					Name = fj.Name ?? ""
				}
			).ToDictionary(t => Convert.ToInt32(t.Id), t => t.Name);

			JuriData.JurisDictionary = fileJurisDictionary;
			JuriData.SortedNameList = fileJurisDictionary.Values.ToList();

			//FuncRoleData
			FuncRoleData = new FunctionalRoleData();
			var functionRoles = (
				from fr in dc_em.FunctionalRole
				orderby fr.Name
				select new FileFunctionalRole
				{
					Id = fr.Id,
					Name = fr.Name ?? ""
				}
			).ToDictionary(t => t.Id, t => t.Name);

			FuncRoleData.FuncRolesDictionary = functionRoles;
			FuncRoleData.SortedNameList = functionRoles.Values.ToList();

			//DocData
			DocData = new DocumentData();
			var documents = (
				from doc in dc_em.Document
				where doc.Inactive == false
				orderby doc.Name
				select new EResultsDocument
				{
					Id = doc.Id,
					//If add version in case duplicated document name
					Name = (String.IsNullOrEmpty(doc.Version) ? doc.Name : doc.Name + " (" + doc.Version + ")") ?? ""
				}
			).ToDictionary(t => t.Id, t => t.Name);

			DocData.DocDictionary = documents;
			DocData.SortedNameList = documents.Values.ToList();

			//Test Case: using the clause ID instead of questionbase ID
			TestCaseData = new QuestionBaseTestCaseData();
			var testCase = (
				from qb in dc_em.QuestionBase
				where qb.aType == 9 && qb.State != 3
				orderby qb.Name
				select new QuestionBaseTestCase
				{
					Id = qb.Id,
					Name = qb.Name ?? ""
				}
			).ToDictionary(t => t.Id, t => t.Name);

			TestCaseData.TestCaseDictionary = testCase;
			TestCaseData.SortedNameList = testCase.Values.ToList();
		}

		public JurisdictionData JuriData { get; set; }
		public FunctionalRoleData FuncRoleData { get; set; }
		public DocumentData DocData { get; set; }
		public QuestionBaseTestCaseData TestCaseData { get; set; }
		public string GridId { get; set; }
	}

	//Search Data
	[Serializable]
	public class JurisdictionData
	{
		public List<string> SortedNameList { get; set; }
		public Dictionary<Int32, string> JurisDictionary { get; set; }
	}

	[Serializable]
	public class FunctionalRoleData
	{
		public List<string> SortedNameList { get; set; }
		public Dictionary<int, string> FuncRolesDictionary { get; set; }
	}

	[Serializable]
	public class DocumentData
	{
		public List<string> SortedNameList { get; set; }
		public Dictionary<int, string> DocDictionary { get; set; }
	}

	[Serializable]
	public class QuestionBaseTestCaseData
	{
		public List<string> SortedNameList { get; set; }
		public Dictionary<Guid, string> TestCaseDictionary { get; set; }
	}

	//Dictionary
	[Serializable]
	public class FileJuris
	{
		public string Name { get; set; }
		public string Id { get; set; }
	}

	[Serializable]
	public class FileFunctionalRole
	{
		public string Name { get; set; }
		public int Id { get; set; }
	}

	[Serializable]
	public class EResultsDocument
	{
		public string Name { get; set; }
		public int Id { get; set; }
	}

	[Serializable]
	public class QuestionBaseTestCase
	{
		public string Name { get; set; }
		public Guid Id { get; set; }
	}

	[Serializable]
	public class AllClauseFuncRole
	{
		public string CId { get; set; }
		public string FuncRoles { get; set; }
	}
}