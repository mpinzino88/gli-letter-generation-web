﻿using Submissions.Core.Models;
using Submissions.Core.Models.EvolutionService;
using Submissions.Core.Models.EvoService;
using Submissions.Web.Models.Query.Evolution;
using System;
using System.Collections.Generic;

namespace Submissions.Web.Areas.Evolution.Models
{
	public class ReviewIndexViewModel
	{
		public ReviewIndexViewModel()
		{
			this.Filters = new EvolutionFilter();
			this.DistinctDocs = new List<ClauseDocumentTestCase>();
			this.ClauseDocumentTestCase = new List<ClauseDocumentTestCase>();
		}

		public EvolutionFilter Filters { get; set; }
		public IList<ClauseDocumentTestCase> DistinctDocs { get; set; }
		public IList<ClauseDocumentTestCase> ClauseDocumentTestCase { get; set; }
		public string ReviewerEmail { get; set; }
		public string UserName { get; set; }
		public string Office { get; set; }
	}

	public class ReviewTestCaseViewModel
	{
		public ReviewTestCaseViewModel()
		{
			this.Display = true;
		}

		public bool Display { get; set; }
		public Int16 TestCaseOrder { get; set; }
		public int PassCount { get; set; }
		public int FailCount { get; set; }
		public int NACount { get; set; }
		public int DoneCount { get; set; }
		public int DoneTotalCount { get; set; }
		public int JiraCount { get; set; }
		public string Name { get; set; }
	}

	public class ReviewTestScriptViewModel
	{
		public ReviewTestScriptViewModel()
		{
			this.Display = true;
			this.OriginalAnswer = this.Answer;
			this.OriginalNotes = this.Notes;
			this.Modified = false;
			this.Visible = null;
		}

		public DateTime? AnsweredTime { get; set; }
		public bool? Visible { get; set; }
		public bool Modified { get; set; }
		public string OriginalAnswer { get; set; }
		public string OriginalNotes { get; set; }
		public bool ShowNote { get; set; }
		public List<ClauseReportModelQuestionAndJuri> ClauseInfo { get; set; }
		public List<TestStep> TestStepInfo { get; set; }
		public List<AnswerHistory> AnswerHistoryInfo { get; set; }
		public List<GPSItemLink> GpsItemLink { get; set; }
		public List<string> JuriList { get; set; }
		public bool Display { get; set; }
		public Guid Id { get; set; }
		public string Name { get; set; }
		public long Group { get; set; }
		public int Level { get; set; }
		public string NormalizedID { get; set; }
		public string Description { get; set; }
		public string Text { get; set; }
		public Guid Parent { get; set; }
		public string OrderID { get; set; }
		public string qOrderID { get; set; }
		public string Answer { get; set; }
		public string Notes { get; set; }
		public string SourceInstance { get; set; }
		public string SourceUser { get; set; }
		public DateTime? SourceDate { get; set; }
		public string CopiedFrom { get; set; }
		//public DateTime? AnswerDate { get; set; }
		public string QuestionType { get; set; }
		public string AnsweredBy { get; set; }
		public string AnswerType { get; set; }
		public string JiraKeyList { get; set; }
		public Guid? SourceAnswer { get; set; }
		public Int16? TestCaseOrder { get; set; }
		public bool? NewToUser { get; set; }
		public string GPSDocUploadIdList { get; set; }
		public string Evaluation { get; set; }
		public Guid? Source { get; set; }
	}

	public class ClauseReportViewModel
	{
		public ClauseReportViewModel()
		{
			this.ClauseReports = new List<ClauseReportModel>();
			this.SummarySections = new List<ClauseDocumentSummary>();
			this.ApplicableTestCases = new List<ApplicableTestCase>();
			this.DistinctDocs = new List<ClauseDocumentTestCase>();
			this.ClauseDocumentTestCase = new List<ClauseDocumentTestCase>();
		}

		public IList<ClauseReportModel> ClauseReports { get; set; }
		public IList<ClauseDocumentSummary> SummarySections { get; set; }
		public IList<ApplicableTestCase> ApplicableTestCases { get; set; }
		public IList<ClauseDocumentTestCase> DistinctDocs { get; set; }
		public IList<ClauseDocumentTestCase> ClauseDocumentTestCase { get; set; }
		public int PassCount { get; set; }
		public int FailCount { get; set; }
		public int NACount { get; set; }
		public int NAStarCount { get; set; }
		public bool hasNevada { get; set; }
		public bool hasPeru { get; set; }
		public int JurisdictionId { get; set; }
		public string Language { get; set; }
	}
}