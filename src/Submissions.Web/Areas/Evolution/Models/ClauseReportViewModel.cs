﻿using Submissions.Web.Models.Query.Evolution;

namespace Submissions.Web.Areas.Evolution.Models
{
	public class ClauseReportIndexViewModel
	{
		public ClauseReportIndexViewModel()
		{
			this.Filters = new EvolutionFilter();
		}

		public EvolutionFilter Filters { get; set; }
	}
}