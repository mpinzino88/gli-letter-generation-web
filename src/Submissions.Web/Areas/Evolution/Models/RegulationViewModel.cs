﻿using Submissions.Core.Models.EvolutionService.Query;
using Submissions.Web.Models.Grids.Evolution;

namespace Submissions.Web.Areas.Evolution.Models
{
	public class RegulationIndexViewModel
	{
		public RegulationIndexViewModel()
		{
			this.Filters = new RegulationSearch();
		}
		public RegulationSearch Filters { get; set; }
	}

	public class RegulationDisplayViewModel
	{
		public RegulationDisplayViewModel()
		{
			this.RegulationGrid = new RegulationGrid();
			this.Filters = new RegulationSearch();
		}

		public RegulationGrid RegulationGrid { get; set; }
		public RegulationSearch Filters { get; set; }

		public string Title
		{
			get
			{
				switch (Filters.SearchType)
				{
					case EvoSearchType.RegulatorySearch: return "Regulation Search";
					case EvoSearchType.RegulatoryCompare: return "Regulation Comparison";
					default: return "Regulation Search";
				}
			}
		}
	}
}