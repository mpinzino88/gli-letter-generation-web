﻿using Submissions.Web.Models.Grids.Evolution;
using System;
using System.Web;
using System.Web.Mvc;

namespace Submissions.Web.Areas.Evolution.Models
{
	public class QANotesIndexViewModel
	{
		public QANotesGrid QANotesGridModel { get; set; }
		public QANotesFilter Filters { get; set; }

		public string ExportUrl
		{
			get
			{
				var url = new UrlHelper(HttpContext.Current.Request.RequestContext);
				return url.Action("Export", "QANotes");
			}
		}
	}

	[Serializable]
	public class QANotesFilter
	{
		public string Active { get; set; }
		public Guid? ValRuleId { get; set; }
		public string NoteId { get; set; }
		public string UniqueId { get; set; }
		public string Heading { get; set; }
		public string Note { get; set; }
		public string LanguageNote { get; set; }
		public string Language { get; set; }
		public string Section { get; set; }
		public string TechLogic { get; set; }
		public string RuleMemo { get; set; }
		public string QBText { get; set; }
		public string ApplicableResult { get; set; }
		public string QAMemo { get; set; }
	}
}