﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Submissions.Web.Areas.Evolution.Models
{
	public class ComparisonIndexViewModel
	{
		public ComparisonIndexViewModel()
		{
			this.Jurisdictions = new List<SelectListItem>();
		}

		[Display(Name = "Jurisdictions")]
		public string[] JurisdictionIds { get; set; }
		[Display(Name = "Jurisdiction")]
		public IList<SelectListItem> Jurisdictions { get; set; }
	}

	public class FunctionalRolesComparisonRow
	{
		public int FunctionalRoleId { get; set; }
		public string FunctionalRole { get; set; }
		public bool ComponentGroup1 { get; set; }
		public bool ComponentGroup2 { get; set; }
		public bool ComponentGroup3 { get; set; }
		public bool ComponentGroup4 { get; set; }
		public bool ComponentGroup5 { get; set; }
		public bool ComponentGroup6 { get; set; }
		public bool ComponentGroup7 { get; set; }
		public bool ComponentGroup8 { get; set; }
		public bool ComponentGroup9 { get; set; }
		public bool ComponentGroup10 { get; set; }
	}
}