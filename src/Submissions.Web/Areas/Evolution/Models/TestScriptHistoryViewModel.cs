﻿using EF.eResultsManaged;
using Submissions.Core.Models.EvolutionService.Query;
using Submissions.Web.Models.Grids.Evolution;
using System;
using System.Collections.Generic;

namespace Submissions.Web.Areas.Evolution.Models
{
	public class TestScriptHistoryIndexViewModel
	{
		public TestScriptHistoryIndexViewModel()
		{
			Filters = new TestScriptHistorySearch();
			TestScriptHistoryGrid = new TestScriptHistoryGrid();
			ContentChangesGrid = new ContentChangesGrid();
			LinkageChangesGrid = new LinkageChangesGrid();
			NevadaChangesGrid = new NevadaChangesGrid();
			NevadaSideGrid = new NevadaSideGrid();
		}
		public TestScriptHistorySearch Filters { get; set; }
		public ContentChangesGrid ContentChangesGrid { get; set; }
		public LinkageChangesGrid LinkageChangesGrid { get; set; }
		public NevadaChangesGrid NevadaChangesGrid { get; set; }
		public NevadaSideGrid NevadaSideGrid { get; set; }
		public TestScriptHistoryGrid TestScriptHistoryGrid { get; set; }

	}

	public class ApproveRejectOptionsViewModel
	{
		public ApproveRejectOptionsViewModel()
		{
			this.RevisionVersions = new List<TCVerisonViewModel>();
		}
		public string TestCase { get; set; }
		public List<TCVerisonViewModel> RevisionVersions { get; set; }
	}

	public class TCVerisonViewModel : JurisdictionTCVersion
	{
		public DateTime? ApprovalSetDate { get; set; }
		public DateTime? RejectSetDate { get; set; }
	}


	//public class TCChangesViewModel : VMBase
	//{
	//	public TCChangesViewModel(Guid tcID)
	//	{
	//		TCVersionVM = new JurisdictionTCVersionViewModel(JurisdictionTCVersion.Queryable.Where(x => x.TestCaseID == tcID && x.State == 1).FirstOrDefault(), null);
	//		WireCommands();
	//	}

	//	public ReactiveCommand RevVersionCommand { get; private set; }
	//	public ReactiveCommand RevMajorVersionCommand { get; private set; }
	//	public ReactiveCommand RevDraftVersionCommand { get; private set; }
	//	public ReactiveCommand RevDraftMajorVersionCommand { get; private set; }
	//	public ReactiveCommand UndoRevCommand { get; private set; }
	//	private void WireCommands()
	//	{
	//		RevVersionCommand = new ReactiveCommand();
	//		RevVersionCommand.Subscribe(_ => RevVersion(false));
	//		RevMajorVersionCommand = new ReactiveCommand();
	//		RevMajorVersionCommand.Subscribe(_ => RevVersion(true));
	//		UndoRevCommand = new ReactiveCommand();
	//		UndoRevCommand.Subscribe(_ => UndoRev());
	//	}
	//	public void RevVersion(bool major = false)
	//	{
	//		string decVerString = "";
	//		NewDraft = true;
	//		if (CurrentVersion == "Not Set")
	//		{
	//			if (major) NewVersion = "1.0";
	//			else NewVersion = "0.1";
	//			return;
	//		}
	//		decVerString = CurrentVersion;
	//		decimal decVer;
	//		if (!Decimal.TryParse(decVerString, out decVer))
	//		{
	//			MessageBox.Show("There was a problem setting a new version for " + TestCaseName);
	//			return;
	//		}
	//		else
	//		{
	//			decVer = IncrementDigit(decVer, major);
	//			NewVersion = decVer.ToString("#.0");
	//		}
	//	}

	//	public void UndoRev()
	//	{
	//		NewVersion = null;
	//		NewDraft = null;
	//	}

	//	static decimal IncrementDigit(decimal value, bool first = false)
	//	{
	//		if (first)
	//		{
	//			return (Decimal.Floor(value) + 1);
	//		}
	//		else
	//		{
	//			int[] bits1 = decimal.GetBits(value);
	//			int saved = bits1[3];
	//			bits1[3] = 0;
	//			// Set scaling to 0, remove sign                
	//			int[] bits2 = decimal.GetBits(new decimal(bits1) + 1);
	//			bits2[3] = saved;
	//			// Restore original scaling and sign                
	//			return new decimal(bits2);
	//		}
	//	}

	//	public string TestCaseName { get; set; }
	//	public Guid TestCaseID { get; set; }
	//	private string _CurrentVersion = null;
	//	public string CurrentVersion
	//	{
	//		get { return TCVersionVM.TCVersion != null ? TCVersionVM.TCVersion.Version : "Not Set"; }
	//		set { _CurrentVersion = value; }
	//	}
	//	private bool _CurrentDraft;
	//	public bool CurrentDraft
	//	{
	//		get { return TCVersionVM.TCVersion != null ? TCVersionVM.TCVersion.Draft : false; }
	//		set { _CurrentDraft = value; }
	//	}
	//	private string _NewVersion = null;
	//	public string NewVersion
	//	{
	//		get { return _NewVersion; }
	//		set { this.RaiseAndSetIfChanged(x => x.NewVersion, value); }
	//	}
	//	private bool? _NewDraft = null;
	//	public bool? NewDraft
	//	{
	//		get { return _NewDraft; }
	//		set { this.RaiseAndSetIfChanged(x => x.NewDraft, value); }
	//	}
	//	private List<Change> _Changes = new List<Change>();
	//	public List<Change> Changes
	//	{
	//		get { return _Changes; }
	//		set { this.RaiseAndSetIfChanged(x => x.Changes, value); }
	//	}
	//	private JurisdictionTCVersionViewModel _TCVersionVM;
	//	public JurisdictionTCVersionViewModel TCVersionVM
	//	{
	//		get { return _TCVersionVM; }
	//		set { this.RaiseAndSetIfChanged(x => x.TCVersionVM, value); }
	//	}
	//}
}