﻿using Submissions.Common;
using Submissions.Common.JQGrid;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;
using Trirand.Web.Mvc;

namespace Submissions.Web.Areas.Evolution.Models
{
	[Serializable]
	public class RegulationAnalysisIndexViewModel
	{
		public EvolutionSearchViewModel EvoSearch { get; set; }
		public RegulationAnalysisIndexGridModel RegulationAnalysisGrid { get; set; }
		public RegulationAnalysisJurisdictionModel autocomplete { get; set; }
	}

	#region Grid
	public class RegulationAnalysisIndexGridModel
	{
		public RegulationAnalysisIndexGridModel()
		{
			Grid = new JQGrid
			{
				Columns = new List<JQGridColumn>
				{
					new JQGridColumn
					{
						DataField = "JuriId",
						HeaderText = "Juri ID",
						PrimaryKey = true,
						Editable = false,
						TextAlign = Trirand.Web.Mvc.TextAlign.Center,
						Width = 50,
						DataType = typeof( string ),
						SearchToolBarOperation = SearchOperation.IsEqualTo
					},
					new JQGridColumn
					{
						DataField = "Jurisdiction",
						DataType = typeof(string),
						HeaderText = "Jurisdiction",
						PrimaryKey = false,
						Editable = false,
						TextAlign = Trirand.Web.Mvc.TextAlign.Center,
						Width = 80
					},
					new JQGridColumn
					{
						DataField = "Name",
						HeaderText = "Name",
						DataType = typeof(string),
						TextAlign = Trirand.Web.Mvc.TextAlign.Center,
						Width = 80,
						Visible = true
					},
					new JQGridColumn
					{
						DataField = "Version",
						HeaderText = "Version",
						DataType = typeof(string),
						TextAlign = Trirand.Web.Mvc.TextAlign.Center,
						Width = 50,
						Visible = true
					},
					new JQGridColumn
					{
						DataField = "ClauseId",
						HeaderText = "Clause ID",
						PrimaryKey = false,
						Editable = false,
						TextAlign = Trirand.Web.Mvc.TextAlign.Center,
						Width = 63,
						DataType = typeof( string ),
						SearchToolBarOperation = SearchOperation.IsEqualTo,
						Visible = true //Hide for public by Jquery
					},
					new JQGridColumn
					{
						DataField = "Section",
						HeaderText = "Section",
						DataType = typeof(string),
						TextAlign = Trirand.Web.Mvc.TextAlign.Center,
						Width = 85
					},
					new JQGridColumn
					{
						DataField = "TestCase",
						HeaderText = "Test Case",
						DataType = typeof(string),
						TextAlign = Trirand.Web.Mvc.TextAlign.Center,
						Width = 85
					},
					new JQGridColumn
					{
						DataField = "Memo",
						HeaderText = "Memo",
						DataType = typeof(string),
						Width = 170
					},
					new JQGridColumn
					{
						DataField = "Text",
						HeaderText = "Text",
						DataType = typeof(string),
						Width = 180
					},
					new JQGridColumn
					{
						DataField = "FuncRoleName",
						HeaderText = "Functional Role",
						DataType = typeof(string),
						Width = 200
					}
				}
			};

			Grid.ID = "RegulationAnalysisGrid";
			Grid.DataUrl = new UrlHelper(((MvcHandler)HttpContext.Current.Handler).RequestContext).Action("LoadGridIndex");
			Grid.AutoWidth = true;
			Grid.Width = Unit.Percentage(100);
			Grid.Height = Unit.Percentage(100);
			Grid.SearchDialogSettings.MultipleSearch = true;
			Grid.ToolBarSettings = new ToolBarSettings
			{
				ShowRefreshButton = true,
				ShowSearchToolBar = true,
				ShowSearchButton = true,
				ToolBarPosition = ToolBarPosition.Top
			};

			Grid.PagerSettings = new Trirand.Web.Mvc.PagerSettings
			{
				PageSize = 20,
				PageSizeOptions = "[]"
			};
			Grid.AppearanceSettings = new AppearanceSettings
			{
				AlternateRowBackground = true,
				HighlightRowsOnHover = true,
				ShowFooter = false
			};
		}

		/// <summary>
		/// Export from grid
		/// </summary>
		public static void Export(ExportType exportType, IQueryable<RegulationAnalysisQuery> query, NameValueCollection queryString,
			Dictionary<int, string> jurisDictionary, Dictionary<string, string> multiFuncRoles)
		{
			var fileName = string.Format("Regulation GAP Analysis - {0:yyyy-MM-dd_hh-mm-ss-tt}", DateTime.Now);
			var grid = new RegulationAnalysisIndexGridModel().Grid;

			var jqGridHelper = new JQGridHelper();
			var gridstate = new JQGridState();
			gridstate.QueryString = queryString;

			if (exportType == ExportType.Xls)
			{
				fileName += ".xls";
				grid.ExportSettings.ExportDataRange = ExportDataRange.Filtered;

				// further filter query
				var dataSource = grid.GetFilteredDataSource(query, gridstate);

				DataGrid dataGrid = grid.GetExportGrid();

				// associate grid with query
				IQueryable iqueryable = dataSource as IQueryable;
				if (iqueryable != null)
				{
					dataGrid.DataSource = iqueryable.ToDataTable(grid);
				}
				else
				{
					dataGrid.DataSource = dataSource;
				}

				// run query
				dataGrid.DataBind();

				// add jurisdictions to datatable
				DataGridModifyCols(ref dataGrid, jurisDictionary, multiFuncRoles);

				// modify "grid" to add jurisdiction info
				jqGridHelper.RenderExcelToStream(dataGrid, fileName);

			}
			else if (exportType == ExportType.Csv)
			{
				fileName += ".csv";
				grid.ExportSettings.ExportDataRange = ExportDataRange.Filtered;
				var exportData = grid.GetExportData(query, gridstate);
				// add jurisdictions to datatable
				DataTableModifyCols(ref exportData, jurisDictionary, multiFuncRoles);
				jqGridHelper.ExportToCSV(exportData, fileName);
			}
			else if (exportType == ExportType.Pdf)
			{
				fileName += ".pdf";
				grid.ExportSettings.ExportDataRange = ExportDataRange.Filtered;
				var exportData = grid.GetExportData(query, gridstate);
				// add jurisdictions to datatable
				DataTableModifyCols(ref exportData, jurisDictionary, multiFuncRoles);
				jqGridHelper.ExportToFormatPDF(exportData, fileName);
			}
			else if (exportType == ExportType.Doc)
			{
				fileName += ".doc";
				grid.ExportSettings.ExportDataRange = ExportDataRange.Filtered;
				var exportData = grid.GetExportData(query, gridstate);
				// add jurisdictions to datatable
				DataTableModifyCols(ref exportData, jurisDictionary, multiFuncRoles);
				jqGridHelper.ExportToWord(exportData, fileName);
			}
		}

		public static void DataGridModifyCols(ref DataGrid dg, Dictionary<int, string> jurisDictionary, Dictionary<string, string> funcRoleDictionary)
		{
			//Get column index
			var gridModel = new RegulationAnalysisIndexGridModel();
			int idIndex = 0;
			int nameIndex = 0;
			int clauseIdIndex = 0;
			int funcRolesIndex = 0;
			for (int i = 0; i < dg.Columns.Count; i++)
			{
				if (dg.Columns[i].HeaderText == gridModel.JuriId)
				{
					idIndex = i;
				}
				if (dg.Columns[i].HeaderText == gridModel.JuriName)
				{
					nameIndex = i;
				}
				if (dg.Columns[i].HeaderText == gridModel.ClauseId)
				{
					clauseIdIndex = i;
				}
				if (dg.Columns[i].HeaderText == gridModel.FuncRoleName)
				{
					funcRolesIndex = i;
				}
			}
			//Modify Columns
			if (jurisDictionary != null && funcRoleDictionary != null)
			{
				foreach (DataGridItem item in dg.Items)
				{
					if (!String.IsNullOrEmpty(item.Cells[idIndex].Text))
					{
						item.Cells[nameIndex].Text = jurisDictionary[Convert.ToInt32(item.Cells[idIndex].Text)];
					}
					if (!String.IsNullOrEmpty(item.Cells[clauseIdIndex].Text))
					{
						item.Cells[funcRolesIndex].Text = funcRoleDictionary[item.Cells[clauseIdIndex].Text];
					}
				}
			}
			//Hidden column Clause ID
			dg.Columns[clauseIdIndex].Visible = false;
		}

		public static void DataTableModifyCols(ref DataTable dt, Dictionary<int, string> jurisDictionary, Dictionary<string, string> funcRoleDictionary)
		{
			//Get column index
			var gridModel = new RegulationAnalysisIndexGridModel();
			var idCol = dt.Columns[gridModel.JuriId];
			var nameCol = dt.Columns[gridModel.JuriName];
			var clauseIdCol = dt.Columns[gridModel.ClauseId];
			var funcRolesCol = dt.Columns[gridModel.FuncRoleName];
			int idIndex = idCol != null ? idCol.Ordinal : 0;
			int nameIndex = nameCol != null ? nameCol.Ordinal : 0;
			int clauseIdIndex = clauseIdCol != null ? clauseIdCol.Ordinal : 0;
			int funcRolesIndex = funcRolesCol != null ? funcRolesCol.Ordinal : 0;
			//Modify Columns
			if (jurisDictionary != null && funcRoleDictionary != null)
			{
				foreach (DataRow row in dt.Rows)
				{
					if (!String.IsNullOrEmpty(row[idIndex].ToString()))
					{
						row.SetField(nameIndex, jurisDictionary[Convert.ToInt32(row[idIndex])]);
					}
					if (!String.IsNullOrEmpty(row[clauseIdIndex].ToString()))
					{
						row.SetField(funcRolesIndex, funcRoleDictionary[row[clauseIdIndex].ToString()].Replace("<br />", "\n"));
					}
				}
			}
			//Hidden column Clause ID
			dt.Columns.RemoveAt(clauseIdIndex);
		}

		public JQGrid Grid { get; set; }

		public string JuriId = "JuriId";
		public string JuriName = "Jurisdiction";
		public string ClauseId = "Clause ID";
		public string FuncRoleName = "Functional Role";
	}

	#endregion

	[Serializable]
	public class RegulationAnalysisJurisdictionModel
	{
		public IEnumerable<string> JuriIdAutocompleteList { get; set; }
		public IEnumerable<string> JurisdictionAutocompleteList { get; set; }
		public IEnumerable<string> NameAutocompleteList { get; set; }
		public IEnumerable<string> VersionAutocompleteList { get; set; }
		//public IEnumerable<string> DocumentIDMemoAutocompleteList { get; set; }
		public IEnumerable<string> SectionAutocompleteList { get; set; }
		public IEnumerable<string> TestCaseAutocompleteList { get; set; }
		public IEnumerable<string> FuncRoleAutocompleteList { get; set; }
	}

	[Serializable]
	public class RegulationAnalysisAutocomplete
	{
		public IQueryable<string> JurIdResults { get; set; }
		public IQueryable<string> JurisdictionResults { get; set; }
		public IQueryable<string> NameResults { get; set; }
		public IQueryable<string> VersionResults { get; set; }
		//public IQueryable<string> DocumentIDResults { get; set; }
		public IQueryable<string> SectionResults { get; set; }
		public IQueryable<string> TestCaseResults { get; set; }
		public IQueryable<string> FuncRoleResults { get; set; }
	}

	[Serializable]
	public class RegulationAnalysisQuery
	{
		public string JuriId { get; set; }
		public string Jurisdiction { get; set; }
		public string Name { get; set; }
		public string Version { get; set; }
		public string ClauseId { get; set; }
		public string Section { get; set; }
		public string TestCase { get; set; }
		public string Memo { get; set; }
		public string Text { get; set; }
		public string FuncRoleName { get; set; }
	}
}