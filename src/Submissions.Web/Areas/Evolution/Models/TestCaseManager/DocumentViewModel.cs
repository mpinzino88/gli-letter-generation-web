﻿using Submissions.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace Submissions.Web.Areas.Evolution.Models
{
	public class DocumentIndexViewModel
	{
		public DocumentIndexViewModel()
		{
			Documents = new List<Document>();
		}
		// add search stuff
		public DocumentSearch DocumentSearch { get; set; }
		public List<Document> Documents { get; set; }
	}

	public class DocumentEditViewModel
	{
		public DocumentEditViewModel()
		{
			JurisdictionIds = new List<string>();
			JurisdictionNames = new List<string>();
		}
		public int? Id { get; set; }
		public string Name { get; set; }
		public string Version { get; set; }
		public List<string> JurisdictionIds { get; set; }
		public IEnumerable<SelectListItem> JurisdictionList { get; set; }
		public List<string> JurisdictionNames { get; set; }
	}

	public class Document
	{
		public Document()
		{
			//Jurisdictions = new List<string>();
			//FunctionalRoles = new List<string>();
		}
		public int Id { get; set; }
		public string Name { get; set; }
		public string Version { get; set; }
		public List<string> Jurisdictions { get; set; }
		//public string Status { get; set; }
		//public List<string> FunctionalRoles { get; set; }
	}

	public class DocumentSearch
	{
		public DocumentSearch()
		{
			StartDate = null;
			EndDate = null;
			FilterAllJurisdictions = false;
			ExactName = false;
			JurisdictionIds = new List<string>();
			JurisdictionNames = new List<string>();
			StatusOptions = LookupsStandard.ConvertEnum<EvolutionComponentState>(false, false).ToList();
			Status = new List<string>();
		}
		public DateTime? StartDate { get; set; }
		public DateTime? EndDate { get; set; }
		public bool FilterAllJurisdictions { get; set; }
		public List<string> JurisdictionIds { get; set; }
		public IEnumerable<SelectListItem> JurisdictionList { get; set; }
		public List<string> JurisdictionNames { get; set; }
		public string Name { get; set; }
		public bool ExactName { get; set; }
		public IEnumerable<SelectListItem> StatusOptions { get; set; }
		public List<string> Status { get; set; }
	}
}