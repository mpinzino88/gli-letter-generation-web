﻿using System;
using System.Collections.Generic;

namespace Submissions.Web.Areas.Evolution.Models
{
	public partial class ClauseViewModel
	{
		public List<JurisdictionViewModel> Jurisdictions { get; set; }
		public List<LinkedDocsViewModel> Documents { get; set; }
		public Guid Id { get; set; }
		public DateTime? ClauseDate { get; set; }
		public DateTime? StateDate { get; set; }
		public int? State { get; set; }
		public string SpecialNote { get; set; }
		public short Category { get; set; }
		public string NativeSectionName { get; set; }
		public string NativeMemo { get; set; }
		public string NormalizedId { get; set; }
		public int? OrderId { get; set; }
		public DateTime? InactiveDate { get; set; }
		public bool? Inactive { get; set; }
		public string Section { get; set; }
		public string SectionName { get; set; }
		public string Memo { get; set; }
		public string Language { get; set; }
	}
	public partial class ClauseSearchResultData
	{
		public List<ClauseViewModel> Clauses { get; set; }
		public int ResultTotalCount { get; set; }
		public int PageCount { get; set; }
	}
	public class JurisdictionViewModel
	{
		public int Id { get; set; }
		public string Jurisdiction { get; set; }
	}
	public class LinkedDocsViewModel
	{
		public int ID { get; set; }
		public string Document { get; set; }
		public string Version { get; set; }
	}
}