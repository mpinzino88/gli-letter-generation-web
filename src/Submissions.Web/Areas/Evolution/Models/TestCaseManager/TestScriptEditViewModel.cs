﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace Submissions.Web.Areas.Evolution.Models
{
	public partial class TestScriptEditViewModel
	{

	}

	public class TestScriptTableOfContentsViewModel
	{
		
		public string NodeText;
		public Guid Id;
		public string OrderId; // This might need to be something else.
	}
}