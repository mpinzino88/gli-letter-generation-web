﻿using System.Collections.Generic;

namespace Submissions.Web.Areas.Evolution.Models
{
	public class RelatedClauseSearch
	{
		public string SectionName { get; set; }
		public List<DocumentSearchInfo> Documents { get; set; }
	}
	public class DocumentSearchInfo
	{
		public int ID { get; set; }
		public string Document { get; set; }
		public string Version { get; set; }
	}
}