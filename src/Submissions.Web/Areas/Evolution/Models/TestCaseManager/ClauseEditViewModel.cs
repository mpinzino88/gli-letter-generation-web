﻿using Submissions.Common;
using Submissions.Web.Areas.Lookups.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace Submissions.Web.Areas.Evolution.Models
{
	public class ClauseEditViewModel : Lookup
	{
		public ClauseEditViewModel()
		{
			OriginalClauseDate = DateTime.Today;
			ClauseStates = LookupsStandard.ConvertEnum<EvoLogic.Constants.State>().ToList();
			ClauseTestingTypes = LookupsStandard.ConvertEnum<EvoLogic.Constants.Category>().ToList(); //this is poorly defined
			ClauseNativeLanguages = LookupsStandard.ConvertEnum<TCMLanguage>(showBlank: true).ToList();
			Jurisdictions = new List<SelectListItem>();
			Documents = new List<SelectListItem>();
			Questions = new List<SelectListItem>();
			Roles = new List<SelectListItem>();
		}
		public Guid Id { get; set; }
		public string ClauseState { get; set; }
		public List<SelectListItem> ClauseStates { get; set; }
		public string ClauseTestingType { get; set; }
		public List<SelectListItem> ClauseTestingTypes { get; set; }
		public string SectionName { get; set; }
		public string SectionId { get; set; }
		public string ClauseText_English { get; set; }
		public string ClauseNativeLanguage { get; set; }
		public List<SelectListItem> ClauseNativeLanguages { get; set; }
		public string ClauseNativeSectionName { get; set; }
		public string ClauseNativeText { get; set; }
		public DateTime? OriginalClauseDate { get; set; }
		public List<int> JurisdictionIds { get; set; }
		public List<SelectListItem> Jurisdictions { get; set; }
		public List<int> DocumentIds { get; set; }
		public List<SelectListItem> Documents { get; set; }
		public string ClauseSpecificTestingNotes { get; set; }
		public List<Guid> QuestionIds { get; set; }
		public List<SelectListItem> Questions { get; set; }
		public List<int> RoleIds { get; set; }
		public List<SelectListItem> Roles { get; set; }
		public string NormalizedId { get; set; }
	}
}