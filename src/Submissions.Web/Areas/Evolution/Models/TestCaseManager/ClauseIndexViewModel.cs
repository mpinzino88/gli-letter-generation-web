﻿using Submissions.Core;

namespace Submissions.Web.Areas.Evolution.Models
{
	public partial class ClauseIndexViewModel
	{
		public ClauseIndexViewModel()
		{
			this.ClauseSearch = new ClauseSearch();
		}
		public ClauseSearch ClauseSearch { get; set; }
	}
}