﻿using Submissions.Common;
using Submissions.Common.JQGrid;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;
using Trirand.Web.Mvc;

namespace Submissions.Web.Areas.Evolution.Models
{
	[Serializable]
	public class ValidationAnalysisIndexViewModel
	{
		public EvolutionSearchViewModel EvoSearch { get; set; }
		public ValidationAnalysisIndexGridModel ValidationAnalysisGrid { get; set; }
	}

	#region Grid
	public class ValidationAnalysisIndexGridModel
	{
		public ValidationAnalysisIndexGridModel()
		{
			Grid = new JQGrid
			{
				Columns = new List<JQGridColumn>
				{
					new JQGridColumn
					{
						DataField = "ClauseId",
						HeaderText = "Clause ID",
						Width = 60,
						DataType = typeof( string ),
						Visible = true //Hide for public by Jquery
					},
					new JQGridColumn
					{
						DataField = "TestCase",
						HeaderText = "Test Case",
						DataType = typeof(string),
						Width = 180
					},
					new JQGridColumn
					{
						DataField = "Validation",
						HeaderText = "Validation",
						DataType = typeof(string),
						Width = 600
					},
					new JQGridColumn
					{
						DataField = "FuncRoleName",
						HeaderText = "Functional Role",
						DataType = typeof(string),
						Width = 250
					}
				}
			};

			Grid.ID = "ValidationAnalysisGrid";
			Grid.DataUrl = new UrlHelper(((MvcHandler)HttpContext.Current.Handler).RequestContext).Action("LoadGridIndex");
			Grid.AutoWidth = true;
			Grid.Width = Unit.Percentage(100);
			Grid.Height = Unit.Percentage(100);
			Grid.SearchDialogSettings.MultipleSearch = true;
			Grid.ToolBarSettings = new ToolBarSettings
			{
				ShowRefreshButton = true,
				ShowSearchToolBar = true,
				ShowSearchButton = true,
				ToolBarPosition = ToolBarPosition.Top
			};

			Grid.PagerSettings = new Trirand.Web.Mvc.PagerSettings
			{
				PageSize = 20,
				PageSizeOptions = "[]"
			};
			Grid.AppearanceSettings = new AppearanceSettings
			{
				AlternateRowBackground = true,
				HighlightRowsOnHover = true,
				ShowFooter = false
			};
		}

		/// <summary>
		/// Export from grid
		/// </summary>
		public static void Export(ExportType exportType, IQueryable<ValidationAnalysisQuery> query, NameValueCollection queryString,
			Dictionary<string, string> multiFuncRoles)
		{
			var fileName = string.Format("Validation GAP Analysis - {0:yyyy-MM-dd_hh-mm-ss-tt}", DateTime.Now);
			var grid = new ValidationAnalysisIndexGridModel().Grid;

			var jqGridHelper = new JQGridHelper();
			var gridstate = new JQGridState();
			gridstate.QueryString = queryString;

			if (exportType == ExportType.Xls)
			{
				fileName += ".xls";
				grid.ExportSettings.ExportDataRange = ExportDataRange.Filtered;

				// further filter query
				var dataSource = grid.GetFilteredDataSource(query, gridstate);

				DataGrid dataGrid = grid.GetExportGrid();

				// associate grid with query
				IQueryable iqueryable = dataSource as IQueryable;
				if (iqueryable != null)
				{
					dataGrid.DataSource = iqueryable.ToDataTable(grid);
				}
				else
				{
					dataGrid.DataSource = dataSource;
				}

				// run query
				dataGrid.DataBind();

				// add jurisdictions to datatable
				DataGridModifyCols(ref dataGrid, multiFuncRoles);

				// modify "grid" to add jurisdiction info
				jqGridHelper.RenderExcelToStream(dataGrid, fileName);

			}
			else if (exportType == ExportType.Csv)
			{
				fileName += ".csv";
				grid.ExportSettings.ExportDataRange = ExportDataRange.Filtered;
				var exportData = grid.GetExportData(query, gridstate);
				// add jurisdictions to datatable
				DataTableModifyCols(ref exportData, multiFuncRoles);
				jqGridHelper.ExportToCSV(exportData, fileName);
			}
			else if (exportType == ExportType.Pdf)
			{
				fileName += ".pdf";
				grid.ExportSettings.ExportDataRange = ExportDataRange.Filtered;
				var exportData = grid.GetExportData(query, gridstate);
				// add jurisdictions to datatable
				DataTableModifyCols(ref exportData, multiFuncRoles);
				jqGridHelper.ExportToFormatPDF(exportData, fileName);
			}
			else if (exportType == ExportType.Doc)
			{
				fileName += ".doc";
				grid.ExportSettings.ExportDataRange = ExportDataRange.Filtered;
				var exportData = grid.GetExportData(query, gridstate);
				// add jurisdictions to datatable
				DataTableModifyCols(ref exportData, multiFuncRoles);
				jqGridHelper.ExportToWord(exportData, fileName);
			}
		}

		public static void DataGridModifyCols(ref DataGrid dg, Dictionary<string, string> funcRoleDictionary)
		{
			//Get column index
			var gridModel = new ValidationAnalysisIndexGridModel();
			int clauseIdIndex = 0;
			int funcRolesIndex = 0;
			for (int i = 0; i < dg.Columns.Count; i++)
			{
				if (dg.Columns[i].HeaderText == gridModel.ClauseId)
				{
					clauseIdIndex = i;
				}
				if (dg.Columns[i].HeaderText == gridModel.FuncRoleName)
				{
					funcRolesIndex = i;
				}
			}
			//Modify Columns
			if (funcRoleDictionary != null)
			{
				foreach (DataGridItem item in dg.Items)
				{
					if (!String.IsNullOrEmpty(item.Cells[clauseIdIndex].Text))
					{
						item.Cells[funcRolesIndex].Text = funcRoleDictionary[item.Cells[clauseIdIndex].Text];
					}
				}
			}
			//Hidden column Clause ID
			dg.Columns[clauseIdIndex].Visible = false;
		}

		public static void DataTableModifyCols(ref DataTable dt, Dictionary<string, string> funcRoleDictionary)
		{
			//Get column index
			var gridModel = new ValidationAnalysisIndexGridModel();
			var clauseIdCol = dt.Columns[gridModel.ClauseId];
			var funcRolesCol = dt.Columns[gridModel.FuncRoleName];
			int clauseIdIndex = clauseIdCol != null ? clauseIdCol.Ordinal : 0;
			int funcRolesIndex = funcRolesCol != null ? funcRolesCol.Ordinal : 0;
			//Modify Columns
			if (funcRoleDictionary != null)
			{
				foreach (DataRow row in dt.Rows)
				{
					if (!String.IsNullOrEmpty(row[clauseIdIndex].ToString()))
					{
						row.SetField(funcRolesIndex, funcRoleDictionary[row[clauseIdIndex].ToString()].Replace("<br />", "\n"));
					}
				}
			}
			//Hidden column Clause ID
			dt.Columns.RemoveAt(clauseIdIndex);
		}

		public JQGrid Grid { get; set; }

		public string ClauseId = "Clause ID";
		public string FuncRoleName = "Functional Role";
	}
	#endregion

	[Serializable]
	public class ValidationAnalysisJurisdictionModel
	{
		public IEnumerable<string> TestCaseAutocompleteList { get; set; }
		public IEnumerable<string> FuncRoleAutocompleteList { get; set; }
	}

	[Serializable]
	public class ValidationAnalysisAutocomplete
	{
		public IQueryable<string> TestCaseResults { get; set; }
		public IQueryable<string> FuncRoleResults { get; set; }
	}

	[Serializable]
	public class ValidationAnalysisQuery
	{
		public string JuriId { get; set; }
		public string Jurisdiction { get; set; }
		public string Name { get; set; }
		public string Version { get; set; }
		public string ClauseId { get; set; }
		public string Section { get; set; }
		public string TestCase { get; set; }
		public string Memo { get; set; }
		public string Validation { get; set; }
		public string FuncRoleName { get; set; }
	}
}