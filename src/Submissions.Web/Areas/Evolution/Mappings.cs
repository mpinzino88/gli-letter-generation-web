﻿using AutoMapper;
using EvoLogic.Models;
using Submissions.Core.Models.EvolutionService;
using Submissions.Web.Areas.Evolution.Models;
using System.Linq;

namespace Submissions.Web.Mappings
{
	public class EvolutionProfile : Profile
	{
		public EvolutionProfile()
		{
			CreateMap<TestScript, ReviewTestScriptViewModel>()
				.ForMember(dest => dest.Display, opt => opt.Ignore())
				.ForMember(dest => dest.OriginalAnswer, opt => opt.Ignore())
				.ForMember(dest => dest.OriginalNotes, opt => opt.Ignore())
				.ForMember(dest => dest.Modified, opt => opt.Ignore())
				.ForMember(dest => dest.Answer, opt => opt.MapFrom(src => src.AnswerText))
				.ForMember(dest => dest.ShowNote, opt => opt.Ignore())
				.ForMember(dest => dest.ClauseInfo, opt => opt.Ignore())
				.ForMember(dest => dest.TestStepInfo, opt => opt.Ignore())
				.ForMember(dest => dest.JuriList, opt => opt.MapFrom(src => src.JuriList.Split(',').Select(jur => jur.Trim()).ToList()))
				.ForMember(dest => dest.AnswerHistoryInfo, opt => opt.Ignore())
				.ForMember(dest => dest.GpsItemLink, opt => opt.Ignore())
				.ForMember(dest => dest.Visible, opt => opt.Ignore());

			CreateMap<ReviewTestScriptViewModel, AnswerBaseVisibleAndModified>()
				.ForMember(dest => dest.FunctionalRoleIds, opt => opt.Ignore())
				.ForMember(dest => dest.Events, opt => opt.Ignore())
				.ForMember(dest => dest.ComponentLinks, opt => opt.Ignore())
				.ForMember(dest => dest.AnswerJoins, opt => opt.Ignore())
				.ForMember(dest => dest.Question, opt => opt.Ignore())
				.ForMember(dest => dest.Version, opt => opt.Ignore())
				.ForMember(dest => dest.NewToUser, opt => opt.Ignore())
				//.ForMember(dest => dest.AnsweredTime, opt => opt.Ignore())
				.ForMember(dest => dest.CopiedFrom, opt => opt.Ignore())
				.ForMember(dest => dest.SourceDate, opt => opt.Ignore())
				.ForMember(dest => dest.SourceUser, opt => opt.Ignore())
				.ForMember(dest => dest.SourceInstance, opt => opt.Ignore())
				.ForMember(dest => dest.OrderID, opt => opt.Ignore())
				.ForMember(dest => dest.GameDescType, opt => opt.Ignore())
				.ForMember(dest => dest.DocumentIds, opt => opt.Ignore())
				.ForMember(dest => dest.GameDescriptionID, opt => opt.Ignore())
				.ForMember(dest => dest.JurisdictionByte, opt => opt.Ignore())
				.ForMember(dest => dest.SourceAnswerID, opt => opt.Ignore())
				.ForMember(dest => dest.SourceAnswerJoinID, opt => opt.Ignore())
				.ForMember(dest => dest.UntestedFRsByte, opt => opt.Ignore())
				.ForMember(dest => dest.VersionID, opt => opt.Ignore())
				.ForMember(dest => dest.ParentID, opt => opt.MapFrom(src => src.Parent))
				//.ForMember(dest => dest.ParentID, opt => opt.Ignore())
				.ForMember(dest => dest.DocumentByte, opt => opt.Ignore())
				.ForMember(dest => dest.FunctionalRoleByte, opt => opt.Ignore())
				//.ForMember(dest => dest.answer, opt => opt.Ignore())
				//.ForMember(dest => dest.notes, opt => opt.Ignore())
				.ForMember(dest => dest.Type, opt => opt.MapFrom(src => src.AnswerType.ToString()))
				//.ForMember(dest => dest.Type, opt => opt.Ignore())
				.ForMember(dest => dest.Complete, opt => opt.Ignore())
				.ForMember(dest => dest.QuestionID, opt => opt.MapFrom(src => src.Id))
				//.ForMember(dest => dest.QuestionID, opt => opt.Ignore())
				.ForMember(dest => dest.ID, opt => opt.Ignore())
				.ForMember(dest => dest.CoversheetByte, opt => opt.Ignore())
				.ForMember(dest => dest.JurisdictionIds, opt => opt.Ignore());
			//CreateMap<ClauseViewModel, Clause>()
			//	.ForMember(dest => dest.ClauseDocuments, opt => opt.Ignore())
			//	.ForMember(dest => dest.ClauseFuncRoles, opt => opt.Ignore())
			//	.ForMember(dest => dest.ClauseJuris, opt => opt.Ignore())
			//	.ForMember(dest => dest.ClauseJurisDates, opt => opt.Ignore())
			//	.ForMember(dest => dest.ClauseQuestions, opt => opt.Ignore())
			//	.ForMember(dest => dest.TestCaseEvents, opt => opt.Ignore());
			//CreateMap<Clause, ClauseViewModel>()
			//	.ForMember(dest => dest.Jurisdictions, opt => opt
			//		.MapFrom(x => x.ClauseJuris.Select(y => new JurisdictionViewModel
			//		{
			//			Id = y.JuriID,
			//			Jurisdiction = ((CustomJurisdiction)y.JuriID).GetEnumDescription()
			//		}).ToList()))
			//	.ForMember(dest => dest.Documents, opt => opt
			//		.MapFrom(x => x.ClauseDocuments.Select(y => new LinkedDocsViewModel
			//		{
			//			ID = y.DocumentId,
			//			Document = y.Document.Name,
			//			Version = y.Document.Version
			//		}).ToList()));
			//CreateMap<ClauseViewModel, ClauseModel>()
			//	.ForMember(dest => dest.TestCaseEvents, opt => opt.Ignore());
			//CreateMap<ClauseModel, ClauseViewModel>()
			//	.ForMember(dest => dest.ClauseDate, opt => opt.Ignore())
			//	.ForMember(dest => dest.StateDate, opt => opt.Ignore())
			//	.ForMember(dest => dest.InactiveDate, opt => opt.Ignore())
			//	.ForMember(dest => dest.Jurisdictions, opt => opt.Ignore())
			//	.ForMember(dest => dest.Documents, opt => opt.Ignore());
			//CreateMap<Clause, ClauseEditViewModel>();
		}
	}
}