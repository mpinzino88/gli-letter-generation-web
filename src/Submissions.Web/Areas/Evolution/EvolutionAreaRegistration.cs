﻿using System.Web.Mvc;

namespace Submissions.Web.Areas.Evolution
{
	public class EvolutionAreaRegistration : AreaRegistration
	{
		public override string AreaName
		{
			get
			{
				return "Evolution";
			}
		}

		public override void RegisterArea(AreaRegistrationContext context)
		{
			context.MapRoute(
				"Evolution_default",
				"Evolution/{controller}/{action}/{id}",
				new { controller = "Evolution", action = "Index", id = UrlParameter.Optional }
			);
		}
	}
}