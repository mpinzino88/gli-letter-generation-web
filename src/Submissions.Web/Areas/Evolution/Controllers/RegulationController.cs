﻿using Newtonsoft.Json;
using Submissions.Core;
using Submissions.Core.Models.EvolutionService.Query;
using Submissions.Web.Areas.Evolution.Models;
using System.Linq;
using System.Web.Mvc;

namespace Submissions.Web.Areas.Evolution.Controllers
{
	public class RegulationController : Controller
	{
		private readonly IEvolutionService _evolutionService;

		public RegulationController(IEvolutionService evolutionService)
		{
			_evolutionService = evolutionService;
		}

		public ActionResult Index()
		{
			var vm = new RegulationIndexViewModel();
			return View(vm);
		}

		public ActionResult RegulationSearch(RegulationIndexViewModel model)
		{
			var vm = new RegulationDisplayViewModel { Filters = model.Filters };
			vm.Filters.SearchType = EvoSearchType.RegulatorySearch;
			vm.Filters.JurisdictionIdsToBeTested = model.Filters.JurisdictionIdsToBeSearched;

			if (Request.Form["exportbutton1"] != null)
			{
				return File(new System.Text.UTF8Encoding().GetBytes(GetExportJson(model.Filters)), "application/json", "RegsExport.json");
			}

			return View("Display", vm);
		}

		public ActionResult RegulationCompare(RegulationIndexViewModel model)
		{
			var vm = new RegulationDisplayViewModel { Filters = model.Filters };
			vm.Filters.SearchType = EvoSearchType.RegulatoryCompare;

			if (Request.Form["exportbutton2"] != null)
			{
				return File(new System.Text.UTF8Encoding().GetBytes(GetExportJson(model.Filters)), "application/json", "RegsExport.json");
			}

			return View("Display", vm);
		}

		private string GetExportJson(RegulationSearch filters)
		{
			return JsonConvert.SerializeObject(_evolutionService.RegulationSearch(filters).ToList());
		}
	}
}
