﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using AutoMapper;
using EF.eResultsManaged;
using JiraLibrary;
using Newtonsoft.Json;
using Submissions.Common;
using Submissions.Common.Email;
using Submissions.Core;
using Submissions.Core.Models.EvolutionService;
using Submissions.Core.Models.EvoService;
using Submissions.Web.Areas.Evolution.Models;
using EF.Submission;
using System.Net.Mail;
using EvoLogic.Models;
namespace Submissions.Web.Areas.Evolution.Controllers
{
	public class TCMTestScriptController : Controller
	{

		private readonly IEvolutionService _evolutionService;
		private readonly IeResultsManagedContext _dbEresultsManaged;
		private readonly JiraMethods _jiraMethods;
		private readonly IEmailService _emailService;
		private readonly IUserContext _userContext;
		private readonly ISubmissionContext _dbSubmission;
		private readonly ITestCaseManager _testCaseManager;

		public TCMTestScriptController(IEvolutionService evolutionService, IeResultsManagedContext dbEResultsManaged, JiraMethods jiraMethods,
			Common.Email.IEmailService emailService, IUserContext userContext, ISubmissionContext dbSubmission, ITestCaseManager testCaseManager)
		{
			_evolutionService = evolutionService;
			_dbEresultsManaged = dbEResultsManaged;
			_jiraMethods = jiraMethods;
			_emailService = emailService;
			_userContext = userContext;
			_dbSubmission = dbSubmission;
			_testCaseManager = testCaseManager;
		}

		public ActionResult Index()
		{
			var vm = new TestScriptEditViewModel();
			return View(vm);
		}

		[HttpPost]
		public ActionResult LoadTableOfContentsForScripts()
		{
			var result =_testCaseManager.GetTableOfContentsTree();
			return Content(ComUtil.JsonEncodeCamelCase(result), "application/json");
		}

		[HttpPost]
		public ActionResult SaveTestScriptTableOfContents(string modifiedToC_String, List<TableOfContentModel> marshaledToCModification)
		{

			var result = true;
			var answer = _testCaseManager.SaveTestScriptTableOfContents(marshaledToCModification);



			return Content(ComUtil.JsonEncodeCamelCase(result), "application/json");
		}
		[HttpPost]
		public ActionResult LoadTestScriptQuestions(List<Guid> rootTestCaseIds)
		{
			//var testScripts = _evolutionService.GetTestScriptsQuestionsFromSproc(rootTestCaseIds[0]);
			//var result = testScripts;
			//var convertedTestScripts = Mapper.Map<IEnumerable<ReviewTestScriptViewModel>>(testScripts);
			var result = _testCaseManager.GetTestScriptQuestionsFromSproc(rootTestCaseIds);
			//var result = _testCaseManager.GetTestScriptQuestions(rootTestCaseIds);
			return Content(ComUtil.JsonEncodeCamelCase(result), "application/json");
		}
	}
}