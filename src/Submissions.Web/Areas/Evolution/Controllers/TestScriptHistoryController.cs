﻿using Newtonsoft.Json;
using Submissions.Core;
using Submissions.Core.Models.EvolutionService;
using Submissions.Web.Areas.Evolution.Models;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace Submissions.Web.Areas.Evolution.Controllers
{
	public class TestScriptHistoryController : Controller
	{
		private readonly IEvolutionService _evolutionService;

		public TestScriptHistoryController(IEvolutionService evolutionService)
		{
			_evolutionService = evolutionService;
		}

		public ActionResult Index()
		{
			var vm = new TestScriptHistoryIndexViewModel();
			return View(vm);
		}

		public ActionResult ContentSearch(TestScriptHistoryIndexViewModel model)
		{
			var vm = new TestScriptHistoryIndexViewModel();
			vm.Filters = model.Filters;
			return View("Index", vm);
		}

		public JsonResult ChangeSetUpdate(bool isMajor, string testScriptFilters)
		{
			var filters = JsonConvert.DeserializeObject<Core.Models.EvolutionService.Query.TestScriptHistorySearch>(testScriptFilters);
			filters.DraftVersion = true;
			_evolutionService.UpgradeNevVersion(filters, isMajor);
			var newVersion = string.Join(".", _evolutionService.CurrentVersion(filters.TestCase));
			return Json(newVersion, JsonRequestBehavior.AllowGet);
		}
		[HttpGet]
		public JsonResult NevTestCaseOptions(string testScriptFilters)
		{
			var filters = JsonConvert.DeserializeObject<Core.Models.EvolutionService.Query.TestScriptHistorySearch>(testScriptFilters);

			return Json(_evolutionService.NevadaChangeTestCases(filters).ToList(), JsonRequestBehavior.AllowGet);
		}

		[HttpGet]
		public JsonResult GetCurrentTestCaseVerison(string testCaseName)
		{
			var version = _evolutionService.CurrentVersion(testCaseName);
			return Json(string.Join(".", version), JsonRequestBehavior.AllowGet);
		}

		[HttpGet]
		public JsonResult GetUnfilteredTestCaseNames()
		{
			return Json(_evolutionService.GetTestCaseNames(), JsonRequestBehavior.AllowGet);
		}

		[HttpGet]
		public JsonResult GetVersionsForApprovalOrReject(string testCaseName)
		{
			var versions = _evolutionService.GetTestScriptVersions(testCaseName);
			return Json(versions, JsonRequestBehavior.AllowGet);
		}

		public ActionResult ApproveRejectOptions(string testCaseName)
		{
			var vm = new ApproveRejectOptionsViewModel();

			if (!string.IsNullOrEmpty(testCaseName))
			{
				vm.RevisionVersions = _evolutionService.GetTestScriptVersions(testCaseName).Select(x => new TCVerisonViewModel
				{
					ApprovalSetDate = null,
					ApprovedOn = x.ApprovedOn,
					CreatedBy = x.CreatedBy,
					CreatedOn = x.CreatedOn,
					Draft = x.Draft,
					ID = x.ID,
					Jurisdiction = x.Jurisdiction,
					RejectedOn = x.RejectedOn,
					RejectSetDate = null,
					State = x.State,
					TestCaseID = x.TestCaseID,
					Version = x.Version
				}).ToList();
				vm.TestCase = testCaseName;
			}

			return View(vm);
		}

		public JsonResult ConfirmSelections(List<ApproveOrRejectObject> selectedOptions)
		{
			return Json(_evolutionService.ProcessOptions(selectedOptions), JsonRequestBehavior.AllowGet);
		}
	}
}
