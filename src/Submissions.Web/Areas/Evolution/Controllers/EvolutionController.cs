﻿
using EF.eResultsManaged;
using Submissions.Common;
using Submissions.Core;
using System;
using System.Linq;
using System.Web.Mvc;

namespace Submissions.Web.Areas.Evolution.Controllers
{
	public class EvolutionController : Controller
	{
		private readonly IEvolutionService _evolutionService;
		private readonly IeResultsManagedContext _dbEresultsManaged;

		public EvolutionController(IEvolutionService evolutionService, IeResultsManagedContext dbEResultsManaged)
		{
			_evolutionService = evolutionService;
			_dbEresultsManaged = dbEResultsManaged;
		}

		[HttpGet]
		public ActionResult LoadInstances(string projectName)
		{
			var result = _evolutionService.GetInstances(projectName.Trim());
			return Content(ComUtil.JsonEncodeCamelCase(result), "application/json");
		}

		[HttpGet]
		public ActionResult LoadComponentGroupJurisdictions(string componentGroupId)
		{
			var componentId = new Guid(componentGroupId);
			var result = _evolutionService.GetComponentGroupJurisdictions(componentId);
			return Content(ComUtil.JsonEncodeCamelCase(result), "application/json");
		}

		[HttpGet]
		public ActionResult LoadComponentGroupDocuments(string componentGroupId)
		{
			var componentId = new Guid(componentGroupId);
			return Content(ComUtil.JsonEncodeCamelCase(componentId), "application/json");
		}

		[HttpPost]
		public ActionResult LoadFunctionalRoles(string componentGroupId)
		{
			var componentId = new Guid(componentGroupId);
			var functionalRoles = _evolutionService.GetFunctionalRoles(componentId);
			var result = _dbEresultsManaged.FunctionalRole.Where(x => functionalRoles.Contains(x.Id)).Select(x => new { Id = x.Id, Name = x.Name, Description = x.Description }).ToList();
			return Content(ComUtil.JsonEncodeCamelCase(result), "application/json");
		}
	}
}
