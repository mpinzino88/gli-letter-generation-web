﻿using EF.eResultsManaged;
using EF.Submission;
using EvoLogic.Models;
using Submissions.Web.Areas.Evolution.Models;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace Submissions.Web.Areas.Evolution.Controllers
{
	public class TCMDocumentController : Controller
	{
		private readonly ISubmissionContext _dbSubmission;
		private readonly IeResultsManagedContext _dbeResultsManaged;

		public TCMDocumentController(ISubmissionContext dbSubmission, IeResultsManagedContext dbeResultsManaged)
		{
			_dbSubmission = dbSubmission;
			_dbeResultsManaged = dbeResultsManaged;
		}

		public ActionResult Index()
		{
			var vm = new DocumentIndexViewModel();

			vm.DocumentSearch = new DocumentSearch();
			vm.DocumentSearch.JurisdictionList = _dbSubmission.tbl_lst_fileJuris
				.OrderBy(x => x.Name)
				.Select(x => new SelectListItem
				{
					Text = x.Name + " (" + x.Id.ToString() + ")",
					Value = x.Id.ToString()
				})
				.ToList();

			return View(vm);
		}

		public ActionResult Edit(int? id)
		{
			var vm = new DocumentEditViewModel();
			vm.Id = id;
			if (id.HasValue)
			{
				// set name and version
			}

			vm.JurisdictionList = _dbSubmission.tbl_lst_fileJuris
				.OrderBy(x => x.Name)
				.Select(x => new SelectListItem
				{
					Text = x.Name + " (" + x.Id.ToString() + ")",
					Value = x.Id.ToString()
				})
				.ToList();

			return View(vm);
		}

		public ActionResult Edit(DocumentEditViewModel documentEditViewModel)
		{
			return View();
		}

		[HttpPost]
		public JsonResult Search(DocumentSearch documentSearch)
		{
			var evoSearchModel = new DocumentSearchModel()
			{
				resultLimit = 600,
				exportToExcel = false,
				SearchTerm = documentSearch.Name,
				Inactive = null,
				LinkedJurisdictions = documentSearch.JurisdictionIds
			};

			if (documentSearch.Status.Count == 1)
			{
				if (documentSearch.Status.First().Equals("Active"))
				{
					evoSearchModel.Inactive = false;
				}
				else
				{
					evoSearchModel.Inactive = true;
				}
			}

			var documentMethods = new EvoLogic.GatherData.DocumentMethods(_dbeResultsManaged);
			var results = AutoMapper.Mapper.Map<List<Models.Document>>(documentMethods.SearchDocuments(evoSearchModel));

			return Json(results);
		}
	}
}
//Jurisdictions
//Status
//FunctionalRoles