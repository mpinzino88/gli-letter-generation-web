﻿using AutoMapper;
using EF.eResultsManaged;
using EF.Submission;
using EvoLogic.Models;
using JiraLibrary;
using Newtonsoft.Json;
using Submissions.Common;
using Submissions.Common.Email;
using Submissions.Core;
using Submissions.Core.Models.EvolutionService;
using Submissions.Core.Models.EvoService;
using Submissions.Web.Areas.Evolution.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Web.Mvc;

namespace Submissions.Web.Areas.Evolution.Controllers
{
	// Due to a class of elmah error handling is done with that. http://submissions/gliintranet/elmah or locally http://localhost:63298/elmah
	public class ReviewController : Controller
	{
		private readonly IEvolutionService _evolutionService;
		private readonly IeResultsManagedContext _dbEresultsManaged;
		private readonly JiraMethods _jiraMethods;
		private readonly IEmailService _emailService;
		private readonly IUserContext _userContext;
		private readonly ISubmissionContext _dbSubmission;

		public ReviewController(IEvolutionService evolutionService, IeResultsManagedContext dbEResultsManaged, JiraMethods jiraMethods,
			Common.Email.IEmailService emailService, IUserContext userContext, ISubmissionContext dbSubmission)
		{
			_evolutionService = evolutionService;
			_dbEresultsManaged = dbEResultsManaged;
			_jiraMethods = jiraMethods;
			_emailService = emailService;
			_userContext = userContext;
			_dbSubmission = dbSubmission;
		}

		public ActionResult Index()
		{
			var vm = new ReviewIndexViewModel();
			vm.ReviewerEmail = _userContext.User.Email;
			vm.UserName = _userContext.User.Username;
			vm.Office = _userContext.Location;
			return View(vm);
		}

		[HttpGet]
		public ActionResult LoadInstances(string projectName)
		{
			var result = _evolutionService.GetInstances(projectName.Trim());

			return Content(ComUtil.JsonEncodeCamelCase(result), "application/json");
		}

		[HttpGet]
		public ActionResult LoadComponentForInstance(string instanceId)
		{
			var result = _evolutionService.GetActiveComponentGroupsFromInstanceId(new Guid(instanceId));
			return Content(ComUtil.JsonEncodeCamelCase(result), "application/json");
		}

		[HttpGet]
		public ActionResult LoadComponentGroupFromId(string componentGroupId)
		{
			var lib = new EvoLogic.GatherData.InstanceCompGroupMethods(_dbEresultsManaged, _dbSubmission);
			var instanceCompGroup = lib.GetCompGroupFromCompGroupId(new Guid(componentGroupId)).FirstOrDefault();
			var result = _evolutionService.GetActiveComponentGroupsFromInstanceId(instanceCompGroup.InstanceID);

			return Content(ComUtil.JsonEncodeCamelCase(result), "application/json");
		}

		[HttpPost]
		public ActionResult LoadClauseReport(Guid componentGroupId, string[] jurisdictionIds, string[] documentIds, string nativeLanguage)
		{
			var nevadaJurisdictions = new List<string>
			{
				((int)CustomJurisdiction.Nevada).ToJurisdictionIdString(),
				((int)CustomJurisdiction.NevadaITL).ToJurisdictionIdString(),
				((int)CustomJurisdiction.NevadaOnline).ToJurisdictionIdString(),
				((int)CustomJurisdiction.NevadaAudit).ToJurisdictionIdString()
			};

			var result = new ClauseReportViewModel();
			result.hasNevada = nevadaJurisdictions.Intersect(jurisdictionIds).Any();
			result.hasPeru = jurisdictionIds.Contains(((int)CustomJurisdiction.Peru).ToJurisdictionIdString());

			var jurisdictionIdsParam = jurisdictionIds.ToList();
			var documentIdInt = documentIds == null ? new List<string>() : documentIds.ToList();

			var clauseReport = _evolutionService.GetClauseReports(componentGroupId, jurisdictionIdsParam, documentIdInt, nativeLanguage == "0" || nativeLanguage == "" ? 0 : 1);

			//Nevada Summary use only
			result.ClauseDocumentTestCase = _evolutionService.GetClauseDocumentTestCase(componentGroupId);
			result.DistinctDocs = result.ClauseDocumentTestCase.Where(x => x.DocumentName != null).GroupBy(x => x.DocumentName).Select(y => y.First()).ToList();
			var sortedDocName = new List<ClauseDocumentSummary>();
			var nevadaSummary = new List<ClauseDocumentSummary>();
			if (result.hasNevada)
			{
				foreach (ClauseDocumentTestCase docs in result.DistinctDocs)
				{
					var docSection = result.ClauseDocumentTestCase.Where(x => x.DocumentName == docs.DocumentName)
					.Select(x => new ClauseSummary
					{
						Section = x.Section,
						SectionName = x.SectionName
					}).ToList();

					nevadaSummary.Add(new ClauseDocumentSummary { DocumentName = docs.DocumentName, SectionSummary = docSection });
				}
			}
			sortedDocName = nevadaSummary.OrderBy(x => x.DocumentName).ToList();

			//Applicable Test Cases
			var sortedApplicableTestCases = new List<ApplicableTestCase>();
			if (result.hasNevada)
			{
				IList<ClauseDocumentTestCase> distinctApplicableTestCases = result.ClauseDocumentTestCase.Where(x => x.TCName != null).GroupBy(x => x.TCName).Select(y => y.First()).ToList();
				var appTestCases = new List<ApplicableTestCase>();
				foreach (var tc in distinctApplicableTestCases)
				{
					appTestCases.Add(new ApplicableTestCase { TCName = tc.TCName, TCVersion = tc.TCVersion });
				}
				sortedApplicableTestCases = appTestCases.OrderBy(x => x.TCName).ToList();
			}

			if (result.hasPeru)
			{
				IList<string> peruApplicableTestCases = _evolutionService.GetPeruClauseDocumentTestCase(componentGroupId, nativeLanguage == "0" || nativeLanguage == "" ? 0 : 1);
				var appTestCases = new List<ApplicableTestCase>();
				foreach (var tc in peruApplicableTestCases)
				{
					appTestCases.Add(new ApplicableTestCase { TCName = tc, TCVersion = "" });
				}
				sortedApplicableTestCases = appTestCases.OrderBy(x => x.TCName).ToList();
			}

			result.ClauseReports = clauseReport;
			result.SummarySections = sortedDocName;
			result.ApplicableTestCases = sortedApplicableTestCases;
			result.PassCount = clauseReport.Count(x => x.Answer == "PASS");
			result.FailCount = clauseReport.Count(x => x.Answer == "FAIL");
			result.NACount = clauseReport.Count(x => (x.Answer == "N/A"));
			result.NAStarCount = clauseReport.Count(x => x.Answer == "N/A*");

			return Content(ComUtil.JsonEncodeCamelCase(result), "application/json");
		}


		[HttpPost]
		public ActionResult LoadClauseForJurisdictionsAndQuestion(string componentGroupId, string jurisdictionId, string question)
		{
			var cgID = new Guid(componentGroupId);
			var result = _evolutionService.GetClausesForJurisdictionAndQuestion(cgID, jurisdictionId, question);
			return Content(ComUtil.JsonEncodeCamelCase(result), "application/json");
		}

		[HttpPost]
		public ActionResult SendAnswerHistoryEmail(string subject, string messageBody, List<int> emailToList, List<int> emailCcList)
		{
			var mail = new MailMessage();
			mail.From = new MailAddress(_userContext.User.Email);
			mail.Subject = subject;
			mail.Body = messageBody;


			if (emailToList != null)
			{
				var emails = _dbSubmission.trLogins.Where(x => emailToList.Contains(x.Id)).Select(x => x.Email).ToList();

				foreach (var email in emails)
				{
					mail.To.Add(email);
				}
			}

			if (emailCcList != null)
			{
				var emails = _dbSubmission.trLogins.Where(x => emailCcList.Contains(x.Id)).Select(x => x.Email).ToList();
				foreach (var email in emails)
				{
					mail.CC.Add(email);
				}
			}

			var result = _emailService.Send(mail);
			return Content(ComUtil.JsonEncodeCamelCase(result), "application/json");
		}

		[HttpGet]
		public ActionResult LoadComponentGroupJurisdictions(string componentGroupId)
		{
			var componentId = new Guid(componentGroupId);
			var jurisdictions = _evolutionService.GetComponentGroupJurisdictions(componentId);
			var result = jurisdictions.Select(x => x.ToJurisdictionIdString()).ToList();

			return Content(ComUtil.JsonEncodeCamelCase(result), "application/json");
		}

		[HttpGet]
		public ActionResult LoadComponentGroupDocuments(string componentGroupId)
		{
			var componentId = new Guid(componentGroupId);

			return Content(ComUtil.JsonEncodeCamelCase(componentId), "application/json");
		}

		[HttpGet]
		public ActionResult LoadEvolutionDocuments(string evoDoc)
		{
			var nativeCompGroupJur = JsonConvert.DeserializeObject<NativeCompGroupJur>(evoDoc);
			var relevantJurDocuments = _evolutionService.GetDocsCompGroupNativeLanguage(nativeCompGroupJur);
			var result = new List<SelectListItem>();
			if (nativeCompGroupJur.nativeLanguage != null && nativeCompGroupJur.nativeLanguage != "")
			{
				result = relevantJurDocuments.Where(x => x.LanguageString == nativeCompGroupJur.nativeLanguage)
				.OrderBy(x => x.DocumentName)
				.Select(x => new SelectListItem { Value = x.DocID.ToString(), Text = x.DocumentName + " (" + x.Version + ")", Selected = true })
				.ToList();
			}
			return Content(ComUtil.JsonEncodeCamelCase(result), "application/json");
		}

		[HttpPost]
		public ActionResult LoadFunctionalRoles(string componentGroupId)
		{
			var componentId = new Guid(componentGroupId);
			var functionalRoles = _evolutionService.GetFunctionalRoles(componentId);
			var result = _dbEresultsManaged.FunctionalRole.Where(x => functionalRoles.Contains(x.Id)).Select(x => new { Id = x.Id, Name = x.Name, Description = x.Description }).ToList();

			return Content(ComUtil.JsonEncodeCamelCase(result), "application/json");
		}

		[HttpPost]
		public ActionResult LoadAnswerHistory(string componentGroupId, string question)
		{
			var result = _evolutionService.GetAnswerHistory(componentGroupId, question);
			return Content(ComUtil.JsonEncodeCamelCase(result), "application/json");
		}

		[HttpPost]
		public ActionResult LoadTestStep(string componentGroupId, string question)
		{
			var result = _evolutionService.GetTestStep(componentGroupId, question);
			return Content(ComUtil.JsonEncodeCamelCase(result), "application/json");
		}

		[HttpPost]
		public ActionResult CompleteReview(Guid compGroupId, string userName, string officeCode)
		{
			string result = "true";
			_evolutionService.CompleteReview(compGroupId, userName, officeCode);
			return Content(ComUtil.JsonEncodeCamelCase(result), "application/json");
		}

		[HttpPost]
		public void UnlockReview(Guid compGroupId, string userName, string officeCode)
		{
			_evolutionService.UnlockReview(compGroupId, userName, officeCode);
		}

		[HttpPost]
		public ActionResult CheckUserDepartment(string userName)
		{
			var user = _dbSubmission.trLogins.Where(x => x.Username == userName).FirstOrDefault();

			if (user != null && (user.DepartmentId == 5 || user.DepartmentId == 4))
			{
				return Content(ComUtil.JsonEncodeCamelCase(true), "application/json");
			}
			else
			{
				return Content(ComUtil.JsonEncodeCamelCase(false), "application/json");
			}
		}

		[HttpPost]
		public ActionResult CheckJurisdictionClosedStatus(Guid compGroupId, List<string> juriIds)
		{
			var result = _evolutionService.CheckJurisdictionClosedStatus(compGroupId, juriIds);
			return Content(ComUtil.JsonEncodeCamelCase(result), "application/json");
		}

		[HttpPost]
		public ActionResult LoadGPSData(List<string> GPSIds)
		{
			var result = _evolutionService.LoadGPSData(GPSIds);
			return Content(ComUtil.JsonEncodeCamelCase(result), "application/json");
		}

		[HttpPost]
		public ActionResult LoadTestCase(string componentGroupId, string clause)
		{
			var result = _evolutionService.GetClauseTestCase(componentGroupId, clause);

			return Content(ComUtil.JsonEncodeCamelCase(result), "application/json");
		}

		[HttpPost]
		public ActionResult SaveAnswers(Guid compGroupId, List<ReviewTestScriptViewModel> testScripts, string userName, string officeCode)
		{
			string result = "true";
			try
			{
				if (testScripts != null && testScripts.Count() > 0)
				{
					var answerBaseMod = Mapper.Map<IEnumerable<AnswerBaseVisibleAndModified>>(testScripts).ToList();
					_evolutionService.SaveAnswers(compGroupId, answerBaseMod, userName, officeCode);
				}
				else
				{
					result = "No answers were modified";
				}

			}
			catch (Exception ex)
			{
				return Content(ComUtil.JsonEncodeCamelCase(ex.Message), "application/json");
			}


			return Content(ComUtil.JsonEncodeCamelCase(result), "application/json");

		}

		[HttpPost]
		public ActionResult LoadTestScripts(Guid componentGroupId, string[] jurisdictionIds)
		{
			string jurisdictionIdInt = null;
			if (jurisdictionIds != null)
			{
				jurisdictionIdInt = string.Join(",", jurisdictionIds);
				if (jurisdictionIdInt.Length == 0)
				{
					jurisdictionIdInt = null;
				}
			}

			var testScriptFilter = new TestScriptFilter { jurisdictionIds = jurisdictionIdInt };

			var testScripts = _evolutionService.GetTestScripts(componentGroupId, testScriptFilter);

			var convertedTestScripts = Mapper.Map<IEnumerable<ReviewTestScriptViewModel>>(testScripts);
			//Setting the originalAnswer to the Answer so that can check originalAnswer against the new answer if needed.
			//Setting the Visible based on the parent found.
			foreach (var ts in convertedTestScripts)
			{
				ts.OriginalAnswer = ts.Answer;
				ts.OriginalNotes = ts.Notes;
				ts.ShowNote = (ts.Notes != null && ts.Notes != "");
				var parent = convertedTestScripts.FirstOrDefault(x => x.Id == ts.Source);
				if (parent == null)
				{
					ts.Visible = true;
				}
				else
				{
					if (ts.QuestionType == ((int)EvolutionQuestionType.TestStepQuestion).ToString())
					{
						ts.Visible = true;
					}
					else
					{
						ts.Visible = (ts.Evaluation == parent.Answer);
					}
					if (parent.QuestionType == ((int)EvolutionQuestionType.TestStepQuestion).ToString())
					{
						ts.Visible = true;
					}
				}
			}

			//Checking to see if the Jiras attached to any of the testScripts (questions/Answers) are open
			var jiraIssues = convertedTestScripts.Where(x => x.JiraKeyList != null && x.Visible == true).Select(jkl => jkl.JiraKeyList).Distinct().ToList();

			string keyFilter = null;
			var openJiraCount = 0;
			var contribUsers = convertedTestScripts.Select(x => x.AnsweredBy).Distinct().ToList();
			if (jiraIssues.Count > 0)
			{
				var filterList = new List<JiraLibrary.Filter>() //jql: (status != "Closed")
				{
					new JiraLibrary.Filter()
					{
						filterType = FilterTypes.Status,
						filterOperator = FilterOpterators.NotEquals,
						filterValue = "Closed",
						keyword = Keywords.NONE
					}
				};

				keyFilter = "(key=";
				keyFilter += string.Join(" OR key=", jiraIssues);
				keyFilter += ")";

				openJiraCount = _jiraMethods.countIssues(filterList, new List<string> { keyFilter });
			}

			var testCases = convertedTestScripts.GroupBy(x => x.Name)
				.Select(x => new ReviewTestCaseViewModel
				{
					Name = x.Key,
					Display = true,
					PassCount = x.Count(y => y.Answer == "PASS" && y.Visible == true),
					FailCount = x.Count(y => y.Answer == "FAIL" && y.Visible == true),
					NACount = x.Count(y => y.Answer == "NA" && y.Visible == true),
					DoneCount = x.Count(y => (y.Answer != null || y.QuestionType == ((int)EvolutionQuestionType.TestStepQuestion).ToString()) && y.Visible == true),
					DoneTotalCount = x.Count(y => y.Visible == true),
					JiraCount = x.Count(y => y.JiraKeyList != null && y.Visible == true)
				})
				.OrderBy(x => x.TestCaseOrder)
				.ToList();

			var result = new
			{
				TestCases = testCases,
				TestScripts = convertedTestScripts,
				testScriptTotalCount = convertedTestScripts.Count(x => x.Visible == true && x.QuestionType != ((int)EvolutionQuestionType.TestStepQuestion).ToString()),
				testScriptPassCount = convertedTestScripts.Count(x => x.Answer == "PASS" && x.Visible == true),
				testScriptFailCount = convertedTestScripts.Count(x => x.Answer == "FAIL" && x.Visible == true),
				testScriptNaCount = convertedTestScripts.Count(x => x.Answer == "NA" && x.Visible == true),
				testScriptConditionalCount = convertedTestScripts.Count(x => x.QuestionType == ((int)EvolutionQuestionType.Conditional).ToString() && x.Visible == true
				&& x.Answer != null),
				testScriptOpenJiraCount = openJiraCount,
				contributingUsers = contribUsers
			};



			return Content(ComUtil.JsonEncodeCamelCase(result), "application/json");
		}

	}
}
