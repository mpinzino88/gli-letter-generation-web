﻿using Submissions.Web.Areas.Evolution.Models;
using System.Web.Mvc;

namespace Submissions.Web.Areas.Evolution.Controllers
{
	public class ClauseReportController : Controller
	{
		public ActionResult Index(string projectName)
		{
			var vm = new ClauseReportIndexViewModel();
			vm.Filters.NativeLanguageIds = new string[] { "English" };
			return View(vm);
		}
	}
}