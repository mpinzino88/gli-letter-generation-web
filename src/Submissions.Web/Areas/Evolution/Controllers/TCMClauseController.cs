﻿using EF.eResultsManaged;
using EF.Submission;
using EvoLogic;
using EvoLogic.Models;
using Newtonsoft.Json;
using Submissions.Common;
using Submissions.Core;
using Submissions.Web.Areas.Evolution.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace Submissions.Web.Areas.Evolution.Controllers
{
	public class TCMClauseController : Controller
	{
		private readonly IeResultsManagedContext _dbeResultsManaged;
		private readonly ISubmissionContext _dbSubmission;
		private readonly IUserContext _userContext;
		public TCMClauseController(IeResultsManagedContext dbeResultsManaged, ISubmissionContext dbSubmission, IUserContext userContext)
		{
			_dbeResultsManaged = dbeResultsManaged;
			_dbSubmission = dbSubmission;
			_userContext = userContext;
		}

		public ActionResult Index()
		{
			var vm = new ClauseIndexViewModel();
			return View(vm);
		}

		public ActionResult Edit(string id, Mode mode)
		{
			var vm = new ClauseEditViewModel
			{
				Mode = mode
			};
			if (mode == Mode.Add)
			{
				vm.ClauseState = EvoLogic.Constants.State.PreRelease.ToString();
				//vm.ClauseTestingTypeId = ?
			}
			else
			{
				var methods = new EvoLogic.GatherData.ClauseMethods(_dbeResultsManaged);
				var clause = methods.GetClauseFromNormalizedId(id);
				vm = MapFromClause(clause);
			}
			return View(vm);
		}

		[HttpPost]
		public ActionResult Edit(string clauseEditViewModel)
		{
			var vm = JsonConvert.DeserializeObject<ClauseEditViewModel>(clauseEditViewModel);
			var clauseMethods = new EvoLogic.SaveData.ClauseMethods(_dbeResultsManaged);
			var cdJoinMethods = new EvoLogic.SaveData.ClauseDocumentJoinMethods(_dbeResultsManaged);
			var cfJoinMethods = new EvoLogic.SaveData.ClauseFuncRoleMethods(_dbeResultsManaged);
			var cjJoinMethods = new EvoLogic.SaveData.ClauseJuriMethods(_dbeResultsManaged);
			//var cjDateJoinMethods = new EvoLogic.SaveData.ClauseJuriDateMethods(_dbeResultsManaged);
			var cvJoinMethods = new EvoLogic.SaveData.ClauseValidationJoinMethods(_dbeResultsManaged);

			Clause clause;
			if (vm.Id == Guid.Empty)
			{
				clause = new Clause();
			}
			else
			{
				clause = _dbeResultsManaged.Clause.Find(vm.Id);
			}

			clause.Category = !string.IsNullOrWhiteSpace(vm.ClauseTestingType) ? (short)(int)Enum.Parse(typeof(EvoLogic.Constants.Category), vm.ClauseTestingType) : (short)0;
			clause.State = !string.IsNullOrWhiteSpace(vm.ClauseState) ? (short)(int)Enum.Parse(typeof(EvoLogic.Constants.State), vm.ClauseState) : (short)0;
			clause.Language = vm.ClauseNativeLanguage;
			clause.ClauseDate = vm.OriginalClauseDate;
			clause.Memo = vm.ClauseText_English;
			clause.NativeMemo = vm.ClauseNativeText;
			clause.SectionName = vm.SectionName;
			clause.Section = vm.SectionId;
			clause.NativeSectionName = vm.ClauseNativeSectionName;
			clause.SpecialNote = vm.ClauseSpecificTestingNotes;
			clause.NormalizedId = vm.NormalizedId;

			clause = clauseMethods.SaveClause(clause);
			foreach (var docId in vm.DocumentIds)
			{
				cdJoinMethods.SaveClauseDocumentJoin(clause.Id, docId, _userContext.User.Username);
			}
			foreach (var jurId in vm.JurisdictionIds)
			{
				cjJoinMethods.SaveClauseJuri(clause.Id, jurId, _userContext.User.Username);
			}
			foreach (var questionId in vm.QuestionIds)
			{
				cvJoinMethods.SaveClauseValidationJoin(clause.Id, questionId, _userContext.User.Username);
			}
			foreach (var roleId in vm.RoleIds)
			{
				cfJoinMethods.SaveClauseFuncRoleJoin(clause.Id, (short)roleId, _userContext.User.Username);
			}
			_dbeResultsManaged.SaveChanges();

			return Json(new { });
		}
		[HttpPost]
		public ActionResult DeprecateAndCloneClause(string clauseEditViewModel)
		{
			var vm = JsonConvert.DeserializeObject<ClauseEditViewModel>(clauseEditViewModel);
			var gatherMethods = new EvoLogic.GatherData.ClauseMethods(_dbeResultsManaged);
			var saveMethods = new EvoLogic.SaveData.ClauseMethods(_dbeResultsManaged);
			var clause = gatherMethods.GetClauseFromNormalizedId(vm.NormalizedId);
			var newClause = saveMethods.DeprecateAndCloneClause(clause);

			var newClauseEditView = MapFromClause(newClause);

			return Json(ComUtil.JsonEncodeCamelCase(newClauseEditView), JsonRequestBehavior.AllowGet);
		}
		[HttpGet]
		public JsonResult Search(string clauseSearch)
		{
			var csModel = JsonConvert.DeserializeObject<ClauseSearch>(clauseSearch);
			var evoSearchModel = new ClauseSearchModel()
			{
				ClauseText = (csModel.ToggleAnyOrExact ? csModel.ExactSearch : csModel.AnyWordSearch),
				SearchType = (csModel.ToggleAnyOrExact ? EvoLogic.Constants.SearchType.ExactMatch : EvoLogic.Constants.SearchType.AnyWord),
				ResultLimit = csModel.SearchLimit ?? 0,
				OriginalDate = csModel.HasOriginalDate,
				LinkedJurs = csModel.HasLinkedJurs,
				LinkedDocs = csModel.HasLinkedDocs,
				Language = csModel.Language,
				State = new List<int>(),
				Category = new List<int>(),
				LinkedValidationType = new List<int>(),
				CreationDateRangeStart = csModel.CreationRangeStart.ToString(),
				CreationDateRangeEnd = csModel.CreationRangeEnd.ToString(),
				OriginalDateRangeStart = csModel.OriginalDateStart.ToString(),
				OriginalDateRangeEnd = csModel.OriginalDateEnd.ToString(),
				TestCase = csModel.TestCaseIds,
				TestCaseAnyAll = (EvoLogic.Constants.SelectOptionFilterType)csModel.TestCasesAnyAll,
				Documents = csModel.DocumentIds,
				DocumentAnyAll = (EvoLogic.Constants.SelectOptionFilterType)csModel.DocumentsAnyAll,
				Jurisdictions = csModel.JurisdictionIds,
				JurisdictionAnyAll = (EvoLogic.Constants.SelectOptionFilterType)csModel.JurisdictionsAnyAll,
				FunctionalRoles = csModel.FunctionalRoleIds,
				FunctionalRoleAnyAll = (EvoLogic.Constants.SelectOptionFilterType)csModel.FunctionalRolesAnyAll
			};
			if (csModel.CategoryNonTestable)
			{
				evoSearchModel.Category.Add((int)EvoLogic.Constants.Category.NonTestable);
			}
			if (csModel.CategoryTestable)
			{
				evoSearchModel.Category.Add((int)EvoLogic.Constants.Category.Testable);
			}
			if (csModel.CategoryUnset)
			{
				evoSearchModel.Category.Add((int)EvoLogic.Constants.Category.Unset);
			}
			if (csModel.StateActive)
			{
				evoSearchModel.State.Add((int)EvoLogic.Constants.State.Active);
			}
			if (csModel.StateInactive)
			{
				evoSearchModel.State.Add((int)EvoLogic.Constants.State.Inactive);
			}
			if (csModel.StatePreRelease)
			{
				evoSearchModel.State.Add((int)EvoLogic.Constants.State.PreRelease);
			}
			if (csModel.LinkedValidationRegular)
			{
				evoSearchModel.LinkedValidationType.Add((int)EvoLogic.Constants.LinkedValidation.Regular);
			}
			if (csModel.LinkedValidationStandIn)
			{
				evoSearchModel.LinkedValidationType.Add((int)EvoLogic.Constants.LinkedValidation.StandIn);
			}
			if (csModel.LinkedValidationNone)
			{
				evoSearchModel.LinkedValidationType.Add((int)EvoLogic.Constants.LinkedValidation.None);
			}
			var results = SearchWrapper(evoSearchModel);
			var skipCount = (csModel.Page - 1) * csModel.ShowCount;
			var pagedResults = new ClauseSearchResultData
			{
				Clauses = results.Skip(skipCount).Take(csModel.ShowCount).ToList(),
				ResultTotalCount = results.Count,
				PageCount = (results.Count - (results.Count % csModel.ShowCount)) / csModel.ShowCount
			};
			if (results.Count % csModel.ShowCount > 0)
			{
				pagedResults.PageCount += 1;
			}
			return Json(ComUtil.JsonEncodeCamelCase(pagedResults), JsonRequestBehavior.AllowGet);
		}

		[HttpGet]
		public JsonResult RelatedClauses(string sourceData)
		{
			var sourceModel = JsonConvert.DeserializeObject<RelatedClauseSearch>(sourceData);
			var clauseSearchModel = new ClauseSearchModel
			{
				ResultLimit = 900,
				ClauseText = sourceModel.SectionName,
				SearchType = EvoLogic.Constants.SearchType.ExactMatch,
				Documents = sourceModel.Documents.Select(doc => doc.ID).ToList()
			};
			return Json(ComUtil.JsonEncodeCamelCase(SearchWrapper(clauseSearchModel)), JsonRequestBehavior.AllowGet);
		}

		private ClauseEditViewModel MapFromClause(Clause clause)
		{
			var vm = new ClauseEditViewModel
			{
				ClauseNativeLanguage = clause.Language,
				ClauseNativeSectionName = clause.NativeSectionName,
				ClauseNativeText = clause.NativeMemo,
				NormalizedId = clause.NormalizedId,
				ClauseSpecificTestingNotes = clause.SpecialNote,
				ClauseState = ((EvoLogic.Constants.State)(int)clause.State).ToString(),
				ClauseTestingType = ((EvoLogic.Constants.Category)(int)clause.Category).ToString(),
				ClauseText_English = clause.Memo,
				Id = clause.Id,
				OriginalClauseDate = clause.ClauseDate,
				SectionId = clause.Section,
				SectionName = clause.SectionName,
				JurisdictionIds = clause.ClauseJuris.Select(x => x.JuriID).ToList()
			};
			foreach (var item in clause.ClauseJuris)
			{
				vm.Jurisdictions.Add(new SelectListItem { Value = item.JuriID.ToString(), Text = _dbSubmission.tbl_lst_fileJuris.Single(x => x.FixId == (short)item.JuriID).Name, Selected = true });
			}
			vm.DocumentIds = clause.ClauseDocuments.Select(x => x.DocumentId).ToList();
			foreach (var item in clause.ClauseDocuments)
			{
				vm.Documents.Add(new SelectListItem { Value = item.DocumentId.ToString(), Text = item.Document.Name, Selected = true });
			}
			vm.QuestionIds = clause.ClauseQuestions.Select(x => x.QuestionId).ToList();
			foreach (var item in clause.ClauseQuestions)
			{
				vm.Questions.Add(new SelectListItem { Value = item.QuestionId.ToString(), Text = item.Question.Name, Selected = true });
			}
			vm.RoleIds = clause.ClauseFuncRoles.Select(x => (int)x.FunctionalRoleID).ToList();
			foreach (var item in clause.ClauseFuncRoles)
			{
				vm.Roles.Add(new SelectListItem { Value = item.FunctionalRoleID.ToString(), Text = item.FunctionalRole.Name, Selected = true });
			}
			return vm;
		}
		private List<ClauseViewModel> SearchWrapper(ClauseSearchModel evoSearchModel)
		{
			var clauseMethods = new EvoLogic.GatherData.ClauseMethods(_dbeResultsManaged);
			return AutoMapper.Mapper.Map<List<ClauseViewModel>>(clauseMethods.SearchClauses(evoSearchModel));
		}
	}
}
