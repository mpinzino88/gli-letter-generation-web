﻿using EF.eResultsManaged;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Submissions.Core;
using Submissions.Core.Models.EvolutionService;
using Submissions.Web.Areas.Evolution.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace Submissions.Web.Areas.Evolution.Controllers
{
	public class ComparisonController : Controller
	{
		private readonly IEvolutionService _evolutionService;
		private readonly IeResultsManagedContext _dbEresultsManaged;

		public ComparisonController(IEvolutionService evolutionService, IeResultsManagedContext dbEResultsManaged)
		{
			_evolutionService = evolutionService;
			_dbEresultsManaged = dbEResultsManaged;
		}

		public ActionResult Index()
		{
			var vm = new ComparisonIndexViewModel();
			return View(vm);
		}

		[HttpPost]
		public ActionResult LoadAnswerHistory(string componentGroupIds, string question)
		{
			IList<string> cGroupIds = componentGroupIds.Split(',').ToList<string>();
			IList<MultipleAnswerHistory> multipleAnswerHistory = new List<MultipleAnswerHistory>();
			for (int i = 0; i < cGroupIds.Count(); i++)
			{
				MultipleAnswerHistory result = new MultipleAnswerHistory
				{
					AnswerHistory = _evolutionService.GetAnswerHistory(cGroupIds.ElementAt(i), question)
				};
				multipleAnswerHistory.Add(result);
			}
			var json = JsonConvert.SerializeObject(multipleAnswerHistory, Formatting.Indented, new JsonSerializerSettings { ContractResolver = new CamelCasePropertyNamesContractResolver() });
			return Content(json, "application/json");
		}

		[HttpGet]
		public ActionResult LoadComponentGroupComparison(string componentGroupIds, string conflict, string[] jurisdictionIds)
		{
			bool isConflict = Convert.ToBoolean(conflict);
			string jurisdictionIdInts = "";
			if (jurisdictionIds != null)
			{
				jurisdictionIdInts = string.Join(",", jurisdictionIds);
			}

			var result = _evolutionService.GetComponentGroupComparison(componentGroupIds, isConflict, jurisdictionIdInts);
			var json = JsonConvert.SerializeObject(result, Formatting.Indented, new JsonSerializerSettings { ContractResolver = new CamelCasePropertyNamesContractResolver() });

			return Content(json, "application/json");
		}

		[HttpGet]
		public ActionResult LoadFunctionalRoles(string componentGroupIds, string conflict)
		{
			List<string> componentGroupIdList = componentGroupIds.Split(',').ToList();
			for (int i = componentGroupIdList.Count; i < 10; i++)
			{
				componentGroupIdList.Add("");
			}
			bool isConflict = Convert.ToBoolean(conflict);

			var functionalRoles = _evolutionService.GetFunctionalRolesComparison(componentGroupIds).ToList();

			if (isConflict)
			{
				var functionalRoleDuplicates = functionalRoles
												.GroupBy(g => g.FunctionalRole)
												.Where(h => h.Count() > 1)
												.Select(h => h.Max(i => i.FunctionalRole)).ToList();

				functionalRoles = functionalRoles.Where(x => !functionalRoleDuplicates.Contains(x.FunctionalRole)).ToList();
			}

			var functionalRolesIds = _evolutionService.GetFunctionalRolesComparison(componentGroupIds).Select(x => x.FunctionalRole).ToList();
			var functionalRoleInfo = _dbEresultsManaged.FunctionalRole.Where(x => functionalRolesIds.Contains(x.Id)).Select(x => new { x.Id, x.Name }).ToList();

			var componentFunctionalRoles = functionalRoles
				.Join(functionalRoleInfo,
					x => x.FunctionalRole,
					y => y.Id,
					(y, x) => new { FunctionalRoleId = x.Id, FunctionalRoleName = x.Name, ComponentGroup = y.CompGroup }).ToList();

			var componentGroupGrid = componentFunctionalRoles
				.GroupBy(g => g.FunctionalRoleId)
				.Select(x => new FunctionalRolesComparisonRow
				{
					FunctionalRoleId = x.Key,
					FunctionalRole = x.Max(y => y.FunctionalRoleName),
					ComponentGroup1 = x.Where(g => g.ComponentGroup == componentGroupIdList[0]).Any(),
					ComponentGroup2 = x.Where(g => g.ComponentGroup == componentGroupIdList[1]).Any(),
					ComponentGroup3 = x.Where(g => g.ComponentGroup == componentGroupIdList[2]).Any(),
					ComponentGroup4 = x.Where(g => g.ComponentGroup == componentGroupIdList[3]).Any(),
					ComponentGroup5 = x.Where(g => g.ComponentGroup == componentGroupIdList[4]).Any(),
					ComponentGroup6 = x.Where(g => g.ComponentGroup == componentGroupIdList[5]).Any(),
					ComponentGroup7 = x.Where(g => g.ComponentGroup == componentGroupIdList[6]).Any(),
					ComponentGroup8 = x.Where(g => g.ComponentGroup == componentGroupIdList[7]).Any(),
					ComponentGroup9 = x.Where(g => g.ComponentGroup == componentGroupIdList[8]).Any(),
					ComponentGroup10 = x.Where(g => g.ComponentGroup == componentGroupIdList[9]).Any()
				}).ToList();

			var json = JsonConvert.SerializeObject(componentGroupGrid, Formatting.Indented, new JsonSerializerSettings { ContractResolver = new CamelCasePropertyNamesContractResolver() });

			return Content(json, "application/json");
		}

		[HttpGet]
		public ActionResult LoadInstances(string projectName)
		{
			var result = _evolutionService.GetInstances(projectName.Trim());
			var json = JsonConvert.SerializeObject(result, Formatting.Indented, new JsonSerializerSettings { ContractResolver = new CamelCasePropertyNamesContractResolver() });

			return Content(json, "application/json");
		}
	}
}