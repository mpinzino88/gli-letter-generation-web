﻿using EF.eResultsManaged;
using EF.QANotes;
using Newtonsoft.Json;
using Submissions.Common;
using Submissions.Web.Areas.Evolution.Models;
using Submissions.Web.Models.Grids.Evolution;
using System;
using System.Linq;
using System.Web.Mvc;

namespace Submissions.Web.Areas.Evolution.Controllers
{
	public class QANotesController : Controller
	{
		private readonly IUserContext _userContext;
		private readonly IQANotesContext _dbQANotes;
		private readonly IeResultsManagedContext _dbEresultsManaged;

		public QANotesController(IUserContext userContext, IQANotesContext dbQANotes, IeResultsManagedContext dbEResultsManaged)
		{
			_userContext = userContext;
			_dbQANotes = dbQANotes;
			_dbEresultsManaged = dbEResultsManaged;
		}

		public ActionResult Index()
		{
			var vm = new QANotesIndexViewModel();
			vm.QANotesGridModel = new QANotesGrid();
			return View(vm);
		}

		[OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
		public JsonResult LoadQANotesIndexGrid()
		{
			var filters = new QANotesFilter()
			{
				Active = Request.QueryString["Active"],
				ApplicableResult = Request.QueryString["ApplicableResult"],
				Heading = Request.QueryString["Heading"],
				Language = Request.QueryString["Language"],
				LanguageNote = Request.QueryString["LanguageNote"],
				Note = Request.QueryString["Note"],
				NoteId = Request.QueryString["NoteID"],
				QAMemo = Request.QueryString["QAMemo"],
				QBText = Request.QueryString["QBText"],
				RuleMemo = Request.QueryString["RuleMemo"],
				Section = Request.QueryString["Section"],
				TechLogic = Request.QueryString["TechLogic"],
				UniqueId = Request.QueryString["uniqueID"]
			};
			var test = Request.QueryString["Language"];
			var gridModel = new QANotesGrid();
			var results = QANotesQuery(filters);
			return gridModel.Grid.DataBind(results);
		}

		private IQueryable<QANotesIndexGridRecord> QANotesQuery(QANotesFilter filters)
		{
			var query = _dbQANotes.vw_QANotesQuery
				.Select(x => new QANotesIndexGridRecord
				{
					Active = x.Active,
					ApplicableResult = x.ApplicableResult,
					Heading = x.Heading,
					Language = x.Language,
					LanguageNote = x.LanguageNote,
					Note = x.Note,
					NoteId = x.NoteID.ToString(),
					QAMemo = x.QAMemo,
					QBText = x.Text,
					RuleMemo = x.Memo,
					Section = x.Section,
					TechLogic = x.TechLogic,
					UniqueId = x.uniqueID.ToString(),
					ValRuleId = x.ValRuleID
				}).AsQueryable();

			if (!String.IsNullOrEmpty(filters.Active))
			{
				if (filters.Active.ToLower() == "true")
				{
					query = query.Where(x => x.Active == true);
				}
				if (filters.Active.ToLower() == "false")
				{
					query = query.Where(x => x.Active == false);
				}
			}

			if (!String.IsNullOrEmpty(filters.ApplicableResult))
			{
				query = query.Where(x => x.ApplicableResult.StartsWith(filters.ApplicableResult.Trim()));
			}

			if (!String.IsNullOrEmpty(filters.Heading))
			{
				query = query.Where(x => x.Heading.StartsWith(filters.Heading.Trim()));
			}

			if (!String.IsNullOrEmpty(filters.Language))
			{
				query = query.Where(x => x.Language.StartsWith(filters.Language.Trim()));
			}

			if (!String.IsNullOrEmpty(filters.LanguageNote))
			{
				query = query.Where(x => x.LanguageNote.StartsWith(filters.LanguageNote.Trim()));
			}

			if (!String.IsNullOrEmpty(filters.Note))
			{
				query = query.Where(x => x.Note.StartsWith(filters.Note.Trim()));
			}

			if (!String.IsNullOrEmpty(filters.NoteId))
			{
				query = query.Where(x => x.NoteId == filters.NoteId);
			}

			if (!String.IsNullOrEmpty(filters.QAMemo))
			{
				query = query.Where(x => x.QAMemo.StartsWith(filters.QAMemo.Trim()));
			}

			if (!String.IsNullOrEmpty(filters.QBText))
			{
				query = query.Where(x => x.QBText.StartsWith(filters.QBText.Trim()));
			}

			if (!String.IsNullOrEmpty(filters.RuleMemo))
			{
				query = query.Where(x => x.RuleMemo.StartsWith(filters.RuleMemo.Trim()));
			}

			if (!String.IsNullOrEmpty(filters.Section))
			{
				query = query.Where(x => x.Section.StartsWith(filters.Section.Trim()));
			}

			if (!String.IsNullOrEmpty(filters.TechLogic))
			{
				query = query.Where(x => x.TechLogic.StartsWith(filters.TechLogic.Trim()));
			}

			if (!String.IsNullOrEmpty(filters.UniqueId))
			{
				query = query.Where(x => x.UniqueId == filters.UniqueId);
			}

			return query;
		}

		private IQueryable<QANotesIndexGridRecord> QANotesQuery()
		{
			var query = _dbQANotes.vw_QANotesQuery
				.Select(x => new QANotesIndexGridRecord
				{
					Active = x.Active,
					ApplicableResult = x.ApplicableResult,
					Heading = x.Heading,
					Language = x.Language,
					LanguageNote = x.LanguageNote,
					Note = x.Note,
					NoteId = x.NoteID.ToString(),
					QAMemo = x.QAMemo,
					QBText = x.Text,
					RuleMemo = x.Memo,
					Section = x.Section,
					TechLogic = x.TechLogic,
					UniqueId = x.uniqueID.ToString(),
					ValRuleId = x.ValRuleID
				}).AsQueryable();

			return query;
		}

		public void Export(ExportType exportType, string Filters)
		{
			var gridModel = new QANotesGrid();
			var grid = gridModel.Grid;
			var query = QANotesQuery(JsonConvert.DeserializeObject<QANotesFilter>(Filters));
			gridModel.Export(exportType, query);
		}
	}
}