﻿using EF.eResultsManaged;
using EF.GLIAccess;
using Submissions.Common;
using Submissions.Web.Areas.Evolution.Models;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Web.Mvc;

namespace Submissions.Web.Areas.Evolution.Controllers
{
	public class ValidationAnalysisController : Controller
	{
		private readonly IGLIAccessContext _dbGLIAccess;
		private readonly IeResultsManagedContext _dbeResultsManaged;

		public ValidationAnalysisController(IGLIAccessContext dbGLIAccess, IeResultsManagedContext dbeResultsManaged)
		{
			_dbGLIAccess = dbGLIAccess;
			_dbeResultsManaged = dbeResultsManaged;
		}

		public ActionResult Index()
		{
			var results = new ValidationAnalysisIndexViewModel();

			//Search View Model
			results.EvoSearch = new EvolutionSearchViewModel();
			results.EvoSearch.GridId = "ValidationAnalysisGrid";

			//Result Grid
			results.ValidationAnalysisGrid = new ValidationAnalysisIndexGridModel();

			return View(results);
		}


		#region Load Grid Data

		public JsonResult LoadGridIndex(List<int?> tJr = null, List<int?> jr = null,
			List<int?> fr = null, List<int?> d = null, List<Guid?> tc = null, string mm = "", string tx = "")
		{
			//if there is no selection (all null)
			if (!(new object[] { tJr, jr, fr, d, tc }).Any(v => v != null))
			{
				return Json(new object[] { new object() }, JsonRequestBehavior.AllowGet);
			}

			var gridModel = new ValidationAnalysisIndexGridModel();

			var fJ = _dbGLIAccess.tbl_lst_fileJuris.ToList();
			var fjNames = fJ.Select(fj => fj.Name);
			var fjIds = fJ.Select(fj => fj.Id);

			var evoQuery = EvoQuery(tJr, jr, fr, d, tc, mm, tx, true);

			var requestString = new NameValueCollection(System.Web.HttpContext.Current.Request.QueryString);
			var jurisdictionNameFilter = requestString["Jurisdiction"];

			//Create dictionary for jurisdicition id and name
			Dictionary<int, string> fileJurisDictionary = (Dictionary<int, string>)this.Session["fileJurisDictionary"];

			//Collect all jurisdiction id if its jurisdiction names contains filter value
			if (jurisdictionNameFilter != null)
			{
				List<int?> idCollection = new List<int?>();

				foreach (KeyValuePair<int, string> entry in fileJurisDictionary)
				{
					if (entry.Value.Contains(jurisdictionNameFilter))
					{
						idCollection.Add(entry.Key);
					}
				}
				// check if jurisdictionNameFilter is not null or empty, if so:
				// modify requestString
				requestString.Remove("Jurisdiction");
				// check if there are still searches, and make search false if not (is necessary)
				//var testFilter = requestString[ "TestCase" ];

				var colList = gridModel.Grid.Columns.Select(col => col.DataField);
				if (colList.All(name => String.IsNullOrEmpty(requestString[name])))
				{
					requestString["_search"] = "false";
				}

				// modify query with a where clause based on the jurisdiction id's associated with the jurisdiction name search
				evoQuery = evoQuery.Where(row => idCollection.Contains(Convert.ToInt32(row.JuriId)));
			}

			var query = (
				from evo in evoQuery
				select new ValidationAnalysisQuery
				{
					ClauseId = evo.ClauseId,
					TestCase = evo.TestCase,
					Memo = evo.Memo,
					Validation = evo.Validation,
					FuncRoleName = evo.FuncRoleName
				}
			).Distinct();

			//Update query in session for grid export
			Session["gridQueryString"] = new NameValueCollection(this.Request.QueryString);
			Session["alreadyBeenTested"] = tJr;
			Session["toBeTested"] = jr;
			Session["funcRoles"] = fr;
			Session["documents"] = d;
			Session["testCases"] = tc;
			Session["memo"] = mm;
			Session["validation"] = tx;

			//var ret = gridModel.Grid.DataBind( query, requestString );
			var ret = gridModel.Grid.DataBind(query);
			return ret;
		}

		#endregion

		private IQueryable<ValidationAnalysisQuery> EvoQuery(List<int?> alreadyBeenTested = null, List<int?> toBeTested = null, List<int?> funcRoles = null,
			List<int?> documents = null, List<Guid?> testCases = null, string memo = null, string validation = null, bool MultiFuncRoles = false)
		{
			var ret = (IQueryable<ValidationAnalysisQuery>)this.Session["EvoQuery"];
			if (ret == null)
			{
				alreadyBeenTested = alreadyBeenTested ?? new List<int?>();
				toBeTested = toBeTested ?? new List<int?>();
				funcRoles = funcRoles ?? new List<int?>();
				documents = documents ?? new List<int?>();
				testCases = testCases ?? new List<Guid?>();

				var notInQuery = (
					from clause in _dbeResultsManaged.Clause
					join clauseValidationJoin in _dbeResultsManaged.ClauseValidationJoin on clause.Id equals clauseValidationJoin.ClauseId
					join questionBase in _dbeResultsManaged.QuestionBase on clauseValidationJoin.QuestionId equals questionBase.Id
					join clauseDocumentJoin in _dbeResultsManaged.ClauseDocumentJoin on clause.Id equals clauseDocumentJoin.ClauseId
					join document in _dbeResultsManaged.Document on clauseDocumentJoin.DocumentId equals document.Id
					join documentJuriJoin in _dbeResultsManaged.DocumentJuriJoin on document.Id equals documentJuriJoin.DocumentId
					//join validationFuncRole in dc_em.ValidationFuncRole on questionBase.ID equals validationFuncRole.QuestionID
					let validationFuncRole = _dbeResultsManaged.ValidationFuncRole.Where(validationFuncRole => questionBase.Id == validationFuncRole.QuestionId).FirstOrDefault() //Cross Apply to prevent duplicates
					where validationFuncRole != null
					where
					//clause.State != 3
					//&& clauseDocumentJoin.State
					//&& documentJuriJoin.State
					//&& clauseValidationJoin.State
					//&& questionBase.State != 3
					//Get JurisdictionID from already been tested
					alreadyBeenTested.Contains(documentJuriJoin.JurisdictionId)
					// Get selected fucntional roles
					&& funcRoles.Contains(validationFuncRole.FunctionalRoleId)
					// Get selected documents
					&& documents.Contains(document.Id)
					// Get selected test cases by child question which parenet question is in the same QuestionBase table
					&& testCases.Contains(questionBase.ParentQuestionId) // A question that aType = 9 is a parent of a question that aType = 11
					select questionBase.Id
				);

				var evoQuery = (
					 from clause in _dbeResultsManaged.Clause
					 join clauseValidationJoin in _dbeResultsManaged.ClauseValidationJoin on clause.Id equals clauseValidationJoin.ClauseId
					 join questionBase in _dbeResultsManaged.QuestionBase on clauseValidationJoin.QuestionId equals questionBase.Id // child question aType = 11
					 join parentQuestion in _dbeResultsManaged.QuestionBase on questionBase.ParentQuestionId equals parentQuestion.Id // child question aType = 9
					 join clauseDocumentJoin in _dbeResultsManaged.ClauseDocumentJoin on clause.Id equals clauseDocumentJoin.ClauseId
					 join document in _dbeResultsManaged.Document on clauseDocumentJoin.DocumentId equals document.Id
					 join documentJuriJoin in _dbeResultsManaged.DocumentJuriJoin on document.Id equals documentJuriJoin.DocumentId
					 //join validationFuncRole in dc_em.ValidationFuncRole on questionBase.ID equals validationFuncRole.QuestionID
					 let validationFuncRole = _dbeResultsManaged.ValidationFuncRole.Where(validationFuncRole => questionBase.Id == validationFuncRole.QuestionId).FirstOrDefault() //Cross Apply to prevent duplicates
					 where validationFuncRole != null
					 join functionalRole in _dbeResultsManaged.FunctionalRole on validationFuncRole.FunctionalRoleId equals functionalRole.Id
					 where
					 clause.State != 3
					 && clauseDocumentJoin.State
					 && documentJuriJoin.State
					 && clauseValidationJoin.State
					 && questionBase.State != 3
					 && parentQuestion.State != 3
					 //Get JurisdictionID from already been tested
					 && toBeTested.Contains(documentJuriJoin.JurisdictionId)
					 // Get selected fucntional roles
					 && funcRoles.Contains(validationFuncRole.FunctionalRoleId)
					 // Get selected fucntional roles
					 && funcRoles.Contains(validationFuncRole.FunctionalRoleId)
					 // Get selected documents
					 && documents.Contains(document.Id)
					 // Get selected test cases by child question which parenet question is in the same QuestionBase table
					 && testCases.Contains(questionBase.ParentQuestionId) // A question that aType = 9 is a parent of a question that aType = 11
																		  //For customized searching
					 && !notInQuery.Contains(questionBase.Id)

					 select new ValidationAnalysisQuery
					 {
						 ClauseId = clause.Id.ToString(),
						 TestCase = parentQuestion.Name,
						 Memo = clause.Memo,
						 Validation = questionBase.Text,
						 FuncRoleName = functionalRole.Name
					 }
				);

				if (!String.IsNullOrEmpty(memo))
				{
					evoQuery = evoQuery.Where(x => x.Memo.Contains(memo));
				}

				if (!String.IsNullOrEmpty(validation))
				{
					evoQuery = evoQuery.Where(x => x.Validation.Contains(validation));
				}

				if (MultiFuncRoles)
				{
					MultiFunctionalRoles(evoQuery);
				}
				ret = evoQuery;
			}
			return ret;
		}

		public JsonResult AutocompleteList()
		{
			var alreadyBeenTested = (List<int?>)Session["alreadyBeenTested"];
			var toBeTested = (List<int?>)Session["toBeTested"];
			var funcRoles = (List<int?>)Session["funcRoles"];
			var documents = (List<int?>)Session["documents"];
			var testCases = (List<Guid?>)Session["testCases"];
			IQueryable<ValidationAnalysisQuery> evoList = EvoQuery(alreadyBeenTested, toBeTested, funcRoles, documents, testCases);

			var model = new ValidationAnalysisAutocomplete();
			model.TestCaseResults = evoList.Select(x => x.TestCase == null ? "" : x.TestCase.ToString()).Distinct();
			model.FuncRoleResults = evoList.Select(x => x.FuncRoleName == null ? "" : x.FuncRoleName.ToString()).Distinct();
			return Json(model, JsonRequestBehavior.AllowGet);
		}

		public void MultiFunctionalRoles(IQueryable<ValidationAnalysisQuery> evoQuery)
		{
			//Get all functional roles in one cell, using LINQ pivot
			var partialEvo = evoQuery
				.GroupBy(e => e.ClauseId)
				.Select(p => new
				{
					ClauseId = p.Key,
					FuncRoleName = p.Select(e => e.FuncRoleName).Distinct().OrderBy(x => x).ToList()
				});

			var pivotList = new List<AllClauseFuncRole>();
			//Using one or two level(s) foeeach to modified the cell data
			foreach (var clause in partialEvo)
			{
				var key = clause.ClauseId;
				var val = String.Join("<br />", clause.FuncRoleName);
				pivotList.Add(new AllClauseFuncRole { CId = key, FuncRoles = val });
			}
			this.Session["multiFuncRoles"] = pivotList;
		}

		public JsonResult GetDictionarySession(string sessionName)
		{
			var list = (List<AllClauseFuncRole>)Session[sessionName];
			return Json(list, JsonRequestBehavior.AllowGet);
		}

		public void GridExport(string type)
		{
			ExportType exportType = (ExportType)Enum.Parse(typeof(ExportType), type, true);

			var alreadyBeenTested = (List<int?>)Session["alreadyBeenTested"];
			var toBeTested = (List<int?>)Session["toBeTested"];
			var funcRoles = (List<int?>)Session["funcRoles"];
			var documents = (List<int?>)Session["documents"];
			var testCases = (List<Guid?>)Session["testCases"];
			var memo = Session["memo"] == null ? "" : Session["memo"].ToString();
			var validation = Session["validation"] == null ? "" : Session["validation"].ToString();

			var query = (
				from evo in this.EvoQuery(alreadyBeenTested, toBeTested, funcRoles, documents, testCases, memo, validation, true)
				select new ValidationAnalysisQuery
				{
					ClauseId = evo.ClauseId,
					TestCase = evo.TestCase,
					Memo = evo.Memo,
					Validation = evo.Validation,
					FuncRoleName = evo.FuncRoleName
				}
			).Distinct();

			var queryString = (NameValueCollection)this.Session["gridQueryString"];
			var funcRolesList = (List<AllClauseFuncRole>)this.Session["multiFuncRoles"];
			Dictionary<string, string> funcRolesDict = funcRolesList == null ? null : funcRolesList.ToDictionary(x => x.CId, x => x.FuncRoles);
			ValidationAnalysisIndexGridModel.Export(exportType, query, queryString, funcRolesDict);
		}
	}
}