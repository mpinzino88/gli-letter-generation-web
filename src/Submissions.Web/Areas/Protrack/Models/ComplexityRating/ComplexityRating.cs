﻿using System.ComponentModel;

namespace Submissions.Web.Areas.Protrack.Models.Project
{
	public class ComplexityRating
	{
		[DisplayName("Complexity Rating:")]
		public int? ComplexityRatingId { get; set; }
		public string ComplexityRatingDesc { get; set; }

		[DisplayName("Product Type:")]
		public int? ProductTypeId { get; set; }
		public string ProductTypeDesc { get; set; }
		public int LoginId { get; set; }
		public string FileNumber { get; set; }
		public string ProjectIds { get; set; }
	}
}