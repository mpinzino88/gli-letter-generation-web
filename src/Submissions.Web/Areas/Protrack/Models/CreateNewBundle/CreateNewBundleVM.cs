﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Submissions.Web.Models.Grids;
using Submissions.Web.Models.Query;

namespace Submissions.Web.Areas.Protrack.Models.Project
{
	public class CreateNewBundleVM
	{
		public CreateNewBundleVM()
		{
			Filters = new ProjectJurisdictionItemSearch();
			JurisdictionGrid = new CreateNewTransferGrid();
		}
		public ProjectJurisdictionItemSearch Filters { get; set; }
				
		public List<SelectListItem> BundleTypeOptions { get; set; }
		public CreateNewTransferGrid JurisdictionGrid { get; set; }
		public string AddToExistingProject { get; set; }
		public int AddToExistingProjectId { get; set; }

	}

	public class BundlesSearch
	{
		public int Id { get; set; }
		public string Name { get; set; }
		public string Status { get; set; }
		public string Holder { get; set; }
		public byte? TypeId {  get; set;}
	}
}