﻿using Submissions.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;
using Trirand.Web.Mvc;

namespace Submissions.Web.Areas.Protrack.Models
{
	public class OnHoldEditViewModel
	{
		public OnHoldEditViewModel()
		{
			Project = new OHProject();
			SubmissionGrid = new SubmissionJurisdictionGrid(false);
			JurisdictionGrid = new SubmissionJurisdictionGrid(true);
		}
		public OHProject Project { get; set; }

		[DisplayName("Reason/Memo:")]
		[Required(ErrorMessage = "OH Memo is required.")]
		public string OhMemo { get; set; }

		[DisplayName("Short Desc:")]
		public string OhShortDesc { get; set; }

		public string ApplyToProjectId { get; set; }
		public string ApplyToSubmissionId { get; set; }
		public string ApplyToJurisdictionId { get; set; }

		public SubmissionJurisdictionGrid SubmissionGrid { get; set; }
		public SubmissionJurisdictionGrid JurisdictionGrid { get; set; }
	}

	public class BatchOHHistory
	{
		public BatchOHHistory()
		{
			BlankOnHoldHistory = new OnHoldEditHistory();
		}
		public string ProjectIds { get; set; }
		public List<OHProject> SelProjects { get; set; }
		public OnHoldEditHistory BlankOnHoldHistory { get; set; }
	}

	public class BatchProjectOnHold
	{
		public BatchProjectOnHold()
		{
			StartDate = DateTime.Now;
		}

		public string ProjectIds { get; set; }
		[DisplayName("Start Date")]
		public DateTime? StartDate { get; set; }

		[DisplayName("Reason:")]
		[Required(ErrorMessage = "OH Memo is required.")]
		public string OhReason { get; set; }

		[DisplayName("Short Description:")]
		public string OhShortDesc { get; set; }
		public bool onHold { get; set; }
	}


	public class OHProject
	{
		public int ProjectId { get; set; }
		public string ProjectName { get; set; }
		public string ProjectStatus { get; set; }
		public Boolean? IsTransfer { get; set; }
		public int? OnHold { get; set; }
		public List<OHSubmission> SubmissionsList { get; set; }
		public List<OHJurisdiction> JurisdictionsList { get; set; }
	}

	public class OHSubmission
	{
		public int Id { get; set; }
		public string IdNumber { get; set; }
		public string Version { get; set; }
		public string PartNumber { get; set; }
	}

	public class OHJurisdiction
	{
		public string JurisdictionId { get; set; }
		public string JurisdictionName { get; set; }
	}

	public class OnHoldHistoryViewModel
	{
		public OnHoldHistoryViewModel()
		{
			OnHoldHistory = new List<OnHoldEditHistory>();
			BlankOnHoldHistory = new OnHoldEditHistory();
			ApplyToAll = new List<ApplyToAllVM>();
		}

		public int MainProjectId { get; set; }
		public bool? ProjectOnHold { get; set; }
		public bool IsTransfer { get; set; }

		public int? ProjectId { get; set; }
		public int? SubmissionId { get; set; }
		public int? PrimaryId { get; set; }
		public string ProjectName { get; set; }
		public string FileNumber { get; set; }
		public string IDNumber { get; set; }
		public string Version { get; set; }
		public string DateCode { get; set; }
		public string GameName { get; set; }
		public string Jurisdiction { get; set; }
		public string JurisdictionId { get; set; }

		public string SubmissionStatus { get; set; }
		public string JurisdictionStatus { get; set; }
		public List<OnHoldEditHistory> OnHoldHistory { get; set; }
		public OnHoldEditHistory BlankOnHoldHistory { get; set; }

		public List<ApplyToAllVM> ApplyToAll { get; set; }
	}

	public class ApplyToAllVM
	{
		public int Id { get; set; }
		public string FileNumber { get; set; }
		public string IDNumber { get; set; }
		public string Version { get; set; }
		public string DateCode { get; set; }
		public string GameName { get; set; }
		public string Jurisdiction { get; set; }
		public string JurisdictionId { get; set; }

		public string SubmissionStatus { get; set; }
		public string JurisdictionStatus { get; set; }

		[DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}")]
		public DateTime? JurRcvdDate { get; set; }
	}

	public class AffectedOHId
	{
		public int Id { get; set; }
		public OnHoldMode OldStatus { get; set; }
		public OnHoldMode NewStatus { get; set; }
	}

	public class ApplyProjectItemIds
	{
		public List<int?> SubmissionId { get; set; }
		public List<int> JurisdictionId { get; set; }
		public int ProjectId { get; set; }
	}

	public class OnHoldEditHistory
	{
		public int Id { get; set; }
		public DateTime? OldStartDate { get; set; }
		public DateTime? OldEndDate { get; set; }
		public DateTime? StartDate { get; set; }
		public DateTime? EndDate { get; set; }
		public string Reason { get; set; }
		public string ShortDesc { get; set; }
		public bool IsBackDated { get; set; }
		public bool ApplyToAll { get; set; }
		public string Error { get; set; }
		public int? SubmissionId { get; set; }
		public int? JurisdictionId { get; set; }
		public int? ProjectId { get; set; }
	}

	public class OHSearch
	{
		public int ProjectId { get; set; }
		public List<int> SubmissionIDList { get; set; }
		public List<string> JurisdictionIDList { get; set; }
		public DateTime? OHPeriodFrom { get; set; }
		public DateTime? OHPeriodTo { get; set; }
		public bool? OnHold { get; set; }
	}

	#region gridModel
	public class SubmissionJurisdictionGridModel
	{
		public int SubmissionId { get; set; }
		public int PrimaryId { get; set; }
		public string IdNumber { get; set; }
		public string Version { get; set; }
		public string DateCode { get; set; }
		public string GameName { get; set; }
		public string SubmissionStatus { get; set; }
		public DateTime? SubRcvdDate { get; set; }
		public string JurisdictionId { get; set; }
		public string Jurisdiction { get; set; }
		public string JurisdictionStatus { get; set; }
		public DateTime? JurRcvdDate { get; set; }
	}

	public class SubmissionJurisdictionGrid
	{
		public JQGrid Grid { get; set; }

		public bool IsJurisdictionGrid { get; set; }

		public SubmissionJurisdictionGrid(bool isJurisdictionGrid = false)
		{
			IsJurisdictionGrid = isJurisdictionGrid;

			Grid = new JQGrid
			{
				Columns = LoadColumns(),
				ClientSideEvents = new ClientSideEvents
				{
					BeforeAjaxRequest = (isJurisdictionGrid) ? "jurisdictionBeforeAjaxRequest" : "submissionBeforeAjaxRequest"
				},
			};

			Grid.ID = (isJurisdictionGrid) ? "JurisdictionGrid" : "SubmissionGrid";
			Grid.DataUrl = GetUrl();
			Grid.AutoWidth = true;
			Grid.Width = Unit.Percentage(100);
			Grid.Height = Unit.Percentage(100);

			Grid.ToolBarSettings = new ToolBarSettings
			{
				ShowRefreshButton = true,
				ShowSearchToolBar = true,
				ToolBarPosition = ToolBarPosition.Top
			};
			Grid.PagerSettings = new Trirand.Web.Mvc.PagerSettings
			{
				PageSize = 100,
				PageSizeOptions = "[]"
			};
			Grid.AppearanceSettings = new AppearanceSettings
			{
				AlternateRowBackground = true,
				HighlightRowsOnHover = true,
				ShowFooter = false,
				Caption = (isJurisdictionGrid) ? "Jurisdictions" : "Records"
			};
		}

		private string GetUrl()
		{
			if (this.IsJurisdictionGrid)
			{
				return new UrlHelper(((MvcHandler)HttpContext.Current.Handler).RequestContext).Action("LoadOHJurisdictionGrid", "GridsAjax", new { Area = "" });
			}
			else
			{
				return new UrlHelper(((MvcHandler)HttpContext.Current.Handler).RequestContext).Action("LoadOHSubmissionGrid", "GridsAjax", new { Area = "" });
			}
		}

		private List<JQGridColumn> LoadColumns()
		{
			var columns = new List<JQGridColumn>();

			if (this.IsJurisdictionGrid)
			{
				columns.Add(new JQGridColumn
				{
					DataField = "PrimaryId",
					PrimaryKey = true,
					Editable = false,
					Visible = false
				});
			}
			else
			{
				columns.Add(new JQGridColumn
				{
					DataField = "SubmissionId",
					PrimaryKey = true,
					Editable = false,
					Visible = false
				});
			}
			columns.Add(new JQGridColumn
			{
				DataField = "IDNumber",
				HeaderText = "ID Number",
				Width = 50,
				DataType = typeof(string),
				Searchable = false,
			});
			columns.Add(new JQGridColumn
			{
				DataField = "GameName",
				HeaderText = "Game Name",
				Width = 50,
				DataType = typeof(string),
				Searchable = false,
			});
			columns.Add(new JQGridColumn
			{
				DataField = "Version",
				HeaderText = "Version",
				Width = 20,
				DataType = typeof(string),
				Searchable = false,
			});
			columns.Add(new JQGridColumn
			{
				DataField = "SubmissionStatus",
				HeaderText = "Sub Status",
				Width = 15,
				DataType = typeof(string),
				Searchable = false,
			});

			if (this.IsJurisdictionGrid)
			{
				columns.Add(new JQGridColumn
				{
					DataField = "Jurisdiction",
					HeaderText = "Jurisdiction",
					Width = 40,
					DataType = typeof(string),
					Searchable = false,
				});
				columns.Add(new JQGridColumn
				{
					DataField = "JurisdictionId",
					HeaderText = "",
					Width = 10,
					DataType = typeof(string),
					Searchable = false,
				});
				columns.Add(new JQGridColumn
				{
					DataField = "JurRcvdDate",
					HeaderText = "Jur Rcvd",
					Width = 15,
					DataType = typeof(DateTime),
					DataFormatString = "{0:MM/dd/yyyy}",
					Searchable = false,
				});
				columns.Add(new JQGridColumn
				{
					DataField = "JurisdictionStatus",
					HeaderText = "Jur Status",
					Width = 15,
					DataType = typeof(string),
					Searchable = false,
				});
				columns.Add(new JQGridColumn
				{
					DataField = "JurisdictionStatus",
					HeaderText = "On Hold",
					Width = 15,
					Formatter = new CustomFormatter { FormatFunction = "JurisdictionOHCheck" },
					Searchable = false,
				});
				columns.Add(new JQGridColumn
				{
					DataField = "PrimaryId",
					HeaderText = "OH History",
					Width = 15,
					Formatter = new CustomFormatter { FormatFunction = "OHHistoryJurisdiction" },
					Searchable = false,
				});
			}
			else
			{
				columns.Add(new JQGridColumn
				{
					DataField = "SubRcvdDate",
					HeaderText = "Sub Rcvd",
					Width = 15,
					DataType = typeof(DateTime),
					DataFormatString = "{0:MM/dd/yyyy}",
					Searchable = false,
				});
				columns.Add(new JQGridColumn
				{
					DataField = "SubmissionStatus",
					HeaderText = "On Hold",
					Width = 15,
					Formatter = new CustomFormatter { FormatFunction = "SubmissionOHCheck" },
					Searchable = false,
				});
				columns.Add(new JQGridColumn
				{
					DataField = "SubmissionId",
					HeaderText = "OH History",
					Width = 15,
					Formatter = new CustomFormatter { FormatFunction = "OHHistorySubmission" },
					Searchable = false,
				});
			}
			return columns;
		}
	}

	public class ApplyToAllGrid : SubmissionJurisdictionGrid
	{
		public ApplyToAllGrid()
		{
			this.IsJurisdictionGrid = true;
			this.Grid.MultiSelect = true;
			this.Grid.DataUrl = new UrlHelper(((MvcHandler)HttpContext.Current.Handler).RequestContext).Action("LoadOHApplyAllGrid", "GridsAjax", new { Area = "" });
			this.Grid.ClientSideEvents = null;
			this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.HeaderText == "On Hold")].Visible = false;
			this.Grid.Columns[this.Grid.Columns.FindIndex(x => x.HeaderText == "OH History")].Visible = false;
		}
	}
	#endregion
}