﻿using Submissions.Common;
using Submissions.Web.Models.LookupsAjax;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Submissions.Web.Areas.Protrack.Models.Project
{
	public class ChangeProjectStatus
	{
		public ChangeProjectStatus()
		{
			ExpediteInfo = new ExpediteProject();
		}
		public int ProjectId { get; set; }
		public string ProjectName { get; set; }
		public ComplexityRating ComplexityRating { get; set; }
		public string FileType { get; set; }
		[DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}")]
		public DateTime? TargetDate { get; set; }
		public ProjectStatus CurrentProjectStatus { get; set; }
		public ProjectStatus ChangeProjectStatusTo { get; set; }
		public ExpediteProject ExpediteInfo { get; set; }
		public RCSanityCheck RCSanityCheck { get; set; }
		public bool IsAnyTaskOpen { get; set; }
		public String CertificationLab { get; set; }
		public bool CanChangeStatusToDC { get; set; }

		[DisplayName("Add'l EN Involvement Needed?:")]
		public bool ENInvolvementWhileFL { get; set; }
		[DisplayName("No Certification")]
		public bool CertNotNeeded { get; set; }
		[DisplayName("ADM Work Needed")]
		public bool ADMWorkNeeded { get; set; }
		public string ENInvolvementWhileFLNote { get; set; }
		public int? QAOfficeId { get; set; }
		public string QAOffice { get; set; }
	}

	public class RCSanityCheck
	{
		public int ProjectId { get; set; }
		public string ProjectHolder { get; set; }
		public string TargetErrorMessage { get; set; }
		public bool CryptographicStrengthMissing { get; set; }
		public bool IsTestIngPerformedMissing { get; set; }
		public bool IncomingReviewMissing { get; set; }
		public bool StatusUpdated { get; set; }
		public bool JurLinkMissinginEVO { get; set; }
	}

	public class SendTOENRRFromQA
	{
		public SendTOENRRFromQA()
		{
			this.ManagerFilter = new UsersFilter { ManagerSearch = true };
			this.ProjectIds = new List<int>();
		}

		public List<int> ProjectIds { get; set; }
		[Display(Name = "User")]
		public int? ENManagerId { get; set; }
		public string ENManager { get; set; }
		public UsersFilter ManagerFilter { get; set; }
		public string Note { get; set; }
	}
}