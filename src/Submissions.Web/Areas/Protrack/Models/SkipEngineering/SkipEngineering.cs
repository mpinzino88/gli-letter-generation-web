﻿using System.ComponentModel;
using System.Web.Mvc;

namespace Submissions.Web.Areas.Protrack.Models.Project
{
	public class SkipEngeeringVM
	{
		public SkipEngeeringVM()
		{

		}
		public int ProjectId { get; set; }
		public bool IsEngineeringSkipped { get; set; }
		public string CurrentStatus { get; set; }
	}
}