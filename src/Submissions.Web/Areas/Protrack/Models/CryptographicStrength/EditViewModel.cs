﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Submissions.Web.Areas.Protrack.Models.CryptographicStrength
{
	public class EditViewModel
	{
		public EditViewModel()
		{

			this.SubmissionsRecord = new List<SubmissionsRecord>();
			this.CryptographicallyStrongOptions = new List<SelectListItem>();

		}

		public List<SubmissionsRecord> SubmissionsRecord { get; set; }
		public int ProjectId { get; set; }
		public string ProjectName { get; set; }
		public int? defaultCryptograhicId { get; set; }
		public int SubmissionKey { get; set; }
		public byte? BulkCryptographicStrengthId { get; set; }
		public string BulkCryptographicStrength { get; set; }
		public List<SelectListItem> CryptographicallyStrongOptions { get; set; }
		public int? CryptographicId { get; set; }
		public string CryptographicText { get; set; }
	}

	public class SubmissionsRecord
	{
		public SubmissionsRecord()
		{
			this.CryptographicallyStrongOptions = new List<SelectListItem>();
		}

		public int ProjectId { get; set; }
		public int SubmissionKey { get; set; }
		public string FileNumber { get; set; }
		public string IdNumber { get; set; }
		public string GameName { get; set; }
		public string Version { get; set; }
		public string DateCode { get; set; }
		public bool CryptographicAvailable { get; set; }
		public bool Selected { get; set; }
		public int? CryptographicId { get; set; }
		[DisplayFormat(DataFormatString = "{0:d}")]
		public Nullable<System.DateTime> RequestDate { get; set; }
		[DisplayFormat(DataFormatString = "{0:d}")]
		public Nullable<System.DateTime> ReceiveDate { get; set; }
		public int? CryptographicallyStrongId { get; set; }
		public List<SelectListItem> CryptographicallyStrongOptions { get; set; }

	}

}





