﻿using System.Collections.Generic;

namespace Submissions.Web.Areas.Protrack.Models.Task
{
	using Submissions.Common;
	using Submissions.Web.Areas.Protrack.Models.Task.KnockoutModels;
	using Submissions.Web.Models.LookupsAjax;
	using System;
	using System.ComponentModel.DataAnnotations;
	using System.Web.Mvc;

	public class EditViewModel
	{
		public EditViewModel()
		{
			this.DefaultTask = new KOTask();
			this.DefaultTaskDefinition = new KOTaskDefinition();

			this.MathTasks = new List<KOTask>();
			this.EngineeringTasks = new List<KOTask>();
			this.FieldInspectionTasks = new List<KOTask>();
			this.QATasks = new List<KOTask>();
			this.DeactivatedTasks = new List<KOTask>();

			this.UsersFilter = new UsersFilter();
			this.DepartmentsFilter = new DepartmentsFilter { DepartmentIds = new List<int>() { (int)Submissions.Common.Department.ENG, (int)Submissions.Common.Department.MTH, (int)Submissions.Common.Department.INS } };
			this.TasksFilter = new TasksFilter();
			this.TaskTemplatesFilter = new TaskTemplatesFilter { UserIds = new List<int>() };
			this.UserTemplateOwnerFilter = new UsersFilter { TemplateOwner = true };
			this.ProjectFilter = new ProjectFilter { InStatus = new List<string> { ProjectStatus.AA.ToString(), ProjectStatus.AU.ToString(), ProjectStatus.IR.ToString(), ProjectStatus.IT.ToString(), ProjectStatus.OH.ToString() } };
			this.engineeringCheckAll = false;
			this.qualityAssuranceCheckAll = false;
			this.mathCheckAll = false;
			this.fieldInspectionCheckAll = false;
			this.DepartmentId = null;
			this.Department = null;
			this.User = "";
			this.UserId = null;

			this.TransferProjectJurisdictions = new List<SelectListItem>();
			this.JurisdictionsFilter = new JurisdictionsFilter();
			//this.ProjectSearch = new SelectListItem();
		}

		public IList<KOTask> EngineeringTasks { get; set; }
		public IList<KOTask> FieldInspectionTasks { get; set; }
		public IList<KOTask> QATasks { get; set; }
		public IList<KOTask> MathTasks { get; set; }
		public IList<KOTask> DeactivatedTasks { get; set; }

		public string FileNumber { get; set; }
		public int ProjectId { get; set; }
		public string ProjectENStatus { get; set; }
		public DateTime? TargetDate { get; set; }
		public DateTime? OrgTargetDate { get; set; }
		public string OrgTargetDateStr { get; set; }
		public bool ApplyTargetOffSet { get; set; }
		public bool isTransfer { get; set; }
		public bool? onHold { get; set; }
		public bool AllowTaskSave { get; set; }
		public bool AllowTargetDate { get; set; }
		public DateTime ProjectAcceptDate { get; set; }
		public DateTime? QADeliveryDate { get; set; }
		public DateTime ProjectCreateDate { get; set; }
		public double HoursSinceAcceptance { get; set; }
		public bool TaskLockEnforced { get; set; }
		public string AddTemplateReason { get; set; }
		public string GeneralTaskChangeReason { get; set; }
		public string BulkTaskUpdateReason { get; set; }
		public IList<SelectListItem> TransferProjectJurisdictions { get; set; }
		public JurisdictionsFilter JurisdictionsFilter { get; set; }

		[Display(Name = "Department")]
		public int? DepartmentId { get; set; }
		public string Department { get; set; }

		[Display(Name = "User")]
		public int? UserId { get; set; }
		public string User { get; set; }

		[Display(Name = "Task")]
		public int? TaskDefId { get; set; }
		public string TaskDef { get; set; }
		public IList<int> DefaultETATasks { get; set; }

		[Display(Name = "Task Template")]
		public int? TaskTemplateId { get; set; }
		public string TaskTemplate { get; set; }

		[Display(Name = "Template Owner")]
		public int? TemplateOwnerId { get; set; }
		public string TemplateOwner { get; set; }

		public UsersFilter UsersFilter { get; set; }
		public DepartmentsFilter DepartmentsFilter { get; set; }
		public TasksFilter TasksFilter { get; set; }
		public TaskTemplatesFilter TaskTemplatesFilter { get; set; }
		public UsersFilter UserTemplateOwnerFilter { get; set; }
		public ProjectFilter ProjectFilter { get; set; }

		public SelectListItem moveTasksToProject { get; set; }

		public bool engineeringCheckAll { get; set; }
		public bool qualityAssuranceCheckAll { get; set; }
		public bool mathCheckAll { get; set; }
		public bool fieldInspectionCheckAll { get; set; }
		public bool showDynamicsTasks { get; set; }

		public int SourceMathReview { get; set; }
		public int SourceMathComplete { get; set; }

		// Default Object
		public KOTask DefaultTask { get; set; }
		public KOTaskDefinition DefaultTaskDefinition { get; set; }

		//specific to UK Queue
		public bool HasUKTestLab { get; set; }
		public string RequestedDate { get; set; }
		public int? ProjectSearchId { get; set; }
		public string ProjectSearch { get; set; }
		//public SelectListItem ProjectSearch { get; set; }
	}
}