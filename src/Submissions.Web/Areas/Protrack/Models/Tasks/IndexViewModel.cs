﻿using Submissions.Common;
using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Submissions.Web.Areas.Protrack.Models.Task
{
	public class IndexViewModel
	{
		public IndexViewModel()
		{

		}

		public Mode Mode { get; set; }
		public int Id { get; set; }
		public string Name { get; set; }
		public string Description { get; set; }

		[DisplayName("Assign Date")]
		[DisplayFormat(DataFormatString = "{0:d}")]
		public DateTime? AssignDate { get; set; }

		[DisplayName("Start Date")]
		[DisplayFormat(DataFormatString = "{0:d}")]
		public DateTime? StartDate { get; set; }

		[DisplayName("Estimated Complete Date")]
		[DisplayFormat(DataFormatString = "{0:d}")]
		public DateTime? EstimateCompleteDate { get; set; }

		[DisplayName("Complete Date")]
		[DisplayFormat(DataFormatString = "{0:d}")]
		public DateTime? CompleteDate { get; set; }

		[DisplayName("Estimated Hours")]
		public decimal? EstimateHours { get; set; }
		public decimal? EstimateInterval { get; set; }

		public string Note { get; set; }
		public short? Completed { get; set; }
		[DisplayName("Original Estimated Hours")]
		public decimal? OriginalEstimateHours { get; set; }

		[DisplayName("Target Date")]
		[DisplayFormat(DataFormatString = "{0:d}")]
		public DateTime? TargetDate { get; set; }
	}
}