﻿namespace Submissions.Web.Areas.Protrack.Models.Tasks
{
	public class TaskJurisdictionalData
	{
		public bool IsSelected { get; set; }
		public string Name { get; set; }
		public int Id { get; set; }
	}
}