﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Submissions.Web.Areas.Protrack.Models.Task.KnockoutModels
{
	public class KOTaskTemplate
	{
		public KOTaskTemplate()
		{
			this.EngineeringTasks = new List<KOTask>();
			this.QATasks = new List<KOTask>();
			this.MathTasks = new List<KOTask>();
		}

		public int? Id { get; set; }
		public int? UserId { get; set; }
		public string Name { get; set; }
		public string Description { get; set; }
		public bool? Global { get; set; }

		public List<KOTask> EngineeringTasks { get; set; }
		public List<KOTask> QATasks { get; set; }
		public List<KOTask> MathTasks { get; set; }

	}
}