﻿namespace Submissions.Web.Areas.Protrack.Models.Task.KnockoutModels
{
	public class KOTaskDefinition
	{
		public int Id { get; set; }
		public string Name { get; set; }
		public string Description { get; set; }
		public int? DepartmentId { get; set; }
		public decimal? EstimatedHours { get; set; }
		public decimal? EstimatedInterval { get; set; }
		public int? Disabled { get; set; }
		public int? IsReviewTask { get; set; }
		public string DynamicsId { get; set; }
		public string TestScriptNum { get; set; }
		public string TaskDependedOn { get; set; }
		public int? TaskFinalizationPriority { get; set; }
		public int? TaskInt { get; set; }
		public string TaskShortDesc { get; set; }
		public bool ISETATask { get; set; }
		public bool IsAcumaticaSet { get; set; }
	}
}