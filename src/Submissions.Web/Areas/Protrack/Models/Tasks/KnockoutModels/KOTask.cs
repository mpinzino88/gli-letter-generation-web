﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;


namespace Submissions.Web.Areas.Protrack.Models.Task.KnockoutModels
{
	public class KOTask
	{
		public KOTask()
		{
			this.AssociatedJurisdictions = new List<string>();
			this.TaskDefinition = new KOTaskDefinition();
			this.User = new SelectListItem();
			this.TaskDef = new SelectListItem();
			this._destroy = false;
			this._dirty = false;
		}

		public int? Id { get; set; }
		public int? TaskDefinitionId { get; set; }
		public int ProjectId { get; set; }
		public int? UserId { get; set; }
		public int? AssignedUserId { get; set; }
		public string Name { get; set; }
		public string Description { get; set; }
		public int? DepartmentId { get; set; }
		public DateTime? AssignDate { get; set; }
		public DateTime? StartDate { get; set; }
		public DateTime? EstimatedCompleteDate { get; set; }
		public DateTime? CompleteDate { get; set; }
		public decimal? EstimatedHours { get; set; }
		public decimal? InitialEstimatedHours { get; set; }
		public decimal? EstimatedInterval { get; set; }
		public string Note { get; set; }
		public decimal? OriginalEstimatedHours { get; set; }
		public DateTime? TargetDate { get; set; }
		public string TargetDateString { get; set; }
		public int? CreatedById { get; set; }
		public DateTime? CreatedDate { get; set; }
		public Guid? ComponentGroupId { get; set; }
		public bool? ProjectIsTransfer { get; set; }
		public string FileNumber { get; set; }
		public string TaskChangeReason { get; set; }

		public KOTaskDefinition TaskDefinition { get; set; }

		public SelectListItem User { get; set; }
		public SelectListItem TaskDef { get; set; }

		public List<string> AssociatedJurisdictions { get; set; }

		public bool IsCPReviewTaskTiedToJur { get; set; }
		public bool _destroy { get; set; }
		public bool _dirty { get; set; }
		public bool forBulkUpdate { get; set; }
		public bool IsInEditMode { get; set; }
		public bool IsETATask { get; set; }
		public bool ShowETACheckbox { get; set; }
		public bool IsAcumaticaSet { get; set; }
	}
}