﻿using Submissions.Common;
using Submissions.Web.Models.Grids;
using Submissions.Web.Models.LookupsAjax;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Submissions.Web.Areas.Protrack.Models.Project
{
	public class ManageBundleVM
	{
		public ManageBundleVM()
		{
			JurisdictionItemGrid = new ManageBundleProjectJurisdictionItemGrid();
			this.QAFilter = new UsersFilter { Roles = new List<Role> { Role.QAR, Role.QLW, Role.QAS, Role.QAE } };
			this.ENFilter = new UsersFilter { Departments = new List<Department> { Department.ENG, Department.MTH } };
		}
		public int ProjectId { get; set; }
		[DisplayName("Bundle Name")]
		public string Name { get; set; }
		public string ENStatus { get; set; }
		public string QAStatus { get; set; }
		public string Holder { get; set; }
		public BundleType BundleType { get; set; }
		public bool IsTransfer { get; set; }
		[DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}")]
		[DisplayName("Created On")]
		public DateTime? CreateDate { get; set; }
		[DisplayName("Created By")]
		public string CreateBy { get; set; }
		public string QASupervisor { get; set; }
		[DisplayName("Supervisor")]
		public int? QASupervisorId { get; set; }
		public string QAReviewer { get; set; }
		[DisplayName("Reviewer")]
		public int? QAReviewerId { get; set; }
		[DisplayName("Target Date")]
		[DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}")]
		public DateTime? TargetDate { get; set; }
		public DateTime? OrgTargetDate { get; set; }
		public bool AllowTargetSave { get; set; }

		[DisplayName("Expedite")]
		public bool IsExpedite { get; set; }
		public string ExpediteReason { get; set; }
		[DisplayName("CP Req'd")]
		public bool CPReqd { get; set; }
		[DisplayName("Notes For Engineers")]
		public string EngNotes { get; set; }
		[DisplayName("Email EN Supervisor")]
		public int? EngSupervisorId { get; set; }
		public string EngSupervisor { get; set; }

		[DisplayName("Skip Engineering")]
		public bool SkipEngineering { get; set; }

		[DisplayName("No Certification")]
		public bool CertNotNeeded { get; set; }

		[DisplayName("Email EN Supervisor")]
		public DateTime? ENEmailSent { get; set; }
		public string MergedWith { get; set; }
		public int MergedWithId { get; set; }
		public UsersFilter QAFilter { get; set; }
		public UsersFilter ENFilter { get; set; }
		public bool UpdateToENAccepted { get; set; }
		public bool ShowMissingItemsFromOrgCertWarning { get; set; }

		public List<BundleJurisdiction> NewSubmissionJurisdictionsNotIncluded { get; set; }
		public ManageBundleProjectJurisdictionItemGrid JurisdictionItemGrid { get; set; }
		public bool HasTasks { get; set; }
	}
	public class BundleJurisdiction
	{
		public int Id { get; set; }
		public string FileNumber { get; set; }
		public string IDNumber { get; set; }
		public string Version { get; set; }
		public string DateCode { get; set; }
		public string GameName { get; set; }
		public string Jurisdiction { get; set; }
		public string JurisdictionId { get; set; }

		public string SubmissionStatus { get; set; }
		public string JurisdictionStatus { get; set; }

		[DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}")]
		public DateTime? JurRcvdDate { get; set; }
	}
}