﻿using Submissions.Common;
using Submissions.Web.Models.Grids;
using Submissions.Web.Models.LookupsAjax;
using Submissions.Web.Models.Query;
using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace Submissions.Web.Areas.Protrack.Models.Project
{
	public class CreateADDCORBundleVM
	{
		public CreateADDCORBundleVM()
		{
			Filters = new ProjectSearch();
			ProjectGrid = new ADDCORSearchProjectGrid();
			this.QAFilter = new UsersFilter { Roles = new List<Role> { Role.QAR, Role.QLW, Role.QAS, Role.QAE } };
			DefaultTask = new QATask();
			this.taskFilter = new QATasksFilter { IsTaskForBundlesDN = true };
		}
		public ProjectSearch Filters { get; set; }
		public ADDCORSearchProjectGrid ProjectGrid { get; set; }
		public QATask DefaultTask { get; set; }
		public QATasksFilter taskFilter { get; set; }
		public UsersFilter QAFilter { get; set; }
	}

	public class QATask
	{
		public QATask()
		{
			User = new SelectListItem();
			TaskDef = new SelectListItem();
		}
		public int? TaskDefId { get; set; }
		public int? UserId { get; set; }
		public SelectListItem User { get; set; }
		public SelectListItem TaskDef { get; set; }
		public DateTime? TargetDate { get; set; }
	}
}