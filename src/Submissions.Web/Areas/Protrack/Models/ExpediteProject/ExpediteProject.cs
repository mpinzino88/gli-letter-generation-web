﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;


namespace Submissions.Web.Areas.Protrack.Models.Project
{
	public class ExpediteProject
	{
		public string ProjectIds { get; set; }
		[DisplayName("Expedite Reason:")]
		public string ExpediteReason { get; set; }
		[DisplayName("Expedite Note:")]
		public string ExpediteNote { get; set; }
		[DisplayName("Expedite:")]
		public bool Expedite { get; set; }
	}
}