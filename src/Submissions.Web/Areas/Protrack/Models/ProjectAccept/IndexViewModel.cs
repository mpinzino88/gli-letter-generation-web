﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Submissions.Common;
using Submissions.Web.Models.Grids;
using Submissions.Web.Models.LookupsAjax;
using Submissions.Web.Models.Query;


namespace Submissions.Web.Areas.Protrack.Models.ProjectAccept
{
	public class IndexViewModel
	{
		public IndexViewModel()
		{
			ProjectGrid = new SupervisorAcceptProjectGrid();
			ReviewerProjectGrid = new ReviewerAcceptProjectGrid();
			ProjectJurGrid = new ChildProjectJurisdictionItemGrid();
			Filters = new ProjectSearch();
			EngineerFilter = new UsersFilter { DropDownType = UserDropDownType.Task };
		}

		public ProjectSearch Filters { get; set; }
		public SupervisorAcceptProjectGrid ProjectGrid { get; set; }
		public ChildProjectJurisdictionItemGrid ProjectJurGrid { get; set; }
		public ProtrackType ProtrackType { get; set; }
		public ReviewerAcceptProjectGrid ReviewerProjectGrid { get; set; }
		public UsersFilter EngineerFilter { get; set; }
		public int ManagerOwner { get; set; }
		public string AcceptIds { get; set; }
		public string DeclineIds { get; set; }
		public string SkipQAIds { get; set; }
	}

	public class DeclineItem
	{
		public int Id { get; set; }
		public string Reason { get; set; }
	}

	public class AssignEngReviewer
	{
		public int ProjectId { get; set; }
		public int? loginId { get; set; }
	}
	
}