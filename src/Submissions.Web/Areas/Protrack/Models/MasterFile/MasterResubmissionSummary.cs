﻿using Submissions.Core.Models.MasterFileService;

namespace Submissions.Web.Areas.Protrack.Models.MasterFile
{
	public class MasterResubmissionSummaryVM
	{
		public MasterResubmissionSummaryVM()
		{
			Summary = new MasterResubmissionSummary();
		}
		public MasterResubmissionSummary Summary { get; set; }
	}
	public class MasterResubmissionEvaluationDataVM
	{
		public MasterResubmissionEvaluationDataVM()
		{
			EvalData = new FileEvaluationData();
		}
		public FileEvaluationData EvalData { get; set; }
	}

}