﻿using Submissions.Common;
using Submissions.Core.Models.MasterFileService;
using System.Collections.Generic;

namespace Submissions.Web.Areas.Protrack.Models.MasterFile
{
	public class MasterFileRecordSetViewModel
	{
		public MasterFileRecordSetViewModel()
		{
			this.Ids = new List<MasterFileRecord>();
		}
		public string FileNumber { get; set; }
		public List<SubmissionRecord> Records { get; set; }
		public ResubmissionOptions ResubmissionOption { get; set; }
		public string ResubmissionOptionExplanation { get; set; }

		public List<MasterFileRecord> Ids { get; set; }
	}
}