﻿using Submissions.Common.Filters;
using Submissions.Core.Models.MasterFileService;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Submissions.Web.Areas.Protrack.Models.MasterFile
{
	public class EditViewModel
	{
		public EditViewModel()
		{
			this.CurrentSubmissionRecords = new List<SubmissionRecord>();
			this.MasterSubmissionRecords = new List<SubmissionRecord>();
			this.SubmissionFilter = new SubmissionsFilter();
			this.MappingTypes = new List<MappingType>();
			this.Validator = new MasterFileValidator();
		}

		public int ProjectId { get; set; }
		[Display(Name = "Master File Number")]
		public string MasterFileNumber { get; set; }
		public string CurrentFileNumber { get; set; }
		public List<SubmissionRecord> MasterSubmissionRecords { get; set; }
		public List<SubmissionRecord> CurrentSubmissionRecords { get; set; }
		public int SelectedMappingType { get; set; }
		public List<MappingType> MappingTypes { get; set; }
		public SubmissionsFilter SubmissionFilter { get; set; }
		public string RequestUrlReferrer { get; set; }
		public MasterFileValidator Validator { get; set; }
	}
}