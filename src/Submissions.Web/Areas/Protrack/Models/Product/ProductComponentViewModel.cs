﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Submissions.Web.Areas.Protrack.Models.Product
{
	public class ProductComponentViewModel
	{
		public ProductComponentViewModel()
		{
			ProductComponentJurisdictions = new List<ProductJurisdictionalDataViewModel>();
		}
		public int Id { get; set; }
		public string FileNumber { get; set; }
		public string GameName { get; set; }
		public string Version { get; set; }
		public string IdNumber { get; set; }
		public bool ShowJurisdictions { get; set; }
		public IList<ProductJurisdictionalDataViewModel> ProductComponentJurisdictions { get; set; }
	}
}