﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Submissions.Web.Areas.Protrack.Models.Product
{
	public class ProductJurisdictionalDataViewModel
	{
		public int Id { get; set; }
		public string JurisdictionId { get; set; }
		public string Name { get; set; }
		public string Status { get; set; }
		public bool IsUnLinked { get; set; }
		public int? ProductJurisdictionLinkTypeId { get; set; }
		public string ProductJurisdictionLinkType { get; set; }
	}
}