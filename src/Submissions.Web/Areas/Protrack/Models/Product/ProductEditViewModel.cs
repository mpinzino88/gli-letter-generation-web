﻿using Submissions.Common;
using System;
using System.Collections.Generic;

namespace Submissions.Web.Areas.Protrack.Models.Product
{
	public class ProductEditViewModel
	{
		public ProductEditViewModel()
		{
			ManufacturerCodes = new List<BaseSelectListItem>();
			Platforms = new List<BaseSelectListItem>();
			ProductComponentViewModels = new List<ProductComponentViewModel>();
			ProductJurisdictionalDataLinkTypes = new List<ProductJurisdictionalDataLinkTypeViewModel>();
			StatusObject = new Dictionary<string, ComponentSubTable>();
		}
		public int Id { get; set; }
		public string CreatedBy { get; set; }
		public DateTime CreatedOn { get; set; }
		public string Product { get; set; }
		public string Version { get; set; }
		public List<BaseSelectListItem> ManufacturerCodes { get; set; }
		public List<BaseSelectListItem> Platforms { get; set; }
		public Mode Mode { get; set; }
		public IList<ProductComponentViewModel> ProductComponentViewModels { get; set; }
		public bool CanDelete { get; set; }
		public IList<ProductJurisdictionalDataLinkTypeViewModel> ProductJurisdictionalDataLinkTypes { get; set; }
		public Dictionary<string, ComponentSubTable> StatusObject { get; set; }
	}

	public class ProductJurisdictionalDataLinkTypeViewModel
	{
		public int Id { get; set; }
		public string Type { get; set; }
	}

	public class BaseSelectListItem
	{
		public string Id { get; set; }
		public string Text { get; set; }
	}
	public class ComponentSubTable
	{
		public ComponentSubTable()
		{
			ComponentStatuses = new List<ComponentStatus>();
		}
		public bool Show { get; set; }
		public List<ComponentStatus> ComponentStatuses { get; set; }
	}

	public class ComponentStatus
	{
		public string Component { get; set; }
		public string Status { get; set; }
	}
}

