﻿using Submissions.Common;
using Submissions.Common.JQGrid;
using Submissions.Web.Models.Grids;
using Submissions.Web.Models.LookupsAjax;
using Submissions.Web.Models.Query;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Submissions.Web.Areas.Protrack.Models.Project
{
	public class IndexViewModel
	{
		public IndexViewModel()
		{
			this.ProjectGrid = new SupervisorMainProjectGrid();
			this.ReviewerProjectGrid = new ReviewerMainProjectGrid();
			this.GlimpseGrid = new GlimpseGrid();

			this.Filters = new ProjectSearch();
			this.Filters.Managers = new List<SelectListItem>();
			this.Filters.SeniorEngineers = new List<SelectListItem>();
			this.Filters.Manufacturers = new List<SelectListItem>();
			this.Filters.DevelopmentReps = new List<SelectListItem>();
			this.Filters.Laboratories = new List<SelectListItem>();
			this.Filters.QALaboratories = new List<SelectListItem>();
			this.Filters.Reviewers = new List<SelectListItem>();

			this.ManagerFilter = new UsersFilter { ManagerSearch = true };
			this.EngineerFilter = new UsersFilter { DropDownType = UserDropDownType.Task };
			this.ProjectLeads = new UsersFilter { ProjectLeads = true };
			this.MAProjectLeads = new UsersFilter { MAProjectLeads = true };
			this.DevReps = new UsersFilter { DevReps = true };

			this.QAFilter = new UsersFilter { Roles = new List<Role> { Role.QAR, Role.QLW, Role.QAS, Role.QAE }, Permission = Permission.ActAsQAR, RoleOrPermission = true };
			this.ProjectAvailableStatus = new List<ProjectCount>();
			this.GlimpseCustomFilters = new List<ProjectCount>();
		}

		public ProtrackType protrackType { get; set; }
		public ProtrackGridType GridType { get; set; }
		public SupervisorMainProjectGrid ProjectGrid { get; set; }
		public ReviewerMainProjectGrid ReviewerProjectGrid { get; set; }
		public GlimpseGrid GlimpseGrid { get; set; }
		public List<string> GridDateFields { get; set; }// this will tell while sorting, if not it will sort as text
		public List<string> GridIntFields { get; set; }// this will tell while sorting, if not it will sort as text

		public ProjectSearch Filters { get; set; }
		public int? FormFiltersId { get; set; }
		public string FormFiltersName { get; set; }
		public int? ColChooserId { get; set; }

		public List<ProjectCount> ProjectAvailableStatus { get; set; }
		public List<ProjectCount> GlimpseCustomFilters { get; set; }
		public int? TaskCount { get; set; }

		public string CurrentManager { get; set; }
		public int CurrentManagerId { get; set; }
		public string CurrentLaboratory { get; set; }
		public int CurrentLaboratoryId { get; set; }

		public UsersFilter ManagerFilter { get; set; }
		public UsersFilter EngineerFilter { get; set; }
		public UsersFilter QAFilter { get; set; }
		public UsersFilter DevReps { get; set; }
		public UsersFilter ProjectLeads { get; set; }
		public UsersFilter MAProjectLeads { get; set; }
	}

	public class ProjectCount
	{
		public string Status { get; set; }
		public int Count { get; set; }
	}

	public class GlimpseColumnChooser
	{
		public List<ColumnChooserColumn> SelectedColumns { get; set; }
		public List<ColumnChooserColumn> AvailableColumns { get; set; }

		[Display(Name = "Mark Filenumbers having Missing Target Dates. Indicate Filenumber - ")]
		public ColumnChooserCheck ShowMissingTargetDateCheck { get; set; }

		[Display(Name = "File is more than 25 days old. Indicate Age Column - ")]
		public ColumnChooserCheck ShowOldFiles { get; set; }

		[Display(Name = "Task actual hours are greater than estimated hours. Indicate Actual Hours - ")]
		public ColumnChooserCheck ShowActMoreThanEst { get; set; }
	}

	public class ColumnChooserCheck
	{
		public bool ColumnCheck { get; set; }
		public string Color { get; set; }
	}

	public class JiraInfo
	{
		public int ProjectId { get; set; }

		public int TaskId { get; set; }
		public int Count { get; set; }
		public string URL { get; set; }
	}

	public class BundleInfo
	{
		public int ProjectId { get; set; }
		public int TaskId { get; set; }
		public string BundleType { get; set; }
		public string BundleIndicator { get; set; }
		public string QANoteWhileAccept { get; set; }
		public int JurCount { get; set; }
	}
}