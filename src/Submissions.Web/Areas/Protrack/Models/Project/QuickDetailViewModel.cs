﻿using Submissions.Web.Models.Grids;
using System;
using System.ComponentModel.DataAnnotations;

namespace Submissions.Web.Areas.Protrack.Models.Project
{
	public class QuickDetailViewModel
	{
		public QuickDetailViewModel()
		{
			SubmissionDetail = new SubmisionQuickDetail();
			TasksGrid = new QuickDetailTaskGrid();
			ProjectNotes = new QuickProjectNotesGrid();
			TargetDateHistory = new QuickTargetDateGrid();
			COSGrid = new QuickCOSGrid();
			JiraGrid = new QuickDetailJirasGrid();
			OHHistory = new QuickOHHistoryGrid();
			IncomingReview = new IncomingReviewModel();
		}

		public int ProjectId { get; set; }
		public string ProjectName { get; set; }
		public string FileNumber { get; set; }
		public string ProjectStatus { get; set; }
		public int OnHold { get; set; }
		[Display(Name = "Target Date")]
		public DateTime? TargetDate { get; set; }

		[Display(Name = "Est. Eng. Complete Date")]
		public DateTime? ENEstCompleteDate { get; set; }

		[Display(Name = "Est. Test Complete Date")]
		public DateTime? EstTestCompleteDate { get; set; }

		[Display(Name = "Est. Math Complete Date")]
		public DateTime? MAEstCompleteDate { get; set; }
		public bool IsEstMathCompleteDateToBeDetermined { get; set; }

		[Display(Name = "QA Accept Date")]
		public DateTime? QAAcceptDate { get; set; }

		[Display(Name = "Est. Completion Date")]
		public DateTime? EstCompleteDate { get; set; }

		[Display(Name = "Target Date on Track")]
		public bool? IsTargetOnTrack { get; set; }

		[Display(Name = "Estimated Hours")]
		public Decimal? EstHours { get; set; }

		[Display(Name = "Actual Hours")]
		public Decimal? ActHours { get; set; }

		[Display(Name = "Unexpected Hours")]
		public Decimal? UnExpectedHours { get; set; }

		[Display(Name = "Percentage Complete")]
		public Decimal? PercentageComplete { get; set; }

		[Display(Name = "Est. Overage")]
		public Decimal? EstOverage { get; set; }

		[Display(Name = "Non-Billable Hours")]
		public Decimal? NonBillable { get; set; }
		public IncomingReviewModel IncomingReview { get; set; }

		//grids 
		//tasks
		public QuickDetailTaskGrid TasksGrid { get; set; }
		public SubmisionQuickDetail SubmissionDetail { get; set; }

		//Project and Target Date Notes
		public QuickTargetDateGrid TargetDateHistory { get; set; }

		//Project Notes
		public QuickProjectNotesGrid ProjectNotes { get; set; }

		//OH NOtes
		public QuickOHHistoryGrid OHHistory { get; set; }

		//Cos
		public ChangeOfStatusGrid COSGrid { get; set; }

		//JiraList
		public QuickDetailJirasGrid JiraGrid { get; set; }
	}

	public class SubmisionQuickDetail
	{
		public SubmisionQuickDetail()
		{
			SubComponentsGrid = new QuickProjectJurisdictionItemGrid();
		}

		[Display(Name = "Contract Type")]
		public string ContractType { get; set; }
		[Display(Name = "Test Lab")]
		public string TestLab { get; set; }
		[Display(Name = "Contract")]
		public string Contract { get; set; }
		[Display(Name = "Archive Location")]
		public string ArchiveLocation { get; set; }
		[Display(Name = "Owning Entity")]
		public string OwningEntity { get; set; }
		[Display(Name = "Currency")]
		public string Currency { get; set; }
		[Display(Name = "Cert Lab")]
		public string CertLab { get; set; }

		//Component/Jurisdiction grid
		public QuickProjectJurisdictionItemGrid SubComponentsGrid { get; set; }

		//Cos grid
	}
}