﻿using Submissions.Common;
using Submissions.Web.Areas.Protrack.Models.MathQueue;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Submissions.Web.Areas.Protrack.Models.Project
{
	public class EditViewModel
	{
		public int Id { get; set; }
		[Display(Name = "File Number")]
		public string FileNumber { get; set; }

		[Display(Name = "Project Name")]
		public string ProjectName { get; set; }
		[Display(Name = "Project Status")]
		public ProjectStatus Status { get; set; }
		[Display(Name = "Received Date")]
		public DateTime? ReceivedDate { get; set; }
		public DateTime? SubmissionsReceivedDate { get; set; }
		public bool? IsTransfer { get; set; }
		[Display(Name = "Complexity")]
		public ComplexityRating ComplexityRating { get; set; }
		public DateTime? TargetDate { get; set; }
		[Display(Name = "QA Delivery Date")]
		public DateTime? QADeliveryDate { get; set; }

		[Display(Name = "No Certification")]
		public bool NoCertNeeded { get; set; }

		//Engineering
		public int ENManagerId { get; set; }
		[Display(Name = "Eng Manager")]
		public string ENManager { get; set; }
		public int? ENProjectLeadId { get; set; }
		[Display(Name = "Eng Project Lead")]
		public string ENProjectLead { get; set; }
		[Display(Name = "Development Representative")]
		public string DevelopmentRepresentative { get; set; }
		public int? DevelopmentRepresentativeId { get; set; }
		[Display(Name = "Quoted Hours")]
		public double QuotedHours { get; set; }
		[Display(Name = "Estimated Test Complete")]
		public DateTime? EstTestComplete { get; set; }
		[Display(Name = "Estimated Math Complete")]
		public DateTime? EstMathComplete { get; set; }
		[Display(Name = "Estimated Math Complete To Be Determined")]
		public bool IsEstMathCompleteDateToBeDetermined { get; set; }
		[Display(Name = "Estimated Review Complete")]
		public DateTime? EstReviewComplete { get; set; }
		[Display(Name = "Estimated Letter Issue Date")]
		public DateTime? EstLetterIssueDate { get; set; }
		[Display(Name = "Original Project Manager")]
		public int? OriginalENManagerId { get; set; }
		public string OriginalENManager { get; set; }
		//iGaming
		[Display(Name = "Requested Delivery Date")]
		public DateTime? RequestedDeliveryDate { get; set; }
		[Display(Name = "Go Live Date")]
		public DateTime? GoLiveDate { get; set; }
		public bool HasiGamingTestingType { get; set; }
		[Display(Name = "FTP Location"), StringLength(2000)]
		public string FTPLocation { get; set; }
		[Display(Name = "Testing Environment"), StringLength(500)]
		public string TestingEnvironment { get; set; }
		[Display(Name = "Translation Houses")]
		public int[] TranslationHouseIds { get; set; }
		public IList<SelectListItem> TranslationHouses { get; set; }
		public bool ShowGameSubmissionType { get; set; }
		[Display(Name = "Game Submission Type")]
		public int? GameSubmissionTypeId { get; set; }
		public string GameSubmissionType { get; set; }
		[StringLength(4000)]
		public string GameSubmissionTypeInfo { get; set; }

		//QA
		[Display(Name = "QA Reviewer")]
		public string QAReviewer { get; set; }
		public int QAReviewerId { get; set; }
		[Display(Name = "QA Supervisor")]
		public string QASupervisor { get; set; }
		public int QASupervisorId { get; set; }
		[Display(Name = "QA Status")]
		public string QAStatus { get; set; }
		[Display(Name = "NCR Req?")]
		public bool? NCRReq { get; set; }
		[Display(Name = "NCR No")]
		public string NCRNum { get; set; }

		//Math
		[Display(Name = "Math Manager")]
		public string MAManager { get; set; }
		public int? MAManagerId { get; set; }
		public int? MAProjectLeadId { get; set; }
		[Display(Name = "Math Project Lead")]
		public string MAProjectLead { get; set; }
	}

	public class ProjectReview
	{
		public ProjectReview()
		{
			//this.EngineerFilter = new UsersFilter { DropDownType = UserDropDownType.Task };
			this.MathIncomingReview = new MathReviewIndexViewModel();
		}
		public int ProjectId { get; set; }
		public ProtrackType ProtrackType { get; set; }
		public ReviewType ReviewType { get; set; }
		public string ProjectName { get; set; }
		//public List<int> TaskAssignedTo { get; set; }
		//public UsersFilter EngineerFilter { get; set; }
		public MathReviewIndexViewModel MathIncomingReview { get; set; }
		public bool AllowSLA { get; set; }
	}
}