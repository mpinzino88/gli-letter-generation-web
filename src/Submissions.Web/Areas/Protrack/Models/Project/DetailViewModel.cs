﻿using GLI.EFCore.Submission;
using Submissions.Common;
using Submissions.Core.Models.ProjectService;
using Submissions.Web.Models.Grids;
using Submissions.Web.Models.Grids.Protrack;
using Submissions.Web.Models.QueryService;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace Submissions.Web.Areas.Protrack.Models.Project
{
	public class DetailViewModel
	{
		public DetailViewModel()
		{
			this.Project = new ProjectDetail();
			this.NotesGrid = new ProjectDetailNotesGrid();

			this.TasksGrid = new ProjectTaskGrid();
			this.QATaskGrid = new ProjectQATaskGrid();

			this.ForeignTasksGrid = new ProjectTaskForeignGrid();
			this.TasksTransferGrid = new ProjectTaskTransferGrid();

			this.HistoryLogGrid = new ProjectHistoryLogGrid();
			this.JurisdictionItemGrid = new ProjectJurisdictionItemGrid();
			this.MetricsLogGrid = new ProjectMetricsGrid();
			this.QAMetricsLogGrid = new ProjectQAMetricsGrid();
			IncomingReview = new IncomingReviewModel();
			this.SubProjectGrid = new SubProjectGrid();
		}

		public ProjectDetail Project { get; set; }
		public IncomingReviewModel IncomingReview { get; set; }
		public ProjectDetailNotesGrid NotesGrid { get; set; }
		public ProjectTaskGrid TasksGrid { get; set; }
		public ProjectQATaskGrid QATaskGrid { get; set; }
		public ProjectTaskForeignGrid ForeignTasksGrid { get; set; }
		public ProjectTaskTransferGrid TasksTransferGrid { get; set; }
		public ProjectHistoryLogGrid HistoryLogGrid { get; set; }
		public ProjectMetricsGrid MetricsLogGrid { get; set; }
		public ProjectQAMetricsGrid QAMetricsLogGrid { get; set; }
		public ProjectJurisdictionItemGrid JurisdictionItemGrid { get; set; }
		public ProtrackType protrackType { get; set; }
		public int ReceiveDatesNotSame { get; set; }
		public DateTime? SubmissionsReceivedDate { get; set; }
		public int UserDeptId { get; set; }
		public int? CryptographicId { get; set; }
		public string CryptographicText { get; set; }
		public bool CPReviewTasksNeedsSync { get; set; }
		public bool AllowToReopen { get; set; }
		public SubProjectGrid SubProjectGrid { get; set; }
		public bool AllowToClose { get; set; }
		public bool ShowGameSubmissionType { get; set; }
	}

	public class ProjectDetail
	{
		public ProjectDetail()
		{
			this.ENDate = new DepartmentDate();
			this.MathDate = new DepartmentDate();
			this.QADate = new DepartmentDate();
			this.TestingTypes = new List<string>();
		}

		public int Id { get; set; }
		public int bundleId { get; set; }

		// General
		public string Name { get; set; }
		public string FileNumber { get; set; }
		public string Manufacturer { get; set; }
		[Display(Name = "Overall Status")]
		public ProjectStatus Status { get; set; }
		[Display(Name = "Holder")]
		public ProjectHolder Holder { get; set; }
		[Display(Name = "Receive Date")]
		public DateTime? ReceiveDate { get; set; }
		[Display(Name = "Requested Delivery Date")]
		public DateTime? RequestedDeliveryDate { get; set; }
		[Display(Name = "Go Live Date")]
		public DateTime? GoLiveDate { get; set; }
		public DateTime? SubmissionsReceivedDate { get; set; }
		[Display(Name = "Certification Lab")]
		public string CertificationLab { get; set; }
		[Display(Name = "Test Lab")]
		public string Lab { get; set; }
		[Display(Name = "On Hold")]
		public int OnHold { get; set; }
		[Display(Name = "Expedite Project")]
		public bool? IsRush { get; set; }
		public bool IsFavorite { get; set; }
		public string OriginalFile { get; set; }
		public bool? IsTransfer { get; set; }
		public string MergedWith { get; set; }
		public bool HasForeignHours { get; set; }
		public int ReceiveDatesNotSame { get; set; }
		public bool IsEstMathCompleteDateToBeDetermined { get; set; }
		public int ResubmissionProjectId { get; set; }

		//Engineering
		[Display(Name = "Manager Location")]
		public string ENManagerLocation { get; set; }
		public int? ENManagerId { get; set; }
		[Display(Name = "Manager")]
		public string ENManager { get; set; }
		[Display(Name = "Original Manager")]
		public string ENOriginalManager { get; set; }

		public int? MAManagerId { get; set; }
		[Display(Name = "Manager")]
		public string MAManager { get; set; }
		public string MAProjectLeadId { get; set; }
		[Display(Name = "Math Project Lead")]
		public string MAProjectLead { get; set; }
		public string ENProjectLeadId { get; set; }
		[Display(Name = "Project Lead")]
		public string ENProjectLead { get; set; }
		[Display(Name = "Product Type")]
		public string ProductType { get; set; }
		[Display(Name = "Complexity Rating")]
		public string ComplexityRating { get; set; }
		[Display(Name = "Development Representative")]
		public string DevelopmentRepresentative { get; set; }
		[Display(Name = "Quoted Hours")]
		public double QuotedHours { get; set; }
		[Display(Name = "Platforms")]
		public string Platforms { get; set; }
		[Display(Name = "FTP Location")]
		public string FTPLocation { get; set; }
		[Display(Name = "Testing Environment")]
		public string TestingEnvironment { get; set; }
		[Display(Name = "Translation Houses")]
		public string TranslationHouses { get; set; }
		[Display(Name = "Game Submission Type")]
		public string GameSubmissionType { get; set; }
		public string GameSubmissionTypeInfo { get; set; }

		//Jurisdictions
		public List<string> JurisdictionIds { get; set; }
		public bool HasLotoQuebec { get; set; }
		[Display(Name = "Open Issues")]
		public int OpenIssues { get; set; }
		public string OpenIssuesURL { get; set; }
		[Display(Name = "Closed Issues")]
		public int ClosedIssues { get; set; }
		public string ClosedIssuesURL { get; set; }
		//Submission
		[Display(Name = "Sub Open Issues")]
		public int SubOpenIssues { get; set; }
		public string SubOpenIssuesURL { get; set; }
		[Display(Name = "Sub Closed Issues")]
		public int SubClosedIssues { get; set; }
		public string SubClosedIssuesURL { get; set; }
		public string JIRAProjectNameFullTextSearchURL { get; set; }
		public bool HasCPRVJiraForFile { get; set; }
		public IList<string> TestingTypes { get; set; }
		public bool HasiGamingTestingType
		{
			get
			{
				return this.TestingTypes.Any(x => x == TestingType.iGaming.ToString()) || this.TestingTypes.Any(x => x == TestingType.Sportsbook.ToString());
			}
		}

		//QA
		[Display(Name = "Reviewer")]
		public string QAReviewer { get; set; }
		[Display(Name = "Supervisor")]
		public string QASupervisor { get; set; }
		[Display(Name = "QA Status")]
		public BundleStatus? QAStatus { get; set; }
		public int? QAOfficeId { get; set; }
		[Display(Name = "QA Office")]
		public string QAOffice { get; set; }

		//[Display(Name = "NCR Req?")]
		//public bool? NCRReq { get; set; }
		[Display(Name = "ADM Work Needed?")]
		public bool ADMWorkNeeded { get; set; }

		[Display(Name = "QFI No")]
		public string QFINum { get; set; }

		[Display(Name = "CP Req'd?")]
		public string CPReq { get; set; }
		[Display(Name = "Letter Writing Started ")]
		public DateTime? LetterWritingStarted { get; set; }
		public bool? QAResubmission { get; set; }
		[Display(Name = "Add'l EN Involvement Needed?")]
		public bool ENInvolvementWhileFL { get; set; }
		[Display(Name = "Notes")]
		public string ENInvolvementWhileFLNotes { get; set; }

		//Targets
		[Display(Name = "Target Date")]
		public DateTime? TargetDate { get; set; }
		public DateTime? OrgTargetDate { get; set; }
		[Display(Name = "Req. QA Delivery Date")]
		public DateTime? QADeliveryDate { get; set; }
		[Display(Name = "Target Date With On Hold")]
		public DateTime? TargetDateWithOH { get; set; }

		public DateTime? CompleteDate { get; set; }
		public int? TotalOHDays { get; set; }
		public bool? JurMissingTarget { get; set; }

		// Dates
		public DepartmentDate ENDate { get; set; }
		public DepartmentDate MathDate { get; set; }
		public DepartmentDate QADate { get; set; }

		//Resubmission Info
		public Resubmission FileResubmission { get; set; }
		public bool IsMasterFile { get; set; }

		//Associated Projects
		public List<AssociatedProject> AssociatedProjects { get; set; }

		//Merge information
		public bool IsMerged { get; set; }
		public List<AssociatedProject> MergedProjects { get; set; }

		//Misc Info
		[Display(Name = "Work Order Number(WON)")]
		public List<string> WorkOrderNumbers { get; set; }
		public bool ViewSLA { get; set; }
		public bool ViewCryptographicStrength { get; set; }
		public int? CryptographicId { get; set; }
		public string CryptographicText { get; set; }
		public int QATasksCount { get; set; }

		[Display(Name = "No Certification Needed")]
		public bool NoCertNeeded { get; set; }

		public DateTime? UpdateAcceptDate { get; set; }
		public List<tbl_Reviews> Reviews { get; set; }
		public int EngReviewCount
		{
			get
			{
				if (Reviews == null || !Reviews.Any())
				{
					return 0;
				}
				return Reviews.Count(review => review.DepartmentId == (int)Department.ENG);
			}
		}
		public int MathReviewCount
		{
			get
			{
				if (Reviews == null || !Reviews.Any())
				{
					return 0;
				}
				return Reviews.Count(review => review.DepartmentId == (int)Department.MTH);
			}
		}
		public int QaReviewCount
		{
			get
			{
				if (Reviews == null || !Reviews.Any())
				{
					return 0;
				}
				return Reviews.Count(review => review.DepartmentId == (int)Department.QA);
			}
		}

	}

	public class DepartmentDate
	{
		public DateTime? Accepted { get; set; }
		public DateTime? EstimateTestCompleted { get; set; }
		public DateTime? TestCompleted { get; set; }
		public DateTime? EstimateCompleted { get; set; }
		public DateTime? Completed { get; set; }

	}

	public class TaskHoursViewModel
	{
		public int ProjectId { get; set; }
		public List<UnassignedTaskHoursDetail> Billable { get; set; }
		public List<UnassignedTaskHoursDetail> NonBillable { get; set; }
		public List<UnassignedTaskHoursDetail> NoCharge { get; set; }
		public List<UnassignedTaskHoursDetail> Assigned { get; set; }
		public List<UnassignedTaskHoursDetail> NonAssigned { get; set; }
		public decimal? TotalEstAssigned { get; set; }
		public decimal? TotalActualAssigned { get; set; }
		public decimal? TotalActualBillable { get; set; }
		public decimal? TotalActualNonBillable { get; set; }
		public decimal? TotalActualNoCharge { get; set; }
		public decimal? TotalActual { get; set; }
		public decimal? TotalActualUnassigned { get; set; }
		public decimal? BudgetPen { get; set; }

		public decimal? MathEst { get; set; }
		public decimal? MathAct { get; set; }
		public decimal? MathPen { get; set; }
	}

	public class IncomingReviewModel
	{
		public DateTime? Created { get; set; }
		public DateTime? Completed { get; set; }
		public string CompletedBy { get; set; }
		public bool Required { get; set; }
	}
}