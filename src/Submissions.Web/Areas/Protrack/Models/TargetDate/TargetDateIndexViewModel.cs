﻿using Submissions.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;
using Trirand.Web.Mvc;


namespace Submissions.Web.Areas.Protrack.Models
{
	#region Model

	public class TargetDateIndexViewModel
	{
		public TargetDateIndexViewModel()
		{
			ApplyOffSet = true;
		}
		public ProjectJurisdictionsGrid JurisdictionsGrid { get; set; }
		public TargetDateEditViewModel TargetDateInfo { get; set; }
		public List<JurisdictionTargetOffset> ProjectJurisdictionOffset { get; set; }
		public bool TargetDateSync { get; set; }
		public bool ApplyOffSet { get; set; }
		public bool AllowUpdateOptions { get; set; }
	}

	public class TargetDateEditViewModel
	{
		public int ProjectID { get; set; }

		public string PrimaryList { get; set; }

		public int ProjectCertificationLabId { get; set; }

		[DisplayName("Project Name")]
		public string ProjectName { get; set; }

		public string ProjectStatus { get; set; }

		[DisplayName("Current Target Date")]
		[DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}")]
		public DateTime? TargetDate { get; set; }

		[DisplayName("New Target Date:")]
		[DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}")]
		[Required(ErrorMessage = "Target Date is required.")]
		public DateTime? NewTargetDate { get; set; }

		// [DisplayName("New Original Target Date:")]
		//[Required(ErrorMessage = "Detail Reason is required.")]
		//[DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}")]
		//public DateTime? NewOrgTargetDate { get; set; }

		[DisplayName("Detail:")]
		[Required(ErrorMessage = "Detail Reason is required.")]
		public string Detail { get; set; }

		[DisplayName("Reason:")]
		[Required(ErrorMessage = "Reason is required.")]
		public string Reason { get; set; }

		public int ReasonId { get; set; }

		[Required(ErrorMessage = "You must select one.")]
		[DisplayName("Apply Changes:")]
		public int ApplyToProject { get; set; }

		[Required(ErrorMessage = "You must select one.")]
		[DisplayName("Revise Date:")]
		public TargetDateReviseMode ReviseMode { get; set; }

		public bool AllowEditOriginalDate { get; set; }
	}

	public class BatchProjectTarget
	{
		public string ProjectIds { get; set; }

		[DisplayName("New Target Date:")]
		[DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}")]
		[Required(ErrorMessage = "Target Date is required.")]
		public DateTime? NewTargetDate { get; set; }

		[DisplayName("Detail:")]
		[Required(ErrorMessage = "Detail Reason is required.")]
		public string Detail { get; set; }

		[DisplayName("Reason:")]
		[Required(ErrorMessage = "Reason is required.")]
		public string Reason { get; set; }

		public int ReasonId { get; set; }

		[Required(ErrorMessage = "You must select one.")]
		[DisplayName("Apply Changes:")]
		public int ApplyToProject { get; set; }

		[Required(ErrorMessage = "You must select one.")]
		[DisplayName("Revise Date:")]
		public TargetDateReviseMode ReviseMode { get; set; }

		public bool AllowEditOriginalDate { get; set; }
	}

	public class JurisdictionTargetOffset
	{
		[DisplayName("Jurisdiction")]
		public string Jurisdiction { get; set; }
		[DisplayName("Offset Days")]
		public int Offset { get; set; }
		[DisplayName("Reason")]
		public string Reason { get; set; }
	}

	public class ViewHistory
	{
		public string ProjectName { get; set; }
		[DisplayName("File:")]
		public string FileNumber { get; set; }
		[DisplayName("ID Number:")]
		public string IDNumber { get; set; }
		[DisplayName("Game Name:")]
		public string GameName { get; set; }
		[DisplayName("Version:")]
		public string Version { get; set; }
		[DisplayName("Date Code:")]
		public string DateCode { get; set; }
		[DisplayName("Jurisdiction:")]
		public string Jurisdiction { get; set; }
		public List<TargetHistory> RevisionHistory { get; set; }

		public string OriginalTargetDateChangedReason { get; set; }

	}

	public class TargetHistory
	{
		[DisplayName("Modified Date")]
		[DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}")]
		public DateTime ModifiedDate { get; set; }

		[DisplayName("Target Date")]
		[DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}")]
		public DateTime? TargetDate { get; set; }

		[DisplayName("Modified By")]
		public string ModifiedBy { get; set; }

		[DisplayName("Reason")]
		public string Reason { get; set; }

		[DisplayName("Detail")]
		public string Note { get; set; }

		public Boolean IsDeleted { get; set; }

	}

	public class ProjectJurisdictionViewModel
	{
		public int ProjectID { get; set; }
		public int JurisdictionId { get; set; }
		public string FileNumber { get; set; }
		public string IDNumber { get; set; }
		public string Version { get; set; }
		public string DateCode { get; set; }
		public string GameName { get; set; }
		public string Jurisdiction { get; set; }
		[DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}")]
		public DateTime? JurisdictionRcvdDate { get; set; }
		public string JurisdictionStatus { get; set; }
		[DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}")]
		public DateTime? TargetDate { get; set; }
		[DisplayFormat(DataFormatString = "{0:MM/dd/yyyy}")]
		public DateTime? OrgTargetDate { get; set; }

	}
	#endregion

	#region gridModel
	public class ProjectJurisdictionsGrid
	{
		public JQGrid Grid { get; set; }

		public ProjectJurisdictionsGrid()
		{
			Grid = new JQGrid
			{
				Columns = new List<JQGridColumn>
				{

					new JQGridColumn
					{
						DataField = "JurisdictionId",
						PrimaryKey = true,
						Editable = false,
						Visible = false
					},
					new JQGridColumn
					{
						DataField = "FileNumber",
						HeaderText = "File Number",
						Width = 40,
						Formatter = new CustomFormatter{FormatFunction = "fileNumberLink"},
						DataType = typeof(string),
						SearchToolBarOperation = SearchOperation.BeginsWith

					},
					new JQGridColumn
					{
						DataField = "IDNumber",
						HeaderText = "ID Number",
						Width = 50,
						DataType = typeof(string),
						SearchToolBarOperation = SearchOperation.BeginsWith
					},
					new JQGridColumn
					{
						DataField = "GameName",
						HeaderText = "Game Name",
						Width = 50,
						DataType = typeof(string),
						SearchToolBarOperation = SearchOperation.BeginsWith
					},
					new JQGridColumn
					{
						DataField = "Version",
						HeaderText = "Version",
						Width = 20,
						DataType = typeof(string),
						SearchToolBarOperation = SearchOperation.BeginsWith
					},
					new JQGridColumn
					{
						DataField = "DateCode",
						HeaderText = "Date Code",
						Width = 25,
						DataType = typeof(string),
						SearchToolBarOperation = SearchOperation.BeginsWith
					},
					new JQGridColumn
					{
						DataField = "Jurisdiction",
						HeaderText = "Jurisdiction",
						Width = 40,
						DataType = typeof(string),
						Searchable = false,
					},
					new JQGridColumn
					{
						DataField = "JurisdictionRcvdDate",
						HeaderText = "Rcvd date",
						Width = 30,
						DataFormatString = "{0:MM/dd/yyyy}",
						Searchable = false,
					},
					new JQGridColumn
					{
						DataField = "JurisdictionStatus",
						HeaderText = "Status",
						Width = 15,
						DataType = typeof(string),
						Searchable = false,
					},
					new JQGridColumn
					{
						DataField = "OrgTargetDate",
						HeaderText = "Org. TargetDate",
						Width = 40,
						Searchable = false,
						DataFormatString = "{0:MM/dd/yyyy}"
					},
					new JQGridColumn
					{
						DataField = "TargetDate",
						HeaderText = "TargetDate",
						Width = 30,
						Searchable = true,
						DataFormatString = "{0:MM/dd/yyyy}",
						DataType = typeof(DateTime),
						SearchToolBarOperation = SearchOperation.IsEqualTo
					},
					new JQGridColumn
					{
						DataField = "TargetDate",
						HeaderText = "View History",
						Width = 30,
						Searchable = false,
						Formatter = new CustomFormatter{FormatFunction = "ViewTargetHistory"}
					}
				}
			};

			Grid.ID = "JurisdictionsGrid";
			Grid.DataUrl = new UrlHelper(((MvcHandler)HttpContext.Current.Handler).RequestContext).Action("LoadProjectJurisdictionsGrid");
			Grid.AutoWidth = true;
			Grid.Width = Unit.Percentage(100);
			Grid.Height = Unit.Percentage(100);
			Grid.MultiSelect = true;

			Grid.ToolBarSettings = new ToolBarSettings
			{
				ShowRefreshButton = true,
				ShowSearchToolBar = true,
				ToolBarPosition = ToolBarPosition.Top
			};
			Grid.PagerSettings = new Trirand.Web.Mvc.PagerSettings
			{
				PageSize = 1000,
				PageSizeOptions = "[]"
			};
			Grid.AppearanceSettings = new AppearanceSettings
			{
				AlternateRowBackground = true,
				HighlightRowsOnHover = true,
				ShowFooter = false
			};

		}

	}
	#endregion

}