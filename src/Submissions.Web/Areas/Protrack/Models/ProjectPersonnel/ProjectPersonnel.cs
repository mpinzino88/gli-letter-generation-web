﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel;

namespace Submissions.Web.Areas.Protrack.Models.Project
{
	public class ProjectPersonnel
	{
		public string ProjectIds { get; set; }
		[DisplayName("User")]
		public int? UserId { get; set; }
	
		public string User { get; set; }
		public ProjectPersonnelType PersonnelType { get; set; }
	}
}