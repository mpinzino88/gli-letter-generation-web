﻿using Submissions.Common;
using Submissions.Web.Models.Grids;
using Submissions.Web.Models.LookupsAjax;
using Submissions.Web.Models.Query;
using System.Collections.Generic;

namespace Submissions.Web.Areas.Protrack.Models.MyTasks
{
	public class IndexViewModel
	{
		public IndexViewModel()
		{
			this.EngMyTaskGrid = new EngTaskGrid();
			this.QAMyTaskGrid = new QATaskGrid();
			//this.GlimpseGrid = new GlimpseGrid();

			this.Filters = new TaskSearch();

			this.QAFilter = new UsersFilter { Roles = new List<Role> { Role.QAR, Role.QLW, Role.QAS, Role.QAE } };
			this.EngFilter = new UsersFilter { Roles = new List<Role> { Role.ENS, Role.EN, Role.MAS, Role.MAT } };
			this.TaskAvailableStatus = new List<TaskCount>();
		}

		public ProtrackType protrackType { get; set; }
		public ProtrackGridType GridType { get; set; }
		public EngTaskGrid EngMyTaskGrid { get; set; }
		public QATaskGrid QAMyTaskGrid { get; set; }
		//public GlimpseGrid GlimpseGrid { get; set; }
		public TaskSearch Filters { get; set; }
		public UsersFilter QAFilter { get; set; }
		public UsersFilter EngFilter { get; set; }
		public List<TaskCount> TaskAvailableStatus { get; set; }
	}

	public class TaskCount
	{
		public string Status { get; set; }
		public int Count { get; set; }
	}

	public class CompleteTaskVM
	{
		public string ProjectName { get; set; }
		public int TaskId { get; set; }
		public string TaskName { get; set; }
		public string TaskNote { get; set; }
		public bool TaskEmailSupervisor { get; set; }
		public bool TaskEmailAll { get; set; }
		public decimal? EstHours { get; set; }
		public decimal? ActHours { get; set; }
	}

	public class TaskHrVM
	{
		public int? ProjectId { get; set; }
		public int? ProtrackTaskId { get; set; }
		public string DynamicsId { get; set; }
		public string Name { get; set; }
		public string User { get; set; }
		public decimal? EstimateHours { get; set; }
		public decimal? OrigEstHours { get; set; }
		public decimal? ActualHours { get; set; }
	}
}