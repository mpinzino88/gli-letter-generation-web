﻿using Submissions.Web.Models.Query;
using Submissions.Web.Models.Query.Protrack;
using System;
using System.Collections.Generic;

namespace Submissions.Web.Areas.Protrack.Models.WorkloadManager
{
	public class ResourceFinderViewModel
	{
		public ResourceFinderViewModel()
		{
			this.FormFilterSection = FormFilterSection.ResourceFinder;
			this.Filters = new ResourceFinderSearch();
		}

		public FormFilterSection FormFilterSection { get; set; }
		public ResourceFinderSearch Filters { get; set; }
		public CurrentFilter CurrentFilter { get; set; }
	}

	public class ResourceEmailViewModel
	{
		public ResourceEmailViewModel()
		{
			this.Tasks = new List<ResourceFinderTask>();
		}

		public int RequestUserId { get; set; }
		public int? ProjectId { get; set; }
		public string ProjectName { get; set; }
		public IList<ResourceFinderTask> Tasks { get; set; }
		public DateTime? RequestStartDate { get; set; }
		public string AdditionalInfo { get; set; }
	}

	public class ResourceFinderTask
	{
		public int? TaskDefinitionId { get; set; }
		public string TaskDefinition { get; set; }
		public decimal EstimateHours { get; set; }
	}
}