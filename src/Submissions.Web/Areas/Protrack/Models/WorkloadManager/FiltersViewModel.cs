﻿using System.Collections.Generic;
using System.Web.Mvc;

namespace Submissions.Web.Areas.Protrack.Models.WorkloadManager
{
	public class FiltersViewModel
	{
		public FiltersViewModel()
		{
			this.ExcludeDepartments = new List<TaskDepartmentViewModel>();
			this.ExcludeResources = new List<SelectListItem>();
			this.ExcludeTasks = new List<TaskDefinitionViewModel>();
		}

		public IList<TaskDepartmentViewModel> ExcludeDepartments { get; set; }
		public string[] ExcludeResourceIds { get; set; }
		public IList<SelectListItem> ExcludeResources { get; set; }
		public IList<TaskDefinitionViewModel> ExcludeTasks { get; set; }
		public string IncludeTasksBeginWith { get; set; }
		public string IncludeUnassignedTasksForENProjectLead { get; set; }
		public int? IncludeUnassignedTasksForENProjectLeadId { get; set; }
	}

	public class TaskDepartmentViewModel
	{
		public int Id { get; set; }
		public string Department { get; set; }
		public bool Selected { get; set; }
	}

	public class TaskDefinitionViewModel
	{
		public int? Id { get; set; }
		public string Definition { get; set; }
	}
}