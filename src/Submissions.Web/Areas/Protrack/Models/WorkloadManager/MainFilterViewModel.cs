﻿using Submissions.Web.Models.LookupsAjax;

namespace Submissions.Web.Areas.Protrack.Models.WorkloadManager
{
	public class MainFilterViewModel
	{
		public MainFilterViewModel()
		{
			this.ManagerFilter = new UsersFilter { ProjectManagers = true };
		}

		public int? ManagerId { get; set; }
		public string Manager { get; set; }
		public string ManagerDepartment { get; set; }
		public UsersFilter ManagerFilter { get; set; }
		public int? TeamId { get; set; }
		public string Team { get; set; }
		public int? SupplierFocusId { get; set; }
		public string SupplierFocus { get; set; }
		public int? UserLocationId { get; set; }
		public string UserLocation { get; set; }
	}
}