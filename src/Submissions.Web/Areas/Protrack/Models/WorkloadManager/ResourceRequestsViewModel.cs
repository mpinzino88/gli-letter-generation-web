﻿using System;
using System.Collections.Generic;

namespace Submissions.Web.Areas.Protrack.Models.WorkloadManager
{
	public class ResourceRequestsViewModel
	{
		public ResourceRequestsViewModel()
		{
			this.MyRequests = new List<MyRequest>();
			this.OtherRequests = new List<OtherRequest>();
		}

		public IList<MyRequest> MyRequests { get; set; }
		public IList<OtherRequest> OtherRequests { get; set; }
	}

	public class MyRequest
	{
		public int Id { get; set; }
		public DateTime RequestDate { get; set; }
		public string Manager { get; set; }
		public string Resource { get; set; }
		public int ProjectId { get; set; }
		public string ProjectName { get; set; }
		public DateTime? RequestStartDate { get; set; }
		public string AdditionalInfo { get; set; }
		public IList<string> Tasks { get; set; }
	}

	public class OtherRequest
	{
		public int Id { get; set; }
		public DateTime RequestDate { get; set; }
		public string Manager { get; set; }
		public string Resource { get; set; }
		public int ProjectId { get; set; }
		public string ProjectName { get; set; }
		public DateTime? RequestStartDate { get; set; }
		public string AdditionalInfo { get; set; }
		public IList<string> Tasks { get; set; }
	}
}