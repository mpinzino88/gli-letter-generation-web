﻿using System;
using System.Collections.Generic;

namespace Submissions.Web.Areas.Protrack.Models.WorkloadManager
{
	public class WorkloadBalancerViewModel
	{
		public WorkloadBalancerViewModel()
		{
			this.Resources = new List<ResourceViewModel>();
			this.Holidays = new List<HolidayViewModel>();
			this.DTORequests = new List<DTORequestViewModel>();
			this.Tasks = new List<TaskViewModel>();
			this.Favorites = new List<FavoriteViewModel>();
			this.Filters = new FiltersViewModel();
		}

		public bool CanEdit { get; set; }
		public int SessionUserId { get; set; }
		public MainFilterViewModel MainFilter { get; set; }
		public CalendarOptionsViewModel CalendarOptions { get; set; }
		public ResourceRequestCountsViewModel ResourceRequest { get; set; }
		public IList<ResourceViewModel> Resources { get; set; }
		public IList<HolidayViewModel> Holidays { get; set; }
		public IList<DTORequestViewModel> DTORequests { get; set; }
		public IList<TaskViewModel> Tasks { get; set; }
		public IList<FavoriteViewModel> Favorites { get; set; }
		public FiltersViewModel Filters { get; set; }
	}

	public class CalendarOptionsViewModel
	{
		public int Days { get; set; }
	}

	public class ResourceRequestCountsViewModel
	{
		public int OtherRequests { get; set; }
		public int MyRequests { get; set; }
	}

	public class ResourceViewModel
	{
		public ResourceViewModel()
		{
			this.WorkdaySchedule = new List<WorkdayScheduleViewModel>();
		}

		public int? Id { get; set; }
		public string Name { get; set; }
		public int? LocationId { get; set; }
		public string Type { get; set; }
		public decimal WorkdayHours { get; set; }
		public int? UnavailableId { get; set; }
		public DateTime? UnavailableStartDate { get; set; }
		public DateTime? UnavailableEndDate { get; set; }
		public string InterofficeAssignment { get; set; }
		public int? ManagerId { get; set; }
		public string ManagerName { get; set; }
		public IList<WorkdayScheduleViewModel> WorkdaySchedule { get; set; }
		public int Level { get; set; }
	}

	public class HolidayViewModel
	{
		public int LocationId { get; set; }
		public string Name { get; set; }
		public string Date { get; set; }
	}

	public class DTORequestViewModel
	{
		public int UserId { get; set; }
		public DateTime Date { get; set; }
		public double Hours { get; set; }
	}

	public class TaskViewModel
	{
		public long Id { get; set; }
		public int TaskId { get; set; }
		public string Task { get; set; }
		public string TaskDepartment { get; set; }
		public int? UserId { get; set; }
		public int? UserIdOld { get; set; }
		public string UserName { get; set; }
		public int ProjectId { get; set; }
		public int ProjectManagerId { get; set; }
		public string ProjectManager { get; set; }
		public string FileNumber { get; set; }
		public string Status { get; set; }
		public string CommitStatus { get; set; }
		public DateTime? EstimateStartDate { get; set; }
		public DateTime? EstimateCompleteDate { get; set; }
		public DateTime? ProjectAcceptDate { get; set; }
		public DateTime? ProjectTargetDate { get; set; }
		public decimal EstimateHours { get; set; }
		public decimal EstimateHoursOld { get; set; }
		public decimal ActualHours { get; set; }
		public decimal ActualHoursOld { get; set; }
		public decimal RemainingHours { get; set; }
		public int OnHold { get; set; }
		public bool LentOrBorrowed { get; set; }
		public string Style { get; set; }
		public bool WeekendWork { get; set; }
		public bool WeekendWorkOld { get; set; }
	}

	public class WorkdayScheduleViewModel
	{
		public int? UserId { get; set; }
		public DayOfWeek DayOfWeek { get; set; }
		public decimal Hours { get; set; }
	}

	public class FavoriteViewModel
	{
		public int? UserId { get; set; }
		public string Name { get; set; }
	}
}