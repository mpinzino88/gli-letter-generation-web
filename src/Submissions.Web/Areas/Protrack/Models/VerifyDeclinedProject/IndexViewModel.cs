﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Submissions.Web.Models.Grids;
using Submissions.Web.Models.Query;

namespace Submissions.Web.Areas.Protrack.Models.VerifyDeclinedProject
{
	public class IndexViewModel
	{
		public IndexViewModel()
		{
			ProjectGrid = new DeclinedProjectGrid();
			Filters = new ProjectSearch();
		}
		public DeclinedProjectGrid ProjectGrid { get; set; }
		public ProjectSearch Filters { get; set; }
	}
}