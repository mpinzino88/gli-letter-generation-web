﻿using Submissions.Common;
using Submissions.Web.Models.Grids;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;


namespace Submissions.Web.Areas.Protrack.Models.MathQueue
{
	public class IndexViewModel
	{
		public IndexViewModel()
		{
			Filters = new MathReviewTaskSearch();
			ReviewGrid = new MathReviewGrid();
			ReviewGridDN = new MathReviewGridDN();
			this.Filters.Laboratories = new List<SelectListItem>();
			//EngineerFilter = new UsersFilter { DropDownType = UserDropDownType.Task };
			TasksGrid = new MathTasksStatusGrid();
		}
		public MathReviewTaskSearch Filters { get; set; }
		public MathReviewGrid ReviewGrid { get; set; }
		public MathGridType MathGridType { get; set; }
		public string selIds { get; set; }
		//public UsersFilter EngineerFilter { get; set; }
		public MathTasksStatusGrid TasksGrid { get; set; }
		public MathReviewGridDN ReviewGridDN { get; set; }
	}

	public class TaskAssignVM
	{
		public int ProjectId { get; set; }
		public int TaskId { get; set; }
		public int AssignTo { get; set; }
		public Decimal? EstHrs { get; set; }
		public DateTime? TaskTarget { get; set; }
	}

	public class MathReviewTaskSearch
	{
		[Display(Name = "Office")]
		public int? LaboratoryId { get; set; }
		public string Laboratory { get; set; }

		public List<string> LaboratoryIds { get; set; }
		public List<SelectListItem> Laboratories { get; set; }
		[Display(Name = "Math Queue Groups")]
		public List<string> ManufacturerGroupIds { get; set; }
		public List<SelectListItem> ManufacturerGroups { get; set; }

		[Display(Name = "Exclude Math Queue Groups")]
		public bool ExcludeMathQueueGroups { get; set; }

		[Display(Name = "Manufacturer")]
		public string ManufacturerShort { get; set; }
		public string Manufacturer { get; set; }

		[Display(Name = "FileNumber")]
		public string FileNumber { get; set; }
		[Display(Name = "Math Manager")]
		public string MAManager { get; set; }
		public int? MAManagerId { get; set; }
		[Display(Name = "Math Analyst")]
		public int MathAnalystId { get; set; }
		public string MathAnalyst { get; set; }

		public MathReviewStatus ReviewStatus { get; set; }
		public TaskStatusFilter TaskStatus { get; set; }
	}

	public class ReviewIdVM
	{
		public int ReviewId { get; set; }
		public MathReviewType ReviewType { get; set; }
		public int AssignTo { get; set; }
		public int EstHrs { get; set; }
		public DateTime? TaskTarget { get; set; }
	}

	public class MathReviewIndexViewModel
	{
		public MathReviewIndexViewModel()
		{
			MathReviews = new List<MathReview>();
			//MathInfo = new MathReviewInfo();
		}
		public int ProjectId { get; set; }
		public string ProjectName { get; set; }
		public List<MathReview> MathReviews { get; set; }
		public MathListType SourceListType { get; set; }
		public bool ThemeAvailableForMathComplete { get; set; }
		//public int MathReviewId { get; set; }
		//public MathReviewInfo MathInfo { get; set; }
	}

	public class ListValue
	{
		public int Id { get; set; }
		public string Text { get; set; }
	}

	public class MathReview
	{
		public int Id { get; set; }
		public DateTime? Submitted { get; set; }
		public string Submitter { get; set; }
		public string Theme { get; set; }
		public string AnalysisType { get; set; }
		public string Status { get; set; }
		public string ModifiedBy { get; set; }
		public string ForeignProject { get; set; }
	}

	//Math Submit form
	public class MathReviewInfo
	{
		public string ProjectName { get; set; }
		public int ProjectId { get; set; }
		public string Location { get; set; }
		public int Id { get; set; }
		public int? ReviewTaskId { get; set; }
		public string AssignedTo { get; set; }

		[Required(ErrorMessage = "Please select Analysis Type.")]
		public string AnalysisType { get; set; }

		[Required(ErrorMessage = "Please select Program Type.")]
		public string ProgramType { get; set; }

		[Required(ErrorMessage = "Please select Game Type.")]
		public string GameType { get; set; }

		public DateTime? CompletionRequested { get; set; }
		public string Note { get; set; }

		[DisplayName("Project Name")]
		public string Themes { get; set; }
		public string PreviousThemes { get; set; }

		public int? MathModelAffected { get; set; }
		public string MathModelNote { get; set; }

		public string Status { get; set; }
		public string StatusNote { get; set; }
		public bool? IsRush { get; set; }

		public int? SubmitterId { get; set; }
		public string Submitter { get; set; }
		public string Modifier { get; set; }
		public string CompletedBy { get; set; }

		public string SubmitterManager { get; set; }
		public DateTime? Submitted { get; set; }
		public string PreviousFiles { get; set; }
		public List<ListValue> AllProgramIds { get; set; }
		public List<ListValue> AllGames { get; set; }

		public MathListType SourceType { get; set; }
		public bool IsLocked { get; set; }

		public List<string> AllThemes { get; set; }
		public string ForeignProject { get; set; }

		//specific to UK Queue
		public bool HasUKTestLab { get; set; }
		public DateTime? EstimateCompleteDate { get; set; }
		public string InternalURL { get; set; }
		public string TestHarness { get; set; }
	}

	public class MathCompleteVM
	{
		public int Id { get; set; }
		public int ProjectId { get; set; }
		public string ProjectName { get; set; }
		public string LoadPrevProject { get; set; }
		public string Location { get; set; }
		public string Theme { get; set; }
		public int? ReviewId { get; set; }
		public string Note { get; set; }
		public string Status { get; set; }
		public List<ListValue> AllThemes { get; set; }
		public List<ListValue> AllProgramIds { get; set; }
		public List<ListValue> AllGames { get; set; }
		public List<MathFeatureItemsVM> FeatureItems { get; set; }
		public string SelFeatureItems { get; set; }
		public string TopAwardOccursIn { get; set; }
		public bool? NeedsReview { get; set; }
		public bool? IsReviewRush { get; set; }
		public bool IsPeerReview { get; set; }
		public MathListType SourceType { get; set; }

		public bool IsLocked { get; set; }

		public string Submitter { get; set; }
		public string Modifier { get; set; }
		public string CompletedBy { get; set; }
		public string ForeignProject { get; set; }
		public DateTime? CompleteDate { get; set; }
	}

	public class DictionaryObject
	{
		public string Feature { get; set; }
		public List<int> SelVal { get; set; }
	}

	public class MathFeatureItemsVM
	{
		public MathFeature Feature { get; set; }
		public List<MathFeatureVM> AllFeatureItems { get; set; }
		public List<int> SelFeatureItems { get; set; }
	}

	public class MathFeatureVM
	{
		public string MathFeature { get; set; }
		public int MathFeatureId { get; set; }
		public string MathFeatureItem { get; set; }
		public int MathFeatureItemId { get; set; }
	}

	public class MathReviewTasks
	{
		public MathReviewTasks()
		{
			MathTasks = new List<ReviewTasks>();
		}
		public int ReviewId { get; set; }
		public string Theme { get; set; }
		public string ProjectName { get; set; }
		public int? currentReviewTaskId { get; set; }
		public string MathReviewType { get; set; }
		public List<ReviewTasks> MathTasks { get; set; }
	}

	public class ReviewTasks
	{
		public int TaskId { get; set; }
		public string TaskName { get; set; }
		public string AssignTo { get; set; }
		public Decimal? EstHrs { get; set; }
		public DateTime? TaskTarget { get; set; }
		public DateTime? AssignedOn { get; set; }
	}

}