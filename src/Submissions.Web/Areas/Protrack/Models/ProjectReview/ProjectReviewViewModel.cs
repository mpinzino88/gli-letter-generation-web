﻿using Submissions.Common;
using Submissions.Web.Models.Grids.Protrack;
using Submissions.Web.Models.LookupsAjax;
using Submissions.Web.Models.Query;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace Submissions.Web.Areas.Protrack.Models.ProjectReview
{
	public class Overview
	{
		public Overview()
		{
			this.ProjectReviewGrid = new ProjectReviewGrid();
			this.Filters = new ProjectReviewSearch();
		}

		public ProjectReviewGrid ProjectReviewGrid { get; set; }
		public ProjectReviewSearch Filters { get; set; }
		public DepartmentsFilter DepartmentsFilter { get; set; }
	}

	public class Review
	{
		public Review()
		{
			Details = new List<ReviewDetail>();
			EmailList = new EmailLogin();
		}
		public int? Id { get; set; }
		public int DepartmentId { get; set; }
		public string Department { get; set; }
		public DateTime AddDate { get; set; }
		public DateTime? CompleteDate { get; set; }
		public string Status { get; set; }
		public string Description { get; set; }
		public string ReviewComments { get; set; }
		public int ProjectId { get; set; }
		public string Filenumber { get; set; }
		public List<ReviewDetail> Details { get; set; }
		public int CurrentUserId { get; set; }
		public int? CompleteLoginId { get; set; }
		public string Completer { get; set; }
		public bool IsReviewer { get { return ReviewerList != null ? ReviewerList.Select(x => int.Parse(x.Value)).Contains(CurrentUserId) : false; } }
		public bool IsAssignee { get { return AssigneeListIds != null ? AssigneeListIds.Contains(CurrentUserId) : false; } }
		public bool IsQA { get { return DepartmentId == (int)Common.Department.QA; } }
		public bool AllMarkCompleted { get; set; }
		public bool AssigneeMarkComplete { get; set; }
		public bool ShowAssigneeEmailPopup { get; set; }
		public IEnumerable<SelectListItem> ActionList { get; set; }
		public IEnumerable<SelectListItem> IssueSeverityOptions { get; set; }
		public IEnumerable<SelectListItem> AssigneeList { get; set; }
		public List<int> AssigneeListIds { get; set; }
		public IEnumerable<SelectListItem> LetterwriterList { get; set; }
		public IEnumerable<SelectListItem> ReviewerList { get; set; }
		public IEnumerable<SelectListItem> WatcherList { get; set; }
		public IEnumerable<SelectListItem> JurisdictionList { get; set; }
		public EmailLogin EmailList { get; set; }

	}

	public class ReviewDetail
	{
		public ReviewDetail()
		{
			Comments = new List<ReviewDetailComment>();
		}
		public int? Id { get; set; }
		public int AssigneeId { get; set; }
		public string Assignee { get; set; }
		public int? LetterwriterId { get; set; }
		public string Letterwriter { get; set; }
		public int? MediatorId { get; set; }
		public string Mediator { get; set; }
		public int CategoryId { get; set; }
		public string Category { get; set; }
		public string JurisdictionId { get; set; }
		public string Jurisdiction { get; set; }
		public string Description { get; set; }
		public string Action { get; set; }
		public string IssueSeverity { get; set; }
		public bool Resolved { get; set; }
		public int ReviewerId { get; set; }
		public string Reviewer { get; set; }
		public DateTime AddDate { get; set; }
		public bool IsSaved { get; set; }
		public List<ReviewDetailComment> Comments { get; set; }
		public bool ShowDetail { get; set; }
	}

	public class ReviewDetailComment
	{
		public int? Id { get; set; }
		public int DetailId { get; set; }
		public int UserId { get; set; }
		public string UserName { get; set; }
		public DateTime AddDate { get; set; }
		public DateTime? EditDate { get; set; }
		public string Comment { get; set; }
	}

	public class UserSuggestion
	{
		public List<SelectListItem> AssigneeOptions { get; set; }
		public List<int> AssigneeIds { get; set; }
		public List<SelectListItem> LetterwriterOptions { get; set; }
		public List<int> LetterwriterIds { get; set; }
		public List<SelectListItem> ReviewerOptions { get; set; }
		public List<int> ReviewerIds { get; set; }
	}

	public class EmailLogin
	{
		public List<Detail> Assignees { get; set; }
		public List<Detail> Letterwriters { get; set; }
		public List<Detail> Reviewers { get; set; }
		public List<Detail> Managers { get; set; }
		public List<Detail> Watchers { get; set; }
		public List<Detail> Others { get; set; }
	}

	public class Detail
	{
		public int Id { get; set; }
		public string Name { get; set; }
	}
	public class RevieweeStatus
	{
		public int RevieweeId { get; set; }
		public string RevieweeName { get; set; }
		public string Status { get; set; }
	}
}