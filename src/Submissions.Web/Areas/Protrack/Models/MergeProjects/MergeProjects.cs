﻿using Submissions.Web.Models.Query;
using System.Collections.Generic;

namespace Submissions.Web.Areas.Protrack.Models.Project
{
	public class MergeProjects
	{
		public MergeProjects()
		{
			Filters = new ProjectSearch();
		}
		public int ProjectId { get; set; }
		public string ProjectName { get; set; }
		public string ProjectStatus { get; set; }
		public bool IsTransfer { get; set; }

		public byte? BundleTypeId { get; set; }
		public ProjectSearch Filters { get; set; }
		public string MergeReason { get; set; }
		public List<MergeProject> SelProjectsToMerge { get; set; }

	}

	public class MergeProject
	{
		public int ProjectId { get; set; }
		public bool WithIndicator { get; set; }
		public bool IsTransfer { get; set; }
	}

	public class SearchResults
	{
		public int ProjectId { get; set; }
		public string ProjectName { get; set; }
		public string ReceivedDate { get; set; }
		public bool? IsTransfer { get; set; }
		public string Status { get; set; }
		public string Supervisor { get; set; }
		public byte? BundleTypeId { get; set; }
	}
}