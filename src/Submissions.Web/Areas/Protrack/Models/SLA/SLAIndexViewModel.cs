﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Submissions.Web.Areas.Protrack.Models.SLA
{
	public class SLAIndexViewModel
	{
		public SLAIndexViewModel()
		{
			this.RemediationOption = new RemediationOption();
			this.SlaOption = new SLAOption();
			this.MasterFileRecord = new List<MasterFileRecord>();
			this.ResubmissionFileRecord = new List<MasterFileRecord>();
			this.Resubmission2FileRecord = new List<MasterFileRecord>();
			this.ContinuationFileRecord = new List<MasterFileRecord>();
		}

		public SLAOption SlaOption { get; set; }
		public IList<MasterFileRecord> MasterFileRecord { get; set; }
		public IList<MasterFileRecord> ResubmissionFileRecord { get; set; }
		public IList<MasterFileRecord> ContinuationFileRecord { get; set; }
		public IList<MasterFileRecord> Resubmission2FileRecord { get; set; }
		public RemediationOption RemediationOption { get; set; }
		public int ProjectId { get; set; }
		public int ExcludeFromSLAId { get; set; }
		public int RemediationId { get; set; }
		public string TransferName { get; set; }
		public string FileNumber { get; set; }
		public string MasterFileNumber { get; set; }
		public string NewMasterFileNumber { get; set; }
		[Display(Name = "Did this submission introduce changes other than bug fixes from prior cycles?")]
		public string IncludesChangesOtherThanBug { get; set; }
		[Display(Name = "Did this submission change in technology versions (Platforms and GDK) from previous cycles?")]
		public string ChangedInTechVersion { get; set; }
		[Display(Name = "Does this submission introduce modifications in the Formal Submission?")]
		public string IncludesModificationsToFormalSubmission { get; set; }
		public SelectList OtherOptionsVal { get; set; }
	}

	public class RemediationOption
	{
		public int ProjectId { get; set; }
		[Display(Name = "Bug Fixes")]
		public bool BugFix { get; set; }
		public string Code { get; set; }
		[Display(Name = "Escape Defect")]
		public bool EscapeDefect { get; set; }
		public bool Rework { get; set; }
		public string remCode { get; set; }
		[Display(Name = "Remediation Date"), DisplayFormat(DataFormatString = "{0:d}")]
		public DateTime? RemediationDate { get; set; }

	}

	public class SLAOption
	{
		[Display(Name = "Project ID")]
		public int ProjectId { get; set; }
		public string SlaCode { get; set; }
		public bool Approvals { get; set; }
		[Display(Name = "First Pass")]
		public bool FirstPass { get; set; }
		[Display(Name = "Final Certification Duration")]
		public bool FCDuration { get; set; }
		[Display(Name = "Pre-Certification Duration")]
		public bool PCDuration { get; set; }

	}

	public class MasterFileRecord
	{
		public MasterFileRecord()
		{
			this.SLAExclude = new List<string>();
			this.Remediation = new List<string>();
		}
		public string FileNumber { get; set; }
		public string MasterFileNumberStr { get; set; }
		[DisplayFormat(DataFormatString = "{0:d}")]
		public DateTime? ReceiveDate { get; set; }
		public bool? IsPrecertification { get; set; }
		public bool? IsContinuation { get; set; }
		public bool IsResubmission { get; set; }
		public bool MFCheckBox { get; set; }
		public int ProjectId { get; set; }
		public List<string> SLAExclude { get; set; }
		public List<string> Remediation { get; set; }
		[Display(Name = "Introduce changes other than bug fixes from prior cycles?")]
		public string IncludesChangesOtherThanBug { get; set; }
		[Display(Name = "Change in technology versions (Platforms and GDK) from previous cycles?")]
		public string ChangedInTechVersion { get; set; }
		[Display(Name = "Introduce modifications in the Formal Submission?")]
		public string IncludesModificationsToFormalSubmission { get; set; }
		public string Type
		{
			get
			{
				if (IsResubmission)
				{
					return "Resubmission";
				}
				else if (Convert.ToBoolean(IsPrecertification))
				{
					return "Resubmission of Pre-Certification";
				}
				else if (Convert.ToBoolean(IsContinuation))
				{
					return "Continuation";
				}
				else
				{
					return "Master File";
				}

			}

		}

	}

}





