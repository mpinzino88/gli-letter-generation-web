﻿using System.Collections.Generic;
using System.Web.Mvc;

namespace Submissions.Web.Areas.Protrack.Models.TestingPerformed
{
	public class EditViewModel
	{
		public EditViewModel()
		{
			this.JurisdictionalData = new List<JurisdictionalDataRow>();
			this.TestingPerformedOptions = new List<SelectListItem>();
			this.ExternalTestLabOptions = new List<SelectListItem>();

		}

		public int ProjectId { get; set; }
		public int? BulkTestingPerformedId { get; set; }
		public string BulkTestingPerformed { get; set; }
		public int? BulkExternalTestLabId { get; set; }
		public string BulkExternalTestLab { get; set; }
		public List<JurisdictionalDataRow> JurisdictionalData { get; set; }
		public List<SelectListItem> TestingPerformedOptions { get; set; }
		public List<SelectListItem> ExternalTestLabOptions { get; set; }
	}

	public class JurisdictionalDataRow
	{
		public JurisdictionalDataRow()
		{
			this.TestingPerformedOptions = new List<SelectListItem>();
			this.ExternalTestLabOptions = new List<SelectListItem>();
		}
		public int Id { get; set; }
		public int SubmissionKey { get; set; }
		public bool TestingPerformedAvailable { get; set; }
		public bool Selected { get; set; }
		public string GameName { get; set; }
		public string IDNumber { get; set; }
		public string Version { get; set; }
		public string JurisdictionID { get; set; }
		public string Jurisdiction { get; set; }
		public int? TestingPerformedId { get; set; }
		public string TestingPerformed { get; set; }
		public int? ExternalTestLabId { get; set; }
		public string ExternalTestLab { get; set; }
		public List<SelectListItem> TestingPerformedOptions { get; set; }
		public List<SelectListItem> ExternalTestLabOptions { get; set; }
	}
}