﻿using System.ComponentModel;
using System.Web.Mvc;

namespace Submissions.Web.Areas.Protrack.Models.Project
{
	public class ProjectNoteVM
	{
		public ProjectNoteVM()
		{

		}
		public int NoteId { get; set; }  //Edit Note or else 0
		public string ProjectIds { get; set; }
		public bool RefreshNotes { get; set; }
		[DisplayName("Project Note:")]
		[AllowHtml]
		public string Note { get; set; }
		[DisplayName("Viewable by Customers:")]
		public bool IsViewableByCustomer { get; set; }
		[DisplayName("Department:")]
		public string Dept { get; set; }
		public bool NotifySupervisor { get; set; }
		public bool NotifyAll { get; set; }
		public bool NotifyDevRep { get; set; }
		public bool NotifyProjectLead { get; set; }
		public bool NotifyMathUsers { get; set; }
		public bool NotifyEngUsers { get; set; }
		public bool NotifyQAUsers { get; set; }
	}
}