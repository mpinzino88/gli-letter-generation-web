﻿using Submissions.Common;
using Submissions.Web.Models.LookupsAjax;
using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace Submissions.Web.Areas.Protrack.Models.QATasks
{
	public class IndexViewModel
	{
		public IndexViewModel()
		{
			ProjectsIds = new List<string>();
			ProjectNames = new List<SelectListItem>();
			Tasks = new List<QATask>();
			ProjectFilter = new ProjectFilter() { InBundleStatus = new List<string>() { BundleStatus.AU.ToString() } };
			DefaultTask = new QATask();
		}

		public List<string> ProjectsIds { get; set; }
		public List<SelectListItem> ProjectNames { get; set; }
		public List<QATask> Tasks { get; set; }
		public QATask DefaultTask { get; set; }
		public ProjectFilter ProjectFilter { get; set; }
		public byte? currentBundleTypeId { get; set; }
		public string currentBundleName { get; set; }
		public bool BatchMode { get; set; }

		public int? ReviewerId { get; set; }
		public string Reviewer { get; set; }

		public int? SupervisorId { get; set; }
		public string Supervisor { get; set; }
	}

	public class QAProjects
	{
		public int Id { get; set; }
		public string Name { get; set; }
		public string Status { get; set; }
	}

	public class QATask
	{
		public QATask()
		{
			User = new SelectListItem();
			TaskDef = new SelectListItem();
			IsInEditMode = true;
		}
		public int Id { get; set; }
		public int TaskDefId { get; set; }
		public string TaskDefName { get; set; }
		public int UserId { get; set; }
		public string UserName { get; set; }
		public SelectListItem User { get; set; }
		public SelectListItem TaskDef { get; set; }
		public DateTime? TargetDate { get; set; }
		public string TargetDateString { get; set; }
		public DateTime? StartDate { get; set; }
		public DateTime? CompleteDate { get; set; }
		public bool IsTaskDefInEditMode { get; set; }
		public bool IsInEditMode { get; set; }
		public bool? _dirty { get; set; }
	}
}