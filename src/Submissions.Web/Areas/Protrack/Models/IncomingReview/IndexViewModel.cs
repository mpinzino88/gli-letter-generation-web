﻿using EvoLogic.Models;
using Submissions.Common;
using Submissions.Common.Filters;
using Submissions.Core.IncomingReview;
using Submissions.Core.Models;
using Submissions.Core.Models.SubProjectService;
using Submissions.Web.Models.LookupsAjax;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace Submissions.Web.Areas.Protrack.Models.IncomingReview
{
	public class IndexViewModel
	{
		public IndexViewModel()
		{
			ProjectDetails = new ProjectDetailsViewModel();
			OriginalTargetDate = new OriginalTargetDateViewModel();
			JurisdictionTargetOffsetRules = new List<JurisdictionTargetOffset>();
			ManufacturerSpecific = new ManufacturerSpecificViewModel();
			Billing = new BillingViewModel();
			IncomingReviewStatus = new IncomingReviewStatus();
			ResubmissionViewModel = new ResubmissionModel();
			OriginalTargetDateErrors = new List<string>();
			ProjDetailErrors = new List<string>();
			ResubmissionErrors = new List<string>();
			RequiringComplexityManufacturer = new List<string>() { ((int)ProductType.NewGame).ToString(), ((int)ProductType.ModGame).ToString(), ((int)ProductType.CloneGame).ToString(), ((int)ProductType.Port).ToString() };
			UK = new UK();
			EvolutionViewModel = new EvolutionViewModel();
			AddTask = new INTask();
			AddTask._new = true;
			usersFilter = new UsersFilter();
			tasksFilter = new TasksFilter { IsDynamicSet = false };
		}
		public int ProjectId { get; set; }
		public bool HasCompletedFormEditPermission { get; set; }
		public string ManufacturerCode { get; set; }
		public string FileNumber { get; set; }
		public string ProjectStatus { get; set; }
		public bool OnHold { get; set; }
		public List<string> OriginalTargetDateErrors { get; set; }
		public List<string> ProjDetailErrors { get; set; }
		public List<string> ResubmissionErrors { get; set; }
		public List<string> RequiringComplexityManufacturer { get; set; }
		public ProjectDetailsViewModel ProjectDetails { get; set; }
		public List<JurisdictionTargetOffset> JurisdictionTargetOffsetRules { get; set; }
		public bool? ApplyJurisdictionOffset { get; set; }
		public OriginalTargetDateViewModel OriginalTargetDate { get; set; }
		public ManufacturerSpecificViewModel ManufacturerSpecific { get; set; }
		public BillingViewModel Billing { get; set; }
		public IncomingReviewStatus IncomingReviewStatus { get; set; }
		public ResubmissionModel ResubmissionViewModel { get; set; }
		public EvolutionViewModel EvolutionViewModel { get; set; }
		public string IncomingReviewEditedBy { get; set; }
		public string EditedDate { get; set; }
		public string CompletedBy { get; set; }
		public string CompletedDate { get; set; }
		public bool HasUK { get; set; }
		public bool HasUKPermission { get; set; }
		public UK UK { get; set; }
		public string OriginalFilenumber { get; set; }
		public INTask AddTask { get; set; }
		public TasksFilter tasksFilter { get; set; }
		public DateTime ProjectAcceptDate { get; set; }
		public double HoursSinceAcceptance { get; set; }
		public bool TaskLockEnforced { get; set; }
		public int CurrentUserId { get; set; }
		public int? ManagerUserId { get; set; }
		public UsersFilter usersFilter { get; set; }
		public string CurrentUsername { get; set; }
	}

	#region ProjectDetailsViewModel
	public class ProjectDetails
	{
		public ProjectDetails()
		{
			GeneralViewModel = new GeneralViewModel();
			ProjectDefinition = new ProjectDefinitionViewModel();
			WorkOrderNumbers = new List<string>();
		}
		public GeneralViewModel GeneralViewModel { get; set; }
		public ProjectDefinitionViewModel ProjectDefinition { get; set; }
		public List<string> WorkOrderNumbers { get; set; }
	}
	#endregion

	#region MasterFileRecordSetModel
	public class MasterFileRecordSetModel
	{
		public MasterFileRecordSetModel()
		{
			Ids = new List<MasterFileRecord>();
		}
		public string FileNumber { get; set; }
		public List<SubmissionRecord> Records { get; set; }
		public ResubmissionOptions ResubmissionOption { get; set; }
		public string ResubmissionOptionExplanation { get; set; }
		public List<MasterFileRecord> Ids { get; set; }
		public int? StudioId { get; set; }
		public string Studio { get; set; }
		public bool HasADistinctStudio { get; set; }
	}
	#endregion

	#region MasterFileRecord
	public class MasterFileRecord
	{
		public int Id { get; set; }
		public int? CountId { get; set; }
		public bool AvailableForMapping { get; set; }
	}
	#endregion

	#region SubmissionRecord
	public class SubmissionRecord
	{
		public int Id { get; set; }
		public int? CountId { get; set; }
		public int? LinkedId { get; set; }
		public string MasterFileNumber { get; set; }
		public string FileNumber { get; set; }
		public string IdNumber { get; set; }
		public string Version { get; set; }
		public string DateCode { get; set; }
		public string GameName { get; set; }
		public bool Resubmission { get; set; }
		public bool Continuation { get; set; }
		public bool PreCertification { get; set; }
		public string Status { get; set; }
		public string ConcurrentReSubmissionFileNumber { get; set; }
		public bool IsAMasterfile { get; set; }
	}
	#endregion

	#region MappingType
	public class MappingType
	{
		public int Id { get; set; }
		public string DisplayText { get; set; }
		public bool Disabled { get; set; }
	}
	#endregion

	#region Resubmission Model
	public class ResubmissionModel
	{
		public ResubmissionModel()
		{
			MasterFileRecordSet = new MasterFileRecordSetModel();
			CurrentSubmissionRecords = new List<SubmissionRecord>();
			MappingTypes = new List<MappingType>();
			SubmissionFilter = new SubmissionsFilter();
			MasterFileOption = new SelectListItem();
		}
		public string MasterFileNumber { get; set; }
		public SelectListItem MasterFileOption { get; set; }
		public string CurrentFileNumber { get; set; }
		public MasterFileRecordSetModel MasterFileRecordSet { get; set; }
		public List<SubmissionRecord> CurrentSubmissionRecords { get; set; }
		public int? SelectedMappingType { get; set; }
		public List<MappingType> MappingTypes { get; set; }
		public SubmissionsFilter SubmissionFilter { get; set; }
	}
	#endregion

	#region UK
	public class UK
	{
		public UK()
		{
			SubProjects = new List<SubProject>();
			SubProject = new SubProject();
			TimePeriodOptions = LookupsStandard.ConvertEnum<TimePeriod>().ToList();
			TestHarnessOptions = LookupsStandard.ConvertEnum<TestHarness>().ToList();
			RequiredOutputOptions = LookupsStandard.ConvertEnum<RequiredOutput>(useDescriptionAsValue: true).ToList();
			GameTypeOptions = LookupsStandard.ConvertEnum<MathGameType>(useDescriptionAsValue: true).ToList();
		}
		public IList<SubProject> SubProjects { get; set; }
		public SubProject SubProject { get; set; }
		public IList<SelectListItem> TimePeriodOptions { get; set; }
		public IList<SelectListItem> TestHarnessOptions { get; set; }
		public IList<SelectListItem> RequiredOutputOptions { get; set; }
		public IList<SelectListItem> GameTypeOptions { get; set; }
	}
	#endregion
}
#region Evo Config
public class ComponentGroupFilter
{
	public ComponentGroupFilter()
	{
		SubmissionsIds = new List<int>();
		RequiredJuridictionIds = new List<int>();
	}
	public List<int> SubmissionsIds { get; set; }
	public int ProjectId { get; set; }
	public List<int> RequiredJuridictionIds { get; set; }
}

public class SaveComponentGroupsPayload
{
	public SaveComponentGroupsPayload()
	{
		ComponentGroups = new List<ComponentGroup>();
		PreserveAnswers = new List<AddCompToGroupModel>();
	}
	public int ProjectId { get; set; }
	public string ProjectName { get; set; }
	public List<ComponentGroup> ComponentGroups { get; set; }
	public bool PreserveAnswer { get; set; }
	public List<AddCompToGroupModel> PreserveAnswers { get; set; }
}
public class TestCaseSeed
{
	public TestCaseSeed()
	{
		functionalRolesIds = new List<int>();
		jurisdictionsIds = new List<int>();
		documentIds = new List<int>();
	}
	public List<int> functionalRolesIds { get; set; }
	public List<int> jurisdictionsIds { get; set; }
	public List<int> documentIds { get; set; }
	public string projectId { get; set; }
	public DateTime? targetDate { get; set; }
}
public class EngTask
{
	public EngTask()
	{
		EngTasks = new List<INTask>();
	}
	public int ProjectId { get; set; }
	public List<INTask> EngTasks { get; set; }
	public bool TaskLockEnforced { get; set; }
}
public class Template
{
	public String[] templateIds { get; set; }
	public int projectId { get; set; }
	public string addReason { get; set; }
	public bool taskLockEngaged { get; set; }
}
#endregion

