﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;
using System.Web.Mvc;
using Trirand.Web.Mvc;

namespace Submissions.Web.Areas.Protrack.Models
{

	#region Model
	public class ProjectHistoryViewModel
	{
		public ProjectHistoryGrid ProjectHistoryGrid { get; set; }
		public int ProjectId { get; set; }
		public string ProjectName { get; set; }

	}

	public class ProjectHistory
	{
		public DateTime? TimeStamp { get; set; }
		public string Employee { get; set; }
		public string ProjectHolder { get; set; }
		public string ProjectStatus { get; set; }
		public string Notes { get; set; }
		public string ForeignProject { get; set; }
	}

	public class ProjectHistoryGrid
	{
		public JQGrid Grid { get; set; }

		public ProjectHistoryGrid()
		{
			Grid = new JQGrid
			{
				Columns = new List<JQGridColumn>
				{
								
					new JQGridColumn
					{
						DataField = "TimeStamp",
						HeaderText = "Time Stamp",
						Width = 20,
						Searchable = false
						
					},
					new JQGridColumn
					{
						DataField = "Employee",
						HeaderText = "Employee",
						Width = 20,
						Searchable = false
						
					},
					new JQGridColumn
					{
						DataField = "ProjectHolder",
						HeaderText = "Holder",
						Width = 5,
						Searchable = false
												
					},
					new JQGridColumn
					{
						DataField = "ProjectStatus",
						HeaderText = "Status",
						Width = 5,
						Searchable = false
												
					},
					new JQGridColumn
					{
						DataField = "Notes",
						HeaderText = "Event",
						Width = 60,
						DataType = typeof(string)
						
					}
				}
			};

			Grid.ID = "ProjectHistoryGrid";
			Grid.DataUrl = new UrlHelper(((MvcHandler)HttpContext.Current.Handler).RequestContext).Action("LoadProjectHistoryGrid");
			Grid.AutoWidth = true;
			Grid.Width = Unit.Percentage(100);
			Grid.Height = Unit.Percentage(100);
			
			Grid.ToolBarSettings = new ToolBarSettings
			{
				ShowRefreshButton = true,
				ShowSearchToolBar = true,
				ToolBarPosition = ToolBarPosition.Top
			};
			Grid.PagerSettings = new Trirand.Web.Mvc.PagerSettings
			{
				PageSize = 100,
				PageSizeOptions = "[]"
			};
			Grid.AppearanceSettings = new AppearanceSettings
			{
				AlternateRowBackground = true,
				HighlightRowsOnHover = true,
				ShowFooter = false
			};

		}

	}

	#endregion
}