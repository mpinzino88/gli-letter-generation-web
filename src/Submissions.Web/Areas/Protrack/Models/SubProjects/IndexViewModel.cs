﻿using Submissions.Common;
using Submissions.Core.Models.SubProjectService;
using Submissions.Web.Models.Query;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using SubProjectTask = Submissions.Core.Models.SubProjectService.Task;

namespace Submissions.Web.Areas.Protrack.Models.SubProjects
{
	public class IndexViewModel
	{
		public IndexViewModel()
		{
			UK = new UK();
			SubProjectSearch = new SubProjectSearch();
			TeamFilterOptions = new List<SelectListItem>();
			EmptyTask = new SubProjectTask();
		}
		public UK UK { get; set; }
		public SubProjectSearch SubProjectSearch { get; set; }
		public IList<SelectListItem> TeamFilterOptions { get; set; }
		public SubProjectTask EmptyTask { get; set; }
		public IList<SelectListItem> TaskStatusOptions
		{
			get
			{
				var selectList = new List<SelectListItem>
				{
					new SelectListItem { Value = SubProjectStatus.Unassigned.ToString(), Text = SubProjectStatus.Unassigned.GetEnumDescription(), Selected = true},
					new SelectListItem { Value = SubProjectStatus.NotStarted.ToString(), Text = SubProjectStatus.NotStarted.GetEnumDescription(), Selected = true},
					new SelectListItem { Value = SubProjectStatus.InProgress.ToString(), Text = SubProjectStatus.InProgress.GetEnumDescription(), Selected = true},
					new SelectListItem { Value = SubProjectStatus.Closed.ToString(), Text = SubProjectStatus.Closed.GetEnumDescription()}
				};
				return selectList;
			}
		}
	}

	#region UK
	public class UK
	{
		public UK()
		{
			SubProject = new SubProject();
			Task = new SubProjectTask();
			TimePeriodOptions = LookupsStandard.ConvertEnum<TimePeriod>().ToList();
			TestHarnessOptions = LookupsStandard.ConvertEnum<TestHarness>().ToList();
			GameTypeOptions = LookupsStandard.ConvertEnum<MathGameType>(useDescriptionAsValue: true).ToList();
			RequiredOutputOptions = LookupsStandard.ConvertEnum<RequiredOutput>(useDescriptionAsValue: true).ToList();
		}
		public SubProject SubProject { get; set; }
		public SubProjectTask Task { get; set; }
		public IList<SelectListItem> TimePeriodOptions { get; set; }
		public IList<SelectListItem> TestHarnessOptions { get; set; }
		public IList<SelectListItem> GameTypeOptions { get; set; }
		public IList<SelectListItem> RequiredOutputOptions { get; set; }
	}
	#endregion
}