﻿using System;
using Submissions.Common;
using Submissions.Web.Models.Grids;
using Submissions.Web.Models.Query;

namespace Submissions.Web.Areas.Protrack.Models.Project
{
	public class CreateTransfer
	{
		public CreateTransfer()
		{
			Filters = new ProjectJurisdictionItemSearch();
			JurisdictionGrid = new CreateTransferGrid();
		}

		public ProjectJurisdictionItemSearch Filters { get; set; }
		public int ProjectId { get; set; }
		public string ProjectName { get; set; }
		public string ProjectStatus { get; set; }
		public int? ProjectSuffix { get; set; }
		public string FileNumber { get; set; }
		public int TotalItems { get; set; }
		public bool IsTransferProject { get; set; }
		public string SelPrimarys { get; set; }
		public CreateTransferGrid JurisdictionGrid { get; set; }
		public bool CreateAsBrandNewBundle { get; set; }
		public BundleType BundleType { get; set; }
		public bool IsForADDCOR { get; set; } //This will allow to exclude jur from closed bundles
	}
		
}