using AutoMapper;
using EF.Submission;
using Submissions.Common;
using Submissions.Core.Models.MasterFileService;
using Submissions.Web.Areas.Protrack.Models;
using Submissions.Web.Areas.Protrack.Models.MathQueue;
using Submissions.Web.Areas.Protrack.Models.MyTasks;
using Submissions.Web.Areas.Protrack.Models.Project;
using Submissions.Web.Areas.Protrack.Models.ProjectReview;
using Submissions.Web.Areas.Protrack.Models.Task.KnockoutModels;
using Submissions.Web.Utility.Email;
using System;
using System.Web.Mvc;
using WorkloadManager = Submissions.Web.Areas.Protrack.Models.WorkloadManager;

namespace Submissions.Web.Mappings
{
	public class ProtrackProfile : Profile
	{
		public ProtrackProfile()
		{
			CreateMap<trProjectJurisdictionalData, ProjectJurisdictionViewModel>()
				 .ForMember(dest => dest.JurisdictionId, src => src.MapFrom(x => x.JurisdictionalDataId))
				 .ForMember(dest => dest.Jurisdiction, src => src.MapFrom(x => x.JurisdictionalData.JurisdictionName))
				 .ForMember(dest => dest.JurisdictionStatus, src => src.MapFrom(x => x.JurisdictionalData.Status))
				 .ForMember(dest => dest.JurisdictionRcvdDate, src => src.MapFrom(x => x.JurisdictionalData.ReceiveDate))
				 .ForMember(dest => dest.IDNumber, src => src.MapFrom(x => x.JurisdictionalData.Submission.IdNumber))
				 .ForMember(dest => dest.GameName, src => src.MapFrom(x => x.JurisdictionalData.Submission.GameName))
				 .ForMember(dest => dest.Version, src => src.MapFrom(x => x.JurisdictionalData.Submission.Version))
				 .ForMember(dest => dest.DateCode, src => src.MapFrom(x => x.JurisdictionalData.Submission.DateCode))
				 .ForMember(dest => dest.FileNumber, src => src.MapFrom(x => x.JurisdictionalData.Submission.FileNumber))
				 .ForMember(dest => dest.OrgTargetDate, src => src.MapFrom(x => x.JurisdictionalData.ProposedTargetDate))
				 .ForMember(dest => dest.TargetDate, src => src.MapFrom(x => x.JurisdictionalData.TargetDate));

			CreateMap<GLI.EFCore.Submission.trProject, EditViewModel>()
				.ForMember(dest => dest.FileNumber, src => src.MapFrom(x => x.FileNumber))
				.ForMember(dest => dest.ProjectName, src => src.MapFrom(x => x.ProjectName))
				.ForMember(dest => dest.ENProjectLead, src => src.MapFrom(x => x.ENProjectLead == null ? "" : (x.ENProjectLead.FName + " " + x.ENProjectLead.LName).Trim()))
				.ForMember(dest => dest.OriginalENManager, src => src.MapFrom(x => x.OriginalENManager == null ? "" : (x.OriginalENManager.FName + " " + x.OriginalENManager.LName).Trim()))
				.ForMember(dest => dest.MAProjectLead, src => src.MapFrom(x => x.MAProjectLead == null ? "" : (x.MAProjectLead.FName + " " + x.MAProjectLead.LName).Trim()))
				.ForMember(dest => dest.DevelopmentRepresentative, src => src.MapFrom(x => x.DevelopmentRepresentative == null ? "" : (x.DevelopmentRepresentative.FName + " " + x.DevelopmentRepresentative.LName).Trim()))
				.ForMember(dest => dest.EstMathComplete, src => src.MapFrom(x => x.MAEstimateCompleteDate))
				.ForMember(dest => dest.EstTestComplete, src => src.MapFrom(x => x.ENEstimateTestCompleteDate))
				.ForMember(dest => dest.EstReviewComplete, src => src.MapFrom(x => x.ENEstimateCompleteDate))
				.ForMember(dest => dest.EstLetterIssueDate, src => src.MapFrom(x => x.QAEstimateCompleteDate))
				.ForMember(dest => dest.ReceivedDate, src => src.MapFrom(x => x.ReceiveDate))
				.ForMember(dest => dest.IsEstMathCompleteDateToBeDetermined, src => src.MapFrom(x => x.IsTBDEstMathCompleteDate))
				.ForMember(dest => dest.Status, opt => opt.Ignore())
				.ForMember(dest => dest.SubmissionsReceivedDate, opt => opt.Ignore())
				.ForMember(dest => dest.ComplexityRating, opt => opt.Ignore())
				.ForMember(dest => dest.QuotedHours, opt => opt.Ignore())
				.ForMember(dest => dest.QAReviewer, opt => opt.Ignore())
				.ForMember(dest => dest.QAReviewerId, opt => opt.Ignore())
				.ForMember(dest => dest.QASupervisor, opt => opt.Ignore())
				.ForMember(dest => dest.QASupervisorId, opt => opt.Ignore())
				.ForMember(dest => dest.QAStatus, opt => opt.Ignore())
				.ForMember(dest => dest.NCRReq, opt => opt.Ignore())
				.ForMember(dest => dest.NCRNum, opt => opt.Ignore())
				.ForMember(dest => dest.NoCertNeeded, opt => opt.Ignore())
				.ForMember(dest => dest.HasiGamingTestingType, opt => opt.Ignore())
				.ForMember(dest => dest.TranslationHouses, opt => opt.Ignore())
				.ForMember(dest => dest.TranslationHouseIds, opt => opt.Ignore())
				.ForMember(dest => dest.ShowGameSubmissionType, opt => opt.Ignore())
				.ForMember(dest => dest.GameSubmissionType, src => src.MapFrom(x => x.GameSubmissionType.Description));

			CreateMap<trTargetDate, TargetHistory>()
				.ForMember(dest => dest.ModifiedBy, src => src.MapFrom(x => x.ModifiedBy.FName + ' ' + x.ModifiedBy.LName))
				.ForMember(dest => dest.Reason, src => src.MapFrom(x => x.TargetDateReasonForChange.Reason));

			CreateMap<trProject, TargetDateChangeEmailModel>()
				.ForMember(dest => dest.ProjectJurisdictions, src => src.MapFrom(x => x.JurisdictionalData))
				.ForMember(dest => dest.TestLab, src => src.MapFrom(x => x.Lab))
				.ForMember(dest => dest.ProjectStatus, src => src.MapFrom(x => x.ENStatus))
				.ForMember(dest => dest.DisplayOnlyProject, opt => opt.Ignore())
				.ForMember(dest => dest.AffectedPrimary, opt => opt.Ignore());

			CreateMap<trProjectJurisdictionalData, ProjectJurisdiction>()
				.ForMember(dest => dest.JurisdictionId, src => src.MapFrom(x => x.JurisdictionalData.JurisdictionId))
				.ForMember(dest => dest.Jurisdiction, src => src.MapFrom(x => x.JurisdictionalData.JurisdictionName))
				.ForMember(dest => dest.JurisdictionRcvdDate, src => src.MapFrom(x => x.JurisdictionalData.ReceiveDate))
				.ForMember(dest => dest.IdNumber, src => src.MapFrom(x => x.JurisdictionalData.Submission.IdNumber))
				.ForMember(dest => dest.PartNumber, src => src.MapFrom(x => x.JurisdictionalData.Submission.PartNumber))
				.ForMember(dest => dest.Version, src => src.MapFrom(x => x.JurisdictionalData.Submission.Version))
				.ForMember(dest => dest.DateCode, src => src.MapFrom(x => x.JurisdictionalData.Submission.DateCode))
				.ForMember(dest => dest.FileNumber, src => src.MapFrom(x => x.JurisdictionalData.Submission.FileNumber))
				.ForMember(dest => dest.OrgTargetDate, src => src.MapFrom(x => x.JurisdictionalData.ProposedTargetDate))
				.ForMember(dest => dest.TargetDate, src => src.MapFrom(x => x.JurisdictionalData.TargetDate));

			CreateMap<trHistory, ProjectHistory>()
				.ForMember(dest => dest.Employee, src => src.MapFrom(x => x.User.FName + " " + x.User.LName))
				.ForMember(dest => dest.TimeStamp, opt => opt.Ignore())
				.ForMember(dest => dest.ProjectStatus, opt => opt.Ignore());

			CreateMap<GLI.EFCore.Submission.trTask, KOTask>()
				.ForMember(dest => dest.InitialEstimatedHours, src => src.MapFrom(x => x.EstimateHours))
				.ForMember(dest => dest.CreatedById, src => src.MapFrom(x => x.AddUserId))
				.ForMember(dest => dest.CreatedDate, src => src.MapFrom(x => x.AddDate))
				.ForMember(dest => dest.EstimatedCompleteDate, src => src.MapFrom(x => x.EstimateCompleteDate))
				.ForMember(dest => dest.EstimatedHours, src => src.MapFrom(x => x.EstimateHours))
				.ForMember(dest => dest.EstimatedInterval, src => src.MapFrom(x => x.EstimateInterval))
				.ForMember(dest => dest.OriginalEstimatedHours, src => src.MapFrom(x => x.OriginalEstimateHours))
				.ForMember(dest => dest.ProjectIsTransfer, src => src.MapFrom(x => x.Project.IsTransfer))
				.ForMember(dest => dest.FileNumber, src => src.MapFrom(x => x.Project.FileNumber))
				.ForMember(dest => dest.AssignedUserId, src => src.MapFrom(x => x.UserId))
				.ForMember(dest => dest.User, src => src.MapFrom(x => new SelectListItem { Value = x.UserId != null ? x.UserId.ToString() : null, Text = x.User != null ? x.User.FName + " " + x.User.LName : null }))
				.ForMember(dest => dest.TaskDef, src => src.MapFrom(x => new SelectListItem { Value = x.TaskDefinitionId.ToString(), Text = x.Definition.Description }))
				.ForMember(dest => dest.IsInEditMode, src => src.MapFrom(x => false))
				.ForMember(dest => dest.IsETATask, src => src.MapFrom(x => x.ETAQueue))
				.ForMember(dest => dest.IsAcumaticaSet, src => src.MapFrom(x => x.Definition.IsAcumaticaSet))
				.ForMember(dest => dest.ShowETACheckbox, opt => opt.Ignore())
				.ForMember(dest => dest._destroy, opt => opt.Ignore())
				.ForMember(dest => dest._dirty, opt => opt.Ignore())
				.ForMember(dest => dest.forBulkUpdate, opt => opt.Ignore())
				.ForMember(dest => dest.TaskDefinition, opt => opt.Ignore())
				.ForMember(dest => dest.TaskChangeReason, opt => opt.Ignore())
				.ForMember(dest => dest.IsCPReviewTaskTiedToJur, opt => opt.Ignore())
				.ForMember(dest => dest.AssociatedJurisdictions, opt => opt.Ignore())
				.ForMember(dest => dest.TargetDateString, opt => opt.Ignore());

			CreateMap<GLI.EFCore.Submission.trTaskDeletions, KOTask>()
				.ForMember(dest => dest.AssignedUserId, src => src.MapFrom(x => x.UserId))
				.ForMember(dest => dest.EstimatedHours, src => src.MapFrom(x => x.EstimateHours))
				.ForMember(dest => dest.OriginalEstimatedHours, src => src.MapFrom(x => x.OriginalEstimateHours))
				.ForMember(dest => dest.User, src => src.MapFrom(x => new SelectListItem { Value = x.UserId.ToString(), Text = x.User.FName + " " + x.User.LName }))
				.ForMember(dest => dest.TaskDef, src => src.MapFrom(x => new SelectListItem { Value = x.TaskDefinitionId.ToString(), Text = x.Definition.Description }))
				.ForMember(dest => dest.AssignDate, opt => opt.Ignore())
				.ForMember(dest => dest.AssociatedJurisdictions, opt => opt.Ignore())
				.ForMember(dest => dest._destroy, opt => opt.Ignore())
				.ForMember(dest => dest._dirty, opt => opt.Ignore())
				.ForMember(dest => dest.IsCPReviewTaskTiedToJur, opt => opt.Ignore())
				.ForMember(dest => dest.CompleteDate, opt => opt.Ignore())
				.ForMember(dest => dest.ComponentGroupId, opt => opt.Ignore())
				.ForMember(dest => dest.CreatedById, opt => opt.Ignore())
				.ForMember(dest => dest.CreatedDate, opt => opt.Ignore())
				.ForMember(dest => dest.DepartmentId, opt => opt.Ignore())
				.ForMember(dest => dest.EstimatedCompleteDate, opt => opt.Ignore())
				.ForMember(dest => dest.EstimatedInterval, opt => opt.Ignore())
				.ForMember(dest => dest.FileNumber, opt => opt.Ignore())
				.ForMember(dest => dest.forBulkUpdate, opt => opt.Ignore())
				.ForMember(dest => dest.InitialEstimatedHours, opt => opt.Ignore())
				.ForMember(dest => dest.IsInEditMode, opt => opt.Ignore())
				.ForMember(dest => dest.IsETATask, opt => opt.Ignore())
				.ForMember(dest => dest.ShowETACheckbox, opt => opt.Ignore())
				.ForMember(dest => dest.IsAcumaticaSet, opt => opt.Ignore())
				.ForMember(dest => dest.ProjectIsTransfer, opt => opt.Ignore())
				.ForMember(dest => dest.StartDate, opt => opt.Ignore())
				.ForMember(dest => dest.TargetDate, opt => opt.Ignore())
				.ForMember(dest => dest.TaskChangeReason, opt => opt.Ignore())
				.ForMember(dest => dest.TaskDefinition, opt => opt.Ignore())
				.ForMember(dest => dest.TargetDateString, opt => opt.Ignore());

			CreateMap<GLI.EFCore.Submission.trTask, Core.IncomingReview.INTask>()
				.ForMember(dest => dest.InitialEstimatedHours, src => src.MapFrom(x => x.EstimateHours))
				.ForMember(dest => dest.CreatedById, src => src.MapFrom(x => x.AddUserId))
				.ForMember(dest => dest.CreatedDate, src => src.MapFrom(x => x.AddDate))
				.ForMember(dest => dest.EstimatedCompleteDate, src => src.MapFrom(x => x.EstimateCompleteDate))
				.ForMember(dest => dest.EstimatedHours, src => src.MapFrom(x => x.EstimateHours))
				.ForMember(dest => dest.EstimatedInterval, src => src.MapFrom(x => x.EstimateInterval))
				.ForMember(dest => dest.OriginalEstimatedHours, src => src.MapFrom(x => x.OriginalEstimateHours))
				.ForMember(dest => dest.ProjectIsTransfer, src => src.MapFrom(x => x.Project.IsTransfer))
				.ForMember(dest => dest.FileNumber, src => src.MapFrom(x => x.Project.FileNumber))
				.ForMember(dest => dest.AssignedUserId, src => src.MapFrom(x => x.UserId))
				.ForMember(dest => dest.TaskDef, src => src.MapFrom(x => x.Description))
				.ForMember(dest => dest.TaskDefinitionId, src => src.MapFrom(x => x.TaskDefinitionId))
				.ForMember(dest => dest.IsInEditMode, src => src.MapFrom(x => false))
				.ForMember(dest => dest.IsETATask, src => src.MapFrom(x => x.ETAQueue))
				.ForMember(dest => dest._destroy, opt => opt.Ignore())
				.ForMember(dest => dest._dirty, opt => opt.Ignore())
				.ForMember(dest => dest._newTemplateTask, opt => opt.Ignore())
				.ForMember(dest => dest.forBulkUpdate, opt => opt.Ignore())
				.ForMember(dest => dest.TaskDefinition, opt => opt.Ignore())
				.ForMember(dest => dest.TaskChangeReason, opt => opt.Ignore())
				.ForMember(dest => dest.IsCPReviewTaskTiedToJur, opt => opt.Ignore())
				.ForMember(dest => dest.AssignedUserName, opt => opt.Ignore())
				.ForMember(dest => dest.AssignToUserId, opt => opt.Ignore())
				.ForMember(dest => dest.AssignToUserName, opt => opt.Ignore())
				.ForMember(dest => dest._new, opt => opt.Ignore())
				.ForMember(dest => dest._isDeactivatedTask, opt => opt.Ignore())
				.ForMember(dest => dest.AssociatedJurisdictions, opt => opt.Ignore())
				.ForMember(dest => dest.EvolutionStatus, opt => opt.Ignore())
				.ForMember(dest => dest.CompGroupDescription, opt => opt.Ignore());

			CreateMap<GLI.EFCore.Submission.trTaskDeletions, Core.IncomingReview.INTask>()
				.ForMember(dest => dest.AssignedUserId, src => src.MapFrom(x => x.UserId))
				.ForMember(dest => dest.EstimatedHours, src => src.MapFrom(x => x.EstimateHours))
				.ForMember(dest => dest.OriginalEstimatedHours, src => src.MapFrom(x => x.OriginalEstimateHours))
				.ForMember(dest => dest.TaskDef, src => src.MapFrom(x => x.Description + " - " + x.Definition.DynamicsId))
				.ForMember(dest => dest.EstimatedCompleteDate, src => src.MapFrom(x => x.EstimateCompleteDate))
				.ForMember(dest => dest.InitialEstimatedHours, src => src.MapFrom(x => x.EstimateHours))
				.ForMember(dest => dest.EstimatedInterval, src => src.MapFrom(x => x.EstimateInterval))
				.ForMember(dest => dest.TaskDefinitionId, src => src.MapFrom(x => x.TaskDefinitionId))
				.ForMember(dest => dest._isDeactivatedTask, src => src.MapFrom(x => true))
				.ForMember(dest => dest.TaskDefinition, opt => opt.Ignore())
				.ForMember(dest => dest.CreatedById, opt => opt.Ignore())
				.ForMember(dest => dest.CreatedDate, opt => opt.Ignore())
				.ForMember(dest => dest.ProjectIsTransfer, opt => opt.Ignore())
				.ForMember(dest => dest.FileNumber, opt => opt.Ignore())
				.ForMember(dest => dest.IsInEditMode, opt => opt.Ignore())
				.ForMember(dest => dest._destroy, opt => opt.Ignore())
				.ForMember(dest => dest._dirty, opt => opt.Ignore())
				.ForMember(dest => dest._newTemplateTask, opt => opt.Ignore())
				.ForMember(dest => dest.forBulkUpdate, opt => opt.Ignore())
				.ForMember(dest => dest.TaskChangeReason, opt => opt.Ignore())
				.ForMember(dest => dest.IsCPReviewTaskTiedToJur, opt => opt.Ignore())
				.ForMember(dest => dest.AssignToUserId, opt => opt.Ignore())
				.ForMember(dest => dest.AssignToUserName, opt => opt.Ignore())
				.ForMember(dest => dest._new, opt => opt.Ignore())
				.ForMember(dest => dest.AssociatedJurisdictions, opt => opt.Ignore())
				.ForMember(dest => dest.ComponentGroupId, opt => opt.Ignore())
				.ForMember(dest => dest.AssignedUserName, opt => opt.Ignore())
				.ForMember(dest => dest.IsETATask, opt => opt.Ignore())
				.ForMember(dest => dest.EvolutionStatus, opt => opt.Ignore())
				.ForMember(dest => dest.CompGroupDescription, opt => opt.Ignore());

			CreateMap<Submissions.Core.Models.EvolutionService.TestCase, Core.IncomingReview.TestCaseViewModel>()
				.ForMember(dest => dest.Id, src => src.MapFrom(x => x.Id))
				.ForMember(dest => dest.Name, src => src.MapFrom(x => x.Name))
				.ForMember(dest => dest.Order, src => src.MapFrom(x => x.Order))
				.ReverseMap();

			CreateMap<MasterFileRecordSetModel, Areas.Protrack.Models.IncomingReview.MasterFileRecordSetModel>()
				.ReverseMap();

			CreateMap<SubmissionRecord, Areas.Protrack.Models.IncomingReview.SubmissionRecord>()
				.ReverseMap();
			CreateMap<SubmissionRecord, Core.IncomingReview.SubmissionRecord>()
				.ReverseMap();

			CreateMap<MappingType, Areas.Protrack.Models.IncomingReview.MappingType>()
				.ReverseMap();
			CreateMap<MappingType, Core.IncomingReview.MappingType>()
				.ReverseMap();
			CreateMap<Areas.Protrack.Models.IncomingReview.MasterFileRecord, MasterFileRecord>()
				.ReverseMap();
			CreateMap<Core.IncomingReview.MasterFileRecord, MasterFileRecord>()
				.ReverseMap();

			//CreateMap<Areas.Protrack.Models.IncomingReview.ProjectDefinitionViewModel, Submissions.Core.Models.SubmissionService.MasterFileModel>()
			//	.ReverseMap();
			CreateMap<Core.IncomingReview.ProjectDefinitionViewModel, MasterFileModel>()
				.ReverseMap();
			CreateMap<Core.IncomingReview.MasterFileRecordSetModel, MasterFileRecordSetModel>()
				.ReverseMap();
			CreateMap<Areas.Protrack.Models.IncomingReview.ResubmissionModel, MasterFileModel>();

			//CreateMap<IncomingReview.ThemeViewModel, Submissions.Core.Models.EvolutionService.Theme>()
			//	.ReverseMap();

			CreateMap<Submissions.Core.Models.MathService.MathReviewModel, MathReviewInfo>()
				.ReverseMap();

			CreateMap<Submissions.Core.Models.MathService.ListValue, ListValue>()
				.ReverseMap();

			// Workload Manager
			CreateMap<Submissions.Core.Models.WorkloadManagerService.Resource, WorkloadManager.ResourceViewModel>();
			CreateMap<Submissions.Core.Models.WorkloadManagerService.Holiday, WorkloadManager.HolidayViewModel>();
			CreateMap<Submissions.Core.Models.WorkloadManagerService.DTORequest, WorkloadManager.DTORequestViewModel>();
			CreateMap<Submissions.Core.Models.WorkloadManagerService.Scheduled, WorkloadManager.TaskViewModel>()
				.ForMember(dest => dest.Status, src => src.MapFrom(x => x.OnHold == 1 ? "OH" : null))
				.ForMember(dest => dest.CommitStatus, opt => opt.Ignore());
			CreateMap<Submissions.Core.Models.WorkloadManagerService.WorkdaySchedule, WorkloadManager.WorkdayScheduleViewModel>();

			CreateMap<GLI.EFCore.Submission.tbl_Reviews, Review>()
				.ForMember(dest => dest.Details, opt => opt.Ignore())
				.ForMember(dest => dest.IsReviewer, opt => opt.Ignore())
				.ForMember(dest => dest.IsAssignee, opt => opt.Ignore())
				.ForMember(dest => dest.IsQA, opt => opt.Ignore())
				.ForMember(dest => dest.AllMarkCompleted, opt => opt.Ignore())
				.ForMember(dest => dest.AssigneeMarkComplete, opt => opt.Ignore())
				.ForMember(dest => dest.Filenumber, opt => opt.Ignore())
				.ForMember(dest => dest.CurrentUserId, opt => opt.Ignore())
				.ForMember(dest => dest.ActionList, opt => opt.Ignore())
				.ForMember(dest => dest.JurisdictionList, opt => opt.Ignore())
				.ForMember(dest => dest.AssigneeList, opt => opt.Ignore())
				.ForMember(dest => dest.WatcherList, opt => opt.Ignore())
				.ForMember(dest => dest.AssigneeListIds, opt => opt.Ignore())
				.ForMember(dest => dest.EmailList, opt => opt.Ignore())
				.ForMember(dest => dest.IssueSeverityOptions, opt => opt.Ignore())
				.ForMember(dest => dest.LetterwriterList, opt => opt.Ignore())
				.ForMember(dest => dest.ReviewerList, opt => opt.Ignore())
				.ForMember(dest => dest.ShowAssigneeEmailPopup, opt => opt.Ignore())
				.ForMember(dest => dest.Status, scr => scr.MapFrom(x => ((ProjectReviewStatus)Enum.Parse(typeof(ProjectReviewStatus), x.Status, true)).GetEnumDescription()))
				.ForMember(dest => dest.ReviewComments, scr => scr.MapFrom(x => x.ReviewComments))
				.ForMember(dest => dest.Completer, scr => scr.MapFrom(x => x.Completer.FName + " " + x.Completer.LName));

			CreateMap<tbl_Reviews, Models.Grids.Protrack.ProjectReviewGrid.ProjectReviewGridRecord>()
				.ForMember(dest => dest.FileNumber, scr => scr.MapFrom(x => x.Project.FileNumber))
				.ForMember(dest => dest.Department, scr => scr.MapFrom(x => x.Department.Code))
				.ForMember(dest => dest.Assignee, opt => opt.Ignore())
				.ForMember(dest => dest.Letterwriter, opt => opt.Ignore())
				.ForMember(dest => dest.Reviewer, opt => opt.Ignore())
				.ForMember(dest => dest.ViewDetails, opt => opt.Ignore())
				.ForMember(dest => dest.TargetDate, opt => opt.Ignore());

			CreateMap<GLI.EFCore.Submission.TaskHour, TaskHrVM>();
		}
	}
}