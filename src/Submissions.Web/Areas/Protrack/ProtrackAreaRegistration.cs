﻿using System.Web.Mvc;

namespace Submissions.Web.Areas.Protrack
{
	public class Protrack : AreaRegistration
	{
		public override string AreaName
		{
			get
			{
				return "Protrack";
			}
		}

		public override void RegisterArea(AreaRegistrationContext context)
		{
			context.MapRoute(
				"Protrack_EditTasks",
				"Protrack/{controller}/{action}/{id}",
				new { controller = "Tasks", action = "Edit" }
			);

			context.MapRoute(
				"Protrack_default",
				"Protrack/{controller}/{action}/{id}",
				new { action = "Index", id = UrlParameter.Optional }
			);
		}
	}
}