﻿extern alias ZEFCore;
using AutoMapper;
using GLI.EFCore.Submission;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Submissions.Common;
using Submissions.Core;
using Submissions.Core.Models.WorkloadManagerService;
using Submissions.Web.Areas.Protrack.Models.WorkloadManager;
using Submissions.Web.Models.Query;
using Submissions.Web.Models.Query.Protrack;
using Submissions.Web.Utility.Email;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Web.Mvc;
using ZEFCore::Z.EntityFramework.Plus;

namespace Submissions.Web.Areas.Protrack.Controllers
{
	using Submissions.Common.Email;

	public class WorkloadManagerController : Controller
	{
		private const int _calenderDays = 90;

		private readonly IUserContext _userContext;
		private readonly SubmissionContext _dbSubmission;
		private readonly IWorkloadManagerService _workloadManagerService;
		private readonly IEmailService _emailService;
		private readonly IProjectService _projectService;

		public WorkloadManagerController
		(
			IUserContext userContext,
			SubmissionContext dbSubmission,
			IWorkloadManagerService workloadManagerService,
			IEmailService emailService,
			IProjectService projectService
		)
		{
			_userContext = userContext;
			_dbSubmission = dbSubmission;
			_workloadManagerService = workloadManagerService;
			_emailService = emailService;
			_projectService = projectService;
		}

		#region Resource Finder
		public ActionResult ResourceFinder()
		{
			var vm = new ResourceFinderViewModel();
			SetFilters(ref vm);

			return View(vm);
		}

		[HttpPost]
		public ActionResult RequestResource(ResourceEmailViewModel email)
		{
			var request = new tbl_ResourceFinderRequest
			{
				AddUserId = _userContext.User.Id,
				RequestUserId = email.RequestUserId,
				ProjectId = Convert.ToInt32(email.ProjectId),
				RequestStartDate = email.RequestStartDate,
				AdditionalInfo = email.AdditionalInfo,
				Status = ResourceFinderRequestStatus.Pending.ToString()
			};

			_dbSubmission.tbl_ResourceFinderRequests.Add(request);
			_dbSubmission.SaveChanges();

			foreach (var task in email.Tasks)
			{
				var newTask = new tbl_ResourceFinderRequestTaskDefinition
				{
					ResourceFinderRequestId = request.Id,
					TaskDefinitionId = Convert.ToInt32(task.TaskDefinitionId),
					EstimateHours = task.EstimateHours
				};

				_dbSubmission.tbl_ResourceFinderRequestTaskDefinitions.Add(newTask);
			}

			_dbSubmission.SaveChanges();

			SendEmail(request.Id, "requested");

			return Json("ok");
		}
		#endregion

		#region Resource Requests
		public ActionResult ResourceRequests()
		{
			var vm = new ResourceRequestsViewModel();

			var myRequests = _dbSubmission.tbl_ResourceFinderRequests
				.Include(x => x.RequestUser.Manager)
				.Include(x => x.Project)
				.Include(x => x.ResourceFinderRequestTaskDefinitions)
				.Where(x => x.AddUserId == _userContext.User.Id && x.Status == ResourceFinderRequestStatus.Pending.ToString())
				.ToList()
				.Select(x => new MyRequest
				{
					Id = x.Id,
					RequestDate = x.RequestDate,
					Manager = x.RequestUser.Manager.FName + " " + x.RequestUser.Manager.LName,
					Resource = x.RequestUser.FName + " " + x.RequestUser.LName,
					ProjectId = x.ProjectId,
					ProjectName = x.Project.ProjectName,
					RequestStartDate = x.RequestStartDate,
					Tasks = x.ResourceFinderRequestTaskDefinitions.Select(y => y.TaskDefinition.Code + " - " + y.TaskDefinition.DynamicsId + " &nbsp;/&nbsp; " + y.EstimateHours).ToList(),
					AdditionalInfo = x.AdditionalInfo
				})
				.OrderByDescending(x => x.RequestDate)
				.ToList();

			var otherRequests = _dbSubmission.tbl_ResourceFinderRequests
				.Include(x => x.RequestUser.Manager)
				.Include(x => x.Project)
				.Include(x => x.ResourceFinderRequestTaskDefinitions)
				.Where(x => x.RequestUser.Manager.Id == _userContext.User.Id && x.Status == ResourceFinderRequestStatus.Pending.ToString())
				.ToList()
				.Select(x => new OtherRequest
				{
					Id = x.Id,
					RequestDate = x.RequestDate,
					Manager = x.AddUser.FName + " " + x.AddUser.LName,
					Resource = x.RequestUser.FName + " " + x.RequestUser.LName,
					ProjectId = x.ProjectId,
					ProjectName = x.Project.ProjectName,
					RequestStartDate = x.RequestStartDate,
					Tasks = x.ResourceFinderRequestTaskDefinitions.Select(y => y.TaskDefinition.Code + " - " + y.TaskDefinition.DynamicsId + " &nbsp;/&nbsp; " + y.EstimateHours).ToList(),
					AdditionalInfo = x.AdditionalInfo
				})
				.OrderByDescending(x => x.RequestDate)
				.ToList();

			vm.MyRequests = myRequests;
			vm.OtherRequests = otherRequests;

			return View(vm);
		}

		[HttpPost]
		public ActionResult ApproveRequest(int id)
		{
			var dbRequest = _dbSubmission.tbl_ResourceFinderRequests
				.Where(x => x.Id == id)
				.Single();

			var dbTasks = _dbSubmission.tbl_ResourceFinderRequestTaskDefinitions
				.Where(x => x.ResourceFinderRequestId == id)
				.Select(x => new
				{
					TaskDefinitionId = x.TaskDefinitionId,
					EstimateHours = x.EstimateHours
				})
				.ToList();

			var requestUser = _dbSubmission.trLogins.Find(dbRequest.RequestUserId);

			var tasks = new List<trTask>();
			foreach (var item in dbTasks)
			{
				tasks.Add
				(
					new trTask
					{
						ProjectId = dbRequest.ProjectId,
						EstimateHours = item.EstimateHours,
						TaskDefinitionId = item.TaskDefinitionId,
						UserId = dbRequest.RequestUserId,
						User = requestUser
					}
				);
			}

			_projectService.AddTasks(tasks);

			dbRequest.ResponseDate = DateTime.Now;
			dbRequest.Status = ResourceFinderRequestStatus.Approved.ToString();
			_dbSubmission.SaveChanges();

			SendEmail(id, "approved");

			return Json("ok");
		}

		[HttpPost]
		public ActionResult CancelRequest(int id)
		{
			SendEmail(id, "cancelled");

			_dbSubmission.tbl_ResourceFinderRequests.Where(x => x.Id == id).Delete();
			_dbSubmission.tbl_ResourceFinderRequestTaskDefinitions.Where(x => x.ResourceFinderRequestId == id).Delete();

			return Json("ok");
		}

		[HttpPost]
		public ActionResult DeclineRequest(int id, string reason)
		{
			var request = new tbl_ResourceFinderRequest
			{
				Id = id,
				ResponseDate = DateTime.Now,
				Status = ResourceFinderRequestStatus.Declined.ToString(),
				DeclineReason = reason
			};
			_dbSubmission.tbl_ResourceFinderRequests.Attach(request);
			_dbSubmission.Entry(request).Property(x => x.ResponseDate).IsModified = true;
			_dbSubmission.Entry(request).Property(x => x.Status).IsModified = true;
			_dbSubmission.Entry(request).Property(x => x.DeclineReason).IsModified = true;
			_dbSubmission.SaveChanges();

			SendEmail(id, "declined");

			return Json("ok");
		}

		private void SendEmail(int id, string action)
		{
			var request = _dbSubmission.tbl_ResourceFinderRequests
				.Where(x => x.Id == id)
				.Select(x => new
				{
					ManagerName = x.AddUser.FName + " " + x.AddUser.LName,
					ManagerEmail = x.AddUser.Email,
					RequestUserId = x.RequestUserId,
					RequestUserName = x.RequestUser.FName + " " + x.RequestUser.LName,
					RequestUserManagerEmail = x.RequestUser.Manager.Email,
					ProjectId = x.ProjectId,
					ProjectName = x.Project.ProjectName,
					RequestStartDate = x.RequestStartDate,
					AdditionalInfo = x.AdditionalInfo,
					DeclineReason = x.DeclineReason
				})
				.Single();

			var tasks = _dbSubmission.tbl_ResourceFinderRequestTaskDefinitions
				.Include(x => x.TaskDefinition)
				.Where(x => x.ResourceFinderRequestId == id)
				.Select(x => new ResourceFinderTaskEmail
				{
					TaskDefinitionId = x.TaskDefinitionId,
					TaskDefinition = x.TaskDefinition.Description,
					EstimateHours = x.EstimateHours
				})
				.ToList();

			var emailViewModel = new ResourceFinderRequestUserEmail
			{
				Action = action,
				ManagerName = request.ManagerName,
				RequestUserId = request.RequestUserId,
				RequestUserName = request.RequestUserName,
				ProjectName = request.ProjectName,
				ProjectUrl = Url.Action("Detail", "Project", new { area = "Protrack", id = request.ProjectId }, Request.Url.Scheme),
				RequestStartDate = request.RequestStartDate,
				AdditionalInfo = request.AdditionalInfo,
				Tasks = tasks,
				ProcessRequestUrl = Url.Action("ResourceRequests", "WorkloadManager", new { area = "Protrack" }, Request.Url.Scheme),
				DeclineReason = request.DeclineReason
			};

			var to = "";
			if (action == "requested" || action == "cancelled")
			{
				to = request.RequestUserManagerEmail;
			}
			else if (action == "approved" || action == "declined")
			{
				to = request.ManagerEmail;
			}

			var mailMsg = new MailMessage();
			mailMsg.From = new MailAddress(_userContext.User.Email);
			mailMsg.To.Add(new MailAddress(to));
			mailMsg.Subject = "Resource Request";
			mailMsg.Body = _emailService.RenderTemplate(EmailType.ResourceFinderRequestUser.ToString(), emailViewModel);
			_emailService.Send(mailMsg);
		}
		#endregion

		#region Workload Balancer
		[HttpPost]
		public ActionResult CommitChanges(IList<Scheduled> tasks)
		{
			var results = _workloadManagerService.CommitChanges(tasks);

			var changedTaskIds = results
				.Where(x => x.NewTask != null)
				.Select(x => new { OldTaskId = x.TaskToReassign.Id, NewTaskId = x.NewTask.Id })
				.ToList();

			return Content(ComUtil.JsonEncodeCamelCase(changedTaskIds), "application/json");
		}

		[HttpPost]
		public ActionResult RearrangeDates(Resource resource, IList<Scheduled> tasks)
		{
			var rearrangedTasks = _workloadManagerService.RearrangeDates(resource, tasks);
			return Content(ComUtil.JsonEncodeCamelCase(rearrangedTasks), "application/json");
		}

		[HttpPost]
		public ActionResult ResourceUnavailability(IList<Resource> resources)
		{
			foreach (var item in resources)
			{
				var resourceUnavailability = new tbl_ResourceUnavailability
				{
					Id = item.UnavailableId ?? 0,
					UserId = (int)item.Id,
					StartDate = item.UnavailableStartDate ?? DateTime.MinValue,
					EndDate = item.UnavailableEndDate,
					InterofficeAssignment = item.InterofficeAssignment
				};

				var state = EntityState.Unchanged;
				if (item.UnavailableId == null && item.UnavailableStartDate != null)
				{
					state = EntityState.Added;
				}
				else if (item.UnavailableId != null && item.UnavailableStartDate != null)
				{
					state = EntityState.Modified;
				}
				else if (item.UnavailableId != null && item.UnavailableStartDate == null)
				{
					item.UnavailableEndDate = null;
					state = EntityState.Deleted;
				}

				_dbSubmission.Entry(resourceUnavailability).State = state;
				_dbSubmission.SaveChanges();

				item.UnavailableId = state == EntityState.Deleted ? null : (int?)resourceUnavailability.Id;
			}

			return Content(ComUtil.JsonEncodeCamelCase(resources), "application/json");
		}

		public ActionResult WorkloadBalancer(MainFilterViewModel mainFilter)
		{
			var sessionUserId = _userContext.User.Id;

			if (mainFilter.ManagerId == null && mainFilter.TeamId == null && mainFilter.SupplierFocusId == null && mainFilter.UserLocationId == null)
			{
				mainFilter.ManagerId = sessionUserId;
			}

			if (mainFilter.ManagerId != null)
			{
				var manager = _dbSubmission.trLogins
					.Include(x => x.Department)
					.Where(x => x.Id == mainFilter.ManagerId)
					.Select(x => new { Name = x.FName + " " + x.LName, Department = x.Department.Code })
					.Single();

				mainFilter.Manager = manager.Name;
				mainFilter.ManagerDepartment = manager.Department;
			}

			var filters = LoadFilters();
			var servicefilters = ConvertToServiceFilters(filters);
			servicefilters.CalendarDays = _calenderDays;
			servicefilters.ManagerId = Convert.ToInt32(mainFilter.ManagerId);
			servicefilters.TeamId = Convert.ToInt32(mainFilter.TeamId);
			servicefilters.SupplierFocusId = Convert.ToInt32(mainFilter.SupplierFocusId);
			servicefilters.UserLocationId = Convert.ToInt32(mainFilter.UserLocationId);

			var results = _workloadManagerService.GetScheduled(servicefilters);

			var resourceRequest = _dbSubmission.tbl_ResourceFinderRequests
				.Where(x => (x.AddUserId == _userContext.User.Id || x.RequestUser.ManagerId == _userContext.User.Id) && x.Status == ResourceFinderRequestStatus.Pending.ToString())
				.AsEnumerable()
				.GroupBy(x => 1)
				.Select(x => new ResourceRequestCountsViewModel
				{
					MyRequests = x.Count(y => y.AddUserId == _userContext.User.Id),
					OtherRequests = x.Count(y => y.RequestUser.ManagerId == _userContext.User.Id)
				})
				.SingleOrDefault();

			var vm = new WorkloadBalancerViewModel();
			vm.CanEdit = Util.HasAccess(Permission.WorkloadManager, AccessRight.Edit);
			vm.CalendarOptions = new CalendarOptionsViewModel { Days = _calenderDays };
			vm.SessionUserId = sessionUserId;
			vm.MainFilter = mainFilter;
			vm.Resources = Mapper.Map<List<ResourceViewModel>>(results[0]);
			vm.Holidays = Mapper.Map<List<HolidayViewModel>>(results[1]);
			vm.DTORequests = Mapper.Map<List<DTORequestViewModel>>(results[2]);
			vm.Tasks = Mapper.Map<List<TaskViewModel>>(results[3]);
			vm.ResourceRequest = resourceRequest ?? new ResourceRequestCountsViewModel();
			vm.Favorites = LoadFavorites();
			vm.Filters = filters;

			return View(vm);
		}

		public ActionResult LoadResource(int userId, MainFilterViewModel mainFilter)
		{
			var dbResource = _dbSubmission.trLogins
				.Where(x => x.Id == userId)
				.Select(x => new { x.Id, Name = x.FName + " " + x.LName, x.LocationId, x.WorkdayHours, x.ManagerId })
				.Single();

			var isFavorite = _dbSubmission.tbl_WorkloadManagerFavorites
				.Where(x => x.ManagerId == _userContext.User.Id && x.UserId == userId)
				.Any();

			var resourceType = "team";
			if (isFavorite)
			{
				resourceType = "favorite";
			}
			else if (dbResource.ManagerId != _userContext.User.Id)
			{
				resourceType = "external";
			}

			var dbWorkdaySchedule = _dbSubmission.tbl_WorkdaySchedules
				.Where(x => x.UserId == userId)
				.Select(x => new WorkdayScheduleViewModel
				{
					UserId = x.UserId,
					DayOfWeek = (DayOfWeek)x.DayOfWeek,
					Hours = x.Hours
				})
				.ToList();

			var serviceFilters = ConvertToServiceFilters(LoadFilters());
			serviceFilters.CalendarDays = _calenderDays;
			serviceFilters.ManagerId = Convert.ToInt32(mainFilter.ManagerId);
			serviceFilters.TeamId = Convert.ToInt32(mainFilter.TeamId);
			serviceFilters.SupplierFocusId = Convert.ToInt32(mainFilter.SupplierFocusId);
			serviceFilters.UserLocationId = Convert.ToInt32(mainFilter.UserLocationId);
			var results = _workloadManagerService.GetScheduled(serviceFilters);

			var resource = new ResourceViewModel
			{
				Id = dbResource.Id,
				Name = dbResource.Name,
				LocationId = dbResource.LocationId,
				Type = resourceType,
				WorkdayHours = dbResource.WorkdayHours,
				WorkdaySchedule = dbWorkdaySchedule
			};

			var vm = new WorkloadBalancerViewModel();
			vm.Resources.Add(resource);
			vm.Holidays = Mapper.Map<List<HolidayViewModel>>(results[1]).Where(x => x.LocationId == resource.LocationId).ToList();
			vm.DTORequests = Mapper.Map<List<DTORequestViewModel>>(results[2]).Where(x => x.UserId == userId).ToList();
			vm.Tasks = Mapper.Map<List<TaskViewModel>>(results[3]).Where(x => x.UserId == userId).Select(x => { x.LentOrBorrowed = false; return x; }).ToList();

			return Content(ComUtil.JsonEncodeCamelCase(vm), "application/json");
		}

		[HttpPost]
		public ActionResult Favorites(string favorites)
		{
			var newFavorites = JsonConvert.DeserializeObject<List<FavoriteViewModel>>(favorites);

			_dbSubmission.tbl_WorkloadManagerFavorites.Where(x => x.ManagerId == _userContext.User.Id).Delete();

			foreach (var item in newFavorites.Where(x => x.UserId != null).ToList())
			{
				var dbFavorite = new tbl_WorkloadManagerFavorite
				{
					ManagerId = _userContext.User.Id,
					UserId = (int)item.UserId
				};

				_dbSubmission.tbl_WorkloadManagerFavorites.Add(dbFavorite);
			}

			_dbSubmission.SaveChanges();

			return RedirectToAction("WorkloadBalancer", Request.UrlReferrer.Query.ToRouteValues());
		}

		[HttpPost]
		public ActionResult SaveFilters(string filters)
		{
			var formFilters = JsonConvert.DeserializeObject<FiltersViewModel>(filters);
			formFilters.ExcludeDepartments = formFilters.ExcludeDepartments.Where(x => x.Selected).ToList();
			formFilters.ExcludeResources = null;
			formFilters.ExcludeTasks = formFilters.ExcludeTasks.Where(x => x.Id != null).ToList();
			var formFiltersJson = JsonConvert.SerializeObject(formFilters);

			var filtersSet = formFilters.ExcludeDepartments.Count > 0 ||
				formFilters.ExcludeResourceIds.Length > 0 ||
				formFilters.ExcludeTasks.Count > 0 ||
				!string.IsNullOrEmpty(formFilters.IncludeTasksBeginWith) ||
				formFilters.IncludeUnassignedTasksForENProjectLeadId != null;

			var dbFormFilters = _dbSubmission.tbl_FormFilters
				.Where(x => x.UserId == _userContext.User.Id && x.Section == FormFilterSection.WorkloadBalancer.ToString())
				.SingleOrDefault();

			if (dbFormFilters == null)
			{
				if (filtersSet)
				{
					var newFormFilter = new tbl_FormFilter
					{
						FilterName = "Default",
						Filters = formFiltersJson,
						Section = FormFilterSection.WorkloadBalancer.ToString(),
						UserId = _userContext.User.Id
					};

					_dbSubmission.tbl_FormFilters.Add(newFormFilter);
					_dbSubmission.SaveChanges();
				}
			}
			else if (!filtersSet)
			{
				_dbSubmission.tbl_FormFilters.Remove(dbFormFilters);
				_dbSubmission.SaveChanges();
			}
			else
			{
				dbFormFilters.Filters = formFiltersJson;
				_dbSubmission.SaveChanges();
			}

			return RedirectToAction("WorkloadBalancer", Request.UrlReferrer.Query.ToRouteValues());
		}
		#endregion

		#region Helper Methods
		private Submissions.Core.Models.WorkloadManagerService.Filters ConvertToServiceFilters(FiltersViewModel filters)
		{
			return new Submissions.Core.Models.WorkloadManagerService.Filters
			{
				ExcludeResourceIds = filters.ExcludeResourceIds == null ? new List<int>() : filters.ExcludeResourceIds.Select(int.Parse).ToList(),
				ExcludeTaskDepartmentIds = filters.ExcludeDepartments.Where(x => x.Selected).Select(x => x.Id).ToList(),
				ExcludeTaskDefinitionIds = filters.ExcludeTasks.Where(x => x.Id != null).Select(x => (int)x.Id).ToList(),
				IncludeTasksBeginWith = filters.IncludeTasksBeginWith,
				IncludeUnassignedTasksForENProjectLeadId = filters.IncludeUnassignedTasksForENProjectLeadId
			};
		}

		private IList<FavoriteViewModel> LoadFavorites()
		{
			var favorites = _dbSubmission.tbl_WorkloadManagerFavorites
				.Where(x => x.ManagerId == _userContext.User.Id)
				.OrderBy(x => x.User.FName)
				.Select(x => new FavoriteViewModel
				{
					UserId = x.UserId,
					Name = x.User.FName + " " + x.User.LName
				})
				.ToList();

			favorites.Add(new FavoriteViewModel { UserId = null, Name = "" });

			return favorites;
		}

		private FiltersViewModel LoadFilters()
		{
			var dbFormFilters = _dbSubmission.tbl_FormFilters
				.Where(x => x.UserId == _userContext.User.Id && x.Section == FormFilterSection.WorkloadBalancer.ToString())
				.Select(x => x.Filters)
				.SingleOrDefault();

			var filters = new FiltersViewModel();
			if (dbFormFilters != null)
			{
				filters = JsonConvert.DeserializeObject<FiltersViewModel>(dbFormFilters);
				filters.ExcludeResources = filters.ExcludeResources ?? new List<SelectListItem>();

				if (filters.ExcludeResourceIds != null && filters.ExcludeResourceIds.Length > 0)
				{
					var excludeResourceIds = filters.ExcludeResourceIds.Select(int.Parse).ToList();
					filters.ExcludeResources = _dbSubmission.trLogins
						.Where(x => excludeResourceIds.Contains(x.Id))
						.Select(x => new SelectListItem
						{
							Selected = true,
							Text = x.FName + " " + x.LName + " - " + x.Department.Code,
							Value = x.Id.ToString()
						})
						.OrderBy(x => x.Text)
						.ToList();
				}

				if (filters.IncludeUnassignedTasksForENProjectLeadId != null)
				{
					filters.IncludeUnassignedTasksForENProjectLead = _dbSubmission.trLogins
						.Where(x => x.Id == filters.IncludeUnassignedTasksForENProjectLeadId)
						.Select(x => x.FName + " " + x.LName + " - " + x.Department.Code)
						.SingleOrDefault();
				}
			}

			var defaultDepartmentCodes = new List<string>
			{
				Department.CP.ToString(),
				Department.ENG.ToString(),
				Department.MTH.ToString()
			};

			var defaultDepartments = _dbSubmission.trDepts
				.Where(x => x.Active && defaultDepartmentCodes.Contains(x.Code))
				.Select(x => new TaskDepartmentViewModel { Id = x.Id, Department = x.Description, Selected = true })
				.ToList();

			var excludeDepartments = (from dd in defaultDepartments
									  join f in filters.ExcludeDepartments on dd.Id equals f.Id into ps
									  from f in ps.DefaultIfEmpty()
									  select new TaskDepartmentViewModel
									  {
										  Id = dd.Id,
										  Department = dd.Department,
										  Selected = f == null ? false : true
									  })
									  .ToList();

			filters.ExcludeDepartments = excludeDepartments;
			filters.ExcludeTasks.Add(new TaskDefinitionViewModel { Id = null, Definition = "" });

			return filters;
		}

		private void SetFilters(ref ResourceFinderViewModel vm)
		{
			var formFilters = _dbSubmission.tbl_FormFilters
				.Where(x => x.UserId == _userContext.User.Id && x.Section == FormFilterSection.ResourceFinder.ToString() && x.Current)
				.Select(x => new { x.Id, x.FilterName, x.Filters })
				.SingleOrDefault();

			if (formFilters == null)
			{
				vm.Filters.ManagerIds = new string[] { _userContext.User.Id.ToString() };
				vm.Filters.Managers = new List<SelectListItem> { new SelectListItem { Value = _userContext.User.Id.ToString(), Text = _userContext.User.FName + " " + _userContext.User.LName, Selected = true } };
				vm.Filters.LocationIds = new string[] { _userContext.LocationId.ToString() };
				vm.Filters.Locations = new List<SelectListItem> { new SelectListItem { Value = _userContext.LocationId.ToString(), Text = _userContext.Location + " - " + _userContext.LocationName, Selected = true } };
			}
			else
			{
				var filters = JsonConvert.DeserializeObject<ResourceFinderSearch>(formFilters.Filters);

				vm.Filters = filters;
				vm.CurrentFilter = new CurrentFilter { Id = formFilters.Id, Name = formFilters.FilterName };

				if (filters.ManagerIds != null)
				{
					vm.Filters.Managers = _dbSubmission.trLogins.Where(x => filters.ManagerIds.Contains(x.Id.ToString())).Select(x => new SelectListItem { Value = x.Id.ToString(), Text = x.FName + " " + x.LName, Selected = true }).ToList();
				}

				if (filters.LocationIds != null)
				{
					vm.Filters.Locations = _dbSubmission.trLaboratories.Where(x => filters.LocationIds.Contains(x.Id.ToString())).Select(x => new SelectListItem { Value = x.Id.ToString(), Text = x.Location + " - " + x.Name, Selected = true }).ToList();
				}

				if (filters.ResourceIds != null)
				{
					vm.Filters.Resources = _dbSubmission.trLogins.Where(x => filters.ResourceIds.Contains(x.Id.ToString())).Select(x => new SelectListItem { Value = x.Id.ToString(), Text = x.FName + " " + x.LName, Selected = true }).ToList();
				}

				if (filters.ManufacturerCodes != null)
				{
					vm.Filters.Manufacturers = _dbSubmission.tbl_lst_fileManu.Where(x => filters.ManufacturerCodes.Contains(x.Code)).Select(x => new SelectListItem { Value = x.Code, Text = x.Description, Selected = true }).ToList();
				}

				if (filters.TaskDefinitionIds != null)
				{
					vm.Filters.TaskDefinitions = _dbSubmission.trTaskDefs.Where(x => filters.TaskDefinitionIds.Contains(x.Id.ToString())).Select(x => new SelectListItem { Value = x.Id.ToString(), Text = x.Code + " - " + x.DynamicsId, Selected = true }).ToList();
				}
			}
		}
		#endregion
	}
}