﻿using GLI.EFCore.Submission;
using Submissions.Common;
using Submissions.Core;
using Submissions.Web.Areas.Protrack.Models.Project;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace Submissions.Web.Areas.Protrack.Controllers
{
	public class CreateNewBundleController : Controller
	{
		private readonly SubmissionContext _dbSubmission;
		private readonly IProjectService _projectService;

		public CreateNewBundleController(SubmissionContext dbSubmission, IProjectService projectService)
		{
			_dbSubmission = dbSubmission;
			_projectService = projectService;
		}

		public ActionResult Index(int AddToExisitingProjectId = 0)
		{
			var vm = new CreateNewBundleVM();
			if (AddToExisitingProjectId == 0)
			{
				vm.Filters.ProjectTestingType = BundleType.OriginalTesting;
			}
			else
			{
				var existingProject = _dbSubmission.trProjects.Where(x => x.Id == AddToExisitingProjectId).Select(x => new { x.ProjectName, x.BundleTypeId }).SingleOrDefault();
				vm.AddToExistingProject = existingProject.ProjectName;
				vm.AddToExistingProjectId = AddToExisitingProjectId;
				vm.Filters.ProjectTestingType = (existingProject.BundleTypeId == 1) ? BundleType.ClosedTesting : BundleType.OriginalTesting;
			}

			//Select all Closed tasks
			var appendText = BundleType.ClosedTesting.GetEnumDescription();

			var allClosedTasks = _dbSubmission.QABundles_TaskDef.Where(x => x.TaskForBundlesDN == true).Select(x => new SelectListItem() { Text = x.TaskDefDesc + " (<font class='small'>" + appendText + "</font>)", Value = (BundleType.ClosedTesting).ToString() }).ToList();

			var allAvailableBundleTypes = new List<SelectListItem>();
			allAvailableBundleTypes.Add(new SelectListItem() { Text = BundleType.OriginalTesting.GetEnumDescription(), Value = (BundleType.OriginalTesting).ToString() });
			allAvailableBundleTypes.AddRange(allClosedTasks);

			vm.BundleTypeOptions = allAvailableBundleTypes;

			return View(vm);
		}

		public JsonResult CheckAddToExistingProject(string projectName)
		{
			var projectInfo = _dbSubmission.trProjects.Where(x => x.ProjectName == projectName && x.IsTransfer == true && x.ENStatus != ProjectStatus.DN.ToString()).Select(x => new { ProjectId = x.Id, x.BundleTypeId }).SingleOrDefault();
			if (projectInfo != null)
			{
				return Json(projectInfo, JsonRequestBehavior.AllowGet);
			}
			else
			{
				return Json(new { ProjectId = 0, BundleTypeId = 0 }, JsonRequestBehavior.AllowGet);
			}
		}

		public ContentResult CanBeAddedToBundleType(string bundleType, List<int> primaries)
		{
			var response = System.Net.HttpStatusCode.OK.ToString();
			var jurStatus = _dbSubmission.JurisdictionalData.Where(x => primaries.Contains(x.Id)).Select(x => new { Primary = x.Id, Status = x.Status }).ToList();
			if (bundleType == BundleType.OriginalTesting.ToString())
			{
				if (jurStatus.Any(x => (x.Status != JurisdictionStatus.RA.ToString() && x.Status != JurisdictionStatus.CP.ToString() && x.Status != JurisdictionStatus.OH.ToString())))
				{
					response = "NoOK";
				}
			}
			else if (bundleType == BundleType.ClosedTesting.ToString())
			{
				if (jurStatus.Any(x => (x.Status == JurisdictionStatus.RA.ToString() || x.Status == JurisdictionStatus.CP.ToString() || x.Status == JurisdictionStatus.OH.ToString())))
				{
					response = "NoOK";
				}
			}
			return Content(response);
		}

		public JsonResult CreateNewBundle(List<int> primaries, string bundleType, int existingProjectId = 0)
		{
			var lPrimaries = primaries;
			var initialProjectId = _dbSubmission.trProjectJurisdictionalData.Where(x => primaries.Contains(x.JurisdictionalDataId)).Select(x => x.ProjectId).FirstOrDefault();
			var projectId = _projectService.CreateNewOrAddToExisting(lPrimaries, (int)Enum.Parse(typeof(BundleType), bundleType), existingProjectId);

			_dbSubmission.SaveChanges();

			if (initialProjectId != 0)
			{
				_projectService.SetDefaultQAOffice(initialProjectId);
			}
			return Json(new { ProjectId = projectId }, JsonRequestBehavior.AllowGet);
		}

		[OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
		public ActionResult SearchBundles(string fileNumber)
		{
			var bundles = _dbSubmission.trProjects.Where(x => x.FileNumber == fileNumber && x.IsTransfer == true)
				.Select(x => new BundlesSearch { Name = x.ProjectName, Id = x.Id, Status = x.ENStatus, Holder = x.Holder, TypeId = x.BundleTypeId }).ToList();
			return PartialView("_SearchBundles", bundles);
		}
	}
}