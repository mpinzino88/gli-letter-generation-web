﻿using GLI.EFCore.Submission;
using Newtonsoft.Json;
using Submissions.Common;
using Submissions.Core;
using Submissions.Web.Areas.Protrack.Models.Project;
using Submissions.Web.Models.Query;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace Submissions.Web.Areas.Protrack.Controllers
{
	public class MergeProjectsController : Controller
	{
		private readonly SubmissionContext _dbSubmission;
		private readonly IProjectService _projectService;
		private readonly IQueryService _queryService;

		public MergeProjectsController(SubmissionContext dbSubmission, IProjectService projectService, IQueryService queryService)
		{
			_dbSubmission = dbSubmission;
			_projectService = projectService;
			_queryService = queryService;
		}

		public ActionResult Index(int Id)
		{
			var vm = new MergeProjects();
			var projectInfo = _dbSubmission.trProjects.Where(x => x.Id == Id).Select(x => new { x.ProjectName, x.IsTransfer, ProjectStatus = x.ENStatus, x.BundleTypeId }).SingleOrDefault();
			vm.ProjectId = Id;
			vm.ProjectName = projectInfo.ProjectName;
			vm.ProjectStatus = projectInfo.ProjectStatus;
			vm.IsTransfer = (projectInfo.IsTransfer == true) ? true : false;
			vm.BundleTypeId = projectInfo.BundleTypeId;
			return View("Index", vm);
		}

		public JsonResult SearchProjects(int projectId, string filters)
		{
			var bundleTypeId = _dbSubmission.trProjects.Where(x => x.Id == projectId).Select(x => x.BundleTypeId).Single();
			var mergeFilters = JsonConvert.DeserializeObject<ProjectSearch>(filters);
			var query = _dbSubmission.trProjects.Where(x => x.Id != projectId && x.ENStatus != ProjectStatus.DN.ToString()).AsQueryable();

			if (!string.IsNullOrEmpty(mergeFilters.FileNumber))
			{
				query = query.Where(x => x.ProjectName.StartsWith(mergeFilters.FileNumber)).AsQueryable();
			}

			if (!string.IsNullOrEmpty(mergeFilters.Jurisdiction))
			{
				query = query.Where(x => x.JurisdictionalData.Any(y => y.JurisdictionalData.JurisdictionId == mergeFilters.Jurisdiction)).AsQueryable();
			}

			if (!string.IsNullOrEmpty(mergeFilters.ManufacturerShort))
			{
				query = query.Where(x => x.FileNumber.Contains(mergeFilters.ManufacturerShort)).AsQueryable();
			}

			var projects = query.Select(x => new SearchResults()
			{
				ProjectId = x.Id,
				ProjectName = x.ProjectName,
				Status = x.ENStatus,
				IsTransfer = x.IsTransfer,
				Supervisor = x.ENManager.FName + " " + x.ENManager.LName,
				BundleTypeId = x.BundleTypeId
			}).OrderBy(x => x.ProjectName).ToList();

			return Json((projects), JsonRequestBehavior.AllowGet);
		}

		public ActionResult IsMergeItemsFromSameFile(int origProjectId, int mergeWithId)
		{
			var itemsFromSameFile = "No";
			var whichOriginalProject = _dbSubmission.trProjects.Where(x => (x.Id == origProjectId || x.Id == mergeWithId)).Select(x => new { Id = x.Id, Name = x.ProjectName, IsTransfer = x.IsTransfer }).ToList();

			if (whichOriginalProject.Where(x => x.IsTransfer == false).Count() == 1)
			{
				var origProject = whichOriginalProject.Where(x => x.IsTransfer == false).Select(x => x.Name).FirstOrDefault();
				//check if all items from Transfer are from same file
				var transferProject = whichOriginalProject.Where(x => x.IsTransfer == true).Select(x => x.Id).FirstOrDefault();

				var itemsFromDiffFile = _dbSubmission.trProjectJurisdictionalData.Where(x => x.ProjectId == transferProject && x.JurisdictionalData.Submission.FileNumber != origProject).Any();
				itemsFromSameFile = (itemsFromDiffFile) ? "No" : "Yes";
			}

			return Content(itemsFromSameFile);
		}

		public ActionResult IsProjectHasBilledHours(int projectId)
		{
			var results = _queryService.ProjectUnassignedTaskHours(projectId).ToList();
			var TotalActualAssigned = results.Where(x => x.IsAssigned == true).ToList().Sum(x => x.Hours);

			return Content((TotalActualAssigned > 0) ? "Yes" : "No");
		}

		public ContentResult Save(MergeProjects vm, string SelProjectsToMerge)
		{
			var projectId = 0;
			var selMergeProjects = JsonConvert.DeserializeObject<List<MergeProject>>(SelProjectsToMerge);
			//check if any selected is a new submission, then Merge orig transfer into New submission
			if (vm.IsTransfer == true && (selMergeProjects.Where(x => x.IsTransfer == false && x.WithIndicator == false).Any()))
			{
				projectId = selMergeProjects.Where(x => x.IsTransfer == false && x.WithIndicator == false).Select(x => x.ProjectId).FirstOrDefault();
			}
			else
			{
				projectId = vm.ProjectId;
			}

			foreach (var project in selMergeProjects)
			{
				var projectInfo = _dbSubmission.trProjects.Where(x => x.Id == project.ProjectId).Select(x => new { x.BundleTypeId, x.MergedWith }).SingleOrDefault();
				if (vm.BundleTypeId != projectInfo.BundleTypeId)
				{
					project.WithIndicator = true;
				}

				if (project.ProjectId == projectId)//Merge Transfer into selected New submission
				{
					_projectService.MergeProjects(projectId, vm.ProjectId, vm.MergeReason, project.WithIndicator);
				}
				else
				{
					_projectService.MergeProjects(projectId, project.ProjectId, vm.MergeReason, project.WithIndicator);
				}
			}
			return Content(projectId.ToString());
		}

		public ActionResult UnMerge(int projectId, int mergeProjectId)
		{
			_projectService.UnMerge(projectId, mergeProjectId);
			return RedirectToAction("Detail", "Project", new { Id = projectId });
		}
	}
}