﻿using GLI.EFCore.Submission;
using Submissions.Core;
using Submissions.Web.Areas.Protrack.Models.Project;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace Submissions.Web.Areas.Protrack.Controllers
{
	public class ExpediteController : Controller
	{
		private readonly IProjectService _projectService;
		private readonly SubmissionContext _dbSubmission;

		public ExpediteController(IProjectService projectService, SubmissionContext submissionContext)
		{
			_projectService = projectService;
			_dbSubmission = submissionContext;
		}

		public ActionResult Index(int projectId)
		{
			var vm = new ExpediteProject();
			var qaBundle = _dbSubmission.QABundles.Where(x => x.ProjectId == projectId).Single();
			vm.ProjectIds = projectId.ToString();
			vm.Expedite = (qaBundle.IsRush == 1) ? true : false;
			vm.ExpediteReason = qaBundle.RushReason;
			vm.ExpediteNote = qaBundle.RushNotes;
			return PartialView("_ExpediteProject", vm);
		}

		public ActionResult BatchExpediteProject(string projectIds, bool Expedite)
		{
			var vm = new ExpediteProject();
			vm.ProjectIds = projectIds;
			vm.Expedite = Expedite;
			return PartialView("_ExpediteProject", vm);
		}

		public JsonResult SaveExpedite(ExpediteProject vm)
		{
			if (!string.IsNullOrEmpty(vm.ProjectIds))
			{
				var listProject = new List<int>(vm.ProjectIds.Split(',').Select(x => int.Parse(x)));
				var isExpedite = Convert.ToByte(vm.Expedite);

				for (int i = 0; i < listProject.Count; i++)
				{
					_projectService.UpdateProjectToExpedite(listProject[i], vm.Expedite, vm.ExpediteReason, vm.ExpediteNote);
				}
				var results = _dbSubmission.QABundles.Where(x => listProject.Contains(x.Id)).Select(x => new { ProjectId = x.Id, NewVal = x.IsRush }).ToList();
				return Json(results, JsonRequestBehavior.AllowGet);
			}
			else
			{
				return Json("NoData", JsonRequestBehavior.AllowGet);
			}
		}
	}
}