﻿using GLI.EFCore.Submission;
using Submissions.Common;
using Submissions.Core;
using Submissions.Web.Areas.Protrack.Models.Project;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Web.Configuration;
using System.Web.Mvc;

namespace Submissions.Web.Areas.Protrack.Controllers
{
	using Submissions.Common.Email;

	public class ProjectNoteController : Controller
	{
		private readonly IUserContext _userContext;
		private readonly SubmissionContext _dbSubmission;
		private readonly INoteService _noteService;
		private readonly IEmailService _emailService;

		public ProjectNoteController(IUserContext userContext, INoteService noteService, SubmissionContext submissionContext, IEmailService emailService)
		{
			_userContext = userContext;
			_dbSubmission = submissionContext;
			_noteService = noteService;
			_emailService = emailService;
		}

		public ActionResult Index(int projectId)
		{
			var vm = new ProjectNoteVM();
			vm.RefreshNotes = true;
			vm.ProjectIds = projectId.ToString();
			vm.NoteId = 0;
			var dept = "";
			switch (_userContext.DepartmentId)
			{
				case ((int)Department.ENG):
					dept = "EN";
					break;
				case ((int)Department.QA):
					dept = "QA";
					break;
				case ((int)Department.CP):
					dept = "CP";
					break;
				case ((int)Department.MTH):
					dept = "MA";
					break;
				default:
					break;
			}
			vm.Dept = dept;

			return View("_ProjectNote", vm);
		}

		public ActionResult EditQANote(int projectId, int noteId)
		{
			var vm = new ProjectNoteVM();
			vm.ProjectIds = projectId.ToString();
			vm.RefreshNotes = true;
			if (noteId > 0)
			{
				var projectNote = _dbSubmission.QABundles_Notes.Where(x => x.Id == noteId).Select(x => new { Note = x.Note }).Single();
				vm.NoteId = noteId;
				vm.Note = projectNote.Note;
				vm.IsViewableByCustomer = false;
				vm.Dept = Department.QA.ToString();
			}
			return View("_ProjectNote", vm);
		}

		public ActionResult EditNote(int projectId, int noteId)
		{
			var vm = new ProjectNoteVM();
			vm.ProjectIds = projectId.ToString();
			vm.RefreshNotes = true;
			if (noteId > 0)
			{
				var projectNote = _dbSubmission.trProjectNotes.Where(x => x.Id == noteId).Select(x => new { Note = x.Note, Scope = x.Scope, Dept = x.Dept }).Single();
				vm.NoteId = noteId;
				vm.Note = projectNote.Note;
				vm.IsViewableByCustomer = (projectNote.Scope == "E") ? true : false;
				if (projectNote.Dept == "EN")
				{
					vm.Dept = Department.ENG.ToString();
				}
				else if (projectNote.Dept == "MA")
				{
					vm.Dept = Department.MTH.ToString();
				}
				else
				{
					vm.Dept = projectNote.Dept;
				}
			}
			return View("_ProjectNote", vm);
		}


		public ActionResult BatchProjectNote(string projectIds)
		{
			var vm = new ProjectNoteVM();
			vm.ProjectIds = projectIds;
			vm.NoteId = 0;
			var dept = "";
			switch (_userContext.DepartmentId)
			{
				case ((int)Department.ENG):
					dept = "EN";
					break;
				case ((int)Department.QA):
					dept = "QA";
					break;
				case ((int)Department.CP):
					dept = "CP";
					break;
				case ((int)Department.MTH):
					dept = "MA";
					break;
				default:
					break;
			}
			vm.Dept = dept;
			return PartialView("_ProjectNote", vm);
		}

		public ActionResult Save(ProjectNoteVM vm)
		{
			var projectlist = new List<int>(vm.ProjectIds.Split(',').Select(x => int.Parse(x)));
			for (int i = 0; i < projectlist.Count; i++)
			{
				var projectId = projectlist[i];
				var qaBundleId = _dbSubmission.QABundles.Where(x => x.ProjectId == projectId).Select(x => x.Id).SingleOrDefault();
				var scope = (vm.IsViewableByCustomer) ? "E" : "I";
				var dept = "";

				if (vm.Dept == Department.ENG.ToString())
				{
					dept = "EN";
				}
				else if (vm.Dept == Department.MTH.ToString())
				{
					dept = "MA";
				}
				else
				{
					dept = vm.Dept;
				}

				if (vm.NoteId == 0)
				{
					if (vm.Dept == Department.QA.ToString()) //this is a temp Solution, once QA Protrack goes live, all notes will be merged in trProjectNotes
					{
						var newNote = new QABundles_Notes()
						{
							BundleId = qaBundleId,
							TimeStamp = DateTime.Now,
							UserId = _userContext.User.Id,
							Note = vm.Note
						};
						_noteService.AddQANote(newNote);
					}
					else
					{
						var newNote = new trProjectNotes()
						{
							Id = vm.NoteId,
							AddDate = DateTime.Now,
							ProjectId = projectId,
							Dept = dept,
							Note = vm.Note,
							Scope = scope,
							UserId = _userContext.User.Id
						};
						_noteService.Add(newNote);
					}
				}
				else
				{
					if (vm.Dept == Department.QA.ToString())
					{
						var editNote = _dbSubmission.QABundles_Notes.Where(x => x.Id == vm.NoteId).Single();

						editNote.Note = vm.Note;
						editNote.UserId = _userContext.User.Id;
						_noteService.UpdateQANote(editNote);
					}
					else
					{
						var editNote = _dbSubmission.trProjectNotes.Where(x => x.Id == vm.NoteId).Single();

						editNote.Dept = dept;
						editNote.Note = vm.Note;
						editNote.Scope = scope;
						editNote.UserId = _userContext.User.Id;
						_noteService.Update(editNote);
					}
				}

				//Email users
				if (vm.NotifySupervisor || vm.NotifyDevRep || vm.NotifyProjectLead || vm.NotifyEngUsers || vm.NotifyMathUsers || vm.NotifyQAUsers || vm.NotifyAll)
				{
					var projectInfo = _dbSubmission.trProjects.Where(x => x.Id == projectId).Select(x => new { ManagerId = x.ENManagerId, ProjectName = x.ProjectName }).Single();
					//Send an email
					var notifyUsers = new List<int>();
					var notifyAllUsers = new List<int>();
					if (vm.NotifySupervisor || vm.NotifyAll)
					{
						if (projectInfo.ManagerId != null)
						{
							notifyUsers.Add((int)projectInfo.ManagerId);
							notifyAllUsers.Add((int)projectInfo.ManagerId);
						}
					}
					if (vm.NotifyDevRep || vm.NotifyAll)
					{
						var devRep = _dbSubmission.trProjects.Where(x => x.Id == projectId).Select(x => x.DevelopmentRepresentativeId).SingleOrDefault();
						if (devRep != null)
						{
							notifyUsers.Add((int)devRep);
							notifyAllUsers.Add((int)devRep);
						}
					}
					if (vm.NotifyProjectLead || vm.NotifyAll)
					{
						var projLead = _dbSubmission.trProjects.Where(x => x.Id == projectId).Select(x => x.ENProjectLeadId).SingleOrDefault();
						if (projLead != null)
						{
							notifyUsers.Add((int)projLead);
							notifyAllUsers.Add((int)projLead);
						}
					}
					if (vm.NotifyEngUsers || vm.NotifyAll)
					{
						var engUsers = _dbSubmission.trTasks.Where(x => x.ProjectId == projectId && x.UserId != null && x.DepartmentId == 1).Select(x => (int)x.UserId).ToList();
						notifyUsers.AddRange(engUsers);
						notifyAllUsers.AddRange(engUsers);
					}
					if (vm.NotifyMathUsers || vm.NotifyAll)
					{
						var mathUsers = _dbSubmission.trTasks.Where(x => x.ProjectId == projectId && x.UserId != null && x.DepartmentId == 2).Select(x => (int)x.UserId).ToList();
						notifyUsers.AddRange(mathUsers);
						notifyAllUsers.AddRange(mathUsers);
					}
					if (vm.NotifyQAUsers || vm.NotifyAll)
					{
						var qaUsers = ((from t in _dbSubmission.QABundles_Task
										join b in _dbSubmission.QABundles on t.BundleID equals b.Id
										where (b.ProjectId == projectId && t.UserId != 0)
										select (int)t.UserId).ToList());
						notifyUsers.AddRange(qaUsers);
						notifyAllUsers.AddRange(qaUsers);
					}
					var userEmail = new List<string>();
					if (vm.NotifyAll)
					{
						userEmail = _dbSubmission.trLogins.Where(x => notifyAllUsers.Contains(x.Id)).Select(x => x.Email).ToList();

					}
					else
					{
						userEmail = _dbSubmission.trLogins.Where(x => notifyUsers.Contains(x.Id)).Select(x => x.Email).ToList();

					}

					var emailAddresses = string.Join(",", userEmail);
					if (WebConfigurationManager.AppSettings["Environment"] != SubmissionContext.Environment.Production.ToString())
					{
						emailAddresses = _userContext.User.Email;
					}
					else
					{
						emailAddresses = (!string.IsNullOrEmpty(emailAddresses)) ? emailAddresses + "," + _userContext.User.Email : _userContext.User.Email;
					}

					var mailMsg = new MailMessage("noreply@gaminglabs.com", emailAddresses);
					var msg = "";
					var modifiedBy = _userContext.User.FName + "  " + _userContext.User.LName;
					if (vm.NoteId == 0)
					{
						msg = "The following note was added for " + projectInfo.ProjectName + " by " + modifiedBy;
					}
					else
					{
						msg = "The following note was modified for " + projectInfo.ProjectName + " by " + modifiedBy;
					}
					mailMsg.Subject = msg;
					mailMsg.IsBodyHtml = true;
					mailMsg.Body = msg + "<br><br> Note: " + vm.Note;
					_emailService.Send(mailMsg);
				}
			}
			//***
			return Content("OK");
		}
	}
}