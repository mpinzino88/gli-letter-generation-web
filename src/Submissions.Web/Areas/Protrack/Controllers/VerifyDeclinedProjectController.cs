﻿using Submissions.Core;
using Submissions.Web.Areas.Protrack.Models.VerifyDeclinedProject;
using System.Collections.Generic;
using System.Web.Mvc;

namespace Submissions.Web.Areas.Protrack.Models
{
	public class VerifyDeclinedProjectController : Controller
	{
		private readonly IProjectService _projectService;

		public VerifyDeclinedProjectController(IProjectService projectService)
		{
			_projectService = projectService;
		}

		public ActionResult Index()
		{
			var vm = new IndexViewModel();
			vm.Filters.IsDeclined = true;
			vm.Filters.CheckHasEngTask = true;
			vm.Filters.IsTransfer = ProjectType.All;
			return View("Index", vm);
		}

		[HttpPost]
		public ActionResult DeleteProjects(List<int> projectIds)
		{
			_projectService.DeleteProject(projectIds);
			return Content("OK");
		}

		[HttpPost]
		public ActionResult PreserveDeclined(List<int> projectIds)
		{
			_projectService.PreserveDeclinedProject(projectIds);
			return Content("OK");
		}
	}
}