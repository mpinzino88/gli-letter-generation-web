﻿extern alias ZEFCore;
using AutoMapper;
using EF.eResultsManaged;
using Elmah;
using EvoLogic.Models;
using EvoLogic.SaveData;
using GLI.EFCore.Submission;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Submissions.Common;
using Submissions.Core;
using Submissions.Web.Areas.Protrack.Models.IncomingReview;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Mvc;
using ZEFCore::Z.EntityFramework.Plus;
using EFGLIAccess = EF.GLIAccess;
using MasterFileServiceAlias = Submissions.Core.Models.MasterFileService;

namespace Submissions.Web.Areas.Protrack.Controllers
{
	public class IncomingReviewController : Controller
	{
		private readonly SubmissionContext _dbSubmission;
		private readonly EFGLIAccess.IGLIAccessContext _dbGLIAccess;
		private readonly IMasterFileService _masterFileService;
		private readonly IProjectService _projectService;
		private readonly IUserContext _userContext;
		private readonly IIncomingReviewService _incomingReviewService;
		private readonly ISubProjectService _subProjectService;
		private readonly IEvolutionService _evolutionService;
		private readonly ILogService _logService;
		private readonly IEvoService _evoService;

		//EvoLogicLibrary
		private readonly IeResultsManagedContext _dbeResultsManaged;
		private readonly IInstanceMethods _instanceMethod;

		public IncomingReviewController
		(
			SubmissionContext dbSubmission,
			EFGLIAccess.IGLIAccessContext dbGLIAccess,
			IMasterFileService masterFileService,
			IProjectService projectService,
			IUserContext userContext,
			IIncomingReviewService incomingReviewService,
			ISubProjectService subProjectService,
			IEvolutionService evolutionService,
			IeResultsManagedContext dbeResultsManaged,
			ILogService logService,
			IEvoService evoService,
			IInstanceMethods instanceMethods
			)
		{
			_dbSubmission = dbSubmission;
			_dbGLIAccess = dbGLIAccess;
			_masterFileService = masterFileService;
			_projectService = projectService;
			_evolutionService = evolutionService;
			_userContext = userContext;
			_incomingReviewService = incomingReviewService;
			_subProjectService = subProjectService;
			_dbeResultsManaged = dbeResultsManaged;
			_logService = logService;
			_evoService = evoService;
			_instanceMethod = instanceMethods;
		}

		#region Index
		public ActionResult Index(int id)
		{
			return View(LoadIndexViewModel(id));
		}

		private IndexViewModel LoadIndexViewModel(int id)
		{
			trProject _project = _dbSubmission.trProjects.Where(x => x.Id == id).Single();

			var editedBy = _project.IncomingReviewEditedBy != null ? _dbSubmission.trLogins.Where(x => x.Id == _project.IncomingReviewEditedBy).Select(x => new { x.FName, x.LName }).SingleOrDefault() : null;
			var completedBy = _project.IncomingReviewCompletedBy != null ? _dbSubmission.trLogins.Where(x => x.Id == _project.IncomingReviewCompletedBy).Select(x => new { x.FName, x.LName }).SingleOrDefault() : null;
			var managerDetails = _dbSubmission.trLogins.Where(x => x.Id == _userContext.User.Id).Select(m => new { managerId = m.ManagerId, status = m.Manager.Status }).SingleOrDefault();
			//var manager = managerDetails.status == "A" ? managerDetails.managerId : null;
			var managerId = _dbSubmission.trLogins.Where(x => x.Id == _userContext.User.Id).Select(x => x.ManagerId).SingleOrDefault();
			var offset = _dbSubmission.tbl_lst_Juris_TargetOffSet
							.Join(_dbSubmission.trProjectJurisdictionalData, jurisoffset => jurisoffset.JurisdictionId, pj => pj.JurisdictionalData.JurisdictionId, (jurisoffset, pj) => new { Offset = jurisoffset, ProjectJurisdictionalData = pj })
							.Where(y => y.ProjectJurisdictionalData.ProjectId == _project.Id && y.Offset.TargetDayOffSet > 0)
							.Select(o => new Models.JurisdictionTargetOffset
							{
								Jurisdiction = o.Offset.Jurisdiction.Name,
								Offset = o.Offset.TargetDayOffSet,
								Reason = o.Offset.Reason
							}).Distinct().ToList();

			var vm = new IndexViewModel
			{
				ProjectId = _project.Id,
				ProjectStatus = _project.ENStatus,
				HasCompletedFormEditPermission = Web.Util.HasAccess(Web.Permission.EditCompleteIncRev, AccessRight.Edit),
				OnHold = _project.OnHold == 1 ? true : false,
				FileNumber = _project.ProjectName,
				OriginalTargetDateErrors = new List<string>(),
				ProjDetailErrors = new List<string>(),
				ResubmissionErrors = new List<string>(),
				EditedDate = _project.IncomingReviewEditDate?.ToString(@"MM\/dd\/yyyy"),
				IncomingReviewEditedBy = editedBy != null ? editedBy.FName + " " + editedBy.LName : null,
				CompletedBy = completedBy != null ? completedBy.FName + " " + completedBy.LName : null,
				CompletedDate = _project.IncomingReviewCompleteDate?.ToString(@"MM\/dd\/yyyy"),
				ManufacturerCode = new FileNumber(_project.ProjectName).ManufacturerCode,
				OriginalFilenumber = _project.FileNumber,
				CurrentUserId = _userContext.User.Id,
				ManagerUserId = managerId,
				CurrentUsername = _userContext.User.Username,
				JurisdictionTargetOffsetRules = offset,
				ApplyJurisdictionOffset = null,
			};
			SetProjectDetailsViewModel(_project, vm);
			setOriginalTargetDateViewModel(_project, vm);
			setManufacturerViewModel(_project.Id, vm);
			setBillingViewModel(_project.Id, vm);
			SetUKViewModel(_project.Id, vm);

			EvoLogic(_project, vm);

			if (_project.ENStatus != ProjectStatus.UU.ToString())
			{
				vm.ProjectAcceptDate = (DateTime)_project.ENAcceptDate;
				var hoursSinceAcceptance = (DateTime.Now - (DateTime)_project.ENAcceptDate).TotalHours;
				vm.HoursSinceAcceptance = hoursSinceAcceptance;
				vm.TaskLockEnforced = hoursSinceAcceptance > 96;
			}
			vm.IncomingReviewStatus = _incomingReviewService.SetIncomingReviewStatus(_project);
			vm.HasUK = _project.Lab == Laboratory.UK.ToString() ? true : false;
			vm.HasUKPermission = Web.Util.HasAccess(Web.Permission.UKQueue, AccessRight.Edit);

			// default Point.Click.Submit data on initial load
			if (_project.IncomingReviewEditDate == null)
			{
				var submitRequestIds = _dbSubmission.trProjectJurisdictionalData
					.Where(x => x.ProjectId == vm.ProjectId)
					.Select(x => x.JurisdictionalData.SubmitRequestId)
					.ToList();

				var pcs = _dbGLIAccess.electronicSubmissionMasters
					.Where(x => submitRequestIds.Contains(x.Id))
					.Select(x => new { x.StudioId, StudioCode = x.Studio.Name, x.GoLiveDate, x.TargetDate })
					.FirstOrDefault();

				if (pcs != null)
				{
					vm.ManufacturerSpecific.StudioId = pcs.StudioId;
					vm.ManufacturerSpecific.Studio = pcs.StudioCode;
					vm.ProjectDetails.General.GoLiveDate = pcs.GoLiveDate;
					vm.ProjectDetails.General.RequestedDeliveryDate = pcs.TargetDate;
				}

				if (string.IsNullOrEmpty(vm.OriginalTargetDate.Date))
				{
					var requestedTargetDate = _dbGLIAccess.electronicSubmissionMasters
						.Where(x => submitRequestIds.Contains(x.Id))
						.Min(x => x.TargetDate);

					vm.OriginalTargetDate.Date = requestedTargetDate.ToString("d");
				}
			}

			return vm;
		}

		[HttpPost]
		public ActionResult SaveIncomingReview(IndexViewModel model)
		{
			try
			{
				SaveProjectDetails(model);
				SaveManufacturerSpecific(model);
				SaveBilling(model);
				if (model.OriginalTargetDate.Date != null)
				{
					SaveTargetDate(model);
				}
				SaveIncomingReviewEditAndComplete(model);

				Response.StatusCode = (int)HttpStatusCode.OK;
				model.OriginalTargetDate.ChangeReason = model.OriginalTargetDate.ChangeReason ?? "";
				model.OriginalTargetDate.Detail = model.OriginalTargetDate.Detail ?? "";
				return Json(ComUtil.JsonEncodeCamelCase(model));
			}
			catch (Exception ex)
			{
				ErrorSignal.FromCurrentContext().Raise(ex);
				Response.StatusCode = (int)HttpStatusCode.InternalServerError;
				return Json(new HttpStatusCodeResult(HttpStatusCode.InternalServerError, ex.Message));
			}
		}

		[HttpPost]
		public ActionResult SaveProjectDetails(IndexViewModel model)
		{
			try
			{
				var _project = _dbSubmission.trProjects.Where(x => x.Id == model.ProjectId).Single();
				_project.ENProjectLeadId = model.ProjectDetails.General.ProjectLeadId;
				//_project.ENManagerId = model.ProjectDetails.General.ManagerId;
				_project.QAOfficeId = model.ProjectDetails.General.QAOfficeId;
				_project.DevelopmentRepresentativeId = model.ProjectDetails.General.DevelopmentRepresentativeId;
				_project.GoLiveDate = model.ProjectDetails.General.GoLiveDate;
				_project.RequestedDeliveryDate = model.ProjectDetails.General.RequestedDeliveryDate;
				_masterFileService.SaveMasterFileModel(Mapper.Map<MasterFileServiceAlias.MasterFileModel>(model.ProjectDetails.ProjectDefinition), _userContext.User.Id);
				_projectService.UpdateQuotedHours(model.ProjectId, Convert.ToDecimal(model.ProjectDetails.General.QuotedHours));

				// Transfers inherit this information from the Prime, so they should not change.
				if (!model.ProjectDetails.General.IsTransfer)
				{
					_projectService.UpdateProjectComplexityRating(model.ProjectId,
						model.ProjectDetails.General.ProductTypeId,
						model.ProjectDetails.General.ComplexityRatingId);
				}


				if (!_dbSubmission.trProjects.Local.Any(x => x.Id == _project.Id))
				{
					_dbSubmission.trProjects.Attach(_project);
				}
				_incomingReviewService.SaveChanges(_userContext.User.Id);

				_dbSubmission.Entry(_project).State = EntityState.Detached;
				_project = _dbSubmission.trProjects.Where(x => x.Id == model.ProjectId).Single();
				SaveTestingTypes(_project, model);
				SaveJurisdictionSpecificSGIFields(model);
				SetProjectDetailsViewModel(_project, model);
				Response.StatusCode = (int)HttpStatusCode.OK;
				return Json(model);
			}
			catch (Exception ex)
			{
				ErrorSignal.FromCurrentContext().Raise(ex);
				Response.StatusCode = (int)HttpStatusCode.InternalServerError;
				return Json(new HttpStatusCodeResult(HttpStatusCode.InternalServerError, ex.Message));
			}
		}

		public void SaveJurisdictionSpecificSGIFields(IndexViewModel indexViewModel)
		{
			if (indexViewModel.ProjectDetails.General.isJurSpecificSGIFieldsVisible)
			{
				var jurisdictionDataIds = indexViewModel.ProjectDetails.General.JurisdictionSpecificSGIFields
				.Select(x => x.JurisdictionalDataId)
				.ToList();

				var jurisdictionDatas = _dbSubmission.JurisdictionalData
					.Where(x => jurisdictionDataIds.Contains(x.Id))
					.ToList();

				foreach (var jurisdictionData in jurisdictionDatas)
				{
					var jurisdictionSpecificFieldsObj = indexViewModel.ProjectDetails.General.JurisdictionSpecificSGIFields
						.Where(x => x.JurisdictionalDataId == jurisdictionData.Id)
						.Select(x => new
						{
							x.RevisionReasonId,
							MarketCode = x.MarketCode != null ? x.MarketCode.Trim() : null
						})
						.SingleOrDefault();

					jurisdictionData.RevisionReasonId = jurisdictionSpecificFieldsObj.RevisionReasonId;
					jurisdictionData.MarketCode = jurisdictionSpecificFieldsObj.MarketCode;
				}

				_dbSubmission.SaveChanges();
			}
		}

		[HttpPost]
		public ActionResult SaveTestingTypes(trProject project, IndexViewModel model)
		{
			try
			{

				var submissions = _dbSubmission.Submissions
					.Where(x => x.FileNumber == project.FileNumber)
					.ToList();
				foreach (var s in submissions)
				{
					s.TestingTypeId = model.ProjectDetails.ProjectDefinition.TestingTypeId;
				}
				_dbSubmission.SaveChanges();

				Response.StatusCode = (int)HttpStatusCode.OK;
				return Json(model);
			}
			catch (Exception ex)
			{
				ErrorSignal.FromCurrentContext().Raise(ex);
				Response.StatusCode = (int)HttpStatusCode.InternalServerError;
				return Json(new HttpStatusCodeResult(HttpStatusCode.InternalServerError, ex.Message));
			}
		}

		[HttpPost]
		public ActionResult SaveManufacturerSpecific(IndexViewModel model)
		{
			var _project = _dbSubmission.trProjects.Where(x => x.Id == model.ProjectId).Single();
			SaveStudio(model);
			SaveProductLine(model);
			SavePlatforms(model);

			setManufacturerViewModel(_project.Id, model);
			return Json(model);
		}

		[HttpPost]
		public ActionResult SaveStudio(IndexViewModel model)
		{
			try
			{
				_dbSubmission.Submissions
					.Where(x => x.FileNumber == model.FileNumber)
					.ToList()
					.ForEach(x => x.StudioId = model.ManufacturerSpecific.StudioId);
				_dbSubmission.SaveChanges();
				Response.StatusCode = (int)HttpStatusCode.OK;
				return Json(model);
			}
			catch (Exception ex)
			{
				ErrorSignal.FromCurrentContext().Raise(ex);
				Response.StatusCode = (int)HttpStatusCode.InternalServerError;
				return Json(new HttpStatusCodeResult(HttpStatusCode.InternalServerError, ex.Message));
			}
		}

		[HttpPost]
		public ActionResult SaveProductLine(IndexViewModel model)
		{
			try
			{
				var jurisdictionalData =
					_dbSubmission.Submissions
					.Where(x => x.FileNumber == model.FileNumber)
					.SelectMany(x => x.JurisdictionalData)
					.Include(x => x.ProductLineJurisdictionalData)
					.ToList();

				var productLineJurisdictionalData =
					jurisdictionalData
					.SelectMany(y => y.ProductLineJurisdictionalData)
					.ToList();

				if (model.ManufacturerSpecific.ProductLine != null)
				{
					_dbSubmission.tbl_lst_ProductLinesJurisdictionalData.RemoveRange(productLineJurisdictionalData);

					foreach (var jurisdiction in jurisdictionalData)
					{
						jurisdiction.ProductLineJurisdictionalData.Add(
							new tbl_lst_ProductLinesJurisdictionalData
							{
								JurisdictionaldataId = jurisdiction.Id,
								ProductLineId = (Int32)model.ManufacturerSpecific.ProductLineId
							});
					}
					_dbSubmission.SaveChanges();
				}

				Response.StatusCode = (int)HttpStatusCode.OK;
				return Json(model);
			}
			catch (Exception ex)
			{
				ErrorSignal.FromCurrentContext().Raise(ex);
				Response.StatusCode = (int)HttpStatusCode.InternalServerError;
				return Json(new HttpStatusCodeResult(HttpStatusCode.InternalServerError, ex.Message));
			}
		}

		[HttpPost]
		public ActionResult SavePlatforms(IndexViewModel model)
		{
			try
			{
				var keytbls = _dbSubmission.Submissions.Where(x => x.FileNumber == model.OriginalFilenumber).Select(x => x.Id).ToList();
				_dbSubmission.SubmissionPlatforms.Where(x => keytbls.Contains(x.SubmissionId)).Delete();
				if (model.ManufacturerSpecific.PlatformIds.Count == 0)
					return Json(model);
				model.ManufacturerSpecific.PlatformIds.ToList().ForEach(x =>
				{
					keytbls.ForEach(y =>
					{
						_dbSubmission.SubmissionPlatforms.Add(new SubmissionPlatforms { SubmissionId = y, PlatformId = x });
					});
				});
				_dbSubmission.SaveChanges();
				Response.StatusCode = (int)HttpStatusCode.OK;
				return Json(model);
			}
			catch (Exception ex)
			{
				ErrorSignal.FromCurrentContext().Raise(ex);
				Response.StatusCode = (int)HttpStatusCode.InternalServerError;
				return Json(new HttpStatusCodeResult(HttpStatusCode.InternalServerError, ex.Message));
			}
		}
		[HttpPost]
		public ActionResult SaveBilling(IndexViewModel model)
		{
			var _project = _dbSubmission.trProjects.Where(x => x.Id == model.ProjectId).Single();
			SaveProductClassification(model);
			setBillingViewModel(_project.Id, model);
			return Json(model);
		}

		[HttpPost]
		public ActionResult SaveProductClassification(IndexViewModel model)
		{
			try
			{
				if (model.Billing.ProductClassificationId != null)
				{
					var masterFileNumber = model.ProjectDetails.ProjectDefinition.MasterFileNumber;
					if (masterFileNumber != null)
					{
						//Resubmission,Continuation and Pre-Certification
						SettingProductClassificationData(masterFileNumber, model);
					}
					else
					{
						//New
						var fileNumberStr = _dbSubmission.trProjects.Where(x => x.Id == model.ManufacturerSpecific.projectId).Select(x => x.FileNumber).FirstOrDefault();
						SettingProductClassificationData(fileNumberStr, model);
					}
					_dbSubmission.SaveChanges();
				}
				else
				{
					//Delete the current record.
					var masterFile = GetMasterFileProductClassification(model);
					if (masterFile != null)
					{
						_dbSubmission.tbl_lst_ProductClassifications.Where(x => x.Masterfile == masterFile).Delete();
					}
				}
				Response.StatusCode = (int)HttpStatusCode.OK;
				return Json(model);
			}
			catch (Exception ex)
			{
				ErrorSignal.FromCurrentContext().Raise(ex);
				Response.StatusCode = (int)HttpStatusCode.InternalServerError;
				return Json(new HttpStatusCodeResult(HttpStatusCode.InternalServerError, ex.Message));
			}
		}

		[HttpPost]
		public ActionResult SaveTargetDate(IndexViewModel model)
		{
			var targetModel = model.OriginalTargetDate;
			try
			{
				var _project = _dbSubmission.trProjects.Where(x => x.Id == model.ProjectId).Single();

				if (targetModel.Date != null && (Convert.ToDateTime(targetModel.Date) != Convert.ToDateTime(targetModel.InitialDateState)))
				{
					var formatedDate = Convert.ToDateTime(targetModel.Date);
					formatedDate = formatedDate.Date;
					_project.ApplyTargetOffSet = (bool)model.ApplyJurisdictionOffset;
					_dbSubmission.SaveChanges();
					_projectService.UpdateProjectJurisdictionTargetDate(_project.Id, "", _userContext.User.Id, formatedDate, targetModel.Detail ?? string.Empty, targetModel.ChangeReasonId ?? 0, _project.TargetDate == null ? (int)TargetDateReviseMode.EnterOriginalTarget : (int)TargetDateReviseMode.ModifyOriginalTarget);
					_project.IncomingReviewEditedBy = _userContext.User.Id;
					_project.IncomingReviewEditDate = DateTime.Now;
					if (model.IncomingReviewStatus.Complete)
					{
						_project.IncomingReviewCompleteDate = DateTime.Now;
						_project.IncomingReviewCompletedBy = _userContext.User.Id;
					}
					if (!_dbSubmission.trProjects.Local.Any(x => x.Id == _project.Id))
					{
						_dbSubmission.trProjects.Attach(_project);
					}
					_incomingReviewService.SaveChanges(_userContext.User.Id);
					setOriginalTargetDateViewModel(_project, model);
					model.OriginalTargetDate.ChangeReason = "";
					model.OriginalTargetDate.ChangeReasonId = null;
					model.OriginalTargetDate.Detail = "";
				}

				_dbSubmission.Entry(_project).State = EntityState.Detached;
				_project = _dbSubmission.trProjects.Where(x => x.Id == model.ProjectId).Single();

				setOriginalTargetDateViewModel(_project, model);
				Response.StatusCode = (int)HttpStatusCode.OK;
				return Json(model);
			}
			catch (Exception ex)
			{
				ErrorSignal.FromCurrentContext().Raise(ex);
				Response.StatusCode = (int)HttpStatusCode.InternalServerError;
				return Json(new HttpStatusCodeResult(HttpStatusCode.InternalServerError, ex.Message));
			}
		}
		public ActionResult SaveIncomingReviewEditAndComplete(IndexViewModel model)
		{
			try
			{
				var project = _dbSubmission.trProjects.Where(x => x.Id == model.ProjectId).Single();
				var complete = model.IncomingReviewStatus.Complete && model.IncomingReviewStatus.Completed == null && model.IncomingReviewStatus.CompletedBy == null;
				_incomingReviewService.LogIncomingReviewEdit(project, complete, _userContext.User.Id);
				var editedBy = project.IncomingReviewEditedBy != null ? _dbSubmission.trLogins.Single(x => x.Id == project.IncomingReviewEditedBy) : null;
				var completedBy = project.IncomingReviewCompletedBy != null ? _dbSubmission.trLogins.Single(x => x.Id == project.IncomingReviewCompletedBy) : null;

				model.EditedDate = project.IncomingReviewEditDate?.ToString(@"MM\/dd\/yyyy");
				model.IncomingReviewEditedBy = editedBy != null ? editedBy.FName + " " + editedBy.LName : null;
				model.CompletedBy = completedBy != null ? completedBy.Name + " " + completedBy.LName : null;
				model.CompletedDate = project.IncomingReviewCompleteDate?.ToString(@"MM\/dd\/yyyy");

				Response.StatusCode = (int)HttpStatusCode.OK;
				return Json(model);
			}
			catch (Exception ex)
			{
				ErrorSignal.FromCurrentContext().Raise(ex);
				Response.StatusCode = (int)HttpStatusCode.InternalServerError;
				return Json(new HttpStatusCodeResult(HttpStatusCode.InternalServerError, ex.Message));
			}
		}
		private void SetProjectDetailsViewModel(trProject project, IndexViewModel indexViewModel)
		{
			//Skipped: ResubmissionModel
			indexViewModel.ProjectDetails.General = _incomingReviewService.SetProjectDetailGeneralViewModel(project);
			indexViewModel.ProjectDetails.ProjectDefinition = _incomingReviewService.SetProjectDetailProjectDefinition(project);
			indexViewModel.ProjectDetails.WorkOrderNumbers = _incomingReviewService.SetProjectDetailWorkOrderNumber(project);
			indexViewModel.ProjectDetails.billingVisibility = _incomingReviewService.BillingSectionVisibility(project);
		}

		private void setOriginalTargetDateViewModel(trProject project, IndexViewModel vm)
		{
			vm.OriginalTargetDate = _incomingReviewService.SetOriginalTargetDate(project, Web.Util.HasAccess(Web.Permission.OriginalTargetDate, AccessRight.Edit));
		}

		private void setManufacturerViewModel(int projectId, IndexViewModel indexViewModel)
		{
			indexViewModel.ManufacturerSpecific = _incomingReviewService.SetManufacturerSpecific(projectId, indexViewModel.ProjectDetails.ProjectDefinition);

			var studioManufacturerGroups = (from s in _dbSubmission.tbl_lst_Studios
											join fm in _dbSubmission.tbl_lst_fileManuManufacturerGroups on s.ManufacturerGroupId equals fm.ManufacturerGroupId
											select new { Manufacturer_shortID = fm.ManufacturerCode, Id = fm.ManufacturerGroupId }).Distinct().ToList();
			var studioManufIncomingSetting = studioManufacturerGroups.FirstOrDefault(x => x.Manufacturer_shortID == indexViewModel.ManufacturerCode);
			if (studioManufIncomingSetting != null)
			{
				ManufGroupIncomingSettings manufIncomingSetting = _dbSubmission.ManufGroupIncomingSettings
					.Where(x => x.ManufacturerGroupId == studioManufIncomingSetting.Id)
					.FirstOrDefault();
				if (manufIncomingSetting != null)
				{
					studioVisibilityRequired(indexViewModel, manufIncomingSetting.IsVisible, manufIncomingSetting.IsRequired);
				}
				else
				{
					studioVisibilityRequired(indexViewModel, false, false);
				}

			}
			else
			{
				studioVisibilityRequired(indexViewModel, false, false);
			}

			var productlineManufacturerGroups = (from s in _dbSubmission.tbl_lst_ProductLines
												 join fm in _dbSubmission.tbl_lst_fileManuManufacturerGroups on s.ManufacturerGroupId equals fm.ManufacturerGroupId
												 select new { Manufacturer_shortID = fm.ManufacturerCode, Id = fm.ManufacturerGroupId }).Distinct().ToList();

			var productLineManufIncomingSetting = productlineManufacturerGroups.FirstOrDefault(x => x.Manufacturer_shortID == indexViewModel.ManufacturerCode);

			if (productLineManufIncomingSetting != null)
			{
				ManufGroupIncomingSettings manufIncomingSetting = _dbSubmission.ManufGroupIncomingSettings
					.Where(x => x.ManufacturerGroupId == productLineManufIncomingSetting.Id)
					.FirstOrDefault();
				if (manufIncomingSetting != null)
				{
					productlineVisibilityRequired(indexViewModel, manufIncomingSetting.IsVisible, manufIncomingSetting.IsRequired);
				}
				else
				{
					productlineVisibilityRequired(indexViewModel, false, false);
				}

			}
			else
			{
				productlineVisibilityRequired(indexViewModel, false, false);
			}

			//Plaform
			var platformManufGroupIds = _dbSubmission.tbl_lst_Platforms.Select(x => x.ManufacturerGroupId).Distinct().ToList();
			var manufGroupId = _dbSubmission.tbl_lst_fileManuManufacturerGroups
				.Where(x => platformManufGroupIds.Contains(x.ManufacturerGroupId) && x.ManufacturerCode == indexViewModel.ManufacturerCode)
				.Select(x => x.ManufacturerGroupId).Distinct().FirstOrDefault();
			indexViewModel.ManufacturerSpecific.PlatformManufGroupId = manufGroupId;
			if (manufGroupId != 0)
			{
				ManufGroupIncomingSettings manufIncomingSetting = _dbSubmission.ManufGroupIncomingSettings
					.Where(x => x.ManufacturerGroupId == manufGroupId).FirstOrDefault();
				if (manufIncomingSetting != null)
					PlatformVisibilityRequired(indexViewModel, manufIncomingSetting.IsVisible && !new List<string>() { SubmissionType.LO.ToString() }.Any(x => indexViewModel.FileNumber.Contains(x)), manufIncomingSetting.IsRequired && !new List<string>() { SubmissionType.SY.ToString() }.Any(x => indexViewModel.FileNumber.Contains(x)));
			}

		}

		[HttpPost]
		public ActionResult SaveComponentGroups(SaveComponentGroupsPayload model)
		{
			try
			{
				if (model.PreserveAnswer)
				{
					foreach (var compGroupInfo in model.PreserveAnswers)
					{
						_evoService.AddComponentToGroup(compGroupInfo);
					}
				}

				var userOfficeCode = _dbSubmission.trLogins.Where(x => x.Id == _userContext.User.Id).Select(x => x.HomeOffice.Location).FirstOrDefault() ?? "NJ";

				var evoInstanceSetupModel = new EvoInstanceSetup()
				{
					ProjectId = model.ProjectId,
					ProjectName = model.ProjectName,
					UserName = _userContext.User.Username,
					ComponentGroups = model.ComponentGroups
				};

				var evoInstance = _instanceMethod.CreateFullInstance(evoInstanceSetupModel, userOfficeCode);

				//Prepare tasks for staging
				var stageTasks = new List<Core.IncomingReview.INTask>();
				foreach (var componentGroupRelevantQuestions in evoInstance.ComponentGroupRelevantQuestions)
				{
					var newTaskdefs = _dbSubmission.trTaskEvolutionGroups
						.Where(x => componentGroupRelevantQuestions.RelevantUnlockedQuestionIds.Contains(x.EvolutionTCId))
						.Select(x => x.TaskDef)
						.Distinct()
						.ToList();

					foreach (var newTaskDef in newTaskdefs)
					{
						stageTasks.Add(new Core.IncomingReview.INTask
						{
							TaskDefinitionId = newTaskDef.Id,
							ProjectId = model.ProjectId,
							Name = newTaskDef.Code,
							Description = newTaskDef.Code,
							DepartmentId = newTaskDef.DepartmentId,
							ComponentGroupId = componentGroupRelevantQuestions.ComponentGroupId,
							CompGroupDescription = componentGroupRelevantQuestions.ComponentGroupDescription,
							//2 = Active. See eResultsManaged.States for reference
							EvolutionStatus = 2,
							_new = true,
							TaskDef = newTaskDef.Code + " - " + newTaskDef.DynamicsId
						});
					}
				}

				var result = _incomingReviewService.SetEvolutionViewModel(model.ProjectId, model.ProjectName, _evolutionService.GetComponentGroups(model.ProjectId));
				//Add staging tasks
				result.ProtrackTasks = stageTasks;

				Response.StatusCode = (int)HttpStatusCode.OK;

				return Content(ComUtil.JsonEncodeCamelCase(result), "application/json");
			}
			catch (Exception ex)
			{
				ErrorSignal.FromCurrentContext().Raise(ex);
				Response.StatusCode = (int)HttpStatusCode.InternalServerError;
				return Json(ex.Message.ToString());
			}
		}

		[HttpPost]
		public ActionResult NewAvailableComponents(int projectId, List<int> componentsInGroups)
		{
			var result = _incomingReviewService.BuildIndividualComponentList(projectId, componentsInGroups);
			return Content(ComUtil.JsonEncodeCamelCase(result), "application/json");
		}

		[HttpGet]
		public ActionResult GetpotentialTasksForComponentGroup(String testCaseSeed)
		{

			var testCase = JsonConvert.DeserializeObject<TestCaseSeed>(testCaseSeed);

			try
			{
				int projectId;
				if (!int.TryParse(testCase.projectId, out projectId))
				{
					throw new System.Exception("IncomingReviewController (ProjectId) - cannot parse string to int");
				}
				IList<Core.IncomingReview.INTask> evolutionTasks = _incomingReviewService.BuildEvoINTasks(projectId);
				var json = JsonConvert.SerializeObject(evolutionTasks, Formatting.Indented, new JsonSerializerSettings { ContractResolver = new CamelCasePropertyNamesContractResolver() });
				return Content(json, "application/json");
			}
			catch (Exception ex)
			{
				ErrorSignal.FromCurrentContext().Raise(ex);
				Response.StatusCode = (int)HttpStatusCode.InternalServerError;
				return Json(new HttpStatusCodeResult(HttpStatusCode.InternalServerError, ex.Message));
			}
		}

		[HttpGet]
		public ActionResult GetTemplateTasks(string template)
		{
			try
			{
				var templateTasks = new List<Core.IncomingReview.INTask>();
				var templateObj = JsonConvert.DeserializeObject<Template>(template);
				var templateIds = templateObj.templateIds.Select(int.Parse).ToList();
				var tasks = _projectService.GetTaskTemplates(templateIds);

				foreach (var task in tasks)
				{
					var inTask = Mapper.Map<Core.IncomingReview.INTask>(task);
					inTask.ProjectId = templateObj.projectId;
					inTask.Note = templateObj.addReason;
					inTask.TaskDef = task.Name + " - " + task.Definition.DynamicsId;
					inTask.TaskDefinitionId = task.TaskDefinitionId;
					inTask.Id = null;
					inTask._newTemplateTask = true;
					inTask.TaskDefinition.TestCases = null;
					inTask._new = true;
					inTask.Description = task.Description;
					templateTasks.Add(inTask);
				}
				Response.StatusCode = (int)HttpStatusCode.OK;
				var json = JsonConvert.SerializeObject(templateTasks, Formatting.Indented, new JsonSerializerSettings { ContractResolver = new CamelCasePropertyNamesContractResolver() });
				return Content(json, "application/json");
			}
			catch (Exception ex)
			{
				Response.StatusCode = (int)HttpStatusCode.InternalServerError;
				return Json(JsonConvert.SerializeObject(ex));
				throw;
			}
		}

		[HttpPost]
		public ActionResult SaveTasks(EngTask tasks)
		{
			//Deprecate Evo Tasks
			var oldCompGroupids = tasks.EngTasks
				.Where(x => x.ComponentGroupId != null && !x._new)
				.Select(x => (Guid)x.ComponentGroupId)
				.ToList();
			if (oldCompGroupids.Count > 0 && tasks.EngTasks.Any(x => x.ComponentGroupId != null && x._new))
			{
				DeprecateEvoTasks(oldCompGroupids);
			}

			try
			{
				var tasksToCreate = BuildTasksToCreate(tasks);
				var tasksToUpdate = BuildTasksToUpdate(tasks);
				var tasksToDelete = BuildTasksToDelete(tasks);
				var tasksAssigned = new List<trTask>();
				var histories = new List<trHistory>();
				var tasksCreated = new List<int>();

				if (tasksToCreate.Count > 0)
				{
					_projectService.AddTasks(tasksToCreate);
					tasksAssigned.AddRange(tasksToCreate.Where(x => x.StartDate == null && x.AssignDate != null && x.UserId != null).ToList());
					tasksCreated = tasksToCreate.Select(x => x.Id).ToList();

					if (tasks.TaskLockEnforced)
					{
						var creations = _dbSubmission.trTasks
							.Where(x => tasksCreated.Contains(x.Id))
							.Select(x => new
							{
								ProjectId = x.ProjectId,
								Notes = "Task " + x.Name + " added after 96 hours"
							})
							.ToList();

						foreach (var a in creations)
						{
							histories.Add(new trHistory
							{
								ProjectId = a.ProjectId,
								Notes = a.Notes
							});
						}
					}
				}
				if (tasksToDelete.Count > 0)
				{
					_projectService.DeleteTasks(tasksToDelete);
				}
				if (tasksToUpdate.Count > 0)
				{
					_projectService.UpdateTasks(tasksToUpdate);

					if (tasks.TaskLockEnforced)
					{
						var updates = tasksToUpdate
							.Select(x => new
							{
								ProjectId = x.ProjectId,
								Notes = "Task " + x.Name + " updated after 96 hours due to: " + x.Note
							})
							.ToList();

						foreach (var a in updates)
						{
							histories.Add(new trHistory
							{
								ProjectId = a.ProjectId,
								Notes = a.Notes
							});
						}
					}
				}

				if (tasks.TaskLockEnforced)
				{
					_logService.AddProjectHistory(histories);
					_dbSubmission.SaveChanges();
				}

				if (tasksAssigned.Count > 0)
				{
					_projectService.SendEmailForAssignedTasks(tasksAssigned, tasksAssigned.First().ProjectId);
				}

				var mergeTasks = new List<Core.IncomingReview.INTask>();
				Response.StatusCode = (int)HttpStatusCode.OK;
				IList<Core.IncomingReview.INTask> protrackTasks = _incomingReviewService.BuildEvoINTasks(tasks.ProjectId);
				mergeTasks.AddRange(BuildDeactivatedTasks(tasks.ProjectId));
				mergeTasks.AddRange(protrackTasks);
				var json = JsonConvert.SerializeObject(mergeTasks, Formatting.Indented, new JsonSerializerSettings { ContractResolver = new CamelCasePropertyNamesContractResolver() });
				return Content(json, "application/json");
			}
			catch (Exception ex)
			{
				ErrorSignal.FromCurrentContext().Raise(ex);
				Response.StatusCode = (int)HttpStatusCode.InternalServerError;
				return Json(new HttpStatusCodeResult(HttpStatusCode.InternalServerError, ex.Message));
			}
		}

		public IList<trTask> BuildTasksToCreate(EngTask tasks)
		{
			var taskList = new List<trTask>();
			var tasksToAdd = tasks.EngTasks.Where(x => !x._destroy && (x.Id == null || x.Id == 0) && x.TaskDefinitionId != null).ToList();
			foreach (var task in tasksToAdd)
			{
				var taskJurisdictions = new List<trTransferTaskJur>();
				foreach (var jurisdiction in task.AssociatedJurisdictions)
				{
					taskJurisdictions.Add(new trTransferTaskJur
					{
						JurisdictionId = jurisdiction,
						jurisNumber = int.Parse(jurisdiction)
					});
				}

				taskList.Add(new trTask
				{
					DepartmentId = task.DepartmentId,
					Description = task.Description,
					EstimateCompleteDate = task.EstimatedCompleteDate,
					EstimateHours = task.EstimatedHours,
					EstimateInterval = task.EstimatedInterval,
					Name = task.Name,
					Note = task.Note,
					ProjectId = task.ProjectId,
					TargetDate = task.TargetDate == null ? (DateTime?)null : (Convert.ToDateTime(task.TargetDate)).Date,
					TaskDefinitionId = task.TaskDefinitionId,
					UserId = task.AssignedUserId,
					TaskJurisdictions = taskJurisdictions,
					ETAQueue = task.IsETATask,
					ComponentGroupId = task.ComponentGroupId,
					EvolutionStatus = task.EvolutionStatus
				});
			}
			return taskList;
		}

		public IList<trTask> BuildTasksToUpdate(EngTask tasks)
		{
			List<Core.IncomingReview.INTask> engTasks = tasks.EngTasks;
			//Instead of updating every task see which one is dirty and set _dirty flag
			var taskIds = engTasks.Where(x => x._dirty && x.Id != null && x.Id != 0 && x.TaskDefinitionId != null)
				.Select(x => x.Id).ToList();
			var origTasksInfo = new List<trTask>();
			var dbTasks = _dbSubmission.trTasks.Where(x => taskIds.Contains(x.Id)).OrderBy(x => x.Id).ToList();
			var query = dbTasks
				.Join(engTasks,
				o => o.Id,
				u => u.Id,
				(o, u) => new { Id = o.Id, oUserId = o.UserId, uUserId = u.AssignedUserId })
			.Where(x => x.oUserId != x.uUserId && x.uUserId != null);
			var taskAssignmentChangedIds = query.Select(x => x.Id).ToList();

			foreach (var item in dbTasks)
			{
				origTasksInfo.Add(new trTask() { Id = item.Id, UserId = item.UserId });

				var eTasks = engTasks.Where(x => x.Id == item.Id).Single();
				item.DepartmentId = eTasks.DepartmentId;
				item.Description = eTasks.Description;
				item.EstimateCompleteDate = eTasks.EstimatedCompleteDate;
				item.EstimateHours = eTasks.EstimatedHours;
				item.EstimateInterval = eTasks.EstimatedInterval;
				item.Name = eTasks.Name;
				item.Note = eTasks.Note;
				item.OriginalEstimateHours = eTasks.OriginalEstimatedHours;
				item.ProjectId = eTasks.ProjectId;
				item.TargetDate = eTasks.TargetDate == null ? (DateTime?)null : (Convert.ToDateTime(eTasks.TargetDate)).Date;
				item.TaskDefinitionId = eTasks.TaskDefinitionId;
				item.ETAQueue = eTasks.IsETATask;
				item.ComponentGroupId = eTasks.ComponentGroupId;

				_dbSubmission.trTransferTaskJurs.Where(x => x.re_taskId == item.Id).Delete();
				if (eTasks.AssociatedJurisdictions.Count > 0)
				{
					item.TaskJurisdictions = eTasks.AssociatedJurisdictions.Select(x => new trTransferTaskJur { JurisdictionId = x, jurisNumber = Int32.Parse(x), re_taskId = item.Id }).ToList();
				}

				if (item.UserId == null)
				{
					// Unassigned -> Assigned : Set assign date.
					if (eTasks.AssignedUserId != null)
					{
						item.AssignDate = DateTime.Now;
						item.UserId = eTasks.AssignedUserId;
					}
				}
				else
				{
					// Assigned -> Unassigned : clear assign date
					if (eTasks.AssignedUserId == null)
					{
						item.AssignDate = null;
						item.UserId = null;
					}
					// Assigned -> Assigned : Update assign date to now
					else if (eTasks.AssignedUserId != item.UserId)
					{
						item.AssignDate = DateTime.Now;
						item.UserId = eTasks.AssignedUserId;
					}
				}
			}

			if (taskAssignmentChangedIds.Count > 0)
			{
				var tasksAssigned = dbTasks.Where(x => taskAssignmentChangedIds.Contains(x.Id)).ToList();
				if (tasksAssigned.Count > 0)
				{
					_projectService.SendEmailForReAssignedTasks(tasksAssigned, origTasksInfo, dbTasks.First().ProjectId);
					_projectService.SendEmailForAssignedTasks(tasksAssigned, dbTasks.First().ProjectId);
				}
			}
			return dbTasks;
		}

		private List<int> BuildTasksToDelete(EngTask tasks)
		{
			List<Core.IncomingReview.INTask> engTasks = tasks.EngTasks;
			//TODO ability to delete 
			return engTasks.Where(x => x._destroy && x.Id != null).Select(x => (int)x.Id).ToList();
		}

		private void setBillingViewModel(int projectId, IndexViewModel indexViewModel)
		{
			indexViewModel.Billing = _incomingReviewService.SetBilling(projectId, indexViewModel.ProjectDetails.ProjectDefinition);
			indexViewModel.Billing.IsRequired = ProductClassification(indexViewModel);
		}

		private void setEvolutionViewModel(trProject project, IndexViewModel indexViewModel)
		{
			indexViewModel.EvolutionViewModel = _incomingReviewService.SetEvolutionViewModel(project.Id, project.FileNumber, _evolutionService.GetComponentGroups(project.Id));

			var hasTemplate = _dbSubmission.trTaskTemplates
				.Where(x => x.UserId == _userContext.User.Id)
				.Any();
			if (hasTemplate)
			{
				indexViewModel.EvolutionViewModel.templatePicker.users.Add(
					new SelectListItem
					{
						Value = _userContext.User.Id.ToString(),
						Text = _userContext.User.FName + " " + _userContext.User.LName,
						Selected = true
					});
			}
			indexViewModel.EvolutionViewModel.ProtrackTasks.AddRange(BuildDeactivatedTasks(project.Id));
			indexViewModel.EvolutionViewModel.ProtrackTasks.AddRange(_incomingReviewService.BuildEvoINTasks(project.Id));
		}

		[HttpPost]
		public ActionResult LoadManufacturerViewModel(int projectId, IndexViewModel indexViewModel)
		{

			var result = _incomingReviewService.SetManufacturerSpecific(projectId, indexViewModel.ProjectDetails.ProjectDefinition);
			var json = JsonConvert.SerializeObject(result, Formatting.Indented, new JsonSerializerSettings { ContractResolver = new CamelCasePropertyNamesContractResolver() });
			return Content(json, "application/json");
		}

		[HttpPost]
		public ActionResult LoadBillingViewModel(int projectId, IndexViewModel indexViewModel)
		{

			var result = _incomingReviewService.SetBilling(projectId, indexViewModel.ProjectDetails.ProjectDefinition); ;
			var json = JsonConvert.SerializeObject(result, Formatting.Indented, new JsonSerializerSettings { ContractResolver = new CamelCasePropertyNamesContractResolver() });
			return Content(json, "application/json");
		}

		[HttpPost]
		public ActionResult SaveSubProject(string subProject, int projectId)
		{
			_subProjectService.SaveSubProject(subProject);
			var result = _subProjectService.SubProjects(projectId);
			var json = JsonConvert.SerializeObject(result, Formatting.Indented, new JsonSerializerSettings { ContractResolver = new CamelCasePropertyNamesContractResolver() });
			return Content(json, "application/json");
		}

		[HttpPost]
		public ActionResult DeleteSubProject(int id, int projectId)
		{
			var hasTask = _dbSubmission.trTasks.Any(x => x.SubProjectId == id);
			if (!hasTask)
				_subProjectService.DeleteSubProject(id);
			var result = _subProjectService.SubProjects(projectId);
			var json = JsonConvert.SerializeObject(new { result = result, hasTask = hasTask }, Formatting.Indented, new JsonSerializerSettings { ContractResolver = new CamelCasePropertyNamesContractResolver() });
			return Content(json, "application/json");
		}

		private void SetUKViewModel(int projectId, IndexViewModel indexViewModel)
		{
			indexViewModel.UK.SubProjects = _subProjectService.SubProjects(projectId);
			indexViewModel.UK.SubProject.ProjectId = projectId;
		}

		[HttpGet]
		public ActionResult GetSubProject(int id)
		{
			var result = _subProjectService.SubProject(id);
			var json = JsonConvert.SerializeObject(result, Formatting.Indented, new JsonSerializerSettings { ContractResolver = new CamelCasePropertyNamesContractResolver() });
			return Content(json, "application/json");
		}

		[HttpPost]
		public ActionResult LockUnlockComponentGroup(Guid componentGroupId, Guid answerId, Guid versionId, bool lockUnlock)
		{
			var query = _dbeResultsManaged.EventType.AsQueryable();
			var unlockId = 0;
			var lockId = 0;
			if (lockUnlock)
			{
				var lockDescription = EvolutionEventType.Lock.ToString();
				lockId = query.Where(x => x.Description == lockDescription)
					.Select(x => x.Id)
					.First();
			}
			else
			{
				var unlockDescription = EvolutionEventType.Unlock.ToString();
				unlockId = query.Where(x => x.Description == unlockDescription)
					.Select(x => x.Id)
					.First();
			}

			var eventType = new Event()
			{
				Id = Guid.NewGuid(),
				Type = lockUnlock ? lockId : unlockId,
				AnswerID = answerId,
				VersionID = versionId,
				CreatedOn = DateTime.UtcNow
			};

			_dbeResultsManaged.Event.Add(eventType);
			_dbeResultsManaged.SaveChanges();

			var json = JsonConvert.SerializeObject(eventType, Formatting.Indented, new JsonSerializerSettings { ContractResolver = new CamelCasePropertyNamesContractResolver() });
			return Content(json, "application/json");
		}

		[HttpPost]
		public void DeleteComponentGroup(Guid componentGroupId)
		{
			//Evo
			EvoLogic.SaveData.InstanceCompGroupMethods instanceCompGroupMethods = new EvoLogic.SaveData.InstanceCompGroupMethods(_dbeResultsManaged, null, null, null, null, null);
			_dbeResultsManaged.InstanceCompGroup.AddRange(instanceCompGroupMethods.DeprecateComponentGroup(componentGroupId, _userContext.User.Username));
			_dbeResultsManaged.SaveChanges();

			//Task
			DeprecateEvoTasks(new List<Guid> { componentGroupId });
		}

		public void DeprecateEvoTasks(List<Guid> compgroupIds)
		{
			var tasksToDeprecate = _dbSubmission.trTasks
				.Where(x => compgroupIds.Contains((Guid)x.ComponentGroupId))
				.ToList();

			foreach (var task in tasksToDeprecate)
			{
				task.EvolutionStatus = 3;
			}

			if (tasksToDeprecate.Count > 0)
			{
				_dbSubmission.SaveChanges();
			}
		}

		#endregion

		#region Read
		[HttpPost]
		public ActionResult GetMasterFileRecords(MasterFileServiceAlias.Filter payload)
		{
			var validator = _masterFileService.GetMasterFileValidator(payload.MasterFilter.CurrentFileNumber, payload.MasterFilter.MasterFileNumber);

			if (validator.IsValid())
			{
				var result = Mapper.Map<MasterFileRecordSetModel>(_masterFileService.GetMasterFileRecords(Mapper.Map<MasterFileServiceAlias.Filter>(payload)));

				var records = _dbSubmission.Submissions.Where(x => x.FileNumber == payload.MasterFilter.MasterFileNumber).ToList();
				var studiosCount = records.Select(x => x.StudioId).Distinct().Count();
				if (studiosCount == 1)
				{
					if (records.Any(x => x.StudioId != null))
					{
						result.Studio = records.First().Studio.Name;
						result.StudioId = records.First().StudioId;
					}
					else
					{
						result.Studio = "";
					}
					result.HasADistinctStudio = true;
				}
				else
				{
					result.Studio = "";
					result.HasADistinctStudio = false;
				}
				Response.StatusCode = (int)HttpStatusCode.OK;
				return Json(ComUtil.JsonEncodeCamelCase(result));
			}
			else if (validator.InvalidManufacturer)
			{
				Response.StatusCode = (int)HttpStatusCode.BadRequest;
				return Json(new { InvalidManufacturer = "Incompatible Manufacturer pair." });
			}
			else if (validator.InvalidHead)
			{
				Response.StatusCode = (int)HttpStatusCode.BadRequest;
				return Json(new { InvalidHead = "A file can not be a master file for another chain.  " + payload.MasterFilter.CurrentFileNumber + " is the master file of " + validator.CurrentHead + "." });
			}
			else if (validator.InvalidMultiplicity)
			{
				Response.StatusCode = (int)HttpStatusCode.BadRequest;
				return Json(new { validator.ConcurrentMasterFile });
			}
			else if (validator.InvalidSelfReference)
			{
				Response.StatusCode = (int)HttpStatusCode.BadRequest;
				return Json(new { InvalidSelfReference = payload.MasterFilter.CurrentFileNumber });
			}
			else
			{
				Response.StatusCode = (int)HttpStatusCode.InternalServerError;
				return Json("Internal Server Error");
			}
		}

		[HttpPost]
		public ActionResult GetJurisdictionsForComponentGroup(ComponentGroupFilter payload)
		{
			try
			{
				var jurisdictions = _dbSubmission.JurisdictionalData
					.Where(x => payload.SubmissionsIds.Contains(x.Submission.Id))
					.Select(x => new Core.IncomingReview.JurisdictionViewModel
					{
						Id = x.Jurisdiction.Id,
						Name = x.Jurisdiction.Name,
						Selected = true,
						Documents = x.Jurisdiction.EvoDocuments
							.Where(y => !y.DocumentInactive && y.JoinState && y.JurisdictionId != 0)
							.Select(y => new Core.IncomingReview.DocumentViewModel
							{
								Id = y.DocumentId,
								Name = y.DocumenetName,
								Version = y.DocumentVersion
							})
							.ToList()
					})
					.AsEnumerable()
					.GroupBy(y => y.Id)
					.Select(grp => grp.FirstOrDefault())
					.ToList();

				var requiredJuridictions = _dbSubmission.tbl_lst_fileJuris
					.Where(x => payload.RequiredJuridictionIds.Contains(x.FixId))
					.Select(x => new Core.IncomingReview.JurisdictionViewModel
					{
						Id = x.Id,
						Name = x.Name,
						Selected = true,
						Documents = _dbSubmission.vw_EM_DocumentJurisdiction
							.Where(y => !y.DocumentInactive && y.JoinState && y.JurisdictionId == x.FixId)
							.Select(y => new Core.IncomingReview.DocumentViewModel
							{
								Id = y.DocumentId,
								Name = y.DocumenetName,
								Version = y.DocumentVersion
							})
							.ToList()
					})
					.AsEnumerable()
					.GroupBy(y => y.Id)
					.Select(grp => grp.FirstOrDefault())
					.ToList();

				jurisdictions.AddRange(requiredJuridictions);
				jurisdictions.OrderBy(x => x.Id);

				Response.StatusCode = (int)HttpStatusCode.OK;
				return Content(ComUtil.JsonEncodeCamelCase(jurisdictions), "application/json");
			}
			catch (Exception ex)
			{
				ErrorSignal.FromCurrentContext().Raise(ex);
				Response.StatusCode = (int)HttpStatusCode.InternalServerError;
				return Json(new HttpStatusCodeResult(HttpStatusCode.InternalServerError, ex.Message));
			}
		}

		[HttpGet]
		public ActionResult GetExtraneousJurisdictionById(int id)
		{
			try
			{
				var result = _dbSubmission.tbl_lst_fileJuris.Where(x => x.FixId == id).Select(x => new Core.IncomingReview.JurisdictionViewModel
				{
					Id = x.Id,
					Name = x.Name,
					Selected = true,
					IsExtraneous = true,
					Documents = x.EvoDocuments
						.Where(y => !y.DocumentInactive && y.JoinState)
						.Select(y => new Core.IncomingReview.DocumentViewModel
						{
							Id = y.DocumentId,
							Name = y.DocumenetName,
							Version = y.DocumentVersion
						})
						.ToList()
				})
				.Single();

				Response.StatusCode = (int)HttpStatusCode.OK;
				return Content(ComUtil.JsonEncodeCamelCase(result), "application/json");
			}
			catch (Exception ex)
			{
				ErrorSignal.FromCurrentContext().Raise(ex);
				Response.StatusCode = (int)HttpStatusCode.InternalServerError;
				return Json(new HttpStatusCodeResult(HttpStatusCode.InternalServerError, ex.Message), JsonRequestBehavior.AllowGet);
			}
		}

		[HttpGet]
		public ActionResult LoadComponentGroups(string filenumber)
		{
			if (!string.IsNullOrEmpty(filenumber.Trim()))
			{
				var query = _dbeResultsManaged.InstanceCompGroup
					.Include(x => x.FuncAndDocs)
					.AsQueryable();

				var isLegacyTransfer = filenumber.Contains("transfer", StringComparison.OrdinalIgnoreCase);
				if (isLegacyTransfer)
				{
					query = query.Where(x => x.Instance.LegacyName == filenumber);
				}
				else if (IsTransfer(filenumber))
				{
					query = query.Where(x => x.Instance.TransferName == filenumber);
				}
				else
				{
					query = query.Where(x => x.Instance.FileNumber == filenumber);
				}

				var instanceComponentGroups = query
					.Select(x => new { x.CompGroupID, x.Description, x.Component.IdNumber, x.State, x.FuncAndDocs })
					.ToList();
				var inactiveComponentGroupIds = instanceComponentGroups
					.Where(x => x.State == (int)EvolutionComponentState.Inactive)
					.Select(x => x.CompGroupID)
					.Distinct()
					.ToList();

				var activeComponentGroups = instanceComponentGroups
					.Where(x => !inactiveComponentGroupIds.Contains(x.CompGroupID))
					.GroupBy(x => x.CompGroupID)
					.Select(x => new
					{
						CompGroupDescription = x.Select(y => y.Description).FirstOrDefault(),
						Components = x.Select(y => y.IdNumber).ToList(),
						FunctionalRoleIds = x.Select(y => y.FuncAndDocs.FunctionalRoleIds.ToList()).FirstOrDefault(),
						FunctionalRoles = new List<Core.IncomingReview.FunctionalRoleViewModel>()
					}).ToList();


				var activeFunctionalRoles = _dbeResultsManaged.FunctionalRole
					.Where(x => !x.Inactive)
					.Select(x => new Core.IncomingReview.FunctionalRoleViewModel
					{
						Id = x.Id,
						Code = x.Name,
						CategoryId = x.CategoryId
					})
					.ToList();

				foreach (var componentGroup in activeComponentGroups)
				{
					var selectedFunctionalRoles = activeFunctionalRoles
						.Where(x => componentGroup.FunctionalRoleIds.Contains(x.Id))
						.Select(x => new Core.IncomingReview.FunctionalRoleViewModel()
						{
							Id = x.Id,
							Code = x.Code,
							CategoryId = x.CategoryId,
							Selected = true
						}).ToList();
					componentGroup.FunctionalRoles.AddRange(selectedFunctionalRoles);
				}

				return Content(ComUtil.JsonEncodeCamelCase(activeComponentGroups), "application/json");
			}
			return null;
		}

		#endregion

		#region Helper Functions
		private void SettingProductClassificationData(string masterFileNumber, IndexViewModel model)
		{
			//Resubmission,Continuation and Pre-Certification
			var productClassificationsEdit = _dbSubmission.tbl_lst_ProductClassifications.Where(x => x.Masterfile == masterFileNumber).SingleOrDefault();
			if (productClassificationsEdit == null)
			{
				var productClassifications = new tbl_lst_ProductClassifications
				{
					Masterfile = masterFileNumber,
					CreatedOn = DateTime.Now,
					CreatedBy = _userContext.User.Id,
					ClassificationId = model.Billing.ProductClassificationId
				};
				_dbSubmission.tbl_lst_ProductClassifications.Add(productClassifications);
			}
			else
			{
				//Edit Previous record
				var productClassifications = new tbl_lst_ProductClassifications();
				productClassifications = _dbSubmission.tbl_lst_ProductClassifications.Find(productClassificationsEdit.Id);
				productClassifications.CreatedOn = DateTime.Now;
				productClassifications.CreatedBy = _userContext.User.Id;
				productClassifications.ClassificationId = model.Billing.ProductClassificationId;
			}
		}

		public string GetMasterFileProductClassification(IndexViewModel model)
		{
			var result = model.ProjectDetails.ProjectDefinition.MasterFileNumber;
			if (result != null)
			{
				var record = _dbSubmission.tbl_lst_ProductClassifications.Where(x => x.Masterfile == result).FirstOrDefault();
				result = record != null ? record.Masterfile : null;
			}

			if (result == null)
			{
				result = _dbSubmission.trProjects.Where(x => x.Id == model.ProjectId).Select(x => x.FileNumber).FirstOrDefault();
				var record = _dbSubmission.tbl_lst_ProductClassifications.Where(x => x.Masterfile == result).FirstOrDefault();
				result = record != null ? record.Masterfile : null;
			}
			return result;
		}

		public void studioVisibilityRequired(IndexViewModel indexViewModel, bool isVisible, bool isRequired)
		{
			indexViewModel.ManufacturerSpecific.manufStudioVisibility = isVisible;
			indexViewModel.ManufacturerSpecific.manufStudioRequired = isRequired;
		}

		public void productlineVisibilityRequired(IndexViewModel indexViewModel, bool isVisible, bool isRequired)
		{
			indexViewModel.ManufacturerSpecific.manufProductLineVisibility = isVisible;
			indexViewModel.ManufacturerSpecific.manufProductLineRequired = isRequired;
		}

		private List<Submissions.Core.IncomingReview.INTask> BuildDeactivatedTasks(int projectId)
		{
			var taskDeletion = _dbSubmission.trTaskDeletions
				.Include("User")
				.Where(x => x.ProjectId == projectId)
				.OrderBy(x => x.Description)
				.ToList();
			List<Core.IncomingReview.INTask> deactivatedTasks = new List<Core.IncomingReview.INTask>();
			foreach (var task in taskDeletion)
			{
				deactivatedTasks.Add(new Core.IncomingReview.INTask
				{
					Id = task.Id,
					AssignDate = task.AssignDate,
					AssignedUserId = task.UserId,
					AssignedUserName = task.User == null ? "" : task.User.FName + " " + task.User.LName,
					Description = task.Description,
					EstimatedHours = task.EstimateHours,
					TargetDate = task.TargetDate,
					StartDate = task.StartDate,
					CompleteDate = task.CompleteDate,
					Note = task.Note,
					TaskDef = task.Name,
					_isDeactivatedTask = true,
					TaskDefinitionId = task.TaskDefinitionId
				});
			}
			return _incomingReviewService.TestCasesForTasks(deactivatedTasks);
		}

		public void PlatformVisibilityRequired(IndexViewModel indexViewModel, bool isVisible, bool isRequired)
		{
			indexViewModel.ManufacturerSpecific.PlatformVisibility = isVisible;
			indexViewModel.ManufacturerSpecific.PlatformRequired = isVisible ? isRequired : false;
		}

		public bool ProductClassification(IndexViewModel model)
		{
			var isMO = _dbSubmission.Submissions.Where(x => x.FileNumber == model.FileNumber).Select(x => x.Type).FirstOrDefault() == SubmissionType.MO.ToString() ? true : false;
			var manuf_code = new FileNumber(model.FileNumber).ManufacturerCode;
			var manufs = new List<string>
			{
				CustomManufacturer.WMS.ToString(),
				CustomManufacturer.BAL.ToString(),
				CustomManufacturer.BLY.ToString(),
				CustomManufacturer.SHU.ToString()
			};
			return manufs.Contains(manuf_code) && isMO ? true : false;
		}

		public void EvoLogic(trProject project, IndexViewModel vm)
		{
			var allProjects = new List<int>() { project.Id };
			allProjects.AddRange(_projectService.ProjectMerged(project.Id).Select(x => x.Id).ToList());

			var JurisdictionComponentIds = _dbSubmission.trProjectJurisdictionalData
				.Where(x => allProjects.Contains(x.ProjectId))
				.Select(x => new { x.JurisdictionalData.Submission.Id, x.JurisdictionalData.Jurisdiction.FixId, x.JurisdictionalData.JurisdictionId })
				.ToList();

			var jurisdictionIds = JurisdictionComponentIds
				.Select(x => x.FixId)
				.Distinct()
				.ToList().ConvertAll(x => (int)x);
			var componentIds = JurisdictionComponentIds
				.Select(x => x.Id)
				.Distinct()
				.ToList();
			var filenumber = _evoService.GetEvolutionInstancesFromComponentsAndJurisdictions(componentIds, jurisdictionIds)
				.Where(x => x == project.ProjectName)
				.FirstOrDefault();

			var isTransfer = false;
			var createdByIncoming = false;

			if (filenumber != null)
			{
				isTransfer = IsTransfer(filenumber);

				var query = _dbeResultsManaged.Instance.AsQueryable();
				if (isTransfer)
				{
					query = query.Where(x => x.TransferName != null && x.TransferName == filenumber);
				}
				else
				{
					query = query.Where(x => x.FileNumber != null && x.FileNumber == filenumber);
				}

				var projectId = query.Select(x => x.ProjectId).FirstOrDefault();
				if (projectId != null)
				{
					createdByIncoming = projectId == project.Id;
				}
			}
			else
			{
				createdByIncoming = true;
			}

			if (createdByIncoming)
			{
				setEvolutionViewModel(project, vm);
			}

			//Purpose: Filter Components by Jur
			var groupedComponents = JurisdictionComponentIds
				.GroupBy(x => x.Id)
				.Select(x => new { x.Key, jurIds = x.Select(y => y.JurisdictionId).ToList() })
				.ToList();
			foreach (var compGroup in vm.EvolutionViewModel.ComponentGroups)
			{
				foreach (var component in compGroup.Components)
				{
					var juriIds = groupedComponents
						.Where(x => x.Key == component.Id)
						.Select(x => x.jurIds)
						.FirstOrDefault();
					if (jurisdictionIds.Count > 0)
					{
						component.JurisdictionIds = juriIds;
					}
				}
			}
		}

		public bool IsTransfer(string filenumber)
		{
			return new FileNumber(filenumber).TransferCode.Length > 0 ? true : false;
		}
		#endregion
	}
}