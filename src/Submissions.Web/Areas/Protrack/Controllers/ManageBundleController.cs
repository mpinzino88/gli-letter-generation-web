﻿using GLI.EFCore.Submission;
using Submissions.Common;
using Submissions.Core;
using Submissions.Web.Areas.Protrack.Models.Project;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Web.Mvc;

namespace Submissions.Web.Areas.Protrack.Controllers
{
	using Submissions.Common.Email;
	public class ManageBundleController : Controller
	{
		private readonly SubmissionContext _dbSubmission;
		private readonly IUserContext _userContext;
		private readonly IProjectService _projectService;
		private readonly IEmailService _emailService;
		private readonly INoteService _noteService;

		public ManageBundleController(SubmissionContext dbSubmission, IUserContext userContext, IProjectService projectService, IEmailService emailService, INoteService noteService)
		{
			_dbSubmission = dbSubmission;
			_emailService = emailService;
			_noteService = noteService;
			_projectService = projectService;
			_userContext = userContext;
		}

		// GET: /Protrack/ManageBundle/
		public ActionResult Index(int projectId)
		{
			var vm = new ManageBundleVM();
			vm.EngSupervisorId = 0;
			vm.EngSupervisor = "";
			vm.QAReviewer = "";
			vm.QAReviewerId = 0;
			vm.QASupervisorId = 0;
			vm.QASupervisor = "";
			vm.ProjectId = projectId;

			var projectInfo = _dbSubmission.trProjects.Where(x => x.Id == projectId).Select(x => new { Name = x.ProjectName, x.IsTransfer, x.TargetDate, x.OrgTargetDate, Status = x.ENStatus, x.MergedWith, x.BundleTypeId, x.OnHold, x.FileNumber, JurId = x.Suffix }).SingleOrDefault();
			var bundleInfo = _dbSubmission.QABundles.Where(x => x.ProjectId == projectId).Select(x => new { Expedite = x.IsRush, ExpediteReason = x.RushReason, ExpediteNote = x.RushNotes, x.QAReviewerId, QAReviewer = (x.QAReviewerId != null) ? x.QAReviewer.FName + " " + x.QAReviewer.LName : "", x.QASupervisorId, QASupervisor = (x.QASupervisorId != null) ? x.QASupervisor.FName + " " + x.QASupervisor.LName : "", CreatedDate = x.AddDate, CreatedBy = x.AddUserId, skipEng = x.SkipEN, EngNotes = x.ENNotes, ENSupervisor = x.NotifyENSupervisor, x.ENEmailSent, CPReqd = x.CPFlag, Status = x.QAStatus, CertNotNeeded = x.NoCertNeeded }).SingleOrDefault();

			vm.TargetDate = projectInfo.TargetDate;
			vm.OrgTargetDate = projectInfo.OrgTargetDate;
			vm.AllowTargetSave = (projectInfo.OnHold != 1 && projectInfo.OrgTargetDate == null) ? true : false;
			vm.Name = projectInfo.Name;
			vm.BundleType = (BundleType)projectInfo.BundleTypeId;
			vm.IsTransfer = (projectInfo.IsTransfer == true) ? true : false;
			vm.ENStatus = projectInfo.Status;
			vm.QAStatus = bundleInfo.Status;
			vm.QAReviewerId = (bundleInfo.QAReviewerId > 0) ? bundleInfo.QAReviewerId : 0;
			vm.QAReviewer = (bundleInfo.QAReviewer != null) ? bundleInfo.QAReviewer : "";
			vm.QASupervisorId = (bundleInfo.QASupervisorId > 0) ? bundleInfo.QASupervisorId : 0;
			vm.QASupervisor = (bundleInfo.QASupervisor != null) ? bundleInfo.QASupervisor : "";
			vm.IsExpedite = (bundleInfo.Expedite == 1) ? true : false;
			vm.ExpediteReason = bundleInfo.ExpediteReason;
			vm.CPReqd = (bundleInfo.CPReqd == true) ? true : false;
			vm.SkipEngineering = (bundleInfo.skipEng == true) ? true : false;
			vm.CertNotNeeded = (bundleInfo.CertNotNeeded == true) ? true : false;
			vm.CreateDate = bundleInfo.CreatedDate;
			if (bundleInfo.CreatedBy != null)
			{
				vm.CreateBy = _dbSubmission.trLogins.Where(x => x.Id == bundleInfo.CreatedBy).Select(x => x.FName + " " + x.LName).SingleOrDefault();
			}

			if (vm.BundleType == BundleType.OriginalTesting)
			{
				if (bundleInfo.ENSupervisor != null)
				{
					vm.EngSupervisorId = (int)bundleInfo.ENSupervisor;
					vm.EngSupervisor = _dbSubmission.trLogins.Where(x => x.Id == bundleInfo.ENSupervisor).Select(x => x.FName + " " + x.LName).SingleOrDefault();
				}
				else
				{
					//get manager who worked on it for original file
					var orgProject = (from p in _dbSubmission.trProjects
									  where p.BundleTypeId == (int)BundleType.OriginalTesting
										   && p.FileNumber == projectInfo.FileNumber
									  select p.Id
								 ).FirstOrDefault();

					var orjProjectManager = _dbSubmission.trProjects.Where(x => x.Id == orgProject).Select(x => new { Id = x.ENManagerId, Name = x.ENManager.FName + " " + x.ENManager.LName }).SingleOrDefault();
					vm.EngSupervisorId = orjProjectManager.Id;
					vm.EngSupervisor = orjProjectManager.Name;
				}
			}
			else
			{
				if (bundleInfo.ENSupervisor == null)
				{
					var jurs = (from tp in _dbSubmission.trProjectJurisdictionalData
								join p in _dbSubmission.trProjects on tp.ProjectId equals p.Id
								where p.Id == projectId
								select tp.JurisdictionalDataId).ToList();

					//get manager who worked on it for original Cert
					var orgProject = (from tp in _dbSubmission.trProjectJurisdictionalData
									  join p in _dbSubmission.trProjects on tp.ProjectId equals p.Id
									  where p.BundleTypeId == (int)BundleType.OriginalTesting
										   && jurs.Contains(tp.JurisdictionalDataId)
									  select p.Id
								 ).FirstOrDefault();

					var orjProjectManager = _dbSubmission.trProjects.Where(x => x.Id == orgProject).Select(x => new { Id = x.ENManagerId, Name = x.ENManager.FName + " " + x.ENManager.LName }).SingleOrDefault();
					if (orjProjectManager != null)
					{
						vm.EngSupervisorId = orjProjectManager.Id;
						vm.EngSupervisor = orjProjectManager.Name;
					}
				}
				else
				{
					vm.EngSupervisorId = (int)bundleInfo.ENSupervisor;
					vm.EngSupervisor = _dbSubmission.trLogins.Where(x => x.Id == bundleInfo.ENSupervisor).Select(x => x.FName + " " + x.LName).SingleOrDefault();
				}
			}
			vm.ENEmailSent = bundleInfo.ENEmailSent;
			vm.EngNotes = bundleInfo.EngNotes;
			vm.MergedWith = projectInfo.MergedWith;
			if (projectInfo.MergedWith != null)
			{
				var mergeId = _dbSubmission.trProjects.Where(x => x.ProjectName == projectInfo.MergedWith).Select(x => x.Id).SingleOrDefault();
				if (mergeId != 0)
				{
					vm.MergedWithId = mergeId;
				}
			}

			if (vm.IsTransfer != true) // New Submission Project
			{

				var jurisdictionsNotInProject = (from s in _dbSubmission.Submissions
												 join j in _dbSubmission.JurisdictionalData on s.Id equals j.SubmissionId
												 join pj in _dbSubmission.trProjectJurisdictionalData on j.Id equals pj.JurisdictionalDataId into pjs
												 from pj in pjs.DefaultIfEmpty()
												 where (s.FileNumber == vm.Name && pj == null)
												 select
												 (
													 new BundleJurisdiction()
													 {
														 FileNumber = s.FileNumber,
														 IDNumber = s.IdNumber,
														 DateCode = s.DateCode,
														 GameName = s.GameName,
														 Version = s.Version,
														 SubmissionStatus = s.Status,
														 JurisdictionId = j.JurisdictionId,
														 Jurisdiction = j.Jurisdiction.Name,
														 JurisdictionStatus = j.Status,
														 Id = j.Id,
														 JurRcvdDate = j.ReceiveDate,
													 }
												)).ToList();
				vm.NewSubmissionJurisdictionsNotIncluded = jurisdictionsNotInProject;
			}

			//Warning for Closed Bundle
			if (vm.BundleType == BundleType.ClosedTesting)
			{
				var jursNotIncludedInClosed = GetJursFromOrgCertAndNotIncluded(projectId);
				if (jursNotIncludedInClosed.Count > 0)
				{
					vm.ShowMissingItemsFromOrgCertWarning = true;
				}
			}

			vm.HasTasks = _dbSubmission.trTasks.Where(x => x.ProjectId == vm.ProjectId).Count() != 0;
			return View("Index", vm);
		}

		public ActionResult Save(ManageBundleVM ManageBundleModel)
		{
			var qaBundle = _dbSubmission.QABundles.Where(x => x.ProjectId == ManageBundleModel.ProjectId).Single();
			if (ManageBundleModel.QASupervisorId != null && ManageBundleModel.QASupervisorId > 0)
			{
				qaBundle.QASupervisorId = ManageBundleModel.QASupervisorId;
			}

			if (ManageBundleModel.QAReviewerId != null && ManageBundleModel.QAReviewerId > 0)
			{
				qaBundle.QAReviewerId = ManageBundleModel.QAReviewerId;
			}

			qaBundle.CPFlag = ManageBundleModel.CPReqd;
			qaBundle.NoCertNeeded = ManageBundleModel.CertNotNeeded;
			if (qaBundle.ENNotes != ManageBundleModel.EngNotes)
			{
				var qaNote = new QABundles_Notes()
				{
					BundleId = qaBundle.Id,
					Note = ManageBundleModel.EngNotes + " (Entered as Eng Notes)",
					TimeStamp = DateTime.Now,
					UserId = _userContext.User.Id
				};
				_noteService.AddQANote(qaNote);
			}
			qaBundle.ENNotes = ManageBundleModel.EngNotes;

			if (ManageBundleModel.EngSupervisorId != null && ManageBundleModel.EngSupervisorId > 0)
			{
				if (ManageBundleModel.ENStatus == ProjectStatus.UU.ToString() && ManageBundleModel.ENEmailSent == null)
				{
					qaBundle.ENEmailSent = DateTime.Now;
					qaBundle.NotifyENSupervisor = ManageBundleModel.EngSupervisorId;

					//Set the location of bundle
					var supervisorlabId = _dbSubmission.trLogins.Where(x => x.Id == ManageBundleModel.EngSupervisorId).Select(x => x.LocationId).Single();
					var project = _dbSubmission.trProjects.Where(x => x.Id == qaBundle.ProjectId).Single();
					project.LabId = supervisorlabId;
					_projectService.UpdateProject(project);

					//Send an email
					SendENMail((int)ManageBundleModel.EngSupervisorId, qaBundle.ProjectName);

					if (ManageBundleModel.UpdateToENAccepted && ManageBundleModel.BundleType == BundleType.ClosedTesting)
					{
						//Accept in Engineering
						project.ENAcceptDate = DateTime.Now;
						project.Holder = ProjectHolder.EN.ToString();
						project.ENManagerId = ManageBundleModel.EngSupervisorId;
						project.OriginalENManagerId = ManageBundleModel.EngSupervisorId;
						_projectService.UpdateENStatus(project, ProjectStatus.AU);
					}
				}
			}

			_projectService.UpdateBundle(qaBundle);

			_projectService.SkipEngineering(ManageBundleModel.ProjectId, ManageBundleModel.SkipEngineering);

			if (ManageBundleModel.AllowTargetSave == true && ManageBundleModel.TargetDate != null)
			{
				_projectService.UpdateProjectJurisdictionTargetDate(ManageBundleModel.ProjectId, "", _userContext.User.Id, ManageBundleModel.TargetDate.Value.Date, "Original Target Date", 0, (int)TargetDateReviseMode.EnterOriginalTarget);
			}

			//This is for Closed Testing Bundles
			if (ManageBundleModel.BundleType == BundleType.ClosedTesting && ManageBundleModel.ENStatus == ProjectStatus.RC.ToString() && string.IsNullOrEmpty(ManageBundleModel.QAStatus))
			{
				//if Reviewer provided
				_projectService.AcceptProjectInQA(ManageBundleModel.ProjectId, (int)ManageBundleModel.QAReviewerId);
			}

			return RedirectToAction("Index", new { projectId = ManageBundleModel.ProjectId });
		}

		public ContentResult DeleteProject(int projectId)
		{
			_projectService.DeleteProject(projectId);
			return Content("OK");
		}

		public ContentResult SendEmail(int sendTo, string bundleName, int projectId)
		{
			var qaBundle = _dbSubmission.QABundles.Where(x => x.ProjectId == projectId).Single();
			qaBundle.ENEmailSent = DateTime.Now;
			qaBundle.NotifyENSupervisor = sendTo;
			_projectService.UpdateBundle(qaBundle);
			SendENMail(sendTo, bundleName);
			return Content("OK");
		}

		private void SendENMail(int sendTo, string bundleName)
		{
			var toEmail = _dbSubmission.trLogins.Where(x => x.Id == sendTo).Select(x => x.Email).SingleOrDefault();
			if (toEmail != null)
			{
				var mailMsg = new MailMessage("noreply@gaminglabs.com", toEmail);
				mailMsg.Subject = "Transfer Bundle - " + bundleName + " created for you.";
				mailMsg.Body = "Transfer Bundle - " + bundleName + " created for you.";
				_emailService.Send(mailMsg);
			}
		}

		private List<int> GetJursFromOrgCertAndNotIncluded(int projectId)
		{
			var jurs = (from tp in _dbSubmission.trProjectJurisdictionalData
						join p in _dbSubmission.trProjects on tp.ProjectId equals p.Id
						where p.Id == projectId
						select tp.JurisdictionalDataId).ToList();

			var jursFromOtherFiles = new List<int>();
			var allAssociatedProjects = new List<int>();
			foreach (var jurId in jurs)
			{
				//get current jur file
				var jurInfo = _dbSubmission.JurisdictionalData.Where(x => x.Id == jurId).Select(x => new { filenumber = x.Submission.FileNumber, jurisdictionId = x.JurisdictionId }).SingleOrDefault();

				//get original projects
				var orgProjectId = (from tp in _dbSubmission.trProjectJurisdictionalData
									join p in _dbSubmission.trProjects on tp.ProjectId equals p.Id
									where p.BundleTypeId == (int)BundleType.OriginalTesting
										 && tp.JurisdictionalDataId == jurId
									select p.Id).SingleOrDefault();

				if (orgProjectId != 0 && !allAssociatedProjects.Any(x => x == orgProjectId))
				{
					allAssociatedProjects.Add(orgProjectId);
				}
			}

			//Check all merge projects
			var allMergedProjects = new List<int>();
			foreach (var associateProject in allAssociatedProjects)
			{
				var mergedProjectIds = _projectService.ProjectMerged(associateProject).Select(x => x.Id).ToList();
				var notAlreadyAdded = mergedProjectIds.Except(allMergedProjects).ToList();
				allMergedProjects.AddRange(notAlreadyAdded);
			}

			//combine both sets
			var notAlreadyAssociated = allMergedProjects.Except(allAssociatedProjects).ToList();
			allAssociatedProjects.AddRange(notAlreadyAssociated);

			//check if same jur exits of some other file
			var allJursFromAssociated = _dbSubmission.trProjectJurisdictionalData.Where(x => allAssociatedProjects.Contains(x.ProjectId)).Select(x => x.JurisdictionalDataId).ToList();

			//closed and Not RJ/WD
			var closedJurs = _dbSubmission.JurisdictionalData.Where(x => allJursFromAssociated.Contains(x.Id) && (x.Status != JurisdictionStatus.RA.ToString() && x.Status != JurisdictionStatus.CP.ToString() && x.Status != JurisdictionStatus.OH.ToString() && x.Status != JurisdictionStatus.RJ.ToString() && x.Status != JurisdictionStatus.WD.ToString())).Select(x => x.Id).ToList();
			jursFromOtherFiles.AddRange(closedJurs.Except(jurs));

			var finallist = jursFromOtherFiles.Distinct().ToList();
			return finallist;
		}

		public ActionResult AddJursFromOrgCert(int projectId)
		{
			var jursNotIncludedInClosed = GetJursFromOrgCertAndNotIncluded(projectId);
			_projectService.CreateNewOrAddToExisting(jursNotIncludedInClosed, (int)BundleType.ClosedTesting, projectId);

			return RedirectToAction("Index", new { projectId = projectId });
		}

		public ContentResult AddToNewSubmissionProject(int projectId, string primarys)
		{
			var lstPrimarys = primarys.Split(',').Select(int.Parse).ToList();
			var newProjectId = _projectService.CreateNewOrAddToExisting(lstPrimarys, (int)BundleType.OriginalTesting, projectId);
			return Content("OK");
		}
	}
}