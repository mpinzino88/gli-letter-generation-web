﻿using GLI.EFCore.Submission;
using Submissions.Core;
using Submissions.Web.Areas.Protrack.Models.Project;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace Submissions.Web.Areas.Protrack.Controllers
{
	public class ComplexityRatingController : Controller
	{
		private readonly IProjectService _projectService;
		private readonly SubmissionContext _dbSubmission;

		public ComplexityRatingController(IProjectService projectService, SubmissionContext submissionContext)
		{
			_projectService = projectService;
			_dbSubmission = submissionContext;
		}

		//
		// GET: /Protrack/ComplexityRating/
		public ActionResult Index(int projectId)
		{
			var vm = new ComplexityRating();
			vm.ProjectIds = projectId.ToString();
			var projectFileNumber = _dbSubmission.trProjects.Where(x => x.Id == projectId).Select(x => x.FileNumber).Single();
			var item = _dbSubmission.FileNumberComplexityRating.Where(x => x.FileNumber == projectFileNumber).SingleOrDefault();
			if (item != null)
			{
				vm.ComplexityRatingId = item.ComplexityRatingId;
				vm.ProductTypeId = item.ProductTypeId;
			}
			return PartialView("_ComplexityRating", vm);
		}

		public ActionResult BatchComplexityRating(string projectIds)
		{
			var vm = new ComplexityRating();
			vm.ProjectIds = projectIds;
			return PartialView("_ComplexityRating", vm);
		}

		public JsonResult SaveComplexityRating(ComplexityRating vm)
		{
			var listProjects = new List<int>(vm.ProjectIds.Split(',').Select(x => int.Parse(x)));
			if (listProjects.Count > 0)
			{
				for (int i = 0; i < listProjects.Count; i++)
				{
					_projectService.UpdateProjectComplexityRating(listProjects[i], (int)vm.ProductTypeId, (int)((vm.ComplexityRatingId == null) ? 0 : vm.ComplexityRatingId));
				}
			}

			var results = (from p in _dbSubmission.trProjects
						   join f in _dbSubmission.FileNumberComplexityRating on p.ProjectName equals f.FileNumber
						   where listProjects.Contains(p.Id)
						   select (new { ProjectId = p.Id, NewVal = _dbSubmission.tbl_lst_ComplexityRating.Where(x => x.Id == f.ComplexityRatingId).Select(x => x.Rating).FirstOrDefault() })).ToList();

			return Json(results, JsonRequestBehavior.AllowGet);

		}

		public JsonResult ManufacturerRequiresComplexityRating(string projectIds)
		{
			var allResponse = "";
			var listProjects = new List<int>(projectIds.Split(',').Select(x => int.Parse(x)));
			if (listProjects.Count > 0)
			{
				var manufCodeList = (from p in _dbSubmission.trProjects
									 join s in _dbSubmission.SubmissionHeaders on p.FileNumber equals s.FileNumber
									 where listProjects.Contains(p.Id)
									 select s.ManufacturerCode).Distinct().ToList();

				for (int i = 0; i < manufCodeList.Count; i++)
				{
					var manufCode = manufCodeList[i];
					var response = _projectService.ManufacturerRequiresComplexityRating(manufCode);
					if (response)
					{
						if (allResponse == "")
						{
							allResponse = manufCode;
						}
						else
						{
							allResponse += "," + manufCode;
						}

					}
				}
			}

			return Json(allResponse, JsonRequestBehavior.AllowGet);
		}
	}
}