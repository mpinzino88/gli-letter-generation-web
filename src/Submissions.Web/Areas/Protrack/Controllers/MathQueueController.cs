﻿using GLI.EFCore.Submission;
using Newtonsoft.Json;
using Submissions.Common;
using Submissions.Core;
using Submissions.Web.Areas.Protrack.Models.MathQueue;
using Submissions.Web.Models.Query;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Web.Configuration;
using System.Web.Mvc;

namespace Submissions.Web.Areas.Protrack.Controllers
{
	using Submissions.Common.Email;
	using Submissions.Web.Utility.Email;

	public class MathQueueController : Controller
	{
		private readonly SubmissionContext _dbSubmission;
		private readonly IUserContext _userContext;
		private readonly IMathService _mathService;
		private readonly IProjectService _projectService;
		private readonly IEmailService _emailService;

		public MathQueueController
		(
			SubmissionContext dbSubmission,
			IUserContext userContext,
			IMathService mathService,
			IProjectService projectService,
			IEmailService emailService
		)
		{
			_dbSubmission = dbSubmission;
			_userContext = userContext;
			_mathService = mathService;
			_projectService = projectService;
			_emailService = emailService;
		}

		// GET: /Protrack/MathQueue/
		public ActionResult Index()
		{
			var vm = new IndexViewModel();
			vm.MathGridType = MathGridType.Reviews;
			SetFilters(vm, null);

			return View(vm);
		}

		public ActionResult LoadMathStatus()
		{
			var vm = new IndexViewModel();
			vm.MathGridType = MathGridType.Tasks;
			vm.Filters.ManufacturerShort = "";
			vm.Filters.Manufacturer = "";
			vm.Filters.TaskStatus = TaskStatusFilter.Open;

			return View("Index", vm);
		}

		public ActionResult MarkReviewComplete(string selIds)
		{
			//Mark Submit and Reviews Complete
			var reviews = JsonConvert.DeserializeObject<List<ReviewIdVM>>(selIds);

			MarkComplete(reviews);
			return Content("OK");
		}

		private void MarkComplete(List<ReviewIdVM> reviews)
		{
			var mathSubmits = reviews.Where(x => x.ReviewType == MathReviewType.Submit).Select(x => x.ReviewId).ToList();
			var mathCompletes = reviews.Where(x => x.ReviewType == MathReviewType.Review).Select(x => x.ReviewId).ToList();

			foreach (var submit in mathSubmits)
			{
				var existingItem = _dbSubmission.trMathSubmits.Where(x => x.Id == submit).SingleOrDefault();
				existingItem.Status = MathReviewStatus.DN.ToString();
				existingItem.Completed = DateTime.Now;
				_mathService.UpdateMathSubmit(existingItem);
			}

			foreach (var complete in mathCompletes)
			{
				var existingItem = _dbSubmission.trMathCompletes.Where(x => x.Id == complete).SingleOrDefault();
				existingItem.Status = MathReviewStatus.DN.ToString();
				existingItem.Completed = DateTime.Now;
				_mathService.UpdateMathComplete(existingItem);
			}
		}

		public ActionResult MarkReviewUnComplete(string selIds)
		{
			//Mark Submit and Reviews Complete
			var reviews = JsonConvert.DeserializeObject<List<ReviewIdVM>>(selIds);

			MarkUnComplete(reviews);

			return Content("OK");
		}

		private void MarkUnComplete(List<ReviewIdVM> reviews)
		{
			var mathSubmits = reviews.Where(x => x.ReviewType == MathReviewType.Submit).Select(x => x.ReviewId).ToList();
			var mathCompletes = reviews.Where(x => x.ReviewType == MathReviewType.Review).Select(x => x.ReviewId).ToList();

			foreach (var submit in mathSubmits)
			{
				var existingItem = _dbSubmission.trMathSubmits.Where(x => x.Id == submit).SingleOrDefault();
				existingItem.Status = MathReviewStatus.PN.ToString();
				existingItem.CompletedById = null;
				existingItem.Completed = null;
				_mathService.UpdateMathSubmit(existingItem);
			}

			foreach (var complete in mathCompletes)
			{
				var existingItem = _dbSubmission.trMathCompletes.Where(x => x.Id == complete).SingleOrDefault();
				existingItem.Status = MathReviewStatus.PN.ToString();
				existingItem.CompletedById = null;
				existingItem.Completed = null;
				_mathService.UpdateMathComplete(existingItem);
			}
		}

		public ActionResult AssignTasks(string selIds, string mathFilters, string mathGridType)
		{
			var reviews = JsonConvert.DeserializeObject<List<ReviewIdVM>>(selIds);

			MarkComplete(reviews);

			//Assign tasks
			foreach (var review in reviews)
			{
				var projectId = 0;
				if (review.ReviewType == MathReviewType.Submit)
				{
					var existingItem = _dbSubmission.trMathSubmits.Where(x => x.Id == review.ReviewId).SingleOrDefault();
					projectId = existingItem.ProjectId;
				}
				else
				{
					var existingItem = _dbSubmission.trMathCompletes.Where(x => x.Id == review.ReviewId).SingleOrDefault();
					projectId = existingItem.ProjectId;
				}

				var newTask = new trTask()
				{
					TargetDate = review.TaskTarget,
					EstimateHours = review.EstHrs,
					UserId = review.AssignTo,
					ProjectId = projectId,
					DepartmentId = (int)Department.MTH,
					TaskDefinitionId = (review.ReviewType == MathReviewType.Submit) ? (int)MathTasks.MathAnalysis : (int)MathTasks.MathReview
				};

				_projectService.AddTask(newTask);
			}

			var vm = new IndexViewModel();
			vm.MathGridType = (MathGridType)Enum.Parse(typeof(MathGridType), mathGridType);

			var filters = JsonConvert.DeserializeObject<MathSearch>(mathFilters);
			SetFilters(vm, filters);

			return View("Index", vm);
		}

		private void SetFilters(IndexViewModel vm, MathSearch selFilter)
		{
			if (selFilter != null)
			{
				vm.Filters.LaboratoryIds = selFilter.LaboratoryIds;
				var locations = _dbSubmission.trLaboratories.Where(i => selFilter.LaboratoryIds.Contains(i.Id.ToString())).Select(x => new SelectListItem() { Value = x.Id.ToString(), Text = x.Location + " -" + x.Name }).ToList();
				if (locations != null)
				{
					vm.Filters.Laboratories = locations;
				}
				vm.Filters.ManufacturerShort = "";
				vm.Filters.Manufacturer = "";
				vm.Filters.ReviewStatus = (vm.MathGridType == MathGridType.ReviewsDN) ? MathReviewStatus.DN : MathReviewStatus.PN;
			}
			else
			{
				vm.Filters.ManufacturerShort = "";
				vm.Filters.Manufacturer = "";
				vm.Filters.ReviewStatus = (vm.MathGridType == MathGridType.ReviewsDN) ? MathReviewStatus.DN : MathReviewStatus.PN;
			}

		}

		public ActionResult SetGridType(string mathFilters, string mathGridType)
		{
			var filters = JsonConvert.DeserializeObject<MathSearch>(mathFilters);
			if (mathGridType == MathGridType.Tasks.ToString())
			{
				return RedirectToAction("LoadMathStatus");
			}
			else
			{
				var vm = new IndexViewModel();
				vm.MathGridType = (mathGridType == MathGridType.ReviewsDN.ToString()) ? MathGridType.ReviewsDN : MathGridType.Reviews;
				SetFilters(vm, filters);
				return View("Index", vm);
			}
		}

		#region Incoming MathSubmit
		[HttpPost]
		public ContentResult CheckThemeExists(string theme, int projectId)
		{
			//Add Theme if not exist in List table
			var isExists = _dbSubmission.tbl_lst_Themes.Where(x => x.Name == theme).Any();
			if (!isExists)
			{
				_dbSubmission.tbl_lst_Themes.Add(new tbl_lst_Themes() { Name = theme, CreatedBy = _userContext.User.Id, CreatedOn = DateTime.Now });
				_dbSubmission.SaveChanges();
			}

			var isThemeExists = _dbSubmission.trMathSubmits.Where(x => x.ProjectId == projectId && x.Themes == theme).Any();
			if (isThemeExists)
			{
				return Content("Yes");
			}
			else
			{
				return Content("No");
			}
		}

		[HttpGet]
		public ContentResult CheckValidFile(string file)
		{
			var isFileExists = _dbSubmission.Submissions.Where(x => x.FileNumber == file).Any();
			if (isFileExists)
			{
				return Content("Yes");
			}
			else
			{
				return Content("No");
			}
		}

		public ActionResult LoadMathReviews(int projectId, string sourceType)
		{
			var vm = new MathReviewIndexViewModel();
			var project = _dbSubmission.trProjects.Where(x => x.Id == projectId).SingleOrDefault();
			vm.ProjectId = project.Id;
			vm.ProjectName = project.ProjectName;
			vm.SourceListType = (MathListType)Enum.Parse(typeof(MathListType), sourceType);

			if (vm.SourceListType == MathListType.Incoming)
			{
				vm.MathReviews = GetReviewList(projectId, MathReviewType.Submit.ToString());
				return PartialView("MathReviews", vm);
			}
			else
			{
				var allCompleteTheme = _dbSubmission.trMathCompletes.Where(x => x.ProjectId == projectId).Select(x => x.MathSubmit.Themes).ToList();
				var mathThemes = _dbSubmission.trMathSubmits.Where(x => x.ProjectId == projectId)
							.Where(x => !allCompleteTheme.Contains(x.Themes))
							.Select(x => new ListValue
							{
								Id = x.Id,
								Text = x.Themes
							}).Distinct().ToList();
				vm.ThemeAvailableForMathComplete = (mathThemes.Count > 0) ? true : false;
				vm.MathReviews = GetReviewList(projectId, MathReviewType.All.ToString());
				return View("MathReviews", vm);
			}
		}

		private List<MathReview> GetReviewList(int projectId, string reviewType)
		{
			//All Reviews
			var mathReviews = new List<MathReview>();
			if (reviewType == MathReviewType.Submit.ToString() || reviewType == MathReviewType.All.ToString())
			{
				mathReviews = _dbSubmission.trMathSubmits.Where(x => x.ProjectId == projectId)
								   .Select(x => new MathReview
								   {
									   Id = x.Id,
									   Submitter = x.Submitter.FName + " " + x.Submitter.LName,
									   Submitted = x.Submitted,
									   Theme = x.Themes,
									   AnalysisType = x.AnalysisType,
									   Status = x.Status,
									   ModifiedBy = x.ModifiedBy.FName + " " + x.ModifiedBy.LName,
									   ForeignProject = x.ForeignProject
								   }).ToList();
			}

			if (reviewType == MathReviewType.Review.ToString() || reviewType == MathReviewType.All.ToString())
			{
				var mathComplete = _dbSubmission.trMathCompletes.Where(x => x.ProjectId == projectId)
								   .Select(x => new MathReview
								   {
									   Id = x.Id,
									   Submitter = x.Submitter.FName + " " + x.Submitter.LName,
									   Submitted = x.Submitted,
									   Theme = x.MathSubmit.Themes,
									   AnalysisType = MathReviewType.Review.ToString(),
									   Status = x.Status,
									   ModifiedBy = x.ModifiedBy.FName + " " + x.ModifiedBy.LName,
									   ForeignProject = x.ForeignProject
								   }).ToList();
				mathReviews = mathReviews.Concat(mathComplete).ToList();
			}
			return mathReviews;
		}

		private MathReviewInfo GetMathInfo(int mathReviewId, int projectId)
		{
			var MathInfo = new MathReviewInfo();
			if (mathReviewId == 0)
			{
				MathInfo.Submitter = _userContext.User.FName + " " + _userContext.User.LName;
				MathInfo.SubmitterId = _userContext.User.Id;
				MathInfo.Submitted = DateTime.Now;
				MathInfo.IsLocked = false;
				MathInfo.IsRush = false;
			}
			else
			{
				var mathReviewInfo = (from m in _dbSubmission.trMathSubmits
									  where m.Id == mathReviewId
									  select (new MathReviewInfo()
									  {
										  Id = m.Id,
										  AnalysisType = m.AnalysisType,
										  ProgramType = m.ProgramType,
										  GameType = m.GameType,
										  CompletionRequested = m.CompletionRequested,
										  Note = m.Note,
										  PreviousThemes = m.PreviousThemes,
										  MathModelAffected = (m.IsMathModelAffected == null) ? 2 : m.IsMathModelAffected,
										  MathModelNote = m.MathModelAffectedNote,
										  Status = m.Status,
										  StatusNote = m.StatusNote,
										  SubmitterId = m.SubmitterId,
										  Submitter = m.Submitter.FName + " " + m.Submitter.LName,
										  Submitted = m.Submitted,
										  Modifier = (m.ModifiedBy != null) ? m.ModifiedBy.FName + " " + m.ModifiedBy.LName : "",
										  Themes = m.Themes,
										  PreviousFiles = m.PreviousFiles,
										  IsRush = (m.IsMathRush == true) ? true : false,
										  ForeignProject = m.ForeignProject,
										  ReviewTaskId = m.TaskId,
										  EstimateCompleteDate = m.EstimateCompleteDate,
										  InternalURL = m.InternalURL,
										  TestHarness = m.TestHarness
									  })).SingleOrDefault();
				MathInfo = mathReviewInfo;
				MathInfo.IsLocked = false;
				if (MathInfo.ReviewTaskId != null && MathInfo.ReviewTaskId > 0)
				{
					var isTaskDone = _dbSubmission.trTasks.Where(x => (x.Name == MathTasks.MathAnalysis.ToString() || x.Name == MathTasks.MathAnalysisDynamics.ToString()) && x.CompleteDate != null && x.Id == MathInfo.ReviewTaskId).Any();
					MathInfo.IsLocked = (isTaskDone) ? true : false;
				}
			}

			var projectInfo = _dbSubmission.trProjects.Where(x => x.Id == projectId).SingleOrDefault();
			MathInfo.HasUKTestLab = projectInfo.Lab == Laboratory.UK.ToString() ? true : false;
			MathInfo.ProjectId = projectInfo.Id;
			MathInfo.ProjectName = projectInfo.ProjectName;
			MathInfo.Location = projectInfo.Lab;

			//get submitter Manager
			if (Util.HasAccess(Role.ENS, Role.CPS, Role.MAS, Role.MG))
			{
				MathInfo.SubmitterManager = MathInfo.Submitter;
			}
			else
			{
				var manager = _dbSubmission.trLogins.Where(x => x.Id == MathInfo.SubmitterId).Select(m => m.Manager.FName + " " + m.Manager.LName).SingleOrDefault();
				MathInfo.SubmitterManager = manager;
			}

			var allProgramIds = _dbSubmission.trProjectJurisdictionalData.Where(x => x.ProjectId == projectId)
								.Select(x => new ListValue
								{
									Id = x.JurisdictionalData.Submission.Id,
									Text = x.JurisdictionalData.Submission.IdNumber
								}).Distinct().ToList();

			var allGames = _dbSubmission.trProjectJurisdictionalData.Where(x => x.ProjectId == projectId)
								.Select(x => new ListValue
								{
									Id = x.JurisdictionalData.Submission.Id,
									Text = x.JurisdictionalData.Submission.GameName
								}).Distinct().ToList();

			//All Themes
			var allThemes = _dbSubmission.tbl_lst_Themes.OrderBy(x => x.Name).Select(x => x.Name).Distinct().ToList();
			MathInfo.AllThemes = allThemes;
			MathInfo.AllProgramIds = allProgramIds;
			MathInfo.AllGames = allGames;

			return MathInfo;
		}

		public ActionResult LoadMathSubmit(int projectId, int id, string sourceType)
		{
			var vm = GetMathInfo(id, projectId);
			vm.SourceType = (MathListType)Enum.Parse(typeof(MathListType), sourceType);

			return PartialView("_MathSubmit", vm);
		}

		public ActionResult SaveMathIncoming(MathReviewInfo mathViewModel)
		{
			//Save InfomathViewModel
			if (mathViewModel.Id == 0)
			{
				var newReview = new trMathSubmit();
				newReview.ProjectId = mathViewModel.ProjectId;
				newReview.Themes = mathViewModel.Themes;
				newReview.SubmitterId = _userContext.User.Id;
				newReview.Submitted = DateTime.Now;
				newReview.AnalysisType = mathViewModel.AnalysisType;
				newReview.ProgramType = mathViewModel.ProgramType;
				newReview.PreviousThemes = mathViewModel.PreviousThemes;
				newReview.PreviousFiles = mathViewModel.PreviousFiles;
				newReview.GameType = mathViewModel.GameType;
				newReview.CompletionRequested = mathViewModel.CompletionRequested;
				newReview.MathModelAffectedNote = mathViewModel.MathModelNote;
				newReview.Note = mathViewModel.Note;
				newReview.IsMathRush = mathViewModel.IsRush;
				newReview.ModifiedById = _userContext.User.Id;
				newReview.Ts_Edit = DateTime.Now;
				newReview.Status = MathReviewStatus.PN.ToString();
				newReview.EstimateCompleteDate = mathViewModel.EstimateCompleteDate;
				newReview.InternalURL = mathViewModel.InternalURL;
				newReview.TestHarness = mathViewModel.TestHarness;
				_mathService.AddMathSubmit(newReview);

				//Send an email to Projectlead Sub-
				var ProjectInfo = _dbSubmission.trProjects.Where(x => x.Id == mathViewModel.ProjectId).Select(x => new { ENLeadMail = x.ENProjectLead.Email, ProjectName = x.ProjectName, Location = x.Lab }).Single();
				if (!string.IsNullOrEmpty(ProjectInfo.ENLeadMail))
				{
					var emailAddresses = ProjectInfo.ENLeadMail;
					if (WebConfigurationManager.AppSettings["Environment"] != SubmissionContext.Environment.Production.ToString())
					{
						emailAddresses = _userContext.User.Email;
					}
					else
					{
						if (ProjectInfo.Location == CustomLab.AT.ToString() || ProjectInfo.Location == CustomLab.ES.ToString() || ProjectInfo.Location == CustomLab.EU.ToString() || ProjectInfo.Location == CustomLab.IL.ToString() || ProjectInfo.Location == CustomLab.ZA.ToString())
						{
							emailAddresses = (!string.IsNullOrEmpty(emailAddresses)) ? emailAddresses + "," + _userContext.User.Email + "," + "A.Ilie@gaminglabs.com" : _userContext.User.Email + "," + "A.Ilie@gaminglabs.com";
						}
						else
						{
							emailAddresses = (!string.IsNullOrEmpty(emailAddresses)) ? emailAddresses + "," + _userContext.User.Email : _userContext.User.Email;
						}

					}

					var mailMsg = new MailMessage("noreply@gaminglabs.com", emailAddresses);
					mailMsg.Subject = "Math Request has been added for file: " + ProjectInfo.ProjectName + "(theme - " + mathViewModel.Themes + ")";

					mailMsg.Body = "Math Request has been added for file: " + ProjectInfo.ProjectName + "(theme - " + mathViewModel.Themes + ")";
					_emailService.Send(mailMsg);
				}
			}
			else
			{
				var existingReview = _dbSubmission.trMathSubmits.Where(x => x.Id == mathViewModel.Id).SingleOrDefault();

				// find all differences
				var formChanges = GetFormChanges(existingReview, mathViewModel);

				existingReview.ProjectId = mathViewModel.ProjectId;
				existingReview.Themes = mathViewModel.Themes;
				existingReview.AnalysisType = mathViewModel.AnalysisType;
				existingReview.ProgramType = mathViewModel.ProgramType;
				existingReview.PreviousThemes = mathViewModel.PreviousThemes;
				existingReview.PreviousFiles = mathViewModel.PreviousFiles;
				existingReview.GameType = mathViewModel.GameType;
				existingReview.CompletionRequested = mathViewModel.CompletionRequested;
				existingReview.MathModelAffectedNote = mathViewModel.MathModelNote;
				existingReview.Note = mathViewModel.Note;
				existingReview.IsMathRush = mathViewModel.IsRush;
				existingReview.ModifiedById = _userContext.User.Id;
				existingReview.Ts_Edit = DateTime.Now;
				existingReview.EstimateCompleteDate = mathViewModel.EstimateCompleteDate;
				existingReview.InternalURL = mathViewModel.InternalURL;
				existingReview.TestHarness = mathViewModel.TestHarness;
				_mathService.UpdateMathSubmit(existingReview);

				if (formChanges.Count > 0)
				{
					//Check if Math Analysis Task has been assigned
					var assignedTo = _dbSubmission.trTasks.Where(x => x.ProjectId == mathViewModel.ProjectId && (x.TaskDefinitionId == (int)MathTasks.MathAnalysis || x.TaskDefinitionId == (int)MathTasks.MathReview || x.TaskDefinitionId == (int)MathTasks.MathAdditionalAnalysisDynamics || x.TaskDefinitionId == (int)MathTasks.MathAdditionalAnalysis || x.TaskDefinitionId == (int)MathTasks.MathAnalysisDynamics || x.TaskDefinitionId == (int)MathTasks.MathReviewDynamics)).Select(x => x.UserId).ToList();

					//send an email to submitter as well.
					assignedTo.Add(existingReview.SubmitterId);

					//send email to Math Manager
					assignedTo.Add(existingReview.Project.MAManagerId);

					if (assignedTo.Count > 0)
					{
						var projectName = _dbSubmission.trProjects.Where(x => x.Id == mathViewModel.ProjectId).Select(x => x.ProjectName).FirstOrDefault();

						var userEmail = _dbSubmission.trLogins.Where(x => assignedTo.Contains(x.Id)).Select(x => x.Email).ToList();
						var emailAddresses = string.Join(",", userEmail);
						if (WebConfigurationManager.AppSettings["Environment"] != SubmissionContext.Environment.Production.ToString())
						{
							emailAddresses = _userContext.User.Email;
						}
						else
						{
							emailAddresses = (!string.IsNullOrEmpty(emailAddresses)) ? emailAddresses + "," + _userContext.User.Email : _userContext.User.Email;
						}

						var emailMathSubmit = new MathSubmitChange();
						emailMathSubmit.ProjectName = projectName;
						emailMathSubmit.Theme = mathViewModel.Themes;
						emailMathSubmit.FormChanges = formChanges;

						var mailMsg = new MailMessage("noreply@gaminglabs.com", emailAddresses);
						mailMsg.Subject = "Math Request has been modified for file: " + projectName + "(theme - " + mathViewModel.Themes + ")";

						mailMsg.Body = _emailService.RenderTemplate(EmailType.MathSubmitChange.ToString(), emailMathSubmit);
						_emailService.Send(mailMsg);
					}
				}
			}
			return RedirectToAction("LoadMathReviews", new { projectId = mathViewModel.ProjectId, sourceType = mathViewModel.SourceType.ToString() });
		}

		private List<ChangeForm> GetFormChanges(trMathSubmit existingForm, MathReviewInfo newFormInfo)
		{
			var formChanges = new List<ChangeForm>();
			if (existingForm.Themes != newFormInfo.Themes)
			{
				formChanges.Add(new ChangeForm()
				{
					FieldName = "Theme",
					NewVal = newFormInfo.Themes,
					OldVal = existingForm.Themes
				});
			};
			if (existingForm.AnalysisType != newFormInfo.AnalysisType)
			{
				formChanges.Add(new ChangeForm()
				{
					FieldName = "Analysis Type",
					NewVal = newFormInfo.AnalysisType,
					OldVal = existingForm.AnalysisType
				});
			};
			if (existingForm.ProgramType != newFormInfo.ProgramType)
			{
				formChanges.Add(new ChangeForm()
				{
					FieldName = "Program Type",
					NewVal = newFormInfo.ProgramType,
					OldVal = existingForm.ProgramType
				});
			};
			if (existingForm.PreviousFiles != newFormInfo.PreviousFiles)
			{
				formChanges.Add(new ChangeForm()
				{
					FieldName = "Previous Files",
					NewVal = newFormInfo.PreviousFiles,
					OldVal = existingForm.PreviousFiles
				});
			};
			if (existingForm.PreviousThemes != newFormInfo.PreviousThemes)
			{
				formChanges.Add(new ChangeForm()
				{
					FieldName = "Previous Themes",
					NewVal = newFormInfo.PreviousThemes,
					OldVal = existingForm.PreviousThemes
				});
			};
			if (existingForm.GameType != newFormInfo.GameType)
			{
				formChanges.Add(new ChangeForm()
				{
					FieldName = "Game Type",
					NewVal = newFormInfo.GameType,
					OldVal = existingForm.GameType
				});
			};
			if (existingForm.CompletionRequested != newFormInfo.CompletionRequested)
			{
				formChanges.Add(new ChangeForm()
				{
					FieldName = "Requested Completion",
					NewVal = newFormInfo.CompletionRequested.ToString(),
					OldVal = existingForm.CompletionRequested.ToString()
				});
			};
			if (existingForm.IsMathRush != newFormInfo.IsRush)
			{
				formChanges.Add(new ChangeForm()
				{
					FieldName = "Math Rush?",
					NewVal = newFormInfo.IsRush.ToString(),
					OldVal = existingForm.IsMathRush.ToString()
				});
			};

			if (existingForm.MathModelAffectedNote != newFormInfo.MathModelNote)
			{
				formChanges.Add(new ChangeForm()
				{
					FieldName = "Math Model Note",
					NewVal = newFormInfo.MathModelNote,
					OldVal = existingForm.MathModelAffectedNote
				});
			};
			return formChanges;
		}

		public ActionResult DeleteMathIncoming(int mathReviewId, int projectId, string sourceType)
		{
			_mathService.DeleteMathSubmit(mathReviewId);
			return RedirectToAction("LoadMathReviews", new { projectId = projectId, sourceType = sourceType });
		}

		#endregion

		#region MathComplete
		public ActionResult LoadMathComplete(int Id, int projectId, string sourceType)
		{
			var vm = LoadMathCompleteInfo(Id, projectId);
			vm.SourceType = (MathListType)Enum.Parse(typeof(MathListType), sourceType);

			return PartialView("_MathComplete", vm);
		}

		public ActionResult LoadFromPreviousProject(string file, string theme, int projectId, string sourceType)
		{
			var prevId = _dbSubmission.trProjects.Where(x => x.ProjectName == file).Select(x => x.Id).SingleOrDefault();
			var prevMathCompleteId = 0;
			if (prevId > 0)
			{
				prevMathCompleteId = _dbSubmission.trMathCompletes.Where(x => x.ProjectId == prevId && x.MathSubmit.Themes == theme).Select(x => x.Id).SingleOrDefault();
			}

			var vm = LoadMathCompleteInfo(0, projectId);

			if (prevMathCompleteId != 0 && prevMathCompleteId > 0)
			{
				foreach (MathFeature feature in Enum.GetValues(typeof(MathFeature)))
				{
					var featureId = (int)feature;
					var prevFeatures = (from mc in _dbSubmission.trMathCompleteItems
										join mf in _dbSubmission.trMathFeatureItems on mc.MathFeatureItemId equals mf.Id
										where mc.MathCompleteId == prevMathCompleteId && mf.MathFeatureId == featureId
										select mc.MathFeatureItemId
								 ).ToList();
					vm.FeatureItems.Find(x => x.Feature == feature).SelFeatureItems = prevFeatures;
				}
			}
			vm.SourceType = (MathListType)Enum.Parse(typeof(MathListType), sourceType);

			return PartialView("_MathComplete", vm);
		}

		private MathCompleteVM LoadMathCompleteInfo(int Id, int projectId)
		{
			var vm = new MathCompleteVM();
			vm.Id = Id;

			var mathComplete = _dbSubmission.trMathCompletes.Where(x => x.Id == Id).SingleOrDefault();
			if (mathComplete != null)
			{
				vm.ReviewId = mathComplete.MathSubmitId;
				vm.Note = mathComplete.Note;
				vm.TopAwardOccursIn = mathComplete.TopAwardOccursIn;
				vm.Status = mathComplete.Status;
				//if review Task done
				vm.IsLocked = false;
				if (mathComplete.TaskId != null && mathComplete.TaskId > 0)
				{
					var isTaskDone = _dbSubmission.trTasks.Where(x => x.Name == MathTasks.MathReview.ToString() && x.CompleteDate != null && x.Id == mathComplete.TaskId).Any();
					vm.IsLocked = (isTaskDone) ? true : false;
				}
				vm.NeedsReview = mathComplete.NeedsReview;
				vm.Submitter = (mathComplete.Submitter != null) ? mathComplete.Submitter.FName + " " + mathComplete.Submitter.LName : "";
				vm.Modifier = (mathComplete.ModifiedBy != null) ? mathComplete.ModifiedBy.FName + " " + mathComplete.ModifiedBy.LName : "";
				vm.CompletedBy = (mathComplete.CompletedBy != null) ? mathComplete.CompletedBy.FName + " " + mathComplete.CompletedBy.LName : "";
				vm.ForeignProject = mathComplete.ForeignProject;
				vm.IsReviewRush = (mathComplete.IsReviewRush == true) ? true : false;
				vm.IsPeerReview = mathComplete.IsPeerReview;
			}
			else
			{
				vm.ReviewId = 0;
				vm.IsLocked = false;
				vm.IsReviewRush = false;
				vm.IsPeerReview = false;
			}

			var project = _dbSubmission.trProjects.Where(x => x.Id == projectId).SingleOrDefault();
			vm.ProjectId = projectId;
			vm.ProjectName = project.ProjectName;
			vm.Location = project.Lab;

			var allFeatures = new List<MathFeatureItemsVM>();

			foreach (MathFeature feature in Enum.GetValues(typeof(MathFeature)))
			{
				var featureId = (int)feature;
				var allAvailable = _dbSubmission.trMathFeatureItems.Where(x => x.MathFeatureId == (int)feature)
									.Select(x => new MathFeatureVM
									{
										MathFeature = x.MathFeature.Name,
										MathFeatureId = x.MathFeatureId,
										MathFeatureItem = x.FeatureItem,
										MathFeatureItemId = x.Id
									}).ToList();

				var allSel = (from mc in _dbSubmission.trMathCompleteItems
							  join mf in _dbSubmission.trMathFeatureItems on mc.MathFeatureItemId equals mf.Id
							  where mc.MathCompleteId == Id && mf.MathFeatureId == featureId
							  select mc.MathFeatureItemId
							 ).ToList();

				allFeatures.Add(new MathFeatureItemsVM() { Feature = feature, AllFeatureItems = allAvailable, SelFeatureItems = allSel });
			}

			vm.FeatureItems = allFeatures;
			var allCompleteTheme = _dbSubmission.trMathCompletes.Where(x => x.ProjectId == projectId).Where(x => x.Id != Id).Select(x => x.MathSubmit.Themes).ToList();
			var mathThemes = _dbSubmission.trMathSubmits.Where(x => x.ProjectId == projectId)
							.Where(x => !allCompleteTheme.Contains(x.Themes))
							.Select(x => new ListValue
							{
								Id = x.Id,
								Text = x.Themes
							}).Distinct().ToList();

			mathThemes.Add(new ListValue() { Id = 0, Text = "--- Select Theme ---" });
			vm.AllThemes = mathThemes;

			var allProgramIds = _dbSubmission.trProjectJurisdictionalData.Where(x => x.ProjectId == projectId)
								.Select(x => new ListValue
								{
									Id = x.JurisdictionalData.Submission.Id,
									Text = x.JurisdictionalData.Submission.IdNumber
								}).Distinct().ToList();

			var allGames = _dbSubmission.trProjectJurisdictionalData.Where(x => x.ProjectId == projectId)
								.Select(x => new ListValue
								{
									Id = x.JurisdictionalData.Submission.Id,
									Text = x.JurisdictionalData.Submission.GameName
								}).Distinct().ToList();

			vm.AllProgramIds = allProgramIds;
			vm.AllGames = allGames;

			return vm;
		}

		public ActionResult SaveMathComplete(MathCompleteVM mathViewModel)
		{
			var selFeatures = JsonConvert.DeserializeObject<List<DictionaryObject>>(mathViewModel.SelFeatureItems);
			var mathCompleteId = mathViewModel.Id;
			if (mathCompleteId == 0)
			{
				//form with theme exist
				var existingform = _dbSubmission.trMathCompletes.Where(x => x.MathSubmitId == mathViewModel.ReviewId).Select(x => x.Id).SingleOrDefault();
				if (existingform > 0)
				{
					_mathService.DeleteMathComplete(existingform);
				}

				var newItem = new trMathComplete()
				{
					MathSubmitId = mathViewModel.ReviewId,
					ProjectId = mathViewModel.ProjectId,
					Note = mathViewModel.Note,
					TopAwardOccursIn = mathViewModel.TopAwardOccursIn,
					Status = ((bool)mathViewModel.NeedsReview) ? MathReviewStatus.PN.ToString() : MathReviewStatus.DN.ToString(),
					NeedsReview = mathViewModel.NeedsReview,
					IsReviewRush = mathViewModel.IsReviewRush,
					IsPeerReview = mathViewModel.IsPeerReview
				};
				_mathService.AddMathComplete(newItem);

				mathCompleteId = _dbSubmission.trMathCompletes.Where(x => x.MathSubmitId == mathViewModel.ReviewId).Select(x => x.Id).SingleOrDefault();
				SendMailMathComplete(mathViewModel);
			}
			else
			{
				var existingItem = _dbSubmission.trMathCompletes.Where(x => x.Id == mathViewModel.Id).SingleOrDefault();
				existingItem.Note = mathViewModel.Note;
				existingItem.MathSubmitId = mathViewModel.ReviewId;
				existingItem.TopAwardOccursIn = mathViewModel.TopAwardOccursIn;
				existingItem.Status = ((bool)mathViewModel.NeedsReview) ? MathReviewStatus.PN.ToString() : MathReviewStatus.DN.ToString();
				existingItem.NeedsReview = mathViewModel.NeedsReview;
				existingItem.IsReviewRush = mathViewModel.IsReviewRush;
				existingItem.IsPeerReview = mathViewModel.IsPeerReview;
				_mathService.UpdateMathComplete(existingItem);

				var allItems = _dbSubmission.trMathCompleteItems.Where(x => x.MathCompleteId == mathCompleteId).Select(x => x.Id).ToList();
				foreach (var item in allItems)
				{
					_mathService.DeleteMathCompleteItems(item);
				}
				_dbSubmission.SaveChanges();
			}

			if (mathCompleteId != 0)
			{
				foreach (var feature in selFeatures)
				{
					var allFeaturs = feature.SelVal;
					foreach (var id in allFeaturs)
					{
						var newMathItem = new trMathCompleteItems()
						{
							MathCompleteId = mathCompleteId,
							MathFeatureItemId = id
						};
						_mathService.AddMathCompleteItem(newMathItem);
					}
				}
			}
			return RedirectToAction("LoadMathReviews", new { projectId = mathViewModel.ProjectId, sourceType = mathViewModel.SourceType });
		}

		private void SendMailMathComplete(MathCompleteVM mathViewModel)
		{
			//Check if Math Analysis Task has been assigned
			var assignedTo = _dbSubmission.trTasks.Where(x => x.ProjectId == mathViewModel.ProjectId && (x.TaskDefinitionId == (int)MathTasks.MathAnalysis || x.TaskDefinitionId == (int)MathTasks.MathAnalysisDynamics)).Select(x => x.UserId).ToList();
			if (assignedTo.Count > 0)
			{
				var theme = _dbSubmission.trMathSubmits.Where(x => x.Id == mathViewModel.ReviewId).Select(x => x.Themes).SingleOrDefault();
				var projectName = _dbSubmission.trProjects.Where(x => x.Id == mathViewModel.ProjectId).Select(x => x.ProjectName).FirstOrDefault();
				var userEmail = _dbSubmission.trLogins.Where(x => assignedTo.Contains(x.Id)).Select(x => x.Email).ToList();
				var emailAddresses = string.Join(",", userEmail);
				if (WebConfigurationManager.AppSettings["Environment"] != SubmissionContext.Environment.Production.ToString())
				{
					emailAddresses = _userContext.User.Email;
				}
				else
				{
					emailAddresses = (!string.IsNullOrEmpty(emailAddresses)) ? emailAddresses + "," + _userContext.User.Email : _userContext.User.Email;
				}

				var mailMsg = new MailMessage("noreply@gaminglabs.com", emailAddresses);
				var msg = (mathViewModel.NeedsReview == true) ? "Math Review for " + projectName + "(theme - " + theme + ") was submitted and is in queue for review" : "Math Review for " + projectName + "(theme - " + theme + ") was submitted with no review";
				mailMsg.Subject = msg;
				mailMsg.Body = msg;
				_emailService.Send(mailMsg);
			}
		}

		public ActionResult DeleteMathComplete(int MathCompleteId, int projectId, string sourceType)
		{
			_mathService.DeleteMathComplete(MathCompleteId);
			return RedirectToAction("LoadMathReviews", new { projectId = projectId, sourceType = sourceType });
		}

		#endregion

		#region Math Review tasks
		public ContentResult AssignTask(MathReviewTasks vm)
		{
			if (vm.currentReviewTaskId != null)
			{
				if (vm.MathReviewType == MathReviewType.Submit.ToString())
				{
					var mathSubmit = _dbSubmission.trMathSubmits.Where(x => x.Id == vm.ReviewId).SingleOrDefault();
					mathSubmit.TaskId = vm.currentReviewTaskId;
					_mathService.UpdateMathSubmit(mathSubmit);
				}
				else if (vm.MathReviewType == MathReviewType.Review.ToString())
				{
					var mathComplete = _dbSubmission.trMathCompletes.Where(x => x.Id == vm.ReviewId).SingleOrDefault();
					mathComplete.TaskId = vm.currentReviewTaskId;
					_mathService.UpdateMathComplete(mathComplete);
				}
			}
			return Content("OK");
		}

		public ActionResult LoadMathReviewTasks(int reviewId, string type)
		{
			var vm = new MathReviewTasks();
			vm.ReviewId = reviewId;
			vm.MathReviewType = type;
			if (type == MathReviewType.Submit.ToString())
			{
				var mathSubmitInfo = _dbSubmission.trMathSubmits.Where(x => x.Id == reviewId).Select(x => new { ProjectID = x.ProjectId, MathTaskId = x.TaskId, ProjectName = x.Project.ProjectName, Theme = x.Themes }).SingleOrDefault();
				var tasks = _dbSubmission.trTasks.Where(x => x.ProjectId == mathSubmitInfo.ProjectID).Where(x => (x.TaskDefinitionId == (int)MathTasks.MathAnalysis || x.TaskDefinitionId == (int)MathTasks.MathAnalysisDynamics))
							.Select(x => new ReviewTasks()
							{
								TaskId = x.Id,
								AssignedOn = x.AddDate,
								AssignTo = (x.User != null) ? x.User.FName + " " + x.User.LName : "",
								EstHrs = x.EstimateHours,
								TaskName = x.Name,
								TaskTarget = x.TargetDate
							}).ToList();
				vm.MathTasks = tasks;
				vm.currentReviewTaskId = mathSubmitInfo.MathTaskId;
				vm.ProjectName = mathSubmitInfo.ProjectName;
				vm.Theme = mathSubmitInfo.Theme;
			}
			else if (type == MathReviewType.Review.ToString())
			{
				var mathCompleteInfo = _dbSubmission.trMathCompletes.Where(x => x.Id == reviewId).Select(x => new { ProjectID = x.ProjectId, MathTaskId = x.TaskId, ProjectName = x.Project.ProjectName, Theme = x.MathSubmit.Themes }).SingleOrDefault();
				var tasks = _dbSubmission.trTasks.Where(x => x.ProjectId == mathCompleteInfo.ProjectID).Where(x => (x.TaskDefinitionId == (int)MathTasks.MathReview || x.TaskDefinitionId == (int)MathTasks.MathReviewDynamics))
							.Select(x => new ReviewTasks()
							{
								TaskId = x.Id,
								AssignedOn = x.AddDate,
								AssignTo = (x.User != null) ? x.User.FName + " " + x.User.LName : "",
								EstHrs = x.EstimateHours,
								TaskName = x.Name,
								TaskTarget = x.TargetDate
							}).ToList();
				vm.MathTasks = tasks;
				vm.currentReviewTaskId = mathCompleteInfo.MathTaskId;
				vm.ProjectName = mathCompleteInfo.ProjectName;
				vm.Theme = mathCompleteInfo.Theme;
			}
			return PartialView("_ReviewMathTasks", vm);
		}

		#endregion
	}
}