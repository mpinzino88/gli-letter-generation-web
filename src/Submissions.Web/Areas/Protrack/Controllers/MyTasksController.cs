﻿using Dapper;
using GLI.EFCore.Submission;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Submissions.Common;
using Submissions.Core;
using Submissions.Web.Areas.Protrack.Models.MyTasks;
using Submissions.Web.Models.Query;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web.Mvc;

namespace Submissions.Web.Areas.Protrack.Controllers
{
	public class MyTasksController : Controller
	{
		private readonly SubmissionContext _dbSubmission;
		private readonly IUserContext _userContext;
		private readonly IMiscService _miscService;
		private readonly IQueryService _queryService;
		private readonly IProjectService _projectService;

		public MyTasksController(SubmissionContext dbSubmission, IUserContext userContext, IProjectService projectService, IMiscService miscService, IQueryService queryService)
		{
			_dbSubmission = dbSubmission;
			_userContext = userContext;
			_projectService = projectService;
			_queryService = queryService;
			_miscService = miscService;
		}
		//
		// GET: /Protrack/MyTasks/
		public ActionResult Index(string protrackType = "ENG")
		{
			ViewBag.FullWidth = true;
			Session["Protrack"] = protrackType;

			var vm = new IndexViewModel();

			vm.Filters.IsTransfer = ProjectType.All;
			vm.Filters.IsRush = false;
			vm.Filters.ShowETAQueue = false;
			vm.protrackType = (ProtrackType)Enum.Parse(typeof(ProtrackType), protrackType.ToString());
			vm.Filters.UserId = _userContext.User.Id;
			vm.Filters.User = _userContext.User.Name;

			//Tasks status count
			if (vm.protrackType == ProtrackType.QA)
			{
				vm.Filters.QATaskViewStatus = QATaskStatusFilter.Open;
				vm.TaskAvailableStatus.Add(new TaskCount() { Status = QATaskStatusFilter.Open.ToString(), Count = 0 });
				vm.TaskAvailableStatus.Add(new TaskCount() { Status = QATaskStatusFilter.NotStarted.ToString(), Count = 0 });
				vm.TaskAvailableStatus.Add(new TaskCount() { Status = QATaskStatusFilter.InProgress.ToString(), Count = 0 });
				vm.TaskAvailableStatus.Add(new TaskCount() { Status = QATaskStatusFilter.Finalization.ToString(), Count = 0 });
				vm.TaskAvailableStatus.Add(new TaskCount() { Status = QATaskStatusFilter.OnHold.ToString(), Count = 0 });
				vm.TaskAvailableStatus.Add(new TaskCount() { Status = QATaskStatusFilter.Closed.ToString(), Count = 0 });
				vm.TaskAvailableStatus.Add(new TaskCount() { Status = QATaskStatusFilter.EngReReview.ToString(), Count = 0 });
			}
			else
			{
				vm.Filters.Status = TaskStatusFilter.Open;
				vm.TaskAvailableStatus.Add(new TaskCount() { Status = TaskStatusFilter.Open.ToString(), Count = 0 });
				vm.TaskAvailableStatus.Add(new TaskCount() { Status = TaskStatusFilter.NotStarted.ToString(), Count = 0 });
				vm.TaskAvailableStatus.Add(new TaskCount() { Status = TaskStatusFilter.InProgress.ToString(), Count = 0 });
				vm.TaskAvailableStatus.Add(new TaskCount() { Status = TaskStatusFilter.OnHold.ToString(), Count = 0 });
				vm.TaskAvailableStatus.Add(new TaskCount() { Status = TaskStatusFilter.Closed.ToString(), Count = 0 });
			}

			return View(vm);
		}

		[HttpGet]
		public JsonResult GetTaskCount(string customFilters, string availableStatusCount, ProtrackType protrackType)
		{
			var taskCount = JsonConvert.DeserializeObject<List<TaskCount>>(availableStatusCount);

			var filters = JsonConvert.DeserializeObject<TaskSearch>(customFilters);

			var Count = 0;

			if (protrackType == ProtrackType.QA)
			{
				filters.Status = null;
				filters.QATaskViewStatus = null;
				var bundleResults = _queryService.QATaskSearch(filters)
					.Select(x => new
					{
						Id = x.Id,
						QATaskViewStatus = x.QATaskStatus,
						BundleStatus = x.BundleStatus
					});

				foreach (var statusCount in taskCount)
				{
					Count = 0;
					if (statusCount.Status == QATaskStatusFilter.Closed.ToString())
					{
						Count = bundleResults.Where(x => x.QATaskViewStatus == statusCount.Status.ToString()).Select(x => x.Id).Count();
					}
					else
					{
						if (statusCount.Status == QATaskStatusFilter.Open.ToString())
						{
							Count = bundleResults.Where(x => (x.QATaskViewStatus == QATaskStatusFilter.NotStarted.ToString() || x.QATaskViewStatus == QATaskStatusFilter.InProgress.ToString() || x.QATaskViewStatus == QATaskStatusFilter.Finalization.ToString() || x.QATaskViewStatus == QATaskStatusFilter.EngReReview.ToString() || x.QATaskViewStatus == QATaskStatusFilter.OnHold.ToString()) && x.BundleStatus != BundleStatus.DN.ToString()).Select(x => x.Id).Count();
						}
						else
						{
							Count = bundleResults.Where(x => x.QATaskViewStatus == statusCount.Status.ToString() && x.BundleStatus != BundleStatus.DN.ToString()).Select(x => x.Id).Count();
						}
					}
					statusCount.Count = Count;
				}
			}
			else
			{
				filters.Status = null;
				filters.QATaskViewStatus = null;
				var results = _queryService.TaskSearch(filters)
					.Select(x => new
					{
						Id = x.Id,
						TaskStatus = x.TaskStatus,
						ProjectStatus = x.Project.ENStatus,
					});

				foreach (var statusCount in taskCount)
				{
					Count = 0;
					if (statusCount.Status == TaskStatusFilter.Open.ToString())
					{
						Count = results.Where(x => (x.TaskStatus == TaskStatusFilter.NotStarted.ToString() || x.TaskStatus == TaskStatusFilter.InProgress.ToString() || x.TaskStatus == TaskStatusFilter.OnHold.ToString()) && x.ProjectStatus != ProjectStatus.DN.ToString()).Select(x => x.Id).Count();
					}
					else
					{
						if (statusCount.Status.ToString() == TaskStatusFilter.Closed.ToString())
						{
							Count = results.Where(x => x.TaskStatus == statusCount.Status.ToString()).Select(x => x.Id).Count();
						}
						else
						{
							Count = results.Where(x => x.TaskStatus == statusCount.Status.ToString() && x.ProjectStatus != ProjectStatus.DN.ToString()).Select(x => x.Id).Count();
						}
					}
					statusCount.Count = Count;
				}
			}

			return Json(taskCount, JsonRequestBehavior.AllowGet);
		}

		//public ContentResult IsTaskQAFinalization(int taskId)
		//{
		//	var isFinalization = _projectService.IsFinalizationTask(taskId);
		//	return Content((isFinalization) ? "Yes" : "No");
		//}

		[HttpPost]
		public JsonResult StartTask(int id)
		{
			try
			{
				var task = _dbSubmission.trTasks.Find(id);
				if (task.StartDate == null)
				{
					_projectService.StartTask(task);
				}

				Response.StatusCode = (int)HttpStatusCode.OK;
				return Json("success");
			}
			catch (Exception ex)
			{
				Response.StatusCode = (int)HttpStatusCode.InternalServerError;
				return Json(JsonConvert.SerializeObject(new { responseText = ex }));
				throw;
			}
		}

		[HttpPost]
		public JsonResult AssignTask(int id)
		{
			if (Util.HasAccess(Permission.ETA, AccessRight.Edit))
			{
				try
				{
					var task = _dbSubmission.trTasks.Find(id);
					if (task.UserId == null)
					{
						task.UserId = _userContext.User.Id;
						_projectService.UpdateTask(task);
					}
					return Json("success");
				}
				catch (Exception ex)
				{
					Response.StatusCode = (int)HttpStatusCode.InternalServerError;
					return Json(JsonConvert.SerializeObject(new { responseText = ex }));
					throw;
				}
			}
			else
			{
				return Json("Fail");
			}

		}

		[HttpPost]
		public JsonResult AssignTasks(List<int> ids)
		{
			try
			{
				var tasks = _dbSubmission.trTasks.Where(x => ids.Contains(x.Id)).ToList(); //.ForEach(x => x.UserId = _userContext.User.Id);
				foreach (var item in tasks)
				{
					item.UserId = _userContext.User.Id;
				}
				_projectService.UpdateTasks(tasks);

				Response.StatusCode = (int)HttpStatusCode.OK;
				return Json("success");
			}
			catch (Exception ex)
			{
				Response.StatusCode = (int)HttpStatusCode.InternalServerError;
				return Json(JsonConvert.SerializeObject(new { responseText = ex }));
				throw;
			}
		}

		[HttpPost]
		public JsonResult StartTasks(List<int> ids)
		{
			try
			{
				var tasks = _dbSubmission.trTasks.Where(x => ids.Contains(x.Id) && x.StartDate == null).ToList();
				_projectService.StartTasks(tasks);

				Response.StatusCode = (int)HttpStatusCode.OK;
				return Json("success");
			}
			catch (Exception ex)
			{
				Response.StatusCode = (int)HttpStatusCode.InternalServerError;
				return Json(JsonConvert.SerializeObject(new { responseText = ex }));
				throw;
			}
		}

		[HttpGet]
		public ActionResult CompleteTask(int taskId)
		{
			var vm = new CompleteTaskVM();
			vm.TaskId = taskId;

			var taskInfo = _dbSubmission.trTasks
							.Where(x => x.Id == taskId)
							.Select(x => new
							{
								TaskDynaId = x.Definition.DynamicsId,
								User = x.User.FName + " " + x.User.LName,
								ProjectId = x.ProjectId,
								Name = x.Definition.Description,
								ProjectName = x.Project.ProjectName

							}).SingleOrDefault();

			vm.TaskName = taskInfo.Name;
			vm.ProjectName = taskInfo.ProjectName;

			var query = "exec [GetQuickDetailTaskInfo] '" + taskInfo.ProjectId + "'";
			var allTasks = _dbSubmission.Database.GetDbConnection().Query<TaskHrVM>(query).ToList();
			if (allTasks != null)
			{
				var task = allTasks.Where(x => x.ProtrackTaskId == taskId).FirstOrDefault();
				if (task != null)
				{
					vm.EstHours = task.EstimateHours;
					vm.ActHours = task.ActualHours;
				}
			}

			return PartialView("_CompleteTask", vm);
		}

		[HttpPost]
		public JsonResult CompleteTask(TaskPayload Payload)
		{
			try
			{
				var task = _dbSubmission.trTasks.Where(x => x.Id == Payload.TaskId).SingleOrDefault();

				if (task == null)
				{
					Response.StatusCode = (int)HttpStatusCode.InternalServerError;
					return Json("The task has been deleted.  Please refresh your page.");
				}

				var email = new List<MailAddress>();

				if (Payload.Options.EmailAll)
				{
					email.AddRange(
						task.Project.Tasks
						.Where(x => x.User != null)
						.Select(x => new MailAddress(x.User.Email))
						.Distinct()
						.ToList()
					);
				}

				if (Payload.Options.EmailSupervisor)
				{
					if (task.Project.ENManager != null)
					{
						email.Add(new MailAddress(task.Project.ENManager.Email));
					}
				}

				var engProjectLeadId = task.Project.ENProjectLeadId;
				if (engProjectLeadId != null)
				{
					email.Add(new MailAddress(task.Project.ENProjectLead.Email));
				}

				if (!String.IsNullOrWhiteSpace(Payload.Options.Note))
				{
					_projectService.AddProjectNote(task.ProjectId, _userContext.User.Id, Payload.Options.Note, "I", "EN");
				}

				_projectService.CompleteTask(task, email);

				Response.StatusCode = (int)HttpStatusCode.OK;
				return Json("success");
			}
			catch (Exception ex)
			{
				Response.StatusCode = (int)HttpStatusCode.InternalServerError;
				return Json(ex);
				throw;
			}
		}
		#region QATasks
		[HttpPost]
		public JsonResult QAStartTask(int taskId)
		{
			try
			{
				_projectService.StartQATask(taskId);
				return Json("success");
			}
			catch (Exception ex)
			{
				Response.StatusCode = (int)HttpStatusCode.InternalServerError;
				return Json(JsonConvert.SerializeObject(new { responseText = ex }));
				throw;
			}
		}

		[HttpPost]
		public JsonResult QACompleteTask(int taskId)
		{
			try
			{
				_projectService.CompleteQATask(taskId);
				return Json("success");
			}
			catch (Exception ex)
			{
				Response.StatusCode = (int)HttpStatusCode.InternalServerError;
				return Json(JsonConvert.SerializeObject(new { responseText = ex }));
				throw;
			}
		}

		public JsonResult QAFinalizeTakeBundle(int taskId)
		{
			try
			{
				var selTask = _dbSubmission.QABundles_Task.Where(x => x.Id == taskId).Single();
				selTask.UserId = _userContext.User.Id;
				_dbSubmission.Entry(selTask).Property(x => x.UserId).IsModified = true;
				_dbSubmission.SaveChanges();
				var history = new QABundles_History
				{
					BundleId = selTask.BundleID,
					Notes = "Letter Writer Updated for Finalization.",
					UserId = _userContext.User.Id,
					AddDate = DateTime.Now
				};
				_projectService.InsertBundleHistory(history);
				//call dynamics to create project
				_dbSubmission.Database.ExecuteSqlRaw("exec Dynamics_SetLabForQAUser " + selTask.BundleID + "," + selTask.UserId);

				return Json("success");
			}
			catch (Exception ex)
			{
				Response.StatusCode = (int)HttpStatusCode.InternalServerError;
				return Json(JsonConvert.SerializeObject(new { responseText = ex }));
				throw;
			}
		}


		[HttpPost]
		public JsonResult QAFinalizeBundle(int bundleId)
		{
			try
			{
				_projectService.CompleteBundle(bundleId);
				return Json("success");
			}
			catch (Exception ex)
			{
				Response.StatusCode = (int)HttpStatusCode.InternalServerError;
				return Json(JsonConvert.SerializeObject(new { responseText = ex }));
				throw;
			}
		}

		public ContentResult CheckQATaskDependedOn(int taskId)
		{
			var result = "OK";
			var tasks = _projectService.CheckQATaskDependedOn(taskId);
			if (tasks.Count > 0)
			{
				result = string.Join(",", tasks);
			}
			return Content(result);
		}

		#endregion

	}

	#region Helper Class
	public class TaskPayload
	{
		public TaskPayload()
		{
			this.Options = new Options();
		}
		public int? TaskId { get; set; }
		public Options Options { get; set; }
	}

	public class Options
	{
		public string Note { get; set; }
		public bool EmailAll { get; set; }
		public bool EmailSupervisor { get; set; }
	}
	#endregion



}