extern alias ZEFCore;
using AutoMapper;
using Dapper;
using GLI.EFCore.Submission;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Submissions.Common;
using Submissions.Common.JQGrid;
using Submissions.Core;
using Submissions.Web.Areas.Protrack.Models.Project;
using Submissions.Web.Models;
using Submissions.Web.Models.Grids;
using Submissions.Web.Models.Query;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net.Mail;
using System.Web.Mvc;
using ZEFCore::Z.EntityFramework.Plus;

namespace Submissions.Web.Areas.Protrack.Controllers
{
	using Submissions.Common.Email;
	using System.Configuration;

	public class ProjectController : Controller
	{
		private readonly SubmissionContext _dbSubmission;
		private readonly IUserContext _userContext;
		private readonly IMiscService _miscService;
		private readonly IQueryService _queryService;
		private readonly IProjectService _projectService;
		private readonly IColumnChooserService _columnChooserService;
		private readonly IIssueService _issueService;
		private readonly IDocumentService _documentService;
		private readonly IEmailService _emailService;
		private readonly ILotoQuebecExportService _lqExportService;
		private readonly IConfigContext _configContext;
		private readonly ILogService _logService;

		public ProjectController
		(
			SubmissionContext dbSubmission,
			IUserContext userContext,
			IMiscService miscService,
			IQueryService queryService,
			IProjectService projectService,
			IEvoService evoService,
			IColumnChooserService columnChooserService,
			IIssueService issueService,
			IDocumentService documentService,
			IEmailService emailService,
			ILotoQuebecExportService lqExportService,
			IConfigContext configContext,
			ILogService logService
		)
		{
			_dbSubmission = dbSubmission;
			_userContext = userContext;
			_miscService = miscService;
			_projectService = projectService;
			_queryService = queryService;
			_columnChooserService = columnChooserService;
			_issueService = issueService;
			_documentService = documentService;
			_emailService = emailService;
			_lqExportService = lqExportService;
			_configContext = configContext;
			_logService = logService;
		}

		public ActionResult Index(string protrackType = "ENG", string gridType = "", string filters = "")
		{
			ViewBag.FullWidth = true;
			Session["Protrack"] = protrackType;
			if (!string.IsNullOrEmpty(gridType))
			{
				Session["GridType"] = gridType;
			}
			else
			{
				if (Session["GridType"] == null)
				{
					//Allow to set user settings
					Session["GridType"] = ProtrackGridType.Protrack;
				}
			}

			var vm = new IndexViewModel();
			vm.protrackType = (ProtrackType)Enum.Parse(typeof(ProtrackType), protrackType.ToString());
			vm.GridType = (ProtrackGridType)Enum.Parse(typeof(ProtrackGridType), Session["GridType"].ToString());

			//Glimpse view
			var selColumns = GetGlimpseSelectedColumns();
			vm.GlimpseGrid.InitializeSelColumns(selColumns, gridType);
			//current colChooserId
			var colChooserId = _dbSubmission.tbl_ColumnChooserGrids.Where(x => x.UserId == _userContext.User.Id && x.Grid == ColumnChooserGrid.GlimpseGrid.ToString() && x.Current).Select(x => x.Id).SingleOrDefault();
			vm.ColChooserId = colChooserId;

			if (vm.protrackType == ProtrackType.QA)
			{
				//form filters
				var formFilters = _dbSubmission.tbl_FormFilters
					.Where(x => x.UserId == _userContext.User.Id && x.Section == FormFilterSection.QAProjectLegacy.ToString() && x.Current)
					.Select(x => new { Id = x.Id, FilterName = x.FilterName, Filters = x.Filters })
					.SingleOrDefault();

				if (formFilters != null)
				{
					vm.FormFiltersId = formFilters.Id;
					vm.FormFiltersName = formFilters.FilterName;
					var selFilters = JsonConvert.DeserializeObject<ProjectSearch>(formFilters.Filters);
					SetFilters(ref vm, selFilters);
				}
				else if (!String.IsNullOrEmpty(filters))
				{
					var selfilters = JsonConvert.DeserializeObject<ProjectSearch>(filters);
					SetFilters(ref vm, selfilters);
				}
				else
				{
					vm.Filters.ReviewerIds.Add(_userContext.User.Id.ToString());
					vm.Filters.Reviewers.Add(new SelectListItem() { Value = _userContext.User.Id.ToString(), Text = _userContext.User.Name });
					vm.Filters.BundleStatus = QAProjectStatusFilter.OPEN.ToString();
					vm.Filters.Sort1 = "QAReceiveDate";
					vm.Filters.Sort1Order = Submissions.Common.SortOrder.Asc;
					vm.Filters.QALWCompletedFrom = DateTime.Now.AddDays(-30);
					vm.Filters.QALWCompletedTo = DateTime.Now;
					vm.Filters.IsTransfer = ProjectType.All;
				}

				//What counts to be shown
				vm.ProjectAvailableStatus.Add(new ProjectCount { Status = QAProjectStatusFilter.UU.ToString(), Count = 0 });
				vm.ProjectAvailableStatus.Add(new ProjectCount { Status = QAProjectStatusFilter.OPEN.ToString(), Count = 0 });
				vm.ProjectAvailableStatus.Add(new ProjectCount { Status = QAProjectStatusFilter.AU.ToString(), Count = 0 });
				vm.ProjectAvailableStatus.Add(new ProjectCount { Status = QAProjectStatusFilter.AA.ToString(), Count = 0 });
				vm.ProjectAvailableStatus.Add(new ProjectCount { Status = QAProjectStatusFilter.DL.ToString(), Count = 0 });
				vm.ProjectAvailableStatus.Add(new ProjectCount { Status = QAProjectStatusFilter.IR.ToString(), Count = 0 });
				vm.ProjectAvailableStatus.Add(new ProjectCount { Status = QAProjectStatusFilter.FL.ToString(), Count = 0 });
				vm.ProjectAvailableStatus.Add(new ProjectCount { Status = QAProjectStatusFilter.ENRR.ToString(), Count = 0 });
				vm.ProjectAvailableStatus.Add(new ProjectCount { Status = QAProjectStatusFilter.DN.ToString(), Count = 0 });
				vm.ProjectAvailableStatus.Add(new ProjectCount { Status = QAProjectStatusFilter.OH.ToString(), Count = 0 });
				vm.ProjectAvailableStatus.Add(new ProjectCount { Status = QAProjectStatusFilter.ENG.ToString(), Count = 0 });

				var filtersTask = new TaskSearch();
				filtersTask.UserId = _userContext.User.Id;
				filtersTask.IsTransfer = ProjectType.All;
				filtersTask.QATaskViewStatus = QATaskStatusFilter.Open;

				var openTasksCount = _queryService.QATaskSearch(filtersTask)
					.Select(x => x.Id).Count();
				vm.TaskCount = openTasksCount;
			}
			else
			{
				vm.CurrentLaboratoryId = _userContext.LocationId;
				vm.CurrentLaboratory = _userContext.Location + " - " + _userContext.LocationName;
				if (Util.HasAccess(Role.ENS, Role.CPS, Role.MAS))
				{
					vm.CurrentManagerId = _userContext.User.Id;
					vm.CurrentManager = _userContext.User.Name;
				}
				else
				{
					var CurrentManagerId = (Util.HasAccess(Role.MG) || _userContext.ManagerId == null) ? _userContext.User.Id : (int)_userContext.ManagerId;
					vm.CurrentManagerId = CurrentManagerId;
					var managerName = _dbSubmission.trLogins.Where(x => x.Id == CurrentManagerId).Select(x => x.FName + " " + x.LName).Single();
					vm.CurrentManager = managerName;
				}

				//form filters
				var formFilters = _dbSubmission.tbl_FormFilters
					.Where(x => x.UserId == _userContext.User.Id && x.Section == FormFilterSection.ProjectLegacy.ToString() && x.Current)
					.Select(x => new { Id = x.Id, FilterName = x.FilterName, Filters = x.Filters })
					.SingleOrDefault();

				if (formFilters != null)
				{
					vm.FormFiltersId = formFilters.Id;
					vm.FormFiltersName = formFilters.FilterName;
					var selFilters = JsonConvert.DeserializeObject<ProjectSearch>(formFilters.Filters);
					SetFilters(ref vm, selFilters);
				}
				else if (!String.IsNullOrEmpty(filters))
				{
					var selfilters = JsonConvert.DeserializeObject<ProjectSearch>(filters);
					SetFilters(ref vm, selfilters);
				}
				else
				{
					vm.Filters.ManagerIds.Add(vm.CurrentManagerId.ToString());
					vm.Filters.Managers.Add(new SelectListItem() { Value = vm.CurrentManagerId.ToString(), Text = vm.CurrentManager });
					vm.Filters.Status = ENProjectStatusFilter.OPEN.ToString();
					vm.Filters.CompleteFrom = DateTime.Now.AddDays(-30);
					vm.Filters.CompleteTo = DateTime.Now;
					vm.Filters.Sort1 = "ReceiveDate";
					vm.Filters.Sort1Order = Submissions.Common.SortOrder.Asc;
					vm.Filters.IsTransfer = ProjectType.All;
				}
				//What counts to be shown
				vm.ProjectAvailableStatus.Add(new ProjectCount { Status = ENProjectStatusFilter.UU.ToString(), Count = 0 });
				vm.ProjectAvailableStatus.Add(new ProjectCount { Status = ENProjectStatusFilter.OPEN.ToString(), Count = 0 });
				vm.ProjectAvailableStatus.Add(new ProjectCount { Status = ENProjectStatusFilter.AU.ToString(), Count = 0 });
				vm.ProjectAvailableStatus.Add(new ProjectCount { Status = ENProjectStatusFilter.AA.ToString(), Count = 0 });
				vm.ProjectAvailableStatus.Add(new ProjectCount { Status = ENProjectStatusFilter.IT.ToString(), Count = 0 });
				vm.ProjectAvailableStatus.Add(new ProjectCount { Status = ENProjectStatusFilter.IR.ToString(), Count = 0 });
				vm.ProjectAvailableStatus.Add(new ProjectCount { Status = ENProjectStatusFilter.RR.ToString(), Count = 0 });
				vm.ProjectAvailableStatus.Add(new ProjectCount { Status = ENProjectStatusFilter.DCRQ.ToString(), Count = 0 });
				vm.ProjectAvailableStatus.Add(new ProjectCount { Status = ENProjectStatusFilter.RC.ToString(), Count = 0 });
				vm.ProjectAvailableStatus.Add(new ProjectCount { Status = ENProjectStatusFilter.QA.ToString(), Count = 0 });
				vm.ProjectAvailableStatus.Add(new ProjectCount { Status = ENProjectStatusFilter.DN.ToString(), Count = 0 });
				vm.ProjectAvailableStatus.Add(new ProjectCount { Status = ENProjectStatusFilter.DCOH.ToString(), Count = 0 });
				vm.ProjectAvailableStatus.Add(new ProjectCount { Status = ENProjectStatusFilter.OH.ToString(), Count = 0 });

				var filtersTask = new TaskSearch();
				filtersTask.UserId = _userContext.User.Id;
				filtersTask.IsTransfer = ProjectType.All;
				filtersTask.Status = TaskStatusFilter.Open;

				var openTasksCount = _queryService.TaskSearch(filtersTask)
					.Select(x => x.Id).Count();
				vm.TaskCount = openTasksCount;
			}

			vm.GridDateFields = vm.ProjectGrid.GetAllDateColumns();
			vm.GridIntFields = vm.ProjectGrid.GetAllIntColumns();
			return View(vm);
		}

		private void SetFilters(ref IndexViewModel vm, ProjectSearch selFilter)
		{
			if (vm.protrackType == ProtrackType.QA)
			{
				vm.Filters.QALaboratoryIds = selFilter.QALaboratoryIds;
				var QAlocations = _dbSubmission.trLaboratories.Where(i => selFilter.QALaboratoryIds.Contains(i.Id.ToString())).Select(x => new SelectListItem() { Value = x.Id.ToString(), Text = x.Location + " -" + x.Name }).ToList(); ;
				vm.Filters.QALaboratories = QAlocations;

				vm.Filters.ReviewerIds = selFilter.ReviewerIds;
				var reviewers = _dbSubmission.trLogins.Where(i => selFilter.ReviewerIds.Contains(i.Id.ToString())).Select(x => new SelectListItem() { Value = x.Id.ToString(), Text = x.FName + " " + x.LName }).ToList();
				vm.Filters.Reviewers = reviewers;

				vm.Filters.QAAcceptedFrom = selFilter.QAAcceptedFrom;
				vm.Filters.QAAcceptedTo = selFilter.QAAcceptedTo;
				vm.Filters.EngCompletedFrom = selFilter.EngCompletedFrom;
				vm.Filters.EngCompletedTo = selFilter.EngCompletedTo;
				vm.Filters.QALWCompletedFrom = DateTime.Now.AddDays(-30);
				vm.Filters.QALWCompletedTo = DateTime.Now;
				vm.Filters.QATaskTarget = selFilter.QATaskTarget;
				vm.Filters.TargetDateFrom = selFilter.TargetDateFrom;
				vm.Filters.TargetDateTo = selFilter.TargetDateTo;

				//QA supervisor

				if (selFilter.QASupervisorId != 0)
				{
					vm.Filters.QASupervisorId = selFilter.QASupervisorId;
					var QASupervisor = _dbSubmission.trLogins.Where(i => i.Id == selFilter.QASupervisorId).Select(x => x.FName + " " + x.LName).SingleOrDefault();
					vm.Filters.QASupervisor = QASupervisor;
				}

				if (selFilter.BundleStatus != null)
				{
					vm.Filters.BundleStatus = selFilter.BundleStatus;
				}
				else
				{
					vm.Filters.BundleStatus = QAProjectStatusFilter.OPEN.ToString();
				}

				vm.Filters.IsTransfer = selFilter.IsTransfer;
			}
			else
			{
				vm.Filters.CompleteFrom = DateTime.Now.AddDays(-30);
				vm.Filters.CompleteTo = DateTime.Now;
				vm.Filters.LaboratoryIds = selFilter.LaboratoryIds;
				var locations = _dbSubmission.trLaboratories.Where(i => selFilter.LaboratoryIds.Contains(i.Id.ToString())).Select(x => new SelectListItem() { Value = x.Id.ToString(), Text = x.Location + " -" + x.Name }).ToList(); ;
				vm.Filters.Laboratories = locations;

				vm.Filters.ManagerIds = selFilter.ManagerIds;
				var managers = _dbSubmission.trLogins.Where(i => selFilter.ManagerIds.Contains(i.Id.ToString())).Select(x => new SelectListItem() { Value = x.Id.ToString(), Text = x.FName + " " + x.LName }).ToList();
				vm.Filters.Managers = managers;

				if (selFilter.DevelopmentRepIds.Count > 0)
				{
					vm.Filters.DevelopmentRepIds = selFilter.DevelopmentRepIds;
					var devReps = _dbSubmission.trLogins.Where(i => selFilter.DevelopmentRepIds.Contains(i.Id.ToString())).Select(x => new SelectListItem() { Value = x.Id.ToString(), Text = x.FName + " " + x.LName }).ToList();
					vm.Filters.DevelopmentReps = devReps;
				}

				if (selFilter.SeniorEngineerIds.Count > 0)
				{
					vm.Filters.SeniorEngineerIds = selFilter.SeniorEngineerIds;
					var seniors = _dbSubmission.trLogins.Where(i => selFilter.SeniorEngineerIds.Contains(i.Id.ToString())).Select(x => new SelectListItem() { Value = x.Id.ToString(), Text = x.FName + " " + x.LName }).ToList();
					vm.Filters.SeniorEngineers = seniors;
				}

				if (selFilter.MAProjectLeadIds.Count > 0)
				{
					vm.Filters.MAProjectLeadIds = selFilter.MAProjectLeadIds;
					var mathProjectLeads = _dbSubmission.trLogins.Where(i => selFilter.MAProjectLeadIds.Contains(i.Id.ToString())).Select(x => new SelectListItem() { Value = x.Id.ToString(), Text = x.FName + " " + x.LName }).ToList();
					vm.Filters.MAProjectLeads = mathProjectLeads;
				}

				vm.Filters.ClientNumber = selFilter.ClientNumber;

				if (selFilter.EngineerIds.Count > 0)
				{
					vm.Filters.EngineerIds = selFilter.EngineerIds;
					var engineers = _dbSubmission.trLogins.Where(i => selFilter.EngineerIds.Contains(i.Id.ToString())).Select(x => new SelectListItem() { Value = x.Id.ToString(), Text = x.FName + " " + x.LName }).ToList();
					vm.Filters.Engineers = engineers;
				}

				if (selFilter.Status != null)
				{
					vm.Filters.Status = selFilter.Status;
				}
				else
				{
					vm.Filters.Status = ProjectStatus.OPEN.ToString();
				}
			}

			//These are common filters between both protrack
			vm.Filters.ReceivedFrom = selFilter.ReceivedFrom;
			vm.Filters.ReceivedTo = selFilter.ReceivedTo;
			vm.Filters.IsRush = selFilter.IsRush;
			vm.Filters.IsTransfer = selFilter.IsTransfer;
			vm.Filters.UserFavoriteId = selFilter.UserFavoriteId;

			if (selFilter.ManufacturerShorts.Count > 0)
			{
				vm.Filters.ManufacturerShorts = selFilter.ManufacturerShorts;
				var manufs = _dbSubmission.tbl_lst_fileManu.Where(i => selFilter.ManufacturerShorts.Contains(i.Code)).Select(x => new SelectListItem() { Value = x.Code, Text = x.Description }).ToList();
				vm.Filters.Manufacturers = manufs;
			}

			if (selFilter.SubmissionType != null)
			{
				vm.Filters.SubmissionType = selFilter.SubmissionType;
				var subType = _dbSubmission.tbl_lst_SubmissionType.Where(i => i.Id == selFilter.SubmissionType).Select(x => x.Code + " - " + x.Description).SingleOrDefault();
				vm.Filters.SubmissionTypeDesc = subType;
			}

			if (selFilter.Jurisdiction != null)
			{
				if (selFilter.Jurisdiction.Length > 0)
				{
					vm.Filters.Jurisdiction = selFilter.Jurisdiction;
					var jurDesc = _dbSubmission.tbl_lst_fileJuris.Where(i => i.Id == selFilter.Jurisdiction).Select(x => x.Name).SingleOrDefault();
					vm.Filters.JurisdictionDesc = jurDesc;
				}
			}

			vm.Filters.Sort1 = selFilter.Sort1;
			vm.Filters.Sort1Order = selFilter.Sort1Order;
		}

		[HttpGet]
		public JsonResult GetProjectCount(string customFilters, string availableStatusCount, ProtrackType protrackType, ProtrackGridType gridType)
		{
			var projectCount = JsonConvert.DeserializeObject<List<ProjectCount>>(availableStatusCount);
			var filters = JsonConvert.DeserializeObject<ProjectSearch>(customFilters);

			var Count = 0;
			var manufList = new List<string>();
			var projectLeadList = new List<string>();
			var mathProjectLeadList = new List<string>();
			if (protrackType == ProtrackType.QA)
			{
				//this is to get distinct Manuf filter filters 
				var bundlefilterResults = _queryService.ProjectSearch(filters).Select(x => new { Manuf = x.Manufacturer }).ToList();

				//this is to get counts of all different status
				filters.BundleStatus = null;
				var bundleResults = _queryService.ProjectSearch(filters)
					.Select(x => new
					{
						Id = x.Id,
						BundleStatus = x.BundleStatus,
						Status = x.Status,
						OnHold = x.OnHold,
						QACompleteDate = x.QACompleteDate,
						QALWCompletedTime = x.QALWCompletedTime
					}).ToList();

				var unacceptedBundleFilter = new ProjectSearch() { Status = ProjectStatus.RC.ToString(), IsTransfer = ProjectType.All };
				var unacceptedBundles = _queryService.ProjectSearch(unacceptedBundleFilter)
					.Select(x => new
					{
						Id = x.Id,
						BundleStatus = x.BundleStatus,
						Status = x.Status,
						OnHold = x.OnHold
					}).ToList();

				foreach (var statusCount in projectCount)
				{
					Count = 0;
					if (statusCount.Status == QAProjectStatusFilter.OPEN.ToString())
					{
						Count = bundleResults.Where(x => x.BundleStatus != BundleStatus.DN.ToString()).Select(x => x.Id).Count();
					}
					else if (statusCount.Status == QAProjectStatusFilter.OH.ToString())
					{
						Count = bundleResults.Where(x => x.OnHold == true).Select(x => x.Id).Count();
					}
					else if (statusCount.Status == QAProjectStatusFilter.ENG.ToString())
					{
						Count = bundleResults.Where(x => x.BundleStatus == "" && x.Status != ProjectStatus.DN.ToString() && x.Status != ProjectStatus.QA.ToString() && x.OnHold != true).Select(x => x.Id).Count();
					}
					else if (statusCount.Status == QAProjectStatusFilter.DN.ToString())
					{
						if (filters.QALWCompletedFrom != null || filters.QALWCompletedTo != null)
						{
							if (filters.QALWCompletedFrom == null)
							{
								Count = bundleResults.Where(x => (x.BundleStatus == BundleStatus.DN.ToString() && x.QALWCompletedTime <= filters.QALWCompletedTo) && x.OnHold != true).Select(x => x.Id).Count();
							}
							else if (filters.QALWCompletedTo == null)
							{
								Count = bundleResults.Where(x => (x.BundleStatus == BundleStatus.DN.ToString() && x.QALWCompletedTime >= filters.QALWCompletedFrom) && x.OnHold != true).Select(x => x.Id).Count();
							}
							else
							{
								Count = bundleResults.Where(x => (x.BundleStatus == BundleStatus.DN.ToString() && x.QALWCompletedTime >= filters.QALWCompletedFrom && x.QALWCompletedTime <= filters.QALWCompletedTo) && x.OnHold != true).Select(x => x.Id).Count();
							}
						}
						else
						{
							Count = bundleResults.Where(x => x.BundleStatus == BundleStatus.DN.ToString() && x.OnHold != true).Select(x => x.Id).Count();
						}
					}
					else if (statusCount.Status == QAProjectStatusFilter.UU.ToString())
					{
						Count = unacceptedBundles.Select(x => x.Id).Count();
					}
					else
					{
						Count = bundleResults.Where(x => x.BundleStatus == statusCount.Status.ToString() && x.OnHold != true).Select(x => x.Id).Count();
					}
					statusCount.Count = Count;
				}
				manufList = bundlefilterResults.Select(x => x.Manuf).Distinct().OrderBy(x => x).ToList();

			}
			else // engineering Protrack
			{
				//this is to get distinct Manuf filter and projectLead filters 
				var filterResults = _queryService.ProjectSearch(filters).Select(x => new { Manuf = x.Manufacturer, ProjectLead = x.ProjectLead, mathProjectLead = x.MAProjectLead }).ToList();

				//to get counts for all different status
				filters.Status = null;
				var results = _queryService.ProjectSearch(filters)
					.Select(x => new
					{
						Id = x.Id,
						Status = x.Status,
						OnHold = x.OnHold,
						QACompleteDate = x.QACompleteDate
					}).ToList();

				var unacceptedFilter = new ProjectSearch() { LaboratoryId = _userContext.LocationId, Status = ProjectStatus.UU.ToString(), IsTransfer = ProjectType.All, ExcludeFileTypes = new List<String>() { SubmissionType.VT.ToString(), SubmissionType.HT.ToString() } };
				var unaccepted = _queryService.ProjectSearch(unacceptedFilter)
					.Select(x => new
					{
						Id = x.Id,
						Status = x.Status,
						OnHold = x.OnHold
					}).ToList();

				foreach (var statusCount in projectCount)
				{
					Count = 0;
					if (statusCount.Status == ENProjectStatusFilter.OPEN.ToString())
					{
						Count = results.Where(x => (x.Status != ProjectStatus.DN.ToString() && x.Status != ProjectStatus.UU.ToString()) || x.OnHold == true).Select(x => x.Id).Distinct().Count();
					}
					else if (statusCount.Status == ENProjectStatusFilter.DCOH.ToString())
					{
						Count = results.Where(x => x.OnHold == true && x.Status == ProjectStatus.DC.ToString()).Count();
					}
					else if (statusCount.Status == ENProjectStatusFilter.DCRQ.ToString())
					{
						Count = results.Where(x => x.OnHold == false && x.Status == ProjectStatus.DC.ToString()).Count();
					}
					else if (statusCount.Status == ENProjectStatusFilter.OH.ToString())
					{
						Count = results.Where(x => x.OnHold == true && x.Status != ProjectStatus.DC.ToString()).Select(x => x.Id).Count();
					}
					else if (statusCount.Status == ENProjectStatusFilter.UU.ToString())
					{
						Count = unaccepted.Select(x => x.Id).Count();
					}
					else if (statusCount.Status == ENProjectStatusFilter.DN.ToString())
					{
						if (filters.CompleteFrom != null || filters.CompleteTo != null)
						{
							if (filters.CompleteFrom == null)
							{
								Count = results.Where(x => (x.Status == ProjectStatus.DN.ToString() && x.QACompleteDate <= filters.CompleteTo) && x.OnHold != true).Select(x => x.Id).Count();
							}
							else if (filters.CompleteTo == null)
							{
								Count = results.Where(x => (x.Status == ProjectStatus.DN.ToString() && x.QACompleteDate >= filters.CompleteFrom) && x.OnHold != true).Select(x => x.Id).Count();
							}
							else
							{
								Count = results.Where(x => (x.Status == ProjectStatus.DN.ToString() && x.QACompleteDate >= filters.CompleteFrom && x.QACompleteDate <= filters.CompleteTo) && x.OnHold != true).Select(x => x.Id).Count();
							}
						}
						else
						{
							Count = results.Where(x => (x.Status == ProjectStatus.DN.ToString()) && x.OnHold != true).Select(x => x.Id).Count();
						}

					}
					else
					{
						Count = results.Where(x => x.Status == statusCount.Status.ToString() && x.OnHold != true).Select(x => x.Id).Count();
					}
					statusCount.Count = Count;
				}
				manufList = filterResults.Select(x => x.Manuf).Distinct().OrderBy(x => x).ToList();
				projectLeadList = filterResults.Where(x => x.ProjectLead != null).Select(x => x.ProjectLead).OrderBy(x => x).Distinct().ToList();
				mathProjectLeadList = filterResults.Where(x => x.mathProjectLead != null).Select(x => x.mathProjectLead).OrderBy(x => x).Distinct().ToList();
			}
			return Json(new { ProjectCount = projectCount, manufList = manufList, projectLeadList = projectLeadList, mathProjectLeadList = mathProjectLeadList }, JsonRequestBehavior.AllowGet);
		}

		[HttpPost]
		public ActionResult SetGridType(string filters, string gridType, string protrackType)
		{
			return RedirectToAction("Index", new { protrackType = protrackType, gridType = gridType, filters = filters });
		}

		#region GlimpseGrid
		public ActionResult ColumnChooser(string filters, int colChooserId = 0)
		{
			//TempData["filters"] = filters;

			var iColExists = _dbSubmission.tbl_ColumnChooserGrids.Where(x => x.Id == colChooserId).Any(); //if exists and not deleted
			if (iColExists == false) { colChooserId = 0; }

			var vm = new ColumnChooserViewModel();
			var selColumns = GetGlimpseSelectedColumns(colChooserId).Replace("\"", "").Replace("[", "").Replace("]", "").Split(',').ToList();
			var allColumns = GetDefaultGlimpse().Replace("\"", "").Replace("[", "").Replace("]", "").Split(',').ToList();
			var availableColumns = allColumns.Except(selColumns).ToList();

			var gm = new GlimpseGrid();
			vm.SubTitle = "Glimpse View";
			vm.SelectedColumns = gm.GetAllColumnChooserDisplayNames(string.Join(",", selColumns));
			vm.AvailableColumns = gm.GetAllColumnChooserDisplayNames(string.Join(",", availableColumns));
			vm.ColChooserMode = (colChooserId != 0) ? ColumnChooserMode.Edit : ColumnChooserMode.Add;
			if (colChooserId != 0)
			{
				var colChooserName = _dbSubmission.tbl_ColumnChooserGrids.Where(x => x.Id == colChooserId).Select(x => x.Name).SingleOrDefault();
				vm.ColChooserName = colChooserName;
			}
			return PartialView("_ColumnChooser", vm);
		}

		public string GetGlimpseSelectedColumns(int colChooserId = 0)
		{

			var selectedColumns = (colChooserId == 0) ? _columnChooserService.LoadColumns(ColumnChooserGrid.GlimpseGrid) : _columnChooserService.LoadColumns(ColumnChooserGrid.GlimpseGrid, colChooserId);
			if (string.IsNullOrEmpty(selectedColumns))
			{
				//Get Default columns 
				selectedColumns = _columnChooserService.LoadDefaultColumns(ColumnChooserGrid.GlimpseGrid);
				_columnChooserService.SaveColumns(ColumnChooserGrid.GlimpseGrid, selectedColumns);

			}
			return selectedColumns;
		}

		[HttpPost]
		public ActionResult ColumnChooser(string SavedColumns, string filters, string colChooserMode, string colChooserName, int colChooserId = 0)
		{
			_columnChooserService.SaveColumns(ColumnChooserGrid.GlimpseGrid, SavedColumns, colChooserId, colChooserName);
			return RedirectToAction("Index", new { protrackType = Session["Protrack"], gridType = Session["gridType"], filters = filters });
		}

		public string GetDefaultGlimpse()
		{
			var selColumns = _columnChooserService.LoadDefaultColumns(ColumnChooserGrid.GlimpseGrid);
			return selColumns;
		}

		#endregion

		#region Bulk updates
		[HttpPost]
		public ContentResult SetUserFavorites(string projectIds)
		{
			var arrProjectIds = projectIds.Split(',').Select(int.Parse).ToList();

			foreach (var projectId in arrProjectIds)
			{
				var favExists = _dbSubmission.trFavoriteFiles.Where(x => x.ProjectId == projectId && x.UserId == _userContext.User.Id).Any();
				if (favExists)
				{
					_dbSubmission.trFavoriteFiles.Where(x => x.ProjectId == projectId && x.UserId == _userContext.User.Id).Delete();
				}
				var favorite = new trFavoriteFile
				{
					ProjectId = projectId,
					UserId = _userContext.User.Id,
					Active = true
				};
				_dbSubmission.trFavoriteFiles.Add(favorite);
			}
			_dbSubmission.SaveChanges();

			return Content("OK");
		}

		[HttpPost]
		public ContentResult UnSetUserFavorites(string projectIds)
		{
			var arrProjectIds = projectIds.Split(',').Select(int.Parse).ToList();
			_dbSubmission.trFavoriteFiles.Where(x => arrProjectIds.Contains(x.ProjectId) && x.UserId == _userContext.User.Id).Delete();

			return Content("OK");
		}

		[HttpPost]
		public JsonResult UpdateEstTestComplete(string projectIds, DateTime date, string protrack)
		{
			var arrProjectIds = new List<int>(projectIds.Split(',').Select(x => int.Parse(x)));
			for (int i = 0; i < arrProjectIds.Count; i++)
			{
				_projectService.UpdateProjectEstTestComplete(Convert.ToInt32(arrProjectIds[i]), date);
			}
			var results = _dbSubmission.trProjects.Where(x => arrProjectIds.Contains(x.Id)).Select(x => new { ProjectId = x.Id, NewVal = x.ENEstimateTestCompleteDate }).ToList();
			var results1 = (from m in results
							select (new { ProjectId = m.ProjectId, NewVal = m.NewVal.ToString("M/d/yyyy") })
							).ToList();

			return Json(results1, JsonRequestBehavior.AllowGet);
		}

		[HttpPost]
		public JsonResult UpdateEstMathComplete(string projectIds, DateTime date, string protrack)
		{
			var arrProjectIds = new List<int>(projectIds.Split(',').Select(x => int.Parse(x)));
			for (int i = 0; i < arrProjectIds.Count; i++)
			{
				_projectService.UpdateProjectEstMathComplete(Convert.ToInt32(arrProjectIds[i]), date);
			}
			var results = _dbSubmission.trProjects
				.Where(x => arrProjectIds.Contains(x.Id))
				.Select(x => new
				{
					ProjectId = x.Id,
					NewVal = x.MAEstimateCompleteDate == date ? x.MAEstimateCompleteDate : null
				}).ToList();
			var results1 = (from m in results
							select (new { ProjectId = m.ProjectId, NewVal = m.NewVal.ToString("M/d/yyyy") })
							).ToList();
			return Json(results1, JsonRequestBehavior.AllowGet);
		}

		[HttpGet]
		public JsonResult GetUserFavorites()
		{
			var myFavs = _dbSubmission.trFavoriteFiles.Where(x => x.UserId == _userContext.User.Id && x.Active == true).Select(x => x.ProjectId).ToList();
			return Json(myFavs, JsonRequestBehavior.AllowGet);
		}
		#endregion

		public ActionResult Edit(int? id)
		{
			var project = _dbSubmission.trProjects
				.Where(x => x.Id == id)
				.Single();

			var commonSubmission = _dbSubmission.Submissions
				.Where(x => x.FileNumber == project.FileNumber)
				.Select(x => new { ManufacturerCode = x.Manufacturer.Code })
				.First();

			var vm = new EditViewModel();
			vm = Mapper.Map<EditViewModel>(project);

			vm.ComplexityRating = new ComplexityRating();
			vm.Status = (ProjectStatus)Enum.Parse(typeof(ProjectStatus), project.ENStatus);

			vm.MAManagerId = project.MAManagerId;
			vm.MAManager = (project.MAManagerId != null) ? project.MAManager.FName + " " + project.MAManager.LName : "";

			vm.SubmissionsReceivedDate = _dbSubmission.Submissions
											.Where(x => x.FileNumber == project.FileNumber)
											.Select(x => x.ReceiveDate).Min();

			var item = _dbSubmission.FileNumberComplexityRating.Where(x => x.FileNumber == project.FileNumber).SingleOrDefault();
			if (item != null)
			{
				if (item.ComplexityRatingId > 0)
				{
					vm.ComplexityRating.ComplexityRatingId = item.ComplexityRatingId;
				}
				vm.ComplexityRating.ProductTypeId = item.ProductTypeId;
			}

			var quoteItem = project.QuotedHours.Where(x => x.DeactivateDate == null).SingleOrDefault();
			if (quoteItem != null)
			{
				vm.QuotedHours = (double)quoteItem.Hours;
			}
			var noCertNeeded = _dbSubmission.QABundles.Where(x => x.ProjectId == id).Select(x => x.NoCertNeeded).SingleOrDefault();
			vm.NoCertNeeded = noCertNeeded;

			var iGamingTestingTypeIds = _dbSubmission.tbl_lst_TestingType.Where(x => x.Description == TestingType.iGaming.ToString() || x.Description == TestingType.Sportsbook.ToString()).Select(x => x.Id).ToList().Cast<int?>().ToList();
			var isiGaming = _dbSubmission.trProjectJurisdictionalData.Where(x => x.ProjectId == id).Any(x => iGamingTestingTypeIds.Contains(x.JurisdictionalData.Submission.TestingTypeId));

			if (isiGaming)
			{
				vm.HasiGamingTestingType = isiGaming;

				CustomManufacturer customManufacturer;
				if (Enum.TryParse(commonSubmission.ManufacturerCode, out customManufacturer))
				{
					vm.ShowGameSubmissionType = ComUtil.GameSubmissionTypeManufacturers().Any(x => x == (CustomManufacturer)Enum.Parse(typeof(CustomManufacturer), commonSubmission.ManufacturerCode));
				}

				var translationHouses = _dbSubmission.trProjectTranslationHouses
					.Where(x => x.ProjectId == id)
					.OrderBy(x => x.TranslationHouse.Description)
					.Select(x => new SelectListItem { Value = x.TranslationHouseId.ToString(), Text = x.TranslationHouse.Description })
					.ToList();
				vm.TranslationHouseIds = translationHouses.Select(x => Int32.Parse(x.Value)).ToArray();
				vm.TranslationHouses = translationHouses;
			}

			return View(vm);
		}

		[HttpPost]
		public ActionResult Edit(int id, EditViewModel vm)
		{
			var project = _dbSubmission.trProjects
				.Where(x => x.Id == vm.Id)
				.Single();

			project.IsTBDEstMathCompleteDate = vm.IsEstMathCompleteDateToBeDetermined;

			if (vm.IsEstMathCompleteDateToBeDetermined)
			{
				project.MAEstimateCompleteDate = null;
			}
			else
			{
				if (vm.EstMathComplete != null)
				{
					_projectService.UpdateProjectEstMathComplete(id, (DateTime)vm.EstMathComplete);
				}
			}
			project.ENEstimateTestCompleteDate = vm.EstTestComplete;
			project.ENEstimateCompleteDate = vm.EstReviewComplete;
			project.QAEstimateCompleteDate = vm.EstLetterIssueDate;
			project.QADeliveryDate = vm.QADeliveryDate;
			if (!String.IsNullOrEmpty(vm.ReceivedDate.ToString()))
			{
				project.ReceiveDate = vm.ReceivedDate;
			}

			if (vm.MAManagerId != null && (vm.MAManagerId != project.MAManagerId))
			{
				project.MAManagerId = vm.MAManagerId;
			}

			if ((vm.OriginalENManagerId != null) && (vm.OriginalENManagerId != project.OriginalENManagerId))
			{
				project.OriginalENManagerId = vm.OriginalENManagerId;
			}
			if (vm.HasiGamingTestingType)
			{
				project.RequestedDeliveryDate = vm.RequestedDeliveryDate;
				project.GoLiveDate = vm.GoLiveDate;
				project.FTPLocation = vm.FTPLocation;
				project.TestingEnvironment = vm.TestingEnvironment;
				project.GameSubmissionTypeId = vm.GameSubmissionTypeId;
				project.GameSubmissionTypeInfo = vm.GameSubmissionTypeInfo;
				if (vm.TranslationHouseIds != null)
				{
					var updateProjectTranslationHouses = !(project.ProjectTranslationHouses.Select(x => x.TranslationHouseId).ToList().All(vm.TranslationHouseIds.Contains) && project.ProjectTranslationHouses.Count == vm.TranslationHouseIds.Length);
					if (updateProjectTranslationHouses)
					{
						foreach (var projectTranslationHouse in project.ProjectTranslationHouses.ToList())
						{
							_dbSubmission.Entry(projectTranslationHouse).State = EntityState.Detached;
						}
						_dbSubmission.trProjectTranslationHouses.Where(x => x.ProjectId == id).Delete();

						foreach (var translationHouseId in vm.TranslationHouseIds)
						{
							var newProjectTranslationHouse = new trProjectTranslationHouse
							{
								ProjectId = id,
								TranslationHouseId = translationHouseId
							};
							_dbSubmission.trProjectTranslationHouses.Add(newProjectTranslationHouse);
						}
						_dbSubmission.SaveChanges();
					}
				}
				else if (project.ProjectTranslationHouses.Count > 0)
				{
					_dbSubmission.trProjectTranslationHouses.Where(x => x.ProjectId == id).Delete();
				}
			}
			_projectService.UpdateProject(project);


			//Since emails go out
			_projectService.UpdateProjectDevRep(id, vm.DevelopmentRepresentativeId);
			if ((vm.ENProjectLeadId != null) && (vm.ENProjectLeadId != project.ENProjectLeadId))
			{
				_projectService.UpdateProjectLead(id, vm.ENProjectLeadId);
			}
			if (vm.MAProjectLeadId != null && vm.MAProjectLeadId != project.MAProjectLeadId)
			{
				_projectService.UpdateMathProjectLead(id, vm.MAProjectLeadId);
			}

			_projectService.UpdateQuotedHours(vm.Id, Convert.ToDecimal(vm.QuotedHours));

			//No cert Needed
			var bundle = _dbSubmission.QABundles.Where(x => x.ProjectId == vm.Id).SingleOrDefault();
			if (bundle != null)
			{
				if (bundle.NoCertNeeded != vm.NoCertNeeded)
				{
					bundle.NoCertNeeded = vm.NoCertNeeded;
					_dbSubmission.QABundles.Attach(bundle);
					_dbSubmission.Entry(bundle).Property(x => x.NoCertNeeded).IsModified = true;
					_dbSubmission.SaveChanges();
				}
			}

			return RedirectToAction("Edit/" + vm.Id);
		}

		#region Detail
		public ActionResult Detail(int id)
		{
			var project = _dbSubmission.trProjects
				.Where(x => x.Id == id)
				.Single();

			var bundle = _dbSubmission.QABundles
				.Where(x => x.ProjectId == id)
				.SingleOrDefault();

			var submission = _dbSubmission.Submissions
				.Where(x => x.FileNumber == project.FileNumber)
				.Select(x => new
				{
					x.Type,
					x.ManufacturerCode,
					x.CryptographicallyStrongId,
					x.CryptographicallyStrong,
					x.ReceiveDate,
					Platforms = x.Platforms.OrderBy(y => y.Platform.Name).Select(y => y.Platform.Name)
				})
				.OrderBy(x => x.ReceiveDate)
				.FirstOrDefault();

			var projectJurisdictions = _dbSubmission.trProjectJurisdictionalData
				.Where(x => x.ProjectId == id)
				.Select(x => new
				{
					x.JurisdictionalData.JurisdictionId,
					x.JurisdictionalData.MasterBillingProject,
					x.JurisdictionalData.Status,
					x.JurisdictionalData.TargetDate,
					SubmissionTestingType = x.JurisdictionalData.Submission.TestingType.Code
				})
				.ToList();

			var translationHouses = _dbSubmission.trProjectTranslationHouses
				.Where(x => x.ProjectId == id)
				.OrderBy(x => x.TranslationHouse.Description)
				.Select(x => x.TranslationHouse.Description)
				.ToList();

			var projectStatus = (ProjectStatus)Enum.Parse(typeof(ProjectStatus), project.ENStatus);

			DateTime? testCompleteDate = null;
			if (ComUtil.TestingCompletedProjectStatuses().Contains(projectStatus))
			{
				testCompleteDate = project.Tasks.Select(x => x.CompleteDate).Max();
			}

			var lQjuris = CustomJurisdictionGroups.LotoQuebec.Select(x => ((int)x).ToJurisdictionIdString()).ToList();

			var projectModel = new ProjectDetail();
			//general
			projectModel.Id = project.Id;
			projectModel.Name = project.ProjectName;
			projectModel.FileNumber = project.FileNumber;
			projectModel.OriginalFile = project.FileNumber;
			projectModel.ReceiveDate = project.ReceiveDate;
			projectModel.RequestedDeliveryDate = project.RequestedDeliveryDate;
			projectModel.GoLiveDate = project.GoLiveDate;
			projectModel.CompleteDate = project.QACompleteDate;
			projectModel.Status = projectStatus;
			projectModel.Holder = (ProjectHolder)Enum.Parse(typeof(ProjectHolder), project.Holder);
			projectModel.CertificationLab = project.CertificationLab;
			projectModel.Lab = project.Lab;
			projectModel.OnHold = project.OnHold;
			projectModel.JurisdictionIds = projectJurisdictions.Select(x => x.JurisdictionId).ToList();
			projectModel.HasLotoQuebec = lQjuris.Any(x => projectModel.JurisdictionIds.Contains(x));
			projectModel.UpdateAcceptDate = project.UpdateAcceptDate;
			projectModel.IsFavorite = _dbSubmission.trFavoriteFiles.Where(x => x.ProjectId == project.Id && x.UserId == _userContext.User.Id).Any();
			projectModel.ReceiveDatesNotSame = 0;
			projectModel.IsEstMathCompleteDateToBeDetermined = project.IsTBDEstMathCompleteDate;
			projectModel.IsTransfer = project.IsTransfer;
			projectModel.MergedWith = project.MergedWith;
			projectModel.Manufacturer = submission.ManufacturerCode;
			projectModel.SubmissionsReceivedDate = submission.ReceiveDate;

			//Engineering info
			projectModel.ENManagerLocation = project.ENManager == null ? "" : project.ENManager.Location.Location;
			projectModel.ENManager = project.ENManager == null ? "" : (project.ENManager.FName + " " + project.ENManager.LName).Trim();
			projectModel.ENManagerId = project.ENManagerId;
			projectModel.ENOriginalManager = (project.OriginalENManager == null) ? "" : (project.OriginalENManager.FName + " " + project.OriginalENManager.LName).Trim();
			projectModel.ENProjectLead = project.ENProjectLead == null ? "" : (project.ENProjectLead.FName + " " + project.ENProjectLead.LName).Trim();
			projectModel.DevelopmentRepresentative = project.DevelopmentRepresentative == null ? "" : project.DevelopmentRepresentative.FName + " " + project.DevelopmentRepresentative.LName;
			projectModel.Platforms = String.Join(", ", submission.Platforms.ToList());
			projectModel.FTPLocation = project.FTPLocation;
			projectModel.TestingEnvironment = project.TestingEnvironment;
			projectModel.TranslationHouses = String.Join(", ", translationHouses);
			projectModel.GameSubmissionType = project.GameSubmissionType?.Description;
			projectModel.GameSubmissionTypeInfo = project.GameSubmissionTypeInfo;

			//Math Info
			projectModel.MAManagerId = project.MAManagerId;
			projectModel.MAManager = (project.MAManagerId != null) ? project.MAManager.FName + " " + project.MAManager.LName : "";
			projectModel.MAProjectLead = project.MAProjectLead == null ? "" : (project.MAProjectLead.FName + " " + project.MAProjectLead.LName).Trim();

			//Targets
			projectModel.TargetDate = project.TargetDate;
			projectModel.OrgTargetDate = project.OrgTargetDate;
			projectModel.QADeliveryDate = project.QADeliveryDate;
			projectModel.TotalOHDays = project.TotalOHTime == null ? 0 : project.TotalOHTime;

			//check if jur missing Target
			var isJurWithoutTargetdate = projectJurisdictions
				.Where(x => x.TargetDate == null &&
					(
						x.Status != JurisdictionStatus.OH.ToString() &&
						x.Status != JurisdictionStatus.RJ.ToString() &&
						x.Status != JurisdictionStatus.WD.ToString()
					))
				.Any();

			projectModel.JurMissingTarget = (isJurWithoutTargetdate && projectModel.TargetDate != null) ? true : false;

			if (bundle == null)
			{
				projectModel.bundleId = 0;
				projectModel.IsRush = false;
				projectModel.QAReviewer = "";
				projectModel.QASupervisor = "";
				projectModel.QFINum = "";
				projectModel.LetterWritingStarted = null;
			}
			else
			{
				projectModel.bundleId = bundle.Id;
				projectModel.IsRush = (bundle.IsRush == 1) ? true : false;
				projectModel.QAReviewer = bundle.QAReviewer == null ? "" : (bundle.QAReviewer.FName + " " + bundle.QAReviewer.LName);
				projectModel.QASupervisor = bundle.QASupervisor == null ? "" : (bundle.QASupervisor.FName + " " + bundle.QASupervisor.LName);
				if (!string.IsNullOrEmpty(bundle.QAStatus))
				{
					projectModel.QAStatus = (BundleStatus)Enum.Parse(typeof(BundleStatus), bundle.QAStatus);
				}

				projectModel.QFINum = bundle.NCRNumber;
				projectModel.CPReq = (bundle.CPFlag == true) ? "Yes" : "";
				projectModel.LetterWritingStarted = bundle.QALWStarted;
				projectModel.QAResubmission = bundle.ReSubmission;
				projectModel.QAOfficeId = project.QAOfficeId;
				projectModel.QAOffice = project.QAOffice?.Name;
				projectModel.QATasksCount = bundle.Tasks.Count();
				projectModel.NoCertNeeded = bundle.NoCertNeeded;
				projectModel.ADMWorkNeeded = bundle.ADMWorkNeeded;
				projectModel.ENInvolvementWhileFL = bundle.ENInvolvementWhileFL;
				projectModel.ENInvolvementWhileFLNotes = bundle.ENInvolvementWhileFLNote;
			}

			/*if ((projectModel.SubmissionsReceivedDate != projectModel.ReceiveDate) && (projectModel.IsTransfer != true))
-			{
-				projectModel.ReceiveDatesNotSame = 1;
-			}*/

			//get Jira Info
			var resultsCountOpenClosed = _issueService.GetIssueOpenClosedCountSQL(new List<int> { id });
			projectModel.OpenIssues = (int)resultsCountOpenClosed.Results[0].OpenCount;
			projectModel.OpenIssuesURL = ConfigurationManager.AppSettings["jira.baseurl"] + "/issues/?jql=" + resultsCountOpenClosed.Results[0].OpenURL;

			projectModel.ClosedIssues = (int)resultsCountOpenClosed.Results[0].ClosedCount;
			projectModel.ClosedIssuesURL = ConfigurationManager.AppSettings["jira.baseurl"] + "/issues/?jql=" + resultsCountOpenClosed.Results[0].ClosedURL;

			//get Sub Jira Info
			var resultsSubCountOpenClosed = _issueService.GetIssueOpenClosedCountSQL(new List<int> { id }, true);
			projectModel.SubOpenIssues = (int)resultsSubCountOpenClosed.Results[0].OpenCount;
			projectModel.SubOpenIssuesURL = ConfigurationManager.AppSettings["jira.baseurl"] + "/issues/?jql=" + resultsSubCountOpenClosed.Results[0].OpenURL;

			projectModel.SubClosedIssues = (int)resultsSubCountOpenClosed.Results[0].ClosedCount;
			projectModel.SubClosedIssuesURL = ConfigurationManager.AppSettings["jira.baseurl"] + "/issues/?jql=" + resultsSubCountOpenClosed.Results[0].ClosedURL;

			// Full Text Jira Search by FileNumber
			projectModel.JIRAProjectNameFullTextSearchURL = ConfigurationManager.AppSettings["jira.baseurl"] + @"/issues/?jql=text%20~%20%22" + projectModel.FileNumber + @"%22";

			projectModel.HasCPRVJiraForFile = _issueService.HasCPRVJirasForFile(id);
			projectModel.TestingTypes = projectJurisdictions.Select(x => x.SubmissionTestingType).Distinct().ToList();

			if (projectModel.OrgTargetDate != null)
			{
				var targetOH =
					_miscService
						.AddBusinessDaysToDate(
							(int)projectModel.TotalOHDays,
							Convert.ToDateTime(projectModel.OrgTargetDate),
							(int)ComUtil.GetLaboratoryForCalculatedDateHolidayList(project.CertificationLab, project.Lab)
						);
				if (projectModel.OrgTargetDate != null)
				{
					projectModel.TargetDateWithOH = targetOH;
				}
			}

			//Complexity Rating
			if (projectModel.IsTransfer != true)
			{
				var item = _dbSubmission.FileNumberComplexityRating.Where(x => x.FileNumber == project.FileNumber).SingleOrDefault();
				if (item != null)
				{
					if (item.ComplexityRatingId > 0)
					{
						projectModel.ComplexityRating = _dbSubmission.tbl_lst_ComplexityRating.Where(x => x.Id == item.ComplexityRatingId).Select(x => x.Rating).SingleOrDefault();
					}
					projectModel.ProductType = (item.ProductTypeId > 0) ? _dbSubmission.tbl_lst_ProductType.Where(x => x.Id == item.ProductTypeId).Select(x => x.Name).SingleOrDefault() : null;
				}
			}

			projectModel.WorkOrderNumbers = projectJurisdictions
				.Where(x => !string.IsNullOrWhiteSpace(x.MasterBillingProject))
				.Select(x => x.MasterBillingProject)
				.Distinct()
				.ToList();

			var resultsQuote = project.QuotedHours.Where(x => x.DeactivateDate == null).OrderByDescending(x => x.AddDate).FirstOrDefault();
			projectModel.QuotedHours = (resultsQuote != null) ? Convert.ToDouble(resultsQuote.Hours) : 0;

			//Resubmission or Masterfile
			if (projectModel.IsTransfer == false)
			{
				projectModel.FileResubmission = _queryService.ProjectResubmission(project.Id).FirstOrDefault();
				if (projectModel.FileResubmission != null)
				{
					projectModel.ResubmissionProjectId = _dbSubmission.trProjects.Where(x => x.FileNumber == projectModel.FileResubmission.MasterFile).Select(x => x.Id).FirstOrDefault();
					if (projectModel.FileResubmission.IsContinuation == false && projectModel.FileResubmission.IsResubmission == false && projectModel.FileResubmission.IsPreCertification == false)
					{
						projectModel.IsMasterFile = _dbSubmission.Submissions.Where(x => x.MasterFileNumber == project.FileNumber && (x.IsResubmission == true || x.IsContinuation == true || x.IsPreCertification == true)).Any();
					}
				}
				else
				{
					projectModel.IsMasterFile = _dbSubmission.Submissions.Where(x => x.MasterFileNumber == project.FileNumber && (x.IsResubmission == true || x.IsContinuation == true || x.IsPreCertification == true)).Any();
				}
			}

			//All Associated Projects
			projectModel.AssociatedProjects = _queryService.ProjectAssociation(project.Id).ToList();

			//All Merged Projects
			projectModel.MergedProjects = _projectService.ProjectMerged(project.Id).ToList();

			//Foreign Tasks
			projectModel.HasForeignHours = (project.ForeignHours.Count > 0) ? true : false;

			var enDateModel = new DepartmentDate
			{
				Accepted = project.ENAcceptDate,
				EstimateCompleted = project.ENEstimateCompleteDate,
				Completed = project.ENCompleteDate,
				EstimateTestCompleted = project.ENEstimateTestCompleteDate,
				TestCompleted = testCompleteDate
			};

			var mathDateModel = new DepartmentDate
			{
				Completed = project.MACompleteDate,
				EstimateCompleted = project.MAEstimateCompleteDate
			};

			var qaDateModel = new DepartmentDate
			{
				Accepted = project.QAAcceptDate,
				EstimateCompleted = project.QAEstimateCompleteDate,
				Completed = (bundle != null) ? bundle.QAFLCompleted : null
			};

			//SLA check if manuf belongs to group
			var allowSLA = (from m in _dbSubmission.tbl_lst_fileManuManufacturerGroups
							join mg in _dbSubmission.ManufacturerGroups on m.ManufacturerGroupId equals mg.Id
							where mg.Description == CustomManufacturerGroup.ARI.ToString() && m.ManufacturerCode == projectModel.Manufacturer
							select m.ManufacturerCode).Any();
			projectModel.ViewSLA = allowSLA;

			//CryptographicallyStrong for fileType RN
			var allowSetCryptographic = (submission.Type == SubmissionType.RN.ToString());
			projectModel.ViewCryptographicStrength = allowSetCryptographic;

			//Display CryptographicStrengh if set
			projectModel.CryptographicId = submission.CryptographicallyStrongId;
			projectModel.CryptographicText = (submission.CryptographicallyStrong == null) ? null : submission.CryptographicallyStrong.Description;

			projectModel.Reviews = project.ProjectReviews.ToList();

			var vm = new DetailViewModel();
			vm.Project = projectModel;
			vm.Project.ENDate = enDateModel;
			vm.Project.MathDate = mathDateModel;
			vm.Project.QADate = qaDateModel;
			vm.UserDeptId = (_userContext.DepartmentId != (int)Department.QA) ? (int)Department.ENG : _userContext.DepartmentId;
			vm.protrackType = (Session["Protrack"] != null) ? (ProtrackType)Enum.Parse(typeof(ProtrackType), Session["Protrack"].ToString()) : ProtrackType.ENG;
			vm.IncomingReview = new IncomingReviewModel
			{
				Created = project.AddDate,
				Completed = project.IncomingReviewCompleteDate,
				Required = project.AddDate >= Convert.ToDateTime(ConfigurationManager.AppSettings["incoming.review.livedate"], new System.Globalization.CultureInfo("en-US")),
				CompletedBy = project.IncomingReviewCompletedByUser == null ? "" : project.IncomingReviewCompletedByUser.FName + " " + project.IncomingReviewCompletedByUser.LName
			};

			//CP review Task in sync
			if (project.BundleTypeId == (int)BundleType.OriginalTesting)
			{
				var isInSync = _projectService.CPReviewTasksEstimateForJurisdictionsIsInSync(id);
				vm.CPReviewTasksNeedsSync = isInSync;
			}
			else
			{
				vm.CPReviewTasksNeedsSync = true;
			}

			var jurisdictionStatus = _dbSubmission.trProjectJurisdictionalData.Where(x => x.ProjectId == id).Select(x => x.JurisdictionalData.Status).ToList();

			//Allow to close

			var isAnyOpenJur = jurisdictionStatus.Any(x => x.Contains(JurisdictionStatus.RA.ToString()) || x.Contains(JurisdictionStatus.OH.ToString()) || x.Contains(JurisdictionStatus.CP.ToString()));
			if (project.BundleType.Id == (int)BundleType.OriginalTesting && !isAnyOpenJur && project.ENStatus != ProjectStatus.DN.ToString())
			{
				vm.AllowToClose = true;
			}
			if (project.BundleType.Id == (int)BundleType.ClosedTesting && bundle.QAStatus == BundleStatus.DN.ToString() && !isAnyOpenJur && project.ENStatus != ProjectStatus.DN.ToString())
			{
				vm.AllowToClose = true;
			}

			//Allow to Reopen
			vm.AllowToReopen = Util.HasAccess(Permission.ReopenProject, AccessRight.Edit) && project.ENStatus == ProjectStatus.DN.ToString() && jurisdictionStatus.Any(x => x.Contains(JurisdictionStatus.RA.ToString()));


			CustomManufacturer customManufacturer;
			if (Enum.TryParse(submission.ManufacturerCode, out customManufacturer))
			{
				vm.ShowGameSubmissionType = ComUtil.GameSubmissionTypeManufacturers().Any(x => x == (CustomManufacturer)Enum.Parse(typeof(CustomManufacturer), submission.ManufacturerCode));
			}

			return View(vm);
		}

		public ViewResult TaskHoursDetail(int projectId, int departmentId)
		{
			var results = _queryService.ProjectUnassignedTaskHours(projectId).ToList();
			var vm = new TaskHoursViewModel();
			vm.ProjectId = projectId;

			vm.Billable = results.Where(x => x.BillType == BillType.Billable.ToString() && x.IsAssigned == false).ToList();
			vm.NonBillable = results.Where(x => x.BillType == BillType.NonBillable.ToString() && x.IsAssigned == false).ToList();
			vm.NoCharge = results.Where(x => x.BillType == BillType.NoCharge.ToString() && x.IsAssigned == false).ToList();
			vm.TotalActualAssigned = results.Where(x => x.IsAssigned == true).ToList().Sum(x => x.Hours);
			vm.TotalActualBillable = results.Where(x => x.BillType == BillType.Billable.ToString() && x.IsAssigned == false).ToList().Sum(x => x.Hours);
			vm.TotalActualNonBillable = results.Where(x => x.BillType == BillType.NonBillable.ToString() && x.IsAssigned == false).ToList().Sum(x => x.Hours);
			vm.TotalActualNoCharge = results.Where(x => x.BillType == BillType.NoCharge.ToString() && x.IsAssigned == false).ToList().Sum(x => x.Hours);
			vm.TotalActual = vm.TotalActualAssigned + vm.TotalActualBillable;

			var query = _dbSubmission.trTasks.Where(x => x.ProjectId == projectId);
			if (departmentId != (int)Department.QA)
			{
				query = query.Where(x => x.DepartmentId != (int)Department.QA);
			}
			else
			{
				query = query.Where(x => x.DepartmentId == (int)Department.QA);
			}
			var estHrs = query.GroupBy(x => x.ProjectId).Select(g => g.Sum(x => x.EstimateHours)).SingleOrDefault();
			vm.TotalEstAssigned = estHrs;

			return View("_TaskHoursDetail", vm);
		}

		public ActionResult Unaccept(int projectId)
		{
			_projectService.UpdateENStatus(projectId, ProjectStatus.UU);
			return RedirectToAction("Detail", new { id = projectId });
		}

		public ActionResult Take(int projectId)
		{
			var transferTo = _userContext.User.Id;
			if (!Util.HasAccess(Role.ENS, Role.MAS, Role.CPS, Role.MG))
			{
				transferTo = (int)_userContext.ManagerId;
			}
			_projectService.TransferInEngineering(projectId, transferTo);
			return RedirectToAction("Detail", new { id = projectId });
		}

		[AuthorizeUser(Permission = Permission.ReopenProject, HasAccessRight = AccessRight.Edit)]
		public ActionResult ReopenProject(int projectId)
		{
			_projectService.ReopenProject(projectId);

			return RedirectToAction("Detail", new { id = projectId });
		}
		#endregion

		#region Incoming
		[HttpGet]
		public ActionResult IncomingReview(int projectId, string protrackType)
		{
			var vm = new ProjectReview();
			var project = _dbSubmission.trProjects.Where(x => x.Id == projectId).SingleOrDefault();
			vm.ProjectId = project.Id;
			vm.ProjectName = project.ProjectName;
			vm.ReviewType = ReviewType.InComing;
			vm.ProtrackType = (ProtrackType)Enum.Parse(typeof(ProtrackType), protrackType);

			//SLA check if manuf belongs to group
			var manufacturer = _dbSubmission.Submissions.Where(x => x.FileNumber == project.FileNumber).Select(x => x.ManufacturerCode).First();
			var allowSLA = (from m in _dbSubmission.tbl_lst_fileManuManufacturerGroups
							join mg in _dbSubmission.ManufacturerGroups on m.ManufacturerGroupId equals mg.Id
							where mg.Description == CustomManufacturerGroup.ARI.ToString() && m.ManufacturerCode == manufacturer
							select m.ManufacturerCode).Any();
			vm.AllowSLA = allowSLA;

			return View("ProjectReview", vm);
		}
		#endregion

		#region QuickDetail
		public JsonResult GetUnassignedHours(int id)
		{
			//Unassigned Hours
			var results = _queryService.ProjectUnassignedTaskHours(id).ToList();
			var TotalUnassignedBillable = results.Where(x => x.BillType == BillType.Billable.ToString() && x.IsAssigned == false).ToList().Sum(x => x.Hours);
			var TotalNonBillable = results.Where(x => x.BillType == BillType.NonBillable.ToString()).ToList().Sum(x => x.Hours);
			var TotalUnassignedNoCharge = results.Where(x => x.BillType == BillType.NoCharge.ToString() && x.IsAssigned == false).ToList().Sum(x => x.Hours);

			var result = new
			{
				UnExpectedHours = TotalUnassignedBillable,
				NonBillable = TotalNonBillable
			};
			return Json((result), JsonRequestBehavior.AllowGet);
		}

		public ActionResult QuickDetail(int id)
		{
			var vm = new QuickDetailViewModel();
			vm.ProjectId = id;

			//Project Info
			var project = _dbSubmission.trProjects.Where(x => x.Id == id).Single();
			vm.ProjectName = project.ProjectName;
			vm.ENEstCompleteDate = project.ENEstimateCompleteDate;
			vm.MAEstCompleteDate = project.MAEstimateCompleteDate;
			vm.IsEstMathCompleteDateToBeDetermined = project.IsTBDEstMathCompleteDate;
			vm.EstTestCompleteDate = project.ENEstimateTestCompleteDate;
			vm.QAAcceptDate = project.QAAcceptDate;
			vm.FileNumber = project.FileNumber;
			vm.ProjectStatus = project.ENStatus;
			vm.OnHold = project.OnHold;
			vm.TargetDate = project.TargetDate;

			var estCompleteDate = project.ENEstimateTestCompleteDate;
			if (project.MAEstimateCompleteDate != null && project.MAEstimateCompleteDate > project.ENEstimateTestCompleteDate)
			{
				estCompleteDate = project.MAEstimateCompleteDate;
			}
			if (estCompleteDate != null)
			{
				estCompleteDate =
					_miscService
						.AddBusinessDaysToDate(
							3,
							Convert.ToDateTime(estCompleteDate),
							(int)ComUtil.GetLaboratoryForCalculatedDateHolidayList(project.CertificationLab, project.Lab)
						);
			}

			vm.EstCompleteDate = estCompleteDate; //Later of Math or Test 

			if (project.OnHold == 0)
			{
				vm.IsTargetOnTrack = (project.TargetDate > estCompleteDate) ? true : false;
			}

			//Unassigned Hours
			vm.UnExpectedHours = 0;

			//Submission Info
			var projectJur = _dbSubmission.trProjectJurisdictionalData.Where(x => x.ProjectId == id).FirstOrDefault();

			var subDetail = new SubmisionQuickDetail();
			if (projectJur != null)
			{
				subDetail.ContractType = projectJur.JurisdictionalData.ContractType.Code;
				subDetail.TestLab = projectJur.Project.Lab;
				subDetail.OwningEntity = projectJur.JurisdictionalData.Submission.Company.Name;
				subDetail.ArchiveLocation = projectJur.JurisdictionalData.Submission.ArchiveLocation;
				subDetail.CertLab = projectJur.JurisdictionalData.Submission.CertificationLab;
				subDetail.Currency = projectJur.JurisdictionalData.Submission.Currency.Code;
			}
			vm.SubmissionDetail = subDetail;
			vm.IncomingReview = new IncomingReviewModel
			{
				Created = project.AddDate,
				Completed = project.IncomingReviewCompleteDate,
				Required = project.AddDate >= Convert.ToDateTime(ConfigurationManager.AppSettings["incoming.review.livedate"]),
				CompletedBy = project.IncomingReviewCompletedByUser == null ? "" : project.IncomingReviewCompletedByUser.FName + " " + project.IncomingReviewCompletedByUser.LName
			};

			return PartialView("_QuickDetail", vm);
		}
		#endregion

		#region Project Grid Export
		[HttpPost]
		public void Export(string excelData, string fileName, ExportType exportType, string title, string subTitle, string projectIds, string exportOptions = null)
		{
			//Add extra columns
			var lstExportOptions = exportOptions.Split(',').ToList();
			if (lstExportOptions.Count > 0)
			{
				var ids = projectIds.Split(',').Select(int.Parse).ToList();
				excelData = AddExportOptionsData(excelData, ids, lstExportOptions);
			}
			var dt1 = ConvertCsvData(excelData);

			var export = new Export
			{
				ExportType = exportType,
				FiltersToolbar = "",
				SavedColumns = "",
				Title = (title + " " + subTitle).TrimEnd()
			};
			var grid = new ProjectGrid();
			grid.ExportCustomData(export, dt1);
		}

		private string AddExportOptionsData(string CSVdata, List<int> ids, List<string> exportOptions = null)
		{
			var excelData = "";
			string[] Lines = CSVdata.Split(new char[] { '\r', '\n' });
			string[] HeaderText = Lines[0].Split('\t');
			int numOfColumns = HeaderText.Count();

			//Add Header
			var LatestOHNoteExist = exportOptions.Contains(GlimpseDownloadOptions.LatestOHNote.ToString());
			//EN data
			var LatestNoteExist = exportOptions.Contains(GlimpseDownloadOptions.LatestProjectNote.ToString());
			var TasksExist = exportOptions.Contains(GlimpseDownloadOptions.Tasks.ToString());
			//QA Data
			var LatestQANoteExist = exportOptions.Contains(GlimpseDownloadOptions.LatestQAProjectNote.ToString());
			var QATasksExist = exportOptions.Contains(GlimpseDownloadOptions.QATasks.ToString());

			//OH Note
			if (LatestOHNoteExist)
			{
				numOfColumns += 1;
				Lines[0] += "\t" + GlimpseDownloadOptions.LatestOHNote.GetEnumDescription();
				var ohNotes = (from n in _dbSubmission.trOH_Histories
							   where ids.Contains((int)n.ProjectId) && n.Reason != null && n.Reason != ""
							   group n by n.ProjectId
								   into groups
							   select groups.OrderByDescending(p => p.StartDate).FirstOrDefault()).ToList();

				for (int i = 1; i < Lines.GetLength(0); i++)
				{
					string[] Fields = Lines[i].Split('\t');
					if (Fields.GetLength(0) > 1)
					{
						var projectId = Convert.ToInt32(Fields[0]);
						var projectOHNote = ohNotes.Where(x => x.ProjectId == projectId).Select(x => x.Reason).SingleOrDefault();
						Lines[i] += "\t" + ((projectOHNote == null) ? "" : projectOHNote);
					}
				}
			}

			//EN Note
			if (LatestNoteExist)
			{
				numOfColumns += 1;
				Lines[0] += "\t" + GlimpseDownloadOptions.LatestProjectNote.GetEnumDescription();
				var Notes = (from n in _dbSubmission.trProjectNotes
							 where ids.Contains((int)n.ProjectId) && n.Note != null && n.Note != ""
							 group n by n.ProjectId
								 into groups
							 select groups.OrderByDescending(p => p.AddDate).FirstOrDefault()).ToList();

				for (int i = 1; i < Lines.GetLength(0); i++)
				{
					string[] Fields = Lines[i].Split('\t');
					if (Fields.GetLength(0) > 1)
					{
						var projectId = Convert.ToInt32(Fields[0]);
						var projectNote = Notes.Where(x => x.ProjectId == projectId).Select(x => x.Note).SingleOrDefault();
						Lines[i] += "\t" + ((projectNote == null) ? "" : projectNote);
					}
				}
			}
			else if (LatestQANoteExist) //QA Note
			{
				numOfColumns += 1;
				Lines[0] += "\t" + GlimpseDownloadOptions.LatestQAProjectNote.GetEnumDescription();
				var Notes = ((from n in _dbSubmission.QABundles_Notes
							  join q in _dbSubmission.QABundles on n.BundleId equals q.Id
							  where ids.Contains((int)q.ProjectId) && n.Note != null && n.Note != ""
							  group new { n, q } by q.ProjectId
								  into groups
							  select groups.OrderByDescending(p => p.n.TimeStamp).FirstOrDefault()).Select(x => new { ProjectId = x.q.ProjectId, Note = x.n.Note })).ToList();

				for (int i = 1; i < Lines.GetLength(0); i++)
				{
					string[] Fields = Lines[i].Split('\t');
					if (Fields.GetLength(0) > 1 && !string.IsNullOrEmpty(Fields[0]))
					{
						var projectId = Convert.ToInt32(Fields[0]);
						var qaProjectNote = Notes.Where(x => x.ProjectId == projectId).Select(x => x.Note).SingleOrDefault();
						Lines[i] += "\t" + ((qaProjectNote == null) ? "" : qaProjectNote);
					}
				}
			}

			//EN tasks
			if (TasksExist)
			{
				List<string> newLines = new List<string>();
				var appendCol = "\t" + "Task\t" + "Description\t" + "Engineer\t" + "IsAssigned\t" + "BillType\t" + "Est. Hrs\t" + "Act. Hrs\t" + "Start\t" + "Complete";
				newLines.Add(Lines[0] + appendCol);

				var query = "exec [GetQuickDetailTaskInfo] '" + string.Join(",", ids) + "'";
				var allTasks = _dbSubmission.Database.GetDbConnection().Query<TasksGridRecord>(query).ToList();
				for (int i = 1; i < Lines.GetLength(0); i++)
				{
					string[] Fields = Lines[i].Split('\t');
					if (Fields.GetLength(0) > 1)
					{
						var projectId = Convert.ToInt32(Fields[0]);

						var projectTasks = allTasks.Where(x => x.ProjectId == projectId).ToList();

						newLines.Add(Lines[i] + "\t" + "\t" + "\t" + "\t" + "\t" + "\t" + "\t" + "\t" + "\t" + "");

						for (int r = 0; r < projectTasks.Count; r++)
						{
							var appendRow = "";
							for (int c = 0; c < numOfColumns; c++)
							{
								appendRow += "\t";
							}
							appendRow += projectTasks[r].Name + "\t";
							appendRow += projectTasks[r].Description + "\t";
							appendRow += projectTasks[r].User + "\t";
							appendRow += projectTasks[r].IsAssigned + "\t";
							appendRow += projectTasks[r].BillType + "\t";
							appendRow += projectTasks[r].EstimateHours + "\t";
							appendRow += projectTasks[r].ActualHours + "\t";
							appendRow += projectTasks[r].StartDate + "\t";
							appendRow += projectTasks[r].CompleteDate;
							newLines.Add(appendRow);
						}
					}
				}
				Lines = newLines.ToArray();

			}
			else if (QATasksExist) //QA Tasks
			{
				List<string> newLines = new List<string>();
				var appendCol = "\t" + "Task\t" + "Engineer\t" + "Start\t" + "Complete";
				newLines.Add(Lines[0] + appendCol);

				var allTasks = (from t in _dbSubmission.QABundles_Task
								join q in _dbSubmission.QABundles on t.BundleID equals q.Id
								where ids.Contains((int)q.ProjectId) && t.tfCurrent == true
								select new
								{
									ProjectId = q.ProjectId,
									Name = t.Definition.TaskDefDesc,
									User = t.User.FName + " " + t.User.LName,
									StartDate = t.StartDate,
									CompleteDate = t.CompleteDate
								}).ToList();

				for (int i = 1; i < Lines.GetLength(0); i++)
				{
					string[] Fields = Lines[i].Split('\t');
					if (Fields.GetLength(0) > 1)
					{
						var projectId = Convert.ToInt32(Fields[0]);

						var projectTasks = allTasks.Where(x => x.ProjectId == projectId && (x.User != "" && x.User != null)).ToList();

						newLines.Add(Lines[i] + "\t" + "\t" + "\t" + "\t" + "");

						for (int r = 0; r < projectTasks.Count; r++)
						{
							var appendRow = "";
							for (int c = 0; c < numOfColumns; c++)
							{
								appendRow += "\t";
							}
							appendRow += projectTasks[r].Name + "\t";
							appendRow += projectTasks[r].User + "\t";
							appendRow += projectTasks[r].StartDate + "\t";
							appendRow += projectTasks[r].CompleteDate;
							newLines.Add(appendRow);
						}
					}
				}
				Lines = newLines.ToArray();
			}

			excelData = string.Join("\r\n", Lines);

			return excelData;
		}

		private System.Data.DataTable ConvertCsvData(string CSVdata)
		{
			//  Convert a tab-separated set of data into a DataTable, ready for our C# CreateExcelFile libraries
			//  to turn into an Excel file.
			var dt = new System.Data.DataTable();
			try
			{
				System.Diagnostics.Trace.WriteLine(CSVdata);

				string[] Lines = CSVdata.Split(new char[] { '\r', '\n' });
				if (Lines == null)
					return dt;
				if (Lines.GetLength(0) == 0)
					return dt;

				string[] HeaderText = Lines[0].Split('\t');
				int numOfColumns = HeaderText.Count();

				//get Header values 
				var header = "";
				for (int f = 1; f < numOfColumns; f++) //Start with 1 because 0 is ID
				{
					header = HeaderText[f];
					dt.Columns.Add(header, typeof(string));
				}

				System.Data.DataRow Row;
				for (int i = 1; i < Lines.GetLength(0); i++)
				{
					string[] Fields = Lines[i].Split('\t');
					if (Fields.GetLength(0) == numOfColumns)
					{
						Row = dt.NewRow();
						for (int f = 1; f < numOfColumns; f++) //Start with 1 because 0 is ID
						{
							Row[f - 1] = Fields[f];
						}
						dt.Rows.Add(Row);
					}
				}

				return dt;
			}
			catch (Exception ex)
			{
				System.Diagnostics.Trace.WriteLine("An exception occurred: " + ex.Message);
				return null;
			}
		}
		#endregion

		#region Excel Export
		public ActionResult Download(int projectId)
		{
			var submissionIds = _dbSubmission.trProjectJurisdictionalData
								.Where(x => x.ProjectId == projectId
										&& (x.JurisdictionalData.Status != JurisdictionStatus.RJ.ToString() && x.JurisdictionalData.Status != JurisdictionStatus.WD.ToString()))
								.Select(x => x.JurisdictionalData.SubmissionId).Distinct().ToList();
			var fileNumber = new FileNumber(_dbSubmission.trProjects.Where(x => x.Id == projectId).Select(x => x.FileNumber).Single());

			var zipStream = new MemoryStream();
			using (var zip = new ZipArchive(zipStream, ZipArchiveMode.Create, true))
			{
				var componentsSpreadsheet = _lqExportService.CreateComponentsSpreadsheet(projectId, fileNumber, submissionIds);
				if (componentsSpreadsheet.Length > 0)
				{
					var componentEntry = zip.CreateEntry("component.xlsx", CompressionLevel.Optimal);
					using (var componentEntryStream = componentEntry.Open())
					{
						componentsSpreadsheet.Seek(0, SeekOrigin.Begin);
						componentsSpreadsheet.CopyTo(componentEntryStream);
					}
				}

				var paytableSpreadsheet = _lqExportService.CreatePaytableSpreadsheet(submissionIds);
				if (paytableSpreadsheet.Length > 0)
				{
					var paytableEntry = zip.CreateEntry("payTable.xlsx", CompressionLevel.Optimal);
					using (var paytableEntryStream = paytableEntry.Open())
					{
						paytableSpreadsheet.Seek(0, SeekOrigin.Begin);
						paytableSpreadsheet.CopyTo(paytableEntryStream);
					}
				}

				var issueSpreadsheet = _lqExportService.CreateJiraSpreadsheet(submissionIds);
				if (issueSpreadsheet.Length > 0)
				{
					var issueEntry = zip.CreateEntry("issueList.xlsx", CompressionLevel.Optimal);
					using (var issueEntryStream = issueEntry.Open())
					{
						issueSpreadsheet.Seek(0, SeekOrigin.Begin);
						issueSpreadsheet.CopyTo(issueEntryStream);
					}
				}
			}

			zipStream.Seek(0, SeekOrigin.Begin);
			return new FileStreamResult(zipStream, "application/zip") { FileDownloadName = "LQ Deliverables.zip" };
		}
		#endregion

		#region Misc info
		public JsonResult CheckFilesMissingTarget(string projectIds)
		{
			var projectList = projectIds.Split(',').Select(int.Parse).ToList();
			var fileNumbers = _dbSubmission.trProjects.Where(t => projectList.Contains(t.Id)).Where(x => x.OrgTargetDate == null).Select(x => x.ProjectName).ToList();
			return Json(fileNumbers, JsonRequestBehavior.AllowGet);
		}

		//public ContentResult GetBundleType(int projectId)
		//{
		//	var sType = _projectService.GetBundleType(projectId);
		//	return Content(sType);
		//}

		//public ContentResult GetProjectIndicator(int projectId)
		//{
		//	var indicator = _projectService.GetProjectIndicator(projectId);
		//	return Content(indicator);
		//}

		public ContentResult GetGPSPath(string fileNumber)
		{
			var path = _documentService.GetFilePath(fileNumber);
			return Content(path);
		}

		//public ContentResult GetJiraOpen(int projectId)
		//{
		//	var results = _issueService.GetIssueCount(projectId, ComUtil.OpenIssueStatuses());
		//	var issueCount = results.Results.Where(x => x.ProjectId == projectId).FirstOrDefault().Count;
		//	return Content(issueCount.ToString());
		//}

		//public ContentResult GetJiraClosed(int projectId)
		//{
		//	var results = _issueService.GetIssueCount(projectId, ComUtil.ClosedIssueStatuses());
		//	var issueCount = results.Results.Where(x => x.ProjectId == projectId).FirstOrDefault().Count;
		//	return Content(issueCount.ToString());
		//}

		public JsonResult SyncRatingWithEResults(int projectId)
		{
			var fileNumber = _dbSubmission.trProjects.Where(x => x.Id == projectId).Select(x => x.FileNumber).Single();
			_miscService.SyncRatingWithEresults(fileNumber);

			var item = _dbSubmission.FileNumberComplexityRating.Where(x => x.FileNumber == fileNumber).SingleOrDefault();
			if (item != null)
			{
				var results = new
				{
					Rating = _dbSubmission.tbl_lst_ComplexityRating.Where(x => x.Id == item.ComplexityRatingId).Select(x => x.Rating).SingleOrDefault(),    //((ComplexityRatingType)item.ComplexityRatingId).GetEnumDescription(),					
					ProductType = _dbSubmission.tbl_lst_ProductType.Where(x => x.Id == item.ProductTypeId).Select(x => x.Name).SingleOrDefault() //((ProductType)item.ProductTypeId).GetEnumDescription()
				};
				return Json(results, JsonRequestBehavior.AllowGet);
			}
			else
			{
				return Json(string.Empty, JsonRequestBehavior.AllowGet);
			}
		}

		public ActionResult SyncCPReviewTasks(int projectId)
		{
			_projectService.CPReviewTasksAdd(projectId);
			return RedirectToAction("Detail", new { id = projectId });
		}
		public ActionResult CloseProject(int projectId)
		{
			_projectService.CompleteProject(projectId);
			return RedirectToAction("Detail", new { id = projectId });
		}

		#endregion

		#region Bundle Actions(QA)
		public ContentResult SetQABundletoRC(List<int> projectIds)
		{
			var nonIR = _dbSubmission.QABundles.Where(x => projectIds.Contains((int)x.ProjectId) && x.QAStatus != BundleStatus.IR.ToString()).Select(x => x.Id).ToList();
			if (nonIR.Count > 0)
			{
				return Content("NOOK");
			}
			else
			{
				foreach (var projectId in projectIds)
				{
					var bundleId = _dbSubmission.QABundles.Where(x => x.ProjectId == projectId).Select(x => x.Id).SingleOrDefault();
					_projectService.UpdateQAStatus(bundleId, BundleStatus.FL);
				}
				return Content("OK");
			}
		}

		public ActionResult UnacceptInQA(int projectId)
		{
			_projectService.UnacceptInQA(projectId);
			return RedirectToAction("Detail", new { id = projectId });
		}

		public ActionResult AcceptInQA(int projectId)
		{
			_projectService.AcceptProjectInQA(projectId, _userContext.User.Id);
			return RedirectToAction("Detail", new { id = projectId });
		}

		//public ActionResult PullInQA(int projectId)
		//{
		//	_projectService.PullBundleInQA(projectId);
		//	return RedirectToAction("Detail", new { id = projectId });
		//}

		//public ActionResult PullInQAAndAccept(int projectId)
		//{
		//	_projectService.PullBundleInQA(projectId);
		//	_projectService.AcceptProjectInQA(projectId, _userContext.User.Id);
		//	return RedirectToAction("Detail", new { id = projectId });
		//}

		public ActionResult CheckProjectsBundleTypes(string projectIds)
		{
			var projectList = projectIds.Split(',').Select(Int32.Parse).ToList();
			var checkBundleTypes = (from p in _dbSubmission.trProjects
									join q in _dbSubmission.QABundles on p.Id equals q.ProjectId
									where projectList.Contains(p.Id)
									select new
									{
										projectIds = p.Id,
										IsResubmission = (p.BundleTypeId == (int)BundleType.ClosedTesting || q.ReSubmission == true) ? true : false,
										Status = q.QAStatus
									}).ToList();
			var bundleTypeCount = checkBundleTypes.Select(x => x.IsResubmission).Distinct().Count();
			var isBundleCompleted = checkBundleTypes.Exists(x => (x.Status == BundleStatus.DN.ToString()));

			if (bundleTypeCount > 1 || isBundleCompleted)
			{
				return Content("NoOK");
			}
			else
			{
				var isResubmission = checkBundleTypes.Select(x => x.IsResubmission).FirstOrDefault();
				return Content((isResubmission) ? BundleType.ClosedTesting.ToString() : BundleType.OriginalTesting.ToString());
			}
		}

		public ActionResult UpdateQAToStatus(int projectId, BundleStatus newStatus)
		{
			var bundleId = _dbSubmission.QABundles.Where(x => x.ProjectId == projectId).Select(x => x.Id).Single();
			if (newStatus == BundleStatus.DL)
			{
				var alltasks = _dbSubmission.QABundles_Task.Where(x => x.BundleID == bundleId && x.tfCurrent == true).ToList();
				foreach (var task in alltasks)
				{
					task.CompleteDate = null;
				}
				_projectService.UpdateBundleTasks(alltasks);
			}
			else
			{
				_projectService.UpdateQAStatus(bundleId, newStatus);
			}
			return RedirectToAction("Detail", new { id = projectId });
		}

		public ContentResult SetQFINum(int projectId, string ncrNum)
		{
			var bundle = _dbSubmission.QABundles.Where(x => projectId == ((int)x.ProjectId)).SingleOrDefault();
			bundle.NCRNumber = ncrNum;
			_dbSubmission.QABundles.Attach(bundle);
			_dbSubmission.Entry(bundle).Property(x => x.NCRNumber).IsModified = true;
			_dbSubmission.SaveChanges();
			return Content("OK");
		}
		public ContentResult SetQAOffice(int projectId, int qaOfficeId)
		{
			var project = _dbSubmission.trProjects.Where(x => x.Id == projectId).SingleOrDefault();
			project.QAOfficeId = qaOfficeId;
			_dbSubmission.SaveChanges();
			return Content("OK");
		}

		public ContentResult SetADMWork(int projectId, bool admWork)
		{
			var bundle = _dbSubmission.QABundles.Where(x => projectId == ((int)x.ProjectId)).SingleOrDefault();
			bundle.ADMWorkNeeded = admWork;
			_dbSubmission.QABundles.Attach(bundle);
			_dbSubmission.Entry(bundle).Property(x => x.ADMWorkNeeded).IsModified = true;
			_dbSubmission.UserId = _userContext.User.Id;
			_dbSubmission.SaveChanges();
			return Content("OK");
		}

		public ContentResult SaveENInvolvement(int projectId, bool isNeeded, string notes)
		{
			var bundle = _dbSubmission.QABundles.Where(x => x.ProjectId == projectId).SingleOrDefault();
			bundle.ENInvolvementWhileFL = isNeeded;
			bundle.ENInvolvementWhileFLNote = notes;
			_dbSubmission.QABundles.Attach(bundle);
			_dbSubmission.Entry(bundle).Property(x => x.ENInvolvementWhileFL).IsModified = true;
			_dbSubmission.Entry(bundle).Property(x => x.ENInvolvementWhileFLNote).IsModified = true;
			_dbSubmission.SaveChanges();

			//Add to history
			var history = new trHistory
			{
				ProjectId = projectId,
				AddDate = DateTime.Now,
				UserId = _userContext.User.Id,
				Notes = null
			};

			if (bundle.ENInvolvementWhileFL)
			{
				history.Notes = "Additional Engineering Involvement Required Flag Set.";

				if (!string.IsNullOrEmpty(bundle.ENInvolvementWhileFLNote))
				{
					history.Notes = history.Notes + " Reason: " + bundle.ENInvolvementWhileFLNote;
				}
			}
			else
			{
				history.Notes = "Additional Engineering Involvement Required Flag Removed.";
			}

			_logService.AddProjectHistory(history);

			//Send an email
			if (isNeeded == false && _userContext.DepartmentId == (int)Department.ENG)
			{
				var bundleInfo = _dbSubmission.QABundles.Where(x => x.ProjectId == projectId).Select(x => new { Id = x.Id, Reviewer = x.QAReviewerId, Name = x.ProjectName }).FirstOrDefault();
				var emailToUserIds = new List<int>();
				if (bundleInfo.Reviewer != null)
				{
					emailToUserIds.Add((int)bundleInfo.Reviewer);
				}
				var allAssigned = _dbSubmission.QABundles_Task.Where(x => x.BundleID == bundleInfo.Id).Select(x => x.UserId).Distinct().ToList();
				emailToUserIds.AddRange(allAssigned);

				var allEmails = _dbSubmission.trLogins.Where(x => emailToUserIds.Contains(x.Id) && x.Email != null).Select(x => x.Email).ToList();

				if (allEmails.Count > 0)
				{
					var mailMsg = new MailMessage();
					foreach (var email in allEmails)
					{
						mailMsg.To.Add(email);
					}
					mailMsg.Subject = bundleInfo.Name + " - Add’l En involvement Needed flag is removed.";
					mailMsg.Body = bundleInfo.Name + " Add’l En involvement Needed flag is removed.";
					_emailService.Send(mailMsg);
				}
			}
			return Content("OK");
		}

		public ActionResult SetNoCertNeeded(bool noCertNeeded, int bundleID)
		{
			var dbQABundle = new QABundle { Id = bundleID, NoCertNeeded = noCertNeeded };
			_dbSubmission.QABundles.Attach(dbQABundle);
			_dbSubmission.Entry(dbQABundle).Property(x => x.NoCertNeeded).IsModified = true;
			_dbSubmission.SaveChanges();

			return Json(noCertNeeded);
		}
		#endregion
	}
}