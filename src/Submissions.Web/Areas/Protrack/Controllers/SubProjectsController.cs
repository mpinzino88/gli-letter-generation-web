﻿using GLI.EFCore.Submission;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using Submissions.Common;
using Submissions.Core;
using Submissions.Web.Areas.Protrack.Models.SubProjects;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace Submissions.Web.Areas.Protrack.Controllers
{
	[AuthorizeUser(Permission = Permission.UKQueue, HasAccessRight = AccessRight.Edit)]
	public class SubProjectsController : Controller
	{
		private readonly SubmissionContext _dbSubmission;
		private readonly IUserContext _userContext;
		private readonly ISubProjectService _subProjectService;

		public SubProjectsController(SubmissionContext dbSubmission, IUserContext userContext, ISubProjectService subProjectService)
		{
			_dbSubmission = dbSubmission;
			_userContext = userContext;
			_subProjectService = subProjectService;
		}

		public ActionResult Index()
		{
			var vm = new IndexViewModel();
			var team = _dbSubmission.tbl_lst_Teams.Where(x => x.Id == _userContext.TeamId).FirstOrDefault();
			vm.SubProjectSearch.TeamIds = _userContext.TeamId == null ? null : new List<int> { (int)_userContext.TeamId };
			if (team != null)
			{
				vm.TeamFilterOptions.Add(new SelectListItem
				{
					Value = team.Id.ToString(),
					Text = team.Code + " - " + team.Description,
					Selected = true
				});
			}
			vm.SubProjectSearch.Statuses.Add(SubProjectStatus.Unassigned.ToString());
			vm.SubProjectSearch.Statuses.Add(SubProjectStatus.NotStarted.ToString());
			vm.SubProjectSearch.Statuses.Add(SubProjectStatus.InProgress.ToString());
			return View(vm);
		}

		[HttpGet]
		public ActionResult GetSubProject(int id)
		{
			var result = _subProjectService.SubProject(id);
			result.Mode = Common.Mode.Edit;
			var json = JsonConvert.SerializeObject(result, Formatting.Indented, new JsonSerializerSettings { ContractResolver = new CamelCasePropertyNamesContractResolver() });
			return Content(json, "application/json");
		}

		[HttpGet]
		public ActionResult LoadTask(int id)
		{
			var result = _subProjectService.SubProjectTask(id);
			var json = JsonConvert.SerializeObject(result, Formatting.Indented, new JsonSerializerSettings { ContractResolver = new CamelCasePropertyNamesContractResolver() });
			return Content(json, "application/json");
		}

		[HttpPost]
		public ActionResult SaveSubProject(string subProject, int projectId) => Json(_subProjectService.SaveSubProject(subProject));

		[HttpPost]
		public ActionResult SaveTask(string task)
		{
			var result = _subProjectService.SaveTask(task);
			var json = JsonConvert.SerializeObject(result, Formatting.Indented, new JsonSerializerSettings { ContractResolver = new CamelCasePropertyNamesContractResolver() });
			return Content(json, "application/json");
		}

		[HttpPost]
		public ActionResult StartTask(string taskId)
		{
			var result = _subProjectService.StartTask(taskId);
			var json = JsonConvert.SerializeObject(result, Formatting.Indented, new JsonSerializerSettings { ContractResolver = new CamelCasePropertyNamesContractResolver() });
			return Content(json, "application/json");
		}

		[HttpPost]
		public ActionResult CompleteTask(string task)
		{
			var result = _subProjectService.CompleteTask(task);
			var json = JsonConvert.SerializeObject(result, Formatting.Indented, new JsonSerializerSettings { ContractResolver = new CamelCasePropertyNamesContractResolver() });
			return Content(json, "application/json");
		}
	}
}