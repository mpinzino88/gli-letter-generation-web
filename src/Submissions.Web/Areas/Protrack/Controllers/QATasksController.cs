﻿using GLI.EFCore.Submission;
using Newtonsoft.Json;
using Submissions.Common;
using Submissions.Core;
using Submissions.Web.Areas.Protrack.Models.QATasks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace Submissions.Web.Areas.Protrack.Controllers
{
	public class QATasksController : Controller
	{
		private readonly SubmissionContext _dbSubmission;
		private readonly IUserContext _userContext;
		private readonly IProjectService _projectService;
		private readonly IQueryService _queryService;

		public QATasksController(SubmissionContext dbSubmission, IUserContext userContext, IProjectService projectService, IQueryService queryService)
		{
			_dbSubmission = dbSubmission;
			_userContext = userContext;
			_projectService = projectService;
			_queryService = queryService;
		}

		public ActionResult Index(int projectId)
		{
			var vm = new IndexViewModel();

			vm.ProjectsIds.Add(projectId.ToString());
			var projectInfo = _dbSubmission.trProjects.Where(x => x.Id == projectId).Select(x => new { Name = x.ProjectName, x.BundleTypeId }).SingleOrDefault();
			var bundleInfo = _dbSubmission.QABundles.Where(x => x.ProjectId == projectId).Select(x => new { BundleId = x.Id, IsResubmission = x.ReSubmission }).Single();
			vm.currentBundleName = projectInfo.Name;
			vm.currentBundleTypeId = (byte)((projectInfo.BundleTypeId == (int)BundleType.ClosedTesting || bundleInfo.IsResubmission == true) ? BundleType.ClosedTesting : BundleType.OriginalTesting);
			vm.ProjectFilter.ProjectType = (vm.currentBundleTypeId == (int)BundleType.ClosedTesting || bundleInfo.IsResubmission == true) ? BundleType.ClosedTesting : BundleType.OriginalTesting;

			var bundleId = _dbSubmission.QABundles.Where(x => x.ProjectId == projectId).Select(x => x.Id).SingleOrDefault();
			var tasks = _dbSubmission.QABundles_Task.Where(x => x.BundleID == bundleId)
						.Select(x =>
							new QATask()
							{
								Id = x.Id,
								TaskDefId = x.TaskDefID,
								TaskDefName = x.Definition.TaskDefDesc,
								TaskDef = new SelectListItem { Value = x.TaskDefID.ToString(), Text = x.Definition.TaskDefDesc },
								UserId = x.UserId,
								UserName = x.User.FName + " " + x.User.LName,
								User = new SelectListItem { Value = x.UserId.ToString(), Text = x.User.FName + " " + x.User.LName },
								TargetDate = x.TargetDate,
								IsInEditMode = (x.tfCurrent == true) ? true : false,
								IsTaskDefInEditMode = false
							}
							).ToList();
			foreach (var task in tasks)
			{
				task.TargetDateString = task.TargetDate.ToString("MM/dd/yyyy");
			}
			vm.Tasks = tasks;

			return View(vm);
		}

		//Batch
		public ActionResult BatchIndex(string bundleType, string projectIds)
		{
			var vm = new IndexViewModel();
			if (!string.IsNullOrEmpty(projectIds))
			{
				var lstProjectIds = projectIds.Split(',').ToList();
				vm.ProjectsIds = lstProjectIds;
				vm.ProjectNames = _dbSubmission.trProjects.Where(i => lstProjectIds.Contains(i.Id.ToString())).Select(x => new SelectListItem() { Value = x.Id.ToString(), Text = x.ProjectName, Selected = true }).ToList();

				//if (vm.ProjectsIds.Count == 1)
				//{
				//vm.currentBundleName = vm.ProjectNames[0].Text;
				//}
			}

			vm.currentBundleTypeId = (byte?)((int)Enum.Parse(typeof(BundleType), bundleType));
			vm.ProjectFilter.ProjectType = (vm.currentBundleTypeId == (int)BundleType.ClosedTesting) ? BundleType.ClosedTesting : BundleType.OriginalTesting;
			vm.BatchMode = true;
			return View("Index", vm);
		}

		[HttpPost]
		public ContentResult Save(string indexViewModel)
		{
			var vm = JsonConvert.DeserializeObject<IndexViewModel>(indexViewModel);
			if (vm.BatchMode == true) //if Task exists and Not Started, update LW, else add new task
			{
				ApplyBatchChanges(vm);
			}
			else
			{
				ApplyChanges(vm);
			}
			return Content("OK");
		}

		#region Removing Tasks
		[HttpPost]
		public JsonResult RemoveTask(QATask task)
		{
			try
			{
				_projectService.DeleteBundleTask(task.Id);
				return Json(JsonConvert.SerializeObject(task));
			}
			catch (Exception ex)
			{
				return Json(JsonConvert.SerializeObject(new { responseText = ex.Message }));
				throw;
			}
		}
		#endregion

		#region TaskUpdates
		private void ApplyBatchChanges(IndexViewModel vm)
		{
			foreach (var projectId in vm.ProjectsIds)
			{
				var iProjectId = Convert.ToInt32(projectId);

				var bundle = _dbSubmission.QABundles.Where(x => x.ProjectId == iProjectId).SingleOrDefault();
				var bundleId = bundle.Id;

				//Save Supervisor & reviewer
				if (vm.ReviewerId > 0 || vm.ReviewerId != null)
				{
					bundle.QAReviewerId = vm.ReviewerId;
				}
				if (vm.SupervisorId > 0 || vm.SupervisorId != null)
				{
					bundle.QASupervisorId = vm.SupervisorId;
				}
				_projectService.UpdateBundle(bundle);

				foreach (var task in vm.Tasks)
				{
					//Tasks
					var ExistedTask = _dbSubmission.QABundles_Task.Where(x => x.BundleID == bundleId && x.tfCurrent == true && x.TaskDefID == task.TaskDefId).ToList();
					if (ExistedTask.Count == 0)
					{
						_projectService.AddBundleTask(new QABundles_Task()
						{
							UserId = task.UserId,
							TaskDefID = task.TaskDefId,
							TargetDate = (task.TargetDate != null) ? task.TargetDate.Value.Date : task.TargetDate,
							tfCurrent = true,
							AddUserId = _userContext.User.Id,
							CreatedTime = DateTime.Now,
							BundleID = bundleId
						});
					}
					else
					{
						if (ExistedTask.Exists(x => x.StartDate == null))
						{
							var taskToUpdate = ExistedTask.Find(x => x.StartDate == null);
							taskToUpdate.UserId = task.UserId;
							_projectService.UpdateBundleTask(taskToUpdate);
						}
						else
						{
							_projectService.AddBundleTask(new QABundles_Task()
							{
								UserId = task.UserId,
								TaskDefID = task.TaskDefId,
								TargetDate = (task.TargetDate != null) ? task.TargetDate.Value.Date : task.TargetDate,
								tfCurrent = true,
								AddUserId = _userContext.User.Id,
								CreatedTime = DateTime.Now,
								BundleID = bundleId
							});
						}
					}
				}//Tasks
			} //Projects
		}
		private void ApplyChanges(IndexViewModel vm)
		{
			foreach (var projectId in vm.ProjectsIds)
			{
				var iProjectId = Convert.ToInt32(projectId);

				var bundleId = _dbSubmission.QABundles.Where(x => x.ProjectId == iProjectId).Select(x => x.Id).Single();
				var newTasks = CreateAddTasks(vm.Tasks, bundleId);
				var updateTasks = CreateUpdateTasks(vm.Tasks, bundleId);

				if (newTasks.Count > 0)
				{
					_projectService.AddBundleTasks(newTasks);
				}

				if (updateTasks.Count > 0)
				{
					_projectService.UpdateBundleTasks(updateTasks);
				}
			}
		}
		private List<QABundles_Task> CreateUpdateTasks(List<QATask> tasks, int bundleId)
		{
			var taskList = new List<QABundles_Task>();
			var updateTasks = tasks.Where(x => x.Id != 0 && x._dirty == true).ToList();
			foreach (var task in updateTasks)
			{
				taskList.Add(new QABundles_Task()
				{
					Id = task.Id,
					UserId = task.UserId,
					TaskDefID = task.TaskDefId,
					TargetDate = (task.TargetDate != null) ? task.TargetDate.Value.Date : task.TargetDate,
					tfCurrent = true,
					AddUserId = _userContext.User.Id,
					CreatedTime = DateTime.Now,
					BundleID = bundleId
				});
			}
			return taskList;
		}

		private List<QABundles_Task> CreateAddTasks(List<QATask> tasks, int bundleId)
		{
			var taskList = new List<QABundles_Task>();
			var newTasks = tasks.Where(x => x.Id == 0).ToList();
			foreach (var task in newTasks)
			{
				taskList.Add(new QABundles_Task()
				{
					UserId = task.UserId,
					TaskDefID = task.TaskDefId,
					TargetDate = (task.TargetDate != null) ? task.TargetDate.Value.Date : task.TargetDate,
					tfCurrent = true,
					AddUserId = _userContext.User.Id,
					CreatedTime = DateTime.Now,
					BundleID = bundleId
				});

			}
			return taskList;
		}
		#endregion
	}
}