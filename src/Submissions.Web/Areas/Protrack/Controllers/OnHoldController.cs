﻿using GLI.EFCore.Submission;
using Newtonsoft.Json;
using Submissions.Common;
using Submissions.Core;
using Submissions.Core.Models.ProjectService;
using Submissions.Web.Areas.Protrack.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace Submissions.Web.Areas.Protrack.Controllers
{
	public class OnHoldController : Controller
	{
		private readonly SubmissionContext _dbSubmission;
		private readonly IUserContext _userContext;
		private readonly IProjectService _projectService;
		private readonly IMemoService _memoService;

		public OnHoldController(SubmissionContext dbSubmission, IUserContext userContext, IProjectService projectService, IMemoService memoService)
		{
			_dbSubmission = dbSubmission;
			_userContext = userContext;
			_projectService = projectService;
			_memoService = memoService;
		}

		public ActionResult Index(int projectId)
		{
			var project = _dbSubmission.trProjects.Where(x => x.Id == projectId).SingleOrDefault();

			var vm = new OnHoldEditViewModel();
			vm.Project.ProjectId = project.Id;
			vm.Project.ProjectName = project.ProjectName;
			vm.Project.ProjectStatus = project.ENStatus;
			vm.Project.OnHold = project.OnHold;
			vm.Project.IsTransfer = project.IsTransfer;

			var submissions = (from s in _dbSubmission.trProjectJurisdictionalData
							   where s.ProjectId == projectId
							   select (new OHSubmission
							   {
								   Id = s.JurisdictionalData.Submission.Id,
								   IdNumber = (s.JurisdictionalData.Submission.IdNumber == null) ? "" : s.JurisdictionalData.Submission.IdNumber,
								   Version = (s.JurisdictionalData.Submission.Version == null) ? "" : s.JurisdictionalData.Submission.Version,
								   PartNumber = (s.JurisdictionalData.Submission.PartNumber == null) ? "" : s.JurisdictionalData.Submission.PartNumber
							   })).Distinct().ToList();
			vm.Project.SubmissionsList = submissions;

			var jurisdictions = (from s in _dbSubmission.trProjectJurisdictionalData
								 where s.ProjectId == projectId
								 select (new OHJurisdiction
								 {
									 JurisdictionId = s.JurisdictionalData.JurisdictionId,
									 JurisdictionName = s.JurisdictionalData.JurisdictionName
								 })).Distinct().ToList();
			vm.Project.JurisdictionsList = jurisdictions;

			return View("Index", vm);
		}

		#region BatchOH
		[OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
		public ActionResult BatchOnHold(string projectIds, bool onHold)
		{
			var vm = new BatchProjectOnHold();
			vm.ProjectIds = projectIds;
			vm.onHold = onHold;
			vm.OhReason = "";

			return PartialView("_BatchOnHold", vm);
		}

		public JsonResult SaveBatchOnHold(BatchProjectOnHold batchModel)
		{
			if (!string.IsNullOrEmpty(batchModel.ProjectIds))
			{
				var projectsWithOverlap = new List<int>();
				var putItemsOnHold = new PutItemsOnHold { OnHold = batchModel.onHold, Reason = batchModel.OhReason, Description = batchModel.OhShortDesc };
				var listProject = new List<int>(batchModel.ProjectIds.Split(',').Select(x => int.Parse(x)));
				var listOHHistory = new List<OnHoldEditHistory>();
				var oHHistory = new OnHoldEditHistory
				{
					StartDate = batchModel.StartDate,
					EndDate = null,
					Reason = batchModel.OhReason,
					ShortDesc = batchModel.OhShortDesc,
					IsBackDated = false
				};
				listOHHistory.Add(oHHistory);

				foreach (var projectId in listProject)
				{
					putItemsOnHold.ProjectIds.Add(projectId);

					if (batchModel.onHold)
					{
						listOHHistory.First().ProjectId = projectId;

						var errorMsg = CheckHistoryData(projectId, 0, 0, listOHHistory, null);
						if (errorMsg != "")
						{
							projectsWithOverlap.Add(projectId);
						}
						else
						{
							SaveHistory(projectId, 0, 0, listOHHistory, null);
						}
					}
				}
				_projectService.PutProjectOrItemsOnHold(putItemsOnHold);

				if (projectsWithOverlap.Count > 0)
				{
					var overlapProjects = _dbSubmission.trProjects.Where(x => projectsWithOverlap.Contains(x.Id))
											.Select(x => new
											{
												ProjectId = x.Id,
												x.ProjectName
											}).ToList();

					return Json(new { overlapProjects }, JsonRequestBehavior.AllowGet);
				}
			}

			return Json("OK", JsonRequestBehavior.AllowGet);
		}

		public ActionResult BatchAddOHHistory(string projectIds)
		{
			var lstProjectIds = projectIds.Split(',').Select(x => int.Parse(x)).ToList();
			var selProjects = _dbSubmission.trProjects.Where(x => lstProjectIds.Contains(x.Id)).Select(x => new OHProject() { ProjectId = x.Id, ProjectName = x.ProjectName, ProjectStatus = x.ENStatus }).ToList();

			var vm = new BatchOHHistory();
			vm.ProjectIds = projectIds;
			vm.BlankOnHoldHistory = new OnHoldEditHistory();
			vm.SelProjects = selProjects;

			return PartialView("_BatchOHHistory", vm);
		}

		public JsonResult SaveBatchOHHistory(BatchOHHistory batchVM)
		{
			var projectsWithOverlap = new List<int>();
			if (!string.IsNullOrEmpty(batchVM.ProjectIds))
			{
				var lstProjects = new List<int>(batchVM.ProjectIds.Split(',').Select(x => int.Parse(x)));
				var lstOHHistory = new List<OnHoldEditHistory>();
				lstOHHistory.Add(batchVM.BlankOnHoldHistory);
				foreach (var projectId in lstProjects)
				{
					var errorMsg = CheckHistoryData(projectId, 0, 0, lstOHHistory, null);
					if (errorMsg != "")
					{
						projectsWithOverlap.Add(projectId);
					}
					else
					{
						SaveHistory(projectId, 0, 0, lstOHHistory, null);
					}
				}

				if (projectsWithOverlap.Count > 0)
				{
					var overlapProjects = _dbSubmission.trProjects.Where(x => projectsWithOverlap.Contains(x.Id))
											.Select(x => new
											{
												ProjectId = x.Id,
												x.ProjectName
											}).ToList();

					return Json(new { overlapProjects }, JsonRequestBehavior.AllowGet);
				}
				else
				{
					return Json("OK", JsonRequestBehavior.AllowGet);
				}
			}
			else
			{
				return Json("OK", JsonRequestBehavior.AllowGet);
			}
		}
		#endregion

		#region SaveOH
		[HttpPost]
		public ActionResult SaveOH(OnHoldEditViewModel onHoldModel)
		{
			var projectId = onHoldModel.Project.ProjectId;

			if (onHoldModel.ApplyToJurisdictionId != null)
			{
				var ApplyJurId = JsonConvert.DeserializeObject<List<AffectedOHId>>(onHoldModel.ApplyToJurisdictionId);

				//get all jurs from On Hold to Off Hold
				var jurOffHoldIds = (from j in ApplyJurId
									 where j.OldStatus == OnHoldMode.On && j.NewStatus == OnHoldMode.Off
									 select j.Id).ToList();
				var putItemsOnHold = new PutItemsOnHold { JurisdictionalDataIds = jurOffHoldIds, OnHold = Convert.ToBoolean((int)OnHoldMode.Off), Reason = onHoldModel.OhMemo, Description = onHoldModel.OhShortDesc };
				_projectService.PutProjectOrItemsOnHold(putItemsOnHold);

				//get all jurs from Off Hold to On Hold
				var jurOnHoldIds = (from j in ApplyJurId
									where j.OldStatus == OnHoldMode.Off && j.NewStatus == OnHoldMode.On
									select j.Id).ToList();

				putItemsOnHold.JurisdictionalDataIds = jurOnHoldIds;
				putItemsOnHold.OnHold = Convert.ToBoolean((int)OnHoldMode.On);
				_projectService.PutProjectOrItemsOnHold(putItemsOnHold);

				//send an email if items going on Hold
				if (jurOnHoldIds.Count > 0)
				{
					_projectService.OHEmail(projectId, null, jurOnHoldIds, onHoldModel.OhMemo, onHoldModel.OhShortDesc);
				}

			}
			else if (onHoldModel.ApplyToSubmissionId != null)
			{
				var ApplySubId = JsonConvert.DeserializeObject<List<AffectedOHId>>(onHoldModel.ApplyToSubmissionId);

				//get all subs from On Hold to Off Hold
				var subOffHoldIds = (from j in ApplySubId
									 where j.OldStatus == OnHoldMode.On && j.NewStatus == OnHoldMode.Off
									 select j.Id).ToList();
				var putItemsOnHold = new PutItemsOnHold { SubmissionIds = subOffHoldIds, OnHold = Convert.ToBoolean((int)OnHoldMode.Off), Reason = onHoldModel.OhMemo, Description = onHoldModel.OhShortDesc };
				_projectService.PutProjectOrItemsOnHold(putItemsOnHold);

				//get all subs from Off Hold to On Hold
				var subOnHoldIds = (from j in ApplySubId
									where j.OldStatus == OnHoldMode.Off && j.NewStatus == OnHoldMode.On
									select j.Id).ToList();
				putItemsOnHold.SubmissionIds = subOnHoldIds;
				putItemsOnHold.OnHold = Convert.ToBoolean((int)OnHoldMode.On);
				_projectService.PutProjectOrItemsOnHold(putItemsOnHold);

				//send an email if items going on Hold
				if (subOnHoldIds.Count > 0)
				{
					_projectService.OHEmail(projectId, subOnHoldIds, null, onHoldModel.OhMemo, onHoldModel.OhShortDesc);
				}
			}
			else if (onHoldModel.ApplyToProjectId != null)
			{
				var ApplyProjectId = JsonConvert.DeserializeObject<List<AffectedOHId>>(onHoldModel.ApplyToProjectId);

				var projectOffHold = (from p in ApplyProjectId
									  where p.OldStatus == OnHoldMode.On && p.NewStatus == OnHoldMode.Off
									  select p.Id).ToList();
				var putItemsOnHold = new PutItemsOnHold { ProjectIds = projectOffHold, OnHold = Convert.ToBoolean((int)OnHoldMode.Off), Reason = onHoldModel.OhMemo, Description = onHoldModel.OhShortDesc };

				if (projectOffHold.Count > 0)
				{
					//off hold
					_projectService.PutProjectOrItemsOnHold(putItemsOnHold);
				}
				else
				{
					var projectOnHold = (from p in ApplyProjectId
										 where p.OldStatus == OnHoldMode.Off && p.NewStatus == OnHoldMode.On
										 select p.Id).ToList();
					putItemsOnHold.ProjectIds = projectOnHold;
					putItemsOnHold.OnHold = Convert.ToBoolean((int)OnHoldMode.On);

					//on hold  
					if (projectOnHold.Count > 0)
					{
						_projectService.PutProjectOrItemsOnHold(putItemsOnHold);

						//send an email if items going on Hold
						_projectService.OHEmail(projectId, null, null, onHoldModel.OhMemo, onHoldModel.OhShortDesc);
					}
				}
			}

			return RedirectToAction("Index", new { projectId });
		}
		#endregion

		#region History
		[OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
		public ActionResult LoadOHHistory(int projectId, int? submissionId, int? jurisdictionId)
		{
			var vm = new OnHoldHistoryViewModel();

			if (submissionId != null && submissionId != 0)
			{
				var submission = _dbSubmission.Submissions.Where(x => x.Id == submissionId).Single();
				vm.SubmissionId = submissionId;
				vm.FileNumber = submission.FileNumber;
				vm.IDNumber = submission.IdNumber;
				vm.GameName = submission.GameName;
				vm.DateCode = submission.DateCode;
				vm.Version = submission.Version;
				vm.SubmissionStatus = submission.Status;
				vm.ProjectId = projectId;
				vm.IsTransfer = false;

				var subOHHistory = (from h in _dbSubmission.trOH_Histories
									where h.SubmissionId == submissionId
									select (new OnHoldEditHistory()
									{
										Id = h.Id,
										StartDate = h.StartDate,
										EndDate = h.EndDate,
										Reason = h.Reason,
										ShortDesc = h.ShortDesc,
										IsBackDated = h.IsBackdated,
										ApplyToAll = true
									})).OrderByDescending(m => m.StartDate).ToList();

				vm.OnHoldHistory = subOHHistory;
				vm.BlankOnHoldHistory.ApplyToAll = true;

				//get All Apply to Items
				var applyToaAllIds = GetAffectedIds(projectId, submissionId, 0);

				var applyToAllResults = _dbSubmission.JurisdictionalData.Where(x => applyToaAllIds.JurisdictionId.Contains(x.Id)).AsQueryable();

				var alljurs = (from j in applyToAllResults
							   select (new ApplyToAllVM()
							   {
								   FileNumber = j.Submission.FileNumber,
								   IDNumber = j.Submission.IdNumber,
								   Version = j.Submission.Version,
								   GameName = j.Submission.GameName,
								   Jurisdiction = j.JurisdictionName,
								   JurisdictionId = j.JurisdictionId,
								   JurisdictionStatus = j.Status,
								   JurRcvdDate = j.ReceiveDate,
								   Id = j.Id
							   }
								 )).OrderBy(x => x.Jurisdiction).ToList();

				vm.ApplyToAll = alljurs;
			}
			else if (jurisdictionId != null && jurisdictionId != 0)
			{
				var jurisdiction = _dbSubmission.JurisdictionalData.Where(x => x.Id == jurisdictionId).Single();
				vm.PrimaryId = jurisdictionId;
				vm.FileNumber = jurisdiction.Submission.FileNumber;
				vm.IDNumber = jurisdiction.Submission.IdNumber;
				vm.GameName = jurisdiction.Submission.GameName;
				vm.DateCode = jurisdiction.Submission.DateCode;
				vm.Version = jurisdiction.Submission.Version;
				vm.Jurisdiction = jurisdiction.JurisdictionName;
				vm.JurisdictionId = jurisdiction.JurisdictionId;
				vm.JurisdictionStatus = jurisdiction.Status;
				vm.ProjectId = projectId;
				vm.IsTransfer = (_dbSubmission.trProjects.Where(m => m.Id == vm.MainProjectId).Select(t => t.IsTransfer).SingleOrDefault() == true) ? true : false;

				var jurOHHistory = (from h in _dbSubmission.trOH_Histories
									where h.JurisdictionalDataId == jurisdictionId
									select (new OnHoldEditHistory()
									{
										Id = h.Id,
										StartDate = h.StartDate,
										EndDate = h.EndDate,
										Reason = h.Reason,
										ShortDesc = h.ShortDesc,
										IsBackDated = h.IsBackdated
									})).OrderByDescending(m => m.StartDate).ToList();

				vm.OnHoldHistory = jurOHHistory;
				vm.BlankOnHoldHistory.ApplyToAll = true;

				//get All Apply to Items
				var applyToaAllIds = GetAffectedIds(projectId, 0, jurisdictionId);

				var applyToAllResults = _dbSubmission.JurisdictionalData.Where(x => applyToaAllIds.JurisdictionId.Contains(x.Id)).AsQueryable();

				var alljurs = (from j in applyToAllResults
							   select (new ApplyToAllVM()
							   {
								   FileNumber = j.Submission.FileNumber,
								   IDNumber = j.Submission.IdNumber,
								   Version = j.Submission.Version,
								   GameName = j.Submission.GameName,
								   Jurisdiction = j.JurisdictionName,
								   JurisdictionId = j.JurisdictionId,
								   JurisdictionStatus = j.Status,
								   JurRcvdDate = j.ReceiveDate,
								   Id = j.Id
							   }
							)).OrderBy(x => x.Jurisdiction).ToList();

				vm.ApplyToAll = alljurs;
			}
			else
			{
				var project = _dbSubmission.trProjects.Where(x => x.Id == projectId).Single();
				vm.ProjectName = project.ProjectName;
				vm.ProjectId = projectId;
				vm.ProjectOnHold = (project.OnHold == 1) ? true : false;
				vm.IsTransfer = (project.IsTransfer == true) ? true : false;

				var projectOHHistory = (from h in _dbSubmission.trOH_Histories
										where h.ProjectId == projectId
										select (new OnHoldEditHistory()
										{
											Id = h.Id,
											StartDate = h.StartDate,
											EndDate = h.EndDate,
											Reason = h.Reason,
											ShortDesc = h.ShortDesc,
											IsBackDated = h.IsBackdated,
											ApplyToAll = true
										})).OrderByDescending(m => m.StartDate).ToList();

				vm.OnHoldHistory = projectOHHistory;
				vm.BlankOnHoldHistory.ApplyToAll = true;
			}

			return PartialView("_History", vm);
		}

		[HttpPost]
		public ActionResult SaveOHHistory(int ProjectId, int SubmissionId, int JurisdictionId, string editViewModel, string ApplyToIds)
		{
			var projectId = ProjectId;
			var ohEditHistory = JsonConvert.DeserializeObject<List<OnHoldEditHistory>>(editViewModel);
			var applyToIdList = (string.IsNullOrEmpty(ApplyToIds)) ? null : ApplyToIds.Split(',').Select(i => int.Parse(i)).ToList();

			var ohChangedDates = ohEditHistory.Where(x => x.OldStartDate != x.StartDate || x.OldEndDate != x.EndDate).ToList();
			var errorMsg = CheckHistoryData(ProjectId, SubmissionId, JurisdictionId, ohChangedDates, applyToIdList);
			if (errorMsg != "")
			{
				return Content("Error:" + errorMsg);
			}
			else
			{
				SaveHistory(ProjectId, SubmissionId, JurisdictionId, ohEditHistory, applyToIdList);
				return RedirectToAction("LoadOHHistory", new { projectId = ProjectId, submissionId = SubmissionId, jurisdictionId = JurisdictionId });
			}
		}
		#endregion

		#region HelperMethods
		private string CheckHistoryData(int projectId, int submissionId, int jurisdictionId, List<OnHoldEditHistory> editHistory, List<int> applyToIds = null)
		{
			var errorMsg = "";

			try
			{
				var ohHistory = _dbSubmission.trOH_Histories.AsQueryable();

				if (submissionId != 0)
				{
					ohHistory = ohHistory.Where(x => x.SubmissionId == submissionId).AsQueryable();
				}
				else if (jurisdictionId != 0)
				{
					ohHistory = ohHistory.Where(x => x.JurisdictionalDataId == jurisdictionId).AsQueryable();
				}
				else
				{
					ohHistory = ohHistory.Where(x => x.ProjectId == projectId).AsQueryable();
				}

				//This is just for single item if Apply All do not exist for a history, only check in this list
				var ohCurrentHistory = (from h in ohHistory
										select (new OnHoldEditHistory()
										{
											Id = h.Id,
											StartDate = h.StartDate,
											EndDate = h.EndDate,
											Reason = h.Reason,
											ShortDesc = h.ShortDesc,
											IsBackDated = h.IsBackdated
										})).ToList();

				//get All affected histories to check overlap
				//Check if Apply All exists
				var isApplyAllExists = editHistory.Where(i => i.ApplyToAll == true).Count();
				var ApplyAllHistory = new List<OnHoldEditHistory>();
				if (isApplyAllExists > 0)
				{
					var ApplyAllIds = GetAffectedIds(projectId, submissionId, jurisdictionId, applyToIds);
					ApplyAllHistory = GetProjectAllAffectedHistories(ApplyAllIds);
				}

				//Loop through to see if overlap exists
				foreach (var history in editHistory)
				{
					if (history.StartDate != null)
					{
						var overLapped = GetOverllapedPeriods(history.Id, history.StartDate, history.EndDate, ohCurrentHistory);
						if (overLapped.Count > 0)
						{
							errorMsg = "Conflict";
							break;
						}
						else
						{
							if (history.ApplyToAll == true)
							{
								var overLappedProjectApplyAll = GetOverllapedPeriods(history.Id, history.StartDate, history.EndDate, ApplyAllHistory);
								if (overLappedProjectApplyAll.Count > 0)
								{
									errorMsg = "ConflictApplyAll";
									break;
								}
							}
						}
					}
				}
			}
			catch (Exception e)
			{
				Console.WriteLine("An error occurred: '{0}'", e);
			}
			return errorMsg;
		}

		private List<OnHoldEditHistory> GetProjectAllAffectedHistories(ApplyProjectItemIds allIds)
		{
			var ohProjectSubHistory = new List<OnHoldEditHistory>();
			var ohProjectJurHistory = new List<OnHoldEditHistory>();
			if (allIds.SubmissionId != null)
			{
				var allAffectedSubHistories = _dbSubmission.trOH_Histories.Where(t => allIds.SubmissionId.Contains(t.SubmissionId)).AsQueryable();
				ohProjectSubHistory = (from h in allAffectedSubHistories
									   select (new OnHoldEditHistory()
									   {
										   Id = h.Id,
										   StartDate = h.StartDate,
										   EndDate = h.EndDate,
										   Reason = h.Reason,
										   ShortDesc = h.ShortDesc,
										   IsBackDated = h.IsBackdated,
										   SubmissionId = h.SubmissionId,
									   })).ToList();
			}

			if (allIds.JurisdictionId != null)
			{
				var allAffectedJurHistories = _dbSubmission.trOH_Histories.Where(t => allIds.JurisdictionId.Contains(t.JurisdictionalData.Id)).AsQueryable();
				ohProjectJurHistory = (from h in allAffectedJurHistories
									   select (new OnHoldEditHistory()
									   {
										   Id = h.Id,
										   StartDate = h.StartDate,
										   EndDate = h.EndDate,
										   Reason = h.Reason,
										   ShortDesc = h.ShortDesc,
										   IsBackDated = h.IsBackdated,
										   JurisdictionId = h.JurisdictionalDataId,
									   })).ToList();
			}
			var allProjectHistory = ohProjectSubHistory.Concat(ohProjectJurHistory).ToList();

			return allProjectHistory;
		}

		private ApplyProjectItemIds GetAffectedIds(int projectId, int? submissionId, int? jurisdictionId, List<int> applyToIds = null)
		{
			var idList = new ApplyProjectItemIds();
			if (jurisdictionId != null && jurisdictionId != 0)
			{
				if (applyToIds == null)
				{
					var jurNum = _dbSubmission.JurisdictionalData.Where(x => x.Id == jurisdictionId).Select(t => t.JurisdictionId).SingleOrDefault().ToString();
					var listSubJurIds = _dbSubmission.trProjectJurisdictionalData.Where(x => x.JurisdictionalData.Id != jurisdictionId).Where(x => x.ProjectId == projectId && x.JurisdictionalData.JurisdictionId == jurNum).AsQueryable();

					var jurIds = listSubJurIds.Select(x => x.JurisdictionalDataId).Distinct().ToList();
					idList.JurisdictionId = jurIds;
				}
				else
				{
					idList.JurisdictionId = applyToIds;
				}
			}
			else if (submissionId != null && submissionId != 0)
			{
				if (applyToIds == null)
				{
					var listSubJurIds = _dbSubmission.trProjectJurisdictionalData.Where(x => x.ProjectId == projectId).AsQueryable();

					var jurIds = listSubJurIds.Where(x => x.JurisdictionalData.SubmissionId == submissionId).Select(x => x.JurisdictionalData.Id).Distinct().ToList();
					idList.JurisdictionId = jurIds;
				}
				else
				{
					idList.JurisdictionId = applyToIds;
				}
			}
			else
			{
				var listSubJurIds = _dbSubmission.trProjectJurisdictionalData.Where(x => x.ProjectId == projectId).AsQueryable();

				var isTransfer = _dbSubmission.trProjects.Where(x => x.Id == projectId).Select(x => x.IsTransfer).SingleOrDefault();
				if (isTransfer == false)
				{
					var subIds = listSubJurIds.Select(x => x.JurisdictionalData.SubmissionId).Distinct().ToList();
					idList.SubmissionId = subIds;
				}

				var jurIds = listSubJurIds.Select(x => x.JurisdictionalDataId).Distinct().ToList();
				idList.JurisdictionId = jurIds;
			}

			return idList;
		}

		private List<OnHoldEditHistory> GetOverllapedPeriods(int editId, DateTime? startDate, DateTime? endDate, List<OnHoldEditHistory> editHistory)
		{
			//important, so it removes timestamp before comparing, When OH is udpated through normal process it has time stamp
			foreach (var history in editHistory)
			{
				history.StartDate = (history.StartDate != null) ? history.StartDate.Value.Date : history.StartDate;
				history.EndDate = (history.EndDate != null) ? history.EndDate.Value.Date : (history.EndDate == null) ? DateTime.Now.Date : history.EndDate;
			}

			var query = editHistory.Where(b => b.Id != editId).AsQueryable();
			if (editId != 0) //Update exsiting period (time zone issue and when other items selected to apply)
			{
				var oldInfo = _dbSubmission.trOH_Histories.Where(x => x.Id == editId).Select(x => new { x.StartDate, x.EndDate }).SingleOrDefault();
				if (oldInfo.StartDate != null && oldInfo.EndDate != null)
				{
					var oldInfoStartDate = oldInfo.StartDate.Value.Date;
					var oldInfoEndDate = oldInfo.EndDate.Value.Date;
					query = query.Where(x => (x.StartDate != oldInfoStartDate && x.EndDate != oldInfoEndDate)).AsQueryable();
				}
				else if (oldInfo.StartDate != null && oldInfo.EndDate == null)
				{
					var oldInfoStartDate = oldInfo.StartDate.Value.Date;
					query = query.Where(x => (x.StartDate != oldInfoStartDate && x.EndDate != oldInfo.EndDate)).AsQueryable();
				}
			}

			if (endDate == null) // when project is on Hold, and start date is changed by AU, find overlapping
			{
				endDate = DateTime.Now.Date;
			}

			query = query.Where(b => (b.StartDate > startDate && b.StartDate < endDate) ||
											(b.EndDate > startDate && b.EndDate < endDate) ||
											(b.StartDate < startDate && b.EndDate > endDate) ||
											(startDate >= b.StartDate && endDate < b.EndDate) ||
											(startDate > b.StartDate && endDate <= b.EndDate) ||
											(startDate == b.StartDate && endDate == b.EndDate)).AsQueryable();

			var overlappedPeriods = query.ToList();

			return overlappedPeriods;
		}

		private void AddApplyAllIds(OnHoldEditHistory history, ApplyProjectItemIds allApplyIds)
		{
			if (allApplyIds.SubmissionId != null)
			{
				foreach (var keytbl in allApplyIds.SubmissionId)
				{
					var rcvdDate = _dbSubmission.Submissions.Where(x => x.Id == keytbl).Select(x => x.ReceiveDate).SingleOrDefault();
					if (history.EndDate > rcvdDate)
					{
						var newHistory = new trOH_History()
						{
							SubmissionId = keytbl,
							StartDate = (history.StartDate < rcvdDate) ? rcvdDate : history.StartDate,
							EndDate = history.EndDate,
							Reason = history.Reason,
							ShortDesc = history.ShortDesc,
							IsBackdated = true
						};
						_projectService.AddOHHistory(newHistory);
					}
				}
			}

			if (allApplyIds.JurisdictionId != null)
			{
				foreach (var primary in allApplyIds.JurisdictionId)
				{
					var jur = _dbSubmission.JurisdictionalData.Where(x => x.Id == primary).SingleOrDefault();
					var rcvdDate = jur.ReceiveDate;
					var closeDate = jur.CloseDate;
					if (history.EndDate > rcvdDate)
					{
						var newHistory = new trOH_History()
						{
							JurisdictionalDataId = primary,
							StartDate = (history.StartDate < rcvdDate) ? rcvdDate : history.StartDate,
							EndDate = (history.EndDate > closeDate) ? closeDate : history.EndDate,
							Reason = history.Reason,
							ShortDesc = history.ShortDesc,
							IsBackdated = true
						};
						_projectService.AddOHHistory(newHistory);
					};
				}
			}
		}

		private void UpdateApplyAllIds(OnHoldEditHistory history, ApplyProjectItemIds allApplyIds)
		{
			var oldStartDateYear = history.OldStartDate.Value.Year;
			var oldStartDateMonth = history.OldStartDate.Value.Month;
			var oldStartDateDay = history.OldStartDate.Value.Day;
			var startDateYear = history.StartDate.Value.Year;
			var startDateMonth = history.StartDate.Value.Month;
			var startDateDay = history.StartDate.Value.Day;

			int oldEndDateYear = 0;
			int oldEndDateMonth = 0;
			int oldEndDateDay = 0;
			if (history.OldEndDate != null)
			{
				oldEndDateYear = history.OldEndDate.Value.Year;
				oldEndDateMonth = history.OldEndDate.Value.Month;
				oldEndDateDay = history.OldEndDate.Value.Day;
			}

			int endDateYear = 0;
			int endDateMonth = 0;
			int endDateDay = 0;
			if (history.EndDate != null)
			{
				endDateYear = history.EndDate.Value.Year;
				endDateMonth = history.EndDate.Value.Month;
				endDateDay = history.EndDate.Value.Day;
			}

			if (allApplyIds.SubmissionId != null)
			{
				foreach (var keytbl in allApplyIds.SubmissionId)
				{
					var selHistory = _dbSubmission.trOH_Histories.Where(x => x.SubmissionId == keytbl).Where(t => (t.StartDate.Value.Year == oldStartDateYear && t.StartDate.Value.Month == oldStartDateMonth && t.StartDate.Value.Day == oldStartDateDay) && (t.EndDate.Value.Year == oldEndDateYear && t.EndDate.Value.Month == oldEndDateMonth && t.EndDate.Value.Day == oldEndDateDay)).FirstOrDefault();
					if (selHistory != null)
					{
						selHistory.StartDate = history.StartDate;
						selHistory.EndDate = history.EndDate;
						selHistory.IsBackdated = true;
						selHistory.Reason = history.Reason;
						selHistory.ShortDesc = history.ShortDesc;

						_projectService.UpdateOHHistory(selHistory);
					}
					else
					{
						var rowExists = _dbSubmission.trOH_Histories.Where(x => x.SubmissionId == keytbl).Where(t => (t.StartDate.Value.Year == startDateYear && t.StartDate.Value.Month == startDateMonth && t.StartDate.Value.Day == startDateDay) && (t.EndDate.Value.Year == endDateYear && t.EndDate.Value.Month == endDateMonth && t.EndDate.Value.Day == endDateDay)).FirstOrDefault();
						if (rowExists == null)
						{
							var rcvdDate = _dbSubmission.Submissions.Where(x => x.Id == keytbl).Select(x => x.ReceiveDate).SingleOrDefault();
							if (history.EndDate > rcvdDate)
							{
								var newHistory = new trOH_History()
								{
									SubmissionId = keytbl,
									StartDate = (history.StartDate < rcvdDate) ? rcvdDate : history.StartDate,
									EndDate = history.EndDate,
									Reason = history.Reason,
									ShortDesc = history.ShortDesc,
									IsBackdated = true
								};
								_projectService.AddOHHistory(newHistory);
							}
						}
					}
				}
			}

			if (allApplyIds.JurisdictionId != null)
			{
				foreach (var primary in allApplyIds.JurisdictionId)
				{
					var selHistory = _dbSubmission.trOH_Histories.Where(x => x.JurisdictionalDataId == primary).Where(t => (t.StartDate.Value.Year == oldStartDateYear && t.StartDate.Value.Month == oldStartDateMonth && t.StartDate.Value.Day == oldStartDateDay) && (t.EndDate.Value.Year == oldEndDateYear && t.EndDate.Value.Month == oldEndDateMonth && t.EndDate.Value.Day == oldEndDateDay)).FirstOrDefault();
					if (selHistory != null)
					{
						selHistory.StartDate = history.StartDate;
						selHistory.EndDate = history.EndDate;
						selHistory.IsBackdated = true;
						selHistory.Reason = history.Reason;
						selHistory.ShortDesc = history.ShortDesc;

						_projectService.UpdateOHHistory(selHistory);
					}
					else
					{
						var jurExists = _dbSubmission.trOH_Histories.Where(x => x.JurisdictionalDataId == primary).Where(t => (t.StartDate.Value.Year == startDateYear && t.StartDate.Value.Month == startDateMonth && t.StartDate.Value.Day == startDateDay) && (t.EndDate.Value.Year == endDateYear && t.EndDate.Value.Month == endDateMonth && t.EndDate.Value.Day == endDateDay)).FirstOrDefault();
						if (jurExists == null)
						{
							var jur = _dbSubmission.JurisdictionalData.Where(x => x.Id == primary).SingleOrDefault();
							var rcvdDate = jur.ReceiveDate;
							var closeDate = jur.CloseDate;
							if (history.EndDate > rcvdDate)
							{
								var newHistory = new trOH_History()
								{
									JurisdictionalDataId = primary,
									StartDate = (history.StartDate < rcvdDate) ? rcvdDate : history.StartDate,
									EndDate = (history.EndDate > closeDate) ? closeDate : history.EndDate,
									Reason = history.Reason,
									ShortDesc = history.ShortDesc,
									IsBackdated = true
								};
								_projectService.AddOHHistory(newHistory);
							}
						}
					}
				}
			}
		}

		private void DeleteApplyAllIds(OnHoldEditHistory history, ApplyProjectItemIds allApplyIds)
		{
			if (allApplyIds.SubmissionId != null)
			{
				foreach (var keytbl in allApplyIds.SubmissionId)
				{
					var selHistory = _dbSubmission.trOH_Histories.Where(x => x.SubmissionId == keytbl).Where(t => (t.StartDate.Value.Year == history.OldStartDate.Value.Year && t.StartDate.Value.Month == history.OldStartDate.Value.Month && t.StartDate.Value.Day == history.OldStartDate.Value.Day) && (t.EndDate.Value.Year == history.OldEndDate.Value.Year && t.EndDate.Value.Month == history.OldEndDate.Value.Month && t.EndDate.Value.Day == history.OldEndDate.Value.Day)).FirstOrDefault();
					if (selHistory != null)
					{
						_projectService.DeleteOHHistory(selHistory.Id);
					}
				}
			}

			if (allApplyIds.JurisdictionId != null)
			{
				foreach (var primary in allApplyIds.JurisdictionId)
				{
					var selHistory = _dbSubmission.trOH_Histories.Where(x => x.JurisdictionalDataId == primary).Where(t => (t.StartDate.Value.Year == history.OldStartDate.Value.Year && t.StartDate.Value.Month == history.OldStartDate.Value.Month && t.StartDate.Value.Day == history.OldStartDate.Value.Day) && (t.EndDate.Value.Year == history.OldEndDate.Value.Year && t.EndDate.Value.Month == history.OldEndDate.Value.Month && t.EndDate.Value.Day == history.OldEndDate.Value.Day)).FirstOrDefault();
					if (selHistory != null)
					{
						_projectService.DeleteOHHistory(selHistory.Id);
					}
				}
			}
		}

		private void SaveHistory(int projectId, int? submissionId, int? jurisdictionId, List<OnHoldEditHistory> editHistory, List<int> applyToIds = null)
		{
			//get All affected histories to check overlap
			//Check if Apply All exists
			var isApplyAllExists = editHistory.Where(i => i.ApplyToAll == true).Count();
			var applyAllIds = new ApplyProjectItemIds();
			if (isApplyAllExists > 0)
			{
				applyAllIds = GetAffectedIds(projectId, submissionId, jurisdictionId, applyToIds);
			}

			foreach (var history in editHistory)
			{
				if (history.Id == 0) // Add
				{
					if (submissionId != 0)
					{
						var rcvdDate = _dbSubmission.Submissions.Where(x => x.Id == submissionId).Select(x => x.ReceiveDate).SingleOrDefault();
						if (history.EndDate > rcvdDate)
						{
							var newHistory = new trOH_History()
							{
								SubmissionId = submissionId,
								StartDate = (history.StartDate < rcvdDate) ? rcvdDate : history.StartDate,
								EndDate = history.EndDate,
								Reason = history.Reason,
								ShortDesc = history.ShortDesc,
								IsBackdated = true
							};
							_projectService.AddOHHistory(newHistory);
						}
					}
					else if (jurisdictionId != 0)
					{
						var jur = _dbSubmission.JurisdictionalData.Where(x => x.Id == jurisdictionId).SingleOrDefault();
						var rcvdDate = jur.ReceiveDate;
						var closeDate = jur.CloseDate;
						if (history.EndDate > rcvdDate)
						{
							var newHistory = new trOH_History()
							{
								JurisdictionalDataId = jurisdictionId,
								StartDate = (history.StartDate < rcvdDate) ? rcvdDate : history.StartDate,
								EndDate = (history.EndDate > closeDate) ? closeDate : history.EndDate,
								Reason = history.Reason,
								ShortDesc = history.ShortDesc,
								IsBackdated = true
							};
							_projectService.AddOHHistory(newHistory);
						}
					}
					else
					{
						var project = _dbSubmission.trProjects.Where(x => x.Id == projectId).SingleOrDefault();
						var rcvdDate = project.ReceiveDate;
						var closeDate = project.QACompleteDate;
						if (history.EndDate > rcvdDate || history.EndDate == null)
						{
							var newHistory = new trOH_History()
							{
								ProjectId = projectId,  //Since projectID will always be passed, we dont want to set ProjectID when it's for Jur or sub
								StartDate = (history.StartDate < rcvdDate) ? rcvdDate : history.StartDate,
								EndDate = history.EndDate,
								Reason = history.Reason,
								ShortDesc = history.ShortDesc,
								IsBackdated = true
							};
							_projectService.AddOHHistory(newHistory);
						}
					}

					if (history.ApplyToAll == true)
					{
						AddApplyAllIds(history, applyAllIds);
					}
				}
				else
				{
					var selHistory = _dbSubmission.trOH_Histories.Where(x => x.Id == history.Id).SingleOrDefault();

					if (history.StartDate != null) //Edit //Currently on Hold entry can not be modified
					{
						//needs to be checked as when Db updates stamp, it's with time, should not overwrite
						//only set backdate if dates are changed
						if (selHistory.StartDate.Value.Date != history.StartDate)
						{
							selHistory.StartDate = history.StartDate;
							selHistory.IsBackdated = true;
						}
						if (history.EndDate != null)
						{
							if (selHistory.EndDate.Value.Date != history.EndDate)
							{
								selHistory.EndDate = history.EndDate;
								selHistory.IsBackdated = true;
							}
						}
						selHistory.Reason = history.Reason;
						selHistory.ShortDesc = history.ShortDesc;

						_projectService.UpdateOHHistory(selHistory);

						if (history.ApplyToAll == true)
						{
							UpdateApplyAllIds(history, applyAllIds);
						}
					}
					else //Delete
					{
						_projectService.DeleteOHHistory(selHistory.Id);
						if (history.ApplyToAll == true)
						{
							DeleteApplyAllIds(history, applyAllIds);
						}
					}
				}
			}
		}
		#endregion

		[HttpPost]
		public ActionResult SaveOHMemo(int projectId, string ohMemo)
		{
			if (projectId != 0)
			{
				var latestOHEntry = _dbSubmission.trOH_Histories.Where(x => x.ProjectId == projectId).Where(x => x.EndDate == null).OrderByDescending(x => x.Id).Take(1).SingleOrDefault();
				latestOHEntry.Reason = ohMemo;
				_projectService.UpdateOHHistory(latestOHEntry);

				var applyAllIds = new ApplyProjectItemIds();
				applyAllIds = GetAffectedIds(projectId, 0, 0); //Get all affected submissionIds and jurids

				if (applyAllIds.SubmissionId != null)
				{
					foreach (var keytbl in applyAllIds.SubmissionId)
					{
						latestOHEntry = _dbSubmission.trOH_Histories.Where(x => x.SubmissionId == keytbl).Where(x => x.EndDate == null).OrderByDescending(x => x.Id).Take(1).SingleOrDefault();
						if (latestOHEntry != null)
						{
							latestOHEntry.Reason = ohMemo;
							_projectService.UpdateOHHistory(latestOHEntry);
						}
					}
				}

				if (applyAllIds.JurisdictionId != null)
				{
					foreach (var primary in applyAllIds.JurisdictionId)
					{
						latestOHEntry = _dbSubmission.trOH_Histories.Where(x => x.JurisdictionalDataId == primary).Where(x => x.EndDate == null).OrderByDescending(x => x.Id).Take(1).SingleOrDefault();
						if (latestOHEntry != null)
						{
							latestOHEntry.Reason = ohMemo;
							_projectService.UpdateOHHistory(latestOHEntry);
						}
					}
				}

				var listSubJurIds = _dbSubmission.trProjectJurisdictionalData.Where(x => x.ProjectId == projectId).AsQueryable();
				var allSubmissionIds = listSubJurIds.Select(x => x.JurisdictionalData.SubmissionId).Distinct().ToList();

				//insert memo
				if (allSubmissionIds != null)
				{
					foreach (var keytbl in allSubmissionIds)
					{
						var newOHMemo = new tblOHMemo()
						{
							Memo = ohMemo,
							Username = _userContext.User.Username,
							SubmissionId = (int)keytbl,
							AddDate = DateTime.Now,
						};

						//Add Memo
						_memoService.AddOnHold(newOHMemo);
					}
				}
			}
			return Content("OK");
		}
	}
}