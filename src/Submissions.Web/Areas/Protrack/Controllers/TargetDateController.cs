﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using EF.Submission;
using Submissions.Common;
using Submissions.Core;
using Submissions.Web.Areas.Protrack.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Web.Configuration;
using System.Web.Mvc;

namespace Submissions.Web.Areas.Protrack.Controllers
{
	using Submissions.Common.Email;
	using Submissions.Web.Utility.Email;

	public class TargetDateController : Controller
	{
		private readonly ISubmissionContext _dbSubmission;
		private readonly IUserContext _userContext;
		private readonly IEmailService _emailService;
		private readonly IProjectService _projectService;
		private readonly INotificationService _notificationService;

		public TargetDateController(ISubmissionContext dbSubmission, IUserContext userContext, IProjectService projectService, INotificationService notificationService, IEmailService emailService)
		{
			_dbSubmission = dbSubmission;
			_userContext = userContext;
			_projectService = projectService;
			_notificationService = notificationService;
			_emailService = emailService;
		}

		public ActionResult Index(int projectId)
		{
			var results = LoadIntialData(projectId);
			return View("Index", results);
		}

		[HttpPost]
		public ActionResult SaveTargetDate(TargetDateIndexViewModel TargetDateModel)
		{
			//Save Target Date
			int projectId = TargetDateModel.TargetDateInfo.ProjectID;
			string primaryIds = TargetDateModel.TargetDateInfo.PrimaryList;
			DateTime newTargetDate = (DateTime)TargetDateModel.TargetDateInfo.NewTargetDate;
			string note = TargetDateModel.TargetDateInfo.Detail;
			int reason = TargetDateModel.TargetDateInfo.ReasonId;
			TargetDateReviseMode mode = TargetDateModel.TargetDateInfo.ReviseMode;
			int iReviseMode = (int)mode;

			SaveProjectTarget(projectId, primaryIds, _userContext.User.Id, newTargetDate, note, reason, iReviseMode, TargetDateModel.ApplyOffSet);

			if (mode != TargetDateReviseMode.EnterOriginalTarget)
			{
				SendEmail(projectId, primaryIds, mode);
			}

			return RedirectToAction("Index", new { projectId = projectId });
		}

		public ActionResult SyncJurisdictionTargetDates(int projectId)
		{
			_projectService.SyncJurisdictionTargetDate(projectId);
			return RedirectToAction("Index", new { projectId = projectId });
		}

		public void SendEmail(int projectId, string primaryIds, TargetDateReviseMode mode)
		{
			var item = _dbSubmission.trProjects
				.AsNoTracking()
				.Where(x => x.Id == projectId)
				.Single();

			//Send an email
			var emailTarget = Mapper.Map<TargetDateChangeEmailModel>(item);
			emailTarget.ProjectJurisdictions.OrderBy(m => m.Jurisdiction);
			emailTarget.AffectedPrimary = (primaryIds == null) ? "" : primaryIds;
			if (mode == TargetDateReviseMode.ModifyOriginalTarget)
			{
				emailTarget.DisplayOnlyProject = false;
			}
			else
			{
				emailTarget.DisplayOnlyProject = (primaryIds == "" || primaryIds == null) ? true : false;
			}

			var EmailFilter = new EmailTypeSearch();
			EmailFilter.ProjectId = projectId;
			EmailFilter.EmailType = EmailType.TargetDateChange;

			var emailAddresses = string.Join(",", _notificationService.GetEmailToAddresses(EmailFilter));
			if (WebConfigurationManager.AppSettings["Environment"] != SubmissionContext.Environment.Production.ToString())
			{
				emailAddresses = _userContext.User.Email;
			}
			else
			{
				emailAddresses = (!string.IsNullOrEmpty(emailAddresses)) ? emailAddresses + "," + _userContext.User.Email : _userContext.User.Email;
			}

			var mailMsg = new MailMessage("noreply@gaminglabs.com", emailAddresses);
			mailMsg.Subject = (mode == TargetDateReviseMode.ModifyOriginalTarget) ? "Original Target Date was modified for " + emailTarget.ProjectName : "Target Date was modified for " + emailTarget.ProjectName;

			mailMsg.Body = _emailService.RenderTemplate(EmailType.TargetDateChange.ToString(), emailTarget);
			_emailService.Send(mailMsg);
		}

		private void SaveProjectTarget(int projectId, string primaryIds, int userId, DateTime newTargetDate, string note, int reason, int mode, bool applyOffSet)
		{
			if (string.IsNullOrEmpty(primaryIds))
			{
				var project = new trProject { Id = projectId, ApplyTargetOffSet = applyOffSet };
				_dbSubmission.trProjects.Attach(project);
				_dbSubmission.Entry(project).Property(x => x.ApplyTargetOffSet).IsModified = true;
				_dbSubmission.Configuration.ValidateOnSaveEnabled = false;
				_dbSubmission.SaveChanges();
			}

			_projectService.UpdateProjectJurisdictionTargetDate(projectId, primaryIds, userId, newTargetDate, note, reason, mode);
		}

		private TargetDateIndexViewModel LoadIntialData(int projectID)
		{
			Session["ProjectID"] = projectID;
			Session["JurIds"] = null;
			var results = new TargetDateIndexViewModel();

			var project = _dbSubmission.trProjects.Find(projectID);
			var certLabId = project.CertificationLab != null ? _dbSubmission.trLaboratories.Where(x => x.Location == project.CertificationLab).Single().Id : 1;
			var revisions = _dbSubmission.trTargetDate.Where(t => t.ProjectId == projectID).ToList();
			var projectAcceptDate = project.ENAcceptDate;

			var newTargetDate = new TargetDateEditViewModel();
			newTargetDate.ProjectID = projectID;
			newTargetDate.ProjectCertificationLabId = certLabId;
			newTargetDate.ProjectName = project.ProjectName;
			newTargetDate.ProjectStatus = project.ENStatus;
			newTargetDate.TargetDate = project.TargetDate;
			newTargetDate.ApplyToProject = 1;
			newTargetDate.Reason = "";
			if (revisions.Count == 0)
			{
				newTargetDate.ReviseMode = TargetDateReviseMode.EnterOriginalTarget;
				newTargetDate.Detail = "Original Target Date";
			}
			else
			{
				newTargetDate.ReviseMode = TargetDateReviseMode.ModifyCurrentTarget;
			}

			if (newTargetDate.TargetDate != null)
			{
				newTargetDate.AllowEditOriginalDate = AllowEditOriginalDate(projectAcceptDate);
			}
			results.TargetDateInfo = newTargetDate;
			results.JurisdictionsGrid = new ProjectJurisdictionsGrid();

			var offSet = (from projectJur in _dbSubmission.trProjectJurisdictionalData
						  join jur in _dbSubmission.JurisdictionalData on projectJur.JurisdictionalDataId equals jur.Id
						  join offset in _dbSubmission.tbl_lst_Juris_TargetOffSet on jur.JurisdictionId equals offset.JurId
						  where projectJur.ProjectId == projectID && offset.TargetDayOffSet > 0
						  select new JurisdictionTargetOffset
						  {
							  Jurisdiction = offset.Jurisdiction.Name,
							  Offset = offset.TargetDayOffSet,
							  Reason = offset.Reason
						  }).Distinct().ToList();

			results.ProjectJurisdictionOffset = offSet;

			var isJurWithoutTargetdate = (from projectJur in _dbSubmission.trProjectJurisdictionalData
										  join jur in _dbSubmission.JurisdictionalData on projectJur.JurisdictionalDataId equals jur.Id
										  where projectJur.ProjectId == projectID
										  && jur.TargetDate == null && jur.ProposedTargetDate == null
										  select jur.Id).Any();  //removed for sub-2286 - && (jur.Status == JurisdictionStatus.RA.ToString() || jur.Status == JurisdictionStatus.CP.ToString())

			results.TargetDateSync = (isJurWithoutTargetdate && project.TargetDate != null) ? true : false;
			results.AllowUpdateOptions = (project.BundleType.Id == (int)BundleType.ClosedTesting) ? false : true;

			return results;
		}

		[OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
		public ViewResult LoadTargetHistory(int projectId = 0, int primaryId = 0)
		{
			var viewHistory = new ViewHistory();

			if (primaryId != 0)
			{
				var results = _dbSubmission.trTargetDate.Include("JurisdictionalData").Where(t => t.JurisdictionalDataId == primaryId && t.ProjectId == null).OrderByDescending(t => t.ModifiedDate).ToList();
				if (results.Count > 0)
				{
					viewHistory.FileNumber = results[0].JurisdictionalData.Submission.FileNumber;
					viewHistory.IDNumber = results[0].JurisdictionalData.Submission.IdNumber;
					viewHistory.DateCode = results[0].JurisdictionalData.Submission.DateCode;
					viewHistory.GameName = results[0].JurisdictionalData.Submission.GameName;
					viewHistory.Version = results[0].JurisdictionalData.Submission.Version;
					viewHistory.Jurisdiction = results[0].JurisdictionalData.JurisdictionName;
					viewHistory.OriginalTargetDateChangedReason = results[0].JurisdictionalData.OrgTargetDateChangeReason;
				}
				viewHistory.RevisionHistory = Mapper.Map<List<trTargetDate>, List<TargetHistory>>(results);
				return View("History", viewHistory);
			}
			else
			{
				var resultsProject = _dbSubmission.trTargetDate.Where(t => t.ProjectId == projectId && t.JurisdictionalDataId == null).OrderByDescending(t => t.ModifiedDate).ToList();
				if (resultsProject.Count > 0)
				{
					viewHistory.ProjectName = resultsProject[0].Project.ProjectName;
					viewHistory.OriginalTargetDateChangedReason = resultsProject[0].Project.OrgTargetDateChangeReason;
				}
				viewHistory.RevisionHistory = Mapper.Map<List<trTargetDate>, List<TargetHistory>>(resultsProject);
				return View("History", viewHistory);
			}
		}
		[HttpGet]
		public JsonResult CheckProjectOnHold(int projectID)
		{
			var projectOnHold = from m in _dbSubmission.trProjects
								where m.Id == projectID
								select new
								{
									onHold = (m.OnHold == 0) ? "0" : "1"
								};

			return Json(projectOnHold, JsonRequestBehavior.AllowGet);
		}

		[HttpGet]
		public JsonResult GetJurOffsetTarget(int projectID, string jurList, DateTime targetDate, int projectCertificationLabId)
		{
			var query = "Select Msg from (Select distinct convert(varchar,dbo.fn_Add_BusinessDaysToDate('" + targetDate + "' , (c.TargetDayOffSet-minOffset.offset)," + projectCertificationLabId + ") ,101) As NewTarget," +
						" d.Jurisdiction + ' - ' + convert(varchar,dbo.fn_Add_BusinessDaysToDate('" + targetDate + "', (c.TargetDayOffSet-minOffset.offset)," + projectCertificationLabId + ") ,101) + '. (Offset by ' + convert(varchar,TargetDayOffSet) + ' days for ' + isnull(Reason,'') + ').' As Msg " +
						" from trProjectJurisdictionalData a with (nolock) join JurisdictionalData b with (nolock) " +
						" on a.re_Primary_ = b.Primary_ " +
						" join tbl_lst_Juris_TargetOffset c on b.JurisdictionId=c.JurId " +
						" join tbl_lst_fileJuris d on d.Jurisdiction_ID=c.JurId " +
						" OUTER APPLY ( " +
						" Select min(isnull(TargetDayOffset,0)) As Offset from trProjectJurisdictionalData a with (nolock) join JurisdictionalData b with (nolock) " +
						" on a.re_Primary_ = b.Primary_ " +
						" left join tbl_lst_Juris_TargetOffset c on b.JurisdictionId=c.JurId Where ProjectID = " + projectID;

			if (!string.IsNullOrEmpty(jurList))
			{
				query = query + " and a.re_Primary_ in (" + jurList + ")";
			}

			query = query + " ) minOffset" +
						" Where b.jurisdictionType not in ('OH') and a.ProjectID = " + projectID +
						" and exists (Select 1 from trProject with (nolock) where ProjectID=" + projectID + " and engCompleteDate is null)";

			if (!string.IsNullOrEmpty(jurList))
			{
				query = query + " and a.re_Primary_ in (" + jurList + ")";
			}

			query += ") final where Datediff(d,NewTarget,'" + targetDate + "')<> 0 ";

			var results = _dbSubmission.Database.SqlQuery<string>(query);
			return Json(results, JsonRequestBehavior.AllowGet);
		}

		[HttpGet]
		public JsonResult GetJurisdictions(int projectID)
		{
			var jurisdictions = from m in _dbSubmission.trProjectJurisdictionalData
								where m.ProjectId == projectID
								orderby m.JurisdictionalData.JurisdictionName
								select new
								{
									Id = m.JurisdictionalData.JurisdictionId.ToString(),
									Name = m.JurisdictionalData.JurisdictionName.ToString()
								};

			return Json(jurisdictions.Distinct().OrderBy(m => m.Name), JsonRequestBehavior.AllowGet);
		}

		public JsonResult GetJurData(int projectId, string jurlist)
		{
			Session["ProjectId"] = projectId;
			Session["JurIds"] = jurlist;
			return (LoadProjectJurisdictionsGrid());
		}
		#region BatchTarget

		public ActionResult BatchTargetDate(string projectIds)
		{
			var vm = new BatchProjectTarget
			{
				ProjectIds = projectIds,
				ApplyToProject = 1,
				ReviseMode = TargetDateReviseMode.ModifyCurrentTarget,
				Reason = "",
				Detail = ""
			};

			return PartialView("~/Areas/Protrack/Views/TargetDate/_BatchTargetDate.cshtml", vm);
		}

		public ActionResult BatchOriginalTargetDate(string projectIds)
		{
			var projectList = new List<int>(projectIds.Split(',').Select(x => int.Parse(x)));
			var projects = _dbSubmission.trProjects.Where(p => projectList.Contains(p.Id)).ToList();
			var ineligibleEditProjects = projects.Where(p => !AllowEditOriginalDate(p.ENAcceptDate))
													.Select(p => p.FileNumber)
													.ToList();

			if (!ineligibleEditProjects.Any())
			{
				var vm = new BatchProjectTarget
				{
					ProjectIds = projectIds,
					ApplyToProject = 1,
					ReviseMode = TargetDateReviseMode.ModifyOriginalTarget,
					Reason = "Other",
					Detail = ""
				};
				return PartialView("~/Areas/Protrack/Views/TargetDate/_BatchTargetDate.cshtml", vm);
			}
			else
			{
				return PartialView("~/Areas/Protrack/Views/TargetDate/_CannotUpdate.cshtml", string.Join("\n", ineligibleEditProjects));
			}
		}

		public ContentResult SaveBatchTarget(BatchProjectTarget batchModel)
		{
			if (!string.IsNullOrEmpty(batchModel.ProjectIds))
			{
				var listProject = new List<int>(batchModel.ProjectIds.Split(',').Select(x => int.Parse(x)));
				foreach (var projectId in listProject)
				{
					SaveProjectTarget(projectId, "", _userContext.User.Id, (DateTime)batchModel.NewTargetDate, batchModel.Detail, batchModel.ReasonId, (int)batchModel.ReviseMode, true);
				}
			}

			return Content("OK");
		}
		#endregion

		#region Load Grid Data
		[OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
		public JsonResult LoadProjectJurisdictionsGrid()
		{
			var projectID = (int)Session["ProjectID"];
			var jurId = (Session["jurIds"] != null) ? (string)Session["jurIds"] : "";

			var query = _dbSubmission.trProjectJurisdictionalData.Where(t => t.ProjectId.Equals(projectID));
			if (!string.IsNullOrEmpty(jurId))
			{
				var selJur = jurId.Split(',');
				query = query.Where(t => selJur.Contains((string)t.JurisdictionalData.JurisdictionId));
			}
			query = query.OrderBy(x => x.JurisdictionalData.JurisdictionName).AsQueryable();
			var results = query.ProjectTo<ProjectJurisdictionViewModel>();
			var jurisdictionGrid = new ProjectJurisdictionsGrid();
			return jurisdictionGrid.Grid.DataBind(results);
		}
		#endregion

		private static bool AllowEditOriginalDate(DateTime? dateTime)
		{
			if (!dateTime.HasValue)
			{
				return false;
			}
			TimeSpan elapsedTime = DateTime.Now.Date - dateTime.Value.Date;
			return elapsedTime.Days <= 3 ? true : false;
		}
	}
}