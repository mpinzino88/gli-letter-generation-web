﻿extern alias ZEFCore;
using AutoMapper;
using GLI.EFCore.Submission;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Submissions.Common;
using Submissions.Core;
using Submissions.Web.Areas.Protrack.Models.Task;
using Submissions.Web.Areas.Protrack.Models.Task.KnockoutModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Web.Mvc;
using ZEFCore::Z.EntityFramework.Plus;

namespace Submissions.Web.Areas.Protrack.Controllers
{
	using Submissions.Common.Email;
	using System.Net;

	[AuthorizeUser(Permission = Permission.EngTasks, HasAccessRight = AccessRight.Edit)]
	public class TasksController : Controller
	{
		private readonly SubmissionContext _dbSubmission;
		private readonly IUserContext _userContext;
		private readonly IEmailService _emailService;
		private readonly IProjectService _projectService;
		private readonly ILogService _logService;
		private readonly IMathService _mathService;

		public TasksController
		(
			SubmissionContext dbSubmission,
			IUserContext userContext,
			IProjectService projectService,
			IEmailService emailService,
			ILogService logService,
			IMathService mathService
		)
		{
			_dbSubmission = dbSubmission;
			_userContext = userContext;
			_emailService = emailService;
			_projectService = projectService;
			_logService = logService;
			_mathService = mathService;
		}

		#region Edit
		public ActionResult Edit(int id, int sourceMathReview = 0, int sourceMathComplete = 0)
		{
			var project = _dbSubmission.trProjects
				.Where(x => x.Id == id)
				.Select(x => new
				{
					Id = x.Id,
					FileNum = x.ProjectName,
					Status = x.ENStatus,
					AcceptDate = x.ENAcceptDate,
					IsTransfer = (bool)x.IsTransfer,
					TargetDate = x.TargetDate,
					ApplyTargetOffSet = x.ApplyTargetOffSet,
					OrgTargetDate = x.OrgTargetDate,
					OnHold = x.OnHold,
					TestLab = x.Lab,
					CreateDate = x.AddDate,
					QADeliveryDate = x.QADeliveryDate
				})
				.Single();

			var vm = new EditViewModel();

			if (project.IsTransfer)
			{
				var transferProjectJurisdictions =
				_dbSubmission.trProjectJurisdictionalData.Where(x => x.ProjectId == id)
				.Select(y => new SelectListItem
				{
					Text = y.JurisdictionalData.Jurisdiction.Name,
					Value = y.JurisdictionalData.Jurisdiction.Id
				})
				.Distinct()
				.ToList();

				vm.JurisdictionsFilter.JurisdictionIds.AddRange(transferProjectJurisdictions.Select(x => x.Value).ToList());
				vm.TransferProjectJurisdictions = transferProjectJurisdictions;
			}

			vm.onHold = (project.OnHold == 1) ? true : false;
			vm.ProjectId = project.Id;
			vm.FileNumber = project.FileNum;
			vm.ProjectENStatus = project.Status;
			vm.TargetDate = project.TargetDate;
			vm.QADeliveryDate = project.QADeliveryDate;
			vm.OrgTargetDate = project.OrgTargetDate;
			vm.ApplyTargetOffSet = project.ApplyTargetOffSet;
			vm.OrgTargetDateStr = project.OrgTargetDate.ToString("MM/dd/yyyy");
			vm.AllowTaskSave = (project.Status == ProjectStatus.QA.ToString() || project.Status == ProjectStatus.DN.ToString() || project.Status == ProjectStatus.UU.ToString()) ? false : true;
			vm.AllowTargetDate = (project.OnHold != 1 && project.OrgTargetDate == null) ? true : false;
			vm.HasUKTestLab = project.TestLab == Laboratory.UK.ToString() ? true : false;
			if (sourceMathReview != 0)
			{
				var completionRequested = _dbSubmission.trMathSubmits.Where(x => x.ProjectId == project.Id && x.Id == sourceMathReview).Select(x => x.CompletionRequested).SingleOrDefault();
				vm.RequestedDate = completionRequested != null ? completionRequested.ToString("d") : "";
			}
			if (project.Status != ProjectStatus.UU.ToString())
			{
				vm.ProjectAcceptDate = (DateTime)project.AcceptDate;
				var hoursSinceAcceptance = (DateTime.Now - (DateTime)project.AcceptDate).TotalHours;
				vm.HoursSinceAcceptance = hoursSinceAcceptance;
				vm.TaskLockEnforced = hoursSinceAcceptance > 96;
			}
			vm.ProjectCreateDate = (DateTime)project.CreateDate;
			vm.SourceMathReview = sourceMathReview;
			vm.SourceMathComplete = sourceMathComplete;
			vm.isTransfer = project.IsTransfer;
			vm.TemplateOwnerId = _userContext.User.Id;
			vm.TemplateOwner = _userContext.User.FName + ' ' + _userContext.User.LName;
			if (vm.TemplateOwnerId != null)
			{
				vm.TaskTemplatesFilter.UserIds.Add((int)vm.TemplateOwnerId);
			}

			var KOTasks = RetriveTasksForProjectAsKOTask(id);

			if (KOTasks.Count > 0)
			{
				vm.EngineeringTasks = KOTasks.Where(x => x.DepartmentId == (int)Department.ENG).ToList();
				vm.MathTasks = KOTasks.Where(x => x.DepartmentId == (int)Department.MTH).ToList();
				vm.FieldInspectionTasks = KOTasks.Where(x => x.DepartmentId == (int)Department.INS).ToList();
			}

			vm.DeactivatedTasks = RetrieveDeactivatedTasksForProjectAsKOTask(vm.ProjectId);

			vm.DefaultETATasks = _dbSubmission.trTaskDefs.Where(x => x.IsETATask).Select(x => x.Id).ToList();

			return View(vm);
		}

		[HttpPost]
		public ActionResult Edit(string editViewModel)
		{
			var vm = JsonConvert.DeserializeObject<EditViewModel>(editViewModel);
			ApplyChanges(vm);
			if (vm.AllowTargetDate == true && vm.OrgTargetDate != null)
			{
				_dbSubmission.trProjects.Where(x => x.Id == vm.ProjectId).Update(x => new trProject() { ApplyTargetOffSet = vm.ApplyTargetOffSet });
				_projectService.UpdateProjectJurisdictionTargetDate(vm.ProjectId, "", _userContext.User.Id, vm.OrgTargetDate.Value.Date, "Original Target Date", 0, (int)TargetDateReviseMode.EnterOriginalTarget);
			}
			return RedirectToAction("Edit", new { id = vm.ProjectId, sourceMathReview = vm.SourceMathReview, sourceMathComplete = vm.SourceMathComplete });
		}

		[HttpPost]
		public ActionResult EditDesc(string editViewModel)
		{
			var vm = JsonConvert.DeserializeObject<EditViewModel>(editViewModel);
			var tasks = vm.EngineeringTasks;
			var mathTasks = vm.MathTasks;
			var allTasks = tasks.Concat(mathTasks);
			foreach (var task in allTasks)
			{
				var currTask = _dbSubmission.trTasks.Where(x => x.Id == task.Id).Single();
				currTask.Description = task.Description;
				_dbSubmission.trTasks.Attach(currTask);
				_dbSubmission.Entry(currTask).Property(x => x.Description).IsModified = true;
				_dbSubmission.SaveChanges();
			}
			return RedirectToAction("Edit", new { id = vm.ProjectId });
		}
		#endregion

		#region Adding Tasks
		[HttpPost]
		public JsonResult AddTaskTemplate(TemplateFilters filter)
		{
			try
			{
				var tasksForReturn = new List<KOTask>();
				var returnedTasks = _projectService.GetTaskTemplate(filter.TemplateId);

				foreach (var task in returnedTasks)
				{
					var resultKOTask = Mapper.Map<KOTask>(task);
					resultKOTask.ProjectId = filter.ProjectId;
					resultKOTask.Note = filter.AddReason;
					resultKOTask.TaskDef = new SelectListItem { Value = task.TaskDefinitionId.ToString(), Text = task.Name };

					tasksForReturn.Add(resultKOTask);
				}

				Response.StatusCode = (int)HttpStatusCode.OK;

				if (tasksForReturn.Any(x => !x.IsAcumaticaSet))
				{
					return Json("TaskMismatch");
				}
				else
				{
					return Json(JsonConvert.SerializeObject(tasksForReturn));
				}

			}
			catch (Exception ex)
			{
				Response.StatusCode = (int)HttpStatusCode.InternalServerError;
				return Json(JsonConvert.SerializeObject(ex));
				throw;
			}
		}

		[HttpPost]
		public JsonResult AddTask(TaskFilters filters, int sourceMathReview = 0, int sourceMathComplete = 0)
		{
			var taskDefinition = _dbSubmission.trTaskDefs
				.Where(x => x.Id == filters.TaskDefinitionId)
				.Select(x => new
				{
					Id = x.Id,
					DepartmentId = x.DepartmentId,
					Description = x.Description,
					Code = x.Code
				})
				.Single();

			var newTask = new trTask
			{
				AddUserId = _userContext.User.Id,
				AddDate = DateTime.Now,
				DepartmentId = taskDefinition.DepartmentId,
				Description = filters.CustomDescription != null ? filters.CustomDescription : taskDefinition.Description,
				EstimateHours = (filters.EstimatedHours == null) ? 0 : filters.EstimatedHours,
				Name = taskDefinition.Code,
				ProjectId = filters.ProjectId,
				TargetDate = filters.TaskTargetDate == null ? (DateTime?)null : (Convert.ToDateTime(filters.TaskTargetDate)).Date,
				TaskDefinitionId = taskDefinition.Id,
				Note = filters.TaskChangeReason,
				UserId = filters.UserId,
				ETAQueue = filters.ETAQueue,
			};

			try
			{
				_projectService.AddTask(newTask);

				if (filters.TaskLockEngaged)
				{
					_logService.AddProjectHistory(
						new trHistory
						{
							ProjectId = filters.ProjectId,
							Notes = "Task " + newTask.Name + " added after 96 hours"
						}
					);
				}

				if (newTask.User == null)
					_dbSubmission.Entry(newTask).Reference(p => p.User).Load();
				if (newTask.Definition == null)
					_dbSubmission.Entry(newTask).Reference(p => p.Definition).Load();

				var resultKOTask = Mapper.Map<KOTask>(newTask);
				resultKOTask.TaskDef = new SelectListItem { Value = newTask.TaskDefinitionId.ToString(), Text = newTask.Name };
				resultKOTask.User = new SelectListItem { Value = newTask.UserId.ToString(), Text = newTask.User == null ? "" : newTask.User.FName + ' ' + newTask.User.LName };
				resultKOTask.TargetDateString = resultKOTask.TargetDate.ToString();

				var taskAssigned = newTask.StartDate == null && newTask.AssignDate != null && newTask.UserId != null ? true : false;
				if (taskAssigned)
				{
					_projectService.SendEmailForAssignedTasks(new List<trTask>() { newTask }, newTask.ProjectId, sourceMathReview, sourceMathComplete);
					//Associate with task
					AssociateTasksToMath(new List<trTask>() { newTask }, sourceMathReview, sourceMathComplete);

				}

				Response.StatusCode = (int)HttpStatusCode.OK;
				return Json(JsonConvert.SerializeObject(resultKOTask));
			}
			catch (Exception ex)
			{
				Response.StatusCode = (int)HttpStatusCode.InternalServerError;
				return Json(JsonConvert.SerializeObject(ex));
				throw;
			}
		}
		#endregion

		#region Removing Tasks
		[HttpPost]
		public JsonResult RemoveTask(KOTask task)
		{
			if (task.Id != null)
			{
				var dbTask = _dbSubmission.trTasks.Single(x => x.Id == task.Id);
				try
				{
					_projectService.DeleteTask(dbTask);

					Response.StatusCode = (int)HttpStatusCode.OK;
					return Json(JsonConvert.SerializeObject(task));
				}
				catch (Exception ex)
				{
					Response.StatusCode = (int)HttpStatusCode.InternalServerError;
					return Json(JsonConvert.SerializeObject(ex));
					throw;
				}
			}
			Response.StatusCode = (int)HttpStatusCode.BadRequest;
			return Json(JsonConvert.SerializeObject(new { responseText = "Task Id was null" }));
		}

		[HttpPost]
		public JsonResult RemoveTasks(DeleteTasksFilter filter)
		{
			if (filter.Tasks != null)
			{
				var forDeletion = filter.Tasks.Count;
				var deptId = filter.Tasks.First().DepartmentId;
				var taskIds = filter.Tasks.Select(x => (int)x.Id).ToList();
				var dbTasks = _dbSubmission.trTasks.Where(x => taskIds.Contains(x.Id)).ToList();

				if (forDeletion > 0)
				{
					try
					{
						_projectService.DeleteTasks(dbTasks);

						Response.StatusCode = (int)HttpStatusCode.OK;
						return Json(JsonConvert.SerializeObject(new { result = true, departmentId = deptId, count = forDeletion, tasks = filter.Tasks }));
					}
					catch (Exception ex)
					{
						Response.StatusCode = (int)HttpStatusCode.InternalServerError;
						return Json(JsonConvert.SerializeObject(ex));
						throw;
					}
				}
			}
			Response.StatusCode = (int)HttpStatusCode.BadRequest;
			return Json(JsonConvert.SerializeObject(new { responseText = "No tasks supplied." }));
		}

		[HttpPost]
		public JsonResult ReloadDeactivatedTasks(int projectId)
		{
			try
			{
				var tasksForReturn = RetrieveDeactivatedTasksForProjectAsKOTask(projectId);

				Response.StatusCode = (int)HttpStatusCode.OK;
				return Json(JsonConvert.SerializeObject(tasksForReturn));
			}
			catch (Exception ex)
			{
				Response.StatusCode = (int)HttpStatusCode.InternalServerError;
				return Json(JsonConvert.SerializeObject(ex));
				throw;
			}
		}
		#endregion

		#region Move Tasks
		[HttpPost]
		public JsonResult MoveTasks(MoveTaskPayload payload)
		{
			var anonSourceProject = _dbSubmission.trProjects
				.Where(x => x.ProjectName == payload.sourceProject)
				.Select(x => new { ProjectId = x.Id, ProjectAcceptDate = (DateTime)x.ENAcceptDate }).Single();

			var anonDestinationProject = _dbSubmission.trProjects
				.Where(x => x.ProjectName == payload.destinationProject)
				.Select(x => new { ProjectId = x.Id, ProjectAcceptDate = (DateTime)x.ENAcceptDate }).Single();

			// -- Can not use parameterized constructors in LINQ to Entities due to the lack of translation to SQL, 
			// -- so select to anon types and convert to Strong Types after.
			var sourceProject = new ProjectValidator(anonSourceProject.ProjectId, anonSourceProject.ProjectAcceptDate);
			var destinationProject = new ProjectValidator(anonDestinationProject.ProjectId, anonDestinationProject.ProjectAcceptDate);

			var MoveTasksOperation = new MoveTaskOperation(sourceProject, destinationProject);

			if (MoveTasksOperation.IsValid())
			{
				var tasksForUpdate = _dbSubmission.trTasks.Where(x => payload.tasksIds.Contains(x.Id)).ToList();

				if (MoveTasksOperation.DestinationProjectIsLocked)
				{
					foreach (var task in tasksForUpdate)
					{
						task.EstimateHours = 0;
					}
				}

				try
				{
					_projectService.MoveTasks(sourceProject.ProjectId, destinationProject.ProjectId, tasksForUpdate);
					Response.StatusCode = (int)HttpStatusCode.OK;
					return Json(JsonConvert.SerializeObject(tasksForUpdate.Select(x => new { DeptId = x.DepartmentId, TaskId = x.Id }).ToList()));
				}
				catch (Exception ex)
				{
					Response.StatusCode = (int)HttpStatusCode.InternalServerError;
					return Json(JsonConvert.SerializeObject(ex));
					throw;
				}
			}
			else
			{
				Response.StatusCode = (int)HttpStatusCode.BadRequest;
				return Json(JsonConvert.SerializeObject(new { Error = "Source Projects tasklock is engaged. Tasks are not allowed to move" }));
			}
		}


		public class MoveTaskPayload
		{
			public MoveTaskPayload()
			{
				this.tasksIds = new List<int>();
			}

			public string destinationProject { get; set; }
			public string sourceProject { get; set; }
			public List<int> tasksIds { get; set; }
		}

		public class MoveTaskOperation
		{
			public MoveTaskOperation(ProjectValidator Source, ProjectValidator Destination)
			{
				this.Source = Source;
				this.Destination = Destination;
				this.SourceProjectIsLocked = Source.TaskLockEngaged;
				this.DestinationProjectIsLocked = Destination.TaskLockEngaged;
			}

			public bool IsValid()
			{
				if (SourceProjectIsLocked)
				{
					return false;
				}
				else
				{ return true; }
			}

			public ProjectValidator Source { get; set; }
			public ProjectValidator Destination { get; set; }
			public bool SourceProjectIsLocked { get; set; }
			public bool DestinationProjectIsLocked { get; set; }
		}

		public class ProjectValidator
		{
			public ProjectValidator()
			{
				var hourssince = (DateTime.Now - (DateTime)this.ProjectAcceptDate).TotalHours;
				this.TaskLockEngaged = hourssince > 96;
			}

			public ProjectValidator(int ProjectId, DateTime ProjectAcceptDate)
			{
				this.ProjectId = ProjectId;
				this.ProjectAcceptDate = ProjectAcceptDate;
				var hourssince = (DateTime.Now - (DateTime)this.ProjectAcceptDate).TotalHours;
				this.TaskLockEngaged = hourssince > 96;
			}
			public int ProjectId { get; set; }
			public DateTime ProjectAcceptDate { get; set; }
			public bool TaskLockEngaged { get; set; }
		}
		#endregion

		public JsonResult CreateTemplateFromTasks(int projectId, string newTemplateName, string newTemplateDescription)
		{
			var newTemplate = new trTaskTemplate // Create and save new template obj
			{
				Code = newTemplateName,
				Description = newTemplateDescription,
				UserId = _userContext.User.Id
			};
			_dbSubmission.trTaskTemplates.Add(newTemplate);
			_dbSubmission.SaveChanges();

			var currentTaskSet = _dbSubmission.trTasks //get current tasks, create new copies, add to the template
				.Where(x => x.ProjectId == projectId && (x.DepartmentId == (int)Department.ENG || x.DepartmentId == (int)Department.MTH || x.DepartmentId == (int)Department.INS)).ToList();
			var newLinks = new List<trTaskLink>();
			foreach (var task in currentTaskSet)
			{
				newLinks.Add(new trTaskLink
				{
					TaskDefinitionId = (int)task.TaskDefinitionId,
					Description = task.Description,
					EstimateHours = task.EstimateHours,
					TaskTemplateId = newTemplate.Id,
					IsETA = task.ETAQueue
				});
			}
			_dbSubmission.trTaskLinks.AddRange(newLinks);
			_dbSubmission.SaveChanges();

			return Json(new { Message = newTemplate.Id }, JsonRequestBehavior.AllowGet);
		}
		public JsonResult ValidateTaskTemplateName(string name)
		{
			name = name.TrimStart(' ').TrimEnd(' ');
			var result = _dbSubmission.trTaskTemplates.Where(x => x.Code == name).Count() == 0;
			return Json(result, JsonRequestBehavior.AllowGet);
		}

		[HttpGet]
		public JsonResult CheckPotentialClonedTasks(int targetProjectId, int currentProjectId)
		{
			var taskMismatch = false;
			var targetTaskSet = _dbSubmission.trTasks
				.Where(x => x.ProjectId == targetProjectId && (x.DepartmentId == (int)Department.ENG || x.DepartmentId == (int)Department.MTH || x.DepartmentId == (int)Department.INS)).ToList();

			var currentProjectCreate = _dbSubmission.trProjects.Where(x => x.Id == currentProjectId).Select(x => x.AddDate).SingleOrDefault();

			taskMismatch = targetTaskSet.Any(x => !x.Definition.IsAcumaticaSet);
			targetTaskSet = targetTaskSet.Where(x => x.Definition.IsAcumaticaSet).ToList();

			var taskCountByDept = new
			{
				EngineeringTasks = targetTaskSet.Where(x => x.DepartmentId == (int)Department.ENG).Count(),
				MathTasks = targetTaskSet.Where(x => x.DepartmentId == (int)Department.MTH).Count(),
				FieldInsTasks = targetTaskSet.Where(x => x.DepartmentId == (int)Department.INS).Count(),
				TaskMismatch = taskMismatch
			};
			return Json(taskCountByDept, JsonRequestBehavior.AllowGet);
		}

		public void CloneTasksFromTargetProject(int currentProject, int targetProject)
		{
			var targetTaskSet = _dbSubmission.trTasks
				.Where(x => x.ProjectId == targetProject && (x.DepartmentId == (int)Department.ENG || x.DepartmentId == (int)Department.MTH || x.DepartmentId == (int)Department.INS)).ToList();

			targetTaskSet = targetTaskSet.Where(x => x.Definition.IsAcumaticaSet).ToList();

			var project = _dbSubmission.trProjects.Where(x => x.Id == currentProject).Single();
			var hoursSinceAcceptance = (DateTime.Now - (DateTime)project.ENAcceptDate).TotalHours;
			var taskLockEnforced = hoursSinceAcceptance > 96;

			var newTasksForCurrentProject = new List<trTask>();
			foreach (var item in targetTaskSet)
			{
				newTasksForCurrentProject.Add(new trTask
				{
					AddUserId = _userContext.User.Id,
					Description = item.Description,
					TaskDefinitionId = item.TaskDefinitionId,
					OriginalEstimateHours = item.OriginalEstimateHours,
					Name = item.Name,
					ProjectId = currentProject,
					AddDate = DateTime.Now,
					DepartmentId = item.DepartmentId,
					EstimateHours = taskLockEnforced ? null : item.EstimateHours
					// Add more as needed?
				});
			}
			_dbSubmission.trTasks.AddRange(newTasksForCurrentProject);
			_dbSubmission.SaveChanges();
		}

		#region Associate Tasks to Math
		private void AssociateTasksToMath(List<trTask> modifiedTasks, int sourceMathSubmit = 0, int sourceMathComplete = 0)
		{

			foreach (var task in modifiedTasks)
			{
				var assignedTo = task.UserId;
				var mathAnalysisTaskExist = ((task.TaskDefinitionId == (int)MathTasks.MathAnalysis || task.TaskDefinitionId == (int)MathTasks.MathAnalysisDynamics)) ? true : false;
				var mathReviewTaskExists = (task.TaskDefinitionId == (int)MathTasks.MathReview || task.TaskDefinitionId == (int)MathTasks.MathReviewDynamics) ? true : false;

				if (mathAnalysisTaskExist == true && sourceMathSubmit > 0)
				{
					var mathAnalysisId = _dbSubmission.trTasks.Where(x => x.UserId == assignedTo && (x.TaskDefinitionId == (int)MathTasks.MathAnalysis || x.TaskDefinitionId == (int)MathTasks.MathAnalysisDynamics)).OrderByDescending(x => x.AssignDate).Select(x => x.Id).FirstOrDefault();
					var mathSubmit = _dbSubmission.trMathSubmits.Where(x => x.Id == sourceMathSubmit).SingleOrDefault();
					mathSubmit.TaskId = mathAnalysisId;
					_mathService.UpdateMathSubmit(mathSubmit);

				}

				if (mathReviewTaskExists == true && sourceMathComplete > 0)
				{
					var mathReviewId = _dbSubmission.trTasks.Where(x => x.UserId == assignedTo && (x.TaskDefinitionId == (int)MathTasks.MathReview || x.TaskDefinitionId == (int)MathTasks.MathReviewDynamics)).OrderByDescending(x => x.AssignDate).Select(x => x.Id).FirstOrDefault();
					var mathComplete = _dbSubmission.trMathCompletes.Where(x => x.Id == sourceMathComplete).SingleOrDefault();
					mathComplete.TaskId = mathReviewId;
					_mathService.UpdateMathComplete(mathComplete);
				}
			}
		}
		#endregion

		#region Query Functions
		private List<KOTask> RetriveTasksForProjectAsKOTask(int ProjectId)
		{
			var tasks = _dbSubmission.trTasks
				.Where(x => x.ProjectId == ProjectId && (x.DepartmentId == (int)Department.ENG || x.DepartmentId == (int)Department.MTH || x.DepartmentId == (int)Department.INS))
				.Select(x => new KOTask
				{
					Id = x.Id,
					TaskDefinitionId = x.TaskDefinitionId,
					ProjectId = x.ProjectId,
					UserId = x.UserId,
					AssignedUserId = x.UserId,
					Name = x.Name,
					Description = x.Description,
					DepartmentId = x.DepartmentId,
					AssignDate = x.AssignDate,
					StartDate = x.StartDate,
					EstimatedCompleteDate = x.EstimateCompleteDate,
					CompleteDate = x.CompleteDate,
					EstimatedHours = x.EstimateHours,
					InitialEstimatedHours = x.EstimateHours,
					EstimatedInterval = x.EstimateInterval,
					Note = x.Note,
					OriginalEstimatedHours = x.OriginalEstimateHours,
					TargetDate = x.TargetDate,
					CreatedById = x.AddUserId,
					CreatedDate = x.AddDate,
					ComponentGroupId = x.ComponentGroupId,
					ProjectIsTransfer = x.Project.IsTransfer,
					FileNumber = x.Project.ProjectName,
					User = new SelectListItem { Value = x.UserId.ToString(), Text = x.User.FName + " " + x.User.LName },
					TaskDef = new SelectListItem { Value = x.TaskDefinitionId.ToString(), Text = x.Definition.Description + " - " + x.Definition.DynamicsId },
					AssociatedJurisdictions = x.TaskJurisdictions.Select(y => y.JurisdictionId).ToList(),
					IsCPReviewTaskTiedToJur = (x.TaskDefinitionId == (int)ProtrackReviewTask.CPOutGoing || x.TaskDefinitionId == (int)ProtrackReviewTask.CPOutGoingDynamics) && x.TaskJurisdictions.Any(),
					IsETATask = x.ETAQueue
				})
				.ToList();
			foreach (var task in tasks)
			{
				task.TargetDateString = task.TargetDate.ToString("MM/dd/yyyy");
			}
			return tasks;
		}

		private List<KOTask> RetriveTasksForProjectAsKOTask(List<int> tasks)
		{
			return
				_dbSubmission.trTasks
				.Where(x => tasks.Contains(x.Id))
				.Select(x => new KOTask
				{
					Id = x.Id,
					TaskDefinitionId = x.TaskDefinitionId,
					ProjectId = x.ProjectId,
					UserId = x.UserId,
					AssignedUserId = x.UserId,
					Name = x.Name,
					Description = x.Description,
					DepartmentId = x.DepartmentId,
					AssignDate = x.AssignDate,
					StartDate = x.StartDate,
					EstimatedCompleteDate = x.EstimateCompleteDate,
					CompleteDate = x.CompleteDate,
					EstimatedHours = x.EstimateHours,
					InitialEstimatedHours = x.EstimateHours,
					EstimatedInterval = x.EstimateInterval,
					Note = x.Note,
					OriginalEstimatedHours = x.OriginalEstimateHours,
					TargetDate = x.TargetDate,
					CreatedById = x.AddUserId,
					CreatedDate = x.AddDate,
					ComponentGroupId = x.ComponentGroupId,
					ProjectIsTransfer = x.Project.IsTransfer,
					FileNumber = x.Project.ProjectName,
					User = new SelectListItem { Value = x.UserId.ToString(), Text = x.User.FName + " " + x.User.LName },
					TaskDef = new SelectListItem { Value = x.TaskDefinitionId.ToString(), Text = x.Definition.Description + " - " + x.Definition.DynamicsId },
					AssociatedJurisdictions = x.TaskJurisdictions.Select(y => y.JurisdictionId).ToList()
				})
				.ToList();
		}

		private IQueryable<trTask> RetriveTasksForProjectAsEFTask(int ProjectId)
		{
			return
				_dbSubmission.trTasks
				.Where(x => x.ProjectId == ProjectId && (x.DepartmentId == (int)Department.ENG || x.DepartmentId == (int)Department.MTH))
				.AsQueryable();
		}

		private List<KOTask> RetrieveDeactivatedTasksForProjectAsKOTask(int ProjectId)
		{
			return Mapper.Map<List<KOTask>>(_dbSubmission.trTaskDeletions.Where(x => x.ProjectId == ProjectId).ToList());
		}

		#endregion

		#region Helper Functions
		private void ApplyChanges(EditViewModel vm)
		{
			var tasks = BuildMasterTaskList(vm);
			var tasksToCreate = BuildTasksToCreate(tasks);
			var tasksToUpdate = BuildTasksToUpdate(tasks, vm.SourceMathReview, vm.SourceMathComplete);
			var tasksToDelete = BuildTasksToDelete(tasks);
			var tasksAssigned = new List<trTask>();
			var histories = new List<trHistory>();
			var tasksCreated = new List<int>();

			if (tasksToCreate.Count > 0)
			{
				_projectService.AddTasks(tasksToCreate);
				tasksAssigned.AddRange(tasksToCreate.Where(x => x.StartDate == null && x.AssignDate != null && x.UserId != null).ToList());
				tasksCreated = tasksToCreate.Select(x => x.Id).ToList();

				if (tasksToCreate.Any(x => x.ETAQueue && x.User == null))
				{
					SendEmail(vm, tasksToCreate);
				}

				if (vm.TaskLockEnforced)
				{
					var creations = _dbSubmission.trTasks
						.Where(x => tasksCreated.Contains(x.Id))
						.Select(x => new
						{
							ProjectId = x.ProjectId,
							Notes = "Task " + x.Name + " added after 96 hours"
						})
						.ToList();

					foreach (var a in creations)
					{
						histories.Add(new trHistory
						{
							ProjectId = a.ProjectId,
							Notes = a.Notes
						});
					}
				}
			}
			if (tasksToDelete.Count > 0)
			{
				_projectService.DeleteTasks(tasksToDelete);
			}
			if (tasksToUpdate.Count > 0)
			{
				_projectService.UpdateTasks(tasksToUpdate);

				if (tasksToUpdate.Any(x => x.ETAQueue && x.User == null))
				{
					SendEmail(vm, tasksToUpdate);
				}

				if (vm.TaskLockEnforced)
				{
					var updates = tasksToUpdate
						.Select(x => new
						{
							ProjectId = x.ProjectId,
							Notes = "Task " + x.Name + " updated after 96 hours due to: " + x.Note
						})
						.ToList();

					foreach (var a in updates)
					{
						histories.Add(new trHistory
						{
							ProjectId = a.ProjectId,
							Notes = a.Notes
						});
					}
				}
			}

			if (vm.TaskLockEnforced)
			{
				_logService.AddProjectHistory(histories);
				_dbSubmission.SaveChanges();
			}

			if (tasksAssigned.Count > 0)
			{
				_projectService.SendEmailForAssignedTasks(tasksAssigned, tasksAssigned.First().ProjectId, vm.SourceMathReview, vm.SourceMathComplete);
				//Associate with task
				AssociateTasksToMath(tasksAssigned, vm.SourceMathReview, vm.SourceMathComplete);
			}
		}

		private ActionResult SendEmail(EditViewModel vm, IList<trTask> tasks)
		{
			var mailMsg = new MailMessage("noreply@gaminglabs.com", _dbSubmission.trLogins.Where(x => x.Id == _userContext.User.Id).Select(x => x.Email).SingleOrDefault());
			mailMsg.Subject = "ETA Task Created";
			mailMsg.Body = "You have added following to the ETA Queue: <br/><br/> <b>Tasks: </b>" + String.Join(", ", tasks.Where(x => x.ETAQueue && x.User == null).Select(x => x.Description).ToList()) + "<br/> <b>Filenumber: </b>" + vm.FileNumber;

			_emailService.Send(mailMsg);
			return null;
		}
		#endregion

		#region Task Builders
		private List<KOTask> BuildMasterTaskList(EditViewModel vm)
		{
			var tasks = new List<KOTask>();

			if (vm.TaskLockEnforced)
			{
				foreach (var ENTask in vm.EngineeringTasks)
				{
					if (!String.IsNullOrWhiteSpace(ENTask.TaskChangeReason))
					{
						ENTask.Note = ENTask.TaskChangeReason;
					}
				}

				foreach (var FITask in vm.FieldInspectionTasks)
				{
					if (!String.IsNullOrWhiteSpace(FITask.TaskChangeReason))
					{
						FITask.Note = FITask.TaskChangeReason;
					}
				}

				foreach (var MathTask in vm.MathTasks)
				{
					if (!String.IsNullOrWhiteSpace(MathTask.TaskChangeReason))
					{
						MathTask.Note = MathTask.TaskChangeReason;
					}
				}
			}

			tasks.AddRange(vm.EngineeringTasks.ToList());
			tasks.AddRange(vm.FieldInspectionTasks.ToList());
			tasks.AddRange(vm.MathTasks.ToList());

			return tasks;
		}

		private IList<trTask> BuildTasksToCreate(List<KOTask> tasks)
		{
			var taskList = new List<trTask>();
			var tasksToAdd = tasks.Where(x => !x._destroy && (x.Id == null || x.Id == 0) && x.TaskDefinitionId != null).ToList();
			foreach (var task in tasksToAdd)
			{
				var taskJurisdictions = new List<trTransferTaskJur>();
				foreach (var jurisdiction in task.AssociatedJurisdictions)
				{
					taskJurisdictions.Add(new trTransferTaskJur
					{
						JurisdictionId = jurisdiction,
						jurisNumber = int.Parse(jurisdiction)
					});
				}

				taskList.Add(new trTask
				{
					DepartmentId = task.DepartmentId,
					Description = task.Description,
					EstimateCompleteDate = task.EstimatedCompleteDate,
					EstimateHours = task.EstimatedHours,
					EstimateInterval = task.EstimatedInterval,
					Name = task.Name,
					Note = task.Note,
					ProjectId = task.ProjectId,
					TargetDate = task.TargetDate == null ? (DateTime?)null : (Convert.ToDateTime(task.TargetDate)).Date,
					TaskDefinitionId = task.TaskDefinitionId,
					UserId = task.AssignedUserId,
					TaskJurisdictions = taskJurisdictions,
					ETAQueue = task.IsETATask
				});
			}

			return taskList;
		}

		private IList<trTask> BuildTasksToUpdate(List<KOTask> tasks, int sourceMathReview = 0, int sourceMathComplete = 0)
		{
			var taskIds = tasks.Where(x => x._dirty && (x.Id != null && x.Id != 0) && x.TaskDefinitionId != null).Select(x => x.Id).ToList();
			var taskList = new List<trTask>();

			var origTasksInfo = new List<trTask>();

			var dbTasks = _dbSubmission.trTasks.Include(x => x.Definition).Where(x => taskIds.Contains(x.Id)).OrderBy(x => x.Id).ToList();

			var query = dbTasks
			.Join(tasks,
				o => o.Id,
				u => u.Id,
				(o, u) => new { Id = o.Id, oUserId = o.UserId, uUserId = u.AssignedUserId })
			.Where(x => x.oUserId != x.uUserId && x.uUserId != null);

			var taskAssignmentChangedIds = query.Select(x => x.Id).ToList();

			foreach (var item in dbTasks)
			{
				origTasksInfo.Add(new trTask() { Id = item.Id, UserId = item.UserId });

				var koTask = tasks.Where(x => x.Id == item.Id).Single();
				item.DepartmentId = koTask.DepartmentId;
				item.Description = koTask.Description;
				item.EstimateCompleteDate = koTask.EstimatedCompleteDate;
				item.EstimateHours = koTask.EstimatedHours;
				item.EstimateInterval = koTask.EstimatedInterval;
				item.Name = koTask.Name;
				item.Note = koTask.Note;
				item.OriginalEstimateHours = koTask.OriginalEstimatedHours;
				item.ProjectId = koTask.ProjectId;
				item.TargetDate = koTask.TargetDate == null ? (DateTime?)null : (Convert.ToDateTime(koTask.TargetDate)).Date;
				item.TaskDefinitionId = koTask.TaskDefinitionId;
				item.ETAQueue = koTask.IsETATask;

				_dbSubmission.trTransferTaskJurs.Where(x => x.re_taskId == item.Id).Delete();
				if (koTask.AssociatedJurisdictions.Count > 0)
				{
					item.TaskJurisdictions = koTask.AssociatedJurisdictions.Select(x => new trTransferTaskJur { JurisdictionId = x, jurisNumber = Int32.Parse(x), re_taskId = item.Id }).ToList();
				}

				if (item.UserId == null)
				{
					// Unassigned -> Assigned : Set assign date.
					if (koTask.AssignedUserId != null)
					{
						item.AssignDate = DateTime.Now;
						item.UserId = koTask.AssignedUserId;
					}
				}
				else
				{
					// Assigned -> Unassigned : clear assign date
					if (koTask.AssignedUserId == null)
					{
						item.AssignDate = null;
						item.UserId = null;
					}
					// Assigned -> Assigned : Update assign date to now
					else if (koTask.AssignedUserId != item.UserId)
					{
						item.AssignDate = DateTime.Now;
						item.UserId = koTask.AssignedUserId;
					}
				}
			}

			if (taskAssignmentChangedIds.Count > 0)
			{
				var tasksAssigned = dbTasks.Where(x => taskAssignmentChangedIds.Contains(x.Id)).ToList();
				if (tasksAssigned.Count > 0)
				{
					_projectService.SendEmailForReAssignedTasks(tasksAssigned, origTasksInfo, dbTasks.First().ProjectId);
					_projectService.SendEmailForAssignedTasks(tasksAssigned, dbTasks.First().ProjectId, sourceMathReview, sourceMathComplete);
				}
			}

			return dbTasks;
		}

		private List<int> BuildTasksToDelete(List<KOTask> tasks)
		{
			return tasks
				.Where(x => x._destroy && x.Id != null)
				.Select(x => (int)x.Id)
				.ToList();
		}
		#endregion

	}

	#region Filters
	public class TaskFilters
	{
		public int TaskDefinitionId { get; set; }
		public int ProjectId { get; set; }
		public int? UserId { get; set; }
		public string CustomDescription { get; set; }
		public decimal? EstimatedHours { get; set; }
		public DateTime? TaskTargetDate { get; set; }
		public string TaskChangeReason { get; set; }
		public bool TaskLockEngaged { get; set; }
		public bool ETAQueue { get; set; }
	}

	public class TemplateFilters
	{
		public int TemplateId { get; set; }
		public int ProjectId { get; set; }
		public string AddReason { get; set; }
		public bool TaskLockEngaged { get; set; }
		public DateTime ProjectCreateDate { get; set; }
	}

	public class BulkUpdateFilter
	{
		public int DeptId { get; set; }
		public int? UserId { get; set; }
		public int? Estimate { get; set; }
		public DateTime? TargetDate { get; set; }
		public List<int> TaskIds { get; set; }
		public string TaskChangeReason { get; set; }
		public bool TaskLockEngaged { get; set; }
	}

	public class DeleteTasksFilter
	{
		public List<KOTask> Tasks { get; set; }
		public string DeleteReason { get; set; }
		public bool TaskLockEngaged { get; set; }
	}
	#endregion
}