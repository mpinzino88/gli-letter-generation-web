﻿using GLI.EFCore.Submission;
using Submissions.Core;
using Submissions.Web.Areas.Protrack.Models.Project;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace Submissions.Web.Areas.Protrack.Controllers
{
	public class ProjectPersonnelController : Controller
	{
		private readonly IProjectService _projectService;
		private readonly SubmissionContext _dbSubmission;

		public ProjectPersonnelController(IProjectService projectService, SubmissionContext submissionContext)
		{
			_projectService = projectService;
			_dbSubmission = submissionContext;
		}

		public ActionResult Index(int projectId, ProjectPersonnelType personnelType)
		{
			var vm = new ProjectPersonnel();
			vm.ProjectIds = projectId.ToString();
			vm.PersonnelType = personnelType;
			vm.User = "";
			if (personnelType == ProjectPersonnelType.ProjectLead)
			{
				var lead = _dbSubmission.trProjects.Where(x => x.Id == projectId).Select(x => new { Id = x.ENProjectLeadId, Name = x.ENProjectLead.FName + " " + x.ENProjectLead.LName }).Single();
				vm.User = lead.Name;
				vm.UserId = lead.Id;
			}
			else if (personnelType == ProjectPersonnelType.DevRep)
			{
				var devRep = _dbSubmission.trProjects.Where(x => x.Id == projectId).Select(x => new { Id = x.DevelopmentRepresentativeId, Name = x.DevelopmentRepresentative.FName + " " + x.DevelopmentRepresentative.LName }).Single();
				vm.User = devRep.Name;
				vm.UserId = devRep.Id;
			}
			else if (personnelType == ProjectPersonnelType.Manager)
			{
				var manager = _dbSubmission.trProjects.Where(x => x.Id == projectId).Select(x => new { Id = x.ENManagerId, Name = x.ENManager.FName + " " + x.ENManager.LName }).Single();
				vm.User = manager.Name;
				vm.UserId = manager.Id;
			}

			return PartialView("_ProjectPersonnel", vm);
		}

		public ActionResult BatchProjectPersonnel(string projectIds, ProjectPersonnelType personnelType)
		{
			var vm = new ProjectPersonnel();
			vm.ProjectIds = projectIds;
			vm.PersonnelType = personnelType;
			vm.User = "";
			return PartialView("_ProjectPersonnel", vm);
		}

		public JsonResult SaveProjectPersonnel(ProjectPersonnel vm)
		{
			var projectlist = new List<int>(vm.ProjectIds.Split(',').Select(x => int.Parse(x)));
			if (projectlist.Count > 0)
			{
				for (int i = 0; i < projectlist.Count; i++)
				{
					if (vm.PersonnelType == ProjectPersonnelType.ProjectLead)
					{
						_projectService.UpdateProjectLead(projectlist[i], vm.UserId);
					}
					else if (vm.PersonnelType == ProjectPersonnelType.DevRep)
					{
						_projectService.UpdateProjectDevRep(projectlist[i], vm.UserId);
					}
					else if (vm.PersonnelType == ProjectPersonnelType.Manager)
					{
						_projectService.TransferInEngineering(projectlist[i], (int)vm.UserId);
					}
					else if (vm.PersonnelType == ProjectPersonnelType.QAReviewer)
					{
						_projectService.TransferInQA(projectlist[i], (int)vm.UserId);
					}
				}
			}

			if (vm.PersonnelType == ProjectPersonnelType.ProjectLead)
			{
				var results = _dbSubmission.trProjects.Where(x => projectlist.Contains(x.Id)).Select(x => new { ProjectId = x.Id, NewVal = x.ENProjectLead.FName + " " + x.ENProjectLead.LName }).ToList();
				return Json(results, JsonRequestBehavior.AllowGet);
			}
			else if (vm.PersonnelType == ProjectPersonnelType.DevRep)
			{
				var results = _dbSubmission.trProjects.Where(x => projectlist.Contains(x.Id)).Select(x => new { ProjectId = x.Id, NewVal = x.DevelopmentRepresentative.FName + " " + x.DevelopmentRepresentative.LName }).ToList();
				return Json(results, JsonRequestBehavior.AllowGet);
			}
			else if (vm.PersonnelType == ProjectPersonnelType.Manager)
			{
				var results = _dbSubmission.trProjects.Where(x => projectlist.Contains(x.Id)).Select(x => new { ProjectId = x.Id, NewVal = x.ENManager.FName + " " + x.ENManager.LName }).ToList();
				return Json(results, JsonRequestBehavior.AllowGet);
			}
			else if (vm.PersonnelType == ProjectPersonnelType.QAReviewer)
			{
				var results = _dbSubmission.QABundles.Where(x => projectlist.Contains((int)x.ProjectId)).Select(x => new { ProjectId = x.Id, NewVal = x.QAReviewer.FName + " " + x.QAReviewer.LName }).ToList();
				return Json(results, JsonRequestBehavior.AllowGet);
			}
			else
			{
				return Json("Error", JsonRequestBehavior.AllowGet);
			}

		}

	}
}