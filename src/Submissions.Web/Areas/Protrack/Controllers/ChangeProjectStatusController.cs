﻿using GLI.EFCore.Submission;
using Submissions.Common;
using Submissions.Core;
using Submissions.Web.Areas.Protrack.Models.Project;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web.Mvc;

namespace Submissions.Web.Areas.Protrack.Controllers
{
	public class ChangeProjectStatusController : Controller
	{
		private readonly IUserContext _userContext;
		private readonly IProjectService _projectService;
		private readonly ILogService _logService;
		private readonly SubmissionContext _dbSubmission;

		public ChangeProjectStatusController(IUserContext userContext, IProjectService projectService, SubmissionContext submissionContext, ILogService logService)
		{
			_userContext = userContext;
			_projectService = projectService;
			_dbSubmission = submissionContext;
			_logService = logService;

		}

		public ActionResult Index(int projectId, ProjectStatus currentStatus, ProjectStatus changeStatusTo)
		{
			var project = _dbSubmission.trProjects
				.Where(x => x.Id == projectId)
				.Select(x => new
				{
					x.ProjectName,
					x.FileNumber,
					x.IsTransfer,
					x.TargetDate,
					Status = x.ENStatus,
					x.CertificationLab,
					x.QAOfficeId,
					QAOffice = x.QAOffice.Location
				})
				.Single();

			var submission = _dbSubmission.Submissions
				.Where(x => x.FileNumber == project.FileNumber)
				.Select(x => new
				{
					CanChangeStatusToDC = x.Manufacturer.UseDelayedCertifications,
					x.Type
				})
				.First();

			var vm = new ChangeProjectStatus();
			vm.ProjectId = projectId;
			vm.CertificationLab = project.CertificationLab;
			vm.TargetDate = project.TargetDate;
			vm.ProjectName = project.ProjectName;
			vm.CurrentProjectStatus = currentStatus;
			vm.ChangeProjectStatusTo = changeStatusTo;
			vm.FileType = submission.Type;
			if (changeStatusTo == ProjectStatus.RC || changeStatusTo == ProjectStatus.QA)
			{
				vm.RCSanityCheck = CheckCanProjectRC(projectId);
			}

			if (changeStatusTo == ProjectStatus.RC && currentStatus != ProjectStatus.DC && Convert.ToBoolean(project.IsTransfer))
			{
				vm.CanChangeStatusToDC = submission.CanChangeStatusToDC;
			}

			if (currentStatus.ToString() != project.Status)
			{
				if (vm.RCSanityCheck == null)
				{
					vm.RCSanityCheck = new RCSanityCheck();
				}
				vm.RCSanityCheck.StatusUpdated = true;
			}

			var isTaskOpen = _dbSubmission.trTasks.Where(x => x.ProjectId == projectId).Where(x => x.CompleteDate == null && x.Definition.IsReview != 1).Any();
			vm.IsAnyTaskOpen = isTaskOpen;
			vm.QAOffice = project.QAOffice ?? "";
			vm.QAOfficeId = project.QAOfficeId;
			//Expedite info
			var qaBundle = _dbSubmission.QABundles.Where(x => x.ProjectId == projectId).Single();
			vm.ExpediteInfo.Expedite = (qaBundle.IsRush == 1) ? true : false;
			vm.ExpediteInfo.ExpediteReason = qaBundle.RushReason;
			vm.ExpediteInfo.ExpediteNote = qaBundle.RushNotes;

			vm.ENInvolvementWhileFL = qaBundle.ENInvolvementWhileFL;
			vm.ENInvolvementWhileFLNote = qaBundle.ENInvolvementWhileFLNote;

			vm.CertNotNeeded = qaBundle.NoCertNeeded;
			vm.ADMWorkNeeded = qaBundle.ADMWorkNeeded;

			return PartialView("_ChangeProjectStatus", vm);
		}

		public RCSanityCheck CheckCanProjectRC(int projectId)
		{
			var sanityRC = new RCSanityCheck();

			// Project details
			var project = _dbSubmission.trProjects
				.Where(x => x.Id == projectId)
				.Select(x => new
				{
					x.Holder,
					x.IncomingReviewCompleteDate,
					x.AddDate,
					x.BundleTypeId
				})
				.Single();

			var ProjectRequiresIncomingReview = (project.AddDate >= Convert.ToDateTime(ConfigurationManager.AppSettings["incoming.review.livedate"]) && project.BundleTypeId == (byte?)BundleType.OriginalTesting);

			sanityRC.ProjectHolder = project.Holder;

			if (ProjectRequiresIncomingReview)
			{
				sanityRC.IncomingReviewMissing = project.IncomingReviewCompleteDate != null ? false : true;
			}

			//below checks is missing or expired
			var errorTarget = _projectService.IsProjectTargetExists(projectId);
			sanityRC.TargetErrorMessage = errorTarget;


			//Sanity check for Cryptographic field

			var CryptographicStrengthMissing = (from pj in _dbSubmission.trProjectJurisdictionalData
												join j in _dbSubmission.JurisdictionalData on pj.JurisdictionalDataId equals j.Id
												join s in _dbSubmission.Submissions on j.SubmissionId equals s.Id
												where (pj.ProjectId == projectId && s.CryptographicallyStrongId != 1 && s.CryptographicallyStrongId != 2 && s.CryptographicallyStrongId != 3)
												select s.Id).Distinct().ToList();
			sanityRC.CryptographicStrengthMissing = (CryptographicStrengthMissing.Count > 0) ? true : false;



			//Testing Performed
			var testingJurs = ComUtil.TestingPerformedJurisdictions().Select(i => (int)i).ToList();
			var testingPerformedMissing = (from p in _dbSubmission.trProjects
										   join pj in _dbSubmission.trProjectJurisdictionalData on p.Id equals pj.ProjectId
										   join j in _dbSubmission.JurisdictionalData on pj.JurisdictionalDataId equals j.Id
										   join s in _dbSubmission.Submissions on j.SubmissionId equals s.Id
										   join lj in _dbSubmission.tbl_lst_fileJuris on j.JurisdictionId equals lj.Id
										   where p.Id == projectId && testingJurs.Contains(lj.FixId)
										   && ((j.TestingPerformedId == null || j.TestingPerformedId == 0) || (s.TestingPerformedId == null || s.TestingPerformedId == 0))
										   select j.Id).ToList();
			sanityRC.IsTestIngPerformedMissing = (testingPerformedMissing.Count > 0) ? true : false;

			//Check Evo link
			//var selJurisdictionalDataIds = (from p in _dbSubmission.trProjects
			//								join pj in _dbSubmission.trProjectJurisdictionalData on p.Id equals pj.ProjectId
			//								where p.Id == projectId
			//								select pj.JurisdictionalDataId).ToList();
			//var notExists = _projectService.GetJurisdictionsNotLinkedIfInstanceExists(selJurisdictionalDataIds, projectId);
			//sanityRC.JurLinkMissinginEVO = (notExists.Count > 0) ? true : false;
			//TODO: commenting above passing PC-73-IGT-21-02 with 48 jurIds took long, will have work with Mike when double lock is being worked on
			sanityRC.JurLinkMissinginEVO = false;

			return sanityRC;
		}

		public ContentResult UpdateProjectStatus(int projectId, ChangeProjectStatus changeStatusVM)
		{

			if (changeStatusVM.ChangeProjectStatusTo == ProjectStatus.RC)
			{
				//Set if EN involvement required
				var qaBundle = _dbSubmission.QABundles.Where(x => x.ProjectId == projectId).Single();
				qaBundle.ENInvolvementWhileFL = changeStatusVM.ENInvolvementWhileFL;
				qaBundle.ENInvolvementWhileFLNote = changeStatusVM.ENInvolvementWhileFLNote;
				qaBundle.NoCertNeeded = changeStatusVM.CertNotNeeded;
				qaBundle.ADMWorkNeeded = changeStatusVM.ADMWorkNeeded;
				qaBundle.Project.QAOfficeId = changeStatusVM.QAOfficeId;
				_dbSubmission.Entry(qaBundle).Property(x => x.ENInvolvementWhileFL).IsModified = true;
				_dbSubmission.Entry(qaBundle).Property(x => x.ENInvolvementWhileFLNote).IsModified = true;
				_dbSubmission.SaveChanges();
				_projectService.UpdateCertificationLab(projectId, changeStatusVM.CertificationLab);
				//Set Expedite
				_projectService.UpdateProjectToExpedite(projectId, changeStatusVM.ExpediteInfo.Expedite, changeStatusVM.ExpediteInfo.ExpediteReason, changeStatusVM.ExpediteInfo.ExpediteNote);
				//Complete all Tasks & Split bundles
				_projectService.UpdateENStatus(projectId, ProjectStatus.RC);
			}
			else if (changeStatusVM.ChangeProjectStatusTo == ProjectStatus.DC)
			{
				_projectService.UpdateENStatus(projectId, ProjectStatus.DC);
			}
			else if (changeStatusVM.ChangeProjectStatusTo == ProjectStatus.IR)
			{
				_projectService.UpdateENStatus(projectId, ProjectStatus.IR);
			}
			else if (changeStatusVM.ChangeProjectStatusTo == ProjectStatus.QA)
			{
				//Complete all Tasks & Split bundles
				_projectService.UpdateENStatus(projectId, ProjectStatus.RC);
				_logService.AddProjectHistory(new trHistory
				{
					AddDate = DateTime.Now,
					ProjectId = projectId,
					UserId = _userContext.User.Id,
					Notes = "QA Pulled Transfer Project in their queue",
					ENProjectStatus = ProjectStatus.RC.ToString()
				});

				//Accept in QA
				_projectService.AcceptProjectInQA(projectId, _userContext.User.Id);
			}
			return Content("OK");

		}

		//QA Send to ENRR
		public ActionResult LoadQASendToENRR(List<int> projectIds)
		{
			var vm = new SendTOENRRFromQA();
			var projectList = projectIds;
			vm.ProjectIds = projectList;
			if (projectList.Count == 1)
			{
				var projectId = projectList[0];
				var managerInfo = _dbSubmission.trProjects.Where(x => x.Id == projectId).Select(x => new { Id = x.ENManagerId, Name = x.ENManager.FName + " " + x.ENManager.LName }).SingleOrDefault();
				if (managerInfo != null)
				{
					vm.ENManagerId = managerInfo.Id;
					vm.ENManager = managerInfo.Name;
				}
			}

			return PartialView("_SendtoENRR", vm);
		}

		public ContentResult CheckProjectsCannotSendToENRR(List<int> projectIds)
		{
			var checkProjects = _dbSubmission.trProjects.Where(x => projectIds.Contains(x.Id) && x.ENStatus != ProjectStatus.QA.ToString()).Select(x => x.ProjectName).ToList();
			if (checkProjects.Count > 0)
			{
				var projects = string.Join(",", checkProjects);
				return Content(projects);
			}
			else
			{
				return Content("OK");
			}
		}

		public ContentResult SendToENRRFromQA(SendTOENRRFromQA ENRRVm)
		{
			var projectList = ENRRVm.ProjectIds;
			if (projectList.Count > 0)
			{
				foreach (var projectId in projectList)
				{
					_projectService.SendBundleBackForEngineering(projectId, (int)ENRRVm.ENManagerId, ENRRVm.Note);
				}
			}

			return Content("OK");
		}
	}
}