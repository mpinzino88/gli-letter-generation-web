﻿using AutoMapper.QueryableExtensions;
using GLI.EFCore.Submission;
using Microsoft.EntityFrameworkCore;
using Submissions.Common;
using Submissions.Core;
using Submissions.Web.Areas.Protrack.Models.MyTasks;
using Submissions.Web.Areas.Protrack.Models.Project;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Submissions.Web.Areas.Protrack.Controllers
{
	//so that multiple requests can execute at the same time and using a session causes them to execute consecutively.
	[SessionState(System.Web.SessionState.SessionStateBehavior.ReadOnly)]
	public class AsyncDataController : Controller
	{
		private readonly SubmissionContext _dbSubmission;
		private readonly IQueryService _queryService;
		private readonly IIssueService _issueService;
		private readonly IProjectService _projectService;

		public AsyncDataController(SubmissionContext dbSubmission, IQueryService queryService, IIssueService issueService, IProjectService projectService)
		{
			_dbSubmission = dbSubmission;
			_queryService = queryService;
			_issueService = issueService;
			_projectService = projectService;
		}

		#region Project related
		public async Task<List<TaskHoursViewModel>> GetBPDataAsync(List<int> projectIds)
		{
			var selBPData = new List<TaskHoursViewModel>();

			var allResults = _queryService.GetBPData(projectIds).ToList();
			var arrProjectIds = projectIds;

			for (int i = 0; i < arrProjectIds.Count; i++)
			{
				var projectId = Convert.ToInt32(arrProjectIds[i]);
				var results = allResults.Where(x => x.ProjectId == projectId).ToList();
				var vm = new TaskHoursViewModel();
				vm.ProjectId = projectId;

				//Unassigned
				vm.Billable = results.Where(x => x.BillType == BillType.Billable.ToString() && x.IsAssigned == false).ToList();
				vm.NonBillable = results.Where(x => x.BillType == BillType.NonBillable.ToString() && x.IsAssigned == false).ToList();
				vm.NoCharge = results.Where(x => x.BillType == BillType.NoCharge.ToString() && x.IsAssigned == false).ToList();

				//Total
				vm.TotalActual = results.ToList().Sum(x => x.Hours);
				vm.TotalActualAssigned = results.Where(x => x.IsAssigned == true).ToList().Sum(x => x.Hours);
				vm.TotalActualUnassigned = results.Where(x => x.IsAssigned == false && x.BillType == BillType.Billable.ToString()).ToList().Sum(x => x.Hours);
				vm.TotalActualBillable = results.Where(x => x.BillType == BillType.Billable.ToString()).ToList().Sum(x => x.Hours);
				vm.TotalActualNonBillable = results.Where(x => x.BillType == BillType.NonBillable.ToString()).ToList().Sum(x => x.Hours);
				vm.TotalActualNoCharge = results.Where(x => x.BillType == BillType.NoCharge.ToString()).ToList().Sum(x => x.Hours);

				//est hours
				var query = _dbSubmission.trTasks.Where(x => x.ProjectId == vm.ProjectId);
				query = query.Where(x => x.DepartmentId != (int)Department.QA);
				var estHrs = query.GroupBy(x => x.ProjectId).Select(g => g.Sum(x => x.EstimateHours)).SingleOrDefault();
				vm.TotalEstAssigned = (estHrs == null) ? 0 : estHrs;

				vm.BudgetPen = (vm.TotalActualBillable == 0 || vm.TotalEstAssigned == 0) ? 0 : decimal.Round((decimal)(vm.TotalActualBillable / vm.TotalEstAssigned) * 100, 2);

				//math hours
				var mathQuery = _dbSubmission.trTasks.Where(x => x.ProjectId == vm.ProjectId);
				mathQuery = mathQuery.Where(x => x.DepartmentId == (int)Department.MTH);
				var mathEst = mathQuery.GroupBy(x => x.ProjectId).Select(g => g.Sum(x => x.EstimateHours)).SingleOrDefault();
				var mathAct = (from r in results
							   join tf in _dbSubmission.trTaskDefs on r.TaskId equals tf.DynamicsId
							   where tf.DepartmentId == (int?)Department.MTH
							   select r.Hours).Sum();
				var mathPen = (mathEst > 0 && mathAct > 0) ? decimal.Round((decimal)(mathAct / mathEst) * 100, 2) : 0;

				vm.MathEst = mathEst;
				vm.MathAct = mathAct;
				vm.MathPen = mathPen;

				selBPData.Add(vm);
			}
			return await Task.FromResult(selBPData);
		}

		[HttpPost]
		public async Task<JsonResult> GetBPData(List<int> projectIds)
		{
			try
			{
				Task<List<TaskHoursViewModel>> vm = GetBPDataAsync(projectIds);
				var result = await vm;
				return (Json(result, JsonRequestBehavior.AllowGet));
			}
			catch
			{
				return Json("Error", JsonRequestBehavior.AllowGet);
			}
		}

		public async Task<IssueOpenClosedResult> GetIssueCountAsync(List<int> projectIds)
		{
			var result = _issueService.GetIssueOpenClosedCountSQL(projectIds);
			return await Task.FromResult(result);
		}

		public async Task<JsonResult> GetOpenClosedJiraInfo(List<int> projectIds)
		{
			try
			{
				Task<IssueOpenClosedResult> vm = GetIssueCountAsync(projectIds);
				var result = await vm;
				return Json(result.Results, JsonRequestBehavior.AllowGet);
			}
			catch
			{
				return Json("Error", JsonRequestBehavior.AllowGet);
			}
		}

		public async Task<IssueOpenClosedResult> GetIssueSubCountAsync(List<int> projectIds)
		{
			var result = _issueService.GetIssueOpenClosedCountSQL(projectIds, true);
			return await Task.FromResult(result);
		}

		public async Task<JsonResult> GetOpenClosedSubJiraInfo(List<int> projectIds)
		{
			try
			{
				Task<IssueOpenClosedResult> vm = GetIssueSubCountAsync(projectIds);
				var result = await vm;
				return Json(result.Results, JsonRequestBehavior.AllowGet);
			}
			catch
			{
				return Json("Error", JsonRequestBehavior.AllowGet);
			}
		}

		public async Task<List<Models.Project.BundleInfo>> GetBundleTypesAsync(List<int> projectIds)
		{
			var bundleTypes = new List<Models.Project.BundleInfo>();
			foreach (var projectId in projectIds)
			{
				var sType = _projectService.GetBundleType(projectId);
				var indicator = _projectService.GetProjectIndicator(projectId);
				bundleTypes.Add(new Models.Project.BundleInfo() { ProjectId = projectId, BundleType = (sType != null) ? sType : "", BundleIndicator = (indicator != null) ? indicator : "" });
			}

			return await Task.FromResult(bundleTypes);
		}

		public async Task<JsonResult> GetBundleTypes(List<int> projectIds)
		{
			if (projectIds != null)
			{
				try
				{
					Task<List<Models.Project.BundleInfo>> vm = GetBundleTypesAsync(projectIds);
					var result = await vm;
					return Json(result, JsonRequestBehavior.AllowGet);
				}
				catch
				{
					return Json("Error", JsonRequestBehavior.AllowGet);
				}
			}
			return Json("Error", JsonRequestBehavior.AllowGet);
		}

		public async Task<List<Models.Project.BundleInfo>> GetQANotesAsync(List<int> projectIds)
		{
			var bundleTypes = new List<Models.Project.BundleInfo>();
			foreach (var projectId in projectIds)
			{
				var sNote = _projectService.GetQAProjectNote(projectId);
				bundleTypes.Add(new Models.Project.BundleInfo() { ProjectId = projectId, QANoteWhileAccept = (sNote != null) ? sNote : "" });
			}

			return await Task.FromResult(bundleTypes);
		}

		public async Task<JsonResult> GetQANotes(List<int> projectIds)
		{
			try
			{
				Task<List<Models.Project.BundleInfo>> vm = GetQANotesAsync(projectIds);
				var result = await vm;
				return Json(result, JsonRequestBehavior.AllowGet);
			}
			catch
			{
				return Json("Error", JsonRequestBehavior.AllowGet);
			}
		}

		public async Task<List<Models.Project.BundleInfo>> GetProjectJurCountAsync(List<int> projectIds)
		{
			var bundleTypes = new List<Models.Project.BundleInfo>();
			foreach (var projectId in projectIds)
			{
				var jurCount = _dbSubmission.trProjectJurisdictionalData.Where(x => x.ProjectId == projectId).Select(x => x.JurisdictionalData.JurisdictionId).Distinct().Count();
				bundleTypes.Add(new Models.Project.BundleInfo() { ProjectId = projectId, JurCount = jurCount });
			}

			return await Task.FromResult(bundleTypes);
		}
		public async Task<JsonResult> GetProjectJurCount(List<int> projectIds)
		{
			if (projectIds != null)
			{
				try
				{
					Task<List<Models.Project.BundleInfo>> vm = GetProjectJurCountAsync(projectIds);
					var result = await vm;
					return Json(result, JsonRequestBehavior.AllowGet);
				}
				catch
				{
					return Json("Error", JsonRequestBehavior.AllowGet);
				}
			}
			return Json("Error", JsonRequestBehavior.AllowGet);
		}
		#endregion

		#region Task related
		public async Task<JsonResult> GetQATaskOpenClosedJiraInfo(List<int> tasks)
		{
			try
			{
				var projectTaskInfo = _dbSubmission.QABundles_Task.Where(x => tasks.Contains(x.Id)).Select(x => new { ProjectId = x.QABundle.ProjectId, taskId = x.Id }).ToList();
				var projects = projectTaskInfo.Select(x => (int)x.ProjectId).Distinct().ToList();
				Task<IssueOpenClosedResult> vm = GetIssueCountAsync(projects);
				var result = await vm;

				var taskJiraCount = (from pt in projectTaskInfo
									 join r in (result.Results) on pt.ProjectId equals r.ProjectId
									 select (new
									 {
										 TaskId = pt.taskId,
										 OpenCount = r.OpenCount,
										 OpenURL = r.OpenURL
									 })).ToList();

				return Json(taskJiraCount, JsonRequestBehavior.AllowGet);
			}
			catch
			{
				return Json("Error", JsonRequestBehavior.AllowGet);
			}
		}

		public async Task<List<TaskHrVM>> GetBPDataForTasksAsync(List<int> projectIds)
		{
			var projectIdsParam = string.Join(",", projectIds);
			var allTasks = _dbSubmission.TaskHours.FromSqlInterpolated($"GetQuickDetailTaskInfo {projectIdsParam}").ProjectTo<TaskHrVM>().ToList();

			return await Task.FromResult(allTasks);
		}

		[HttpPost]
		public async Task<JsonResult> GetBPDataForTasks(List<int> tasks)
		{
			var projectIds = _dbSubmission.trTasks.Where(x => tasks.Contains(x.Id)).Select(x => x.ProjectId).ToList();
			try
			{
				Task<List<TaskHrVM>> vm = GetBPDataForTasksAsync(projectIds);
				var result = await vm;
				return (Json(result, JsonRequestBehavior.AllowGet));
			}
			catch
			{
				return Json("Error", JsonRequestBehavior.AllowGet);
			}
		}

		public async Task<JsonResult> GetBundleTypesForTaskDisplay(List<int> tasks)
		{
			if (tasks != null)
			{
				try
				{
					var qaTaskInfo = (from qt in _dbSubmission.QABundles_Task
									  join q in _dbSubmission.QABundles on qt.BundleID equals q.Id
									  where tasks.Contains(qt.Id)
									  select new { projectId = (int)q.ProjectId, qataskId = qt.Id }).ToList();

					Task<List<Models.Project.BundleInfo>> vm = GetBundleTypesAsync(qaTaskInfo.Select(x => x.projectId).Distinct().ToList());
					var result = await vm;

					var finalresult = (from r in result
									   join t in qaTaskInfo on r.ProjectId equals t.projectId
									   select new Models.Project.BundleInfo
									   {
										   ProjectId = r.ProjectId,
										   TaskId = t.qataskId,
										   BundleIndicator = r.BundleIndicator,
										   BundleType = r.BundleType
									   }).ToList();

					return Json(finalresult, JsonRequestBehavior.AllowGet);
				}
				catch
				{
					return Json("Error", JsonRequestBehavior.AllowGet);
				}
			}
			return Json("Error", JsonRequestBehavior.AllowGet);
		}

		public async Task<JsonResult> GetQATaskProjectJurCount(List<int> tasks)
		{
			try
			{
				Task<List<Models.Project.BundleInfo>> vm = GetQATaskProjectJurCountAsync(tasks);
				var result = await vm;
				return Json(result, JsonRequestBehavior.AllowGet);
			}
			catch
			{
				return Json("Error", JsonRequestBehavior.AllowGet);
			}
		}

		public async Task<List<Models.Project.BundleInfo>> GetQATaskProjectJurCountAsync(List<int> tasks)
		{
			if (tasks != null)
			{
				var projectTasks = _dbSubmission.QABundles_Task.Where(x => tasks.Contains(x.Id)).Select(x => new Models.Project.BundleInfo() { TaskId = x.Id, ProjectId = (int)x.QABundle.ProjectId }).ToList();

				var allProjects = projectTasks.Select(x => x.ProjectId).Distinct().ToList();
				foreach (var projectId in allProjects)
				{
					var jurCount = _dbSubmission.trProjectJurisdictionalData.Where(x => x.ProjectId == projectId).Select(x => x.JurisdictionalData.JurisdictionId).Distinct().Count();

					var allProjectTasks = projectTasks.Where(x => x.ProjectId == projectId).ToList();
					foreach (var task in allProjectTasks)
					{
						task.JurCount = jurCount;
					}
				}

				return await Task.FromResult(projectTasks);
			}
			return await Task.FromResult(new List<Models.Project.BundleInfo>());
		}
		#endregion
	}
}