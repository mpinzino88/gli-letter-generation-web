﻿using AutoMapper.QueryableExtensions;
using GLI.EFCore.Submission;
using Submissions.Common;
using Submissions.Core;
using Submissions.Core.Models.DTO;
using Submissions.Core.Models.ProductService;
using Submissions.Web.Areas.Protrack.Models.Product;
using Submissions.Web.Models.Query;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web.Mvc;
using Z.EntityFramework.Plus;

namespace Submissions.Web.Areas.Protrack.Controllers
{
	public class ProductController : Controller
	{
		private readonly IQueryService _queryService;
		private readonly IUserContext _userContext;
		private readonly SubmissionContext _dbSubmission;
		private readonly IProductService _productService;

		public ProductController(SubmissionContext dbSubmission, IQueryService queryService, IUserContext userContext, IProductService productService)
		{
			_queryService = queryService;
			_dbSubmission = dbSubmission;
			_userContext = userContext;
			_productService = productService;
		}

		#region MVC Views
		public ActionResult Index()
		{
			return View(new ProductIndexViewModel());
		}
		public ActionResult Edit(int? id)
		{
			var vm = new ProductEditViewModel();

			vm.Mode = Mode.Add;
			if (id != null)
			{
				var rawProduct = _dbSubmission.Products.Include("ProductJurisdictionalDatas").Where(x => x.Id == id).Single();
				var rawProductComponentIds = rawProduct.ProductJurisdictionalDatas.Select(x => x.JurisdictionalData.Submission.Id).ToList();
				var currentlyLinked = rawProduct.ProductJurisdictionalDatas
							.Select(x => new
							{
								JurisdictionalDataId = x.JurisdictionalDataId,
								ProductJurisdictionLinkType = x.ProductJurisdictionalDataLinkType.Type,
								ProductJurisdictionLinkTypeId = x.ProductJurisdictionalDataLinkType.Id
							}).ToList();
				var rawComponents = _dbSubmission.Submissions.Where(x => rawProductComponentIds.Contains(x.Id)).Select(x => new ProductComponentViewModel
				{
					Id = x.Id,
					GameName = x.GameName,
					IdNumber = x.IdNumber,
					Version = x.Version,
					ProductComponentJurisdictions = x.JurisdictionalData.Select(y => new ProductJurisdictionalDataViewModel
					{
						Id = y.Id,
						JurisdictionId = y.Jurisdiction.Id,
						Name = y.Jurisdiction.Name,
						Status = y.Status,
						ProductJurisdictionLinkType = string.Empty,
						ProductJurisdictionLinkTypeId = null
					}).ToList()
				}).ToList();


				foreach (var component in rawComponents)
				{
					foreach (var jurisdiction in component.ProductComponentJurisdictions)
					{
						if (currentlyLinked.Where(z => z.JurisdictionalDataId == jurisdiction.Id).FirstOrDefault() != null)
						{
							jurisdiction.ProductJurisdictionLinkType = currentlyLinked.Where(z => z.JurisdictionalDataId == jurisdiction.Id).FirstOrDefault().ProductJurisdictionLinkType;
							jurisdiction.ProductJurisdictionLinkTypeId = currentlyLinked.Where(z => z.JurisdictionalDataId == jurisdiction.Id).FirstOrDefault().ProductJurisdictionLinkTypeId;
						}
					}
				}

				vm = new ProductEditViewModel
				{
					Id = rawProduct.Id,
					Product = rawProduct.Name,
					Version = rawProduct.Version,
					ManufacturerCodes = rawProduct.ProductManufacturers
								.Select(x => new BaseSelectListItem()
								{
									Id = x.ManufacturerShortId,
									Text = x.Manufacturer.Description,
								}).ToList(),
					Platforms = rawProduct.ProductPlatforms
								.Select(x => new BaseSelectListItem()
								{
									Id = x.Platform.Id.ToString(),
									Text = x.Platform.Name + "  (" + x.Platform.ManufacturerGroup.ManufacturerGroupCategory.Code + ")",
								}).ToList(),
					CreatedOn = rawProduct.CreatedOn,
					CreatedBy = rawProduct.CreatedByUser.FName + " " + rawProduct.CreatedByUser.LName,
					ProductComponentViewModels = rawComponents,
					ProductJurisdictionalDataLinkTypes = _dbSubmission.tbl_lst_ProductJurisdictionalDataLinkTypes.Select(x => new ProductJurisdictionalDataLinkTypeViewModel { Id = x.Id, Type = x.Type }).ToList()
				};
				vm.Mode = Mode.Edit;
			}
			vm.CanDelete = Util.HasAccess(Permission.Submission, AccessRight.Delete);
			return View(vm);
		}
		#endregion

		#region API
		[HttpPost]
		public ActionResult Save(ProductEditViewModel vm)
		{
			var product = SaveProduct(vm);
			return Content(ComUtil.JsonEncodeCamelCase(product.Id), "application/json");
		}
		[HttpPost]
		public ActionResult Delete(int? id)
		{
			var product = _dbSubmission.Products.Find(id);
			try
			{
				product.Active = false;
				_dbSubmission.SaveChanges();
				return Content("OK");
			}
			catch
			{
				return Content("ERROR");
			}

		}
		[HttpPost]
		public ActionResult SearchComponents(SearchComponentsFilter filter)
		{
			var searchFilter = new SubmissionSearch
			{
				FileNumber = filter.FileNumber,
				IdNumber = filter.IdNumber,
				GameName = filter.GameName,
				Version = filter.Version,
			};

			if (!string.IsNullOrEmpty(searchFilter.FileNumber))
			{
				searchFilter.SingleInputSearch = filter.FileNumber;
			}
			var rawResults = _queryService.SubmissionSearch(searchFilter).ToList().AsQueryable().ProjectTo<SubmissionDTO>().ToList();
			if (filter.ComponentExclusions.Count > 0)
				rawResults = rawResults.Where(x => !filter.ComponentExclusions.Contains(x.Id)).ToList();

			var results = BuildProductComponentViewModels(rawResults);

			return Content(ComUtil.JsonEncodeCamelCase(results), "application/json");
		}
		[HttpPost]
		public ActionResult GetProducts(SearchProductsFilter filter)
		{
			var searchfilter = new ProductSearchFilter { Jurisdictions = filter.Jurisdictions, Manufacturers = filter.Manufacturers, Name = filter.Name, Status = filter.Status, Version = filter.Version };
			var results = _productService.GetProducts(searchfilter).Select(x => new { x.Name, x.Version, x.Id, x.CreatedOn, Manufacturers = string.Join(",", x.Manufacturers.Select(y => y.Code).ToList()) }).ToList();

			return Content(ComUtil.JsonEncodeCamelCase(results), "application/json");
		}
		#endregion

		#region Builders
		private List<ProductComponentViewModel> BuildProductComponentViewModels(List<SubmissionDTO> submissions)
		{
			var result = new List<ProductComponentViewModel>();

			foreach (var submission in submissions)
			{
				result.Add(BuildProductComponentViewModel(submission));
			}

			return result;
		}
		private ProductComponentViewModel BuildProductComponentViewModel(SubmissionDTO submission)
		{
			var result = new ProductComponentViewModel()
			{
				Id = submission.Id,
				FileNumber = submission.FileNumber,
				GameName = submission.GameName,
				IdNumber = submission.IdNumber,
				Version = submission.Version,
				ProductComponentJurisdictions = BuildProductComponentJurisdictionViewModels(submission.JurisdictionalData.ToList())
			};

			return result;
		}
		private List<ProductJurisdictionalDataViewModel> BuildProductComponentJurisdictionViewModels(List<JurisdictionalDataDTO> jurisdicitonalDatas)
		{
			var result = new List<ProductJurisdictionalDataViewModel>();

			foreach (var jurisdictionalData in jurisdicitonalDatas)
			{
				result.Add(BuildProductComponentJurisdictionViewModel(jurisdictionalData));
			}

			return result;
		}
		private ProductJurisdictionalDataViewModel BuildProductComponentJurisdictionViewModel(JurisdictionalDataDTO jurisdicitonalData)
		{
			var result = new ProductJurisdictionalDataViewModel
			{
				Id = jurisdicitonalData.Id,
				JurisdictionId = jurisdicitonalData.JurisdictionId,
				Name = jurisdicitonalData.JurisdictionName,
				Status = jurisdicitonalData.Status
			};

			return result;
		}
		#endregion

		#region Helpers
		private void SaveProductComponentAssociations(List<ProductComponentViewModel> productComponents, int productId)
		{
			var rawAssociations = productComponents.SelectMany(x => x.ProductComponentJurisdictions).Where(x => x.ProductJurisdictionLinkTypeId != null).Select(x => new { jurisdictionalDataId = x.Id, x.ProductJurisdictionLinkTypeId }).ToList();

			_dbSubmission.ProductJurisdictionalData.RemoveRange(_dbSubmission.ProductJurisdictionalData.Where(x => x.ProductId == productId));

			foreach (var association in rawAssociations)
			{
				_dbSubmission.ProductJurisdictionalData.Add(new ProductJurisdictionalData() { JurisdictionalDataId = association.jurisdictionalDataId, ProductJurisdictionalDataLinkTypeId = (int)association?.ProductJurisdictionLinkTypeId, ProductId = productId });
			}
		}
		private void SaveProductManufacturerAssociations(List<BaseSelectListItem> manufacturers, int productId)
		{
			_dbSubmission.ProductManufacturers.RemoveRange(_dbSubmission.ProductManufacturers.Where(x => x.ProductId == productId));
			_dbSubmission.ProductManufacturers.AddRange(manufacturers.Select(x => new ProductManufacturer { ManufacturerShortId = x.Id, ProductId = productId }).ToList());
		}
		private void SaveProductPlatformAssociations(List<BaseSelectListItem> platforms, int productId)
		{
			_dbSubmission.ProductPlatforms.RemoveRange(_dbSubmission.ProductPlatforms.Where(x => x.ProductId == productId));
			_dbSubmission.ProductPlatforms.AddRange(platforms.Select(x => new ProductPlatform { PlatformId = int.Parse(x.Id), ProductId = productId }).ToList());
		}
		private void SaveProductCore(ProductEditViewModel vm, GLI.EFCore.Submission.Product product)
		{
			product.Name = vm.Product;
			product.Version = vm.Version;
			if (vm.Mode == Mode.Add)
				product.CreatedBy = _userContext.User.Id;
		}
		private GLI.EFCore.Submission.Product SaveProduct(ProductEditViewModel vm)
		{
			var product = vm.Mode == Mode.Edit ? _dbSubmission.Products.Find(vm.Id) : new GLI.EFCore.Submission.Product();
			SaveProductCore(vm, product);
			if (vm.Mode == Mode.Add)
			{
				_dbSubmission.Products.Add(product);
				SaveSubmissionsContext();
			}
			SaveProductManufacturerAssociations(vm.ManufacturerCodes, vm.Id);
			SaveProductPlatformAssociations(vm.Platforms, vm.Id);
			SaveProductComponentAssociations(vm.ProductComponentViewModels.ToList(), vm.Id);
			SaveSubmissionsContext();
			return product;
		}
		private void SaveSubmissionsContext()
		{
			_dbSubmission.UserId = _userContext.User.Id;
			_dbSubmission.SaveChanges();
			_dbSubmission.UserId = null;
		}
		#endregion
	}

	public class SearchComponentsFilter
	{
		public SearchComponentsFilter()
		{
			ComponentExclusions = new List<int>();
		}
		public string FileNumber { get; set; }
		public string IdNumber { get; set; }
		public string Version { get; set; }
		public string GameName { get; set; }
		public List<int> ComponentExclusions { get; set; }
	}

	public class SearchProductsFilter
	{
		public SearchProductsFilter()
		{
			Jurisdictions = new List<int>();
			Manufacturers = new List<string>();
			Status = new List<string>();
		}
		public string Name { get; set; }
		public string Version { get; set; }
		public List<int> Jurisdictions { get; set; }
		public List<string> Manufacturers { get; set; }
		public List<string> Status { get; set; }

	}
}