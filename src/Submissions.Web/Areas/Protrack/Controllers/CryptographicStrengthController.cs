﻿using GLI.EFCore.Submission;
using Submissions.Common;
using Submissions.Core;
using Submissions.Web.Areas.Protrack.Models.CryptographicStrength;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace Submissions.Web.Areas.Protrack.Controllers
{
	[AuthorizeUser(Permission = Permission.MathCryptoStrength, HasAccessRight = AccessRight.Edit)]
	public class CryptographicStrengthController : Controller
	{
		private readonly SubmissionContext _dbSubmission;
		private readonly IUserContext _userContext;
		private readonly ILogService _logService;
		private readonly INoteService _noteService;
		private readonly ILogger _logger;


		public CryptographicStrengthController(SubmissionContext dbSubmission, IUserContext userContext, ILogService logService, INoteService noteService, ILogger logger)
		{
			_dbSubmission = dbSubmission;
			_userContext = userContext;
			_logService = logService;
			_noteService = noteService;
			_logger = logger;
		}

		#region Edit
		public ActionResult Edit(int projId)
		{
			var vm = new EditViewModel();
			vm.ProjectId = projId;
			ViewBag.Title = "Cryptographic Strength as defined in GLI-11 v3.0 for - ";
			vm.ProjectName = _dbSubmission.trProjects.Where(x => x.Id == projId).Select(x => x.FileNumber).Single();
			ViewBag.ProjectName = vm.ProjectName;
			vm.defaultCryptograhicId = _dbSubmission.CryptographicallyStrong.Where(x => x.Code == "NotEvaluated").Select(x => x.Id).Single();

			var CryptographicallyStrongOptions = _dbSubmission.CryptographicallyStrong.Select(x => new SelectListItem { Text = x.Description, Value = x.Id.ToString(), Selected = false }).ToList();
			CryptographicallyStrongOptions.Insert(0, new SelectListItem { Text = " ", Value = "0", Selected = true });
			vm.CryptographicallyStrongOptions.AddRange(CryptographicallyStrongOptions);


			vm.SubmissionsRecord = ((from s in _dbSubmission.Submissions
									 join p in _dbSubmission.trProjects on s.FileNumber equals p.FileNumber
									 where (p.Id == projId)
									 orderby s.ReceiveDate
									 select (new SubmissionsRecord
									 {
										 FileNumber = s.FileNumber,
										 SubmissionKey = s.Id,
										 ProjectId = p.Id,
										 ReceiveDate = s.ReceiveDate,
										 RequestDate = s.SubmitDate,
										 IdNumber = s.IdNumber,
										 GameName = s.GameName,
										 Version = s.Version,
										 DateCode = s.DateCode,
										 CryptographicId = s.CryptographicallyStrongId == null ? vm.defaultCryptograhicId : s.CryptographicallyStrongId,

									 })).Distinct()
									).ToList();


			foreach (var record in vm.SubmissionsRecord)
			{

				record.CryptographicallyStrongOptions.AddRange(GetCryptographicallyStrongOptions());
				var selectedCryptographicStrength = record.CryptographicallyStrongOptions.Where(x => x.Value == record.CryptographicId.ToString()).SingleOrDefault();
				if (selectedCryptographicStrength != null)
				{
					record.CryptographicallyStrongOptions.ToList().ForEach(x => x.Selected = false);
					selectedCryptographicStrength.Selected = true;

				}
			}

			return View(vm);

		}

		[HttpPost]
		public ActionResult Edit(EditViewModel vm)
		{
			UpdateSubmissions(vm);

			try
			{
				var userId = _userContext.User.Id;
				var dept = _dbSubmission.trLogins.Where(x => x.Id == userId).Select(x => x.Department.Code).FirstOrDefault();
				vm.ProjectName = _dbSubmission.trProjects.Where(x => x.Id == vm.ProjectId).Select(x => x.FileNumber).Single();
				vm.CryptographicText = (from c in _dbSubmission.CryptographicallyStrong
										join s in _dbSubmission.Submissions on c.Id equals s.CryptographicallyStrongId
										where s.FileNumber == vm.ProjectName
										select c.Description).FirstOrDefault();

				_logService.AddProjectHistory(
						new GLI.EFCore.Submission.trHistory
						{
							ProjectId = vm.ProjectId,
							Notes = "Cryptographic Strength " + vm.CryptographicText + " added"
						}
					);

				var newNote = new GLI.EFCore.Submission.trProjectNotes
				{
					AddDate = DateTime.Now,
					ProjectId = vm.ProjectId,
					Dept = dept,
					Note = "Cryptographic Strength " + vm.CryptographicText + " added",
					Scope = "I",
					UserId = _userContext.User.Id
				};
				_noteService.Add(newNote);

				_dbSubmission.SaveChanges();

				return RedirectToAction("Edit", new { projId = vm.ProjectId });
			}
			catch (Exception ex)
			{
				_logger.LogError(ex);
				throw new Exception("Failed to save Cryptographic Strength. See Inner Exception for details.", ex);
			}

		}
		#endregion

		#region Helper Methods
		private List<SelectListItem> GetCryptographicallyStrongOptions()
		{
			var options = _dbSubmission.CryptographicallyStrong.Select(x => new SelectListItem { Text = x.Description, Value = x.Id.ToString(), Selected = false }).ToList();
			options.Insert(0, new SelectListItem { Text = " ", Value = "0", Selected = true });
			return options;
		}


		public bool UpdateSubmissions(EditViewModel vm)
		{
			try
			{
				var forUpdate = vm.SubmissionsRecord;

				foreach (var record in forUpdate)
				{
					var recordForUpdate = _dbSubmission.Submissions.Where(x => x.Id == record.SubmissionKey).Single();
					recordForUpdate.CryptographicallyStrongId = record.CryptographicId;

				}

				_dbSubmission.UserId = _userContext.User.Id;
				_dbSubmission.SaveChanges();
				_dbSubmission.UserId = null;
			}
			catch (Exception ex)
			{
				_logger.LogError(ex);
				throw new Exception("Failed to save Cryptographic Strength flag to Submissions records.", ex);
			}
			return true;
		}
		#endregion
	}
}