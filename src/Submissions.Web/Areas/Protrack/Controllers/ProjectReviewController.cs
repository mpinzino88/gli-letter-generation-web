﻿extern alias ZEFCore;
using AutoMapper;
using GLI.EFCore.Submission;
using Submissions.Common;
using Submissions.Common.Linq.Dynamic;
using Submissions.Core;
using Submissions.Web.Areas.Protrack.Models.ProjectReview;
using Submissions.Web.Models.LookupsAjax;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Web.Mvc;
using ZEFCore::Z.EntityFramework.Plus;

namespace Submissions.Web.Areas.Protrack.Controllers
{
	using FUWUtil;
	using Submissions.Common.Email;
	using Submissions.Web.Utility.Email;
	using System.Data.Entity;
	using System.Web.UI.WebControls;

	public class ProjectReviewController : Controller
	{
		private readonly SubmissionContext _dbSubmission;
		private readonly IUserContext _userContext;
		private readonly IProjectReviewService _projectReviewService;
		private readonly IEmailService _emailService;
		private Dictionary<string, int> _reviewUserType;

		public ProjectReviewController(SubmissionContext dbSubmission, IProjectReviewService projectReviewService, IUserContext userContext, IEmailService emailService)
		{
			_dbSubmission = dbSubmission;
			_userContext = userContext;
			_projectReviewService = projectReviewService;
			_emailService = emailService;
		}

		public ActionResult Overview(int projectId = 0)
		{
			var vm = new Overview();

			vm.DepartmentsFilter = new DepartmentsFilter { DepartmentIds = new List<int>() { (int)Department.ENG, (int)Department.MTH, (int)Department.QA } };

			IList<SelectListItem> statusList = new SelectList(LookupsStandard.ConvertEnum<ProjectReviewStatus>(false, false), "Value", "Text", vm.Filters.StatusIds)
				.Where(x => x.Value != "Mediation")
				.ToList();

			vm.Filters.Status = statusList.OrderBy(x => x.Value).ToList();

			if (projectId != 0)
			{
				var project = _dbSubmission.trProjects.SingleOrDefault(proj => proj.Id == projectId);
				if (project != null)
				{
					vm.Filters.ProjectId = project.Id;
					vm.Filters.Filenumber = project.ProjectName;
				}
				foreach (var item in statusList)
				{
					if (item.Value != "")
					{
						item.Selected = true;
					}
				}
			}
			else
			{
				int departmentId = _userContext.DepartmentId;
				if (departmentId == (int)Department.ENG || departmentId == (int)Department.MTH || departmentId == (int)Department.QA)
				{
					vm.Filters.DepartmentId = departmentId;
					vm.Filters.Department = _userContext.Department.ToString();
				}

				vm.Filters.User = new List<SelectListItem> { new SelectListItem { Value = _userContext.User.Id.ToString(), Text = _userContext.User.Name } };
				vm.Filters.UserId = new List<int> { _userContext.User.Id };

				foreach (var item in statusList)
				{
					if (item.Value != "" && item.Value != "Completed")
					{
						item.Selected = true;
					}
				}
			}
			return View(vm);
		}
		public ActionResult Index(int? reviewId, int? projectId, int? departmentId)
		{
			var vm = new Review();

			if (reviewId != null)
			{
				vm = BuildExistingReview((int)reviewId);
			}
			if (projectId != null && departmentId != null)
			{
				vm = BuildNewReview((int)projectId, (int)departmentId);
			}

			return View(vm);
		}
		private Review BuildNewReview(int projectId, int departmentId)
		{
			var vm = new Review();

			vm.ProjectId = (int)projectId;
			vm.Filenumber = _dbSubmission.trProjects.Where(x => x.Id == projectId).Select(x => x.ProjectName).SingleOrDefault();
			vm.DepartmentId = (int)departmentId;
			vm.Department = _dbSubmission.trDepts.Where(x => x.Id == departmentId).Select(x => x.Code).SingleOrDefault();
			vm.ActionList = LookupsStandard.ConvertEnum<ReviewDetailAction>(false, true).ToList();
			vm.IssueSeverityOptions = LookupsStandard.ConvertEnum<IssueSeverity>(false, false).ToList();
			vm.AssigneeList = new List<SelectListItem> { new SelectListItem { Value = "", Text = "" } };
			vm.LetterwriterList = null;
			vm.ReviewerList = null;

			var managerAndLead = _dbSubmission.trProjects.Where(x => x.Id == vm.ProjectId).Select(x => new { x.ENManager, x.ENProjectLead }).ToList();
			var managerIds = managerAndLead
				.Where(x => x.ENManager != null)
				.Select(x => x.ENManager.Id)
				.ToList();
			if (managerAndLead.Select(x => x.ENProjectLead).SingleOrDefault() != null)
			{
				var leadIds = managerAndLead
				.Where(x => x.ENProjectLead != null)
				.Select(x => x.ENProjectLead.Id)
				.ToList();

				managerIds.AddRange(leadIds);
			}
			var reviewerIds = new List<int>();
			var letterwriterIds = new List<int>();
			var assigneeIds = new List<int>();
			var otherIds = new List<int>();
			var watcherIds = new List<int>();

			vm.EmailList = GetEmailAddresses(managerIds, reviewerIds, assigneeIds, letterwriterIds, watcherIds, otherIds);

			vm.AllMarkCompleted = true;

			return (vm);
		}
		private Review BuildExistingReview(int reviewId)
		{
			LoadUserTypesDictionary();
			var vm = new Review();
			var review = _dbSubmission.tbl_Reviews
				.Include("ReviewUsers.User")
				.Include("ReviewUsers.UserType")
				.Include("Project.JurisdictionalData")
				.Include("Department")
				.SingleOrDefault(x => x.Id == reviewId);
			vm = Mapper.Map<Review>(review);
			vm.Filenumber = review.Project.ProjectName;
			vm.Department = review.Department.Code;
			vm.CurrentUserId = _userContext.User.Id;
			vm.ActionList = LookupsStandard.ConvertEnum<ReviewDetailAction>(false, false).ToList();
			vm.IssueSeverityOptions = LookupsStandard.ConvertEnum<IssueSeverity>(false, false).ToList();

			vm.AssigneeList = PopulateUserList(review.ReviewUsers
				.Where(r => r.UserType.Id == UserTypeId(ReviewUserType.Reviewee))
				.DistinctBy(r => r.UserId)
				.Select(r => r.User));
			vm.ReviewerList = PopulateUserList(review.ReviewUsers
				.Where(r => r.UserType.Id == UserTypeId(ReviewUserType.Reviewer))
				.DistinctBy(r => r.UserId)
				.Select(r => r.User));
			vm.LetterwriterList = PopulateUserList(review.ReviewUsers
				.Where(r => r.UserType.Id == UserTypeId(ReviewUserType.LetterWriter))
				.DistinctBy(r => r.UserId)
				.Select(r => r.User));
			vm.WatcherList = PopulateUserList(review.ReviewUsers
				.Where(r => r.UserType.Id == UserTypeId(ReviewUserType.Watcher))
				.DistinctBy(r => r.UserId)
				.Select(r => r.User));
			var watcherIds = vm.WatcherList.Select(x => int.Parse(x.Value)).ToList();

			vm.AssigneeListIds = vm.AssigneeList
				.Select(x => int.Parse(x.Value))
				.ToList();
			var letterwriterIds = vm.LetterwriterList.Select(x => int.Parse(x.Value)).ToList();

			vm.Details = BuildReviewDetails(reviewId);
			vm.AssigneeMarkComplete = review.ReviewUsers
			.Where(x => x.UserId == _userContext.User.Id && x.UserType.Id == UserTypeId(ReviewUserType.Reviewee))
			.Select(x => (bool)x.CorrectionComplete)
			.SingleOrDefault();

			var reviewerIds = vm.ReviewerList.Select(x => int.Parse(x.Value)).ToList();
			var mediatorIds = vm.Details.Where(x => x.MediatorId != null).Select(x => (int)x.MediatorId).Distinct().ToList();
			reviewerIds.AddRange(mediatorIds);

			var managerIds = new List<int>();
			if (review.Project.ENManagerId.HasValue)
			{
				managerIds.Add(review.Project.ENManagerId.Value);
			}
			if (review.Project.ENProjectLeadId.HasValue)
			{
				managerIds.Add(review.Project.ENProjectLeadId.Value);
			}

			vm.EmailList = GetEmailAddresses(managerIds, reviewerIds, vm.AssigneeListIds, letterwriterIds, watcherIds);

			vm.JurisdictionList = review.Project.JurisdictionalData
				.Select(jdlink => jdlink.JurisdictionalData)
				.Select(jd => jd.Jurisdiction)
				.Distinct()
				.Select(x => new SelectListItem
				{
					Text = x.Name,
					Value = x.Id.ToString()
				})
				.OrderByDescending(x => x.Text)
				.ToList();

			var unresolvedIssues = vm.Details.Where(x => !x.Resolved).Select(x => x.Resolved).ToList();
			vm.AllMarkCompleted = unresolvedIssues.Count == 0 ? true : false;

			var assigneeWithIssue = vm.Details.Select(x => x.AssigneeId).Distinct();
			vm.ShowAssigneeEmailPopup = review.ReviewUsers
				.Where(x => x.ReviewId == review.Id && assigneeWithIssue.Contains(x.UserId) && x.UserTypeId == UserTypeId(ReviewUserType.Reviewee) && !(bool)x.CorrectionComplete).Count() == 1;

			return vm;
		}

		private List<ReviewDetail> BuildReviewDetails(int reviewId)
		{
			var details = _dbSubmission.tbl_ReviewDetails
				.Where(x => x.ReviewId == reviewId && x.Active == true)
				.OrderBy(x => x.Id)
				.Select(x => new ReviewDetail
				{
					Id = x.Id,
					AssigneeId = x.AssigneeId,
					Assignee = x.Assignee.FName + " " + x.Assignee.LName,
					LetterwriterId = x.LetterwriterId,
					Letterwriter = x.LetterwriterId == null ? null : x.Letterwriter.FName + " " + x.Letterwriter.LName,
					MediatorId = x.MediatorId,
					Mediator = x.Mediator.FName + " " + x.Mediator.LName,
					CategoryId = x.CategoryId,
					Category = x.Category.Description,
					JurisdictionId = x.JurisdictionId,
					Jurisdiction = x.Jurisdiction == null ? null : x.Jurisdiction.Name + " (" + x.Jurisdiction.Id + ")",
					Description = x.Description,
					Action = x.Action,
					IssueSeverity = x.IssueSeverity,
					Resolved = x.Resolved,
					ReviewerId = x.ReviewerId,
					Reviewer = x.Reviewer.FName + " " + x.Reviewer.LName,
					AddDate = x.AddDate,
					ShowDetail = true,
					Comments = x.Comments.Select(y => new ReviewDetailComment
					{
						Id = y.Id,
						UserId = y.UserId,
						UserName = y.User.FName + " " + y.User.LName,
						AddDate = y.AddDate,
						EditDate = y.EditDate,
						Comment = y.Comment
					}).ToList()
				})
				.ToList();

			return details;
		}
		private EmailLogin GetEmailAddresses(List<int> managerIds, List<int> reviewerIds, List<int> assigneeIds, List<int> letterwriterIds, List<int> watcherIds = null, List<int> otherIds = null)
		{
			var emailList = new EmailLogin();

			var idList = new List<int>();
			idList.AddRange(managerIds);
			idList.AddRange(reviewerIds);
			idList.AddRange(assigneeIds);
			idList.AddRange(letterwriterIds);
			idList.AddRange(watcherIds);
			if (otherIds != null)
			{
				idList.AddRange(otherIds);
			}

			var nameList = new List<Detail>();
			nameList = _dbSubmission.trLogins
				.Where(x => idList.Contains(x.Id))
				.Select(x => new Detail
				{
					Id = x.Id,
					Name = x.FName + " " + x.LName
				})
				.ToList();

			if (managerIds.Count != 0)
			{
				emailList.Managers = nameList.Where(x => managerIds.Contains(x.Id)).ToList();
			}
			else
			{
				emailList.Managers = new List<Detail>();
			}
			if (reviewerIds.Count != 0)
			{
				emailList.Reviewers = nameList.Where(x => reviewerIds.Contains(x.Id)).ToList();
			}
			else
			{
				emailList.Reviewers = new List<Detail>();
			}
			if (assigneeIds.Count != 0)
			{
				emailList.Assignees = nameList.Where(x => assigneeIds.Contains(x.Id)).ToList();
			}
			else
			{
				emailList.Assignees = new List<Detail>();
			}
			if (letterwriterIds.Count != 0)
			{
				emailList.Letterwriters = nameList.Where(x => letterwriterIds.Contains(x.Id)).ToList();
			}
			else
			{
				emailList.Letterwriters = new List<Detail>();
			}
			if (watcherIds.Count != 0)
			{
				emailList.Watchers = nameList.Where(x => watcherIds.Contains(x.Id)).ToList();
			}
			else
			{
				emailList.Watchers = new List<Detail>();
			}
			if (otherIds != null && otherIds.Count != 0)
			{
				emailList.Others = nameList.Where(x => otherIds.Contains(x.Id)).ToList();
			}
			else
			{
				emailList.Others = new List<Detail>();
			}

			return emailList;
		}

		[HttpPost]
		public ActionResult AddNewReview(int projectId, int departmentId, int[] reviewees, int[] reviewers, int[] letterwriters, int[] watchers, string description)
		{
			var review = _projectReviewService.AddNewReview(projectId, departmentId, reviewees, reviewers, letterwriters, watchers, description);
			return Json(review.Id);
		}

		[HttpPost]
		public ActionResult AddNewUsers(int[] revieweeIds, int[] reviewerIds, int[] letterwriterIds, int[] watcherIds, int reviewId)
		{
			_projectReviewService.AddNewUsers(revieweeIds, reviewerIds, letterwriterIds, watcherIds, reviewId);
			return Json("ok");
		}

		[HttpPost]
		public ActionResult DeleteReviewItem(int detailId)
		{
			var detail = _projectReviewService.DeleteIssue(detailId);
			if (_dbSubmission.tbl_ReviewDetails.Where(x => x.ReviewId == detail.ReviewId && x.Active).Count() == 0)
			{
				UpdateStatus(detail.ReviewId, ProjectReviewStatus.Pending);

				return Json(ProjectReviewStatus.Pending.ToString());
			}
			else
			{
				return null;
			}
		}

		[HttpPost]
		public ActionResult DeleteReview(int reviewId)
		{
			bool result = _projectReviewService.DeleteReview(reviewId);
			return Json(result);

		}

		[HttpPost]
		public ActionResult DeleteUser(int reviewId, int userId)
		{
			var result = _projectReviewService.DeleteUser(reviewId, userId);
			return Json(result);
		}

		[HttpPost]
		public ActionResult AssignReview(Review vm, string note)
		{
			if (!vm.Id.HasValue)
			{
				throw new NullReferenceException("AssignReview requires a valid tbl_Review ID");
			}
			if (vm.DepartmentId == (int)Department.QA)
			{
				_projectReviewService.AssignQAReviewTasks(vm.ProjectId, vm.Id.Value);
			}

			var assigned = _projectReviewService.AssignReview(vm.Id.Value);

			if (assigned)
			{
				var subject = vm.Status == ProjectReviewStatus.Pending.ToString() ? ProjectReviewEmailSubject.Assign : ProjectReviewEmailSubject.Reassign;
				vm.Status = ProjectReviewStatus.CorrectionsPending.ToString();
				UpdateStatus((int)vm.Id, ProjectReviewStatus.CorrectionsPending);
				SendEmail(vm, subject, note);
				return Json("ok");
			}
			else
			{
				throw new Exception("No Open Issues to Assign");
			}
		}

		[HttpPost]
		public ActionResult CompleteReview(Review vm, string note)
		{
			var review = new tbl_Reviews();
			review = _dbSubmission.tbl_Reviews.Where(x => x.Id == vm.Id).Single();
			review.Status = vm.Status = ProjectReviewStatus.Completed.ToString();
			review.CompleteDate = DateTime.UtcNow;
			review.CompleteLoginId = _userContext.User.Id;

			_dbSubmission.SaveChanges();

			SendEmail(vm, ProjectReviewEmailSubject.ReviewComplete, note);

			return null;
		}

		[HttpPost]
		public ActionResult CompleteReviewFinalization(Review vm, string note)
		{
			var review = new tbl_Reviews();
			review = _dbSubmission.tbl_Reviews.Where(x => x.Id == vm.Id).Single();
			review.Status = vm.Status = ProjectReviewStatus.Completed.ToString();
			review.CompleteDate = DateTime.UtcNow;
			review.CompleteLoginId = _userContext.User.Id;

			_dbSubmission.SaveChanges();//save review update, then check status before potentially changing

			var currentBundle = _dbSubmission.QABundles.Where(x => x.ProjectId == review.ProjectId).Single();
			if (currentBundle.QAStatus == QAProjectStatusFilter.DN.ToString())
			{
				return null;
			}
			currentBundle.QAStatus = QAProjectStatusFilter.FL.ToString();

			_dbSubmission.SaveChanges();

			SendEmail(vm, ProjectReviewEmailSubject.ReviewComplete, note);

			return null;
		}

		[HttpPost]
		public ActionResult CompleteReviewLetterbook(Review vm, string note)
		{
			var review = new tbl_Reviews();
			review = _dbSubmission.tbl_Reviews.Where(x => x.Id == vm.Id).Single();
			review.Status = vm.Status = ProjectReviewStatus.Completed.ToString();
			review.CompleteDate = DateTime.UtcNow;
			review.CompleteLoginId = _userContext.User.Id;
			_dbSubmission.SaveChanges();//save review update, then check status before potentially changing

			var currentBundle = _dbSubmission.QABundles.Where(x => x.ProjectId == review.ProjectId).Single();
			if (currentBundle.QAStatus == QAProjectStatusFilter.DN.ToString())
			{
				return Json(currentBundle.Id);
			}
			currentBundle.QAStatus = QAProjectStatusFilter.IR.ToString();

			_dbSubmission.SaveChanges();

			SendEmail(vm, ProjectReviewEmailSubject.ReviewComplete, note);

			return Json(currentBundle.Id);
		}

		[HttpPost]
		public ActionResult SaveComment(int detailId, string comment)
		{
			var newComment = new tbl_ReviewDetailComments();
			newComment.Id = 0;
			newComment.DetailId = detailId;
			newComment.AddDate = DateTime.UtcNow;
			newComment.UserId = _userContext.User.Id;
			newComment.Comment = comment;

			_dbSubmission.tbl_ReviewDetailComments.Add(newComment);

			_dbSubmission.SaveChanges();


			var newCommentReturn = _dbSubmission.tbl_ReviewDetailComments
				.Where(x => x.Id == newComment.Id)
				.Select(x => new ReviewDetailComment
				{
					Id = x.Id,
					UserId = x.UserId,
					UserName = x.User.FName + " " + x.User.LName,
					AddDate = x.AddDate,
					EditDate = x.EditDate,
					Comment = x.Comment
				}).SingleOrDefault();
			return Content(ComUtil.JsonEncodeCamelCase(newCommentReturn), "application/json");
		}

		[HttpPost]
		public ActionResult SaveDescription(int reviewId, string description)
		{
			var review = _dbSubmission.tbl_Reviews.Where(x => x.Id == reviewId).SingleOrDefault();
			review.Description = description;
			_dbSubmission.SaveChanges();
			return null;
		}

		[HttpPost]
		public ActionResult SaveDepartment(int reviewId, int departmentId)
		{
			var review = _dbSubmission.tbl_Reviews.Where(x => x.Id == reviewId).SingleOrDefault();
			review.DepartmentId = departmentId;
			_dbSubmission.SaveChanges();
			return null;
		}

		[HttpPost]
		public ActionResult SaveReviewDetail(ReviewDetail item, int? reviewId)
		{
			var reviewDetail = new tbl_ReviewDetails();

			if (item.Id != null)
			{
				reviewDetail = _dbSubmission.tbl_ReviewDetails.Find(item.Id);
			}

			reviewDetail.AssigneeId = item.AssigneeId;
			reviewDetail.MediatorId = item.MediatorId;
			reviewDetail.LetterwriterId = item.LetterwriterId;
			reviewDetail.CategoryId = item.CategoryId;
			reviewDetail.JurisdictionId = item.JurisdictionId;
			reviewDetail.Action = item.Action;
			reviewDetail.IssueSeverity = item.IssueSeverity;
			reviewDetail.Resolved = item.Resolved;
			reviewDetail.Description = item.Description;

			if (item.Id == null)
			{
				reviewDetail.ReviewId = (int)reviewId;
				reviewDetail.ReviewerId = _userContext.User.Id;
				reviewDetail.AddDate = DateTime.UtcNow;
				reviewDetail.Active = true;

				_dbSubmission.tbl_ReviewDetails.Add(reviewDetail);
			}
			else
			{
				reviewDetail.Id = item.Id ?? 0;
			}
			_dbSubmission.SaveChanges();

			var reviewDetailReturn = new ReviewDetail();

			reviewDetailReturn = item;
			reviewDetailReturn.Id = reviewDetail.Id;
			reviewDetailReturn.ReviewerId = reviewDetail.ReviewerId;
			reviewDetailReturn.Reviewer = _userContext.User.FName + " " + _userContext.User.LName;
			reviewDetailReturn.AddDate = reviewDetail.AddDate;

			return Content(ComUtil.JsonEncodeCamelCase(reviewDetailReturn), "application/json");
		}

		[HttpPost]
		public ActionResult UpdateResolved(int detailId, bool value)
		{
			var reviewDetail = new tbl_ReviewDetails();
			reviewDetail = _dbSubmission.tbl_ReviewDetails.Find(detailId);
			reviewDetail.Resolved = value;
			_dbSubmission.SaveChanges();

			if (!value)
			{
				var reviewee = _dbSubmission.tbl_ReviewUsers.Where(x => x.ReviewId == reviewDetail.ReviewId && x.UserId == reviewDetail.AssigneeId).Single();
				reviewee.CorrectionComplete = false;
				_dbSubmission.SaveChanges();
				UpdateStatus(reviewDetail.ReviewId, ProjectReviewStatus.CorrectionsPending);
			}

			var allDetailsResolved = _dbSubmission.tbl_ReviewDetails
				.Where(x => x.ReviewId == reviewDetail.ReviewId && x.Active)
				.Select(x => x.Resolved)
				.ToList();

			if (!allDetailsResolved.Contains(false))
			{
				UpdateStatus(reviewDetail.ReviewId, ProjectReviewStatus.VerificationPending);
			}

			var review = _dbSubmission.tbl_Reviews.Find(reviewDetail.ReviewId);

			var reviewStatus = "";

			if (review.Status == ProjectReviewStatus.CorrectionsPending.ToString())
			{
				reviewStatus = ProjectReviewStatus.CorrectionsPending.GetEnumDescription();
			}
			else if (review.Status == ProjectReviewStatus.VerificationPending.ToString())
			{
				reviewStatus = ProjectReviewStatus.VerificationPending.GetEnumDescription();
			}
			else
			{
				reviewStatus = review.Status;
			}

			return Json(reviewStatus);
		}

		[HttpPost]
		public ActionResult UpdateCorrectionComplete(Review vm, int revieweeId, string emailNote)
		{
			var revieweeDetails = new tbl_ReviewUsers();
			revieweeDetails = _dbSubmission.tbl_ReviewUsers.Where(x => x.ReviewId == vm.Id && x.UserId == revieweeId).SingleOrDefault();
			revieweeDetails.CorrectionComplete = true;
			_dbSubmission.SaveChanges();

			var revieweeCorrectionPending = _dbSubmission.tbl_ReviewUsers
				.Where(x => x.ReviewId == vm.Id && x.CorrectionComplete == false)
				.Select(x => x.UserId)
				.ToList();

			bool changeStatus = true;

			if (revieweeCorrectionPending != null)
			{
				foreach (var item in revieweeCorrectionPending)
				{
					var reviewDetailExists = _dbSubmission.tbl_ReviewDetails
						.Where(x => x.ReviewId == vm.Id && x.AssigneeId == item && x.Resolved == false && x.Active)
						.ToList();

					if (reviewDetailExists.Count != 0)
					{
						changeStatus = false;
					}

				}
			}
			if (changeStatus)
			{
				vm.Status = ProjectReviewStatus.VerificationPending.ToString();
				UpdateStatus((int)vm.Id, ProjectReviewStatus.VerificationPending);

				SendEmail(vm, ProjectReviewEmailSubject.CorrectionsComplete, emailNote);
			}
			return null;
		}

		[HttpPost]
		public ActionResult ReopenReview(int reviewId)
		{
			var review = _dbSubmission.tbl_Reviews.Where(x => x.Id == reviewId).Single();
			review.Status = ProjectReviewStatus.VerificationPending.ToString();
			review.CompleteDate = null;
			_dbSubmission.SaveChanges();
			return null;
		}

		[HttpPost]
		public ActionResult UpdateStatus(int reviewId, ProjectReviewStatus reviewStatus)
		{
			var review = _dbSubmission.tbl_Reviews.Where(x => x.Id == reviewId).Single();
			review.Status = reviewStatus.ToString();
			_dbSubmission.SaveChanges();
			return null;
		}

		[HttpPost]
		[OutputCache(NoStore = true, Duration = 0, VaryByParam = "None")]
		public ActionResult SendEmail(Review vm, ProjectReviewEmailSubject subject, string note, int? mediatorId = null)
		{
			var _email = Mapper.Map<ProjectReviewEmail>(vm);
			_email.IsDev = _userContext.User.Environment != Submissions.Common.Environment.Production;
			_email.Link = new UrlHelper(HttpContext.Request.RequestContext).Action("Index", "ProjectReview", new { reviewId = vm.Id }, Request.Url.Scheme);

			_email.Header = "Project Review Email";

			if (subject == ProjectReviewEmailSubject.Assign || subject == ProjectReviewEmailSubject.Reassign || subject == ProjectReviewEmailSubject.CorrectionsComplete)
			{
				var assignee = new List<string>();

				var openIssues = vm.Details.Where(x => !x.Resolved)
					.ToList();
				var openIssueAssigneeIds = openIssues.Select(x => x.AssigneeId).Distinct().ToList();

				foreach (var item in openIssueAssigneeIds)
				{
					var issueIdList = openIssues.Where(x => x.AssigneeId == item).Select(x => x.Id).ToList();
					var assigneeIssueDetails = string.Join(" ", openIssues.Where(x => x.AssigneeId == item).DistinctBy(x => x.AssigneeId).Select(x => x.Assignee).ToList()) + " - " + string.Join(", ", issueIdList) + " Detail Ids are " + subject.GetEnumDescription().ToString();
					assignee.Add(assigneeIssueDetails);
				}

				_email.AssigneeNames = string.Join("<br />", assignee);

				var reviewerNames = new List<string>();
				foreach (var item in vm.ReviewerList)
				{
					reviewerNames.Add(item.Text);
				}
				_email.ReviewerNames = string.Join(", ", reviewerNames);

				if (!vm.LetterwriterList.IsNullOrEmpty())
				{
					var letterwriterNames = new List<string>();
					foreach (var item in vm.LetterwriterList)
					{
						letterwriterNames.Add(item.Text);
					}
					_email.LetterwriterNames = string.Join(", ", letterwriterNames);
				}

				_email.Header = _email.Header + " - " + _userContext.User.FName.ToString() + " " + _userContext.User.LName.ToString();

				if (subject == ProjectReviewEmailSubject.Assign)
				{
					_email.Header = _email.Header + " has assigned this review";
				}
				else if (subject == ProjectReviewEmailSubject.Reassign)
				{
					_email.Header = _email.Header + " has re-assigned this review";
				}
				else if (subject == ProjectReviewEmailSubject.CorrectionsComplete)
				{
					_email.Header = _email.Header + " has completed the corrections";
				}
			}
			if (subject == ProjectReviewEmailSubject.Mediation)
			{
				_email.Header = _email.Header + " has requested mediation.";
			}
			if (subject == ProjectReviewEmailSubject.ReviewComplete && vm.Details.Count() == 0)
			{
				_email.Status = _email.Status + " (With no Issues Found)";
			}

			if (string.IsNullOrEmpty(note))
			{
				_email.Note = "N/A";
			}
			else
			{
				_email.Note = note;

				if (vm.Status == ProjectReviewStatus.Completed.ToString())
				{
					SaveFinalReviewComment((int)vm.Id, note);
				}
			}
			var gliGlobe = Server.MapPath("~/Img/GLIGlobe.gif");
			_emailService.EmbeddedImages = new List<LinkedResource> { new LinkedResource(gliGlobe) { ContentId = "GLIGlobe" }, };

			var emailList = new List<int>();

			if (mediatorId == null)
			{
				if (vm.EmailList.Assignees != null)
				{
					var assigneeIds = vm.EmailList.Assignees.Select(x => x.Id).ToList();
					emailList.AddRange(assigneeIds);
				}

				if (vm.EmailList.Reviewers != null)
				{
					var reviewerIds = vm.EmailList.Reviewers.Select(x => x.Id).ToList();
					emailList.AddRange(reviewerIds);
				}

				if (vm.EmailList.Letterwriters != null)
				{
					var letterwriterIds = vm.EmailList.Letterwriters.Select(x => x.Id).ToList();
					emailList.AddRange(letterwriterIds);
				}

				if (vm.EmailList.Managers != null && vm.DepartmentId != (int)Department.QA)
				{
					var managerIds = vm.EmailList.Managers.Select(x => x.Id).ToList();
					emailList.AddRange(managerIds);
				}

				if (vm.EmailList.Watchers != null)
				{
					var watcherIds = vm.EmailList.Watchers.Select(x => x.Id).ToList();
					emailList.AddRange(watcherIds);
				}

				if (vm.EmailList.Others != null)
				{
					var otherIds = vm.EmailList.Others.Select(x => x.Id).ToList();
					emailList.AddRange(otherIds);
				}
			}
			else
			{
				var mediatorIds = vm.Details.Where(x => x.MediatorId != null).Select(x => (int)x.MediatorId).Distinct().ToList();
				emailList.AddRange(mediatorIds);
			}

			var emailAddresses = _dbSubmission.trLogins.Where(x => emailList.Contains(x.Id)).Select(x => x.Email).ToList();

			_email.EmailAddressForTesting = string.Join("<br />", emailAddresses);

			var mailMsg = new MailMessage("noreply@gaminglabs.com", string.Join(",", emailAddresses));

			mailMsg.Subject = "Project Review (" + vm.Filenumber.ToString() + ") - " + subject.GetEnumDescription().ToString();
			mailMsg.Body = _emailService.RenderTemplate(EmailType.ProjectReview.ToString(), _email);

			_emailService.Send(mailMsg);
			return null;
		}

		[HttpPost]
		public ActionResult MediationEmail(Review vm, int detailId)
		{
			string note = "Mediation Has been assigned for Detail Id: " + vm.Details.Where(x => x.Id == detailId).Select(x => x.Id).SingleOrDefault();
			SendEmail(vm, ProjectReviewEmailSubject.Mediation, note, vm.Details.Where(x => x.Id == detailId).Select(x => x.MediatorId).SingleOrDefault());

			return null;
		}

		[HttpGet]
		public ActionResult getUserSuggestions(int projectId, int departmentId)
		{
			var userSuggestion = new UserSuggestion();
			if (departmentId == (int)Department.QA)
			{
				var sugggestion = _dbSubmission.QABundles_Task
					.Where(x => x.QABundle.ProjectId == projectId)
					.ToList();

				userSuggestion.ReviewerOptions = sugggestion
					.Distinct()
					.Where(x => x.TaskDefID == (int)QATasks.LetterReviewing)
					.Select(x => new SelectListItem
					{
						Text = x.User.FName + " " + x.User.LName,
						Value = x.UserId.ToString(),
						Selected = true
					})
					.ToList();

				userSuggestion.LetterwriterOptions = sugggestion
					.Where(x => x.TaskDefID == (int)QATasks.LetterWriting)
					.Select(x => new SelectListItem
					{
						Text = x.User.FName + " " + x.User.LName,
						Value = x.UserId.ToString(),
						Selected = true
					})
					.Distinct()
					.ToList();
			}
			else
			{
				var suggestion = _dbSubmission.trTasks
					.Where(x => x.ProjectId == projectId && x.UserId.HasValue)
					.DistinctBy(x => new { x.TaskDefinitionId, x.UserId })
					.ToList();

				userSuggestion.AssigneeOptions = suggestion
					.Where(x => x.Name != Tasks.TechReview.GetEnumDescription() && x.Name != Tasks.TestETATechReviewOut.GetEnumDescription() && x.Name != Tasks.TestTechAnalysis.GetEnumDescription())
					.Select(x => new SelectListItem
					{
						Text = x.User.FName + " " + x.User.LName,
						Value = x.UserId.ToString(),
						Selected = true
					})
					.DistinctBy(x => x.Value)
					.ToList();

				userSuggestion.ReviewerOptions = suggestion
					.Where(x => x.Name == Tasks.TechReview.GetEnumDescription() || x.Name == Tasks.TestETATechReviewOut.GetEnumDescription())
					.Distinct()
					.Select(x => new SelectListItem
					{
						Text = x.User.FName + " " + x.User.LName,
						Value = x.UserId.ToString(),
						Selected = true
					})
					.DistinctBy(x => x.Value)
					.ToList();
			}

			if (!userSuggestion.AssigneeOptions.IsNullOrEmpty())
			{
				userSuggestion.AssigneeIds = userSuggestion.AssigneeOptions
					.Select(x => int.Parse(x.Value))
					.ToList();
			}
			if (!userSuggestion.LetterwriterOptions.IsNullOrEmpty())
			{
				userSuggestion.LetterwriterIds = userSuggestion.LetterwriterOptions
					.Select(x => int.Parse(x.Value))
					.ToList();
			}
			if (!userSuggestion.ReviewerOptions.IsNullOrEmpty())
			{
				userSuggestion.ReviewerIds = userSuggestion.ReviewerOptions
					.Select(x => int.Parse(x.Value))
					.ToList();
			}


			if (userSuggestion.ReviewerIds == null)
			{
				userSuggestion.ReviewerOptions.Add(new SelectListItem()
				{
					Text = _userContext.User.FName + " " + _userContext.User.LName,
					Value = _userContext.User.Id.ToString(),
					Selected = true
				});

				userSuggestion.ReviewerIds = new List<int> { _userContext.User.Id };
			}
			else if (!userSuggestion.ReviewerIds.Contains(_userContext.User.Id))
			{
				userSuggestion.ReviewerOptions.Add(new SelectListItem()
				{
					Text = _userContext.User.FName + " " + _userContext.User.LName,
					Value = _userContext.User.Id.ToString(),
					Selected = true
				});

				userSuggestion.ReviewerIds.Add(_userContext.User.Id);
			}
			return Content(ComUtil.JsonEncodeCamelCase(userSuggestion), "application/json");
		}

		[HttpGet]
		public ActionResult getRevieweeSpecificStatus(int reviewId)
		{
			LoadUserTypesDictionary();
			var revieweeSpecificStatus = new List<RevieweeStatus>();
			var revieweesWithIssue = _dbSubmission.tbl_ReviewDetails
				.Where(x => x.ReviewId == reviewId && x.Active)
				.Select(x => x.AssigneeId)
				.Distinct()
				.ToList();
			var revieweeDetails = _dbSubmission.tbl_ReviewUsers
				.Where(x => x.ReviewId == reviewId && x.UserTypeId == UserTypeId(ReviewUserType.Reviewee))
				.DistinctBy(x => x.UserId)
				.ToList();
			foreach (var item in revieweeDetails)
			{
				var revieweeDetail = new RevieweeStatus();

				if (revieweesWithIssue.Contains(item.UserId))
				{
					revieweeDetail.RevieweeId = item.UserId;
					revieweeDetail.RevieweeName = item.User.FName + " " + item.User.LName;
					revieweeDetail.Status = (bool)item.CorrectionComplete ? "Correction Complete" : "Correction Pending";
				}
				else
				{
					revieweeDetail.RevieweeId = item.UserId;
					revieweeDetail.RevieweeName = item.User.FName + " " + item.User.LName;
					revieweeDetail.Status = "No Issue";

				}
				revieweeSpecificStatus.Add(revieweeDetail);
			}

			return Content(ComUtil.JsonEncodeCamelCase(revieweeSpecificStatus), "application/json");
		}

		[HttpPost]
		public ActionResult MarkAllResolved(Review vm, string note)
		{
			var unresolvedIssues = vm.Details.Where(x => x.Resolved == false).Select(x => x.Id).ToList();

			var reviewDetailComments = new List<tbl_ReviewDetailComments>();
			var date = DateTime.UtcNow;
			var user = _userContext.User.Id;

			foreach (var item in unresolvedIssues)
			{
				reviewDetailComments.Add(new tbl_ReviewDetailComments()
				{
					DetailId = (int)item,
					AddDate = date,
					UserId = user,
					Comment = "Issue Resolved (System)"
				});
			}

			_dbSubmission.tbl_ReviewDetailComments.AddRange(reviewDetailComments);
			_dbSubmission.SaveChanges();


			_dbSubmission.tbl_ReviewDetails
				.Where(x => unresolvedIssues.Contains(x.Id))
				.Update(x => new tbl_ReviewDetails
				{
					Resolved = true
				});

			if (vm.Department != Department.QA.ToString())
			{
				CompleteReview(vm, note);
			}
			else
			{
				UpdateStatus((int)vm.Id, ProjectReviewStatus.VerificationPending);
			}

			return null;
		}

		private List<SelectListItem> PopulateUserList(IEnumerable<trLogin> trLogins)
		{
			return trLogins
				.Select(x => new SelectListItem
				{
					Text = x.FName + " " + x.LName,
					Value = x.Id.ToString()
				})
				.ToList();
		}

		private void SaveFinalReviewComment(int reviewId, string note)
		{
			var review = _dbSubmission.tbl_Reviews.Where(x => x.Id == reviewId).SingleOrDefault();
			review.ReviewComments = note;
			_dbSubmission.SaveChanges();
		}

		private int UserTypeId(ReviewUserType user)
		{
			return _reviewUserType[user.GetEnumDescription()];
		}

		private void LoadUserTypesDictionary()
		{
			_reviewUserType = _dbSubmission.tbl_lst_ReviewUserType
				.Select(x => new
				{
					x.UserType,
					x.Id
				})
				.ToDictionary(
					x => x.UserType,
					x => x.Id);
		}
	}
}