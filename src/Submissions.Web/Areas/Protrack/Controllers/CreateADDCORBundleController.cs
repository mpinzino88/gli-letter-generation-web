﻿using GLI.EFCore.Submission;
using Microsoft.EntityFrameworkCore;
using Submissions.Common;
using Submissions.Core;
using Submissions.Web.Areas.Protrack.Models.Project;
using System;
using System.Linq;
using System.Web.Mvc;

namespace Submissions.Web.Areas.Protrack.Controllers
{
	public class CreateADDCORBundleController : Controller
	{
		private readonly SubmissionContext _dbSubmission;
		private readonly IUserContext _userContext;
		private readonly IProjectService _projectService;

		public CreateADDCORBundleController(SubmissionContext dbSubmission, IUserContext userContext, IProjectService projectService)
		{
			_dbSubmission = dbSubmission;
			_userContext = userContext;
			_projectService = projectService;
		}

		public ActionResult Index()
		{
			var vm = new CreateADDCORBundleVM();
			vm.Filters.Status = ProjectStatus.DN.ToString();
			vm.Filters.BundleStatus = BundleStatus.DN.ToString();
			vm.Filters.IsTransfer = ProjectType.All;
			return View("Index", vm);
		}

		public ContentResult Assign(CreateADDCORBundleVM vm, string selIds)
		{
			var projectList = selIds.Split(',').Select(int.Parse).ToList();
			foreach (var projectId in projectList)
			{
				var bundleId = _dbSubmission.QABundles.Where(x => x.ProjectId == projectId).Select(x => x.Id).Single();
				var currentTasks = _dbSubmission.QABundles_Task.Where(x => x.BundleID == bundleId).ToList();
				foreach (var task in currentTasks)
				{
					task.tfCurrent = false;
					_dbSubmission.Entry(task).State = EntityState.Modified;
					_dbSubmission.SaveChanges();
				}

				var newTask = new QABundles_Task()
				{
					BundleID = bundleId,
					TaskDefID = (int)vm.DefaultTask.TaskDefId,
					UserId = (int)vm.DefaultTask.UserId,
					AddUserId = _userContext.User.Id,
					CreatedTime = DateTime.Now,
					tfCurrent = true,
					TargetDate = vm.DefaultTask.TargetDate
				};
				_projectService.AddBundleTask(newTask, BundleStatus.AA.ToString());

				var bundle = _dbSubmission.QABundles.Where(x => x.Id == bundleId).Single();
				bundle.QAReviewerId = _userContext.User.Id;
				bundle.ReSubmission = true;
				_projectService.UpdateBundle(bundle);
			}
			return Content("OK");
		}
	}
}