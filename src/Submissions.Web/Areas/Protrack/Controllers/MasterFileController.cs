﻿using GLI.EFCore.Submission;
using Submissions.Common;
using Submissions.Common.Email;
using Submissions.Core;
using Submissions.Core.Models.MasterFileService;
using Submissions.Web.Areas.Protrack.Models.MasterFile;
using Submissions.Web.Utility.Email;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web.Mvc;

namespace Submissions.Web.Areas.Protrack.Controllers
{
	public class MasterFileController : Controller
	{
		private readonly SubmissionContext _dbSubmission;
		private readonly IUserContext _userContext;
		private readonly IEmailService _emailService;
		private readonly IMasterFileService _masterFileService;

		public MasterFileController(SubmissionContext dbSubmission, IUserContext userContext, IEmailService emailService, IMasterFileService masterFileService)
		{
			_dbSubmission = dbSubmission;
			_userContext = userContext;
			_emailService = emailService;
			_masterFileService = masterFileService;
		}

		#region Edit
		public ActionResult Edit(int id)
		{
			var vm = new EditViewModel();

			vm.ProjectId = id;
			vm.MappingTypes.AddRange(ConstructMappingTypes());
			vm.CurrentFileNumber = _dbSubmission.trProjects.Where(x => x.Id == id).Select(x => x.FileNumber).Single();
			vm.SubmissionFilter.FileNumbersToExclude.Add(vm.CurrentFileNumber);

			var manufCode = new FileNumber(vm.CurrentFileNumber).ManufacturerCode;
			var manufacturers = _dbSubmission.tbl_lst_fileManuManufacturerGroups
				.Where(x => x.ManufacturerGroup.ManufacturerGroupCategory.Code == "Resubmissions" && x.ManufacturerCode == manufCode)
				.SelectMany(x => x.ManufacturerGroup.ManufacturerLinks.Select(z => z.ManufacturerCode))
				.ToList();

			if (manufacturers.Count > 0)
			{
				vm.SubmissionFilter.ManufacturerCode.AddRange(manufacturers);
			}
			else
			{
				vm.SubmissionFilter.ManufacturerCode.Add(manufCode);
			}

			vm.CurrentSubmissionRecords = _dbSubmission.Submissions
				.Where(x => x.FileNumber == vm.CurrentFileNumber)
				.Select(x => new SubmissionRecord
				{
					DateCode = x.DateCode,
					FileNumber = x.FileNumber,
					GameName = x.GameName,
					Id = x.Id,
					IdNumber = x.IdNumber,
					LinkedId = x.MasterSubmissionId,
					Status = x.Status,
					Version = x.Version,
					Continuation = x.IsContinuation ?? false,
					PreCertification = x.IsPreCertification ?? false,
					Resubmission = x.IsResubmission,
					MasterFileNumber = x.MasterFileNumber,
				})
				.ToList();

			var currentSubmissionRecord = vm.CurrentSubmissionRecords.First();

			vm.MasterFileNumber = currentSubmissionRecord.MasterFileNumber;

			if (currentSubmissionRecord.Resubmission)
			{
				vm.SelectedMappingType = (int)ResubmissionOptions.Resubmission;
			}
			else if (currentSubmissionRecord.Continuation)
			{
				vm.SelectedMappingType = (int)ResubmissionOptions.Continuation;
			}
			else if (currentSubmissionRecord.PreCertification)
			{
				vm.SelectedMappingType = (int)ResubmissionOptions.PreCertification;
			}
			else
			{
				vm.SelectedMappingType = 0;
			}

			vm.Validator = _masterFileService.GetMasterFileValidator(vm.CurrentFileNumber, vm.MasterFileNumber);

			return View(vm);
		}

		[HttpPost]
		public ActionResult Edit(EditViewModel vm)
		{
			if (ModelState.IsValid)
			{
				_dbSubmission.UserId = _userContext.User.Id;

				if (
					((vm.SelectedMappingType == (int)ResubmissionOptions.Resubmission || vm.SelectedMappingType == (int)ResubmissionOptions.PreCertification)
					&& vm.CurrentSubmissionRecords.Any(x => x.LinkedId != null))
					|| (vm.SelectedMappingType == (int)ResubmissionOptions.Continuation)
				)
				{
					var isInitial = false;
					var previousMasterFile = "";

					foreach (var record in vm.CurrentSubmissionRecords)
					{
						var forUpdate = _dbSubmission.Submissions.Where(x => x.Id == record.Id).Single();

						if (forUpdate.MasterFileNumber == null)
						{
							isInitial = true;
						}
						else
						{
							previousMasterFile = forUpdate.MasterFileNumber;
						}

						if (vm.SelectedMappingType == (int)ResubmissionOptions.Continuation)
						{
							forUpdate.MasterFileNumber = vm.MasterFileNumber;
						}
						else
						{
							forUpdate.MasterFileNumber = vm.MasterFileNumber;
							forUpdate.MasterSubmissionId = record.LinkedId == 0 ? null : record.LinkedId;
						}

						switch (vm.SelectedMappingType)
						{
							case (int)ResubmissionOptions.Resubmission:
								{
									if (record.LinkedId != 0 && record.LinkedId != null)
									{
										forUpdate.IsResubmission = true;
										forUpdate.IsContinuation = false;
										forUpdate.IsPreCertification = false;
									}
									break;
								}
							case (int)ResubmissionOptions.Continuation:
								{
									forUpdate.IsContinuation = true;
									forUpdate.IsResubmission = false;
									forUpdate.IsPreCertification = false;
									forUpdate.MasterSubmissionId = null;

									break;
								}
							case (int)ResubmissionOptions.PreCertification:
								{
									if (record.LinkedId != 0 && record.LinkedId != null)
									{
										forUpdate.IsPreCertification = true;
										forUpdate.IsContinuation = false;
										forUpdate.IsResubmission = false;
									}
									break;
								}
							default:
								{
									break;
								}
						}
						_dbSubmission.SaveChanges();
					}

					var masterFileModel = ConstructMasterFileLinkageModel(vm, isInitial);
					masterFileModel.PreviousMasterFileNumber = previousMasterFile;
					SendEmail(Common.Email.EmailType.MasterFileLinkage, masterFileModel);
				}
				else if (vm.SelectedMappingType == 0)
				{
					foreach (var record in vm.CurrentSubmissionRecords)
					{
						var unMap = _dbSubmission.Submissions.Where(x => x.Id == record.Id).Single();
						unMap.MasterFileNumber = null;
						unMap.MasterSubmissionId = null;
						unMap.IsContinuation = false;
						unMap.IsResubmission = false;
						unMap.IsPreCertification = false;
					}
					_dbSubmission.SaveChanges();
				}
				if (!String.IsNullOrWhiteSpace(vm.RequestUrlReferrer))
				{
					return Redirect(vm.RequestUrlReferrer);
				}
			}
			return View("Close");
		}

		#endregion

		#region Summary and Evaluation

		public ActionResult ViewSummary(string fileNumber)
		{
			var vm = new MasterResubmissionSummaryVM();
			vm.Summary = _masterFileService.GetSummaryInfo(fileNumber);
			return View("Summary", vm);
		}
		public ActionResult MasterResubmissionEvaluationData(string fileNumber)
		{
			var vm = new MasterResubmissionEvaluationDataVM();
			vm.EvalData = _masterFileService.MasterResubmissionEvaluationData(fileNumber);
			return PartialView("_MasterResubmissionEvaluationData", vm);
		}
		public MasterResubmissionSummary GetSummaryInfo(string fileNumber)
		{
			var vm = _masterFileService.GetSummaryInfo(fileNumber);
			return vm;
		}

		#endregion

		#region Ajax Requests
		[HttpPost]
		public ActionResult GetMasterFileRecords(Core.Models.MasterFileService.Filter payload)
		{
			var ResubmissionType = (ResubmissionOptions)Enum.Parse(typeof(ResubmissionOptions), payload.MasterFilter.ResubmissionType, true);
			var validator = _masterFileService.GetMasterFileValidator(payload.MasterFilter.CurrentFileNumber, payload.MasterFilter.MasterFileNumber);
			if (ResubmissionType == ResubmissionOptions.Continuation && validator.InvalidHead)
			{
				validator.InvalidHead = false; //Continuation file can be master file multiple resubmissions
			}

			if (validator.IsValid())
			{
				var vm = new MasterFileRecordSetViewModel();
				vm.FileNumber = payload.MasterFilter.MasterFileNumber;
				vm.Records = _masterFileService.GetMasterFileSubmissionRecords(payload, ResubmissionType);
				vm.Ids = vm.Records.Select(x => new MasterFileRecord { Id = x.Id, CountId = x.CountId, AvailableForMapping = x.ConcurrentReSubmissionFileNumber == null || x.IsAMasterfile ? true : false }).ToList();
				vm.ResubmissionOption = ResubmissionType;

				switch (vm.ResubmissionOption)
				{
					case ResubmissionOptions.PreCertification:
						vm.ResubmissionOptionExplanation = "This Resubmission mapping requires that the records of the Master File be of status RJ, WD, TC, AP, OH, PN, RV, NL or NU.";
						break;
					case ResubmissionOptions.Resubmission:
						vm.ResubmissionOptionExplanation = "This Resubmission mapping requires that the records of the Master File be of status AP, OH, RJ, TC, or WD";
						break;
					default:
						break;
				}

				Response.StatusCode = (int)HttpStatusCode.OK;
				return PartialView("_MasterFileRecordSet", vm);
			}
			else if (validator.InvalidManufacturer)
			{
				Response.StatusCode = (int)HttpStatusCode.BadRequest;
				return Json(new { InvalidManufacturer = "Incompatible Manufacturer pair." });
			}
			else if (validator.InvalidHead)
			{
				Response.StatusCode = (int)HttpStatusCode.BadRequest;
				return Json(new { InvalidHead = "A file can not be a master file for another chain.  " + payload.MasterFilter.CurrentFileNumber + " is the master file of " + validator.CurrentHead + "." });
			}
			else if (validator.InvalidMultiplicity)
			{
				Response.StatusCode = (int)HttpStatusCode.BadRequest;
				return Json(new { ConcurrentMasterFile = validator.ConcurrentMasterFile });
			}
			else if (validator.InvalidSelfReference)
			{
				Response.StatusCode = (int)HttpStatusCode.BadRequest;
				return Json(new { InvalidSelfReference = payload.MasterFilter.CurrentFileNumber });
			}
			else
			{
				Response.StatusCode = (int)HttpStatusCode.InternalServerError;
				return Json("");
			}
		}
		#endregion

		#region Private Helper Functions
		private List<MappingType> ConstructMappingTypes()
		{
			return new List<MappingType> {
				new MappingType(){Id = 1, DisplayText = "Resubmission"},
				new MappingType(){Id = 2, DisplayText = "Continuation"},
				new MappingType(){Id = 3, DisplayText = "Pre-Certification"},
				new MappingType(){Id = 0, DisplayText = "None"}
			};
		}
		#endregion

		#region Email
		private MasterFileLinkageEmail ConstructMasterFileLinkageModel(EditViewModel vm, bool isInitial)
		{
			var result = new MasterFileLinkageEmail();

			result.CurrentFileNumber = vm.CurrentFileNumber;
			result.MappingType = (ResubmissionOptions)vm.SelectedMappingType;
			result.MasterFileNumber = vm.MasterFileNumber;
			result.IsInitial = isInitial;
			result.Props = _dbSubmission.trProjects
				.Where(x => x.FileNumber == vm.CurrentFileNumber)
				.Select(x => new MasterFileEmailProperties
				{
					Manufacturer = "",
					TestLab = x.Lab
				})
				.First();

			result.Props.Manufacturer = vm.CurrentFileNumber.Split('-')[2];

			return result;
		}
		private List<string> ConstructEmailRecipiants(MasterFileEmailProperties props)
		{
			var result = new List<string>();
			int distributionId;

			if (CustomManufacturerGroups.Aristocrat.Contains(props.Manufacturer))
			{
				distributionId = _dbSubmission.EmailDistributions.Where(x => x.Code == "MasterFile-ARI").Select(x => x.Id).Single();
				result.AddRange(_dbSubmission.EmailDistributionUsers.Where(x => x.EmailDistributionId == distributionId).Select(x => x.User.Email).ToList());
			}

			if (props.TestLab == "EU")
			{
				distributionId = _dbSubmission.EmailDistributions.Where(x => x.Code == "MasterFile-EU").Select(x => x.Id).Single();
				result.AddRange(_dbSubmission.EmailDistributionUsers.Where(x => x.EmailDistributionId == distributionId).Select(x => x.User.Email).ToList());
			}

			return result;
		}

		private void SendEmail(Submissions.Common.Email.EmailType type, MasterFileLinkageEmail model)
		{
			var gliGlobe = Server.MapPath("~/Img/GLIGlobe.gif");
			_emailService.EmbeddedImages = new List<LinkedResource> { new LinkedResource(gliGlobe) { ContentId = "GLIGlobe" } };


			var recipiants = ConstructEmailRecipiants(model.Props);
			if (recipiants.Count > 0)
			{
				var mailMsg = new MailMessage();
				foreach (var recipient in recipiants) { mailMsg.To.Add(new MailAddress(recipient)); }
				mailMsg.Subject = model.GetEmailSubject();
				mailMsg.Body = _emailService.RenderTemplate(type.ToString(), model);

				_emailService.Send(mailMsg);
			}
		}
		#endregion
	}

	#region Helper Classes
	public class MasterFileEmailProperties
	{
		public string TestLab { get; set; }
		public string Manufacturer { get; set; }
	}
	#endregion
}