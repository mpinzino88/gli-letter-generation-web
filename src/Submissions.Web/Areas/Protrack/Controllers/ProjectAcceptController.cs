﻿using GLI.EFCore.Submission;
using Newtonsoft.Json;
using Submissions.Common;
using Submissions.Core;
using Submissions.Web.Areas.Protrack.Models.ProjectAccept;
using Submissions.Web.Models.Query;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace Submissions.Web.Areas.Protrack.Controllers
{
	public class ProjectAcceptController : Controller
	{
		private readonly SubmissionContext _dbSubmission;
		private readonly IUserContext _userContext;
		private readonly IProjectService _projectService;

		public ProjectAcceptController
		(
			SubmissionContext dbSubmission,
			IUserContext userContext,
			IProjectService projectService
		)
		{
			_dbSubmission = dbSubmission;
			_userContext = userContext;
			_projectService = projectService;
		}

		public ActionResult Index(string protrackType = "ENG", int managerOwner = 0)
		{
			var vm = new IndexViewModel();
			vm.Filters.Manufacturers = new List<SelectListItem>();

			vm.ManagerOwner = managerOwner;
			Session["Protrack"] = protrackType;
			vm.ProtrackType = (ProtrackType)Enum.Parse(typeof(ProtrackType), Session["Protrack"].ToString());

			setFilters(ref vm, null);

			return View("Index", vm);
		}

		public JsonResult CheckAllItemsClosed(string skipQAIds)
		{
			var response = "";

			var skipQAIdlst = skipQAIds.Split(',').ToList();
			for (int i = 0; i < skipQAIdlst.Count; i++)
			{
				var projectId = Convert.ToInt32(skipQAIdlst[i]);
				if (!_projectService.CheckProjectForAllClosedItems(projectId))
				{
					var projectName = _dbSubmission.trProjects.Where(x => x.Id == projectId).SingleOrDefault().ProjectName;
					response += projectName + ",";
				}
			}

			if (!string.IsNullOrEmpty(response)) { response = response.Left(response.Length - 1); }

			return Json(response, JsonRequestBehavior.AllowGet);

		}

		public JsonResult CheckRequiredBeforeAcceptEng(string acceptIds)
		{
			var response = "";

			var acceptIdlst = acceptIds.Split(',').ToList();
			for (int i = 0; i < acceptIdlst.Count; i++)
			{
				var projectId = Convert.ToInt32(acceptIdlst[i]);
				if (_projectService.IsProjectTargetExists(projectId) != "")
				{
					//Check Task
					if (!_projectService.IsProjectInReviewTaskExists(projectId)) //If task not assigned
					{
						var projectName = _dbSubmission.trProjects.Where(x => x.Id == projectId).SingleOrDefault().ProjectName;
						response += projectName + ",";
					}
				}
			}

			if (!string.IsNullOrEmpty(response)) { response = response.Left(response.Length - 1); }

			return Json(response, JsonRequestBehavior.AllowGet);
		}

		private void setFilters(ref IndexViewModel vm, ProjectSearch selFilter)
		{
			if (selFilter == null)
			{
				var locId = _userContext.LocationId.ToString();
				var locName = _userContext.Location + " - " + _userContext.LocationName;
				if (vm.ProtrackType == ProtrackType.QA)
				{
					vm.Filters.TestLabs = new List<SelectListItem>();
					//if (_userContext.Location == CustomLab.NJ.ToString() || _userContext.Location == CustomLab.LV.ToString() || _userContext.Location == CustomLab.CO.ToString())
					//{
					//	var selItems = _dbSubmission.trLaboratories.Where(x => x.Location == CustomLab.NJ.ToString() || x.Location == CustomLab.CO.ToString() || x.Location == CustomLab.LV.ToString()).Select(x => new SelectListItem() { Text = x.Location + " - " + x.Name, Value = x.Id.ToString() }).ToList();
					//	vm.Filters.TestLabIds.AddRange(selItems.Select(x => x.Value).ToList());
					//	vm.Filters.TestLabs.AddRange(selItems);
					//}
					//else if (_userContext.Location == CustomLab.SA.ToString() || _userContext.Location == CustomLab.MA.ToString() || _userContext.Location == CustomLab.NS.ToString() || _userContext.Location == CustomLab.ME.ToString())
					//{
					//	var selItems = _dbSubmission.trLaboratories.Where(x => x.Location == CustomLab.SA.ToString() || x.Location == CustomLab.NS.ToString() || x.Location == CustomLab.ME.ToString() || x.Location == CustomLab.MA.ToString()).Select(x => new SelectListItem() { Text = x.Location + " - " + x.Name, Value = x.Id.ToString() }).ToList();
					//	vm.Filters.TestLabIds.AddRange(selItems.Select(x => x.Value).ToList());
					//	vm.Filters.TestLabs.AddRange(selItems);
					//}
					//else
					//{
					//	vm.Filters.TestLabIds.Add(locId);
					//	vm.Filters.TestLabs.Add(new SelectListItem() { Value = locId, Text = locName });
					//}
					vm.Filters.Status = ProjectStatus.RC.ToString();
				}
				else
				{
					vm.Filters.Laboratories = new List<SelectListItem>();
					vm.Filters.LaboratoryIds.Add(locId);
					vm.Filters.Laboratories.Add(new SelectListItem() { Value = locId, Text = locName });

					vm.Filters.Status = ProjectStatus.UU.ToString();
				}
				vm.Filters.IsTransfer = ProjectType.All;
			}
			else
			{
				if (Session["Protrack"].ToString() == ProtrackType.QA.ToString())
				{
					vm.Filters.TestLabIds = selFilter.TestLabIds;
					var locations = _dbSubmission.trLaboratories.Where(i => selFilter.TestLabIds.Contains(i.Id.ToString())).Select(x => new SelectListItem() { Value = x.Id.ToString(), Text = x.Location + " -" + x.Name }).ToList(); ;
					vm.Filters.TestLabs = locations;
					vm.Filters.Status = ProjectStatus.RC.ToString();
				}
				else
				{
					vm.Filters.LaboratoryIds = selFilter.LaboratoryIds;
					var locations = _dbSubmission.trLaboratories.Where(i => selFilter.LaboratoryIds.Contains(i.Id.ToString())).Select(x => new SelectListItem() { Value = x.Id.ToString(), Text = x.Location + " -" + x.Name }).ToList(); ;
					vm.Filters.Laboratories = locations;
					vm.Filters.Status = ProjectStatus.UU.ToString();
				}

				vm.Filters.ManufacturerShorts = selFilter.ManufacturerShorts;
				var manufs = _dbSubmission.tbl_lst_fileManu.Where(i => selFilter.ManufacturerShorts.Contains(i.Code)).Select(x => new SelectListItem() { Value = x.Code, Text = x.Description }).ToList();
				vm.Filters.Manufacturers = manufs;

				vm.Filters.IsTransfer = selFilter.IsTransfer;
			}
			vm.Filters.ExcludeFileTypes = new List<String>() { SubmissionType.VT.ToString(), SubmissionType.HT.ToString() };
		}

		public ActionResult Save(IndexViewModel ProjectAcceptModel)
		{
			if (ProjectAcceptModel.AcceptIds != null)
			{
				var managerId = 0;
				var labId = 0;

				if (ProjectAcceptModel.ProtrackType == ProtrackType.QA)
				{
					managerId = _userContext.User.Id;

					var Ids = ProjectAcceptModel.AcceptIds.Split(',').ToList();
					for (int i = 0; i < Ids.Count; i++)
					{
						_projectService.AcceptProjectInQA(Convert.ToInt32(Ids[i]), managerId);
					}
				}
				else
				{
					if (Util.HasAccess(Role.ENS, Role.MAS, Role.CPS, Role.MG))
					{
						managerId = _userContext.User.Id;
						labId = _userContext.LocationId;
					}
					else
					{
						managerId = (int)_userContext.ManagerId;
						labId = (int)_dbSubmission.trLogins.Where(x => x.Id == managerId).Select(x => x.LocationId).SingleOrDefault();
					}

					var acceptItems = JsonConvert.DeserializeObject<List<AssignEngReviewer>>(ProjectAcceptModel.AcceptIds);
					foreach (var item in acceptItems)
					{
						AcceptProjectInEngineering(item.ProjectId, managerId, labId, item.loginId);
					}
				}


			}
			else if (ProjectAcceptModel.DeclineIds != null)
			{
				var declineItems = JsonConvert.DeserializeObject<List<DeclineItem>>(ProjectAcceptModel.DeclineIds);
				foreach (var item in declineItems)
				{
					_projectService.DeclineProject(item.Id, item.Reason);
				}
			}
			else if (ProjectAcceptModel.SkipQAIds != null)
			{
				var skipQAIds = ProjectAcceptModel.SkipQAIds.Split(',').ToList();
				for (int i = 0; i < skipQAIds.Count; i++)
				{
					var projectId = Convert.ToInt32(skipQAIds[i]);
					_projectService.SkipQA(projectId);
				}
			}

			//set current selection
			var vm = new IndexViewModel();
			vm.ManagerOwner = ProjectAcceptModel.ManagerOwner;
			vm.ProtrackType = ProjectAcceptModel.ProtrackType;

			//set same filters again
			setFilters(ref vm, ProjectAcceptModel.Filters);

			return View("Index", vm);
		}

		public ActionResult AcceptinEngineering(int projectId, int managerId, int labId)
		{
			AcceptProjectInEngineering(projectId, managerId, labId);
			return RedirectToAction("Detail", "Project", new { id = projectId });
		}

		#region Helper Methods
		private void AcceptProjectInEngineering(int projectId, int managerId, int labId, int? reviewTaskAssignId = 0)
		{
			var project = _dbSubmission.trProjects.Where(t => t.Id == projectId).Single();
			project.ENAcceptDate = DateTime.Now;
			project.Holder = ProjectHolder.EN.ToString();
			project.ENManagerId = managerId;
			project.OriginalENManagerId = managerId;
			project.LabId = labId;
			project.Lab = _dbSubmission.trLaboratories.Where(x => x.Id == labId).Select(x => x.Location).SingleOrDefault();
			_projectService.UpdateENStatus(project, ProjectStatus.AU);

			if (project.BundleTypeId == (int)BundleType.OriginalTesting)
			{
				//Add CP review tasks
				_projectService.CPReviewTasksAdd(projectId);
			}

			if (reviewTaskAssignId != 0 && reviewTaskAssignId != null)
			{
				//var taskName = ProtrackReviewTask.EngIncoming.GetEnumDescription();
				//var taskdefId = _dbSubmission.trTaskDefs.Where(t => t.Code == taskName).Select(t => t.Id).SingleOrDefault();

				var taskdefId = (int)ProtrackReviewTask.EngIncoming;
				var taskName = _dbSubmission.trTaskDefs.Where(t => t.Id == taskdefId).Select(t => t.Description).SingleOrDefault();

				//if (taskdefId != null)
				if (taskdefId > 0)
				{
					var task = new trTask()
					{
						TaskDefinitionId = taskdefId,
						Name = taskName,
						Description = taskName,
						ProjectId = projectId,
						DepartmentId = (int)Department.ENG,
						UserId = reviewTaskAssignId
					};
					_projectService.AddTask(task);
				}
			}
		}
		#endregion
	}
}