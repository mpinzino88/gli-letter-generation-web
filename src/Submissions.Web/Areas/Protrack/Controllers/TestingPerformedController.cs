﻿using GLI.EFCore.Submission;
using Submissions.Web.Areas.Protrack.Models.TestingPerformed;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace Submissions.Web.Areas.Protrack.Controllers
{
	public class TestingPerformedController : Controller
	{
		private readonly SubmissionContext _dbSubmission;
		private readonly IUserContext _userContext;

		public TestingPerformedController(SubmissionContext dbSubmission, IUserContext userContext)
		{
			_dbSubmission = dbSubmission;
			_userContext = userContext;
		}

		#region Edit
		public ActionResult Edit(int id)
		{
			var vm = new EditViewModel();
			vm.ProjectId = id;
			ViewBag.Title = "Testing Performed - ";
			ViewBag.ProjectName = _dbSubmission.trProjects.Where(x => x.Id == id).Select(x => x.ProjectName).Single();

			var testingPerformedOptions = _dbSubmission.tbl_lst_TestingPerformed.Select(x => new SelectListItem { Text = x.Code, Value = x.Id.ToString(), Selected = false }).ToList();
			testingPerformedOptions.Insert(0, new SelectListItem { Text = " ", Value = "0", Selected = true });

			var externalTestLabs = _dbSubmission.tbl_lst_ExternalTestLab.Where(x => !x.InternalLab).Select(x => new SelectListItem { Text = x.Code, Value = x.Id.ToString(), Selected = false }).ToList();
			externalTestLabs.Insert(0, new SelectListItem { Text = " ", Value = "0", Selected = true });

			vm.TestingPerformedOptions.AddRange(testingPerformedOptions);
			vm.ExternalTestLabOptions.AddRange(externalTestLabs);

			vm.JurisdictionalData.AddRange(_dbSubmission.trProjectJurisdictionalData
				.Where(x => x.Project.Id == id)
				.OrderBy(x => x.JurisdictionalData.Submission.SubmitDate)
				.Select(x => new JurisdictionalDataRow
				{
					SubmissionKey = x.JurisdictionalData.Submission.Id,
					IDNumber = x.JurisdictionalData.Submission.IdNumber,
					Version = x.JurisdictionalData.Submission.Version,
					GameName = x.JurisdictionalData.Submission.GameName,
					Id = x.JurisdictionalData.Id,
					JurisdictionID = x.JurisdictionalData.JurisdictionId,
					Jurisdiction = x.JurisdictionalData.JurisdictionName,
					TestingPerformedId = x.JurisdictionalData.TestingPerformedId,
					TestingPerformed = x.JurisdictionalData.TestingPerformed.Code,
					ExternalTestLabId = x.JurisdictionalData.ExternalLab == 0 ? null : x.JurisdictionalData.ExternalLab,
					ExternalTestLab = x.JurisdictionalData.ExternalTestLab.Code,
					TestingPerformedAvailable = x.JurisdictionalData.Jurisdiction.TestingPerformedAvailable
				})
				.ToList());

			foreach (var jd in vm.JurisdictionalData)
			{
				jd.ExternalTestLabOptions.AddRange(GetExternalTestLabOptions());
				var selectedExternalLab = jd.ExternalTestLabOptions.Where(x => x.Value == jd.ExternalTestLabId.ToString()).SingleOrDefault();
				if (selectedExternalLab != null)
				{
					jd.ExternalTestLabOptions.ToList().ForEach(x => x.Selected = false);
					selectedExternalLab.Selected = true;
				}

				jd.TestingPerformedOptions.AddRange(GetTestingPerformedOptions());
				var selectedTesting = jd.TestingPerformedOptions.Where(x => x.Value == jd.TestingPerformedId.ToString()).SingleOrDefault();
				if (selectedTesting != null)
				{
					jd.TestingPerformedOptions.ToList().ForEach(x => x.Selected = false);
					selectedTesting.Selected = true;

				}
			}

			return View(vm);
		}

		[HttpPost]
		public ActionResult Edit(EditViewModel vm)
		{
			if (UpdateJurisdictionalData(vm))
			{
				UpdateSubmissions(vm);
			}

			return View("Close");
		}
		#endregion

		#region Helper Functions
		private List<SelectListItem> GetTestingPerformedOptions()
		{
			var options = _dbSubmission.tbl_lst_TestingPerformed.Select(x => new SelectListItem { Text = x.Code, Value = x.Id.ToString(), Selected = false }).ToList();
			options.Insert(0, new SelectListItem { Text = " ", Value = "0", Selected = true });

			return options;
		}

		private List<SelectListItem> GetExternalTestLabOptions()
		{
			var options = _dbSubmission.tbl_lst_ExternalTestLab.Where(x => !x.InternalLab).Select(x => new SelectListItem { Text = x.Code, Value = x.Id.ToString(), Selected = false }).ToList();
			options.Insert(0, new SelectListItem { Text = " ", Value = "0", Selected = true });

			return options;
		}

		public bool UpdateJurisdictionalData(EditViewModel vm)
		{
			var mutatedJurisdictionalData =
				vm.JurisdictionalData
					.Select(y => y.Id)
					.ToList();

			var jurisdictionalData =
				_dbSubmission.JurisdictionalData
				.Where(x => mutatedJurisdictionalData.Contains(x.Id))
				.ToList();

			foreach (var item in vm.JurisdictionalData)
			{
				var forUpdate = jurisdictionalData.Where(x => x.Id == item.Id).Single();
				forUpdate.ExternalLab = item.ExternalTestLabId == 0 ? null : item.ExternalTestLabId;
				forUpdate.TestingPerformedId = item.TestingPerformedId == 0 ? null : item.TestingPerformedId;
			}
			try
			{
				_dbSubmission.UserId = _userContext.User.Id;
				_dbSubmission.SaveChanges();
			}
			catch
			{
				return false;
			}

			return true;
		}

		public bool UpdateSubmissions(EditViewModel vm)
		{
			var distinctKeyTbls =
					vm.JurisdictionalData
					.GroupBy(x => x.SubmissionKey)
					.Select(x => x.First().SubmissionKey)
					.ToList();

			var primaries = vm.JurisdictionalData.Select(x => x.Id).ToList();

			var highestPriorityTestingPerformed =
				_dbSubmission.JurisdictionalData
				.Where(x => primaries.Contains(x.Id))
				.Min(x => x.TestingPerformedId);

			var forUpdate = _dbSubmission.Submissions.Where(x => distinctKeyTbls.Contains(x.Id)).ToList();

			foreach (var record in forUpdate)
			{
				record.TestingPerformedId = highestPriorityTestingPerformed;
			}

			try
			{
				_dbSubmission.UserId = _userContext.User.Id;
				_dbSubmission.SaveChanges();
			}
			catch
			{
				return false;
			}

			return true;
		}
		#endregion
	}
}