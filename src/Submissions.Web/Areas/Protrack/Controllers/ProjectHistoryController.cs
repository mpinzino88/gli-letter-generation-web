﻿using GLI.EFCore.Submission;
using Submissions.Web.Areas.Protrack.Models;
using System.Linq;
using System.Web.Mvc;

namespace Submissions.Web.Areas.Protrack.Controllers
{
	public class ProjectHistoryController : Controller
	{
		private readonly SubmissionContext _dbSubmission;

		public ProjectHistoryController(SubmissionContext dbSubmission)
		{
			_dbSubmission = dbSubmission;
		}

		public ActionResult Index(int projectId)
		{
			Session["ProjectID"] = projectId;
			var results = new ProjectHistoryViewModel();

			var project = _dbSubmission.trProjects.Find(projectId);
			results.ProjectId = projectId;
			results.ProjectName = project.ProjectName;
			results.ProjectHistoryGrid = new ProjectHistoryGrid();

			return View("Index", results);
		}

		[HttpGet]
		public JsonResult GetAllProjects(int projectID)
		{
			var projects = (from m in _dbSubmission.trHistories
							where m.ProjectId == projectID && m.ForeignProject != null
							select new
							{
								name = m.ForeignProject,
								value = m.ForeignProject

							}).Distinct();

			var currentProject = from m in _dbSubmission.trProjects
								 where m.Id == projectID
								 select new
								 {
									 name = m.ProjectName,
									 value = m.ProjectName
								 };
			projects = projects.Union(currentProject);

			return Json(projects.Distinct().ToList(), JsonRequestBehavior.AllowGet);
		}

		public JsonResult GetProjectData(string selectedForeign, int projectId)
		{
			Session["ProjectId"] = projectId;
			Session["selectedForeign"] = selectedForeign;
			return (LoadProjectHistoryGrid());
		}

		[OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
		public JsonResult LoadProjectHistoryGrid()
		{
			var projectID = (int)Session["ProjectID"];
			var foreignProject = (string)Session["selectedForeign"];

			var query = (from m in _dbSubmission.trHistories
						 where m.ProjectId == projectID
						 select (
							 new ProjectHistory
							 {
								 TimeStamp = m.AddDate,
								 Employee = m.User.FName + " " + m.User.LName,
								 ProjectHolder = m.ProjectHolder,
								 ProjectStatus = m.ENProjectStatus,
								 Notes = m.Notes,
								 ForeignProject = m.ForeignProject
							 }
						  )).Union(from qh in _dbSubmission.QABundles_History
								   where qh.QABundle.ProjectId == projectID
								   select
								   (
									   new ProjectHistory
									   {
										   TimeStamp = qh.AddDate,
										   Employee = qh.User.FName + " " + qh.User.LName,
										   ProjectHolder = "QA",
										   ProjectStatus = qh.QAProjectStatus,
										   Notes = qh.Notes,
										   ForeignProject = qh.ForeignProject
									   }
								   ));


			if (foreignProject == "")
			{
				query = query.Where(m => m.ForeignProject == null || m.ForeignProject == "");
			}
			else if (foreignProject != "" && foreignProject != "All" && foreignProject != null)
			{
				query = query.Where(m => m.ForeignProject == foreignProject);
			}

			query = query.OrderBy(m => m.TimeStamp).Distinct();

			var ProjectHistoryGrid = new ProjectHistoryGrid();
			return ProjectHistoryGrid.Grid.DataBind(query);
		}

	}
}