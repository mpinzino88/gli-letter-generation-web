﻿using GLI.EFCore.Submission;
using Submissions.Common;
using Submissions.Core;
using Submissions.Web.Areas.Protrack.Models.Project;
using System;
using System.Linq;
using System.Web.Mvc;

namespace Submissions.Web.Areas.Protrack.Controllers
{
	public class CreateTransferController : Controller
	{
		private readonly SubmissionContext _dbSubmission;
		private readonly IUserContext _userContext;
		private readonly IProjectService _projectService;
		private readonly IJurisdictionalDataService _jurisdictionalDataService;
		private readonly ILogService _logService;

		public CreateTransferController(SubmissionContext dbSubmission, IUserContext userContext, IProjectService projectService, IJurisdictionalDataService jurisdictionalDataService, ILogService logService)
		{
			_dbSubmission = dbSubmission;
			_userContext = userContext;
			_projectService = projectService;
			_jurisdictionalDataService = jurisdictionalDataService;
			_logService = logService;
		}

		public ActionResult Index(int Id)
		{
			var vm = new CreateTransfer();
			vm.ProjectId = Id;
			vm.Filters.ProjectIds.Add(Id);

			var projectInfo = _dbSubmission.trProjects.Where(x => x.Id == Id).Select(x => new { x.ProjectName, x.FileNumber, ProjectSuffix = x.Suffix, x.IsTransfer, ProjectStatus = x.ENStatus, x.BundleTypeId }).Single();
			vm.ProjectName = projectInfo.ProjectName;
			vm.FileNumber = projectInfo.FileNumber;
			vm.ProjectSuffix = projectInfo.ProjectSuffix;
			vm.ProjectStatus = projectInfo.ProjectStatus;
			vm.IsTransferProject = (projectInfo.IsTransfer == true) ? true : false;
			vm.BundleType = (projectInfo.BundleTypeId == 1) ? BundleType.ClosedTesting : BundleType.OriginalTesting;
			vm.IsForADDCOR = (projectInfo.BundleTypeId == 1) ? true : false;

			var projectItems = _dbSubmission.trProjectJurisdictionalData.Where(x => x.ProjectId == Id).Count();
			vm.TotalItems = projectItems;

			return View(vm);
		}

		//public ActionResult CheckEVOLock(int projectId, string selIds)
		//{
		//	bool isAllJurBelongtoEVO = true;
		//	var projectInfo = _dbSubmission.trProjects.Where(x => x.Id == projectId).Select(x => new { ProjectName = x.ProjectName, FileNumber = x.FileNumber, ProjectSuffix = x.Suffix, IsTransfer = x.IsTransfer, x.Holder }).Single();
		//	var selJurisdictonalDataIds = selIds.Split(',').Select(int.Parse).ToList();

		//	//EVO check if Holder is not QA(needs to happen for both NEw submission and TRansfers
		//	if (projectInfo.Holder != ProjectHolder.QA.ToString() && projectInfo.Holder != ProjectHolder.QACP.ToString())
		//	{
		//		//Check all jurs belong to compgroup
		//		var notExists = _projectService.GetJurisdictionsNotLinkedIfInstanceExists(selJurisdictonalDataIds, projectId);
		//		var jurNeedstoBelinked = _dbSubmission.JurisdictionalData
		//								.Where(x => notExists.Contains(x.Id))
		//								.Select(x => new
		//								{
		//									x.Submission.FileNumber,
		//									x.Submission.IdNumber,
		//									x.JurisdictionId
		//								}).ToList();

		//		isAllJurBelongtoEVO = (jurNeedstoBelinked.Count > 0) ? false : true;
		//		return Json(new { IsAllJurBelongtoEVO = isAllJurBelongtoEVO, JurNeedstoBelinked = jurNeedstoBelinked }, JsonRequestBehavior.AllowGet);
		//	}
		//	else
		//	{
		//		return Json(new { IsAllJurBelongtoEVO = isAllJurBelongtoEVO }, JsonRequestBehavior.AllowGet);
		//	}
		//  //TODO: commenting above passing PC-73-IGT-21-02 with 48 jurIds took long, will have work with Mike when double lock is being worked on
		//}

		public ActionResult CheckIsOkToExclude(int projectId, string selIds)
		{
			var projectInfo = _dbSubmission.trProjects.Where(x => x.Id == projectId).Select(x => new { ProjectName = x.ProjectName, FileNumber = x.FileNumber, ProjectSuffix = x.Suffix, IsTransfer = x.IsTransfer }).Single();
			var selPrimaryIDs = selIds.Split(',').Select(int.Parse).ToList();
			bool isOk = true;
			var allJurs = _dbSubmission.trProjectJurisdictionalData
						.Where(x => x.ProjectId == projectId)
						.Select(x => new
						{
							FileNumber = x.JurisdictionalData.Submission.FileNumber,
							JurNum = x.JurisdictionalData.JurisdictionId
						})
						.OrderBy(x => x.JurNum).ToList();

			if (allJurs.Exists(x => x.FileNumber == projectInfo.FileNumber && Convert.ToInt32(x.JurNum) == projectInfo.ProjectSuffix))
			{
				var allAvailItems = _dbSubmission.trProjectJurisdictionalData
						.Where(x => x.ProjectId == projectId && (!selPrimaryIDs.Contains(x.JurisdictionalDataId)))
						.Select(x => new
						{
							FileNumber = x.JurisdictionalData.Submission.FileNumber,
							JurNum = x.JurisdictionalData.JurisdictionId
						})
						.OrderBy(x => x.JurNum).ToList();

				isOk = allAvailItems.Exists(x => x.FileNumber == projectInfo.FileNumber && Convert.ToInt32(x.JurNum) == projectInfo.ProjectSuffix);
			}

			return Content((isOk) ? "OK" : "NOOK");
		}


		public ActionResult Save(CreateTransfer vm)
		{
			var selPrimaryIDs = vm.SelPrimarys.Split(',').Select(int.Parse).ToList();
			var newProjectId = 0;
			var bundleId = _dbSubmission.QABundles.Where(x => x.ProjectId == vm.ProjectId).Select(x => x.Id).Single();
			if (vm.CreateAsBrandNewBundle)
			{
				newProjectId = _projectService.CreateNewOrAddToExisting(selPrimaryIDs, (int)vm.BundleType);//_projectService.CreateNewTransferBundle(selPrimaryIDs);
			}
			else
			{
				newProjectId = _projectService.SplitJurisdictionsIntoProjects(bundleId, selPrimaryIDs, true, true);
			}

			_projectService.SetDefaultQAOffice(vm.ProjectId);
			return RedirectToAction("Detail", "Project", new { Id = newProjectId });
		}

		public ContentResult ExcludeJurisdictions(string primarys, int projectId)
		{
			var bundleTypeId = _dbSubmission.trProjects.Where(x => x.Id == projectId).Select(x => x.BundleTypeId).Single();
			var selPrimaryIDs = primarys.Split(',').Select(int.Parse).ToList();
			foreach (var primary in selPrimaryIDs)
			{
				if (bundleTypeId == (byte)BundleType.OriginalTesting)
				{
					var jurisdiction = _dbSubmission.JurisdictionalData.Where(x => x.Id == primary).Single();
					if (jurisdiction.Status == JurisdictionStatus.RA.ToString() || jurisdiction.Status == JurisdictionStatus.CP.ToString() || jurisdiction.Status == JurisdictionStatus.OH.ToString())
					{
						if (jurisdiction.TransferAvailable == false)
						{
							jurisdiction.TransferAvailable = true;
						}
						else
						{
							jurisdiction.TransferAvailable = null;
						}
						jurisdiction.TransferCreated = false;
						_jurisdictionalDataService.Update(jurisdiction);
					}
				}

				var projectJur = _dbSubmission.trProjectJurisdictionalData.Where(x => x.ProjectId == projectId && x.JurisdictionalDataId == primary).Single();
				_dbSubmission.trProjectJurisdictionalData.Remove(projectJur);
			}

			var history = new trHistory()
			{
				ProjectId = projectId,
				AddDate = DateTime.Now,
				UserId = _userContext.User.Id,
				Notes = "Item Removed."
			};
			_logService.AddProjectHistory(history);

			return Content("OK");
		}

		#region ADD/COR transfer

		public ActionResult CreateADDCOR(int Id)
		{
			var vm = new CreateTransfer();
			vm.ProjectId = Id;
			vm.Filters.ProjectIds.Add(Id);
			vm.CreateAsBrandNewBundle = false;
			vm.IsForADDCOR = true;

			var projectInfo = _dbSubmission.trProjects.Where(x => x.Id == Id).Select(x => new { ProjectName = x.ProjectName, FileNumber = x.FileNumber, ProjectSuffix = x.Suffix, IsTransfer = x.IsTransfer, ProjectStatus = x.ENStatus }).Single();
			vm.ProjectName = projectInfo.ProjectName;
			vm.FileNumber = projectInfo.FileNumber;
			vm.ProjectSuffix = projectInfo.ProjectSuffix;
			vm.ProjectStatus = projectInfo.ProjectStatus;
			vm.IsTransferProject = (projectInfo.IsTransfer == true) ? true : false;

			return View("Index", vm);
		}

		#endregion
	}
}