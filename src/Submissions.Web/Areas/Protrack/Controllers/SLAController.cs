﻿extern alias ZEFCore;
using GLI.EFCore.Submission;
using Submissions.Common;
using Submissions.Web.Areas.Protrack.Models.SLA;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using ZEFCore::Z.EntityFramework.Plus;

namespace Submissions.Web.Areas.Protrack.Controllers
{
	[AuthorizeUser(Permission = Permission.SLA, HasAccessRight = AccessRight.Edit, Roles = new Role[] { Role.ENS, Role.MG })]
	public class SLAController : Controller
	{
		private readonly SubmissionContext _dbSubmission;
		private readonly IUserContext _userContext;

		public SLAController(SubmissionContext dbSubmission, IUserContext userContext)
		{
			_dbSubmission = dbSubmission;
			_userContext = userContext;
		}

		public ActionResult Index(int projId)
		{
			var vm = new SLAIndexViewModel();
			vm.ProjectId = projId;
			var newMfNumber = "";
			var newFileNumber = "";
			var newProjectIdSet = new HashSet<int>();

			var remediationList = _dbSubmission.trProjectRemediations
				.Where(x => x.ProjectId == vm.ProjectId)
				.Select(x => new RemediationOption
				{
					ProjectId = x.ProjectId,
					RemediationDate = x.RemediationDate,
					remCode = x.Remediation.Code
				})
				.ToList();

			//Get checked Remediations for main project from DB
			foreach (var rem in remediationList)
			{
				if (rem.remCode == "BugFix")
				{
					vm.RemediationOption.BugFix = true;
					vm.RemediationOption.RemediationDate = rem.RemediationDate;
				}
				else if (rem.remCode == "EscapeDefect")
				{
					vm.RemediationOption.EscapeDefect = true;
					vm.RemediationOption.RemediationDate = rem.RemediationDate;
				}
				else if (rem.remCode == "Rework")
				{
					vm.RemediationOption.Rework = true;
					vm.RemediationOption.RemediationDate = rem.RemediationDate;
				}

				newProjectIdSet.Add(rem.ProjectId);
			}

			//Get checked SLAs for main project from DB
			var slaList = _dbSubmission.trProjectExcludeFromSLA
				.Where(x => x.ProjectId == vm.ProjectId)
				.Select(x => new SLAOption
				{
					ProjectId = x.ProjectId,
					SlaCode = x.ExcludeFromSLA.Code,
				})
				.ToList();

			foreach (var sla in slaList)
			{
				if (sla.SlaCode == "Approvals")
				{
					vm.SlaOption.Approvals = true;

				}
				else if (sla.SlaCode == "FirstPass")
				{
					vm.SlaOption.FirstPass = true;

				}
				else if (sla.SlaCode == "FCDuration")
				{
					vm.SlaOption.FCDuration = true;

				}
				else if (sla.SlaCode == "PCDuration")
				{
					vm.SlaOption.PCDuration = true;

				}
				newProjectIdSet.Add(sla.ProjectId);
			}

			//2 new options SUB-650
			var slaOptions = _dbSubmission.trProjectSLAOptions.Where(x => x.ProjectId == projId).SingleOrDefault();
			if (slaOptions != null)
			{
				vm.IncludesChangesOtherThanBug = slaOptions.IncludesChangesOtherThanBug;
				vm.ChangedInTechVersion = slaOptions.ChangedInTechVersion;
				vm.IncludesModificationsToFormalSubmission = slaOptions.IncludesModificationsToFormalSubmission;
			}

			// Get allowed options

			var items = new List<SelectListItem>();
			items.Add(new SelectListItem { Text = "", Value = "" });
			items.Add(new SelectListItem { Text = "Yes", Value = "Yes" });
			items.Add(new SelectListItem { Text = "No", Value = "No" });
			items.Add(new SelectListItem { Text = "TBD", Value = "TBD" });
			items.Add(new SelectListItem { Text = "N/A", Value = "N/A" });

			vm.OtherOptionsVal = new SelectList(items, "Value", "Text");


			//Get Master-Resubmission info************************
			var project = _dbSubmission.trProjects.Where(x => x.Id == vm.ProjectId).Select(x => new { IsTransfer = x.IsTransfer, FileNumber = x.FileNumber, TransferName = x.ProjectName }).Single();
			vm.TransferName = project.TransferName;
			vm.FileNumber = project.FileNumber;
			vm.MasterFileNumber = _dbSubmission.Submissions.Where(x => x.FileNumber == vm.FileNumber).Select(x => x.MasterFileNumber).First();

			if (string.IsNullOrEmpty(vm.MasterFileNumber))
			{
				vm.MasterFileNumber = _dbSubmission.Submissions.Where(x => x.MasterFileNumber == vm.FileNumber).Select(x => x.MasterFileNumber).FirstOrDefault();
			}

			if (!string.IsNullOrEmpty(vm.MasterFileNumber))
			{

				newMfNumber = _dbSubmission.Submissions.Where(x => x.FileNumber == vm.MasterFileNumber).Select(X => X.MasterFileNumber).FirstOrDefault();

				if (string.IsNullOrEmpty(newMfNumber))
				{
					newMfNumber = vm.MasterFileNumber;//if MasterFileNumberStr is null do not load
				}

				vm.MasterFileRecord = ((from s in _dbSubmission.Submissions
										join p in _dbSubmission.trProjects on s.FileNumber equals p.ProjectName
										where (s.FileNumber == newMfNumber)
										orderby s.ReceiveDate
										select (new MasterFileRecord
										{
											FileNumber = s.FileNumber,
											ProjectId = p.Id,
											ReceiveDate = s.ReceiveDate,
											IsResubmission = s.IsResubmission,
											IsPrecertification = s.IsPreCertification,
											IsContinuation = s.IsContinuation,
											MasterFileNumberStr = newMfNumber,
											ChangedInTechVersion = _dbSubmission.trProjectSLAOptions.Where(x => x.ProjectId == p.Id).Select(x => x.ChangedInTechVersion).FirstOrDefault(),
											IncludesChangesOtherThanBug = _dbSubmission.trProjectSLAOptions.Where(x => x.ProjectId == p.Id).Select(x => x.IncludesChangesOtherThanBug).FirstOrDefault(),
											IncludesModificationsToFormalSubmission = _dbSubmission.trProjectSLAOptions.Where(x => x.ProjectId == p.Id).Select(x => x.IncludesModificationsToFormalSubmission).FirstOrDefault()
										})).Distinct()
				).ToList();


				foreach (var file in vm.MasterFileRecord)
				{
					file.SLAExclude = _dbSubmission.trProjectExcludeFromSLA.Where(x => x.ProjectId == file.ProjectId).Select(x => x.ExcludeFromSLA.Description).ToList();
					file.Remediation = _dbSubmission.trProjectRemediations.Where(x => x.ProjectId == file.ProjectId).Select(x => x.Remediation.Description).ToList();
				}

				var masterList = vm.MasterFileRecord;
				setCheckboxes(newProjectIdSet, masterList);

				vm.ResubmissionFileRecord = ((from s in _dbSubmission.Submissions
											  join p in _dbSubmission.trProjects on s.FileNumber equals p.ProjectName
											  where (((s.IsResubmission == true) || (s.IsPreCertification == true)) && (s.MasterFileNumber == newMfNumber)
											  && (s.FileNumber != s.MasterFileNumber))
											  orderby 2
											  select (new MasterFileRecord
											  {
												  FileNumber = s.FileNumber,
												  ProjectId = p.Id,
												  ReceiveDate = s.ReceiveDate,
												  IsResubmission = s.IsResubmission,
												  IsPrecertification = s.IsPreCertification,
												  IsContinuation = s.IsContinuation,
												  MasterFileNumberStr = newMfNumber,
												  ChangedInTechVersion = _dbSubmission.trProjectSLAOptions.Where(x => x.ProjectId == p.Id).Select(x => x.ChangedInTechVersion).FirstOrDefault(),
												  IncludesChangesOtherThanBug = _dbSubmission.trProjectSLAOptions.Where(x => x.ProjectId == p.Id).Select(x => x.IncludesChangesOtherThanBug).FirstOrDefault(),
												  IncludesModificationsToFormalSubmission = _dbSubmission.trProjectSLAOptions.Where(x => x.ProjectId == p.Id).Select(x => x.IncludesModificationsToFormalSubmission).FirstOrDefault()
											  })).Distinct()
							).ToList();

				foreach (var file in vm.ResubmissionFileRecord)
				{
					file.SLAExclude = _dbSubmission.trProjectExcludeFromSLA.Where(x => x.ProjectId == file.ProjectId).Select(x => x.ExcludeFromSLA.Description).ToList();
					file.Remediation = _dbSubmission.trProjectRemediations.Where(x => x.ProjectId == file.ProjectId).Select(x => x.Remediation.Description).ToList();
				}

				var resubList = vm.ResubmissionFileRecord;
				setCheckboxes(newProjectIdSet, resubList);

				vm.ContinuationFileRecord = ((from s in _dbSubmission.Submissions
											  join p in _dbSubmission.trProjects on s.FileNumber equals p.ProjectName
											  where ((s.MasterFileNumber == newMfNumber) && (s.MasterFileNumber != s.FileNumber) && (s.IsContinuation == true))
											  orderby (s.ReceiveDate)
											  select (new MasterFileRecord
											  {
												  FileNumber = s.FileNumber,
												  ProjectId = p.Id,
												  ReceiveDate = s.ReceiveDate,
												  IsResubmission = s.IsResubmission,
												  IsPrecertification = s.IsPreCertification,
												  IsContinuation = s.IsContinuation,
												  MasterFileNumberStr = newMfNumber,
												  ChangedInTechVersion = _dbSubmission.trProjectSLAOptions.Where(x => x.ProjectId == p.Id).Select(x => x.ChangedInTechVersion).FirstOrDefault(),
												  IncludesChangesOtherThanBug = _dbSubmission.trProjectSLAOptions.Where(x => x.ProjectId == p.Id).Select(x => x.IncludesChangesOtherThanBug).FirstOrDefault(),
												  IncludesModificationsToFormalSubmission = _dbSubmission.trProjectSLAOptions.Where(x => x.ProjectId == p.Id).Select(x => x.IncludesModificationsToFormalSubmission).FirstOrDefault()
											  })).Distinct()
							).ToList();

				foreach (var file in vm.ContinuationFileRecord)
				{
					file.SLAExclude = _dbSubmission.trProjectExcludeFromSLA.Where(x => x.ProjectId == file.ProjectId).Select(x => x.ExcludeFromSLA.Description).ToList();
					file.Remediation = _dbSubmission.trProjectRemediations.Where(x => x.ProjectId == file.ProjectId).Select(x => x.Remediation.Description).ToList();
				}

				var contList = vm.ContinuationFileRecord;
				setCheckboxes(newProjectIdSet, contList);


				newFileNumber = (from s in _dbSubmission.Submissions
								 join p in _dbSubmission.trProjects on s.FileNumber equals p.ProjectName
								 where ((s.MasterFileNumber == newMfNumber) && (s.MasterFileNumber != s.FileNumber) && (s.IsContinuation == true))
								 orderby (s.ReceiveDate)
								 select (s.FileNumber)).FirstOrDefault();

				if (contList.Any())
				{
					vm.Resubmission2FileRecord = ((from s in _dbSubmission.Submissions
												   join p in _dbSubmission.trProjects on s.FileNumber equals p.ProjectName
												   where ((s.MasterFileNumber == newFileNumber) && (s.MasterFileNumber != s.FileNumber)
													&& ((s.IsResubmission == true) || (s.IsPreCertification == true)))
												   orderby s.ReceiveDate
												   select (new MasterFileRecord
												   {
													   FileNumber = s.FileNumber,
													   ProjectId = p.Id,
													   ReceiveDate = s.ReceiveDate,
													   IsResubmission = s.IsResubmission,
													   IsPrecertification = s.IsPreCertification,
													   IsContinuation = s.IsContinuation,
													   ChangedInTechVersion = _dbSubmission.trProjectSLAOptions.Where(x => x.ProjectId == p.Id).Select(x => x.ChangedInTechVersion).FirstOrDefault(),
													   IncludesChangesOtherThanBug = _dbSubmission.trProjectSLAOptions.Where(x => x.ProjectId == p.Id).Select(x => x.IncludesChangesOtherThanBug).FirstOrDefault(),
													   IncludesModificationsToFormalSubmission = _dbSubmission.trProjectSLAOptions.Where(x => x.ProjectId == p.Id).Select(x => x.IncludesModificationsToFormalSubmission).FirstOrDefault()
												   })).Distinct()
												).ToList();

					foreach (var file in vm.Resubmission2FileRecord)
					{
						file.SLAExclude = _dbSubmission.trProjectExcludeFromSLA.Where(x => x.ProjectId == file.ProjectId).Select(x => x.ExcludeFromSLA.Description).ToList();
						file.Remediation = _dbSubmission.trProjectRemediations.Where(x => x.ProjectId == file.ProjectId).Select(x => x.Remediation.Description).ToList();
					}

					var resub2List = vm.Resubmission2FileRecord;
					setCheckboxes(newProjectIdSet, resub2List);
				}
			}

			return View(vm);
		}

		private void setCheckboxes(HashSet<int> newProjectIdSet, IList<MasterFileRecord> recordList)
		{
			if (!recordList.Any())
			{
				return;
			}

			var projectIds = recordList.Select(x => x.ProjectId).ToList();
			var slaProjectIds = _dbSubmission.trProjectExcludeFromSLA.Where(x => projectIds.Contains(x.ProjectId)).Select(x => x.ProjectId).ToList();
			var remProjectIds = _dbSubmission.trProjectRemediations.Where(x => projectIds.Contains(x.ProjectId)).Select(x => x.ProjectId).ToList();

			foreach (var mf in recordList)
			{
				newProjectIdSet.Add(mf.ProjectId);
				if ((slaProjectIds.Contains(mf.ProjectId)) || (remProjectIds.Contains(mf.ProjectId)))
				{
					mf.MFCheckBox = true;
				}
			}
		}

		[HttpPost]
		public ActionResult SLARemediationSave(SLAIndexViewModel vm)
		{
			var projectIdSet = new HashSet<int>();

			_dbSubmission.trProjectExcludeFromSLA.Where(x => x.ProjectId == vm.ProjectId).Delete();
			_dbSubmission.trProjectRemediations.Where(x => x.ProjectId == vm.ProjectId).Delete();

			if (vm.RemediationOption.BugFix)
			{
				var remId = _dbSubmission.tbl_lst_Remediations.Where(X => X.Code == "BugFix").Select(x => x.Id).Single();
				_dbSubmission.trProjectRemediations.Add(new trProjectRemediations { ProjectId = vm.ProjectId, RemediationDate = vm.RemediationOption.RemediationDate, RemediationId = remId });
			}

			if (vm.RemediationOption.EscapeDefect)
			{
				var remId = _dbSubmission.tbl_lst_Remediations.Where(X => X.Code == "EscapeDefect").Select(x => x.Id).Single();
				_dbSubmission.trProjectRemediations.Add(new trProjectRemediations { ProjectId = vm.ProjectId, RemediationDate = vm.RemediationOption.RemediationDate, RemediationId = remId });
			}

			if (vm.RemediationOption.Rework)
			{
				var remId = _dbSubmission.tbl_lst_Remediations.Where(X => X.Code == "Rework").Select(x => x.Id).Single();
				_dbSubmission.trProjectRemediations.Add(new trProjectRemediations { ProjectId = vm.ProjectId, RemediationDate = vm.RemediationOption.RemediationDate, RemediationId = remId });
			}

			if (vm.SlaOption.Approvals)
			{
				var slaId = _dbSubmission.tbl_lst_ExcludeFromSLA.Where(X => X.Code == "Approvals").Select(x => x.Id).Single();
				_dbSubmission.trProjectExcludeFromSLA.Add(new trProjectExcludeFromSLA { ProjectId = vm.ProjectId, ExcludeFromSLAId = slaId });
			}

			if (vm.SlaOption.FirstPass)
			{
				var slaId = _dbSubmission.tbl_lst_ExcludeFromSLA.Where(X => X.Code == "FirstPass").Select(x => x.Id).Single();
				_dbSubmission.trProjectExcludeFromSLA.Add(new trProjectExcludeFromSLA { ProjectId = vm.ProjectId, ExcludeFromSLAId = slaId });
			}

			if (vm.SlaOption.FCDuration)
			{
				var slaId = _dbSubmission.tbl_lst_ExcludeFromSLA.Where(X => X.Code == "FCDuration").Select(x => x.Id).Single();
				_dbSubmission.trProjectExcludeFromSLA.Add(new trProjectExcludeFromSLA { ProjectId = vm.ProjectId, ExcludeFromSLAId = slaId });
			}

			if (vm.SlaOption.PCDuration)
			{
				var slaId = _dbSubmission.tbl_lst_ExcludeFromSLA.Where(X => X.Code == "PCDuration").Select(x => x.Id).Single();
				_dbSubmission.trProjectExcludeFromSLA.Add(new trProjectExcludeFromSLA { ProjectId = vm.ProjectId, ExcludeFromSLAId = slaId });
			}

			//Save SLA Options SUB-650
			var selSLAOption = _dbSubmission.trProjectSLAOptions.Where(x => x.ProjectId == vm.ProjectId).SingleOrDefault();
			if (selSLAOption != null)
			{
				selSLAOption.ChangedInTechVersion = vm.ChangedInTechVersion;
				selSLAOption.IncludesChangesOtherThanBug = vm.IncludesChangesOtherThanBug;
				selSLAOption.IncludesModificationsToFormalSubmission = vm.IncludesModificationsToFormalSubmission;
			}
			else
			{
				var slaOptionNew = new trProjectSLAOptions()
				{
					ProjectId = vm.ProjectId,
					ChangedInTechVersion = vm.ChangedInTechVersion,
					IncludesChangesOtherThanBug = vm.IncludesChangesOtherThanBug,
					IncludesModificationsToFormalSubmission = vm.IncludesModificationsToFormalSubmission
				};
				_dbSubmission.trProjectSLAOptions.Add(slaOptionNew);
			}

			_dbSubmission.UserId = _userContext.User.Id;
			_dbSubmission.SaveChanges();
			_dbSubmission.UserId = null;

			return RedirectToAction("Index", new { projId = vm.ProjectId });
		}
	}
}