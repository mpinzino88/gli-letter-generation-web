﻿using GLI.EFCore.Submission;
using Submissions.Core;
using Submissions.Web.Areas.Protrack.Models.Project;
using System.Linq;
using System.Web.Mvc;

namespace Submissions.Web.Areas.Protrack.Controllers
{
	public class SkipEngineeringController : Controller
	{
		private readonly IProjectService _projectService;
		private readonly SubmissionContext _dbSubmission;

		public SkipEngineeringController(IProjectService projectService, SubmissionContext submissionContext)
		{
			_projectService = projectService;
			_dbSubmission = submissionContext;
		}

		public ActionResult Index(int projectId)
		{
			var vm = new SkipEngeeringVM();
			vm.ProjectId = projectId;

			var isEngSkipped = _dbSubmission.QABundles.Where(x => x.ProjectId == projectId).Select(x => x.SkipEN).SingleOrDefault();
			vm.IsEngineeringSkipped = (isEngSkipped == true) ? true : false;

			var curStatus = _dbSubmission.trProjects.Where(x => x.Id == projectId).Select(x => x.ENStatus).SingleOrDefault();
			vm.CurrentStatus = curStatus;

			return PartialView("_SkipEngineering", vm);
		}

		public ContentResult Save(SkipEngeeringVM vm)
		{
			_projectService.SkipEngineering(vm.ProjectId, vm.IsEngineeringSkipped);
			return Content("OK");
		}
	}
}