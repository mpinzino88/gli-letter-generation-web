﻿using GLI.EFCore.Submission;
using Microsoft.EntityFrameworkCore;
using Submissions.Common;
using Submissions.Core;
using Submissions.Web.Areas.Compliance.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace Submissions.Web.Areas.Compliance.Controllers
{
	[AuthorizeUser(Permission = Permission.CPAdminQueue, HasAccessRight = AccessRight.Edit, Roles = new Role[] { Role.QAS })]
	public class AdminReviewQueueController : Controller
	{
		private readonly SubmissionContext _dbSubmission;
		private readonly IUserContext _userContext;
		private readonly IJurisdictionalDataService _jurisdictionalDataService;
		private readonly IProjectService _projectService;

		public AdminReviewQueueController(SubmissionContext submissionContext, IUserContext userContext, IJurisdictionalDataService jurisdictionalDataService, IProjectService projectService)
		{
			_dbSubmission = submissionContext;
			_userContext = userContext;
			_jurisdictionalDataService = jurisdictionalDataService;
			_projectService = projectService;
		}

		public ActionResult Index()
		{
			var vm = new IndexViewModel();
			return View(vm);
		}

		public ContentResult GetMergedProjectsInfo(int projectId)
		{
			var merged = _projectService.ProjectMerged(projectId).Select(x => x.Name).ToList();
			var mergedProjects = string.Join(",", merged);

			return Content(mergedProjects);
		}
		public ActionResult Update(string jurisdictionalDataIds)
		{
			//Update selected Jurs
			var jurisdictionalDataIdList = jurisdictionalDataIds.Split(',').Select(Int32.Parse).ToList();
			UpdateJurisdictions(jurisdictionalDataIdList);

			//Find all selected jur Projects and their Merged Items
			var mergedProjectJurisdictionalDataIds = new List<int>();

			var projectIds = _dbSubmission.trProjectJurisdictionalData.Where(x => jurisdictionalDataIdList.Contains(x.JurisdictionalDataId)).Select(x => x.ProjectId).ToList().Distinct();
			foreach (var projectId in projectIds)
			{
				var anyCPInProject = _dbSubmission.trProjectJurisdictionalData.Where(x => x.ProjectId == projectId && x.JurisdictionalData.Status == JurisdictionStatus.CP.ToString()).Any();
				if (!anyCPInProject)
				{
					var merged = _projectService.ProjectMerged(projectId).Select(x => x.Name).ToList();
					foreach (var mergedProject in merged)
					{
						mergedProjectJurisdictionalDataIds.AddRange(_dbSubmission.trProjectJurisdictionalData.Where(x => x.Project.ProjectName == mergedProject).Select(x => x.JurisdictionalDataId).ToList());
					}
				}
			}
			if (mergedProjectJurisdictionalDataIds.Count > 0)
			{
				UpdateJurisdictions(mergedProjectJurisdictionalDataIds);
			}

			//*******************************

			return Content("OK");
		}

		private void UpdateJurisdictions(List<int> jurisdictionalDataId)
		{
			//Update Jurs
			var jurisdictionalDataIds = string.Join(",", jurisdictionalDataId);
			var query = "exec devin_setcontext " + _userContext.User.Id + "; UPDATE JurisdictionalData SET JurisdictionType = Coalesce(" +
								 "(select top 1 q.changedto from " +
								 "(select loginName, userid, changedate, " +
								 "cast(cast(changedfrom as varchar) as varchar) as changedfrom, " +
								 "cast(cast(changedto as varchar) as varchar) changedto, fname, lname " +
								 "from historylog left join trlogin on userid = loginid " +
								 "where columnName = 'JurisdictionType' and tablename = 'JurisdictionalData' and primarykey = primary_ ) q " +
								 "where changedto <> '" + JurisdictionStatus.CP.ToString() + "'  " +
								 "ORDER BY ChangeDate Desc),'" + JurisdictionStatus.RA.ToString() + "') " +
								 "WHERE primary_ in (" + jurisdictionalDataIds + ")";
			_dbSubmission.Database.ExecuteSqlRaw(query);

			//For Protrack
			var jurisdictionDataList = _dbSubmission.JurisdictionalData.Where(x => jurisdictionalDataId.Contains(x.Id)).ToList();
			_jurisdictionalDataService.ProcessForProtrackDataAfterModifiedJurisdictions(jurisdictionDataList);

			//Submission Staus 
			var affectedSubmissionIds = _dbSubmission.JurisdictionalData.Where(x => jurisdictionalDataId.Contains(x.Id)).Where(x => x.Submission.Status == SubmissionStatus.CP.ToString()).Select(x => x.SubmissionId).ToList();

			foreach (var submissionId in affectedSubmissionIds)
			{
				//any cp jur exists
				var anyCPExists = _dbSubmission.JurisdictionalData.Where(x => x.SubmissionId == submissionId && x.Status == SubmissionStatus.CP.ToString()).Any();
				if (!anyCPExists)
				{
					query = "exec devin_setcontext " + _userContext.User.Id + "; UPDATE Submissions SET status = Coalesce(" +
								 "(select top 1 q.changedto from " +
								 "(select loginName, userid, changedate, " +
								 "cast(changedfrom as nvarchar) as changedfrom, " +
								 "cast(changedto as nvarchar) changedto, fname, lname " +
								 "from historylog left join trlogin on userid = loginid " +
								 "where columnName = 'status' and tablename = 'Submissions' and primarykey = keytbl ) q " +
								 "where changedto <> '" + SubmissionStatus.CP.ToString() + "'  " +
								 "ORDER BY ChangeDate Desc),'" + SubmissionStatus.PN.ToString() + "') " +
								 "WHERE keytbl in (" + submissionId + ") and status = '" + SubmissionStatus.CP.ToString() + "'";

					_dbSubmission.Database.ExecuteSqlRaw(query);
				}
			}
		}
	}
}