﻿using Submissions.Web.Models.Grids;
using Submissions.Web.Models.Query;

namespace Submissions.Web.Areas.Compliance.Models
{
	public class IndexViewModel
	{
		public IndexViewModel()
		{
			SearchVm = new AdminReviewSearch();
			ReviewGrid = new AdminReviewGrid();
		}
		public AdminReviewSearch SearchVm { get; set; }
		public AdminReviewGrid ReviewGrid { get; set; }
	}

}