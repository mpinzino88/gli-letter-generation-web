﻿using AutoMapper;
using EF.Submission;
using Submissions.Common;
using Submissions.Core.Models.SubmissionService;
using Submissions.Web.Areas.Documents.Models;
using Submissions.Web.Areas.Documents.Models.Book;
using Submissions.Web.Areas.Documents.Models.Coversheet;
using System;

namespace Submissions.Web.Mappings
{
	public class LettersProfile : Profile
	{
		public LettersProfile()
		{
			CreateMap<GLI.EFCore.Submission.tbl_LetterBook, BookEditViewModel>()
				.ForMember(dest => dest.EntryUser, opt => opt.MapFrom(src => src.EntryUser.FName + " " + src.EntryUser.LName))
				.ForMember(dest => dest.ReviewUser, opt => opt.MapFrom(src => src.ReviewUser.FName + " " + src.ReviewUser.LName))
				.ForMember(dest => dest.BillingUser, opt => opt.MapFrom(src => src.BillingUser.FName + " " + src.BillingUser.LName))
				.ForMember(dest => dest.BallyType, opt => opt.MapFrom(src => src.BallyType == null ? (LetterBookBallyType?)null : (LetterBookBallyType)Enum.Parse(typeof(LetterBookBallyType), src.BallyType)))
				//.ForMember(dest => dest.BillingSheet, opt => opt.MapFrom(src => src.BillingSheet == null ? (LetterBookBillingSheet?)null : (LetterBookBillingSheet)Enum.Parse(typeof(LetterBookBillingSheet), src.BillingSheet)))
				.ForMember(dest => dest.IsBillable, opt => opt.MapFrom(src => src.Billable))
				.ForMember(dest => dest.BillingSheet, opt => opt.Ignore())
				.ForMember(dest => dest.BillingSheetId, opt => opt.Ignore())
				.ForMember(dest => dest.BillingSheetFilter, opt => opt.Ignore())
				.ForMember(dest => dest.Department, opt => opt.MapFrom(src => (Department)Enum.Parse(typeof(Department), src.Department.Code)))
				.ForMember(dest => dest.DocumentVersionLinks, opt => opt.Ignore())
				.ForMember(dest => dest.Laboratory, opt => opt.MapFrom(src => src.Laboratory.Location + " - " + src.Laboratory.Name))
				.ForMember(dest => dest.Mode, opt => opt.Ignore())
				.ForMember(dest => dest.ReportTypes, opt => opt.Ignore())
				.ForMember(dest => dest.QAStatus, opt => opt.MapFrom(src => (LetterBookQAStatus)Enum.Parse(typeof(LetterBookQAStatus), src.QAStatus)))
				.ForMember(dest => dest.ACTStatus, opt => opt.MapFrom(src => src.ACTStatus == null ? null : (LetterBookACTStatus?)Enum.Parse(typeof(LetterBookACTStatus), src.ACTStatus)))
				.ForMember(dest => dest.StatusList, opt => opt.Ignore())
				.ForMember(dest => dest.SpecializedTypes, opt => opt.Ignore())
				.ForMember(dest => dest.IsBillingUser, opt => opt.Ignore())
				.ForMember(dest => dest.IsEntryUser, opt => opt.Ignore())
				.ForMember(dest => dest.IsReviewUser, opt => opt.Ignore())
				.ForMember(dest => dest.FileNumber, opt => opt.Ignore())
				.ForMember(dest => dest.BilledCount, opt => opt.Ignore())
				.ForMember(dest => dest.ContractNumber, opt => opt.Ignore());
			CreateMap<JurisdictionalData, JurisdictionalDataViewModel>()
				.ForMember(dest => dest.Include, opt => opt.Ignore())
				.ForMember(dest => dest.ComponentName, opt => opt.Ignore());
			CreateMap<MathEngineeringInformationViewModel, MathEngineeringInformation>()
				.ForMember(dest => dest.IsOld, opt => opt.Ignore())
				.ForMember(dest => dest.EngineeringInformationDenomCurrencyJoins, opt => opt.Ignore())
				.ForMember(dest => dest.EngineeringInformationDoubleUpTypeJoins, opt => opt.Ignore())
				.ForMember(dest => dest.EngineeringInformationJuriJoins, opt => opt.Ignore())
				.ForMember(dest => dest.MultiMulti, opt => opt.Ignore())
				.ForMember(dest => dest.Coversheet, opt => opt.Ignore())
				.ForMember(dest => dest.Cycle, opt => opt.Ignore())
				.ForMember(dest => dest.TopAwardHits, opt => opt.Ignore())
				.ForMember(dest => dest.TopAwardProgressiveHits, opt => opt.Ignore())
				.ForMember(dest => dest.TopAwardProgressiveHitsString, opt => opt.MapFrom(src => src.TopAwardProgressiveHits))
				.ForMember(dest => dest.TopAwardHitsString, opt => opt.MapFrom(src => src.TopAwardHits))
				.ForMember(dest => dest.CycleString, opt => opt.MapFrom(src => src.Cycle))
				.ForMember(dest => dest.PreviousEngineeringInformation, opt => opt.Ignore());

			CreateMap<tblSignature, SignatureDTO>()
				.ForMember(dest => dest.AlphaVersion, opt => opt.MapFrom(x => x.ComponentID))
				.ForMember(dest => dest.PublicId, opt => opt.Ignore())
				.ForMember(dest => dest.TypeId, opt => opt.MapFrom(x => x.TypeOfId))
				.ForMember(dest => dest.UsedSeed, opt => opt.Ignore())
				.ForMember(dest => dest.UsedSalt, opt => opt.Ignore())
				.ForMember(dest => dest.UsedOffsets, opt => opt.Ignore())
				.ForMember(dest => dest.Salt, opt => opt.Ignore())
				.ForMember(dest => dest.StartOffset, opt => opt.Ignore())
				.ForMember(dest => dest.EndOffset, opt => opt.Ignore())
				.ForMember(dest => dest.Result, opt => opt.Ignore())
				.ForMember(dest => dest.ScopeId, opt => opt.MapFrom(x => x.ScopeOfId))
				.ForMember(dest => dest.Version, opt => opt.MapFrom(x => x.VerifyVersion));
		}
	}
}