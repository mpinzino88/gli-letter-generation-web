﻿using AutoMapper.QueryableExtensions;
using EF.Submission;
using GLIMathOperations.Interfaces;
using Submissions.Common;
using Submissions.Core.Models.LetterContent;
using Submissions.Web.Areas.Documents.Models.Coversheet;
using System;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Z.EntityFramework.Plus;

namespace Submissions.Web.Areas.Documents.Controllers
{
	public class CoversheetController : Controller
	{
		private readonly ICoversheetGatherMethods _coverSheetGatherMethods;
		private readonly IEngineeringInformationSaveMethods _engineeringInformationSaveMethods;
		private readonly ISubmissionContext _dbSubmissionContext;

		public CoversheetController(ICoversheetGatherMethods coverSheetGatherMethods,
			IEngineeringInformationSaveMethods engineeringInformationSaveMethods,
			ISubmissionContext submissionContext)
		{
			_coverSheetGatherMethods = coverSheetGatherMethods;
			_engineeringInformationSaveMethods = engineeringInformationSaveMethods;
			_dbSubmissionContext = submissionContext;
		}

		[HttpPost]
		public ContentResult CreateMathEngineeringInfo(Guid coversheetId)
		{
			var mathCoversheet = _dbSubmissionContext
				.MathCoversheets
				.Find(coversheetId);

			if (mathCoversheet == null)
			{
				throw new HttpException((int)HttpStatusCode.NotFound, "Coversheet not found");
			}

			var mathEngineeringInfo = _engineeringInformationSaveMethods.CreateInitialEngineeringInfo(mathCoversheet);

			_dbSubmissionContext.MathEngineeringInformations.Add(mathEngineeringInfo);

			_dbSubmissionContext.SaveChanges();

			return Content(ComUtil.JsonEncodeCamelCase(mathEngineeringInfo), "application/json");
		}

		[HttpPost]
		public HttpStatusCode DeleteMathEngineeringInfo(Guid mathEngineeringInfoId)
		{
			var mathEngineerInfo = new MathEngineeringInformation { Id = mathEngineeringInfoId };

			_dbSubmissionContext.Entry(mathEngineerInfo).State = EntityState.Deleted;

			_dbSubmissionContext.SaveChanges();

			return HttpStatusCode.NoContent;
		}

		[HttpPost]
		public ContentResult EditMathEngineeringInfo(MathEngineeringInformationViewModel mathEngineeringInfoViewModel)
		{
			if (!ModelState.IsValid)
			{
				mathEngineeringInfoViewModel.ErrorMessage = GetValidationErrorsMessage();
				return Content(ComUtil.JsonEncodeCamelCase(mathEngineeringInfoViewModel), "application/json");
			}

			var mathEngineeringInfo = AutoMapper.Mapper.Map<MathEngineeringInformation>(mathEngineeringInfoViewModel);

			_dbSubmissionContext.MathEngineeringInformations.Attach(mathEngineeringInfo);

			_dbSubmissionContext.Entry(mathEngineeringInfo).State = System.Data.Entity.EntityState.Modified;

			_dbSubmissionContext.SaveChanges();

			return Content(ComUtil.JsonEncodeCamelCase(mathEngineeringInfoViewModel), "application/json");
		}

		[HttpGet]
		public ContentResult GetCoversheets(string fileNumber)
		{
			var coversheetModel = new MathCoversheetViewModel
			{
				Coversheets = _coverSheetGatherMethods
					.GetUploadedCoversheetsByFileNumber(fileNumber)
					.AsQueryable()
					.ProjectTo<MathCoversheetDTO>()
					.ToList(),
			};

			return Content(ComUtil.JsonEncodeCamelCase(coversheetModel), "application/json");
		}

		[HttpGet]
		public ContentResult GetMathEngineeringInfos(Guid coversheetId)
		{
			var mathEngineeringInfos = _dbSubmissionContext
				.MathEngineeringInformations
				.Where(x => x.CoversheetId == coversheetId)
				.ProjectTo<MathEngineeringInformationViewModel>()
				.ToList();

			return Content(ComUtil.JsonEncodeCamelCase(mathEngineeringInfos), "application/json");
		}

		private string GetValidationErrorsMessage()
		{
			var errorMessages = ModelState.Values
					.Where(x => x.Errors
						.Any())
					.SelectMany(x => x.Errors
						.Select(y => "[ " + y.ErrorMessage + " ]"))
					.ToList();
			return "The data has errors: " + string.Join(". ", errorMessages);
		}
	}
}