﻿using EF.Submission;
using GLI.EF.LetterContent;
using Submissions.Common;
using Submissions.Core.Interfaces.LetterContent;
using Submissions.Web.Areas.Documents.Models.LetterSection;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Submissions.Web.Areas.Documents.Controllers
{
	[AuthorizeUser(Permission = Permission.NoteManager, HasAccessRight = AccessRight.Read)]
	public class LetterSectionController : Controller
	{
		#region Private Members
		private readonly ILetterContentService _letterContentService;
		private readonly ILetterContentContext _letterContentContext;
		private readonly ISubmissionContext _dbSubmissions;
		private readonly IUserContext _user;
		private readonly IList<string> _blackList;

		#endregion

		public LetterSectionController(ILetterContentService letterContentService, ILetterContentContext letterContentContext, ISubmissionContext submissionContext, IUserContext user)
		{
			_letterContentService = letterContentService;
			_letterContentContext = letterContentContext;
			_dbSubmissions = submissionContext;
			_user = user;
			_blackList = new List<string>() { "Workspace Category", "PDF" };
		}

		public async Task<ActionResult> Index()
		{
			var vm = new LetterSectionIndexViewModel
			{
				LetterSections = await _letterContentContext.Sections.Select(LetterSectionIndexListItem.Projection).ToListAsync()
			};
			vm.LetterSectionOrderSet = BuildLetterSectionOrderSet(vm.LetterSections);
			vm.IsNoteManagerAdmin = Util.HasAccess(Permission.NoteManagerAdmin, AccessRight.Edit) || Util.HasAccess(Permission.NoteManagerAdmin, AccessRight.Add);

			return View(vm);
		}

		public async Task<ActionResult> Edit(int? id, Mode mode)
		{
			var result = new LetterSectionEditViewModel();
			result.Mode = mode;
			if (mode == Mode.Edit)
			{
				result = new LetterSectionEditViewModel
				{
					Mode = Mode.Edit,
					LetterSection = await _letterContentContext.Sections.Where(x => x.Id == id).Select(LetterSectionEditItem.Projection).SingleAsync()
				};
			}
			return View(result);
		}

		[HttpPost]
		public ActionResult Save(LetterSectionEditViewModel vm)
		{
			var letterSection = new Section
			{
				Active = vm.LetterSection.Active,
				Name = vm.LetterSection.Name,
				OrderId = vm.LetterSection.OrderId
			};

			if (vm.Mode == Mode.Add)
			{
				var maxOrderId = _letterContentContext.Sections.Where(x => x.OrderId != null).Select(x => x.OrderId).OrderByDescending(x => x).FirstOrDefault();
				letterSection.OrderId = maxOrderId == null ? 1 : maxOrderId + 1;
				letterSection.Active = true;
			}
			else
			{
				letterSection.Id = vm.LetterSection.Id;
			}

			_letterContentContext.Sections.Attach(letterSection);
			var sectionEntity = _letterContentContext.Entry(letterSection);
			sectionEntity.State = vm.Mode == Mode.Add ? EntityState.Added : EntityState.Modified;

			if (vm.Mode == Mode.Edit)
			{
				sectionEntity.Property(x => x.Name).IsModified = true;
				sectionEntity.Property(x => x.Active).IsModified = true;
				sectionEntity.Property(x => x.OrderId).IsModified = true;
			}

			_letterContentContext.SaveChanges();

			return Content(ComUtil.JsonEncodeCamelCase(""), "application/json");
		}

		[HttpPost]
		public ContentResult CommitLetterSectionOrder(LetterSectionOrderSet payload)
		{
			var letterSectionIds = payload.LetterSections.Select(x => x.Id).ToList();
			var sectionsToOrder = _letterContentContext.Sections.Where(x => letterSectionIds.Contains(x.Id)).ToList();

			foreach (var section in sectionsToOrder)
			{
				var target = payload.LetterSections.Where(x => x.Id == section.Id).Single();
				section.OrderId = target.TargetLocation;
			}

			_letterContentContext.SaveChanges();

			return Content(ComUtil.JsonEncodeCamelCase(BuildLetterSectionOrderSet(_letterContentContext.Sections.Select(LetterSectionIndexListItem.Projection).ToList())), "application/json");
		}

		[HttpPost]
		public ContentResult GetLetterSectionOrderSet()
		{
			return Content(ComUtil.JsonEncodeCamelCase(BuildLetterSectionOrderSet(_letterContentContext.Sections.Select(LetterSectionIndexListItem.Projection).ToList())), "application/json");
		}


		[HttpPost]
		public ContentResult CommitLetterContentOrder(LetterContentOrderPayload payload)
		{
			var letterContentIds = payload.LetterContentItems.Select(x => x.Id).ToList();
			var letterContentToOrder = _letterContentContext.LetterContents.Where(x => letterContentIds.Contains(x.Id)).ToList();

			foreach (var letterContent in letterContentToOrder)
			{
				var target = payload.LetterContentItems.Where(x => x.Id == letterContent.Id).Single();
				letterContent.OrderId = target.NewPosition;
			}

			_letterContentContext.SaveChanges();

			return Content(ComUtil.JsonEncodeCamelCase(BuildLetterContentSet(payload.SectionId)), "application/json");
		}

		[HttpPost]
		public ContentResult GetLetterContentBySectionId(int sectionId)
		{
			return Content(ComUtil.JsonEncodeCamelCase(BuildLetterContentSet(sectionId)), "application/json");
		}


		private List<LetterContentItem> BuildLetterContentSet(int sectionId)
		{
			return _letterContentContext.Sections.Where(x => x.Id == sectionId).SingleOrDefault().LetterContents.Where(x => x.Lifecycle == Lifecycle.Active).Select(x => new LetterContentItem { Id = x.Id, InitialPosition = x.OrderId, Name = x.Name }).OrderBy(x => x.InitialPosition).ToList();
		}


		/// <summary>
		/// I'm not sure what to do with this, in reality a section that has LCs associated would orphan them so maybe no deletion here.
		/// </summary>
		/// <param name="id"></param>
		private void Delete(int id)
		{
			var sections = _letterContentContext.Sections.Where(x => x.OrderId != null).OrderBy(x => x.OrderId).ToList();
			var toRemove = sections.Where(x => x.Id == id).SingleOrDefault();

			var sectionsRankedHigher = sections.Where(x => x.OrderId > toRemove.OrderId).ToList();
			_letterContentContext.Sections.Remove(toRemove);

			if (sectionsRankedHigher.Count == 0)
			{
				_letterContentContext.SaveChanges();
			}
			else
			{
				sectionsRankedHigher.ForEach(x => x.OrderId -= 1);
				_letterContentContext.SaveChanges();
			}
		}


		private LetterSectionOrderSet BuildLetterSectionOrderSet(List<LetterSectionIndexListItem> LetterSections)
		{
			var result = new LetterSectionOrderSet
			{
				LetterSections = LetterSections.Where(x => !_blackList.Contains(x.Name)).Select(x => new LetterSectionOrderItem { Id = x.Id, Name = x.Name, CurrentLocation = x.OrderId }).OrderBy(x => x.CurrentLocation).ToList()
			};
			return result;
		}
	}
}