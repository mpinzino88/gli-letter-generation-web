﻿using EF.Submission;
using Submissions.Common;
using Submissions.Core;
using Submissions.Core.Models;
using Submissions.Web.Areas.Documents.Models.DraftLetters;
using System;
using System.Linq;
using System.Web.Mvc;

namespace Submissions.Web.Areas.Documents.Controllers
{
	[AuthorizeUser(Permissions = new Permission[] { Permission.LetterBookBilling, Permission.LetterBookEntry, Permission.LetterBookReview }, HasAccessRight = AccessRight.Read)]
	public class DraftLettersController : Controller
	{
		private readonly ISubmissionContext _dbSubmission;

		private readonly ILettersService _letterService;

		private readonly IUserContext _userContext;

		private readonly ILogger _logger;

		private readonly string _errorView = "~/Views/Error/Index.cshtml";

		public DraftLettersController(ISubmissionContext dbSubmission, ILogger logger, ILettersService letterService, IUserContext userContext)
		{
			_dbSubmission = dbSubmission;
			_logger = logger;
			_letterService = letterService;
			_userContext = userContext;
		}

		public ActionResult Index(int projectId)
		{
			return View(new IndexViewModel(projectId));
		}

		public ActionResult Edit(int projectId, int draftLetterId)
		{
			var draftLetter = _dbSubmission.DraftLetters.Where(x => x.Id == draftLetterId).First();

			var model = new EditViewModel
			{
				AddendumGpsFileId = draftLetter.AddendumGpsFileId,
				AddendumLetterhead = draftLetter.LetterheadAddendumId != null ? ((LetterHeadType)draftLetter.LetterheadAddendumId).GetEnumDescription() : "",
				AddendumLetterheadId = draftLetter.LetterheadAddendumId,
				AttachCorrectionAddendum = draftLetter.AttachCorrectionAddendum ?? false,
				DraftDate = draftLetter.DraftDate,
				DraftLetterId = draftLetter.Id,
				FilePath = draftLetter.FilePath,
				GPSFilePath = draftLetter.GPSFilePath,
				LetterheadId = draftLetter.LetterheadId,
				Letterhead = draftLetter.LetterheadId != null ? ((LetterHeadType)draftLetter.LetterheadId).GetEnumDescription() : "",
				PdfFileGpsId = draftLetter.PdfFileGpsId,
				ProjectId = projectId,
				RepeatLetterhead = draftLetter.RepeatOverlay ?? false,
				RepeatAddLetterhead = draftLetter.RepeatOverlayAddendum ?? false,
				Status = ParseEnum<DraftLetterStatus>(draftLetter.Status),
				TimelineRequested = draftLetter.IsTimelineRequested,
				TimelineType = draftLetter.TimelineType,
				UpdatedDateReason = (UpdateDateReason?)draftLetter.UpdatedDateReason,
				WordFileGPSId = draftLetter.WordFileGpsId
			};

			return View(model);
		}

		[HttpPost]
		public ActionResult Edit(EditViewModel editViewModel)
		{
			DraftLetterModel draftLetterModel = new DraftLetterModel
			{
				AddendumLetterheadId = editViewModel.AddendumLetterheadId,
				AttachCorrectionAddendum = editViewModel.AttachCorrectionAddendum,
				draftLetterId = editViewModel.DraftLetterId,
				DraftDate = editViewModel.DraftDate,
				FilePath = editViewModel.FilePath,
				GPSFilePath = editViewModel.GPSFilePath,
				LetterheadId = editViewModel.LetterheadId,
				RepeatAddLetterhead = editViewModel.RepeatAddLetterhead,
				RepeatLetterhead = editViewModel.RepeatLetterhead,
				Status = editViewModel.Status.ToString(),
				IsTimelineRequested = editViewModel.TimelineRequested,
				TimelineType = editViewModel.TimelineType,
				UpdatedDateReason = editViewModel.UpdatedDateReason
			};

			_letterService.SetUser(_userContext.User);

			try
			{
				_letterService.UpdateDraftLetter(draftLetterModel);
			}
			catch (Exception e)
			{
				_logger.LogError(e, string.Format("Error updating draft letter with id: {0}", editViewModel.DraftLetterId));
				ViewBag.Message = string.Format("There was a problem editing the draft letter: {0}", e.Message);
				ViewBag.Title = "DraftLetter Edit Error";
				return View(_errorView);
			}

			return RedirectToAction("Index", new { projectId = editViewModel.ProjectId });
		}

		private static T? ParseEnum<T>(string enumString) where T : struct
		{
			bool success = Enum.TryParse<T>(enumString, out T output);
			if (!success)
			{
				return new T?();
			}
			return output;
		}
	}
}