﻿extern alias ZEFCore;
using EF.GPS;
using GLI.EFCore.Submission;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using Submissions.Common;
using Submissions.Common.Email;
using Submissions.Core;
using Submissions.Core.Interfaces.LetterGen;
using Submissions.Core.Models;
using Submissions.Core.Models.LetterBook;
using Submissions.Web.Areas.Documents.Create;
using Submissions.Web.Areas.Documents.Models;
using Submissions.Web.Areas.Documents.Models.Create;
using Submissions.Web.Utility.Email;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Web;
using System.Web.Mvc;
using ZEFCore::Z.EntityFramework.Plus;

namespace Submissions.Web.Areas.Documents.Controllers
{
	[AuthorizeUser(Permissions = new Permission[] { Permission.LetterBookBilling, Permission.LetterBookEntry, Permission.LetterBookReview }, HasAccessRight = AccessRight.Read)]
	public class CreateVueController : Controller
	{
		private readonly SubmissionContext _dbSubmission;
		private readonly IProjectService _projectService;
		private readonly IDocumentService _documentService;
		private readonly IUser _user;
		private readonly IUserContext _userContext;
		private readonly IEmailService _emailService;
		private readonly IDocumentDataService _documentUpdateService;
		private readonly IGPSContext _gpsContext;
		private readonly ISupplementalLettersService _supplementalsService;
		private readonly ILetterDataUpdateService _letterDataUpdateService;
		private readonly IPdfService _iPdfService;
		private readonly ILetterbookService _letterbookService;
		private readonly IJurisdictionalDataService _jurisdictionalDataService;
		private readonly ILogger _logger;
		private readonly IConfig _config;
		private readonly INotificationService _notificationService;
		private readonly ILetterGenerationService _letterGenServices;

		private static DateTime GenerationCutoffDate = new DateTime(2016, 3, 15);

		private static List<string> _draftDateJurisdictions = new List<string>{
			CustomJurisdiction.WVirginia.GetEnumDescription(),
			CustomJurisdiction.WVLimited.GetEnumDescription(),
			CustomJurisdiction.WVTableGames.GetEnumDescription()
		};

		private static List<string> _letterWritingTasks = new List<string>
		{
			BundleTask.LetterWriting.GetEnumDescription(),
			BundleTask.Addendum.GetEnumDescription(),
			BundleTask.Correction.GetEnumDescription(),
			BundleTask.LotoQuebecFinalizing.GetEnumDescription(),
			BundleTask.Rescission.GetEnumDescription(),
			BundleTask.RevocationWithoutReplacement.GetEnumDescription(),
			BundleTask.Decline.GetEnumDescription(),
			BundleTask.ReDraft.GetEnumDescription(),
			BundleTask.ObtainingTKNumber.GetEnumDescription()
		};

		private static Dictionary<string, List<string>> _nonBillableJurisdictions = new Dictionary<string, List<string>>
		{
			{((int)CustomJurisdiction.ArgentinaBuenosAires).ToJurisdictionIdString(),new List<string>()
			{
				((int)CustomJurisdiction.ArgentinaCordoba).ToJurisdictionIdString(),
				((int)CustomJurisdiction.ArgentinaMendoza).ToJurisdictionIdString(),
				((int)CustomJurisdiction.ArgentinaSantaFe).ToJurisdictionIdString(),
				((int)CustomJurisdiction.ArgentinaLoteríaNacionalSociedaddelEstado).ToJurisdictionIdString(),
				((int)CustomJurisdiction.ArgentinaSalta).ToJurisdictionIdString(),
				((int)CustomJurisdiction.ArgentinaEntreRios).ToJurisdictionIdString(),
				((int)CustomJurisdiction.ArgentinaFormosa).ToJurisdictionIdString(),
				((int)CustomJurisdiction.ArgentinaMisiones).ToJurisdictionIdString()
			}},
			{((int)CustomJurisdiction.OklahomaClassIII).ToJurisdictionIdString(),new List<string> //171
			{
				((int)CustomJurisdiction.OklahomaChickasawIII).ToJurisdictionIdString(),
				((int)CustomJurisdiction.OklahomaChoctawIII).ToJurisdictionIdString()
			}},
			{((int)CustomJurisdiction.OklahomaChickasawIII).ToJurisdictionIdString(),new List<string>
			{
				((int)CustomJurisdiction.OklahomaChoctawIII).ToJurisdictionIdString()
			}},
			{((int)CustomJurisdiction.OklahomaChickasawII).ToJurisdictionIdString(),new List<string>
			{
				((int)CustomJurisdiction.OklahomaChoctawII).ToJurisdictionIdString()
			}},
			{((int)CustomJurisdiction.Missouri).ToJurisdictionIdString(),new List<string>
			{
				((int)CustomJurisdiction.MissouriCashless).ToJurisdictionIdString()
			}},
			{((int)CustomJurisdiction.Illinois).ToJurisdictionIdString(),new List<string>
			{
				((int)CustomJurisdiction.IllinoisCashless).ToJurisdictionIdString()
			}},
			{((int)CustomJurisdiction.WISCONSIN).ToJurisdictionIdString(),new List<string>
			{
				((int)CustomJurisdiction.WIForestCountyPotawatomi).ToJurisdictionIdString()
			}},
			{((int)CustomJurisdiction.IATribal).ToJurisdictionIdString(),new List<string>
			{
				((int)CustomJurisdiction.Iowa).ToJurisdictionIdString()
			}},
			{((int)CustomJurisdiction.MNShakopeeMdewakantonSioux).ToJurisdictionIdString(),new List<string>
			{
				((int)CustomJurisdiction.Minnesota).ToJurisdictionIdString()
			}}
		};

		private static List<string> _appliesBallyType = new List<string>
		{
			CustomManufacturer.BAL.ToString(),
			CustomManufacturer.BLY.ToString(),
			CustomManufacturer.SHU.ToString(),
			CustomManufacturer.WMS.ToString(),
			CustomManufacturer.ADC.ToString(),
			CustomManufacturer.SDS.ToString()
		};

		public CreateVueController(
			SubmissionContext dbSubmission,
			IUser user,
			IUserContext userContext,
			IDocumentService documentService,
			IQueryService queryService,
			IEmailService emailService,
			IDocumentDataService dataUpdatesService,
			IGPSContext gpsContext,
			IProjectService projectService,
			ISupplementalLettersService supplementalsService,
			IPdfService iPdfService,
			ILetterDataUpdateService databaseUpdatesService,
			ILetterbookService letterbookService,
			IJurisdictionalDataService jurisdictionalDataService,
			ILogger logger,
			IConfig config,
			INotificationService notificationService,
			ILetterGenerationService letterGenerationService
		)
		{
			_dbSubmission = dbSubmission;
			_documentService = documentService;
			_user = user;
			_userContext = userContext;
			_emailService = emailService;
			_documentUpdateService = dataUpdatesService;
			_gpsContext = gpsContext;
			_projectService = projectService;
			_supplementalsService = supplementalsService;
			_iPdfService = iPdfService;
			_letterDataUpdateService = databaseUpdatesService;
			_letterbookService = letterbookService;
			_jurisdictionalDataService = jurisdictionalDataService;
			_logger = logger;
			_config = config;
			_notificationService = notificationService;
			_letterGenServices = letterGenerationService;
		}

		#region Build Intial VM
		public ActionResult Index(int bundleId)
		{
			var bundle = _dbSubmission.QABundles.Include("Project").Include("Tasks").Where(x => x.Id == bundleId).Select(x => new { x.Id, x.ProjectId, x.ProjectName, BundleType = x.Project.BundleType.Id, x.Project.FileNumber, x.QAReviewer, x.Project.ENManager, x.Project.TargetDate, x.QAStatus, x.Tasks, x.Project.ENStatus, x.ENInvolvementWhileFL }).SingleOrDefault();
			if (bundle == null)
			{
				throw new NullReferenceException("Bundle does not exist.");
			}

			//var firstSub = bundle.Project.JurisdictionalData.FirstOrDefault().JurisdictionalData.Submission;
			//TODODLB: Hook up a view.

			var vm = new IndexVueModel
			{
				BundleId = bundleId,
				ProjectId = bundle.ProjectId,
				BundleName = bundle.ProjectName,
				BundleStatus = bundle.QAStatus,
				ProjectStatus = bundle.ENStatus,
				BundleTarget = bundle.TargetDate,
				IsClosedBundle = (bundle.BundleType == (int)BundleType.OriginalTesting) ? false : true,
				ProjectIndicator = _projectService.GetBundleType(bundle.ProjectId),
				ENInvolvementNeeded = bundle.ENInvolvementWhileFL,
				MergedProjectIds = GetAllMergedProjects(bundle.ProjectId)
			};

			vm.CreateLetters = SetLetterModels(bundleId);

			//Available drop dpwn options
			vm.Languages = LookupsStandard.ConvertEnum<LetterLanguage>(useSameValueDescription: true).ToList();

			vm.NonDraftLetterheads = _dbSubmission.tbl_lst_Letterheads.Where(x => x.IsDraft == false).Select(x => new SelectListItem()
			{
				Text = x.Description,
				Value = x.Id.ToString(),
				Selected = false
			}).ToList();
			vm.DraftLetterheads = _dbSubmission.tbl_lst_Letterheads.Where(x => x.IsDraft == true).Select(x => new SelectListItem()
			{
				Text = x.Description,
				Value = x.Id.ToString(),
				Selected = false
			}).ToList();
			vm.SettableStatuses = ComUtil.ClosedJurisdictionStatuses().Select(x => new SelectListItem { Value = x.ToString(), Text = x.ToString() }).ToList();
			vm.SettableStatuses.Add(new SelectListItem { Value = JurisdictionStatus.CP.ToString(), Text = JurisdictionStatus.CP.ToString() });

			//En Review Model
			var RCEngineer = _dbSubmission.trHistories
				.Where(x => x.Project.ProjectName == bundle.ProjectName && x.ENProjectStatus == "RC")
				.Select(x => x.User)
				.OrderByDescending(x => x.AddDate)
				.FirstOrDefault();

			var letterWriter = GetBundleLetterWriterOrReviewerUser(bundle.Tasks.ToList());
			vm.LWLocationId = (int)letterWriter.LocationId;

			var recipientsList = new List<string>
			{
				(RCEngineer != null ? RCEngineer.Id : 0).ToString(),
				(bundle.ENManager != null ? bundle.ENManager.Id : 0).ToString(),
				(bundle.QAReviewer != null ? bundle.QAReviewer.Id: 0).ToString(),
				(letterWriter != null ? letterWriter.Id : 0).ToString()
			};
			vm.ENReviewData = new ENReviewData
			{
				RecipientUserIds = recipientsList,
				SelRecipientUsers = new List<SelectListItem>
					{
						 new SelectListItem {
							 Value = RCEngineer != null ? RCEngineer.Id.ToString() : "0",
							 Text = RCEngineer != null ? RCEngineer.FName + " " + RCEngineer.LName : "No RC Engineer",
							 Selected = true},
						 new SelectListItem {
							 Value = bundle.ENManager != null ? bundle.ENManager.Id.ToString() : "0",
							 Text = bundle.ENManager != null ? bundle.ENManager.FName + " " + bundle.ENManager.LName : "No EN Manager",
							 Selected = true},
						 new SelectListItem {
							 Value = bundle.QAReviewer != null ? bundle.QAReviewer.Id.ToString() :"0" ,
							 Text = bundle.QAReviewer != null ? bundle.QAReviewer.FName + " " + bundle.QAReviewer.LName : "No QA Reviewer",
							 Selected = true},
						 new SelectListItem {
							 Value = letterWriter != null ? letterWriter.Id.ToString() : "0",
							 Text = letterWriter != null ? letterWriter.FName + " " + letterWriter.LName : "No Letter Writer",
							 Selected = true}
					}
			};

			//Compliance Review
			vm.ComplianceInfo.TargetDate = (DateTime)((bundle.TargetDate != null) ? bundle.TargetDate : DateTime.Now);
			vm.ComplianceInfo.IncludeFilesInTicket = true;

			//NURVEmail Default users
			var emailsTo = _notificationService.GetEmailToAddresses(Submissions.Common.Email.EmailType.NURVFromLB).ToList();
			var users = _dbSubmission.trLogins.Where(x => emailsTo.Contains(x.Email)).Select(x => new { x.Id, x.FName, x.LName }).ToList();
			vm.NURVEmailToUserIds = users.Select(x => x.Id).ToList();
			vm.NURVEmailToUsers = users.Select(x => new SelectListItem { Value = x.Id.ToString(), Text = x.FName + " " + x.LName, Selected = true }).ToList();

			//Set extra permission checks
			vm.CanRepostLetters = Util.HasAccess(Permission.CanRepostLetters, AccessRight.Edit);
			vm.CanDeleteLetter = Util.HasAccess(Permission.Submission, AccessRight.Edit) || Util.HasAccess(Permission.LetterBookDeletePDF, AccessRight.Edit);
			vm.CanAccessLetterGen = Util.HasAccess(Permission.LetterGeneration, AccessRight.Edit) || Util.HasAccess(Permission.LetterGeneration, AccessRight.Read);

			//Will be deleting once it goes live
			vm.IsProductionEnvironment = (_config.Environment == Common.Environment.Production) ? true : false;

			//find nonBillableJurisdictions
			vm.NonBillableJurisdictions = SetNonBillableJurisdictions(vm.CreateLetters, vm.BundleName); ;

			return View(vm);
		}

		private List<CreateLetterModel> SetLetterModels(int bundleId)
		{
			var createLetters = new List<CreateLetterModel>();

			var bundle = _dbSubmission.QABundles.Include("Project").Include("Tasks").Where(x => x.Id == bundleId).Select(x => new { x.Id, x.ProjectId, x.ProjectName, BundleType = x.Project.BundleType.Id, x.Project.FileNumber, x.QAReviewer, x.Project.ENManager, x.Project.TargetDate, x.QAStatus, x.Tasks, x.Project.ENStatus }).SingleOrDefault();
			var isClosedBundle = (bundle.BundleType == (int)BundleType.OriginalTesting) ? false : true;

			var letterWriter = GetBundleLetterWriterOrReviewerUser(bundle.Tasks.ToList());
			if (letterWriter == null)
			{
				letterWriter = _dbSubmission.trLogins.Where(x => x.Id == _userContext.User.Id).SingleOrDefault();
			}

			var bundleJurisdictions = GetAllAvailableJurisdictions(bundleId);
			var allRelevantGpsDocuments = _documentService.GetLettersAllTypes(bundleJurisdictions.Select(jd => jd.Id).ToList());
			var jurisdictionalDataGroups = BuildJurisdictionGroupModels(bundleJurisdictions, allRelevantGpsDocuments);

			foreach (var group in jurisdictionalDataGroups)
			{
				//Get all different available letters 
				var coverLetter = group.AvailableSourceDocs.Where(x => x.GPSItemGPSOptions.Any(y => y.Name == GPSOptions.BasicCoverPageSource.ToString())).OrderByDescending(x => x.LifeCycle.CreatedOn).FirstOrDefault();
				var backLetter = group.AvailableSourceDocs.Where(x => x.GPSItemGPSOptions.Any(y => y.Name == GPSOptions.BasicFinalPageSource.ToString())).OrderByDescending(x => x.LifeCycle.CreatedOn).FirstOrDefault(); //Need commented for now || y.Name == GPSOptions.LetterGenBackPageSource.ToString()
				var correctionLetter = group.AvailableSourceDocs.Where(x => x.GPSItemGPSOptions.Any(y => y.Name == GPSOptions.AddendumCoverPageSource.ToString())).OrderByDescending(x => x.LifeCycle.CreatedOn).FirstOrDefault();
				var mainLetter = group.AvailableSourceDocs.Where(x => x.GPSItemGPSOptions.Any(y => y.Name == GPSOptions.BasicSourceLetter.ToString())).OrderByDescending(x => x.LifeCycle.CreatedOn).FirstOrDefault(); //for now commentiong || y.Name == GPSOptions.LetterGenSourcePDF.ToString() || y.Name == GPSOptions.LetterGenSourceDocument.ToString()
				if (isClosedBundle)
				{
					//PT-1409, before mainletter was getting replaced
					var correctionLetterClosed = group.AvailableSourceDocs.Where(x => x.GPSItemGPSOptions.Where(y => y.LifeCycleState.ToLower() == "active").ToList().Any(y => y.Name == GPSOptions.CorrectionSourceLetter.ToString() || y.Name == GPSOptions.AddendumSourceLetter.ToString())).OrderByDescending(x => x.LifeCycle.CreatedOn).FirstOrDefault();
					if (correctionLetterClosed != null)
					{
						if (mainLetter == null)
						{
							mainLetter = correctionLetterClosed;
						}
						else
						{
							correctionLetter = correctionLetterClosed;
						}
					}
				}

				var firstSub = group.JurisdictionalDatas.FirstOrDefault().Submission;
				var manufCode = firstSub.ManufacturerCode;
				var subType = firstSub.Type;
				var company = group.JurisdictionalDatas.FirstOrDefault().Company.Name;
				var companyId = group.JurisdictionalDatas.FirstOrDefault().CompanyId;
				var updateStatusTo = ComUtil.GetLetterApprovalStatus(group.JurisdictionId, subType);

				createLetters.Add(new CreateLetterModel
				{
					BundleId = bundleId,
					DateOfReport = DateTime.Now,
					ManufacturerCode = manufCode,
					SubmissionType = subType,
					CompanyId = (int)companyId,
					Company = company,
					CorrectionCoverPage = new IndividualLetterData
					{
						FileId = correctionLetter?.Id ?? 0,
						FileName = correctionLetter?.Name,
						Type = LetterBookPageType.Addendum
					},
					GenericCoverPage = new IndividualLetterData
					{
						FileId = coverLetter?.Id ?? 0,
						FileName = coverLetter?.Name,
						Type = LetterBookPageType.General
					},
					MainLetter = new IndividualLetterData
					{
						FileId = mainLetter?.Id ?? 0,
						FileName = mainLetter?.Name,
						Type = LetterBookPageType.Main
					},
					BackPage = new IndividualLetterData
					{
						FileId = backLetter?.Id ?? 0,
						FileName = backLetter?.Name,
						Type = LetterBookPageType.Back
					},
					LetterApprovalData = new LetterApprovalData
					{
						AvailableSourceDocs = group.AvailableSourceDocs.Select(doc => new GPSItemViewModel(doc)).ToList(),
						AvailableSourceDocsCover = group.AvailableSourceDocs.Where(x => x.GPSItemGPSOptions.Any(y => y.Name == GPSOptions.BasicCoverPageSource.ToString())).Select(doc => new GPSItemViewModel(doc)).ToList(),
						AvailableSourceDocsBack = group.AvailableSourceDocs.Where(x => x.GPSItemGPSOptions.Any(y => y.Name == GPSOptions.BasicFinalPageSource.ToString() || y.Name == GPSOptions.LetterGenBackPageSource.ToString())).Select(doc => new GPSItemViewModel(doc)).ToList(),
						JurisdictionalDataIds = group.JurisdictionalDatas.Select(data => new JurisdictionalDataSubmissionIdPair
						{
							JurisdictionalDataId = data.Id,
							SubmissionId = (int)data.SubmissionId,
							IdNumber = data.Submission.IdNumber,
							GameName = data.Submission.GameName,
							DateCode = data.Submission.DateCode,
							Version = data.Submission.Version,
							FileNumber = data.Submission.FileNumber,
							IsAttachedToLetter = true,
							Status = data.Status,
							PdfPath = data.PdfPath
						}).ToList(),
						JurisdictionId = group.JurisdictionId,
						JurisdictionName = group.JurisdictionName,
						//SetStatus = updateStatusTo.ToString() Do we need it?
					},
					Translations = new List<TranslationLetter>(),
					JurisdictionId = group.JurisdictionId.ToJurisdictionIdString(),
					FileNumber = bundle.FileNumber,
					HasExistingCertification = group.HasExistingCertification,
					CurrentJurisdictionalStatus = group.CurrentJurisdictionalStatus,
					DefaultApprovalStatus = updateStatusTo.ToString(),
					IsDraftJurisdiction = (updateStatusTo == JurisdictionStatus.DR) ? true : false,
					IsSelectedToProcess = false,
					ExistingPdfPath = (group.HasExistingCertification) ? group.PdfPath : "",
					ExistingPdfName = (group.HasExistingCertification) ? Path.GetFileName(group.PdfPath) : null,
					CompleteDate = group.CloseDate ?? DateTime.Now,
					UpdateDate = group.UpdatedDate ?? DateTime.Now,
					UpdatedDateReasonId = group.UpdateReasonId,
					UpdatedDateReason = (group.UpdateReasonId != null) ? _dbSubmission.tbl_lst_UpdatedDateReason.Where(x => x.Id == group.UpdateReasonId).Select(x => x.Reason).FirstOrDefault() : "",
					DraftDate = group.DraftDate ?? DateTime.Now,
					CertificationLabId = (group.CertificationLabId != null && group.CertificationLabId != 0) ? group.CertificationLabId : letterWriter.LocationId,
					CertificationLab = (group.CertificationLabId != null && group.CertificationLabId != 0) ? _dbSubmission.trLaboratories.Where(x => x.Id == group.CertificationLabId).Select(x => x.Location + " - " + x.Name).FirstOrDefault() : letterWriter.Location.Location + " - " + letterWriter.Location.Name,
					CopyToWeb = true,
					BillingInfo = new BillingInfo() { ACTStatus = LetterBookACTStatus.Pending, ContractNumber = group.JurisdictionalDatas.FirstOrDefault().ContractNumber, LaboratoryId = letterWriter.LocationId, Lab = letterWriter.Location.Location + " - " + letterWriter.Location.Name },
					ContractTypeId = group.ContractTypeId,
					BillingParty = group.BillingParty
				});
			}

			//Process for more data setting, Update DocumentType,ApprovalStatus, update flag for exisitng pdf
			foreach (var letter in createLetters)
			{
				var selectedJurs = letter.LetterApprovalData.JurisdictionalDataIds.Select(x => x.JurisdictionalDataId).ToList();

				//default status
				letter.SelectedApprovalStatus = (letter.CurrentJurisdictionalStatus == JurisdictionStatus.RA.ToString() || letter.CurrentJurisdictionalStatus == JurisdictionStatus.CP.ToString()) ? letter.DefaultApprovalStatus : letter.CurrentJurisdictionalStatus;

				SetDefaultDocumentTypeAndStatus(letter, bundle.Tasks.ToList(), isClosedBundle);

				var existingDocumentVersion = _dbSubmission.DocumentVersionLinks.Where(x => selectedJurs.Contains(x.JurisdictionalDataId) && x.DocumentVersion.FilePath == letter.ExistingPdfPath && x.ProjectId == bundle.ProjectId)
												.OrderByDescending(x => x.DocumentVersion.ApproveDate)
												.Select(x => new { x.DocumentVersion.Document.DocumentTypeId, x.DocumentVersionId, x.DocumentVersion.Version, x.DocumentVersion.DocumentId, x.DocumentVersion.ContainsRVNU, x.Billable, x.DocumentVersion.LetterheadGPSItemId, x.DocumentVersion.LetterheadPath, x.DocumentVersion.RepeatOVerlay })
												.FirstOrDefault();

				//TODO -LetterBook, confirm if while working on correction should it increament version?
				if (existingDocumentVersion != null)
				{
					letter.DocumentId = existingDocumentVersion.DocumentId;
					letter.DocumentVersion = existingDocumentVersion.Version;
					letter.DocumentVersionId = existingDocumentVersion.DocumentVersionId;
					letter.DocumentTypeId = existingDocumentVersion.DocumentTypeId;
					letter.DocumentType = ((DocumentType)existingDocumentVersion.DocumentTypeId).ToString(); // check
					letter.ExistingPdfDocumentType = ((DocumentType)existingDocumentVersion.DocumentTypeId).ToString(); //check
					letter.ContainsRVNU = existingDocumentVersion.ContainsRVNU;
					var gpsTranslations = _dbSubmission.tbl_DocumentVersionTranslations.Where(x => x.DocumentVersionId == existingDocumentVersion.DocumentVersionId).Select(x => new { LanguageId = x.LanguageId, PdfPath = x.PdfPath, Id = x.Id }).ToList();
					var webTranslations = _dbSubmission.SubmissionLetters.Where(x => selectedJurs.Contains((int)x.JurisdictionalDataId)).Select(x => x.Letter.PdfPath).ToList().Distinct();
					letter.TranslationCount = gpsTranslations.Count(); //check
					letter.MainTranslationsDisplay = gpsTranslations.Select(x => new MainPdfsDisplay()
					{
						Language = ((LetterLanguage)x.LanguageId).ToString(),
						PdfPath = x.PdfPath,
						PdfName = (x.PdfPath != null) ? Path.GetFileName(x.PdfPath) : null,
						Id = x.Id,
						ExistsInDB = (webTranslations.Any(w => w == x.PdfPath))
					}).ToList();

					//update flag to mark pdf is already attached
					letter.LetterApprovalData.JurisdictionalDataIds.ForEach(x => x.IsAttachedToLetter = true);
					//Get current billingInfo so report has same info while repost
					letter.BillingInfo = GetBillingInfoForDocVersion(letter.DocumentVersionId);
					letter.LetterApprovalData.IsBillable = false; //commented Repost should be Non-Billable  existingDocumentVersion.Billable;
					letter.MainLetter.LetterheadId = _dbSubmission.tbl_lst_Letterheads.Where(x => x.GPSItemId == existingDocumentVersion.LetterheadGPSItemId).Select(x => x.Id).FirstOrDefault();
					letter.MainLetter.LetterheadFilePath = existingDocumentVersion.LetterheadPath;
					letter.MainLetter.RepeatOverlay = existingDocumentVersion.RepeatOVerlay;
				}

				//update draft flag 
				if (letter.SelectedApprovalStatus == JurisdictionStatus.DR.ToString()
					|| letter.SelectedApprovalStatus == JurisdictionStatus.LQ.ToString()
					|| letter.CurrentJurisdictionalStatus == JurisdictionStatus.DR.ToString()
					|| letter.CurrentJurisdictionalStatus == JurisdictionStatus.LQ.ToString())
				{
					//if (letter.CurrentJurisdictionalStatus != JurisdictionStatus.AP.ToString() || letter.DocumentTypeId == (int)DocumentType.ReDraft)
					if (letter.SelectedApprovalStatus == JurisdictionStatus.DR.ToString() || letter.SelectedApprovalStatus == JurisdictionStatus.LQ.ToString())
					{
						letter.IsDraftLetter = true;
					}
					if (letter.CurrentJurisdictionalStatus != JurisdictionStatus.RA.ToString())
					{
						var draftLetterId = _dbSubmission.SubmissionDrafts.Where(x => x.DraftLetter.FilePath == letter.ExistingPdfPath && selectedJurs.Contains((int)x.JurisdictionDataId)).Select(x => (int)x.DraftLetterId).FirstOrDefault();
						if (draftLetterId != 0)
						{
							letter.DraftLetter = BuildDraftLetterModel(draftLetterId);
						}
					}
				}

			}

			return createLetters.OrderBy(x => x.HasExistingCertification).ToList();
		}

		private List<JurisdictionalDataGroup> BuildJurisdictionGroupModels(List<JurisdictionalData> bundleJurisdictionalDatas, List<EF.GPS.GPSItem> allRelevantGpsDocuments)
		{
			var jurisdictionalDatas = bundleJurisdictionalDatas
										.Where(pjd => pjd.Status != JurisdictionStatus.RJ.ToString() && pjd.Status != JurisdictionStatus.WD.ToString())
										.Select(pjd => pjd).ToList();

			//***update pdfPath for DR so it fits in a group
			var draftJurisdictionIds = jurisdictionalDatas.Where(x => x.Status == JurisdictionStatus.DR.ToString()).Select(x => x.Id).ToList(); //curently DR 
			var submissionDraftsPathInfo = _dbSubmission.SubmissionDrafts.Where(x => draftJurisdictionIds.Contains((int)x.JurisdictionDataId))
										.Select(x => new
										{
											pdfPath = x.DraftLetter.FilePath,
											jurisdictionalDataId = x.JurisdictionDataId
										}).ToList();

			var draftJurisdictions = jurisdictionalDatas.Where(x => draftJurisdictionIds.Contains(x.Id)).ToList();
			foreach (var draftJur in draftJurisdictions)
			{
				draftJur.PdfPath = submissionDraftsPathInfo.Where(y => y.jurisdictionalDataId == draftJur.Id).Select(y => y.pdfPath).SingleOrDefault();
			}
			//*********

			var jurisdictionalDataGroups = jurisdictionalDatas
				.GroupBy(jd => new { jd.Jurisdiction, jd.Status, PdfPath = (jd.PdfPath == null || jd.PdfPath == "") ? null : jd.PdfPath })
				.Select(group => new JurisdictionalDataGroup(group.ToList())).ToList();

			foreach (var jurDataGroup in jurisdictionalDataGroups)
			{
				jurDataGroup.HasExistingCertification = jurDataGroup.JurisdictionalDatas.Any(jd => !string.IsNullOrWhiteSpace(jd.PdfPath));
				jurDataGroup.AvailableSourceDocs = allRelevantGpsDocuments
					.Where(doc => doc.GPSJurisdictionalDatas.Any(gjd => gjd.JuriId == Convert.ToInt32(jurDataGroup.Jurisdiction.Id) && gjd.LifeCycleState == "active")).ToList();
			}

			return jurisdictionalDataGroups.OrderBy(jdg => jdg.Jurisdiction.Name).ThenBy(jdg => jdg.CurrentJurisdictionalStatus).ToList();
		}

		private List<CreateLetterModel> BuildSupplementJurisdictionGroupAndLetterModels(List<JurisdictionalData> bundleJurisdictionalDatas, int bundleId, List<EF.GPS.GPSItem> allRelevantGpsDocuments)
		{
			var bundle = _dbSubmission.QABundles.Include("Project").Include("Tasks").Where(x => x.Id == bundleId).Select(x => new { x.Id, x.ProjectId, x.ProjectName, BundleType = x.Project.BundleType.Id, x.Project.FileNumber, x.QAReviewer, x.Project.ENManager, x.Project.TargetDate, x.QAStatus, x.Tasks, x.Project.ENStatus }).SingleOrDefault();
			var letterWriter = GetBundleLetterWriterOrReviewerUser(bundle.Tasks.ToList());
			if (letterWriter == null)
			{
				letterWriter = _dbSubmission.trLogins.Where(x => x.Id == _userContext.User.Id).SingleOrDefault();
			}

			var supplementLists = new List<CreateLetterModel>();

			var jurisdictionalDataGroups = bundleJurisdictionalDatas
				.GroupBy(jd => new { jd.Jurisdiction, jd.Status })
				.Select(group => new JurisdictionalDataGroup(group.ToList())).ToList();

			foreach (var jurDataGroup in jurisdictionalDataGroups)
			{
				jurDataGroup.AvailableSourceDocs = allRelevantGpsDocuments
					.Where(doc => doc.GPSJurisdictionalDatas.Any(gjd => gjd.JuriId == Convert.ToInt32(jurDataGroup.Jurisdiction.Id) && gjd.LifeCycleState == "active")).ToList();
			}

			var jurGroups = jurisdictionalDataGroups.OrderBy(jdg => jdg.Jurisdiction.Name).ThenBy(jdg => jdg.CurrentJurisdictionalStatus).ToList();

			foreach (var group in jurGroups)
			{
				var company = group.JurisdictionalDatas.FirstOrDefault().Company.Name;
				var companyId = group.JurisdictionalDatas.FirstOrDefault().CompanyId;

				supplementLists.Add(new CreateLetterModel
				{
					BundleId = bundleId,
					JurisdictionId = group.JurisdictionId,
					FileNumber = group.JurisdictionalDatas.FirstOrDefault().Submission.FileNumber,
					SubmissionType = group.JurisdictionalDatas.FirstOrDefault().Submission.Type,
					ManufacturerCode = group.JurisdictionalDatas.FirstOrDefault().Submission.ManufacturerCode,
					Company = company,
					CompanyId = (int)companyId,
					MainLetter = new IndividualLetterData
					{
						Type = LetterBookPageType.Main
					},
					LetterApprovalData = new LetterApprovalData
					{
						AvailableSourceDocs = group.AvailableSourceDocs.Select(doc => new GPSItemViewModel(doc)).ToList(),
						JurisdictionalDataIds = group.JurisdictionalDatas.Select(data => new JurisdictionalDataSubmissionIdPair
						{
							JurisdictionalDataId = data.Id,
							SubmissionId = (int)data.SubmissionId,
							IdNumber = data.Submission.IdNumber,
							GameName = data.Submission.GameName,
							DateCode = data.Submission.DateCode,
							Version = data.Submission.Version,
							FileNumber = data.Submission.FileNumber,
							IsAttachedToLetter = true
						}).ToList(),
						JurisdictionId = group.JurisdictionId,
						JurisdictionName = group.JurisdictionName,
						//SetStatus = updateStatusTo.ToString() Do we need it?
					},
					DateOfReport = DateTime.Now,
					CompleteDate = DateTime.Now,
					CurrentJurisdictionalStatus = group.CurrentJurisdictionalStatus,
					CopyToWeb = true,
					BillingInfo = new BillingInfo() { ACTStatus = LetterBookACTStatus.Pending, ContractNumber = group.JurisdictionalDatas.FirstOrDefault().ContractNumber, LaboratoryId = letterWriter.LocationId, Lab = letterWriter.Location.Location + " - " + letterWriter.Location.Name }
				});
			}

			return supplementLists;
		}

		private List<SupplementLetterDisplay> BuildJurisdictionGroupModelsForSupplementLettersDisplay(List<JurisdictionalData> jurisdictionalDatas, List<int> letterIds = null)
		{
			var jurisdictionalDataIds = jurisdictionalDatas.Select(x => x.Id).ToList();
			var supplements = (from sl in _dbSubmission.SubmissionLetters
							   where jurisdictionalDataIds.Contains((int)sl.JurisdictionalDataId)
							   select new
							   {
								   SupplementLetter = sl
							   }).ToList();

			if (letterIds != null)
			{
				if (letterIds.Count > 0)
				{
					supplements = supplements.Where(x => letterIds.Contains((int)x.SupplementLetter.LetterId)).ToList();
				}
			}

			var jurisdictionalDatasSup = (from j in jurisdictionalDatas
										  join sl in supplements on j.Id equals sl.SupplementLetter.JurisdictionalDataId
										  select new
										  {
											  jurisdictionaldata = j,
											  SupplementLetter = sl.SupplementLetter
										  }).ToList();

			var jurisdictionalDataGroups = jurisdictionalDatasSup
				.GroupBy(jd => new { jd.jurisdictionaldata.JurisdictionId, jd.jurisdictionaldata.Status, jd.SupplementLetter.Letter.Id })
				.Select(group => new SupplementLetterDisplay()
				{
					JurisdictionalDataIds = group.ToList().Select(data => new JurisdictionalDataSubmissionIdPair
					{
						JurisdictionalDataId = data.jurisdictionaldata.Id,
						SubmissionId = (int)data.jurisdictionaldata.SubmissionId,
						IdNumber = data.jurisdictionaldata.Submission.IdNumber,
						GameName = data.jurisdictionaldata.Submission.GameName,
						DateCode = data.jurisdictionaldata.Submission.DateCode,
						Version = data.jurisdictionaldata.Submission.Version,
						FileNumber = data.jurisdictionaldata.Submission.FileNumber,
						IsAttachedToLetter = true
					}).ToList(),
					DocumentType = group.ToList().Select(x => x.SupplementLetter.Letter.LetterType).FirstOrDefault(),
					Language = group.ToList().Select(x => x.SupplementLetter.Letter.Language).FirstOrDefault(),
					LetterId = group.ToList().Select(x => x.SupplementLetter.Letter.Id).FirstOrDefault(),
					LetterDate = group.ToList().Select(x => x.SupplementLetter.Letter.LetterDate).FirstOrDefault(),
					CreateDate = group.ToList().Select(x => x.SupplementLetter.AddDate).FirstOrDefault(),
					JurisdictionName = group.ToList().Select(x => x.jurisdictionaldata.JurisdictionName).FirstOrDefault(),
					JurisdictionId = group.ToList().Select(x => x.jurisdictionaldata.JurisdictionId).FirstOrDefault(),
					PdfPath = group.ToList().Select(x => x.SupplementLetter.Letter.PdfPath).FirstOrDefault(),
					JurisdictionStatus = group.ToList().Select(x => x.jurisdictionaldata.Status).FirstOrDefault(),
				}).ToList();

			jurisdictionalDataGroups.ForEach(x => x.DocumentType = GetSupplementDocumentTypeDesc(x.DocumentType));
			jurisdictionalDataGroups.ForEach(x => x.PdfName = Path.GetFileName(x.PdfPath));

			return jurisdictionalDataGroups.OrderBy(jdg => jdg.LetterDate).OrderBy(jdg => jdg.JurisdictionName).ThenBy(jdg => jdg.JurisdictionStatus).ToList();
		}
		#endregion

		#region Process Letters

		[HttpPost]
		public ContentResult ProcessLetter(CreateLetterModel letterToApprove)
		{
			try
			{
				var bundle = GetValidQABundle(letterToApprove.BundleId);
				letterToApprove.JurisdictionalDataIds = letterToApprove.LetterApprovalData.JurisdictionalDataIds
					.Where(pair => pair.IsAttachedToLetter).Select(pair => pair.JurisdictionalDataId).ToList();

				//Check if Previously posted Translations needs to be removed
				var submissionLetterIdsToBeRemoved = CheckIfAdditionalLettersNeedsRemoved(letterToApprove);

				var pdfs = _iPdfService.SavePdfs(letterToApprove);
				if (pdfs.MainLetterResult.GPSFileId == 0)
				{
					throw new NullReferenceException("Failed to Upload to GPS.");
				}
				//Upload Add'l letters if GPS uploaded
				var uploadedpdfs = pdfs.TranslationResults.Where(x => x.GPSFileId != 0).ToList();
				pdfs.TranslationResults = uploadedpdfs;

				//billing info
				var billingData = letterToApprove.BillingInfo;

				var billableJurisdictionList = letterToApprove.LetterApprovalData.JurisdictionalDataIds
												.Where(pair => pair.IsAttachedToLetter)
												.Select(idObj => new KeyValuePair<int, bool>(idObj.JurisdictionalDataId, letterToApprove.LetterApprovalData.IsBillable))
												.ToList();

				var billingInfo = new LetterBillingInfo(billingData.BallyType, billingData.ItemsBeingBilledCount, DateTime.Now, billingData.BillingSheet, billingData.LaboratoryId, billingData.ReportType, billableJurisdictionList);
				billingInfo.ACTStatus = billingData.ACTStatus;
				billingInfo.IsBillable = letterToApprove.LetterApprovalData.IsBillable;
				billingInfo.Note = billingData.Note;

				//Check if this letter is not for all jurisdictions that were included in previous draft //PT-1488
				var neededSplit = _letterDataUpdateService.CheckIfDraftNeededSplit(letterToApprove);
				if (neededSplit)
				{
					//Treat as new document
					letterToApprove.DocumentId = 0;
					letterToApprove.DocumentVersion = 0;
				}

				//below adds in new sets of document Tables which keeps a version
				_documentUpdateService.AddDocument(letterToApprove, billingInfo, bundle.ProjectId, pdfs.MainLetterResult.pdfPath, pdfs.MainLetterResult.GPSFilePath, pdfs.MainLetterResult.GPSFileId, true, pdfs.TranslationResults);

				//Below adds in all legacy places
				_letterDataUpdateService.UpdateJurisdictionalDataAfterLetterApproval(bundle, letterToApprove, pdfs.MainLetterResult, pdfs.TranslationResults);

				//Remove Additional Letters
				foreach (var id in submissionLetterIdsToBeRemoved)
				{
					DeleteSupplement(id, letterToApprove.LetterApprovalData.JurisdictionalDataIds);
				}

				//set model to send data back
				var postedLetter = BuildApprovedLetterModel(letterToApprove.JurisdictionalDataIds, letterToApprove, pdfs);

				var retResult = new LetterApprovalResult() { Error = false, Message = "Letter Approved.", PostedLetter = postedLetter, UploadedToGPS = (pdfs.TranslationResults.Count == letterToApprove.Translations.Count) };
				return Content(ComUtil.JsonEncodeCamelCase(retResult), "application/json");
			}
			catch (Exception ex)
			{
				_logger.LogError(ex);
				var retResult = new LetterApprovalResult() { Error = true, Message = ex.Message };
				return Content(ComUtil.JsonEncodeCamelCase(retResult), "application/json");
			}
		}

		[HttpPost]
		public ActionResult PostSupplementalLetter(CreateLetterModel letterModel)
		{
			try
			{
				var bundle = _dbSubmission.QABundles.Include(b => b.Project.JurisdictionalData).SingleOrDefault(b => b.Id == letterModel.BundleId);
				if (bundle == null)
				{
					throw new NullReferenceException("Bundle does not exist.");
				}

				letterModel.JurisdictionalDataIds = letterModel.LetterApprovalData.JurisdictionalDataIds
				.Where(pair => pair.IsAttachedToLetter).Select(pair => pair.JurisdictionalDataId).ToList();

				var pdfs = _iPdfService.SavePdfs(letterModel);
				if (pdfs.MainLetterResult.GPSFileId == 0)
				{
					throw new NullReferenceException("Failed to Upload to GPS.");
				}
				//Upload Add'l letters if GPS uploaded
				var uploadedpdfs = pdfs.TranslationResults.Where(x => x.GPSFileId != 0).ToList();
				pdfs.TranslationResults = uploadedpdfs;

				var billingData = letterModel.BillingInfo;
				var billableJurisdictionList = letterModel.LetterApprovalData.JurisdictionalDataIds
												.Where(pair => pair.IsAttachedToLetter)
												.Select(idObj => new KeyValuePair<int, bool>(idObj.JurisdictionalDataId, letterModel.LetterApprovalData.IsBillable))
												.ToList();
				var billingInfo = new LetterBillingInfo(billingData.BallyType, billingData.ItemsBeingBilledCount, DateTime.Now, billingData.BillingSheet, billingData.LaboratoryId, billingData.ReportType, billableJurisdictionList);
				billingInfo.ACTStatus = billingData.ACTStatus;
				billingInfo.IsBillable = letterModel.LetterApprovalData.IsBillable;
				billingInfo.Note = billingData.Note;

				//below adds in new sets of document Tables which keeps a version
				_documentUpdateService.AddDocument(letterModel, billingInfo, bundle.ProjectId, pdfs.MainLetterResult.pdfPath, pdfs.MainLetterResult.GPSFilePath, pdfs.MainLetterResult.GPSFileId, true, pdfs.TranslationResults);
				//adds all legacy tables
				var affectedLetterIds = _supplementalsService.ApproveSupplementalLetter(bundle.ProjectId, billingInfo, letterModel, pdfs);

				if (!pdfs.MainLetterResult.Error)
				{
					if (string.IsNullOrWhiteSpace(pdfs.MainLetterResult.Message))
					{
						pdfs.MainLetterResult.Message = "Letter has been posted successfully";
					}
					var emailModel = new SupplementalEmail
					{
						Body = ConstructSupplementalEmailBody(letterModel, billingInfo, bundle),
						BundleName = bundle.ProjectName,
						Recipients = new List<string>() { bundle.QAReviewer.Email, _user.Email } //letterWriter.Email,
					};
					SendSupplementEmail(emailModel);
				}

				//for affected letterIds, send data back
				var afectedJurisdictionalData = _dbSubmission.JurisdictionalData.Where(x => letterModel.JurisdictionalDataIds.Contains(x.Id)).ToList();
				var updatedStatus = afectedJurisdictionalData.First().Status; //For Revocation and Decline
				letterModel.CurrentJurisdictionalStatus = updatedStatus;

				var retResult = new LetterApprovalResult() { PostedLetter = letterModel, Error = false, Message = "Letter Approved.", UploadedToGPS = (pdfs.TranslationResults.Count == letterModel.Translations.Count) };
				return Content(ComUtil.JsonEncodeCamelCase(retResult), "application/json");
			}
			catch (Exception ex)
			{
				//var pdfGenerateError = new DocumentCreateException("Error in SaveSupplementalPDF method", ex);
				//_logger.LogError(pdfGenerateError);
				//return Json("Error", JsonRequestBehavior.AllowGet);
				_logger.LogError(ex);
				var retResult = new LetterApprovalResult() { Error = true, Message = ex.Message };
				return Content(ComUtil.JsonEncodeCamelCase(retResult), "application/json");
				//return Json(JsonConvert.SerializeObject(ex), JsonRequestBehavior.AllowGet);
			}
		}

		[HttpPost]
		public ActionResult PostTranslationLetters(CreateLetterModel letterModel, int documentVersionId)//not sure of we need BillingInfo billingData
		{
			try
			{
				letterModel.JurisdictionalDataIds = letterModel.LetterApprovalData.JurisdictionalDataIds
				.Where(pair => pair.IsAttachedToLetter).Select(pair => pair.JurisdictionalDataId).ToList();

				//generate all translation pdfs
				var uploadedPdfs = _iPdfService.SaveTranslationPdfs(letterModel);

				//Process translations for pdfs that were uploaded to GPS.
				var pdfs = uploadedPdfs.Where(x => x.GPSFileId != 0).ToList();

				if (pdfs.Count > 0)
				{
					_documentUpdateService.AddTranslations(letterModel.Translations, pdfs, documentVersionId);

					//add in submissionLetter
					var jurisdictionalDatasToUpdate = _dbSubmission.JurisdictionalData.Where(x => letterModel.JurisdictionalDataIds.Contains(x.Id)).ToList();
					foreach (var translation in pdfs)
					{
						_supplementalsService.AddSupplementalLetter(jurisdictionalDatasToUpdate, translation.pdfPath, translation.GPSFilePath, translation.Language, translation.GPSFileId, letterModel.DateOfReport, letterModel.DocumentTypeId);
					}

					//return data back for counts
					var translations = _dbSubmission.tbl_DocumentVersionTranslations.Where(x => x.DocumentVersionId == documentVersionId).Select(x => new { LanguageId = x.LanguageId, PdfPath = x.PdfPath, Id = x.Id }).ToList();
					var translationCount = translations.Count();
					var selectedJurs = letterModel.JurisdictionalDataIds;
					var webTranslations = _dbSubmission.SubmissionLetters.Where(x => selectedJurs.Contains((int)x.JurisdictionalDataId)).Select(x => x.Letter.PdfPath).ToList().Distinct();

					var TranslationsDisplay = translations.Select(x => new MainPdfsDisplay()
					{
						Language = ((LetterLanguage)x.LanguageId).ToString(),
						PdfPath = x.PdfPath,
						PdfName = (x.PdfPath != null) ? Path.GetFileName(x.PdfPath) : null,
						ExistsInDB = (webTranslations.Any(w => w == x.PdfPath)),
						Id = x.Id
					}).ToList();

					return Content(ComUtil.JsonEncodeCamelCase(new { TranslationCount = translationCount, TranslationsDisplay = TranslationsDisplay, UploadedToGPS = (uploadedPdfs.Count == pdfs.Count) }), "application/json");
				}
				else
				{
					throw new NullReferenceException("Failed to Upload to GPS.");
				}
			}
			catch (Exception ex)
			{
				var pdfGenerateError = new DocumentCreateException("Error in SaveTranslation method", ex);
				_logger.LogError(pdfGenerateError);
				var retResult = new LetterApprovalResult() { Error = true, Message = ex.Message };
				return Content(ComUtil.JsonEncodeCamelCase(retResult), "application/json");
				//return Json(JsonConvert.SerializeObject(ex), JsonRequestBehavior.AllowGet);
			}
		}

		#endregion

		#region Different Letter Reviews
		[HttpPost]
		public JsonResult ProcessCompliance(CreateLetterModel complianceLetter, ComplianceReviewData complianceData)
		{
			try
			{
				var bundle = GetValidQABundle(complianceLetter.BundleId);
				var currentUser = _user.Username;
				var userId = _user.Id;

				return Json(_letterDataUpdateService.ProcessComplianceLetter(complianceLetter, bundle, currentUser, userId, complianceData), JsonRequestBehavior.AllowGet);
			}
			catch (Exception ex)
			{
				var cpError = new DocumentCreateException("Error in CP review", ex);
				_logger.LogError(cpError);
				return Json("Error", JsonRequestBehavior.AllowGet);
			}
		}

		[HttpPost]
		public ContentResult SendForENReview(List<CreateLetterModel> letters, ENReviewData reviewData, string bundleName, DateTime? targetDate)
		{
			try
			{
				var selDocuments = letters.Where(x => x.IsSelectedToProcess).ToList();
				var docIds = selDocuments.Select(x => x.MainLetter.FileId).ToList();
				var gpsLetters = _gpsContext.GPSItem.Where(gi => docIds.Contains(gi.Id)).ToList();

				var documents = selDocuments
							.Select(x =>
							{
								var doc = new EngineeringReviewDocument
								{
									Components = x.LetterApprovalData.JurisdictionalDataIds.Select(j => j.FileNumber + " - " + j.IdNumber).Distinct().ToList(),
									Jurisdictions = new List<string> { x.LetterApprovalData.JurisdictionName }//probably doesn't need to be a list
								};
								var gpsItem = gpsLetters.SingleOrDefault(gi => gi.Id == x.MainLetter.FileId);
								if (gpsItem != null)
								{
									doc.FilePath = gpsItem.FullName;
								}
								return doc;
							}).ToList();

				var recipients = reviewData.RecipientUserIds;
				var recipientEmails = _dbSubmission.trLogins.Where(x => recipients.Contains(x.Id.ToString())).Select(x => x.Email).ToList();
				var emailModel = new EngineeringReviewEmail
				{
					AdditionalComments = reviewData.Comments,
					BundleName = bundleName,
					Recipients = recipientEmails,
					Documents = documents,
					ReviewerEmail = _user.Email,
					TargetDate = (DateTime)targetDate,
					Attachments = gpsLetters.Select(gi => gi.FullName).ToList()
				};

				var mailMsg = new MailMessage
				{
					From = new MailAddress(emailModel.ReviewerEmail)
				};

				foreach (var recipient in emailModel.Recipients)
				{
					mailMsg.To.Add(new MailAddress(recipient));
				}

				mailMsg.Subject = "Engineer Review Required: " + emailModel.BundleName;
				mailMsg.Body = _emailService.RenderTemplate(Submissions.Common.Email.EmailType.EngineeringReview.ToString(), emailModel);
				emailModel.Attachments.RemoveAll(x => x == null);
				foreach (var item in emailModel.Attachments)
				{
					_logger.LogError(new ArgumentException("Letterbook Testing - ENG Review email attachment: " + item));
					var attachment = new Attachment(item);
					mailMsg.Attachments.Add(attachment);
				}

				_emailService.Send(mailMsg);

				return Content("OK");
			}
			catch (Exception ex)
			{
				var enError = new DocumentCreateException("Error in EN review", ex);
				_logger.LogError(enError);
				return Content("Error");
			}
		}

		[HttpPost]
		public JsonResult SendForLatinAmericaReview(List<CreateLetterModel> letters, int bundleId, int userId)
		{
			try
			{
				var bundle = _dbSubmission.QABundles.Where(x => x.Id == bundleId).Include("Tasks").SingleOrDefault();

				var selectedLetters = letters.Where(x => x.IsSelectedToProcess).ToList();
				var selectedJurs = (selectedLetters.Select(x => x.JurisdictionId).ToList()).Select(int.Parse).ToList();

				var bundleTaskId = bundle.Tasks.First().TaskDefID;
				var letterTaskId = BundleTask.Translation;
				if (bundleTaskId == (int)BundleTask.Addendum || bundleTaskId == (int)BundleTask.Correction)
				{
					letterTaskId = BundleTask.TranslationCOR;
				}

				var tfSplit = false;
				var projectSuffix = (bundle.Project.Suffix == null) ? 0 : bundle.Project.Suffix;
				if (bundle.Project.IsTransfer == false && letters.Count != selectedLetters.Count) //new submission && not all are selected
				{
					tfSplit = true;
				}
				else if (!selectedJurs.Contains((int)projectSuffix) && bundle.Project.IsTransfer == true && letters.Count != selectedLetters.Count)// transfer and not primary jur selected and not all are selected for Latin America review Then split bundle
				{
					tfSplit = true;
				}

				if (tfSplit)
				{
					var selJurisdictionalDataIds = new List<int>();
					foreach (var letter in selectedLetters)
					{
						selJurisdictionalDataIds.AddRange(letter.LetterApprovalData.JurisdictionalDataIds.Select(x => x.JurisdictionalDataId).ToList());
					}
					var newProjectId = _projectService.SplitJurisdictionsIntoProjects(bundleId, selJurisdictionalDataIds, true);
					bundle = _dbSubmission.QABundles.Where(x => x.ProjectId == newProjectId).Single();
				}

				//Latin America tasks are automatically assigned to Ana Soares (1287)
				var LatinAmericaTask = new QABundles_Task()
				{
					BundleID = bundle.Id,
					TaskDefID = (int)letterTaskId,
					UserId = userId,
					CreatedTime = DateTime.Now,
					AddUserId = _user.Id,
					tfCurrent = true
				};

				_projectService.AddBundleTask(LatinAmericaTask, BundleStatus.DL.ToString());

				if (tfSplit)
				{
					return Json(new { Result = "OK", NewBundleName = bundle.ProjectName }, JsonRequestBehavior.AllowGet);
				}
				else
				{
					return Json(new { Result = "OK", NewBundleName = "" }, JsonRequestBehavior.AllowGet);
				}
			}
			catch (Exception ex)
			{
				var latAmericaError = new DocumentCreateException("Error in Latin America Review", ex);
				_logger.LogError(latAmericaError);
				return Json("Error", JsonRequestBehavior.AllowGet);
			}
		}

		public JsonResult SendForSecondaryReview(List<CreateLetterModel> letters, int bundleId)
		{
			try
			{
				var bundle = _dbSubmission.QABundles.Where(x => x.Id == bundleId).Include("Tasks").SingleOrDefault();
				var letterWriter = GetBundleLetterWriterOrReviewerUser(bundle.Tasks.ToList());

				var selectedLetters = letters.Where(x => x.IsSelectedToProcess).ToList();
				var selectedJurs = (selectedLetters.Select(x => x.JurisdictionId).ToList()).Select(int.Parse).ToList();

				var tfSplit = false;

				if (bundle.Project.IsTransfer == false && letters.Count != selectedLetters.Count) //new submission && not all are selected
				{
					tfSplit = true;
				}
				else if (bundle.Project.IsTransfer == true && letters.Count != selectedLetters.Count)// transfer and not primary jur selected and not all are selected for Latin America review Then split bundle
				{
					if (!selectedJurs.Contains((int)bundle.Project.Suffix))
					{
						tfSplit = true;
					}
				}

				if (tfSplit)
				{
					var selJurisdictionalDataIds = new List<int>();
					foreach (var letter in selectedLetters)
					{
						selJurisdictionalDataIds.AddRange(letter.LetterApprovalData.JurisdictionalDataIds.Select(x => x.JurisdictionalDataId).ToList());
					}
					var newProjectId = _projectService.SplitJurisdictionsIntoProjects(bundleId, selJurisdictionalDataIds, true);
					bundle = _dbSubmission.QABundles.Where(x => x.ProjectId == newProjectId).Single();
				}

				var secondaryReviewTask = new QABundles_Task()
				{
					BundleID = bundle.Id,
					TaskDefID = (int)BundleTask.ChangesRequiresSecondaryReview,
					UserId = letterWriter.Id,
					CreatedTime = DateTime.Now,
					AddUserId = _user.Id,
					tfCurrent = true
				};

				_projectService.AddBundleTask(secondaryReviewTask, BundleStatus.DL.ToString());

				if (tfSplit)
				{
					return Json(new { Result = "OK", NewBundleName = bundle.ProjectName }, JsonRequestBehavior.AllowGet);
				}
				else
				{
					return Json(new { Result = "OK", NewBundleName = "" }, JsonRequestBehavior.AllowGet);
				}
			}
			catch (Exception ex)
			{
				var latAmericaError = new DocumentCreateException("Error in Secondary Review", ex);
				_logger.LogError(latAmericaError);
				return Json("Error", JsonRequestBehavior.AllowGet);
			}
		}

		#endregion

		#region Preview

		[HttpPost]
		public ActionResult PreviewTranslationPdf(string translationModelString, string letterModelString)
		{
			var translation = JsonConvert.DeserializeObject<TranslationLetter>(translationModelString);
			var letterModel = JsonConvert.DeserializeObject<CreateLetterModel>(letterModelString);
			var translationLetterModel = new CreateLetterModel()
			{
				MainLetter = translation.MainLetter,
				CorrectionCoverPage = translation.CorrectionCoverPageLetter,
				GenericCoverPage = letterModel.GenericCoverPage,
				BackPage = letterModel.BackPage,
				JurisdictionId = letterModel.JurisdictionId,
				BundleId = letterModel.BundleId,
				JurisdictionalDataIds = letterModel.JurisdictionalDataIds,
				IsDraftLetter = letterModel.IsDraftLetter,
				CopyToWeb = letterModel.CopyToWeb,
				ManufacturerCode = letterModel.ManufacturerCode,
				FileNumber = letterModel.FileNumber,
				SubmissionType = letterModel.SubmissionType,
				DateOfReport = letterModel.DateOfReport
			};
			return Preview(translationLetterModel);
		}

		[HttpPost]
		public ActionResult PreviewPdf(string letterModelString)
		{
			var letterModel = JsonConvert.DeserializeObject<CreateLetterModel>(letterModelString);
			return Preview(letterModel);
		}

		private FileStreamResult Preview(CreateLetterModel letterModel)
		{
			var reportDates = new DateGenerationModel(true, letterModel.MainLetter.Language)
			{
				GenerationType = letterModel.IsDraftLetter ? DateGenerationType.Manual : DateGenerationType.Autogenerate,
				DateOfReport = new LetterBookmark { Name = LetterBookmarkName.DateOfReport, Value = DateGenerationModel.GetFormattedDate(letterModel.DateOfReport, letterModel.MainLetter.Language) }
			};
			var file = _iPdfService.GeneratePdf(letterModel, reportDates, true);
			var fileName = letterModel.MainLetter.FileName.Replace(".docx", ".pdf").Replace(".doc", ".pdf").Replace(".html", ".pdf").Replace(".htm", ".pdf");
			return File(file, "application/pdf", fileName);
		}

		[HttpPost]
		public ActionResult PreviewSupplementPdf(string letterModelString)
		{
			var letterModel = JsonConvert.DeserializeObject<CreateLetterModel>(letterModelString);
			return Preview(letterModel);
		}
		#endregion

		#region View PDf
		public ActionResult ViewPdf(string filePath)
		{
			var fileName = Path.GetFileName(filePath);
			var contentType = MimeMapping.GetMimeMapping(fileName);
			return File(filePath, contentType, fileName);
		}
		#endregion

		#region Ajax calls

		[HttpGet]
		public ActionResult ResetLetterbookTestCase(int bundleId)
		{
			if (_config.Environment != Common.Environment.Production)
			{
				var bundle = _dbSubmission.QABundles.SingleOrDefault(x => x.Id == bundleId);
				if (bundle == null)
				{
					throw new KeyNotFoundException("No Bundle found for ID: " + bundleId);
				}

				var projectId = bundle.ProjectId;
				//Total jurs with merge projects
				var jurisdictionalDatas = GetAllAvailableJurisdictions(bundleId);  //_dbSubmission.trProjectJurisdictionalData.Where(x => x.ProjectId == bundle.ProjectId).Select(x => x.JurisdictionalData).ToList();

				var allProjects = new List<int>();
				allProjects.Add(bundle.ProjectId);
				var mergedProjects = GetAllMergedProjects(bundle.ProjectId);
				allProjects.AddRange(mergedProjects);

				foreach (var project in allProjects)
				{
					_projectService.OpenClosedProject(project);
					var bundleTypeId = _dbSubmission.trProjects.Where(x => x.Id == project).Select(x => x.BundleTypeId).SingleOrDefault();
					if (bundleTypeId == (int)BundleType.OriginalTesting)
					{
						var projectJurisdictions = jurisdictionalDatas.Where(x => x.JurisdictionalDataProjects.Any(p => p.ProjectId == project)).ToList();
						foreach (var jd in projectJurisdictions)
						{
							jd.Status = JurisdictionStatus.RA.ToString();
							jd.PdfPath = null;
							jd.CloseDate = null;
							jd.UpdateDate = null;
							jd.DraftDate = null;

							_jurisdictionalDataService.Update(jd);
						}
					}
				}

				var jurDocumentLinks = jurisdictionalDatas.SelectMany(x => x.DocumentVersionLinks).ToList();
				var documentLinks = jurDocumentLinks.Where(x => x.ProjectId == projectId).ToList();
				var documentVersions = documentLinks.Select(x => x.DocumentVersion).ToList();
				var documentVersionIds = documentVersions.Select(x => x.Id).ToList();
				var documents = documentVersions.Select(x => x.Document).ToList();
				var letterbooks = _dbSubmission.tbl_LetterBook.Where(x => documentVersionIds.Contains(x.DocumentVersionId)).ToList();

				_dbSubmission.DocumentVersionLinks.RemoveRange(documentLinks);
				_dbSubmission.DocumentVersions.RemoveRange(documentVersions);
				_dbSubmission.Documents.RemoveRange(documents);
				_dbSubmission.tbl_LetterBook.RemoveRange(letterbooks);
				_dbSubmission.SaveChanges();
				return Content("OK");
			}
			else
			{
				throw new NotImplementedException("The ResetLetterbookTestCase method is not supported in Production");
			}
		}

		[HttpGet]
		public JsonResult GetBillingSheetsNeedBilledCount()
		{
			var val = _dbSubmission.tbl_lst_BillingSheets.Where(x => (x.BilledItem1AppFeeId != 0 && x.BilledItem1AppFeeId != null) || (x.BilledItem2AppFeeId != 0 && x.BilledItem2AppFeeId != null) || (x.BilledItem3AppFeeId != 0 && x.BilledItem3AppFeeId != null)).Select(x => x.Id).ToList();
			return Json(val, JsonRequestBehavior.AllowGet);
		}

		[HttpGet]
		public JsonResult GetComponentName(int submissionId)
		{
			var val = _dbSubmission.Submissions.Where(sub => sub.Id == submissionId)
				.Select(sub => new { IdNumber = sub.IdNumber, Version = sub.Version, FileNumber = sub.FileNumber, GameName = sub.GameName }).SingleOrDefault();
			return Json(val, JsonRequestBehavior.AllowGet);
		}

		public JsonResult GetDefaults(string jurisdictionId, string manufacturer, string subType)
		{
			var defaultRules = _dbSubmission.LetterbookDefaultingOptions.Where(x =>
			   (x.Manufacturers.Select(y => y.ManufacturerCode).Any(m => m == manufacturer) || (!x.Manufacturers.Any())) &&
			   (x.Jurisdictions.Select(y => y.JurisdictionId).Any(j => j == jurisdictionId) || (!x.Jurisdictions.Any())) &&
			   (x.SubTypes.Select(y => y.SubmissionType.Code).Any(s => s == subType) || (!x.SubTypes.Any()))
			   ).Select(y => new
			   {
				   JurisdictionIds = y.Jurisdictions.Select(j => j.JurisdictionId).ToList(),
				   Manufactures = y.Manufacturers.Select(m => m.ManufacturerCode).ToList(),
				   SubTypes = y.SubTypes.Select(s => s.SubmissionType.Code).ToList(),
				   DocumentTypeIds = y.DocumentTypes.Select(d => d.DocumentTypeId).ToList(),
				   ContractTypeIds = y.ContractTypes.Select(d => d.ContractTypeId).ToList(),
				   BillingPartyIds = y.BillingParties.Select(d => d.BillingPartyId).ToList(),
				   BillingParties = y.BillingParties.Select(d => d.BillingParty.BillingParty).ToList(),
				   LocationIds = y.Locations.Select(d => d.LocationId).ToList(),
				   CompanyIds = y.Companies.Select(c => c.CompanyId).ToList(),
				   y.ApprovalStatus,
				   y.LetterheadId,
				   LetterheadName = (y.LetterheadId != null) ? y.Letterhead.Description : "",
				   y.CorrectionLetterheadId,
				   CorrectionLetterheadName = (y.CorrectionLetterheadId != null) ? y.CorrectionLetterhead.Description : "",
				   y.RepeatLetterhead,
				   y.RepeatCorrectionLetterhead,
				   y.Billable,
				   y.BillingSheetId,
				   BillingSheet = y.DefaultBillingSheet.Name,
				   AvailableBillingSheets = y.AvailableBillingSheets.Select(x => x.BillingSheet.Name).ToList(),
				   y.BillingLabId,
				   BillingLab = (y.BillingLabId != null) ? y.BillingLab.Location + " - " + y.BillingLab.Name : "",
				   y.BillingNote,
				   CriteriaCount = ((y.Jurisdictions.Count > 0) ? 1 : 0) + ((y.Manufacturers.Count > 0) ? 1 : 0) + ((y.SubTypes.Count > 0) ? 1 : 0) + ((y.ContractTypes.Count > 0) ? 1 : 0) + ((y.BillingParties.Count > 0) ? 1 : 0)
			   }).ToList();

			return Json(defaultRules, JsonRequestBehavior.AllowGet);
		}

		public JsonResult GetQANotesComment(string fileNumber, string jurisdictionId)
		{
			var jurId = Convert.ToInt32(jurisdictionId);
			var note = _letterbookService.GetQANotesComments(jurId, fileNumber);
			return Json(note, JsonRequestBehavior.AllowGet);
		}

		[HttpGet]
		public ContentResult GetLetterModels(int bundleId)
		{
			var letterModels = SetLetterModels(bundleId);
			return Content(ComUtil.JsonEncodeCamelCase(letterModels), "application/json");
		}

		[HttpGet]
		public ContentResult GetSupplementLetterModels(int bundleId)
		{
			var bundleJurisdictions = GetAllAvailableJurisdictions(bundleId);
			var allRelevantGpsDocuments = _documentService.GetLettersAllTypes(bundleJurisdictions.Select(jd => jd.Id).ToList());
			var supplementVM = BuildSupplementJurisdictionGroupAndLetterModels(bundleJurisdictions, bundleId, allRelevantGpsDocuments);
			return Content(ComUtil.JsonEncodeCamelCase(supplementVM), "application/json");
		}

		[HttpPost]
		public ContentResult GetAllSupplements(List<int> jurisdictionalDataIds)
		{
			var bundleJurisdictions = _dbSubmission.JurisdictionalData.Where(x => jurisdictionalDataIds.Contains(x.Id)).ToList();

			var allSupplements = BuildJurisdictionGroupModelsForSupplementLettersDisplay(bundleJurisdictions);
			return Content(ComUtil.JsonEncodeCamelCase(allSupplements), "application/json");
			//return Json(allSupplements, JsonRequestBehavior.AllowGet);
		}

		[HttpPost]
		public JsonResult DeleteSupplement(int letterId, List<JurisdictionalDataSubmissionIdPair> jurisdictionDataIds)
		{
			try
			{
				var ids = jurisdictionDataIds.Select(x => x.JurisdictionalDataId).ToList();
				_supplementalsService.DeleteSupplements(letterId, ids);
				return Json("OK", JsonRequestBehavior.AllowGet);
			}
			catch
			{
				return Json("Error", JsonRequestBehavior.AllowGet);
			}
		}

		[HttpPost]
		public JsonResult GetProjectJurisdictionIndicator(int projectId, List<int> jurisdictionalDataIds)
		{
			var indicator = _projectService.GetProjectJurisdictionIndicator(projectId, jurisdictionalDataIds);
			return Json(indicator, JsonRequestBehavior.AllowGet);
		}

		#endregion

		#region Draft Info Edit
		[HttpGet]
		public ContentResult GetDraftOptionsInfo()
		{
			var vm = new DraftOptionsInfo();
			return Content(ComUtil.JsonEncodeCamelCase(vm), "application/json");
			//return Json(allSupplements, JsonRequestBehavior.AllowGet);
		}

		[HttpPost]
		public ContentResult SaveDraftInfo(DraftLetterModel vm)
		{
			try
			{
				var draftLetter = _dbSubmission.DraftLetters.Where(x => x.Id == vm.draftLetterId).SingleOrDefault();
				if (draftLetter != null)
				{
					draftLetter.Status = vm.Status;
					draftLetter.DraftDate = vm.DraftDate;
					draftLetter.UpdatedDateReason = (int?)vm.UpdatedDateReason;
					draftLetter.IsTimelineRequested = vm.IsTimelineRequested;
					draftLetter.TimelineType = vm.TimelineType;
					draftLetter.TimelineDate = vm.TimelineDate;
					draftLetter.LetterheadId = vm.LetterheadId;
					draftLetter.LetterheadPath = vm.LetterheadFilePath;
					draftLetter.LetterheadAddendumId = vm.AddendumLetterheadId;
					draftLetter.LetterheadPathAddendum = vm.AddendumLetterheadFilePath;
					draftLetter.RepeatOverlay = (vm.RepeatLetterhead == true) ? true : false;
					draftLetter.RepeatOverlayAddendum = (vm.RepeatAddLetterhead == true) ? true : false;
					draftLetter.AttachCorrectionAddendum = (vm.AttachCorrectionAddendum == true) ? true : false;
					_dbSubmission.UserId = _user.Id;
					_dbSubmission.SaveChanges();
				}
				return Content("OK");
			}
			catch (Exception ex)
			{
				return Content("Error" + ex.Message);
			}

		}

		#endregion

		#region Letter Versions & edit billing

		public ContentResult GetJurisdictionDocumentVersions(List<int> jurisdictionalDataIds)
		{
			//var jurisdictionalDataIds = _dbSubmission.DocumentVersionLinks.Where(x => x.DocumentVersionId == documentVersionId).Select(x => x.JurisdictionalDataId).ToList();

			//var documentsall = _dbSubmission.Documents
			//	.Where(x => x.DocumentVersions.Any(y => y.DocumentVersionLinks.Select(z => z.JurisdictionalDataId).ToList().Intersect(jurisdictionalDataIds).Any()))
			//	.ToList();

			var documents = _dbSubmission.Documents
				.Where(x => x.DocumentVersions.Any(y => y.DocumentVersionLinks.Any(z => jurisdictionalDataIds.Contains(z.JurisdictionalDataId))))
				.Select(x => new JurisdictionDocuments
				{
					DocumentId = x.Id,
					Language = x.Language.Language,
					LetterType = x.DocumentType.Description,
					JurisdictionDocumentVersions = x.DocumentVersions.Select(y => new JurisdictionDocumentVersions
					{
						CreateDate = y.AddDate,
						LetterDate = y.DocumentDate,
						DocumentVersionId = y.Id,
						PdfPath = y.FilePath,
						Version = y.Version,
						Jurisdictions = y.DocumentVersionLinks.Select(j => new JurisdictionalDataSubmissionIdPair
						{
							FileNumber = j.JurisdictionalData.Submission.FileNumber,
							IdNumber = j.JurisdictionalData.Submission.IdNumber,
							GameName = j.JurisdictionalData.Submission.GameName,
							JurisdictionalDataId = j.JurisdictionalDataId,
							DateCode = j.JurisdictionalData.Submission.DateCode,
							Version = j.JurisdictionalData.Submission.Version
						}).ToList(),
						IsBillable = y.DocumentVersionLinks.Select(d => d.Billable).FirstOrDefault(),
						ProjectName = y.DocumentVersionLinks.Select(d => d.Project.ProjectName).FirstOrDefault(),
						Translations = y.DocumentVersionTranslations.Select(t => new Translations
						{
							CreateDate = t.CreatedOnUTC,
							LetterDate = y.DocumentDate,
							PdfPath = t.PdfPath,
							LanguageId = t.LanguageId
						}).ToList()
					}).OrderByDescending(o => o.Version).ToList()
				}).OrderByDescending(x => x.DocumentId).ToList();

			var allSupplementTypes = _dbSubmission.tbl_lst_DocumentTypes.Where(x => x.IsSupplementType).Select(x => x.DocumentType).ToList();

			foreach (var doc in documents)
			{
				doc.DocumentCurrentVersion = doc.JurisdictionDocumentVersions.Max(x => x.Version);
				doc.JurisdictionDocumentVersions.ForEach(y =>
				{
					y.PdfName = Path.GetFileName(y.PdfPath);
					var affectedJurs = y.Jurisdictions.Select(j => j.JurisdictionalDataId).ToList();
					y.ExistsInDB = _dbSubmission.JurisdictionalData.Any(x => affectedJurs.Contains((int)x.Id) && (x.PdfPath == y.PdfPath || x.SubmissionDrafts.Any(s => s.DraftLetter.FilePath == y.PdfPath)));
					if (allSupplementTypes.Contains(doc.LetterType) && y.ExistsInDB == false)
					{
						y.ExistsInDB = _dbSubmission.SubmissionLetters.Any(x => affectedJurs.Contains((int)x.JurisdictionalDataId) && x.Letter.PdfPath.Trim() == y.PdfPath.Trim());
						y.AllowEditBilling = true;
					}
				});
				doc.JurisdictionDocumentVersions.ForEach(y => y.Translations.ForEach(t =>
				{
					t.PdfName = Path.GetFileName(t.PdfPath);
					t.Language = ((LetterLanguage)t.LanguageId).ToString();
					var affectedJurs = y.Jurisdictions.Select(j => j.JurisdictionalDataId).ToList();
					var allLetters = _dbSubmission.SubmissionLetters.Where(x => affectedJurs.Contains((int)x.JurisdictionalDataId)).Select(x => x.Letter).ToList();
					t.ExistsInDB = allLetters.Any(x => x.PdfPath.Trim() == t.PdfPath.Trim());
				}));

			}

			return Content(ComUtil.JsonEncodeCamelCase(documents), "application/json");
		}

		[HttpGet]
		public ContentResult GetBillingInfo(int documentVersionId)
		{
			var billingInfo = GetBillingInfoForDocVersion(documentVersionId);
			return Content(ComUtil.JsonEncodeCamelCase(billingInfo), "application/json");
		}

		private BillingInfo GetBillingInfoForDocVersion(int documentVersionId)
		{
			var billingInfo = new BillingInfo() { };
			var projectId = _dbSubmission.DocumentVersionLinks.Where(x => x.DocumentVersionId == documentVersionId).Select(x => x.ProjectId).FirstOrDefault();
			var letterBookList = _dbSubmission.tbl_LetterBook.Where(x => x.DocumentVersionId == documentVersionId);
			if (letterBookList.Count() > 0)
			{
				var letterBook = letterBookList.First();
				var billedCount = letterBook.DocumentVersion.DocumentVersionLinks
					.OrderByDescending(x => x.Id)
					.FirstOrDefault().BilledCount;

				billingInfo = new BillingInfo
				{
					Id = letterBook.Id,
					ACTStatus = !string.IsNullOrEmpty(letterBook.ACTStatus) ? (LetterBookACTStatus?)Enum.Parse(typeof(LetterBookACTStatus), letterBook.ACTStatus) : null,
					BillingDate = letterBook.BillingDate,
					BillingUser = letterBook.BillingUser != null ? (letterBook.BillingUser.FName + " " + letterBook.BillingUser.LName) : "",
					Lab = (letterBook.LaboratoryId != 0) ? letterBook.Laboratory.Location + " - " + letterBook.Laboratory.Name : "",
					LaboratoryId = letterBook.LaboratoryId,
					ReportType = letterBook.ReportType,
					SpecializedType = letterBook.SpecializedType,
					ItemsBeingBilledCount = (int)billedCount,
					//ContractNumber = _dbSubmission.trProjects.Find(projectId).JurisdictionalData.FirstOrDefault().JurisdictionalData.Submission.ContractNumber,
					Note = letterBook.Note
				};
				if (letterBook.BillingSheet == LetterBookBillingSheetOld.NonSpecialized.ToString())
				{
					billingInfo.BillingSheet = LetterBookBillingSheet.USNonSpecialized.ToString();
				}
				else if (letterBook.BillingSheet == LetterBookBillingSheetOld.Specialized.ToString())
				{
					billingInfo.BillingSheet = LetterBookBillingSheet.USSpecialized.ToString();
				}
				else if (letterBook.BillingSheet == LetterBookBillingSheetOld.CorrectionAddendum.ToString())
				{
					billingInfo.BillingSheet = LetterBookBillingSheet.USCorrectionAddendum.ToString();
				}
				else
				{
					billingInfo.BillingSheet = letterBook.BillingSheet;
				}
				billingInfo.BillingSheetId = _dbSubmission.tbl_lst_BillingSheets.Where(x => x.Name == letterBook.BillingSheet).Select(x => x.Id).SingleOrDefault();

				if (!string.IsNullOrWhiteSpace(letterBook.BallyType))
					billingInfo.BallyType = letterBook.BallyType;
			}

			return billingInfo;
		}

		[HttpPost]
		public ContentResult SaveBillingInfo(int documentVersionId, BillingInfo billingData, bool isBillable)
		{
			try
			{
				var isCurrentBillable = _dbSubmission.DocumentVersionLinks.Where(x => x.DocumentVersionId == documentVersionId).First();
				if (isCurrentBillable.Billable != isBillable || isCurrentBillable.BilledCount != billingData.ItemsBeingBilledCount)
				{
					_dbSubmission.DocumentVersionLinks.Where(x => x.DocumentVersionId == documentVersionId).Update(x => new tbl_DocumentVersionLink { Billable = isBillable, BilledCount = billingData.ItemsBeingBilledCount });
				}
				var docVersions = _dbSubmission.DocumentVersionLinks.Where(x => x.DocumentVersionId == documentVersionId).ToList();
				var billableJurisdictionList = docVersions.Select(idObj => new KeyValuePair<int, bool>(idObj.JurisdictionalDataId, idObj.Billable)).ToList();
				var billingInfo = new LetterBillingInfo(billingData.BallyType, billingData.ItemsBeingBilledCount, DateTime.Now, billingData.BillingSheet, billingData.LaboratoryId, billingData.ReportType, billableJurisdictionList);
				billingInfo.ACTStatus = (billingData.ACTStatus == null) ? LetterBookACTStatus.Pending : billingData.ACTStatus;
				billingInfo.IsBillable = isBillable;
				billingInfo.SpecializedType = billingData.SpecializedType;
				billingInfo.Note = billingData.Note;

				_letterbookService.AddLetterBookVue((int)documentVersionId, billingInfo, DateTime.Today);
				return Content("OK");
			}
			catch (Exception ex)
			{
				_logger.LogError(ex);
				return Content("NOOK");
			}
		}

		public ActionResult PullFromAccounting(int documentVersionId)
		{
			var documentVersionToPull = _dbSubmission.DocumentVersions.Find(documentVersionId);
			if (documentVersionToPull == null)
			{
				return Json(new { Error = true, Message = "No DocumentVersion was found for ID: " + documentVersionId }, JsonRequestBehavior.AllowGet);
			}

			var letterbookEntries = _dbSubmission.tbl_LetterBook.Where(lb => lb.DocumentVersionId == documentVersionId).ToList();
			if (!letterbookEntries.Any())
			{
				return Json(new { Error = true, Message = "No Letterbook entries were found for DocumentVersion ID: " + documentVersionId }, JsonRequestBehavior.AllowGet);
			}

			foreach (var letterbookEntry in letterbookEntries)
			{
				var billingStatus = letterbookEntry.ACTStatus;
				var qaStatus = letterbookEntry.QAStatus;
				if (billingStatus == LetterBookACTStatus.PulledBack.ToString())
				{
					return Json(new { Error = true, Message = "Cannot pull Letter back - This entry is already in Pulled Back status." }, JsonRequestBehavior.AllowGet);
				}
				if (billingStatus != LetterBookACTStatus.Pending.ToString())
				{
					return Json(new { Error = true, Message = "Cannot pull Letter back - Accounting has already acted on it. Please contact Accounting now." }, JsonRequestBehavior.AllowGet);
				}
				else if (letterbookEntry.BatchNumber != null)
				{
					return Json(new { Error = true, Message = "Cannot pull Letter back - Accounting has already added it to a batch. Please contact Accounting now." }, JsonRequestBehavior.AllowGet);

				}
			}

			foreach (var letterbookEntry in letterbookEntries)
			{
				letterbookEntry.ACTStatus = LetterBookACTStatus.PulledBack.ToString();
				letterbookEntry.QAStatus = LetterBookACTStatus.PulledBack.ToString();
			}
			_dbSubmission.SaveChanges();

			return Json(new { Error = false, Message = "Successfully set status of Letterbook entry to PulledBack." }, JsonRequestBehavior.AllowGet);
		}

		public ActionResult PushBackToAccounting(int documentVersionId)
		{
			var documentVersionToPush = _dbSubmission.DocumentVersions.Find(documentVersionId);
			if (documentVersionToPush == null)
			{
				return Json(new { Error = true, Message = "No DocumentVersion was found for ID: " + documentVersionId }, JsonRequestBehavior.AllowGet);
			}

			var letterbookEntries = _dbSubmission.tbl_LetterBook.Where(lb => lb.DocumentVersionId == documentVersionId).ToList();
			if (!letterbookEntries.Any())
			{
				return Json(new { Error = true, Message = "No Letterbook entries were found for DocumentVersion ID: " + documentVersionId }, JsonRequestBehavior.AllowGet);
			}

			foreach (var letterbookEntry in letterbookEntries)
			{
				letterbookEntry.ACTStatus = LetterBookACTStatus.Pending.ToString();
				letterbookEntry.QAStatus = LetterBookACTStatus.Pending.ToString();
			}
			_dbSubmission.SaveChanges();
			return Json(new { Error = false, Message = "Successfully set status of Letterbook back to Pending." }, JsonRequestBehavior.AllowGet);
		}
		#endregion

		#region Delete Letter

		public ActionResult GetDeleteConfirmData(List<int> jurisdictionDataIds)
		{
			var jurisdictionalDatas = _dbSubmission.JurisdictionalData.Where(x => jurisdictionDataIds.Contains(x.Id)).ToList()
									.Select(x => new
									{
										x.Id,
										x.JurisdictionId,
										x.JurisdictionName,
										x.Submission.FileNumber,
										x.Submission.IdNumber,
										x.Submission.GameName,
										x.Submission.Version,
										x.Status,
										x.CloseDate,
										x.UpdateDate,
										x.RevokeDate,
										x.ObsoleteDate,
										x.DraftDate,
										x.Active
									}).ToList();

			//Original Certification, Do by filenumber and jurisdictionId
			var confirmData = jurisdictionalDatas
							  .Select(x => new DeleteConfirmData
							  {
								  JurisdictionDataId = x.Id,
								  JurisdictionId = x.JurisdictionId,
								  JurisdictionName = x.JurisdictionName,
								  FileNumber = x.FileNumber,
								  IdNumber = x.IdNumber,
								  GameName = x.GameName,
								  Version = x.Version,
								  Status = x.Status
							  }).ToList();

			foreach (var jurisdiction in confirmData)
			{
				var dbData = jurisdictionalDatas.Where(x => x.Id == jurisdiction.JurisdictionDataId).Single();
				//Original Testing
				if (dbData.Status == JurisdictionStatus.AP.ToString() && dbData.CloseDate != null && dbData.UpdateDate == null)
				{
					var changes = new List<UpdateData>();
					changes.Add(new UpdateData() { DBFieldName = JurisdictionalDataField.JurisdictionType.ToString(), FieldName = JurisdictionalDataField.JurisdictionType.GetEnumDescription(), FromVal = dbData.Status, ToVal = JurisdictionStatus.RA.ToString() });
					changes.Add(new UpdateData() { DBFieldName = JurisdictionalDataField.JurisdictionDate.ToString(), FieldName = JurisdictionalDataField.JurisdictionDate.GetEnumDescription(), FromVal = dbData.CloseDate.ToString("d"), ToVal = null });
					changes.Add(new UpdateData() { DBFieldName = JurisdictionalDataField.Active.ToString(), FieldName = JurisdictionalDataField.Active.GetEnumDescription(), FromVal = dbData.Active, ToVal = JurisdictionActiveStatus.No.ToString() });

					if (dbData.DraftDate != null)
					{
						changes.Add(new UpdateData() { DBFieldName = JurisdictionalDataField.DraftLetterDate.ToString(), FieldName = JurisdictionalDataField.DraftLetterDate.GetEnumDescription(), FromVal = dbData.DraftDate.ToString("d"), ToVal = null });
					}
					if (dbData.RevokeDate != null)
					{
						changes.Add(new UpdateData() { DBFieldName = JurisdictionalDataField.RevokedDate.ToString(), FieldName = JurisdictionalDataField.RevokedDate.GetEnumDescription(), FromVal = dbData.RevokeDate.ToString("d"), ToVal = null });
					}
					if (dbData.ObsoleteDate != null)
					{
						changes.Add(new UpdateData() { DBFieldName = JurisdictionalDataField.ObsoletedDate.ToString(), FieldName = JurisdictionalDataField.ObsoletedDate.GetEnumDescription(), FromVal = dbData.ObsoleteDate.ToString("d"), ToVal = null });
					}
					jurisdiction.UpdateData = changes;
				}

				//Addendum & Correction
				//if (dbData.Status == JurisdictionStatus.AP.ToString() && dbData.CloseDate != null && dbData.UpdateDate != null)
				//{
				//	//TODO - LetterBook If they do want later on, we may have to get it from HistoryLog so we can update prev updatedate it existed, For now just nullify
				//	var changes = new List<UpdateData>();
				//	changes.Add(new UpdateData() { DBFieldName = JurisdictionalDataField.UpdateDate.ToString(), FieldName = JurisdictionalDataField.UpdateDate.GetEnumDescription(), FromVal = dbData.UpdateDate.ToString("d"), ToVal = null });
				//	jurisdiction.UpdateData = changes;
				//}

				//Drafts
				if (dbData.Status == JurisdictionStatus.DR.ToString())
				{
					var changes = new List<UpdateData>();
					changes.Add(new UpdateData() { DBFieldName = JurisdictionalDataField.JurisdictionType.ToString(), FieldName = JurisdictionalDataField.JurisdictionType.GetEnumDescription(), FromVal = dbData.Status, ToVal = JurisdictionStatus.RA.ToString() });
					changes.Add(new UpdateData() { DBFieldName = JurisdictionalDataField.DraftLetterDate.ToString(), FieldName = JurisdictionalDataField.DraftLetterDate.GetEnumDescription(), FromVal = dbData.DraftDate.ToString("d"), ToVal = null });
					jurisdiction.UpdateData = changes;
				}

				var msg = "";
				foreach (var data in jurisdiction.UpdateData)
				{
					msg += data.FieldName + " will be updated from <b>" + data.FromVal + "</b> to <b>" + data.ToVal + "</b><br>";
				}
				jurisdiction.Message = msg;
			}

			//Final confirmation for filenumber and jurisdiction, if needed we can pass down this model and show in detail
			var finalMsg = confirmData.Select(x => new
			{
				x.FileNumber,
				x.JurisdictionId,
				x.JurisdictionName,
				x.Message
			}).Distinct().ToList();

			return Json(new { Message = finalMsg, ConfirmDeleteData = confirmData }, JsonRequestBehavior.AllowGet);
		}

		[HttpPost]
		public ActionResult DeleteLetterFromDB(int documentVersionId, List<int> updateJurisdictionDataIds, List<DeleteConfirmData> deleteConfirmData, bool updateFields)
		{
			try
			{
				var sqlUpdateQuery = new StringBuilder();
				var setClause = "";

				//remove Drafts
				if (deleteConfirmData.Any(x => x.Status == JurisdictionStatus.DR.ToString()))
				{
					var draftJurisdictionDataIds = _dbSubmission.JurisdictionalData.Where(x => updateJurisdictionDataIds.Contains(x.Id) && x.Status == JurisdictionStatus.DR.ToString()).Select(x => x.Id).ToList();
					var draftLetterIds = _dbSubmission.SubmissionDrafts.Where(x => draftJurisdictionDataIds.Contains((int)x.JurisdictionDataId)).Select(x => x.DraftLetterId).ToList();
					_dbSubmission.SubmissionDrafts.Where(x => draftJurisdictionDataIds.Contains((int)x.JurisdictionDataId) && draftLetterIds.Contains(x.DraftLetterId)).Delete();
					foreach (var draftLetterId in draftLetterIds)
					{
						if (!_dbSubmission.SubmissionDrafts.Where(x => x.DraftLetterId == draftLetterId).Any())
						{
							_dbSubmission.DraftLetters.Where(x => x.Id == draftLetterId).Delete();
						}
					}
				}

				if (!updateFields) //just update JurisdictionPDF field
				{
					sqlUpdateQuery.AppendFormat("exec devin_setcontext {0};", _userContext.User.Id);
					sqlUpdateQuery.AppendFormat("UPDATE {0}", "JurisdictionalData");

					setClause = "JurisdictionPDF = null ";
					setClause += ", JurisdictionGPSPDF = null ";

					sqlUpdateQuery.AppendFormat(" SET {0}", setClause);
					var sWhere = string.Join(",", updateJurisdictionDataIds);
					sqlUpdateQuery.AppendFormat(" Where primary_ in ({0})", sWhere);
					var query = sqlUpdateQuery.ToString();
					_dbSubmission.Database.ExecuteSqlRaw(query);
				}
				else
				{
					// try to do it in group based on confirm Message
					var groupDelete = deleteConfirmData.Select(x => new
					{
						x.FileNumber,
						x.JurisdictionId,
						x.JurisdictionName,
						x.Message
					}).Distinct().ToList();

					var updateVal = "";
					foreach (var jurGroup in groupDelete)
					{
						sqlUpdateQuery.Clear();
						sqlUpdateQuery.AppendFormat("exec devin_setcontext {0};", _userContext.User.Id);
						sqlUpdateQuery.AppendFormat("UPDATE {0}", "JurisdictionalData");

						var jurData = deleteConfirmData.Where(x => x.FileNumber == jurGroup.FileNumber && x.JurisdictionId == jurGroup.JurisdictionId && x.Message == jurGroup.Message).Select(x => x.UpdateData).FirstOrDefault();

						setClause = "JurisdictionPDF = null ";
						setClause += ", JurisdictionGPSPDF = null ";
						foreach (var updateData in jurData)
						{
							updateVal = !string.IsNullOrEmpty(updateData.ToVal) ? "'" + updateData.ToVal + "'" : "null";
							setClause += "," + updateData.DBFieldName + "=" + updateVal;
						}

						sqlUpdateQuery.AppendFormat(" SET {0}", setClause);
						var sWhere = string.Join(",", updateJurisdictionDataIds);
						sqlUpdateQuery.AppendFormat(" Where primary_ in ({0})", sWhere);
						var query = sqlUpdateQuery.ToString();
						_dbSubmission.Database.ExecuteSqlRaw(query);

						_jurisdictionalDataService.ProcessForSubmissionStatus(updateJurisdictionDataIds);
						var jurisdictionalDatas = _dbSubmission.JurisdictionalData.Where(x => updateJurisdictionDataIds.Contains(x.Id)).ToList();

						_jurisdictionalDataService.ProcessForProtrackDataAfterModifiedJurisdictions(jurisdictionalDatas);
					}
				}

				//Remove from letterBook
				_dbSubmission.tbl_LetterBook.Where(x => x.DocumentVersionId == documentVersionId && x.ACTStatus == LetterBookACTStatus.Pending.ToString()).Delete();

				//Remove all Translations posted as supplement
				var allTranslations = _dbSubmission.tbl_DocumentVersionTranslations.Where(x => x.DocumentVersionId == documentVersionId).ToList();
				foreach (var tran in allTranslations)
				{
					var letterId = _dbSubmission.Letters.Where(x => x.PdfPath == tran.PdfPath).Select(x => x.Id).FirstOrDefault();
					_supplementalsService.DeleteSupplements(letterId, updateJurisdictionDataIds);
				}

				return Content("OK");
			}
			catch (Exception ex)
			{
				_logger.LogError(ex, "Delete Letter - DocVersionId:" + documentVersionId);
				return Content("Error");
			}
		}

		#endregion

		#region Update Jurisdictions info
		[HttpPost]
		public ContentResult UpdateJurisdictionCloseDate(List<JurisdictionalDataSubmissionIdPair> approvalData, DateTime closeDate)
		{
			var jurisdictionalDataIds = approvalData.Select(x => x.JurisdictionalDataId).ToList();
			//_dbSubmission.UserId = _userContext.User.Id;
			//_dbSubmission.JurisdictionalData.Where(x => jurisdictionalDataIds.Contains(x.Id)).Update(x => new JurisdictionalData { CloseDate = closeDate });

			if (jurisdictionalDataIds.Count > 0)
			{
				var strJurIds = string.Join(",", jurisdictionalDataIds);
				var query = "Update JurisdictionalData set JurisdictionDate = '" + closeDate + "' Where Primary_ in (" + strJurIds + ")";
				RunSQLCommand(query);
			}

			return Content("OK");
		}
		[HttpPost]
		public ContentResult UpdateJurisdictionUpdatedDate(List<JurisdictionalDataSubmissionIdPair> approvalData, DateTime updateDate, int updateDateReasonId)
		{
			var jurisdictionalDataIds = approvalData.Select(x => x.JurisdictionalDataId).ToList();
			//_dbSubmission.UserId = _userContext.User.Id;
			//_dbSubmission.JurisdictionalData.Where(x => jurisdictionalDataIds.Contains(x.Id)).Update(x => new JurisdictionalData { UpdateDate = updateDate, UpdateReasonId = updateDateReasonId });
			if (jurisdictionalDataIds.Count > 0)
			{
				var strJurIds = string.Join(",", jurisdictionalDataIds);
				var query = "Update JurisdictionalData set UpdatedDate = '" + updateDate + "', UpdatedDateReason= '" + updateDateReasonId + "' Where Primary_ in (" + strJurIds + ")";
				RunSQLCommand(query);
			}
			return Content("OK");
		}
		[HttpPost]
		public ContentResult UpdateJurisdictionDraftDate(List<JurisdictionalDataSubmissionIdPair> approvalData, DateTime draftDate)
		{
			var jurisdictionalDataIds = approvalData.Select(x => x.JurisdictionalDataId).ToList();
			//_dbSubmission.UserId = _userContext.User.Id;
			//_dbSubmission.JurisdictionalData.Where(x => jurisdictionalDataIds.Contains(x.Id)).Update(x => new JurisdictionalData { DraftDate = draftDate });

			if (jurisdictionalDataIds.Count > 0)
			{
				var strJurIds = string.Join(",", jurisdictionalDataIds);
				var query = "Update JurisdictionalData set DraftLetterDate = '" + draftDate + "' Where Primary_ in (" + strJurIds + ")";
				RunSQLCommand(query);
			}

			return Content("OK");
		}
		[HttpPost]
		public ContentResult UpdateCertificationLab(List<JurisdictionalDataSubmissionIdPair> approvalData, int certLabId)
		{
			var jurisdictionalDataIds = approvalData.Select(x => x.JurisdictionalDataId).ToList();
			//_dbSubmission.UserId = _userContext.User.Id;
			//_dbSubmission.JurisdictionalData.Where(x => jurisdictionalDataIds.Contains(x.Id)).Update(x => new JurisdictionalData { CertificationLabId = certLabId });

			if (jurisdictionalDataIds.Count > 0)
			{
				var strJurIds = string.Join(",", jurisdictionalDataIds);
				var query = "Update JurisdictionalData set CertLab = '" + certLabId + "' Where Primary_ in (" + strJurIds + ")";
				RunSQLCommand(query);
			}

			return Content("OK");
		}

		private void RunSQLCommand(string query)
		{
			var finalQuery = "exec devin_setcontext " + _userContext.User.Id + ";" + query;
			_dbSubmission.Database.ExecuteSqlRaw(finalQuery);
		}

		#endregion

		#region Confirmation before Process and Confirmation Email
		[HttpPost]
		public ContentResult GetConfirmationBeforeProcess(int bundleId, List<CreateLetterModel> letters)
		{
			var approvalSummary = GetProcessSummary(bundleId, letters);
			var message = GetSummaryMessage(approvalSummary, null);
			return Content(message);
		}

		[HttpPost]
		public void SendConfirmationEmail(int bundleId, List<CreateLetterModel> approvedLetters, List<CreateLetterModel> failedLetters)
		{
			var approvalSummary = GetProcessSummary(bundleId, approvedLetters);
			var message = GetSummaryMessage(approvalSummary, failedLetters);

			//NURVEmail
			SendEmailNotificationForRVNU(approvedLetters, approvalSummary.BundleName);

			//Confirmation Email
			var mailMsg = new MailMessage
			{
				From = new MailAddress("noreply@gaminglabs.com")
			};

			var emailAddresses = _dbSubmission.trLogins.Where(x => x.Id == approvalSummary.ReviewerId || x.Id == approvalSummary.LetterWriterId).Select(x => x.Email).ToList();
			emailAddresses.Add(_userContext.User.Email);
			foreach (var recipient in emailAddresses.Distinct().ToList())
			{
				mailMsg.To.Add(new MailAddress(recipient));
			}

			mailMsg.Subject = "Approved Letter: " + approvalSummary.BundleName;
			mailMsg.Body = "<strong>Letters have been processed. Below is the Summary for processed jurisdictions.</strong><br><br>" + message;

			_emailService.Send(mailMsg);

		}

		private string GetSummaryMessage(ProcessSummary approvalSummary, List<CreateLetterModel> failedLetters)
		{
			var emailBody = "<p><strong>Bundle Name: </strong>" + approvalSummary.BundleName + "</p>";
			emailBody += "<p><strong>Date of Report: </strong>" + (approvalSummary.DateOfReport).ToString("MM/dd/yyyy") + "</p>";
			emailBody += !string.IsNullOrWhiteSpace(approvalSummary.TypeOfReport) ? "<p><strong>Report Type: </strong>" + approvalSummary.TypeOfReport + "</p>" : "";
			emailBody += "<p><strong>Reviewer: </strong>" + approvalSummary.Reviewer + "</p>";
			emailBody += "<p><strong>Letter Writer: </strong>" + approvalSummary.LetterWriter + "</p>";
			emailBody += "<p><strong>Number of Billable Jurisdictions: </strong>" + approvalSummary.NumOfBillable + "</p>";
			emailBody += "<ul>";
			foreach (var item in approvalSummary.JurisdictionsBillableInfo)
			{
				if (Convert.ToBoolean(item.Info))
				{
					emailBody += "<li>" + item.JurisdictionName + " (" + item.JurisdictionId + ")</li>";
				}
			}
			emailBody += "</ul>";

			emailBody += "<p><strong>Number of Non Billable Jurisdictions: </strong>" + approvalSummary.NumOfNonBillable + "</p>";
			emailBody += "<ul>";
			foreach (var item in approvalSummary.JurisdictionsBillableInfo)
			{
				if (!Convert.ToBoolean(item.Info))
				{
					emailBody += "<li>" + item.JurisdictionName + " (" + item.JurisdictionId + ")</li>";
				}
			}
			emailBody += "</ul>";

			if (failedLetters != null)
			{
				emailBody += "<p><strong>FAILED Approvals: </strong>" + failedLetters.Count().ToString() + "</p>";
				emailBody += "<ul>";
				foreach (var item in failedLetters)
				{
					emailBody += "<li>" + item.LetterApprovalData.JurisdictionName + " (" + item.LetterApprovalData.JurisdictionId + ")</li>";
				}
				emailBody += "</ul>";
			}

			//Contains NU/RV
			if (approvalSummary.ContainsNURV.Select(x => x.Info).Distinct().Count() == 1)
			{
				emailBody += "<p><strong>Contains NU/RV: </strong>" + (Convert.ToBoolean(approvalSummary.ContainsNURV[0].Info) ? "Yes" : "No") + "</p>";
			}
			else if (approvalSummary.ContainsNURV.Select(x => x.Info).Distinct().Count() > 1)
			{
				emailBody += "<p><strong>Contains NU/RV: </strong></p>";
				emailBody += "</ul>";
				foreach (var item in approvalSummary.ContainsNURV)
				{
					emailBody += "<li>" + item.JurisdictionName + " (" + item.JurisdictionId + ")" + " - " + (Convert.ToBoolean(item.Info) ? "Yes" : "No") + "</li>";
				}
				emailBody += "</ul>";
			}

			//Billing Sheet
			if (approvalSummary.BillingSheet.Select(x => x.Info).Distinct().Count() == 1)
			{
				emailBody += "<p><strong>Billing Sheet: </strong>" + approvalSummary.BillingSheet[0].Info + "</p>";
				emailBody += approvalSummary.BillingSheet[0].Info == LetterBookBillingSheet.USCorrectionAddendum.ToString() ? "<p><strong>This approval contains a Correction/Addendum</strong></p>" : "";
			}
			else if (approvalSummary.BillingSheet.Select(x => x.Info).Distinct().Count() > 1)
			{
				emailBody += "<p><strong>Billing Sheet: </strong></p>";
				emailBody += "</ul>";
				foreach (var item in approvalSummary.BillingSheet)
				{
					emailBody += "<li>" + item.JurisdictionName + " (" + item.JurisdictionId + ")" + " - " + item.Info;
					emailBody += item.Info == LetterBookBillingSheet.USCorrectionAddendum.ToString() ? " <strong>This approval contains a Correction/Addendum</strong>" : "";
					emailBody += "</li>";
				}
				emailBody += "</ul>";
			}

			//Baly Info
			if (approvalSummary.BallyInfo.Count() > 0)
			{
				if (approvalSummary.BallyInfo.Select(x => x.Info).Distinct().Count() == 1)
				{
					emailBody += "<p><strong>Bally Type:  </strong>" + approvalSummary.BallyInfo[0].Info + "</p>";
				}
				else if (approvalSummary.BallyInfo.Select(x => x.Info).Distinct().Count() > 1)
				{
					emailBody += "<p><strong>Bally Type:  </strong></p>";
					emailBody += "</ul>";
					foreach (var item in approvalSummary.BallyInfo)
					{
						emailBody += "<li>" + item.JurisdictionName + " (" + item.JurisdictionId + ")" + " - " + item.Info + "</li>";
					}
					emailBody += "</ul>";
				}
			}

			// num of items billed
			if (approvalSummary.NumOfItemsBilled.Select(x => x.Info).Distinct().Count() == 1)
			{
				emailBody += "<p><strong>Number of Items Being Billed: </strong>" + approvalSummary.NumOfItemsBilled[0].Info + "</p>";
			}
			else if (approvalSummary.NumOfItemsBilled.Select(x => x.Info).Distinct().Count() > 1)
			{
				emailBody += "<p><strong>Number of Items Being Billed: </strong></p>";
				emailBody += "</ul>";
				foreach (var item in approvalSummary.NumOfItemsBilled)
				{
					emailBody += "<li>" + item.JurisdictionName + " (" + item.JurisdictionId + ")" + " - " + item.Info + "</li>";
				}
				emailBody += "</ul>";
			}

			//fixed Price info
			if (approvalSummary.FixedPriceContractNumber.Count() > 0)
			{
				if (approvalSummary.FixedPriceContractNumber.Select(x => x.Info).Distinct().Count() == 1)
				{
					emailBody += "<p><strong>Specialized (Fixed Price / Statement of Work / Proposal): </strong>" + approvalSummary.FixedPriceContractNumber[0].Info + "</p>";
				}
				else if (approvalSummary.FixedPriceContractNumber.Select(x => x.Info).Distinct().Count() > 1)
				{
					emailBody += "<p><strong>Specialized (Fixed Price / Statement of Work / Proposal): </strong></p>";
					emailBody += "</ul>";
					foreach (var item in approvalSummary.FixedPriceContractNumber)
					{
						emailBody += "<li>" + item.JurisdictionName + " (" + item.JurisdictionId + ")" + " - " + item.Info + "</li>";
					}
					emailBody += "</ul>";
				}
			}

			return emailBody;
		}


		private ProcessSummary GetProcessSummary(int bundleId, List<CreateLetterModel> letters)
		{
			var bundleInfo = _dbSubmission.QABundles.Where(x => x.Id == bundleId).Select(x => new { Name = x.ProjectName, Reviewer = x.QAReviewer, Tasks = x.Tasks }).SingleOrDefault();
			var letterWriter = GetBundleLetterWriterOrReviewerUser(bundleInfo.Tasks.ToList());

			var summary = new ProcessSummary();
			summary.BundleName = bundleInfo.Name;
			summary.Reviewer = (bundleInfo.Reviewer != null) ? bundleInfo.Reviewer.FName + " " + bundleInfo.Reviewer.LName : "";
			summary.ReviewerId = (bundleInfo.Reviewer != null) ? bundleInfo.Reviewer.Id : 0;
			summary.LetterWriter = (letterWriter != null) ? letterWriter.FName + " " + letterWriter.LName : "";
			summary.LetterWriterId = (letterWriter != null) ? letterWriter.Id : 0;

			if (letters != null)
			{
				summary.DateOfReport = letters[0].DateOfReport;
				summary.TypeOfReport = letters[0].DocumentType;
				summary.NumOfBillable = letters.Where(x => x.LetterApprovalData.IsBillable).ToList().Count();
				summary.NumOfNonBillable = letters.Where(x => !x.LetterApprovalData.IsBillable).ToList().Count();
				summary.ContainsNURV = letters.Select(x => new JurisdictionInfo { JurisdictionId = x.LetterApprovalData.JurisdictionId, JurisdictionName = x.LetterApprovalData.JurisdictionName, Info = x.ContainsRVNU.ToString() }).ToList();
				summary.JurisdictionsBillableInfo = letters.Select(x => new JurisdictionInfo { JurisdictionId = x.LetterApprovalData.JurisdictionId, JurisdictionName = x.LetterApprovalData.JurisdictionName, Info = x.LetterApprovalData.IsBillable.ToString() }).ToList();
				summary.NumOfItemsBilled = letters.Select(x => new JurisdictionInfo { JurisdictionId = x.LetterApprovalData.JurisdictionId, JurisdictionName = x.LetterApprovalData.JurisdictionName, Info = x.BillingInfo.ItemsBeingBilledCount.ToString() }).ToList();
				summary.BillingSheet = letters.Select(x => new JurisdictionInfo { JurisdictionId = x.LetterApprovalData.JurisdictionId, JurisdictionName = x.LetterApprovalData.JurisdictionName, Info = x.BillingInfo.BillingSheet }).ToList();
				summary.FixedPriceContractNumber = letters.Where(x => x.BillingInfo.IsFixedPrice).Select(x => new JurisdictionInfo { JurisdictionId = x.LetterApprovalData.JurisdictionId, JurisdictionName = x.LetterApprovalData.JurisdictionName, Info = (x.BillingInfo.IsFixedPrice) ? x.BillingInfo.ContractNumber : "" }).ToList();

				var fileInfo = new FileNumber(bundleInfo.Name);
				if (_appliesBallyType.Contains(fileInfo.ManufacturerCode))
				{
					summary.BallyInfo = letters.Select(x => new JurisdictionInfo { JurisdictionId = x.LetterApprovalData.JurisdictionId, JurisdictionName = x.LetterApprovalData.JurisdictionName, Info = x.BillingInfo.BallyType }).ToList();
				}
			}

			return summary;
		}


		#endregion

		#region  Helper methods


		private DraftLetterModel BuildDraftLetterModel(int draftLetterId)
		{
			var results = _dbSubmission.DraftLetters.Where(d => d.Id == draftLetterId);
			var draft = results.FirstOrDefault();

			var items = results.FirstOrDefault().SubmissionDrafts
			.Select(x => new DraftItem
			{
				DateCode = x.Submission.DateCode,
				FileNumber = x.Submission.FileNumber,
				GameName = x.Submission.GameName,
				IdNumber = x.Submission.IdNumber,
				JurisdictionalDataId = x.JurisdictionalData.Id,
				Jurisdiction = x.JurisdictionalData.JurisdictionName,
				JurisdictionId = x.JurisdictionalData.JurisdictionId,
				Manufacturer = x.Submission.ManufacturerDescription,
				ManufacturerCode = x.Submission.ManufacturerCode,
				PartNumber = x.Submission.PartNumber,
				Version = x.Submission.Version
			}).ToList();

			var hasDraftDateJurisdictions = items.Exists(x => _draftDateJurisdictions.Contains(x.Jurisdiction));

			var letter = results.Select(x => new DraftLetterModel
			{
				draftLetterId = x.Id,
				FilePath = x.FilePath,
				DraftDate = x.DraftDate,
				Status = x.Status,
				TimelineType = x.TimelineType == null || x.TimelineType == "" ? "None" : x.TimelineType,
				IsTimelineRequested = x.IsTimelineRequested,
				IsInfoRequested = x.IsInfoRequested != null ? (bool)x.IsInfoRequested : false,
				TimelineDate = x.TimelineDate,
				IsOnHold = x.IsOnHold != null ? (bool)x.IsOnHold : false,
				IsRegeneratedPDF = x.WordFilepath != null &&
									x.WordFilepath.Length > 0 &&
									x.DraftDate > GenerationCutoffDate ? true : false,
				ShouldReplaceDates = !hasDraftDateJurisdictions,
				AssignedUserEmail = x.AssignedUserEmail,
				AssignedUserName = x.AssignedUserName != null ? x.AssignedUserName : "",
				Language = LetterLanguage.English,
				ApprovalDate = x.ApprovalDate
			}).FirstOrDefault();

			letter.DraftItems = items;

			return letter;
		}

		private CreateLetterModel BuildApprovedLetterModel(List<int> approvedJurisdictionalDataIds, CreateLetterModel approvedLetterModel, PDFSaveResults pdfs)
		{
			var updatedLetter = new CreateLetterModel();
			updatedLetter = approvedLetterModel; //either we can recreate the whole model by generating group from approvedids above or set the new values, we can discuss out
			updatedLetter.CurrentJurisdictionalStatus = approvedLetterModel.SelectedApprovalStatus;
			updatedLetter.ExistingPdfPath = pdfs.MainLetterResult.pdfPath; // Do we need to show Translation or just main?
			updatedLetter.ExistingPdfName = (pdfs.MainLetterResult.pdfPath != null) ? Path.GetFileName(pdfs.MainLetterResult.pdfPath) : null;
			updatedLetter.HasExistingCertification = true;
			var existingDocumentVersion = _dbSubmission.DocumentVersionLinks.Where(x => approvedLetterModel.JurisdictionalDataIds.Contains(x.JurisdictionalDataId) && x.DocumentVersion.FilePath == pdfs.MainLetterResult.pdfPath)
										.OrderByDescending(x => x.DocumentVersion.ApproveDate)
										.Select(x => new { x.DocumentVersion.Document.DocumentTypeId, x.DocumentVersionId, x.DocumentVersion.Version, x.DocumentVersion.DocumentId })
										.FirstOrDefault();

			if (existingDocumentVersion != null)
			{
				updatedLetter.DocumentId = existingDocumentVersion.DocumentId;
				updatedLetter.DocumentVersion = existingDocumentVersion.Version;
				updatedLetter.DocumentVersionId = existingDocumentVersion.DocumentVersionId;
				var translations = _dbSubmission.tbl_DocumentVersionTranslations.Where(x => x.DocumentVersionId == existingDocumentVersion.DocumentVersionId).Select(x => new { LanguageId = x.LanguageId, PdfPath = x.PdfPath, Id = x.Id }).ToList();
				updatedLetter.TranslationCount = translations.Count();
				var selectedJurs = approvedLetterModel.JurisdictionalDataIds;
				var webTranslations = _dbSubmission.SubmissionLetters.Where(x => selectedJurs.Contains((int)x.JurisdictionalDataId)).Select(x => x.Letter.PdfPath).ToList().Distinct();

				updatedLetter.MainTranslationsDisplay = translations.Select(x => new MainPdfsDisplay()
				{
					Language = ((LetterLanguage)x.LanguageId).ToString(),
					PdfPath = x.PdfPath,
					PdfName = (x.PdfPath != null) ? Path.GetFileName(x.PdfPath) : null,
					ExistsInDB = (webTranslations.Any(w => w == x.PdfPath)),
					Id = x.Id
				}).ToList();
				if (_dbSubmission.DraftLetters.Any(x => x.FilePath == updatedLetter.ExistingPdfPath))
				{
					var draftLetterId = _dbSubmission.SubmissionDrafts.Where(x => x.DraftLetter.FilePath == updatedLetter.ExistingPdfPath && selectedJurs.Contains((int)x.JurisdictionDataId)).Select(x => (int)x.DraftLetterId).FirstOrDefault();
					updatedLetter.DraftLetter = BuildDraftLetterModel(draftLetterId);
				}
			}
			return updatedLetter;
		}

		//private SupplementalLettersVue BuildSupplementViewModel(List<JurisdictionalData> allJurisdictions, int bundleId, List<EF.GPS.GPSItem> allRelevantGpsDocuments)
		//{
		//	var bundleInfo = _dbSubmission.QABundles.Where(x => x.Id == bundleId).Select(x => new { BundleName = x.Project.ProjectName, FileNumber = x.Project.FileNumber, JurisdictionExt = x.Project.Suffix }).SingleOrDefault();
		//	var manufCode = _dbSubmission.Submissions.Where(x => x.FileNumber == bundleInfo.FileNumber).Select(x => x.ManufacturerCode).FirstOrDefault();

		//	var vm = new SupplementalLettersVue();
		//	vm.SupplementLetter.BundleId = bundleId;
		//	vm.SupplementLetter.DateOfReport = DateTime.Now;
		//	vm.SupplementLetter.FileNumber = bundleInfo.FileNumber;
		//	vm.SupplementLetter.CopyToWeb = true;
		//	vm.SupplementLetter.MainLetter = new IndividualLetterData();
		//	vm.SupplementLetter.LetterApprovalData = new LetterApprovalData();
		//	vm.SupplementLetter.ManufacturerCode = manufCode;

		//	var jurisdictionGroups = allJurisdictions.Select(x => x.Jurisdiction).Distinct();
		//	vm.AvailableJurisdictions = jurisdictionGroups.Select(x => new SelectListItem()
		//	{
		//		Text = x.Name,
		//		Value = x.Id,
		//		Selected = false
		//	}).ToList();

		//	var allAvailableJurIds = allJurisdictions
		//						.GroupBy(jd => new { jd.JurisdictionId })
		//						.Select(group => new
		//						{
		//							JurisdictionalDataIds = group.ToList().Select(x => new JurisdictionalDataSubmissionIdPair()
		//							{
		//								JurisdictionalDataId = x.Id,
		//								SubmissionId = (int)x.SubmissionId,
		//								IdNumber = x.Submission.IdNumber,
		//								DateCode = x.Submission.DateCode,
		//								Version = x.Submission.Version,
		//								FileNumber = x.Submission.FileNumber,
		//								IsAttachedToLetter = true
		//							}).ToList(),
		//							JurisdictionId = group.Key.JurisdictionId
		//						}).ToList();


		//	foreach (var jurisdiction in jurisdictionGroups)
		//	{
		//		var availableSourceDocs = allRelevantGpsDocuments
		//			.Where(doc => doc.GPSJurisdictionalDatas.Any(gjd => gjd.JuriId == Convert.ToInt32(jurisdiction.Id)))
		//			.ToList();

		//		vm.AvailableApprovalData.Add(new LetterApprovalData()
		//		{
		//			JurisdictionId = jurisdiction.Id,
		//			JurisdictionName = jurisdiction.Name,
		//			JurisdictionalDataIds = allAvailableJurIds.Where(x => x.JurisdictionId.ToString() == jurisdiction.Id).Select(x => x.JurisdictionalDataIds).FirstOrDefault(),
		//			AvailableSourceDocs = availableSourceDocs.Select(doc => new GPSItemViewModel(doc)).ToList(),
		//		});
		//	}

		//	return vm;
		//}

		private List<JurisdictionalData> GetAllAvailableJurisdictions(int bundleId)
		{
			var bundle = _dbSubmission.QABundles.Include("Project").SingleOrDefault(b => b.Id == bundleId);
			var mergedProjects = GetAllMergedProjects(bundle.ProjectId);
			var bundleJurisdictions = _dbSubmission.trProjectJurisdictionalData
			.Where(pjd => pjd.ProjectId == bundle.ProjectId || mergedProjects.Contains(pjd.ProjectId))
			.Select(pjd => pjd.JurisdictionalData)
			.ToList();

			return bundleJurisdictions;
		}

		private List<int> GetAllMergedProjects(int projectId)
		{
			var project = _dbSubmission.trProjects.Where(x => x.Id == projectId).Select(x => new { MergedWith = x.MergedWith, ProjectName = x.ProjectName }).FirstOrDefault();
			var query = (from p in _dbSubmission.trProjects
						 where (p.ProjectName == project.MergedWith)
						 select p.Id
						).Union(
						from p in _dbSubmission.trProjects
						where (p.MergedWith == project.ProjectName)
						select p.Id
						).AsQueryable();

			if (!string.IsNullOrEmpty(project.MergedWith))
			{
				query = query.Union(
						from p in _dbSubmission.trProjects
						where (p.MergedWith == project.MergedWith && p.ProjectName != project.ProjectName)
						select p.Id
						).AsQueryable();
			}

			var allProjects = query.ToList();
			return (allProjects);
		}
		/*need to discuus this with QA, as may be Letter table can have same decription as our enum and instead of abbr it can be more descriptive*/
		private string GetSupplementDocumentTypeDesc(string abbr)
		{
			if (abbr == "Repo")
			{
				return "Report";
			}
			else if (abbr == "Supp")
			{
				return "Supplement";
			}
			else
			{
				return abbr;
			}
		}

		private void SetDefaultDocumentTypeAndStatus(CreateLetterModel letter, List<QABundles_Task> bundleTasks, bool isClosedBundle)
		{
			if (!isClosedBundle)
			{
				letter.DocumentTypeId = (int)DocumentType.Certification;
				letter.DocumentType = DocumentType.Certification.ToString();
			}
			else
			{
				var allLWTasks = bundleTasks.Where(x => _letterWritingTasks.Contains(x.Definition.TaskDefDesc) && x.tfCurrent == true).ToList();
				if (allLWTasks.Any(x => x.Definition.TaskDefDesc == DocumentType.Addendum.ToString()))
				{
					letter.DocumentTypeId = (int)DocumentType.Addendum;
					letter.DocumentType = DocumentType.Addendum.ToString();

					var reasonInfo = _dbSubmission.tbl_lst_UpdatedDateReason.Where(x => x.Reason == DocumentType.Addendum.ToString()).Select(x => new { x.Id, x.Reason }).SingleOrDefault();
					if (reasonInfo != null)
					{
						letter.UpdatedDateReasonId = reasonInfo.Id;
						letter.UpdatedDateReason = reasonInfo.Reason;
					}
					letter.UpdateDate = DateTime.Now;
				}
				else if (allLWTasks.Any(x => (x.Definition.TaskDefDesc == DocumentType.Correction.ToString() || x.Definition.TaskDefDesc == "Revocation Without Replacement")))
				{
					letter.DocumentTypeId = (int)DocumentType.Correction;
					letter.DocumentType = DocumentType.Correction.ToString();

					var reasonInfo = _dbSubmission.tbl_lst_UpdatedDateReason.Where(x => x.Reason == DocumentType.Correction.ToString()).Select(x => new { x.Id, x.Reason }).SingleOrDefault();
					if (reasonInfo != null)
					{
						letter.UpdatedDateReasonId = reasonInfo.Id;
						letter.UpdatedDateReason = reasonInfo.Reason;
					}
					letter.UpdateDate = DateTime.Now;
				}
				else if (allLWTasks.Any(x => x.Definition.TaskDefDesc == DocumentType.ReDraft.ToString() || x.Definition.TaskDefDesc == "Re-Draft"))
				{
					letter.DocumentTypeId = (int)DocumentType.ReDraft;
					letter.DocumentType = DocumentType.ReDraft.ToString();
				}
				else if (allLWTasks.Any(x => x.Definition.TaskDefDesc == DocumentType.NonClient.ToString()))
				{
					letter.DocumentTypeId = (int)DocumentType.NonClient;
					letter.DocumentType = DocumentType.NonClient.ToString();
				}
				else if (allLWTasks.Any(x => x.Definition.TaskDefDesc == "Loto Quebec Finalizing"))
				{
					letter.DocumentTypeId = (int)DocumentType.Final;
					letter.DocumentType = DocumentType.Final.ToString();
					letter.SelectedApprovalStatus = JurisdictionStatus.AP.ToString();
				}
				else if (allLWTasks.Any(x => x.Definition.TaskDefDesc == DocumentType.TKNumber.GetEnumDescription()))
				{
					letter.DocumentTypeId = (int)DocumentType.TKNumber;
					letter.DocumentType = DocumentType.TKNumber.ToString();
					letter.SelectedApprovalStatus = JurisdictionStatus.AP.ToString();
				}
			}
		}

		private QABundle GetValidQABundle(int bundleId)
		{
			var bundle = _dbSubmission.QABundles.Include(b => b.Project.JurisdictionalData).SingleOrDefault(b => b.Id == bundleId);
			if (bundle == null)
			{
				throw new NullReferenceException("Bundle does not exist.");
			}
			return bundle;
		}

		private string ConstructSupplementalEmailBody(CreateLetterModel letter, LetterBillingInfo billingInfo, QABundle bundle)
		{
			var emailBody = "<p><strong>A new Supplemental Letter has been added for: </strong>" + bundle.ProjectName + "</p>";
			emailBody += "<p><strong>Date Added: </strong>" + DateTime.Now.ToString("MM/dd/yyyy") + "</p>";
			emailBody += "<p><strong>For Jurisdiction: </strong>" +
				string.Join(", ", bundle.Project.JurisdictionalData
					.Where(x => x.JurisdictionalData.JurisdictionId == letter.JurisdictionId)?.Select(x => x.JurisdictionalData.JurisdictionName).Distinct()) + "</p>";
			emailBody += "<p><strong>Language: </strong>" + letter.MainLetter.Language.ToString() + "</p>";
			emailBody += "<p><strong>Billing Sheet: </strong>" + billingInfo.BillingSheet + "</p>";
			emailBody += billingInfo.BillingSheet != LetterBookBillingSheet.None.ToString()
				? "<p><strong>Number of Billable Items: </strong>" + billingInfo.ItemsBeingBilledCount + "</p>" : "";
			return emailBody;
		}

		private EmailResponse SendSupplementEmail(SupplementalEmail emailModel)
		{
			var mailMsg = new MailMessage
			{
				From = new MailAddress("noreply@gaminglabs.com")
			};
			foreach (var recipient in emailModel.Recipients)
			{
				mailMsg.To.Add(new MailAddress(recipient));
			}
			mailMsg.Subject = "Supplemental Letter: " + emailModel.BundleName;
			mailMsg.Body = emailModel.Body;
			return _emailService.Send(mailMsg);
		}

		private void SendEmailNotificationForRVNU(List<CreateLetterModel> letters, string bundleName)
		{
			var mailMsg = new MailMessage
			{
				From = new MailAddress("noreply@gaminglabs.com")
			};

			if (letters != null)
			{
				var containsNURVLetters = letters.Where(x => x.ContainsRVNU).ToList();
				if (containsNURVLetters.Count > 0)
				{
					var emailTo = containsNURVLetters.Select(x => x.SendEmailRVNUTo).FirstOrDefault(); //currently they all have same
					var jurisdictions = containsNURVLetters.Select(x => x.LetterApprovalData.JurisdictionName + "(" + x.LetterApprovalData.JurisdictionId + ")").Distinct().ToList();

					var emailAddressesList = _dbSubmission.trLogins.Where(x => emailTo.Contains(x.Id)).Select(x => x.Email).ToList();
					foreach (var recipient in emailAddressesList)
					{
						mailMsg.To.Add(new MailAddress(recipient));
					}
					mailMsg.Subject = "Letter contains RV/NU Items: " + bundleName;
					var emailBody = "<p><strong>A new Letter has been posted for: </strong>" + bundleName + " <strong>which contains RV/NU items.</strong></p> ";
					emailBody += "<p><strong>Date Added: </strong>" + DateTime.Now.ToString("MM/dd/yyyy") + "</p>";
					emailBody += "<p><strong>For Jurisdiction: </strong><br>" + String.Join("<br>", jurisdictions) + "</p>";
					mailMsg.Body = emailBody;

					_emailService.Send(mailMsg);
				}
			}
		}

		private List<string> SetNonBillableJurisdictions(List<CreateLetterModel> letterGroups, string bundleName)
		{
			var nonBillableKeys = letterGroups.Where(x => _nonBillableJurisdictions.ContainsKey(x.JurisdictionId)).ToList();
			var nonBillableJurIds = new List<string>();

			var manufacturerCode = (new FileNumber(bundleName)).ManufacturerCode;

			if (nonBillableKeys.Count > 0)
			{
				foreach (var item in nonBillableKeys)
				{
					if (item.JurisdictionId != ((int)CustomJurisdiction.ArgentinaBuenosAires).ToString() && letterGroups.Any(x => x.JurisdictionId == item.JurisdictionId))
					{
						nonBillableJurIds.AddRange(_nonBillableJurisdictions[item.JurisdictionId]);
					}
					else if (item.JurisdictionId == ((int)CustomJurisdiction.ArgentinaBuenosAires).ToString() && letterGroups.Any(x => x.JurisdictionId == item.JurisdictionId) &&
						(_appliesBallyType.Contains(manufacturerCode)))
					{
						nonBillableJurIds.AddRange(_nonBillableJurisdictions[item.JurisdictionId]);
					}
				}
			}

			foreach (var letter in letterGroups)
			{
				if (nonBillableJurIds.Contains(letter.JurisdictionId))
				{
					letter.LetterApprovalData.IsBillable = false;
				}
			}

			return nonBillableJurIds;
		}

		private List<int> CheckIfAdditionalLettersNeedsRemoved(CreateLetterModel letterToApprove)
		{
			var whichIdsToRemove = new List<int>();
			//repost or closed bubdle, previously posted language needs to be removed
			if (letterToApprove.Translations.Count > 0)
			{
				foreach (var tranLetter in letterToApprove.Translations)
				{
					var lang = tranLetter.MainLetter.Language.ToString();
					var ids = _dbSubmission.SubmissionLetters.Where(x => x.Letter.Language == lang && x.Letter.LetterType == "Repo" && letterToApprove.JurisdictionalDataIds.Contains((int)x.JurisdictionalDataId)).Select(x => (int)x.LetterId).Distinct().ToList();
					whichIdsToRemove.AddRange(ids);
				}
			}
			return whichIdsToRemove;
		}

		private trLogin GetBundleLetterWriterOrReviewerUser(List<QABundles_Task> bundleTasks)
		{
			var projectId = bundleTasks.Select(x => x.QABundle.ProjectId).FirstOrDefault();
			var isBundleFinalizedInLetterGen = _letterGenServices.IsBundleLetterFinalizedInLetterGen(projectId);

			var task = bundleTasks
				.Where(x => x.Definition.TaskDefDesc == BundleTask.LetterReviewing.GetEnumDescription()
				&& x.tfCurrent == true)
				.FirstOrDefault();

			if (task == null || !isBundleFinalizedInLetterGen)
			{
				var lwTaskUser = bundleTasks
				.Where(x => _letterWritingTasks.Contains(x.Definition.TaskDefDesc) && x.tfCurrent == true)
				.OrderByDescending(x => x.Definition.TaskFinalizationPriority)
				.FirstOrDefault();

				task = (lwTaskUser != null) ? lwTaskUser : task;
			}

			var user = task?.User ?? new trLogin { LocationId = 0 };
			return user;
		}
		#endregion
	}
}