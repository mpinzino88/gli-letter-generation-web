using EF.Submission;
using GLI.EF.LetterContent;
using GLIMathOperations.Interfaces;
using Submissions.Common;
using Submissions.Core;
using Submissions.Core.Interfaces.LetterContent;
using Submissions.Core.Models;
using Submissions.Core.Models.LetterContent;
using Submissions.Web.Areas.Documents.Models.Coversheet;
using Submissions.Web.Areas.Documents.Models.Generate;
using Submissions.Web.Areas.Documents.Models.Generate.Helper;
using Submissions.Web.Areas.Documents.Models.NoteManager;
using Submissions.Web.Areas.Evolution.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Memo = Submissions.Web.Areas.Documents.Models.NoteManager.Memo;

namespace Submissions.Web.Areas.Documents.Controllers
{
	[AuthorizeUser(Permission = Permission.NoteManager, HasAccessRight = AccessRight.Read)]
	public class NoteManagerController : Controller
	{
		private readonly ICoversheetGatherMethods _coverSheetGatherMethods;
		private readonly ILetterContentService _letterContentService;
		private readonly ILetterContentContext _dbLetterContentContext;
		private readonly ISubmissionContext _dbSubmissions;
		private readonly IConfig _config;

		private readonly IGenerateService _generateService;
		private readonly IEvolutionService _evolutionService;
		private readonly IUserContext _user;
		private readonly ILetterContentSQLService _letterContentSQLService;

		public NoteManagerController(ILetterContentService letterContentService, ILetterContentContext dbLetterContentContext, ISubmissionContext dbSubmissions, IUserContext user, IConfig config, IGenerateService generateService, IEvolutionService evolutionService, ICoversheetGatherMethods coversheetGatherMethods, ILetterContentSQLService letterContentSQLService)
		{
			_generateService = generateService;
			_evolutionService = evolutionService;
			_letterContentService = letterContentService;
			_dbLetterContentContext = dbLetterContentContext;
			_dbSubmissions = dbSubmissions;
			_user = user;
			_config = config;
			_coverSheetGatherMethods = coversheetGatherMethods;
			_letterContentSQLService = letterContentSQLService;
		}

		#region Index
		public ActionResult Index()
		{
			return View(new IndexNoteViewModel() { allowSearchToggle = _user.User.Id == 1415 }); //To be removed later
		}
		#endregion

		#region Edit
		public ActionResult Edit(int? id)
		{
			var vm = new EditNoteViewModel();
			vm.Sections = BuildLetterSections();
			vm.CompareCriterias = BuildCompareCriterias();
			vm.MockMemoData = BuildMockMemoData(58769);
			vm.Mode = Mode.Add;

			if (id != null)
			{
				var result = _letterContentService.GetLetterContent((int)id, false);
				//var result = _letterContentSQLService.GetLetterContent((int)id, false);
				if (result == null)
				{
					return RedirectToAction("Index");
				}
				var converted = BuildLetterContent(result);
				vm.LetterContent = converted;
				vm.Mode = Mode.Edit;
			}

			return View(vm);
		}
		#endregion

		#region API
		[HttpPost]
		public ContentResult GetQuestion(int id)
		{
			var result = BuildQuestion(_letterContentService.GetQuestion(id));

			return Content(ComUtil.JsonEncodeCamelCase(result), "application/json");
		}

		[HttpPost]
		public ContentResult GetMockDataByBundleId(int id)
		{
			return Content(ComUtil.JsonEncodeCamelCase(BuildMockMemoData(id)), "application/json");
		}

		[HttpPost]
		public ContentResult Save(Models.NoteManager.LetterContent letterContent)
		{
			Models.NoteManager.LetterContent result;
			var letterContentToMake = BuildLetterContentDTO(letterContent);

			// Add
			if (letterContent.Id == 0)
			{
				result = BuildLetterContent(_letterContentService.AddLetterContent(letterContentToMake));
			}
			// Update
			else
			{
				result = BuildLetterContent(_letterContentService.UpdateLetterContent(letterContentToMake));
			}

			return Content(ComUtil.JsonEncodeCamelCase(result), "application/json");
		}

		[HttpPost]
		public ContentResult GetBaseTemplate(string name)
		{
			string result = string.Empty;

			var baseTableTemplate = "<div style=\"width:100%;\"> <h2>Component Information Table</h2> <table border=1 cellspacing=0 cellpadding=0 style='width:100%;border-collapse:collapse;border:none'> <thead> <tr> <th>ID Number</th> <th v-if=\"includeVersionInComponentTable\">Version</th> <th>Game Name</th> <th>Jurisdiction Count</th> </tr> </thead> <tbody> <tr v-for=\"component in components\"> <td>{{component.idNumber}}</td> <td v-if=\"includeVersionInComponentTable\">{{component.version}}</td> <td>{{component.gameName}}</td> <td>{{ component.jurisdictionalData.length }}</td> </tr> </tbody> </table> </div>";

			var baseHeaderTemplate = "<div style=\"width:100%;\"> <p class=MsoNormal style='text-align:justify;background:#B8CCE4'><b><span lang=EN-US style='font-size:12.0pt;font-family: \"Calibri\",sans-serif;color:black'>Header Title</span></b></p> </div>";

			switch (name)
			{
				case "baseTable":
					result = baseTableTemplate;
					break;
				case "baseHeader":
					result = baseHeaderTemplate;
					break;
				default:
					break;
			}

			return Content(ComUtil.JsonEncodeCamelCase(result), "application/json");
		}

		[HttpPost]
		public ContentResult GetMemoLineage(int originialId)
		{
			var result = new List<MemoLineageItem>();
			result = BuildMemoLineage(_letterContentService.GetMemoLineage(originialId));
			return Content(ComUtil.JsonEncodeCamelCase(result), "application/json");
		}

		[HttpPost]
		public ContentResult GetMemo(int id)
		{
			return Content(ComUtil.JsonEncodeCamelCase(BuildMemo(_letterContentService.GetMemo(id))), "application/json");
		}

		[HttpPost]
		public ContentResult SearchQuestions(QuestionSearchFilter filter)
		{
			return Content(ComUtil.JsonEncodeCamelCase(_letterContentService.GetQuestions(filter)), "application/json");
		}

		[HttpPost]
		public ContentResult SearchMemos(MemoSearchFilter filter)
		{
			//var results = BuildMemos(_letterContentSQLService.GetMemosSQL(filter));
			//var results = BuildMemos(_letterContentService.GetMemos(filter));

			//Keep to switch, to compare counts. will be taken out once new associations will be added
			//Hit EF or SQL

			var memoData = new List<MemoDTO>();
			if (filter.UseSQL)
			{
				memoData = _letterContentSQLService.GetMemos(filter, false); // new
			}
			else
			{
				memoData = _letterContentService.GetMemos(filter);
			}

			//Build VM
			var results = new List<Memo>();
			if (filter.UseSQL)
			{
				results = BuildMemoForSearch(memoData); //new
			}
			else
			{
				results = BuildMemos(memoData);
			}
			return Content(ComUtil.JsonEncodeCamelCase(results), "application/json");
		}

		private List<Memo> BuildMemoForSearch(List<MemoDTO> memos)
		{
			var languages = _dbSubmissions.tbl_lst_Language.ToList();
			var results = memos.Select(x => new Memo()
			{
				BusinessLogicExpression = x.BusinessLogicExpression,
				Content = x.Content,
				Description = x.Description,
				Id = x.Id,
				Language = languages.Where(l => l.Id == x.Language).Select(l => l.Language).FirstOrDefault(),
				LanguageId = x.Language,
				LetterContentId = x.LetterContentId,
				LetterContentName = x.LetterContentName,
				LocalId = x.LocalId,
				Name = x.Name,
				OrderId = x.OrderId,
				SecondaryLanguage = languages.Where(l => l.Id == x.SecondaryLanguage).Select(l => l.Language).FirstOrDefault(),
				SecondaryLanguageId = x.SecondaryLanguage
			}).ToList();

			return results;
		}

		[HttpPost]
		public ContentResult DeprecateMemo(int id)
		{
			return Content(ComUtil.JsonEncodeCamelCase(_letterContentService.DeprecateMemo(id)), "application/json");
		}
		[HttpPost]
		public ContentResult DeprecateLetterContent(Models.NoteManager.LetterContent payload)
		{
			return Content(ComUtil.JsonEncodeCamelCase(_letterContentService.DeprecateLetterContent(payload.Id, payload.SectionId)), "application/json");
		}
		[HttpPost]
		public ContentResult GetMemos(int letterContentId)
		{
			return Content(ComUtil.JsonEncodeCamelCase(BuildMemos(_letterContentService.GetMemos(new MemoSearchFilter { LetterContentId = letterContentId, IncludeDeprecated = false }))), "application/json");
		}

		/// <summary>
		/// Check to see if any of the questions used in this memo have changed since the memo has been opened.
		/// </summary>
		/// <param name="questions">Current Questions from Client</param>
		/// <returns>List of questions that have been modified since the memo has been open in the client.</returns>
		public ContentResult ValidateMemoQuestions(List<Models.NoteManager.Question> questionsFromClient)
		{
			var result = new List<Models.NoteManager.Question>();
			if (questionsFromClient == null)
			{
				return Content(ComUtil.JsonEncodeCamelCase(result), "application/json");
			}
			var questionIdsToCheck = questionsFromClient.Select(x => x.Id).ToList();
			var latestQuestions = _dbLetterContentContext.Questions.Where(x => questionIdsToCheck.Contains(x.Id)).Select(x => new { x.Id, x.EditDate }).ToList();

			foreach (var latestQuestion in latestQuestions)
			{
				var target = questionsFromClient.Where(x => x.Id == latestQuestion.Id).Single();
				if (latestQuestion.EditDate != target.EditDate)
					result.Add(target);
			}

			return Content(ComUtil.JsonEncodeCamelCase(result), "application/json");
		}

		[HttpPost]
		public ContentResult GetValidations(ValidationSearchFilter filter)
		{
			var result = new List<Validation>();

			var validationSearchResults = _letterContentService.ValidationSearch(filter.ValidationText, filter.NormalizedId, filter.ExcludeValidations, filter.MemoId);
			result = BuildValidations(validationSearchResults);

			return Content(ComUtil.JsonEncodeCamelCase(result), "application/json");
		}

		#endregion

		#region ModelBuilders

		#region VM to DTO

		private Core.Models.LetterContent.LetterContentDTO BuildLetterContentDTO(Models.NoteManager.LetterContent letterContent)
		{
			var result = new Core.Models.LetterContent.LetterContentDTO()
			{
				Id = letterContent.Id,
				Description = letterContent.Description,
				Name = letterContent.Name,
				Optional = letterContent.Optional,
				OrderId = letterContent.OrderId,
				Jurisdictions = BuildJurisdictionDTOs(letterContent.JurisdictionIds),
				Manufacturers = BuildManufacturerDTOs(letterContent.ManufacturerCodes),
				LetterTypes = SetLetterTypes(letterContent.LetterTypes),
				SpecialFlags = SetSpecialFlags(letterContent.SpecialFlags),
				TestingTypes = SetTestingTypes(letterContent.TestingTypes),
				Memos = BuildMemoDTOs(letterContent.Memos),
				AddDate = letterContent.AddDate,
				AddedBy = letterContent.AddedBy,
				EditDate = letterContent.EditDate,
				EditedBy = letterContent.EditedBy,
				LifeCycle = letterContent.LifeCycle,
				SectionId = letterContent.SectionId,
			};

			return result;
		}
		private List<JurisdictionDTO> BuildJurisdictionDTOs(List<string> jurisdictionIds, bool NotInclude = false)
		{
			var result = new List<JurisdictionDTO>();

			foreach (var jurisdictionId in jurisdictionIds)
			{
				result.Add(BuildJurisdictionDTO(jurisdictionId, NotInclude));
			}

			return result;
		}
		private JurisdictionDTO BuildJurisdictionDTO(string jurisdictionId, bool NotInclude)
		{
			var result = new JurisdictionDTO()
			{
				JurisdictionId = int.Parse(jurisdictionId),
				NotInclude = NotInclude
			};

			return result;
		}
		private List<ManufacturerDTO> BuildManufacturerDTOs(List<string> manufacturerCodes, bool NotInclude = false)
		{
			var result = new List<ManufacturerDTO>();

			foreach (var manufacturerCode in manufacturerCodes)
			{
				result.Add(BuildManufacturerDTO(manufacturerCode, NotInclude));
			}

			return result;
		}
		private ManufacturerDTO BuildManufacturerDTO(string manufacturerCode, bool NotInclude)
		{
			var result = new ManufacturerDTO()
			{
				ManufacturerId = manufacturerCode,
				NotInclude = NotInclude
			};

			return result;
		}
		private GLI.EF.LetterContent.LetterTypes SetLetterTypes(List<string> letterTypes)
		{
			var selectedTypes = new GLI.EF.LetterContent.LetterTypes();

			foreach (var letterType in letterTypes)
			{
				Enum.TryParse(letterType, out GLI.EF.LetterContent.LetterTypes tempEnumValue);
				selectedTypes |= tempEnumValue;
			}

			return selectedTypes;
		}
		private GLI.EF.LetterContent.SpecialFlags SetSpecialFlags(List<string> specialFlags)
		{
			var selectedSpecialFlags = new GLI.EF.LetterContent.SpecialFlags();

			foreach (var flag in specialFlags)
			{
				Enum.TryParse(flag, out GLI.EF.LetterContent.SpecialFlags tempEnumValue);
				selectedSpecialFlags |= tempEnumValue;
			}

			return selectedSpecialFlags;
		}
		private GLI.EF.LetterContent.TestingTypes SetTestingTypes(List<string> testingTypes)
		{
			var selectedTestingTypes = new GLI.EF.LetterContent.TestingTypes();

			foreach (var flag in testingTypes)
			{
				Enum.TryParse(flag, out GLI.EF.LetterContent.TestingTypes tempEnumValue);
				selectedTestingTypes |= tempEnumValue;
			}

			return selectedTestingTypes;
		}
		private VersionDTO BuildVersionDTO(Models.NoteManager.Version version)
		{
			var result = new VersionDTO();

			result = new VersionDTO
			{
				AddDate = version.AddDate,
				CreatedBy = version.CreatedBy,
				Lifecycle = (Lifecycle)version.Lifecycle,
				DeprecateDate = version.DeprecateDate,
				DeprecatedBy = version.DeprecatedBy,
				OriginalId = version.OriginalId
			};

			return result;
		}
		private List<MemoDTO> BuildMemoDTOs(List<Memo> memos)
		{
			var result = new List<MemoDTO>();

			foreach (var memo in memos)
			{
				result.Add(BuildMemoDTO(memo));
			}

			return result;
		}
		private MemoDTO BuildMemoDTO(Memo memo)
		{
			var result = new MemoDTO()
			{
				Id = memo.Id,
				Content = memo.Content,
				Language = memo.LanguageId,
				OrderId = memo.OrderId,
				BusinessLogicExpression = memo.BusinessLogicExpression,
				Description = memo.Description,
				Jurisdictions = BuildJurisdictionDTOs(memo.JurisdictionIds, false),
				JurisdictionsNotIncluded = BuildJurisdictionDTOs(memo.JurisdictionIdsNotIncluded, true),
				LetterContentId = memo.LetterContentId,
				LocalId = memo.LocalId,
				Manufacturers = BuildManufacturerDTOs(memo.ManufacturerCodes, false),
				ManufacturersNotIncluded = BuildManufacturerDTOs(memo.ManufacturerCodesNotIncluded, true),
				Name = memo.Name,
				SecondaryLanguage = memo.SecondaryLanguageId,
				SecondaryLanguageContent = memo.SecondaryLanguageContent,
				Source = memo.SourceId,
				SourceProperty = memo.SourceProperty,
				SpecialFlags = SetSpecialFlags(memo.SpecialFlags),
				Questions = BuildQuestionDTOs(memo.Questions),
				Version = BuildVersionDTO(memo.Version),
				IdNumberLinks = BuildIdNumberLinkDTOs(memo.IDNumbers),
				GameNameLinks = BuildGameNameLinkDTOs(memo.GameNames),
				Platforms = BuildPlatformDTOs(memo.PlatformIds),
				Functions = BuildFunctionDTOs(memo.FunctionIds),
				Validations = BuildValidationDTOs(memo.Validations),
				SubTypes = BuildSubTypeLinkDTOs(memo.SubTypeIds),
				TestingTypes = BuildTestTypeLinkDTOs(memo.TestTypeIds),
				SignatureScopes = BuildSignatureScopeDTOs(memo.SignatureScopeIds),
				ChipTypes = BuildChipTypeDTOs(memo.ChipTypeIds)
			};

			return result;
		}

		private List<ChipTypeDTO> BuildChipTypeDTOs(List<int> chipTypeIds)
		{
			var results = new List<ChipTypeDTO>();
			foreach (var chipTypeId in chipTypeIds)
			{
				results.Add(BuildChipTypeDTO(chipTypeId));
			}
			return results;
		}

		private ChipTypeDTO BuildChipTypeDTO(int chipTypeId)
		{
			return new ChipTypeDTO
			{
				ChipTypeId = chipTypeId
			};
		}

		private List<SignatureScopeDTO> BuildSignatureScopeDTOs(List<int> sigScopeIds)
		{
			var result = new List<SignatureScopeDTO>();

			foreach (var sigScopeId in sigScopeIds)
			{
				result.Add(BuildSignatureScopeDTO(sigScopeId));
			}

			return result;
		}

		private SignatureScopeDTO BuildSignatureScopeDTO(int sigScopeId)
		{
			var result = new SignatureScopeDTO
			{
				SigScopeId = sigScopeId
			};
			return result;
		}


		private List<TestingTypeLinkDTO> BuildTestTypeLinkDTOs(List<int> testTypeIds)
		{
			var result = new List<TestingTypeLinkDTO>();

			foreach (var testTypeId in testTypeIds)
			{
				result.Add(BuildTestTypeLinkDTO(testTypeId));
			}

			return result;
		}

		private TestingTypeLinkDTO BuildTestTypeLinkDTO(int testTypeId)
		{
			var result = new TestingTypeLinkDTO
			{
				TestingTypeId = testTypeId
			};
			return result;
		}

		private List<SubTypeLinkDTO> BuildSubTypeLinkDTOs(List<int> subTypeIds)
		{
			var result = new List<SubTypeLinkDTO>();

			foreach (var subTypeId in subTypeIds)
			{
				result.Add(BuildSubTypeLinkDTO(subTypeId));
			}

			return result;
		}

		private SubTypeLinkDTO BuildSubTypeLinkDTO(int subTypeId)
		{
			var result = new SubTypeLinkDTO
			{
				SubTypeId = subTypeId
			};
			return result;
		}
		private List<PlatformDTO> BuildPlatformDTOs(List<int> platformIds)
		{
			var result = new List<PlatformDTO>();
			foreach (var platformId in platformIds)
			{
				result.Add(BuildPlatformLinkDTO(platformId));
			}
			return result;
		}
		private PlatformDTO BuildPlatformLinkDTO(int platformId)
		{
			var result = new PlatformDTO
			{
				PlatformId = platformId
			};
			return result;
		}
		private List<FunctionDTO> BuildFunctionDTOs(List<int> functionIds)
		{
			var result = new List<FunctionDTO>();

			foreach (var functionId in functionIds)
			{
				result.Add(BuildFunctionLinkDTO(functionId));
			}

			return result;
		}

		private FunctionDTO BuildFunctionLinkDTO(int functionId)
		{
			var result = new FunctionDTO
			{
				FunctionId = functionId
			};
			return result;
		}

		private List<IdNumberLinksDTO> BuildIdNumberLinkDTOs(List<IdNumberCompare> idNumberList)
		{
			var result = new List<IdNumberLinksDTO>();

			foreach (var idNumber in idNumberList)
			{
				result.Add(BuildIdNumberLinkDTO(idNumber));
			}

			return result;
		}

		private IdNumberLinksDTO BuildIdNumberLinkDTO(IdNumberCompare IdNumberCompare)
		{
			var result = new IdNumberLinksDTO
			{
				IdNumber = IdNumberCompare.IdNumber.Trim(),
				CompareCriteriaId = (int)IdNumberCompare.CompareCriteriaId
			};
			return result;
		}

		private List<GameNameLinksDTO> BuildGameNameLinkDTOs(List<GameNameCompare> gameNameList)
		{
			var result = new List<GameNameLinksDTO>();

			foreach (var gameName in gameNameList)
			{
				result.Add(BuildGameNameLinkDTO(gameName));
			}

			return result;
		}

		private GameNameLinksDTO BuildGameNameLinkDTO(GameNameCompare GameNameCompare)
		{
			var result = new GameNameLinksDTO
			{
				GameName = GameNameCompare.GameName.Trim(),
				CompareCriteriaId = (int)GameNameCompare.CompareCriteriaId
			};
			return result;
		}

		private List<QuestionDTO> BuildQuestionDTOs(List<Models.NoteManager.Question> questions)
		{
			var result = new List<QuestionDTO>();

			foreach (var question in questions)
			{
				result.Add(BuildQuestionDTO(question));
			}

			return result;
		}
		private QuestionDTO BuildQuestionDTO(Models.NoteManager.Question question)
		{
			var result = new QuestionDTO
			{
				ColumnHeader = question.ColumnHeader,
				ConditionalExpression = question.ConditionalExpression,
				Id = question.Id,
				LocalId = question.LocalId,
				Name = question.Name,
				OptionDelimiter = question.OptionDelimiter,
				ParentQuestionId = question.ParentQuestionId,
				QuestionOptions = question.QuestionOptions,
				QuestionSpecialFlags = question.QuestionSpecialFlags,
				QuestionText = question.QuestionText,
				ReferenceLookup = question.ReferenceLookup,
				Source = question.Source,
				SourceProperty = question.SourceProperty,
				TokenKey = question.TokenKey,
				TokenValue = question.TokenValue,
				Type = question.Type,
				TableRows = BuildQuestionDTOTableRows(question.TableRows)
			};

			return result;
		}
		private List<List<QuestionDTO>> BuildQuestionDTOTableRows(List<List<Models.NoteManager.Question>> tableRows)
		{
			var result = new List<List<QuestionDTO>>();

			foreach (var row in tableRows)
			{
				result.Add(BuildQuestionDTOTableRow(row));
			}

			return result;
		}
		private List<QuestionDTO> BuildQuestionDTOTableRow(List<Models.NoteManager.Question> row)
		{
			var result = new List<QuestionDTO>();

			foreach (var col in row)
			{
				result.Add(BuildQuestionDTO(col));
			}

			return result;
		}

		private List<ValidationDTO> BuildValidationDTOs(List<Validation> validations)
		{
			var result = new List<ValidationDTO>();

			foreach (var validation in validations)
			{
				result.Add(BuildValidationDTO(validation));
			}

			return result;
		}

		private ValidationDTO BuildValidationDTO(Validation validation)
		{
			return new ValidationDTO
			{
				Answer = validation.Answer,
				ConditionalAnswer = validation.ConditionalAnswer,
				Id = validation.Id,
				MemoID = validation.MemoID ?? 0,
				Status = validation.Status ?? 1,
				QuestionType = validation.QuestionType,

				ValidationID = validation.ValidationID ?? Guid.NewGuid()
			};
		}
		#endregion

		#region DTO to VM
		private List<MemoLineageItem> BuildMemoLineage(List<MemoDTO> memos)
		{
			var result = new List<MemoLineageItem>();

			foreach (var item in memos)
			{
				result.Add(BuildMemoLineageItem(item));
			}

			return result.OrderByDescending(x => x.Id).ToList();
		}
		private MemoLineageItem BuildMemoLineageItem(MemoDTO memo)
		{
			var result = new MemoLineageItem
			{
				Id = memo.Id,
				OriginalId = memo.Version.OriginalId,
				DeprecatedDate = memo.Version.DeprecateDate,
				Description = memo.Description,
				Name = memo.Name
			};

			return result;
		}
		private Models.NoteManager.LetterContent BuildLetterContent(Core.Models.LetterContent.LetterContentDTO letterContentDTO)
		{
			var letterContentJurs = letterContentDTO.Jurisdictions.Select(x => x.JurisdictionId).ToList();
			var letterContentManu = letterContentDTO.Manufacturers.Select(x => x.ManufacturerId).ToList();
			List<string> specialFlags = new List<string>();
			List<string> letterTypes = new List<string>();
			List<string> testingTypes = new List<string>();

			foreach (var flag in Enum.GetValues(typeof(SpecialFlags)).Cast<SpecialFlags>())
			{
				if (letterContentDTO.SpecialFlags.HasFlag(flag) && flag != SpecialFlags.None)
				{
					specialFlags.Add(flag.ToString());
				}
			}
			foreach (var flag in Enum.GetValues(typeof(LetterTypes)).Cast<LetterTypes>())
			{
				if (letterContentDTO.LetterTypes.HasFlag(flag) && flag != LetterTypes.None)
				{
					letterTypes.Add(flag.ToString());
				}
			}
			foreach (var flag in Enum.GetValues(typeof(TestingTypes)).Cast<TestingTypes>())
			{
				if (letterContentDTO.TestingTypes.HasFlag(flag) && flag != TestingTypes.None)
				{
					testingTypes.Add(flag.ToString());
				}
			}

			var selectedJurs = _dbSubmissions.tbl_lst_fileJuris.Where(x => letterContentJurs.Contains(x.FixId))
					.Select(x => new SelectListItem { Value = x.FixId.ToString(), Text = x.Name, Selected = true }).ToList();

			var selectedManus = _dbSubmissions.tbl_lst_fileManu.Where(x => letterContentManu.Contains(x.Code))
				.Select(x => new SelectListItem { Value = x.Code, Text = x.Description, Selected = true }).ToList();

			var result = new Models.NoteManager.LetterContent()
			{
				Description = letterContentDTO.Description,
				Id = letterContentDTO.Id,
				Name = letterContentDTO.Name,
				Optional = letterContentDTO.Optional,
				OrderId = letterContentDTO.OrderId,
				JurisdictionIds = letterContentJurs.Select(x => x.ToString()).ToList(),
				Jurisdictions = selectedJurs,
				LetterTypes = letterTypes,
				ManufacturerCodes = letterContentManu.ToList(),
				Manufacturers = selectedManus,
				SpecialFlags = specialFlags,
				TestingTypes = testingTypes,
				Memos = BuildMemos(letterContentDTO.Memos),
				AddDate = letterContentDTO.AddDate,
				AddedBy = letterContentDTO.AddedBy,
				EditDate = letterContentDTO.EditDate,
				EditedBy = letterContentDTO.EditedBy,
				LifeCycle = letterContentDTO.LifeCycle,
				SectionId = letterContentDTO.SectionId,
				SelectedSection = _dbLetterContentContext.Sections.Where(x => x.Id == letterContentDTO.SectionId).Select(x => new { key = x.Name, value = x.Id }).SingleOrDefault()
			};

			return result;
		}
		private List<Memo> BuildMemos(List<MemoDTO> memoDTOs)
		{
			var result = new List<Memo>();

			foreach (var memoDTO in memoDTOs)
			{
				result.Add(BuildMemo(memoDTO));
			}

			return result;
		}
		private Memo BuildMemo(MemoDTO memoDTO)
		{
			var memoJurs = memoDTO.Jurisdictions.Select(x => x.JurisdictionId).ToList();
			var memoJursNotIncluded = memoDTO.JurisdictionsNotIncluded.Select(x => x.JurisdictionId).ToList();
			var memoManufs = memoDTO.Manufacturers.Select(x => x.ManufacturerId).ToList();
			var memoManufsNotIncluded = memoDTO.ManufacturersNotIncluded.Select(x => x.ManufacturerId).ToList();
			var memoPlatforms = memoDTO.Platforms.Select(x => x.PlatformId).ToList();
			var memoFunctions = memoDTO.Functions.Select(x => x.FunctionId).ToList();
			var memoSubTypes = memoDTO.SubTypes.Select(x => x.SubTypeId).ToList();
			var memoTestTypes = memoDTO.TestingTypes.Select(x => x.TestingTypeId).ToList();
			var memoSigScopes = memoDTO.SignatureScopes.Select(x => x.SigScopeId).ToList();

			var memoLanguage = string.Empty;
			var memoSecondaryLanguage = string.Empty;
			if (memoDTO.Language != null && memoDTO.Language != 0)
			{
				memoLanguage = _dbSubmissions.tbl_lst_Language.Where(x => x.Id == memoDTO.Language).SingleOrDefault().Language;
			}

			if (memoDTO.SecondaryLanguage != null && memoDTO.SecondaryLanguage != 0)
			{
				memoSecondaryLanguage = _dbSubmissions.tbl_lst_Language.Where(x => x.Id == memoDTO.SecondaryLanguage).SingleOrDefault().Language;
			}

			var selectedJurs = _dbSubmissions.tbl_lst_fileJuris.Where(x => memoJurs.Contains(x.FixId))
					.Select(x => new SelectListItem { Value = x.FixId.ToString(), Text = x.Name, Selected = true }).ToList();

			var selectedJursNotInclluded = _dbSubmissions.tbl_lst_fileJuris.Where(x => memoJursNotIncluded.Contains(x.FixId))
				.Select(x => new SelectListItem { Value = x.FixId.ToString(), Text = x.Name, Selected = true }).ToList();

			var selectedManus = _dbSubmissions.tbl_lst_fileManu.Where(x => memoManufs.Contains(x.Code))
				.Select(x => new SelectListItem { Value = x.Code, Text = x.Description, Selected = true }).ToList();

			var selectedManusNotIncluded = _dbSubmissions.tbl_lst_fileManu.Where(x => memoManufsNotIncluded.Contains(x.Code))
				.Select(x => new SelectListItem { Value = x.Code, Text = x.Description, Selected = true }).ToList();

			var selectedPlatforms = _dbSubmissions.tbl_lst_Platforms.Where(x => memoPlatforms.Contains(x.Id))
				.Select(x => new SelectListItem { Value = x.Id.ToString(), Text = x.Name, Selected = true }).ToList();

			var selectedFunctions = _dbSubmissions.tbl_lst_EPROMFunction.Where(x => memoFunctions.Contains(x.Id))
				.Select(x => new SelectListItem { Value = x.Id.ToString(), Text = x.Code, Selected = true }).ToList();

			var selectedSubTypes = _dbSubmissions.tbl_lst_SubmissionType.Where(x => memoSubTypes.Contains(x.Id))
				.Select(x => new SelectListItem { Value = x.Id.ToString(), Text = x.Code + "-" + x.Description, Selected = true }).ToList();

			var selectedTestTypes = _dbSubmissions.tbl_lst_TestingType.Where(x => memoTestTypes.Contains(x.Id))
			.Select(x => new SelectListItem { Value = x.Id.ToString(), Text = x.Description, Selected = true }).ToList();

			var selectedSigScopes = _dbSubmissions.tbl_lst_SignatureScope.Where(x => memoSigScopes.Contains(x.Id))
			.Select(x => new SelectListItem { Value = x.Id.ToString(), Text = x.Scope, Selected = true }).ToList();

			List<string> specialFlags = new List<string>();

			foreach (var flag in Enum.GetValues(typeof(SpecialFlags)).Cast<SpecialFlags>())
			{
				if (memoDTO.SpecialFlags.HasFlag(flag) && flag != SpecialFlags.None)
				{
					specialFlags.Add(flag.ToString());
				}
			}

			var result = new Memo
			{
				BusinessLogicExpression = memoDTO.BusinessLogicExpression,
				Content = memoDTO.Content,
				Description = memoDTO.Description,
				Id = memoDTO.Id,
				JurisdictionIds = memoJurs.Select(x => x.ToString()).ToList(),
				Jurisdictions = selectedJurs,
				JurisdictionIdsNotIncluded = memoJursNotIncluded.Select(x => x.ToString()).ToList(),
				JurisdictionsNotIncluded = selectedJursNotInclluded,
				Language = memoLanguage,
				LanguageId = memoDTO.Language,
				LetterContentId = memoDTO.LetterContentId,
				LetterContentName = memoDTO.LetterContentName,
				LocalId = memoDTO.LocalId,
				ManufacturerCodes = memoManufs.ToList(),
				Manufacturers = selectedManus,
				ManufacturerCodesNotIncluded = memoManufsNotIncluded.ToList(),
				ManufacturersNotIncluded = selectedManusNotIncluded,
				Name = memoDTO.Name,
				OrderId = memoDTO.OrderId,
				Questions = BuildQuestions(memoDTO.Questions),
				SecondaryLanguage = memoSecondaryLanguage,
				SecondaryLanguageId = memoDTO.SecondaryLanguage,
				SecondaryLanguageContent = memoDTO.SecondaryLanguageContent,
				SourceId = memoDTO.Source,
				SourceName = ParseSource(memoDTO.Source),
				SourceProperty = memoDTO.SourceProperty,
				SpecialFlags = specialFlags,
				Version = BuildVersion(memoDTO.Version),
				Validations = BuildValidations(memoDTO.Validations),
				IDNumbers = memoDTO.IdNumberLinks.Select(x => new IdNumberCompare()
				{
					IdNumber = x.IdNumber,
					CompareCriteriaId = x.CompareCriteriaId,
					SelectedCompareCriteria = _dbLetterContentContext.CompareCriterias.Where(y => y.Id == x.CompareCriteriaId).Select(y => new { key = y.Description, value = y.Id }).SingleOrDefault()
				}).ToList(),
				GameNames = memoDTO.GameNameLinks.Select(x => new GameNameCompare()
				{
					GameName = x.GameName,
					CompareCriteriaId = x.CompareCriteriaId,
					SelectedCompareCriteria = _dbLetterContentContext.CompareCriterias.Where(y => y.Id == x.CompareCriteriaId).Select(y => new { key = y.Description, value = y.Id }).SingleOrDefault()
				}).ToList(),
				PlatformIds = memoPlatforms.Select(x => (int)x).ToList(),
				Platforms = selectedPlatforms,
				FunctionIds = memoFunctions.Select(x => (int)x).ToList(),
				Functions = selectedFunctions,
				SubTypeIds = memoSubTypes,
				SubTypes = selectedSubTypes,
				TestTypeIds = memoTestTypes,
				TestTypes = selectedTestTypes,
				SignatureScopeIds = memoSigScopes,
				SignatureScopes = selectedSigScopes
			};

			return result;
		}
		private string ParseSource(int id)
		{
			var enumVal = (Source)id;
			return enumVal.ToString();
		}

		private List<Models.NoteManager.Question> BuildQuestions(List<QuestionDTO> questionDTOs)
		{
			var result = new List<Models.NoteManager.Question>();

			foreach (var questionDTO in questionDTOs)
			{
				result.Add(BuildQuestion(questionDTO));
			}

			return result;
		}
		private Models.NoteManager.Question BuildQuestion(QuestionDTO questionDTO)
		{
			var result = new Models.NoteManager.Question
			{
				ColumnHeader = questionDTO.ColumnHeader,
				ConditionalExpression = questionDTO.ConditionalExpression,
				Id = questionDTO.Id,
				LocalId = questionDTO.LocalId,
				Name = questionDTO.Name,
				OptionDelimiter = questionDTO.OptionDelimiter ?? ",",
				ParentQuestionId = questionDTO.ParentQuestionId,
				QuestionOptions = questionDTO.QuestionOptions,
				QuestionSpecialFlags = questionDTO.QuestionSpecialFlags,
				QuestionText = questionDTO.QuestionText,
				ReferenceLookup = questionDTO.ReferenceLookup,
				Source = questionDTO.Source,
				SourceProperty = questionDTO.SourceProperty,
				TableRows = BuildQuestionTableRows(questionDTO.TableRows),
				TokenKey = questionDTO.TokenKey,
				TokenValue = questionDTO.TokenValue,
				Type = questionDTO.Type,
				EditDate = questionDTO.EditDate,
				IsRequired = questionDTO.IsRequired
			};

			return result;
		}
		public List<List<Models.NoteManager.Question>> BuildQuestionTableRows(List<List<QuestionDTO>> questionDTOTableRows)
		{
			var result = new List<List<Models.NoteManager.Question>>();

			foreach (var questionDTOTableRow in questionDTOTableRows)
			{
				result.Add(BuildQuestionTableRow(questionDTOTableRow));
			}

			return result;
		}
		public List<Models.NoteManager.Question> BuildQuestionTableRow(List<QuestionDTO> questionDTOTableRow)
		{
			var result = new List<Models.NoteManager.Question>();

			foreach (var questionDTO in questionDTOTableRow)
			{
				result.Add(BuildQuestion(questionDTO));
			}

			return result;
		}
		public Models.NoteManager.Version BuildVersion(VersionDTO versionDTO)
		{
			var result = new Models.NoteManager.Version
			{
				AddDate = versionDTO.AddDate,
				CreatedBy = versionDTO.CreatedBy,
				DeprecateDate = versionDTO.DeprecateDate,
				DeprecatedBy = versionDTO.DeprecatedBy,
				Lifecycle = (int)versionDTO.Lifecycle,
				OriginalId = versionDTO.OriginalId
			};

			return result;
		}

		public List<Validation> BuildValidations(List<ValidationDTO> validations)
		{
			var result = new List<Validation>();

			foreach (var validation in validations)
			{
				result.Add(BuildValidation(validation));
			}

			return result;
		}

		public Validation BuildValidation(ValidationDTO validation)
		{
			return new Validation
			{
				Answer = validation.Answer,
				ConditionalAnswer = validation.ConditionalAnswer,
				Id = validation.Id,
				MemoID = validation.MemoID,
				Status = validation.Status,
				ValidationID = validation.ValidationID,
				Jurisdictions = validation.Jurisdictions.Select(x => new ValidationJurisdiction { Id = x.Key, Name = x.Value }).ToList(),
				NormalizedID = validation.NormalizedID,
				ValidationText = validation.ValidationText,
				QuestionType = validation.QuestionType
			};
		}

		public List<Validation> BuildValidations(List<ValidationsSearchDTO> validations)
		{
			var result = new List<Validation>();

			var distinctJurisdId = validations.SelectMany(x => x.Jurisdictions).Distinct().ToList();
			var localJurisdictionLookup = _dbSubmissions.tbl_lst_fileJuris.Where(x => distinctJurisdId.Contains(x.FixId)).ToDictionary(x => (int)x.FixId, x => x.Name);

			foreach (var validation in validations)
			{
				var jurSet = localJurisdictionLookup.Where(x => validation.Jurisdictions.Contains(x.Key)).Select(x => new ValidationJurisdiction { Id = x.Key, Name = x.Value }).ToList();
				if (jurSet.Count > 0)
					result.Add(BuildValidation(validation, jurSet));
				else
					result.Add(BuildValidation(validation));
			}

			return result;
		}

		public Validation BuildValidation(ValidationsSearchDTO validation)
		{
			return new Validation
			{
				Answer = string.Empty,
				Id = null,
				MemoID = null,
				NormalizedID = validation.NormalizedID,
				Status = validation.ValidationStatus,
				StatusText = string.Empty,
				ValidationID = validation.ValidationID,
				ValidationText = validation.ValidationText,
				QuestionType = validation.QuestionType
			};
		}

		public Validation BuildValidation(ValidationsSearchDTO validation, List<ValidationJurisdiction> jurisdictionSet)
		{
			var result = BuildValidation(validation);
			result.Jurisdictions = jurisdictionSet;

			return result;
		}

		#endregion

		#endregion

		#region BuildMockMemoData
		private List<Core.Models.DTO.SubmissionDTO> BuildMockDataComponents(int projectId, int limiter = 5)
		{
			var jurisdictionalDatas = _dbSubmissions.trProjectJurisdictionalData.Where(x => x.ProjectId == projectId).Select(x => x.JurisdictionalData);
			var components = jurisdictionalDatas.GroupBy(x => x.SubmissionId)
							.Select(jd => new Core.Models.DTO.SubmissionDTO
							{
								Id = (int)jd.Key,
								IdNumber = jd.FirstOrDefault().Submission.IdNumber,
								Version = jd.FirstOrDefault().Submission.Version,
								DateCode = jd.FirstOrDefault().Submission.DateCode,
								GameName = jd.FirstOrDefault().Submission.GameName,
								OrgIGTMaterial = jd.FirstOrDefault().Submission.OrgIGTMaterial,
								ManufacturerBuildId = jd.FirstOrDefault().Submission.ManufacturerBuildId,
								ChipType = jd.FirstOrDefault().Submission.ChipType,
								Function = jd.FirstOrDefault().Submission.Function,
								Position = jd.FirstOrDefault().Submission.Position,
								JurisdictionalData = jd.Select(x => new Core.Models.DTO.JurisdictionalDataDTO
								{
									Id = x.Id,
									JurisdictionName = x.JurisdictionName,
									JurisdictionId = x.JurisdictionId,
									CertNumbers = x.JurisdictionalDataCertificationNumbers.Select(jdcn => jdcn.CertificationNumber.CertNumber).ToList()
								}).Take(5).ToList()
							}).Take(5).ToList();

			components.ForEach(x => x.SignatureData = new List<Core.Models.SubmissionService.SignatureDTO>());

			return components;
		}
		private List<Models.Generate.Helper.CertificationIssuer> BuildMockDataCertificationIssuers()
		{
			return _dbSubmissions.CertificationIssuer.Select(x => new Models.Generate.Helper.CertificationIssuer
			{
				FirstName = x.FirstName,
				LastName = x.LastName,
				MiddleInital = x.MiddleInitial,
				Title = x.Title,
				UserId = x.UserId,
				Signature = new CertificationIssuerSignature
				{
					Base64Image = x.Signature,
					FileExtension = x.SignatureFileExtension
				},
				PresidesOver = x.Laboratories.Select(y => y.Location).ToList()
			})
			.ToList();
		}
		private List<MathCoversheetDTO> BuildMockDataPaytables(string fileNumber, int limiter = 5)
		{
			var result = _letterContentService
				.GetMathCoversheetsByFileNumber(fileNumber, limiter);

			return result;
		}
		private List<GamingGuideline> BuildMockDataGamingGuidelines(List<Core.Models.DTO.SubmissionDTO> components)
		{
			var jurisdictionIds = components.SelectMany(x => x.JurisdictionalData)
				.Select(y => y.JurisdictionId)
				.Distinct()
				.ToList();

			return _dbSubmissions.tbl_lst_GamingGuidelines
				.Where(x => jurisdictionIds.Contains(x.JurisdictionId))
				.Distinct()
				.Select(x => new GamingGuideline()
				{
					JurisdictionId = x.Jurisdiction.FixId,
					RTPRequirements = x.GamingGuidelineRTPRequirements
					.Select(y => new RTPRequirement
					{
						Type = y.GameType.Name,
						RTP = y.RTP,
						Symbol = y.DenomSymbol,
						RTPType = ((GamingGuidelineRTPType)y.RTPType).ToString()
					}).ToList()
				})
				.ToList();
		}
		private List<ClauseReportViewModel> BuildMockDataClauseReports(int projectId, int limiter = 5)
		{
			var result = new List<ClauseReportViewModel>();

			var clauseReports = GetClauseReportsForProject(projectId).Take(limiter).ToList();
			result = AddCCDocuments(clauseReports);

			return result;
		}
		private IList<ClauseReportModel> GetClauseReportsForProject(int projectId)
		{
			try
			{
				var clauses = _evolutionService.GetProjectClauseReports(projectId);
				return clauses;
			}
			catch (Exception ex)
			{
				var copy = ex;
				return new List<ClauseReportModel>(); //so app can be loaded with 0 clauses and for dev test clauses can be displayed
			}
		}
		private List<ClauseReportViewModel> AddCCDocuments(IList<ClauseReportModel> clauseReportModels)
		{
			var result = new List<ClauseReportViewModel>();

			foreach (var clauseGroup in clauseReportModels.GroupBy(cr => cr.JurisdictionId))
			{
				result.Add(new ClauseReportViewModel()
				{
					ClauseReports = clauseGroup.ToList(),
					PassCount = clauseGroup.DistinctBy(c => c.ClauseID).Count(x => x.Answer == "PASS"),
					FailCount = clauseGroup.DistinctBy(c => c.ClauseID).Count(x => x.Answer == "FAIL"),
					NACount = clauseGroup.DistinctBy(c => c.ClauseID).Count(x => x.Answer == "NA"),
					NAStarCount = clauseGroup.DistinctBy(c => c.ClauseID).Count(x => x.Answer == "NA*"),
					JurisdictionId = clauseGroup.Key
				});
			}

			return result;
		}
		private MockMemoData BuildMockMemoData(int bundleId, int limiter = 5)
		{
			var result = new MockMemoData();

			var bundleData = _dbSubmissions.QABundles
				.Where(x => x.Id == bundleId)
				.Select(x => new
				{
					ProjectId = x.Project.Id,
					BundleName = x.ProjectName,
					SubmissionsInfo = x.Project.JurisdictionalData.Select(y => new { ManufacturerCode = y.JurisdictionalData.Submission.Manufacturer.Code, ManufacturerName = y.JurisdictionalData.Submission.Manufacturer.Description, y.JurisdictionalData.Submission.FileNumber }).FirstOrDefault()
				}).First();

			result.CertificationIssuers = BuildMockDataCertificationIssuers();
			result.Components = BuildMockDataComponents(bundleData.ProjectId, limiter);
			//result.ClauseReports = BuildMockDataClauseReports(bundleData.ProjectId, limiter);
			result.Paytables = BuildMockDataPaytables(bundleData.SubmissionsInfo.FileNumber, limiter);
			result.GamingGuidelines = BuildMockDataGamingGuidelines(result.Components);
			result.BundleName = bundleData.BundleName;
			result.ManufacturerId = bundleData.SubmissionsInfo.ManufacturerCode;
			result.ManufacturerName = bundleData.SubmissionsInfo.ManufacturerName;
			result.FileNumber = bundleData.SubmissionsInfo.FileNumber;
			result.JIRAInformation = new GenerateJIRAInformationViewModel()
			{
				RevocationInformations = new List<GenerateRevocationInformationViewModel>()
				{
					new GenerateRevocationInformationViewModel
				{
					JurisdictionID = "22",
					SubmissionID = result.Components.First().Id.ToString(),
					RevocationPeriod = 30,
					RevocationWording = "Arizona: Revocation wording",
					SubmissionIdNumber = result.Components.First().IdNumber,
					SubmissionGameName = result.Components.First().GameName,
					SubmissionVersion = result.Components.First().Version,
					SubmissionDateCode = result.Components.First().DateCode,
					SubmissionItemsBeingRevoked =
						new List<GenerateSubmissionItemsBeingRevoked>(){
							new GenerateSubmissionItemsBeingRevoked
							{
								Id = 1,
								DateCode = "DateCode1",
								GameName = "GameName1",
								Version = "Version1",
								IdNumber ="ImBeingRevoked1"
							},
							new GenerateSubmissionItemsBeingRevoked
							{
								Id = 2,
								DateCode = "DateCode2",
								GameName = "GameName2",
								Version = "Version2",
								IdNumber ="ImBeingRevoked2"
							},
						}
				},
					new GenerateRevocationInformationViewModel
				{
					JurisdictionID = "22",
					SubmissionID = result.Components[1].Id.ToString(),
					RevocationPeriod = 30,
					RevocationWording = "Arizona: Revocation wording",
					SubmissionIdNumber = result.Components.First().IdNumber,
					SubmissionGameName = result.Components.First().GameName,
					SubmissionVersion = result.Components.First().Version,
					SubmissionDateCode = result.Components.First().DateCode,
					SubmissionItemsBeingRevoked =
						new List<GenerateSubmissionItemsBeingRevoked>(){
							new GenerateSubmissionItemsBeingRevoked
							{
								Id = 1,
								DateCode = "DateCode1",
								GameName = "GameName1",
								Version = "Version1",
								IdNumber ="ImBeingRevoked1"
							},
							new GenerateSubmissionItemsBeingRevoked
							{
								Id = 2,
								DateCode = "DateCode2",
								GameName = "GameName2",
								Version = "Version2",
								IdNumber ="ImBeingRevoked2"
							},
						}
				},
					new GenerateRevocationInformationViewModel
				{
					JurisdictionID = "73",
					SubmissionID = result.Components.First().Id.ToString(),
					RevocationPeriod = 15,
					RevocationWording = "California revocation wording",
					SubmissionIdNumber = result.Components.First().IdNumber,
					SubmissionGameName = result.Components.First().GameName,
					SubmissionVersion = result.Components.First().Version,
					SubmissionDateCode = result.Components.First().DateCode,
					SubmissionItemsBeingRevoked =
						new List<GenerateSubmissionItemsBeingRevoked>(){
							new GenerateSubmissionItemsBeingRevoked
							{
								Id = 1,
								DateCode = "DateCode1",
								GameName = "GameName1",
								Version = "Version1",
								IdNumber ="ImBeingRevoked1"
							},
							new GenerateSubmissionItemsBeingRevoked
							{
								Id = 2,
								DateCode = "DateCode2",
								GameName = "GameName2",
								Version = "Version2",
								IdNumber ="ImBeingRevoked2"
							},
						}
				}
				},
				DisclosureInformations = new List<GenerateDisclosureInformationViewModel>()
				{
					new GenerateDisclosureInformationViewModel
					{
						DisclosureWording = "Disclosure Wording For Arizona",
						JurisdictionID = "22",
						SubmissionID = "1"
					},
					new GenerateDisclosureInformationViewModel
					{
						DisclosureWording = "Disclosure Wording For California",
						JurisdictionID = "73",
						SubmissionID = "2"
					},
						new GenerateDisclosureInformationViewModel
					{
						DisclosureWording = "Disclosure Wording For California number 2",
						JurisdictionID = "73",
						SubmissionID = "2"
					}
				}
			};
			result.SpecializedJurisdictionClauseReports = new List<GenerateSpecializedJurisdictionClauseReportViewModel>()
			{
				new GenerateSpecializedJurisdictionClauseReportViewModel
				{
					JurisdictionID = 394,
					JurisdictionName = "Nevada ITL",
					EvaluatedClauses = new List<GenerateSpeacalizedJurisdcictionEvaluatedViewModel>()
					{
						new GenerateSpeacalizedJurisdcictionEvaluatedViewModel
						{
							Name = "Clause ONE",
							Result = "PASS"
						}
						,
						new GenerateSpeacalizedJurisdcictionEvaluatedViewModel
						{
							Name = "Clause TWO",
							Result = "FAIL"
						},
						new GenerateSpeacalizedJurisdcictionEvaluatedViewModel
						{
							Name = "Clause THREE",
							Result = "N/A"
						}
					},
					TestCases = new List<GenerateSpeacalizedJurisdcictionTestCaseViewModel>()
					{
						new GenerateSpeacalizedJurisdcictionTestCaseViewModel()
						{
							Name = "Test Case ONE",
							Version = "1.0.0"
						},
						new GenerateSpeacalizedJurisdcictionTestCaseViewModel()
						{
							Name = "Test Case Two",
							Version = "1.0.0(a)"
						}
					}
				},
				new GenerateSpecializedJurisdictionClauseReportViewModel
				{
					JurisdictionID = 59,
					JurisdictionName = "Peru",
					TestCases = new List<GenerateSpeacalizedJurisdcictionTestCaseViewModel>()
					{
						new GenerateSpeacalizedJurisdcictionTestCaseViewModel()
						{
							Name = "Test Case ONE",
							Version = "1.0.0"
						},
						new GenerateSpeacalizedJurisdcictionTestCaseViewModel()
						{
							Name = "Test Case Two",
							Version = "1.0.0(a)"
						}
					}
				},
			};
			result.JurisdictionData = new List<GenerateJurisdictionModel>() {
				new GenerateJurisdictionModel(){
					JurisdictionId = 38,
					EvalLabLetterCodes = new List<String> { "GL1", "GL5-1", "GLI-1", "GLI-2", "GLI-3"}
				}
			};
			result.OCUITs = new List<GenerateOCUITComponentViewModel>() {
				new GenerateOCUITComponentViewModel()
				{
					 Id = 1,
					 SubmissionId = 11111,
					 FileNumber = "Mo-22-IGT-17-01",
					 Description = "Test",
					 IdNumber = "Test Id",
					 NVLabNumber = "s2-234",
					 JurisdictionIds = new List<string>{"22","394"}
				},
				new GenerateOCUITComponentViewModel()
				{
					 Id = 2,
					 SubmissionId = 11112,
					 FileNumber = "Mo-22-IGT-17-02",
					 Description = "Test 2",
					 IdNumber = "Test Id 2",
					 NVLabNumber = "s2-235",
					 JurisdictionIds = new List<string>{"73","394"}
				}
			};
			return result;
		}
		#endregion


		private List<KeyValuePair<string, int>> BuildCompareCriterias()
		{
			var result = new List<KeyValuePair<string, int>>();
			var criterias = _dbLetterContentContext.CompareCriterias.Select(x => new { key = x.Description, value = x.Id, Order = x.Order }).OrderBy(x => x.Order).ToList();
			foreach (var criteria in criterias)
			{
				result.Add(new KeyValuePair<string, int>(criteria.key, criteria.value));
			}
			return result;
		}

		private List<KeyValuePair<string, int>> BuildLetterSections()
		{
			var result = new List<KeyValuePair<string, int>>();
			var sections = _dbLetterContentContext.Sections.Select(x => new { key = x.Name, value = x.Id }).ToList();
			foreach (var section in sections)
			{
				result.Add(new KeyValuePair<string, int>(section.key, section.value));
			}

			return result;
		}

		private List<Memo> LoadTestData()
		{
			var result = new List<Memo>();

			result.AddRange(
				new List<Memo> {
					new Memo
					{
						Id = 1,
						LocalId = Guid.NewGuid(),
						Name = "Header_ProductDetails",
						Content = "<div style=\"width:100%;\"> <p class=MsoNormal style='text-align:justify;background:#B8CCE4'><b><span lang=EN-US style='font-size:12.0pt;font-family: \"Calibri\",sans-serif;color:black'>Product Details</span></b></p> <p class=MsoNormal style='text-align:justify'><span lang=EN-CA style='font-size:12.0pt;font-family:\"Calibri\",sans-serif'>&nbsp;</span></p></div>",
						Description = "A header for the product details section.",
					},
					new Memo
					{
						Id = 2,
						LocalId = Guid.NewGuid(),
						Name="Table_EnvironmentTestedTable",
						Content="<div style=\"width:100%;\"><p class=MsoNormal style='text-align:justify'><span lang=EN-US style='font-size:12.0pt;font-family:\"Calibri\",sans-serif'>&nbsp;</span></p><p class=MsoNormal style='text-align:justify'><span lang=EN-US style='font-size:12.0pt;font-family:\"Calibri\",sans-serif'>The game<span style='color:red'> </span> was tested in an environment consisting of the following components: </span></p><p class=MsoNormal style='text-align:justify'><span lang=EN-US style='font-size:9.0pt;font-family:\"Calibri\",sans-serif'>&nbsp;</span></p><div align=center><table class=MsoNormalTable border=1 cellspacing=0 cellpadding=0 style='width:100%;border-collapse:collapse;border:none'><tr style='height:14.4pt'><td colspan=3 style=\"width:100%;border:solid windowtext 1.0pt; background:#E5E5E5;padding:0cm 5.4pt 0cm 5.4pt;height:14.4pt\"><p class=MsoNormal align=center style='text-align:center'><b><span lang=EN-US style='font-size:12.0pt;font-family:\"Calibri\",sans-serif'>Client-Side Details</span></b></p></td></tr><tr style='height:14.4pt'><td style=\"width:50%;border:solid windowtext 1.0pt;border-top: none;padding:0cm 5.4pt 0cm 5.4pt;height:14.4pt\"><p class=MsoNormal><span lang=EN-US style='font-size:12.0pt;font-family:\"Calibri\",sans-serif'>Delivery Mechanism(s):</span></p></td><td colspan=2 style=\"width:50%;border-top:none;border-left: none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt; padding:0cm 5.4pt 0cm 5.4pt;height:14.4pt\"><p class=MsoNormal><span lang=EN-US style='font-size:12.0pt;font-family:\"Calibri\",sans-serif'>{{deliveryMechanismsQuestion}}</span></p></td></tr><tr  style='height:14.4pt'><td style=\"width:50%;border:solid windowtext 1.0pt;border-top: none;padding:0cm 5.4pt 0cm 5.4pt;height:14.4pt\"><p class=MsoNormal><span lang=EN-US style='font-size:12.0pt;font-family:\"Calibri\",sans-serif'>Windows Client Installer Package:</span></p></td><td colspan=2 style=\"width:50%;border-top:none;border-left: none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt; padding:0cm 5.4pt 0cm 5.4pt;height:14.4pt\"><p class=MsoNormal><span lang=EN-US style='font-size:12.0pt;font-family:\"Calibri\",sans-serif'>{{windowsClientInstallerPackage}}</span></p></td></tr><tr style='height:14.4pt'><td style=\"width:50%;border:solid windowtext 1.0pt;border-top: none;padding:0cm 5.4pt 0cm 5.4pt;height:14.4pt\"><p class=MsoNormal><span lang=EN-US style='font-size:12.0pt;font-family:\"Calibri\",sans-serif'>Mac Client Installer Package:</span></p></td><td colspan=2 style=\"width:50%;border-top:none;border-left: none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt; padding:0cm 5.4pt 0cm 5.4pt;height:14.4pt\"><p class=MsoNormal><span lang=EN-US style='font-size:12.0pt;font-family:\"Calibri\",sans-serif'>{{macClientInstallerPackage}}</span></p></td></tr><tr style='height:14.4pt'><td style=\"width:50%;border:solid windowtext 1.0pt;border-top: none;padding:0cm 5.4pt 0cm 5.4pt;height:14.4pt\"><p class=MsoNormal><span lang=EN-US style='font-size:12.0pt;font-family:\"Calibri\",sans-serif'>Mobile Client Installer Package:</span></p></td><td colspan=2 style=\"width:50%;border-top:none;border-left: none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt; padding:0cm 5.4pt 0cm 5.4pt;height:14.4pt\"><p class=MsoNormal><span lang=EN-US style='font-size:12.0pt;font-family:\"Calibri\",sans-serif'>{{mobileClientInstallerPackage}}</span></p></td></tr><tr style='height:14.4pt'><td style=\"width:50%;border:solid windowtext 1.0pt;border-top: none;padding:0cm 5.4pt 0cm 5.4pt;height:14.4pt\"><p class=MsoNormal><span lang=EN-US style='font-size:12.0pt;font-family:\"Calibri\",sans-serif'>Delivery Mechanism Type(s):</span></p></td><td colspan=2 style=\"width:50%;border-top:none;border-left: none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt; padding:0cm 5.4pt 0cm 5.4pt;height:14.4pt\"><p class=MsoNormal><span lang=EN-US style='font-size:12.0pt;font-family:\"Calibri\",sans-serif'>{{deliveryMechanismTypes}}</span></p></td></tr><tr style='height:14.4pt'><td colspan=3 style=\"width:100%;border:solid windowtext 1.0pt; border-top:none;background:#E5E5E5;padding:0cm 5.4pt 0cm 5.4pt;height:14.4pt\"><p class=MsoNormal align=center style='text-align:center'><b><span lang=EN-US style='font-size:12.0pt;font-family:\"Calibri\",sans-serif; color:black'>Testing Details</span></b></p></td></tr><tr style='height:14.4pt'><td style=\"width:50%;border:solid windowtext 1.0pt;border-top: none;padding:0cm 5.4pt 0cm 5.4pt;height:14.4pt\"><p class=MsoNormal><span lang=EN-US style='font-size:12.0pt;font-family:\"Calibri\",sans-serif'>Platform(s) and Version(s) Tested With:</span></p></td><td colspan=2 style=\"width:50%;border-top:none;border-left: none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt; padding:0cm 5.4pt 0cm 5.4pt;height:14.4pt\"><p class=MsoNormal><span v-for=\"row in platformsAndVersionsTestedwithTable\" style='font-size:12.0pt;font-family:\"Calibri\",sans-serif'> {{row.platformsAndVersionsTestedwithDetail}} <br /></span></p></td></tr><tr style='height:14.4pt'><td style=\"width:50%;border:solid windowtext 1.0pt;border-top: none;padding:0cm 5.4pt 0cm 5.4pt;height:14.4pt\"><p class=MsoNormal><span lang=EN-US style='font-size:12.0pt;font-family:\"Calibri\",sans-serif'>Browser(s) and Version(s) Tested With:</span></p></td><td colspan=2 style=\"width:50%;border-top:none;border-left: none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt; padding:0cm 5.4pt 0cm 5.4pt;height:14.4pt\"><p class=MsoNormal><span v-for=\"row in browserVersionTestedWithTable\" style='font-size:12.0pt;font-family:\"Calibri\",sans-serif'> {{ (row.platformsAndVersionsTestedwithBrowser ? row.platformsAndVersionsTestedwithBrowser : ' ' ) + ' ' + (row.platformsAndVersionsTestedwithBrowserVersion ? row.platformsAndVersionsTestedwithBrowserVersion : '') }} <br /></span></p></td></tr><tr style='height:14.4pt'><td style=\"width:50%;border:solid windowtext 1.0pt;border-top: none;padding:0cm 5.4pt 0cm 5.4pt;height:14.4pt\"><p class=MsoNormal><span lang=EN-US style='font-size:12.0pt;font-family:\"Calibri\",sans-serif'>Flash Version(s) Tested With:</span></p></td><td colspan=2 style=\"width:50%;border-top:none;border-left: none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt; padding:0cm 5.4pt 0cm 5.4pt;height:14.4pt\"><p class=MsoNormal><span v-for=\"row in adobeFlashTestedTable\" style='font-size:12.0pt;font-family:\"Calibri\",sans-serif'> {{ (row.adobeFlashVersionTestedWithQuestionDetail ? row.adobeFlashVersionTestedWithQuestionDetail : ' ' ) }} <br /></span></p></td></tr><tr style='height:14.4pt'><td <td style=\"width:50%;border:solid windowtext 1.0pt;border-top: none;padding:0cm 5.4pt 0cm 5.4pt;height:14.4pt\"><p class=MsoNormal><span lang=EN-US style='font-size:12.0pt;font-family:\"Calibri\",sans-serif'>Operating System(s) with Version:</span></p></td><td colspan=2 style=\"width:50%;border-top:none;border-left: none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt; padding:0cm 5.4pt 0cm 5.4pt;height:14.4pt\"><p class=MsoNormal><span v-for=\"row in operatingSystemVersionTestedWithTable\" style='font-size:12.0pt;font-family:\"Calibri\",sans-serif'> {{ (row.operatingSystemVersionTestedWithDetail ? row.operatingSystemVersionTestedWithDetail : ' ' ) + ' ' + (row.operatingSystemVersionTestedWithDetailVersion ? row.operatingSystemVersionTestedWithDetailVersion : '') }} <br /></span></p></td></tr><tr style='height:14.4pt'><td style=\"width:50%;border:solid windowtext 1.0pt;border-top: none;padding:0cm 5.4pt 0cm 5.4pt;height:14.4pt\"><p class=MsoNormal><span lang=EN-US style='font-size:12.0pt;font-family:\"Calibri\",sans-serif'>Mobile Device(s):</span></p></td><td colspan=\"1\" style=\"width:50%;border-top:none;border-left:none; border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt; padding:0cm 5.4pt 0cm 5.4pt;height:14.4pt\"><p class=MsoNormal><span v-for=\"row in mobileDevicesTestedTable\" style='font-size:12.0pt;font-family:\"Calibri\",sans-serif'> {{ (row.mobileDevicesTestedMake ? row.mobileDevicesTestedMake : ' ' ) + ' ' + (row.mobileDevicesTestedModel ? row.mobileDevicesTestedModel : '') }} <br /></span></p></td><td colspan=\"2\" style=\"width:50%;border-top:none;border-left:none; border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt; padding:0cm 5.4pt 0cm 5.4pt;height:14.4pt\"><p class=MsoNormal><span v-for=\"row in mobileDevicesTestedTable\" style='font-size:12.0pt;font-family:\"Calibri\",sans-serif'> {{ ' Model: ' + (row.mobileDevicesTestedModelNumber ? row.mobileDevicesTestedModelNumber : '') }} <br /></span></p></td></tr><tr style='height:14.4pt'><td style=\"width:50%;border:solid windowtext 1.0pt;border-top: none;padding:0cm 5.4pt 0cm 5.4pt;height:14.4pt\"><p class=MsoNormal><span lang=EN-US style='font-size:12.0pt;font-family:\"Calibri\",sans-serif'>Tablet Device(s):</span></p></td><td colspan=\"1\" style=\"width:50%;border-top:none;border-left:none; border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt; padding:0cm 5.4pt 0cm 5.4pt;height:14.4pt\"><p class=MsoNormal><span v-for=\"row in tabletDevicesTestedTable\" style='font-size:12.0pt;font-family:\"Calibri\",sans-serif'> {{ (row.tabletDevicesTestedMake ? row.tabletDevicesTestedMake : ' ' ) + ' ' + (row.tabletDevicesTestedModel ? row.tabletDevicesTestedModel : '')}} <br /></span></p></td><td colspan=\"2\" style=\"width:50%;border-top:none;border-left:none; border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt; padding:0cm 5.4pt 0cm 5.4pt;height:14.4pt\"><p class=MsoNormal><span v-for=\"row in tabletDevicesTestedTable\" style='font-size:12.0pt;font-family:\"Calibri\",sans-serif'> {{ ' Model: ' + (row.tabletDevicesTestedModelNumber ? row.tabletDevicesTestedModelNumber : '') }} <br /></span></p></td></tr><tr style='height:14.4pt'><td colspan=3 style=\"width:100%;border:solid windowtext 1.0pt; border-top:none;background:#E5E5E5;padding:0cm 5.4pt 0cm 5.4pt;height:14.4pt\"><p class=MsoNormal><span lang=EN-US style='font-size:12.0pt;font-family:\"Calibri\",sans-serif'>&nbsp;</span></p></td></tr></table></div><p class=MsoNormal style='text-align:justify'><span lang=EN-CA style='font-size:12.0pt;font-family:\"Calibri\",sans-serif;'>&nbsp;</span></p></div>",
						Description = "The table holds the information about the various environment variables used during testing. Things such as browser name and version.",
					}
				});

			return result;
		}
	}
}