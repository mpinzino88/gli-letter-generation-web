using EF.Submission;
using GLI.EF.LetterContent;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Submissions.Common;
using Submissions.Core.Interfaces.LetterContent;
using Submissions.Web.Areas.Documents.Models.LetterQuestion;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web.Mvc;
using InputType = GLI.EF.LetterContent.InputType;

namespace Submissions.Web.Areas.Documents.Controllers
{
	[AuthorizeUser(Permission = Permission.NoteManager, HasAccessRight = AccessRight.Read)]
	public class LetterQuestionController : Controller
	{
		#region Private Members
		private readonly ILetterContentService _letterContentService;
		private readonly GLI.EF.LetterContent.ILetterContentContext _letterContentContext;
		private readonly ISubmissionContext _dbSubmissions;
		private readonly IUserContext _user;

		#endregion

		public LetterQuestionController(ILetterContentService letterContentService, ILetterContentContext letterContentContext, ISubmissionContext submissionContext, IUserContext user)
		{
			_letterContentService = letterContentService;
			_letterContentContext = letterContentContext;
			_dbSubmissions = submissionContext;
			_user = user;
		}

		public async Task<ActionResult> Index()
		{
			var questions = await _letterContentContext.Questions
				.Where(q => !q.DeactivatedDate.HasValue)
				.Select(LetterQuestionIndexListItem.Projection)
				.ToListAsync();

			var model = new LetterQuestionIndexViewModel
			{
				Questions = questions
			};

			return View(model);
		}

		[HttpPost]
		public async Task<ActionResult> Deactivate(int id)
		{
			try
			{
				var match = await _letterContentContext.Questions.FindAsync(id);
				if (match.Memos.Any())
				{
					// Client-side checks should prevent attached questions from
					// being deleted, but the model is not updated in real-time.
					// This prevents possible conflicts from those timing issues.
					return new HttpStatusCodeResult(HttpStatusCode.Conflict);
				}
				match.DeactivatedDate = DateTime.Now;
				match.DeactivatedBy = _user.User.Id;
				_letterContentContext.SaveChanges();
				return new HttpStatusCodeResult(HttpStatusCode.OK);
			}
			catch
			{
				return new HttpStatusCodeResult(HttpStatusCode.InternalServerError);
			}
		}

		[HttpGet]
		public async Task<ActionResult> Edit(int? id)
		{
			var model = new LetterQuestionEditViewModelContainer
			{
				Question = new LetterQuestionEditViewModel
				{
					QuestionText = "New Question"
				},
				IsNewQuestion = true,
				IsAttached = false,
				LookupTypes = EnumToDictionary<LookupAjax>(),
				Sources = EnumToDictionary<Source>(),
				InputTypes = EnumToDictionary<InputType>(),
			};
			model.Questions = _letterContentContext.Questions.Select(x => new { x.Id, x.Name }).ToList().Select(x => new KeyValuePair<string, int>(x.Name, x.Id)).ToDictionary(t => t.Value, t => t.Key);


			if (id != null)
			{
				var match = await _letterContentContext.Questions.FindAsync(id);
				if (match == null)
				{
					return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
				}
				model.Question = new LetterQuestionEditViewModel(match);
				model.IsNewQuestion = false;
				model.IsAttached = match.Memos.Any();
			}

			return View(model);
		}

		[HttpPost]
		public async Task<ActionResult> Edit(LetterQuestionEditViewModelContainer vm)
		{
			try
			{
				// Ensure that token key has correct formatting, and apply prefix
				var tokenKey = VariableConverter.ToCamelCase(vm.Question.TokenKey);
				if (!tokenKey.StartsWith("_token"))
				{
					tokenKey = $"token_{tokenKey}";
				}
				vm.Question.TokenKey = tokenKey;

				var question = new Question
				{
					Name = vm.Question.Name,
					QuestionSpecialFlags = vm.Question.QuestionSpecialFlags,
					QuestionText = vm.Question.QuestionText,
					Source = (Source)vm.Question.SourceId,
					Type = (InputType)vm.Question.TypeId,
					ReferenceLookup = vm.Question.ReferenceLookup,
					TokenKey = vm.Question.TokenKey,
					OptionDelimiter = vm.Question.OptionDelimiter?.Value,
					DefaultValue = vm.Question.DefaultValue,
					IsRequired = vm.Question.IsRequired,
					ParentQuestionId = vm.Question.ParentQuestionId,
					ConditionalExpression = vm.Question.ConditionalExpression
				};

				if (vm.Question.QuestionOptions != null)
				{
					// Storing QuestionOptions as a string is a bit of a mess
					var req = Request.InputStream;
					req.Seek(0, SeekOrigin.Begin);
					var jsonString = await new StreamReader(req).ReadToEndAsync();
					var json = JObject.Parse(jsonString);
					question.QuestionOptions = json.SelectToken("question.questionOptions").ToString(Formatting.None);
				}

				var isNew = !vm.Question.Id.HasValue;

				if (isNew)
				{
					question.AddDate = DateTime.Now;
					question.AddedBy = _user.User.Id;
				}
				else
				{
					question.Id = vm.Question.Id.Value;
					question.EditDate = DateTime.Now;
					question.EditedBy = _user.User.Id;
				}

				_letterContentContext.Questions.Attach(question);
				var qEntity = _letterContentContext.Entry(question);
				qEntity.State = isNew ? EntityState.Added : EntityState.Modified;

				if (!isNew)
				{
					qEntity.Property(q => q.Name).IsModified = true;
					qEntity.Property(q => q.QuestionSpecialFlags).IsModified = true;
					qEntity.Property(q => q.QuestionText).IsModified = true;
					qEntity.Property(q => q.Source).IsModified = true;
					qEntity.Property(q => q.Type).IsModified = true;
					qEntity.Property(q => q.TokenKey).IsModified = true;
					qEntity.Property(q => q.EditDate).IsModified = true;
					qEntity.Property(q => q.EditedBy).IsModified = true;
					qEntity.Property(q => q.AddDate).IsModified = false;
					qEntity.Property(q => q.AddedBy).IsModified = false;
					qEntity.Property(q => q.QuestionOptions).IsModified = true;
					qEntity.Property(q => q.DefaultValue).IsModified = true;
					qEntity.Property(q => q.IsRequired).IsModified = true;
					qEntity.Property(q => q.ParentQuestionId).IsModified = true;
					qEntity.Property(q => q.ConditionalExpression).IsModified = true;
				}

				_letterContentContext.SaveChanges();

				return new HttpStatusCodeResult(HttpStatusCode.OK);
			}
			catch
			{
				return new HttpStatusCodeResult(HttpStatusCode.InternalServerError);
			}
		}

		#region Private Classes & Methods

		private Dictionary<int, string> EnumToDictionary<T>() where T : struct, IConvertible
		{
			var returnPairs = new Dictionary<int, string>();
			foreach (T enumType in Enum.GetValues(typeof(T)))
			{
				returnPairs.Add(Convert.ToInt32(enumType), enumType.ToString());
			}
			return returnPairs;
		}

		// TODO: is there a better place to put this?
		// Slugify class modified from https://stackoverflow.com/a/19937132/1074198
		private class VariableConverter
		{
			// white space, em-dash, en-dash, underscore
			static readonly Regex WordDelimiters = new Regex(@"[\s—–_]", RegexOptions.Compiled);

			// characters that are not valid
			static readonly Regex InvalidChars = new Regex(@"[^a-z0-9_]", RegexOptions.Compiled);

			// multiple hyphens
			static readonly Regex MultipleUnderscores = new Regex(@"_{2,}", RegexOptions.Compiled);

			public static string ToCamelCase(string value)
			{
				// convert to lower case
				value = value.ToLowerInvariant();

				// remove diacritics (accents)
				value = RemoveDiacritics(value);

				// ensure all word delimiters are underscores
				value = WordDelimiters.Replace(value, "_");

				// strip out invalid characters
				value = InvalidChars.Replace(value, "");

				// replace multiple underscores (_) with a single underscore
				value = MultipleUnderscores.Replace(value, "_");

				// trim underscores (_) from ends
				return value.Trim('_');
			}

			/// See: http://www.siao2.com/2007/05/14/2629747.aspx
			private static string RemoveDiacritics(string stIn)
			{
				string stFormD = stIn.Normalize(NormalizationForm.FormD);
				var sb = new StringBuilder();

				for (int ich = 0; ich < stFormD.Length; ich++)
				{
					var uc = CharUnicodeInfo.GetUnicodeCategory(stFormD[ich]);
					if (uc != UnicodeCategory.NonSpacingMark)
					{
						sb.Append(stFormD[ich]);
					}
				}

				return (sb.ToString().Normalize(NormalizationForm.FormC));
			}
		}

		#endregion
	}
}