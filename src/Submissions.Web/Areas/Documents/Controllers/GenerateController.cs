using AutoMapper.QueryableExtensions;
using EF.eResultsManaged;
using EF.Submission;
using EvoLogic.GatherData;
using GLI.EF.LetterContent;
using GLIMathOperations.Interfaces;
using iText.Html2pdf;
using iText.Html2pdf.Attach.Impl;
using iText.Kernel.Pdf;
using iText.Layout.Font;
using iText.StyledXmlParser.Css.Media;
using Newtonsoft.Json;
using Submissions.Common;
using Submissions.Core;
using Submissions.Core.Interfaces.LetterContent;
using Submissions.Core.Interfaces.LetterGen;
using Submissions.Core.Models;
using Submissions.Core.Models.DTO;
using Submissions.Core.Models.LetterContent;
using Submissions.Core.Models.LetterGen;
using Submissions.Core.Models.SubmissionService;
using Submissions.Web.Areas.Documents.Models.Coversheet;
using Submissions.Web.Areas.Documents.Models.Generate;
using Submissions.Web.Areas.Documents.Models.Generate.Helper;
using Submissions.Web.Areas.Evolution.Models;
using Submissions.Web.Models.Query;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using LetterGenMode = Submissions.Common.LetterGenMode;
using MemoDTO = Submissions.Core.Models.LetterContent.MemoDTO;

namespace Submissions.Web.Areas.Documents.Controllers
{
	[AuthorizeUser(Permission = Permission.LetterGeneration, HasAccessRight = AccessRight.Read)]
	public class GenerateController : Controller
	{

		private readonly ISubmissionContext _dbSubmission;
		private readonly ISubmissionService _submissionService;
		private readonly IeResultsManagedContext _dbeResultsManaged;
		private readonly IConfig _config;
		private readonly IGenerateService _generateService;
		private readonly IEvolutionService _evolutionService;
		private readonly ILetterContentService _letterContentService;
		private readonly ICoversheetGatherMethods _coverSheetGatherMethods;
		private readonly ILetterGenerationService _letterGenerationService;
		private readonly InstanceCompGroupMethods _instanceCompGroupMethods;
		private readonly IUserContext _userContext;
		private readonly IMasterFileService _masterFileService;
		private readonly ISignatureService _signatureService;
		private readonly ILetterContentContext _dbLetterContent;
		private readonly IQueryService _queryService;
		private readonly ILogger _logger;
		private readonly IProjectService _projectService;


		private List<EF.Submission.tbl_lst_Language> _availableLanguages;
		private List<string> _availableJurisdictions;
		private List<trLogin> _trLogins;
		public GenerateController(IGenerateService generateService,
			ISubmissionContext dbSubmission,
			ISubmissionService submissionService,
			IeResultsManagedContext dbeResultsManaged,
			IEvolutionService evolutionService,
			ILetterContentService letterContentService,
			IUserContext userContext,
			ISignatureService signatureService,
			IMasterFileService masterFileService,
			ICoversheetGatherMethods coverSheetGatherMethods,
			IConfig config,
			ILetterContentContext dbLetterContent,
			IQueryService queryService,
			ILetterGenerationService letterGenerationService,
			ILogger logger,
			IProjectService projectService
		)
		{
			_dbSubmission = dbSubmission;
			_submissionService = submissionService;
			_dbeResultsManaged = dbeResultsManaged;
			_generateService = generateService;
			_evolutionService = evolutionService;
			_letterContentService = letterContentService;
			_userContext = userContext;
			_signatureService = signatureService;
			_masterFileService = masterFileService;
			_instanceCompGroupMethods = new InstanceCompGroupMethods(_dbeResultsManaged, _dbSubmission);
			_dbLetterContent = dbLetterContent;
			_coverSheetGatherMethods = coverSheetGatherMethods;
			_config = config;
			_queryService = queryService;
			_letterGenerationService = letterGenerationService;
			_logger = logger;
			_projectService = projectService;
		}

		#region Experimental
		public ActionResult Experimental(int bundleId, DateTime? letterDate, LetterGenMode mode = LetterGenMode.ALL)
		{
			//var letterDate = new DateTime(2020, 9, 30);
			//var letterGenerationInstance = _letterGenerationService.GetLetterGenerationInstance(bundleId, mode, new LetterGenerationPermissionSet() { EditLetterGenSubissionSignature = Util.HasAccess(Permission.LetterGenSubSig, AccessRight.All), LetterGenDetail = Util.HasAccess(Permission.LetterGenDetail, AccessRight.All) }, letterDate);
			//var vm = BuildGenerateIndexViewModel(letterGenerationInstance);
			return RedirectToAction("Index", new { bundleId = bundleId, mode = mode, letterDate = letterDate });
		}
		#endregion

		public ActionResult Index(int bundleId, LetterGenMode? mode, DateTime? letterDate)
		{
			var locMode = mode ?? LetterGenMode.ALL;
			var vm = GeneratePDFVM(bundleId, locMode, letterDate);
			return View(vm);
		}

		private GenerateIndexViewModel GeneratePDFVM(int bundleId, LetterGenMode mode, DateTime? letterDate)
		{
			_availableLanguages = _dbSubmission.tbl_lst_Language.ToList();
			var projectHolder = _dbSubmission.QABundles.Where(x => x.Id == bundleId).Single().Project.Holder;
			var permissionSet = new LetterGenerationPermissionSet()
			{
				EditLetterGenSubmissionSignature = Util.HasAccess(Permission.LetterGenSubSig, AccessRight.All),
				LetterGenDetail = Util.HasAccess(Permission.LetterGenDetail, AccessRight.All),
				EditLetterGen = Util.HasAccess(Permission.LetterGeneration, AccessRight.Edit),
				AnswerLockEngaged = projectHolder.ToUpper().StartsWith("QA"),
				EditCustomMemosInQA = _userContext.Department == Department.QA
			};

			var letterGenerationInstance = _letterGenerationService.GetLetterGenerationInstance(bundleId, mode, permissionSet, true, letterDate);
			var problemSection = letterGenerationInstance.Letter.Sections.FirstOrDefault(sec => sec.Id == 7);
			if (problemSection != null)
			{
				problemSection.LetterContents.RemoveAll(lc => lc.Id == 211);
			}
			letterGenerationInstance.Letter.Sections = letterGenerationInstance.Letter.Sections.Where(x => x.Name != "PDF").ToList();

			//load if not imported
			LoadAnswersFromNewSubmission(letterGenerationInstance.Bundle.ProjectId);

			var vm2 = BuildGenerateIndexViewModel(letterGenerationInstance);

			//Uncomment this to get a letter with just the Paytables note. Works with BundleId 345001 with SubmissionsContext pointing to production
			//var targetSection = vm2.Letter.Sections.FirstOrDefault(section => section.Id ==12);
			//var mathTable = targetSection.Memos.FirstOrDefault(memo => memo.Id == 1217);
			//mathTable.SourceId = "Paytables";
			//mathTable.LetterContent = "<div><table><thead><tr height=50 style=\"background-color:#e5e5e5;\"><th width=100>Paytable ID</th><th width=100>Game Name</th><th width=100>Denom</th><th width=100>Max Bet</th><th width=150 v-show=\"letterPreviewJurisdiction !== 508\">Supplier's RTP %</th><th width=150 v-show=\"letterPreviewJurisdiction !== 508\">GLI's RTP %</th><th width=150 v-show=\"letterPreviewJurisdiction !== 508\">On Screen RTP %</th><th width=100 v-show=\"letterPreviewJurisdiction === 50 || letterPreviewJurisdiction === 231\">Weighted Average</th><th width=100 v-show=\"letterPreviewJurisdiction === 508\">RTP with Increment rate</th><th width=100 v-show=\"letterPreviewJurisdiction === 508\">RTP without Increment rate</th><th width=100 v-show=\"letterPreviewJurisdiction === 508\">Worst Odds</th></tr></thead><tbody><tr v-for=\"item in sourceItems\"><td>{{item.paytableId}}</td><td>{{item.themeName}}</td><td>{{item.riDenom}}</td><td>{{item.maxBet}}</td><td v-show=\"letterPreviewJurisdiction !== 508\"/><td v-show=\"letterPreviewJurisdiction !== 508\">{{item.minReturn}} - {{item.maxReturn}}</td><td v-show=\"letterPreviewJurisdiction !== 508\"/><td v-show=\"letterPreviewJurisdiction === 50 || letterPreviewJurisdiction === 231\">{{item.deAverage}}</td><td v-show=\"letterPreviewJurisdiction === 508\">{{item.minReturn}} - {{item.maxReturn}}</td><td v-show=\"letterPreviewJurisdiction === 508\">{{item.minReturnExcludingProgressive}} - {{item.maxReturnExcludingProgressive}}</td><td v-show=\"letterPreviewJurisdiction === 508\">{{item.paWorstOdds}}</td></tr></tbody></table><br><div v-show=\"letterPreviewJurisdiction !== 0 && !isNaN(letterPreviewJurisdiction)\"><div v-for=\"item in sourceItems\"><div v-if=\"isRTPInRange(item)\">                Paytable: {{item.paytableId}} should be within {{isRTPInRange(item).min/100}} - {{isRTPInRange(item).max/100}}            </div></div></div></div>";
			//targetSection.Memos = new List<GenerateLetterMemo>() { mathTable };
			//vm2.Letter.Sections = new List<GenerateLetterSection>() { targetSection };


			//Uncomment this to get a letter with just the game description. Works with BundleId 345001 with SubmissionsContext pointing to production
			//var targetSection = vm2.Letter.Sections.FirstOrDefault(section => section.Id == 9);
			//var gameDesc = targetSection.Memos.FirstOrDefault(memo => memo.Id == 1095);
			//gameDesc.SourceId = "Components";
			//gameDesc.LetterContent = "<div v-for=\"item in sourceItems\" v-html=\"item.token_game_description_test_question\"></div>";
			//gameDesc.LetterContentControls[0].Splittable = true;
			//targetSection.Memos = new List<GenerateLetterMemo>() { gameDesc };
			//vm2.Letter.Sections = new List<GenerateLetterSection>() { targetSection };

			//var targetSection = vm2.Letter.Sections.FirstOrDefault(x => x.Id == 9);
			//vm2.Letter.Sections = new List<GenerateLetterSection>() { targetSection };
			//vm2.Letter.Sections.FirstOrDefault(x => x.Id == 9).Memos.Last().OrderId = 302;


			return vm2;
		}

		private List<int> JurisdictionsWithNoContent(IList<GenerateLetterSection> sections, List<int> jurisdictionIds)
		{
			var memos = sections.SelectMany(sec => sec.Memos);
			var ret = new List<int>();
			foreach (var jur in jurisdictionIds)
			{
				if (!memos.Any(memo => memo.ApplicableJurisdictions.Count == 0 || memo.ApplicableJurisdictions.Contains(jur)))
				{
					ret.Add(jur);
				}
			}
			return ret;
		}

		[HttpPost]
		public JsonResult SaveUserColumnSettings(string userColumnSettings)
		{
			var settingsDTO = new UserColumnSettingsDTO
			{
				UserId = _userContext.User.Id,
				SourceTable = ColumnChooserTables.LetterGenSubmissions,
				ColumnSettings = userColumnSettings
			};
			_submissionService.SaveUserColumnPreferences(settingsDTO);

			return Json(new { }, JsonRequestBehavior.AllowGet);
		}

		[HttpGet]
		public JsonResult GetUserColumnSettings() // Doenst need to be a Get, but not sure where to throw it into the vm 
		{
			return Json(_submissionService.GetColumnSettings(ColumnChooserTables.LetterGenSubmissions, _userContext.User.Id), JsonRequestBehavior.AllowGet);
		}

		private GenerateLetterMemo ConvertMemoDTO(MemoDTO memo, bool custom, decimal? customMemoOrder = 0, int anchorMemoId = 0)
		{
			var applicableJurs = new List<int>();
			if (memo.JurisdictionsNotIncluded.Any())
			{
				if (_availableJurisdictions == null)
				{
					_availableJurisdictions = _dbSubmission.tbl_lst_fileJuris.Select(x => x.Id).ToList();
				}
				var jurIds = _availableJurisdictions.Select(x => int.Parse(x)).ToList();
				applicableJurs = jurIds.Where(x => !memo.JurisdictionsNotIncluded.Select(m => m.JurisdictionId).ToList().Contains(x)).Select(x => x).ToList();
			}
			else
			{
				applicableJurs = memo.Jurisdictions.Select(x => x.JurisdictionId).ToList();
			}

			var memoLocalId = Guid.NewGuid();
			var newCustomMemo = new GenerateLetterMemo()
			{
				AnchorMemoId = anchorMemoId,
				ApplicableJurisdictions = applicableJurs,
				ApplicableManufacturers = memo.Manufacturers.Select(x => x.ManufacturerId).ToList(),
				BusinessLogicExpression = memo.BusinessLogicExpression,
				Custom = custom,
				DefaultInclude = true,
				Description = memo.Description,
				Id = memo.Id,
				Include = true,
				LetterContent = custom ? "<div v-html='custom'></div>" : memo.Content,
				SecondaryLanguageLetterContent = custom ? "<div v-html='secondarycustom'></div>" : memo.SecondaryLanguageContent,
				SecondaryLanguageId = custom ? 0 : memo.SecondaryLanguage,
				LetterContentControls = memo.Questions.Select(y => new GenerateLetterInputControl
				{
					ApplicableJurisdictions = applicableJurs,
					ColumnHeader = y.ColumnHeader,
					ConditionalExpression = y.ConditionalExpression,
					Content = custom ? "<div>" + y.TokenValue + "</div>" : y.TokenValue,
					SecondaryContent = custom ? "<div>" + y.SecondaryTokenValue + "</div>" : y.TokenValue,
					SecondaryLanguage = y.SecondaryLanguage,
					Description = y.QuestionText,
					Label = custom ? "custom" : y.TokenKey,
					Id = y.Id != 0 ? y.Id : memo.Id,
					LocalId = Guid.NewGuid(),
					OptionDelimiter = y.OptionDelimiter,
					Options = y.QuestionOptions != null ?
					  new JavaScriptSerializer().Deserialize<List<SelectListItem>>(y.QuestionOptions) : new List<SelectListItem>(),
					SourceId = "",
					SourceProperty = "",
					Name = y.Name,
					//TableRows = y.TableRows,
					MemoLocalId = memoLocalId,
					ReferenceLookup = y.ReferenceLookup,
					Type = y.Type,
					IsRequired = y.IsRequired,
					ParentId = y.ParentQuestionId != null ? (int)y.ParentQuestionId : 0
				}).ToList(),
				Name = memo.Name,
				WorkSpaceCategory = memo.Section?.Name,
				LocalId = memoLocalId,
				//SourceId = "Components",
				Paging = applicableJurs.Select(x => new GenerateLetterPaging
				{
					JurisdictionId = x,
					LocalId = memoLocalId,
					Page = 1,
					IsLandscape = false
				}).ToList(),
				OrderId = (decimal)(custom ? customMemoOrder : memo.OrderId ?? 0)
			};
			return newCustomMemo;
		}

		[HttpPost]
		public ContentResult SavePaytables(List<MathCoversheetDTO> paytables)
		{
			_letterGenerationService.SaveMathCoverSheetData(paytables, _userContext.User.Id);
			return Content(ComUtil.JsonEncodeCamelCase(""), "application/json");
		}

		#region Review This
		// Review This
		[HttpPost]
		public JsonResult Save(ProjectLetterDTO projectLetter)
		{
			projectLetter.ModifiedDate = DateTime.UtcNow;
			projectLetter.UserId = _userContext.User.Id;

			_letterContentService.SaveProjectLetter(projectLetter);

			return Json(new { }, JsonRequestBehavior.AllowGet);
		}
		// Review This
		[HttpPost]
		public JsonResult SaveAnswer(AnswerDTO answer)
		{
			answer.UserId = _userContext.User.Id;
			answer.AnswerDate = DateTime.UtcNow;
			answer.Id = _letterContentService.SaveProjectAnswerNew(answer);
			return Json(new { answerId = answer.Id, savedOn = answer.AnswerDate, savedBy = _userContext.User.Name }, JsonRequestBehavior.AllowGet);
		}


		[HttpPost, ValidateInput(false)]
		public JsonResult FinalizeAnswers(string documents, int projectId, int mode)
		{
			try
			{
				if (mode == 0) //0- Letter Gen, 1-Evo
				{
					_letterContentService.SetAnswerStateActive(projectId, DateTime.UtcNow, _userContext.User.Id);
				}

				SaveToGPSQueue(documents, projectId, mode);
				return Json(new { Result = "OK" }, JsonRequestBehavior.AllowGet);
			}
			catch (Exception ex)
			{
				_logger.LogError(ex);
				return Json(new { Result = "Error" }, JsonRequestBehavior.AllowGet);
			}
		}

		private void SaveToGPSQueue(string documents, int projectId, int mode)
		{
			List<LetterGenLettersToGPSQueue> lettersToAdd = new List<LetterGenLettersToGPSQueue>();
			var docs = JsonConvert.DeserializeObject<GeneratedLetterDTO[]>(documents);

			var pendingAnswersJurisdictions = _dbSubmission.trProjectJurisdictionalData.Where(x => x.ProjectId == projectId).Select(x => (int)x.JurisdictionalData.Jurisdiction.FixId).ToList();
			foreach (var document in docs.Where(x => pendingAnswersJurisdictions.Contains(x.JurisdictionId)))
			{
				var html = ReplaceStylesheets(document.HTML);
				var bytes = System.Text.Encoding.UTF8.GetBytes(html);
				lettersToAdd.Add(new LetterGenLettersToGPSQueue()
				{
					ProjectId = projectId,
					LetterHtml = bytes,
					JurisdictionFixId = document.JurisdictionId,
					JurisdictionId = document.JurisdictionId.ToJurisdictionIdString(),
					CreatedBy = _userContext.User.Id,
					CreateDate = DateTime.Now,
					Status = LetterGenQueueStatus.PENDING.ToString(),
					Language = document.Language,
					LetterType = (mode == (int)LetterGenMode.EVOLUTION) ? "Back" : "" //for now just as string, we will see if more types, we will put enum
				}); ;
			}

			_dbSubmission.LetterGenLettersToGPSQueue.AddRange(lettersToAdd);
			_dbSubmission.SaveChanges();
		}


		// Review This
		[HttpGet]
		public JsonResult GetAnswer(int questionId, string jurisdictionalDataIds)
		{
			if (jurisdictionalDataIds == null)
			{
				return Json(null, JsonRequestBehavior.AllowGet);
			}
			var value = _letterContentService.LoadAnswerNew(questionId, JsonConvert.DeserializeObject<List<int>>(jurisdictionalDataIds));
			return Json(value, JsonRequestBehavior.AllowGet);
		}

		// Review This
		[HttpGet]
		public ActionResult LoadApplicableTemplates(string jurisdictionIds, string manufacturerId)
		{
			var Ids = JsonConvert.DeserializeObject<int[]>(jurisdictionIds);
			var existing = _dbeResultsManaged.ClauseTemplates
				.Where(template => !template.Inactive && template.ManufacturerId == manufacturerId && Ids.Contains(template.JurisdictionId))
				.Select(template => new { template.Id, template.Label, template.JurisdictionId })
				.ToList();
			var items = existing.Select(template => new
			{
				Value = template.Id.ToString(),
				Text = string.Format("{0} ({1})", template.Label, template.JurisdictionId),
				template.JurisdictionId
			}).ToList();
			return Content(ComUtil.JsonEncodeCamelCase(items), "application/json");
		}
		// Review This
		[HttpGet]
		public ActionResult GetTemplate(int templateId)
		{
			var dbTemplate = _dbeResultsManaged.ClauseTemplateExplanations
				.Where(explanation => explanation.ClauseTemplateId == templateId && explanation.Inactive != true)
				.Select(explanation => new
				{
					explanation.ClauseId,
					explanation.FailExplanation,
					explanation.PassExplanation,
					explanation.NAExplanation,
					explanation.NAStarExplanation,
					explanation.ExplanationLanguage
				}).ToList();

			var explanations = dbTemplate.Where(x => x.ExplanationLanguage == (int)ClauseTemplateExplanationLanguage.English).ToList();

			var nativeExplanations = dbTemplate.Where(x => x.ExplanationLanguage == (int)ClauseTemplateExplanationLanguage.Native).ToList();

			var result = new { explanations, nativeExplanations };

			return Content(ComUtil.JsonEncodeCamelCase(result), "application/json");
		}
		// Review This
		[HttpGet]
		public JsonResult GetLetterContactData(int letterContactId)
		{
			var contact = _dbSubmission.tbl_lst_LetterContacts
				.Find(letterContactId);

			var result = contact != null ? new
			{
				AddressLines = new List<string>
				{
					contact.AddressLine1 ?? "",
					contact.AddressLine2 ?? "",
					contact.AddressLine3 ?? "",
					contact.AddressLine4 ?? "",
					contact.AddressLine5 ?? "",
					contact.AddressLine6 ?? ""
				},
				contact.Name
			} : null;
			return Json(result, JsonRequestBehavior.AllowGet);
		}
		public ContentResult SaveEvoExplanations(int projectId, string clauseReports)
		{
			try
			{
				var clausesVM = JsonConvert.DeserializeObject<List<ClauseReportModel>>(clauseReports);
				_evolutionService.SaveProjectClauseExplanations(projectId, clausesVM, _userContext.Location);
				return Content("OK");
			}
			catch
			{
				return Content("Error");
			}
		}
		// Review These

		[HttpPost]
		public JsonResult SaveCustomMemo(CustomMemoDTO customMemo)
		{
			var savedCustomMemoId = _letterContentService.SaveCustomMemo(customMemo);
			return Json(ComUtil.JsonEncodeCamelCase(savedCustomMemoId), JsonRequestBehavior.AllowGet);
		}

		[HttpPost]
		public JsonResult DeleteCustomMemo(int memoId)
		{
			_letterContentService.DeleteMemo(memoId);
			return Json(new { msg = "Memo has been deleted" }, JsonRequestBehavior.AllowGet);
		}

		[HttpGet]
		public JsonResult GetMemoAssociations(int memoId)
		{
			var results = _letterContentService.BuildMemoAssociation(memoId);
			return Json(ComUtil.JsonEncodeCamelCase(results), JsonRequestBehavior.AllowGet);
		}

		#region Components
		public IList<SubmissionDTO> BuildComponents(int bundleId)
		{
			var projectInfo = _dbSubmission.QABundles.Where(x => x.Id == bundleId).Select(x => new { x.Project.FileNumber, x.ProjectId, x.Project.ENCompleteDate }).SingleOrDefault();
			var projectId = projectInfo.ProjectId;

			var jurisdictionalDatas = _dbSubmission.trProjectJurisdictionalData.Where(x => x.ProjectId == projectId && x.JurisdictionalData.Status != JurisdictionStatus.RJ.ToString() && x.JurisdictionalData.Status != JurisdictionStatus.WD.ToString()).Select(x => x.JurisdictionalData);
			var startDate = _dbSubmission.trTasks.Where(x => x.ProjectId == projectId && x.TaskDefinitionId != (int)ProtrackReviewTask.CPOutGoingDynamics && x.StartDate != null).OrderBy(x => x.StartDate).FirstOrDefault()?.StartDate;
			if ((startDate is null || startDate > projectInfo.ENCompleteDate) && projectInfo.ENCompleteDate != null) // This means no Task exists and its split or Task was added after RC -> RR
			{
				var mainProjectId = _dbSubmission.trProjects.Where(x => x.FileNumber == projectInfo.FileNumber && x.IsTransfer == false).Select(x => x.Id).SingleOrDefault();
				startDate = _dbSubmission.trTasks.Where(x => x.ProjectId == mainProjectId).OrderBy(x => x.StartDate).FirstOrDefault()?.StartDate;
			}

			var components = jurisdictionalDatas.GroupBy(x => x.SubmissionId)
							.Select(jd => new SubmissionDTO
							{
								Id = (int)jd.Key,
								FileNumber = jd.FirstOrDefault().Submission.FileNumber,
								IdNumber = jd.FirstOrDefault().Submission.IdNumber,
								Version = jd.FirstOrDefault().Submission.Version,
								DateCode = jd.FirstOrDefault().Submission.DateCode,
								GameName = jd.FirstOrDefault().Submission.GameName,
								OrgIGTMaterial = jd.FirstOrDefault().Submission.OrgIGTMaterial,
								ManufacturerBuildId = jd.FirstOrDefault().Submission.ManufacturerBuildId,
								ChipType = jd.FirstOrDefault().Submission.ChipType,
								Function = jd.FirstOrDefault().Submission.Function,
								Position = jd.FirstOrDefault().Submission.Position,
								SecondaryFunction = jd.FirstOrDefault().Submission.SecondaryFunction.Name,
								JurisdictionalData = jd.Select(x => new JurisdictionalDataDTO
								{
									Id = x.Id,
									JurisdictionName = x.JurisdictionName,
									JurisdictionId = x.JurisdictionId,
									ClientNumber = x.ClientNumber,
									LetterNumber = x.LetterNumber,
									CertNumbers = x.JurisdictionalDataCertificationNumbers.Select(jdcn => jdcn.CertificationNumber.CertNumber).ToList()
								}).ToList(),
								SubmitDate = (DateTime)jd.FirstOrDefault().Submission.SubmitDate,
								TaskStartDate = startDate,// LETAUT-865, i think this is correct,
								PurchaseOrder = jd.FirstOrDefault().PurchaseOrder,
								TestingTypeId = jd.FirstOrDefault().Submission.TestingTypeId ?? 0
							}).ToList();

			var mergedProjects = _projectService.GetAllMergedProjectIds(projectId);
			var mergedJurisdictionalDatas = _dbSubmission.trProjectJurisdictionalData.Where(x => mergedProjects.Contains(x.ProjectId) && x.JurisdictionalData.Status != JurisdictionStatus.RJ.ToString() && x.JurisdictionalData.Status != JurisdictionStatus.WD.ToString()).Select(x => x.JurisdictionalData);
			var mergedComponents = mergedJurisdictionalDatas.GroupBy(x => x.SubmissionId)
							.Select(jd => new SubmissionDTO
							{
								Id = (int)jd.Key,
								FileNumber = jd.FirstOrDefault().Submission.FileNumber,
								IdNumber = jd.FirstOrDefault().Submission.IdNumber,
								Version = jd.FirstOrDefault().Submission.Version,
								DateCode = jd.FirstOrDefault().Submission.DateCode,
								GameName = jd.FirstOrDefault().Submission.GameName,
								OrgIGTMaterial = jd.FirstOrDefault().Submission.OrgIGTMaterial,
								ManufacturerBuildId = jd.FirstOrDefault().Submission.ManufacturerBuildId,
								ChipType = jd.FirstOrDefault().Submission.ChipType,
								Function = jd.FirstOrDefault().Submission.Function,
								Position = jd.FirstOrDefault().Submission.Position,
								SecondaryFunction = jd.FirstOrDefault().Submission.SecondaryFunction.Name,
								JurisdictionalData = jd.Select(x => new JurisdictionalDataDTO
								{
									Id = x.Id,
									JurisdictionName = x.JurisdictionName,
									JurisdictionId = x.JurisdictionId,
									ClientNumber = x.ClientNumber,
									LetterNumber = x.LetterNumber,
									CertNumbers = x.JurisdictionalDataCertificationNumbers.Select(jdcn => jdcn.CertificationNumber.CertNumber).ToList()
								}).ToList(),
								SubmitDate = (DateTime)jd.FirstOrDefault().Submission.SubmitDate,
								TaskStartDate = startDate,// LETAUT-865, i think this is correct
								SubmissionFromMerged = true,
								PurchaseOrder = jd.FirstOrDefault().PurchaseOrder,
								TestingTypeId = jd.FirstOrDefault().Submission.TestingTypeId ?? 0
							}).ToList();
			components.AddRange(mergedComponents);

			components.ForEach(x => x.SignatureData = new List<SignatureDTO>());

			return components;
		}
		[HttpGet]
		public ActionResult GetComponentData(int bundleId)
		{
			return Json(ComUtil.JsonEncodeCamelCase(BuildComponents(bundleId)), JsonRequestBehavior.AllowGet);
		}
		[HttpGet]
		public ActionResult GetSubmissionSignatureData(string submissionIds)
		{
			var componentIds = JsonConvert.DeserializeObject<int[]>(submissionIds);
			var signatures = _dbSubmission.tblSignatures
							.Where(x => componentIds.Any(id => id == x.SubmissionId))
							.Select(x => new SignatureDTO
							{
								Id = x.Id,
								SubmissionId = x.SubmissionId.Value,
								Type = x.Type,
								Seed = x.Seed,
								VersionId = x.VersionId,
								Scope = x.Scope,
								FilePath = x.FilePath,
								Signature = x.Signature,
								AlphaVersion = x.ComponentID,
								TypeId = x.TypeOfId,
								ScopeId = x.ScopeOfId,
								Version = x.VerifyVersion
							}).ToList();

			var signatureData = signatures
								.GroupBy(x => x.SubmissionId)
								.Select(y => new
								{
									SubmissionId = (int)y.Key,
									Signatures = y.ToList()
								});

			var finalData = ComUtil.JsonEncodeCamelCase(signatureData);

			return Content(finalData, "application/json");

		}
		[HttpGet]
		public ActionResult SearchComponents(string searchVM)
		{
			var filter = JsonConvert.DeserializeObject<SubmissionSearch>(searchVM);
			if (!string.IsNullOrEmpty(filter.FileNumber))
			{
				filter.SingleInputSearch = filter.FileNumber;
			}
			var results = _queryService.SubmissionSearch(filter).ToList().AsQueryable().ProjectTo<SubmissionDTO>();

			return Json(ComUtil.JsonEncodeCamelCase(results), JsonRequestBehavior.AllowGet);
		}
		[HttpPost]
		public ActionResult UpdateComponent(SubmissionDTO row)
		{
			var component = _dbSubmission.Submissions.Find(row.Id);
			component.IdNumber = row.IdNumber;
			component.Version = row.Version;
			component.DateCode = row.DateCode;
			component.GameName = row.GameName;
			return Json(_dbSubmission.SaveChanges() > 0 ? true : false);
		}
		[HttpGet]
		public JsonResult GetSignatureData(int submissionId)
		{
			return Json(ComUtil.JsonEncodeCamelCase(_signatureService.GetSignaturesFromSubmissionId(submissionId)),
				JsonRequestBehavior.AllowGet);
		}
		[HttpPost]
		public JsonResult PostNewSignatureData(SignatureDTO signatureViewModel)
		{
			var signatureValidation = _signatureService.ValidateSignatureLength(Convert.ToString(signatureViewModel.TypeId), signatureViewModel.Signature);

			if (signatureValidation.HasError)
			{
				return Json(new { signatureValidation, signatureViewModel });
			}
			else
			{
				_signatureService.AddUpdateSignature(signatureViewModel);
				return Json(new { signatureValidation, signatureViewModel }, JsonRequestBehavior.AllowGet);
			}
		}
		[HttpGet]
		public JsonResult SignatureValidation(string TypeId, string Signature)
		{
			var signatureValidation = _signatureService.ValidateSignatureLength(TypeId, Signature);
			return Json(ComUtil.JsonEncodeCamelCase(signatureValidation), JsonRequestBehavior.AllowGet);
		}
		#endregion
		#endregion

		#region Generating the PDF
		[HttpPost, ValidateInput(false)]
		public ActionResult GeneratePDFPost(string document)
		{
			var doc = JsonConvert.DeserializeObject<GeneratedLetterDTO>(document);
			var html = ReplaceStylesheets(doc.HTML);
			var bytes = GeneratePDFStream(html);

			//System.IO.File.WriteAllBytes(PathUtil.Temp("Test1.pdf"), bytes);
			string fileName = string.Format("GeneratedPDF-{0}", doc.JurisdictionId);
			string htmlFileName = fileName + ".html";
			string pdfFileName = fileName + ".pdf";
#if DEBUG
			//uncomment this to save the raw html that is passed through to iText
			//var userFolder = System.Environment.GetFolderPath(System.Environment.SpecialFolder.UserProfile);
			//string filePath = $"{userFolder}\\Source\\_testFiles\\{htmlFileName}";
			//System.IO.File.WriteAllText(filePath, html);
#endif
			Response.ContentType = "blob";
			return File(new MemoryStream(bytes), "application/pdf", pdfFileName);//byte[] base64EncodedStringBytes = )
																				 //return File(Convert.ToBase64String(bytes), "application/pdf", "GeneratedPDF.pdf");
		}

		[HttpPost]
		public JsonResult PreviewPDFPost(GeneratedLetterDTO payload)
		{
			dynamic result = new System.Dynamic.ExpandoObject();
			var html = ReplaceStylesheets(payload.HTML);
			var bytes = GeneratePDFStream(html);
			var directoryPath = PathUtil.Temp($"LetterPreviews\\{_userContext.User.Username}\\");
			var directory = System.IO.Directory.CreateDirectory(directoryPath);
			string fileName = string.Format("PREVIEWGeneratedPDF-{0}{1}-{2}.pdf", payload.JurisdictionId, payload.Language, DateTime.Now.Ticks);
			var path = Path.Combine(PathUtil.Temp($"LetterPreviews\\{_userContext.User.Username}\\"), fileName);

			var currentFiles = directory.GetFiles();
			foreach (var file in currentFiles)
			{
				file.Delete();
			}

			try
			{
				System.IO.File.WriteAllBytes(path, bytes);
				result.filePath = $"Temp\\LetterPreviews\\{_userContext.User.Username}\\{fileName}";
			}
			catch (Exception error)
			{
				result.error = error;
			}

			result.userName = _userContext.User.Username;
			result.timeStamp = DateTime.Now;

			return Json(ComUtil.JsonEncodeCamelCase(result), "application/json");
		}


		[HttpPost, ValidateInput(false)]
		public ActionResult GenerateAllPDFs(string documents)
		{
			var docs = JsonConvert.DeserializeObject<GeneratedLetterDTO[]>(documents);
			using (var memoryStream = new MemoryStream())
			{
				using (var archive = new ZipArchive(memoryStream, ZipArchiveMode.Create, true))
				{
					foreach (var document in docs)
					{
						string pdfFileName = string.Format("GeneratedPDF-{0}.pdf", document.JurisdictionId);
						var file = archive.CreateEntry(pdfFileName);
						var bytes = GeneratePDFStream(document.HTML);
						using (Stream stream = file.Open())
						{
							stream.Write(bytes, 0, bytes.Length);
						}
					}
				}

				return File(memoryStream.ToArray(), "application/zip", "Letters.zip");
			}

		}
		public byte[] GeneratePDFStream(string htmlContent)
		{
			ConverterProperties props = new ConverterProperties();
			props.SetMediaDeviceDescription(new MediaDeviceDescription(MediaType.PRINT));
			FontProvider fp = new FontProvider();
			fp.AddStandardPdfFonts();
			fp.AddSystemFonts();
			props.SetFontProvider(fp);

			DefaultTagWorkerFactory tagworker = new DefaultTagWorkerFactory();

			props.SetTagWorkerFactory(tagworker);
			var workStream = new MemoryStream();
			var pdfWriter = new PdfWriter(workStream);

			HtmlConverter.ConvertToPdf(htmlContent, pdfWriter, props);

			return workStream.ToArray();
		}

		private string ReplaceStylesheets(string html)
		{
			// Read contents of pdf.css
			const string stylesheetPath = @"Submissions.Web.Vue.components.documents.generate.Styles.pdf.css";
			string newCss;
			var assembly = Assembly.GetExecutingAssembly();

			using (Stream stream = assembly.GetManifestResourceStream(stylesheetPath))
			using (StreamReader reader = new StreamReader(stream))
			{
				newCss = reader.ReadToEnd();
			}

			// Replace <head> with single style block containing pdf CSS
			var parser = new AngleSharp.Html.Parser.HtmlParser();
			var doc = parser.ParseDocument(html);
			var newHead = doc.CreateElement("head");

			var viewport = doc.CreateElement("meta");
			viewport.SetAttribute("name", "viewport");
			viewport.SetAttribute("content", "width=device-width, initial-scale=1.0");
			newHead.AppendChild(viewport);

			var newStyles = doc.CreateElement("style");
			newStyles.SetAttribute("type", "text/css");
			newStyles.InnerHtml = newCss;
			newHead.AppendChild(newStyles);

			doc.Head.Replace(newHead);

			return doc.DocumentElement.OuterHtml;
		}
		#endregion

		#region Saving HTML 
		[HttpPost, ValidateInput(false)]
		public ActionResult SaveAllHTML(string documents, int projectId)
		{
			var docs = JsonConvert.DeserializeObject<GeneratedLetterDTO[]>(documents);
			List<GeneratedLetter> lettersToAdd = new List<GeneratedLetter>();
			foreach (var doc in docs)
			{
				lettersToAdd.Add(new GeneratedLetter()
				{
					ProjectId = projectId,
					Letter = Encoding.ASCII.GetBytes(doc.HTML),
					JurisdictionFixId = doc.JurisdictionId,
					JurisdictionId = doc.JurisdictionId.ToJurisdictionIdString()
				});
			}
			_dbSubmission.GeneratedLetters.AddRange(lettersToAdd);
			_dbSubmission.SaveChanges();
			return Index(projectId, null, null);
		}
		#endregion

		// FROM SERVICE DTOs to Local
		#region DTO => VM
		private GenerateIndexViewModel BuildGenerateIndexViewModel(LetterGenerationInstanceDTO letterGenerationInstance)
		{
			var result = new GenerateIndexViewModel()
			{
				PermissionSet = letterGenerationInstance.PermissionSet,
				CurrentMode = letterGenerationInstance.Mode,
				BlankMemo = BuildBlankMemo(),
				Letter = BuildGenerateLetter(letterGenerationInstance),
				ClauseReports = BuildClauseReports(letterGenerationInstance.ClauseReports),
				Components = new List<SubmissionDTO>(),
				Paytables = letterGenerationInstance.Paytables.OrderBy(x => x.ThemeName).ThenBy(x => x.PaytableId).ToList(),
				GamingGuidelines = BuildGamingGuidelines(letterGenerationInstance.JurisdictionalData),
				Bundle = BuildBundle(letterGenerationInstance.Bundle),
				CertificationIssuers = BuildCertificationIssuers(letterGenerationInstance.CertificationIssuers),
				LetterGenerationSourceTabs = letterGenerationInstance.SourceTabs,
				JurisdictionData = BuildGenerateJurisdictionModels(letterGenerationInstance.JurisdictionalData),
				LetterJurisdictionOptions = letterGenerationInstance.LetterJurisdictionOptions.OrderBy(x => x.Text).ToList(),
				EditLetterGenSubSig = letterGenerationInstance.PermissionSet.EditLetterGenSubmissionSignature,
				EditLetterGeneration = letterGenerationInstance.PermissionSet.EditLetterGen,
				EvolutionInfo = BuildGenerateLetterEvolutionInfo(letterGenerationInstance.Evolution),
				ProjectInfo = BuildGenerateLetterProjectInfo(letterGenerationInstance.Bundle),
				JIRAInformation = BuildJIRAInformation(letterGenerationInstance.JIRAInformation),
				SpecializedJurisdictionClauseReports = BuildLetterGenerationSpecializedJurisdictionClauseReports(letterGenerationInstance.SpecializedJurisdictionClauseReports),
				OCUITComponents = BuildOCUITComponents(letterGenerationInstance.OCUITComponentDTOs),
				Modifications = letterGenerationInstance.Submissions.DistinctBy(x => x.Id).SelectMany(x => x.Modifications).ToList()
			};

			// Do not allow edit 
			if (!result.ProjectInfo.EvolutionInstanceExists)
			{
				result.PermissionSet.LockEvoMissing = true;
			}

			return result;
		}

		private List<GenerateOCUITComponentViewModel> BuildOCUITComponents(List<LetterGenOCUITComponent> components)
		{
			var result = new List<GenerateOCUITComponentViewModel>();
			foreach (var comp in components)
			{
				result.Add(BuildLetterGenerationOCUITComponent(comp));
			}
			return result;
		}

		private GenerateOCUITComponentViewModel BuildLetterGenerationOCUITComponent(LetterGenOCUITComponent component)
		{
			var result = new GenerateOCUITComponentViewModel()
			{
				Id = component.Id,
				Description = component.Description,
				FileNumber = component.FileNumber,
				NVLabNumber = component.NVLabNumber,
				IdNumber = component.IdNumber,
				PartNumber = component.PartNumber,
				SubmissionId = component.SubmissionId,
				JurisdictionIds = component.JurisdictionIds,
				Version = component.Version,
				Continuity = component.Continuity,
				TestedInParallel = component.TestedInParallel,
				JurisdictionChip = component.JurisdictionChip
			};
			return result;
		}

		private List<GenerateSpecializedJurisdictionClauseReportViewModel> BuildLetterGenerationSpecializedJurisdictionClauseReports(List<LetterGenerationSpecializedJurisdictionClauseReportDTO> specalizedClauseReports)
		{
			var result = new List<GenerateSpecializedJurisdictionClauseReportViewModel>();

			foreach (var report in specalizedClauseReports)
			{
				result.Add(BuildLetterGenerationSpecializedJurisdictionClauseReport(report));
			}

			return result;
		}
		private GenerateSpecializedJurisdictionClauseReportViewModel BuildLetterGenerationSpecializedJurisdictionClauseReport(LetterGenerationSpecializedJurisdictionClauseReportDTO report)
		{
			var result = new GenerateSpecializedJurisdictionClauseReportViewModel()
			{
				JurisdictionID = report.JurisdictionID,
				JurisdictionName = report.JurisdictionName,
				EvaluatedClauses = report.EvaluatedClauses.Select(x => new GenerateSpeacalizedJurisdcictionEvaluatedViewModel { Name = x.Name, Result = x.Result, NormalizedID = x.NormalizedId, ErrorText = x.ErrorText }).ToList(),
				TestCases = report.TestCases.Select(x => new GenerateSpeacalizedJurisdcictionTestCaseViewModel { Name = x.Name, Version = x.Version }).ToList()
			};

			return result;
		}
		private List<ClauseReportViewModel> BuildClauseReports(List<ClauseReportModel> clauseReportModels)
		{
			var result = new List<ClauseReportViewModel>();

			foreach (var clauseGroup in clauseReportModels.GroupBy(cr => cr.JurisdictionId))
			{
				result.Add(new ClauseReportViewModel()
				{
					ClauseReports = clauseGroup.ToList(),
					PassCount = clauseGroup.DistinctBy(c => c.ClauseID).Count(x => x.Answer == "PASS"),
					FailCount = clauseGroup.DistinctBy(c => c.ClauseID).Count(x => x.Answer == "FAIL"),
					NACount = clauseGroup.DistinctBy(c => c.ClauseID).Count(x => x.Answer == "NA"),
					NAStarCount = clauseGroup.DistinctBy(c => c.ClauseID).Count(x => x.Answer == "NA*"),
					JurisdictionId = clauseGroup.Key
				});
			}

			return result;
		}
		private List<GamingGuideline> BuildGamingGuidelines(List<LetterGenerationJurisdictionalDataDTO> jurisdictionalDataDTOs)
		{
			var jurisdictionIds = jurisdictionalDataDTOs
				.Select(y => y.JurisdictionFixId)
				.Distinct()
				.ToList();

			var gamingGuidelines = _dbSubmission.tbl_lst_GamingGuidelines
				.Where(x => jurisdictionIds.Contains(x.Jurisdiction.FixId))
				.Distinct()
				.Select(x => new GamingGuideline()
				{
					JurisdictionId = x.Jurisdiction.FixId,
					RTPRequirements = x.GamingGuidelineRTPRequirements
					.Select(y => new RTPRequirement
					{
						Type = y.GameType.Name,
						RTP = y.RTP,
						Symbol = y.DenomSymbol,
						RTPType = ((GamingGuidelineRTPType)y.RTPType).ToString()
					}).ToList()
				})
				.ToList();

			foreach (var gamingGuideline in gamingGuidelines)
			{
				foreach (var rtpRequirement in gamingGuideline.RTPRequirements)
				{
					if (string.IsNullOrEmpty(rtpRequirement.Symbol))
					{
						continue;
					}

					var symbols = new List<string> { "-", "<", ">", "=" };
					var pattern = @"(" + String.Join("|", symbols) + ")";
					var tokens = Regex.Split(rtpRequirement.Symbol, pattern).ToList();
					if (tokens.Any(x => x == "-"))
					{
						rtpRequirement.Symbol = tokens.Where(x => symbols.Contains(x)).FirstOrDefault();
						rtpRequirement.DenomOne = double.Parse(tokens.First());
						rtpRequirement.DenomTwo = double.Parse(tokens.Last());
					}
					else
					{
						rtpRequirement.Symbol = tokens.Where(x => symbols.Contains(x)).FirstOrDefault();
						rtpRequirement.DenomOne = double.Parse(tokens.Last());
					}
				}
			}

			return gamingGuidelines;
		}
		private GenerateQABundleModel BuildBundle(LetterGenerationBundleDTO bundle)
		{
			var result = new GenerateQABundleModel()
			{
				BundleId = bundle.BundleId,
				JurisdictionalData =
					bundle.JurisdictionalData.Select(x => new GenerateJurisdictionalDataModel
					{
						CertificationLab = x.CertificationLab,
						JurisdictionFixId = x.JurisdictionFixId,
						JurisdictionId = x.JurisdictionId,
						JurisdictionName = x.JurisdictionName,
						Primarykey = x.Id,
						RequestDate = x.RequestDate
					})
					.ToList(),
				Manufacturer = new ManufacturerDTO { ManufacturerId = bundle.ManufacturerId }, //bundle.Manufacturer,
				ProjectENStatus = bundle.ProjectStatus,
				ProjectHolder = bundle.ProjectHolder,
				ProjectEvalLab = bundle.ProjectEvalLab,
				ProjectFileNumber = bundle.FileNumber,
				ProjectId = bundle.ProjectId,
				ProjectIsTransfer = (bool)bundle.ProjectIsTransfer,
				ProjectName = bundle.ProjectName,
				QAStatus = bundle.QAStatus,
				ContainsRJWD = bundle.ContainsRJWD
			};

			return result;
		}
		private List<Models.Generate.Helper.CertificationIssuer> BuildCertificationIssuers(List<LetterGenerationCertificationIssuerDTO> certificationIssuers)
		{
			var result = new List<Models.Generate.Helper.CertificationIssuer>();

			foreach (var certificationIssuer in certificationIssuers)
			{
				result.Add(BuildCertificationIssuer(certificationIssuer));
			}

			return result;
		}
		private Models.Generate.Helper.CertificationIssuer BuildCertificationIssuer(LetterGenerationCertificationIssuerDTO certificationIssuer)
		{
			var result = new Models.Generate.Helper.CertificationIssuer()
			{
				FirstName = certificationIssuer.FirstName,
				LastName = certificationIssuer.LastName,
				MiddleInital = certificationIssuer.MiddleInital,
				PresidesOver = certificationIssuer.PresidesOver,
				Title = certificationIssuer.Title,
				UserId = certificationIssuer.UserId,
				Signature = BuildCertificationIssuerSignature(certificationIssuer.Signature),
			};

			return result;
		}
		private CertificationIssuerSignature BuildCertificationIssuerSignature(CertificationIssuerSignatureDTO signatrue)
		{
			var result = new CertificationIssuerSignature
			{
				Base64Image = signatrue.Base64Image,
				FileExtension = signatrue.FileExtension
			};

			return result;
		}
		private GenerateLetter BuildGenerateLetter(LetterGenerationInstanceDTO letterGenerationInstanceDTO)
		{
			//var alphaLetterContent = new AlphaLetterContent(letterGenerationInstanceDTO.Mode);
			//var aMemo = alphaLetterContent.Memos.FirstOrDefault();
			var jurisdictionalinfo = _dbSubmission.trProjectJurisdictionalData
				.Where(x => x.ProjectId == letterGenerationInstanceDTO.Bundle.ProjectId)
				.Select(x => new { x.JurisdictionalDataId, x.JurisdictionalData.JurisdictionId }).ToList();

			var jurisdictionalDataIds = jurisdictionalinfo.Select(x => x.JurisdictionalDataId).ToList();
			var jurisdictionIds = jurisdictionalinfo.Select(x => x.JurisdictionId).Distinct().ToList();

			var result = new GenerateLetter()
			{
				Sections = BuildGenerateLetterSections(letterGenerationInstanceDTO.Letter.Sections, jurisdictionalDataIds, jurisdictionIds),
				Headers = BuildGenerateLetterMemos(letterGenerationInstanceDTO.Letter.Headers, jurisdictionalDataIds, jurisdictionIds),
				Footers = BuildGenerateLetterMemos(letterGenerationInstanceDTO.Letter.Footers, jurisdictionalDataIds, jurisdictionIds)
			};

			SetDefaultPaging(letterGenerationInstanceDTO.Bundle.JurisdictionIds, result);

			return result;
		}
		private List<GenerateLetterSection> BuildGenerateLetterSections(List<SectionDTO> sections, List<int> jurisdictionalDataIds, List<string> jurisdictionalds)
		{
			var result = new List<GenerateLetterSection>();

			foreach (var section in sections)
			{
				result.Add(BuildGenerateLetterSection(section, jurisdictionalDataIds, jurisdictionalds));
			}

			return result;
		}
		private GenerateLetterSection BuildGenerateLetterSection(SectionDTO section, List<int> jurisdictionalDataIds, List<string> jurisdictionalds)
		{
			section.LetterContents.ForEach(x => x.Memos.ForEach(y => y.Section = new SectionDTO { Name = section.Name }));

			var result = new GenerateLetterSection()
			{
				Id = section.Id,
				Description = section.Name,
				Name = section.Name,
				OrderId = section.OrderId ?? 0,
				Memos = BuildGenerateLetterMemos(section.LetterContents, jurisdictionalDataIds, jurisdictionalds)
			};

			return result;
		}
		private List<GenerateLetterMemo> BuildGenerateLetterMemos(List<LetterContentDTO> letterContents, List<int> jurisdictionalDataIds, List<string> jurisdictionalds)
		{
			var result = new List<GenerateLetterMemo>();

			foreach (var letterContent in letterContents.OrderBy(x => x.OrderId))
			{
				letterContent.Memos.ForEach(x => x.OrderId = x.OrderId + (letterContent.OrderId * 100)); // this is to fix to keep all memos for a lettercontent together, otherwise memos with orderId=1 from diff lettercontent is shown one after another
				result.AddRange(BuildGenerateLetterMemos(letterContent.Memos, jurisdictionalDataIds, jurisdictionalds));
			}

			return result;
		}
		private List<GenerateLetterMemo> BuildGenerateLetterMemos(List<MemoDTO> memos, List<int> jurisdictionalDataIds, List<string> jurisdictionalds)
		{
			var result = new List<GenerateLetterMemo>();

			foreach (var memo in memos)
			{
				var memoToAdd = BuildGenerateLetterMemo(memo, jurisdictionalDataIds, jurisdictionalds);
				if (!memoToAdd.ApplicableJurisdictions.Any())
				{
					continue;
				}
				result.Add(memoToAdd); //

				var allCustomMemos = new List<GenerateLetterMemo>();

				//commented below - custom memos order needs to be manipulated otherwise with below commented code order is messed up by pa
				for (int i = 0; i < memo.CustomMemos.Count; i++)
				{
					var orderId = (decimal)memo.OrderId + ((i + 1) / 100);
					var convertedMemo = ConvertMemoDTO(memo.CustomMemos[i], true, orderId, memo.Id);
					allCustomMemos.Add(convertedMemo);
				}
				result.AddRange(allCustomMemos);
				//result.AddRange(memo.CustomMemos
				//	.Where(x => x.Active)
				//	.Select(x => ConvertMemoDTO(x, true)).ToList());
			}

			return result;
		}
		private GenerateLetterMemo BuildGenerateLetterMemo(MemoDTO memo, List<int> jurisdictionalDataIds, List<string> jurisdictionalds)
		{
			var memoLocalId = Guid.NewGuid();

			var applicableJurs = new List<int>();
			if (memo.JurisdictionsNotIncluded.Any())
			{
				if (_availableJurisdictions == null)
				{
					_availableJurisdictions = _dbSubmission.tbl_lst_fileJuris.Select(x => x.Id).ToList();
				}
				var jurIds = _availableJurisdictions.Select(x => new { JurIdInt = int.Parse(x), JurIdStr = x }).ToList();
				applicableJurs = jurIds.Where(x => (!memo.JurisdictionsNotIncluded.Select(m => m.JurisdictionId).ToList().Contains((int)x.JurIdInt)) && jurisdictionalds.Contains(x.JurIdStr)).Select(x => x.JurIdInt).ToList();
			}
			else
			{
				if (memo.Jurisdictions.Any())
				{
					applicableJurs = memo.Jurisdictions.Where(x => jurisdictionalds.Select(j => int.Parse(j)).ToList().Contains(x.JurisdictionId)).Select(x => x.JurisdictionId).ToList();
				}
				else
				{
					if (_availableJurisdictions == null)
					{
						_availableJurisdictions = _dbSubmission.tbl_lst_fileJuris.Select(x => x.Id).ToList();
					}
					var jurIds = _availableJurisdictions.Select(x => new { JurIdInt = int.Parse(x), JurIdStr = x }).ToList();
					applicableJurs = jurIds.Where(x => jurisdictionalds.Contains(x.JurIdStr)).Select(x => x.JurIdInt).ToList();
				}
			}

			var questionAnsJurDict = new Dictionary<int, List<AnswerDTO>>();
			foreach (var question in memo.Questions)
			{
				questionAnsJurDict.Add(question.Id, _letterContentService.LoadAllAnswers(question.Id, jurisdictionalDataIds, !string.IsNullOrWhiteSpace(question.Source), question.DefaultValue));
			}
			// Each question should have a set of answers,
			// Create dupe of the quesion if it has more than 1 answer returned
			var questionsToMapToControls = FormatQuestionsForJurSplit(memo.Questions, questionAnsJurDict);
			var result = new GenerateLetterMemo
			{
				ApplicableJurisdictions = applicableJurs,
				ApplicableManufacturers = memo.Manufacturers.Select(x => x.ManufacturerId).ToList(),
				BusinessLogicExpression = memo.BusinessLogicExpression,
				ContentChanged = string.Empty,
				Custom = false,
				IsGameDescription = memo.SpecialFlags == SpecialFlags.GameDescription,
				IsModification = memo.SpecialFlags == SpecialFlags.Modification,
				Paging = null,
				Removable = false,
				RepeatableContentControls = new List<GenerateLetterInputControl>(),
				SourceItemTargets = new List<SourceItemTarget>(),
				SplittableFields = new List<string>(),
				UserIncluded = false,
				DefaultInclude = false,
				Description = memo.Description,
				Id = memo.Id,
				Include = true,
				LetterContent = memo.Content,
				SecondaryLanguageLetterContent = memo.SecondaryLanguageContent,
				SecondaryLanguageId = memo.SecondaryLanguage,
				//Remove hard-coded game description conditions (memo.SpecialFlags == SpecialFlags.GameDescription) after we add a new HTML question type to letter question
				LetterContentControls = new List<GenerateLetterInputControl>(),
				Name = memo.Name,
				WorkSpaceCategory = memo.Section?.Name,
				LocalId = memoLocalId,
				Language = _availableLanguages.Where(y => y.Id == memo.Language).Select(x => x.Language).SingleOrDefault(),
				SecondaryLanguage = _availableLanguages.Where(y => y.Id == memo.SecondaryLanguage).Select(x => x.Language).SingleOrDefault(),
				OrderId = memo.OrderId ?? 0,
				SourceId = ParseLetterGenerationSource(memo.Source),
				SourceProperty = memo.SourceProperty,
				OriginalVersionId = memo.Version.OriginalId,
				LetterContentId = memo.LetterContentId,
				SpecialFlag = memo.SpecialFlags.ToString()
			};

			result.LetterContentControls = GenerateControls(questionsToMapToControls, questionAnsJurDict, memo, memoLocalId, applicableJurs);
			return result;
		}
		private List<GenerateLetterInputControl> GenerateControls(List<QuestionDTO> questions, Dictionary<int, List<AnswerDTO>> map, MemoDTO memo, Guid memoLocalId, List<int> applicableJurs)
		{
			var result = new List<GenerateLetterInputControl>();

			foreach (var quest in questions)
			{
				var selectedOptions = new List<String>();
				var answer = string.Empty;
				var savedBy = "";
				if (map[quest.Id].Any())
				{
					if (string.IsNullOrWhiteSpace(quest.Source))
					{
						answer = map[quest.Id].FirstOrDefault()?.AnswerValue ?? "";

						if (quest.Type == GLI.EF.LetterContent.InputType.Multiselect.ToString() && map[quest.Id].Any() && map[quest.Id].All(q => q.AnswerValue != null))
						{
							selectedOptions = JsonConvert.DeserializeObject<List<String>>(map[quest.Id].FirstOrDefault()?.AnswerValue ?? "");
							answer = String.Join(map[quest.Id].FirstOrDefault()?.Delimiter ?? ",", selectedOptions == null ? new List<string>() : selectedOptions);
						}
					}
					if (map[quest.Id].FirstOrDefault().UserId != 0)
					{
						var userId = map[quest.Id].FirstOrDefault().UserId;
						if (_trLogins == null)
						{
							_trLogins = new List<trLogin>();
							_trLogins = _dbSubmission.trLogins.ToList();
						}
						savedBy = _trLogins.Where(X => X.Id == userId).Select(x => x.FName + " " + x.LName).FirstOrDefault();
					}
				}

				var newControl = new GenerateLetterInputControl
				{
					AnswerId = map[quest.Id].FirstOrDefault()?.Id ?? null,
					SavedOn = map[quest.Id].FirstOrDefault()?.AnswerDate ?? null,
					SavedBy = savedBy,
					ApplicableJurisdictions = string.IsNullOrWhiteSpace(quest.Source) ? map[quest.Id].FirstOrDefault()?.ApplicableJurs ?? new List<int>() : new List<int>(),
					ColumnHeader = quest.ColumnHeader,
					SplitGroupApplicable = string.IsNullOrWhiteSpace(quest.Source) ? map[quest.Id].FirstOrDefault()?.ApplicableJurs ?? new List<int>() : new List<int>(),
					ConditionalExpression = quest.ConditionalExpression,
					Content = String.IsNullOrEmpty(answer) ? quest.DefaultValue : answer,
					Description = quest.QuestionText,
					Label = quest.TokenKey,
					Id = quest.Id,
					LocalId = Guid.NewGuid(),
					OptionDelimiter = map[quest.Id].FirstOrDefault()?.Delimiter ?? quest.OptionDelimiter ?? ",",
					Options = quest.QuestionOptions != "null" && quest.QuestionOptions != null ? new JavaScriptSerializer().Deserialize<List<SelectListItem>>(quest.QuestionOptions) : new List<SelectListItem>(),
					SourceId = (quest.Source == "None" || string.IsNullOrEmpty(quest.Source)) ? "" : quest.Source,
					SourceProperty = (quest.Source == "Components") ? "Component" : "",
					Name = quest.Name,
					//TableRows = quest.TableRows,
					MemoLocalId = memoLocalId,
					ReferenceLookup = quest.ReferenceLookup,
					Type = ((memo.SpecialFlags == SpecialFlags.GameDescription || memo.SpecialFlags == SpecialFlags.Modification) && quest.Source == "Components") ? "HTML" : quest.Type,
					RepeatableContent = (memo.SpecialFlags == SpecialFlags.GameDescription || memo.SpecialFlags == SpecialFlags.Modification) && quest.Source == "Components",
					IsGameDescription = (memo.SpecialFlags == SpecialFlags.GameDescription && quest.Source == "Components"),
					IsModification = (memo.SpecialFlags == SpecialFlags.Modification && quest.Source == "Components"),
					IsBusinessLogic = (!string.IsNullOrEmpty(memo.BusinessLogicExpression)) ? true : false,
					IsSplitFromDefault = map[quest.Id].FirstOrDefault()?.BreakoutUsed ?? false,
					SelectedOptions = selectedOptions,
					Splittable = quest.QuestionSpecialFlags == QuestionSpecialFlags.Splittable,
					SourceTargetAnswers = map[quest.Id].Select(x => new SourceTargetAnswer { Answer = x.AnswerValue, Source = x.Source, SourceTargetId = x.SourceTargetId, ApplicableJurisdictions = x.ApplicableJurs, IsSplitFromDefault = x.BreakoutUsed }).ToList(),
					IsRequired = quest.IsRequired,
					ParentId = (int)quest.ParentQuestionId
				};
				newControl.SavedBy = string.IsNullOrEmpty(newControl.SavedBy) && newControl.SavedOn != null ? "System" : newControl.SavedBy;
				newControl.SplitGroupApplicable = (!newControl.IsSplitFromDefault) ? new List<int>() : newControl.SplitGroupApplicable;

				if (map[quest.Id].Count > 0 && map[quest.Id].FirstOrDefault().AnswerValue != null)
				{
					if (newControl.Type == GLI.EF.LetterContent.InputType.LookupMultiple.ToString())
					{
						newControl.Options = JsonConvert.DeserializeObject<List<SelectListItem>>(map[quest.Id].FirstOrDefault().AnswerValue);
						newControl.SelectedOptions = newControl.Options.Select(x => x.Text).ToList();
						newControl.Content = string.Join(newControl.OptionDelimiter, newControl.SelectedOptions);
					}
					else if (newControl.Type == GLI.EF.LetterContent.InputType.Lookup.ToString())
					{
						newControl.Options = new List<SelectListItem> { JsonConvert.DeserializeObject<SelectListItem>(map[quest.Id].FirstOrDefault().AnswerValue) };
						newControl.Content = newControl.Options.First().Text;
					}
				}

				if (newControl.Type == GLI.EF.LetterContent.InputType.Multiselect.ToString())
				{
					foreach (var option in newControl.Options)
					{
						if (selectedOptions != null && selectedOptions.Any(x => x == option.Value))
						{
							option.Selected = true;
						}
					}
				}

				result.Add(newControl);
				if (map[quest.Id].Count > 1)
				{
					map[quest.Id].RemoveAt(0);
				}
			}

			var final = (result.Any(x => x.ParentId != 0)) ? AdjustParentChildSplitAndOrder(result, applicableJurs) : result.OrderBy(x => x.IsSplitFromDefault).ToList();
			return final;
		}

		private List<GenerateLetterInputControl> AdjustParentChildSplitAndOrder(List<GenerateLetterInputControl> result, List<int> applicableJurs)
		{
			var final = new List<GenerateLetterInputControl>();
			var allParents = result.Where(x => x.ParentId == 0).Select(x => x.Id).Distinct().ToList();
			for (int i = 0; i < allParents.Count; i++)
			{
				var parents = result.Where(x => x.Id == allParents[i]).OrderBy(x => x.IsSplitFromDefault).ToList();
				if (parents.Count == 1)
				{
					parents[0].ApplicableJurisdictions = applicableJurs;
				}
				parents.ForEach(x => x.IsBusinessLogic = true);
				final.AddRange(parents);

				var childs = GetChildControls(allParents[i], result, final);
				while (childs.Count > 0)
				{
					childs.ForEach(x => x.IsBusinessLogic = true);
					final.AddRange(childs);
					childs = GetChildControls(childs[0].Id, result, final);
				}
			}
			return final;
		}

		private List<GenerateLetterInputControl> GetChildControls(int parentId, List<GenerateLetterInputControl> result, List<GenerateLetterInputControl> checkParent)
		{
			var parents = checkParent.Where(x => x.Id == parentId).ToList();
			var childs = result.Where(x => x.ParentId == parentId).ToList();
			if (childs.Count > 0) //When split each child should also have same set
			{
				var copyChilds = new List<GenerateLetterInputControl>();
				for (int j = 0; j < parents.Count; j++)
				{
					var applicableJurs = parents[j].ApplicableJurisdictions;
					var checkSameChildWithJurExists = childs.Where(x => x.ApplicableJurisdictions.Any(jur => applicableJurs.Contains(jur))).FirstOrDefault();
					var copyAndAdd = false;
					if (checkSameChildWithJurExists != null)
					{
						var onlyInParent = applicableJurs.Except(checkSameChildWithJurExists.ApplicableJurisdictions).ToList();
						var onlyInChild = checkSameChildWithJurExists.ApplicableJurisdictions.Except(applicableJurs).ToList();
						if (!onlyInParent.Any() && !onlyInChild.Any())
						{
							copyChilds.Add(checkSameChildWithJurExists);
						}
						else
						{
							copyAndAdd = true;
						}
					}
					else
					{
						copyAndAdd = true;
					}

					if (copyAndAdd)
					{
						var newControl = CopyControl(childs[0]);
						newControl.LocalId = Guid.NewGuid();
						newControl.ApplicableJurisdictions = applicableJurs;
						newControl.IsSplitFromDefault = parents[j].IsSplitFromDefault;
						newControl.SplitGroupApplicable = parents[j].SplitGroupApplicable;
						newControl.ParentId = parentId;
						copyChilds.Add(newControl);
					}
				}
				return copyChilds.OrderBy(x => x.IsSplitFromDefault).ToList();
			}
			else
			{
				return childs.OrderBy(x => x.IsSplitFromDefault).ToList();
			}
		}

		private GenerateLetterInputControl CopyControl(GenerateLetterInputControl copyFrom)
		{
			var newChild = new GenerateLetterInputControl();
			newChild.ColumnHeader = copyFrom.ColumnHeader;
			newChild.SplitGroupApplicable = copyFrom.SplitGroupApplicable;
			newChild.ConditionalExpression = copyFrom.ConditionalExpression;
			newChild.Content = copyFrom.Content;
			newChild.Description = copyFrom.Description;
			newChild.Label = copyFrom.Label;
			newChild.Id = copyFrom.Id;
			newChild.OptionDelimiter = copyFrom.OptionDelimiter;
			newChild.Options = copyFrom.Options;
			newChild.SourceId = copyFrom.SourceId;
			newChild.SourceProperty = copyFrom.SourceProperty;
			newChild.Name = copyFrom.Name;
			//TableRows = quest.TableRows,
			newChild.MemoLocalId = copyFrom.MemoLocalId;
			newChild.ReferenceLookup = copyFrom.ReferenceLookup;
			newChild.Type = copyFrom.Type;
			newChild.RepeatableContent = copyFrom.RepeatableContent;
			newChild.IsGameDescription = copyFrom.IsGameDescription;
			newChild.IsModification = copyFrom.IsModification;
			newChild.IsBusinessLogic = copyFrom.IsBusinessLogic;
			newChild.IsSplitFromDefault = copyFrom.IsSplitFromDefault;
			newChild.SelectedOptions = copyFrom.SelectedOptions;
			newChild.Splittable = copyFrom.Splittable;
			newChild.SourceTargetAnswers = copyFrom.SourceTargetAnswers;
			newChild.IsRequired = copyFrom.IsRequired;
			newChild.SplitGroupApplicable = copyFrom.SplitGroupApplicable;
			newChild.SavedBy = copyFrom.SavedBy;
			return newChild;
		}

		private List<QuestionDTO> FormatQuestionsForJurSplit(List<QuestionDTO> questions, Dictionary<int, List<AnswerDTO>> map)
		{
			var result = new List<QuestionDTO>();
			foreach (var quest in questions)
			{
				var answerSet = map[quest.Id];
				if (answerSet.Count > 1)
				{
					if (!string.IsNullOrWhiteSpace(quest.Source))
					{

					}
					else
					{
						for (int i = 0; i < answerSet.Count - 1; i++)
						{
							var dupeQuest = new QuestionDTO
							{
								Answers = quest.Answers,
								ColumnHeader = quest.ColumnHeader,
								ConditionalExpression = quest.ConditionalExpression,
								EditDate = quest.EditDate,
								Id = quest.Id,
								LocalId = quest.LocalId,
								Name = quest.Name,
								OptionDelimiter = quest.OptionDelimiter,
								ParentQuestionId = quest.ParentQuestionId,
								QuestionOptions = quest.QuestionOptions,
								QuestionOptionsObj = quest.QuestionOptionsObj,
								QuestionSpecialFlags = quest.QuestionSpecialFlags,
								QuestionText = quest.QuestionText,
								ReferenceLookup = quest.ReferenceLookup,
								SecondaryLanguage = quest.SecondaryLanguage,
								SecondaryTokenValue = quest.SecondaryTokenValue,
								Source = quest.Source,
								SourceProperty = quest.SourceProperty,
								Table = quest.Table,
								TableRows = quest.TableRows,
								TokenKey = quest.TokenKey,
								TokenValue = quest.TokenValue,
								Type = quest.Type
							};
							result.Add(dupeQuest);
						}
					}
				}
			}
			result.AddRange(questions);
			return result;
		}
		private string ParseLetterGenerationSource(int enumId)
		{
			if (enumId != 0)
			{
				var result = (Source)enumId;
				return result.ToString();
			}

			return string.Empty;
		}
		private void SetDefaultPaging(List<int> jurisdictionIds, GenerateLetter letter)
		{
			letter.Sections.SelectMany(sec => sec.Memos).Where(memo => memo.Paging == null || memo.Paging.Count == 0).ToList().ForEach((memo) =>
			{
				SetMemoPaging(memo, jurisdictionIds, false, 1);
			});

			letter.Footers.ToList().ForEach(memo =>
			{
				//I'm not sure why this is needed, but Letter Preview breaks without this. That should be corrected.
				if (memo.ApplicableJurisdictions == null || memo.ApplicableJurisdictions.Count == 0)
					memo.ApplicableJurisdictions = jurisdictionIds;
				memo.Paging = new List<GenerateLetterPaging>();
				foreach (var locJurId in memo.ApplicableJurisdictions)
				{
					memo.Paging.Add(new GenerateLetterPaging
					{
						JurisdictionId = locJurId,
						LocalId = memo.LocalId,
						Page = 1,
						IsLandscape = false
					});
				}
			});

			//I don't know if Headers needs this, but let's be consistent
			letter.Headers.Where(header => header.Paging == null || header.Paging.Count == 0).ToList().ForEach(memo =>
			{
				SetMemoPaging(memo, jurisdictionIds, false, 1);
			});

		}
		private void SetMemoPaging(GenerateLetterMemo memo, List<int> bundleJurisdictionIds, bool isLandscape, int page)
		{
			var locJurIds = new List<int>();
			if (memo.ApplicableJurisdictions == null || memo.ApplicableJurisdictions.Count == 0)
				locJurIds = bundleJurisdictionIds;
			else
				locJurIds = memo.ApplicableJurisdictions.ToList();

			memo.Paging = new List<GenerateLetterPaging>();
			foreach (var locJurId in locJurIds)
			{

				memo.Paging.Add(new GenerateLetterPaging
				{
					JurisdictionId = locJurId,
					LocalId = memo.LocalId,
					Page = page,
					IsLandscape = isLandscape
				});
			}
		}
		private GenerateLetterMemo BuildBlankMemo()
		{
			return new GenerateLetterMemo()
			{
				Custom = true,
				DefaultInclude = true,
				Include = true,
				Removable = true,
				Name = "Custom",
				Description = "",
				Language = "English",
				LetterContent = "<div v-html=\"custom\"></div>",
				SecondaryLanguageLetterContent = "<div v-html=\"secondarycustom\"></div>",
				Paging = new List<GenerateLetterPaging>(),
				LetterContentControls = new List<GenerateLetterInputControl>()
				{
					new GenerateLetterInputControl()
					{
						Type = GenerateLetterInputControlType.HTML.ToString(),
						Description = "",
						Label = "custom",
						Name = "Custom",
						Content = "<div>This is a custom note.</div>",
						Splittable = true
					}
				}
			};
		}
		private List<GenerateJurisdictionModel> BuildGenerateJurisdictionModels(List<LetterGenerationJurisdictionalDataDTO> jurisdictionalData)
		{
			var result = new List<GenerateJurisdictionModel>();

			foreach (var jurisdiction in jurisdictionalData)
			{
				result.Add(BuildGenerateJurisdictionModel(jurisdiction));
			}

			return result;
		}
		private GenerateJurisdictionModel BuildGenerateJurisdictionModel(LetterGenerationJurisdictionalDataDTO jurisdiction)
		{
			var result = new GenerateJurisdictionModel()
			{
				AvailTechnicalStandards = BuildAvaiableTechnicalStandards(jurisdiction.AvailibleTechnicalStandards),
				CertificationIssuer = BuildCertificationIssuer(jurisdiction.CertificationIssuer),
				EvalLabs = jurisdiction.EvalLabs,
				EvalLabLetterCodes = jurisdiction.EvalLabLetterCodes,
				EvalPeriodFrom = jurisdiction.EvalPeriodFrom,
				EvalPeriodTo = jurisdiction.EvalPeriodTo,
				JurisdictionalCertificationLab = jurisdiction.CertificationLab,
				JurisdictionId = jurisdiction.JurisdictionFixId,
				LetterName = jurisdiction.LetterName,
				Name = jurisdiction.JurisdictionName,
				Recipient = jurisdiction.Recipient,
				ReportType = jurisdiction.ReportType,
				RequestDate = jurisdiction.RequestDate,
				SoftwareSupplier = jurisdiction.SoftwareSupplier,
				SubmittingParty = jurisdiction.SubmittingParty,
				TechnicalStandards = jurisdiction.TechnicalStandards,
				TestingResult = jurisdiction.TestingResult,
				LetterNumber = jurisdiction.LetterNumber,
				ClientNumber = jurisdiction.ClientNumber,
				CertNumbers = jurisdiction.CertNumbers,
				IssueDate = DateTime.Now,
				ManufacturerDisplay = jurisdiction.ManufacturerDisplay,
				CountryOfManufOrigin = jurisdiction.CountryOfManufOrigin,
				CountryOfManufOrigin_SecLang = jurisdiction.CountryOfManufOrigin_SecLang,
				ControlInstruments = jurisdiction.ControlInstruments,
				ControlInstruments_SecLang = jurisdiction.ControlInstruments_SecLang
			};

			return result;
		}
		private List<TechnicalStandard> BuildAvaiableTechnicalStandards(List<LetterGenerationTechnicalStandardDTO> techStandards)
		{
			var result = new List<TechnicalStandard>();

			foreach (var techStandard in techStandards)
			{
				result.Add(BuildTechnicalStandard(techStandard));
			}

			return result;
		}
		private TechnicalStandard BuildTechnicalStandard(LetterGenerationTechnicalStandardDTO techStandard)
		{
			var result = new TechnicalStandard()
			{
				JurisdictionId = techStandard.JurisdictionId,
				Name = techStandard.Name,
				Selected = techStandard.Selected
			};

			return result;
		}
		private GenerateLetterEvolutionInfo BuildGenerateLetterEvolutionInfo(LetterGenerationEvolutionDTO evolutionInfo)
		{
			var result = new GenerateLetterEvolutionInfo()
			{
				ComponentGroupDoubleLockStatus = evolutionInfo.ComponentGroupDoubleLockStatus
			};

			return result;
		}
		private GenerateLetterProjectInfo BuildGenerateLetterProjectInfo(LetterGenerationBundleDTO bundle)
		{
			var result = new GenerateLetterProjectInfo()
			{
				BundleId = bundle.BundleId,
				BundleName = bundle.BundleName,
				ComponentGroupIds = bundle.ComponentGroupIds,
				EvalLabs = bundle.EvalLabs,
				EvalPeriodFrom = bundle.EvalPeriodFrom,
				EvalPeriodTo = bundle.EvalPeriodTo,
				FileNumber = bundle.FileNumber,
				JurisdictionIds = bundle.JurisdictionIds,
				ManufacturerId = bundle.ManufacturerId,
				EvalLabLetterCodes = bundle.EvalLabLetterCodes,
				//ManufacturerName = bundle.ManufacturerName,
				ProjectEvalLab = bundle.ProjectEvalLab,
				ProjectId = bundle.ProjectId,
				ProjectStatus = bundle.ProjectStatus,
				ProjectHolder = bundle.ProjectHolder,
				QAStatus = bundle.QAStatus,
				ContainsRJWD = bundle.ContainsRJWD,
				MergedProjectIds = bundle.MergedProjectIds,
				EvolutionInstanceExists = bundle.EvolutionInstanceExists
			};

			return result;
		}
		private List<GenerateRevocationInformationViewModel> BuildGenerateRevocationInformations(List<LetterGenerationRevocationInformationDTO> revocationInformation)
		{
			var result = new List<GenerateRevocationInformationViewModel>();

			foreach (var revInfo in revocationInformation)
			{
				result.Add(BuildGenerateRevocationInformation(revInfo));
			}

			return result;
		}
		private GenerateRevocationInformationViewModel BuildGenerateRevocationInformation(LetterGenerationRevocationInformationDTO revInfo)
		{
			var result = new GenerateRevocationInformationViewModel()
			{
				JurisdictionID = revInfo.JurisdictionID,
				RevocationPeriod = revInfo.RevocationPeriod,
				RevocationWording = revInfo.RevocationWording,
				SubmissionID = revInfo.SubmissionID,
				SubmissionDateCode = revInfo.SubmissionDateCode,
				SubmissionGameName = revInfo.SubmissionGameName,
				SubmissionIdNumber = revInfo.SubmissionIdNumber,
				SubmissionVersion = revInfo.SubmissionVersion,
				SubmissionItemsBeingRevoked = BuildGenerateSubmissionItemsBeingRevoked(revInfo.ItemsBeingRevokedSubmissionInformation)
			};

			return result;
		}
		private List<GenerateSubmissionItemsBeingRevoked> BuildGenerateSubmissionItemsBeingRevoked(List<LetterGenerationRevocationSubmissionInformationDTO> submissionItemsBeingRevoked)
		{
			var result = new List<GenerateSubmissionItemsBeingRevoked>();

			foreach (var item in submissionItemsBeingRevoked)
			{
				result.Add(BuildGenerateSubmissionItemBeingRevoked(item));
			}

			return result;
		}
		private GenerateSubmissionItemsBeingRevoked BuildGenerateSubmissionItemBeingRevoked(LetterGenerationRevocationSubmissionInformationDTO item)
		{
			var result = new GenerateSubmissionItemsBeingRevoked
			{
				Id = item.Id,
				DateCode = item.DateCode,
				GameName = item.GameName,
				IdNumber = item.IdNumber,
				Version = item.Version
			};

			return result;
		}
		private GenerateJIRAInformationViewModel BuildJIRAInformation(LetterGenerationJIRAInformationDTO letterGenerationJIRAInformation)
		{
			var result = new GenerateJIRAInformationViewModel();

			result.RevocationInformations = BuildGenerateRevocationInformations(letterGenerationJIRAInformation.RevocationInformation);
			result.DisclosureInformations = BuildGenerateDisclosureInformations(letterGenerationJIRAInformation.DisclosureInformation);

			return result;
		}
		private List<GenerateDisclosureInformationViewModel> BuildGenerateDisclosureInformations(List<LetterGenerationDisclosureWordingDTO> disclosureInfo)
		{
			var result = new List<GenerateDisclosureInformationViewModel>();

			foreach (var disclosure in disclosureInfo)
			{
				result.Add(BuildGenerateDisclosureInformation(disclosure));
			}

			return result;
		}
		private GenerateDisclosureInformationViewModel BuildGenerateDisclosureInformation(LetterGenerationDisclosureWordingDTO disclosure)
		{
			var result = new GenerateDisclosureInformationViewModel
			{
				DisclosureWording = disclosure.DisclosureWording,
				JurisdictionID = disclosure.JurisdictionID,
				SubmissionID = disclosure.SubmissionID
			};

			return result;
		}
		#endregion

		#region Helper Methods
		private void LoadAnswersFromNewSubmission(int projectId)
		{
			//this is for whatever reason while project creation did not import, give it a one more try
			var projectJurisdictions = _dbSubmission.trProjectJurisdictionalData.Where(x => x.ProjectId == projectId && x.Project.IsTransfer == true).Select(x => x.JurisdictionalDataId).ToList();
			if (projectJurisdictions.Count > 0)
			{
				if (!_dbLetterContent.AnswerAssociations.Where(x => projectJurisdictions.Contains((int)x.JurisdictionalDataId)).Any())
				{
					_projectService.ImportAnswersForLetterGenFromNewSubmission(projectId);
				}
			}
		}
		#endregion
	}
}
