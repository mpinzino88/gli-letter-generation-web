﻿extern alias ZEFCore;
using AutoMapper;
using GLI.EFCore.Submission;
using Microsoft.EntityFrameworkCore;
using Submissions.Common;
using Submissions.Web.Areas.Documents.Models.Book;
using Submissions.Web.Models.Grids.Documents;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web.Mvc;
using ZEFCore::Z.EntityFramework.Plus;

namespace Submissions.Web.Areas.Documents.Controllers
{
	[AuthorizeUser(Permissions = new Permission[] { Permission.LetterBookBilling, Permission.LetterBookEntry, Permission.LetterBookReview }, HasAccessRight = AccessRight.Read)]
	public class BookController : Controller
	{
		private readonly SubmissionContext _dbSubmission;
		private readonly IUserContext _userContext;
		private readonly ILogger _logger;
		private static readonly List<string> _headerProperties = new List<string>()
		{
			"Ref Number",
			"Branch",
			"Billable Quantity",
			"Project Task",
			"Account Group",
			"Inventory ID",
			"Quantity",
			"Billable",
			"Fin. Period",
			"Project",
			"ContractType",
			"OwningCompany",
			"Currency",
			"Date",
			"Work Location",
			"Billing Sheet",
			"Number Of Items",
			"Amount",
			"Description"
		};

		public BookController(SubmissionContext dbSubmission, IUserContext userContext, ILogger logger)
		{
			_dbSubmission = dbSubmission;
			_userContext = userContext;
			_logger = logger;
		}

		public ActionResult Index()
		{
			var vm = new BookIndexViewModel();
			var defaultDate = GetPreviousBusinessDay();
			var todayDate = DateTime.Today;
			vm.Filters.ReportStartDate = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(defaultDate, "Eastern Standard Time");
			vm.Filters.ReportEndDate = TimeZoneInfo.ConvertTimeBySystemTimeZoneId(todayDate, "Eastern Standard Time");
			vm.Filters.ShowBillable = true;
			vm.Filters.ShowNonBillable = true;
			vm.Filters.OnlyMajorRevisions = false;
			vm.Filters.BatchStatus = new string[] { LetterBookACTStatus.Pending.ToString() };
			vm.Filters.LaboratoryId = _userContext.LocationId;
			vm.Filters.Laboratory = _dbSubmission.trLaboratories.Where(x => x.Id == _userContext.LocationId).Select(x => x.Location + " - " + x.Name).FirstOrDefault();

			if (Util.HasAccess(Permission.LetterBookBilling, AccessRight.Read))
			{
				var grid = new LetterBookGrid(Department.ACT);
				grid.Grid.MultiSelect = true;

				vm.Filters.DepartmentId = _dbSubmission.trDepts.Where(x => x.Code == Department.ACT.ToString()).Select(x => x.Id).Single();
				vm.Filters.Department = Department.ACT;
				//vm.Filters.BatchStatus = new List<string> { "Open" };
				vm.Department = Department.ACT;
				vm.LetterBookGrid = grid;
			}
			else
			{
				var grid = new LetterBookGrid(Department.QA);
				vm.Filters.DepartmentId = _dbSubmission.trDepts.Where(x => x.Code == Department.QA.ToString()).Select(x => x.Id).Single();
				vm.Filters.Department = Department.QA;
				vm.Department = Department.QA;
				vm.LetterBookGrid = grid;
			}

			return View(vm);
		}

		public ActionResult Edit(int? id, int? documentVersionId, Mode mode)
		{
			//TODO: make this non-hardcoded
			documentVersionId = 2;

			var letterBook = new tbl_LetterBook();

			var vm = new BookEditViewModel
			{
				DocumentVersionId = (int)documentVersionId,
				EntryUser = _userContext.User.Name,
				EntryDate = DateTime.Now,
				LaboratoryId = _userContext.LocationId,
				Laboratory = _userContext.Location + " - " + _userContext.LocationName
			};

			if (mode == Mode.Add)
			{
				var departmentId = _dbSubmission.trDepts.Where(x => x.Code == Department.QA.ToString()).Select(x => x.Id).Single();
				vm.DepartmentId = departmentId;
				vm.Department = Department.QA;
				var document = _dbSubmission.DocumentVersions
					.Include(x => x.Document)
					.Where(x => x.Id == documentVersionId)
					.Select(x => new
					{
						x.Document.DocumentTypeId,
						x.Document.DocumentType.DocumentType
					})
					.Single();

				var documentVersionLinks = _dbSubmission.DocumentVersionLinks
					.Include(x => x.DocumentVersion.Document)
					.Include(x => x.JurisdictionalData.Submission.Manufacturer)
					.Where(x => x.DocumentVersionId == documentVersionId)
					.Select(x => new
					{
						x.Id,
						x.JurisdictionalData.JurisdictionId,
						x.JurisdictionalData.JurisdictionName,
						ManufacturerCode = x.JurisdictionalData.Submission.Manufacturer.Code,
						Manufacturer = x.JurisdictionalData.Submission.Manufacturer.Description
					})
					.ToList();

				vm.DocumentVersionLinks = documentVersionLinks.Select(x => new DocumentVersionLink
				{
					Id = x.Id,
					JurisdictionId = x.JurisdictionId,
					JurisdictionName = x.JurisdictionName
				})
				.OrderBy(x => x.JurisdictionName)
				.ToList();
			}
			else if (mode == Mode.Edit)
			{
				var jurNameComparer = new JurisComparer();

				letterBook = _dbSubmission.tbl_LetterBook.Where(x => x.Id == id).Single();
				vm = Mapper.Map<BookEditViewModel>(letterBook);
				// automapping for billing sheet temporarily disabled
				if (!string.IsNullOrWhiteSpace(letterBook.BillingSheet))
				{
					//Enum.TryParse<LetterBookBillingSheetOld>(letterBook.BillingSheet, out var holderValOld);
					//Enum.TryParse<LetterBookBillingSheet>(letterBook.BillingSheet, out var holderValNew);

					//if ((holderValNew.ToString() == holderValOld.ToString()) || holderValOld == 0)
					//{

					//	vm.BillingSheet = holderValNew.ToString();
					//}
					//else
					//{
					//	switch (holderValOld)
					//	{
					//		case LetterBookBillingSheetOld.NonSpecialized:
					//			vm.BillingSheet = LetterBookBillingSheet.USNonSpecialized.ToString();
					//			break;
					//		case LetterBookBillingSheetOld.Specialized:
					//			vm.BillingSheet = LetterBookBillingSheet.USSpecialized.ToString();
					//			break;
					//		case LetterBookBillingSheetOld.CorrectionAddendum:
					//			vm.BillingSheet = LetterBookBillingSheet.USCorrectionAddendum.ToString();
					//			break;
					//		default:
					//			break;
					//	}
					//}
					vm.BillingSheet = letterBook.BillingSheet;
					vm.BillingSheetId = _dbSubmission.tbl_lst_BillingSheets.Where(x => x.Name == vm.BillingSheet).Select(x => x.Id).SingleOrDefault();
				}
				vm.DocumentVersionLinks = _dbSubmission.DocumentVersionLinks
					.Where(x => x.DocumentVersionId == letterBook.DocumentVersionId)
					.OrderBy(x => x.JurisdictionalData.Jurisdiction.Name)
					.Select(x => new DocumentVersionLink
					{
						Id = x.Id,
						JurisdictionId = x.JurisdictionalData.JurisdictionId,
						JurisdictionName = x.JurisdictionalData.Jurisdiction.Name,
						Billable = x.Billable,
						ProjectId = x.ProjectId,
						BilledCount = x.BilledCount
					})
					.ToList();
				vm.DocumentVersionLinks = vm.DocumentVersionLinks.Distinct(jurNameComparer).ToList();
				var projectId = vm.DocumentVersionLinks.Select(x => x.ProjectId).Distinct().First();
				vm.FileNumber = _dbSubmission.trProjects.Where(y => y.Id == projectId).SingleOrDefault().FileNumber;
				vm.BilledCount = vm.DocumentVersionLinks.OrderByDescending(x => x.BilledCount).Select(x => x.BilledCount).Distinct().FirstOrDefault() != null ?
					(int)vm.DocumentVersionLinks.Select(x => x.BilledCount).Distinct().FirstOrDefault() : 0;
			};

			vm.Mode = mode;
			vm.IsEntryUser = Util.HasAccess(Permission.LetterBookEntry, AccessRight.Edit);
			vm.IsReviewUser = Util.HasAccess(Permission.LetterBookReview, AccessRight.Edit);
			vm.IsBillingUser = Util.HasAccess(Permission.LetterBookBilling, AccessRight.Edit);
			vm.StatusList = LoadAvailableStatuses(vm);
			return View(vm);
		}


		private class JurisComparer : IEqualityComparer<DocumentVersionLink>
		{
			public bool Equals(DocumentVersionLink dvl1, DocumentVersionLink dvl2)
			{
				return dvl1.JurisdictionName == dvl2.JurisdictionName;
			}
			public int GetHashCode(DocumentVersionLink link)
			{
				return link.JurisdictionName.GetHashCode();
			}
		}

		[HttpPost]
		public ActionResult Edit(BookEditViewModel vm)
		{
			var now = DateTime.Now;
			var letterBook = new tbl_LetterBook();
			var oldDepartment = Department.QA;
			if (vm.Mode == Mode.Edit)
			{
				letterBook = _dbSubmission.tbl_LetterBook.Find(vm.Id);
				oldDepartment = (Department)Enum.Parse(typeof(Department), letterBook.Department.Code);
			}

			letterBook.Id = vm.Id ?? 0;
			letterBook.DocumentVersionId = vm.DocumentVersionId;
			letterBook.DepartmentId = vm.DepartmentId;
			letterBook.Billable = vm.IsBillable;
			letterBook.BillingSheet = vm.BillingSheet.ToString();
			letterBook.GameName = null;
			letterBook.LaboratoryId = (int)vm.LaboratoryId;
			letterBook.LetterType = null;
			letterBook.ReportDate = vm.ReportDate;
			letterBook.Note = vm.Note;

			// specialized and nonspecialized settings
			if (vm.BillingSheet == LetterBookBillingSheet.USNonSpecialized.ToString())
			{
				letterBook.BallyType = vm.BallyType?.ToString();
				letterBook.ReportType = null;
				letterBook.SpecializedType = null;
			}
			else
			{
				letterBook.BallyType = null;
				letterBook.ReportType = vm.ReportType;
				letterBook.SpecializedType = vm.SpecializedType;
			}

			// update statuses
			if (oldDepartment == Department.QA)
			{
				letterBook.QAStatus = vm.QAStatus.ToString();

				if (vm.IsReviewUser)
				{
					// review user
					letterBook.ReviewUserId = _userContext.User.Id;
					letterBook.ReviewDate = now;

					if (vm.QAStatus == LetterBookQAStatus.Approved)
					{
						letterBook.ACTStatus = LetterBookACTStatus.Pending.ToString();
						letterBook.DepartmentId = _dbSubmission.trDepts.Where(x => x.Code == Department.ACT.ToString()).Select(x => x.Id).Single();
					}
				}
			}
			else
			{
				letterBook.ACTStatus = vm.ACTStatus.ToString();
				letterBook.BillingUserId = _userContext.User.Id;
				letterBook.BillingDate = now;
			}

			if (vm.Mode == Mode.Add)
			{
				letterBook.EntryUserId = _userContext.User.Id;
				letterBook.EntryDate = now;

				_dbSubmission.tbl_LetterBook.Add(letterBook);
			}

			foreach (var item in vm.DocumentVersionLinks)
			{
				_dbSubmission.DocumentVersionLinks.Where(x => x.Id == item.Id).Update(x => new tbl_DocumentVersionLink { Billable = vm.IsBillable, BilledCount = vm.BilledCount });
			}

			_dbSubmission.SaveChanges();

			return RedirectToAction("Index");
		}

		[HttpPost]
		public ActionResult Delete(int id)
		{
			_dbSubmission.tbl_LetterBook.Where(x => x.Id == id).Delete();

			return RedirectToAction("Index");
		}

		[HttpPost]
		public ActionResult UpdateACTStatus(int id, string status)
		{
			var letterBook = new tbl_LetterBook { Id = id, ACTStatus = status, BillingUserId = _userContext.User.Id, BillingDate = DateTime.Now };
			_dbSubmission.tbl_LetterBook.Attach(letterBook);
			_dbSubmission.Entry(letterBook).Property(x => x.ACTStatus).IsModified = true;
			_dbSubmission.Entry(letterBook).Property(x => x.BillingUserId).IsModified = true;
			_dbSubmission.Entry(letterBook).Property(x => x.BillingDate).IsModified = true;
			_dbSubmission.SaveChanges();

			return Json("ok");
		}

		[HttpGet]
		public JsonResult TestExportResults(int? batchId, DateTime? startDate, DateTime? endDate, int? laboratoryId)
		{
			var data = GetExportData(batchId, startDate, endDate, laboratoryId);

			var nullDocVersions = data.Where(x => x.DocumentVersion == null);
			if (nullDocVersions.Count() > 0)
			{
				var missing = string.Join(",", nullDocVersions.Select(x => x.Id.ToString()));
				_logger.LogError(new Exception("The included Ids were not in the export, their document versions are null"), missing);
			}
			var gatJurs = _dbSubmission.tbl_lst_fileJuris.Where(x => x.IsGAT).Select(x => x.Id);
			var resultItemList = new List<string>();
			var uniqueDynamicsNumbers = new List<string>();
			var duplicatedDynamicsNumbers = new List<string>();
			foreach (var item in data.Where(x => x.DocumentVersion != null).OrderByDescending(x => x.EntryDate))
			{
				try
				{
					var project = item.DocumentVersion.DocumentVersionLinks.FirstOrDefault().Project;
					string dynamicsProjName = "";
					if (project != null)
					{
						var jurId = item.DocumentVersion.DocumentVersionLinks.Select(y => y.JurisdictionalData.JurisdictionId).Distinct().FirstOrDefault().ToString();
						dynamicsProjName = new FileNumber(string.Format("{0}{1}{2}", project.FileNumber, "-", jurId)).AccountingFormat;
					}
					var billableCount = (int)item.DocumentVersion.DocumentVersionLinks
						.OrderByDescending(x => x.BilledCount)
						.Select(x => x.BilledCount)
						.Distinct()
						.FirstOrDefault();
					// Check if value below is GAT jur

					if (!uniqueDynamicsNumbers.Contains(dynamicsProjName))
					{
						uniqueDynamicsNumbers.Add(dynamicsProjName);
					}
					else
					{
						duplicatedDynamicsNumbers.Add(dynamicsProjName);
					}

					var isGatJur = gatJurs.Contains(item.DocumentVersion.DocumentVersionLinks.First().JurisdictionalData.JurisdictionId);

					//Check in Admin tables first
					var appFee = GetAcctAppFee(item, isGatJur, billableCount);
					if (appFee == 0)
					{
						appFee = (int)GetAppFee(item, isGatJur, billableCount);
					}

					if (appFee == 0)
					{
						resultItemList.Add(dynamicsProjName);
					}
				}
				catch
				{

				}
			}
			return Json(new { resultItemList, duplicatedDynamicsNumbers }, JsonRequestBehavior.AllowGet);
		}

		private List<tbl_LetterBook> GetExportData(int? batchId, DateTime? startDate, DateTime? endDate, int? laboratoryId)
		{
			var query = _dbSubmission.tbl_LetterBook.AsQueryable();
			if (batchId == null)
			{
				query = query.Where(x => x.Billable && (x.ACTStatus == LetterBookACTStatus.InProgress.ToString() || x.ACTStatus == LetterBookACTStatus.Pending.ToString())).AsQueryable();
				if (startDate == null || endDate == null)
				{
					var lastBusinessDay = GetPreviousBusinessDay();
					query = query.Where(x => x.ReportDate == lastBusinessDay).AsQueryable();
				}
				{
					query = query.Where(x => x.ReportDate >= startDate && x.ReportDate <= endDate).AsQueryable();
				}
				if (laboratoryId != 0 && laboratoryId != null)
				{
					query = query.Where(x => x.LaboratoryId == laboratoryId).AsQueryable();
				}
			}
			else
			{
				query = query.Where(x => x.BatchNumber == batchId).AsQueryable();
			}
			var data = query.ToList();

			return data;
		}

		public void Export(int? batchId, DateTime? startDate, DateTime? endDate, int? laboratoryId)
		{
			var data = GetExportData(batchId, startDate, endDate, laboratoryId);
			var exportedFileName = batchId == null ? "LetterBookExport.csv" : "LetterBookExport-" + batchId.ToString() + ".csv";
			var nevadaGatManufs = _dbSubmission.ManufacturerGroups.Where(x => x.Description == "ARI").SelectMany(x => x.ManufacturerLinks.Select(y => y.ManufacturerCode)).Distinct().ToList();
			HttpContext.Response.Clear();
			HttpContext.Response.AddHeader("content-disposition", string.Format("attachment; filename={0}", exportedFileName));
			HttpContext.Response.ContentType = "application/vnd.ms-excel";

			var newLine = System.Environment.NewLine;
			//var t = data[0].GetType();
			using (StringWriter sw = new StringWriter())
			{
				sw.Write(string.Join(",", _headerProperties));
				sw.Write(newLine);
				var nullDocVersions = data.Where(x => x.DocumentVersion == null);
				if (data.Where(x => x.DocumentVersion == null).Count() > 0)
				{
					var missing = string.Join(",", nullDocVersions.Select(x => x.Id.ToString()));
					_logger.LogError(new Exception("The included Ids were not in the export, their document versions are null"), missing);
				}
				foreach (var item in data.Where(x => x.DocumentVersion != null).OrderByDescending(x => x.EntryDate))
				{
					try
					{
						var project = item.DocumentVersion.DocumentVersionLinks.FirstOrDefault().Project;

						var dynamicsProjName = "";
						var contractType = "";
						var owningCompany = "";
						var currency = "";
						if (project != null)
						{
							var jurId = item.DocumentVersion.DocumentVersionLinks.Select(y => y.JurisdictionalData.JurisdictionId).Distinct().FirstOrDefault().ToString();
							dynamicsProjName = new FileNumber(string.Format("{0}{1}{2}", project.FileNumber, "-", jurId)).AccountingFormatNoDashes;
							contractType = _dbSubmission.JurisdictionalData.Where(x => x.Submission.FileNumber == project.FileNumber && x.JurisdictionId == jurId).Select(x => x.ContractType.Code).FirstOrDefault();
							owningCompany = _dbSubmission.JurisdictionalData.Where(x => x.Submission.FileNumber == project.FileNumber && x.JurisdictionId == jurId).Select(x => x.Company.DynamicsCompanyId).FirstOrDefault();
							currency = _dbSubmission.JurisdictionalData.Where(x => x.Submission.FileNumber == project.FileNumber && x.JurisdictionId == jurId).Select(x => x.Currency.Code).FirstOrDefault();
						}
						var billableCount = (int)item.DocumentVersion.DocumentVersionLinks
							.OrderByDescending(x => x.BilledCount)
							.Select(x => x.BilledCount)
							.Distinct()
							.FirstOrDefault();
						// Check if value below is GAT jur
						var gatJurs = _dbSubmission.tbl_lst_fileJuris.Where(x => x.IsGAT).Select(x => x.Id);
						var jurItemId = item.DocumentVersion.DocumentVersionLinks.First().JurisdictionalData.JurisdictionId;
						var numberOfItems = item.DocumentVersion.DocumentVersionLinks.First().BilledCount;

						var isGatJur = jurItemId == ((int)CustomJurisdiction.NevadaITL).ToJurisdictionIdString()
							? gatJurs.Contains(jurItemId) && nevadaGatManufs.Contains(new FileNumber(project.FileNumber).ManufacturerCode)
							: gatJurs.Contains(jurItemId);

						var appFee = GetAcctAppFee(item, isGatJur, billableCount);
						if (appFee == 0)
						{
							appFee = (int)GetAppFee(item, isGatJur, billableCount);
						}

						var reportDate = item.ReportDate;
						var finPeriod = reportDate.ToString("MMyyyy").PadLeft(6, '0');
						var isBillable = (item.Billable) ? "True" : "False";

						var valuesList = new List<string>()
						{
							"100",
							item.Laboratory.Company.DynamicsCompanyId,
							"1",
							"00APP",
							"APP FEE",
							"APP FEE",
							"1",
							"=\"" + isBillable  + "\"",
							"=\"" + finPeriod + "\"",
							dynamicsProjName,
							contractType,
							owningCompany,
							currency,
							String.Format("{0:M/d/yyyy}", reportDate),
							item.Laboratory.Location,
							item.BillingSheet.Replace(","," "),
							(numberOfItems != null)? numberOfItems.ToString() : "0",
							appFee.ToString(),
							String.IsNullOrEmpty(item.Note) ? "" : '\"' + item.Note.Replace("\"", "").Replace("*", "") + '\"'
						};

						var linesValue = Regex.Replace(string.Join(",", valuesList), "[^\\w\\s\\p{P}\\p{Sm}<>]+", String.Empty); //To remove special characters
						sw.Write(linesValue + ",");
						sw.Write(newLine);
					}
					catch (Exception e)
					{
						_logger.LogError(e, "Failed on letterbook with Id: " + item.Id.ToString());
						throw e;
					}
				}
				HttpContext.Response.Write(sw.ToString());
				HttpContext.Response.End();
			}
		}

		#region Batch Actions
		public PartialViewResult BatchActions()
		{
			var vm = new BatchActionsViewModel
			{
				Action = BatchAction.Add,
				BatchNumber = GetNextBatchNumber()
			};

			return PartialView("_BatchActions", vm);
		}

		[HttpPost]
		public ActionResult BatchActions(BatchActionsViewModel vm)
		{
			var letterBookIds = new List<int>();
			if (vm.Action != BatchAction.ExportSelected)
			{
				letterBookIds = vm.LetterBookIds.Split(',').Select(int.Parse).ToList();
			}

			switch (vm.Action)
			{
				case BatchAction.Add:
					_dbSubmission.tbl_LetterBook
						.Where(x => letterBookIds.Contains(x.Id))
						.Update(x => new tbl_LetterBook
						{
							BatchNumber = vm.BatchNumber,
							ACTStatus = LetterBookACTStatus.InProgress.ToString(),
							BillingUserId = _userContext.User.Id,
							BillingDate = DateTime.Now
						});
					break;
				case BatchAction.ExportNew:
					_dbSubmission.tbl_LetterBook
						.Where(x => letterBookIds.Contains(x.Id))
						.Update(x => new tbl_LetterBook
						{
							BatchNumber = vm.BatchNumber,
							ACTStatus = LetterBookACTStatus.InProgress.ToString(),
							BillingUserId = _userContext.User.Id,
							BillingDate = DateTime.Now
						});
					Export(vm.BatchNumber, null, null, null);
					break;
				case BatchAction.ExportSelected:
					Export(vm.BatchNumber, null, null, null);
					break;
				case BatchAction.Move:
					_dbSubmission.tbl_LetterBook
						.Where(x => letterBookIds.Contains(x.Id))
						.Update(x => new tbl_LetterBook
						{
							BatchNumber = vm.BatchNumber,
							BillingUserId = _userContext.User.Id,
							BillingDate = DateTime.Now
						});
					break;
				case BatchAction.Remove:
					_dbSubmission.tbl_LetterBook
						.Where(x => letterBookIds.Contains(x.Id))
						.Update(x => new tbl_LetterBook
						{
							BatchNumber = null,
							BillingUserId = _userContext.User.Id,
							BillingDate = DateTime.Now
						});
					break;
				case BatchAction.UpdateStatus:
					_dbSubmission.tbl_LetterBook
						.Where(x => letterBookIds.Contains(x.Id))
						.Update(x => new tbl_LetterBook
						{
							ACTStatus = vm.ACTStatus.ToString(),
							BillingUserId = _userContext.User.Id,
							BillingDate = DateTime.Now
						});
					break;
				default:
					break;
			}

			return RedirectToAction("Index");
		}

		[HttpGet]
		public ActionResult NextBatchNumber()
		{
			return Json(GetNextBatchNumber(), JsonRequestBehavior.AllowGet);
		}

		[HttpPost]
		public ActionResult ValidateBatchActions(BatchActionsViewModel vm)
		{

			var letterBookIds = new List<int>();
			if (vm.Action != BatchAction.ExportSelected)
			{
				letterBookIds = vm.LetterBookIds.Split(',').Select(int.Parse).ToList();
			}
			var validateMessage = "";

			switch (vm.Action)
			{
				case BatchAction.Add:
					if (_dbSubmission.tbl_LetterBook.Any(x => x.BatchNumber == vm.BatchNumber))
					{
						validateMessage += "Batch Number already exists<br />";
					}

					if (_dbSubmission.tbl_LetterBook.Any(x => letterBookIds.Contains(x.Id) && x.BatchNumber != null))
					{
						validateMessage += "Selected letters already belong to a batch<br />";
					}
					break;
				case BatchAction.ExportNew:
					if (_dbSubmission.tbl_LetterBook.Any(x => x.BatchNumber == vm.BatchNumber))
					{
						validateMessage += "Batch Number already exists<br />";
					}

					if (_dbSubmission.tbl_LetterBook.Any(x => letterBookIds.Contains(x.Id) && x.BatchNumber != null))
					{
						validateMessage += "Selected letters already belong to a batch<br />";
					}
					break;
				case BatchAction.ExportSelected:
					if (!_dbSubmission.tbl_LetterBook.Any(x => x.BatchNumber == vm.BatchNumber))
					{
						validateMessage += "Batch number provided does not exist<br />";
					}
					break;
				case BatchAction.Move:
					if (!_dbSubmission.tbl_LetterBook.Any(x => x.BatchNumber == vm.BatchNumber))
					{
						validateMessage += "Batch Number does not exist<br />";
					}
					break;
				case BatchAction.Remove:
					if (_dbSubmission.tbl_LetterBook.Any(x => letterBookIds.Contains(x.Id) && (x.ACTStatus == LetterBookACTStatus.Billed.ToString() || x.ACTStatus == LetterBookACTStatus.Closed.ToString())))
					{
						validateMessage += "Batch Number cannot be removed from letters with a Closed status<br />";
					}
					break;
				case BatchAction.UpdateStatus:
					break;
			}

			return Json(validateMessage, JsonRequestBehavior.AllowGet);
		}
		#endregion

		#region Helper Methods
		private int GetNextBatchNumber()
		{
			return (_dbSubmission.tbl_LetterBook.Select(x => x.BatchNumber).Max() ?? 999) + 1;
		}

		public int GetAcctAppFee(tbl_LetterBook item, bool isGatJur, int billableCount)
		{
			var billingSheet = item.BillingSheet;
			var billingSheetInfo = _dbSubmission.tbl_lst_BillingSheets.Where(x => x.Name == billingSheet).SingleOrDefault();

			var AppFee = 0;
			if (billingSheetInfo != null)
			{
				if (billingSheetInfo.BilledItem1AppFee != null && billableCount == 1)
				{
					AppFee = billingSheetInfo.BilledItem1AppFee.Amount;
				}
				else if (billingSheetInfo.BilledItem2AppFee != null && billableCount == 2)
				{
					AppFee = billingSheetInfo.BilledItem2AppFee.Amount;
				}
				else if (billingSheetInfo.BilledItem3AppFee != null && billableCount == 3)
				{
					AppFee = billingSheetInfo.BilledItem3AppFee.Amount;
				}
				else if (billingSheetInfo.AppFee != null)
				{
					AppFee = billingSheetInfo.AppFee.Amount;
				}

				if (billingSheetInfo.GetsGatChargeIfApplicable && isGatJur)
				{
					AppFee += 20; //Create a constant if it gets used many places later on
				}
			}

			return AppFee;
		}

		public AppFee GetAppFee(tbl_LetterBook item, bool isGatJur, int billableCount)
		{
			var appFee = AppFee.APPFEE000;// added checks for old names should be unnecessary eventually, or update the old data 
			if (item.BillingSheet == LetterBookBillingSheet.USNonSpecialized.ToString() ||
				item.BillingSheet == LetterBookBillingSheetOld.NonSpecialized.ToString())
			{
				switch (billableCount)
				{
					case 1:
						appFee = isGatJur ? AppFee.APPFEE519 : AppFee.APPFEE499;
						break;
					case 2:
						appFee = isGatJur ? AppFee.APPFEE770 : AppFee.APPFEE750;
						break;
					case 3:
						appFee = isGatJur ? AppFee.APPFEE1015 : AppFee.APPFEE995;
						break;
					default:
						break;
				}
			}
			else if (item.BillingSheet == LetterBookBillingSheet.USCorrectionAddendum.ToString() ||
				item.BillingSheet == LetterBookBillingSheetOld.CorrectionAddendum.ToString())
			{
				appFee = AppFee.APPFEE150;
			}
			else if (item.BillingSheet == LetterBookBillingSheet.USSpecialized.ToString() ||
				item.BillingSheet == LetterBookBillingSheetOld.Specialized.ToString())
			{
				// fix these
				if (item.ReportType == ReportTypes.ARIInstallationPersonality.GetEnumDescription())
				{
					appFee = isGatJur ? AppFee.APPFEE519 : AppFee.APPFEE499;
				}
				else if (item.ReportType == ReportTypes.KONPersonalitySoundGraphics.GetEnumDescription())
				{
					appFee = AppFee.APPFEE505;
				}
				else if (item.ReportType == ReportTypes.VGTInteroperability.GetEnumDescription())
				{
					appFee = AppFee.APPFEE130;
				}
				else if (item.ReportType == ReportTypes.OnSite_1_5.GetEnumDescription())
				{
					appFee = AppFee.APPFEE150;
				}
				else if (item.ReportType == ReportTypes.OnSite_6_10.GetEnumDescription())
				{
					appFee = AppFee.APPFEE300;
				}
				else if (item.ReportType == ReportTypes.OnSite_11_30.GetEnumDescription())
				{
					appFee = AppFee.APPFEE450;
				}
				else if (item.ReportType == ReportTypes.OnSite_31_Plus.GetEnumDescription())
				{
					appFee = AppFee.APPFEE600;
				}
				else if (item.ReportType == ReportTypes.OnSite_Jamaica.GetEnumDescription())
				{
					appFee = AppFee.APPFEE200;
				}

			}
			else if (item.BillingSheet == LetterBookBillingSheet.EUPriceLevel1.ToString())
			{
				appFee = AppFee.APPFEE275;
			}
			else if (item.BillingSheet == LetterBookBillingSheet.EUPriceLevel2.ToString())
			{
				appFee = AppFee.APPFEE345;
			}
			else if (item.BillingSheet == LetterBookBillingSheet.EUPriceLevel3.ToString())
			{
				appFee = AppFee.APPFEE690;
			}
			else if (item.BillingSheet == LetterBookBillingSheet.EUPriceLevel4.ToString())
			{
				appFee = AppFee.APPFEE1035;
			}
			else if (item.BillingSheet == LetterBookBillingSheet.EUPriceLevel5.ToString())
			{
				appFee = AppFee.APPFEE1250;
			}
			else if (item.BillingSheet == LetterBookBillingSheet.EUPriceLevel6.ToString())
			{
				appFee = AppFee.APPFEE1438;
			}
			else if (item.BillingSheet == LetterBookBillingSheet.EUPriceLevel7.ToString())
			{
				appFee = AppFee.APPFEE1840;
			}
			else if (item.BillingSheet == LetterBookBillingSheet.EUSpecialized.ToString())
			{
				if (item.ReportType == EUReport.Eur25.ToString())
				{
					appFee = AppFee.APPFEE025;
				}
				else if (item.ReportType == EUReport.Eur50.ToString())
				{
					appFee = AppFee.APPFEE050;
				}
				else if (item.ReportType == EUReport.Eur80.ToString())
				{
					appFee = AppFee.APPFEE080;
				}
				else if (item.ReportType == EUReport.Eur105.ToString())
				{
					appFee = AppFee.APPFEE105;
				}
				else if (item.ReportType == EUReport.Eur150.ToString())
				{
					appFee = AppFee.APPFEE150;
				}
				else if (item.ReportType == EUReport.Eur200.ToString())
				{
					appFee = AppFee.APPFEE200;
				}
				else if (item.ReportType == EUReport.Eur264.ToString())
				{
					appFee = AppFee.APPFEE264;
				}
				else if (item.ReportType == EUReport.Eur300.ToString())
				{
					appFee = AppFee.APPFEE300;
				}
				else if (item.ReportType == EUReport.Eur345.ToString())
				{
					appFee = AppFee.APPFEE345;
				}
				else if (item.ReportType == EUReport.Eur350.ToString())
				{
					appFee = AppFee.APPFEE350;
				}
				else if (item.ReportType == EUReport.Eur400.ToString())
				{
					appFee = AppFee.APPFEE400;
				}
				else if (item.ReportType == EUReport.Eur435.ToString())
				{
					appFee = AppFee.APPFEE435;
				}
				else if (item.ReportType == EUReport.Eur600.ToString())
				{
					appFee = AppFee.APPFEE600;
				}
				else if (item.ReportType == EUReport.Eur650.ToString())
				{
					appFee = AppFee.APPFEE650;
				}
				else if (item.ReportType == EUReport.Eur750.ToString())
				{
					appFee = AppFee.APPFEE750;
				}
				else if (item.ReportType == EUReport.Eur800.ToString())
				{
					appFee = AppFee.APPFEE800;
				}
				else if (item.ReportType == EUReport.Eur850.ToString())
				{
					appFee = AppFee.APPFEE850;
				}
				else if (item.ReportType == EUReport.Eur875.ToString())
				{
					appFee = AppFee.APPFEE875;
				}
				else if (item.ReportType == EUReport.Eur950.ToString())
				{
					appFee = AppFee.APPFEE950;
				}
				else if (item.ReportType == EUReport.Eur1000.ToString())
				{
					appFee = AppFee.APPFEE1000;
				}
				else if (item.ReportType == EUReport.Eur1050.ToString())
				{
					appFee = AppFee.APPFEE1050;
				}
			}
			var fileNumber = item.DocumentVersion.DocumentVersionLinks.FirstOrDefault().JurisdictionalData.Submission.FileNumber;
			if (fileNumber.StartsWith("RN"))
			{
				appFee = AppFee.APPFEE995;
			}
			else if (fileNumber.StartsWith("LO") || fileNumber.StartsWith("MA"))
			{
				appFee = AppFee.APPFEE499;
			}
			return appFee;
		}

		private IEnumerable<SelectListItem> LoadAvailableStatuses(BookEditViewModel vm)
		{
			if (vm.IsReviewUser)
			{
				return LookupsStandard.ConvertEnum<LetterBookQAStatus>();
			}
			else if (vm.IsEntryUser)
			{
				var statusList = new List<LetterBookQAStatus>
				{
					LetterBookQAStatus.InProgress,
					LetterBookQAStatus.PendingApproval
				};
				if (vm.Mode == Mode.Edit && vm.ReviewDate != null)
				{
					statusList.Add(LetterBookQAStatus.Rejected);
				}
				return LookupsStandard.ConvertEnum<LetterBookQAStatus>(enumValues: statusList.ToArray());
			}
			else
			{
				return LookupsStandard.ConvertEnum<LetterBookACTStatus>();
			}
		}
		private List<string> SplitForComments(string gameComment)
		{
			var line1 = "";
			var line2 = "";
			var line3 = "";

			if (!string.IsNullOrWhiteSpace(gameComment))
			{
				gameComment = gameComment.Replace("\"", "").Replace("*", "");
				line1 = '\"' + (gameComment.Substring(0, Math.Min(gameComment.Length, 100))) + '\"';
				line2 = '\"' + (gameComment.Length > 100 ? gameComment.Substring(100, Math.Min(gameComment.Length - 100, 30)) : "") + '\"';
				line3 = '\"' + (gameComment.Length > 130 ? gameComment.Substring(130, Math.Min(gameComment.Length - 130, 30)) : "") + '\"';
			}

			var commentsList = new List<string> { line1, line2, line3 };
			return commentsList;
		}
		private DateTime GetPreviousBusinessDay()
		{
			var today = DateTime.Today;
			if (today.DayOfWeek == DayOfWeek.Monday)
			{
				return today.AddDays(-3);
			}
			return today.AddDays(-1);

		}
		private List<SelectListItem> ProjectEnumIntoSelectList(Enum enumToConvert)
		{
			var enumType = enumToConvert.GetType();
			var enumValues = Enum.GetValues(enumType);
			return (from object enumValue in enumValues select new SelectListItem() { Value = ((int)enumValue).ToString(), Text = enumValue.GetEnumDescription() }).ToList();
		}
		#endregion
	}
}