﻿using EF.eResultsManaged;
using EF.Submission;
using Submissions.Common;
using Submissions.Core.Interfaces.LetterGen;
using Submissions.Core.Models;
using Submissions.Web.Areas.Documents.Models.Generate;
using Submissions.Web.Areas.Evolution.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace Submissions.Web.Areas.Documents.Controllers
{
	public class ConformanceCriteriaController : Controller//
	{
		private readonly ISubmissionContext _dbSubmission;
		private readonly IeResultsManagedContext _dbeResultsManaged;
		private readonly IUserContext _userContext;
		private readonly ILetterGenerationService _letterGenerationService;

		public ConformanceCriteriaController(
			ILetterGenerationService letterGenerationService,
			ISubmissionContext dbSubmission,
			IeResultsManagedContext dbeResultsManaged,
			IUserContext userContext
			)
		{
			_dbSubmission = dbSubmission;
			_dbeResultsManaged = dbeResultsManaged;
			_userContext = userContext;
			_letterGenerationService = letterGenerationService;

		}

		public ActionResult Index(int bundleId)
		{
			var vm = new ConformanceCriteriaIndexViewModel();
			var bundle = _dbSubmission.QABundles.Find(bundleId);
			var jurIds = SetJurisdictions(vm, bundleId);

			SetProjectInfo(vm, bundle, jurIds);
			var clauses = _letterGenerationService.GetClauseReportsForProject(bundle.ProjectId, jurIds);
			SetCCDocuments(vm, clauses);
			var secondaryLanguage = clauses.Where(x => x.IsNativeDataExists).Select(x => x.Language).FirstOrDefault();
			vm.Letter = BuildLetter(secondaryLanguage, jurIds);
			return View(vm);
		}

		private void SetCCDocuments(ConformanceCriteriaIndexViewModel vm, IList<ClauseReportModel> clauseReportModels)
		{
			foreach (var clauseGroup in clauseReportModels.GroupBy(cr => cr.JurisdictionId))
			{
				vm.ClauseReports.Add(new ClauseReportViewModel()
				{
					ClauseReports = clauseGroup.ToList(),
					PassCount = clauseGroup.DistinctBy(c => c.ClauseID).Count(x => x.Answer == "PASS"),
					FailCount = clauseGroup.DistinctBy(c => c.ClauseID).Count(x => x.Answer == "FAIL"),
					NACount = clauseGroup.DistinctBy(c => c.ClauseID).Count(x => x.Answer == "NA"),
					NAStarCount = clauseGroup.DistinctBy(c => c.ClauseID).Count(x => x.Answer == "NA*"),
					JurisdictionId = clauseGroup.Key
				});
			}
		}

		private void SetProjectInfo(ConformanceCriteriaIndexViewModel vm, QABundle bundle, List<int> jurIds)
		{
			var fileNum = new FileNumber(bundle.ProjectName);
			vm.ProjectInfo = new GenerateLetterProjectInfo()
			{
				BundleId = bundle.Id,
				BundleName = bundle.ProjectName,
				FileNumber = bundle.ProjectName ?? "Project Not Found",
				ManufacturerId = fileNum.ManufacturerCode,
				ManufacturerName = bundle.ProjectName,
				JurisdictionIds = jurIds,
				ProjectId = bundle.ProjectId
			};
		}

		private List<int> SetJurisdictions(ConformanceCriteriaIndexViewModel vm, int locBundleId)
		{
			var jurs = _dbSubmission.QABundles.Find(locBundleId).Project.JurisdictionalData
						.Select(juris => new { juris.JurisdictionalData.JurisdictionId, juris.JurisdictionalData.JurisdictionName, juris.JurisdictionalData.SubmissionId })
						.ToList();
			int trash;
			var jurIds = jurs.Select(jur => jur.JurisdictionId)
						.Distinct()
						.Where(juris => int.TryParse(juris, out trash))
						.Select(juris => int.Parse(juris))
						.ToList();

			vm.LetterJurisdictionOptions = jurs.GroupBy(jur => jur.JurisdictionId)
								  .Select(j => j.First())
								  .Select(j => new SelectListItem()
								  {
									  Value = j.JurisdictionId,
									  Text = j.JurisdictionName + " (" + j.JurisdictionId + ")",
								  })
								  .ToList();

			vm.JurisdictionData = jurs.GroupBy(jur => jur.JurisdictionId)
								.Select(j => j.First()).Select(x => new GenerateJurisdictionModel()
								{
									JurisdictionId = int.Parse(x.JurisdictionId),
									Name = x.JurisdictionName,
								}).Distinct().ToList();

			return jurIds;
		}

		private GenerateLetter BuildLetter(string secondaryLanguage, List<int> applicableJurs)
		{
			int SectionId = 1000;
			int MemoId = 2000;
			var letter = new GenerateLetter
			{
				Sections = new List<GenerateLetterSection>()
			};

			letter.Sections.Add(
					new GenerateLetterSection()
					{
						Name = "Evolution Clauses",
						Description = "Clause related questions.",
						Id = SectionId,
						OrderId = SectionId,
						Memos = new List<GenerateLetterMemo>()
						{
							new GenerateLetterMemo()
							{
								ApplicableJurisdictions = applicableJurs,
								Id = MemoId,
								Name = "Conformance Criteria",
								Description = "The standard Conformance Criteria table.",
								//LetterContent = "<table class=\"default\" style=\"width:604px;\"><template v-for=\"(item, itemIndex) in sourceItems\"><template v-if=\"itemIndex === 0 || item.sectionName !== sourceItems[itemIndex-1].sectionName\"><tr class=\"default-header-background\" style='page-break-inside:avoid;page-break-after:avoid;'><td class=\"default-header-border default-header-padding\" style=\"width:100%;\" colspan=4><p class=\"default-header-font default-margins\">{{ item.sectionName }} ({{ item.documentName }})</p></td></tr><tr class=\"default-header-background\" style='page-break-inside:avoid;page-break-after:avoid;page-break-before:avoid;'><td class=\"default-header-border default-header-padding\" style=\"width:60%;\"  ><p class=\"default-header-font default-margins align-center\">Text</p></td><td class=\"default-header-border default-header-padding\" style=\"width:5%;\"><p class=\"default-header-font default-margins align-center\">Applicable?</p></td><td class=\"default-header-border default-header-padding\" style=\"width:5%;\"><p class=\"default-header-font default-margins align-center\">Pass/Fail</p></td><td class=\"default-header-border\" style=\"width:30%;\"><p class=\"default-header-font default-margins align-center\">Internal comments</p></td></tr></template><tr style='page-break-inside:avoid;page-break-before:avoid;'><td class=\"default-border default-margins default-padding default-word-break\"><p class=\"default-font\">{{ item.section }} {{ item.memo }}</p></td><td class=\"default-border default-margins default-padding default-word-break\"><p class=\"default-font align-center\">Applicable</p></td><td class=\"default-border default-margins default-padding default-word-break\"><p class=\"default-font align-center\">{{ item.answer }}</p></td><td class=\"default-border default-margins default-padding default-word-break\"><p class=\"default-font\">{{ item.explanation }}</p></td></tr></template></table>",
								LetterContent = "<table><template v-for=\"(item, itemIndex) in source\"><template v-if=\"itemIndex === 0 || item.sectionName !== sourceItems[itemIndex-1].sectionName\"><tr class=\"default-header-background\" ><td colspan=4>{{ item.sectionName }} ({{ item.documentName }})</td></tr><tr class=\"default-header-background\"><td>Text</td><td width=\"95px\">Applicable?</td><td width=\"75\">Pass/Fail</td><td width=\"150px\">Internal comments</td></tr></template><tr><td >{{ item.section	}}{{ item.memo }}</td><td> Applicable </td><td>{{ item.answer }}</td><td>{{ item.explanation }}</td></tr></template></table>",
								Include = true,
								DefaultInclude = true,
								Removable = false,
								Custom = false,
								SplittableFields = new List<string>() { "memo" },
								SourceId = "ClauseReports",
								SourceItemTargets = new List<SourceItemTarget>(),
								RepeatableContentControls = new List<GenerateLetterInputControl>() {
									new GenerateLetterInputControl()
									{
										SourceId = "ClauseReports",
										SourceProperty = "Clause",
										Type = GenerateLetterInputControlType.STATIC.ToString(),
										Id = 1000,
										LocalId = System.Guid.NewGuid(),
										SourceFilter = "true",
										RepeatableContent = true
									},
								},
								SecondaryLanguage = secondaryLanguage,
								SecondaryLanguageId = 1,
								SecondaryLanguageLetterContent = "<table><template v-for=\"(item, itemIndex) in sourceItems\"><template v-if=\"itemIndex === 0 || item.sectionName !== sourceItems[itemIndex-1].sectionName\"><tr class=\"default-header-background\" ><td colspan=4>{{ item.nativeSectionName }} ({{ item.documentName }})</td></tr><tr class=\"default-header-background\"><td>Text</td><td width=\"95px\">Applicable?</td><td width=\"75\">Pass/Fail</td><td width=\"150px\">Internal comments</td></tr></template><tr><td >{{ item.section	}}{{ item.nativeMemo }}</td><td> Applicable </td><td>{{ item.answer }}</td><td>{{ item.explanation }}</td></tr></template></table>",
							}
						}
					});

			//below needs it to work though we dont use paging
			letter.Sections.SelectMany(sec => sec.Memos).ToList().ForEach((memo) =>
			{
				var localId = Guid.NewGuid();
				memo.Id = MemoId++;
				memo.LocalId = localId;
				memo.OrderId = MemoId++;
				memo.Paging = new List<GenerateLetterPaging>();
				foreach (var jur in memo.ApplicableJurisdictions)
				{
					memo.Paging.Add(new GenerateLetterPaging()
					{
						JurisdictionId = jur,
						LocalId = localId,
						Page = 1
					});
				}
			});

			return letter;
		}
	}
}
