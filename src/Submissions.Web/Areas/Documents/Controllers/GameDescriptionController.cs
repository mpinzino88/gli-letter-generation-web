﻿using EF.Submission;
using Submissions.Common;
using Submissions.Core.Interfaces;
using Submissions.Core.Models.GameDescriptionService;
using Submissions.Web.Areas.Documents.Models.GameDescription;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace Submissions.Web.Areas.Documents.Controllers
{
	public class GameDescriptionController : Controller
	{
		private readonly IGameDescriptionService _gameDescriptionService;
		private readonly ISubmissionContext _dbSubmissions;

		public GameDescriptionController(IGameDescriptionService gameDescriptionService, ISubmissionContext dbSubmissions)
		{
			_gameDescriptionService = gameDescriptionService;
			_dbSubmissions = dbSubmissions;
		}

		public ActionResult Index()
		{
			return View(new GameDescriptionIndexViewModel());
		}
		public ActionResult Edit(int? id, int? submissionId, int? projectId, string type, int? letterGenProjectId, int? copyOf)
		{
			var vm = new GameDescriptionEditViewModel();

			vm.Id = id;
			vm.SubmissionId = submissionId;
			vm.ProjectId = projectId;
			vm.ProjectName = projectId != null ? _dbSubmissions.trProjects.Where(x => x.Id == projectId).Single().ProjectName : string.Empty;
			vm.Type = type;
			vm.LetterGenProjectId = letterGenProjectId;
			vm.LetterGenProjectName = letterGenProjectId != null ? _dbSubmissions.trProjects.Where(x => x.Id == letterGenProjectId).Single().ProjectName : string.Empty;
			vm.CopyOf = copyOf;
			return View(vm);
		}

		[HttpPost]
		public ContentResult SaveGameDescription(GameDescriptionViewModel vm)
		{
			int result = 0;

			// Edit
			if (vm.Id > 0)
			{
				result = _gameDescriptionService.EditGameDescription(BuildGameDescriptionDTO(vm));
			}
			// Add
			else
			{
				result = _gameDescriptionService.AddGameDescription(BuildGameDescriptionDTO(vm));
			}

			return Content(ComUtil.JsonEncodeCamelCase(result), "application/json");
		}

		[HttpPost]
		public ContentResult GetGameDescription(GameDescriptionSearchFilter filter)
		{
			var result = new GameDescriptionViewModel();
			// Edit
			if (filter.Id > 0)
			{
				result = BuildGameDescriptionViewModel(_gameDescriptionService.GetGameDescription(filter));
			}
			// Add
			else
			{
				if (filter.CopyOf != null)
				{
					var gameDescription = _dbSubmissions.GameDescriptions.Find(filter.CopyOf);
					result.Name = gameDescription.Name;
					result.DescriptionContent = gameDescription.DescriptionContent;
				}
				var descriptionType = (GameDescriptionType)Enum.Parse(typeof(GameDescriptionType), filter.Type, true);
				var descriptionVal = (int)descriptionType;
				result.Type = new SelectListItem()
				{
					Text = descriptionType.ToString(),
					Value = descriptionVal.ToString()
				};
				result.ProjectId = filter.ProjectId;
				result.SubmissionId = filter.SubmissionId;
				result.JurisdictionOptions = BuildGameDescriptionJurisdictionViewModels(_gameDescriptionService.GetAvailableJurisdictions(filter));
			}
			return Content(ComUtil.JsonEncodeCamelCase(result), "application/json");
		}

		[HttpPost]
		public ContentResult GetGameDescriptions(GameDescriptionSearchFilter filter)
		{
			var model = _gameDescriptionService.GetGameDescriptionSubmissionGroups(filter);

			return Content(ComUtil.JsonEncodeCamelCase(model), "application/json");
		}

		[HttpPost]
		public ContentResult DeleteGameDescription(int gameDescriptionId)
		{
			var result = true;

			try
			{
				_gameDescriptionService.DeleteGameDescription(gameDescriptionId);
			}
			catch
			{
				result = false;
			}

			return Content(ComUtil.JsonEncodeCamelCase(result), "application/json");
		}

		#region Builders
		private GameDescriptionDTO BuildGameDescriptionDTO(GameDescriptionViewModel vm)
		{
			var result = new GameDescriptionDTO()
			{
				Id = vm.Id,
				Active = vm.Active,
				CreatedBy = vm.CreatedBy,
				CreatedById = vm.CreatedById,
				CreatedOn = vm.CreatedOn,
				DescriptionContent = vm.DescriptionContent,
				LastUpdatedBy = vm.LastUpdatedBy,
				LastUpdatedById = vm.LastUpdatedById,
				LastUpdatedOn = vm.LastUpdatedOn,
				Name = vm.Name,
				ProjectId = vm.ProjectId,
				ModificationTypeSelection = vm.ModificationTypeSelection != null && vm.ModificationTypeSelection.Value != null ? Int32.Parse(vm.ModificationTypeSelection.Value) : (int?)null,
				PreviousComponents = vm.PreviouslyApprovedComponents,
				Type = new KeyValuePair<int, GameDescriptionType>(int.Parse(vm.Type.Value), (GameDescriptionType)Enum.Parse(typeof(GameDescriptionType), vm.Type.Text, true)),
				ApplicableJurisdictions = vm.JurisdictionSelection.Select(x => new GameDescriptionJurisdictionDTO { Id = x.Id, JurisdictionId = x.JurisdictionId, JurisdictionName = x.JurisdictionName, SubmissionId = x.SubmissionId }).ToList()
			};

			return result;
		}
		private GameDescriptionViewModel BuildGameDescriptionViewModel(GameDescriptionDTO gameDescription)
		{
			var result = new GameDescriptionViewModel
			{
				Active = gameDescription.Active,
				CreatedBy = gameDescription.CreatedBy,
				CreatedById = gameDescription.CreatedById,
				CreatedOn = gameDescription.CreatedOn,
				DescriptionContent = gameDescription.DescriptionContent,
				Id = gameDescription.Id,
				LastUpdatedBy = gameDescription.LastUpdatedBy,
				LastUpdatedById = gameDescription.LastUpdatedById,
				LastUpdatedOn = gameDescription.LastUpdatedOn,
				Name = gameDescription.Name,
				ProjectId = gameDescription.ProjectId,
				SubmissionId = gameDescription.ApplicableJurisdictions.FirstOrDefault().SubmissionId,
				Type = new SelectListItem() { Text = gameDescription.Type.Value.ToString(), Value = gameDescription.Type.Key.ToString() },
				JurisdictionSelection = BuildGameDescriptionJurisdictionViewModels(gameDescription.ApplicableJurisdictions),
				JurisdictionOptions = BuildGameDescriptionJurisdictionViewModels(gameDescription.ApplicableJurisdictions.Concat(gameDescription.AvailableJurisdictions).ToList()),
				ModificationTypeSelection = new SelectListItem()
				{
					Value = gameDescription.ModificationTypeSelection != null ? gameDescription.ModificationTypeSelection.ToString() : null,
					Text = gameDescription.ModificationTypeSelection != null ? ((ModificationType)gameDescription.ModificationTypeSelection).GetEnumDescription() : null,
					Selected = true
				},
				PreviouslyApprovedComponents = gameDescription.PreviousComponents
			};

			return result;
		}
		private List<GameDescriptionJurisdictionViewModel> BuildGameDescriptionJurisdictionViewModels(List<GameDescriptionJurisdictionDTO> gameDescriptionJurisdictionDTOs)
		{
			var result = new List<GameDescriptionJurisdictionViewModel>();
			foreach (var gameDescJuri in gameDescriptionJurisdictionDTOs)
			{
				result.Add(BuildGameDescriptionJurisdictionViewModel(gameDescJuri));
			}
			return result;
		}
		private GameDescriptionJurisdictionViewModel BuildGameDescriptionJurisdictionViewModel(GameDescriptionJurisdictionDTO gameDescriptionJurisdictionDTO)
		{
			var result = new GameDescriptionJurisdictionViewModel()
			{
				Id = gameDescriptionJurisdictionDTO.Id,
				JurisdictionId = gameDescriptionJurisdictionDTO.JurisdictionId,
				JurisdictionName = gameDescriptionJurisdictionDTO.JurisdictionName,
				SubmissionId = gameDescriptionJurisdictionDTO.SubmissionId
			};
			return result;
		}
		#endregion
	}
}