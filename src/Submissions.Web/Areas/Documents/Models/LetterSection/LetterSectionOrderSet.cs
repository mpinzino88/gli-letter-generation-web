﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Submissions.Web.Areas.Documents.Models.LetterSection
{
	public class LetterSectionOrderSet
	{
		public LetterSectionOrderSet()
		{
			LetterSections = new List<LetterSectionOrderItem>();
		}
		public List<LetterSectionOrderItem> LetterSections { get; set; }
	}

	public class LetterSectionOrderItem
	{
		public int Id { get; set; }
		public string Name { get; set; }
		public int? CurrentLocation { get; set; }
		public int? TargetLocation { get; set; }
	}
}