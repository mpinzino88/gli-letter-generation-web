﻿using GLI.EF.LetterContent;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;

namespace Submissions.Web.Areas.Documents.Models.LetterSection
{
	public class LetterSectionIndexViewModel
	{
		public List<LetterSectionIndexListItem> LetterSections { get; set; }
		public LetterSectionOrderSet LetterSectionOrderSet { get; set; }
		public bool IsNoteManagerAdmin { get; set; }
	}

	public class LetterSectionIndexListItem
	{
		public static Expression<Func<Section, LetterSectionIndexListItem>> Projection
		{
			get
			{
				return e => new LetterSectionIndexListItem
				{
					Id = e.Id,
					Name = e.Name,
					OrderId = e.OrderId
				};
			}
		}
		public int Id { get; set; }
		public string Name { get; set; }
		public int? OrderId { get; set; }
	}
}