﻿using GLI.EF.LetterContent;
using Submissions.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;

namespace Submissions.Web.Areas.Documents.Models.LetterSection
{
	public class LetterSectionEditViewModel
	{
		public LetterSectionEditViewModel()
		{
			LetterSection = new LetterSectionEditItem();
		}
		public Mode Mode { get; set; }
		public LetterSectionEditItem LetterSection { get; set; }
	}

	public class LetterSectionEditItem
	{
		public LetterSectionEditItem()
		{
			LetterContents = new List<LetterContentItem>();
		}

		public static Expression<Func<Section, LetterSectionEditItem>> Projection
		{
			get
			{
				return e => new LetterSectionEditItem
				{
					Id = e.Id,
					Name = e.Name,
					Active = e.Active,
					OrderId = e.OrderId,
					LetterContents = e.LetterContents.Where(x => x.Lifecycle == Lifecycle.Active).Select(x => new LetterContentItem
					{
						Id = x.Id,
						Name = x.Name,
						InitialPosition = x.OrderId
					})
					.OrderBy(x => x.InitialPosition)
					.ToList()
				};
			}
		}

		public int Id { get; set; }
		public string Name { get; set; }
		public bool Active { get; set; }
		public int? OrderId { get; set; }
		public List<LetterContentItem> LetterContents { get; set; }
	}

	public class LetterContentItem
	{
		public int Id { get; set; }
		public string Name { get; set; }
		public int? InitialPosition { get; set; }
		public int? NewPosition { get; set; }
	}

	public class LetterContentOrderPayload
	{
		public LetterContentOrderPayload()
		{
			LetterContentItems = new List<LetterContentItem>();
		}
		public int SectionId { get; set; }
		public List<LetterContentItem> LetterContentItems { get; set; }
	}

}