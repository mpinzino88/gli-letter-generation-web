﻿using Submissions.Web.Areas.Evolution.Models;
using System.Collections.Generic;
using System.Web.Mvc;

namespace Submissions.Web.Areas.Documents.Models.Generate
{
	public class ConformanceCriteriaIndexViewModel
	{
		public ConformanceCriteriaIndexViewModel()
		{
			this.Bundles = new List<SelectListItem>();
			ClauseReports = new List<ClauseReportViewModel>();
		}

		#region Letter Objects
		public GenerateLetter Letter { get; set; }
		public GenerateLetterProjectInfo ProjectInfo { get; set; }
		#endregion

		#region Sources
		public IList<ClauseReportViewModel> ClauseReports { get; set; }
		#endregion

		#region Model Properties
		public IList<GenerateJurisdictionModel> JurisdictionData { get; set; }
		#endregion

		#region Submission Models
		public IList<SelectListItem> Bundles { get; set; }
		#endregion

		#region Drop-down options
		public IList<SelectListItem> LetterJurisdictionOptions { get; set; }
		#endregion
	}
}