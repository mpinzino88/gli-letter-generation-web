﻿using GLI.EF.LetterContent;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Submissions.Web.Areas.Documents.Models.LetterQuestion
{
	public class LetterQuestionIndexViewModel
	{
		public List<LetterQuestionIndexListItem> Questions { get; set; }
	}

	public class LetterQuestionIndexListItem
	{
		public static Expression<Func<Question, LetterQuestionIndexListItem>> Projection
		{
			get
			{
				return e => new LetterQuestionIndexListItem
				{
					Id = e.Id,
					Name = e.Name,
					QuestionText = e.QuestionText,
					Attached = e.Memos.Any()
				};
			}
		}

		public int Id { get; set; }
		public string Name { get; set; }
		public string QuestionText { get; set; }
		public bool Attached { get; set; }
	}
}