using GLI.EF.LetterContent;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Web.Mvc;

namespace Submissions.Web.Areas.Documents.Models.LetterQuestion
{
	public class LetterQuestionEditViewModelContainer
	{
		public LetterQuestionEditViewModel Question { get; set; }

		public bool IsNewQuestion { get; set; }

		public Dictionary<int, string> InputTypes { get; set; }
		public Dictionary<int, string> LookupTypes { get; set; }
		public Dictionary<int, string> Sources { get; set; }
		public Dictionary<int, string> Questions { get; set; }
		public bool IsAttached { get; internal set; }
	}

	public class LetterQuestionEditViewModel
	{
		public LetterQuestionEditViewModel() { }

		public LetterQuestionEditViewModel(Question match)
		{
			Name = match.Name;
			DefaultValue = match.DefaultValue;
			QuestionSpecialFlags = match.QuestionSpecialFlags;
			QuestionText = match.QuestionText;
			Id = match.Id;
			SourceId = (int)match.Source;
			TypeId = (int)match.Type;
			ReferenceLookup = match.ReferenceLookup;
			OptionDelimiter = new SelectListItem { Value = match.OptionDelimiter };
			var opts = match.QuestionOptions ?? "";
			var questionOpts = (dynamic)JsonConvert.DeserializeObject(opts);
			QuestionOptions = questionOpts ?? new List<dynamic>();
			IsRequired = match.IsRequired;
			ParentQuestionId = match.ParentQuestionId;
			ConditionalExpression = match.ConditionalExpression;

			var tokenKey = match.TokenKey.StartsWith("token_") ? match.TokenKey.Substring(6) : match.TokenKey;
			TokenKey = tokenKey;
		}

		/// <summary>
		/// The question's text. This is admin-facing - the Description property is what gets shown to the letter generator user.
		/// </summary>
		public string Name { get; set; }

		/// <summary>
		/// Converted to a series of boolean values for the viewmodel
		/// </summary>
		public QuestionSpecialFlags QuestionSpecialFlags
		{
			get
			{
				QuestionSpecialFlags ret = QuestionSpecialFlags.None;
				if (IsBusinessLogic)
				{
					ret |= QuestionSpecialFlags.IsBusinessLogic;
				}
				if (IsRepeatableContent)
				{
					ret |= QuestionSpecialFlags.IsRepeatableContent;
				}
				if (IsSplittable)
				{
					ret |= QuestionSpecialFlags.Splittable;
				}
				return ret;
			}

			set
			{
				IsBusinessLogic = value.HasFlag(QuestionSpecialFlags.IsBusinessLogic);
				IsRepeatableContent = value.HasFlag(QuestionSpecialFlags.IsRepeatableContent);
				IsSplittable = value.HasFlag(QuestionSpecialFlags.Splittable);
			}
		}

		/// <summary>
		/// Formerly Description. This is user-facing - the Name property is the name of the question in admin contexts.
		/// </summary>
		public string QuestionText { get; set; }

		/// <summary>
		/// Primary key for the question. If null, this is a new question.
		/// </summary>
		public int? Id { get; set; }

		/// <summary>
		/// 
		/// </summary>
		public int SourceId { get; set; }

		/// <summary>
		/// The variable/property name for this question
		/// </summary>
		public string TokenKey { get; set; }

		/// <summary>
		/// 
		/// </summary>
		public int TypeId { get; set; }

		/// <summary>
		/// </summary>
		public string ReferenceLookup { get; set; }

		/// <summary>
		/// Ties into QuestionSpecialFlags. Should **NOT** editable for end user, but we need to track the value to rebuild the flag
		/// </summary>
		public bool IsBusinessLogic { get; set; }

		/// <summary>
		/// Ties into QuestionSpecialFlags
		/// </summary>
		public bool IsRepeatableContent { get; set; }

		/// <summary>
		/// Ties into QuestionSpecialFlags
		/// </summary>
		public bool IsSplittable { get; set; }

		public dynamic QuestionOptions { get; set; }
		public SelectListItem OptionDelimiter { get; set; }

		/// <summary>
		/// Use this as a default value for the question
		/// </summary>
		public string DefaultValue { get; set; }

		/// <summary>
		/// Makes it required to answer on letter gen
		/// </summary>
		public bool IsRequired { get; set; }
		/// <summary>
		///Parent Question
		/// </summary>
		public int? ParentQuestionId { get; set; }

		/// <summary>
		/// Expression based on Parent question answer
		/// </summary>
		public string ConditionalExpression { get; set; }
	}
}