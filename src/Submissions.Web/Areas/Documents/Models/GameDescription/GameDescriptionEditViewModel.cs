﻿using Submissions.Common;
using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace Submissions.Web.Areas.Documents.Models.GameDescription
{
	public class GameDescriptionEditViewModel
	{
		public List<SelectListItem> DescriptionTypes
		{
			get
			{
				var result = new List<SelectListItem>();

				foreach (var type in Enum.GetNames(typeof(GameDescriptionType)))
				{
					var typeVal = (int)Enum.Parse(typeof(GameDescriptionType), type);
					result.Add(new SelectListItem { Value = typeVal.ToString(), Text = type });
				}

				return result;
			}
		}

		public List<SelectListItem> ModificationTypes
		{
			get
			{
				var result = new List<SelectListItem>();

				foreach (var type in Enum.GetNames(typeof(ModificationType)))
				{
					var description = Enum.Parse(typeof(ModificationType), type).GetEnumDescription();
					var typeVal = (int)Enum.Parse(typeof(ModificationType), type);
					result.Add(new SelectListItem { Value = typeVal.ToString(), Text = description });
				}

				return result;
			}
		}

		public int? Id { get; set; }
		public int? SubmissionId { get; set; }
		public int? ProjectId { get; set; }
		public string ProjectName { get; set; }
		public string Type { get; set; }
		public int? LetterGenProjectId { get; set; }
		public string LetterGenProjectName { get; set; }
		public int? CopyOf { get; set; }
	}
}