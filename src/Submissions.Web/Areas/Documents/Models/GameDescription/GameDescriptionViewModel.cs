﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace Submissions.Web.Areas.Documents.Models.GameDescription
{
	public class GameDescriptionViewModel
	{
		public GameDescriptionViewModel()
		{
			Type = new SelectListItem();
			ModificationTypeSelection = new SelectListItem();
			JurisdictionSelection = new List<GameDescriptionJurisdictionViewModel>();
			JurisdictionOptions = new List<GameDescriptionJurisdictionViewModel>();
		}

		public int Id { get; set; }
		public string Name { get; set; }
		public DateTime CreatedOn { get; set; }
		public int CreatedById { get; set; }
		public string CreatedBy { get; set; }
		public DateTime LastUpdatedOn { get; set; }
		public int LastUpdatedById { get; set; }
		public string LastUpdatedBy { get; set; }
		public string DescriptionContent { get; set; }
		public bool Active { get; set; }
		public int ProjectId { get; set; }
		public int? SubmissionId { get; set; }
		public SelectListItem Type { get; set; }
		public SelectListItem ModificationTypeSelection { get; set; }
		public string PreviouslyApprovedComponents { get; set; }
		public List<GameDescriptionJurisdictionViewModel> JurisdictionSelection { get; set; }
		public List<GameDescriptionJurisdictionViewModel> JurisdictionOptions { get; set; }
	}

	public class GameDescriptionJurisdictionViewModel
	{
		public int Id { get; set; }
		public int SubmissionId { get; set; }
		public string JurisdictionId { get; set; }
		public string JurisdictionName { get; set; }
	}
}