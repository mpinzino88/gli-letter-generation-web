﻿using Submissions.Common;
using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace Submissions.Web.Areas.Documents.Models.GameDescription
{
	public class GameDescriptionIndexViewModel
	{
		public List<SelectListItem> DescriptionTypes
		{
			get
			{
				var result = new List<SelectListItem>();

				foreach (var type in Enum.GetNames(typeof(GameDescriptionType)))
				{
					var typeVal = (int)Enum.Parse(typeof(GameDescriptionType), type);
					result.Add(new SelectListItem { Value = typeVal.ToString(), Text = type });
				}

				return result;
			}
		}
	}
}