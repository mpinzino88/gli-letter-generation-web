﻿using EF.GPS;
using GLI.EFCore.Submission;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Submissions.Web.Areas.Documents.Models
{
	public class JurisdictionalDataGroup
	{
		public JurisdictionalDataGroup(List<JurisdictionalData> jurisdictionalDatas)
		{
			var allJurs = jurisdictionalDatas.Select(jd => jd.Jurisdiction).Distinct();
			if (!allJurs.Any())
			{
				throw new Exception("No Jurisdictions found for this JurisdictionalDataGroup");
			}
			if (allJurs.Count() > 1)
			{
				throw new Exception("All JurisdictionalDatas passed into JurisdictionalDataGroup must belong the same Jurisdiction");
			}
			if (jurisdictionalDatas.Select(jd => jd.Status).Distinct().Count() > 1)
			{
				throw new Exception("All JurisdictionalDatas passed into JurisdictionalDataGroup must have the same current status.");
			}
			Jurisdiction = allJurs.FirstOrDefault();
			JurisdictionalDatas = jurisdictionalDatas;//AutoMapper.Mapper.Map<List<JurisdictionalDataViewModel>>(jurisdictionalDatas);
			JurisdictionName = this.Jurisdiction.Name;
			JurisdictionId = this.Jurisdiction.Id;
			CurrentJurisdictionalStatus = jurisdictionalDatas.First().Status;
			PdfPath = jurisdictionalDatas.First().PdfPath;
			CloseDate = jurisdictionalDatas.First().CloseDate;
			UpdatedDate = jurisdictionalDatas.First().UpdateDate;
			UpdateReasonId = jurisdictionalDatas.First().UpdateReasonId;
			DraftDate = jurisdictionalDatas.First().DraftDate;
			CertificationLabId = jurisdictionalDatas.First().CertificationLabId;
			ContractTypeId = jurisdictionalDatas.First().ContractTypeId;
			BillingParty = jurisdictionalDatas.First().PurchaseOrder;
		}
		public tbl_lst_fileJuris Jurisdiction { get; set; }
		public List<JurisdictionalData> JurisdictionalDatas { get; set; }
		public bool HasExistingCertification { get; internal set; }
		public List<GPSItem> AvailableSourceDocs { get; internal set; }
		public string JurisdictionName { get; internal set; }
		public string JurisdictionId { get; internal set; }
		public string CurrentJurisdictionalStatus { get; internal set; }
		public string PdfPath { get; set; }
		public DateTime? CloseDate { get; set; }
		public DateTime? UpdatedDate { get; set; }
		public DateTime? DraftDate { get; set; }
		public int? UpdateReasonId { get; set; }
		public int? CertificationLabId { get; set; }
		public int? ContractTypeId { get; set; }
		public string BillingParty { get; set; }
	}

	public class JurisdictionalDataViewModel
	{
		public JurisdictionalDataViewModel()
		{
		}
		public string JurisdictionId { get; set; }
		public int? SubmissionId { get; set; }
		public int Id { get; set; }
		public bool Include { get; set; }
		public string ComponentName { get; set; }
	}

}