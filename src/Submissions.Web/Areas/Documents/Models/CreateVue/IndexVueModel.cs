﻿using Submissions.Common;
using Submissions.Core.Models.LetterBook;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Web.Mvc;


namespace Submissions.Web.Areas.Documents.Create
{
	public class IndexVueModel
	{
		public IndexVueModel()
		{
			//this.LetterTypeOptions = LookupsStandard.ConvertEnum<DocumentType>().ToList();
			this.CreateLetters = new List<CreateLetterModel>();
			this.CreateSupplementLetters = new List<CreateLetterModel>();
			this.SupplementLetters = new List<SupplementLetterDisplay>();
			this.DraftLetterOptions = new DraftOptionsInfo();
			this.ENReviewData = new ENReviewData();
			this.ComplianceInfo = new ComplianceReviewData();
			this.NURVEmailToUsers = new List<SelectListItem>();
			this.ApplicableKSAJurisdictions = new List<int>()
			{
				(int) CustomJurisdiction.HollandCasinos,
				(int) CustomJurisdiction.HollandArcade,
				(int) CustomJurisdiction.HollandLottery,
				(int) CustomJurisdiction.HollandStreet,
				(int) CustomJurisdiction.HollandSkillAmusementGamey
			};
		}

		public int BundleId { get; set; }
		public int ProjectId { get; set; }
		public string BundleName { get; set; }
		public bool IsClosedBundle { get; set; }
		public string BundleStatus { get; set; }
		public string ProjectStatus { get; set; }
		public DateTime? BundleTarget { get; set; }
		public string ProjectIndicator { get; set; }
		public bool ENInvolvementNeeded { get; set; }
		public int LWLocationId { get; set; }

		public List<CreateLetterModel> CreateLetters { get; set; }
		public List<CreateLetterModel> CreateSupplementLetters { get; set; }
		public List<SupplementLetterDisplay> SupplementLetters { get; set; } //This lists all supplements

		public IEnumerable<SelectListItem> LetterTypeOptions { get; set; }
		public List<SelectListItem> Languages { get; internal set; }
		public List<SelectListItem> SettableStatuses { get; internal set; }
		public List<SelectListItem> DraftLetterheads { get; internal set; }
		public List<SelectListItem> NonDraftLetterheads { get; internal set; }
		public List<string> NonBillableJurisdictions { get; internal set; }

		public List<int> MergedProjectIds { get; set; }
		public ComplianceReviewData ComplianceInfo { get; set; }
		public ENReviewData ENReviewData { get; set; }
		public List<int> NURVEmailToUserIds { get; set; }
		public IList<SelectListItem> NURVEmailToUsers { get; set; }

		public DraftOptionsInfo DraftLetterOptions { get; set; }
		public List<int> ApplicableKSAJurisdictions { get; set; }

		public bool CanRepostLetters { get; set; }
		public bool CanDeleteLetter { get; set; }
		public bool CanAccessLetterGen { get; set; }

		public bool IsProductionEnvironment { get; set; }

	}

	public class SupplementLetterDisplay
	{
		public int LetterId { get; set; }
		public string JurisdictionId { get; set; }
		public string JurisdictionName { get; set; }
		public string JurisdictionStatus { get; set; }
		public string DocumentType { get; set; }
		public string Language { get; set; }
		public string PdfPath { get; set; }
		public string PdfName { get; set; }
		public DateTime CreateDate { get; set; }
		public DateTime? LetterDate { get; set; }
		public List<JurisdictionalDataSubmissionIdPair> JurisdictionalDataIds { get; set; }
	}

	public class DraftOptionsInfo
	{
		public DraftOptionsInfo()
		{
			DraftStatuses = LookupsStandard.ConvertEnum<DraftLetterStatus>(showValueInDescription: true, showBlank: true);
			DraftUpdateReasons = LookupsStandard.ConvertEnum<UpdateDateReason>(showValueInDescription: true, showBlank: true);
			DraftTimelines = new List<SelectListItem>() {
				new SelectListItem { Value = "RV", Text = "RV"},
				new SelectListItem { Value = "NU", Text = "NU", }
			};
		}
		public IEnumerable<SelectListItem> DraftStatuses { get; internal set; }
		public IList<SelectListItem> DraftTimelines { get; internal set; }
		public IEnumerable<SelectListItem> DraftUpdateReasons { get; internal set; }
	}

	public class DeleteConfirmData
	{
		public int JurisdictionDataId { get; set; }
		public string JurisdictionId { get; set; }
		public string JurisdictionName { get; set; }
		public string FileNumber { get; set; }
		public string IdNumber { get; set; }
		public string GameName { get; set; }
		public string Version { get; set; }
		public string Status { get; set; }
		public List<UpdateData> UpdateData { get; set; }
		public string Message { get; set; }
	}

	public class UpdateData
	{
		public string DBFieldName { get; set; }
		public string FieldName { get; set; }
		public string FromVal { get; set; }
		public string ToVal { get; set; }
	}

	public enum JurisdictionalDataField
	{
		[Description("Active")]
		Active,
		[Description("Status")]
		JurisdictionType,
		[Description("Close Date")]
		JurisdictionDate,
		[Description("Update Date")]
		UpdateDate,
		[Description("Draft Date")]
		DraftLetterDate,
		[Description("Revoke Date")]
		RevokedDate,
		[Description("NU Date")]
		ObsoletedDate
	}

	public class DocumentCreateException : Exception
	{
		public DocumentCreateException()
		{

		}
		public DocumentCreateException(string message) : base(message)
		{

		}
		public DocumentCreateException(string message, Exception innerEx) : base(message, innerEx)
		{

		}
	}
}