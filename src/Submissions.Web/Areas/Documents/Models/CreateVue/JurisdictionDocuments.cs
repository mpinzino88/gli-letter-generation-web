﻿using Submissions.Core.Models.LetterBook;
using System;
using System.Collections.Generic;

namespace Submissions.Web.Areas.Documents.Models.Create
{
	public class JurisdictionDocuments
	{
		public int DocumentId { get; set; }
		public string Language { get; set; }
		public string LetterType { get; set; }
		public Double DocumentCurrentVersion { get; set; }
		public List<JurisdictionDocumentVersions> JurisdictionDocumentVersions { get; set; }
	}

	public class JurisdictionDocumentVersions
	{
		public int DocumentVersionId { get; set; }
		public string PdfName { get; set; }
		public string PdfPath { get; set; }
		public Double Version { get; set; }
		public DateTime CreateDate { get; set; }
		public DateTime LetterDate { get; set; }
		public List<JurisdictionalDataSubmissionIdPair> Jurisdictions { get; set; }
		public bool IsBillable { get; set; }
		public string ProjectName { get; set; }
		public List<Translations> Translations { get; set; }
		public bool ExistsInDB { get; set; } //need this to show supplement if deleted
		public bool AllowEditBilling { get; set; }
	}

	public class Translations
	{
		public string PdfPath { get; set; }
		public string PdfName { get; set; }
		public string Language { get; set; }
		public int LanguageId { get; set; }
		public DateTime CreateDate { get; set; }
		public DateTime LetterDate { get; set; }
		public bool ExistsInDB { get; set; } //need this to show supplement if deleted
	}
}