﻿using System;
using System.Collections.Generic;

namespace Submissions.Web.Areas.Documents.Models.Create
{
	public class ProcessSummary
	{
		public ProcessSummary()
		{
			ContainsNURV = new List<JurisdictionInfo>();
			JurisdictionsBillableInfo = new List<JurisdictionInfo>();
			BillingSheet = new List<JurisdictionInfo>();
			NumOfItemsBilled = new List<JurisdictionInfo>();
			BallyInfo = new List<JurisdictionInfo>();
			FixedPriceContractNumber = new List<JurisdictionInfo>();
		}
		public string BundleName { get; set; }
		public int? ReviewerId { get; set; }
		public string Reviewer { get; set; }
		public int? LetterWriterId { get; set; }
		public string LetterWriter { get; set; }
		public DateTime DateOfReport { get; set; }
		public int NumOfBillable { get; set; }
		public int NumOfNonBillable { get; set; }
		public string TypeOfReport { get; set; }
		public List<JurisdictionInfo> ContainsNURV { get; set; }
		public List<JurisdictionInfo> JurisdictionsBillableInfo { get; set; }
		public List<JurisdictionInfo> BillingSheet { get; set; }
		public List<JurisdictionInfo> NumOfItemsBilled { get; set; }
		public List<JurisdictionInfo> BallyInfo { get; set; }
		public List<JurisdictionInfo> FixedPriceContractNumber { get; set; }
	}

	public class JurisdictionInfo
	{
		public string JurisdictionId { get; set; }
		public string JurisdictionName { get; set; }
		public string Info { get; set; }
	}
}