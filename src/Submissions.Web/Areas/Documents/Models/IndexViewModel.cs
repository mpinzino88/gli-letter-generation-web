﻿using Submissions.Web.Models.Grids.Documents;

namespace Submissions.Web.Areas.Documents.Models.DraftLetters
{
	public class IndexViewModel
	{
		public DraftLettersGrid DraftLettersGrid { get; } = new DraftLettersGrid();

		private readonly int ProjectId;

		public IndexViewModel(int projectId)
		{
			ProjectId = projectId;
		}

		public int GetProjectId()
		{
			return ProjectId;
		}
	}
}