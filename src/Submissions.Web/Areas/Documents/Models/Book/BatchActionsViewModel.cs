﻿using Submissions.Common;
using System.ComponentModel.DataAnnotations;

namespace Submissions.Web.Areas.Documents.Models.Book
{
	public class BatchActionsViewModel
	{
		public BatchAction Action { get; set; }
		[Display(Name = "Batch Number"), Required]
		public int? BatchNumber { get; set; }
		public string LetterBookIds { get; set; }
		[Display(Name = "Status")]
		public LetterBookACTStatus ACTStatus { get; set; }
	}
}