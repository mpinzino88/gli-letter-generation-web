﻿using Submissions.Common;
using Submissions.Web.Models.Grids.Documents;
using Submissions.Web.Models.LookupsAjax;
using Submissions.Web.Models.Query;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Submissions.Web.Areas.Documents.Models.Book
{
	public class BookIndexViewModel
	{
		public BookIndexViewModel()
		{
			this.Filters = new LetterBookSearch();
			this.BillerFilter = new UsersFilter { Permission = Permission.LetterBookBilling };
		}

		public LetterBookSearch Filters { get; set; }
		public LetterBookGrid LetterBookGrid { get; set; }
		public Department Department { get; set; }
		public UsersFilter BillerFilter { get; set; }
	}

	public class BookEditViewModel
	{
		private IEnumerable<SelectListItem> _reportTypes = new List<SelectListItem>
		{
			new SelectListItem { Text = "", Value = "" },
			new SelectListItem { Text = "AGY 10+ Jurisdictions", Value = "AGY 10+ Jurisdictions" },
			new SelectListItem { Text = "ARI Installation/Personality", Value = "ARI Installation/Personality" },
			new SelectListItem { Text = "ARZ/AML Personality/GAL Chip with Signature", Value = "ARZ/AML Personality/GAL Chip with Signature" },
			new SelectListItem { Text = "ARZ/AML Personality/GAL Chip without Signature", Value = "ARZ/AML Personality/GAL Chip without Signature" },
			new SelectListItem { Text = "ARZ/AML Top Box/Personality/GAL Chip", Value = "ARZ/AML Top Box/Personality/GAL Chip" },
			new SelectListItem { Text = "IGT Installation/Package", Value = "IGT Installation/Package" },
			new SelectListItem { Text = "KON Personality/Sound/Graphics", Value = "KON Personality/Sound/Graphics" },
			new SelectListItem { Text = "VGT Interoperability", Value = "VGT Interoperability" }
		};

		private IEnumerable<SelectListItem> _specializedTypes = new List<SelectListItem>
		{
			new SelectListItem { Text = "", Value = "" },
			new SelectListItem { Text = "Fixed Price", Value = "Fixed Price"},
			new SelectListItem { Text = "Proposal", Value = "Proposal" },
			new SelectListItem { Text = "Statement of Work", Value = "Statement of Work" }
		};

		public BookEditViewModel()
		{
			this.Department = Department.QA;
			this.QAStatus = LetterBookQAStatus.InProgress;
			this.ReportDate = DateTime.Now.Date;
			this.DocumentVersionLinks = new List<DocumentVersionLink>();
			this.ReportTypes = _reportTypes;
			this.SpecializedTypes = _specializedTypes;
			this.BillingSheetFilter = new BillingSheetFilter() { };
		}

		public Mode Mode { get; set; }
		public int? Id { get; set; }
		public int DocumentVersionId { get; set; }
		public int DepartmentId { get; set; }
		public Department Department { get; set; }
		public IEnumerable<SelectListItem> StatusList { get; set; }
		[Display(Name = "Status")]
		public LetterBookQAStatus QAStatus { get; set; }
		[Display(Name = "Status")]
		public LetterBookACTStatus? ACTStatus { get; set; }
		[Display(Name = "Billing Sheet"), Required]
		public string BillingSheet { get; set; }
		public int? BillingSheetId { get; set; }
		[Display(Name = "Is Billable?")]
		public bool IsBillable { get; set; }
		[Display(Name = "Report Date"), Required]
		public DateTime ReportDate { get; set; }
		[Display(Name = "Document Specialist")]
		public string EntryUser { get; set; }
		public DateTime? EntryDate { get; set; }
		[Display(Name = "Document Reviewer")]
		public string ReviewUser { get; set; }
		public DateTime? ReviewDate { get; set; }
		[Display(Name = "Biller")]
		public string BillingUser { get; set; }
		public DateTime? BillingDate { get; set; }
		[Display(Name = "Office"), Required]
		public int? LaboratoryId { get; set; }
		public string Laboratory { get; set; }
		[Display(Name = "Note")]
		public string Note { get; set; }
		public IList<DocumentVersionLink> DocumentVersionLinks { get; set; }

		// Non-Specialized
		[Display(Name = "Bally")]
		public LetterBookBallyType? BallyType { get; set; }

		// Specialized
		public IEnumerable<SelectListItem> ReportTypes { get; set; }
		[Display(Name = "Report Type"), Required]
		public string ReportType { get; set; }
		public IEnumerable<SelectListItem> SpecializedTypes { get; set; }
		[Display(Name = "Specialized Type"), Required]
		public string SpecializedType { get; set; }

		// User Types
		public bool IsEntryUser { get; set; }
		public bool IsReviewUser { get; set; }
		public bool IsBillingUser { get; set; }
		public string FileNumber { get; set; }
		[Display(Name = "Billed Count")]
		public int BilledCount { get; set; }
		[Display(Name = "Contract Number")]
		public string ContractNumber { get; set; }

		//filter
		public BillingSheetFilter BillingSheetFilter { get; set; }
	}

	public class DocumentVersionLink
	{
		public DocumentVersionLink()
		{
			this.Billable = true;
		}

		public int Id { get; set; }
		public string JurisdictionId { get; set; }
		public string JurisdictionName { get; set; }
		public bool Billable { get; set; }
		public int ProjectId { get; set; }
		public int? BilledCount { get; set; }
	}
}