﻿using Submissions.Core.Models.DTO;
using Submissions.Core.Models.LetterContent;
using Submissions.Web.Areas.Documents.Models.Coversheet;
using Submissions.Web.Areas.Documents.Models.Generate;
using Submissions.Web.Areas.Documents.Models.Generate.Helper;
using Submissions.Web.Areas.Evolution.Models;
using System.Collections.Generic;

namespace Submissions.Web.Areas.Documents.Models.NoteManager
{
	public class MockMemoData
	{
		public MockMemoData()
		{
			Components = new List<SubmissionDTO>();
			CertificationIssuers = new List<CertificationIssuer>();
			ClauseReports = new List<ClauseReportViewModel>();
			Paytables = new List<MathCoversheetDTO>();
			Footer = new Footer();
			JIRAInformation = new GenerateJIRAInformationViewModel();
			GamingGuidelines = new List<GamingGuideline>();
			SpecializedJurisdictionClauseReports = new List<GenerateSpecializedJurisdictionClauseReportViewModel>();
			JurisdictionData = new List<GenerateJurisdictionModel>();
			OCUITs = new List<GenerateOCUITComponentViewModel>();
		}

		public string ManufacturerId { get; set; }
		public string ManufacturerName { get; set; }
		public string FileNumber { get; set; }
		public string BundleName { get; set; }
		public List<SubmissionDTO> Components { get; set; }
		public List<CertificationIssuer> CertificationIssuers { get; set; }
		public List<ClauseReportViewModel> ClauseReports { get; set; }
		public List<MathCoversheetDTO> Paytables { get; set; }
		public Footer Footer { get; set; }
		public GenerateJIRAInformationViewModel JIRAInformation { get; set; }
		public List<GamingGuideline> GamingGuidelines { get; set; }
		public List<GenerateSpecializedJurisdictionClauseReportViewModel> SpecializedJurisdictionClauseReports { get; set; }
		public List<GenerateJurisdictionModel> JurisdictionData { get; set; }
		public List<GenerateOCUITComponentViewModel> OCUITs { get; set; }
	}
}