﻿namespace Submissions.Web.Areas.Documents.Models.NoteManager
{
	public class Footer
	{
		public int TotalPages
		{
			get
			{
				return 20;
			}
		}

		public int CurrentPage
		{
			get
			{
				return 1;
			}
		}
	}
}