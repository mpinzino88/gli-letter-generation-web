﻿using System;
using System.Collections.Generic;

namespace Submissions.Web.Areas.Documents.Models.NoteManager
{
	public class Validation
	{
		public Validation()
		{
			Jurisdictions = new List<ValidationJurisdiction>();
		}
		public int? Id { get; set; }
		public Guid? ValidationID { get; set; }
		public string NormalizedID { get; set; }
		public string Answer { get; set; }
		public string ConditionalAnswer { get; set; }
		public int? Status { get; set; }
		public string StatusText { get; set; }
		public int? MemoID { get; set; }
		public string ValidationText { get; set; }
		public int? QuestionType { get; set; }
		public List<ValidationJurisdiction> Jurisdictions { get; set; }
	}

	public class ValidationJurisdiction
	{
		public int Id { get; set; }
		public string Name { get; set; }
	}
}