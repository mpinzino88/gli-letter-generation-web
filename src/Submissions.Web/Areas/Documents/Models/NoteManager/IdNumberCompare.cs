﻿namespace Submissions.Web.Areas.Documents.Models.NoteManager
{
	public class IdNumberCompare
	{
		public string IdNumber { get; set; }
		public int? CompareCriteriaId { get; set; }
		public dynamic SelectedCompareCriteria { get; set; }
	}
}