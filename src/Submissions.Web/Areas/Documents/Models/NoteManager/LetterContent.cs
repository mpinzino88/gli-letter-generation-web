﻿using GLI.EF.LetterContent;
using Submissions.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Submissions.Web.Areas.Documents.Models.NoteManager
{
	public class LetterContent
	{
		public LetterContent()
		{
			Jurisdictions = new List<SelectListItem>();
			JurisdictionIds = new List<string>();
			LetterTypeOptions = LookupsStandard.ConvertEnum<LetterTypes>().ToList();
			LetterTypes = new List<string>();
			Manufacturers = new List<SelectListItem>();
			ManufacturerCodes = new List<string>();
			Memos = new List<Memo>();
			SpecialFlags = new List<string>();
			SpecialFlagOptions = LookupsStandard.ConvertEnum<SpecialFlags>().ToList();
			TestingTypes = new List<string>();
			TestingTypeOptions = LookupsStandard.ConvertEnum<TestingTypes>().ToList();
		}

		public int Id { get; set; }
		public string Description { get; set; }
		public List<SelectListItem> Jurisdictions { get; set; }
		public List<string> JurisdictionIds { get; set; }
		public List<string> LetterTypes { get; set; }
		public List<SelectListItem> LetterTypeOptions { get; set; }
		public List<SelectListItem> Manufacturers { get; set; }
		public List<string> ManufacturerCodes { get; set; }
		public List<Memo> Memos { get; set; }
		public string Name { get; set; }
		public bool Optional { get; set; }
		public int? OrderId { get; set; }
		public DateTime? AddDate { get; set; }
		public int? AddedBy { get; set; }
		public DateTime? EditDate { get; set; }
		public int? EditedBy { get; set; }
		public int LifeCycle { get; set; }
		public int SectionId { get; set; }
		public dynamic SelectedSection { get; set; }
		public List<string> SpecialFlags { get; set; }
		public List<SelectListItem> SpecialFlagOptions { get; set; }
		public List<string> TestingTypes { get; set; }
		public List<SelectListItem> TestingTypeOptions { get; set; }
	}
}