﻿using Submissions.Common;
using System;
using System.Collections.Generic;
using InputType = GLI.EF.LetterContent.InputType;

namespace Submissions.Web.Areas.Documents.Models.NoteManager
{
	public class EditNoteViewModel
	{
		public EditNoteViewModel()
		{
			UserHasRightsToSave = Util.HasAccess(Permission.NoteManager, AccessRight.Edit);
			LetterContent = new LetterContent();
			MemoTemplate = new Memo()
			{
				Id = 0,
				Name = "",
				Content = "<div>Super Awesome new Memo.</div>"
			};
			MockMemoData = new MockMemoData();
			QuestionTypes = ComUtil.GetEnumList<InputType>();
			Sections = new List<KeyValuePair<string, int>>();
			CompareCriterias = new List<KeyValuePair<string, int>>();
		}

		public bool UserHasRightsToSave { get; set; }
		public MockMemoData MockMemoData { get; set; }
		public LetterContent LetterContent { get; set; }
		public Memo MemoTemplate { get; set; }
		public Mode Mode { get; set; }
		public List<KeyValuePair<string, int>> QuestionTypes { get; set; }
		public List<KeyValuePair<string, int>> Sections { get; set; }
		public List<KeyValuePair<string, int>> CompareCriterias { get; set; }
	}

	public class ValidationSearchFilter
	{
		public ValidationSearchFilter()
		{
			ExcludeValidations = new List<Guid>();
		}
		public int? MemoId { get; set; }
		public string ValidationText { get; set; }
		public string NormalizedId { get; set; }
		public int? State { get; set; }
		public List<Guid> ExcludeValidations { get; set; }
	}
}