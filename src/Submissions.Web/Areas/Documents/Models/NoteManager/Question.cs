﻿using GLI.EF.LetterContent;
using System;
using System.Collections.Generic;

namespace Submissions.Web.Areas.Documents.Models.NoteManager
{
	public class Question
	{
		public Question()
		{
			TableRows = new List<List<Question>>();
			SelectedOptions = new List<object>();
		}

		public int Id { get; set; }
		public string ColumnHeader { get; set; }
		public string ConditionalExpression { get; set; }
		public string TokenValue { get; set; }
		public string QuestionText { get; set; }
		public string TokenKey { get; set; }
		public Guid? LocalId { get; set; }
		public string Name { get; set; }
		public string OptionDelimiter { get; set; }
		public string ReferenceLookup { get; set; }
		public string Source { get; set; }
		public string SourceProperty { get; set; }
		public bool Table { get; set; }
		public string Type { get; set; }
		public string QuestionOptions { get; set; }
		public dynamic QuestionOptionsObj { get; set; }
		public int? ParentQuestionId { get; set; }
		public DateTime? EditDate { get; set; }
		public QuestionSpecialFlags QuestionSpecialFlags { get; set; }
		public List<List<Question>> TableRows { get; set; }
		public List<object> SelectedOptions { get; set; }
		public bool IsRequired { get; set; }
	}
}