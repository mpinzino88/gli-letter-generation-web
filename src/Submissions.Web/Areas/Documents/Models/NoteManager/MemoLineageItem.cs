﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Submissions.Web.Areas.Documents.Models.NoteManager
{
	public class MemoLineageItem
	{
		public int Id { get; set; }
		public int OriginalId { get; set; }
		public string Name { get; set; }
		public string Description { get; set; }
		public DateTime? DeprecatedDate { get; set; }
	}
}