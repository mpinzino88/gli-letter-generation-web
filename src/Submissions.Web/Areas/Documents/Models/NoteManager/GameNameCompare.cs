﻿namespace Submissions.Web.Areas.Documents.Models.NoteManager
{
	public class GameNameCompare
	{
		public string GameName { get; set; }
		public int? CompareCriteriaId { get; set; }
		public dynamic SelectedCompareCriteria { get; set; }
	}
}