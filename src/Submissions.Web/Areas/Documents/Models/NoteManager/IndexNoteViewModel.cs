﻿using Submissions.Core.Models.LetterContent;

namespace Submissions.Web.Areas.Documents.Models.NoteManager
{
	public class IndexNoteViewModel
	{
		public IndexNoteViewModel()
		{
			Filter = new MemoSearchFilter();
		}

		public MemoSearchFilter Filter { get; set; }
		public bool allowSearchToggle { get; set; } //To be removed later
	}
}