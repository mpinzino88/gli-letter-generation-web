﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Submissions.Web.Areas.Documents.Models.NoteManager
{
	public class Version
	{
		public Version()
		{
		}
		public DateTime? AddDate { get; set; }
		public DateTime? DeprecateDate { get; set; }
		public int CreatedBy { get; set; }
		public int? DeprecatedBy { get; set; }
		public int OriginalId { get; set; }
		public int Lifecycle { get; set; }
	}
}