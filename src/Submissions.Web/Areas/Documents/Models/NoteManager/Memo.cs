﻿using GLI.EF.LetterContent;
using Submissions.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace Submissions.Web.Areas.Documents.Models.NoteManager
{
	public class Memo
	{
		public Memo()
		{
			Jurisdictions = new List<SelectListItem>();
			JurisdictionIds = new List<string>();
			JurisdictionsNotIncluded = new List<SelectListItem>();
			JurisdictionIdsNotIncluded = new List<string>();
			Manufacturers = new List<SelectListItem>();
			ManufacturerCodes = new List<string>();
			ManufacturersNotIncluded = new List<SelectListItem>();
			ManufacturerCodesNotIncluded = new List<string>();
			Questions = new List<Question>();
			SpecialFlags = new List<string>();
			SpecialFlagOptions = LookupsStandard.ConvertEnum<SpecialFlags>().ToList();
			Version = new Version();
			PlatformIds = new List<int>();
			Platforms = new List<SelectListItem>();
			FunctionIds = new List<int>();
			Functions = new List<SelectListItem>();
			IDNumbers = new List<IdNumberCompare>();
			GameNames = new List<GameNameCompare>();
			SourceOptions = new List<object>();
			SubTypeIds = new List<int>();
			SubTypes = new List<SelectListItem>();
			TestTypeIds = new List<int>();
			TestTypes = new List<SelectListItem>();
			SignatureScopeIds = new List<int>();
			SignatureScopes = new List<SelectListItem>();
			foreach (Source source in Enum.GetValues(typeof(Source)))
			{
				SourceOptions.Add(new { Value = (int)source, Text = source.ToString() });
			}
			Validations = new List<Validation>();
			ChipTypeIds = new List<int>();
			ChipTypes = new List<SelectListItem>();

		}
		private string GetIntSourceAsString(Source source)
		{
			var number = (int)source;
			return number.ToString();
		}

		public int Id { get; set; }
		public string BusinessLogicExpression { get; set; }
		public string Content { get; set; }
		public string Description { get; set; }
		public List<SelectListItem> Platforms { get; set; }
		public List<int> PlatformIds { get; set; }
		public List<SelectListItem> Functions { get; set; }
		public List<int> FunctionIds { get; set; }
		public List<IdNumberCompare> IDNumbers { get; set; }
		public List<GameNameCompare> GameNames { get; set; }
		public List<SelectListItem> Jurisdictions { get; set; }
		public List<string> JurisdictionIds { get; set; }
		public List<SelectListItem> JurisdictionsNotIncluded { get; set; }
		public List<string> JurisdictionIdsNotIncluded { get; set; }
		public int? LanguageId { get; set; }
		public string Language { get; set; }
		public int? LetterContentId { get; set; }
		public string LetterContentName { get; set; }
		public Guid? LocalId { get; set; }
		public List<SelectListItem> Manufacturers { get; set; }
		public List<string> ManufacturerCodes { get; set; }
		public List<SelectListItem> ManufacturersNotIncluded { get; set; }
		public List<string> ManufacturerCodesNotIncluded { get; set; }
		public string Name { get; set; }
		public int? OrderId { get; set; }
		public virtual List<Question> Questions { get; set; }
		public int? SecondaryLanguageId { get; set; }
		public string SecondaryLanguage { get; set; }
		public string SecondaryLanguageContent { get; set; }
		public string SourceProperty { get; set; }
		public List<string> SpecialFlags { get; set; }
		public List<SelectListItem> SpecialFlagOptions { get; set; }
		public List<SelectListItem> SubTypes { get; set; }
		public List<int> SubTypeIds { get; set; }
		public List<SelectListItem> TestTypes { get; set; }
		public List<int> TestTypeIds { get; set; }
		public Version Version { get; set; }
		public List<SelectListItem> WorkspaceCategoryOptions { get; set; }
		public int SourceId { get; set; }
		public string SourceName { get; set; }
		public List<object> SourceOptions { get; set; }
		public List<Validation> Validations { get; set; }
		public List<int> SignatureScopeIds { get; set; }
		public List<SelectListItem> SignatureScopes { get; set; }
		public List<int> ChipTypeIds { get; set; }
		public List<SelectListItem> ChipTypes { get; set; }
	}
}