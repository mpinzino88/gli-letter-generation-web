using Kendo.Mvc.Extensions;
using Submissions.Common;
using Submissions.Core.Models.LetterContent;
using Submissions.web.Areas.Documents.Models.Generate;
using Submissions.Web.Areas.Documents.Models.Generate.Helper;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using LetterGenMode = Submissions.Common.LetterGenMode;

namespace Submissions.Web.Areas.Documents.Models.Generate
{
	public class AlphaLetterContent
	{
		#region Variables
		public LetterGenMode mode;
		private IList<int> _alphaLettersJurisdictions;
		private IList<string> _alphaLetterManufacturers;
		#endregion

		#region Properties
		/// <summary>
		/// Taken from the confluence page at https://gd-docs.gaminglabs.com/display/SD/Letter+Content+Type+Support
		/// </summary>
		public IList<string> AlphaLetterManufacturers
		{
			get
			{
				if (_alphaLetterManufacturers == null)
				{
					_alphaLetterManufacturers = new List<string>()
					{
						CustomManufacturer.IGT.ToString(),
						CustomManufacturer.EVI.ToString(),
						CustomManufacturer.NGG.ToString(),
						CustomManufacturer.OTL.ToString(),
						CustomManufacturer.WII.ToString(),
						CustomManufacturer.IWG.ToString(),
						CustomManufacturer.ALC.ToString(),
						CustomManufacturer.LQB.ToString()
					};
				}
				return _alphaLetterManufacturers;

			}
		}
		public IList<int> AlphaLettersJurisdictions
		{
			get
			{
				if (_alphaLettersJurisdictions == null)
				{
					_alphaLettersJurisdictions = new List<int>
					{
						(int) CustomJurisdiction.ALCOnline,
						(int) CustomJurisdiction.Alderney,
						(int) CustomJurisdiction.ArgentinaBuenosAiresOnline,
						(int) CustomJurisdiction.BahamasInteractive,
						(int) CustomJurisdiction.BelarusRemote,
						(int) CustomJurisdiction.BritishColumbiaOnline,
						(int) CustomJurisdiction.BulgariaOnline,
						(int) CustomJurisdiction.ColombiaOnline,
						(int) CustomJurisdiction.CroatiaOnline,
						(int) CustomJurisdiction.EstoniaOnline,
						(int) CustomJurisdiction.Gibraltar,
						(int) CustomJurisdiction.GreeceOnline,
						(int) CustomJurisdiction.ISLEOFMAN,
						(int) CustomJurisdiction.LataviaOnline,
						(int) CustomJurisdiction.LithuniaiGaming,
						(int) CustomJurisdiction.LotoQuebecOnline,
						(int) CustomJurisdiction.MaltaOnline,
						(int) CustomJurisdiction.NewJerseyiGaming,
						(int) CustomJurisdiction.PennsylvaniaInteractive,
						(int) CustomJurisdiction.Romania,
						(int) CustomJurisdiction.SwedeniGaming,
						(int) CustomJurisdiction.ArgentinaBuenosAiresOnline,
						(int) CustomJurisdiction.GreeceOnline,
						(int) CustomJurisdiction.BelarusRemote,
						(int) CustomJurisdiction.NorwayiGaming,
						(int) CustomJurisdiction.UKRemote
					};
				}
				return _alphaLettersJurisdictions;
			}
		}
		/// <summary>
		/// Master list of controls supported in this version
		/// </summary>
		public AlphaLetterControl AlphaLetterControl { get; set; }
		/// <summary>
		/// Master list of memos provided in this version
		/// </summary>
		public List<GenerateLetterMemo> Memos { get; set; }
		/// <summary>
		/// Master list of Categories provided in this version
		/// </summary>
		public List<GenerateLetterSection> WorkspaceCategories { get; set; }
		#endregion

		#region Constructors
		public AlphaLetterContent(LetterGenMode mode)
		{
			this.mode = mode;
			AlphaLetterControl = new AlphaLetterControl();
			Memos = new List<GenerateLetterMemo>();
			WorkspaceCategories = LoadCategories();
		}
		#endregion

		public IQueryable<GenerateLetterMemo> GetApplicableMemos(LetterContentSearchFilter filter)
		{
			return Memos.AsQueryable().ApplicableToLetter(filter);
		}
		public void AttachLetterMemoToWorkspace(GenerateLetter letter, LetterGenWorkspaceCategories workspaceCategory, LetterContentSearchFilter mfgJuriFilter)
		{
			mfgJuriFilter.WorkspaceCategory = workspaceCategory.GetEnumDescription();
			var allApplicableMemos = GetApplicableMemos(mfgJuriFilter).ToList();
			if (allApplicableMemos.Count > 0)
			{
				var category = letter.Sections.Where(x => x.Name == workspaceCategory.GetEnumDescription()).SingleOrDefault();
				if (category == null)
				{
					category = AddCategory(letter, workspaceCategory);
				}
				category.Memos = allApplicableMemos;
			}
		}

		public void AdjustApplicableJurisdictions(List<int> jurIds)
		{
			var allJurs = new List<int>(jurIds);

			Memos.ForEach(x =>
			{

				if (x.ApplicableJurisdictions.Count == 0)
				{
					//find all memo with same name
					var copyMemos = this.Memos.Where(m => m.Name == x.Name).ToList();
					//find for what all jurs
					var copyMemoJursApplicable = new List<int>();
					copyMemos.ForEach(cm =>
					{
						copyMemoJursApplicable.AddRange(cm.ApplicableJurisdictions);
					});
					var applicableJurs = jurIds.Except(copyMemoJursApplicable).ToList();
					x.ApplicableJurisdictions = applicableJurs;
				}
			});

			//Memos without any jurisdictions are *supposed* to be applicable to all jurisdictions...
			//I guess that convention has been killed? 
			var memosWihtoutJurs = Memos.Where(x => x.ApplicableJurisdictions.Count == 0).ToList();
			memosWihtoutJurs.ForEach(x =>
			{
				Memos.Remove(x);
			});

			//***this can be removed when we really query from DB as we wont pick up unnecessary memos
			var memosToRemove = Memos.Except(Memos.Where(x => x.ApplicableJurisdictions.Any(y => jurIds.Contains(y))).ToList()).ToList();
			memosToRemove.ForEach(x =>
			{
				Memos.Remove(x);
			});

		}

		/// <summary>
		/// Adds a basic memo to the Letter for any jurisdictions that don't have content included in the AlphaContent list
		/// </summary>
		/// <param name="letter">The letter</param>
		/// <param name="emptyJurisdictions">The JurisdictionIds of jurisdictions not applicable to any other memo in the letter currently.</param>
		internal void AddUnsupportedMessage(GenerateLetter letter, List<int> emptyJurisdictions)
		{
			var first = letter.Sections.FirstOrDefault();
			if (first == null)
			{
				first = new GenerateLetterSection()
				{
					Id = -1,
					OrderId = -1,
					Name = "Not Supported",
					Description = "Not Supported"
				};
				letter.Sections.Add(first);
			}
			first.Memos.Add(
				new GenerateLetterMemo()
				{
					Name = "Not Supported",
					LetterContent = "<div>{{ juriLetterDetails.letterName }} is not currently supported in this version of Letter Gen.</div>",
					Include = true,
					DefaultInclude = true,
					Removable = false,
					Custom = false,
					Id = -1,
					ApplicableJurisdictions = emptyJurisdictions,
					Description = "This jurisdiction is not currently supported.",
					LocalId = Guid.NewGuid(),
				}
			);
		}

		#region Workspace Categories
		public List<GenerateLetterSection> LoadCategories()
		{
			var result = new List<GenerateLetterSection>();
			foreach (var category in Enum.GetValues(typeof(LetterGenWorkspaceCategories)).Cast<LetterGenWorkspaceCategories>())
			{
				result.Add(BuildCategory(category));
			}
			return result;
		}

		private GenerateLetterSection BuildCategory(LetterGenWorkspaceCategories category)
		{
			var name = category.GetEnumDescription();
			return new GenerateLetterSection()
			{
				Name = name,
				Description = name,
				Id = (int)category,
				OrderId = (int)category
			};
		}
		#endregion

		//#region Memos
		//public List<GenerateLetterMemo> LoadMemos()
		//{
		//	var result = new List<GenerateLetterMemo>();
		//	return result;
		//}

		#region Helpers
		private GenerateLetterSection AddCategory(GenerateLetter letter, LetterGenWorkspaceCategories workspaceCategory)
		{
			var category = WorkspaceCategories.SingleOrDefault(wc => wc.Id == (int)workspaceCategory);
			letter.Sections.Add(category);
			return category;
		}
		/// <summary>
		/// Used for adding 'negate' logic for ApplicableJurisdictions in the definitions
		/// </summary>
		/// <param name="inapplicableJurisdictions">List of Jurisdiction Ids this Memo doesn't apply to</param>
		/// <returns>Returns all jurisdictions in the master list *except* the ones provided in the parameter</returns>
		private List<int> NegateJurisdictions(List<int> inapplicableJurisdictions)
		{
			return AlphaLettersJurisdictions.Except(inapplicableJurisdictions).ToList();
		}
		/// <summary>
		/// Used for adding 'negate' logic for ApplicableJurisdictions in the definitions
		/// </summary>
		/// <param name="inapplicableJurisdictions">A Jurisdiction Id this Memo doesn't apply to</param>
		/// <returns>Returns all jurisdictions in the master list *except* the ones provided in the parameter</returns>
		private List<int> NegateJurisdiction(int inapplicableJurisdiction)
		{
			return NegateJurisdictions(new List<int>() { inapplicableJurisdiction });
		}
		/// <summary>
		/// Used for adding 'negate' logic for ApplicableManufacturers in the definitions
		/// </summary>
		/// <param name="inapplicableManufacturers">A list of Manufacturer Codes this memo doesn't apply to</param>
		/// <returns>Returns all manufacturers in the master list *except* the ones provided in the parameter</returns>
		private List<string> NegateManufacturers(List<string> inapplicableManufacturers)
		{
			return AlphaLetterManufacturers.Except(inapplicableManufacturers).ToList();
		}
		/// <summary>
		/// Used for adding 'negate' logic for ApplicableManufacturers in the definitions
		/// </summary>
		/// <param name="inapplicableManufacturers">A Manufacturer Code this memo doesn't apply to</param>
		/// <returns>Returns all manufacturers in the master list *except* the ones provided in the parameter</returns>
		private List<string> NegateManufacturers(string inapplicableManufacturer)
		{
			return NegateManufacturers(new List<string>() { inapplicableManufacturer });
		}
		#endregion
	}
	/// <summary>
	/// Workspace Category Master List. The ordering of these determines the order as they appear in the letter. If you modify this order, 
	/// you must modify BuildCategory(...)
	/// </summary>
	public enum LetterGenWorkspaceCategories
	{
		[Description("PDF")]
		PDF,

		[Description("Letter Details")]
		LetterDetails,

		[Description("Product Details")]
		ProductDetails,

		[Description("Math")]
		MathDetails,

		[Description("Game Integration Evaluation")]
		GameIntegrationEvaluation,

		[Description("Terms and Conditions")]
		TermsAndConditions,

		[Description("Conditions of Evaluation")]
		ConditionsOfEvaluation,

		[Description("Evolution Clauses")]
		EvolutionClauseDetails,

		[Description("Conclusion")]
		Conclusion,

		[Description("Appendix")]
		Appendix
	}

}