﻿using Submissions.Common;
using Submissions.Web;
using Submissions.Web.Areas.Documents.Models.Generate;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace Submissions.web.Areas.Documents.Models.Generate
{
	public class AlphaLetterControl
	{
		private List<GenerateLetterInputControl> Controls;

		public AlphaLetterControl()
		{
			//CreateQuestions();
		}

		private void CreateQuestions()
		{
			Controls = new List<GenerateLetterInputControl>();
			Controls.Add(new GenerateLetterInputControl()
			{
				SourceId = "Components",
				SourceProperty = "Component",
				Type = GenerateLetterInputControlType.STATIC.ToString(),
				Id = 1,
				LocalId = Guid.NewGuid(),
				SourceFilter = "true",
				RepeatableContent = true

			});
			Controls.Add(new GenerateLetterInputControl()
			{
				Id = 2,
				SourceId = "Components",
				SourceProperty = "Component",
				Type = GenerateLetterInputControlType.RADIO.ToString(),
				Label = "productTested",
				Description = "Include in Tested Products?",
				LocalId = Guid.NewGuid(),
				Name = "Product Tested",
				Content = "1",//We should be consistent with our true/false values. 1 vs blank is not ok.
				Options = new List<SelectListItem>
				{
					new SelectListItem { Text = "Yes", Value = "1" },
					new SelectListItem { Text = "No", Value = "" },
				}

			});
			Controls.Add(new GenerateLetterInputControl()
			{
				Id = 102,
				SourceId = "Components",
				SourceProperty = "Component",
				Type = GenerateLetterInputControlType.RADIO.ToString(),
				Label = "materialProductTested",
				Description = "Include in IGT Material Tested?",
				LocalId = Guid.NewGuid(),
				Name = "IGT Material Product Tested",
				Content = "",
				Options = new List<SelectListItem>
				{
					new SelectListItem { Text = "Yes", Value = "1" },
					new SelectListItem { Text = "No", Value = "" },
				}

			});
			Controls.Add(new GenerateLetterInputControl()
			{
				Id = 3,
				LocalId = Guid.NewGuid(),
				Description = "The date of this report.",
				Label = "reportDate",
				Content = DateTime.Today.ToString("yyyy-MM-dd"),
				Type = GenerateLetterInputControlType.DATEPICKER.ToString()

			});
			Controls.Add(new GenerateLetterInputControl()
			{
				Id = 4,
				LocalId = Guid.NewGuid(),
				Description = "The lab this report is issued from.",
				Label = "issuingLab",
				ReferenceLookup = "LaboratoryLetterAddress",
				OptionDelimiter = "<br/><br/>",
				Type = GenerateLetterInputControlType.LOOKUPMULTIPLE.ToString(),
				Options = new List<SelectListItem>
				{
					new SelectListItem()
					{
						Value = "Gaming Laboratories International, LLC<br/>600 Airport Road,<br/>Lakewood, NJ 08701",
						Text = "NJ",
						Selected = true
					}
				},
				Content = "Gaming Laboratories International, LLC<br/>600 Airport Road,<br/>Lakewood, NJ 08701",
				SelectedOptions = new List<string>() { }

			});
			Controls.Add(new GenerateLetterInputControl()
			{
				Id = 5,
				LocalId = Guid.NewGuid(),
				Description = "Technical Standards",
				Label = "juriLetterDetails.technicalStandards",
				Content = "",
				Type = GenerateLetterInputControlType.TECHNICALSTANDARD.ToString(),
				ReferenceLookup = "",

			});
			Controls.Add(new GenerateLetterInputControl()
			{
				SourceId = "Components",
				SourceProperty = "Component",
				Type = GenerateLetterInputControlType.STATIC.ToString(),
				Id = 6,
				LocalId = Guid.NewGuid(),
				SourceFilter = "true",
				RepeatableContent = true

			});

			Controls.Add(new GenerateLetterInputControl()
			{
				Id = 10,
				LocalId = Guid.NewGuid(),
				Description = "Evaluating Lab.",
				Label = "evalLab",
				ReferenceLookup = "LaboratoryLetterAddress",
				OptionDelimiter = "<br/><br/>",
				Type = GenerateLetterInputControlType.LOOKUPMULTIPLE.ToString(),
				Options = new List<SelectListItem>
				{
					new SelectListItem()
					{
						//Value = "600 Airport Road, Lakewood, NJ  08701<br/>Gaming Laboratories World Headquarters",
						//Text = "NJ",
						//Selected = true
					}
				},
				Content = "",
				SelectedOptions = new List<string>() { }

			});

			Controls.Add(new GenerateLetterInputControl()
			{
				SourceId = "Components",
				SourceProperty = "Component",
				IsGameDescription = true,
				Type = GenerateLetterInputControlType.HTML.ToString(),
				Label = "gameDescription",
				Description = "",
				Id = 19,
				LocalId = System.Guid.NewGuid(),
				Name = "Game Description",
				Content = "",
				Splittable = true
			});

			Controls.Add(new GenerateLetterInputControl()
			{
				IsModification = true,
				Type = GenerateLetterInputControlType.RADIO.ToString(),
				Label = "isModification",
				Description = "Is this a Modification?",
				Id = 21,
				LocalId = System.Guid.NewGuid(),
				Name = "ModificationsType",
				Content = "",
				Splittable = true,
				IsBusinessLogic = true,
				Options = new List<SelectListItem>
				{
					new SelectListItem { Text = "Yes", Value = "1" },
					new SelectListItem { Text = "No", Value = "" },
				}
			});
			Controls.Add(new GenerateLetterInputControl()
			{
				SourceId = "Components",
				//SourceProperty = "ModificationList",
				SourceProperty = "Component",
				IsModification = true,
				Type = GenerateLetterInputControlType.HTML.ToString(),
				Label = "modifications",
				Description = "Modifications",
				Id = 22,
				LocalId = System.Guid.NewGuid(),
				Name = "Modifications",
				Content = "",
				Splittable = true,
			});
			Controls.Add(new GenerateLetterInputControl()
			{
				SourceId = "Components",
				//SourceProperty = "ModificationList",
				SourceProperty = "Component",
				Type = GenerateLetterInputControlType.STATIC.ToString(),
				Id = 23,
				LocalId = System.Guid.NewGuid(),
				SourceFilter = "true",
				RepeatableContent = true
			});
			Controls.Add(new GenerateLetterInputControl()
			{
				SourceId = "Components",
				SourceProperty = "Component",
				Type = GenerateLetterInputControlType.RADIO.ToString(),
				Label = "versionControlled",
				Description = "Is this component version controlled?",
				Id = 24,
				LocalId = Guid.NewGuid(),
				Name = "Version Controlled",
				Content = "",
				Options = new List<SelectListItem>
				{
					new SelectListItem { Text = "Yes", Value = "1" },
					new SelectListItem { Text = "No", Value = "" },
				}
			});
			Controls.Add(new GenerateLetterInputControl()
			{
				SourceId = "Components",
				SourceProperty = "Component",
				Label = "componentTypeServerOrClient",
				Description = "What type of component is this?",
				Id = 25,
				LocalId = Guid.NewGuid(),
				Name = "componentTypeServerOrClient",
				Content = "",
				//IsBusinessLogic = true,
				Type = GenerateLetterInputControlType.DROPDOWN.ToString(),
				Options = new List<SelectListItem>()
				{
					new SelectListItem() { Value = "Server", Text = "Server" },
					new SelectListItem() { Value = "Client", Text = "Client" },
				}
			});
			Controls.Add(new GenerateLetterInputControl()
			{
				SourceId = "Components",
				SourceProperty = "Component",
				Type = GenerateLetterInputControlType.STATIC.ToString(),
				Id = 26,
				LocalId = System.Guid.NewGuid(),
				SourceFilter = "true",
				RepeatableContent = true
			});
			Controls.Add(new GenerateLetterInputControl()
			{
				Label = "browserTestedQuestion",
				Description = "Were web browser(s) evaluated?",
				Id = 27,
				LocalId = System.Guid.NewGuid(),
				Name = "browserTestedQuestion",
				Content = "",
				Type = GenerateLetterInputControlType.RADIO.ToString(),
				Options = new List<SelectListItem>
				{
					new SelectListItem { Text = "Yes", Value = "1" },
					new SelectListItem { Text = "No", Value = "" },
				}
			});
			Controls.Add(new GenerateLetterInputControl()
			{
				Label = "browserVersionTestedWithTable",
				Description = "A table containing the browser vendor(s) and version(s) evaluated.",
				Id = 28,
				ParentId = 27,
				IsBusinessLogic = true,
				LocalId = Guid.NewGuid(),
				Name = "Browser Vendor(s) and Version(s) Tested",
				ConditionalExpression = "result",
				Content = "",
				Type = GenerateLetterInputControlType.TABLE.ToString(),
				TableRows = new List<List<GenerateLetterInputControl>>()
				{
					new List<GenerateLetterInputControl>()
					{
						new GenerateLetterInputControl()
						{
							Id = 29,
							LocalId = System.Guid.NewGuid(),
							Label = "browserVersionTestedWithQuestionLabel",
							Description = "Browser tested with label.",
							Content = "Browser(s) and Version(s) Tested With:",
							Type = GenerateLetterInputControlType.STATIC.ToString(),
						},
						new GenerateLetterInputControl()
						{
							Id = 30,
							LocalId = System.Guid.NewGuid(),
							Label = "platformsAndVersionsTestedwithBrowser",
							ColumnHeader = "Browser",
							OptionDelimiter = "<br/>",
							Type = GenerateLetterInputControlType.DROPDOWN.ToString(),
							Options = new List<SelectListItem>()
							{
								new SelectListItem() { Value = "Internet Explorer", Text = "Internet Explorer" },
								new SelectListItem() { Value = "Firefox", Text = "Firefox" },
								new SelectListItem() { Value = "Safari", Text = "Safari" },
								new SelectListItem() { Value = "Chrome", Text = "Chrome" },
								new SelectListItem() { Value = "Opera", Text = "Opera" },
								new SelectListItem() { Value = "Edge", Text = "Edge" },
							}
						},
						new GenerateLetterInputControl()
						{
							Id = 31,
							ColumnHeader = "Version",
							LocalId = System.Guid.NewGuid(),
							Label = "platformsAndVersionsTestedwithBrowserVersion",
							Type = GenerateLetterInputControlType.PLAINTEXT.ToString(),
						},
						new GenerateLetterInputControl()
						{
							Label = "platformOption",
							Description = "What platform was the browser tested on?",
							Id = 2040,
							LocalId = Guid.NewGuid(),
							Name = "platformOption",
							Content = "",
							Options = new List<SelectListItem>
								{
									new SelectListItem { Text = "Mobile", Value = "Mobile" },
									new SelectListItem { Text = "Desktop", Value = "Desktop" },
								},
							Type = GenerateLetterInputControlType.DROPDOWN.ToString(),
						}
					}
				}
			});
			Controls.Add(new GenerateLetterInputControl()
			{
				Label = "adobeFlashTestedQuestion",
				Description = "Was Adobe Flash Evaluated?",
				Id = 32,
				LocalId = System.Guid.NewGuid(),
				Name = "adobeFlashTestedQuestion",
				Content = "",
				Type = GenerateLetterInputControlType.RADIO.ToString(),
				Options = new List<SelectListItem>
				{
					new SelectListItem { Text = "Yes", Value = "1" },
					new SelectListItem { Text = "No", Value = "" },
				}
			});
			Controls.Add(new GenerateLetterInputControl()
			{
				Label = "adobeFlashTestedTable",
				Description = "A table containing the Adobe Flash versions(s) evaluated.",
				Id = 33,
				ParentId = 32,
				ConditionalExpression = "result",
				IsBusinessLogic = true,
				LocalId = System.Guid.NewGuid(),
				Name = "Adobe Flash Version(s) Evaluated",
				Content = "",
				Type = GenerateLetterInputControlType.TABLE.ToString(),
				TableRows = new List<List<GenerateLetterInputControl>>()
				{
					new List<GenerateLetterInputControl>()
					{
						new GenerateLetterInputControl()
						{
							Id = 34,
							LocalId = System.Guid.NewGuid(),
							Label = "adobeFlashVersionTestedWithQuestionLabel",
							Description = "Adobe flash tested with label.",
							Content = "Adobe Flash Version(s) Tested With:",
							Type = GenerateLetterInputControlType.STATIC.ToString()
						},
						new GenerateLetterInputControl()
						{
							Id = 35,
							ColumnHeader = "Adobe Flash Version",
							LocalId = System.Guid.NewGuid(),
							Label = "adobeFlashVersionTestedWithQuestionDetail",
							Type = GenerateLetterInputControlType.PLAINTEXT.ToString(),
						}
					}
				},
			});
			Controls.Add(new GenerateLetterInputControl()
			{
				Label = "mobileDevicesTestedQuestion",
				Description = "Were Mobile device(s) evaluated?",
				Id = 36,
				LocalId = System.Guid.NewGuid(),
				Name = "mobileDevicesTestedQuestion",
				Content = "",
				Type = GenerateLetterInputControlType.RADIO.ToString(),
				Options = new List<SelectListItem>
				{
					new SelectListItem { Text = "Yes", Value = "1" },
					new SelectListItem { Text = "No", Value = "" },
				}
			});
			Controls.Add(new GenerateLetterInputControl()
			{
				Label = "mobileDevicesTestedTable",
				Description = "A table containing the Mobile Device(s) and Versions(s) evaluated.",
				Id = 37,
				ParentId = 36,
				ConditionalExpression = "result",
				IsBusinessLogic = true,
				LocalId = System.Guid.NewGuid(),
				Name = "Mobile Device(s) and Version(s) Evaluated",
				Content = "",
				Type = GenerateLetterInputControlType.TABLE.ToString(),
				TableRows = new List<List<GenerateLetterInputControl>>()
				{
					new List<GenerateLetterInputControl>()
					{
						new GenerateLetterInputControl()
						{
							Id = 38,
							LocalId = System.Guid.NewGuid(),
							Label = "mobileDevicesTestedLabel",
							Description = "Mobile device tested with label.",
							Content = "Mobile Device(s) Tested With:",
							Type = GenerateLetterInputControlType.STATIC.ToString()
						},
						new GenerateLetterInputControl()
						{
							Id = 390,
							ColumnHeader = "Device",
							LocalId = Guid.NewGuid(),
							Label = "mobileDeviceTestedSelect",
							Name = "mobileDeviceTestedSelect",
							Type = GenerateLetterInputControlType.LOOKUP.ToString(),
							ReferenceLookup = LookupAjax.MobileDevicesTested.ToString(),
							SelectedOptions = new List<string>()
						},
					}
				},
			});
			Controls.Add(new GenerateLetterInputControl()
			{
				Label = "tabletDevicesTestedQuestion",
				Description = "Were tablet device(s) evaluated?",
				Id = 42,
				LocalId = System.Guid.NewGuid(),
				Name = "tabletDevicesTestedQuestion",
				Content = "",
				Type = GenerateLetterInputControlType.RADIO.ToString(),
				Options = new List<SelectListItem>
				{
					new SelectListItem { Text = "Yes", Value = "1" },
					new SelectListItem { Text = "No", Value = "" },
				}
			});
			Controls.Add(new GenerateLetterInputControl()
			{
				Label = "tabletDevicesTestedTable",
				Description = "A table containing the Tablet Device(s) and Versions(s) evaluated.",
				Id = 43,
				ParentId = 42,
				ConditionalExpression = "result",
				IsBusinessLogic = true,
				LocalId = System.Guid.NewGuid(),
				Name = "Tablet Device(s) and Version(s) Evaluated",
				Content = "",
				Type = GenerateLetterInputControlType.TABLE.ToString(),
				TableRows = new List<List<GenerateLetterInputControl>>()
				{
					new List<GenerateLetterInputControl>()
					{
						new GenerateLetterInputControl()
						{
							Id = 44,
							LocalId = System.Guid.NewGuid(),
							Label = "tabletDevicesTestedLabel",
							Description = "Tablet device tested with label.",
							Content = "Tablet Device(s) Tested With:",
							Type = GenerateLetterInputControlType.STATIC.ToString()
						},
						new GenerateLetterInputControl()
						{
							Id = 45,
							ColumnHeader = "Make",
							LocalId = System.Guid.NewGuid(),
							Label = "tabletDevicesTestedMake",
							Type = GenerateLetterInputControlType.PLAINTEXT.ToString(),
						},
						new GenerateLetterInputControl()
						{
							Id = 46,
							ColumnHeader = "Model",
							LocalId = System.Guid.NewGuid(),
							Label = "tabletDevicesTestedModel",
							Type = GenerateLetterInputControlType.PLAINTEXT.ToString(),
						},
						new GenerateLetterInputControl()
						{
							Id = 47,
							ColumnHeader = "Model Number",
							LocalId = System.Guid.NewGuid(),
							Label = "tabletDevicesTestedModelNumber",
							Type = GenerateLetterInputControlType.PLAINTEXT.ToString(),
						}
					}
				},
			});
			Controls.Add(new GenerateLetterInputControl()
			{
				Label = "platformTestedQuestion",
				Description = "Were platform(s) evaluated?",
				Id = 48,
				LocalId = System.Guid.NewGuid(),
				Name = "platformTestedQuestion",
				Content = "",
				Type = GenerateLetterInputControlType.RADIO.ToString(),
				Options = new List<SelectListItem>
				{
					new SelectListItem { Text = "Yes", Value = "1" },
					new SelectListItem { Text = "No", Value = "" },
				}
			});
			Controls.Add(new GenerateLetterInputControl()
			{
				Label = "platformsAndVersionsTestedwithTable",
				Description = "Platform(s) and Versions(s) Evaluated",
				Id = 49,
				ParentId = 48,
				IsBusinessLogic = true,
				ConditionalExpression = "result",
				LocalId = Guid.NewGuid(),
				Name = "Platform(s) and Versions(s) Tested With",
				Content = "",
				Type = GenerateLetterInputControlType.TABLE.ToString(),
				TableRows = new List<List<GenerateLetterInputControl>>()
				{
					new List<GenerateLetterInputControl>()
					{
						new GenerateLetterInputControl()
						{
							Id = 50,
							LocalId = System.Guid.NewGuid(),
							Label = "platformsAndVersionsTestedwithLabel",
							Content = "Platform(s) and Versions(s) Tested With:",
							Type = GenerateLetterInputControlType.STATIC.ToString()
						},
						new GenerateLetterInputControl()
						{
							Id = 51,
							ColumnHeader = "Platform and Version",
							LocalId = System.Guid.NewGuid(),
							Label = "platformsAndVersionsTestedwithDetail",
							OptionDelimiter = "<br/>",
							Type = GenerateLetterInputControlType.PLAINTEXT.ToString()
						}
					}
				}
			});
			Controls.Add(new GenerateLetterInputControl()
			{
				Label = "operatingSystemsEvaluatedQuestion",
				Description = "Were operating system(s) evaluated?",
				Id = 52,
				LocalId = System.Guid.NewGuid(),
				Name = "operatingSystemsEvaluatedQuestion",
				Content = "",
				Type = GenerateLetterInputControlType.RADIO.ToString(),
				Options = new List<SelectListItem>
				{
					new SelectListItem { Text = "Yes", Value = "1" },
					new SelectListItem { Text = "No", Value = "" },
				}
			});
			Controls.Add(new GenerateLetterInputControl()
			{
				Label = "operatingSystemVersionTestedWithTable",
				Description = "Operating System(s) and Versions(s) Evaluated",
				Id = 53,
				ParentId = 52,
				IsBusinessLogic = true,
				ConditionalExpression = "result",
				LocalId = Guid.NewGuid(),
				Name = "operatingSystemVersionTestedWithTable",
				Content = "",
				Type = GenerateLetterInputControlType.TABLE.ToString(),
				TableRows = new List<List<GenerateLetterInputControl>>()
				{
					new List<GenerateLetterInputControl>()
					{
						new GenerateLetterInputControl()
						{
							Id = 54,
							LocalId = System.Guid.NewGuid(),
							Label = "operatingSystemVersionTestedWithLabel",
							Description = "Operating system(s) tested with label.",
							Content = "Operating System(s) and Version(s) Tested With:",
							Type = GenerateLetterInputControlType.STATIC.ToString()
						},
						new GenerateLetterInputControl()
						{
							Id = 55,
							ColumnHeader = "Operating System",
							LocalId = System.Guid.NewGuid(),
							Label = "operatingSystemVersionTestedWithDetail",
							OptionDelimiter = "<br/>",
							Type = GenerateLetterInputControlType.PLAINTEXT.ToString(),
						},
						new GenerateLetterInputControl()
						{
							Id = 56,
							LocalId = System.Guid.NewGuid(),
							ColumnHeader = "Version",
							Label = "operatingSystemVersionTestedWithDetailVersion",
							Type = GenerateLetterInputControlType.PLAINTEXT.ToString(),
						}
					}
				}
			});
			Controls.Add(new GenerateLetterInputControl()
			{
				Label = "deliveryMechanismsQuestion",
				Description = "What delivery mechanism(s) were used for the testing environment?",
				Id = 57,
				IsBusinessLogic = true,
				LocalId = System.Guid.NewGuid(),
				Name = "deliveryMechanismsQuestion",
				Content = "",
				Type = GenerateLetterInputControlType.MULTISELECT.ToString(),
				OptionDelimiter = ", ",
				Options = new List<SelectListItem>()
				{
					new SelectListItem() { Value = "Desktop Web-based", Text = "Desktop Web-based" },
					new SelectListItem() { Value = "Mobile Web-base", Text = "Mobile Web-based" },
					new SelectListItem() { Value = "Tablet Web-based", Text = "Tablet Web-based" },
					new SelectListItem() { Value = "Desktop Downloadable", Text = "Desktop Downloadable" },
					new SelectListItem() { Value = "Mobile Downloadable", Text = "Mobile Downloadable" },
				},
				SelectedOptions = new List<string>()
			});
			Controls.Add(new GenerateLetterInputControl()
			{
				Label = "denominations",
				Description = "What denomination(s) were used for the testing environment?",
				Id = 570,
				IsBusinessLogic = true,
				LocalId = System.Guid.NewGuid(),
				Name = "denominations",
				Content = "",
				Type = GenerateLetterInputControlType.MULTISELECT.ToString(),
				OptionDelimiter = ", ",
				Options = new List<SelectListItem>()
				{
					new SelectListItem() { Value = "$0.01", Text = "$0.01" },
					new SelectListItem() { Value = "$0.02", Text = "$0.02" },
					new SelectListItem() { Value = "$0.03", Text = "$0.03" },
					new SelectListItem() { Value = "$0.04", Text = "$0.04" },
					new SelectListItem() { Value = "$0.05", Text = "$0.05" },
					new SelectListItem() { Value = "$0.06", Text = "$0.06" },
					new SelectListItem() { Value = "$0.07", Text = "$0.07" },
					new SelectListItem() { Value = "$0.08", Text = "$0.08" },
					new SelectListItem() { Value = "$0.09", Text = "$0.09" },
					new SelectListItem() { Value = "$0.10", Text = "$0.10" },
				},
				SelectedOptions = new List<string>()
			});
			Controls.Add(new GenerateLetterInputControl()
			{
				Label = "lineConfigurations",
				Description = "What line configuration(s) were used for the testing environment?",
				Id = 571,
				IsBusinessLogic = true,
				LocalId = System.Guid.NewGuid(),
				Name = "lineConfigurations",
				Content = "",
				Type = GenerateLetterInputControlType.MULTISELECT.ToString(),
				OptionDelimiter = ", ",
				Options = new List<SelectListItem>()
				{
					new SelectListItem() { Value = "5-Reel", Text = "5-Reel" },
					new SelectListItem() { Value = "25-Line", Text = "25-Line" },
				},
				SelectedOptions = new List<string>()
			});
			Controls.Add(new GenerateLetterInputControl()
			{
				Label = "doubleUp",
				Description = "Are double up features supported?",
				Id = 572,
				LocalId = Guid.NewGuid(),
				Name = "doubleUp",
				Content = "",
				//IsBusinessLogic = true,
				Type = GenerateLetterInputControlType.DROPDOWN.ToString(),
				Options = new List<SelectListItem>()
				{
					new SelectListItem() { Value = "Supported", Text = "Supported" },
					new SelectListItem() { Value = "Not Supported", Text = "Not Supported" },
				}
			});
			Controls.Add(new GenerateLetterInputControl()
			{
				Label = "bonusFeatures",
				Description = "Are bonus features supported?",
				Id = 576,
				LocalId = Guid.NewGuid(),
				Name = "bonusFeatures",
				Content = "",
				//IsBusinessLogic = true,
				Type = GenerateLetterInputControlType.DROPDOWN.ToString(),
				Options = new List<SelectListItem>()
				{
					new SelectListItem() { Value = "Supported", Text = "Supported" },
					new SelectListItem() { Value = "Not Supported", Text = "Not Supported" },
				}
			});
			Controls.Add(new GenerateLetterInputControl()
			{
				Label = "progressives",
				Description = "Are progressives supported?",
				Id = 573,
				LocalId = Guid.NewGuid(),
				Name = "progressives",
				Content = "",
				//IsBusinessLogic = true,
				Type = GenerateLetterInputControlType.DROPDOWN.ToString(),
				Options = new List<SelectListItem>()
				{
					new SelectListItem() { Value = "Supported", Text = "Supported" },
					new SelectListItem() { Value = "Not Supported", Text = "Not Supported" },
				}
			});
			Controls.Add(new GenerateLetterInputControl()
			{
				Label = "autoplay",
				Description = "Is Autoplay supported?",
				Id = 574,
				LocalId = Guid.NewGuid(),
				Name = "autoplay",
				Content = "",
				//IsBusinessLogic = true,
				Type = GenerateLetterInputControlType.DROPDOWN.ToString(),
				Options = new List<SelectListItem>()
				{
					new SelectListItem() { Value = "Supported", Text = "Supported" },
					new SelectListItem() { Value = "Not Supported", Text = "Not Supported" },
				}
			});

			Controls.Add(new GenerateLetterInputControl()
			{
				Id = 575,
				LocalId = Guid.NewGuid(),
				Description = "What languages are supported?",
				Name = "languages",
				Label = "languages",
				Content = "",
				Type = GenerateLetterInputControlType.MULTISELECT.ToString(),
				OptionDelimiter = ", ",
				ReferenceLookup = LookupAjax.Language.ToString(),
				Options = LookupsStandard.ConvertEnum<LetterLanguage>()
					.Select(x => new SelectListItem { Value = x.Value, Text = x.Value }).ToList(),
				SelectedOptions = new List<string>() { }
			});

			Controls.Add(new GenerateLetterInputControl()
			{
				Type = GenerateLetterInputControlType.MULTISELECT.ToString(),
				Label = "deskTopClients",
				Description = "Which downloadable Desktop Client(s) were evaluated?",
				Id = 58,
				LocalId = Guid.NewGuid(),
				Name = "Downloadable Desktop Client(s)",
				Content = "",
				IsBusinessLogic = true,
				ParentId = 57,
				ConditionalExpression = "result.includes('Desktop Downloadable')",
				OptionDelimiter = ", ",
				Options = new List<SelectListItem>()
				{
					new SelectListItem() { Value = "Windows", Text = "Windows" },
					new SelectListItem() { Value = "Mac", Text = "Mac" },
				}
			});
			Controls.Add(new GenerateLetterInputControl()
			{
				Label = "windowsClientInstallerPackage",
				Description = "You indicated a Windows downloadable client was evaluated, what Windows Client Installer Package was used?",
				Id = 59,
				LocalId = Guid.NewGuid(),
				Name = "Windows Client Installer Package:",
				Content = "",
				IsBusinessLogic = true,
				ParentId = 58,
				ConditionalExpression = "result.includes('Windows')",
				Type = GenerateLetterInputControlType.PLAINTEXT.ToString()
			});
			Controls.Add(new GenerateLetterInputControl()
			{
				Label = "macClientInstallerPackage",
				Description = "You indicated a Mac downloadable client was evaluated, what Mac Client Installer Package was used?",
				Id = 60,
				LocalId = Guid.NewGuid(),
				Name = "Mac Client Installer Package:",
				Content = "",
				IsBusinessLogic = true,
				ParentId = 58,
				ConditionalExpression = "result.includes('Mac')",
				Type = GenerateLetterInputControlType.PLAINTEXT.ToString()
			});
			Controls.Add(new GenerateLetterInputControl()
			{
				Label = "mobileClientInstallerPackage",
				Description = "You indicated a Mobile downloadable client was evaluated, what Mobile Client Installer Package was used?",
				Id = 61,
				LocalId = Guid.NewGuid(),
				Name = "Mobile Client Installer Package",
				Content = "",
				IsBusinessLogic = true,
				ParentId = 57,
				ConditionalExpression = "result.includes('Mobile Downloadable')",
				Type = GenerateLetterInputControlType.PLAINTEXT.ToString()
			});
			Controls.Add(new GenerateLetterInputControl()
			{
				Label = "deliveryMechanismTypes",
				Description = "What delivery mechanism types(s) were used for the testing environment?",
				Id = 62,
				LocalId = Guid.NewGuid(),
				Name = "Delivery Mechanism Types",
				Content = "",
				Type = GenerateLetterInputControlType.MULTISELECT.ToString(),
				OptionDelimiter = ", ",
				Options = new List<SelectListItem>()
				{
					new SelectListItem() { Value = "Embedded Flash", Text = "Embedded Flash" },
					new SelectListItem() { Value = "Flash", Text = "Flash" },
					new SelectListItem() { Value = "HTML", Text = "HTML" },
					new SelectListItem() { Value = "HTML5", Text = "HTML5" },
				},
				SelectedOptions = new List<string>()
			});
			Controls.Add(new GenerateLetterInputControl()
			{
				Label = "correctionAddendumQuestion",
				Description = "Is this a correction or Addendum ?",
				Id = 63,
				LocalId = System.Guid.NewGuid(),
				Name = "correctionAddendumQuestion",
				Content = "",
				Type = GenerateLetterInputControlType.RADIO.ToString(),
				Options = new List<SelectListItem>
				{
					new SelectListItem { Text = "Yes", Value = "1" },
					new SelectListItem { Text = "No", Value = "" },
				}
			});
			Controls.Add(new GenerateLetterInputControl()
			{
				Label = "nonGuaranteedPoints",
				Description = "Include note that there about non guaranteed points?",
				Id = 630,
				LocalId = System.Guid.NewGuid(),
				Name = "nonGuaranteedPoints",
				Content = "",
				Type = GenerateLetterInputControlType.RADIO.ToString(),
				Options = new List<SelectListItem>
				{
					new SelectListItem { Text = "Yes", Value = "1" },
					new SelectListItem { Text = "No", Value = "" },
				}
			});
			Controls.Add(new GenerateLetterInputControl()
			{
				Label = "belarusToolNote",
				Description = "Include note about tool used to generate checksums?",
				Id = 631,
				LocalId = System.Guid.NewGuid(),
				Name = "belarusToolNote",
				Content = "",
				Type = GenerateLetterInputControlType.RADIO.ToString(),
				Options = new List<SelectListItem>
				{
					new SelectListItem { Text = "Yes", Value = "1" },
					new SelectListItem { Text = "No", Value = "" },
				}
			});
			Controls.Add(new GenerateLetterInputControl()
			{
				Label = "correctionAddendumOriginalReportDateQuestion",
				Description = "You indicated this a correction or Addendum, what was the date of the Original Report ?",
				Id = 64,
				ParentId = 63,
				IsBusinessLogic = true,
				ConditionalExpression = "result",
				LocalId = System.Guid.NewGuid(),
				Name = "correctionAddendumOriginalReportDateQuestion",
				Content = "",
				Type = GenerateLetterInputControlType.DATEPICKER.ToString(),
			});
			Controls.Add(new GenerateLetterInputControl()
			{
				Label = "correctionAddendumChangesTable",
				Description = "A table containing the changes for this correction/addendum letter.",
				Id = 65,
				ParentId = 63,
				IsBusinessLogic = true,
				LocalId = Guid.NewGuid(),
				Name = "Change List",
				ConditionalExpression = "result",
				Content = "",
				Type = GenerateLetterInputControlType.TABLE.ToString(),
				TableRows = new List<List<GenerateLetterInputControl>>()
				{
					new List<GenerateLetterInputControl>()
					{
						new GenerateLetterInputControl()
						{
							Id = 63,
							ColumnHeader = "Change",
							LocalId = System.Guid.NewGuid(),
							Label = "correctionAddendumChangeDetail",
							Type = GenerateLetterInputControlType.PLAINTEXT.ToString(),
						}
					}
				}
			});
			//Controls.Add(new GenerateLetterInputControl()
			//{
			//	Label = "gameNameQuestion",
			//	Description = "What game name should be used in the compliance evaluation?",
			//	Id = 66,
			//	LocalId = System.Guid.NewGuid(),
			//	Name = "gameNameQuestion",
			//	Content = "",
			//	Type = GenerateLetterInputControlType.PLAINTEXT.ToString(),
			//});
			Controls.Add(new GenerateLetterInputControl()
			{
				Label = "additionalFIFNotesQuestion",
				Description = "Are there additional notes contained on the File Information Form ?",
				Id = 67,
				LocalId = System.Guid.NewGuid(),
				Name = "additionalFIFNotesQuestion",
				Content = "",
				Type = GenerateLetterInputControlType.RADIO.ToString(),
				Options = new List<SelectListItem>
				{
					new SelectListItem { Text = "Yes", Value = "1" },
					new SelectListItem { Text = "No", Value = "" },
				}
			});
			Controls.Add(new GenerateLetterInputControl()
			{
				Label = "additionalFIFNotesTable",
				Description = "A table containing the additional notes from the File Information Form",
				Id = 68,
				ParentId = 67,
				IsBusinessLogic = true,
				LocalId = Guid.NewGuid(),
				Name = "File information Notes",
				ConditionalExpression = "result",
				Content = "",
				Type = GenerateLetterInputControlType.TABLE.ToString(),
				TableRows = new List<List<GenerateLetterInputControl>>()
				{
					new List<GenerateLetterInputControl>()
					{
						new GenerateLetterInputControl()
						{
							Id = 69,
							ColumnHeader = "Label",
							LocalId = System.Guid.NewGuid(),
							Label = "fileInformationFormNoteLabel",
							Type = GenerateLetterInputControlType.PLAINTEXT.ToString(),
						},
						new GenerateLetterInputControl()
						{
							Id = 70,
							ColumnHeader = "Note",
							LocalId = System.Guid.NewGuid(),
							Label = "fileInformationFormNoteDetail",
							Type = GenerateLetterInputControlType.TEXTAREA.ToString(),
						}
					}
				}
			});
			//Controls.Add(new GenerateLetterInputControl()
			//{
			//	Label = "gameNameQuestion",
			//	Description = "What game name should be used in the conclusion?",
			//	Id = 71,
			//	LocalId = System.Guid.NewGuid(),
			//	Name = "gameNameQuestion",
			//	Content = "",
			//	Type = GenerateLetterInputControlType.PLAINTEXT.ToString(),
			//});
			Controls.Add(new GenerateLetterInputControl()
			{
				SourceId = "Paytables",
				SourceProperty = "Paytable",
				Type = GenerateLetterInputControlType.STATIC.ToString(),
				Id = 72,
				LocalId = Guid.NewGuid(),
				SourceFilter = "true",
				RepeatableContent = true
			});
			Controls.Add(new GenerateLetterInputControl()
			{
				SourceId = "Paytables",
				SourceProperty = "Paytable",
				Type = GenerateLetterInputControlType.PLAINTEXT.ToString(),
				Label = "manufacturerMin",
				Description = "Manufacturer Minimum RTP",
				Id = 73,
				LocalId = System.Guid.NewGuid(),
				Name = "Manufacturer Minimum RTP",
				Content = ".8876"
			});
			Controls.Add(new GenerateLetterInputControl()
			{
				SourceId = "Paytables",
				SourceProperty = "Paytable",
				Type = GenerateLetterInputControlType.PLAINTEXT.ToString(),
				Label = "manufacturerMax",
				Description = "Manufacturer Maximum RTP",
				Id = 74,
				LocalId = System.Guid.NewGuid(),
				Name = "Manufacturer Maximum RTP",
				Content = ".8946"
			});
			Controls.Add(new GenerateLetterInputControl()
			{
				Label = "note1",
				Description = "Include analytical method note?",
				Id = 75,
				LocalId = Guid.NewGuid(),
				Name = "note1",
				Content = "",
				Type = GenerateLetterInputControlType.RADIO.ToString(),
				Options = new List<SelectListItem>
				{
					new SelectListItem { Text = "Yes", Value = "1" },
					new SelectListItem { Text = "No", Value = "" },
				}
			});
			Controls.Add(new GenerateLetterInputControl()
			{
				Label = "note10",
				Description = "Include progressive jackpot note?",
				Id = 750,
				LocalId = Guid.NewGuid(),
				Name = "note10",
				Content = "",
				Type = GenerateLetterInputControlType.RADIO.ToString(),
				Options = new List<SelectListItem>
				{
					new SelectListItem { Text = "Yes", Value = "1" },
					new SelectListItem { Text = "No", Value = "" },
				}
			});
			Controls.Add(new GenerateLetterInputControl()
			{
				Label = "note2",
				Description = "Include RTP Range note?",
				Id = 76,
				LocalId = Guid.NewGuid(),
				Name = "note2",
				Content = "",
				Type = GenerateLetterInputControlType.RADIO.ToString(),
				Options = new List<SelectListItem>
				{
					new SelectListItem { Text = "Yes", Value = "1" },
					new SelectListItem { Text = "No", Value = "" },
				}
			});
			Controls.Add(new GenerateLetterInputControl()
			{
				Label = "note3",
				Description = "Include GLI's simulation method note?",
				Id = 77,
				LocalId = Guid.NewGuid(),
				Name = "note3",
				Content = "",
				Type = GenerateLetterInputControlType.RADIO.ToString(),
				Options = new List<SelectListItem>
				{
					new SelectListItem { Text = "Yes", Value = "1" },
					new SelectListItem { Text = "No", Value = "" },
				}
			});
			Controls.Add(new GenerateLetterInputControl()
			{
				Label = "note4",
				Description = "Include different RTP note?",
				Id = 78,
				LocalId = Guid.NewGuid(),
				Name = "note4",
				Content = "",
				Type = GenerateLetterInputControlType.RADIO.ToString(),
				Options = new List<SelectListItem>
				{
					new SelectListItem { Text = "Yes", Value = "1" },
					new SelectListItem { Text = "No", Value = "" },
				}
			});
			Controls.Add(new GenerateLetterInputControl()
			{
				Label = "note5",
				Description = "Include RTP rounding note?",
				Id = 79,
				LocalId = Guid.NewGuid(),
				Name = "note5",
				Content = "",
				Type = GenerateLetterInputControlType.RADIO.ToString(),
				Options = new List<SelectListItem>
				{
					new SelectListItem { Text = "Yes", Value = "1" },
					new SelectListItem { Text = "No", Value = "" },
				}
			});
			//
			Controls.Add(new GenerateLetterInputControl()
			{
				Label = "rtsArticleQuestion75",
				Description = "Include RTS articles 7A, 7B, 7C, 7E, 5A?",
				Id = 80,
				LocalId = Guid.NewGuid(),
				Name = "rtsArticleQuestion75",
				Content = "1",
				Options = new List<SelectListItem>
					{
						new SelectListItem { Text = "Yes", Value = "1" },
						new SelectListItem { Text = "No", Value = "" },
					},
				Type = GenerateLetterInputControlType.RADIO.ToString(),
			});
			Controls.Add(new GenerateLetterInputControl()
			{
				Label = "rtsArticleQuestion9",
				Description = "Include RTS articles 9B(b) and 9B(d)?",
				Id = 81,
				LocalId = Guid.NewGuid(),
				Name = "rtsArticleQuestion9",
				Content = "",
				Options = new List<SelectListItem>
					{
						new SelectListItem { Text = "Yes", Value = "1" },
						new SelectListItem { Text = "No", Value = "" }
					},
				Type = GenerateLetterInputControlType.RADIO.ToString(),
			});
			Controls.Add(new GenerateLetterInputControl()
			{
				SourceId = "Components",
				SourceProperty = "Component",
				Label = "clientType",
				Description = "What type of client is this?",
				Id = 82,
				LocalId = Guid.NewGuid(),
				Name = "clientType",
				Content = "",
				ParentId = 25,
				IsBusinessLogic = true,
				Type = GenerateLetterInputControlType.PLAINTEXT.ToString(),
				ConditionalExpression = "result === 'Client'",
			});
			Controls.Add(new GenerateLetterInputControl()
			{
				Label = "gamblingAct2005",
				Description = "Include Gambling Act 2005?",
				Id = 83,
				LocalId = Guid.NewGuid(),
				Name = "gamblingAct2005",
				Content = "",
				Options = new List<SelectListItem>
					{
						new SelectListItem { Text = "Yes", Value = "1" },
						new SelectListItem { Text = "No", Value = "" },
					},
				Type = GenerateLetterInputControlType.RADIO.ToString(),
			});
			Controls.Add(new GenerateLetterInputControl()
			{
				Label = "standard2",
				Description = "Include Standard 2 - Registration of Participants and Administration for Game Evaluation?",
				Id = 84,
				LocalId = Guid.NewGuid(),
				Name = "standard2",
				Content = "",
				Options = new List<SelectListItem>
					{
						new SelectListItem { Text = "Yes", Value = "1" },
						new SelectListItem { Text = "No", Value = "" },
					},
				Type = GenerateLetterInputControlType.RADIO.ToString(),
			});
			Controls.Add(new GenerateLetterInputControl()
			{
				Label = "standard3",
				Description = "Include Standard 3 - Presentation of Rules and Customer Information for Game Evaluation?",
				Id = 85,
				LocalId = Guid.NewGuid(),
				Name = "standard3",
				Content = "",
				Options = new List<SelectListItem>
					{
						new SelectListItem { Text = "Yes", Value = "1" },
						new SelectListItem { Text = "No", Value = "" },
					},
				Type = GenerateLetterInputControlType.RADIO.ToString(),
			});
			Controls.Add(new GenerateLetterInputControl()
			{
				Label = "standard4",
				Description = "Include Standard 4 - Responsible Gambling Awareness and Provision of Information for Game Evaluation?",
				Id = 86,
				LocalId = Guid.NewGuid(),
				Name = "standard4",
				Content = "",
				Options = new List<SelectListItem>
					{
						new SelectListItem { Text = "Yes", Value = "1" },
						new SelectListItem { Text = "No", Value = "" },
					},
				Type = GenerateLetterInputControlType.RADIO.ToString(),
			});
			Controls.Add(new GenerateLetterInputControl()
			{
				Label = "standard6",
				Description = "Include Standard 6 - Preventing Underage Gambling for Game Evaluation?",
				Id = 87,
				LocalId = Guid.NewGuid(),
				Name = "standard6",
				Content = "",
				Options = new List<SelectListItem>
					{
						new SelectListItem { Text = "Yes", Value = "1" },
						new SelectListItem { Text = "No", Value = "" },
					},
				Type = GenerateLetterInputControlType.RADIO.ToString(),
			});
			Controls.Add(new GenerateLetterInputControl()
			{
				Label = "standard7",
				Description = "Include Standard 7 - Fair Gambling for Game Evaluation?",
				Id = 88,
				LocalId = Guid.NewGuid(),
				Name = "standard7",
				Content = "",
				Options = new List<SelectListItem>
					{
						new SelectListItem { Text = "Yes", Value = "1" },
						new SelectListItem { Text = "No", Value = "" },
					},
				Type = GenerateLetterInputControlType.RADIO.ToString(),
			});
			Controls.Add(new GenerateLetterInputControl()
			{
				Label = "standard9",
				Description = "Include Standard 9 - Business Continuity for Game Evaluation?",
				Id = 89,
				LocalId = Guid.NewGuid(),
				Name = "standard9",
				Content = "",
				Options = new List<SelectListItem>
					{
						new SelectListItem { Text = "Yes", Value = "1" },
						new SelectListItem { Text = "No", Value = "" },
					},
				Type = GenerateLetterInputControlType.RADIO.ToString(),
			});
			Controls.Add(new GenerateLetterInputControl()
			{
				Label = "addtlGameEval",
				Description = "Include Aditional Game Evaluation",
				Id = 90,
				LocalId = Guid.NewGuid(),
				Name = "addtlGameEval",
				Content = "",
				Type = GenerateLetterInputControlType.HTML.ToString(),
			});
			Controls.Add(new GenerateLetterInputControl()
			{
				Label = "additionalNotesTable",
				Description = "Add any additional notes for Conditions Of Evaluation",
				Id = 91,
				LocalId = Guid.NewGuid(),
				Name = "AdditionalNotes",
				Content = "",
				Type = GenerateLetterInputControlType.TABLE.ToString(),
				TableRows = new List<List<GenerateLetterInputControl>>()
				{
					new List<GenerateLetterInputControl>()
					{
						new GenerateLetterInputControl()
						{
							Id = 92,
							ColumnHeader = "Note",
							LocalId = System.Guid.NewGuid(),
							Label = "detail",
							Type = GenerateLetterInputControlType.HTML.ToString(),
						}
					}
				}
			});
			Controls.Add(new GenerateLetterInputControl()
			{

				SourceId = "ClauseReports",
				SourceProperty = "Clause",
				Type = GenerateLetterInputControlType.STATIC.ToString(),
				Id = 92,
				LocalId = System.Guid.NewGuid(),
				SourceFilter = "true",
				RepeatableContent = true
			});
			Controls.Add(new GenerateLetterInputControl()
			{
				Label = "condOfEvalPlayerFacingHistory",
				Description = "Include the Player Facing History not available note ?",
				Id = 93,
				LocalId = Guid.NewGuid(),
				Name = "condOfEvalPlayerFacingHistory",
				Content = "",
				Options = new List<SelectListItem>
					{
						new SelectListItem { Text = "Yes", Value = "1" },
						new SelectListItem { Text = "No", Value = "" },
					},
				Type = GenerateLetterInputControlType.RADIO.ToString(),
			});
			Controls.Add(new GenerateLetterInputControl()
			{
				Label = "condOfEvalSigCompareDisclaimer",
				Description = "Include the GLI lack of facility to compare all signatures disclaimer?",
				Id = 94,
				LocalId = Guid.NewGuid(),
				Name = "condOfEvalSigCompareDisclaimer",
				Content = "",
				Options = new List<SelectListItem>
					{
						new SelectListItem { Text = "Yes", Value = "1" },
						new SelectListItem { Text = "No", Value = "" },
					},
				Type = GenerateLetterInputControlType.RADIO.ToString(),
			});
			Controls.Add(new GenerateLetterInputControl()
			{
				Label = "condOfEvalPlayerBalanceRecordingDisclaimer",
				Description = "Include the player balance not recorded before and after game play disclaimer?",
				Id = 95,
				LocalId = Guid.NewGuid(),
				Name = "condOfEvalPlayerBalanceRecordingDisclaimer",
				Content = "",
				Options = new List<SelectListItem>
					{
						new SelectListItem { Text = "Yes", Value = "1" },
						new SelectListItem { Text = "No", Value = "" },
					},
				Type = GenerateLetterInputControlType.RADIO.ToString(),
			});
			Controls.Add(new GenerateLetterInputControl()
			{
				Label = "applicableReqForJurs",
				Description = "Are there any applicable jurisdictional requirements?",
				Id = 96,
				LocalId = Guid.NewGuid(),
				Name = "applicableReqForJurs",
				Content = "",
				Options = new List<SelectListItem>
					{
						new SelectListItem { Text = "Yes", Value = "1" },
						new SelectListItem { Text = "No", Value = "" },
					},
				Type = GenerateLetterInputControlType.RADIO.ToString(),
			});
			Controls.Add(new GenerateLetterInputControl()
			{
				Id = 97,
				LocalId = Guid.NewGuid(),
				Label = "reportType",
				Description = "Report Type",
				Name = "reportType",
				Content = "Evaluation and Certification",
				//IsBusinessLogic = true,
				Type = GenerateLetterInputControlType.DROPDOWN.ToString(),
				Options = new List<SelectListItem>()
				{
					new SelectListItem() { Value = "Evaluation and Certification", Text = "Evaluation and Certification" },
					new SelectListItem() { Value = "Correction", Text = "Correction" },
				}
			});
			Controls.Add(new GenerateLetterInputControl()
			{
				SourceId = "Components",
				SourceProperty = "Component",
				Type = GenerateLetterInputControlType.DROPDOWN.ToString(),
				Label = "gameType",
				Description = "Game Type",
				Id = 98,
				LocalId = System.Guid.NewGuid(),
				Name = "Game Type",
				Options = new List<SelectListItem>()
						{
							new SelectListItem() { Value = "Slot Game", Text = "Slot Game" },
							new SelectListItem() { Value = "Card Game", Text = "Card Game" },
						},
				Content = ""
			});
			Controls.Add(new GenerateLetterInputControl()
			{
				SourceId = "Components",
				SourceProperty = "Component",
				Label = "deliveryCompMechanismTypes",
				Description = "What delivery mechanism types(s) were used?",
				Id = 99,
				LocalId = Guid.NewGuid(),
				Name = "Delivery Mechanism Types",
				Content = "",
				Type = GenerateLetterInputControlType.MULTISELECT.ToString(),
				OptionDelimiter = ", ",
				Options = new List<SelectListItem>()
				{
					new SelectListItem() { Value = "Embedded Flash", Text = "Embedded Flash" },
					new SelectListItem() { Value = "Flash", Text = "Flash" },
					new SelectListItem() { Value = "HTML", Text = "HTML" },
					new SelectListItem() { Value = "HTML5", Text = "HTML5" },
				},
				SelectedOptions = new List<string>()
			});
			Controls.Add(new GenerateLetterInputControl()
			{
				SourceId = "Components",
				SourceProperty = "Component",
				Label = "rngApprovedReport",
				Description = "What RNG approved Report used?",
				Id = 100,
				LocalId = Guid.NewGuid(),
				Name = "RNG approved Report",
				Content = "",
				Type = GenerateLetterInputControlType.PLAINTEXT.ToString(),
			});
			Controls.Add(new GenerateLetterInputControl()
			{
				SourceId = "Components",
				SourceProperty = "Component",
				Label = "versionOnConclusion",
				Description = "What is the Game Client version on conlcusion of the evaluation?",
				Id = 1001,
				LocalId = Guid.NewGuid(),
				Name = "Game Client Version",
				Content = "",
				Type = GenerateLetterInputControlType.PLAINTEXT.ToString(),
			});
			Controls.Add(new GenerateLetterInputControl()
			{
				Type = GenerateLetterInputControlType.DROPDOWN.ToString(),
				Label = "internalMethodUsed",
				Description = "Internal methods used",
				Id = 101,
				LocalId = System.Guid.NewGuid(),
				Name = "Internal methods",
				Options = new List<SelectListItem>()
						{
							new SelectListItem() { Value = "WIP 24", Text = "WIP 24" },
							new SelectListItem() { Value = "WIP 25", Text = "WIP 25" },
						},
				Content = "WIP 24"
			});
			Controls.Add(new GenerateLetterInputControl()
			{
				Label = "additionalNotesGameEvalTable",
				Description = "Add any additional notes for Game Of Evaluation",
				Id = 103,
				LocalId = Guid.NewGuid(),
				Name = "AdditionalGameEvalNotes",
				Content = "",
				Type = GenerateLetterInputControlType.TABLE.ToString(),
				TableRows = new List<List<GenerateLetterInputControl>>()
				{
					new List<GenerateLetterInputControl>()
					{
						new GenerateLetterInputControl()
						{
							Id = 104,
							ColumnHeader = "Note",
							LocalId = System.Guid.NewGuid(),
							Label = "detail",
							Type = GenerateLetterInputControlType.HTML.ToString(),
						}
					}
				}
			});
			Controls.Add(new GenerateLetterInputControl()
			{
				Label = "recommendations",
				Description = "Enter Recommendations to be made",
				Id = 105,
				LocalId = Guid.NewGuid(),
				Name = "Recommendations",
				Content = "There are no recommendations to be made.",
				Type = GenerateLetterInputControlType.HTML.ToString(),
			});
			Controls.Add(new GenerateLetterInputControl()
			{
				Label = "condOfComplianceTesting",
				Description = "Include the GLI Compliance testing available note ?",
				Id = 106,
				LocalId = Guid.NewGuid(),
				Name = "CondOfEvalGLIComplaiceNote",
				Content = "",
				Options = new List<SelectListItem>
					{
						new SelectListItem { Text = "Yes", Value = "1" },
						new SelectListItem { Text = "No", Value = "" },
					},
				Type = GenerateLetterInputControlType.RADIO.ToString(),
			});
			Controls.Add(new GenerateLetterInputControl()
			{
				Label = "condOfresponsibilityOperator",
				Description = "Include REsponsibility Operator note ?",
				Id = 107,
				LocalId = Guid.NewGuid(),
				Name = "CondOfResponsibilityOperator",
				Content = "",
				Options = new List<SelectListItem>
					{
						new SelectListItem { Text = "Yes", Value = "1" },
						new SelectListItem { Text = "No", Value = "" },
					},
				Type = GenerateLetterInputControlType.RADIO.ToString(),
			});
			Controls.Add(new GenerateLetterInputControl()
			{
				Label = "confidenceLevel95",
				Description = "Include 95% Confidence Level note?",
				Id = 108,
				LocalId = Guid.NewGuid(),
				Name = "confidenceLevel95",
				Content = "",
				Options = new List<SelectListItem>
					{
						new SelectListItem { Text = "Yes", Value = "1" },
						new SelectListItem { Text = "No", Value = "" },
					},
				Type = GenerateLetterInputControlType.RADIO.ToString(),
			});
			Controls.Add(new GenerateLetterInputControl()
			{
				Label = "accreditationQuestion",
				Description = "Registration number of accreditation.",
				Id = 109,
				LocalId = Guid.NewGuid(),
				Name = "accreditationQuestion",
				Content = "",
				Type = GenerateLetterInputControlType.LOOKUP.ToString(),
				ReferenceLookup = "Accreditations",
				SelectedOptions = new List<string>() { }
			});
			Controls.Add(new GenerateLetterInputControl()
			{
				Label = "supportColombianPesos",
				Description = "Include Note: Supports Colombian Pesos?",
				Id = 110,
				LocalId = Guid.NewGuid(),
				Name = "supportColombianPesos",
				Content = "",
				Options = new List<SelectListItem>
					{
						new SelectListItem { Text = "Yes", Value = "1" },
						new SelectListItem { Text = "No", Value = "" },
					},
				Type = GenerateLetterInputControlType.RADIO.ToString(),
			});
			Controls.Add(new GenerateLetterInputControl()
			{
				Label = "section4_3",
				Description = "Include Section 4.3: Random Number Generator (RNG) Requirements?",
				Id = 200,
				LocalId = Guid.NewGuid(),
				Name = "section4_3",
				Content = "",
				Options = new List<SelectListItem>
				{
					new SelectListItem { Text = "Yes", Value = "1" },
					new SelectListItem { Text = "No", Value = "" },
				},
				Type = GenerateLetterInputControlType.RADIO.ToString(),
			});
			Controls.Add(new GenerateLetterInputControl()
			{
				Label = "section4_7",
				Description = "Include Section 4.7: eGambling Requirements?",
				Id = 201,
				LocalId = Guid.NewGuid(),
				Name = "section4_7",
				Content = "",
				Options = new List<SelectListItem>
					{
						new SelectListItem { Text = "Yes", Value = "1" },
						new SelectListItem { Text = "No", Value = "" },
					},
				Type = GenerateLetterInputControlType.RADIO.ToString(),
			});
			Controls.Add(new GenerateLetterInputControl()
			{
				Label = "section4_8",
				Description = "Include Section 4.8: Game Design?",
				Id = 202,
				LocalId = Guid.NewGuid(),
				Name = "section4_8",
				Content = "",
				Options = new List<SelectListItem>
					{
						new SelectListItem { Text = "Yes", Value = "1" },
						new SelectListItem { Text = "No", Value = "" },
					},
				Type = GenerateLetterInputControlType.RADIO.ToString(),
			});
			Controls.Add(new GenerateLetterInputControl()
			{
				Label = "section4_10",
				Description = "Include Section 4.10: Game Artwork?",
				Id = 203,
				LocalId = Guid.NewGuid(),
				Name = "section4_10",
				Content = "",
				Options = new List<SelectListItem>
					{
						new SelectListItem { Text = "Yes", Value = "1" },
						new SelectListItem { Text = "No", Value = "" },
					},
				Type = GenerateLetterInputControlType.RADIO.ToString(),
			});
			Controls.Add(new GenerateLetterInputControl()
			{
				Label = "section4_11",
				Description = "Include Section 4.11: Spinning Wheel (Reel) Requirements?",
				Id = 204,
				LocalId = Guid.NewGuid(),
				Name = "section4_11",
				Content = "",
				Options = new List<SelectListItem>
					{
						new SelectListItem { Text = "Yes", Value = "1" },
						new SelectListItem { Text = "No", Value = "" },
					},
				Type = GenerateLetterInputControlType.RADIO.ToString(),
			});
			Controls.Add(new GenerateLetterInputControl()
			{
				Label = "section4_12",
				Description = "Include Section 4.12: Positioning, Size, Colour and Shape?",
				Id = 205,
				LocalId = Guid.NewGuid(),
				Name = "section4_12",
				Content = "",
				Options = new List<SelectListItem>
					{
						new SelectListItem { Text = "Yes", Value = "1" },
						new SelectListItem { Text = "No", Value = "" },
					},
				Type = GenerateLetterInputControlType.RADIO.ToString(),
			});
			Controls.Add(new GenerateLetterInputControl()
			{
				Label = "section4_14",
				Description = "Include Section 4.14: Winning Patterns?",
				Id = 206,
				LocalId = Guid.NewGuid(),
				Name = "section4_14",
				Content = "",
				Options = new List<SelectListItem>
					{
						new SelectListItem { Text = "Yes", Value = "1" },
						new SelectListItem { Text = "No", Value = "" },
					},
				Type = GenerateLetterInputControlType.RADIO.ToString(),
			});
			Controls.Add(new GenerateLetterInputControl()
			{
				Label = "section4_15",
				Description = "Include Section 4.15: Features?",
				Id = 207,
				LocalId = Guid.NewGuid(),
				Name = "section4_15",
				Content = "",
				Options = new List<SelectListItem>
					{
						new SelectListItem { Text = "Yes", Value = "1" },
						new SelectListItem { Text = "No", Value = "" },
					},
				Type = GenerateLetterInputControlType.RADIO.ToString(),
			});
			Controls.Add(new GenerateLetterInputControl()
			{
				Label = "certificationIssuerOverride",
				Description = "Would you like to override the default Certification Issuer choice? (The default is based off the Certification Lab)",
				Id = 208,
				LocalId = Guid.NewGuid(),
				Name = "certificationIssuerOverride",
				Content = null,
				Type = GenerateLetterInputControlType.LOOKUP.ToString(),
				ReferenceLookup = "CertificationIssuers"
			});
			Controls.Add(new GenerateLetterInputControl()
			{
				Label = "contactInfo",
				Description = "Include contact our office note ?",
				Id = 209,
				LocalId = Guid.NewGuid(),
				Name = "contactInfo",
				Content = "1",
				Options = new List<SelectListItem>
					{
						new SelectListItem { Text = "Yes", Value = "1" },
						new SelectListItem { Text = "No", Value = "" },
					},
				Type = GenerateLetterInputControlType.RADIO.ToString(),
			});
			Controls.Add(new GenerateLetterInputControl()
			{
				Label = "notCertification",
				Description = "Include Note: This report is not a certification for any jurisdiction.",
				Id = 210,
				LocalId = Guid.NewGuid(),
				Name = "notCertification",
				Content = "1",
				Options = new List<SelectListItem>
					{
						new SelectListItem { Text = "Yes", Value = "1" },
						new SelectListItem { Text = "No", Value = "" },
					},
				Type = GenerateLetterInputControlType.RADIO.ToString(),
			});
			Controls.Add(new GenerateLetterInputControl()
			{
				Label = "technicalRequirementIOG",
				Description = "Include Technical Requirements for Internet Operated Games?",
				Id = 211,
				LocalId = Guid.NewGuid(),
				Name = "technicalRequirementIOG",
				Content = "",
				Options = new List<SelectListItem>
					{
						new SelectListItem { Text = "Yes", Value = "1" },
						new SelectListItem { Text = "No", Value = "" },
					},
				Type = GenerateLetterInputControlType.RADIO.ToString(),
			});
			Controls.Add(new GenerateLetterInputControl()
			{
				Label = "dataModelIOG",
				Description = "Include Data Model Requirements for Internet Operated Games?",
				Id = 212,
				LocalId = Guid.NewGuid(),
				Name = "dataModelIOG",
				Content = "",
				Options = new List<SelectListItem>
					{
						new SelectListItem { Text = "Yes", Value = "1" },
						new SelectListItem { Text = "No", Value = "" },
					},
				Type = GenerateLetterInputControlType.RADIO.ToString(),
			});
			Controls.Add(new GenerateLetterInputControl()
			{
				Label = "accordNo4",
				Description = "Include Accord No. 04?",
				Id = 213,
				LocalId = Guid.NewGuid(),
				Name = "accordNo4",
				Content = "",
				Options = new List<SelectListItem>
					{
						new SelectListItem { Text = "Yes", Value = "1" },
						new SelectListItem { Text = "No", Value = "" },
					},
				Type = GenerateLetterInputControlType.RADIO.ToString(),
			});
			Controls.Add(new GenerateLetterInputControl()
			{
				Label = "exclusionOfTechnicalStandards",
				Description = "Include note and list of excluded technical standards?",
				Id = 214,
				LocalId = Guid.NewGuid(),
				Name = "exclusionOfTechnicalStandards",
				Content = "",
				Options = new List<SelectListItem>
					{
						new SelectListItem { Text = "Yes", Value = "1" },
						new SelectListItem { Text = "No", Value = "" },
					},
				Type = GenerateLetterInputControlType.RADIO.ToString(),
			});
			Controls.Add(new GenerateLetterInputControl()
			{
				Label = "excludedTechnicalStandardsTable",
				Description = "A table containing the technical standards and reason for exclusion.",
				Id = 215,
				ParentId = 214,
				IsBusinessLogic = true,
				LocalId = Guid.NewGuid(),
				Name = "Excluded Technical Standards",
				ConditionalExpression = "result",
				Content = "",
				Type = GenerateLetterInputControlType.TABLE.ToString(),
				TableRows = new List<List<GenerateLetterInputControl>>()
				{
					new List<GenerateLetterInputControl>()
					{
						new GenerateLetterInputControl()
						{
							Id = 216,
							LocalId = System.Guid.NewGuid(),
							Label = "excludedTechnicalStandardLabel",
							Description = "Excluded Technical standard label.",
							Content = "Excluded Technical Standard:",
							Type = GenerateLetterInputControlType.STATIC.ToString(),
						},
						new GenerateLetterInputControl()
						{
							Id = 217,
							LocalId = System.Guid.NewGuid(),
							Label = "technicalStandardSection",
							ColumnHeader = "Technical Standard Section(s)",
							Type = GenerateLetterInputControlType.PLAINTEXT.ToString()
						},
						new GenerateLetterInputControl()
						{
							Id = 218,
							ColumnHeader = "Reason for Exclusion",
							LocalId = System.Guid.NewGuid(),
							Label = "reasonForExclusion",
							Type = GenerateLetterInputControlType.PLAINTEXT.ToString()
						}
					}
				}
			});
			Controls.Add(new GenerateLetterInputControl()
			{
				Label = "softwareAndSystemVersionControl",
				Description = "Include Software and System Version Control?",
				Id = 300,
				LocalId = Guid.NewGuid(),
				Name = "softwareAndSystemVersionControl",
				Content = "",
				Options = new List<SelectListItem>
					{
						new SelectListItem { Text = "Yes", Value = "1" },
						new SelectListItem { Text = "No", Value = "" },
					},
				Type = GenerateLetterInputControlType.RADIO.ToString(),
			});
			Controls.Add(new GenerateLetterInputControl()
			{
				Label = "submittedDocumentationReview",
				Description = "Include Submitted Documentation Review?",
				Id = 301,
				LocalId = Guid.NewGuid(),
				Name = "submittedDocumentationReview",
				Content = "",
				Options = new List<SelectListItem>
					{
						new SelectListItem { Text = "Yes", Value = "1" },
						new SelectListItem { Text = "No", Value = "" },
					},
				Type = GenerateLetterInputControlType.RADIO.ToString(),
			});
			Controls.Add(new GenerateLetterInputControl()
			{
				Label = "artworkAndGameRulesReview",
				Description = "Include Artwork and Game Rules Review?",
				Id = 302,
				LocalId = Guid.NewGuid(),
				Name = "artworkAndGameRulesReview",
				Content = "",
				Options = new List<SelectListItem>
					{
						new SelectListItem { Text = "Yes", Value = "1" },
						new SelectListItem { Text = "No", Value = "" },
					},
				Type = GenerateLetterInputControlType.RADIO.ToString(),
			});
			Controls.Add(new GenerateLetterInputControl()
			{
				Label = "sourceCodeReview",
				Description = "Include Source Code Review?",
				Id = 303,
				LocalId = Guid.NewGuid(),
				Name = "sourceCodeReview",
				Content = "",
				Options = new List<SelectListItem>
					{
						new SelectListItem { Text = "Yes", Value = "1" },
						new SelectListItem { Text = "No", Value = "" },
					},
				Type = GenerateLetterInputControlType.RADIO.ToString(),
			});
			Controls.Add(new GenerateLetterInputControl()
			{
				Label = "gameAccounting",
				Description = "Include Game Accounting?",
				Id = 304,
				LocalId = Guid.NewGuid(),
				Name = "gameAccounting",
				Content = "",
				Options = new List<SelectListItem>
					{
						new SelectListItem { Text = "Yes", Value = "1" },
						new SelectListItem { Text = "No", Value = "" },
					},
				Type = GenerateLetterInputControlType.RADIO.ToString(),
			});
			Controls.Add(new GenerateLetterInputControl()
			{
				Label = "gameMathematics",
				Description = "Include Game Mathematics?",
				Id = 305,
				LocalId = Guid.NewGuid(),
				Name = "gameMathematics",
				Content = "",
				Options = new List<SelectListItem>
					{
						new SelectListItem { Text = "Yes", Value = "1" },
						new SelectListItem { Text = "No", Value = "" },
					},
				Type = GenerateLetterInputControlType.RADIO.ToString(),
			});
			Controls.Add(new GenerateLetterInputControl()
			{
				Label = "emulationTesting",
				Description = "Include Emulation Testing?",
				Id = 306,
				LocalId = Guid.NewGuid(),
				Name = "emulationTesting",
				Content = "",
				Options = new List<SelectListItem>
					{
						new SelectListItem { Text = "Yes", Value = "1" },
						new SelectListItem { Text = "No", Value = "" },
					},
				Type = GenerateLetterInputControlType.RADIO.ToString(),
			});
			Controls.Add(new GenerateLetterInputControl()
			{
				Label = "deploymentVerification",
				Description = "Include Deployment Verification?",
				Id = 307,
				LocalId = Guid.NewGuid(),
				Name = "deploymentVerification",
				Content = "",
				Options = new List<SelectListItem>
					{
						new SelectListItem { Text = "Yes", Value = "1" },
						new SelectListItem { Text = "No", Value = "" },
					},
				Type = GenerateLetterInputControlType.RADIO.ToString(),
			});
			Controls.Add(new GenerateLetterInputControl()
			{
				Label = "jurisdictionalRequirementsVerification",
				Description = "Include Jurisdictional Requirements Verification?",
				Id = 350,
				LocalId = Guid.NewGuid(),
				Name = "jurisdictionalRequirementsVerification",
				Content = "1",
				Options = new List<SelectListItem>
					{
						new SelectListItem { Text = "Yes", Value = "1" },
						new SelectListItem { Text = "No", Value = "" },
					},
				Type = GenerateLetterInputControlType.RADIO.ToString(),
			});
		}

		public List<GenerateLetterInputControl> GetRelevantControls(List<int> controlIds)
		{
			return Controls.Where(x => controlIds.Contains(x.Id)).ToList();
		}
	}
}