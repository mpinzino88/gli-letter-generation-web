﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Submissions.Web.Areas.Documents.Models.Generate.Helper
{
	public class CertificationIssuer
	{
		public CertificationIssuer()
		{
			PresidesOver = new List<string>();
			Signature = new CertificationIssuerSignature();
		}

		public int UserId { get; set; }
		public string FirstName { get; set; }
		public string LastName { get; set; }
		public string MiddleInital { get; set; }
		public string FullName
		{
			get
			{
				return FirstName + " " + (string.IsNullOrWhiteSpace(MiddleInital) ? string.Empty : MiddleInital + ". ") + LastName;
			}
			private set
			{
				FullName = value;
			}
		}
		public string Title { get; set; }
		public CertificationIssuerSignature Signature { get; set; }
		public List<string> PresidesOver { get; set; }
	}

	public class CertificationIssuerSignature
	{
		public string FileExtension { get; set; }
		public string Base64Image { get; set; }
		public string DataURI
		{
			get
			{
				return @"data:image/" + FileExtension + @";base64," + Base64Image;
			}
			private set
			{
				DataURI = value;
			}
		}
	}
}