﻿using System.Collections.Generic;

namespace Submissions.Web.Areas.Documents.Models.Generate.Helper
{
	public class Contact
	{
		public List<string> JurisdictionIds { get; set; }
		public List<string> Address { get; set; }
		public string Name { get; set; }
		public List<string> LetterTypes { get; set; }
	}
}