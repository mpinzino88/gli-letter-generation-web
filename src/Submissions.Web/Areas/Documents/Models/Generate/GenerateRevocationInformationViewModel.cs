﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Submissions.Web.Areas.Documents.Models.Generate
{
	public class GenerateRevocationInformationViewModel
	{
		public string SubmissionID { get; set; }
		public string SubmissionIdNumber { get; set; }
		public string SubmissionGameName { get; set; }
		public string SubmissionVersion { get; set; }
		public string SubmissionDateCode { get; set; }
		public string JurisdictionID { get; set; }
		public string RevocationWording { get; set; }
		public decimal? RevocationPeriod { get; set; }
		public List<GenerateSubmissionItemsBeingRevoked> SubmissionItemsBeingRevoked { get; set; }
	}

	public class GenerateSubmissionItemsBeingRevoked
	{
		public int Id { get; set; }
		public string IdNumber { get; set; }
		public string GameName { get; set; }
		public string Version { get; set; }
		public string DateCode { get; set; }
	}
}