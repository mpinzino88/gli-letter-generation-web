﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Submissions.Web.Areas.Documents.Models.Generate
{
	public class GenerateJIRAInformationViewModel
	{
		public GenerateJIRAInformationViewModel()
		{
			RevocationInformations = new List<GenerateRevocationInformationViewModel>();
			DisclosureInformations = new List<GenerateDisclosureInformationViewModel>();
		}

		public List<GenerateRevocationInformationViewModel> RevocationInformations { get; set; }
		public List<GenerateDisclosureInformationViewModel> DisclosureInformations { get; set; }
	}
}