﻿using Submissions.Web.Areas.Documents.Models.Generate.Helper;
using System;
using System.Collections.Generic;
namespace Submissions.Web.Areas.Documents.Models.Generate
{
	public class GenerateJurisdictionModel
	{
		public GenerateJurisdictionModel()
		{
			AvailTechnicalStandards = new List<TechnicalStandard>();
			CertificationIssuer = new CertificationIssuer();
			EvalLabs = new List<string>();
			EvalLabLetterCodes = new List<string>();
			ControlInstruments = new List<string>();
			ControlInstruments_SecLang = new List<string>();
		}
		public string Name { get; set; }
		public string LetterName { get; set; }

		public int JurisdictionId { get; set; }
		public string JurisdictionalCertificationLab { get; set; }
		public string TechnicalStandards { get; set; }
		public List<TechnicalStandard> AvailTechnicalStandards { get; set; }
		public string Recipient { get; set; }
		public DateTime? RequestDate { get; set; }
		public string SoftwareSupplier { get; set; }
		public string SubmittingParty { get; set; }
		public CertificationIssuer CertificationIssuer { get; set; }

		public DateTime? EvalPeriodFrom { get; set; }
		public DateTime? EvalPeriodTo { get; set; }
		public List<string> EvalLabs { get; set; }
		public List<string> EvalLabLetterCodes { get; set; }
		public string ReportType { get; set; }
		public string TestingResult { get; set; }
		/// <summary>
		/// Called "Reference Number" in legacy and on the letter
		/// </summary>
		public string LetterNumber { get; set; }
		/// <summary>
		/// Called "Network Number" in legacy and on the letter
		/// </summary>
		public string ClientNumber { get; set; }
		/// <summary>
		/// Certification Numbers from the tbl_CertJobNumber table
		/// </summary>
		public List<string> CertNumbers { get; set; }
		/// <summary>
		/// Used to store the date the letter is issued.
		/// </summary>
		public DateTime IssueDate { get; set; }

		public string ManufacturerDisplay { get; set; }

		public string CountryOfManufOrigin { get; set; }
		public string CountryOfManufOrigin_SecLang { get; set; }

		public List<string> ControlInstruments { get; set; }
		public List<string> ControlInstruments_SecLang { get; set; }
	}

	public class TechnicalStandard
	{
		public string Name { get; set; }
		public int JurisdictionId { get; set; }
		public bool Selected { get; set; }
	}

	public class AvailableJurisdiction
	{
		public string JurisdictionId { get; set; }
		public int JurisdictionFixId { get; set; }
		public string JurisdictionName { get; set; }
		public DateTime? RequestDate { get; set; }
		public string JurisdictionalCertificationLab { get; set; }
	}

	public class JurisdictionalDefaultValue
	{
		public int JurisdictionId { get; set; }
		public int ReportTypeId { get; set; }
		public int TestingResultId { get; set; }
		public string LetterJurisdiction { get; set; }
	}

	public class JurisdictionalReportType
	{
		public int Id { get; set; }
		public string Type { get; set; }
	}

	public class JurisdictionalTestResult
	{
		public int Id { get; set; }
		public string Result { get; set; }
	}

	public class JurisdictionalDefaultValueResult
	{
		public string ReportType { get; set; }
		public string TestingResult { get; set; }
		public string LetterJurisdiction { get; set; }
	}
}