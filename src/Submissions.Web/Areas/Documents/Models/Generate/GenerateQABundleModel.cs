﻿using Submissions.Common;
using Submissions.Core.Models.LetterContent;
using System.Collections.Generic;

namespace Submissions.Web.Areas.Documents.Models.Generate
{
	public class GenerateQABundleModel
	{
		public GenerateQABundleModel()
		{
			JurisdictionalData = new List<GenerateJurisdictionalDataModel>();
		}
		public int BundleId { get; set; }
		public int ProjectId { get; set; }
		public string ProjectName { get; set; }
		public bool ProjectIsTransfer { get; set; }
		public string ProjectFileNumber { get; set; }
		public string ProjectENStatus { get; set; }
		public string ProjectHolder { get; set; }
		public string QAStatus { get; set; }
		public string ProjectEvalLab { get; set; }
		public bool ContainsRJWD { get; set; }
		public List<GenerateJurisdictionalDataModel> JurisdictionalData { get; set; }
		public FileNumber FileNumber
		{
			get { return new FileNumber(ProjectName); }
		}
		public ManufacturerDTO Manufacturer { get; internal set; }
	}
}