﻿using System;

namespace Submissions.Web.Areas.Documents.Models.Generate
{
	public class GenerateJurisdictionalDataModel
	{
		public int Primarykey { get; set; }
		public string JurisdictionId { get; set; }
		public int JurisdictionFixId { get; set; }
		public string JurisdictionName { get; set; }
		public DateTime? RequestDate { get; set; }
		public string CertificationLab { get; set; }
	}
}