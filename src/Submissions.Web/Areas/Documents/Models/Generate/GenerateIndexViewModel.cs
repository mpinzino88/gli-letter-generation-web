using GLI.EF.LetterContent;
using Kendo.Mvc.Extensions;
using Submissions.Common;
using Submissions.Core.Models;
using Submissions.Core.Models.DTO;
using Submissions.Core.Models.LetterContent;
using Submissions.Core.Models.LetterGen;
using Submissions.Web.Areas.Documents.Models.Coversheet;
using Submissions.Web.Areas.Documents.Models.Generate.Helper;
using Submissions.Web.Areas.Evolution.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using LetterGenMode = Submissions.Common.LetterGenMode;

namespace Submissions.Web.Areas.Documents.Models.Generate
{
	public class GenerateIndexViewModel
	{
		public GenerateIndexViewModel()
		{
			// Letter Objects
			BlankMemo = new GenerateLetterMemo();
			Letter = new GenerateLetter();
			// Sources
			ClauseReports = new List<ClauseReportViewModel>();
			Components = new List<SubmissionDTO>();
			Paytables = new List<MathCoversheetDTO>();
			// Model Properties
			Bundle = new GenerateQABundleModel();
			Bundles = new List<SelectListItem>();
			CertificationIssuers = new List<CertificationIssuer>();
			EvolutionInfo = new GenerateLetterEvolutionInfo();
			JurisdictionData = new List<GenerateJurisdictionModel>();
			LetterGenerationSourceTabs = new List<LetterGenerationSourceTab>();
			LetterJurisdictionOptions = new List<SelectListItem>();
			ProjectInfo = new GenerateLetterProjectInfo();
			RevocationInformations = new List<GenerateRevocationInformationViewModel>();
			JIRAInformation = new GenerateJIRAInformationViewModel();
			GamingGuidelines = new List<GamingGuideline>();
			SpecializedJurisdictionClauseReports = new List<GenerateSpecializedJurisdictionClauseReportViewModel>();
			PermissionSet = new LetterGenerationPermissionSet();
			OCUITComponents = new List<GenerateOCUITComponentViewModel>();
			Modifications = new List<Modification>();
		}

		public GenerateIndexViewModel(LetterGenMode mode)
		{
			CurrentMode = mode;
			LetterGenerationSourceTabs = new List<LetterGenerationSourceTab>();
			this.Bundles = new List<SelectListItem>();
			Components = new List<SubmissionDTO>();

			ClauseReports = new List<ClauseReportViewModel>();
			Letter = new GenerateLetter();

			SetMemoIds(Letter);
			SetLanguages(Letter);
			BlankMemo = BuildBlankMemo();
		}

		// Objects that represent the Letter or parts of the letter
		#region Letter Objects
		public GenerateLetterMemo BlankMemo { get; set; }
		public GenerateLetter Letter { get; set; }

		#endregion

		// Sources of data for use in the Letter
		#region Sources
		public List<ClauseReportViewModel> ClauseReports { get; set; }
		public List<SubmissionDTO> Components { get; set; }
		public List<MathCoversheetDTO> Paytables { get; set; }
		public List<GenerateSpecializedJurisdictionClauseReportViewModel> SpecializedJurisdictionClauseReports { get; set; }
		public List<GenerateOCUITComponentViewModel> OCUITComponents { get; set; }
		#endregion

		// Additional information relevant to the Letter
		#region Model Properties
		public GenerateQABundleModel Bundle { get; set; }
		public List<SelectListItem> Bundles { get; set; }
		public List<CertificationIssuer> CertificationIssuers { get; set; }
		public LetterGenMode CurrentMode { get; set; }
		public GenerateLetterEvolutionInfo EvolutionInfo { get; set; }
		public List<GenerateJurisdictionModel> JurisdictionData { get; set; }
		public List<LetterGenerationSourceTab> LetterGenerationSourceTabs { get; set; }
		public List<SelectListItem> LetterJurisdictionOptions { get; set; }
		private static int MemoId = 2000;
		public GenerateLetterProjectInfo ProjectInfo { get; set; }
		// This needs to be reworked or removed, I dont want this hanging around forever.
		public bool TestLetter { get; set; }
		public List<GenerateRevocationInformationViewModel> RevocationInformations { get; set; }
		public GenerateJIRAInformationViewModel JIRAInformation { get; set; }
		public List<GamingGuideline> GamingGuidelines { get; set; }
		public LetterGenerationPermissionSet PermissionSet { get; set; }
		public List<Modification> Modifications { get; set; }
		#endregion

		// I want to move this to a permissions object.
		#region Permissions 
		public bool EditLetterGenSubSig { get; set; }
		public bool EditLetterGeneration { get; set; }
		#endregion

		/// <summary>
		/// Adds a collection of ClauseReportModel objects to this model, adhering to the structure defined by the 
		/// Evolution Vue components
		/// </summary>
		/// <param name="clauseReportModels">The clauses for this bundle</param>
		public void AddCCDocuments(List<ClauseReportModel> clauseReportModels)
		{
			foreach (var clauseGroup in clauseReportModels.GroupBy(cr => cr.JurisdictionId))
			{
				ClauseReports.Add(new ClauseReportViewModel()
				{
					ClauseReports = clauseGroup.ToList(),
					PassCount = clauseGroup.DistinctBy(c => c.ClauseID).Count(x => x.Answer == "PASS"),
					FailCount = clauseGroup.DistinctBy(c => c.ClauseID).Count(x => x.Answer == "FAIL"),
					NACount = clauseGroup.DistinctBy(c => c.ClauseID).Count(x => x.Answer == "NA"),
					NAStarCount = clauseGroup.DistinctBy(c => c.ClauseID).Count(x => x.Answer == "NA*"),
					JurisdictionId = clauseGroup.Key
				});
			}
		}

		public void AddPaytables(List<MathCoversheetDTO> mathCoversheets)
		{
			Paytables.AddRange(mathCoversheets);
		}







		#region Building Test Data








		public void SetDefaultPaging()
		{
			var jurIds = ProjectInfo.JurisdictionIds.ToList();
			Letter.Sections.SelectMany(sec => sec.Memos).Where(memo => memo.Paging == null || memo.Paging.Count == 0).ToList().ForEach((memo) =>
			{
				SetMemoPaging(ref memo, jurIds, false, 1);
			});

			Letter.Footers.ToList().ForEach(memo =>
			{
				//I'm not sure why this is needed, but Letter Preview breaks without this. That should be corrected.
				if (memo.ApplicableJurisdictions == null || memo.ApplicableJurisdictions.Count == 0)
					memo.ApplicableJurisdictions = jurIds;
				memo.Paging = new List<GenerateLetterPaging>();
				foreach (var locJurId in memo.ApplicableJurisdictions)
				{
					memo.Paging.Add(new GenerateLetterPaging
					{
						JurisdictionId = locJurId,
						LocalId = memo.LocalId,
						Page = 1,
						IsLandscape = false
					});
				}
			});

			//I don't know if Headers needs this, but let's be consistent
			Letter.Headers.Where(header => header.Paging == null || header.Paging.Count == 0).ToList().ForEach(memo =>
			{
				SetMemoPaging(ref memo, jurIds, false, 1);
			});

		}

		public void SetMemoPaging(ref GenerateLetterMemo memo, List<int> bundleJurisdictionIds, bool isLandscape, int page)
		{
			var locJurIds = new List<int>();
			if (memo.ApplicableJurisdictions == null || memo.ApplicableJurisdictions.Count == 0)
				locJurIds = bundleJurisdictionIds;
			else
				locJurIds = memo.ApplicableJurisdictions.ToList();

			memo.Paging = new List<GenerateLetterPaging>();
			foreach (var locJurId in locJurIds)
			{

				memo.Paging.Add(new GenerateLetterPaging
				{
					JurisdictionId = locJurId,
					LocalId = memo.LocalId,
					Page = page,
					IsLandscape = isLandscape
				});
			}
		}

		private void SetMemoIds(GenerateLetter letter)
		{

			letter.Sections.SelectMany(sec => sec.Memos).ToList().ForEach((memo) =>
			{
				var locId = Guid.NewGuid();
				if (memo.Id == 0)
				{
					memo.Id = MemoId++;
				}
				memo.LocalId = locId;
				memo.OrderId = MemoId++;
				if (memo.LetterContentControls != null)
				{
					foreach (var control in memo.LetterContentControls)
					{
						control.LocalId = Guid.NewGuid();
					}
				}
			});
		}
		private void SetLanguages(GenerateLetter letter)
		{
			letter.Sections.SelectMany(sec => sec.Memos).ToList().ForEach((memo) =>
			{
				memo.Language = "English";
			});
		}
		private GenerateLetterMemo BuildBlankMemo()
		{
			return new GenerateLetterMemo()
			{
				Custom = true,
				DefaultInclude = true,
				Include = true,
				Removable = true,
				Name = "Custom",
				Description = "",
				Language = "English",
				LetterContent = "<div v-html=\"custom\"></div>",
				Paging = new List<GenerateLetterPaging>(),
				LetterContentControls = new List<GenerateLetterInputControl>()
				{
					new GenerateLetterInputControl()
					{
						Type = GenerateLetterInputControlType.HTML.ToString(),
						Description = "",
						Label = "custom",
						Name = "Custom",
						Content = "<div>This is a custom note.</div>",
						Splittable = true
					}
				}

			};
		}


		public void BuildLetterContents(IList<Core.Models.LetterContent.LetterContentDTO> letterContents)
		{
			var jurIds = ProjectInfo.JurisdictionIds.ToList();
			var layoutTracker = false; // Portrait
			var pageTracker = 1; // initial page
			foreach (var letterContent in letterContents)
			{
				foreach (var memo in letterContent.Memos)
				{
					var newMemo = new GenerateLetterMemo()
					{
						Id = MemoId++,
						LocalId = Guid.NewGuid(),
						Name = letterContent.Name,
						Description = letterContent.Description,
						LetterContent = memo.Content,
						Include = true,
						DefaultInclude = true,
						Removable = true,
						Custom = false,
						LetterContentControls = new List<GenerateLetterInputControl>(),
						//We only want to add applicableJurisdictions if they're in this bundle
						//the current EF query adds all jurisdictions to the LetterContentDTO
						ApplicableJurisdictions = letterContent.Jurisdictions.Select(x => x.JurisdictionId).Intersect(jurIds).ToList(),
						Paging = null
					};
					//if this memo is applicable to all jurisdictions in this bundle, empty the list
					//we have lots of logic that assumes this
					// (pa)in order to differentiate with manuf specific and manuf/jur specific we may need this, as we are grabbing all headers and we may want to default to one that specifically set for jur
					//if (newMemo.ApplicableJurisdictions.Count == jurIds.Count)
					//{
					//newMemo.ApplicableJurisdictions = new List<int>();
					//}


					var tokens = memo.DynamicExpressions.ToDictionary(dynamicExpression => dynamicExpression.LocationId, dynamicExpression => dynamicExpression.Expression);
					foreach (var token in tokens)
					{
						var pattern = @"<span [^>]*id=""token-" + token.Key + @"""(.*?)</span>";
						newMemo.LetterContent = newMemo.LetterContent.StripRegex(pattern, @"{{ " + token.Value + @" }}");
					}

					if (letterContent.SpecialFlags.HasFlag(SpecialFlags.Footer))
					{
						if (CurrentMode == LetterGenMode.EVOLUTION)
						{
							if (newMemo.Name.Replace(" ", "").ToLower() == "footerevolutionmode".Replace(" ", "").ToLower())
								Letter.Footers.Add(newMemo);
						}
						else
						{
							Letter.Footers.Add(newMemo);
						}

					}
					else if (letterContent.SpecialFlags.HasFlag(SpecialFlags.Header))
					{
						if (CurrentMode == LetterGenMode.EVOLUTION)
						{
							if (newMemo.Name.Replace(" ", "").ToLower() == "headerevolutionmode".Replace(" ", "").ToLower())
								Letter.Headers.Add(newMemo);
						}
						else
						{
							Letter.Headers.Add(newMemo);
						}
					}
					else
					{

					}
					var isLandscape = letterContent.SpecialFlags.HasFlag(SpecialFlags.Landscape);
					if (isLandscape != layoutTracker)
					{
						layoutTracker = isLandscape;
						pageTracker++;
					}
					var locJurIds = jurIds;
					if (newMemo.ApplicableJurisdictions.Count > 0)
						locJurIds = newMemo.ApplicableJurisdictions.ToList();
					SetMemoPaging(ref newMemo, locJurIds, isLandscape, pageTracker);
				}
				//var isLandscape = (letterContent.SpecialFlags & GLI.EF.LetterContent.SpecialFlags.Landscape) != 0;
				//SetDefaultPagingNew(ref newMemo, jurIds, isLandscape);
			}

		}

		#endregion
	}
}