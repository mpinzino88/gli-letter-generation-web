﻿using System.Collections.Generic;

namespace Submissions.Web.Areas.Documents.Models.Generate
{
	public class GenerateOCUITComponentViewModel
	{
		public GenerateOCUITComponentViewModel()
		{
			JurisdictionIds = new List<string>();
		}

		public int Id { get; set; }
		public int SubmissionId { get; set; }
		public string Description { get; set; }
		public string NVLabNumber { get; set; }
		public string FileNumber { get; set; }
		public string IdNumber { get; set; }
		public string PartNumber { get; set; }
		public string Version { get; set; }
		public bool Continuity { get; set; }
		public bool JurisdictionChip { get; set; }
		public bool TestedInParallel { get; set; }
		public IList<string> JurisdictionIds { get; set; }
	}
}