﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Submissions.Web.Areas.Documents.Models.Generate
{
	public class GenerateDisclosureInformationViewModel
	{
		public string SubmissionID { get; set; }
		public string JurisdictionID { get; set; }
		public string DisclosureWording { get; set; }
	}
}