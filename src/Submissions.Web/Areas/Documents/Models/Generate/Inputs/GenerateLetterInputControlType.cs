﻿namespace Submissions.Web.Areas.Documents.Models.Generate
{
	public enum GenerateLetterInputControlType
	{
		CHECKBOX,
		DATEPICKER,
		DROPDOWN,
		HTML,
		LOOKUP,
		LOOKUPMULTIPLE,
		MULTISELECT,
		PLAINTEXT,
		RADIO,
		STATIC,
		TABLE,
		TEXTAREA,
		TECHNICALSTANDARD,
		COMPONENTSEARCH
	}
}