﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace Submissions.Web.Areas.Documents.Models.Generate
{
	public class GenerateLetterInputControl
	{
		public GenerateLetterInputControl()
		{
			ApplicableJurisdictions = new List<int>();
			SplitGroupApplicable = new List<int>();
			TableRows = new List<List<GenerateLetterInputControl>>();
			Options = new List<SelectListItem>();
			SourceTargetAnswers = new List<SourceTargetAnswer>();
		}
		/// <summary>
		/// Intended to be a database generated unique Id
		/// </summary>
		public int Id { get; set; }
		/// <summary>
		/// A localized Id, not used, future proofing in case controls are duplicated, perhaps due to jurisdiction splitting out
		/// </summary>
		public System.Guid LocalId { get; set; }
		/// <summary>
		/// The string used to reference the value returned by this control. Writing {{ productType }} in the note will look for the GenerateLetterInputControl object where 
		/// Label == "productType" and return the value found in the Content property of that GenerateLetterInputControl object
		/// </summary>
		public string Label { get; set; }
		/// <summary>
		/// Stores the actual value of this input control, usually to 
		/// </summary>
		public string Content { get; set; }
		/// <summary>
		/// Stores the actual value of this input control in secondary lanaguge, usually to 
		/// </summary>
		public string SecondaryContent { get; set; }
		/// <summary>
		/// Stores the Language of secondary lanaguge 
		/// </summary>
		public int? SecondaryLanguage { get; set; }
		/// <summary>
		/// Set from the GenerateLetterInputControlType enum, but javascript prefers strings, so this is strongly typed as a string. 
		/// </summary>
		public string Type { get; set; }
		/// <summary>
		/// Used for input controls that should be added to "Source" material, such as clause reports. Should hold a string like "ClauseReports" or perhaps "Paytables"
		/// </summary>
		public string SourceId { get; set; }
		/// <summary>
		/// For complex Sources with 1-to-many relations, this holds the name of the collection, e.g. SourceId = "ClauseReports" and SourceProperty "Clauses" will repeat this
		/// control for every "Clause" item in the entire Clause Report Source
		/// </summary>
		public string SourceProperty { get; set; }

		/// <summary>
		/// Used to replace the usage of SourceProperty to identify hte modifications memos. This will likely exist as a 
		/// 'special flag' option.
		/// </summary>
		public bool IsModification { get; set; }
		/// <summary>
		/// Used to replace the usage of SourceProperty to identify hte game descriptions memos. This will likely exist as a 
		/// 'special flag' option.
		/// </summary>
		public bool IsGameDescription { get; set; }
		/// <summary>
		/// Just the user-intuitive name for this control, likely related to the Note it belongs to
		/// </summary>
		public string Name { get; set; }
		/// <summary>
		/// Any guidance on what this control is actually for
		/// </summary>
		public string Description { get; set; }
		/// <summary>
		/// Used with the LOOKUP GenerateLetterInputControlType, tells control which LookupAjax to query against
		/// </summary>
		public string ReferenceLookup { get; set; }
		/// <summary>
		/// Generic object to be used as a filter for the lookupajax as needed
		/// </summary>
		public object LookupFilter { get; set; }
		/// <summary>
		/// If Type == DROPDOWN or MULTISELECT, these options will be listed in the drop-down
		/// </summary>
		public IList<SelectListItem> Options { get; set; }
		public bool IsOptionValueNumber { get; set; }
		/// <summary>
		/// DROPDOWN stores the answer in Content, but MULTISELECT stores selected answers in this IList
		/// </summary>
		public IList<string> SelectedOptions { get; set; }
		/// <summary>
		/// Used to concatenate all SelectedOptions into the letter, e.g. OptionDelemeter == ", " would result in a comma-delimited list of the SelectedOptions
		/// </summary>
		public string OptionDelimiter { get; set; }

		/// <summary>
		/// Used for a table of multiple controls
		/// </summary>
		public List<List<GenerateLetterInputControl>> TableRows { get; set; }
		/// <summary>
		/// Used for splitting out jurisdictional answers
		/// </summary>
		public IList<int> ApplicableJurisdictions { get; set; }
		/// <summary>
		/// If this is a control in a Table, this will be the header label
		/// </summary>
		public string ColumnHeader { get; set; }
		/// <summary>
		/// If true, marks this value as being potentially large and may need to be split up for paging
		/// </summary>
		public bool Splittable { get; set; }
		/// <summary>
		/// Used to find Default question and not split ones
		/// </summary>
		public bool IsSplitFromDefault { get; set; } // this needs it in order to find the default question 
		/// <summary>
		/// Used to find which split group it belongs specially when a control applies to multiple memo and applicable Jurs are different
		/// </summary>
		public List<int> SplitGroupApplicable { get; set; } // this needs when question belonging to multiple memos is split to maintain the grouping
		/// </summary>
		/// Used to distinguish controls for business logic
		/// </summary>
		public bool IsBusinessLogic { get; set; }
		/// <summary>
		/// For nested controls
		/// </summary>
		public int ParentId { get; set; }
		/// <summary>
		/// Expression to determine if this question should be shown
		/// </summary>
		public string ConditionalExpression { get; set; }
		/// <summary>
		/// This is true for the dynamic expressions that dictate filters on the sources
		/// </summary>
		public bool RepeatableContent { get; set; }
		public string SourceFilter { get; set; }
		/// <summary>
		/// Clones this control and gives it a new Local Id
		/// </summary>
		/// <returns>A copy of this Input Control with a new Local Id</returns>
		internal GenerateLetterInputControl CloneControl()
		{
			var clone = this.MemberwiseClone() as GenerateLetterInputControl;
			clone.LocalId = System.Guid.NewGuid();
			return clone;
		}
		public Guid MemoLocalId { get; set; }
		/// <summary>
		/// Id of the answer this control is associated with
		/// </summary>
		public int? AnswerId { get; set; }

		public List<SourceTargetAnswer> SourceTargetAnswers { get; set; }
		/// <summary>
		/// to show Saved Answer info
		/// </summary>
		public DateTime? SavedOn { get; set; }
		public String SavedBy { get; set; }
		public bool IsRequired { get; set; }
	}

	public class SourceTargetAnswer
	{
		public SourceTargetAnswer()
		{
			ApplicableJurisdictions = new List<int>();
		}

		public string SourceTargetId { get; set; }
		public string Source { get; set; }
		public string Answer { get; set; }
		public List<int> ApplicableJurisdictions { get; set; }
		public bool IsSplitFromDefault { get; set; }
	}
}