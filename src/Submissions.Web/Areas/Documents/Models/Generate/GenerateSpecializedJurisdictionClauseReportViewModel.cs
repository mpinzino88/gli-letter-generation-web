﻿using System.Collections.Generic;

namespace Submissions.Web.Areas.Documents.Models.Generate
{
	public class GenerateSpecializedJurisdictionClauseReportViewModel
	{
		public GenerateSpecializedJurisdictionClauseReportViewModel()
		{
			TestCases = new List<GenerateSpeacalizedJurisdcictionTestCaseViewModel>();
			EvaluatedClauses = new List<GenerateSpeacalizedJurisdcictionEvaluatedViewModel>();
		}

		public int JurisdictionID { get; set; }
		public string JurisdictionName { get; set; }
		public List<GenerateSpeacalizedJurisdcictionTestCaseViewModel> TestCases { get; set; }
		public List<GenerateSpeacalizedJurisdcictionEvaluatedViewModel> EvaluatedClauses { get; set; }
	}

	public class GenerateSpeacalizedJurisdcictionTestCaseViewModel
	{
		public string Name { get; set; }
		public string Version { get; set; }
	}

	public class GenerateSpeacalizedJurisdcictionEvaluatedViewModel
	{
		public string Name { get; set; }
		public string Result { get; set; }
		public string NormalizedID { get; set; }
		public string ErrorText { get; set; }
	}
}