﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Submissions.Web.Areas.Documents.Models.Generate
{
	public class SplitSourceItem
	{
		/// <summary>
		/// The name of the property that is split. 
		/// </summary>
		public string Property { get; set; }
		/// <summary>
		/// The index of the start. A source item that is split into 2 would have 2 SplitSourceItem objects, 1 with start: 0, end: midpoint, and the other 
		/// with start: midpoint, end: 0.
		/// </summary>
		public int Start { get; set; }
		/// <summary>
		/// The index of the end. A source item that is split into 2 would have 2 SplitSourceItem objects, 1 with start: 0, end: midpoint, and the other 
		/// with start: midpoint, end: 0.
		/// </summary>
		public int End { get; set; }
	}
}