﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Submissions.Web.Areas.Documents.Models.Generate
{
	public class SourceItemTarget
	{
		public SourceItemTarget()
		{
			SplitSourceItems = new List<SplitSourceItem>();
			LetterContentControls = new List<GenerateLetterInputControl>();
		}
		/// <summary>
		/// The Id of the source item; Keytbl for components, paytable.Id for paytables
		/// </summary>
		public string Id { get; set; }
		/// <summary>
		/// If the content of one source item is split across pages, it's defined here
		/// </summary>
		public IList<SplitSourceItem> SplitSourceItems { get; set; }
		/// <summary>
		/// The controls for this sourceItem
		/// </summary>
		public IList<GenerateLetterInputControl> LetterContentControls { get; set; }

	}
}