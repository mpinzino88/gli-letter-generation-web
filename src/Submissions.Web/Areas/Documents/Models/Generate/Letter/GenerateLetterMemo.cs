﻿using System.Collections.Generic;

namespace Submissions.Web.Areas.Documents.Models.Generate
{
	public class GenerateLetterMemo
	{
		public GenerateLetterMemo()
		{
			ApplicableJurisdictions = new List<int>();
			ApplicableManufacturers = new List<string>();
			LetterContentControls = new List<GenerateLetterInputControl>();
			RepeatableContentControls = new List<GenerateLetterInputControl>();
		}
		public IList<int> ApplicableJurisdictions { get; set; }
		public IList<string> ApplicableManufacturers { get; set; }
		public int Id { get; set; }
		public System.Guid LocalId { get; set; }
		public decimal OrderId { get; set; } //this was changed to decimal to make custom memos appaear at correct place, so it will do memo.orderid + 0.1 so it will appear right after that memo
		public string Name { get; set; }
		public string Description { get; set; }
		/// <summary>
		/// If true, memo will be rendered in Letter Preview. Below property will be used to include memo even businessLogic is evaluated to  false
		/// </summary>
		public bool UserIncluded { get; set; }
		/// <summary>
		/// If true, memo will be rendered in Letter Preview
		/// </summary>
		public bool Include { get; set; }
		/// <summary>
		/// The original value of "include" based on dynamic expressions
		/// </summary>
		public bool DefaultInclude { get; set; }
		/// <summary>
		/// True if the user can remove this memo from the letter
		/// </summary>
		public bool Removable { get; set; }
		/// <summary>
		/// Only true for memos that are manually added by the user
		/// </summary>
		public bool Custom { get; set; }
		public IList<string> SplittableFields { get; set; }
		public IList<GenerateLetterPaging> Paging { get; set; }
		/// <summary>
		/// This specifies the memo is related to a data Source. Currently hte options are
		/// ClauseReports
		/// Components
		/// Paytables
		/// This list should exist in an enum somewhere someday
		/// </summary>
		public string SourceId { get; set; }
		/// <summary>
		/// This is intended to specify a subset of Source objects this memo is referencing. 
		/// For instance, SourceId may be Paytables, and SourceProperty could be "ProgressiveLevels" or "Volatilities"
		/// Another example, SourceId may be Components and SourceProperty could be "Signatures"
		/// </summary>
		public string SourceProperty { get; set; }
		/// <summary>
		/// Used to replace the usage of SourceProperty to identify hte modifications memos. This will likely exist as a 
		/// 'special flag' option.
		/// </summary>
		public bool IsModification { get; set; }
		/// <summary>
		/// Used to replace the usage of SourceProperty to identify hte game descriptions memos. This will likely exist as a 
		/// 'special flag' option.
		/// </summary>
		public bool IsGameDescription { get; set; }
		/// <summary>
		/// The actual HTML of this memo that will be rendered in Letter Preview
		/// </summary>
		public string LetterContent { get; set; }
		public string SecondaryLanguageLetterContent { get; set; }
		public int? SecondaryLanguageId { get; set; }

		public IList<GenerateLetterInputControl> LetterContentControls { get; set; }
		/// <summary>
		/// Let's put the dynamic expressions that dictate repeating content in a separate property, so they don't have to be ignored
		/// when iterating over controls the user has to interact with. This should probably be a new model, too
		/// </summary>
		public IList<GenerateLetterInputControl> RepeatableContentControls { get; set; }
		/// <summary>
		/// Expression to determine if this memo should be shown
		/// </summary>
		public string BusinessLogicExpression { get; set; }
		/// <summary>
		/// Each memo will be related to a subset of all of the Source items there are. This collection will keep track of which 
		/// Source items it's displaying. For long sources, like clauses, there will be many clones of this memo, each with a different
		/// subset of the Source items
		/// </summary>
		public IList<SourceItemTarget> SourceItemTargets { get; set; }
		public string Language { get; set; }
		public string SecondaryLanguage { get; set; }
		/// <summary>
		/// A property to notify grandparent components of changes to the Content property. Needs to be reactive, so 
		/// is included in the model here.
		/// </summary>
		public string ContentChanged { get; set; }

		/// <summary>
		///  This string should hold the name of the workspace category that this memo is a member of.
		/// </summary>
		public string WorkSpaceCategory { get; set; }

		/// <summary>
		/// Use this flag to determine if the memo is in edit mode.
		/// </summary>
		public bool EditMode { get; set; }

		/// <summary>
		/// This field holds the original version id of this memo. If the memo is new then the version id is 0
		/// Use this field to link a custom memo. If the version id is 0, then use memo id
		/// </summary>
		public int OriginalVersionId { get; set; }

		/// <summary>
		/// This is the ID of the Letter Content Database Entity that this Memo Entity is a part of.
		/// </summary>
		public int? LetterContentId { get; set; }

		public string SpecialFlag { get; set; }

		public string ContentSplitFrom { get; set; }
		public int AnchorMemoId { get; set; }

	}
}