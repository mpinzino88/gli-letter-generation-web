﻿namespace Submissions.Web.Areas.Documents.Models.Generate
{
	public enum GenerateLetterMemoType
	{
		STATIC,
		DYNAMIC
	}
}