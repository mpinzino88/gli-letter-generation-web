﻿namespace Submissions.Web.Areas.Documents.Models.Generate
{
	public class GenerateLetterPaging
	{
		/// <summary>
		/// The jurisdiction Id to make the Page number jurisdiction-specific
		/// </summary>
		public int JurisdictionId { get; set; }
		/// <summary>
		/// The LocalId of the memo this is attached to
		/// </summary>
		public System.Guid LocalId { get; set; }
		/// <summary>
		/// The page number for this memo, starting at 1
		/// </summary>
		public int Page { get; set; }
		/// <summary>
		/// The height in this memo, set in MemoComponent.js setHeight() using offsetHeight
		/// </summary>
		public int Height { get; set; }
		/// <summary>
		/// Determines if the page this memo is on is Landscape oriented
		/// </summary>
		public bool IsLandscape { get; set; }
	}
}