﻿using System;
using System.Collections.Generic;

namespace Submissions.Web.Areas.Documents.Models.Generate
{
	public class GenerateLetterProjectInfo
	{
		public GenerateLetterProjectInfo()
		{
			JurisdictionIds = new List<int>();
			ComponentGroupIds = new List<System.Guid>();
			MergedProjectIds = new List<int>();
		}
		public int BundleId { get; set; }
		public string BundleName { get; set; }
		public string FileNumber { get; set; }
		public string ManufacturerName { get; set; }
		public string ManufacturerId { get; set; }
		public List<int> JurisdictionIds { get; set; }
		public string ProjectStatus { get; set; }
		public string ProjectHolder { get; set; }
		public string QAStatus { get; set; }
		public string ProjectEvalLab { get; set; }
		public List<System.Guid> ComponentGroupIds { get; set; }
		public DateTime? EvalPeriodFrom { get; set; }
		public DateTime? EvalPeriodTo { get; set; }
		public int ProjectId { get; internal set; }
		public bool ContainsRJWD { get; set; }
		/// <summary>
		/// Holds the addresses of the labs where testing happened
		/// </summary>
		public List<string> EvalLabs { get; set; }
		public List<string> EvalLabLetterCodes { get; set; }
		public List<int> MergedProjectIds { get; set; }
		public bool EvolutionInstanceExists { get; set; }

	}
}