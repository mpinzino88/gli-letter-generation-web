﻿using System.Collections.Generic;

namespace Submissions.Web.Areas.Documents.Models.Generate
{
	public class GenerateLetter
	{
		public GenerateLetter()
		{
			Sections = new List<GenerateLetterSection>();
			Footers = new List<GenerateLetterMemo>();
			Headers = new List<GenerateLetterMemo>();
		}

		public IList<GenerateLetterSection> Sections { get; set; }
		public IList<GenerateLetterMemo> Footers { get; set; }
		public IList<GenerateLetterMemo> Headers { get; set; }
	}
}