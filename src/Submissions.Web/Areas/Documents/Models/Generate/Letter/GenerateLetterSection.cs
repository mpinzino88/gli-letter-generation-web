﻿using System.Collections.Generic;

namespace Submissions.Web.Areas.Documents.Models.Generate
{
	public class GenerateLetterSection
	{
		public GenerateLetterSection()
		{
			Memos = new List<GenerateLetterMemo>();

		}

		public string Name { get; set; }
		public string Description { get; set; }

		public int Id { get; set; }

		public List<GenerateLetterMemo> Memos { get; set; }

		public int OrderId { get; set; }
	}
}