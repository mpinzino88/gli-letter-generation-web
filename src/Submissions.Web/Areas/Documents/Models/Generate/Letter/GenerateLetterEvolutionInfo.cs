﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Submissions.Web.Areas.Documents.Models.Generate
{
	public class GenerateLetterEvolutionInfo
	{
		public GenerateLetterEvolutionInfo()
		{
			ComponentGroupDoubleLockStatus = new Dictionary<Guid, bool>();
		}
		public Dictionary<Guid, bool> ComponentGroupDoubleLockStatus { get; set; }
		public bool EvolutionInstanceIsSecure => !ComponentGroupDoubleLockStatus.Any(x => !x.Value);
	}
}