﻿using LinqKit;
using Submissions.Core.Models.LetterContent;
using System.Linq;

namespace Submissions.Web.Areas.Documents.Models.Generate
{
	public static class AlphaLetterContentQueryExtender
	{
		public static IQueryable<GenerateLetterMemo> ApplicableToLetter(this IQueryable<GenerateLetterMemo> query, LetterContentSearchFilter filter)
		{
			var predicate = PredicateBuilder.New<GenerateLetterMemo>();
			predicate = predicate.Start(x => true);

			if (filter.JurisdictionIds.Count > 0)
			{
				predicate = predicate.And(p => !p.ApplicableJurisdictions.Any() || p.ApplicableJurisdictions.Any(jurisdiction => filter.JurisdictionIds.Contains(jurisdiction)));
			}
			if (filter.ManufacturerIds.Count > 0)
			{
				predicate = predicate.And(p => !p.ApplicableManufacturers.Any() || p.ApplicableManufacturers.Any(manufacturer => filter.ManufacturerIds.Contains(manufacturer)));
			}
			if (!string.IsNullOrWhiteSpace(filter.WorkspaceCategory))
			{
				predicate = predicate.And(p => p.WorkSpaceCategory == filter.WorkspaceCategory);
			}

			query = query.Where(predicate);
			return query;
		}
	}
}