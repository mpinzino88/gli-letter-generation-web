﻿using System;

namespace Submissions.Web.Areas.Documents.Models.Coversheet
{
	public class MathCoversheetModel
	{
		public bool? AdvertisedAwardsAvailable { get; set; }
		public bool? NV1in100 { get; set; }
		public bool? PA1in50 { get; set; }
		public bool? MinIncrementUsed { get; set; }
		public bool? MinResetUsed { get; set; }
		public bool? ProgIncrementAt0 { get; set; }
		public bool? ProgResetAt0 { get; set; }
		public bool? ProgIncrementConfigurable { get; set; }
		public bool? ProgResetConfigurable { get; set; }
		public decimal? BaseHitRate { get; set; }
		public bool? SkillGame { get; set; }
		public decimal? PlayerChoiceRatio { get; set; }
		public bool? EqualWeightedLines { get; set; }
		public bool? EqualWeightedSymbols { get; set; }
		public decimal? FreeGames99 { get; set; }
		public bool? PARTPDecrease { get; set; }
		public decimal? PAWorstOdds { get; set; }
		public int? GPSItemId { get; set; }
		public decimal? MaxProbabilityWithoutProgressive { get; set; }
		public DateTime? StateDate { get; set; }
		public double? RIDenom { get; set; }
		public bool? State { get; set; }
		public bool? HelpScreenReview { get; set; }
		public int? LinesOrWaysCount { get; set; }
		public int? LinesOrWays { get; set; }
		public int? KenoDrawnBalls { get; set; }
		public int? KenoTotalBalls { get; set; }
		public int? KenoMaxSpots { get; set; }
		public int? KenoMinSpots { get; set; }
		public Guid? PreviousCoversheet { get; set; }
		public bool? CanDisableProgressive { get; set; }
		public bool? MWAP { get; set; }
		public decimal? JurisdictionalOdds { get; set; }
		public double? MaxProbabilityWithProgressive { get; set; }
		public decimal? AverageReturn { get; set; }
		public decimal? MaxReturnExcludingProgressive { get; set; }
		public decimal? MaxReturn { get; set; }
		public decimal? MinReturnExcludingProgressive { get; set; }
		public decimal? MinReturn { get; set; }
		public bool? WideAreaProgressive { get; set; }
		public bool? Progressive { get; set; }
		public int? MaxBet { get; set; }
		public int? MinBet { get; set; }
		public string ThemeName { get; set; }
		public string Type { get; set; }
		public string PaytableId { get; set; }
		public Guid Id { get; set; }
		public decimal? LinkedOdds { get; set; }
		public string CycleString { get; set; }
		public int? MaxAward { get; set; }
		public string PAWorstOddsDescription { get; set; }
		public bool? PlayerStrategyInCalculations { get; set; }
		public string CoversheetVersion { get; set; }
		public string OriginalMathFileNumber { get; set; }
		public string MathFileNumber { get; set; }
		public string ProgramID { get; set; }
		public string OriginalMathematician { get; set; }
		public string Mathematician { get; set; }
		public bool? ConfigurableProgressive { get; set; }
		public string Analysis { get; set; }
		public string MathNotes { get; set; }
		public string SpecialNotes { get; set; }
		public System.Numerics.BigInteger? TopAwardProgressiveHits { get; set; }
		public string TopAwardProgressiveHitsString { get; set; }
		public string TopAwardHitsString { get; set; }
		public decimal? DEAverage { get; set; }
		public decimal? MOLowerBound { get; set; }
		public string PAWorstOddsCalculationDescription { get; set; }
		public decimal? MDAverage { get; set; }

	}
}