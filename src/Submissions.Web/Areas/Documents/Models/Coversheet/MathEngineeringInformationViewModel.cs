﻿using Newtonsoft.Json;
using Submissions.Common.CustomJsonConverters;
using Submissions.Common.Validation;
using System;
using System.ComponentModel.DataAnnotations;

namespace Submissions.Web.Areas.Documents.Models.Coversheet
{
	public class MathEngineeringInformationViewModel
	{
		public string ErrorMessage { get; set; }

		public bool? AdvertisedAwardsAvailable { get; set; }

		[StringLength(255)]
		public string Analysis { get; set; }

		public bool? AutoHold { get; set; }

		[DecimalPrecisionAttribute(19, 5)]
		public decimal? AverageReturn { get; set; }

		[DecimalPrecisionAttribute(19, 5)]
		public decimal? BaseHitRate { get; set; }

		public bool? BonusFeature { get; set; }

		public bool? CanDisableProgressive { get; set; }

		public bool? ConfigurableProgressive { get; set; }

		[RegularExpression("^[a-zA-Z0-9]{8}-[a-zA-Z0-9]{4}-[a-zA-Z0-9]{4}-[a-zA-Z0-9]{4}-[a-zA-Z0-9]{12}$")]
		public Guid? CoversheetId { get; set; }

		[StringLength(150)]
		public string CoversheetVersion { get; set; }

		[StringLength(255)]
		public string CRC { get; set; }

		[StringLength(255)]
		public string Cycle { get; set; }

		[DecimalPrecisionAttribute(19, 5)]
		public decimal? DEAverage { get; set; }

		public bool? DoubleDown { get; set; }

		public bool? DoubleUp { get; set; }

		public bool? EqualWeightedLines { get; set; }

		public bool? EqualWeightedSymbols { get; set; }

		public bool? ExternalLinked { get; set; }

		[DecimalPrecisionAttribute(19, 5)]
		public decimal? FreeGames99 { get; set; }

		public bool? HardCodedDenom { get; set; }

		public bool? HelpscreenReview { get; set; }

		[Required]
		[RegularExpression("^[a-zA-Z0-9]{8}-[a-zA-Z0-9]{4}-[a-zA-Z0-9]{4}-[a-zA-Z0-9]{4}-[a-zA-Z0-9]{12}$")]
		public Guid Id { get; set; }

		public bool? Insurance { get; set; }

		public bool? InternalLinked { get; set; }

		[DecimalPrecisionAttribute(19, 5)]
		public decimal? JurisdictionalOdds { get; set; }

		[Range(int.MinValue, int.MaxValue)]
		public int? KenoDrawnBalls { get; set; }

		[Range(int.MinValue, int.MaxValue)]
		public int? KenoMaxSpots { get; set; }

		[Range(int.MinValue, int.MaxValue)]
		public int? KenoMinSpots { get; set; }

		[Range(int.MinValue, int.MaxValue)]
		public int? KenoTotalBalls { get; set; }

		[StringLength(255)]
		public string Kobe4 { get; set; }

		[Range(int.MinValue, int.MaxValue)]
		public int? LinesOrWays { get; set; }

		[Range(int.MinValue, int.MaxValue)]
		public int? LinesOrWaysCount { get; set; }

		[DecimalPrecisionAttribute(19, 5)]
		public decimal? LinkedOdds { get; set; }

		[StringLength(150)]
		public string Mathematician { get; set; }

		[StringLength(150)]
		public string MathFileNumber { get; set; }

		[StringLength(4000)]
		public string MathNotes { get; set; }

		[Range(int.MinValue, int.MaxValue)]
		public int? MaxAward { get; set; }

		[Range(int.MinValue, int.MaxValue)]
		public int? MaxBet { get; set; }

		[Range(double.MinValue, double.MaxValue)]
		public double? MaxProbabilityWithoutProgressive { get; set; }

		[Range(double.MinValue, double.MaxValue)]
		public double? MaxProbabilityWithProgressive { get; set; }

		[DecimalPrecisionAttribute(19, 5)]
		public decimal? MaxReturn { get; set; }

		[DecimalPrecisionAttribute(19, 5)]
		public decimal? MaxReturnExcludingProgressive { get; set; }

		[DecimalPrecisionAttribute(19, 5)]
		public decimal? MDAverage { get; set; }

		[Range(int.MinValue, int.MaxValue)]
		public int? MinBet { get; set; }

		public bool? MinIncrementUsed { get; set; }

		public bool? MinResetUsed { get; set; }

		[DecimalPrecisionAttribute(19, 5)]
		public decimal? MinReturn { get; set; }

		[DecimalPrecisionAttribute(19, 5)]
		public decimal? MinReturnExcludingProgressive { get; set; }

		[DecimalPrecisionAttribute(19, 5)]
		public decimal? MOLowerBound { get; set; }

		[RegularExpression("^[a-zA-Z0-9]{8}-[a-zA-Z0-9]{4}-[a-zA-Z0-9]{4}-[a-zA-Z0-9]{4}-[a-zA-Z0-9]{12}$")]
		public Guid? MultiMultiId { get; set; }

		public bool? MWAP { get; set; }

		public bool? MysteryLinked { get; set; }

		[Range(int.MinValue, int.MaxValue)]
		public int? NumberOfCards { get; set; }

		[Range(int.MinValue, int.MaxValue)]
		public int? NumberOfDecks { get; set; }

		public bool? NV1in100 { get; set; }

		[StringLength(255)]
		public string OnScreenId { get; set; }

		[StringLength(150)]
		public string OriginalMathematician { get; set; }

		[StringLength(150)]
		public string OriginalMathFileNumber { get; set; }

		[DecimalPrecisionAttribute(19, 5)]
		public decimal? OverallReturn { get; set; }

		public bool? ParSheetVols { get; set; }

		[DecimalPrecisionAttribute(19, 5)]
		public decimal? PAWorstOdds { get; set; }

		[StringLength(4000)]
		public string PAWorstOddsCalculationDescription { get; set; }

		public bool? PARTPDecrease { get; set; }

		[StringLength(4000)]
		public string PAWorstOddsDescription { get; set; }

		[StringLength(255)]
		public string PaytableId { get; set; }

		[Range(short.MinValue, short.MaxValue)]
		public short? PaytablePosition { get; set; }

		public bool? PA1in50 { get; set; }

		[DecimalPrecisionAttribute(19, 5)]
		public decimal? PlayerChoiceRatio { get; set; }

		public bool? PlayerStrategyInCalculations { get; set; }

		public bool? ProgIncrementAt0 { get; set; }

		public bool? ProgIncrementConfigurable { get; set; }

		[StringLength(150)]
		public string ProgramID { get; set; }

		public bool? ProgResetAt0 { get; set; }

		public bool? ProgResetConfigurable { get; set; }

		public bool? Progressive { get; set; }

		[Range(int.MinValue, int.MaxValue)]
		public int? ProgressiveLevels { get; set; }

		[Range(int.MinValue, int.MaxValue)]
		public int? Reels { get; set; }

		public bool? Relevant { get; set; }

		[Range(double.MinValue, double.MaxValue)]
		public double? RIDenom { get; set; }

		[StringLength(255)]
		public string SHA1 { get; set; }

		public bool? SkillBonus { get; set; }

		public bool? SkillGame { get; set; }

		[StringLength(4000)]
		public string SpecialNotes { get; set; }

		public bool? Split { get; set; }

		public bool? State { get; set; }

		[JsonConverter(typeof(OnlyDateConverter))]
		public DateTime? StateDate { get; set; }

		public bool? StandaloneProgressive { get; set; }

		public bool? Surrender { get; set; }

		public bool? TableGame { get; set; }

		[StringLength(255)]
		public string ThemeName { get; set; }

		[StringLength(255)]
		public string TopAwardHits { get; set; }

		[StringLength(255)]
		public string TopAwardProgressiveHits { get; set; }

		public bool? Tournament { get; set; }

		[StringLength(255)]
		public string Type { get; set; }

		[RegularExpression("^[a-zA-Z0-9]{8}-[a-zA-Z0-9]{4}-[a-zA-Z0-9]{4}-[a-zA-Z0-9]{4}-[a-zA-Z0-9]{12}$")]
		public Guid? Version { get; set; }

		public bool? WideAreaProgressive { get; set; }

		[StringLength(255)]
		public string WVMaxBet { get; set; }

		[Range(int.MinValue, int.MaxValue)]
		public int? WVOdds { get; set; }
	}
}