﻿using Submissions.Core.Models.LetterContent;
using System.Collections.Generic;

namespace Submissions.Web.Areas.Documents.Models.Coversheet
{
	public class MathCoversheetViewModel
	{
		public MathCoversheetViewModel()
		{
			CoversheetHeadings = new string[]
			{
				"Math FileNumber",
				"Paytable Id",
				"Theme Name",
				"Min Return",
				"Max Return",
				"Progressive",
				"Skill Game"
			};

			MathEngineeringHeadings = new string[]
			{
				"Math FileNumber",
				"Paytable Id",
				"Theme Name",
				"Min Return",
				"Max Return",
				"Progressive",
				"Skill Game"
			};
		}

		public IList<string> CoversheetHeadings { get; set; }

		public IList<string> MathEngineeringHeadings { get; set; }

		public IList<MathCoversheetDTO> Coversheets { get; set; }
	}
}