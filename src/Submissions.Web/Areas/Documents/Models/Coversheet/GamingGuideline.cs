﻿using System.Collections.Generic;

namespace Submissions.Web.Areas.Documents.Models.Coversheet
{
	public class GamingGuideline
	{
		public GamingGuideline()
		{
			RTPRequirements = new List<RTPRequirement>();
		}

		public int JurisdictionId { get; set; }
		public List<RTPRequirement> RTPRequirements { get; set; }
	}

	public class RTPRequirement
	{
		public string Type { get; set; }
		public double RTP { get; set; }
		public double? DenomOne { get; set; }
		public double? DenomTwo { get; set; }
		public string RTPType { get; set; }
		public string Symbol { get; set; }
	}
}