﻿using Submissions.Web.Models.Grids.Documents;

namespace Submissions.Web.Areas.Documents.Models.DraftLetters
{
	public class IndexViewModel
	{
		public DraftLettersGrid DraftLettersGrid { get; } = new DraftLettersGrid();

		public int ProjectId { get; set; }

		public IndexViewModel(int projectId)
		{
			ProjectId = projectId;
		}
	}
}