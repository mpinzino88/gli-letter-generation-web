﻿using Submissions.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Submissions.Web.Areas.Documents.Models.DraftLetters
{
	public class EditViewModel
	{
		public EditViewModel()
		{
			Statuses = LookupsStandard.ConvertEnum<DraftLetterStatus>(showValueInDescription: true, showBlank: true);
			UpdateDateReasons = LookupsStandard.ConvertEnum<UpdateDateReason>(showValueInDescription: true, showBlank: true);
			TimelineTypes = new List<SelectListItem>() {
				new SelectListItem { Value = "None", Text = "None" },
				new SelectListItem { Value = "RU", Text = "RU"},
				new SelectListItem { Value = "NV", Text = "NV", }
			};
		}
		public int? AddendumGpsFileId { get; set; }

		public string AddendumLetterhead { get; set; }

		[Display(Name = "Addendum Letterhead")]
		public int? AddendumLetterheadId { get; set; }

		[Display(Name = "Attach Correction Addendum")]
		public bool AttachCorrectionAddendum { get; set; }

		[Display(Name = "Draft Date")]
		public DateTime? DraftDate { get; set; }

		public int DraftLetterId { get; set; }

		[MaxLength(255)]
		[Display(Name = "Filepath")]
		public string FilePath { get; set; }

		[MaxLength(255)]
		[Display(Name = "GPS Filepath")]
		public string GPSFilePath { get; set; }

		public string Letterhead { get; set; }

		[Display(Name = "Main Letterhead")]
		public int? LetterheadId { get; set; }

		public int? PdfFileGpsId { get; set; }

		public int ProjectId { get; set; }

		[Display(Name = "Repeat Overlay:")]
		public bool RepeatAddLetterhead { get; set; }

		[Display(Name = "Repeat Overlay:")]
		public bool RepeatLetterhead { get; set; }

		public DraftLetterStatus? Status { get; set; }

		public IEnumerable<SelectListItem> Statuses { get; set; }

		[Required]
		[Display(Name = "Timeline Requested")]
		public bool TimelineRequested { get; set; }

		[Display(Name = "Timeline Type")]
		public string TimelineType { get; set; }

		public IList<SelectListItem> TimelineTypes { get; set; }

		[Display(Name = "Updated Date Reason")]
		public UpdateDateReason? UpdatedDateReason { get; set; }

		public IEnumerable<SelectListItem> UpdateDateReasons { get; set; }

		public int? WordFileGPSId { get; set; }
	}
}