﻿using System.Web.Mvc;

namespace Submissions.Web.Areas.Acumatica
{
	public class AcumaticaAreaRegistration : AreaRegistration
	{
		public override string AreaName
		{
			get
			{
				return "Acumatica";
			}
		}

		public override void RegisterArea(AreaRegistrationContext context)
		{
			context.MapRoute(
				"Acumatica_default",
				"Acumatica/{controller}/{action}/{id}",
				new { action = "Index", id = UrlParameter.Optional }
			);
		}
	}
}