﻿namespace Submissions.Web.Areas.Acumatica.Models
{
	public class ErrorResolutionResult
	{
		public int ErrorLogID { get; set; }
		public string EntityType { get; set; }
		public int EntityID { get; set; }
		public string ProjectID { get; set; }
		public bool Success { get; set; }
	}
}