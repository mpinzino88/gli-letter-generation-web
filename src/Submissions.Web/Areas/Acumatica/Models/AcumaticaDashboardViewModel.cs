﻿namespace Submissions.Web.Areas.Acumatica.Models
{
	public class AcumaticaDashboardViewModel
	{
		public int AcumaticaProjectCreationErrorCount { get; set; }
		public int AcumaticaTaskErrorCount { get; set; }
		public int AcumaticaPendingJurisdictionalDataCount { get; set; }
		public int AcumaticaPendingTasksCount { get; set; }
		public int AcumaticaPendingProjectsCount { get; set; }
		public int AcumaticaPendingProjectTasksCount { get; set; }
		public string AcumaticaProjectCreationServiceStatus { get; set; }
		public string AcumaticaProjectTaskCreationServiceStatus { get; set; }
		public bool NJServices2Status { get; set; }
		public bool Debug { get; set; }
		public string Environment { get; set; }
	}
}