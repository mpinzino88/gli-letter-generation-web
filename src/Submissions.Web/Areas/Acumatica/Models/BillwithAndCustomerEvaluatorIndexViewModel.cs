﻿using GLI.Accounting.Acumatica.Models.AcumaticaBWAC;

namespace Submissions.Web.Areas.Acumatica.Models
{
	public class BillwithAndCustomerEvaluatorIndexViewModel
	{
		public BillwithAndCustomerEvaluatorIndexViewModel()
		{
			BWACModule = new BWACModuleDTO();
		}

		public BWACModuleDTO BWACModule { get; set; }
	}
}