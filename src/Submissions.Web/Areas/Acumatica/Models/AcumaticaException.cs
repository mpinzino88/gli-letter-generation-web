﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Text.RegularExpressions;

namespace Submissions.Web.Areas.Acumatica.Models
{
	public class AcumaticaException
	{
		private readonly string[] separatingStrings = { "PX.Data.PXException:", "PX.Data.PXOuterException:", "--- End of inner exception stack trace ---" };
		private readonly string[] separatingStrings2 = { ":", "'" };

		public AcumaticaException()
		{

		}

		public void ParseAndSetException()
		{
			var exceptionMessage = string.Empty;
			try
			{
				exceptionMessage = (string)JsonConvert.DeserializeObject<JObject>(this.RawException)["ExceptionMessage"] ?? string.Empty;
			}
			catch
			{
				exceptionMessage = string.Empty;
			}

			if (!string.IsNullOrEmpty(exceptionMessage))
			{
				var exceptionParts = exceptionMessage.Split(separatingStrings, System.StringSplitOptions.RemoveEmptyEntries);

				for (int i = 0; i < exceptionParts.Length; i++)
				{
					exceptionParts[i] = exceptionParts[i].Replace("\n", string.Empty).Replace("\r", "").Replace("--->", "");
				}

				if (exceptionParts.Length == 3)
				{
					ExceptionMessage = exceptionParts[0];
					OuterExceptionMessage = exceptionParts[1];
					TargetExceptionMessage = exceptionParts[2];

					Regex regex = new Regex(@"'(.*)'");
					Match match = regex.Match(TargetExceptionMessage);
					if (match.Success)
					{
						TargetExceptionMessage = TargetExceptionMessage.Replace(match.Value, match.Value.Replace(" ", "")).Trim();
						Target = TargetExceptionMessage.Split(' ')[2].Trim('\'');
						Problem = TargetExceptionMessage.Split('\'')[2].Trim();
					}
				}
				else
				{
					var customSplit = exceptionParts[0].Split(separatingStrings2, System.StringSplitOptions.RemoveEmptyEntries);
					TargetExceptionMessage = exceptionParts[0].Trim();
					Target = customSplit[2].Replace(" ", "").Trim('\'');
					Problem = customSplit[3].Trim('\'');
					ExceptionMessage = Target + Problem;
				}
			}
		}


		public string RawException { get; set; }
		public string ExceptionMessage { get; set; }
		public string OuterExceptionMessage { get; set; }
		public string TargetExceptionMessage { get; set; }
		public string Target { get; set; }
		public string Problem { get; set; }
	}
}