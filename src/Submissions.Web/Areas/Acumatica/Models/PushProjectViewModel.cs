﻿using System.Collections.Generic;

namespace Submissions.Web.Areas.Acumatica.Models
{
	public class PushProjectViewModel
	{
		public PushProjectViewModel()
		{
			PushProjectGridModel = new PushProjectGridModel();
		}

		public string ProjectName { get; set; }
		public string DestinationTenant { get; set; }
		public bool Debug { get; set; }
		public PushProjectGridModel PushProjectGridModel { get; set; }
	}

	public class PushProjectGridModel
	{
		public PushProjectGridModel()
		{
			JurisdictionProjectRows = new List<PushJurisdictionProjectRow>();
		}
		public string Filenumber { get; set; }
		public string ProjectName { get; set; }
		public List<PushJurisdictionProjectRow> JurisdictionProjectRows { get; set; }
	}

	public class PushJurisdictionProjectRow
	{
		public int JurisdictionalDataId { get; set; }
		public string JurisdictionId { get; set; }
		public string JurisdictionName { get; set; }
		public string BillWith { get; set; }
	}
}