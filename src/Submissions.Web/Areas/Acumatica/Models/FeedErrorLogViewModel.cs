﻿using System.Collections.Generic;

namespace Submissions.Web.Areas.Acumatica.Models
{
	public class FeedErrorLogViewModel
	{
		public FeedErrorLogViewModel()
		{
			UnResolved = new List<FeedErrorLog>();
		}
		public List<FeedErrorLog> UnResolved { get; set; }
	}
}