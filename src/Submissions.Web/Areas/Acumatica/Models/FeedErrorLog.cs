﻿using System;

namespace Submissions.Web.Areas.Acumatica.Models
{
	public class FeedErrorLog
	{
		public FeedErrorLog()
		{
			AcumaticaException = new AcumaticaException();
		}

		public int Id { get; set; }
		public DateTime LogDate { get; set; }
		public string LogSource { get; set; }
		public AcumaticaException AcumaticaException { get; set; }
		public int? EntityId { get; set; }
		public string EntityType { get; set; }
		public string OwningCompany { get; set; }
		public string PerformingCompany { get; set; }
		public string AcumaticaProjectId { get; set; }
		public bool Resolved { get; set; }
		public string Customer { get; set; }
		public string TemplateUsed { get; set; }
		public string AllocationId { get; set; }
		public string BillingCurrencyId { get; set; }
		public string BillingId { get; set; }
		public string GameName { get; set; }
		public string ConsolidationProjectId { get; set; }
		public string ContractType { get; set; }
		public string ProjectCurrencyId { get; set; }
	}
}