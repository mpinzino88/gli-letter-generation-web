﻿using GLI.Accounting.Acumatica.Interfaces;
using GLI.Accounting.Acumatica.Models.AcumaticaService.TopLevelEntityDTOs;
using GLI.EFCore.Submission;
using Submissions.Common;
using Submissions.Web.Areas.Acumatica.Models;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web.Http;
using System.Web.Mvc;

namespace Submissions.Web.Areas.Acumatica.Controllers
{
	public class FeedErrorLogController : Controller
	{
		private readonly SubmissionContext _dbSubmission;
		private readonly IAcumaticaService _acumaticaService;
		private readonly IUserContext _userContext;
		public FeedErrorLogController(SubmissionContext dbSubmission, IAcumaticaService acumaticaService, IUserContext userContext)
		{
			_dbSubmission = dbSubmission;
			_acumaticaService = acumaticaService;
			_userContext = userContext;
		}

		#region MVC ActionResults
		public ActionResult Index()
		{
			return View(BuildFeedErrorLogViewModel());
		}
		#endregion

		#region Public API
		[System.Web.Http.HttpPost]
		public ContentResult MarkResolved([FromBody] List<FeedErrorLog> resolvedItems)
		{
			try
			{
				_dbSubmission.ChangeTracker.AutoDetectChangesEnabled = false;
				foreach (var id in resolvedItems.Select(x => x.Id).ToList())
				{
					AcumaticaAPIErrorLog updateAcumaticaErrorLog = new AcumaticaAPIErrorLog
					{
						Id = id,
						Resolved = true
					};

					_dbSubmission.AcumaticaAPIErrorLogs.Attach(updateAcumaticaErrorLog);
					_dbSubmission.Entry(updateAcumaticaErrorLog).Property(x => x.Resolved).IsModified = true;
				}

				SaveChanges();
			}
			finally
			{
				_dbSubmission.ChangeTracker.AutoDetectChangesEnabled = true;
			};

			return Content(ComUtil.JsonEncodeCamelCase(resolvedItems), "application/json");
		}
		[System.Web.Http.HttpPost]
		public ContentResult SetupProjects([FromBody]List<FeedErrorLog> toRetry)
		{
			var results = BuildErrorResolutionResultSet(toRetry);
			MarkJurisdictionalDataSentToAcumatica(results.Where(x => x.EntityType.ToLower() == "jurisdictionaldata" && x.Success).Select(x => x.EntityID).ToList());
			MarkTrTaskSentToAcumatica(results.Where(x => x.EntityType.ToLower() == "trtask" && x.Success).Select(x => x.EntityID).ToList());
			MarkErrorsResolved(results.Where(x => x.Success).Select(x => x.ErrorLogID).ToList());

			return Content(ComUtil.JsonEncodeCamelCase(new { results }), "application/json");
		}
		[System.Web.Http.HttpPost]
		public ContentResult GetUnResolvedAcumaticaFeedErrors()
		{
			return Content(ComUtil.JsonEncodeCamelCase(BuildUnResolvedFeedErrorLogs()), "application/json");
		}
		#endregion

		#region Model Builders
		private FeedErrorLogViewModel BuildFeedErrorLogViewModel()
		{
			return new FeedErrorLogViewModel
			{
				UnResolved = BuildUnResolvedFeedErrorLogs()
			};
		}
		private List<FeedErrorLog> BuildUnResolvedFeedErrorLogs()
		{
			var result = _dbSubmission.AcumaticaAPIErrorLogs
									.Where(x => !x.Resolved)
									.Select(x => new FeedErrorLog
									{
										Id = x.Id,
										AcumaticaProjectId = x.AcumaticaProjectId,
										EntityId = x.EntityId,
										EntityType = x.EntityType,
										LogDate = x.LogDate,
										AcumaticaException = new AcumaticaException
										{
											RawException = x.LogMessage
										},
										LogSource = x.LogSource,
										OwningCompany = x.OwningCompany,
										PerformingCompany = x.PerformingCompany,
										Resolved = x.Resolved,
										AllocationId = x.AllocationId,
										BillingCurrencyId = x.BillingCurrencyId,
										BillingId = x.BillingId,
										ConsolidationProjectId = x.ConsolidationProjectId,
										ContractType = x.ContractType,
										Customer = x.Customer,
										GameName = x.GameName,
										ProjectCurrencyId = x.ProjectCurrencyId,
										TemplateUsed = x.TemplateUsed
									})
									.ToList();
			result.ForEach(x => x.AcumaticaException.ParseAndSetException());

			return result;
		}
		private List<ErrorResolutionResult> BuildErrorResolutionResultSet(List<FeedErrorLog> feedErrorLogs)
		{
			var result = new List<ErrorResolutionResult>();

			result.AddRange(SetupProjectFromJurisdictionalData(feedErrorLogs.Where(x => x.EntityType.ToLower() == "jurisdictionaldata").ToList()));
			result.AddRange(SetupProjectFromTrTask(feedErrorLogs.Where(x => x.EntityType.ToLower() == "trtask").ToList()));

			return result;
		}
		#endregion

		#region Helpers
		private List<ErrorResolutionResult> SetupProjectFromJurisdictionalData(List<FeedErrorLog> jurisdictionalDataErrors)
		{
			var result = new List<ErrorResolutionResult>();

			foreach (var jd in jurisdictionalDataErrors)
			{
				var toCreate = _acumaticaService.SetupAndCreateJurisdictionProject((int)jd.EntityId);

				result.Add(
					new ErrorResolutionResult
					{
						EntityID = (int)jd.EntityId,
						EntityType = jd.EntityType,
						ErrorLogID = jd.Id,
						ProjectID = toCreate.ProjectDTO.AcumaticaProject.ProjectId.Value,
						Success = toCreate.Response.IsSuccessStatusCode
					});
			}

			return result;
		}
		private List<ErrorResolutionResult> SetupProjectFromTrTask(List<FeedErrorLog> trTaskErrors)
		{
			var result = new List<ErrorResolutionResult>();

			foreach (var task in trTaskErrors)
			{
				AcumaticaProjectTaskDTO taskToCreate = _acumaticaService.SetupProjectTask((int)task.EntityId);
				var projectToCreate = _acumaticaService.SetupAndCreateInitialProject(taskToCreate.trProjectId, taskToCreate.PerformingCompany);
				if (projectToCreate.Response.IsSuccessStatusCode)
				{
					HttpResponseMessage taskResponse = _acumaticaService.CreateProjectTask(taskToCreate);
					result.Add(
						new ErrorResolutionResult
						{
							EntityID = (int)task.EntityId,
							EntityType = task.EntityType,
							ErrorLogID = task.Id,
							ProjectID = taskToCreate.ProjectId.Value,
							Success = taskResponse.IsSuccessStatusCode
						});
				}
				else
				{
					result.Add(
						new ErrorResolutionResult
						{
							EntityID = (int)task.EntityId,
							EntityType = task.EntityType,
							ErrorLogID = task.Id,
							ProjectID = taskToCreate.ProjectId.Value,
							Success = false
						});
				}
			}

			return result;
		}
		private void MarkErrorsResolved(List<int> ids)
		{
			var toUpdate = _dbSubmission.AcumaticaAPIErrorLogs.Where(x => ids.Contains(x.Id)).ToList();
			toUpdate.ForEach(x => x.Resolved = true);
			SaveChanges();

		}
		private void MarkJurisdictionalDataSentToAcumatica(List<int> ids)
		{
			var toUpdate = _dbSubmission.JurisdictionalData.Where(x => ids.Contains(x.Id)).ToList();
			toUpdate.ForEach(x => x.SentToAcumatica = 1);
			SaveChanges();
		}
		private void MarkTrTaskSentToAcumatica(List<int> ids)
		{
			var toUpdate = _dbSubmission.trTasks.Where(x => ids.Contains(x.Id)).ToList();
			toUpdate.ForEach(x => x.SentToAcumatica = 1);
			SaveChanges();
		}
		private void SaveChanges()
		{
			_dbSubmission.UserId = _userContext.User.Id;
			_dbSubmission.SaveChanges();
			_dbSubmission.UserId = null;
		}
		#endregion
	}
}