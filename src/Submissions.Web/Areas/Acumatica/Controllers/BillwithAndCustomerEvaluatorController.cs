﻿using GLI.Accounting.Acumatica.Interfaces;
using GLI.Accounting.Acumatica.Models.AcumaticaBWAC;
using GLI.EFCore.Submission;
using Submissions.Common;
using Submissions.Web.Areas.Acumatica.Models;
using System.Linq;
using System.Web.Mvc;

namespace Submissions.Web.Areas.Acumatica.Controllers
{
	public class BillwithAndCustomerEvaluatorController : Controller
	{
		private readonly IAcumaticaService _acumaticaService;
		private readonly SubmissionContext _dbSubmissions;

		public BillwithAndCustomerEvaluatorController(IAcumaticaService acumaticaService, SubmissionContext dbSubmissions)
		{
			_acumaticaService = acumaticaService;
			_dbSubmissions = dbSubmissions;
		}

		public ActionResult Index() => View(new BillwithAndCustomerEvaluatorIndexViewModel());

		[HttpPost]
		public ContentResult GetEvaluatedBWACModuleResult(BWACModuleDTO seed)
		{
			seed.PerformingCompany = _dbSubmissions.tbl_lst_Company.Where(x => x.Id == seed.PerformingCompanyId).Select(x => x.DynamicsCompanyId).SingleOrDefault();
			seed.OwningCompany = _dbSubmissions.tbl_lst_Company.Where(x => x.Id == seed.OwningCompanyId).Select(x => x.DynamicsCompanyId).SingleOrDefault();
			seed.Jurisdiction = seed.Jurisdiction.ToJurisdictionIdString().PadLeft(3, '0');
			seed.TransferJurisdiction = string.IsNullOrWhiteSpace(seed.TransferJurisdiction) ? null : seed.TransferJurisdiction.ToJurisdictionIdString().PadLeft(3, '0');
			seed.Sequence = seed.Sequence.PadLeft(3, '0');

			return Content(ComUtil.JsonEncodeCamelCase(_acumaticaService.GetEvaluatedBWACModuleResult(seed)), "application/json");
		}
	}
}