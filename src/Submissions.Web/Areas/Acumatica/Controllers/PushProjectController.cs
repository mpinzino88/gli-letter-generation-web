﻿using GLI.Accounting.Acumatica.Interfaces;
using GLI.EFCore.Submission;
using Newtonsoft.Json;
using Submissions.Common;
using Submissions.Web.Areas.Acumatica.Models;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace Submissions.Web.Areas.Acumatica.Controllers
{
	public class PushProjectController : Controller
	{
		private readonly IUserContext _userContext;
		private readonly SubmissionContext _dbSubmission;
		private readonly IAcumaticaService _acumaticaService;

		public PushProjectController(IAcumaticaService acumaticaService, SubmissionContext dbSubmission, IUserContext userContext)
		{
			_acumaticaService = acumaticaService;
			_dbSubmission = dbSubmission;
			_userContext = userContext;
		}

		#region MVC Actions
		public ActionResult Index()
		{
			var result = new PushProjectViewModel();

			if (_userContext.User.Id == 1031)
				result.Debug = true;

			return View(result);
		}
		#endregion

		#region API
		[HttpPost]
		public ContentResult PushJurisdictions(List<int> jurisdictionIds, string destinationTenant)
		{
			var result = new List<dynamic>();
			var success = new List<int>();
			var destinationCompanyCode = _acumaticaService.GetCompanyCodeFromAcumaticaTenant(destinationTenant);
			var fileNumber = _dbSubmission.JurisdictionalData.Where(x => x.Id == jurisdictionIds.FirstOrDefault())?.FirstOrDefault()?.Submission.FileNumber;

			foreach (var jurisdictionId in jurisdictionIds)
			{
				var toPush = _acumaticaService.SetupAndCreateJurisdictionProject(jurisdictionId, destinationCompanyCode);

				if (toPush.Response.IsSuccessStatusCode)
				{
					var filterParams = @"$filter=ProjectID eq '" + toPush.ProjectDTO.AcumaticaProject.ProjectId.Value + @"'";
					var getResponse = _acumaticaService.GetProject(filterParams, destinationCompanyCode);
					dynamic content = JsonConvert.DeserializeObject(getResponse.Content.ReadAsStringAsync().Result);

					result.Add(new { success = true, acumaticaId = content[0].id.ToString(), billWith = toPush.ProjectDTO.AcumaticaProject.ProjectId.Value });
					success.Add(jurisdictionId);
				}
				else
				{
					result.Add(new { success = false, billWith = toPush.ProjectDTO.AcumaticaProject.ProjectId.Value });
				}
			}

			UpdateJDSuperSetPerFileNumberSentToAcumatica(success, fileNumber);

			return Content(ComUtil.JsonEncodeCamelCase(new { Created = result, Error = false, ErrorMessage = "" }), "application/json");
		}
		[HttpPost]
		public ContentResult GetPushProjectGridModel(string fileNumber)
		{
			return Content(ComUtil.JsonEncodeCamelCase(BuildPushProjectGridModel(fileNumber)), "application/json");
		}
		[HttpPost]
		public ContentResult GetJsonModel(int jurisdictionId, string destinationTenant)
		{
			var model = GLI.Accounting.Acumatica.Common
				.GetJSONFromAcumaticaDTO(_acumaticaService.SetupJurisdictionProject(jurisdictionId, _acumaticaService.GetCompanyCodeFromAcumaticaTenant(destinationTenant)));

			return Content(ComUtil.JsonEncodeCamelCase(model), "application/json");
		}
		#endregion

		#region Model Builders
		private PushProjectGridModel BuildPushProjectGridModel(string fileNumber)
		{
			var result = new PushProjectGridModel();
			if (!string.IsNullOrWhiteSpace(fileNumber))
			{
				result.JurisdictionProjectRows = _dbSubmission.JurisdictionalData
					.Where(x => x.Submission.FileNumber == fileNumber)
					.AsEnumerable()
					.GroupBy(x => x.JurisdictionId)
					.Select(x => new PushJurisdictionProjectRow
					{
						JurisdictionId = x.Key,
						JurisdictionalDataId = x.Max(y => y.Id),
						JurisdictionName = x.Max(y => y.Jurisdiction.Name)
					})
					.ToList();

				result.JurisdictionProjectRows.ForEach(x => x.BillWith = _acumaticaService.GetBillWithFromFileNumber(fileNumber + "-" + x.JurisdictionId));
			}
			return result;
		}
		#endregion

		#region Helpers
		private void UpdateJDSuperSetPerFileNumberSentToAcumatica(List<int> jdPrimarySubSet, string fileNumber)
		{
			var jurisdictionIDs = _dbSubmission.JurisdictionalData.Where(x => jdPrimarySubSet.Contains(x.Id)).Select(x => x.JurisdictionId).ToList();
			var jurisdictionalDatas = _dbSubmission.Submissions.Where(x => x.FileNumber == fileNumber).SelectMany(x => x.JurisdictionalData).ToList();
			var jdSuperSet = jurisdictionalDatas.Where(x => jurisdictionIDs.Contains(x.JurisdictionId)).ToList();
			jdSuperSet.ForEach(x => x.SentToAcumatica = 1);

			SubmissionsSaveChanges(_userContext.User.Id);
		}
		private void SubmissionsSaveChanges(int userId)
		{
			_dbSubmission.UserId = userId;
			_dbSubmission.SaveChanges();
			_dbSubmission.UserId = null;
		}
		#endregion
	}
}