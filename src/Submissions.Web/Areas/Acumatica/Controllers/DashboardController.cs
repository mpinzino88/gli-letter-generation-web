﻿using GLI.Accounting.Acumatica.Interfaces;
using Submissions.Common;
using Submissions.Web.Areas.Acumatica.Models;
using System.Web.Configuration;
using System.Web.Mvc;

namespace Submissions.Web.Areas.Acumatica.Controllers
{
	public class DashboardController : Controller
	{
		private readonly IUserContext _userContext;
		private readonly IAcumaticaService _acumaticaService;

		public DashboardController(IAcumaticaService acumaticaService, IUserContext userContext)
		{
			_acumaticaService = acumaticaService;
			_userContext = userContext;
		}

		#region MVC Actions
		public ActionResult Index()
		{
			return View(BuildAcumaticaDashboardViewModel());
		}
		#endregion

		#region API
		[HttpPost]
		public ContentResult GetAcumaticaDashboardViewModel()
		{
			return Content(ComUtil.JsonEncodeCamelCase(new { AcumaticaDashboardViewModel = BuildAcumaticaDashboardViewModel() }), "application/json");
		}
		[HttpPost]
		public ContentResult GetAcumaticaPendingJurisdictionalDataCount()
		{
			return Content(ComUtil.JsonEncodeCamelCase(new { AcumaticaPendingJurisdictionalDataCount = GetAcumaticaPendingJurisdictionalDataCountFromDB() }), "application/json");
		}
		[HttpPost]
		public ContentResult GetAcumaticaPendingProjectsCount()
		{
			return Content(ComUtil.JsonEncodeCamelCase(new { AcumaticaPendingProjectsCount = GetAcumaticaPendingProjectsCountFromDB() }), "application/json");
		}
		[HttpPost]
		public ContentResult GetAcumaticaProjectCreationErrorCount()
		{
			return Content(ComUtil.JsonEncodeCamelCase(new { AcumaticaProjectCreationErrorCount = GetAcumaticaProjectCreationErrorCountFromDB() }), "application/json");
		}
		[HttpPost]
		public ContentResult GetAcumaticaTaskErrorCount()
		{
			return Content(ComUtil.JsonEncodeCamelCase(new { AcumaticaTaskErrorCount = GetAcumaticaTaskErrorCountFromDB() }), "application/json");
		}

		[HttpPost]
		public ContentResult ControlService(AcumaticaDashboardServiceControl payload)
		{
			var result = _acumaticaService.ControlAcumaticaService(payload.ServiceName, payload.Action);
			return Content(ComUtil.JsonEncodeCamelCase(result), "application/json");
		}
		#endregion

		#region Queries
		private int GetAcumaticaPendingProjectsCountFromDB()
		{
			return _acumaticaService.GetAcumaticaPendingProjectsCount();
		}
		private int GetAcumaticaTaskErrorCountFromDB()
		{
			return _acumaticaService.GetAcumaticaTaskErrorCount();
		}
		private int GetAcumaticaPendingProjectTasksCountFromDB()
		{
			return _acumaticaService.GetAcumaticaPendingtrTaskCount();
		}
		private int GetAcumaticaProjectCreationErrorCountFromDB()
		{
			return _acumaticaService.GetAcumaticaProjectCreationErrorCount();
		}
		private int GetAcumaticaPendingJurisdictionalDataCountFromDB()
		{
			return _acumaticaService.GetAcumaticaPendingJurisdictionalDataCount();
		}
		#endregion

		#region Model Builders
		private AcumaticaDashboardViewModel BuildAcumaticaDashboardViewModel()
		{
			return new AcumaticaDashboardViewModel()
			{
				AcumaticaProjectCreationErrorCount = GetAcumaticaProjectCreationErrorCountFromDB(),
				AcumaticaTaskErrorCount = GetAcumaticaTaskErrorCountFromDB(),
				AcumaticaPendingJurisdictionalDataCount = GetAcumaticaPendingJurisdictionalDataCountFromDB(),
				AcumaticaPendingProjectsCount = GetAcumaticaPendingProjectsCountFromDB(),
				AcumaticaPendingProjectTasksCount = GetAcumaticaPendingProjectTasksCountFromDB(),
				AcumaticaProjectCreationServiceStatus = _acumaticaService.GetStatusOfAcumaticaService(WebConfigurationManager.AppSettings["Environment"] == "Production" ? "AcumaticaProjectCreationService" : "DEVELOPMENTAcumaticaProjectCreationService"),
				AcumaticaProjectTaskCreationServiceStatus = _acumaticaService.GetStatusOfAcumaticaService(WebConfigurationManager.AppSettings["Environment"] == "Production" ? "AcumaticaProjectTaskCreationService" : "DEVELOPMENTAcumaticaProjectTaskCreationService"),
				NJServices2Status = _acumaticaService.GetNJServices2Status(),
				Environment = WebConfigurationManager.AppSettings["Environment"],
				Debug = _userContext.User.Id == 1031
			};
		}
		#endregion
	}

	public class AcumaticaDashboardServiceControl
	{
		public string Action { get; set; }
		public string ServiceName { get; set; }
	}
}