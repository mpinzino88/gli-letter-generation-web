﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Submissions.Web.Areas.PDFManager.Models
{
	public class GPSDocumentsViewModel
	{
		public GPSDocumentsViewModel()
		{
			this.FileAttributesGPSVersions = new List<ColumnNames>();
		}

		public GPSDocumentsViewModel(IList<ColumnNames> fileAttributes, string filenumberStr, string idNumber, string version, string position, string gameName, string jurisdictionName, string jurisdictionType)
		{
			this.FileAttributes = fileAttributes;
			this.FileNumberStr = filenumberStr;
			this.IdNumber = idNumber;
			this.Version = version;
			this.Position = position;
			this.GameName = gameName;
			this.JurisdictionName = jurisdictionName;
			this.JurisdictionType = jurisdictionType;
			this.FileAttributesGPSVersions = new List<ColumnNames>();
		}

		public IList<ColumnNames> FileAttributes { get; set; }
		public IList<ColumnNames> FileAttributesGPSVersions { get; set; }

		[DisplayName("File Number"), Required]
		public string FileNumberStr { get; set; }
		public string IdNumber { get; set; }
		public string Version { get; set; }
		public string Position { get; set; }
		public string GameName { get; set; }
		public int JurisdictionDataId { get; set; }
		public string JurisdictionName { get; set; }
		public string JurisdictionType { get; set; }
		public DateTime? JurisdictionCloseDate { get; set; }
		public DateTime? JurisdictionUpdateDate { get; set; }
		public bool CanDelete { get; set; }
	}

	public class ColumnNames
	{
		public string FilePath { get; set; }
		public string Location { get; set; }
		public string LetterType { get; set; }
		public DateTime? LetterDate { get; set; }
		public string Language { get; set; }
		public int GPSId { get; set; }
		public int? SubmissionLetterId { get; set; }
		public int? DraftLetterId { get; set; }
		public SourceType SourceType { get; set; }
	}

	public enum SourceType
	{
		MainCertification,
		SupplementLetter,
		DraftLetter
	}
}