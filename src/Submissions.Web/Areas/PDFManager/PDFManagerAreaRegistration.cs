﻿using System.Web.Mvc;

namespace Submissions.Web.Areas.PDFManager {
	public class PDFManagerAreaRegistration : AreaRegistration {
		public override string AreaName {
			get {
				return "PDFManager";
			}
		}

		public override void RegisterArea(AreaRegistrationContext context) {
			context.MapRoute(
				"PDFManager_default",
				"PDFManager/{controller}/{action}/{id}",
				new { controller = "PDFManager", action = "Index", id = UrlParameter.Optional }
			);
		}
	}
}