﻿extern alias ZEFCore;
using GLI.EFCore.Submission;
using Submissions.Common;
using Submissions.Web.Areas.PDFManager.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using ZEFCore::Z.EntityFramework.Plus;

namespace Submissions.Web.Areas.PDFManager.Controllers
{
	public class GPSDocumentsController : Controller
	{
		private readonly SubmissionContext _dbSubmission;
		private readonly IUserContext _userContext;

		public GPSDocumentsController(SubmissionContext dbSubmission, IUserContext userContext)
		{
			_dbSubmission = dbSubmission;
			_userContext = userContext;
		}

		public ActionResult IndexNew(int jurisdictionalDataId)
		{
			var jurisdictionalData = (
				from j in _dbSubmission.JurisdictionalData
				where j.Id == jurisdictionalDataId
				select new GPSDocumentsViewModel()
				{
					FileNumberStr = j.Submission.FileNumber,
					GameName = j.Submission.GameName,
					Version = j.Submission.Version,
					Position = j.Submission.Position,
					IdNumber = j.Submission.IdNumber,
					JurisdictionDataId = j.Id,
					JurisdictionName = j.JurisdictionName,
					JurisdictionType = j.Status,
					JurisdictionCloseDate = j.CloseDate,
					JurisdictionUpdateDate = j.UpdateDate
				}
				).SingleOrDefault();

			//main PDf
			var initialQuery = (
				from jurData in _dbSubmission.JurisdictionalData
				where (((!string.IsNullOrEmpty(jurData.PdfPath)) || (!string.IsNullOrEmpty(jurData.JurisdictionGPSPDF))) && (jurData.Id == jurisdictionalDataId))
				select jurData
			).ToList();

			var secondQuery = (
				from subLetter in _dbSubmission.SubmissionLetters
				join letter in _dbSubmission.Letters on subLetter.LetterId equals letter.Id
				join languageTbl in _dbSubmission.tbl_lst_Language on letter.LanguageId equals languageTbl.Id
				where subLetter.JurisdictionalDataId == jurisdictionalDataId
				select new ColumnNames
				{
					Language = languageTbl.Language,
					FilePath = (!string.IsNullOrEmpty(letter.GPSFilePath)) ? letter.GPSFilePath : letter.PdfPath,
					Location = (!string.IsNullOrEmpty(letter.GPSFilePath)) ? "GPS" : "Web Copy",
					LetterDate = letter.LetterDate,
					LetterType = letter.LetterType,
					SourceType = SourceType.SupplementLetter,
					SubmissionLetterId = subLetter.LetterId,
					DraftLetterId = null
				}
			).ToList();

			var tertiaryQuery = (
				from subDrafts in _dbSubmission.SubmissionDrafts
				join draftLetter in _dbSubmission.DraftLetters on subDrafts.DraftLetterId equals draftLetter.Id
				where subDrafts.JurisdictionDataId == jurisdictionalDataId
				select new ColumnNames
				{
					Language = "",
					FilePath = (!string.IsNullOrEmpty(draftLetter.GPSFilePath)) ? draftLetter.GPSFilePath : draftLetter.FilePath,
					Location = (!string.IsNullOrEmpty(draftLetter.GPSFilePath)) ? "GPS" : "Web Copy",
					LetterDate = draftLetter.DraftDate,
					LetterType = "Draft",
					SourceType = SourceType.DraftLetter,
					SubmissionLetterId = null,
					DraftLetterId = subDrafts.DraftLetterId
				}
			).ToList();

			var quaternaryQuery = _dbSubmission.JurisdictionalData.Where(x => x.Id == jurisdictionalDataId).Select(x => new ColumnNames
			{
				Language = "",
				FilePath = x.PdfPath,
				Location = "Web Copy",
				LetterDate = x.UpdateDate ?? x.CloseDate,
				LetterType = "Report",
				SourceType = SourceType.MainCertification,
				SubmissionLetterId = null,
				DraftLetterId = null
			}).ToList();

			IList<ColumnNames> columnNameQuery;
			if (jurisdictionalData.JurisdictionType == "NU" || jurisdictionalData.JurisdictionType == "RV")
			{
				if (jurisdictionalData.JurisdictionType == "NU")
				{
					// use obsoleted date
					columnNameQuery = initialQuery.Select(x => new ColumnNames
					{
						Language = "",
						FilePath = (!string.IsNullOrEmpty(x.JurisdictionGPSPDF)) ? x.JurisdictionGPSPDF : x.PdfPath,
						Location = (!string.IsNullOrEmpty(x.JurisdictionGPSPDF)) ? "GPS" : "Web Copy",
						LetterDate = (x.UpdateDate == null) ? x.CloseDate : x.UpdateDate, // as per phyllis sub-1847 to match pdf date
						LetterType = "Report",
						SourceType = SourceType.MainCertification,
						SubmissionLetterId = null,
						DraftLetterId = null
					}).ToList();
				}
				else
				{
					// use RV date
					columnNameQuery = initialQuery.Select(x => new ColumnNames
					{
						Language = "",
						FilePath = (!string.IsNullOrEmpty(x.JurisdictionGPSPDF)) ? x.JurisdictionGPSPDF : x.PdfPath,
						Location = (!string.IsNullOrEmpty(x.JurisdictionGPSPDF)) ? "GPS" : "Web Copy",
						LetterDate = x.RevokeDate,
						LetterType = "Report",
						SourceType = SourceType.MainCertification,
						SubmissionLetterId = null,
						DraftLetterId = null
					}).ToList();
				}
			}
			else
			{
				// use updated date else use close date
				columnNameQuery = initialQuery.Select(x => new ColumnNames
				{
					Language = "",
					FilePath = (!string.IsNullOrEmpty(x.JurisdictionGPSPDF)) ? x.JurisdictionGPSPDF : x.PdfPath,
					Location = (!string.IsNullOrEmpty(x.JurisdictionGPSPDF)) ? "GPS" : "Web Copy",
					LetterDate = x.UpdateDate ?? x.CloseDate,
					LetterType = "Report",
					SourceType = SourceType.MainCertification,
					SubmissionLetterId = null,
					DraftLetterId = null
				}).ToList();
			}

			IEnumerable<ColumnNames> rowQuery;
			if (jurisdictionalData.JurisdictionType == JurisdictionStatus.DR.ToString() || jurisdictionalData.JurisdictionType == JurisdictionStatus.LQ.ToString())
			{
				rowQuery = tertiaryQuery.Concat(secondQuery);
			}
			else
			{
				rowQuery = columnNameQuery.Concat(secondQuery);
			}

			if (jurisdictionalData == null)
			{
				jurisdictionalData = new GPSDocumentsViewModel();
			}
			else
			{
				rowQuery = rowQuery.Concat(quaternaryQuery);
				jurisdictionalData.FileAttributes = rowQuery.Where(x => x.FilePath != null && x.FilePath != string.Empty).Distinct().ToList();
			}

			jurisdictionalData.CanDelete = Util.HasAccess(Permission.Submission, AccessRight.Edit);

			return View("Index", jurisdictionalData);
		}
		public ActionResult Index(string re_KeyTbl, string jurData_Pri)
		{
			int ReKeyTblInt = 0;
			var JurDataPriInt = 0;

			Int32.TryParse(re_KeyTbl, out ReKeyTblInt);
			Int32.TryParse(jurData_Pri, out JurDataPriInt);

			// If the keyTbl is missing, get it
			if ((ReKeyTblInt == 0) && (JurDataPriInt != 0))
			{
				var rowData = (
					from jd in _dbSubmission.JurisdictionalData
					where jd.Id == JurDataPriInt
					select jd
				).SingleOrDefault();

				if (rowData != null)
				{ // found it
					ReKeyTblInt = rowData.SubmissionId ?? 0;
				}
				else
				{    // missing, 0
					ReKeyTblInt = 0;
				}
			}

			GPSDocumentsViewModel finalQuery = null;

			// Find the submission from the keyTbl
			var submission = (
				from s in _dbSubmission.Submissions
				where s.Id == ReKeyTblInt
				select s
			);

			// If we have the jurPri, use it
			if (JurDataPriInt != 0)
			{
				finalQuery = (
					from s in submission
					from j in _dbSubmission.JurisdictionalData
					where s.Id == j.SubmissionId && j.Id == JurDataPriInt
					select new GPSDocumentsViewModel()
					{
						FileNumberStr = s.FileNumber,
						GameName = s.GameName,
						Version = s.Version,
						Position = s.Position,
						IdNumber = s.IdNumber,
						JurisdictionName = j.JurisdictionName,
						JurisdictionType = j.Status,
						JurisdictionCloseDate = j.CloseDate,
						JurisdictionUpdateDate = j.UpdateDate
					}
				).SingleOrDefault();
			}
			else
			{    // or use the fileJuris table
				finalQuery = (
					from s in submission
					from j in _dbSubmission.tbl_lst_fileJuris.Where(fj => fj.Id == s.JurisdictionId).DefaultIfEmpty()
					select new GPSDocumentsViewModel()
					{
						FileNumberStr = s.FileNumber,
						GameName = s.GameName,
						Version = s.Version,
						Position = s.Position,
						IdNumber = s.IdNumber,
						JurisdictionName = j == null ? "" : j.Name,
						JurisdictionType = s.Status
					}
				).SingleOrDefault();
			}

			/*
			 * Get the Rows for the Table 
			 *
			 */
			IEnumerable<ColumnNames> rowQuery;

			if (JurDataPriInt != 0)
			{
				var initialQuery = (
					from jurData in _dbSubmission.JurisdictionalData
					where (((!string.IsNullOrEmpty(jurData.PdfPath)) || (!string.IsNullOrEmpty(jurData.JurisdictionGPSPDF))) && (jurData.Id == JurDataPriInt))
					select jurData
				).ToList();

				var secondQuery = (
					from subLetter in _dbSubmission.SubmissionLetters
					join letter in _dbSubmission.Letters on subLetter.LetterId equals letter.Id
					join languageTbl in _dbSubmission.tbl_lst_Language on letter.LanguageId equals languageTbl.Id
					where (subLetter.SubmissionId == ReKeyTblInt) && (subLetter.JurisdictionalDataId == JurDataPriInt)
					select new ColumnNames
					{
						Language = languageTbl.Language,
						FilePath = (!string.IsNullOrEmpty(letter.GPSFilePath)) ? letter.GPSFilePath : letter.PdfPath,
						Location = (!string.IsNullOrEmpty(letter.GPSFilePath)) ? "GPS" : "Web Copy",
						LetterDate = letter.LetterDate,
						LetterType = letter.LetterType
					}
				).ToList();

				var tertiaryQuery = (
					from subDrafts in _dbSubmission.SubmissionDrafts
					where (subDrafts.SubmissionId == ReKeyTblInt) && (subDrafts.JurisdictionDataId == JurDataPriInt)
					from draftLetter in _dbSubmission.DraftLetters.Where(draft => draft.Id == subDrafts.DraftLetterId).DefaultIfEmpty()
					select new ColumnNames
					{
						Language = "",
						FilePath = (!string.IsNullOrEmpty(draftLetter.GPSFilePath)) ? draftLetter.GPSFilePath : draftLetter.FilePath,
						Location = (!string.IsNullOrEmpty(draftLetter.GPSFilePath)) ? "GPS" : "Web Copy",
						LetterDate = draftLetter.DraftDate,
						LetterType = "Draft"
					}
				).ToList();

				IList<ColumnNames> columnNameQuery;

				if (finalQuery.JurisdictionType == "NU" || finalQuery.JurisdictionType == "RV")
				{
					if (finalQuery.JurisdictionType == "NU")
					{
						// use obsoleted date
						columnNameQuery = initialQuery.Select(x => new ColumnNames
						{
							Language = "",
							FilePath = (!string.IsNullOrEmpty(x.JurisdictionGPSPDF)) ? x.JurisdictionGPSPDF : x.PdfPath,
							Location = (!string.IsNullOrEmpty(x.JurisdictionGPSPDF)) ? "GPS" : "Web Copy",
							LetterDate = (x.UpdateDate == null) ? x.CloseDate : x.UpdateDate, // as per phyllis sub-1847 to match pdf date
							LetterType = "Report"
						}).ToList();
					}
					else
					{
						// use RV date
						columnNameQuery = initialQuery.Select(x => new ColumnNames
						{
							Language = "",
							FilePath = (!string.IsNullOrEmpty(x.JurisdictionGPSPDF)) ? x.JurisdictionGPSPDF : x.PdfPath,
							Location = (!string.IsNullOrEmpty(x.JurisdictionGPSPDF)) ? "GPS" : "Web Copy",
							LetterDate = x.RevokeDate,
							LetterType = "Report"
						}).ToList();
					}
				}
				else
				{
					// use updated date else use close date
					columnNameQuery = initialQuery.Select(x => new ColumnNames
					{
						Language = "",
						FilePath = (!string.IsNullOrEmpty(x.JurisdictionGPSPDF)) ? x.JurisdictionGPSPDF : x.PdfPath,
						Location = (!string.IsNullOrEmpty(x.JurisdictionGPSPDF)) ? "GPS" : "Web Copy",
						LetterDate = x.UpdateDate ?? x.CloseDate,
						LetterType = "Report"
					}).ToList();
				}

				if (finalQuery.JurisdictionType == "DR" || finalQuery.JurisdictionType == JurisdictionStatus.LQ.ToString())
				{
					rowQuery = tertiaryQuery.Concat(secondQuery);
				}
				else
				{
					rowQuery = columnNameQuery.Concat(secondQuery);
				}
			}
			else
			{
				var initialQuery = (
					from subm in _dbSubmission.Submissions
					where (((!string.IsNullOrEmpty(subm.SubmissionPDF)) || (!string.IsNullOrEmpty(subm.SubmissionGPSPDF))) && (subm.Id == ReKeyTblInt))
					select new ColumnNames
					{
						Language = "",
						FilePath = (!string.IsNullOrEmpty(subm.SubmissionGPSPDF)) ? subm.SubmissionGPSPDF : subm.SubmissionPDF,
						Location = (!string.IsNullOrEmpty(subm.SubmissionGPSPDF)) ? "GPS" : "Web Copy",
						LetterDate = subm.ReceiveDate, // need to add an updated date to submissions table
						LetterType = "Report"
					}
				).ToList();

				var secondQuery = (
					from subLetter in _dbSubmission.SubmissionLetters
					where (subLetter.SubmissionId == ReKeyTblInt) && (subLetter.JurisdictionalDataId == null)
					from letter in _dbSubmission.Letters.Where(lett => lett.Id == subLetter.LetterId).DefaultIfEmpty()
					from subm in _dbSubmission.Submissions.Where(sub => sub.Id == subLetter.SubmissionId).DefaultIfEmpty()
					from fileManu in _dbSubmission.tbl_lst_fileManu.Where(manu => manu.Code == subm.ManufacturerCode).DefaultIfEmpty()
					from languageTbl in _dbSubmission.tbl_lst_Language.Where(language => language.Id == letter.LanguageId)
					select new ColumnNames
					{
						Language = languageTbl.Language,
						FilePath = (letter.GpsFileId != null) ? letter.GPSFilePath : letter.PdfPath,
						Location = (letter.GpsFileId != null) ? "GPS" : "Web Copy",
						LetterDate = letter.LetterDate,
						LetterType = letter.LetterType
					}
				).ToList();

				rowQuery = initialQuery.Union(secondQuery);
			}

			if (finalQuery == null)
			{
				finalQuery = new GPSDocumentsViewModel();
			}
			else
			{
				finalQuery.FileAttributes = rowQuery.Distinct().ToList();
			}

			return View(finalQuery);
		}

		public ActionResult OpenDoc(string filePath)
		{
			TempData["File"] = filePath;

			return RedirectToAction("index", "documentviewer", new { area = "" });
		}

		[AuthorizeUser(Permission = Permission.Submission, HasAccessRight = AccessRight.Edit)]
		public ActionResult DeleteDocument(int jurisdictionalDataId, string sourceType, string filePath, int? subLetterId, int? draftLetterId)
		{
			if (sourceType == SourceType.MainCertification.ToString())
			{
				var jurisdictionData = _dbSubmission.JurisdictionalData.Where(x => x.Id == jurisdictionalDataId).Single();
				jurisdictionData.PdfPath = null;
				jurisdictionData.JurisdictionGPSPDF = null;
				_dbSubmission.Entry(jurisdictionData).Property(x => x.PdfPath).IsModified = true;
				_dbSubmission.Entry(jurisdictionData).Property(x => x.JurisdictionGPSPDF).IsModified = true;
			}
			else if (sourceType == SourceType.SupplementLetter.ToString())
			{
				_dbSubmission.SubmissionLetters.Where(x => x.JurisdictionalDataId == jurisdictionalDataId && x.LetterId == subLetterId).Delete();
				if (!_dbSubmission.SubmissionLetters.Where(x => x.LetterId == subLetterId).Any())
				{
					_dbSubmission.Letters.Where(x => x.Id == subLetterId).Delete();
				}
			}
			else if (sourceType == SourceType.DraftLetter.ToString())
			{
				_dbSubmission.SubmissionDrafts.Where(x => x.JurisdictionDataId == jurisdictionalDataId && x.DraftLetterId == draftLetterId).Delete();
				if (!_dbSubmission.SubmissionDrafts.Where(x => x.DraftLetterId == draftLetterId).Any())
				{
					_dbSubmission.DraftLetters.Where(x => x.Id == draftLetterId).Delete();
				}
			}
			_dbSubmission.UserId = _userContext.User.Id;
			_dbSubmission.SaveChanges();

			return RedirectToAction("IndexNew", new { jurisdictionalDataId = jurisdictionalDataId });
		}

	}
}