using EF.Submission;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.DirectoryServices;
using System.Linq;
using System.Security.Principal;
using System.Web;
using System.Web.Mvc;

namespace Submissions.Web
{
	using Submissions.Common;

	public class SessionConfig
	{
		public static void Register()
		{
			var dbUser = LoadUser();

			var userContext = DependencyResolver.Current.GetService<IUserContext>();
			userContext.AccessRights = ConfigureAccessRights(dbUser);
			userContext.DepartmentId = dbUser.DepartmentId ?? 0;
			userContext.Department = (Department)Enum.Parse(typeof(Department), dbUser.Department.Code);
			userContext.LocationId = dbUser.LocationId ?? 0;
			userContext.Location = dbUser.Location.Location;
			userContext.LocationName = dbUser.Location.Name;
			userContext.ManagerId = dbUser.ManagerId;
			userContext.RoleId = dbUser.RoleId;
			userContext.Role = (Role)Enum.Parse(typeof(Role), dbUser.Role.Code);
			userContext.TeamId = dbUser.TeamId;
			userContext.User.Id = dbUser.Id;
			userContext.User.Username = dbUser.Username;
			userContext.User.FName = dbUser.FName;
			userContext.User.LName = dbUser.LName;
			userContext.User.Email = dbUser.Email;
			userContext.User.Environment = DependencyResolver.Current.GetService<IConfig>().Environment;
			userContext.User.Impersonating = false;
		}

		private static trLogin LoadUser()
		{
			var sAMAccountName = HttpContext.Current.User.Identity.Name.Replace("GAMINGLABS\\", "");

			var adSearch = new DirectorySearcher(new DirectoryEntry("LDAP://gaminglabs"));
			adSearch.Filter = "(&(sAMAccountType=805306368)(sAMAccountName=" + sAMAccountName + "))";
			adSearch.PropertiesToLoad.Add("objectSid");

			var adUser = adSearch.FindOne();
			trLogin user = null;
			if (adUser == null)
			{
				throw new Exception("User not found in active directory.");
			}
			else
			{
				var _dbSubmission = DependencyResolver.Current.GetService<ISubmissionContext>();
				var sid = new SecurityIdentifier((byte[])adUser.Properties["objectSid"][0], 0).ToString();

				user = _dbSubmission.trLogins
					.Include(x => x.UserPermissions.Select(y => y.Permission))
					.Where(x => x.LDAP_SID == sid && x.Status == "A")
					.SingleOrDefault();

				if (user == null)
				{
					throw new Exception("User SID not found in trLogin.");
				}
			}

			return user;
		}

		private static Dictionary<Permission, AccessRight> ConfigureAccessRights(trLogin user)
		{
			var accessRights = new Dictionary<Permission, AccessRight>();

			foreach (var permission in user.UserPermissions)
			{
				if (Enum.IsDefined(typeof(Permission), permission.Permission.Code))
				{
					var permissionEnum = (Permission)Enum.Parse(typeof(Permission), permission.Permission.Code);
					var accessRight = (AccessRight)Enum.ToObject(typeof(AccessRight), permission.PermissionValue);
					accessRights.Add(permissionEnum, accessRight);
				}
			}

			return accessRights;
		}
	}
}