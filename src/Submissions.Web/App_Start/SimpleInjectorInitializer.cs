[assembly: WebActivator.PostApplicationStartMethod(typeof(Submissions.Web.App_Start.SimpleInjectorInitializer), "Initialize")]

namespace Submissions.Web.App_Start
{
	using EF.eResults;
	using EF.eResultsManaged;
	using EF.GLIAccess;
	using EF.GLiCloud;
	using EF.GLIExtranet;
	using EF.GPS;
	using EF.LegacyModernized;
	using EF.QANotes;
	using EF.Submission;
	using EvoLogic.GatherData;
	using EvoLogic.SaveData;
	using GLI.EF.LetterContent;
	using GLIMathOperations.GatherMethods;
	using GLIMathOperations.Interfaces;
	using JiraLibrary;
	using SimpleInjector;
	using SimpleInjector.Integration.Web;
	using SimpleInjector.Integration.Web.Mvc;
	using Submissions.Common;
	using Submissions.Common.Email;
	using Submissions.Core;
	using Submissions.Core.Interfaces;
	using Submissions.Core.Interfaces.LetterContent;
	using Submissions.Core.Interfaces.LetterGen;
	using Submissions.Core.Services;
	using Submissions.Core.Services.LetterBook;
	using Submissions.Core.Services.LetterContent;
	using Submissions.Core.Services.LetterGen;
	using Submissions.Web.Areas.PointClick.Services;
	using Submissions.Web.Helpers;
	using System.Reflection;
	using System.Web;
	using System.Web.Configuration;
	using System.Web.Mvc;

	public static class SimpleInjectorInitializer
	{
		/// <summary>Initialize the container and register it as MVC Dependency Resolver.</summary>
		public static void Initialize()
		{
			var container = new Container();
			container.Options.DefaultScopedLifestyle = new WebRequestLifestyle();

			InitializeContainer(container);

			container.RegisterMvcControllers(Assembly.GetExecutingAssembly());
			container.RegisterMvcIntegratedFilterProvider();
			container.Verify();

			DependencyResolver.SetResolver(new SimpleInjectorDependencyResolver(container));
		}

		private static void InitializeContainer(Container container)
		{
			container.Register<HttpContextBase>(() =>
			{
				var context = HttpContext.Current;
				if (context == null && container.IsVerifying) return new FakeHttpContext();
				return new HttpContextWrapper(context);
			}, Lifestyle.Scoped);

			// Singletons
			container.RegisterSingleton<IConfig, Submissions.Common.Config>();
			container.RegisterSingleton<IConfigContext, ConfigContext>();
			container.Register<JiraMethods>(() => new JiraMethods(WebConfigurationManager.AppSettings["jira.baseurl"], WebConfigurationManager.AppSettings["jira.username"], WebConfigurationManager.AppSettings["jira.userpassword"]), Lifestyle.Singleton);

			// Database
			container.Register<IGLIAccessContext, GLIAccessContext>(Lifestyle.Scoped);
			container.Register<IGLIExtranetContext, GLIExtranetContext>(Lifestyle.Scoped);
			container.Register<ISubmissionContext, SubmissionContext>(Lifestyle.Scoped);
			container.Register<IGLiCloudContext, GLiCloudContext>(Lifestyle.Scoped);
			container.Register<IeResultsManagedContext, eResultsManagedContext>(Lifestyle.Scoped);
			container.Register<IQANotesContext, QANotesContext>(Lifestyle.Scoped);
			container.Register<IeResultsContext, eResultsContext>(Lifestyle.Scoped);
			container.Register<IGPSContext, GPSContext>(Lifestyle.Scoped);
			container.Register<ILegacyModernizedContext, LegacyModernizedContext>(Lifestyle.Scoped);
			container.Register<ILetterContentContext, LetterContentContext>(Lifestyle.Scoped);

			// Datebase Core
			container.Register(() => new GLI.EFCore.Submission.SubmissionContext(), Lifestyle.Scoped);

			// Libraries
			container.Register<GLI.Accounting.Acumatica.Interfaces.IAcumaticaService, GLI.Accounting.Acumatica.AcumaticaService>(Lifestyle.Scoped);
			container.Register<GLI.Accounting.Acumatica.Interfaces.IAcumaticaAPIService, GLI.Accounting.Acumatica.AcumaticaAPIService>(Lifestyle.Scoped);
			container.Register<GLI.Accounting.Acumatica.Interfaces.IAcumaticaModelBuilder, GLI.Accounting.Acumatica.AcumaticaModelBuilder>(Lifestyle.Scoped);
			container.Register<GLI.Accounting.Acumatica.Interfaces.IAcumaticaDashboardService, GLI.Accounting.Acumatica.AcumaticaDashboardService>(Lifestyle.Scoped);
			container.Register<IAnswerBaseMethods, EvoLogic.SaveData.AnswerBaseMethods>(Lifestyle.Scoped);
			container.Register<IBulkMethods, BulkMethods>(Lifestyle.Scoped);
			container.Register<IEventMethods, EvoLogic.SaveData.EventMethods>(Lifestyle.Scoped);
			container.Register<IInstanceComponentJoinMethods, EvoLogic.SaveData.InstanceComponentJoinMethods>(Lifestyle.Scoped);
			container.Register<EvoLogic.SaveData.IInstanceCompGroupMethods, EvoLogic.SaveData.InstanceCompGroupMethods>(Lifestyle.Scoped);
			container.Register<IInstanceJurisMethods, EvoLogic.SaveData.InstanceJurisMethods>(Lifestyle.Scoped);
			container.Register<EvoLogic.GatherData.IInstanceCompGroupMethods, EvoLogic.GatherData.InstanceCompGroupMethods>(Lifestyle.Scoped);
			container.Register<IQuestionBaseMethods, EvoLogic.GatherData.QuestionBaseMethods>(Lifestyle.Scoped);
			container.Register<IVersionMethods, EvoLogic.SaveData.VersionMethods>(Lifestyle.Scoped);
			container.Register<IInstanceMethods, EvoLogic.SaveData.InstanceMethods>(Lifestyle.Scoped);

			// Local
			container.Register<Submissions.Common.IUser, Submissions.Common.User>(Lifestyle.Scoped);
			container.Register<IUserContext, UserContext>(Lifestyle.Scoped);
			container.Register<IColumnChooserService, ColumnChooserService>(Lifestyle.Scoped);
			container.Register<ILogger, Logger>(Lifestyle.Scoped);
			container.Register<IEmailService, EmailService>(Lifestyle.Scoped);
			container.Register<IQueryService, QueryService>(Lifestyle.Scoped);
			container.Register<IWidgetService, WidgetService>(Lifestyle.Scoped);

			// Submissions.Core
			container.Register<IAccountingService, AccountingService>(Lifestyle.Scoped);
			container.Register<IActiveDirectoryService, ActiveDirectoryService>(Lifestyle.Scoped);
			container.Register<IActivityLog, ActivityLog>(Lifestyle.Scoped);
			container.Register<ICoversheetGatherMethods, CoversheetMethods>(Lifestyle.Scoped);
			container.Register<ILetterDataUpdateService, LetterDataUpdateService>(Lifestyle.Scoped);
			container.Register<IDocumentDataService, DocumentDataService>(Lifestyle.Scoped);
			container.Register<IDocumentService, DocumentService>(Lifestyle.Scoped);
			container.Register<IElectronicSubmissionService, ElectronicSubmissionService>(Lifestyle.Scoped);
			container.Register<IEngineeringInformationSaveMethods, GLIMathOperations.SaveMethods.EngineeringInformationMethods>(Lifestyle.Scoped);
			container.Register<IEvolutionService, EvolutionService>(Lifestyle.Scoped);
			container.Register<IEvoService, EvoService>(Lifestyle.Scoped);
			container.Register<IGenerateService, GenerateService>(Lifestyle.Scoped);
			container.Register<IGameDescriptionService, GameDescriptionService>(Lifestyle.Scoped);
			container.Register<IGamingGuidelineService, GamingGuidelineService>(Lifestyle.Scoped);
			container.Register<IGPSFileTreeService, GPSFileTreeService>(Lifestyle.Scoped);
			container.Register<IHumanResourcesService, HumanResourcesService>(Lifestyle.Scoped);
			container.Register<IIncomingReviewService, IncomingReviewService>(Lifestyle.Scoped);
			container.Register<IIssueService, IssueService>(Lifestyle.Scoped);
			container.Register<IJiraService, JiraService>(Lifestyle.Scoped);
			container.Register<IJurisdictionalDataService, JurisdictionalDataService>(Lifestyle.Scoped);
			container.Register<ILegacyEresultsService, LegacyEresultsService>(Lifestyle.Scoped);
			container.Register<ILetterbookService, LetterbookService>(Lifestyle.Scoped);
			container.Register<ILetterGenerationService, LetterGenerationService>(Lifestyle.Scoped);
			container.Register<ILettersService, LettersService>(Lifestyle.Scoped);
			container.Register<ILogService, LogService>(Lifestyle.Scoped);
			container.Register<ILotoQuebecExportService, LotoQuebecExportService>(Lifestyle.Scoped);
			container.Register<IMasterFileService, MasterFileService>(Lifestyle.Scoped);
			container.Register<IMathService, MathService>(Lifestyle.Scoped);
			container.Register<IMemoService, MemoService>(Lifestyle.Scoped);
			container.Register<IMiscService, MiscService>(Lifestyle.Scoped);
			container.Register<IMobileDeviceService, MobileDeviceService>(Lifestyle.Scoped);
			container.Register<INoteService, NoteService>(Lifestyle.Scoped);
			container.Register<INotificationService, NotificationService>(Lifestyle.Scoped);
			container.Register<INotificationMonitorService, NotificationMonitorService>(Lifestyle.Scoped);
			container.Register<IPdfService, PdfService>(Lifestyle.Scoped);
			container.Register<IProjectReviewService, ProjectReviewService>(Lifestyle.Scoped);
			container.Register<IProjectService, ProjectService>(Lifestyle.Scoped);
			container.Register<ISharepointService, SharepointService>(Lifestyle.Scoped);
			container.Register<ISignatureService, SignatureService>(Lifestyle.Scoped);
			container.Register<ISubmissionIGTService, SubmissionIGTService>(Lifestyle.Scoped);
			container.Register<ISubmissionService, SubmissionService>(Lifestyle.Scoped);
			container.Register<ISubmissionSCIService, SubmissionSCIService>(Lifestyle.Scoped);
			container.Register<ISubProjectService, SubProjectService>(Lifestyle.Scoped);
			container.Register<ISupplementalLettersService, SupplementalLettersService>(Lifestyle.Scoped);
			container.Register<ITransferIGTService, TransferIGTService>(Lifestyle.Scoped);
			container.Register<ITransferService, TransferService>(Lifestyle.Scoped);
			container.Register<ITransferRequest, TransferRequest>(Lifestyle.Scoped);
			container.Register<IWithdrawRequest, WithdrawRequest>(Lifestyle.Scoped);
			container.Register<IWithdrawRejectService, WithdrawRejectService>(Lifestyle.Scoped);
			container.Register<IWorkloadManagerService, WorkloadManagerService>(Lifestyle.Scoped);
			container.Register<ILetterContentService, LetterContentService>(Lifestyle.Scoped);
			container.Register<ILetterContentSQLService, LetterContentSQLService>(Lifestyle.Scoped);
			container.Register<IProductService, ProductService>(Lifestyle.Scoped);
		}
	}
}