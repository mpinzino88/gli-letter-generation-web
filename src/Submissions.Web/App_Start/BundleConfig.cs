using System.Collections.Generic;
using System.Web;
using System.Web.Optimization;

namespace Submissions.Web
{
	public class BundleConfig
	{
		//This is a workaround for an issue where the relative urls in the css files are not mapping correctly when the root of the site is a virtual
		//directory in IIS.  This corrects the issue by rewriting the url references in the CSS files which corrects the relative urls
		public class CssRewriteUrlTransformWrapper : IItemTransform
		{
			public string Process(string includedVirtualPath, string input)
			{
				return new CssRewriteUrlTransform().Process("~" + VirtualPathUtility.ToAbsolute(includedVirtualPath), input);
			}
		}

		//If your file has a .js file and a .min.js file, just include the .js file.
		//The .min.js file will automatically be included when the project is built in Release mode
		public static void RegisterBundles(BundleCollection bundles)
		{
			//Bundle Scripts
			bundles.Add(new ScriptBundle("~/bundles/script/jquery")
				.Include("~/Scripts/jquery-{version}.js")
				.Include("~/Scripts/jquery-ui-{version}.js")
				.Include("~/Scripts/jquery.validate*"));

			bundles.Add(new ScriptBundle("~/bundles/script/thirdparty")
				.Include("~/Scripts/modernizr-*")
				.Include("~/Scripts/bootstrap.js")
				.Include("~/Scripts/bootbox.js")
				.Include("~/Scripts/moment.js")
				.Include("~/Scripts/typeahead.bundle.js")
				.Include("~/Scripts/bootstrap-datetimepicker.js")
				.Include("~/Scripts/select2.js")
				.Include("~/Scripts/gli/gli.js")
				.ForceOrdered());

			//page specific bundles
			bundles.Add(new ScriptBundle("~/bundles/script/chart")
				.Include("~/Scripts/Chart.js"));

			bundles.Add(new ScriptBundle("~/bundles/script/colorpicker")
				.Include("~/Scripts/bootstrap-colorpicker.js"));

			bundles.Add(new ScriptBundle("~/bundles/script/hopscotch")
					.Include("~/Scripts/hopscotch/hopscotch.js")
					.Include("~/Scripts/hopscotch/tourModels.js")
					.ForceOrdered());

			bundles.Add(new ScriptBundle("~/bundles/script/jqfiletree")
				.Include("~/Scripts/jqueryFileTree.js")
				.ForceOrdered());

			bundles.Add(new ScriptBundle("~/bundles/script/jqgrid")
				.Include("~/Scripts/trirand/i18n/grid.locale-en.js")
				.Include("~/Scripts/trirand/jquery.jqGrid.min.js")
				.Include("~/Scripts/gli/gli-grid.js")
				.ForceOrdered());

			bundles.Add(new ScriptBundle("~/bundles/script/kendo")
				.Include("~/Scripts/kendo/2016.3.1118/kendo.all.min.js")
				.Include("~/Scripts/kendo/2016.3.1118/kendo.aspnetmvc.min.js")
				.ForceOrdered());

			bundles.Add(new ScriptBundle("~/bundles/script/knockout")
				.Include("~/Scripts/knockout-{version}.js")
				.Include("~/Scripts/knockout.mapping-latest.js")
				.Include("~/Scripts/knockout.validation.js")
				.Include("~/Scripts/knockout-sortable.js")
				.Include("~/Scripts/gli/knockout.js")
				.Include("~/Scripts/gli/ko-gli-{version}.js")
				.ForceOrdered());

			bundles.Add(new ScriptBundle("~/bundles/script/ko-documents")
				.Include("~/Scripts/gli/ko-documents.js")
				.ForceOrdered());

			bundles.Add(new ScriptBundle("~/bundles/script/vue")
				.Include("~/Scripts/vue/vue.js")
				.Include("~/Scripts/vue/vue-resource.js")
				.ForceOrdered());

			// vue.js configured via webpack
			bundles.Add(new ScriptBundle("~/bundles/script/vue-vendors")
				.Include("~/Vue/vendors.js"));

			bundles.Add(new ScriptBundle("~/bundles/script/vue-bundle")
				.Include("~/Vue/bundle.js"));

			bundles.Add(new ScriptBundle("~/bundles/script/widget")
				.Include("~/Scripts/gridster/jquery.gridster.js")
				.Include("~/Scripts/gli/widget.js")
				.ForceOrdered());

			bundles.Add(new ScriptBundle("~/bundles/script/letterGen")
				.Include("~/Scripts/gli/letterGen/paging.js")
				.Include("~/Scripts/gli/letterGen/utils.js"));

			//Bundle Styles
			bundles.Add(new StyleBundle("~/bundles/css/css")
				.Include("~/Content/fontawesome/css/all.min.css", new CssRewriteUrlTransformWrapper())
				.Include("~/Content/jquery-ui-bootstrap/jquery-ui-1.10.3.custom.css", new CssRewriteUrlTransformWrapper())
				.Include("~/Content/site.css", new CssRewriteUrlTransformWrapper())
				.Include("~/Content/themes/default/bootstrap.css", new CssRewriteUrlTransformWrapper())
				.Include("~/Content/typeaheadjs.css", new CssRewriteUrlTransformWrapper())
				.Include("~/Content/bootstrap-datetimepicker.css", new CssRewriteUrlTransformWrapper())
				.Include("~/Content/css/select2.css", new CssRewriteUrlTransformWrapper())
				.Include("~/Content/css/select2-bootstrap.css", new CssRewriteUrlTransformWrapper()));

			bundles.Add(new StyleBundle("~/bundles/css/glioverrides")
				.Include("~/Content/gli.overrides.css", new CssRewriteUrlTransformWrapper()));

			//page specific bundles
			bundles.Add(new StyleBundle("~/bundles/css/chart")
				.Include("~/Content/gli/chart.css", new CssRewriteUrlTransformWrapper()));

			bundles.Add(new StyleBundle("~/bundles/css/colorpicker")
				.Include("~/Content/css/bootstrap-colorpicker.css", new CssRewriteUrlTransformWrapper()));

			bundles.Add(new StyleBundle("~/bundles/css/hopscotch")
				.Include("~/Content/hopscotch/hopscotch.css", new CssRewriteUrlTransformWrapper()));

			bundles.Add(new StyleBundle("~/bundles/css/jqfiletree")
				.Include("~/Content/jqueryFileTree.css", new CssRewriteUrlTransformWrapper()));

			bundles.Add(new StyleBundle("~/bundles/css/jqgrid")
				.Include("~/Content/trirand/ui.jqgrid.css", new CssRewriteUrlTransformWrapper())
				.Include("~/Content/trirand/ui.multiselect.css", new CssRewriteUrlTransformWrapper())
				.Include("~/Content/jquery-ui-bootstrap/jqGrid.overrides.css", new CssRewriteUrlTransformWrapper()));

			bundles.Add(new StyleBundle("~/bundles/css/kendo")
				.Include("~/Content/kendo/2016.3.1118/kendo.common-bootstrap.min.css", new CssRewriteUrlTransformWrapper())
				.Include("~/Content/kendo/2016.3.1118/kendo.bootstrap.min.css", new CssRewriteUrlTransformWrapper()));

			bundles.Add(new StyleBundle("~/bundles/css/widget")
				.Include("~/Content/gridster/jquery.gridster.css", new CssRewriteUrlTransformWrapper()));

			bundles.Add(new StyleBundle("~/bundles/css/letterGeneration")
				.Include("~/Content/gli/LetterGeneration.css", new CssRewriteUrlTransformWrapper()));

		}
	}

	internal static class BundleExtensions
	{
		public static Bundle ForceOrdered(this Bundle sb)
		{
			sb.Orderer = new AsIsBundleOrderer();
			return sb;
		}
	}

	internal class AsIsBundleOrderer : IBundleOrderer
	{
		public virtual IEnumerable<BundleFile> OrderFiles(BundleContext context, IEnumerable<BundleFile> files)
		{
			return files;
		}
	}
}