const path = require('path');

function resolve() {
	return path.join(__dirname, '../Vue/');
}

console.log("resolve", resolve());

module.exports = {
    stories: ['../Vue/components/**/*.stories.@(ts|js)'],
    addons: [
        "@storybook/addon-links",
        "@storybook/addon-essentials",
        {
            name: '@storybook/addon-docs',
            options: {
              vueDocgenOptions: {
                alias: {
                  '@': resolve(),
                },
              },
            },
          },
    ],
    webpackFinal: async (config, { configType }) => {
        // `configType` has a value of 'DEVELOPMENT' or 'PRODUCTION'
        // You can change the configuration based on that.
        // 'PRODUCTION' is used when building the static version of storybook.
    
        // Make whatever fine-grained changes you need
        config.module.rules.push({
            test: /\.s[ac]ss$/,
            use: [
                'vue-style-loader',
                'css-loader',
                // Compiles Sass to CSS
                'sass-loader',
            ],
        });

        config.resolve = {
            ...config.resolve,
            alias: {
                ...config.resolve.alias,
                'vue$': 'vue/dist/vue.esm.js',
                '@': resolve()
            }
        }
    
        // Return the altered config
        return config;
    },
};