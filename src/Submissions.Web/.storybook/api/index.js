import Axios from 'axios';
import MockAdapter from 'axios-mock-adapter';

import { paginateData } from "./mock/manufacturerData";

const axios = Axios.create({
	baseURL: jsPath,
	headers: {
		'Cache-Control': 'no-cache',
		'Cache-control': 'no-store',
		'Pragma': 'no-cache',
		'Expires': '0'
	}
})

// delayResponse is set to time it takes for me to load all manufacturers.
// Other lookups, like fileNumber, can take much longer.
const mock = new MockAdapter(axios, { delayResponse: 180 });

// Mocked API calls to enable testing
// Base Lookup
const lookupUri = `LookupsAjax/`;
const lookupRegex = new RegExp(`${jsPath}${lookupUri}?[^\/?]+\/?.*`)

console.log(lookupRegex);
mock.onGet(lookupRegex)
	.reply(config => {
		console.log(config.params);
		const data = paginateData(config.params.searchTerm, config.params.pageSize, config.params.pageNum);
		return [200, data];
	});

export default axios;