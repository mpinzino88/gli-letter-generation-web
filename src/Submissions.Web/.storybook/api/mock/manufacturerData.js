export const testDataSet = [
    {
        "id": null,
        "text": "",
        "deprecated": false
    },
    {
        "id": "07G",
        "text": "Zero Seven Gaming Limited",
        "deprecated": false
    },
    {
        "id": "10B",
        "text": "10Bet",
        "deprecated": false
    },
    {
        "id": "11M",
        "text": "11 Manitoba Liquor and Gaming Authority (non-billable)",
        "deprecated": false
    },
    {
        "id": "16G",
        "text": "1609 Group (Pty) Ltd",
        "deprecated": false
    },
    {
        "id": "18G",
        "text": "18 Gaming LLC",
        "deprecated": false
    },
    {
        "id": "1LV",
        "text": "oneLIVE Inc.",
        "deprecated": false
    },
    {
        "id": "1NI",
        "text": "One North Interactive LLc",
        "deprecated": false
    },
    {
        "id": "21H",
        "text": "21HeadsUP",
        "deprecated": false
    },
    {
        "id": "21I",
        "text": "21 Insure",
        "deprecated": false
    },
    {
        "id": "247",
        "text": "24/7 Lets Play Triple Spin",
        "deprecated": false
    },
    {
        "id": "2MS",
        "text": "2112 Media \u0026 Sports Group LLP",
        "deprecated": false
    },
    {
        "id": "3DC",
        "text": "3D+Corp",
        "deprecated": false
    },
    {
        "id": "3GS",
        "text": "3G Studios Inc",
        "deprecated": false
    },
    {
        "id": "3JG",
        "text": "3JGaming LLC",
        "deprecated": false
    },
    {
        "id": "3PG",
        "text": "3 Point Gaming (Pty) Ltd T/A Apex Gaming Africa",
        "deprecated": false
    },
    {
        "id": "3RC",
        "text": "3 Rivers Casino",
        "deprecated": false
    },
    {
        "id": "46S",
        "text": "46 To Shinjuku medialab S.L.",
        "deprecated": false
    },
    {
        "id": "4DG",
        "text": "4-D Gaming LLC",
        "deprecated": false
    },
    {
        "id": "4LL",
        "text": "4 Leaf Lotto LLC",
        "deprecated": false
    },
    {
        "id": "50C",
        "text": "AscendFS Canada, Ltd.",
        "deprecated": false
    },
    {
        "id": "5CG",
        "text": "Five Cedars Group, Inc.",
        "deprecated": false
    },
    {
        "id": "5ST",
        "text": "Fifth Street Gaming",
        "deprecated": false
    },
    {
        "id": "714",
        "text": "7-14-21",
        "deprecated": false
    },
    {
        "id": "7CR",
        "text": "7 Cedars Resort",
        "deprecated": false
    },
    {
        "id": "7GN",
        "text": "7 Generations",
        "deprecated": false
    },
    {
        "id": "888",
        "text": "888 Spain Plc",
        "deprecated": false
    },
    {
        "id": "8HO",
        "text": "888 Holdings PLC",
        "deprecated": false
    },
    {
        "id": "8ID",
        "text": "Eight Integrated Development Corporation dba Royce Hotel and Casino",
        "deprecated": false
    },
    {
        "id": "8US",
        "text": "888 US Ltd",
        "deprecated": false
    },
    {
        "id": "93C",
        "text": "93 Connect Sdn. Bhd.",
        "deprecated": false
    },
    {
        "id": "963",
        "text": "9634517 Canada Corp.",
        "deprecated": false
    },
    {
        "id": "AAC",
        "text": "American Amusements Co.",
        "deprecated": false
    },
    {
        "id": "AAF",
        "text": "Aruze Gaming Africa (Pty) Ltd",
        "deprecated": false
    },
    {
        "id": "AAG",
        "text": "Ambra Gaming",
        "deprecated": false
    },
    {
        "id": "AAH",
        "text": "Alpha Allied Holdings Limited",
        "deprecated": false
    },
    {
        "id": "AAT",
        "text": "Avantime Amusement Technology",
        "deprecated": false
    },
    {
        "id": "AAY",
        "text": "The Stars Group Inc.",
        "deprecated": false
    },
    {
        "id": "AB1",
        "text": "Argentina Buenos Aires (non-billable)",
        "deprecated": false
    },
    {
        "id": "ABA",
        "text": "A Bet A Technology Limited",
        "deprecated": false
    },
    {
        "id": "ABB",
        "text": "Abbiati Casino Equipment SNC",
        "deprecated": false
    },
    {
        "id": "ABC",
        "text": "Amelia Bell Casino",
        "deprecated": false
    },
    {
        "id": "ABE",
        "text": "Aberrant Software",
        "deprecated": false
    },
    {
        "id": "ABG",
        "text": "M.A.G. Elettronica s.r.l.",
        "deprecated": false
    },
    {
        "id": "ABH",
        "text": "Ameristar Black Hawk Casino",
        "deprecated": false
    },
    {
        "id": "ABI",
        "text": "ACRES BONUSING INC",
        "deprecated": false
    },
    {
        "id": "ABL",
        "text": "Alamo Bingo LLC",
        "deprecated": false
    },
    {
        "id": "ABM",
        "text": "ABM GAMES",
        "deprecated": false
    },
    {
        "id": "ABS",
        "text": "Absentee Shawnee Tribe Gaming Commission",
        "deprecated": false
    },
    {
        "id": "ABT",
        "text": "A Better Internet Ltd",
        "deprecated": false
    },
    {
        "id": "ABU",
        "text": "Abuan Gaming LLC",
        "deprecated": false
    },
    {
        "id": "ABV",
        "text": "Alrecrea B.V.",
        "deprecated": false
    },
    {
        "id": "AC1",
        "text": "Argentina Cordoba (non-billable)",
        "deprecated": false
    },
    {
        "id": "AC3",
        "text": "Acres Manufacturing Company",
        "deprecated": false
    },
    {
        "id": "ACA",
        "text": "Automated Cashier",
        "deprecated": false
    },
    {
        "id": "ACB",
        "text": "Airport Casino Basel",
        "deprecated": false
    },
    {
        "id": "ACC",
        "text": "Ameristar Casino Council",
        "deprecated": false
    },
    {
        "id": "ACD",
        "text": "Accredia",
        "deprecated": false
    },
    {
        "id": "ACE",
        "text": "African Casino Equipment",
        "deprecated": false
    },
    {
        "id": "ACF",
        "text": "NOVOMATIC AG - CoolFire",
        "deprecated": false
    },
    {
        "id": "ACG",
        "text": "Acoma Gaming Commission /Sky City Casino",
        "deprecated": false
    },
    {
        "id": "ACH",
        "text": "Accurate Chip Rack LLC",
        "deprecated": false
    },
    {
        "id": "ACI",
        "text": "APPLIED CONCEPTS INC",
        "deprecated": false
    },
    {
        "id": "ACL",
        "text": "Argentina Cordoba Lottery",
        "deprecated": false
    },
    {
        "id": "ACM",
        "text": "Acme Srl",
        "deprecated": false
    },
    {
        "id": "ACN",
        "text": "Arte Coin S.R.L.",
        "deprecated": false
    },
    {
        "id": "ACO",
        "text": "Argosy Casino Riverside",
        "deprecated": false
    },
    {
        "id": "ACP",
        "text": "Aconpays Corporation Limited",
        "deprecated": false
    },
    {
        "id": "ACS",
        "text": "AC SLOTS",
        "deprecated": false
    },
    {
        "id": "ACT",
        "text": "ACCESS CONTROL TECH",
        "deprecated": false
    },
    {
        "id": "ACV",
        "text": "Activa Games SL",
        "deprecated": false
    },
    {
        "id": "ACW",
        "text": "AceCrown",
        "deprecated": false
    },
    {
        "id": "ADB",
        "text": "Admiral Global Betting a.s.",
        "deprecated": false
    },
    {
        "id": "ADC",
        "text": "ADVANCED CASINO SYST",
        "deprecated": false
    },
    {
        "id": "ADE",
        "text": "Arizona Dept. of Gaming",
        "deprecated": false
    },
    {
        "id": "ADG",
        "text": "ADRENALIN GAMING, LLC",
        "deprecated": false
    },
    {
        "id": "ADI",
        "text": "Advantech Innocore",
        "deprecated": false
    },
    {
        "id": "ADL",
        "text": "Casino Admiral SA (Czech Republic)",
        "deprecated": false
    },
    {
        "id": "ADM",
        "text": "Admirals S.R.L.",
        "deprecated": false
    },
    {
        "id": "ADN",
        "text": "Admiral Casinos \u0026 Entertainment AG",
        "deprecated": false
    },
    {
        "id": "ADO",
        "text": "Admiral d.o.o. - Admiral Casino Mediteran - Ibralni Sal",
        "deprecated": false
    },
    {
        "id": "ADP",
        "text": "ADP Gauselmann GmbH",
        "deprecated": false
    },
    {
        "id": "ADR",
        "text": "Adriatica Giochi SRL",
        "deprecated": false
    },
    {
        "id": "ADS",
        "text": "Adriasys Računalniški Sistemi d.o.o. dba Adriasys d.o.o",
        "deprecated": false
    },
    {
        "id": "AEA",
        "text": "ACE Casino \u0026 Games, Inc.",
        "deprecated": false
    },
    {
        "id": "AEB",
        "text": "Automation Europe bvba",
        "deprecated": false
    },
    {
        "id": "AEC",
        "text": "Ameristar East Chicago Casino",
        "deprecated": false
    },
    {
        "id": "AEE",
        "text": "Ace Coin Entertainment Ltd",
        "deprecated": false
    },
    {
        "id": "AEI",
        "text": "Amiga Entertainment Limited",
        "deprecated": false
    },
    {
        "id": "AEL",
        "text": "Ameba Entertainment Limited",
        "deprecated": false
    },
    {
        "id": "AEM",
        "text": "Akamon Entertainment Milenium SL",
        "deprecated": false
    },
    {
        "id": "AEN",
        "text": "AMI Entertainment Network",
        "deprecated": false
    },
    {
        "id": "AEO",
        "text": "Aeon Gaming",
        "deprecated": false
    },
    {
        "id": "AER",
        "text": "AERIES",
        "deprecated": false
    },
    {
        "id": "AES",
        "text": "AS Eesti Loto Ltd.",
        "deprecated": false
    },
    {
        "id": "AET",
        "text": "Advanced Entertainment of Michigan",
        "deprecated": false
    },
    {
        "id": "AEU",
        "text": "Aristocrat Technologies Europe Ltd",
        "deprecated": false
    },
    {
        "id": "AFC",
        "text": "Autoridad de Fiscalizacion del Juego",
        "deprecated": false
    },
    {
        "id": "AFE",
        "text": "Agence Française d’Expertise Technique Internationale (AFETI) dba Expertise France",
        "deprecated": false
    },
    {
        "id": "AFI",
        "text": "ACRES FIORE INC",
        "deprecated": false
    },
    {
        "id": "AFL",
        "text": "AI Factory Limited",
        "deprecated": false
    },
    {
        "id": "AFM",
        "text": "AFM NETWORK/COMPUTER",
        "deprecated": false
    },
    {
        "id": "AFR",
        "text": "Anthony Frate",
        "deprecated": false
    },
    {
        "id": "AFS",
        "text": "Automated Financial Services Inc. dba AFS",
        "deprecated": false
    },
    {
        "id": "AG1",
        "text": "Alberta Gaming Liquor and Cannabis Commission",
        "deprecated": false
    },
    {
        "id": "AG2",
        "text": "Algorithm Games Nigeria Limited",
        "deprecated": false
    },
    {
        "id": "AG3",
        "text": "APEX gaming s.r.o.",
        "deprecated": false
    },
    {
        "id": "AGA",
        "text": "ALCOHOL AND GAMING A",
        "deprecated": false
    },
    {
        "id": "AGB",
        "text": "Augustine Band of Cahuilla Indians Gaming Commission",
        "deprecated": false
    },
    {
        "id": "AGC",
        "text": "Agua Caliente Tribal Gaming Commission",
        "deprecated": false
    },
    {
        "id": "AGD",
        "text": "Alea Ltd.",
        "deprecated": false
    },
    {
        "id": "AGE",
        "text": "American Gaming \u0026 Electronics, Inc.",
        "deprecated": false
    },
    {
        "id": "AGG",
        "text": "AutoGaming, Inc.",
        "deprecated": false
    },
    {
        "id": "AGH",
        "text": "AGB Lottery \u0026 Gaming Holdings Group Limited",
        "deprecated": false
    },
    {
        "id": "AGI",
        "text": "Pollard Games, Inc. dba American Games",
        "deprecated": false
    },
    {
        "id": "AGL",
        "text": "AGL DISTRIBUTION SER",
        "deprecated": false
    },
    {
        "id": "AGM",
        "text": "Amusement Games \u0026 Machines",
        "deprecated": false
    },
    {
        "id": "AGN",
        "text": "Admiral Gaming Network SRL",
        "deprecated": false
    },
    {
        "id": "AGO",
        "text": "APPIA GIOCHI SRL",
        "deprecated": false
    },
    {
        "id": "AGR",
        "text": "Alderney Gambling Control Commission",
        "deprecated": false
    },
    {
        "id": "AGS",
        "text": "A Games INC.",
        "deprecated": false
    },
    {
        "id": "AGY",
        "text": "AGS, LLC.",
        "deprecated": false
    },
    {
        "id": "AHA",
        "text": "Aha Bingo Ltd",
        "deprecated": false
    },
    {
        "id": "AHG",
        "text": "Ash Gaming",
        "deprecated": false
    },
    {
        "id": "AHY",
        "text": "The Alternative Hypothesis, LLC",
        "deprecated": false
    },
    {
        "id": "AI1",
        "text": "Advantage Information Technology",
        "deprecated": false
    },
    {
        "id": "AIA",
        "text": "Aruze Interactive (division of Aruze Gaming America Inc.)",
        "deprecated": false
    },
    {
        "id": "AIC",
        "text": "Allin Interactive Corporation",
        "deprecated": false
    },
    {
        "id": "AID",
        "text": "Amgo iGaming Denmark A/S",
        "deprecated": false
    },
    {
        "id": "AIG",
        "text": "International Game Support of America LLC",
        "deprecated": false
    },
    {
        "id": "AIL",
        "text": "Aspect International Limited",
        "deprecated": false
    },
    {
        "id": "AIN",
        "text": "Advanced Internet Technologies Pty Ltd",
        "deprecated": false
    },
    {
        "id": "AIO",
        "text": "Aceking IOM Limited",
        "deprecated": false
    },
    {
        "id": "AIR",
        "text": "AireTech Solutions Limited",
        "deprecated": false
    },
    {
        "id": "AIS",
        "text": "Ace Interactive",
        "deprecated": false
    },
    {
        "id": "AIT",
        "text": "Atum International Trading Co. LTD",
        "deprecated": false
    },
    {
        "id": "AIV",
        "text": "Atria Invest S.R.",
        "deprecated": false
    },
    {
        "id": "AJG",
        "text": "Artemio Juan Gusella",
        "deprecated": false
    },
    {
        "id": "AJS",
        "text": "Antena 3 Juegos S.A.U.",
        "deprecated": false
    },
    {
        "id": "AKA",
        "text": "Akanis Technologies",
        "deprecated": false
    },
    {
        "id": "AKE",
        "text": "Askott Entertainment Inc.",
        "deprecated": false
    },
    {
        "id": "AKM",
        "text": "Askott Entertainment (Malta) Limited",
        "deprecated": false
    },
    {
        "id": "AKT",
        "text": "AceKing Tech Limited",
        "deprecated": false
    },
    {
        "id": "AL1",
        "text": "Allanion LTD",
        "deprecated": false
    },
    {
        "id": "AL2",
        "text": "Albania (non-billable)",
        "deprecated": false
    },
    {
        "id": "AL3",
        "text": "Argentina LOTBA (non-billable)",
        "deprecated": false
    },
    {
        "id": "AL4",
        "text": "ALOT Limited",
        "deprecated": false
    },
    {
        "id": "AL5",
        "text": "Alvin5",
        "deprecated": false
    },
    {
        "id": "ALA",
        "text": "Alabama-Coushatta Gaming Commission",
        "deprecated": false
    },
    {
        "id": "ALB",
        "text": "ALBERTA GAMING",
        "deprecated": false
    },
    {
        "id": "ALC",
        "text": "Atlantic Lottery Corporation",
        "deprecated": false
    },
    {
        "id": "ALD",
        "text": "Alderney Gambling Control Group",
        "deprecated": false
    },
    {
        "id": "ALE",
        "text": "ALEARA Sindicato De Trabajadores De Juegos De Azar, Entretenimiento, Esparcimiento, Recreación Y Afines De La Repub Arg",
        "deprecated": false
    },
    {
        "id": "ALF",
        "text": "Alfard Malta Ltd.",
        "deprecated": false
    },
    {
        "id": "ALG",
        "text": "Aristocrat Global Gaming",
        "deprecated": false
    },
    {
        "id": "ALI",
        "text": "Alternative Gaming Solutions, Inc.",
        "deprecated": false
    },
    {
        "id": "ALL",
        "text": "Alliance Gaming Solutions Limited",
        "deprecated": false
    },
    {
        "id": "ALM",
        "text": "Alpha Malta (OutboxT)",
        "deprecated": false
    },
    {
        "id": "ALN",
        "text": "Altenar Ltd",
        "deprecated": false
    },
    {
        "id": "ALO",
        "text": "Alcohol and Gaming Commission of Ontario",
        "deprecated": false
    },
    {
        "id": "ALP",
        "text": "ACRILPAL S.A.",
        "deprecated": false
    },
    {
        "id": "ALR",
        "text": "Alturas Casino",
        "deprecated": false
    },
    {
        "id": "ALS",
        "text": "ALC - Slot Testing",
        "deprecated": false
    },
    {
        "id": "ALT",
        "text": "ALTEC",
        "deprecated": false
    },
    {
        "id": "ALU",
        "text": "Alturas Rancheria Gaming Commission",
        "deprecated": false
    },
    {
        "id": "ALV",
        "text": "ALVARADO",
        "deprecated": false
    },
    {
        "id": "ALW",
        "text": "Almarlwin, LLC",
        "deprecated": false
    },
    {
        "id": "AM1",
        "text": "AGSi Malta Limited",
        "deprecated": false
    },
    {
        "id": "AMA",
        "text": "AMA DISTRIBUTORS",
        "deprecated": false
    },
    {
        "id": "AMB",
        "text": "Amber Gaming",
        "deprecated": false
    },
    {
        "id": "AMC",
        "text": "AUTOMATED CURRENCY",
        "deprecated": false
    },
    {
        "id": "AMD",
        "text": "American Data S.A.",
        "deprecated": false
    },
    {
        "id": "AME",
        "text": "AMERICAN ALPHA INC.",
        "deprecated": false
    },
    {
        "id": "AMG",
        "text": "AMERICAN GAMING TECH",
        "deprecated": false
    },
    {
        "id": "AMI",
        "text": "Amatic Industries GMBH",
        "deprecated": false
    },
    {
        "id": "AML",
        "text": "Aruze Gaming Macau Limited",
        "deprecated": false
    },
    {
        "id": "AMM",
        "text": "Ameristar Casino St. Charles",
        "deprecated": false
    },
    {
        "id": "AMN",
        "text": "American Gaming Technology",
        "deprecated": false
    },
    {
        "id": "AMP",
        "text": "AMP3 Gaming",
        "deprecated": false
    },
    {
        "id": "AMR",
        "text": "Ameristar Casinos, Inc - Corporate",
        "deprecated": false
    },
    {
        "id": "AMS",
        "text": "AMERISTAR CASINO",
        "deprecated": false
    },
    {
        "id": "AMT",
        "text": "AMTOTE",
        "deprecated": false
    },
    {
        "id": "AMU",
        "text": "AMUTRON",
        "deprecated": false
    },
    {
        "id": "AMY",
        "text": "AlchemyBet Limited",
        "deprecated": false
    },
    {
        "id": "AMZ",
        "text": "Actimize, Inc.",
        "deprecated": false
    },
    {
        "id": "ANA",
        "text": "Vanage LLC.",
        "deprecated": false
    },
    {
        "id": "AND",
        "text": "Andromeda SNC",
        "deprecated": false
    },
    {
        "id": "ANG",
        "text": "Anderson Gambling Ltd.",
        "deprecated": false
    },
    {
        "id": "ANI",
        "text": "American National Standards Institute",
        "deprecated": false
    },
    {
        "id": "ANL",
        "text": "Albertyne Ltd.",
        "deprecated": false
    },
    {
        "id": "ANR",
        "text": "Andor d.o.o",
        "deprecated": false
    },
    {
        "id": "ANS",
        "text": "Axes Network Solutions Inc.",
        "deprecated": false
    },
    {
        "id": "ANT",
        "text": "Alpha Networks Solution Pte. Ltd.",
        "deprecated": false
    },
    {
        "id": "ANU",
        "text": "NOVOMATIC AG - NovoUnity",
        "deprecated": false
    },
    {
        "id": "AOL",
        "text": "Asia-Wide Online Limited",
        "deprecated": false
    },
    {
        "id": "AON",
        "text": "ADMIRAL ONLINE S.R.L.",
        "deprecated": false
    },
    {
        "id": "AOS",
        "text": "Ankhos Oncology Software",
        "deprecated": false
    },
    {
        "id": "AOU",
        "text": "Alien Gain OÜ (formerly Alien Gaming Intelligence)",
        "deprecated": false
    },
    {
        "id": "APA",
        "text": "Apache Gold Casino Resort",
        "deprecated": false
    },
    {
        "id": "APC",
        "text": "Angel Playing Card Manufacturing",
        "deprecated": false
    },
    {
        "id": "APE",
        "text": "APEX pro gaming a.s.",
        "deprecated": false
    },
    {
        "id": "APG",
        "text": "ACOMA PUEBLO GAMING",
        "deprecated": false
    },
    {
        "id": "API",
        "text": "ATLANTIS PARADISE IS",
        "deprecated": false
    },
    {
        "id": "APL",
        "text": "Aristocrat Technologies Australia Pty Ltd",
        "deprecated": false
    },
    {
        "id": "APN",
        "text": "Apsaalooke Nights Casino",
        "deprecated": false
    },
    {
        "id": "APO",
        "text": "Apogee Gaming, Inc.",
        "deprecated": false
    },
    {
        "id": "APP",
        "text": "Arcade Planet, Inc.",
        "deprecated": false
    },
    {
        "id": "APR",
        "text": "Appricotta Limited",
        "deprecated": false
    },
    {
        "id": "APS",
        "text": "ADVANTAGE PROMO SYS.",
        "deprecated": false
    },
    {
        "id": "APU",
        "text": "Apuesta Ganador Online SA",
        "deprecated": false
    },
    {
        "id": "APX",
        "text": "Apex Gaming Europe A.S.",
        "deprecated": false
    },
    {
        "id": "AQG",
        "text": "AliQuantum Gaming Limited",
        "deprecated": false
    },
    {
        "id": "AQU",
        "text": "Aquila Global Group S.A.S. dba WPlay",
        "deprecated": false
    },
    {
        "id": "AR1",
        "text": "Arapaho Tribe",
        "deprecated": false
    },
    {
        "id": "AR2",
        "text": "ARB Labs Inc.",
        "deprecated": false
    },
    {
        "id": "ARB",
        "text": "A.R.B. automaten BV",
        "deprecated": false
    },
    {
        "id": "ARC",
        "text": "ARGOSY CASINO",
        "deprecated": false
    },
    {
        "id": "ARE",
        "text": "ARENA ANGELO",
        "deprecated": false
    },
    {
        "id": "ARF",
        "text": "AEROFAB",
        "deprecated": false
    },
    {
        "id": "ARG",
        "text": "AUTOMATED RESOURCE G",
        "deprecated": false
    },
    {
        "id": "ARI",
        "text": "Aristocrat Technologies, Inc.",
        "deprecated": false
    },
    {
        "id": "ARK",
        "text": "Arkansas Racing Commission ",
        "deprecated": false
    },
    {
        "id": "ARL",
        "text": "Arizona Lottery",
        "deprecated": false
    },
    {
        "id": "ARM",
        "text": "Arkadium Inc",
        "deprecated": false
    },
    {
        "id": "ARN",
        "text": "Arrontech Software Limited",
        "deprecated": false
    },
    {
        "id": "ARO",
        "text": "ARROW INTERNATIONAL",
        "deprecated": false
    },
    {
        "id": "ARP",
        "text": "Aristocrat Technologies Inc (Pre Cert)",
        "deprecated": false
    },
    {
        "id": "ARR",
        "text": "AERRE S.r.l.",
        "deprecated": false
    },
    {
        "id": "ARS",
        "text": "Argosy Sioux City",
        "deprecated": false
    },
    {
        "id": "ART",
        "text": "ARES Gaming Technologies Inc.",
        "deprecated": false
    },
    {
        "id": "ARU",
        "text": "Aruba Gaming Commission",
        "deprecated": false
    },
    {
        "id": "ARV",
        "text": "Arrive Software",
        "deprecated": false
    },
    {
        "id": "ARW",
        "text": "Arrow Games S.A.",
        "deprecated": false
    },
    {
        "id": "ARY",
        "text": "Alton Casino LLC",
        "deprecated": false
    },
    {
        "id": "ARZ",
        "text": "Aruze Gaming America",
        "deprecated": false
    },
    {
        "id": "ASA",
        "text": "Africabet South Africa (PTY) Ltd",
        "deprecated": false
    },
    {
        "id": "ASB",
        "text": "Autoscribe Limited",
        "deprecated": false
    },
    {
        "id": "ASC",
        "text": "Astro Corp",
        "deprecated": false
    },
    {
        "id": "ASE",
        "text": "AMUSYS Amusement Systems Electronics GmbH",
        "deprecated": false
    },
    {
        "id": "ASF",
        "text": "Apollo Soft Limited",
        "deprecated": false
    },
    {
        "id": "ASG",
        "text": "ALFA STREET GAMBLING",
        "deprecated": false
    },
    {
        "id": "ASI",
        "text": "Automated Systems America Incorporate",
        "deprecated": false
    },
    {
        "id": "ASK",
        "text": "Apollo Soft s.r.o.",
        "deprecated": false
    },
    {
        "id": "ASL",
        "text": "Arkansas Lottery Commission",
        "deprecated": false
    },
    {
        "id": "ASM",
        "text": "AS-MB d.o.o",
        "deprecated": false
    },
    {
        "id": "ASO",
        "text": "Ascend Solutions Sdn Bhd",
        "deprecated": false
    },
    {
        "id": "ASP",
        "text": "AG Communications Limited",
        "deprecated": false
    },
    {
        "id": "ASR",
        "text": "A.M.S. S.r.l.",
        "deprecated": false
    },
    {
        "id": "ASS",
        "text": "Atlantic Supplies S.A.",
        "deprecated": false
    },
    {
        "id": "AST",
        "text": "ASTRA",
        "deprecated": false
    },
    {
        "id": "ASV",
        "text": "Ace Star Investment Ltd.",
        "deprecated": false
    },
    {
        "id": "ASW",
        "text": "Alea Software Limited",
        "deprecated": false
    },
    {
        "id": "ASY",
        "text": "Agilysys",
        "deprecated": false
    },
    {
        "id": "AT1",
        "text": "Aktiebolaget Trav och Galopp",
        "deprecated": false
    },
    {
        "id": "ATA",
        "text": "IGT Austria GmbH",
        "deprecated": false
    },
    {
        "id": "ATC",
        "text": "Augustine Casino",
        "deprecated": false
    },
    {
        "id": "ATD",
        "text": "Arland Trading GmbH",
        "deprecated": false
    },
    {
        "id": "ATE",
        "text": "Atesting co DOO",
        "deprecated": false
    },
    {
        "id": "ATF",
        "text": "umAfrika Gaming Technologies (Pty) Ltd.",
        "deprecated": false
    },
    {
        "id": "ATG",
        "text": "Ak-Chin Tribal Gaming Agency",
        "deprecated": false
    },
    {
        "id": "ATI",
        "text": "Everi",
        "deprecated": false
    },
    {
        "id": "ATK",
        "text": "Astok LTD",
        "deprecated": false
    },
    {
        "id": "ATL",
        "text": "Alphabet Technology Limited",
        "deprecated": false
    },
    {
        "id": "ATM",
        "text": "Atac Media",
        "deprecated": false
    },
    {
        "id": "ATN",
        "text": "Atlantis S.R.L.",
        "deprecated": false
    },
    {
        "id": "ATO",
        "text": "ASTROSYSTEMS",
        "deprecated": false
    },
    {
        "id": "ATP",
        "text": "Acon Technology PTY",
        "deprecated": false
    },
    {
        "id": "ATR",
        "text": "GTECH USA, LLC",
        "deprecated": false
    },
    {
        "id": "ATS",
        "text": "Assist S.A.",
        "deprecated": false
    },
    {
        "id": "ATT",
        "text": "Atlas Gaming Technologies Pty Ltd",
        "deprecated": false
    },
    {
        "id": "ATV",
        "text": "Atventure",
        "deprecated": false
    },
    {
        "id": "AUB",
        "text": "AUBURN RANCHERIA",
        "deprecated": false
    },
    {
        "id": "AUC",
        "text": "NOVOMATIC AG - Impera",
        "deprecated": false
    },
    {
        "id": "AUD",
        "text": "Auditor General",
        "deprecated": false
    },
    {
        "id": "AUG",
        "text": "Automated Gaming Technologies",
        "deprecated": false
    },
    {
        "id": "AUH",
        "text": "Authentic Gaming Ltd",
        "deprecated": false
    },
    {
        "id": "AUK",
        "text": "Ainsworth (UK) Ltd.",
        "deprecated": false
    },
    {
        "id": "AUL",
        "text": "Amelco UK Ltd",
        "deprecated": false
    },
    {
        "id": "AUP",
        "text": "Aces Up Gaming Inc.",
        "deprecated": false
    },
    {
        "id": "AUS",
        "text": "Automated Cash Systems",
        "deprecated": false
    },
    {
        "id": "AUT",
        "text": "AUTOTOTE",
        "deprecated": false
    },
    {
        "id": "AVA",
        "text": "Avia International Inc.",
        "deprecated": false
    },
    {
        "id": "AVD",
        "text": "Advanced Technologies Group, LLC",
        "deprecated": false
    },
    {
        "id": "AVG",
        "text": "Advanced Gaming Solutions, Inc.",
        "deprecated": false
    },
    {
        "id": "AVI",
        "text": "AVI RESORT \u0026  CASINO",
        "deprecated": false
    },
    {
        "id": "AVM",
        "text": "Av Imperial Technologies Limited (AITL) t/a MTech Communications",
        "deprecated": false
    },
    {
        "id": "AVN",
        "text": "Avento MT Limited",
        "deprecated": false
    },
    {
        "id": "AVS",
        "text": "Advansys D.O.O.",
        "deprecated": false
    },
    {
        "id": "AVT",
        "text": "Advanced Gaming Technology LLC",
        "deprecated": false
    },
    {
        "id": "AVU",
        "text": "Avantage USA LLC",
        "deprecated": false
    },
    {
        "id": "AWG",
        "text": "AINSWORTH GAMING TEC",
        "deprecated": false
    },
    {
        "id": "AWH",
        "text": "American Wagering Inc, dba William Hill US",
        "deprecated": false
    },
    {
        "id": "AWI",
        "text": "AWI",
        "deprecated": false
    },
    {
        "id": "AWO",
        "text": "Asia Wide Online Limited",
        "deprecated": false
    },
    {
        "id": "AXC",
        "text": "Axiomtek Co Ltd.",
        "deprecated": false
    },
    {
        "id": "AXE",
        "text": "Progamesys Progressive Gaming Systems Kft.",
        "deprecated": false
    },
    {
        "id": "AXS",
        "text": "AXS Group LLC (Formerly Outbox AXS LLC)",
        "deprecated": false
    },
    {
        "id": "AZG",
        "text": "Arizona Indian Gaming Association",
        "deprecated": false
    },
    {
        "id": "AZI",
        "text": "Azimut Software Limited",
        "deprecated": false
    },
    {
        "id": "B4B",
        "text": "Bet4theBest Ltd",
        "deprecated": false
    },
    {
        "id": "BAB",
        "text": "Balch Bingham LLP",
        "deprecated": false
    },
    {
        "id": "BAC",
        "text": "Bacchilega Video Games Sr1",
        "deprecated": false
    },
    {
        "id": "BAD",
        "text": "Baldazzi Styl Art s.r.l",
        "deprecated": false
    },
    {
        "id": "BAF",
        "text": "Bally Gaming Africa (Pty) Ltd",
        "deprecated": false
    },
    {
        "id": "BAG",
        "text": "Barona Gaming Commission ",
        "deprecated": false
    },
    {
        "id": "BAH",
        "text": "2M2B Bahia Brasil Ltd.",
        "deprecated": false
    },
    {
        "id": "BAJ",
        "text": "Beers, Anderson, Jackson, Patty \u0026 Fawal",
        "deprecated": false
    },
    {
        "id": "BAL",
        "text": "SG Gaming, Inc. f/k/a Bally Gaming, Inc.",
        "deprecated": false
    },
    {
        "id": "BAM",
        "text": "BaddaMedia",
        "deprecated": false
    },
    {
        "id": "BAN",
        "text": "Banegras Union S.A.",
        "deprecated": false
    },
    {
        "id": "BAS",
        "text": "British Association for Shooting and Conservation",
        "deprecated": false
    },
    {
        "id": "BAT",
        "text": "BACTA Ltd.",
        "deprecated": false
    },
    {
        "id": "BAU",
        "text": "Bingo Australia",
        "deprecated": false
    },
    {
        "id": "BAY",
        "text": "BAY TEK INC.",
        "deprecated": false
    },
    {
        "id": "BBC",
        "text": "Bally\u0027s Belle Casino",
        "deprecated": false
    },
    {
        "id": "BBD",
        "text": "BBINTECH Digital Entertainment Co. Ltd.",
        "deprecated": false
    },
    {
        "id": "BBG",
        "text": "Bluberi Gaming Tech.",
        "deprecated": false
    },
    {
        "id": "BBI",
        "text": "Bingo Brain Inc.",
        "deprecated": false
    },
    {
        "id": "BBJ",
        "text": "BONUS BLACKJACK",
        "deprecated": false
    },
    {
        "id": "BBK",
        "text": "Be-A-Bookie Ltd",
        "deprecated": false
    },
    {
        "id": "BBL",
        "text": "BESTBET LIMITED",
        "deprecated": false
    },
    {
        "id": "BBM",
        "text": "Bigaboom",
        "deprecated": false
    },
    {
        "id": "BBN",
        "text": "BBIN International Limited",
        "deprecated": false
    },
    {
        "id": "BBP",
        "text": "BEST BET PRODUCTS",
        "deprecated": false
    },
    {
        "id": "BBR",
        "text": "Belle of Baton Rouge Casino",
        "deprecated": false
    },
    {
        "id": "BBS",
        "text": "BB Studio Inc.",
        "deprecated": false
    },
    {
        "id": "BBT",
        "text": "BB IN Technology",
        "deprecated": false
    },
    {
        "id": "BC1",
        "text": "BILLAR CLUB MULTICOLOR S.A (BCM)",
        "deprecated": false
    },
    {
        "id": "BCA",
        "text": "Bermuda Casino Gaming Commission",
        "deprecated": false
    },
    {
        "id": "BCC",
        "text": "Blue Chip Casino",
        "deprecated": false
    },
    {
        "id": "BCG",
        "text": "Bee Cave Games Inc.",
        "deprecated": false
    },
    {
        "id": "BCH",
        "text": "Blue Chippers Limited",
        "deprecated": false
    },
    {
        "id": "BCI",
        "text": "Betcircuit",
        "deprecated": false
    },
    {
        "id": "BCK",
        "text": "Berry Creek Rancheria Gaming Commission",
        "deprecated": false
    },
    {
        "id": "BCL",
        "text": "British Columbia Lottery Corporation (BCLC)",
        "deprecated": false
    },
    {
        "id": "BCM",
        "text": "BCM",
        "deprecated": false
    },
    {
        "id": "BCN",
        "text": "Bingo Card Minder Corporation for Nebraska",
        "deprecated": false
    },
    {
        "id": "BCO",
        "text": "BetConstruct",
        "deprecated": false
    },
    {
        "id": "BCP",
        "text": "Betting Company S.A.",
        "deprecated": false
    },
    {
        "id": "BCQ",
        "text": "Banca de Cubierta Colectiva de Quinielas de Montevideo",
        "deprecated": false
    },
    {
        "id": "BCR",
        "text": "BetCluster",
        "deprecated": false
    },
    {
        "id": "BCS",
        "text": "Bytecraft Systems",
        "deprecated": false
    },
    {
        "id": "BCT",
        "text": "Barcrest Development",
        "deprecated": false
    },
    {
        "id": "BCV",
        "text": "BC Innovations Inc.",
        "deprecated": false
    },
    {
        "id": "BCX",
        "text": "BC Technologies, LLC",
        "deprecated": false
    },
    {
        "id": "BDI",
        "text": "Betdigital Ltd",
        "deprecated": false
    },
    {
        "id": "BDL",
        "text": "Beweb Direct Limited",
        "deprecated": false
    },
    {
        "id": "BDM",
        "text": "BDM",
        "deprecated": false
    },
    {
        "id": "BDP",
        "text": "BD Sport Ltd.",
        "deprecated": false
    },
    {
        "id": "BDS",
        "text": "Brody Secore",
        "deprecated": false
    },
    {
        "id": "BDT",
        "text": "Borden Technology Corporation",
        "deprecated": false
    },
    {
        "id": "BE1",
        "text": "Belgium (non-billable)",
        "deprecated": false
    },
    {
        "id": "BEA",
        "text": "Beast Gaming",
        "deprecated": false
    },
    {
        "id": "BEC",
        "text": "Betfair Casino Limited",
        "deprecated": false
    },
    {
        "id": "BED",
        "text": "Bingo Electronico de Espana S.A.",
        "deprecated": false
    },
    {
        "id": "BEE",
        "text": "BETSON ENTERPRISES",
        "deprecated": false
    },
    {
        "id": "BEF",
        "text": "Betfair Interactive US LLC",
        "deprecated": false
    },
    {
        "id": "BEG",
        "text": "Benito Gonzales",
        "deprecated": false
    },
    {
        "id": "BEH",
        "text": "Bet-at-home.com Internet LTD",
        "deprecated": false
    },
    {
        "id": "BEI",
        "text": "BETTINA CORPORATION",
        "deprecated": false
    },
    {
        "id": "BEL",
        "text": "BELTERRA RESORT \u0026 CA",
        "deprecated": false
    },
    {
        "id": "BEM",
        "text": "Bingo Electronicode Mexico S de RL de",
        "deprecated": false
    },
    {
        "id": "BEN",
        "text": "Benchmark Entertain",
        "deprecated": false
    },
    {
        "id": "BEO",
        "text": "BEM Operations Ltd.",
        "deprecated": false
    },
    {
        "id": "BER",
        "text": "Bell Aliant Regional Communications",
        "deprecated": false
    },
    {
        "id": "BES",
        "text": "BELL ELECTRONICS SRL",
        "deprecated": false
    },
    {
        "id": "BET",
        "text": "BETSTAR",
        "deprecated": false
    },
    {
        "id": "BEU",
        "text": "Bernardi Eugenio",
        "deprecated": false
    },
    {
        "id": "BEY",
        "text": "Betnology S.A.",
        "deprecated": false
    },
    {
        "id": "BF2",
        "text": "Betfred USA Sports LLC",
        "deprecated": false
    },
    {
        "id": "BF3",
        "text": "Betfred",
        "deprecated": false
    },
    {
        "id": "BFC",
        "text": "Bluefrog Contents \u0026 Support Inc.",
        "deprecated": false
    },
    {
        "id": "BFD",
        "text": "BEACHFIELD",
        "deprecated": false
    },
    {
        "id": "BFE",
        "text": "B.F. Enterprises",
        "deprecated": false
    },
    {
        "id": "BFG",
        "text": "Greentube UK Ltd.",
        "deprecated": false
    },
    {
        "id": "BFI",
        "text": "Black Fund Inc",
        "deprecated": false
    },
    {
        "id": "BFL",
        "text": "Betflag S.p.A.",
        "deprecated": false
    },
    {
        "id": "BFM",
        "text": "Bee-Fee Limited",
        "deprecated": false
    },
    {
        "id": "BFR",
        "text": "Betfair International PLC",
        "deprecated": false
    },
    {
        "id": "BFT",
        "text": "Bois Forte Gaming Regulatory",
        "deprecated": false
    },
    {
        "id": "BG1",
        "text": "Bgame S.r.l",
        "deprecated": false
    },
    {
        "id": "BG2",
        "text": "Bethard Group Limited",
        "deprecated": false
    },
    {
        "id": "BGA",
        "text": "B\u0026L Gaming",
        "deprecated": false
    },
    {
        "id": "BGB",
        "text": "Botswana Gambling Authority",
        "deprecated": false
    },
    {
        "id": "BGC",
        "text": "BOYD GAMING CORPORATION",
        "deprecated": false
    },
    {
        "id": "BGD",
        "text": "Best Gambling Limited",
        "deprecated": false
    },
    {
        "id": "BGE",
        "text": "Betting Entertainment Technologies",
        "deprecated": false
    },
    {
        "id": "BGG",
        "text": "Bingo Gaming Tech",
        "deprecated": false
    },
    {
        "id": "BGH",
        "text": "Best Gaming Technology GmbH",
        "deprecated": false
    },
    {
        "id": "BGI",
        "text": "Barrett Gaming International",
        "deprecated": false
    },
    {
        "id": "BGL",
        "text": "Betting Gaming \u0026 Lotteries Commission (Treasure Hunt Casino)",
        "deprecated": false
    },
    {
        "id": "BGM",
        "text": "Banilla Games, Inc.",
        "deprecated": false
    },
    {
        "id": "BGN",
        "text": "Boss Gaming Group N.V.",
        "deprecated": false
    },
    {
        "id": "BGO",
        "text": "B.G.S. gostinstvo in turizem d.o.o. ",
        "deprecated": false
    },
    {
        "id": "BGP",
        "text": "BG Global Phils., Inc.",
        "deprecated": false
    },
    {
        "id": "BGR",
        "text": "BankGiro Loterij N.V.",
        "deprecated": false
    },
    {
        "id": "BGS",
        "text": "Big Sandy Rancheria Gaming Commission",
        "deprecated": false
    },
    {
        "id": "BGT",
        "text": "BELLATRIX GAMING TEC",
        "deprecated": false
    },
    {
        "id": "BGU",
        "text": "Betgenius Limited",
        "deprecated": false
    },
    {
        "id": "BGY",
        "text": "Brilogy Corporation",
        "deprecated": false
    },
    {
        "id": "BH1",
        "text": "Bosnia \u0026 Herzegovina (non-billable)",
        "deprecated": false
    },
    {
        "id": "BHA",
        "text": "Bok Homa Casino",
        "deprecated": false
    },
    {
        "id": "BHC",
        "text": "BLACK HAWK CASINO",
        "deprecated": false
    },
    {
        "id": "BHG",
        "text": "Black Hawk Gaming",
        "deprecated": false
    },
    {
        "id": "BHM",
        "text": "Baha Mar Ltd",
        "deprecated": false
    },
    {
        "id": "BHN",
        "text": "BlackHawk Network, Inc.",
        "deprecated": false
    },
    {
        "id": "BHO",
        "text": "Bayamon Hotel Company LLC",
        "deprecated": false
    },
    {
        "id": "BHS",
        "text": "BHS Bohm GmbH \u0026 Co. KG",
        "deprecated": false
    },
    {
        "id": "BI8",
        "text": "Bit8 Ltd",
        "deprecated": false
    },
    {
        "id": "BIA",
        "text": "BIGACE",
        "deprecated": false
    },
    {
        "id": "BIC",
        "text": "BRIDGEPORT INDIAN",
        "deprecated": false
    },
    {
        "id": "BIE",
        "text": "Bwin Interactive Entertainment",
        "deprecated": false
    },
    {
        "id": "BIF",
        "text": "Unilogic Information Technology Ltd.",
        "deprecated": false
    },
    {
        "id": "BIG",
        "text": "BIG SANDY",
        "deprecated": false
    },
    {
        "id": "BII",
        "text": "Biim Ltd",
        "deprecated": false
    },
    {
        "id": "BIL",
        "text": "Bet invest LTD",
        "deprecated": false
    },
    {
        "id": "BIM",
        "text": "Bingomach Inc.",
        "deprecated": false
    },
    {
        "id": "BIN",
        "text": "BINSUR S.A.",
        "deprecated": false
    },
    {
        "id": "BIR",
        "text": "BARONA INDIAN RESERV",
        "deprecated": false
    },
    {
        "id": "BIT",
        "text": "Big Time Gaming",
        "deprecated": false
    },
    {
        "id": "BIZ",
        "text": "Biztech Software (NI) LTD",
        "deprecated": false
    },
    {
        "id": "BJ1",
        "text": "Beijing Sunrise Mien Trade Development Co Ltd",
        "deprecated": false
    },
    {
        "id": "BJ2",
        "text": "Beijing Welfare Lottery Center",
        "deprecated": false
    },
    {
        "id": "BJX",
        "text": "BJX Entertainment, LLC",
        "deprecated": false
    },
    {
        "id": "BJY",
        "text": "Berjaya Gia Thinh Investment Technology JSC",
        "deprecated": false
    },
    {
        "id": "BKM",
        "text": "BOOKMACHER SP Z O.O.",
        "deprecated": false
    },
    {
        "id": "BKO",
        "text": "Bakoo S.p.A.",
        "deprecated": false
    },
    {
        "id": "BLC",
        "text": "Boomtown Belle Casino",
        "deprecated": false
    },
    {
        "id": "BLD",
        "text": "Baldo Line S.r.l.",
        "deprecated": false
    },
    {
        "id": "BLG",
        "text": "Blue Lake Gaming",
        "deprecated": false
    },
    {
        "id": "BLI",
        "text": "Blackwrist Interactive LLC",
        "deprecated": false
    },
    {
        "id": "BLK",
        "text": "BlockProof Tech LLC",
        "deprecated": false
    },
    {
        "id": "BLM",
        "text": "MLB Advance Media",
        "deprecated": false
    },
    {
        "id": "BLO",
        "text": "B.C. Lottotech International Inc",
        "deprecated": false
    },
    {
        "id": "BLR",
        "text": "BIG LAGOON RANCHERIA",
        "deprecated": false
    },
    {
        "id": "BLS",
        "text": "Blue Sky Limited",
        "deprecated": false
    },
    {
        "id": "BLT",
        "text": "BLR Technologies del Caribe S.A.",
        "deprecated": false
    },
    {
        "id": "BLU",
        "text": "Blue Lake Casino",
        "deprecated": false
    },
    {
        "id": "BLW",
        "text": "Blue Whale Gaming LLC",
        "deprecated": false
    },
    {
        "id": "BLY",
        "text": "BALLY NORTH",
        "deprecated": false
    },
    {
        "id": "BMD",
        "text": "Bapro Medios De Pago SA",
        "deprecated": false
    },
    {
        "id": "BME",
        "text": "BME DESIGN",
        "deprecated": false
    },
    {
        "id": "BMI",
        "text": "Bay Mills Indian Community Gaming Commission",
        "deprecated": false
    },
    {
        "id": "BMS",
        "text": "Bogardus Medical Systems, Inc. (ONCOCHART)",
        "deprecated": false
    },
    {
        "id": "BMT",
        "text": "Bermuda Ministry of Tourism Development \u0026 Transport",
        "deprecated": false
    },
    {
        "id": "BNC",
        "text": "BetterNet Consulting Ltd",
        "deprecated": false
    },
    {
        "id": "BNE",
        "text": "BANDAI NAMCO Entertainment Inc.",
        "deprecated": false
    },
    {
        "id": "BNG",
        "text": "Bestingame",
        "deprecated": false
    },
    {
        "id": "BNI",
        "text": "BINGOTEC Inc",
        "deprecated": false
    },
    {
        "id": "BNL",
        "text": "Bingo Nation LLC",
        "deprecated": false
    },
    {
        "id": "BNS",
        "text": "BONUS 6",
        "deprecated": false
    },
    {
        "id": "BNT",
        "text": "BGNT",
        "deprecated": false
    },
    {
        "id": "BNV",
        "text": "Blitz NV",
        "deprecated": false
    },
    {
        "id": "BNZ",
        "text": "BNZ Plus Limited",
        "deprecated": false
    },
    {
        "id": "BOA",
        "text": "Bo\u0027s Amusement BVBA",
        "deprecated": false
    },
    {
        "id": "BOC",
        "text": "Black Oak Casino",
        "deprecated": false
    },
    {
        "id": "BOE",
        "text": "Beatya Online Entertainment Ltd",
        "deprecated": false
    },
    {
        "id": "BOG",
        "text": "BogusTrivia",
        "deprecated": false
    },
    {
        "id": "BOK",
        "text": "Bok Iek Investment Consultancy Limited",
        "deprecated": false
    },
    {
        "id": "BOL",
        "text": "BOLDT S.A.",
        "deprecated": false
    },
    {
        "id": "BOM",
        "text": "BotGaming",
        "deprecated": false
    },
    {
        "id": "BON",
        "text": "Bonabo PLC",
        "deprecated": false
    },
    {
        "id": "BOO",
        "text": "Boot Hill Casino",
        "deprecated": false
    },
    {
        "id": "BOP",
        "text": "BET OPTIONS PTY Ltd",
        "deprecated": false
    },
    {
        "id": "BOR",
        "text": "Borden Ladner Gervais LLP",
        "deprecated": false
    },
    {
        "id": "BOS",
        "text": "Boom Shakalaka, Inc.",
        "deprecated": false
    },
    {
        "id": "BOT",
        "text": "Boomtown Casino for LA Riverboats",
        "deprecated": false
    },
    {
        "id": "BOW",
        "text": "Broward County",
        "deprecated": false
    },
    {
        "id": "BPA",
        "text": "BINGO PALACE",
        "deprecated": false
    },
    {
        "id": "BPG",
        "text": "BPG",
        "deprecated": false
    },
    {
        "id": "BPH",
        "text": "BELLAGIO PHIL. AMUSEMENT INC.",
        "deprecated": false
    },
    {
        "id": "BPI",
        "text": "Bingo Pilar S.A.",
        "deprecated": false
    },
    {
        "id": "BPL",
        "text": "Betsson Platform Solutions Ltd.",
        "deprecated": false
    },
    {
        "id": "BPN",
        "text": "Betspawn AB",
        "deprecated": false
    },
    {
        "id": "BPO",
        "text": "Blackpot Pty Ltd",
        "deprecated": false
    },
    {
        "id": "BPP",
        "text": "BIG PINE PAIUTE",
        "deprecated": false
    },
    {
        "id": "BPR",
        "text": "Blueprint Gaming Ltd",
        "deprecated": false
    },
    {
        "id": "BPS",
        "text": "Bulletproof Solutions ULC",
        "deprecated": false
    },
    {
        "id": "BPT",
        "text": "Bishop Paiute Gaming Commission",
        "deprecated": false
    },
    {
        "id": "BPW",
        "text": "Money$uit Industries LLC.",
        "deprecated": false
    },
    {
        "id": "BQM",
        "text": "BQ Media Lab Pty. Ltd",
        "deprecated": false
    },
    {
        "id": "BR1",
        "text": "Blue Ribbon Software Ltd.",
        "deprecated": false
    },
    {
        "id": "BR2",
        "text": "Blue Ribbon Software Malta Limited",
        "deprecated": false
    },
    {
        "id": "BRC",
        "text": "BLUFFS RUN CASINO",
        "deprecated": false
    },
    {
        "id": "BRD",
        "text": "Blue Ribbon Downs",
        "deprecated": false
    },
    {
        "id": "BRG",
        "text": "Bear River Tribal Gaming Commission",
        "deprecated": false
    },
    {
        "id": "BRH",
        "text": "Bloomberry Resorts and Hotels Inc.",
        "deprecated": false
    },
    {
        "id": "BRM",
        "text": "BROMBLEY",
        "deprecated": false
    },
    {
        "id": "BRN",
        "text": "Betfair Romania Development SRL",
        "deprecated": false
    },
    {
        "id": "BRO",
        "text": "BARONA",
        "deprecated": false
    },
    {
        "id": "BRP",
        "text": "Bridge Patient Portal, LLC",
        "deprecated": false
    },
    {
        "id": "BRR",
        "text": "Beau Rivage Resort \u0026 Casino",
        "deprecated": false
    },
    {
        "id": "BRS",
        "text": "Bingo Rose",
        "deprecated": false
    },
    {
        "id": "BRT",
        "text": "Barona Casino",
        "deprecated": false
    },
    {
        "id": "BSB",
        "text": "BestBetInNet Ltd",
        "deprecated": false
    },
    {
        "id": "BSC",
        "text": "Blue Sky Casino",
        "deprecated": false
    },
    {
        "id": "BSG",
        "text": "Bison Group, S.A.",
        "deprecated": false
    },
    {
        "id": "BSI",
        "text": "BOINK SYSTEMS, INC.",
        "deprecated": false
    },
    {
        "id": "BSK",
        "text": "Bar Skills Limited",
        "deprecated": false
    },
    {
        "id": "BSL",
        "text": "Buzz Sports Limited",
        "deprecated": false
    },
    {
        "id": "BSM",
        "text": "IGT Sweden Interactive AB",
        "deprecated": false
    },
    {
        "id": "BSO",
        "text": "B-Solution S.r.l.",
        "deprecated": false
    },
    {
        "id": "BSR",
        "text": "BOB\u0027S SPACE RACERS",
        "deprecated": false
    },
    {
        "id": "BSS",
        "text": "BAM Software and Services, LLC.",
        "deprecated": false
    },
    {
        "id": "BST",
        "text": "BESTCO",
        "deprecated": false
    },
    {
        "id": "BSV",
        "text": "Betsson Services Limited",
        "deprecated": false
    },
    {
        "id": "BSY",
        "text": "BetStone Systems Limited ",
        "deprecated": false
    },
    {
        "id": "BT3",
        "text": "Bet365",
        "deprecated": false
    },
    {
        "id": "BTA",
        "text": "Big Trak Technologies",
        "deprecated": false
    },
    {
        "id": "BTB",
        "text": "BACK TO BACK GAMING",
        "deprecated": false
    },
    {
        "id": "BTC",
        "text": "Bingotimes Technology",
        "deprecated": false
    },
    {
        "id": "BTE",
        "text": "Bingo Table Games",
        "deprecated": false
    },
    {
        "id": "BTF",
        "text": "Betfair Italia SRL",
        "deprecated": false
    },
    {
        "id": "BTG",
        "text": "BetSoft Gaming",
        "deprecated": false
    },
    {
        "id": "BTH",
        "text": "Boomtown Casino \u0026 Hotel - Bossier City",
        "deprecated": false
    },
    {
        "id": "BTI",
        "text": "BET TECHNOLOGY INC",
        "deprecated": false
    },
    {
        "id": "BTK",
        "text": "BetClic Limited",
        "deprecated": false
    },
    {
        "id": "BTL",
        "text": "Betable Limited ",
        "deprecated": false
    },
    {
        "id": "BTM",
        "text": "BetSmart",
        "deprecated": false
    },
    {
        "id": "BTN",
        "text": "Betsson Technologies",
        "deprecated": false
    },
    {
        "id": "BTO",
        "text": "Betconnect Ltd.",
        "deprecated": false
    },
    {
        "id": "BTP",
        "text": "Bet Tech Gaming PTY LTD",
        "deprecated": false
    },
    {
        "id": "BTR",
        "text": "BELATRA CO LTD",
        "deprecated": false
    },
    {
        "id": "BTS",
        "text": "BetStone Limited",
        "deprecated": false
    },
    {
        "id": "BTT",
        "text": "BINGO TECH",
        "deprecated": false
    },
    {
        "id": "BTU",
        "text": "LLC Betsolutions",
        "deprecated": false
    },
    {
        "id": "BTV",
        "text": "Victor Chandler International, Ltd.",
        "deprecated": false
    },
    {
        "id": "BTW",
        "text": "NOVOMATIC Lottery Solutions (Iceland)",
        "deprecated": false
    },
    {
        "id": "BTX",
        "text": "BTT-Texas LLC",
        "deprecated": false
    },
    {
        "id": "BTY",
        "text": "Boller Technology Co., Ltd.",
        "deprecated": false
    },
    {
        "id": "BU1",
        "text": "Bulgaria (non-billable)",
        "deprecated": false
    },
    {
        "id": "BUB",
        "text": "BudgetBet",
        "deprecated": false
    },
    {
        "id": "BUG",
        "text": "D.B.A. Bullion Gaming, Co.",
        "deprecated": false
    },
    {
        "id": "BUL",
        "text": "BULLWHACKERS",
        "deprecated": false
    },
    {
        "id": "BUM",
        "text": "Bump Worldwide Inc.",
        "deprecated": false
    },
    {
        "id": "BUR",
        "text": "Burswood Casino",
        "deprecated": false
    },
    {
        "id": "BUS",
        "text": "Betfair US LLC",
        "deprecated": false
    },
    {
        "id": "BVC",
        "text": "BROWARD VENDING",
        "deprecated": false
    },
    {
        "id": "BVR",
        "text": "Big Valley Gaming Commission",
        "deprecated": false
    },
    {
        "id": "BVT",
        "text": "Buena Vista Tribal Gaming Commission",
        "deprecated": false
    },
    {
        "id": "BWA",
        "text": "Boardwalk 1000, LLC dba Hard Rock Hotel \u0026 Casino",
        "deprecated": false
    },
    {
        "id": "BWC",
        "text": "Betworks Corporation",
        "deprecated": false
    },
    {
        "id": "BWE",
        "text": "Bally Wulff Games \u0026 Entertainment GmbH",
        "deprecated": false
    },
    {
        "id": "BWF",
        "text": "BALLY WULFF ESPAÑA SLU",
        "deprecated": false
    },
    {
        "id": "BWG",
        "text": "BetWiser Games, LLC",
        "deprecated": false
    },
    {
        "id": "BWI",
        "text": "Bwin Italia SRL",
        "deprecated": false
    },
    {
        "id": "BWN",
        "text": "Bet \u0026 Win S.r.l.",
        "deprecated": false
    },
    {
        "id": "BWP",
        "text": "Best Western Pichi\u0027s Hotel",
        "deprecated": false
    },
    {
        "id": "BWS",
        "text": "Best Western San Juan Airport Hotel Casino",
        "deprecated": false
    },
    {
        "id": "BYD",
        "text": "Beyond Gaming",
        "deprecated": false
    },
    {
        "id": "BYG",
        "text": "Beyond Games Limited",
        "deprecated": false
    },
    {
        "id": "BYI",
        "text": "Bally Interactive",
        "deprecated": false
    },
    {
        "id": "BYL",
        "text": "BYL Enterprises LLC.",
        "deprecated": false
    },
    {
        "id": "BYN",
        "text": "Banyan Gaming, Inc.",
        "deprecated": false
    },
    {
        "id": "BZL",
        "text": "bZillions",
        "deprecated": false
    },
    {
        "id": "C16",
        "text": "CASA 168 INC.",
        "deprecated": false
    },
    {
        "id": "C2G",
        "text": "C2 Gaming",
        "deprecated": false
    },
    {
        "id": "CA4",
        "text": "Casino Admiral Zeeland B.V.",
        "deprecated": false
    },
    {
        "id": "CA6",
        "text": "Casinos Austria AG",
        "deprecated": false
    },
    {
        "id": "CAA",
        "text": "Casinos Austria R\u0026D",
        "deprecated": false
    },
    {
        "id": "CAB",
        "text": "Cabazon Band of Mission Indians Gaming Control",
        "deprecated": false
    },
    {
        "id": "CAC",
        "text": "CALIFORNIA CONCEPTS",
        "deprecated": false
    },
    {
        "id": "CAD",
        "text": "CADILLAC JACK",
        "deprecated": false
    },
    {
        "id": "CAE",
        "text": "CoverAll Entertainment Inc",
        "deprecated": false
    },
    {
        "id": "CAF",
        "text": "California Secretary of State",
        "deprecated": false
    },
    {
        "id": "CAG",
        "text": "Canadian Gaming Service, LTD",
        "deprecated": false
    },
    {
        "id": "CAH",
        "text": "CAHUILLA BAND OF MIS",
        "deprecated": false
    },
    {
        "id": "CAI",
        "text": "COASTAL AMUSEMENT",
        "deprecated": false
    },
    {
        "id": "CAK",
        "text": "Castle King LLC",
        "deprecated": false
    },
    {
        "id": "CAL",
        "text": "CUMMINS-ALLISON CORP",
        "deprecated": false
    },
    {
        "id": "CAM",
        "text": "C.A.M.A",
        "deprecated": false
    },
    {
        "id": "CAN",
        "text": "CANYON CASINO",
        "deprecated": false
    },
    {
        "id": "CAO",
        "text": "Casinos Ocio SA",
        "deprecated": false
    },
    {
        "id": "CAP",
        "text": "Cali Gaming Promotion Company Limited",
        "deprecated": false
    },
    {
        "id": "CAQ",
        "text": "CASINO QUEEN",
        "deprecated": false
    },
    {
        "id": "CAR",
        "text": "CARDCO",
        "deprecated": false
    },
    {
        "id": "CAS",
        "text": "Calder Casino \u0026 Race Course",
        "deprecated": false
    },
    {
        "id": "CAT",
        "text": "Catfish Bend Casino",
        "deprecated": false
    },
    {
        "id": "CAU",
        "text": "Cahuilla Tribal Gaming Agency",
        "deprecated": false
    },
    {
        "id": "CAV",
        "text": "CASINOVATIONS",
        "deprecated": false
    },
    {
        "id": "CAW",
        "text": "Casino Admiral Waalwijk B.V.",
        "deprecated": false
    },
    {
        "id": "CAY",
        "text": "Cayetano Technologies (IOM) Ltd.",
        "deprecated": false
    },
    {
        "id": "CAZ",
        "text": "CASINO AZTAR",
        "deprecated": false
    },
    {
        "id": "CBA",
        "text": "Costa Bahia Hotel and Convention Center",
        "deprecated": false
    },
    {
        "id": "CBC",
        "text": "CYPRESS BAYOU CASINO",
        "deprecated": false
    },
    {
        "id": "CBI",
        "text": "Colorado Bureau of Investigations",
        "deprecated": false
    },
    {
        "id": "CBJ",
        "text": "CyberJ Co., Ltd.",
        "deprecated": false
    },
    {
        "id": "CBK",
        "text": "CAMPO BAND KUMEYAAY",
        "deprecated": false
    },
    {
        "id": "CBL",
        "text": "City of Burlington, North Carolina",
        "deprecated": false
    },
    {
        "id": "CBM",
        "text": "The CB Malta Corporation",
        "deprecated": false
    },
    {
        "id": "CBN",
        "text": "Canadian Bank Note Company Limited",
        "deprecated": false
    },
    {
        "id": "CBO",
        "text": "Circus Boncelles",
        "deprecated": false
    },
    {
        "id": "CBS",
        "text": "Circus Belgium S.A.",
        "deprecated": false
    },
    {
        "id": "CBT",
        "text": "CashBet",
        "deprecated": false
    },
    {
        "id": "CBU",
        "text": "CASINO BUENOS AIRES S.A. COMPAÑIA DE INVERSIONES EN ENTRETENIMIENTOS S.A- UTE",
        "deprecated": false
    },
    {
        "id": "CBV",
        "text": "Coyote Valley Gaming Commission",
        "deprecated": false
    },
    {
        "id": "CC2",
        "text": "CELLCORE LTD",
        "deprecated": false
    },
    {
        "id": "CCA",
        "text": "CARD-CASINOS AUSTRIA",
        "deprecated": false
    },
    {
        "id": "CCB",
        "text": "Cow Creek Band of Umpqua Tribe of Indians Gaming Commission",
        "deprecated": false
    },
    {
        "id": "CCC",
        "text": "COUNTER CONCEPTS CONSULTING",
        "deprecated": false
    },
    {
        "id": "CCD",
        "text": "Choctaw Casino Resort - Durant",
        "deprecated": false
    },
    {
        "id": "CCE",
        "text": "COAST TO COAST ENTMT",
        "deprecated": false
    },
    {
        "id": "CCG",
        "text": "Customized Casino Games Ltd.",
        "deprecated": false
    },
    {
        "id": "CCH",
        "text": "Capri Corporate HQ",
        "deprecated": false
    },
    {
        "id": "CCI",
        "text": "COIN CONTROL INC",
        "deprecated": false
    },
    {
        "id": "CCL",
        "text": "Creative Cloud Group Co., Ltd.",
        "deprecated": false
    },
    {
        "id": "CCN",
        "text": "Casino Controls",
        "deprecated": false
    },
    {
        "id": "CCO",
        "text": "CHICO RANCHERIA",
        "deprecated": false
    },
    {
        "id": "CCP",
        "text": "Casino Cosmopol",
        "deprecated": false
    },
    {
        "id": "CCR",
        "text": "Cache Creek Casino Resort",
        "deprecated": false
    },
    {
        "id": "CCS",
        "text": "Casino Club S.A.",
        "deprecated": false
    },
    {
        "id": "CCT",
        "text": "Cincotech s.a.",
        "deprecated": false
    },
    {
        "id": "CD1",
        "text": "Casino Data Imaging, Inc.",
        "deprecated": false
    },
    {
        "id": "CD2",
        "text": "Creative Dream Gaming, LLC",
        "deprecated": false
    },
    {
        "id": "CDA",
        "text": "Codere Argentina S.A.",
        "deprecated": false
    },
    {
        "id": "CDB",
        "text": "Colbet S.A.S.",
        "deprecated": false
    },
    {
        "id": "CDE",
        "text": "Condor Game Development LLC",
        "deprecated": false
    },
    {
        "id": "CDG",
        "text": "COLORADO DIVISION GA",
        "deprecated": false
    },
    {
        "id": "CDH",
        "text": "Casino Admiral Holland BV",
        "deprecated": false
    },
    {
        "id": "CDI",
        "text": "Churchill Downs Incorporated",
        "deprecated": false
    },
    {
        "id": "CDL",
        "text": "Casino du Lac Meyrin SA",
        "deprecated": false
    },
    {
        "id": "CDM",
        "text": "Condor Malta Ltd dba Condor Gaming",
        "deprecated": false
    },
    {
        "id": "CDN",
        "text": "Cornerstone CDN Services Ltd dba SuperSpade Games",
        "deprecated": false
    },
    {
        "id": "CDP",
        "text": "Highlight of the Game s.r.l.",
        "deprecated": false
    },
    {
        "id": "CDR",
        "text": "Coeur d\u0027Alene Resort",
        "deprecated": false
    },
    {
        "id": "CDT",
        "text": "Churchill Downs Technology Initiative Company",
        "deprecated": false
    },
    {
        "id": "CDV",
        "text": "Casino Gaming Development",
        "deprecated": false
    },
    {
        "id": "CDX",
        "text": "Casino De Montreux SA",
        "deprecated": false
    },
    {
        "id": "CEC",
        "text": "Caesars Entertainment Corp",
        "deprecated": false
    },
    {
        "id": "CEG",
        "text": "Cantor G W",
        "deprecated": false
    },
    {
        "id": "CEI",
        "text": "CHARITABLE EQUIPMENT",
        "deprecated": false
    },
    {
        "id": "CEL",
        "text": "CELEBRITY HOTEL",
        "deprecated": false
    },
    {
        "id": "CEN",
        "text": "Centurion Games Inc",
        "deprecated": false
    },
    {
        "id": "CER",
        "text": "Certus Technologies",
        "deprecated": false
    },
    {
        "id": "CES",
        "text": "CES, Inc.",
        "deprecated": false
    },
    {
        "id": "CET",
        "text": "Century Casinos, Inc.",
        "deprecated": false
    },
    {
        "id": "CEV",
        "text": "C.A. Evolvit LTD",
        "deprecated": false
    },
    {
        "id": "CEX",
        "text": "Casino Excursions Gaming Group",
        "deprecated": false
    },
    {
        "id": "CFA",
        "text": "Chi Fat Au-Yeung",
        "deprecated": false
    },
    {
        "id": "CFD",
        "text": "Confederated Tribes of Coos, Lower Umpqua and Siuslaw Indians Gaming Commission",
        "deprecated": false
    },
    {
        "id": "CFL",
        "text": "Chaudfontaine Loisirs SA",
        "deprecated": false
    },
    {
        "id": "CG1",
        "text": "Catco Gaming Inc.",
        "deprecated": false
    },
    {
        "id": "CG3",
        "text": "Casumo Games Limited",
        "deprecated": false
    },
    {
        "id": "CG4",
        "text": "Camelot Global Lottery Solutions Limited",
        "deprecated": false
    },
    {
        "id": "CGA",
        "text": "Chicago Gaming Company",
        "deprecated": false
    },
    {
        "id": "CGB",
        "text": "Camelot Global Services Limited",
        "deprecated": false
    },
    {
        "id": "CGC",
        "text": "Colorado Grande Casino",
        "deprecated": false
    },
    {
        "id": "CGD",
        "text": "Custom Game Design Inc.",
        "deprecated": false
    },
    {
        "id": "CGE",
        "text": "Casino Games \u0026 Equipment LLC",
        "deprecated": false
    },
    {
        "id": "CGG",
        "text": "Central Gaming Limited",
        "deprecated": false
    },
    {
        "id": "CGH",
        "text": "Continent Group Holdings Co. Ltd",
        "deprecated": false
    },
    {
        "id": "CGI",
        "text": "CGI GAMES MACHINES",
        "deprecated": false
    },
    {
        "id": "CGK",
        "text": "Casino Game Maker Inc",
        "deprecated": false
    },
    {
        "id": "CGL",
        "text": "CASINO GAMING, L.L.C",
        "deprecated": false
    },
    {
        "id": "CGM",
        "text": "Casino de Juego Gran Madrid Sistemas S.L.U.",
        "deprecated": false
    },
    {
        "id": "CGN",
        "text": "Compliant Gaming LLC",
        "deprecated": false
    },
    {
        "id": "CGO",
        "text": "Colville Tribal Gaming Commission",
        "deprecated": false
    },
    {
        "id": "CGP",
        "text": "Cabaret Gaming Pty Ltd",
        "deprecated": false
    },
    {
        "id": "CGR",
        "text": "CMS Group LLC",
        "deprecated": false
    },
    {
        "id": "CGS",
        "text": "Custom Gaming Solutions",
        "deprecated": false
    },
    {
        "id": "CGT",
        "text": "Cogetech S.p.A",
        "deprecated": false
    },
    {
        "id": "CGW",
        "text": "CG Technology",
        "deprecated": false
    },
    {
        "id": "CH1",
        "text": "ChartLogic, Division of Medsphere Systems Corporation",
        "deprecated": false
    },
    {
        "id": "CH2",
        "text": "Chile (non-billable)",
        "deprecated": false
    },
    {
        "id": "CH3",
        "text": "Cerberus Healthcare, LLC",
        "deprecated": false
    },
    {
        "id": "CHA",
        "text": "Clubhouse Amusements",
        "deprecated": false
    },
    {
        "id": "CHB",
        "text": "Carwell Hart \u0026 Bennett LLP",
        "deprecated": false
    },
    {
        "id": "CHC",
        "text": "Chinese Gamer International Corporation",
        "deprecated": false
    },
    {
        "id": "CHD",
        "text": "Casino Hollywood",
        "deprecated": false
    },
    {
        "id": "CHE",
        "text": "Chemehuevi Gaming Commission",
        "deprecated": false
    },
    {
        "id": "CHF",
        "text": "Championship Football LLC",
        "deprecated": false
    },
    {
        "id": "CHG",
        "text": "Chicago Gaming Concepts Inc",
        "deprecated": false
    },
    {
        "id": "CHH",
        "text": "Castle Hill Holding LLC ",
        "deprecated": false
    },
    {
        "id": "CHI",
        "text": "CHIPCO",
        "deprecated": false
    },
    {
        "id": "CHK",
        "text": "Charidimos Kondylakis",
        "deprecated": false
    },
    {
        "id": "CHL",
        "text": "Cuzco Holdings Limited",
        "deprecated": false
    },
    {
        "id": "CHM",
        "text": "CHEMEHUEVI INDIAN",
        "deprecated": false
    },
    {
        "id": "CHN",
        "text": "Chinook Gaming, LLC",
        "deprecated": false
    },
    {
        "id": "CHO",
        "text": "China Lottery Online Corporation Ltd",
        "deprecated": false
    },
    {
        "id": "CHS",
        "text": "Chukchansi Gold Resort \u0026 Casino",
        "deprecated": false
    },
    {
        "id": "CHT",
        "text": "Chitimacha Gaming Commission",
        "deprecated": false
    },
    {
        "id": "CHU",
        "text": "Chumash Casino",
        "deprecated": false
    },
    {
        "id": "CHW",
        "text": "NYX Digital Gaming (Alberta) Inc.",
        "deprecated": false
    },
    {
        "id": "CHY",
        "text": "City of Hickory, North Carolina",
        "deprecated": false
    },
    {
        "id": "CHZ",
        "text": "Chezasita Limited",
        "deprecated": false
    },
    {
        "id": "CIA",
        "text": "Cirsa Italia S.p.A.",
        "deprecated": false
    },
    {
        "id": "CIC",
        "text": "Caesars Indiana Cas.",
        "deprecated": false
    },
    {
        "id": "CID",
        "text": "Sportium Apuestas Digital, S.A.",
        "deprecated": false
    },
    {
        "id": "CIE",
        "text": "Ciesse S.r.l.",
        "deprecated": false
    },
    {
        "id": "CIG",
        "text": "Casino and Gaming Industry",
        "deprecated": false
    },
    {
        "id": "CII",
        "text": "CI INNOVATIONS INC",
        "deprecated": false
    },
    {
        "id": "CIL",
        "text": "Competition Interactive LLC",
        "deprecated": false
    },
    {
        "id": "CIM",
        "text": "Cimerron Casino",
        "deprecated": false
    },
    {
        "id": "CIN",
        "text": "Colusa Indian Community",
        "deprecated": false
    },
    {
        "id": "CIR",
        "text": "Cirsa Interactive Corp., S.L.",
        "deprecated": false
    },
    {
        "id": "CIS",
        "text": "Casino Information Services",
        "deprecated": false
    },
    {
        "id": "CIT",
        "text": "CIS Technology Inc",
        "deprecated": false
    },
    {
        "id": "CJA",
        "text": "Cool Japan Inc.",
        "deprecated": false
    },
    {
        "id": "CJI",
        "text": "Cojent Systems Inc.",
        "deprecated": false
    },
    {
        "id": "CJQ",
        "text": "CJQ Enterprises LLC",
        "deprecated": false
    },
    {
        "id": "CJZ",
        "text": "CC Gaming LLC dba Johnny Z\u0027s Casino ",
        "deprecated": false
    },
    {
        "id": "CKG",
        "text": "Cherokee Gaming Commission",
        "deprecated": false
    },
    {
        "id": "CKN",
        "text": "Chicken Ranch Gaming Commission",
        "deprecated": false
    },
    {
        "id": "CKR",
        "text": "Cusimano, Keener, Roberts, Knowles \u0026 Raley",
        "deprecated": false
    },
    {
        "id": "CKS",
        "text": "Checkraise, Inc.",
        "deprecated": false
    },
    {
        "id": "CL1",
        "text": "ClusterTech International Inc.",
        "deprecated": false
    },
    {
        "id": "CL2",
        "text": "China Center Amsterdam Airport B.V. dba Lucky24 Casino Hoofddorp",
        "deprecated": false
    },
    {
        "id": "CLA",
        "text": "Claremont Automatics LTD",
        "deprecated": false
    },
    {
        "id": "CLB",
        "text": "CLUB Gaming Pty Ltd",
        "deprecated": false
    },
    {
        "id": "CLC",
        "text": "Connecticut Lottery Corporation",
        "deprecated": false
    },
    {
        "id": "CLE",
        "text": "Cloud Entertainment, S.A. de C. V.",
        "deprecated": false
    },
    {
        "id": "CLG",
        "text": "Casino Lugano",
        "deprecated": false
    },
    {
        "id": "CLI",
        "text": "Cliff Castle Casino",
        "deprecated": false
    },
    {
        "id": "CLL",
        "text": "Colorado Lottery",
        "deprecated": false
    },
    {
        "id": "CLM",
        "text": "Fifth Street Entertainment Ltd.",
        "deprecated": false
    },
    {
        "id": "CLN",
        "text": "CliniComp International, Inc. dba CliniComp, Intl.",
        "deprecated": false
    },
    {
        "id": "CLO",
        "text": "California Lottery",
        "deprecated": false
    },
    {
        "id": "CLS",
        "text": "CALSINO INC.",
        "deprecated": false
    },
    {
        "id": "CLT",
        "text": "Cluster Tech International d.o.o.",
        "deprecated": false
    },
    {
        "id": "CLU",
        "text": "Colusa Indian Gaming Commission",
        "deprecated": false
    },
    {
        "id": "CLV",
        "text": "The Cosmopolitan Las Vegas",
        "deprecated": false
    },
    {
        "id": "CLX",
        "text": "Clonix Co., Ltd.",
        "deprecated": false
    },
    {
        "id": "CM1",
        "text": "Casino Metro at Sheraton Convention Center",
        "deprecated": false
    },
    {
        "id": "CM2",
        "text": "CM Consulting Management GmbH",
        "deprecated": false
    },
    {
        "id": "CMB",
        "text": "Combase Ltd",
        "deprecated": false
    },
    {
        "id": "CMC",
        "text": "CMU Gamin Consultant Inc.",
        "deprecated": false
    },
    {
        "id": "CME",
        "text": "CheckMate Gaming LLC",
        "deprecated": false
    },
    {
        "id": "CMF",
        "text": "CMFG Life Insurance Company",
        "deprecated": false
    },
    {
        "id": "CMG",
        "text": "CMS Group",
        "deprecated": false
    },
    {
        "id": "CMH",
        "text": "careMESH, Inc.",
        "deprecated": false
    },
    {
        "id": "CMI",
        "text": "COIN MECHANISMS",
        "deprecated": false
    },
    {
        "id": "CML",
        "text": "Camasino LTD",
        "deprecated": false
    },
    {
        "id": "CMM",
        "text": "CAMMEGH Ltd.",
        "deprecated": false
    },
    {
        "id": "CMN",
        "text": "Casinomoney",
        "deprecated": false
    },
    {
        "id": "CMO",
        "text": "CO-Gaming Limited",
        "deprecated": false
    },
    {
        "id": "CMP",
        "text": "Campo Tribal Gaming Commison",
        "deprecated": false
    },
    {
        "id": "CMR",
        "text": "Comar Inversiones S.A.",
        "deprecated": false
    },
    {
        "id": "CMS",
        "text": "COTSWOLD MICROSYSTEM",
        "deprecated": false
    },
    {
        "id": "CMT",
        "text": "Commit LLC.",
        "deprecated": false
    },
    {
        "id": "CMU",
        "text": "COINMASTER USA",
        "deprecated": false
    },
    {
        "id": "CMV",
        "text": "Casino Municipale di Benezia SpA",
        "deprecated": false
    },
    {
        "id": "CMY",
        "text": "CASINO MYKONOS",
        "deprecated": false
    },
    {
        "id": "CNA",
        "text": "Comanche Nation Gaming Commission",
        "deprecated": false
    },
    {
        "id": "CNB",
        "text": "Casino New/ Nouveau Brunswick, Canada",
        "deprecated": false
    },
    {
        "id": "CNC",
        "text": "Chickasaw Nation Office of the Gaming Commissioner (CNOGC)",
        "deprecated": false
    },
    {
        "id": "CND",
        "text": "Condado Plaza",
        "deprecated": false
    },
    {
        "id": "CNE",
        "text": "Comnet Management Corporation",
        "deprecated": false
    },
    {
        "id": "CNF",
        "text": "Confederated Tribes of Grand Ronde Gaming Commission",
        "deprecated": false
    },
    {
        "id": "CNG",
        "text": "Cherokee Casino",
        "deprecated": false
    },
    {
        "id": "CNI",
        "text": "Cocona International Ltd",
        "deprecated": false
    },
    {
        "id": "CNJ",
        "text": "Comision Nacional de Juegos de Azar (CONAJZAR)",
        "deprecated": false
    },
    {
        "id": "CNM",
        "text": "Commonwealth Casino Commission - Commonwealth of Northern Mariana Islands (CNMI)",
        "deprecated": false
    },
    {
        "id": "CNO",
        "text": "Caddo Nation of Oklahoma Gaming Commission",
        "deprecated": false
    },
    {
        "id": "CNP",
        "text": "Concept Gaming Ltd.",
        "deprecated": false
    },
    {
        "id": "CNR",
        "text": "Comanche Nation Entertainment",
        "deprecated": false
    },
    {
        "id": "CNS",
        "text": "Contest Systems",
        "deprecated": false
    },
    {
        "id": "CNT",
        "text": "Connective Games, LLC",
        "deprecated": false
    },
    {
        "id": "CNW",
        "text": "Codere Network S.p.A.",
        "deprecated": false
    },
    {
        "id": "CNX",
        "text": "Conexred",
        "deprecated": false
    },
    {
        "id": "CNY",
        "text": "Conecxys S.A.S.",
        "deprecated": false
    },
    {
        "id": "CO1",
        "text": "Condado Plaza Hotel and Casino",
        "deprecated": false
    },
    {
        "id": "CO2",
        "text": "Choctaw Gaming Commission Oklahoma",
        "deprecated": false
    },
    {
        "id": "CO3",
        "text": "Codere Online, S.A",
        "deprecated": false
    },
    {
        "id": "COA",
        "text": "Cometa Wireless",
        "deprecated": false
    },
    {
        "id": "COB",
        "text": "Colossusbets Ltd.",
        "deprecated": false
    },
    {
        "id": "COC",
        "text": "Mississippi Choctaw Gaming Commission",
        "deprecated": false
    },
    {
        "id": "COD",
        "text": "Casino at Ocean Downs",
        "deprecated": false
    },
    {
        "id": "COE",
        "text": "Corredor Empresarial",
        "deprecated": false
    },
    {
        "id": "COF",
        "text": "COM Coin Operated Machines",
        "deprecated": false
    },
    {
        "id": "COG",
        "text": "Conan Gaming",
        "deprecated": false
    },
    {
        "id": "COH",
        "text": "Casino Omaha",
        "deprecated": false
    },
    {
        "id": "COI",
        "text": "COINCO",
        "deprecated": false
    },
    {
        "id": "COK",
        "text": "COCA-COLA",
        "deprecated": false
    },
    {
        "id": "COL",
        "text": "COLLINS",
        "deprecated": false
    },
    {
        "id": "COM",
        "text": "COMPU-GAME",
        "deprecated": false
    },
    {
        "id": "CON",
        "text": "CONACA",
        "deprecated": false
    },
    {
        "id": "COP",
        "text": "COCOPAH CASINO",
        "deprecated": false
    },
    {
        "id": "COQ",
        "text": "Core Gaming Ltd",
        "deprecated": false
    },
    {
        "id": "COR",
        "text": "CORY INVESTMENTS",
        "deprecated": false
    },
    {
        "id": "COS",
        "text": "Coparma S.L.",
        "deprecated": false
    },
    {
        "id": "COT",
        "text": "Coushatta Tribal Gaming Commission",
        "deprecated": false
    },
    {
        "id": "COU",
        "text": "COUSHATTA TRIBE (LA)",
        "deprecated": false
    },
    {
        "id": "COV",
        "text": "Colville Tribal Casinos",
        "deprecated": false
    },
    {
        "id": "CP2",
        "text": "CASINOS POLAND SP Z.O.O.",
        "deprecated": false
    },
    {
        "id": "CPA",
        "text": "City Play",
        "deprecated": false
    },
    {
        "id": "CPC",
        "text": "Capecod S.r.l. a socio unico",
        "deprecated": false
    },
    {
        "id": "CPD",
        "text": "Chicago Police Department",
        "deprecated": false
    },
    {
        "id": "CPG",
        "text": "Citizen Potawatomi Gaming Commission",
        "deprecated": false
    },
    {
        "id": "CPL",
        "text": "Cato Partners Limited",
        "deprecated": false
    },
    {
        "id": "CPM",
        "text": "Complete Medical Solutions, LLC",
        "deprecated": false
    },
    {
        "id": "CPN",
        "text": "Compañia Orenes De Recreativos S.A.U.",
        "deprecated": false
    },
    {
        "id": "CPO",
        "text": "Casino Portorož d.d., prirejanje posebnih iger na srečo",
        "deprecated": false
    },
    {
        "id": "CPS",
        "text": "Cash Processing Solutions, Inc. dba CPS",
        "deprecated": false
    },
    {
        "id": "CPT",
        "text": "Cocopah Tribal Gaming Agency",
        "deprecated": false
    },
    {
        "id": "CPZ",
        "text": "CASINOS POLAND CP SP Z.O.O.",
        "deprecated": false
    },
    {
        "id": "CQ9",
        "text": "CQ9 Limited",
        "deprecated": false
    },
    {
        "id": "CQH",
        "text": "CQ Holding Company, Inc.",
        "deprecated": false
    },
    {
        "id": "CQI",
        "text": "Coquille Gaming Commission",
        "deprecated": false
    },
    {
        "id": "CR1",
        "text": "Coast Redwood Management Solutions Inc.",
        "deprecated": false
    },
    {
        "id": "CR2",
        "text": "Croatia (non-billable)",
        "deprecated": false
    },
    {
        "id": "CR3",
        "text": "Colorado River Indian Tribe Gaming Agency (non-billable)",
        "deprecated": false
    },
    {
        "id": "CR4",
        "text": "Creation Robco Inc dba Robco Gaming",
        "deprecated": false
    },
    {
        "id": "CRA",
        "text": "Coral Racing Limited",
        "deprecated": false
    },
    {
        "id": "CRB",
        "text": "Chicken Ranch Bingo and Casino",
        "deprecated": false
    },
    {
        "id": "CRC",
        "text": "CRON CORPORATION",
        "deprecated": false
    },
    {
        "id": "CRD",
        "text": "CARDINAL",
        "deprecated": false
    },
    {
        "id": "CRE",
        "text": "Curacao Real Estate B.V. dba Veneto Casino",
        "deprecated": false
    },
    {
        "id": "CRG",
        "text": "CountR GmbH",
        "deprecated": false
    },
    {
        "id": "CRI",
        "text": "Cristal Srl",
        "deprecated": false
    },
    {
        "id": "CRJ",
        "text": "Crown NJ Gaming Inc.",
        "deprecated": false
    },
    {
        "id": "CRL",
        "text": "Crown Limited",
        "deprecated": false
    },
    {
        "id": "CRM",
        "text": "Cross Media International LLC",
        "deprecated": false
    },
    {
        "id": "CRN",
        "text": "Crown Casino S.A.",
        "deprecated": false
    },
    {
        "id": "CRP",
        "text": "CompuRep PTY",
        "deprecated": false
    },
    {
        "id": "CRR",
        "text": "Comanche Red River Casino",
        "deprecated": false
    },
    {
        "id": "CRS",
        "text": "Cristaltec Spa",
        "deprecated": false
    },
    {
        "id": "CRT",
        "text": "Crown Tactic",
        "deprecated": false
    },
    {
        "id": "CRU",
        "text": "Carousel Game S.A.",
        "deprecated": false
    },
    {
        "id": "CRW",
        "text": "CROWN RIVERBOAT CSNO",
        "deprecated": false
    },
    {
        "id": "CRX",
        "text": "Codeworx 3 Pty Ltd  ",
        "deprecated": false
    },
    {
        "id": "CRY",
        "text": "Cryptologic",
        "deprecated": false
    },
    {
        "id": "CRZ",
        "text": "Creative Zone Investments Limited",
        "deprecated": false
    },
    {
        "id": "CS1",
        "text": "Casumo Services Limited",
        "deprecated": false
    },
    {
        "id": "CS2",
        "text": "China Sports Lottery Technology Group Co., Ltd.",
        "deprecated": false
    },
    {
        "id": "CSA",
        "text": "Coseres S.A.",
        "deprecated": false
    },
    {
        "id": "CSB",
        "text": "Consumer \u0026 Business Services (CBS)",
        "deprecated": false
    },
    {
        "id": "CSC",
        "text": "CASINO SOFTWARE CORP",
        "deprecated": false
    },
    {
        "id": "CSE",
        "text": "Cassava Enterprises",
        "deprecated": false
    },
    {
        "id": "CSF",
        "text": "CASINFO, LLC",
        "deprecated": false
    },
    {
        "id": "CSG",
        "text": "Codespace Gaming, Inc.",
        "deprecated": false
    },
    {
        "id": "CSH",
        "text": "Cashpoint Agentur \u0026 IT Service GmbH",
        "deprecated": false
    },
    {
        "id": "CSI",
        "text": "Coin Machine Service",
        "deprecated": false
    },
    {
        "id": "CSJ",
        "text": "CASHPOINT Solutions GmbH",
        "deprecated": false
    },
    {
        "id": "CSL",
        "text": "Crystalott Solutions Limited",
        "deprecated": false
    },
    {
        "id": "CSM",
        "text": "Cyberslotz Services Malta Ltd.",
        "deprecated": false
    },
    {
        "id": "CSN",
        "text": "ChengDu SpeedFish Network Technology Co., Ltd",
        "deprecated": false
    },
    {
        "id": "CSO",
        "text": "Casino Solutions S.A.",
        "deprecated": false
    },
    {
        "id": "CSP",
        "text": "Casinò S.p.A.",
        "deprecated": false
    },
    {
        "id": "CSR",
        "text": "Casino Rouge",
        "deprecated": false
    },
    {
        "id": "CSS",
        "text": "Circus Salones, S.L.",
        "deprecated": false
    },
    {
        "id": "CST",
        "text": "C.S.G. Software Group Limited - Organizacni Slozka",
        "deprecated": false
    },
    {
        "id": "CSY",
        "text": "CGI Informations Systems \u0026 Managment Consultants Inc.",
        "deprecated": false
    },
    {
        "id": "CSZ",
        "text": "CASINO SP. Z O.O.",
        "deprecated": false
    },
    {
        "id": "CT1",
        "text": "Cerino Trading 12 PTY LTD",
        "deprecated": false
    },
    {
        "id": "CT2",
        "text": "Connecticut Tribal (non-billable)",
        "deprecated": false
    },
    {
        "id": "CTA",
        "text": "Casino Technology AD",
        "deprecated": false
    },
    {
        "id": "CTB",
        "text": "Complete Table Games",
        "deprecated": false
    },
    {
        "id": "CTC",
        "text": "CAPITOL",
        "deprecated": false
    },
    {
        "id": "CTD",
        "text": "ComTrade d.o.o.",
        "deprecated": false
    },
    {
        "id": "CTG",
        "text": "CHEROKEE TRIBAL GAMI",
        "deprecated": false
    },
    {
        "id": "CTI",
        "text": "CIS Technology Inc.",
        "deprecated": false
    },
    {
        "id": "CTJ",
        "text": "Casino Technology JSC",
        "deprecated": false
    },
    {
        "id": "CTL",
        "text": "C2 Gaming, LLC",
        "deprecated": false
    },
    {
        "id": "CTP",
        "text": "Consorzio Toto \u0026 Partners",
        "deprecated": false
    },
    {
        "id": "CTR",
        "text": "CHARLES TOWN RACES",
        "deprecated": false
    },
    {
        "id": "CTS",
        "text": "CURRENT TECHNOLOGY",
        "deprecated": false
    },
    {
        "id": "CTT",
        "text": "Companhia de Technologia de Tai Xing Limitada",
        "deprecated": false
    },
    {
        "id": "CTV",
        "text": "Chancers Technology B.V.",
        "deprecated": false
    },
    {
        "id": "CTX",
        "text": "CTXM",
        "deprecated": false
    },
    {
        "id": "CUA",
        "text": "Repeater Bets LLC",
        "deprecated": false
    },
    {
        "id": "CUB",
        "text": "Cube Limited",
        "deprecated": false
    },
    {
        "id": "CUC",
        "text": "Colusa Casino",
        "deprecated": false
    },
    {
        "id": "CUD",
        "text": "Curacao Down Town casino",
        "deprecated": false
    },
    {
        "id": "CUI",
        "text": "Currency Systems International",
        "deprecated": false
    },
    {
        "id": "CUN",
        "text": "Curasoft Investments N.V.",
        "deprecated": false
    },
    {
        "id": "CUR",
        "text": "Curacao Gaming Control Board",
        "deprecated": false
    },
    {
        "id": "CUV",
        "text": "C\u0027Era Una Volta . . .S.R.L.",
        "deprecated": false
    },
    {
        "id": "CUY",
        "text": "CUYAPAIPE COMMUNITY",
        "deprecated": false
    },
    {
        "id": "CVA",
        "text": "Carlo Vista International Limited",
        "deprecated": false
    },
    {
        "id": "CVB",
        "text": "COYOTE VALLEY BAND",
        "deprecated": false
    },
    {
        "id": "CVG",
        "text": "Cavannah Gaming",
        "deprecated": false
    },
    {
        "id": "CVI",
        "text": "CVI TECHNOLOGY",
        "deprecated": false
    },
    {
        "id": "CVL",
        "text": "CAVALIER",
        "deprecated": false
    },
    {
        "id": "CVN",
        "text": "Casino Vision Inc.",
        "deprecated": false
    },
    {
        "id": "CVT",
        "text": "CYBERVIEW TECHNOLOGY",
        "deprecated": false
    },
    {
        "id": "CVU",
        "text": "Casino Over Under LLC",
        "deprecated": false
    },
    {
        "id": "CWA",
        "text": "Commonwealth\u0027s Attorney Sussex County, Virginia",
        "deprecated": false
    },
    {
        "id": "CWC",
        "text": "CHINOOK WINDS CASINO",
        "deprecated": false
    },
    {
        "id": "CWG",
        "text": "Cantor",
        "deprecated": false
    },
    {
        "id": "CWN",
        "text": "Crown Sydney Gaming Pty. Ltd.",
        "deprecated": false
    },
    {
        "id": "CWT",
        "text": "CW Technologies, LLC",
        "deprecated": false
    },
    {
        "id": "CWV",
        "text": "Commonwealth of Virginia, Department of General Services",
        "deprecated": false
    },
    {
        "id": "CXP",
        "text": "CommXP IT Solutions Limited",
        "deprecated": false
    },
    {
        "id": "CY1",
        "text": "Cyprus (non-billable)",
        "deprecated": false
    },
    {
        "id": "CYA",
        "text": "CyberArts",
        "deprecated": false
    },
    {
        "id": "CYB",
        "text": "CYBERDYNE",
        "deprecated": false
    },
    {
        "id": "CYC",
        "text": "Chengdu Yoyousoft Co. Ltd.",
        "deprecated": false
    },
    {
        "id": "CYE",
        "text": "Cayn Enterprises Inc.",
        "deprecated": false
    },
    {
        "id": "CYN",
        "text": "Cyan Blue International Limited",
        "deprecated": false
    },
    {
        "id": "CYS",
        "text": "Cressida Yacht Service Inc.",
        "deprecated": false
    },
    {
        "id": "CYT",
        "text": "Cyberscan Tech.",
        "deprecated": false
    },
    {
        "id": "CZ1",
        "text": "Czech Republic (non-billable)",
        "deprecated": false
    },
    {
        "id": "CZI",
        "text": "Casino Aztar Indiana",
        "deprecated": false
    },
    {
        "id": "CZT",
        "text": "CZ Trading Ltd",
        "deprecated": false
    },
    {
        "id": "CZU",
        "text": "Casino Zürichsee AG",
        "deprecated": false
    },
    {
        "id": "DAE",
        "text": "DANSK AUTOMAT EXPERT",
        "deprecated": false
    },
    {
        "id": "DAJ",
        "text": "Dania Jai-Alai",
        "deprecated": false
    },
    {
        "id": "DAK",
        "text": "DAKK ELECTRONICS",
        "deprecated": false
    },
    {
        "id": "DAL",
        "text": "Dalt",
        "deprecated": false
    },
    {
        "id": "DAM",
        "text": "Dos Anjos Martins Mendes Ribeiro Fernando Jose dba Singular Connect from Mr. Fernando Ribeiro",
        "deprecated": false
    },
    {
        "id": "DAN",
        "text": "Danbook",
        "deprecated": false
    },
    {
        "id": "DAP",
        "text": "IOC Davenport Inc",
        "deprecated": false
    },
    {
        "id": "DAR",
        "text": "Dārta SIA",
        "deprecated": false
    },
    {
        "id": "DAV",
        "text": "DAV 1",
        "deprecated": false
    },
    {
        "id": "DAW",
        "text": "Dawn Takacs d/b/a Elite Casino Events",
        "deprecated": false
    },
    {
        "id": "DAY",
        "text": "Dayco Gaming",
        "deprecated": false
    },
    {
        "id": "DBA",
        "text": "DBA KINGS GAMING",
        "deprecated": false
    },
    {
        "id": "DBI",
        "text": "Debranol Investment SA dba Autogamesys",
        "deprecated": false
    },
    {
        "id": "DBJ",
        "text": "Double Back Jack",
        "deprecated": false
    },
    {
        "id": "DBM",
        "text": "Dober Media Ltd",
        "deprecated": false
    },
    {
        "id": "DCA",
        "text": "Diversified Casino Services",
        "deprecated": false
    },
    {
        "id": "DCB",
        "text": "DIVI CARINA BAY",
        "deprecated": false
    },
    {
        "id": "DCC",
        "text": "Doe Creek Capital, LLC A Nevada LLC",
        "deprecated": false
    },
    {
        "id": "DCE",
        "text": "DC Entertainment, Inc.",
        "deprecated": false
    },
    {
        "id": "DCG",
        "text": "DCG Inc.",
        "deprecated": false
    },
    {
        "id": "DCK",
        "text": "DAVID KOELLING",
        "deprecated": false
    },
    {
        "id": "DCL",
        "text": "DC Lottery",
        "deprecated": false
    },
    {
        "id": "DCR",
        "text": "DRY CREEK RANCHERIA",
        "deprecated": false
    },
    {
        "id": "DCS",
        "text": "DIEBOLD CAMPUS SYST",
        "deprecated": false
    },
    {
        "id": "DCT",
        "text": "Idemia Identity \u0026 Security France",
        "deprecated": false
    },
    {
        "id": "DCU",
        "text": "Digitcube.com Limited",
        "deprecated": false
    },
    {
        "id": "DDC",
        "text": "Desert Development Company Ltd.",
        "deprecated": false
    },
    {
        "id": "DDE",
        "text": "Dedem S.p.A.",
        "deprecated": false
    },
    {
        "id": "DDG",
        "text": "DraftDay Gaming Group",
        "deprecated": false
    },
    {
        "id": "DDM",
        "text": "Digital Distribution Management Ibenca S.A.",
        "deprecated": false
    },
    {
        "id": "DE1",
        "text": "Dench eGaming Solutions AD",
        "deprecated": false
    },
    {
        "id": "DE2",
        "text": "Denmark (non-billable)",
        "deprecated": false
    },
    {
        "id": "DEB",
        "text": "Derby Giochi",
        "deprecated": false
    },
    {
        "id": "DEC",
        "text": "Decart Ltd",
        "deprecated": false
    },
    {
        "id": "DED",
        "text": "Delta Downs",
        "deprecated": false
    },
    {
        "id": "DEG",
        "text": "Dragon\u0027s Eye Gaming, LLC.",
        "deprecated": false
    },
    {
        "id": "DEL",
        "text": "Delaware Nation Gaming Commission",
        "deprecated": false
    },
    {
        "id": "DEM",
        "text": "Edward Demicco",
        "deprecated": false
    },
    {
        "id": "DEN",
        "text": "Den Videogiochi",
        "deprecated": false
    },
    {
        "id": "DEQ",
        "text": "DEQ SYSTEMS CORP",
        "deprecated": false
    },
    {
        "id": "DFE",
        "text": "Doug Ferrario",
        "deprecated": false
    },
    {
        "id": "DFF",
        "text": "DFI Inc.",
        "deprecated": false
    },
    {
        "id": "DFI",
        "text": "Digital Fish Software AB t/a Play\u0027N Go",
        "deprecated": false
    },
    {
        "id": "DFK",
        "text": "Draftkings, Inc.",
        "deprecated": false
    },
    {
        "id": "DFN",
        "text": "Data Financial, Inc.",
        "deprecated": false
    },
    {
        "id": "DFS",
        "text": "DUE F SRL",
        "deprecated": false
    },
    {
        "id": "DFU",
        "text": "Dean Fuerbringer",
        "deprecated": false
    },
    {
        "id": "DG1",
        "text": "Dumarca Gaming Limited",
        "deprecated": false
    },
    {
        "id": "DG2",
        "text": "Digitain LLC",
        "deprecated": false
    },
    {
        "id": "DGA",
        "text": "Dutch Gaming Authority, Kansspelautoriteit (KSA)",
        "deprecated": false
    },
    {
        "id": "DGC",
        "text": "Digital Gaming Corporation",
        "deprecated": false
    },
    {
        "id": "DGE",
        "text": "Diamond Game Enterprises",
        "deprecated": false
    },
    {
        "id": "DGF",
        "text": "Digital Foundry",
        "deprecated": false
    },
    {
        "id": "DGJ",
        "text": "Dirección General de Juegos y Sorteos ",
        "deprecated": false
    },
    {
        "id": "DGL",
        "text": "Diamond Gaming LLC",
        "deprecated": false
    },
    {
        "id": "DGM",
        "text": "David Group (Macau) Ltd",
        "deprecated": false
    },
    {
        "id": "DGN",
        "text": "DGN Games LLC",
        "deprecated": false
    },
    {
        "id": "DGP",
        "text": "Dubuque Greyhound",
        "deprecated": false
    },
    {
        "id": "DGR",
        "text": "De Nationale Grote Clubactie B.V.",
        "deprecated": false
    },
    {
        "id": "DGS",
        "text": "DISCOVERY GAMING",
        "deprecated": false
    },
    {
        "id": "DGT",
        "text": "Dicetown Gaming Technologies, Inc.",
        "deprecated": false
    },
    {
        "id": "DHC",
        "text": "DreamHolder Corp.",
        "deprecated": false
    },
    {
        "id": "DHP",
        "text": "Dooley Herr Pedersen and Berglund Bailey LLP",
        "deprecated": false
    },
    {
        "id": "DIA",
        "text": "Diamond Team Ltd.",
        "deprecated": false
    },
    {
        "id": "DIC",
        "text": "Dickinson Wright LLP",
        "deprecated": false
    },
    {
        "id": "DIF",
        "text": "Ditronics Financial Services, LLC",
        "deprecated": false
    },
    {
        "id": "DIG",
        "text": "DigiDeal Corporation",
        "deprecated": false
    },
    {
        "id": "DIS",
        "text": "Discovery S.R.L.",
        "deprecated": false
    },
    {
        "id": "DIT",
        "text": "Dusane Infotech India Pvt Ltd",
        "deprecated": false
    },
    {
        "id": "DIV",
        "text": "Diversien S.A.S.",
        "deprecated": false
    },
    {
        "id": "DIX",
        "text": "DIXIE-NARCO",
        "deprecated": false
    },
    {
        "id": "DJA",
        "text": "Diamond Jacks Casino",
        "deprecated": false
    },
    {
        "id": "DJC",
        "text": "Diamond Jo Casino",
        "deprecated": false
    },
    {
        "id": "DJH",
        "text": "Dynam Japan Holdings Co.,Ltd.",
        "deprecated": false
    },
    {
        "id": "DJW",
        "text": "Diamond Jo Worth",
        "deprecated": false
    },
    {
        "id": "DLG",
        "text": "Double Luck Gaming Company",
        "deprecated": false
    },
    {
        "id": "DLI",
        "text": "Deltronic Labs Inc",
        "deprecated": false
    },
    {
        "id": "DLL",
        "text": "Diamond Link Limited",
        "deprecated": false
    },
    {
        "id": "DLR",
        "text": "Del Lago Resort \u0026 Casino",
        "deprecated": false
    },
    {
        "id": "DLS",
        "text": "Danske Licens Spil A/S",
        "deprecated": false
    },
    {
        "id": "DLT",
        "text": "De Lotto",
        "deprecated": false
    },
    {
        "id": "DLV",
        "text": "SIA DLV",
        "deprecated": false
    },
    {
        "id": "DMI",
        "text": "Darelle Media Inc.",
        "deprecated": false
    },
    {
        "id": "DML",
        "text": "Democracy Live",
        "deprecated": false
    },
    {
        "id": "DMN",
        "text": "Diamond Mountain Casino",
        "deprecated": false
    },
    {
        "id": "DMO",
        "text": "Dennis Morrision",
        "deprecated": false
    },
    {
        "id": "DMS",
        "text": "Daking Management Support, Inc.",
        "deprecated": false
    },
    {
        "id": "DNA",
        "text": "DeLaRue North America",
        "deprecated": false
    },
    {
        "id": "DND",
        "text": "Detail \u0026 Design",
        "deprecated": false
    },
    {
        "id": "DNF",
        "text": "DnF24 srl",
        "deprecated": false
    },
    {
        "id": "DNG",
        "text": "Danger Room Gaming, B.V.",
        "deprecated": false
    },
    {
        "id": "DNK",
        "text": "Dansk Underholding Limited",
        "deprecated": false
    },
    {
        "id": "DNM",
        "text": "Denim SRL",
        "deprecated": false
    },
    {
        "id": "DNT",
        "text": "Digient Technologies",
        "deprecated": false
    },
    {
        "id": "DOA",
        "text": "Digital Orchid Applications de Mexico S. de R.L. de C.V.",
        "deprecated": false
    },
    {
        "id": "DOG",
        "text": "Merkur Dosniha, S.L.",
        "deprecated": false
    },
    {
        "id": "DOJ",
        "text": "California Department of Justice",
        "deprecated": false
    },
    {
        "id": "DOL",
        "text": "Desarrollo Online Juegos Regulatos SA",
        "deprecated": false
    },
    {
        "id": "DON",
        "text": "Donira AS",
        "deprecated": false
    },
    {
        "id": "DOS",
        "text": "Dosniha S.L.",
        "deprecated": false
    },
    {
        "id": "DPC",
        "text": "Diamond Palace Hotel Casino",
        "deprecated": false
    },
    {
        "id": "DPI",
        "text": "Demax DPI PLC",
        "deprecated": false
    },
    {
        "id": "DPK",
        "text": "Dennis Pastirik",
        "deprecated": false
    },
    {
        "id": "DPL",
        "text": "Draftpot LLC",
        "deprecated": false
    },
    {
        "id": "DPS",
        "text": "DP Company S.R.L. dba Dalla Pria",
        "deprecated": false
    },
    {
        "id": "DRA",
        "text": "DreamIT GmbH",
        "deprecated": false
    },
    {
        "id": "DRG",
        "text": "DR Gaming Technology dba DRGT Systems GmbH",
        "deprecated": false
    },
    {
        "id": "DRI",
        "text": "Dribble Media Limited",
        "deprecated": false
    },
    {
        "id": "DRK",
        "text": "Dr. Karlheinz Krebs",
        "deprecated": false
    },
    {
        "id": "DRM",
        "text": "DREAMPORT, INC.",
        "deprecated": false
    },
    {
        "id": "DRS",
        "text": "Dreams Technologies LTD trading as Lottotech ",
        "deprecated": false
    },
    {
        "id": "DRT",
        "text": "DR Technologies",
        "deprecated": false
    },
    {
        "id": "DS1",
        "text": "Darena Solutions LLC",
        "deprecated": false
    },
    {
        "id": "DSA",
        "text": "Dynamite S.A.",
        "deprecated": false
    },
    {
        "id": "DSC",
        "text": "D\u0026S Computer Technologies LLC",
        "deprecated": false
    },
    {
        "id": "DSE",
        "text": "DS Electronics",
        "deprecated": false
    },
    {
        "id": "DSL",
        "text": "Delaware State Lottery",
        "deprecated": false
    },
    {
        "id": "DSM",
        "text": "Datastream Media LLC",
        "deprecated": false
    },
    {
        "id": "DSR",
        "text": "Downstream Casino Resort",
        "deprecated": false
    },
    {
        "id": "DSS",
        "text": "DS Software GmbH",
        "deprecated": false
    },
    {
        "id": "DSW",
        "text": "Digital Software Limited",
        "deprecated": false
    },
    {
        "id": "DTA",
        "text": "D.A.T.A. Games S.p.a.",
        "deprecated": false
    },
    {
        "id": "DTE",
        "text": "Dazzle Tag Entertainment",
        "deprecated": false
    },
    {
        "id": "DTF",
        "text": "Department of Treasury and Finance Tasmania",
        "deprecated": false
    },
    {
        "id": "DTG",
        "text": "Desert Gaming",
        "deprecated": false
    },
    {
        "id": "DTL",
        "text": "DIGITAL GAMING SOLUT",
        "deprecated": false
    },
    {
        "id": "DTM",
        "text": "DocToMe, Inc.",
        "deprecated": false
    },
    {
        "id": "DTV",
        "text": "DemTech Voting, Inc.",
        "deprecated": false
    },
    {
        "id": "DUE",
        "text": "DueGi s.r.l.",
        "deprecated": false
    },
    {
        "id": "DUG",
        "text": "Dunow Gaming LLC",
        "deprecated": false
    },
    {
        "id": "DUK",
        "text": "Design Works Gaming (UK) Ltd",
        "deprecated": false
    },
    {
        "id": "DUR",
        "text": "JOE DURBIN",
        "deprecated": false
    },
    {
        "id": "DVS",
        "text": "Dominion Voting Systems ",
        "deprecated": false
    },
    {
        "id": "DWG",
        "text": "Design Works Gaming",
        "deprecated": false
    },
    {
        "id": "DWR",
        "text": "Dee Why RSL Club",
        "deprecated": false
    },
    {
        "id": "DYG",
        "text": "Dynamic Gaming Systems LLC",
        "deprecated": false
    },
    {
        "id": "DYS",
        "text": "Dynamic System Limited",
        "deprecated": false
    },
    {
        "id": "DZT",
        "text": "Dozengame Technology (International) Pte. Ltd.",
        "deprecated": false
    },
    {
        "id": "EAC",
        "text": "European Amusement Company",
        "deprecated": false
    },
    {
        "id": "EAG",
        "text": "Escor Automaten AG",
        "deprecated": false
    },
    {
        "id": "EAI",
        "text": "Electronic Arts Inc.",
        "deprecated": false
    },
    {
        "id": "EAK",
        "text": "EAC Kft.",
        "deprecated": false
    },
    {
        "id": "EAL",
        "text": "Eaton Gate Gaming Ltd",
        "deprecated": false
    },
    {
        "id": "EAS",
        "text": "Eastern Shoshone Gaming Agency",
        "deprecated": false
    },
    {
        "id": "EBE",
        "text": "eBet Gaming Systems Limited",
        "deprecated": false
    },
    {
        "id": "EBG",
        "text": "EvenBet Gaming Limited",
        "deprecated": false
    },
    {
        "id": "EBO",
        "text": "EUROBOX s.a.",
        "deprecated": false
    },
    {
        "id": "EC1",
        "text": "Ensico CMS d.o.o.",
        "deprecated": false
    },
    {
        "id": "ECA",
        "text": "Elite Casino Resorts, LLC",
        "deprecated": false
    },
    {
        "id": "ECC",
        "text": "Etowah County Commission",
        "deprecated": false
    },
    {
        "id": "ECD",
        "text": "EC Development, Inc.",
        "deprecated": false
    },
    {
        "id": "ECF",
        "text": "Electronic Commerce Factory, S.L.U.",
        "deprecated": false
    },
    {
        "id": "ECG",
        "text": "Eastern Cape Gambling and Gaming Board",
        "deprecated": false
    },
    {
        "id": "ECH",
        "text": "Empress Casino \u0026 Hotel",
        "deprecated": false
    },
    {
        "id": "ECI",
        "text": "Hollywood Casino Joliet",
        "deprecated": false
    },
    {
        "id": "ECL",
        "text": "Eclipse Gaming Systems",
        "deprecated": false
    },
    {
        "id": "ECM",
        "text": "Electro Chance SRL (Mexico)",
        "deprecated": false
    },
    {
        "id": "ECN",
        "text": "Eyecon",
        "deprecated": false
    },
    {
        "id": "ECR",
        "text": "El Conquistador Reseort",
        "deprecated": false
    },
    {
        "id": "ECS",
        "text": "Electro Chance S.R.L.",
        "deprecated": false
    },
    {
        "id": "EDC",
        "text": "EDC",
        "deprecated": false
    },
    {
        "id": "EDE",
        "text": "Edict Egaming GmbH",
        "deprecated": false
    },
    {
        "id": "EDM",
        "text": "edict Malta Ltd",
        "deprecated": false
    },
    {
        "id": "EDN",
        "text": "Endon Technologies Limited",
        "deprecated": false
    },
    {
        "id": "EDP",
        "text": "Endorphina Limited",
        "deprecated": false
    },
    {
        "id": "EDR",
        "text": "Ed Roberts S.A.",
        "deprecated": false
    },
    {
        "id": "EDV",
        "text": "Edelian Development Limited",
        "deprecated": false
    },
    {
        "id": "EEB",
        "text": "Evoplay Entertainment B.V.",
        "deprecated": false
    },
    {
        "id": "EEC",
        "text": "Essex Electronic Consultants Ltd.",
        "deprecated": false
    },
    {
        "id": "EEG",
        "text": "The Star Entertainment Group Limited",
        "deprecated": false
    },
    {
        "id": "EES",
        "text": "ELECTEC Election Services, Inc",
        "deprecated": false
    },
    {
        "id": "EFF",
        "text": "Effiteech S.A.S",
        "deprecated": false
    },
    {
        "id": "EFN",
        "text": "e-Fun sp. z o.o.",
        "deprecated": false
    },
    {
        "id": "EG2",
        "text": "Evento Gioco 2 SRL",
        "deprecated": false
    },
    {
        "id": "EG4",
        "text": "EGC Services Ltd",
        "deprecated": false
    },
    {
        "id": "EGB",
        "text": "Eurocoin Gaming B.V.",
        "deprecated": false
    },
    {
        "id": "EGC",
        "text": "EAGLE GAMING CORPORATION",
        "deprecated": false
    },
    {
        "id": "EGD",
        "text": "Exon Group Desarrollos Informaticos S.L.",
        "deprecated": false
    },
    {
        "id": "EGE",
        "text": "EUROPEAN GAME \u0026 ENT.",
        "deprecated": false
    },
    {
        "id": "EGG",
        "text": "Empire Global Gaming Inc.",
        "deprecated": false
    },
    {
        "id": "EGH",
        "text": "Egghart LLC CPAs",
        "deprecated": false
    },
    {
        "id": "EGI",
        "text": "EGT Interactive Ltd.",
        "deprecated": false
    },
    {
        "id": "EGL",
        "text": "Elec Games Alderney Limited",
        "deprecated": false
    },
    {
        "id": "EGM",
        "text": "EsGaming S.A.",
        "deprecated": false
    },
    {
        "id": "EGN",
        "text": "E-Genting Sdn Bhd",
        "deprecated": false
    },
    {
        "id": "EGO",
        "text": "Easygo Solutions Pty Limited",
        "deprecated": false
    },
    {
        "id": "EGS",
        "text": "ELECTRONIC GAME SOLU",
        "deprecated": false
    },
    {
        "id": "EGT",
        "text": "Entertainment Gaming Asia, Inc.",
        "deprecated": false
    },
    {
        "id": "EGX",
        "text": "Europa Games s.r.l.",
        "deprecated": false
    },
    {
        "id": "EHL",
        "text": "Eastern Hawaii Leisure Company LTD.",
        "deprecated": false
    },
    {
        "id": "EHR",
        "text": "TeamEHR, Inc.",
        "deprecated": false
    },
    {
        "id": "EHW",
        "text": "Eastern Hawaii",
        "deprecated": false
    },
    {
        "id": "EIB",
        "text": "Electronica Informatica Brasil",
        "deprecated": false
    },
    {
        "id": "EIE",
        "text": "Empresa Industrial del Estado Administradora del Monopolio Restístico del Juego-Coljuegos",
        "deprecated": false
    },
    {
        "id": "EIG",
        "text": "East Industries Group Limited",
        "deprecated": false
    },
    {
        "id": "EIL",
        "text": "Everest IP Limited",
        "deprecated": false
    },
    {
        "id": "EIN",
        "text": "Eurocoin Interactive B.V.",
        "deprecated": false
    },
    {
        "id": "EIO",
        "text": "EMPIRE (IOM) LIMITED",
        "deprecated": false
    },
    {
        "id": "EJO",
        "text": "Eric Johnson",
        "deprecated": false
    },
    {
        "id": "EKS",
        "text": "Elk Studios AB",
        "deprecated": false
    },
    {
        "id": "EL1",
        "text": "EU Lotto Ltd",
        "deprecated": false
    },
    {
        "id": "EL2",
        "text": "ELS Gaming, LLC",
        "deprecated": false
    },
    {
        "id": "ELA",
        "text": "Elam Group Electronic Amusement B.V.",
        "deprecated": false
    },
    {
        "id": "ELB",
        "text": "Elmac di Tiziano Tredese",
        "deprecated": false
    },
    {
        "id": "ELC",
        "text": "Electronic Center S.r.l.",
        "deprecated": false
    },
    {
        "id": "ELD",
        "text": "El Dorado Resort and Casino",
        "deprecated": false
    },
    {
        "id": "ELE",
        "text": "Interblock d.d.",
        "deprecated": false
    },
    {
        "id": "ELK",
        "text": "ELK VALLEY RANCHERIA",
        "deprecated": false
    },
    {
        "id": "ELM",
        "text": "ELEM INDIAN COLONY",
        "deprecated": false
    },
    {
        "id": "ELR",
        "text": "Elsirel Limited",
        "deprecated": false
    },
    {
        "id": "ELS",
        "text": "ELSYM",
        "deprecated": false
    },
    {
        "id": "ELT",
        "text": "Elaut NV",
        "deprecated": false
    },
    {
        "id": "ELV",
        "text": "Elettronica Video Games S.r.l.",
        "deprecated": false
    },
    {
        "id": "EMA",
        "text": "e-Magine Gaming Corporation",
        "deprecated": false
    },
    {
        "id": "EMC",
        "text": "EAGLE MT. CASINO",
        "deprecated": false
    },
    {
        "id": "EMI",
        "text": "Emil Interactive Games LLC dba Draft Ops LLC",
        "deprecated": false
    },
    {
        "id": "EMP",
        "text": "Empire Games",
        "deprecated": false
    },
    {
        "id": "EMR",
        "text": "EMR-Bear, LLC",
        "deprecated": false
    },
    {
        "id": "EMS",
        "text": "EGT MULTIPLAYER LTD",
        "deprecated": false
    },
    {
        "id": "EMX",
        "text": "EveryMatrix Limited",
        "deprecated": false
    },
    {
        "id": "ENA",
        "text": "Engage Atlantic",
        "deprecated": false
    },
    {
        "id": "ENC",
        "text": "Navajo Gaming Regulatory Office",
        "deprecated": false
    },
    {
        "id": "END",
        "text": "Endemol",
        "deprecated": false
    },
    {
        "id": "ENG",
        "text": "Ensico Gaming d.o.o.",
        "deprecated": false
    },
    {
        "id": "ENH",
        "text": "Engaged Nation Holdings LLC ",
        "deprecated": false
    },
    {
        "id": "ENJ",
        "text": "Entretenimientos Y Juegos De Azar S.A. (EnJASA)",
        "deprecated": false
    },
    {
        "id": "ENL",
        "text": "Enerlogik",
        "deprecated": false
    },
    {
        "id": "ENR",
        "text": "Exacta Systems, LLC",
        "deprecated": false
    },
    {
        "id": "ENS",
        "text": "Enchanted Sun",
        "deprecated": false
    },
    {
        "id": "ENT",
        "text": "Entertasia",
        "deprecated": false
    },
    {
        "id": "ENV",
        "text": "Ezugi NV",
        "deprecated": false
    },
    {
        "id": "EOE",
        "text": "Ebingo Online España S.A.",
        "deprecated": false
    },
    {
        "id": "EPA",
        "text": "Ellis Park",
        "deprecated": false
    },
    {
        "id": "EPC",
        "text": "ELICON Μ.Ι.Κ.Ε. / Elicon Single Member PC",
        "deprecated": false
    },
    {
        "id": "EPG",
        "text": "Emporio Games S.r.l.",
        "deprecated": false
    },
    {
        "id": "EPL",
        "text": "E-Play24 Limited",
        "deprecated": false
    },
    {
        "id": "EPS",
        "text": "Epic Software, LLC.",
        "deprecated": false
    },
    {
        "id": "EPT",
        "text": "ESAT PILIPINAS TEKNIK, OPC",
        "deprecated": false
    },
    {
        "id": "EPZ",
        "text": "HelloWorld, Inc.",
        "deprecated": false
    },
    {
        "id": "EQL",
        "text": "Equilotttery LLC",
        "deprecated": false
    },
    {
        "id": "EQU",
        "text": "eQube",
        "deprecated": false
    },
    {
        "id": "ERC",
        "text": "Elwha River Casino",
        "deprecated": false
    },
    {
        "id": "ERE",
        "text": "Eldorado Resorts Inc.",
        "deprecated": false
    },
    {
        "id": "ERL",
        "text": "ERREL INDUSTRIES",
        "deprecated": false
    },
    {
        "id": "ERM",
        "text": "Errin Miller",
        "deprecated": false
    },
    {
        "id": "ERN",
        "text": "Ern Enterprises, Inc.",
        "deprecated": false
    },
    {
        "id": "ERO",
        "text": "Eclipse Route Operations, LLC dba Eclipse Gaming",
        "deprecated": false
    },
    {
        "id": "ES1",
        "text": "Estoril Sol Digital",
        "deprecated": false
    },
    {
        "id": "ES2",
        "text": "Estonia (non-billable)",
        "deprecated": false
    },
    {
        "id": "ES3",
        "text": "Spain (non-billable)",
        "deprecated": false
    },
    {
        "id": "ESA",
        "text": "El San Juan Hotel \u0026 Casino",
        "deprecated": false
    },
    {
        "id": "ESB",
        "text": "Extreme Sports Betting",
        "deprecated": false
    },
    {
        "id": "ESC",
        "text": "Eastern Shawnee Gaming Commission",
        "deprecated": false
    },
    {
        "id": "ESD",
        "text": "Embassy Suites Dorado del Mar Resort",
        "deprecated": false
    },
    {
        "id": "ESF",
        "text": "EG Software Limited dba EventBet Gaming",
        "deprecated": false
    },
    {
        "id": "ESG",
        "text": "Ehlers Star Galleries",
        "deprecated": false
    },
    {
        "id": "ESI",
        "text": "ESI- E-Success, Inc.",
        "deprecated": false
    },
    {
        "id": "ESJ",
        "text": "Embassy Suites San Juan ",
        "deprecated": false
    },
    {
        "id": "ESL",
        "text": "Emphasis Services Limited",
        "deprecated": false
    },
    {
        "id": "ESN",
        "text": "Eclectic Gaming Solutions LTD. ",
        "deprecated": false
    },
    {
        "id": "ESO",
        "text": "eGame Solutions (Holdings) Limited",
        "deprecated": false
    },
    {
        "id": "ESP",
        "text": "Estrada Polska Sp. z o.o.",
        "deprecated": false
    },
    {
        "id": "ESR",
        "text": "El San Juan Resort \u0026 Casino",
        "deprecated": false
    },
    {
        "id": "ESS",
        "text": "ELECTRO SYSTEM SPA",
        "deprecated": false
    },
    {
        "id": "EST",
        "text": "Eastern Shawnee Tribe of Oklahoma",
        "deprecated": false
    },
    {
        "id": "ESY",
        "text": "Election System \u0026 Software",
        "deprecated": false
    },
    {
        "id": "ET1",
        "text": "E Total Gaming S.A.S.",
        "deprecated": false
    },
    {
        "id": "ETA",
        "text": "ETA Gaming",
        "deprecated": false
    },
    {
        "id": "ETB",
        "text": "Etable Games Inc",
        "deprecated": false
    },
    {
        "id": "ETC",
        "text": "ELECTROCOIN AUTOMAT",
        "deprecated": false
    },
    {
        "id": "ETE",
        "text": "Energetic Technology Co., Ltd.",
        "deprecated": false
    },
    {
        "id": "ETG",
        "text": "Empire Technological Group Limited",
        "deprecated": false
    },
    {
        "id": "ETI",
        "text": "ETI",
        "deprecated": false
    },
    {
        "id": "ETL",
        "text": "etoylab",
        "deprecated": false
    },
    {
        "id": "ETN",
        "text": "eTabs Inc",
        "deprecated": false
    },
    {
        "id": "ETP",
        "text": "Entwine Technology PTE LTD",
        "deprecated": false
    },
    {
        "id": "ETR",
        "text": "Entera",
        "deprecated": false
    },
    {
        "id": "ETS",
        "text": "Empresa Territorial para la Salud",
        "deprecated": false
    },
    {
        "id": "ETT",
        "text": "ETT Gaming Systems",
        "deprecated": false
    },
    {
        "id": "ETU",
        "text": "Enterra, Inc",
        "deprecated": false
    },
    {
        "id": "ETX",
        "text": "Etix",
        "deprecated": false
    },
    {
        "id": "EUA",
        "text": "Eurasian Gaming Limited",
        "deprecated": false
    },
    {
        "id": "EUB",
        "text": "Eurobed S.r.l.",
        "deprecated": false
    },
    {
        "id": "EUF",
        "text": "Eurofootball Limited",
        "deprecated": false
    },
    {
        "id": "EUG",
        "text": "EURO GRUPPO GIOCHI",
        "deprecated": false
    },
    {
        "id": "EUI",
        "text": "Eurobet Italia S.R.L.",
        "deprecated": false
    },
    {
        "id": "EUM",
        "text": "Euro Matic",
        "deprecated": false
    },
    {
        "id": "EUR",
        "text": "Eurobet",
        "deprecated": false
    },
    {
        "id": "EUS",
        "text": "Eurostar s.r.l.",
        "deprecated": false
    },
    {
        "id": "EUT",
        "text": "Euro Games Technology",
        "deprecated": false
    },
    {
        "id": "EVA",
        "text": "Evangeline Downs RT\u0026C",
        "deprecated": false
    },
    {
        "id": "EVC",
        "text": "ELK VALLEY CASINO",
        "deprecated": false
    },
    {
        "id": "EVE",
        "text": "EVONA Electronic s.r.o.",
        "deprecated": false
    },
    {
        "id": "EVG",
        "text": "Evolution Gaming",
        "deprecated": false
    },
    {
        "id": "EVI",
        "text": "Everi Interactive",
        "deprecated": false
    },
    {
        "id": "EVL",
        "text": "Evolution Latvia SIA",
        "deprecated": false
    },
    {
        "id": "EVM",
        "text": "Evolution Malta Ltd.",
        "deprecated": false
    },
    {
        "id": "EVO",
        "text": "Evolution Gaming International",
        "deprecated": false
    },
    {
        "id": "EVP",
        "text": "Evoplay LLP",
        "deprecated": false
    },
    {
        "id": "EWA",
        "text": "E-Wallet Africa Limited",
        "deprecated": false
    },
    {
        "id": "EWG",
        "text": "Extra Wagers LLC",
        "deprecated": false
    },
    {
        "id": "EXG",
        "text": "Extragiochi di Ciofi s.r.l.",
        "deprecated": false
    },
    {
        "id": "EXN",
        "text": "Expekt Nordics Limited",
        "deprecated": false
    },
    {
        "id": "EXP",
        "text": "Experiencias Xcaret S.A. de C.V.",
        "deprecated": false
    },
    {
        "id": "EXT",
        "text": "EXTREME TEK,LLC",
        "deprecated": false
    },
    {
        "id": "F2S",
        "text": "F2 SYSTEMS INC.",
        "deprecated": false
    },
    {
        "id": "FAB",
        "text": "First American Bankcard, Inc aka FabiCash",
        "deprecated": false
    },
    {
        "id": "FAF",
        "text": "FantasyFeud",
        "deprecated": false
    },
    {
        "id": "FAI",
        "text": "Full Advantage Investments Limited",
        "deprecated": false
    },
    {
        "id": "FAL",
        "text": "Fairluck",
        "deprecated": false
    },
    {
        "id": "FAN",
        "text": "FanDuel",
        "deprecated": false
    },
    {
        "id": "FAP",
        "text": "Favourit Australia PTY LTD",
        "deprecated": false
    },
    {
        "id": "FAV",
        "text": "Favola Srl",
        "deprecated": false
    },
    {
        "id": "FAZ",
        "text": "Fazi D.O.O",
        "deprecated": false
    },
    {
        "id": "FBC",
        "text": "Fortune Bay Casino Resort",
        "deprecated": false
    },
    {
        "id": "FBD",
        "text": "Faegre Baker Daniels LLP",
        "deprecated": false
    },
    {
        "id": "FBE",
        "text": "Fort Belknap Casino, Inc.",
        "deprecated": false
    },
    {
        "id": "FBR",
        "text": "FL Department of Business and Professional Regulation",
        "deprecated": false
    },
    {
        "id": "FBT",
        "text": "F.B. Tecnicos Asociados S.A.",
        "deprecated": false
    },
    {
        "id": "FBZ",
        "text": "Fortuna dba Bet Zone SRL",
        "deprecated": false
    },
    {
        "id": "FCA",
        "text": "Flamingo Casino Alkmaar B.V.",
        "deprecated": false
    },
    {
        "id": "FCG",
        "text": "Flower City Gaming, LLC",
        "deprecated": false
    },
    {
        "id": "FCH",
        "text": "Funding Change",
        "deprecated": false
    },
    {
        "id": "FCN",
        "text": "Four Corners Inc.",
        "deprecated": false
    },
    {
        "id": "FCO",
        "text": "Full Color Games, Inc.",
        "deprecated": false
    },
    {
        "id": "FCT",
        "text": "Fifty Cats Ltd",
        "deprecated": false
    },
    {
        "id": "FDC",
        "text": "FDC Computer Services",
        "deprecated": false
    },
    {
        "id": "FDH",
        "text": "Florida Department of Health, Office of Information Technology, Bureau of Clinic Management \u0026 Informatics",
        "deprecated": false
    },
    {
        "id": "FDR",
        "text": "FantasyDraft, LLC",
        "deprecated": false
    },
    {
        "id": "FDS",
        "text": "Foundstone",
        "deprecated": false
    },
    {
        "id": "FDT",
        "text": "Flagler Dog Track and Sports\r\n",
        "deprecated": false
    },
    {
        "id": "FEA",
        "text": "Fertitta Acquisitionsco LLC",
        "deprecated": false
    },
    {
        "id": "FEC",
        "text": "FORTUNE ENTERTAIN",
        "deprecated": false
    },
    {
        "id": "FER",
        "text": "Feral Electronics S.A.C.",
        "deprecated": false
    },
    {
        "id": "FEU",
        "text": "Funfair Technologies Europe Limited",
        "deprecated": false
    },
    {
        "id": "FFC",
        "text": "FEATHER FALLS CASINO",
        "deprecated": false
    },
    {
        "id": "FGA",
        "text": "Fantasy Sports Gaming Limited",
        "deprecated": false
    },
    {
        "id": "FGC",
        "text": "FITNESS GAMING CORP",
        "deprecated": false
    },
    {
        "id": "FGE",
        "text": "Future Gaming Europe AB",
        "deprecated": false
    },
    {
        "id": "FGI",
        "text": "Frankel Gaming, Inc.",
        "deprecated": false
    },
    {
        "id": "FHC",
        "text": "Flamingo Hilton Casino",
        "deprecated": false
    },
    {
        "id": "FHT",
        "text": "Full House Tracking",
        "deprecated": false
    },
    {
        "id": "FIF",
        "text": "Folkspel Ideell Förening",
        "deprecated": false
    },
    {
        "id": "FIG",
        "text": "Federated Indians of Graton Rancheria Tribal Gaming ",
        "deprecated": false
    },
    {
        "id": "FIL",
        "text": "Filco Limited",
        "deprecated": false
    },
    {
        "id": "FIS",
        "text": "Financial Services Authority (Seychelles)",
        "deprecated": false
    },
    {
        "id": "FIT",
        "text": "Fils Italia Srl",
        "deprecated": false
    },
    {
        "id": "FKC",
        "text": "Firekeepers Casino ",
        "deprecated": false
    },
    {
        "id": "FLB",
        "text": "Flightbet LLC",
        "deprecated": false
    },
    {
        "id": "FLC",
        "text": "French Lick Casino Indiana",
        "deprecated": false
    },
    {
        "id": "FLD",
        "text": "Flushed Game LLC",
        "deprecated": false
    },
    {
        "id": "FLE",
        "text": "Flash Entertainment Limited",
        "deprecated": false
    },
    {
        "id": "FLG",
        "text": "Florida Gaming Centers Inc",
        "deprecated": false
    },
    {
        "id": "FLL",
        "text": "Florida Lottery",
        "deprecated": false
    },
    {
        "id": "FLO",
        "text": "FotoLotto AS",
        "deprecated": false
    },
    {
        "id": "FLS",
        "text": "Flash Game SRL",
        "deprecated": false
    },
    {
        "id": "FLT",
        "text": "Four Leaf Technology",
        "deprecated": false
    },
    {
        "id": "FMD",
        "text": "FlagshipMD",
        "deprecated": false
    },
    {
        "id": "FMG",
        "text": "FM Gaming LLC",
        "deprecated": false
    },
    {
        "id": "FMI",
        "text": "Fleetwood Gaming, Inc.",
        "deprecated": false
    },
    {
        "id": "FMT",
        "text": "Fort McDowell Tribal Gaming Commission",
        "deprecated": false
    },
    {
        "id": "FNC",
        "text": "Fincore Ltd.",
        "deprecated": false
    },
    {
        "id": "FNL",
        "text": "FantasyNetwork Ltd.",
        "deprecated": false
    },
    {
        "id": "FNP",
        "text": "Finnplay (VT)",
        "deprecated": false
    },
    {
        "id": "FOB",
        "text": "FRANK O\u0027BRIEN",
        "deprecated": false
    },
    {
        "id": "FOC",
        "text": "FOCUS",
        "deprecated": false
    },
    {
        "id": "FOR",
        "text": "FortuneTeller Gaming, Inc.",
        "deprecated": false
    },
    {
        "id": "FOT",
        "text": "Fortuna Game A.S.",
        "deprecated": false
    },
    {
        "id": "FP1",
        "text": "Four Points By Sheraton Caguas Real",
        "deprecated": false
    },
    {
        "id": "FP2",
        "text": "Four Points by Sheraton Palmas del Mar",
        "deprecated": false
    },
    {
        "id": "FPG",
        "text": "The Four Point Group",
        "deprecated": false
    },
    {
        "id": "FPL",
        "text": "FingerPlay Limited",
        "deprecated": false
    },
    {
        "id": "FPS",
        "text": "Funplaza Spijkenisse B.V.",
        "deprecated": false
    },
    {
        "id": "FRA",
        "text": "Recreativos Franco S.A.",
        "deprecated": false
    },
    {
        "id": "FRC",
        "text": "Fire Rock Casino",
        "deprecated": false
    },
    {
        "id": "FRP",
        "text": "France Pari SAS",
        "deprecated": false
    },
    {
        "id": "FRT",
        "text": "Fort Mojave Indian Tribe",
        "deprecated": false
    },
    {
        "id": "FS1",
        "text": "FSB Technology (UK) Ltd",
        "deprecated": false
    },
    {
        "id": "FS2",
        "text": "Flandreau Santee Sioux Tribe",
        "deprecated": false
    },
    {
        "id": "FSA",
        "text": "Office of Florida State Attorney",
        "deprecated": false
    },
    {
        "id": "FSB",
        "text": "Flemington Sportsbet",
        "deprecated": false
    },
    {
        "id": "FSC",
        "text": "Flandreau Santee Gaming Commission",
        "deprecated": false
    },
    {
        "id": "FSG",
        "text": "FIVE STAR GAMING",
        "deprecated": false
    },
    {
        "id": "FSH",
        "text": "Five Star Holdings, LLC",
        "deprecated": false
    },
    {
        "id": "FSI",
        "text": "Fabamaq Sistemas Informaticos Lda",
        "deprecated": false
    },
    {
        "id": "FSN",
        "text": "Fair Share Nederland BV",
        "deprecated": false
    },
    {
        "id": "FSO",
        "text": "FSport AB",
        "deprecated": false
    },
    {
        "id": "FSP",
        "text": "Fantasy Sports Global Limited",
        "deprecated": false
    },
    {
        "id": "FSR",
        "text": "FIVE STAR REDEMPTION",
        "deprecated": false
    },
    {
        "id": "FSS",
        "text": "Fantasy Sports Shark, LLC",
        "deprecated": false
    },
    {
        "id": "FST",
        "text": "Fiesta Systems Ltd",
        "deprecated": false
    },
    {
        "id": "FSV",
        "text": "Folkspel i Sverige AB",
        "deprecated": false
    },
    {
        "id": "FSZ",
        "text": "FORGAME Sp. z o.o.",
        "deprecated": false
    },
    {
        "id": "FTB",
        "text": "Football 1x2",
        "deprecated": false
    },
    {
        "id": "FTG",
        "text": "Foot Traffic Gaming LLC",
        "deprecated": false
    },
    {
        "id": "FTN",
        "text": "FortuNet",
        "deprecated": false
    },
    {
        "id": "FTR",
        "text": "Fort Randall Casino \u0026 Hotel",
        "deprecated": false
    },
    {
        "id": "FTS",
        "text": "Fortiss LLC",
        "deprecated": false
    },
    {
        "id": "FUG",
        "text": "Future Gate s.u.r.l.",
        "deprecated": false
    },
    {
        "id": "FUL",
        "text": "Taiwan Fulgent Ent",
        "deprecated": false
    },
    {
        "id": "FUN",
        "text": "Funny Games S.A.",
        "deprecated": false
    },
    {
        "id": "FUT",
        "text": "Futurelogic, LLC",
        "deprecated": false
    },
    {
        "id": "FUZ",
        "text": "Fuzemedia",
        "deprecated": false
    },
    {
        "id": "FVU",
        "text": "Fivebet \u0026 Vincitù S.r.l. Unipersonale",
        "deprecated": false
    },
    {
        "id": "FWC",
        "text": "Four Winds New Buffalo",
        "deprecated": false
    },
    {
        "id": "FWV",
        "text": "“Fresh Wind V.V.” LLC",
        "deprecated": false
    },
    {
        "id": "FXW",
        "text": "FOXWOODS",
        "deprecated": false
    },
    {
        "id": "G1M",
        "text": "Gaming1 Malta",
        "deprecated": false
    },
    {
        "id": "G2G",
        "text": "G2 Gaming, Inc.",
        "deprecated": false
    },
    {
        "id": "G36",
        "text": "Game360 S.r.l.",
        "deprecated": false
    },
    {
        "id": "G3A",
        "text": "G3 Asia Limited",
        "deprecated": false
    },
    {
        "id": "G8B",
        "text": "Great 8 LLC",
        "deprecated": false
    },
    {
        "id": "GAA",
        "text": "GameArt Ltd.",
        "deprecated": false
    },
    {
        "id": "GAB",
        "text": "Galaxy Bingo",
        "deprecated": false
    },
    {
        "id": "GAC",
        "text": "Everi Payments, Inc.",
        "deprecated": false
    },
    {
        "id": "GAD",
        "text": "Gage Data, SL",
        "deprecated": false
    },
    {
        "id": "GAE",
        "text": "G.A.M.E Gaming Advancement Marketing Entertainment LLC",
        "deprecated": false
    },
    {
        "id": "GAF",
        "text": "Gaming Foundation",
        "deprecated": false
    },
    {
        "id": "GAG",
        "text": "Game Account Global Limited ",
        "deprecated": false
    },
    {
        "id": "GAH",
        "text": "Great American Holdings, Inc.",
        "deprecated": false
    },
    {
        "id": "GAI",
        "text": "Gaming Informatics, LLC.",
        "deprecated": false
    },
    {
        "id": "GAL",
        "text": "Gaming Arts, LLC",
        "deprecated": false
    },
    {
        "id": "GAM",
        "text": "GAMMA",
        "deprecated": false
    },
    {
        "id": "GAN",
        "text": "GameCo LLC",
        "deprecated": false
    },
    {
        "id": "GAO",
        "text": "Gaming Solution Technology S.R.L.",
        "deprecated": false
    },
    {
        "id": "GAP",
        "text": "Galaxy Products \u0026 Services, Inc",
        "deprecated": false
    },
    {
        "id": "GAR",
        "text": "GameArt, LLC ",
        "deprecated": false
    },
    {
        "id": "GAS",
        "text": "GAMEART, SRL",
        "deprecated": false
    },
    {
        "id": "GAT",
        "text": "GLI AFRICA TEST",
        "deprecated": false
    },
    {
        "id": "GAU",
        "text": "Grupo Amusgo, s. De R.L\u003e DE C.V.",
        "deprecated": false
    },
    {
        "id": "GAX",
        "text": "GRUPO AYEX S.A.",
        "deprecated": false
    },
    {
        "id": "GAY",
        "text": "Gamesys Limited",
        "deprecated": false
    },
    {
        "id": "GBB",
        "text": "Golden Boys Bet",
        "deprecated": false
    },
    {
        "id": "GBC",
        "text": "Golden Beach Casino N.V.",
        "deprecated": false
    },
    {
        "id": "GBE",
        "text": "Global Betting Exchange",
        "deprecated": false
    },
    {
        "id": "GBG",
        "text": "Gamblit Gaming LLC",
        "deprecated": false
    },
    {
        "id": "GBK",
        "text": "Griffin, Bodi \u0026 Krause, Inc",
        "deprecated": false
    },
    {
        "id": "GBL",
        "text": "Gambling S.A.",
        "deprecated": false
    },
    {
        "id": "GBM",
        "text": "Greentube Malta Limited",
        "deprecated": false
    },
    {
        "id": "GBS",
        "text": "Gobsmacked Loyalty Pty Ltd dba Gobsmacked Entertainment",
        "deprecated": false
    },
    {
        "id": "GBT",
        "text": "Gaming Board of Tanzania",
        "deprecated": false
    },
    {
        "id": "GBV",
        "text": "Gaming Support B.V.",
        "deprecated": false
    },
    {
        "id": "GBY",
        "text": "Gameboy SRL",
        "deprecated": false
    },
    {
        "id": "GC1",
        "text": "Grand Casino St. Gallen AG",
        "deprecated": false
    },
    {
        "id": "GC2",
        "text": "GC2 Incorporated",
        "deprecated": false
    },
    {
        "id": "GCB",
        "text": "Game Castle B.V.",
        "deprecated": false
    },
    {
        "id": "GCC",
        "text": "Great Canadian Casinos Inc.",
        "deprecated": false
    },
    {
        "id": "GCD",
        "text": "Best Gold Bet D.O.O",
        "deprecated": false
    },
    {
        "id": "GCG",
        "text": "GOLD CIRCLE GROUP",
        "deprecated": false
    },
    {
        "id": "GCH",
        "text": "Gold Circle Horse Racing and Betting",
        "deprecated": false
    },
    {
        "id": "GCL",
        "text": "Gran Casino Lelystad B.V.",
        "deprecated": false
    },
    {
        "id": "GCN",
        "text": "Great Canadian Gaming Corporation",
        "deprecated": false
    },
    {
        "id": "GCO",
        "text": "GeoComply",
        "deprecated": false
    },
    {
        "id": "GCS",
        "text": "Gaming Control Systems",
        "deprecated": false
    },
    {
        "id": "GCT",
        "text": "Game Changing Technologies Inc dba",
        "deprecated": false
    },
    {
        "id": "GCU",
        "text": "Genting Casinos UK Limited",
        "deprecated": false
    },
    {
        "id": "GCY",
        "text": "Global Casino Supply",
        "deprecated": false
    },
    {
        "id": "GDA",
        "text": "Giesecke \u0026 Devrient America Inc.",
        "deprecated": false
    },
    {
        "id": "GDF",
        "text": "Global Daily Fantasy Sports Inc.",
        "deprecated": false
    },
    {
        "id": "GDI",
        "text": "Gioco Digitale Italian SRL",
        "deprecated": false
    },
    {
        "id": "GDL",
        "text": "GLobal Draw Limited",
        "deprecated": false
    },
    {
        "id": "GDP",
        "text": "Giesecke and Devrient Asia Pacific Ltd. ",
        "deprecated": false
    },
    {
        "id": "GDR",
        "text": "SoftQuo Holding Ltd",
        "deprecated": false
    },
    {
        "id": "GDS",
        "text": "GRINDSTONE RANCHERIA",
        "deprecated": false
    },
    {
        "id": "GDT",
        "text": "Generator Digital Technology Co.,Ltd.",
        "deprecated": false
    },
    {
        "id": "GDY",
        "text": "Gaming Dynamics, LLC. ",
        "deprecated": false
    },
    {
        "id": "GEB",
        "text": "Games and Betting S.A.S.",
        "deprecated": false
    },
    {
        "id": "GEC",
        "text": "Grey Eagle Resort and Casino",
        "deprecated": false
    },
    {
        "id": "GEE",
        "text": "Greentube Malta SEE Ltd.",
        "deprecated": false
    },
    {
        "id": "GEG",
        "text": "Golden Eagle Gaming",
        "deprecated": false
    },
    {
        "id": "GEI",
        "text": "GAMING ENTER. INC",
        "deprecated": false
    },
    {
        "id": "GEL",
        "text": "Georgia Lottery",
        "deprecated": false
    },
    {
        "id": "GEM",
        "text": "Oryx Gaming International",
        "deprecated": false
    },
    {
        "id": "GEN",
        "text": "Genesis Software International Ltd.",
        "deprecated": false
    },
    {
        "id": "GEO",
        "text": "George Spivey",
        "deprecated": false
    },
    {
        "id": "GEP",
        "text": "GEPA Games SRL",
        "deprecated": false
    },
    {
        "id": "GER",
        "text": "Geraldine Fischer",
        "deprecated": false
    },
    {
        "id": "GES",
        "text": "Gaming Energy Sol.",
        "deprecated": false
    },
    {
        "id": "GET",
        "text": "Gest Service S.r.l.",
        "deprecated": false
    },
    {
        "id": "GEU",
        "text": "Gem Solutions S A S",
        "deprecated": false
    },
    {
        "id": "GEW",
        "text": "Gelwins Group LLC",
        "deprecated": false
    },
    {
        "id": "GFA",
        "text": "The Game Factory LLC",
        "deprecated": false
    },
    {
        "id": "GFH",
        "text": "IGT Foreign Holdings Corporation Sucursal Colombia",
        "deprecated": false
    },
    {
        "id": "GFI",
        "text": "GFI Gaming LLC",
        "deprecated": false
    },
    {
        "id": "GFL",
        "text": "Gamefloor 360, Inc",
        "deprecated": false
    },
    {
        "id": "GFM",
        "text": "Garfum.com Inc.",
        "deprecated": false
    },
    {
        "id": "GG1",
        "text": "GGL",
        "deprecated": false
    },
    {
        "id": "GG2",
        "text": "Gore Gaming",
        "deprecated": false
    },
    {
        "id": "GG3",
        "text": "Galaxy Group Ltd",
        "deprecated": false
    },
    {
        "id": "GG4",
        "text": "Gitchi Gaming, Inc.",
        "deprecated": false
    },
    {
        "id": "GG5",
        "text": "Genlot Game Technology Co., Ltd.",
        "deprecated": false
    },
    {
        "id": "GGA",
        "text": "Global Gaming Industries",
        "deprecated": false
    },
    {
        "id": "GGB",
        "text": "Gauteng Gambling Board",
        "deprecated": false
    },
    {
        "id": "GGC",
        "text": "GOLDEN GATE CASINO",
        "deprecated": false
    },
    {
        "id": "GGD",
        "text": "Genesis Global Limited",
        "deprecated": false
    },
    {
        "id": "GGE",
        "text": "Goodwill Gaming Enterprises Ltd.",
        "deprecated": false
    },
    {
        "id": "GGG",
        "text": "Global Gaming Group",
        "deprecated": false
    },
    {
        "id": "GGI",
        "text": "GREAT GAMES, INC.",
        "deprecated": false
    },
    {
        "id": "GGL",
        "text": "Gateway Gaming LLC",
        "deprecated": false
    },
    {
        "id": "GGM",
        "text": "Galaxy Gaming Of Mo.",
        "deprecated": false
    },
    {
        "id": "GGN",
        "text": "GG International Ltd",
        "deprecated": false
    },
    {
        "id": "GGO",
        "text": "Giant Goose Co., Ltd.",
        "deprecated": false
    },
    {
        "id": "GGP",
        "text": "Gilpin Givhan PC",
        "deprecated": false
    },
    {
        "id": "GGR",
        "text": "Gaming Group LLC.",
        "deprecated": false
    },
    {
        "id": "GGS",
        "text": "GEI Games SRL",
        "deprecated": false
    },
    {
        "id": "GGT",
        "text": "Gametek Global PTY LTD",
        "deprecated": false
    },
    {
        "id": "GGW",
        "text": "GableGotwals",
        "deprecated": false
    },
    {
        "id": "GGY",
        "text": "Gaming Authority of Guyana",
        "deprecated": false
    },
    {
        "id": "GHA",
        "text": "Garry A. Hamud",
        "deprecated": false
    },
    {
        "id": "GHG",
        "text": "Golden Horseshoe Gaming Enterprise",
        "deprecated": false
    },
    {
        "id": "GHS",
        "text": "Gaming Hosp. Solutio",
        "deprecated": false
    },
    {
        "id": "GI1",
        "text": "Gi Tech Gaming Company India Private Limited",
        "deprecated": false
    },
    {
        "id": "GIA",
        "text": "Gioia Systems LLC",
        "deprecated": false
    },
    {
        "id": "GIC",
        "text": "Gila River Indian Community Gaming Commission",
        "deprecated": false
    },
    {
        "id": "GID",
        "text": "Gaming Inspectorate Department",
        "deprecated": false
    },
    {
        "id": "GIE",
        "text": "Gieffe s.r.l.",
        "deprecated": false
    },
    {
        "id": "GIG",
        "text": "GIOG s.r.l.",
        "deprecated": false
    },
    {
        "id": "GII",
        "text": "Games International, Inc.",
        "deprecated": false
    },
    {
        "id": "GIL",
        "text": "Gaming Inc Limited",
        "deprecated": false
    },
    {
        "id": "GIM",
        "text": "GIGames, S.L.",
        "deprecated": false
    },
    {
        "id": "GIN",
        "text": "Games Incorporated Ltd.",
        "deprecated": false
    },
    {
        "id": "GIO",
        "text": "Gioca Live Limited",
        "deprecated": false
    },
    {
        "id": "GIP",
        "text": "GAMING INSIGHT PLC.",
        "deprecated": false
    },
    {
        "id": "GIR",
        "text": "Gila River Gaming Enterprises, Inc.",
        "deprecated": false
    },
    {
        "id": "GIS",
        "text": "Global Investment Services",
        "deprecated": false
    },
    {
        "id": "GIT",
        "text": "Gilelorn Infotech",
        "deprecated": false
    },
    {
        "id": "GIX",
        "text": "Giamax Gaming Ltd",
        "deprecated": false
    },
    {
        "id": "GK1",
        "text": "Geokul Limited",
        "deprecated": false
    },
    {
        "id": "GLA",
        "text": "Buzz Group Limited",
        "deprecated": false
    },
    {
        "id": "GLB",
        "text": "Global Starnet Limited",
        "deprecated": false
    },
    {
        "id": "GLC",
        "text": "Gamelogic",
        "deprecated": false
    },
    {
        "id": "GLD",
        "text": "Golden Gaming Inc",
        "deprecated": false
    },
    {
        "id": "GLE",
        "text": "Gears of Leo AB",
        "deprecated": false
    },
    {
        "id": "GLG",
        "text": "Global Innovative Gaming LLC dba G.I. Gaming",
        "deprecated": false
    },
    {
        "id": "GLH",
        "text": "GLC NV (Gaming Luna Chip)",
        "deprecated": false
    },
    {
        "id": "GLI",
        "text": "GLI, LLC",
        "deprecated": false
    },
    {
        "id": "GLL",
        "text": "GLL Consulting",
        "deprecated": false
    },
    {
        "id": "GLM",
        "text": "Gold Moon Info-Tech Solutions",
        "deprecated": false
    },
    {
        "id": "GLN",
        "text": "GLENVIEW SYSTEMS INC",
        "deprecated": false
    },
    {
        "id": "GLO",
        "text": "Glory Global Solutions, Inc.",
        "deprecated": false
    },
    {
        "id": "GLP",
        "text": "Global Payment Technologies (Pty) Ltd",
        "deprecated": false
    },
    {
        "id": "GLR",
        "text": "G.L. Intrattenimenti S.R.L",
        "deprecated": false
    },
    {
        "id": "GLS",
        "text": "Globus Sistemas S.A.S.",
        "deprecated": false
    },
    {
        "id": "GLT",
        "text": "Global Tronic SIA",
        "deprecated": false
    },
    {
        "id": "GLU",
        "text": "GI.LU.PI. S.r.l.",
        "deprecated": false
    },
    {
        "id": "GLV",
        "text": "Global View Software Ltd.",
        "deprecated": false
    },
    {
        "id": "GLX",
        "text": "Gamelix s.r.l.",
        "deprecated": false
    },
    {
        "id": "GLY",
        "text": "Gloria Cohaesio Technology Co.,Ltd",
        "deprecated": false
    },
    {
        "id": "GM1",
        "text": "GML Interactive Ltd",
        "deprecated": false
    },
    {
        "id": "GM2",
        "text": "Gao Mei Group Limited",
        "deprecated": false
    },
    {
        "id": "GMA",
        "text": "Game Account Network",
        "deprecated": false
    },
    {
        "id": "GMB",
        "text": "G. Matica SRL",
        "deprecated": false
    },
    {
        "id": "GMC",
        "text": "GOLDEN MOON CASINO",
        "deprecated": false
    },
    {
        "id": "GMD",
        "text": "Game Media Networks S.r.l",
        "deprecated": false
    },
    {
        "id": "GME",
        "text": "GMP Electronics SRL",
        "deprecated": false
    },
    {
        "id": "GMG",
        "text": "Gameologic Ltd",
        "deprecated": false
    },
    {
        "id": "GMH",
        "text": "GM Game Machine S.r.l.",
        "deprecated": false
    },
    {
        "id": "GMI",
        "text": "Gaming Machine Test Institute GMTI",
        "deprecated": false
    },
    {
        "id": "GML",
        "text": "Gama Technology Limited ",
        "deprecated": false
    },
    {
        "id": "GMM",
        "text": "Gameiom Technologies Limited",
        "deprecated": false
    },
    {
        "id": "GMN",
        "text": "Game Network S.r.l.",
        "deprecated": false
    },
    {
        "id": "GMO",
        "text": "GEMACO",
        "deprecated": false
    },
    {
        "id": "GMP",
        "text": "Gran Melia Puerto Rico",
        "deprecated": false
    },
    {
        "id": "GMR",
        "text": "Gaming Media Group Limited",
        "deprecated": false
    },
    {
        "id": "GMS",
        "text": "G.M. Srl",
        "deprecated": false
    },
    {
        "id": "GMT",
        "text": "Gametronics",
        "deprecated": false
    },
    {
        "id": "GMV",
        "text": "Gaming Venture",
        "deprecated": false
    },
    {
        "id": "GMX",
        "text": "GAMMAX",
        "deprecated": false
    },
    {
        "id": "GMZ",
        "text": "Gamanza Solutions Limited",
        "deprecated": false
    },
    {
        "id": "GN1",
        "text": "GAMENET S.P.A.",
        "deprecated": false
    },
    {
        "id": "GNA",
        "text": "GSN Gamies Inc",
        "deprecated": false
    },
    {
        "id": "GNB",
        "text": "Golden Nugget Casino - Biloxi",
        "deprecated": false
    },
    {
        "id": "GND",
        "text": "Grand Casino Resort",
        "deprecated": false
    },
    {
        "id": "GNG",
        "text": "GNS Gaming Ltd",
        "deprecated": false
    },
    {
        "id": "GNI",
        "text": "Gaming Network, Inc.",
        "deprecated": false
    },
    {
        "id": "GNL",
        "text": "Gun Lake Casino",
        "deprecated": false
    },
    {
        "id": "GNM",
        "text": "Ganapati (Malta) Limited",
        "deprecated": false
    },
    {
        "id": "GNN",
        "text": "Genera Networks",
        "deprecated": false
    },
    {
        "id": "GNO",
        "text": "Ganlot Inc.",
        "deprecated": false
    },
    {
        "id": "GNR",
        "text": "Ginar Limited",
        "deprecated": false
    },
    {
        "id": "GNS",
        "text": "Gaming \u0026 Systems S.A. DE C.V",
        "deprecated": false
    },
    {
        "id": "GNT",
        "text": "Gaming Network Solutions LLC",
        "deprecated": false
    },
    {
        "id": "GNU",
        "text": "Golden Nugget Lake Charles",
        "deprecated": false
    },
    {
        "id": "GNV",
        "text": "GAMES OF NEVADA",
        "deprecated": false
    },
    {
        "id": "GNW",
        "text": "Gaming Northwest LLC",
        "deprecated": false
    },
    {
        "id": "GNZ",
        "text": "Generation Z, LLC",
        "deprecated": false
    },
    {
        "id": "GOA",
        "text": "Goalgaming S.A.",
        "deprecated": false
    },
    {
        "id": "GOB",
        "text": "Golden Bell s.r.I",
        "deprecated": false
    },
    {
        "id": "GOC",
        "text": "GAMES OF CHANCE",
        "deprecated": false
    },
    {
        "id": "GOE",
        "text": "Goldmoon Enterprise Ltd.",
        "deprecated": false
    },
    {
        "id": "GOG",
        "text": "Gordon Gaming",
        "deprecated": false
    },
    {
        "id": "GOI",
        "text": "Goldensky International Group Inc.",
        "deprecated": false
    },
    {
        "id": "GOL",
        "text": "Goldcash Investments Limited",
        "deprecated": false
    },
    {
        "id": "GOM",
        "text": "Golden Monarch Limited",
        "deprecated": false
    },
    {
        "id": "GOP",
        "text": "Go Public LTD",
        "deprecated": false
    },
    {
        "id": "GOR",
        "text": "Golden Ring Poker Ltd.",
        "deprecated": false
    },
    {
        "id": "GOS",
        "text": "Golden Software Group S.A.S.",
        "deprecated": false
    },
    {
        "id": "GPE",
        "text": "Gaming Policy and Enforcement Branch",
        "deprecated": false
    },
    {
        "id": "GPG",
        "text": "Global Payments Gaming Services Inc ",
        "deprecated": false
    },
    {
        "id": "GPI",
        "text": "GAMING PARTNERS INTL.",
        "deprecated": false
    },
    {
        "id": "GPL",
        "text": "Gameplay Interactive Limited",
        "deprecated": false
    },
    {
        "id": "GPN",
        "text": "GILPIN CASINO in CO",
        "deprecated": false
    },
    {
        "id": "GPP",
        "text": "Green Pyramid Productions LLC",
        "deprecated": false
    },
    {
        "id": "GPR",
        "text": "Grand Products",
        "deprecated": false
    },
    {
        "id": "GPS",
        "text": "Game Point Software Services",
        "deprecated": false
    },
    {
        "id": "GPT",
        "text": "GLOBAL PAYMENT TECH",
        "deprecated": false
    },
    {
        "id": "GPY",
        "text": "Game Play Network Inc",
        "deprecated": false
    },
    {
        "id": "GR1",
        "text": "Gaming Realms PLC",
        "deprecated": false
    },
    {
        "id": "GR2",
        "text": "Greece (non-billable)",
        "deprecated": false
    },
    {
        "id": "GR3",
        "text": "Gromada Games LTD",
        "deprecated": false
    },
    {
        "id": "GRA",
        "text": "Grand Lake Casino",
        "deprecated": false
    },
    {
        "id": "GRB",
        "text": "GRG Banking",
        "deprecated": false
    },
    {
        "id": "GRC",
        "text": "Gold Reef City",
        "deprecated": false
    },
    {
        "id": "GRD",
        "text": "GRAND RONDE CASINO",
        "deprecated": false
    },
    {
        "id": "GRE",
        "text": "Greatbet Limited",
        "deprecated": false
    },
    {
        "id": "GRG",
        "text": "GR GAMES SRL",
        "deprecated": false
    },
    {
        "id": "GRH",
        "text": "Mauritius Gambling Regulatory Authority",
        "deprecated": false
    },
    {
        "id": "GRI",
        "text": "GRIPS",
        "deprecated": false
    },
    {
        "id": "GRL",
        "text": "Greentube Alderney Ltd",
        "deprecated": false
    },
    {
        "id": "GRM",
        "text": "Gold Rush Management Group",
        "deprecated": false
    },
    {
        "id": "GRN",
        "text": "Graton Resort and Casino",
        "deprecated": false
    },
    {
        "id": "GRO",
        "text": "Grover Gaming",
        "deprecated": false
    },
    {
        "id": "GRP",
        "text": "GRAND PALAIS",
        "deprecated": false
    },
    {
        "id": "GRR",
        "text": "Grand Sierra Resort and Casino",
        "deprecated": false
    },
    {
        "id": "GRS",
        "text": "Grand Spiral Corporation",
        "deprecated": false
    },
    {
        "id": "GRT",
        "text": "Greentube Internet Entertainment Solutions, GmbH",
        "deprecated": false
    },
    {
        "id": "GRU",
        "text": "Gruppo Idea Srl",
        "deprecated": false
    },
    {
        "id": "GRV",
        "text": "Grand Virtual",
        "deprecated": false
    },
    {
        "id": "GRW",
        "text": "Growth Machine Limited",
        "deprecated": false
    },
    {
        "id": "GS1",
        "text": "Genius Solutions, Inc.",
        "deprecated": false
    },
    {
        "id": "GS2",
        "text": "Game Solution S.r.l.",
        "deprecated": false
    },
    {
        "id": "GS3",
        "text": "Game System Technology Gaming S.R.L. dba GST Gaming S.R.L.",
        "deprecated": false
    },
    {
        "id": "GS4",
        "text": "Gamesys Spain, PLC",
        "deprecated": false
    },
    {
        "id": "GSA",
        "text": "GSA (Gaming Standards Association)",
        "deprecated": false
    },
    {
        "id": "GSC",
        "text": "Gambling Supervision Commission",
        "deprecated": false
    },
    {
        "id": "GSD",
        "text": "International Gaming Solutions Comercio e Desenvolvimen",
        "deprecated": false
    },
    {
        "id": "GSE",
        "text": "Gobsmacked Marketing Pty Ltd dba GSL Solutions",
        "deprecated": false
    },
    {
        "id": "GSG",
        "text": "Genesis Gaming Solutions Inc",
        "deprecated": false
    },
    {
        "id": "GSK",
        "text": "Gameshark Consulting",
        "deprecated": false
    },
    {
        "id": "GSL",
        "text": "Gamelia S.r.l.",
        "deprecated": false
    },
    {
        "id": "GSM",
        "text": "Government of Sint Maarten",
        "deprecated": false
    },
    {
        "id": "GSN",
        "text": "Gaming Solutions International SAC",
        "deprecated": false
    },
    {
        "id": "GSO",
        "text": "Gaming Solutions Inc.",
        "deprecated": false
    },
    {
        "id": "GSP",
        "text": "Gulfstream Park",
        "deprecated": false
    },
    {
        "id": "GSR",
        "text": "Grupposette SRL",
        "deprecated": false
    },
    {
        "id": "GSS",
        "text": "Game Systems Inc.",
        "deprecated": false
    },
    {
        "id": "GST",
        "text": "Gaming Support USA, Inc.",
        "deprecated": false
    },
    {
        "id": "GSU",
        "text": "GameScale Europe Ltd.",
        "deprecated": false
    },
    {
        "id": "GSV",
        "text": "GVC Services Limited",
        "deprecated": false
    },
    {
        "id": "GSW",
        "text": "GAMES WEST",
        "deprecated": false
    },
    {
        "id": "GSY",
        "text": "Gaming Synergies",
        "deprecated": false
    },
    {
        "id": "GT1",
        "text": "GT",
        "deprecated": false
    },
    {
        "id": "GT2",
        "text": "GTS",
        "deprecated": false
    },
    {
        "id": "GT3",
        "text": "Green Telecom (T) Limited",
        "deprecated": false
    },
    {
        "id": "GT4",
        "text": "GT SOLUTIONS S.A.S",
        "deprecated": false
    },
    {
        "id": "GTA",
        "text": "G.T.A. s.r.l.",
        "deprecated": false
    },
    {
        "id": "GTB",
        "text": "Grand Traverse Band of Ottawa and Chippewa Indians Gaming Commission",
        "deprecated": false
    },
    {
        "id": "GTC",
        "text": "Games Technical Services G.T.S. S.A.",
        "deprecated": false
    },
    {
        "id": "GTD",
        "text": "G2 Database Marketing",
        "deprecated": false
    },
    {
        "id": "GTE",
        "text": "IGT Global Solutions Corporation",
        "deprecated": false
    },
    {
        "id": "GTG",
        "text": "Gaming Technology Group",
        "deprecated": false
    },
    {
        "id": "GTH",
        "text": "Vector Gaming, Inc.",
        "deprecated": false
    },
    {
        "id": "GTI",
        "text": "GAME TECH",
        "deprecated": false
    },
    {
        "id": "GTL",
        "text": "Game-Tec Labs Inc",
        "deprecated": false
    },
    {
        "id": "GTM",
        "text": "IGT Mexico Lottery, S. De R.L. de C.V.",
        "deprecated": false
    },
    {
        "id": "GTN",
        "text": "Gametime of Nevada LLC",
        "deprecated": false
    },
    {
        "id": "GTP",
        "text": "Greenberg Traurig, P.A",
        "deprecated": false
    },
    {
        "id": "GTT",
        "text": "Gustavo Toja",
        "deprecated": false
    },
    {
        "id": "GTU",
        "text": "Greentube (Gibraltar) Limited",
        "deprecated": false
    },
    {
        "id": "GTY",
        "text": "Geneity Limited",
        "deprecated": false
    },
    {
        "id": "GUB",
        "text": "Glucks- und Unterhaltungsspiel Betriebsges.m.b.H.",
        "deprecated": false
    },
    {
        "id": "GUK",
        "text": "IGT UK Interactive Limited",
        "deprecated": false
    },
    {
        "id": "GUL",
        "text": "Gulf Coast Gaming",
        "deprecated": false
    },
    {
        "id": "GUN",
        "text": "Gun Lake Tribal Commission",
        "deprecated": false
    },
    {
        "id": "GV1",
        "text": "Grand Victoria Casino and Resort",
        "deprecated": false
    },
    {
        "id": "GVC",
        "text": "Grand Victoria Casin",
        "deprecated": false
    },
    {
        "id": "GVG",
        "text": "Grand Vision Gaming, LLC",
        "deprecated": false
    },
    {
        "id": "GVL",
        "text": "G-Virtuals Limited",
        "deprecated": false
    },
    {
        "id": "GVR",
        "text": "GREENVILLE RANCHERIA",
        "deprecated": false
    },
    {
        "id": "GVV",
        "text": "GVV Holdings (Pty) Ltd",
        "deprecated": false
    },
    {
        "id": "GW1",
        "text": "G-World Ltd",
        "deprecated": false
    },
    {
        "id": "GWA",
        "text": "Games Warehouse Limited",
        "deprecated": false
    },
    {
        "id": "GWC",
        "text": "Gaming \u0026 Wagering Commission of Western Australia",
        "deprecated": false
    },
    {
        "id": "GWL",
        "text": "Gameway Ltd.",
        "deprecated": false
    },
    {
        "id": "GXS",
        "text": "Giomax S.RL.",
        "deprecated": false
    },
    {
        "id": "GXY",
        "text": "Galaxy Casino SA",
        "deprecated": false
    },
    {
        "id": "GYK",
        "text": "Griffin York and Krause Marketing and Communication",
        "deprecated": false
    },
    {
        "id": "GYS",
        "text": "Genensys, LLC",
        "deprecated": false
    },
    {
        "id": "GZG",
        "text": "Gamanza Group AG",
        "deprecated": false
    },
    {
        "id": "GZX",
        "text": "Gamzix Technology OU",
        "deprecated": false
    },
    {
        "id": "H5G",
        "text": "High 5 Games",
        "deprecated": false
    },
    {
        "id": "HA1",
        "text": "Harrahs Casino",
        "deprecated": false
    },
    {
        "id": "HA2",
        "text": "Harrahs Casino and Hotel",
        "deprecated": false
    },
    {
        "id": "HA3",
        "text": "Harrah\u0027s Corporate Nevada",
        "deprecated": false
    },
    {
        "id": "HA4",
        "text": "Harrahs Entertainment",
        "deprecated": false
    },
    {
        "id": "HA5",
        "text": "Harrahs Louisiana Downs Casino",
        "deprecated": false
    },
    {
        "id": "HA6",
        "text": "Harrahs Metropolis Casino",
        "deprecated": false
    },
    {
        "id": "HA7",
        "text": "Harrahs New Orleans Management Co",
        "deprecated": false
    },
    {
        "id": "HA8",
        "text": "Harrah\u0027s North Kansas City",
        "deprecated": false
    },
    {
        "id": "HA9",
        "text": "Harrah\u0027s Joliet Casino",
        "deprecated": false
    },
    {
        "id": "HAC",
        "text": "Hipodromo de Agua Caliente SA de CV",
        "deprecated": false
    },
    {
        "id": "HAD",
        "text": "Holland Casino (Amsterdam)",
        "deprecated": false
    },
    {
        "id": "HAE",
        "text": "Hammond Entertainment",
        "deprecated": false
    },
    {
        "id": "HAI",
        "text": "Hastings International B.V.",
        "deprecated": false
    },
    {
        "id": "HAL",
        "text": "House Advantage LLC.",
        "deprecated": false
    },
    {
        "id": "HAM",
        "text": "Hamburg Casino at the Fairgrounds",
        "deprecated": false
    },
    {
        "id": "HAP",
        "text": "Hipódromo Argentino de Palermo S.A. - Casino Club S.A. - U.T.E.",
        "deprecated": false
    },
    {
        "id": "HAR",
        "text": "HARRAH\u0027S",
        "deprecated": false
    },
    {
        "id": "HAT",
        "text": "Hattrick- PSK d.o.o.",
        "deprecated": false
    },
    {
        "id": "HAW",
        "text": "Holland Casino Amsterdam West",
        "deprecated": false
    },
    {
        "id": "HBA",
        "text": "Horseshoe Casino Baltimore",
        "deprecated": false
    },
    {
        "id": "HBC",
        "text": "Harveys Iowa Management Co. Inc. dba Harrah\u0027s Council Bluffs Casino",
        "deprecated": false
    },
    {
        "id": "HBG",
        "text": "HBG Connex S.p.a.",
        "deprecated": false
    },
    {
        "id": "HBI",
        "text": "Hopbet Inc.",
        "deprecated": false
    },
    {
        "id": "HBL",
        "text": "HOT BOX LTDA",
        "deprecated": false
    },
    {
        "id": "HBR",
        "text": "Holland Casino (Breda)",
        "deprecated": false
    },
    {
        "id": "HC1",
        "text": "Health Companion, Inc.",
        "deprecated": false
    },
    {
        "id": "HCA",
        "text": "Hollywood Casino Aurora",
        "deprecated": false
    },
    {
        "id": "HCB",
        "text": "Harrah\u0027s Council Bluffs",
        "deprecated": false
    },
    {
        "id": "HCC",
        "text": "Horseshoe Casino Cleveland",
        "deprecated": false
    },
    {
        "id": "HCE",
        "text": "Holland Casino Eindhoven",
        "deprecated": false
    },
    {
        "id": "HCF",
        "text": "Hill, Carter, Franco, Cole, \u0026 Black Attorneys at Law",
        "deprecated": false
    },
    {
        "id": "HCI",
        "text": "HAPP CONTROLS INC.",
        "deprecated": false
    },
    {
        "id": "HCK",
        "text": "Ho-Chunk Gaming Commission",
        "deprecated": false
    },
    {
        "id": "HCL",
        "text": "HOLiC Co Ltd.",
        "deprecated": false
    },
    {
        "id": "HCN",
        "text": "Holland Casino (Hoofdorp)",
        "deprecated": false
    },
    {
        "id": "HCS",
        "text": "Henderson County Sheriffs Office",
        "deprecated": false
    },
    {
        "id": "HCT",
        "text": "Hard Rock Casino Cincinnati, LLC",
        "deprecated": false
    },
    {
        "id": "HCV",
        "text": "Holland Casino (Valkenburg)",
        "deprecated": false
    },
    {
        "id": "HDH",
        "text": "Hawaii Department of the Honolulu Prosecuting Attorney",
        "deprecated": false
    },
    {
        "id": "HDR",
        "text": "Hon-Dah Resort and Casino",
        "deprecated": false
    },
    {
        "id": "HEG",
        "text": "Heartland Gaming",
        "deprecated": false
    },
    {
        "id": "HEH",
        "text": "Hall Estill Hardwick Gable Golden Nelson PC",
        "deprecated": false
    },
    {
        "id": "HEL",
        "text": "Hellenic Gaming Commission",
        "deprecated": false
    },
    {
        "id": "HEN",
        "text": "Holland Casino Enschede",
        "deprecated": false
    },
    {
        "id": "HES",
        "text": "HESS SB-Automatenbau GmBH \u0026 ",
        "deprecated": false
    },
    {
        "id": "HET",
        "text": "HEAL TECHNOLOGY",
        "deprecated": false
    },
    {
        "id": "HG1",
        "text": "Horseshoe Gaming Holding Corporation (LV)",
        "deprecated": false
    },
    {
        "id": "HG2",
        "text": "Horseshoe Gaming Holding Corporation (Reno)",
        "deprecated": false
    },
    {
        "id": "HGA",
        "text": "Hit Game",
        "deprecated": false
    },
    {
        "id": "HGB",
        "text": "HungryBear Games Ltd",
        "deprecated": false
    },
    {
        "id": "HGC",
        "text": "Hopland Gaming Commission",
        "deprecated": false
    },
    {
        "id": "HGH",
        "text": "HG Holdings Limited",
        "deprecated": false
    },
    {
        "id": "HGI",
        "text": "Heartland Gaming, Inc.",
        "deprecated": false
    },
    {
        "id": "HGL",
        "text": "Hero Gaming Limited",
        "deprecated": false
    },
    {
        "id": "HGM",
        "text": "Mardi Gras Casino",
        "deprecated": false
    },
    {
        "id": "HGR",
        "text": "Hungrybear Gaming Limited",
        "deprecated": false
    },
    {
        "id": "HGT",
        "text": "Hollywood Greyhound Track",
        "deprecated": false
    },
    {
        "id": "HGU",
        "text": "Hughes Gaming Inc.",
        "deprecated": false
    },
    {
        "id": "HHC",
        "text": "Hard Rock Hotel \u0026 Casino",
        "deprecated": false
    },
    {
        "id": "HHO",
        "text": "HIT hoteli, igralnice, turizem d.d. Nova Gorica",
        "deprecated": false
    },
    {
        "id": "HHT",
        "text": "Hui Hsun Technology Co., Ltd.",
        "deprecated": false
    },
    {
        "id": "HIC",
        "text": "Hannahville Indian Community Gaming Commission",
        "deprecated": false
    },
    {
        "id": "HIK",
        "text": "Hikai",
        "deprecated": false
    },
    {
        "id": "HIL",
        "text": "Hillside (New Media) Limited",
        "deprecated": false
    },
    {
        "id": "HIN",
        "text": "Hart InterCivic, INC",
        "deprecated": false
    },
    {
        "id": "HIT",
        "text": "HI TECH",
        "deprecated": false
    },
    {
        "id": "HKJ",
        "text": "Hong Kong Jockey Club",
        "deprecated": false
    },
    {
        "id": "HKL",
        "text": "HK Leeu Internet Technology Limited",
        "deprecated": false
    },
    {
        "id": "HKQ",
        "text": "HK QILEYOU TECHNOLOGY LIMITED",
        "deprecated": false
    },
    {
        "id": "HL2",
        "text": "Holland (non-billable)",
        "deprecated": false
    },
    {
        "id": "HLC",
        "text": "Hollywood Casino at Kansas Speedway",
        "deprecated": false
    },
    {
        "id": "HLD",
        "text": "Hold\u0027em 10/2 LLC",
        "deprecated": false
    },
    {
        "id": "HLE",
        "text": "Hainan Legend Intellectual Sports Technology Co., Ltd.",
        "deprecated": false
    },
    {
        "id": "HLG",
        "text": "Harlow Gaming Concepts Pty Ltd",
        "deprecated": false
    },
    {
        "id": "HLI",
        "text": "He Lin",
        "deprecated": false
    },
    {
        "id": "HLR",
        "text": "Havasu Landing Resort and Casino",
        "deprecated": false
    },
    {
        "id": "HLT",
        "text": "Heber Ltd",
        "deprecated": false
    },
    {
        "id": "HLX",
        "text": "Hit Larix d.d",
        "deprecated": false
    },
    {
        "id": "HMH",
        "text": "Harrah\u0027s Maryland Heights-MO",
        "deprecated": false
    },
    {
        "id": "HMI",
        "text": "Harrah\u0027s Metropolis Il",
        "deprecated": false
    },
    {
        "id": "HML",
        "text": "Hector Manpower and Logistics Ltd",
        "deprecated": false
    },
    {
        "id": "HNJ",
        "text": "Holland Casino Nijmegen",
        "deprecated": false
    },
    {
        "id": "HO1",
        "text": "Bossier City Horseshoe Casino",
        "deprecated": false
    },
    {
        "id": "HOC",
        "text": "Harveys BR Management Co. Inc. dba Horseshoe Council Bluffs Casino",
        "deprecated": false
    },
    {
        "id": "HOE",
        "text": "Holey Entertainment Group Limited (Previously Lion Key Ventures Limited)",
        "deprecated": false
    },
    {
        "id": "HOG",
        "text": "HoGaming Limited",
        "deprecated": false
    },
    {
        "id": "HOL",
        "text": "Hollywood Casino",
        "deprecated": false
    },
    {
        "id": "HON",
        "text": "Ho-Chunk Nation",
        "deprecated": false
    },
    {
        "id": "HOR",
        "text": "HORSESHOE CASINO",
        "deprecated": false
    },
    {
        "id": "HOT",
        "text": "HOT STREAK GAMING",
        "deprecated": false
    },
    {
        "id": "HPA",
        "text": "HOOPA VALLEY RESERVE",
        "deprecated": false
    },
    {
        "id": "HPD",
        "text": "Honolulu Police Department",
        "deprecated": false
    },
    {
        "id": "HPE",
        "text": "High Point Entertainment",
        "deprecated": false
    },
    {
        "id": "HPG",
        "text": "High Pulse Gaming",
        "deprecated": false
    },
    {
        "id": "HPP",
        "text": "HOPLAND BAND POMO",
        "deprecated": false
    },
    {
        "id": "HPR",
        "text": "Hoosier Park Racing Casino",
        "deprecated": false
    },
    {
        "id": "HPS",
        "text": "HPS Gaming",
        "deprecated": false
    },
    {
        "id": "HPT",
        "text": "Hengqin Pinzu Technology Co Limited",
        "deprecated": false
    },
    {
        "id": "HPW",
        "text": "Handelsonderneming Paul Wijnants",
        "deprecated": false
    },
    {
        "id": "HPY",
        "text": "Happy Srl",
        "deprecated": false
    },
    {
        "id": "HQ1",
        "text": "Harlequin Gaming Ltd",
        "deprecated": false
    },
    {
        "id": "HRA",
        "text": "Hellenic Republic Asset Development Fund S.A",
        "deprecated": false
    },
    {
        "id": "HRC",
        "text": "Hill Ross Casino BV",
        "deprecated": false
    },
    {
        "id": "HRD",
        "text": "Holland Casino Rotterdam",
        "deprecated": false
    },
    {
        "id": "HRG",
        "text": "HR IA Gaming, LLC dba Hard Rock Casino",
        "deprecated": false
    },
    {
        "id": "HRH",
        "text": "Hard Rock Hotel \u0026 Casino Albuquerque NM",
        "deprecated": false
    },
    {
        "id": "HRI",
        "text": "HAND ROLLE INC.",
        "deprecated": false
    },
    {
        "id": "HRO",
        "text": "HR Ottawa, L.P. dba Hard Rock Ottawa",
        "deprecated": false
    },
    {
        "id": "HRT",
        "text": "Historical Race technologies, LLC",
        "deprecated": false
    },
    {
        "id": "HSA",
        "text": "Horseraces S.A.",
        "deprecated": false
    },
    {
        "id": "HSC",
        "text": "Harrahs Resort Southern California",
        "deprecated": false
    },
    {
        "id": "HSD",
        "text": "Hobbs Straus Dean \u0026 Walker",
        "deprecated": false
    },
    {
        "id": "HSG",
        "text": "Hattrick Sports Group",
        "deprecated": false
    },
    {
        "id": "HSH",
        "text": "Holland Casino Scheveningen",
        "deprecated": false
    },
    {
        "id": "HSI",
        "text": "Horseshoe Southern Indiana Casino",
        "deprecated": false
    },
    {
        "id": "HSV",
        "text": "Hotbox Sports Ventures Inc.",
        "deprecated": false
    },
    {
        "id": "HT1",
        "text": "Holiday Inn Tropical Casino in Mayaquez",
        "deprecated": false
    },
    {
        "id": "HT2",
        "text": "Holiday Inn Tropical Casino in Ponce",
        "deprecated": false
    },
    {
        "id": "HTE",
        "text": "High Technology Systems",
        "deprecated": false
    },
    {
        "id": "HTF",
        "text": "High Tech Future Systems S.A.",
        "deprecated": false
    },
    {
        "id": "HTG",
        "text": "Hotel Gaming B.V.",
        "deprecated": false
    },
    {
        "id": "HTI",
        "text": "Hest Technologies",
        "deprecated": false
    },
    {
        "id": "HTL",
        "text": "Hoosier Lottery",
        "deprecated": false
    },
    {
        "id": "HTS",
        "text": "HTS",
        "deprecated": false
    },
    {
        "id": "HTV",
        "text": "Hollywood TV Limited",
        "deprecated": false
    },
    {
        "id": "HUI",
        "text": "Hit Utopia Inc.",
        "deprecated": false
    },
    {
        "id": "HUL",
        "text": "Habematolel Pomo of Upper Lake Gaming Commission",
        "deprecated": false
    },
    {
        "id": "HUT",
        "text": "Holland Casino (Utrecht)",
        "deprecated": false
    },
    {
        "id": "HVA",
        "text": "High Variance Games LLC",
        "deprecated": false
    },
    {
        "id": "HVG",
        "text": "Happy Valley Global Ltd",
        "deprecated": false
    },
    {
        "id": "HVI",
        "text": "MonKota Gaming",
        "deprecated": false
    },
    {
        "id": "HVN",
        "text": "Hayvin Gaming",
        "deprecated": false
    },
    {
        "id": "HWC",
        "text": "Hollywood Casino (IL)",
        "deprecated": false
    },
    {
        "id": "HWD",
        "text": "HOLLYWOOD RIVERBOAT",
        "deprecated": false
    },
    {
        "id": "HWG",
        "text": "Hardway Games, LLC",
        "deprecated": false
    },
    {
        "id": "HWL",
        "text": "Hearts Worldwide Co., Ltd",
        "deprecated": false
    },
    {
        "id": "HWM",
        "text": "Hollywood Casino - Perryville",
        "deprecated": false
    },
    {
        "id": "HWY",
        "text": "Highway Group PTY LTD",
        "deprecated": false
    },
    {
        "id": "HYD",
        "text": "Hydako",
        "deprecated": false
    },
    {
        "id": "IAC",
        "text": "INVERSIONES ARISTOS DE COLOMBIA S.A.S.",
        "deprecated": false
    },
    {
        "id": "IAF",
        "text": "IAFAS",
        "deprecated": false
    },
    {
        "id": "IAG",
        "text": "INAG Inc",
        "deprecated": false
    },
    {
        "id": "IAL",
        "text": "International Alliance Systems Limited",
        "deprecated": false
    },
    {
        "id": "IAM",
        "text": "INTL AMUSEMENT",
        "deprecated": false
    },
    {
        "id": "IAP",
        "text": "Interblock Asia Pacific",
        "deprecated": false
    },
    {
        "id": "IAS",
        "text": "Instituto de Asistencia Social de la Provincia de Formosa",
        "deprecated": false
    },
    {
        "id": "IAT",
        "text": "Indiana Alcohol Tobacco Commission",
        "deprecated": false
    },
    {
        "id": "IBC",
        "text": "Infobanca Inc",
        "deprecated": false
    },
    {
        "id": "IBE",
        "text": "IB EXQUISITE TECHNOLOGIES LTD",
        "deprecated": false
    },
    {
        "id": "IBN",
        "text": "INSPIRED BROADCAST",
        "deprecated": false
    },
    {
        "id": "IBO",
        "text": "Inthavong Bonk Nanthavongdouangsy",
        "deprecated": false
    },
    {
        "id": "IBT",
        "text": "INTERBET",
        "deprecated": false
    },
    {
        "id": "IC2",
        "text": "Isle of Capri Casino Biloxi",
        "deprecated": false
    },
    {
        "id": "IC3",
        "text": "Isle of Capri Casino West Lake LA",
        "deprecated": false
    },
    {
        "id": "IC4",
        "text": "Isle of Capri Casino Kansas City",
        "deprecated": false
    },
    {
        "id": "ICA",
        "text": "International Casino Systems Europe BV (ICSE)",
        "deprecated": false
    },
    {
        "id": "ICB",
        "text": "IOC - Bettedorf, IA",
        "deprecated": false
    },
    {
        "id": "ICC",
        "text": "Information Control Corp",
        "deprecated": false
    },
    {
        "id": "ICD",
        "text": "IOC - Rhythm City",
        "deprecated": false
    },
    {
        "id": "ICE",
        "text": "INNOVATIVE CONCEPTS",
        "deprecated": false
    },
    {
        "id": "ICG",
        "text": "INTUICODE GAMING CORPORATION",
        "deprecated": false
    },
    {
        "id": "ICH",
        "text": "IntuiSoft Technologies LLC dba ChiroSpring",
        "deprecated": false
    },
    {
        "id": "ICI",
        "text": "IDEA CENTERS",
        "deprecated": false
    },
    {
        "id": "ICL",
        "text": "IC Group LP",
        "deprecated": false
    },
    {
        "id": "ICM",
        "text": "ISLE OF CAPRI\u0027S - MO",
        "deprecated": false
    },
    {
        "id": "ICN",
        "text": "ICode Consultors SLU",
        "deprecated": false
    },
    {
        "id": "ICO",
        "text": "Independent Gaming Corporation",
        "deprecated": false
    },
    {
        "id": "ICP",
        "text": "International Innovative Casino Products \u0026 Services dba I2CPS",
        "deprecated": false
    },
    {
        "id": "ICS",
        "text": "Intracom, SA",
        "deprecated": false
    },
    {
        "id": "ICT",
        "text": "ICIT d.o.o. ",
        "deprecated": false
    },
    {
        "id": "ICU",
        "text": "iGamingCloud Limited",
        "deprecated": false
    },
    {
        "id": "ICW",
        "text": "IOC - Waterloo",
        "deprecated": false
    },
    {
        "id": "IDB",
        "text": "Indelible Technologies, LLC",
        "deprecated": false
    },
    {
        "id": "IDC",
        "text": "Iowa Division of Criminal Investigation",
        "deprecated": false
    },
    {
        "id": "IDE",
        "text": "I.D.E.A.T. Srl",
        "deprecated": false
    },
    {
        "id": "IDI",
        "text": "Iowa Department of Inspections and Appeals",
        "deprecated": false
    },
    {
        "id": "IDL",
        "text": "Idaho Lottery",
        "deprecated": false
    },
    {
        "id": "IDO",
        "text": "Indiana Downs",
        "deprecated": false
    },
    {
        "id": "IDP",
        "text": "IDP IT Sverige AB",
        "deprecated": false
    },
    {
        "id": "IDR",
        "text": "IDealer LTD",
        "deprecated": false
    },
    {
        "id": "IDS",
        "text": "IDS",
        "deprecated": false
    },
    {
        "id": "IDV",
        "text": "iDevco LLC",
        "deprecated": false
    },
    {
        "id": "IDX",
        "text": "IDX",
        "deprecated": false
    },
    {
        "id": "IE1",
        "text": "ICE ELITE LTD",
        "deprecated": false
    },
    {
        "id": "IEL",
        "text": "IEL",
        "deprecated": false
    },
    {
        "id": "IEM",
        "text": "I.E.M. Industria Elettronica",
        "deprecated": false
    },
    {
        "id": "IEN",
        "text": "International Gaming Enhancements",
        "deprecated": false
    },
    {
        "id": "IEP",
        "text": "Interwetten España P.L.C.",
        "deprecated": false
    },
    {
        "id": "IES",
        "text": "IES SAS NIT",
        "deprecated": false
    },
    {
        "id": "IFT",
        "text": "Innovative Funds Transfer",
        "deprecated": false
    },
    {
        "id": "IG1",
        "text": "Incentive Games Limited",
        "deprecated": false
    },
    {
        "id": "IG2",
        "text": "Infinity Gaming Limited",
        "deprecated": false
    },
    {
        "id": "IGA",
        "text": "International Games Tra",
        "deprecated": false
    },
    {
        "id": "IGB",
        "text": "ICONIC GLOBAL CORP.",
        "deprecated": false
    },
    {
        "id": "IGC",
        "text": "IGCA",
        "deprecated": false
    },
    {
        "id": "IGD",
        "text": "Infinite Game Developers",
        "deprecated": false
    },
    {
        "id": "IGE",
        "text": "iGAMES ENTERTAINMENT",
        "deprecated": false
    },
    {
        "id": "IGG",
        "text": "Inspired Gaming Group",
        "deprecated": false
    },
    {
        "id": "IGH",
        "text": "Impera GmbH",
        "deprecated": false
    },
    {
        "id": "IGI",
        "text": "Integrated Group Assets Inc.",
        "deprecated": false
    },
    {
        "id": "IGJ",
        "text": "IGT Juegos S.A.S.",
        "deprecated": false
    },
    {
        "id": "IGK",
        "text": "Igralni Salon Karneval D.O.O.",
        "deprecated": false
    },
    {
        "id": "IGL",
        "text": "In Touch Games Ltd",
        "deprecated": false
    },
    {
        "id": "IGM",
        "text": "Indiana Gaming Commission",
        "deprecated": false
    },
    {
        "id": "IGN",
        "text": "IGN Group S.r.l.",
        "deprecated": false
    },
    {
        "id": "IGO",
        "text": "International Gamco, Inc.",
        "deprecated": false
    },
    {
        "id": "IGP",
        "text": "Independent Gaming Pty Ltd.",
        "deprecated": false
    },
    {
        "id": "IGR",
        "text": "Indiana Grand Casino",
        "deprecated": false
    },
    {
        "id": "IGS",
        "text": "INTL GAMING SYSTEMS",
        "deprecated": false
    },
    {
        "id": "IGT",
        "text": "IGT",
        "deprecated": false
    },
    {
        "id": "IGV",
        "text": "I.G. Development Systems Ltd.",
        "deprecated": false
    },
    {
        "id": "IGW",
        "text": "IGT (Gibraltar) Solutions Ltd.",
        "deprecated": false
    },
    {
        "id": "IGY",
        "text": "Intelligent Gaming Systems",
        "deprecated": false
    },
    {
        "id": "IHD",
        "text": "Iowa Greyhound Association",
        "deprecated": false
    },
    {
        "id": "IHL",
        "text": "IGS Holdings Ltd",
        "deprecated": false
    },
    {
        "id": "IHP",
        "text": "Ithuba Holdings Proprietary Limited (RF)",
        "deprecated": false
    },
    {
        "id": "IHS",
        "text": "Infocare Healthcare Systems (Ireland) Limited",
        "deprecated": false
    },
    {
        "id": "IIB",
        "text": "Isibet S.r.l.",
        "deprecated": false
    },
    {
        "id": "IIC",
        "text": "Interactive Communications International, Inc. dba InComm",
        "deprecated": false
    },
    {
        "id": "IIG",
        "text": "IIan Gould",
        "deprecated": false
    },
    {
        "id": "III",
        "text": "Illuminate Interactive International Inc.",
        "deprecated": false
    },
    {
        "id": "IIL",
        "text": "ID Interactive, LLC",
        "deprecated": false
    },
    {
        "id": "IIN",
        "text": "IGT Indiana",
        "deprecated": false
    },
    {
        "id": "IIP",
        "text": "Insultex Int del Per",
        "deprecated": false
    },
    {
        "id": "IIS",
        "text": "Intralot Italia Spa",
        "deprecated": false
    },
    {
        "id": "IIT",
        "text": "IGT Italia Gaming Machine Solutions Srl",
        "deprecated": false
    },
    {
        "id": "IIV",
        "text": "Impresora Internacional De Valores SAIC (IVISA)",
        "deprecated": false
    },
    {
        "id": "IL1",
        "text": "Italy (non-billable)",
        "deprecated": false
    },
    {
        "id": "IL2",
        "text": "Index Labs Limited",
        "deprecated": false
    },
    {
        "id": "ILA",
        "text": "Iowa Lottery Authority",
        "deprecated": false
    },
    {
        "id": "ILC",
        "text": "Indiana Live Casino",
        "deprecated": false
    },
    {
        "id": "ILE",
        "text": "Idle Gaming Inc.",
        "deprecated": false
    },
    {
        "id": "ILG",
        "text": "Illinois Gaming Board",
        "deprecated": false
    },
    {
        "id": "ILJ",
        "text": "Impact Loterij B.V.",
        "deprecated": false
    },
    {
        "id": "ILL",
        "text": "Illinois Lottery",
        "deprecated": false
    },
    {
        "id": "ILM",
        "text": "Invet Laboral de Mexico S de RL de CV",
        "deprecated": false
    },
    {
        "id": "ILS",
        "text": "International Lottery \u0026 Totalizator Systems, Inc",
        "deprecated": false
    },
    {
        "id": "ILT",
        "text": "Intralot Gaming Services Pty Ltd",
        "deprecated": false
    },
    {
        "id": "IM1",
        "text": "ISB Magma Limited",
        "deprecated": false
    },
    {
        "id": "IMA",
        "text": "IMAGINEERING",
        "deprecated": false
    },
    {
        "id": "IMC",
        "text": "Imperial Casino Games",
        "deprecated": false
    },
    {
        "id": "IMD",
        "text": "Intermedia S.R.L.",
        "deprecated": false
    },
    {
        "id": "IMG",
        "text": "Inn of the Mountain Gods",
        "deprecated": false
    },
    {
        "id": "IML",
        "text": "IceMiller Legal Business Advisors",
        "deprecated": false
    },
    {
        "id": "IMM",
        "text": "Imaginamics LLC",
        "deprecated": false
    },
    {
        "id": "IMP",
        "text": "Innovative Medical Practice Solutions, LLC",
        "deprecated": false
    },
    {
        "id": "IMR",
        "text": "IMARCSGROUP.COM, LLC",
        "deprecated": false
    },
    {
        "id": "IMS",
        "text": "Interactive Media Services Limited",
        "deprecated": false
    },
    {
        "id": "IMT",
        "text": "IGT Malta Casino Limited",
        "deprecated": false
    },
    {
        "id": "IN2",
        "text": "In 2 Business Ltd",
        "deprecated": false
    },
    {
        "id": "IN3",
        "text": "Integrated Services For Gaming S.A.C.",
        "deprecated": false
    },
    {
        "id": "INA",
        "text": "INAJA-COSMIT BAND",
        "deprecated": false
    },
    {
        "id": "INC",
        "text": "Iceland Lottery",
        "deprecated": false
    },
    {
        "id": "IND",
        "text": "INDIANA",
        "deprecated": false
    },
    {
        "id": "INE",
        "text": "I-Neda Ltd",
        "deprecated": false
    },
    {
        "id": "INF",
        "text": "INFINITY GROUP INC.",
        "deprecated": false
    },
    {
        "id": "ING",
        "text": "INTERGAME",
        "deprecated": false
    },
    {
        "id": "INH",
        "text": "Inotech Casino Solutions LLC",
        "deprecated": false
    },
    {
        "id": "INI",
        "text": "integrationsGA",
        "deprecated": false
    },
    {
        "id": "INL",
        "text": "Intralot S.A. Integrated Lottery Systems and Services dba Intralot S.A.",
        "deprecated": false
    },
    {
        "id": "INM",
        "text": "Inplay Matrix Limited",
        "deprecated": false
    },
    {
        "id": "INN",
        "text": "Innovative Technology Ltd. (Sucursal en Espana) ",
        "deprecated": false
    },
    {
        "id": "INO",
        "text": "Inclub D.O.O. Nova Gorica",
        "deprecated": false
    },
    {
        "id": "INP",
        "text": "Intralot de Peru SAC",
        "deprecated": false
    },
    {
        "id": "INQ",
        "text": "Inspired Gaming (Gibraltar) Ltd",
        "deprecated": false
    },
    {
        "id": "INR",
        "text": "Intercard Inc.",
        "deprecated": false
    },
    {
        "id": "INS",
        "text": "Interseven Gaming Team S.L.U",
        "deprecated": false
    },
    {
        "id": "INT",
        "text": "INCREDIBLE TECHNOLOG",
        "deprecated": false
    },
    {
        "id": "INU",
        "text": "Inventure Gaming Ltd",
        "deprecated": false
    },
    {
        "id": "INV",
        "text": "Intervideo Coporation",
        "deprecated": false
    },
    {
        "id": "IOC",
        "text": "ISLE OF CAPRI\u0027S",
        "deprecated": false
    },
    {
        "id": "IOM",
        "text": "IOC - Marquette",
        "deprecated": false
    },
    {
        "id": "IOW",
        "text": "Iowa Tribe Gaming Commission",
        "deprecated": false
    },
    {
        "id": "IPA",
        "text": "IPA d.o.o.",
        "deprecated": false
    },
    {
        "id": "IPB",
        "text": "iPinball",
        "deprecated": false
    },
    {
        "id": "IPC",
        "text": "IP Game Creations",
        "deprecated": false
    },
    {
        "id": "IPD",
        "text": "Instituto Provincial De Lotería Y Casinos De La Provincia De Buenos Aires",
        "deprecated": false
    },
    {
        "id": "IPG",
        "text": "Infinitygames Production",
        "deprecated": false
    },
    {
        "id": "IPI",
        "text": "Imperial Pacific International (CNMI), LLC",
        "deprecated": false
    },
    {
        "id": "IPJ",
        "text": "Instituto Provincial de Juegos de Azar de Neuquén - I.J.A.N.",
        "deprecated": false
    },
    {
        "id": "IPL",
        "text": "IPLYC",
        "deprecated": false
    },
    {
        "id": "IPO",
        "text": "iPro Games Inc.  ",
        "deprecated": false
    },
    {
        "id": "IPP",
        "text": "Isle of Pompano Park ",
        "deprecated": false
    },
    {
        "id": "IPR",
        "text": "International Gaming Projects Limited",
        "deprecated": false
    },
    {
        "id": "IPS",
        "text": "InPracSys",
        "deprecated": false
    },
    {
        "id": "IPY",
        "text": "Inteplay Global Limited",
        "deprecated": false
    },
    {
        "id": "IQS",
        "text": "IQ Soft LLC",
        "deprecated": false
    },
    {
        "id": "IR1",
        "text": "Adams Hoefer Holwadel",
        "deprecated": false
    },
    {
        "id": "IRC",
        "text": "Isleta Resort Casino",
        "deprecated": false
    },
    {
        "id": "IRG",
        "text": "Iowa Racing and Gaming Commission",
        "deprecated": false
    },
    {
        "id": "IRL",
        "text": "Init Rise Technology Co.Ltd",
        "deprecated": false
    },
    {
        "id": "IRM",
        "text": "Intergames SRL",
        "deprecated": false
    },
    {
        "id": "IRS",
        "text": "Internal Revenue Service",
        "deprecated": false
    },
    {
        "id": "IRT",
        "text": "Idrottens Spel Service Göteborg AB",
        "deprecated": false
    },
    {
        "id": "IS1",
        "text": "Iatric Systems, Inc.",
        "deprecated": false
    },
    {
        "id": "IS3",
        "text": "iSolutions S.r.l.",
        "deprecated": false
    },
    {
        "id": "ISA",
        "text": "Interprod S.A.",
        "deprecated": false
    },
    {
        "id": "ISB",
        "text": "iSoftBet",
        "deprecated": false
    },
    {
        "id": "ISC",
        "text": "Isles Capital  LLC",
        "deprecated": false
    },
    {
        "id": "ISD",
        "text": "INTEGRATED SYS DSN",
        "deprecated": false
    },
    {
        "id": "ISE",
        "text": "IsCool Entertainment",
        "deprecated": false
    },
    {
        "id": "ISF",
        "text": "ILG Software Limited",
        "deprecated": false
    },
    {
        "id": "ISG",
        "text": "ISIS Lab Inc.",
        "deprecated": false
    },
    {
        "id": "ISI",
        "text": "Integrated Solutions, Inc.",
        "deprecated": false
    },
    {
        "id": "ISJ",
        "text": "Intercontinental San Juan",
        "deprecated": false
    },
    {
        "id": "ISK",
        "text": "Indigo Sky Casino",
        "deprecated": false
    },
    {
        "id": "ISL",
        "text": "ISI Ltd.",
        "deprecated": false
    },
    {
        "id": "ISO",
        "text": "ISOCHRON",
        "deprecated": false
    },
    {
        "id": "ISP",
        "text": "Indiana State Excise Police",
        "deprecated": false
    },
    {
        "id": "ISR",
        "text": "Idaho State Racing Commission",
        "deprecated": false
    },
    {
        "id": "ISS",
        "text": "International Games System Co., Ltd.",
        "deprecated": false
    },
    {
        "id": "IST",
        "text": "Intelligent Deposit Systems Ltd.",
        "deprecated": false
    },
    {
        "id": "ISY",
        "text": "Integrated Gaming System",
        "deprecated": false
    },
    {
        "id": "IT2",
        "text": "Interacting Technology Asia Company Limited",
        "deprecated": false
    },
    {
        "id": "ITA",
        "text": "Italgiochi 2001 Srl",
        "deprecated": false
    },
    {
        "id": "ITB",
        "text": "THUNDERBIRD",
        "deprecated": false
    },
    {
        "id": "ITC",
        "text": "Interprovincial Lottery Corporation",
        "deprecated": false
    },
    {
        "id": "ITE",
        "text": "Inspire Technology LLC",
        "deprecated": false
    },
    {
        "id": "ITG",
        "text": "Interwetten Gaming Ltd",
        "deprecated": false
    },
    {
        "id": "ITI",
        "text": "I-Ticket Inc.",
        "deprecated": false
    },
    {
        "id": "ITK",
        "text": "IdeaTek technology co., Ltd.",
        "deprecated": false
    },
    {
        "id": "ITL",
        "text": "Intralot",
        "deprecated": false
    },
    {
        "id": "ITM",
        "text": "Italmatic Srl",
        "deprecated": false
    },
    {
        "id": "ITN",
        "text": "iTeam Network LLC",
        "deprecated": false
    },
    {
        "id": "ITO",
        "text": "Itoga LLC",
        "deprecated": false
    },
    {
        "id": "ITP",
        "text": "Italpiste Srl",
        "deprecated": false
    },
    {
        "id": "ITR",
        "text": "Intralot Interactive (I2)",
        "deprecated": false
    },
    {
        "id": "ITS",
        "text": "ITS",
        "deprecated": false
    },
    {
        "id": "ITT",
        "text": "Interactive Technology Inc.",
        "deprecated": false
    },
    {
        "id": "ITV",
        "text": "Inversiones Traviata SAC",
        "deprecated": false
    },
    {
        "id": "IUK",
        "text": "Inspired Gaming (UK) Ltd",
        "deprecated": false
    },
    {
        "id": "IVA",
        "text": "InvaSoft GmbH",
        "deprecated": false
    },
    {
        "id": "IVC",
        "text": "Inversiones Cerro Blanco SAC",
        "deprecated": false
    },
    {
        "id": "IVG",
        "text": "IVG S.r.l",
        "deprecated": false
    },
    {
        "id": "IVS",
        "text": "Iverson Gaming Systems",
        "deprecated": false
    },
    {
        "id": "IVY",
        "text": "Ivy Comptech Private Limited",
        "deprecated": false
    },
    {
        "id": "IWG",
        "text": "Instant Win Gaming",
        "deprecated": false
    },
    {
        "id": "IXS",
        "text": "Idox Software Ltd.",
        "deprecated": false
    },
    {
        "id": "J16",
        "text": "Jackpot 16 Card Game",
        "deprecated": false
    },
    {
        "id": "JAG",
        "text": "Jacarilla Apache Gaming Regulatory Commission",
        "deprecated": false
    },
    {
        "id": "JAN",
        "text": "Jicarilla Apache Nation",
        "deprecated": false
    },
    {
        "id": "JAS",
        "text": "Jackpot Spa",
        "deprecated": false
    },
    {
        "id": "JAY",
        "text": "JayVegas",
        "deprecated": false
    },
    {
        "id": "JCB",
        "text": "Juicebox",
        "deprecated": false
    },
    {
        "id": "JCI",
        "text": "Johnson Controls Inc.",
        "deprecated": false
    },
    {
        "id": "JCJ",
        "text": "Junta De Control De Juengos",
        "deprecated": false
    },
    {
        "id": "JCM",
        "text": "JCM",
        "deprecated": false
    },
    {
        "id": "JCR",
        "text": "Jumers Casino Rock Island",
        "deprecated": false
    },
    {
        "id": "JCS",
        "text": "Josbe Ceao SL",
        "deprecated": false
    },
    {
        "id": "JDG",
        "text": "Jadestone Group",
        "deprecated": false
    },
    {
        "id": "JDP",
        "text": "J\u0026J Development \u0026 Production",
        "deprecated": false
    },
    {
        "id": "JEB",
        "text": "Jena Band of Choctaw Gaming Commission",
        "deprecated": false
    },
    {
        "id": "JEN",
        "text": "JENOSYS TECHNOLOGIES",
        "deprecated": false
    },
    {
        "id": "JES",
        "text": "Juegos Espana PLC",
        "deprecated": false
    },
    {
        "id": "JET",
        "text": "Jack Entertainment LLC",
        "deprecated": false
    },
    {
        "id": "JEU",
        "text": "JCM Europe GmbH",
        "deprecated": false
    },
    {
        "id": "JEY",
        "text": "J Evans Y Asociados SAC ",
        "deprecated": false
    },
    {
        "id": "JGG",
        "text": "The Jagger Casino Spijkenisse",
        "deprecated": false
    },
    {
        "id": "JGV",
        "text": "John Galvani",
        "deprecated": false
    },
    {
        "id": "JHG",
        "text": "Janshen-Hahnraths Group B.V.",
        "deprecated": false
    },
    {
        "id": "JIC",
        "text": "Jose I. Casas",
        "deprecated": false
    },
    {
        "id": "JIL",
        "text": "Jarol Investment Limited",
        "deprecated": false
    },
    {
        "id": "JIR",
        "text": "Japan Integrate Resort Regime Promotion (non-billable)",
        "deprecated": false
    },
    {
        "id": "JIV",
        "text": "Jamul Gaming Commission",
        "deprecated": false
    },
    {
        "id": "JJG",
        "text": "J \u0026 J Gaming",
        "deprecated": false
    },
    {
        "id": "JJO",
        "text": "JackpotJoy Operations Ltd.",
        "deprecated": false
    },
    {
        "id": "JKP",
        "text": "JackP \u0026 Friends ApS",
        "deprecated": false
    },
    {
        "id": "JKS",
        "text": "Jackson Rancheria Casino Resort",
        "deprecated": false
    },
    {
        "id": "JLE",
        "text": "Johnny Le",
        "deprecated": false
    },
    {
        "id": "JLG",
        "text": "Jacobson Law Group",
        "deprecated": false
    },
    {
        "id": "JLI",
        "text": "JPL Limited",
        "deprecated": false
    },
    {
        "id": "JMA",
        "text": "Johnny Ma",
        "deprecated": false
    },
    {
        "id": "JMB",
        "text": "Law Offices of John M. Bolton",
        "deprecated": false
    },
    {
        "id": "JMI",
        "text": "JMI Concepts, Inc.",
        "deprecated": false
    },
    {
        "id": "JNG",
        "text": "Joingo, LLC",
        "deprecated": false
    },
    {
        "id": "JNS",
        "text": "Green Felt Gaming, LLC",
        "deprecated": false
    },
    {
        "id": "JOE",
        "text": "JOSS ELETTRONICA s.r.l",
        "deprecated": false
    },
    {
        "id": "JOH",
        "text": "TCS - International - John Huxley",
        "deprecated": false
    },
    {
        "id": "JOK",
        "text": "Joker Games d.o.o.",
        "deprecated": false
    },
    {
        "id": "JOL",
        "text": "La Jolla Gaming Commission",
        "deprecated": false
    },
    {
        "id": "JP1",
        "text": "Junta de Protección Social",
        "deprecated": false
    },
    {
        "id": "JPD",
        "text": "Jackpot Digital Inc.",
        "deprecated": false
    },
    {
        "id": "JPI",
        "text": "Jackpocket Inc.",
        "deprecated": false
    },
    {
        "id": "JPL",
        "text": "Jonsden Properties Limited",
        "deprecated": false
    },
    {
        "id": "JPM",
        "text": "JPM Interactive",
        "deprecated": false
    },
    {
        "id": "JPS",
        "text": "Jackpot Software Inc.",
        "deprecated": false
    },
    {
        "id": "JRS",
        "text": "JR\u0027s Sportsbook PL",
        "deprecated": false
    },
    {
        "id": "JSC",
        "text": "JSC TOP sport",
        "deprecated": false
    },
    {
        "id": "JTB",
        "text": "Jetbull",
        "deprecated": false
    },
    {
        "id": "JTC",
        "text": "Jumbo Technology Co, LTD",
        "deprecated": false
    },
    {
        "id": "JTH",
        "text": "Jibo Technology Holdings Limited",
        "deprecated": false
    },
    {
        "id": "JTL",
        "text": "Jenellcore Trading Ltd",
        "deprecated": false
    },
    {
        "id": "JTY",
        "text": "Jumbo Technology",
        "deprecated": false
    },
    {
        "id": "JUE",
        "text": "Juego Online",
        "deprecated": false
    },
    {
        "id": "JUK",
        "text": "JCM Europe (UK) Ltd",
        "deprecated": false
    },
    {
        "id": "JUM",
        "text": "Jumbo Interactive",
        "deprecated": false
    },
    {
        "id": "JVG",
        "text": "JVL Labs Inc.",
        "deprecated": false
    },
    {
        "id": "JVH",
        "text": "JVH gaming B.V.",
        "deprecated": false
    },
    {
        "id": "JVS",
        "text": "JVL Systems Inc.",
        "deprecated": false
    },
    {
        "id": "JX1",
        "text": "Jungle X UK Ltd",
        "deprecated": false
    },
    {
        "id": "KAB",
        "text": "Kaba Australia Pty Ltd",
        "deprecated": false
    },
    {
        "id": "KAC",
        "text": "Kangwonland Casino",
        "deprecated": false
    },
    {
        "id": "KAG",
        "text": "Karma Gaming International",
        "deprecated": false
    },
    {
        "id": "KAJ",
        "text": "Kajot Casino Ltd.",
        "deprecated": false
    },
    {
        "id": "KAK",
        "text": "KAKTUS SYSTEMER AS",
        "deprecated": false
    },
    {
        "id": "KAM",
        "text": "Kambi Services Ltd",
        "deprecated": false
    },
    {
        "id": "KAN",
        "text": "KANSAS LOTTERY",
        "deprecated": false
    },
    {
        "id": "KAP",
        "text": "Kibow Asia Pte. Ltd.",
        "deprecated": false
    },
    {
        "id": "KAS",
        "text": "KA System Global Ltd.",
        "deprecated": false
    },
    {
        "id": "KAT",
        "text": "Kate BVBA",
        "deprecated": false
    },
    {
        "id": "KAZ",
        "text": "LLP (Limited Liability Partnership) \"Kazakhstan Lottery Technologies\"",
        "deprecated": false
    },
    {
        "id": "KBC",
        "text": "Kickback CO",
        "deprecated": false
    },
    {
        "id": "KBI",
        "text": "Keweenaw Bay Indian Community Tribal Gaming Commission",
        "deprecated": false
    },
    {
        "id": "KBS",
        "text": "King Bet SRL",
        "deprecated": false
    },
    {
        "id": "KCC",
        "text": "Kansas Crossing Casino \u0026 Hotel",
        "deprecated": false
    },
    {
        "id": "KDC",
        "text": "King Dice Gaming",
        "deprecated": false
    },
    {
        "id": "KDG",
        "text": "Kentucky Department of Charitable Gaming ",
        "deprecated": false
    },
    {
        "id": "KDK",
        "text": "KDK Holding GmbH",
        "deprecated": false
    },
    {
        "id": "KEM",
        "text": "KEMO",
        "deprecated": false
    },
    {
        "id": "KER",
        "text": "K.E.R. Kft.",
        "deprecated": false
    },
    {
        "id": "KEY",
        "text": "Keynectis",
        "deprecated": false
    },
    {
        "id": "KGA",
        "text": "Kansas State Gaming Agency",
        "deprecated": false
    },
    {
        "id": "KGC",
        "text": "KGC",
        "deprecated": false
    },
    {
        "id": "KGI",
        "text": "Kings Gaming, Inc.",
        "deprecated": false
    },
    {
        "id": "KGM",
        "text": "KGM Gaming",
        "deprecated": false
    },
    {
        "id": "KGP",
        "text": "KODIAK GAMING PRODUCTS",
        "deprecated": false
    },
    {
        "id": "KGR",
        "text": "KGR Entertainment GmbH",
        "deprecated": false
    },
    {
        "id": "KIB",
        "text": "Kibow Pte, LTD",
        "deprecated": false
    },
    {
        "id": "KIN",
        "text": "King.com",
        "deprecated": false
    },
    {
        "id": "KIR",
        "text": "Kiron Interactive",
        "deprecated": false
    },
    {
        "id": "KIS",
        "text": "Kodiak Industrial System Inc. dba Kodiak Entertainment",
        "deprecated": false
    },
    {
        "id": "KIU",
        "text": "Kibow (UK) Ltd",
        "deprecated": false
    },
    {
        "id": "KJT",
        "text": "KAJOT",
        "deprecated": false
    },
    {
        "id": "KLC",
        "text": "Kentucky Lottery Cor",
        "deprecated": false
    },
    {
        "id": "KLD",
        "text": "Kansas Legislative Division of Post Audit",
        "deprecated": false
    },
    {
        "id": "KLG",
        "text": "Kalamba Games Limited",
        "deprecated": false
    },
    {
        "id": "KLL",
        "text": "King Lotto Limited",
        "deprecated": false
    },
    {
        "id": "KLU",
        "text": "Kindred (London) Limited {formerly Unibet (London) Ltd}",
        "deprecated": false
    },
    {
        "id": "KMG",
        "text": "King Matic di Giovanni",
        "deprecated": false
    },
    {
        "id": "KMS",
        "text": "KISMET STUDIOS",
        "deprecated": false
    },
    {
        "id": "KMY",
        "text": "Klamath Tribes Gaming Regulatory Commission",
        "deprecated": false
    },
    {
        "id": "KN1",
        "text": "KOHA Noorwijk B.V.",
        "deprecated": false
    },
    {
        "id": "KNB",
        "text": "Preferred Games Inc.",
        "deprecated": false
    },
    {
        "id": "KNG",
        "text": "Kwa-Zulu Natal Gaming Board",
        "deprecated": false
    },
    {
        "id": "KNI",
        "text": "Konami Gaming, Inc. (Interactive Division)",
        "deprecated": false
    },
    {
        "id": "KNV",
        "text": "Koninklijke Nederlandse Voetbalbond",
        "deprecated": false
    },
    {
        "id": "KO1",
        "text": "Konami Australia Pty Ltd",
        "deprecated": false
    },
    {
        "id": "KOA",
        "text": "Konami of America, I",
        "deprecated": false
    },
    {
        "id": "KOB",
        "text": "Kobetron, Inc.",
        "deprecated": false
    },
    {
        "id": "KOE",
        "text": "Keen Ocean Entertainment (IOM) Limited",
        "deprecated": false
    },
    {
        "id": "KOG",
        "text": "Knockout Gaming",
        "deprecated": false
    },
    {
        "id": "KON",
        "text": "KONAMI GAMING INC.",
        "deprecated": false
    },
    {
        "id": "KOO",
        "text": "Kootac Trading Limited",
        "deprecated": false
    },
    {
        "id": "KOT",
        "text": "Koborise App Technologies Ltd",
        "deprecated": false
    },
    {
        "id": "KPF",
        "text": "Kancelaria Prawa Finansowego Forum",
        "deprecated": false
    },
    {
        "id": "KPG",
        "text": "KP Gaming Supplies",
        "deprecated": false
    },
    {
        "id": "KPI",
        "text": "KITPLAN INC",
        "deprecated": false
    },
    {
        "id": "KRA",
        "text": "KRAMER",
        "deprecated": false
    },
    {
        "id": "KRC",
        "text": "Kentucky Horse Racing Commission",
        "deprecated": false
    },
    {
        "id": "KRM",
        "text": "KRM Wagering, LLC",
        "deprecated": false
    },
    {
        "id": "KSC",
        "text": "Kansas Star Casino",
        "deprecated": false
    },
    {
        "id": "KSG",
        "text": "Kansas Racing and Gaming Agency",
        "deprecated": false
    },
    {
        "id": "KSK",
        "text": "Kiosk Information Systems, Inc.",
        "deprecated": false
    },
    {
        "id": "KSN",
        "text": "KaSuN LLC",
        "deprecated": false
    },
    {
        "id": "KTG",
        "text": "Kickapoo Tribe of Texas Gaming Commission ",
        "deprecated": false
    },
    {
        "id": "KTL",
        "text": "Kentucky Lottery",
        "deprecated": false
    },
    {
        "id": "KVG",
        "text": "Katavi Gaming Limited",
        "deprecated": false
    },
    {
        "id": "KWC",
        "text": "Kiowa Casino",
        "deprecated": false
    },
    {
        "id": "KWF",
        "text": "KWF Kankerbestrijding",
        "deprecated": false
    },
    {
        "id": "KWG",
        "text": "Karaway Gaming",
        "deprecated": false
    },
    {
        "id": "KYD",
        "text": "Kentucky Downs",
        "deprecated": false
    },
    {
        "id": "KYR",
        "text": "Keyrunon Ltd",
        "deprecated": false
    },
    {
        "id": "KZE",
        "text": "KaZee, Inc.",
        "deprecated": false
    },
    {
        "id": "KZN",
        "text": "KwaZulu-Natal Gaming \u0026 Betting Board (KZN Gaming Board)",
        "deprecated": false
    },
    {
        "id": "KZS",
        "text": "Kazunobu Saeki",
        "deprecated": false
    },
    {
        "id": "L3T",
        "text": "Lottery Innovations LLC",
        "deprecated": false
    },
    {
        "id": "LA1",
        "text": "Louisiana State Police (non-billable)",
        "deprecated": false
    },
    {
        "id": "LAA",
        "text": "LaaSTech, LLC",
        "deprecated": false
    },
    {
        "id": "LAC",
        "text": "L Auberge",
        "deprecated": false
    },
    {
        "id": "LAD",
        "text": "LA Downs Casino",
        "deprecated": false
    },
    {
        "id": "LAF",
        "text": "Lottomatrix South Africa (PTY) Ltd",
        "deprecated": false
    },
    {
        "id": "LAG",
        "text": "Lasso Gaming, LLC",
        "deprecated": false
    },
    {
        "id": "LAI",
        "text": "Ladbrokes International",
        "deprecated": false
    },
    {
        "id": "LAJ",
        "text": "LAJOLLA INDIAN",
        "deprecated": false
    },
    {
        "id": "LAL",
        "text": "Lazlo 326 LLC",
        "deprecated": false
    },
    {
        "id": "LAR",
        "text": "Lar Sistemi Srl",
        "deprecated": false
    },
    {
        "id": "LAS",
        "text": "LA State Police",
        "deprecated": false
    },
    {
        "id": "LAU",
        "text": "L\u0027Auberge",
        "deprecated": false
    },
    {
        "id": "LAX",
        "text": "Laxino USA LLC",
        "deprecated": false
    },
    {
        "id": "LAY",
        "text": "LAYTONVILLE RANCH",
        "deprecated": false
    },
    {
        "id": "LAZ",
        "text": "Lazer-Tron",
        "deprecated": false
    },
    {
        "id": "LB1",
        "text": "Lightning Box Games Pty Limited",
        "deprecated": false
    },
    {
        "id": "LBA",
        "text": "LOTBA S.E.-Lotería de la Ciudad de Buenos Aires Sociedad del Estado",
        "deprecated": false
    },
    {
        "id": "LBB",
        "text": "Plus 12 Gaming, LLC",
        "deprecated": false
    },
    {
        "id": "LBC",
        "text": "Lucky Bear Casino",
        "deprecated": false
    },
    {
        "id": "LBG",
        "text": "LadBrokes Betting and Gaming Limited",
        "deprecated": false
    },
    {
        "id": "LBN",
        "text": "LBN Eletronic Games s.r.l.",
        "deprecated": false
    },
    {
        "id": "LBS",
        "text": "LazyBug Studio Limited",
        "deprecated": false
    },
    {
        "id": "LBY",
        "text": "Loteria de Boyaca",
        "deprecated": false
    },
    {
        "id": "LCD",
        "text": "Lucky Cards",
        "deprecated": false
    },
    {
        "id": "LCG",
        "text": "LIF Capital Group",
        "deprecated": false
    },
    {
        "id": "LCH",
        "text": "Lotus Cherry Ltd",
        "deprecated": false
    },
    {
        "id": "LCI",
        "text": "LAMBRIDES CONSUL INC",
        "deprecated": false
    },
    {
        "id": "LCO",
        "text": "La Commercial Games SRL",
        "deprecated": false
    },
    {
        "id": "LCS",
        "text": "Luckystream Co., Ltd. Corporation",
        "deprecated": false
    },
    {
        "id": "LCT",
        "text": "Electracade",
        "deprecated": false
    },
    {
        "id": "LDA",
        "text": "LUCKY DOG AMUSEMENT",
        "deprecated": false
    },
    {
        "id": "LDB",
        "text": "Loteria de Bogota",
        "deprecated": false
    },
    {
        "id": "LDC",
        "text": "Laguna Development Corporation",
        "deprecated": false
    },
    {
        "id": "LDP",
        "text": "Loterias Del Peru",
        "deprecated": false
    },
    {
        "id": "LEA",
        "text": "Laboratorio Elettronico",
        "deprecated": false
    },
    {
        "id": "LED",
        "text": "L.E.D. EFFECTS",
        "deprecated": false
    },
    {
        "id": "LEG",
        "text": "Lionline Entertainment",
        "deprecated": false
    },
    {
        "id": "LEI",
        "text": "LAKES ENTERTAINMENT",
        "deprecated": false
    },
    {
        "id": "LEN",
        "text": "Lupine Enterprises LTD.",
        "deprecated": false
    },
    {
        "id": "LEO",
        "text": "LeoVegas Gaming p.l.c.",
        "deprecated": false
    },
    {
        "id": "LES",
        "text": "Lessinger Gaming LLC",
        "deprecated": false
    },
    {
        "id": "LFG",
        "text": "Leap Forward Gaming, Inc.",
        "deprecated": false
    },
    {
        "id": "LFI",
        "text": "Longford International Ltd.",
        "deprecated": false
    },
    {
        "id": "LFL",
        "text": "Lean Forward Ltd",
        "deprecated": false
    },
    {
        "id": "LGA",
        "text": "Liquor and Gaming Authority of Manitoba",
        "deprecated": false
    },
    {
        "id": "LGC",
        "text": "Lombarda Giochi Srl",
        "deprecated": false
    },
    {
        "id": "LGG",
        "text": "LUCKIA GAMING GROUP S.A.",
        "deprecated": false
    },
    {
        "id": "LGI",
        "text": "LASVEGAS GAMING INC.",
        "deprecated": false
    },
    {
        "id": "LGL",
        "text": "LT Game Limited",
        "deprecated": false
    },
    {
        "id": "LGN",
        "text": "LG CNS Co., Ltd.",
        "deprecated": false
    },
    {
        "id": "LGO",
        "text": "Lagos State Lotteries Board",
        "deprecated": false
    },
    {
        "id": "LGR",
        "text": "Livescore Group Limited",
        "deprecated": false
    },
    {
        "id": "LGS",
        "text": "Lodging and Gaming Systems Ltd.",
        "deprecated": false
    },
    {
        "id": "LGT",
        "text": "IGT \u0026 LGL Multi-Party Agreement",
        "deprecated": false
    },
    {
        "id": "LGZ",
        "text": "Lotteries \u0026 Gaming Board of Zimbabwe",
        "deprecated": false
    },
    {
        "id": "LHI",
        "text": "LaunchHub Inc.",
        "deprecated": false
    },
    {
        "id": "LHL",
        "text": "Local Hospice Lottery Ltd",
        "deprecated": false
    },
    {
        "id": "LHS",
        "text": "Mr. Larry Haas",
        "deprecated": false
    },
    {
        "id": "LI1",
        "text": "Liechtenstein (non-billable)",
        "deprecated": false
    },
    {
        "id": "LI5",
        "text": "Live 5 Ltd",
        "deprecated": false
    },
    {
        "id": "LIB",
        "text": "888 Liberty Ltd.",
        "deprecated": false
    },
    {
        "id": "LIL",
        "text": "Legend Interactive Limited",
        "deprecated": false
    },
    {
        "id": "LIN",
        "text": "Linea Informatica srl",
        "deprecated": false
    },
    {
        "id": "LIT",
        "text": "Little River Band of Ottawa Indians Gaming Commission ",
        "deprecated": false
    },
    {
        "id": "LIV",
        "text": "Legolas Invest Ltd",
        "deprecated": false
    },
    {
        "id": "LJG",
        "text": "LARRY J. GORDON",
        "deprecated": false
    },
    {
        "id": "LJL",
        "text": "Lubin-Jones, LLC",
        "deprecated": false
    },
    {
        "id": "LJO",
        "text": "La Jolla Gaming, INC",
        "deprecated": false
    },
    {
        "id": "LKA",
        "text": "Luckia Games S.A.",
        "deprecated": false
    },
    {
        "id": "LKC",
        "text": "Lady Luck Casino",
        "deprecated": false
    },
    {
        "id": "LKG",
        "text": "Luka\u0027s Game",
        "deprecated": false
    },
    {
        "id": "LKL",
        "text": "Linklaters LLP",
        "deprecated": false
    },
    {
        "id": "LKO",
        "text": "Lotaria Kombetare sh.p.k.",
        "deprecated": false
    },
    {
        "id": "LL1",
        "text": "Lottovate Limited",
        "deprecated": false
    },
    {
        "id": "LL2",
        "text": "Lucky Lincoln Gaming, LLC",
        "deprecated": false
    },
    {
        "id": "LL3",
        "text": "LL Lucky Games AB",
        "deprecated": false
    },
    {
        "id": "LLA",
        "text": "LENSKE,LENSKE,ABRAMS",
        "deprecated": false
    },
    {
        "id": "LLC",
        "text": "LOUISIANA LOTTERY CO",
        "deprecated": false
    },
    {
        "id": "LLG",
        "text": "Leech Lake Gaming Regulatory Board",
        "deprecated": false
    },
    {
        "id": "LLL",
        "text": "Lottoland Ltd.",
        "deprecated": false
    },
    {
        "id": "LLM",
        "text": "Lady Luck Marquette",
        "deprecated": false
    },
    {
        "id": "LLV",
        "text": "Seven Gaming Ltd.",
        "deprecated": false
    },
    {
        "id": "LMG",
        "text": "Lummi Tribal Gaming Agency",
        "deprecated": false
    },
    {
        "id": "LML",
        "text": "LA MACHINE LEASING",
        "deprecated": false
    },
    {
        "id": "LMO",
        "text": "Liquid Medical Office, Inc.",
        "deprecated": false
    },
    {
        "id": "LMT",
        "text": "Logispin Malta Limited",
        "deprecated": false
    },
    {
        "id": "LNA",
        "text": "Loterie Nationale Sénégalaise (LO.NA.SE)",
        "deprecated": false
    },
    {
        "id": "LNB",
        "text": "Loteria Nacional de Beneficencia del Salvador",
        "deprecated": false
    },
    {
        "id": "LNI",
        "text": "Linti",
        "deprecated": false
    },
    {
        "id": "LNK",
        "text": "Linktek S.A.C.",
        "deprecated": false
    },
    {
        "id": "LNS",
        "text": "Lucan\u0027s Distribuzioni",
        "deprecated": false
    },
    {
        "id": "LNW",
        "text": "Lottery Now, Inc.",
        "deprecated": false
    },
    {
        "id": "LNZ",
        "text": "Lotto New Zealand",
        "deprecated": false
    },
    {
        "id": "LOA",
        "text": "Logispin Austria GmbH",
        "deprecated": false
    },
    {
        "id": "LOD",
        "text": "Lodestar Gaming LLC",
        "deprecated": false
    },
    {
        "id": "LOG",
        "text": "LOGICAL GAMES 46 IMPORTACION Y SERVICIOS, SL",
        "deprecated": false
    },
    {
        "id": "LOL",
        "text": "Lot.to Systems Limited",
        "deprecated": false
    },
    {
        "id": "LOM",
        "text": "LO MOOSE#244",
        "deprecated": false
    },
    {
        "id": "LON",
        "text": "Longitude LLC",
        "deprecated": false
    },
    {
        "id": "LOP",
        "text": "Lottomatrix Operations Limited",
        "deprecated": false
    },
    {
        "id": "LOS",
        "text": "Lotería Nacional Sociedad del Estado",
        "deprecated": false
    },
    {
        "id": "LOT",
        "text": "Lottomatica S.p.A.",
        "deprecated": false
    },
    {
        "id": "LOV",
        "text": "APP. LOVE SOFT SRLS",
        "deprecated": false
    },
    {
        "id": "LOW",
        "text": "Lotterywest",
        "deprecated": false
    },
    {
        "id": "LP2",
        "text": "LP ES PLC",
        "deprecated": false
    },
    {
        "id": "LPE",
        "text": "La Pausa Entretenimientos",
        "deprecated": false
    },
    {
        "id": "LPI",
        "text": "Lightning Gaming, Inc.",
        "deprecated": false
    },
    {
        "id": "LPO",
        "text": "Lopoca Gaming Limited",
        "deprecated": false
    },
    {
        "id": "LQ1",
        "text": "La Societe Des Loteries Video Du Quebec Inc",
        "deprecated": false
    },
    {
        "id": "LQB",
        "text": "Loto-Quebec",
        "deprecated": false
    },
    {
        "id": "LRB",
        "text": "LRB Enterprises",
        "deprecated": false
    },
    {
        "id": "LRC",
        "text": "LITTLE RIVER CASINO",
        "deprecated": false
    },
    {
        "id": "LRO",
        "text": "Lewis and Roca LLP",
        "deprecated": false
    },
    {
        "id": "LRP",
        "text": "Lotto Rheinland-Pfalz GmbH",
        "deprecated": false
    },
    {
        "id": "LRS",
        "text": "Logispin RS doo",
        "deprecated": false
    },
    {
        "id": "LSA",
        "text": "Lottoland South Africa (Pty) Ltd",
        "deprecated": false
    },
    {
        "id": "LSC",
        "text": "Laser Center Games",
        "deprecated": false
    },
    {
        "id": "LSG",
        "text": "Lower Sioux Gaming Commission",
        "deprecated": false
    },
    {
        "id": "LSI",
        "text": "Logical Solutions International",
        "deprecated": false
    },
    {
        "id": "LSL",
        "text": "Little Star Media Limited",
        "deprecated": false
    },
    {
        "id": "LSM",
        "text": "LIBERTY SLOT MACHINE",
        "deprecated": false
    },
    {
        "id": "LST",
        "text": "Losimu Strategine Grupe UAB",
        "deprecated": false
    },
    {
        "id": "LT1",
        "text": "Lithuania (non-billable)",
        "deprecated": false
    },
    {
        "id": "LTH",
        "text": "Lotto Hamburg GmbH",
        "deprecated": false
    },
    {
        "id": "LTJ",
        "text": "LT Game Japan Inc.",
        "deprecated": false
    },
    {
        "id": "LTL",
        "text": "Laxino Technology Limited",
        "deprecated": false
    },
    {
        "id": "LTM",
        "text": "Lotterie-Treuhandgesellschaft mbH Thüringen",
        "deprecated": false
    },
    {
        "id": "LTR",
        "text": "Let Them Roll, LLC",
        "deprecated": false
    },
    {
        "id": "LTV",
        "text": "Lucky Trip VIP Entertainment and Investment Company Limited",
        "deprecated": false
    },
    {
        "id": "LU8",
        "text": "Lucky 8",
        "deprecated": false
    },
    {
        "id": "LUC",
        "text": "Louisiana Lottery Corporation",
        "deprecated": false
    },
    {
        "id": "LUG",
        "text": "Lucky Games LLC",
        "deprecated": false
    },
    {
        "id": "LUK",
        "text": "Logispin (UK) Limited",
        "deprecated": false
    },
    {
        "id": "LUM",
        "text": "Lumiere Place",
        "deprecated": false
    },
    {
        "id": "LUN",
        "text": "Lunar Gain Limited",
        "deprecated": false
    },
    {
        "id": "LUP",
        "text": "Louisiana State Police Charitable Bingo Division",
        "deprecated": false
    },
    {
        "id": "LV1",
        "text": "Latvia (non-billable)",
        "deprecated": false
    },
    {
        "id": "LVC",
        "text": "Las Vegas Sands Corporation",
        "deprecated": false
    },
    {
        "id": "LVD",
        "text": "Las Vegas Dissemination Company",
        "deprecated": false
    },
    {
        "id": "LVG",
        "text": "Las Vegas Single Hand",
        "deprecated": false
    },
    {
        "id": "LVI",
        "text": "Lac Vieux Desert Band of Lake Superior Chippewa Indians",
        "deprecated": false
    },
    {
        "id": "LVN",
        "text": "Lottovate Nederland B.V.",
        "deprecated": false
    },
    {
        "id": "LVR",
        "text": "Lottomatica Videolot Rete S.p.A.",
        "deprecated": false
    },
    {
        "id": "LVS",
        "text": "Las Vegas Games, S.R.L.",
        "deprecated": false
    },
    {
        "id": "LW1",
        "text": "Lotto Warehouse Limited",
        "deprecated": false
    },
    {
        "id": "LWG",
        "text": "Livewire Gaming Limited",
        "deprecated": false
    },
    {
        "id": "LWL",
        "text": "Lucky Win Limited",
        "deprecated": false
    },
    {
        "id": "LXN",
        "text": "Laxino Systems Limited",
        "deprecated": false
    },
    {
        "id": "LYL",
        "text": "LATREY LIMITED",
        "deprecated": false
    },
    {
        "id": "LYT",
        "text": "Lytton Tribal Gaming Commission",
        "deprecated": false
    },
    {
        "id": "M21",
        "text": "Million-2-1",
        "deprecated": false
    },
    {
        "id": "M8I",
        "text": "M8 Investment Group Limited",
        "deprecated": false
    },
    {
        "id": "MA1",
        "text": "Marathon Alderney Limited",
        "deprecated": false
    },
    {
        "id": "MA2",
        "text": "Mach Ltd dba ChampionStudio.net",
        "deprecated": false
    },
    {
        "id": "MAA",
        "text": "Malta Gaming Authority",
        "deprecated": false
    },
    {
        "id": "MAB",
        "text": "Mariblu Srl",
        "deprecated": false
    },
    {
        "id": "MAC",
        "text": "Maria Cristescu SL",
        "deprecated": false
    },
    {
        "id": "MAD",
        "text": "MADtek Games LLC",
        "deprecated": false
    },
    {
        "id": "MAE",
        "text": "State of Missouri Office of the Attorney General",
        "deprecated": false
    },
    {
        "id": "MAF",
        "text": "Ministry of Tourism, Economic Affairs, Transportation \u0026 Telecommunications",
        "deprecated": false
    },
    {
        "id": "MAG",
        "text": "MAGIC ENTERTAINMENT",
        "deprecated": false
    },
    {
        "id": "MAI",
        "text": "Masilor S.A.",
        "deprecated": false
    },
    {
        "id": "MAL",
        "text": "Massachusetts Lottery",
        "deprecated": false
    },
    {
        "id": "MAM",
        "text": "Marim S.r.l.",
        "deprecated": false
    },
    {
        "id": "MAN",
        "text": "Manitoba Gaming Control Commission",
        "deprecated": false
    },
    {
        "id": "MAP",
        "text": "Magnet Gaming ApS",
        "deprecated": false
    },
    {
        "id": "MAR",
        "text": "Crane Payment Innovations, Inc. (CPI, Inc.)",
        "deprecated": false
    },
    {
        "id": "MAS",
        "text": "Masque Publishing Inc",
        "deprecated": false
    },
    {
        "id": "MAT",
        "text": "Mattco Gaming LLC",
        "deprecated": false
    },
    {
        "id": "MAV",
        "text": "Margaritaville Casino",
        "deprecated": false
    },
    {
        "id": "MAX",
        "text": "Maximum",
        "deprecated": false
    },
    {
        "id": "MAY",
        "text": "MAYGAY MACHINES LTD",
        "deprecated": false
    },
    {
        "id": "MAZ",
        "text": "MAZOOMA GAMES LTD",
        "deprecated": false
    },
    {
        "id": "MBD",
        "text": "MBDCO, Inc",
        "deprecated": false
    },
    {
        "id": "MBE",
        "text": "Millennium Bet",
        "deprecated": false
    },
    {
        "id": "MBG",
        "text": "My Box Gaming",
        "deprecated": false
    },
    {
        "id": "MBK",
        "text": "Matchbook LTD",
        "deprecated": false
    },
    {
        "id": "MBM",
        "text": "Morongo Band of Mission Indians",
        "deprecated": false
    },
    {
        "id": "MBO",
        "text": "Mad Bookie Pty Ltd",
        "deprecated": false
    },
    {
        "id": "MBP",
        "text": "MANCHESTER BAND POMO",
        "deprecated": false
    },
    {
        "id": "MBR",
        "text": "Multi Brand Gaming Limited",
        "deprecated": false
    },
    {
        "id": "MBS",
        "text": "Marina Bay Sands PTE LTD",
        "deprecated": false
    },
    {
        "id": "MBT",
        "text": "mBet Solutions N.V.",
        "deprecated": false
    },
    {
        "id": "MC1",
        "text": "Macedonia (non-billable)",
        "deprecated": false
    },
    {
        "id": "MCA",
        "text": "MCA PROCESSING LLC.",
        "deprecated": false
    },
    {
        "id": "MCB",
        "text": "M\u0026C Gaming Development",
        "deprecated": false
    },
    {
        "id": "MCC",
        "text": "MICRO CLEVER CONSUL",
        "deprecated": false
    },
    {
        "id": "MCD",
        "text": "Maine Gambling Control Board",
        "deprecated": false
    },
    {
        "id": "MCE",
        "text": "McCann Erickson USA, Inc.",
        "deprecated": false
    },
    {
        "id": "MCG",
        "text": "Manilamatic Computer Games",
        "deprecated": false
    },
    {
        "id": "MCH",
        "text": "Michigan Gaming Control Board",
        "deprecated": false
    },
    {
        "id": "MCI",
        "text": "Money Controls, Inc.",
        "deprecated": false
    },
    {
        "id": "MCM",
        "text": "Microgame",
        "deprecated": false
    },
    {
        "id": "MCN",
        "text": "Manson Corp. NV",
        "deprecated": false
    },
    {
        "id": "MCO",
        "text": "Mississippi Gaming Commission",
        "deprecated": false
    },
    {
        "id": "MCP",
        "text": "Macao Polytechnic Institute",
        "deprecated": false
    },
    {
        "id": "MCR",
        "text": "MICROHARD ELETTRONICA s.r.l.",
        "deprecated": false
    },
    {
        "id": "MCS",
        "text": "Melange Computer Services",
        "deprecated": false
    },
    {
        "id": "MCT",
        "text": "Macau CAA Information Technology ltd.",
        "deprecated": false
    },
    {
        "id": "MCY",
        "text": "Mercury Gaming Inc.",
        "deprecated": false
    },
    {
        "id": "MDA",
        "text": "Mansion Digital Limited",
        "deprecated": false
    },
    {
        "id": "MDE",
        "text": "MGM Grand Detroit LLC",
        "deprecated": false
    },
    {
        "id": "MDG",
        "text": "Maryland Attny Gen",
        "deprecated": false
    },
    {
        "id": "MDI",
        "text": "MDI Entertainment",
        "deprecated": false
    },
    {
        "id": "MDL",
        "text": "Maryland Live Casino",
        "deprecated": false
    },
    {
        "id": "MDM",
        "text": "Minedomain Inc.",
        "deprecated": false
    },
    {
        "id": "MDN",
        "text": "Mandalorian Technologies Limited",
        "deprecated": false
    },
    {
        "id": "MDP",
        "text": "Midasplayer.com Ltd",
        "deprecated": false
    },
    {
        "id": "MDR",
        "text": "Middletown Rancheria Tribal Gaming Regulatory Agency",
        "deprecated": false
    },
    {
        "id": "MDS",
        "text": "Magic Dreams S.r.I.",
        "deprecated": false
    },
    {
        "id": "MDT",
        "text": "Arion Labs Co., Ltd.",
        "deprecated": false
    },
    {
        "id": "MDW",
        "text": "Mindway AI ApS",
        "deprecated": false
    },
    {
        "id": "MDX",
        "text": "Medaxion, Inc.",
        "deprecated": false
    },
    {
        "id": "ME1",
        "text": "Magilen Enterprises, Inc",
        "deprecated": false
    },
    {
        "id": "ME2",
        "text": "MEDELLA S A",
        "deprecated": false
    },
    {
        "id": "MEA",
        "text": "Modern Gaming East LLC",
        "deprecated": false
    },
    {
        "id": "MEC",
        "text": "M. \u0026 C. Control",
        "deprecated": false
    },
    {
        "id": "MED",
        "text": "Mediagame s.r.l.",
        "deprecated": false
    },
    {
        "id": "MEE",
        "text": "MAG Elettronica SRL",
        "deprecated": false
    },
    {
        "id": "MEG",
        "text": "Mega Roll, LLC",
        "deprecated": false
    },
    {
        "id": "MEI",
        "text": "Medoc Investments S.A",
        "deprecated": false
    },
    {
        "id": "MEL",
        "text": "MELANGE COMPUTER",
        "deprecated": false
    },
    {
        "id": "MEM",
        "text": "Megamatic S.r.l.",
        "deprecated": false
    },
    {
        "id": "MEN",
        "text": "Meridian Gaming Ltd.",
        "deprecated": false
    },
    {
        "id": "MEO",
        "text": "Medios Electronicos y de Comunicacion, Sapi de C.V.",
        "deprecated": false
    },
    {
        "id": "MEP",
        "text": "Medsphere Systems Corporation",
        "deprecated": false
    },
    {
        "id": "MES",
        "text": "Mescalero Apache Tribal Gaming Commission",
        "deprecated": false
    },
    {
        "id": "MET",
        "text": "Metronia Online S.L.",
        "deprecated": false
    },
    {
        "id": "MFE",
        "text": "Ministry of Finance \u0026 Economy - Trinidad and Tobago",
        "deprecated": false
    },
    {
        "id": "MFH",
        "text": "Mifal Hapais - Israel National Lottery",
        "deprecated": false
    },
    {
        "id": "MFI",
        "text": "Ming Fa International Group Limited",
        "deprecated": false
    },
    {
        "id": "MFM",
        "text": "MARK F McINERNEY",
        "deprecated": false
    },
    {
        "id": "MFR",
        "text": "Gran Melia Golf Resort and Villa",
        "deprecated": false
    },
    {
        "id": "MFT",
        "text": "Ministry of FInance Turks and Caicos",
        "deprecated": false
    },
    {
        "id": "MFW",
        "text": "Micros Fidelio Worldwide Inc",
        "deprecated": false
    },
    {
        "id": "MG1",
        "text": "Mansion (Gibraltar) Ltd.",
        "deprecated": false
    },
    {
        "id": "MG2",
        "text": "Metric Gaming LLC",
        "deprecated": false
    },
    {
        "id": "MG3",
        "text": "SIA Mr Green Latvia",
        "deprecated": false
    },
    {
        "id": "MGA",
        "text": "Merkur Gaming GmbH",
        "deprecated": false
    },
    {
        "id": "MGB",
        "text": "MESA GRANDE BAND",
        "deprecated": false
    },
    {
        "id": "MGC",
        "text": "Missouri Gaming Commission",
        "deprecated": false
    },
    {
        "id": "MGD",
        "text": "MOUNTAIN GAMING DIST",
        "deprecated": false
    },
    {
        "id": "MGE",
        "text": "MAK Gaming Enterprises, LLC",
        "deprecated": false
    },
    {
        "id": "MGG",
        "text": "Magic Games di Mancuso Gia",
        "deprecated": false
    },
    {
        "id": "MGK",
        "text": "Mascot Gaming Solutions Inc.",
        "deprecated": false
    },
    {
        "id": "MGL",
        "text": "Majestic Gaming PTY Ltd",
        "deprecated": false
    },
    {
        "id": "MGM",
        "text": "MGM Resorts International",
        "deprecated": false
    },
    {
        "id": "MGN",
        "text": "Magner Corporation of America",
        "deprecated": false
    },
    {
        "id": "MGO",
        "text": "M3 GAMING OF OKLAHOMA",
        "deprecated": false
    },
    {
        "id": "MGP",
        "text": "MICHAEL G. PORTO",
        "deprecated": false
    },
    {
        "id": "MGR",
        "text": "Magellan Robotech Limited",
        "deprecated": false
    },
    {
        "id": "MGS",
        "text": "MIDWEST GAME SUPPLY",
        "deprecated": false
    },
    {
        "id": "MGT",
        "text": "MULTIPLAYER GAMING TECH Inc",
        "deprecated": false
    },
    {
        "id": "MGU",
        "text": "Machines Games Automatics",
        "deprecated": false
    },
    {
        "id": "MGV",
        "text": "Millennium Games for Virginia Charitable",
        "deprecated": false
    },
    {
        "id": "MGX",
        "text": "Moligix sh.p.k.",
        "deprecated": false
    },
    {
        "id": "MHF",
        "text": "Merkur Casino Hoofddorp",
        "deprecated": false
    },
    {
        "id": "MHG",
        "text": "Massachusetts Gaming Commission",
        "deprecated": false
    },
    {
        "id": "MHI",
        "text": "MOHIO International",
        "deprecated": false
    },
    {
        "id": "MIC",
        "text": "MICRO MFG., INC.",
        "deprecated": false
    },
    {
        "id": "MID",
        "text": "MIDWAY GAMES, INC.",
        "deprecated": false
    },
    {
        "id": "MIG",
        "text": "Mike Gordon",
        "deprecated": false
    },
    {
        "id": "MII",
        "text": "Mayfair International Technologies, Inc.",
        "deprecated": false
    },
    {
        "id": "MIL",
        "text": "MICHIGAN LOTTERY",
        "deprecated": false
    },
    {
        "id": "MIM",
        "text": "M\u0026M Internet Marketing",
        "deprecated": false
    },
    {
        "id": "MIN",
        "text": "MINNESOTA STATE LOTE",
        "deprecated": false
    },
    {
        "id": "MIO",
        "text": "Miomni Gaming Limited",
        "deprecated": false
    },
    {
        "id": "MIP",
        "text": "Mind Play",
        "deprecated": false
    },
    {
        "id": "MIT",
        "text": "Micro Technologies LLC",
        "deprecated": false
    },
    {
        "id": "MIY",
        "text": "MGT Interactive",
        "deprecated": false
    },
    {
        "id": "MJA",
        "text": "Maja Solutions Sdn. Bhd.",
        "deprecated": false
    },
    {
        "id": "MJP",
        "text": "Mills James Producti",
        "deprecated": false
    },
    {
        "id": "MKD",
        "text": "Mikado Games Limited",
        "deprecated": false
    },
    {
        "id": "MKL",
        "text": "Mobile Kik Limited",
        "deprecated": false
    },
    {
        "id": "MKO",
        "text": "mkodo Limited",
        "deprecated": false
    },
    {
        "id": "MKR",
        "text": "Marketing Results Inc.",
        "deprecated": false
    },
    {
        "id": "ML1",
        "text": "MLC ?",
        "deprecated": false
    },
    {
        "id": "MLB",
        "text": "Mille Lacs Band of Ojibwe",
        "deprecated": false
    },
    {
        "id": "MLC",
        "text": "Manitoba Liquor and Lotteries",
        "deprecated": false
    },
    {
        "id": "MLE",
        "text": "Milele Entertainment LLC",
        "deprecated": false
    },
    {
        "id": "MLG",
        "text": "Macao Live Games Ltd",
        "deprecated": false
    },
    {
        "id": "MLI",
        "text": "Medialivecasino Ltd",
        "deprecated": false
    },
    {
        "id": "MLL",
        "text": "MyLotto 24 ltd",
        "deprecated": false
    },
    {
        "id": "MLO",
        "text": "Maltco Lotteries Ltd",
        "deprecated": false
    },
    {
        "id": "MLR",
        "text": "Casino del Mar La Concha Resort",
        "deprecated": false
    },
    {
        "id": "MLS",
        "text": "Mille Lacs Corporate Ventures",
        "deprecated": false
    },
    {
        "id": "MLV",
        "text": "MGM Grand Hotel LLC dba MGM Grand Las Vegas",
        "deprecated": false
    },
    {
        "id": "MLY",
        "text": "Marlony S.A. ",
        "deprecated": false
    },
    {
        "id": "MMG",
        "text": "Everi Games, Inc.",
        "deprecated": false
    },
    {
        "id": "MMH",
        "text": "Mega Millionair Holdings",
        "deprecated": false
    },
    {
        "id": "MMI",
        "text": "MMI",
        "deprecated": false
    },
    {
        "id": "MMK",
        "text": "Micromarket",
        "deprecated": false
    },
    {
        "id": "MML",
        "text": "Maxbet Malta Limited",
        "deprecated": false
    },
    {
        "id": "MMM",
        "text": "MGM Macau",
        "deprecated": false
    },
    {
        "id": "MMR",
        "text": "MISS MARQUETTE RB C",
        "deprecated": false
    },
    {
        "id": "MMY",
        "text": "Sega Sammy Creation",
        "deprecated": false
    },
    {
        "id": "MN1",
        "text": "Minnesota Shakopee (non-billable)",
        "deprecated": false
    },
    {
        "id": "MN2",
        "text": "MNG Solutions Limited",
        "deprecated": false
    },
    {
        "id": "MNC",
        "text": "Moonlite Casino (Pty) Ltd",
        "deprecated": false
    },
    {
        "id": "MND",
        "text": "Mondo Gaming Ltd. dba MondoGaming",
        "deprecated": false
    },
    {
        "id": "MNE",
        "text": "Minnesota Alcohol \u0026 Gambling Enforcement Division",
        "deprecated": false
    },
    {
        "id": "MNG",
        "text": "Mohican Gaming Comm.",
        "deprecated": false
    },
    {
        "id": "MNH",
        "text": "MGM National Harbor",
        "deprecated": false
    },
    {
        "id": "MNL",
        "text": "Maine Lottery",
        "deprecated": false
    },
    {
        "id": "MNR",
        "text": "Monster Gaming, Inc.",
        "deprecated": false
    },
    {
        "id": "MNS",
        "text": "Minnesota Department of Public Safety",
        "deprecated": false
    },
    {
        "id": "MNT",
        "text": "Manati Hotel Company LLC",
        "deprecated": false
    },
    {
        "id": "MNW",
        "text": "MONOWIND CASINO",
        "deprecated": false
    },
    {
        "id": "MNZ",
        "text": "MANZANITA TRIBE",
        "deprecated": false
    },
    {
        "id": "MOC",
        "text": "Montreign Operating Company, LLC",
        "deprecated": false
    },
    {
        "id": "MOD",
        "text": "Mondogiochi s.r.l",
        "deprecated": false
    },
    {
        "id": "MOG",
        "text": "MODERN GAMING INC",
        "deprecated": false
    },
    {
        "id": "MOH",
        "text": "MOHEGAN-SUN",
        "deprecated": false
    },
    {
        "id": "MOL",
        "text": "Montana Lottery",
        "deprecated": false
    },
    {
        "id": "MOM",
        "text": "Momaco B.V.",
        "deprecated": false
    },
    {
        "id": "MON",
        "text": "Mondial Games SRL",
        "deprecated": false
    },
    {
        "id": "MOR",
        "text": "Morongo Gaming Agency",
        "deprecated": false
    },
    {
        "id": "MOS",
        "text": "Mobile Solutions Consulting Group S.L.",
        "deprecated": false
    },
    {
        "id": "MOT",
        "text": "MERIT NJ Divsion of Alcoholic Beverages",
        "deprecated": false
    },
    {
        "id": "MOV",
        "text": "Mondo Virtuale",
        "deprecated": false
    },
    {
        "id": "MPA",
        "text": "Mpm Automatics",
        "deprecated": false
    },
    {
        "id": "MPC",
        "text": "MP Gaming Consulting Services PTY LTD",
        "deprecated": false
    },
    {
        "id": "MPE",
        "text": "Mitchell Pearson",
        "deprecated": false
    },
    {
        "id": "MPG",
        "text": "Mpumalanga Gaming Board",
        "deprecated": false
    },
    {
        "id": "MPH",
        "text": "MINERAL PALACE HOTEL",
        "deprecated": false
    },
    {
        "id": "MPI",
        "text": "Manchester Band of Pomo Indians",
        "deprecated": false
    },
    {
        "id": "MPL",
        "text": "Melco Resorts Services Limited",
        "deprecated": false
    },
    {
        "id": "MPM",
        "text": "M.P.M Service SRL",
        "deprecated": false
    },
    {
        "id": "MPO",
        "text": "Mission Poker Inc",
        "deprecated": false
    },
    {
        "id": "MPP",
        "text": "Manatt Phelps \u0026 Phillips LLP",
        "deprecated": false
    },
    {
        "id": "MPR",
        "text": "Marpro SRL",
        "deprecated": false
    },
    {
        "id": "MPS",
        "text": "Mobile Program Solutions LLC",
        "deprecated": false
    },
    {
        "id": "MQH",
        "text": "Marquee Holdings Ltd",
        "deprecated": false
    },
    {
        "id": "MRA",
        "text": "Michael R. Aboumrad",
        "deprecated": false
    },
    {
        "id": "MRC",
        "text": "Mayaguez Resort Casino",
        "deprecated": false
    },
    {
        "id": "MRE",
        "text": "Mara Enterprises Ltd. ",
        "deprecated": false
    },
    {
        "id": "MRF",
        "text": "Mario Fernandez",
        "deprecated": false
    },
    {
        "id": "MRG",
        "text": "Mr Green Limited",
        "deprecated": false
    },
    {
        "id": "MRK",
        "text": "Merkur Interactive Malta plc",
        "deprecated": false
    },
    {
        "id": "MRL",
        "text": "MISSOURI LOTTERY",
        "deprecated": false
    },
    {
        "id": "MRR",
        "text": "MOORETOWN RANCHERIA",
        "deprecated": false
    },
    {
        "id": "MRS",
        "text": "MRG Spain PLC",
        "deprecated": false
    },
    {
        "id": "MRT",
        "text": "\u0027Everi (formerly Micro Gaming Technologies, Inc.)",
        "deprecated": false
    },
    {
        "id": "MRX",
        "text": "Maroxx Software GmbH",
        "deprecated": false
    },
    {
        "id": "MS2",
        "text": "M.S.L. LLC",
        "deprecated": false
    },
    {
        "id": "MS3",
        "text": "Marathonbet Spain S.A.",
        "deprecated": false
    },
    {
        "id": "MS4",
        "text": "Midel Studios SL",
        "deprecated": false
    },
    {
        "id": "MSA",
        "text": "Maryland Lottery and Gaming Control Agency",
        "deprecated": false
    },
    {
        "id": "MSC",
        "text": "MAJESTIC STAR CASINO",
        "deprecated": false
    },
    {
        "id": "MSE",
        "text": "Media Systems Technologies S.r.L.",
        "deprecated": false
    },
    {
        "id": "MSF",
        "text": "Macau Swift Resort Corporation",
        "deprecated": false
    },
    {
        "id": "MSG",
        "text": "Muckleshoot Tribal Gaming Commission",
        "deprecated": false
    },
    {
        "id": "MSH",
        "text": "Macon County Sheriff\u0027s Office",
        "deprecated": false
    },
    {
        "id": "MSI",
        "text": "Most Success International Group Limited",
        "deprecated": false
    },
    {
        "id": "MSK",
        "text": "mskgaming.com",
        "deprecated": false
    },
    {
        "id": "MSL",
        "text": "MULTI-STATE LOTTERY",
        "deprecated": false
    },
    {
        "id": "MSM",
        "text": "Multigame speelautomaten",
        "deprecated": false
    },
    {
        "id": "MSO",
        "text": "Mediatech Solutions S.L.",
        "deprecated": false
    },
    {
        "id": "MSP",
        "text": "MSP Technlogies Inc.",
        "deprecated": false
    },
    {
        "id": "MSR",
        "text": "Multiloteri s.r.o.",
        "deprecated": false
    },
    {
        "id": "MSS",
        "text": "McPhillips Street Station",
        "deprecated": false
    },
    {
        "id": "MST",
        "text": "Master Axis Ltd",
        "deprecated": false
    },
    {
        "id": "MSW",
        "text": "Apricot Investments Limited",
        "deprecated": false
    },
    {
        "id": "MSY",
        "text": "Microsystem Controls Pty Ltd",
        "deprecated": false
    },
    {
        "id": "MSZ",
        "text": "Marek Stankiewicz",
        "deprecated": false
    },
    {
        "id": "MT1",
        "text": "Malta (non-billable)",
        "deprecated": false
    },
    {
        "id": "MT2",
        "text": "Michigan Tribal (non-billable)",
        "deprecated": false
    },
    {
        "id": "MT3",
        "text": "Minnesota Tribal (non-billable)",
        "deprecated": false
    },
    {
        "id": "MTA",
        "text": "Manitronica srl",
        "deprecated": false
    },
    {
        "id": "MTB",
        "text": "M-Tabs LLC",
        "deprecated": false
    },
    {
        "id": "MTC",
        "text": "Mark Twain Casino",
        "deprecated": false
    },
    {
        "id": "MTD",
        "text": "MTD GAMING, INC.",
        "deprecated": false
    },
    {
        "id": "MTE",
        "text": "More Tech Group Ltd",
        "deprecated": false
    },
    {
        "id": "MTG",
        "text": "Menominee Tribal Gaming Commission",
        "deprecated": false
    },
    {
        "id": "MTH",
        "text": "Ministry of Tourism, Culture and Heritage-Turks and Caicos",
        "deprecated": false
    },
    {
        "id": "MTI",
        "text": "Miccosukee Tribal Gaming Agency",
        "deprecated": false
    },
    {
        "id": "MTL",
        "text": "Mediatel",
        "deprecated": false
    },
    {
        "id": "MTO",
        "text": "Miami Tribe of Oklahoma Nation Gaming Commission",
        "deprecated": false
    },
    {
        "id": "MTP",
        "text": "Mountaineer Casino, Racetrack \u0026 Resort",
        "deprecated": false
    },
    {
        "id": "MTS",
        "text": "Metis TPS, LLC",
        "deprecated": false
    },
    {
        "id": "MTX",
        "text": "Marker Trax, LLC",
        "deprecated": false
    },
    {
        "id": "MUA",
        "text": "Multilot AB",
        "deprecated": false
    },
    {
        "id": "MUC",
        "text": "Muckleshoot Casino",
        "deprecated": false
    },
    {
        "id": "MUL",
        "text": "MultiSlot Limited",
        "deprecated": false
    },
    {
        "id": "MUN",
        "text": "Mundo Video",
        "deprecated": false
    },
    {
        "id": "MUS",
        "text": "Muscogee Creek Nation Office of Public Gaming",
        "deprecated": false
    },
    {
        "id": "MVG",
        "text": "MOVILGATE S.R.L.",
        "deprecated": false
    },
    {
        "id": "MVJ",
        "text": "Ministerie van Velligheid en Justitie",
        "deprecated": false
    },
    {
        "id": "MVS",
        "text": "MVS Assessoria e Gestao de Negocios Ltda.",
        "deprecated": false
    },
    {
        "id": "MWG",
        "text": "Moret World Gaming",
        "deprecated": false
    },
    {
        "id": "MWL",
        "text": "Icon Gaming Ltd.",
        "deprecated": false
    },
    {
        "id": "MWN",
        "text": "Merkur Win S.r.l.",
        "deprecated": false
    },
    {
        "id": "MWS",
        "text": "MAKEWISE",
        "deprecated": false
    },
    {
        "id": "MX1",
        "text": "Mexico SAT (non-billable)",
        "deprecated": false
    },
    {
        "id": "MY5",
        "text": "My5050 Inc.",
        "deprecated": false
    },
    {
        "id": "MYL",
        "text": "Mystic Lake Casino Hotel",
        "deprecated": false
    },
    {
        "id": "MYS",
        "text": "Mystique Casino",
        "deprecated": false
    },
    {
        "id": "MZA",
        "text": "Maize Analytics, Inc.",
        "deprecated": false
    },
    {
        "id": "N1W",
        "text": "No. 1 World Security Technology Inc.",
        "deprecated": false
    },
    {
        "id": "N4U",
        "text": "NET4UONLINE PVT. LTD",
        "deprecated": false
    },
    {
        "id": "NA1",
        "text": "Northern Arapaho Gaming Agency",
        "deprecated": false
    },
    {
        "id": "NAE",
        "text": "Naskila Entertainment",
        "deprecated": false
    },
    {
        "id": "NAG",
        "text": "Native Games America, LLC",
        "deprecated": false
    },
    {
        "id": "NAK",
        "text": "NAKUKOP LTD, odštěpný závod",
        "deprecated": false
    },
    {
        "id": "NAM",
        "text": "NetEnt Americas LLC",
        "deprecated": false
    },
    {
        "id": "NAN",
        "text": "Nanoptix Inc.",
        "deprecated": false
    },
    {
        "id": "NAO",
        "text": "NAOE Corp",
        "deprecated": false
    },
    {
        "id": "NAP",
        "text": "Nappi Rosario",
        "deprecated": false
    },
    {
        "id": "NAS",
        "text": "Naked Ape Studios Ltd.",
        "deprecated": false
    },
    {
        "id": "NAZ",
        "text": "Na Zvezi d.o.o.",
        "deprecated": false
    },
    {
        "id": "NBA",
        "text": "NBA Properties, Inc.",
        "deprecated": false
    },
    {
        "id": "NBC",
        "text": "New Brunswick Casino",
        "deprecated": false
    },
    {
        "id": "NBD",
        "text": "New Brunswick Department of Public Safety",
        "deprecated": false
    },
    {
        "id": "NBE",
        "text": "NetBet Enterprises limited",
        "deprecated": false
    },
    {
        "id": "NBI",
        "text": "New BI US Gaming, LLC dba VizExplorer",
        "deprecated": false
    },
    {
        "id": "NBL",
        "text": "NEBRASKA LOTTERY",
        "deprecated": false
    },
    {
        "id": "NBU",
        "text": "NBUSTER Co., Inc.",
        "deprecated": false
    },
    {
        "id": "NCD",
        "text": "North Carolina District Attorneys Office",
        "deprecated": false
    },
    {
        "id": "NCG",
        "text": "Northern Cape Gaming Board",
        "deprecated": false
    },
    {
        "id": "NCL",
        "text": "North Carolina Lottery",
        "deprecated": false
    },
    {
        "id": "NCO",
        "text": "North Carolina Office of the United States Attorney",
        "deprecated": false
    },
    {
        "id": "NCP",
        "text": "North Carolina Department of Public Safety, Alcohol Law Enforcement Branch",
        "deprecated": false
    },
    {
        "id": "NCR",
        "text": "NCR Corporation",
        "deprecated": false
    },
    {
        "id": "NDE",
        "text": "Indie Gaming Eugenios Katravas",
        "deprecated": false
    },
    {
        "id": "NDG",
        "text": "NYX Digital Gaming (Malta) Ltd",
        "deprecated": false
    },
    {
        "id": "NDH",
        "text": "NeoDeck Holdings Corp.",
        "deprecated": false
    },
    {
        "id": "NDL",
        "text": "North Dakota Lottery",
        "deprecated": false
    },
    {
        "id": "NDO",
        "text": "North Dakota Office of Attorney General",
        "deprecated": false
    },
    {
        "id": "NE1",
        "text": "NetEnt Product Services Ltd (previously Net Entertainment Malta Services Ltd)",
        "deprecated": false
    },
    {
        "id": "NEA",
        "text": "Neptune Assets Ltd",
        "deprecated": false
    },
    {
        "id": "NEB",
        "text": "Nebraska Department of Revenue, Charitable Gaming Division",
        "deprecated": false
    },
    {
        "id": "NEC",
        "text": "Neowiz Corp",
        "deprecated": false
    },
    {
        "id": "NEG",
        "text": "Netent Gaming Solutions PLC",
        "deprecated": false
    },
    {
        "id": "NEN",
        "text": "Northern Edge Navajo Casino",
        "deprecated": false
    },
    {
        "id": "NEO",
        "text": "Neon Games Co Ltd.",
        "deprecated": false
    },
    {
        "id": "NEP",
        "text": "Nez Perce Tribe",
        "deprecated": false
    },
    {
        "id": "NES",
        "text": "Nese Casino",
        "deprecated": false
    },
    {
        "id": "NET",
        "text": "NetEnt AB",
        "deprecated": false
    },
    {
        "id": "NEW",
        "text": "Newgioco Group Inc.",
        "deprecated": false
    },
    {
        "id": "NEX",
        "text": "NEXCADE CO., LTD.",
        "deprecated": false
    },
    {
        "id": "NEZ",
        "text": "Nazionale Elettronica Srl",
        "deprecated": false
    },
    {
        "id": "NG1",
        "text": "Newport Grand Slots",
        "deprecated": false
    },
    {
        "id": "NG3",
        "text": "NexGen Technology, LLC",
        "deprecated": false
    },
    {
        "id": "NGA",
        "text": "North America Gaming, Inc",
        "deprecated": false
    },
    {
        "id": "NGC",
        "text": "Nevada Gaming Control Board",
        "deprecated": false
    },
    {
        "id": "NGE",
        "text": "Netgame Entertainment N.V.",
        "deprecated": false
    },
    {
        "id": "NGG",
        "text": "NextGen Gaming Pty Ltd (Part of NYX Gaming Group)",
        "deprecated": false
    },
    {
        "id": "NGI",
        "text": "NOVA GAMING INC.",
        "deprecated": false
    },
    {
        "id": "NGL",
        "text": "Novigroup Limited",
        "deprecated": false
    },
    {
        "id": "NGM",
        "text": "NAMCO AMERICA",
        "deprecated": false
    },
    {
        "id": "NGN",
        "text": "NOVOMATIC AG - RGS System",
        "deprecated": false
    },
    {
        "id": "NGP",
        "text": "NexGen Poker",
        "deprecated": false
    },
    {
        "id": "NGT",
        "text": "NG Entertainment Limited",
        "deprecated": false
    },
    {
        "id": "NHB",
        "text": "Nottawaseppi Huron Band of the Potawatomi Gaming Commission",
        "deprecated": false
    },
    {
        "id": "NHC",
        "text": "NextGen Healthcare Information Systems, LLC",
        "deprecated": false
    },
    {
        "id": "NHL",
        "text": "New Hampshire Lottery",
        "deprecated": false
    },
    {
        "id": "NHO",
        "text": "NOVOMATIC AG – Coolfire Holland",
        "deprecated": false
    },
    {
        "id": "NIA",
        "text": "National Indian Gaming Association",
        "deprecated": false
    },
    {
        "id": "NIG",
        "text": "NAT.IND.GAMING COMM.",
        "deprecated": false
    },
    {
        "id": "NIK",
        "text": "Nikolas Tsikkou",
        "deprecated": false
    },
    {
        "id": "NJL",
        "text": "New Jersey Lottery",
        "deprecated": false
    },
    {
        "id": "NJT",
        "text": "NJ Lotto LLC",
        "deprecated": false
    },
    {
        "id": "NKB",
        "text": "Nicholas Kallabat",
        "deprecated": false
    },
    {
        "id": "NL1",
        "text": "NL Gaming Sweden AB",
        "deprecated": false
    },
    {
        "id": "NLC",
        "text": "National Lotteries Control Board",
        "deprecated": false
    },
    {
        "id": "NLG",
        "text": "Northstar Lottery Group",
        "deprecated": false
    },
    {
        "id": "NLO",
        "text": "Nederlandse Loterij Organisatie B.V.",
        "deprecated": false
    },
    {
        "id": "NLS",
        "text": "Nederlandse Staatsloterij",
        "deprecated": false
    },
    {
        "id": "NM1",
        "text": "New Mexico Gaming (non-billable)",
        "deprecated": false
    },
    {
        "id": "NM2",
        "text": "New Mexico Tribal (non-billable)",
        "deprecated": false
    },
    {
        "id": "NME",
        "text": "NMi Metrology \u0026 Gaming Ltd.",
        "deprecated": false
    },
    {
        "id": "NMG",
        "text": "NOVOMATIC AG - Magic Games HD",
        "deprecated": false
    },
    {
        "id": "NMI",
        "text": "NOVOMATIC AG - Novoline Interactive",
        "deprecated": false
    },
    {
        "id": "NML",
        "text": "New Mexico Lottery",
        "deprecated": false
    },
    {
        "id": "NMS",
        "text": "NOVOMATIC AG - Magic Games SD",
        "deprecated": false
    },
    {
        "id": "NMY",
        "text": "NOVOMATIC AG - MyACP",
        "deprecated": false
    },
    {
        "id": "NNJ",
        "text": "Northstar New Jersey Lottery Group",
        "deprecated": false
    },
    {
        "id": "NNM",
        "text": "Nanum Lotto",
        "deprecated": false
    },
    {
        "id": "NNO",
        "text": "NanoTech Gaming Labs LLC",
        "deprecated": false
    },
    {
        "id": "NNP",
        "text": "NPL News Promotions Ltd",
        "deprecated": false
    },
    {
        "id": "NOA",
        "text": "NOVACOR",
        "deprecated": false
    },
    {
        "id": "NOC",
        "text": "Nolimit City Limited",
        "deprecated": false
    },
    {
        "id": "NOJ",
        "text": "NOVOMATIC AG - Octavian Jackpot",
        "deprecated": false
    },
    {
        "id": "NOK",
        "text": "NOK Limited",
        "deprecated": false
    },
    {
        "id": "NOL",
        "text": "Nordic Odds Limited",
        "deprecated": false
    },
    {
        "id": "NOM",
        "text": "NOVOMATIC AG - Octavian Management System",
        "deprecated": false
    },
    {
        "id": "NOR",
        "text": "Nordic Lottery AB",
        "deprecated": false
    },
    {
        "id": "NOS",
        "text": "A Nossa Aposta, SA",
        "deprecated": false
    },
    {
        "id": "NPA",
        "text": "Nevada Gaming Partners LLC",
        "deprecated": false
    },
    {
        "id": "NPB",
        "text": "National Police Board",
        "deprecated": false
    },
    {
        "id": "NPG",
        "text": "Nambe Pueblo Gaming Commission",
        "deprecated": false
    },
    {
        "id": "NPI",
        "text": "NeoGames S.a.r.l.",
        "deprecated": false
    },
    {
        "id": "NPL",
        "text": "Nationale Postcode Loterij",
        "deprecated": false
    },
    {
        "id": "NPP",
        "text": "Next Payments Pty Ltd",
        "deprecated": false
    },
    {
        "id": "NPS",
        "text": "NOVOMATIC AG – Sports Betting",
        "deprecated": false
    },
    {
        "id": "NPT",
        "text": "No Peak 21, Inc.",
        "deprecated": false
    },
    {
        "id": "NPW",
        "text": "NEWPORTALWEB S.R.L.",
        "deprecated": false
    },
    {
        "id": "NQT",
        "text": "Nisqually Tribal Gaming Commission",
        "deprecated": false
    },
    {
        "id": "NRC",
        "text": "NRC Roulette Technology Ltd",
        "deprecated": false
    },
    {
        "id": "NRD",
        "text": "ENLABS AB",
        "deprecated": false
    },
    {
        "id": "NRW",
        "text": "Nisqually RedWind Casino",
        "deprecated": false
    },
    {
        "id": "NS1",
        "text": "N-Serve Ltd",
        "deprecated": false
    },
    {
        "id": "NSA",
        "text": "Nova Scotia Alcohol",
        "deprecated": false
    },
    {
        "id": "NSB",
        "text": "NSBet Ltd",
        "deprecated": false
    },
    {
        "id": "NSC",
        "text": "Nidec Sankyo America Corp",
        "deprecated": false
    },
    {
        "id": "NSD",
        "text": "Nova Scotia Dept. of Labour / Workforce Dev.",
        "deprecated": false
    },
    {
        "id": "NSF",
        "text": "Nationale Serviceorganisatie Fondsenwerving B.V.",
        "deprecated": false
    },
    {
        "id": "NSG",
        "text": "New South Wales Office of Liquor Gaming and Racing",
        "deprecated": false
    },
    {
        "id": "NSM",
        "text": "NSoft d.o.o. Mostar",
        "deprecated": false
    },
    {
        "id": "NSP",
        "text": "Novomatic Gaming Spain S. A.",
        "deprecated": false
    },
    {
        "id": "NSR",
        "text": "Netsweeper Inc",
        "deprecated": false
    },
    {
        "id": "NSU",
        "text": "NOVOMATIC AG - Superia Games",
        "deprecated": false
    },
    {
        "id": "NSW",
        "text": "Registered Clubs Association of New South Wales",
        "deprecated": false
    },
    {
        "id": "NSY",
        "text": "NAMSYS INC",
        "deprecated": false
    },
    {
        "id": "NTB",
        "text": "Northern Territory Department of Business",
        "deprecated": false
    },
    {
        "id": "NTC",
        "text": "NRT TECHNOLOGY CORP.",
        "deprecated": false
    },
    {
        "id": "NTE",
        "text": "New Technology Engineering",
        "deprecated": false
    },
    {
        "id": "NTG",
        "text": "National Table Games",
        "deprecated": false
    },
    {
        "id": "NTL",
        "text": "Neopoint Technologies Ltd",
        "deprecated": false
    },
    {
        "id": "NTM",
        "text": "New Tech Machines",
        "deprecated": false
    },
    {
        "id": "NTN",
        "text": "NTS Network S.p.A.",
        "deprecated": false
    },
    {
        "id": "NTP",
        "text": "NetPlay TV",
        "deprecated": false
    },
    {
        "id": "NTS",
        "text": "NETSTAR TECHNOLOGY",
        "deprecated": false
    },
    {
        "id": "NUG",
        "text": "Nu Games",
        "deprecated": false
    },
    {
        "id": "NUO",
        "text": "NUOVA PLAY SHOP SRL",
        "deprecated": false
    },
    {
        "id": "NVG",
        "text": "New Vision Gaming \u0026 Development, Inc.",
        "deprecated": false
    },
    {
        "id": "NVI",
        "text": "Novomatic Italia S.p.A",
        "deprecated": false
    },
    {
        "id": "NVL",
        "text": "Novomatic Lottery Solutions GmbH - Spain",
        "deprecated": false
    },
    {
        "id": "NVM",
        "text": "Novamedia B.V.",
        "deprecated": false
    },
    {
        "id": "NVP",
        "text": "NOVA Productions",
        "deprecated": false
    },
    {
        "id": "NVS",
        "text": "Novamedia Sverige AB",
        "deprecated": false
    },
    {
        "id": "NWA",
        "text": "Software Specialists Inc, (dba) New Wave Automation",
        "deprecated": false
    },
    {
        "id": "NWB",
        "text": "North West Gambling Board",
        "deprecated": false
    },
    {
        "id": "NWG",
        "text": "Newport Grand",
        "deprecated": false
    },
    {
        "id": "NWZ",
        "text": "Neowiz Play Studio Corporation",
        "deprecated": false
    },
    {
        "id": "NXT",
        "text": "Next Gaming LLC",
        "deprecated": false
    },
    {
        "id": "NYC",
        "text": "Normalización y Certificación Electrónica NYCE, S.C.",
        "deprecated": false
    },
    {
        "id": "NYG",
        "text": "New York State Gaming Commission",
        "deprecated": false
    },
    {
        "id": "NYL",
        "text": "New York Lottery",
        "deprecated": false
    },
    {
        "id": "NYS",
        "text": "New York State Board of Elections",
        "deprecated": false
    },
    {
        "id": "NYX",
        "text": "NYX Interactive",
        "deprecated": false
    },
    {
        "id": "NZ3",
        "text": "NZ360 (2015) Limited",
        "deprecated": false
    },
    {
        "id": "OAG",
        "text": "OHIO ATTORNEY GENERAL",
        "deprecated": false
    },
    {
        "id": "OAK",
        "text": "One of A Kind Gaming, LLC",
        "deprecated": false
    },
    {
        "id": "OAM",
        "text": "Online Amusement Solutions NV",
        "deprecated": false
    },
    {
        "id": "OAS",
        "text": "Oasis Gaming",
        "deprecated": false
    },
    {
        "id": "OBE",
        "text": "OBELIS LIMITED",
        "deprecated": false
    },
    {
        "id": "OBS",
        "text": "Outabox Solutions Pty Ltd",
        "deprecated": false
    },
    {
        "id": "OCD",
        "text": "Oneida County District Attorney\u0027s Office",
        "deprecated": false
    },
    {
        "id": "OCE",
        "text": "Ocean Casino",
        "deprecated": false
    },
    {
        "id": "OCG",
        "text": "Ohio Casino Control Commision",
        "deprecated": false
    },
    {
        "id": "OCI",
        "text": "Octavian Italy S.r.l.",
        "deprecated": false
    },
    {
        "id": "OCR",
        "text": "Ocean Resort Casino",
        "deprecated": false
    },
    {
        "id": "ODA",
        "text": "Octavian de Argentina",
        "deprecated": false
    },
    {
        "id": "ODC",
        "text": "Odawa Casino (LTBB of Odawa Indians)",
        "deprecated": false
    },
    {
        "id": "ODE",
        "text": "Onlinecasino Deutschland Gmbh",
        "deprecated": false
    },
    {
        "id": "ODF",
        "text": "Oasis Del Fin Del Mundo S.A.",
        "deprecated": false
    },
    {
        "id": "ODG",
        "text": "Omni Design Group",
        "deprecated": false
    },
    {
        "id": "ODI",
        "text": "Odissea Betriebsinformatik Beratung GmbH",
        "deprecated": false
    },
    {
        "id": "ODR",
        "text": "Odrex Ltd.",
        "deprecated": false
    },
    {
        "id": "ODS",
        "text": "Odin Services Limited",
        "deprecated": false
    },
    {
        "id": "OEX",
        "text": "OTG Experience, LLC",
        "deprecated": false
    },
    {
        "id": "OFR",
        "text": "One For the Revolution",
        "deprecated": false
    },
    {
        "id": "OG1",
        "text": "Oryx Gaming Limited",
        "deprecated": false
    },
    {
        "id": "OG2",
        "text": "Ocean\u0027s 3 Gaming, LLC",
        "deprecated": false
    },
    {
        "id": "OGA",
        "text": "Online Gaming Systems Australia PTY LTD",
        "deprecated": false
    },
    {
        "id": "OGC",
        "text": "Osage Nation Gaming Commission",
        "deprecated": false
    },
    {
        "id": "OGE",
        "text": "Ontario Gaming East Limited Partnership",
        "deprecated": false
    },
    {
        "id": "OGL",
        "text": "Signature Gaming (Pty) Ltd.",
        "deprecated": false
    },
    {
        "id": "OGS",
        "text": "Omega Gaming Services",
        "deprecated": false
    },
    {
        "id": "OHK",
        "text": "OHKAY Casino",
        "deprecated": false
    },
    {
        "id": "OHL",
        "text": "OHIO LOTTERY",
        "deprecated": false
    },
    {
        "id": "OHR",
        "text": "Oklahoma Horse Racing Commission",
        "deprecated": false
    },
    {
        "id": "OHS",
        "text": "One Hundered Sands Limited",
        "deprecated": false
    },
    {
        "id": "OIH",
        "text": "Ourgame International Holdings Limited",
        "deprecated": false
    },
    {
        "id": "OII",
        "text": "Ocean Miles International Investment Ltd",
        "deprecated": false
    },
    {
        "id": "OIL",
        "text": "The Optimizer Investments Limited",
        "deprecated": false
    },
    {
        "id": "OIW",
        "text": "Oneida Tribe of Indians of Wisconsin",
        "deprecated": false
    },
    {
        "id": "OK1",
        "text": "Oklahoma Tribal (Non-Billable Code for Tracking Only)",
        "deprecated": false
    },
    {
        "id": "OKG",
        "text": "OKGaming Pty Ltd",
        "deprecated": false
    },
    {
        "id": "OKL",
        "text": "OK LOTTERY COMMISSION",
        "deprecated": false
    },
    {
        "id": "OKP",
        "text": "Okie Poker",
        "deprecated": false
    },
    {
        "id": "OKT",
        "text": "Oliver Keith Tyler Jones Inc. LLC",
        "deprecated": false
    },
    {
        "id": "OKY",
        "text": "Okyanus Group",
        "deprecated": false
    },
    {
        "id": "OLC",
        "text": "ONTARIO LOTTERY",
        "deprecated": false
    },
    {
        "id": "OLD",
        "text": "Olympia Dice",
        "deprecated": false
    },
    {
        "id": "OLE",
        "text": "OLE Group International Limited",
        "deprecated": false
    },
    {
        "id": "OLG",
        "text": "Olympian Gaming LLC",
        "deprecated": false
    },
    {
        "id": "OLI",
        "text": "OLIMP (Pty) Ltd",
        "deprecated": false
    },
    {
        "id": "OLY",
        "text": "Olympic Casino Latvia",
        "deprecated": false
    },
    {
        "id": "OMG",
        "text": "Omnia Giochi",
        "deprecated": false
    },
    {
        "id": "OMI",
        "text": "Orefjar Media Interatactive",
        "deprecated": false
    },
    {
        "id": "OMN",
        "text": "OMNILIFE, INC.",
        "deprecated": false
    },
    {
        "id": "ONC",
        "text": "Organización Nacional de Ciegos Españoles (ONCE)",
        "deprecated": false
    },
    {
        "id": "OND",
        "text": "ONDACOM INC",
        "deprecated": false
    },
    {
        "id": "ONE",
        "text": "Oneida Indian Nation Gaming Commission",
        "deprecated": false
    },
    {
        "id": "ONG",
        "text": "Oneida Gaming Comission",
        "deprecated": false
    },
    {
        "id": "ONL",
        "text": "Onliner Italia Srl",
        "deprecated": false
    },
    {
        "id": "ONT",
        "text": "Online Technologies Limited",
        "deprecated": false
    },
    {
        "id": "OOP",
        "text": "Odds on Promotions",
        "deprecated": false
    },
    {
        "id": "OPA",
        "text": "OPAP SA",
        "deprecated": false
    },
    {
        "id": "OPG",
        "text": "Optimum Gaming LLC",
        "deprecated": false
    },
    {
        "id": "OPM",
        "text": "Optima™ dba Optima Gaming",
        "deprecated": false
    },
    {
        "id": "OPP",
        "text": "OAKLAND PARK POLICE",
        "deprecated": false
    },
    {
        "id": "OPS",
        "text": "Operacinės sistemos, UAB",
        "deprecated": false
    },
    {
        "id": "OPT",
        "text": "Optiwin OU",
        "deprecated": false
    },
    {
        "id": "ORC",
        "text": "Oregon Racing Commission",
        "deprecated": false
    },
    {
        "id": "ORE",
        "text": "Oriental Game Limited",
        "deprecated": false
    },
    {
        "id": "ORG",
        "text": "Orange Joker S.R.O.",
        "deprecated": false
    },
    {
        "id": "ORI",
        "text": "ORION GAMING BV",
        "deprecated": false
    },
    {
        "id": "ORL",
        "text": "2PO Inc",
        "deprecated": false
    },
    {
        "id": "ORM",
        "text": "ORing Limited",
        "deprecated": false
    },
    {
        "id": "ORS",
        "text": "ORS, Inc. dba myhELO, Inc.",
        "deprecated": false
    },
    {
        "id": "ORT",
        "text": "Ortiz Gaming",
        "deprecated": false
    },
    {
        "id": "OSA",
        "text": "Ongame Services AB",
        "deprecated": false
    },
    {
        "id": "OSG",
        "text": "OptiShot Golf LLC",
        "deprecated": false
    },
    {
        "id": "OSI",
        "text": "On Site Inspection",
        "deprecated": false
    },
    {
        "id": "OSL",
        "text": "OREGON STATE LOTTERY",
        "deprecated": false
    },
    {
        "id": "OSO",
        "text": "Ocean Servicios On Line SA",
        "deprecated": false
    },
    {
        "id": "OSP",
        "text": "OREGON STATE POLICE",
        "deprecated": false
    },
    {
        "id": "OSR",
        "text": "Oakpoint Ltd. dba SideplayR",
        "deprecated": false
    },
    {
        "id": "OSS",
        "text": "Raffle Nexus Canada Inc.",
        "deprecated": false
    },
    {
        "id": "OT1",
        "text": "One Touch Technology Limited",
        "deprecated": false
    },
    {
        "id": "OT2",
        "text": "Oregon Tribal (non-billable)",
        "deprecated": false
    },
    {
        "id": "OTC",
        "text": "Omaha Tribal Gaming Commission",
        "deprecated": false
    },
    {
        "id": "OTE",
        "text": "Outbox Technology CRB Inc.",
        "deprecated": false
    },
    {
        "id": "OTG",
        "text": "Oklahoma Tribal Gaming",
        "deprecated": false
    },
    {
        "id": "OTL",
        "text": "OpenBet Technologies Ltd.",
        "deprecated": false
    },
    {
        "id": "OTM",
        "text": "OpenBet Technologies Ltd (formerly Orbis Technology Limited) (2nd Code)",
        "deprecated": false
    },
    {
        "id": "OTN",
        "text": "OpenBet Technologies Ltd (formerly Orbis Technology Limited) (Sportsbook Product Line)",
        "deprecated": false
    },
    {
        "id": "OTO",
        "text": "Otoe-Missouria Tribe of Oklahoma Gaming Commission",
        "deprecated": false
    },
    {
        "id": "OTT",
        "text": "Otto Malta Ltd",
        "deprecated": false
    },
    {
        "id": "OTW",
        "text": "Ottawa Gaming Commission",
        "deprecated": false
    },
    {
        "id": "OTZ",
        "text": "Ortiz Gaming S.L.",
        "deprecated": false
    },
    {
        "id": "OUT",
        "text": "Outwit Inc.",
        "deprecated": false
    },
    {
        "id": "OVG",
        "text": "Olivano Gaming Creations Inc.",
        "deprecated": false
    },
    {
        "id": "OVL",
        "text": "Open Vantage Ltd / BetNow",
        "deprecated": false
    },
    {
        "id": "OWK",
        "text": "OneWorks IP Holdings Limited",
        "deprecated": false
    },
    {
        "id": "OWL",
        "text": "Oneworks Limited",
        "deprecated": false
    },
    {
        "id": "P12",
        "text": "Poker 123, LLC",
        "deprecated": false
    },
    {
        "id": "P49",
        "text": "Pipeline49 Technology Group Inc",
        "deprecated": false
    },
    {
        "id": "PA1",
        "text": "PAF Consulting Ab",
        "deprecated": false
    },
    {
        "id": "PA2",
        "text": "Panama (non-billable)",
        "deprecated": false
    },
    {
        "id": "PAA",
        "text": "Pennsylvania Attorney General",
        "deprecated": false
    },
    {
        "id": "PAB",
        "text": "Petti and Briones, PLLC",
        "deprecated": false
    },
    {
        "id": "PAC",
        "text": "PACIFIC",
        "deprecated": false
    },
    {
        "id": "PAD",
        "text": "PAR-A-DICE Hotel and Casino",
        "deprecated": false
    },
    {
        "id": "PAE",
        "text": "Paradise Entertainment",
        "deprecated": false
    },
    {
        "id": "PAF",
        "text": "Alands Penningautomatforening",
        "deprecated": false
    },
    {
        "id": "PAG",
        "text": "Pro-Aggressive Gamin",
        "deprecated": false
    },
    {
        "id": "PAI",
        "text": "Pala Interactive",
        "deprecated": false
    },
    {
        "id": "PAL",
        "text": "PALTRONICS Inc.",
        "deprecated": false
    },
    {
        "id": "PAM",
        "text": "Pauma Gaming Commission",
        "deprecated": false
    },
    {
        "id": "PAN",
        "text": "PANAMA",
        "deprecated": false
    },
    {
        "id": "PAO",
        "text": "Pace-O-Matic, Inc.",
        "deprecated": false
    },
    {
        "id": "PAP",
        "text": "PALTRONICS AUSTRALAS",
        "deprecated": false
    },
    {
        "id": "PAR",
        "text": "PAR-4",
        "deprecated": false
    },
    {
        "id": "PAT",
        "text": "Patricks Vending",
        "deprecated": false
    },
    {
        "id": "PAU",
        "text": "CASINO PAUMA",
        "deprecated": false
    },
    {
        "id": "PAV",
        "text": "Players Advantage LLC",
        "deprecated": false
    },
    {
        "id": "PAX",
        "text": "PayMaxs",
        "deprecated": false
    },
    {
        "id": "PAY",
        "text": "Pariplay Ltd.",
        "deprecated": false
    },
    {
        "id": "PB1",
        "text": "Pointsbet Pty Ltd",
        "deprecated": false
    },
    {
        "id": "PB2",
        "text": "PointsBet Indiana LLC",
        "deprecated": false
    },
    {
        "id": "PBA",
        "text": "PB Tech \u0026 Advisory LLC",
        "deprecated": false
    },
    {
        "id": "PBC",
        "text": "Prairie Band Casino",
        "deprecated": false
    },
    {
        "id": "PBD",
        "text": "Play Better Divina Ltd",
        "deprecated": false
    },
    {
        "id": "PBG",
        "text": "Probability Games",
        "deprecated": false
    },
    {
        "id": "PBI",
        "text": "Poarch Band of Creek Indians Tribal Gaming Commission",
        "deprecated": false
    },
    {
        "id": "PBL",
        "text": "Pollard Banknote Limited",
        "deprecated": false
    },
    {
        "id": "PBM",
        "text": "PALA BAND OF MISSION",
        "deprecated": false
    },
    {
        "id": "PBN",
        "text": "Paskenta Band of Nomlaki Tribal Gaming Commission",
        "deprecated": false
    },
    {
        "id": "PBP",
        "text": "Prairie Band Potawatomi Gaming Commission",
        "deprecated": false
    },
    {
        "id": "PBS",
        "text": "Webshopmatic Ltd.",
        "deprecated": false
    },
    {
        "id": "PBT",
        "text": "Playtech BGT Sports Limited",
        "deprecated": false
    },
    {
        "id": "PBU",
        "text": "PointsBet USA Inc.",
        "deprecated": false
    },
    {
        "id": "PBV",
        "text": "PlayDOM B.V.",
        "deprecated": false
    },
    {
        "id": "PCA",
        "text": "The Point Casino",
        "deprecated": false
    },
    {
        "id": "PCB",
        "text": "PLAYERS CHOICE BLACKJACK",
        "deprecated": false
    },
    {
        "id": "PCD",
        "text": "PLP Casino Game Development",
        "deprecated": false
    },
    {
        "id": "PCG",
        "text": "Pure Canadian Gaming",
        "deprecated": false
    },
    {
        "id": "PCH",
        "text": "Plan Chance LTD",
        "deprecated": false
    },
    {
        "id": "PCI",
        "text": "Play Craps, Inc.",
        "deprecated": false
    },
    {
        "id": "PCL",
        "text": "Perimeter Co. LLC",
        "deprecated": false
    },
    {
        "id": "PCM",
        "text": "PROJECT COIN MACHINE",
        "deprecated": false
    },
    {
        "id": "PCO",
        "text": "Pala Gaming Commission",
        "deprecated": false
    },
    {
        "id": "PCP",
        "text": "Plus Connect (NT) Pty Ltd",
        "deprecated": false
    },
    {
        "id": "PCQ",
        "text": "Paradise Casino - Quechan",
        "deprecated": false
    },
    {
        "id": "PCR",
        "text": "Plataforma de Apuestas Cruzadas, SA",
        "deprecated": false
    },
    {
        "id": "PCS",
        "text": "Pala Band of Mission Indians dba Pala Casino Spa and Resort",
        "deprecated": false
    },
    {
        "id": "PCY",
        "text": "Picayune Rancheria of the Chukchansi Indians Tribal Gaming Commission",
        "deprecated": false
    },
    {
        "id": "PDA",
        "text": "Prism Data Systems, Inc.",
        "deprecated": false
    },
    {
        "id": "PDC",
        "text": "Pechanga Development Corporation",
        "deprecated": false
    },
    {
        "id": "PDI",
        "text": "PERIPHERAL DYNAMICS",
        "deprecated": false
    },
    {
        "id": "PDK",
        "text": "Produkte.al Sh.P.K. dba TopTech",
        "deprecated": false
    },
    {
        "id": "PDN",
        "text": "Palese Design Srl",
        "deprecated": false
    },
    {
        "id": "PDO",
        "text": "Pando International Co. Ltd.",
        "deprecated": false
    },
    {
        "id": "PDP",
        "text": "Paddy Power",
        "deprecated": false
    },
    {
        "id": "PDR",
        "text": "Pedretti SNC",
        "deprecated": false
    },
    {
        "id": "PDS",
        "text": "PDS Financial",
        "deprecated": false
    },
    {
        "id": "PE1",
        "text": "Peru (non-billable)",
        "deprecated": false
    },
    {
        "id": "PE2",
        "text": "Peermont Global Proprietary Limited",
        "deprecated": false
    },
    {
        "id": "PEA",
        "text": "PA Entertainment \u0026 Automaten AG",
        "deprecated": false
    },
    {
        "id": "PEB",
        "text": "Personal Belongings Limited",
        "deprecated": false
    },
    {
        "id": "PEC",
        "text": "Pechanga Gaming Commission",
        "deprecated": false
    },
    {
        "id": "PED",
        "text": "PA DOR",
        "deprecated": false
    },
    {
        "id": "PEE",
        "text": "PAOKAI Electronic Enterprise",
        "deprecated": false
    },
    {
        "id": "PEG",
        "text": "Peoria Tribal Gaming Commission",
        "deprecated": false
    },
    {
        "id": "PEI",
        "text": "Pinnacle Entertainment Inc",
        "deprecated": false
    },
    {
        "id": "PEL",
        "text": "Pennsylvania Lottery",
        "deprecated": false
    },
    {
        "id": "PEM",
        "text": "PHYSICIANS EMR LLC",
        "deprecated": false
    },
    {
        "id": "PEN",
        "text": "Commonwealth of Pennsylvania",
        "deprecated": false
    },
    {
        "id": "PEO",
        "text": "Peoples S.R.L.",
        "deprecated": false
    },
    {
        "id": "PEP",
        "text": "Peppermill Casinos, Inc.",
        "deprecated": false
    },
    {
        "id": "PEQ",
        "text": "MASHANTUCKET PEQUOT",
        "deprecated": false
    },
    {
        "id": "PER",
        "text": "PERU",
        "deprecated": false
    },
    {
        "id": "PES",
        "text": "Prisma Engineering Srl.",
        "deprecated": false
    },
    {
        "id": "PET",
        "text": "P\u0026E TECHNOLOGIES",
        "deprecated": false
    },
    {
        "id": "PFA",
        "text": "PRO FOOTBALL ASSOC.",
        "deprecated": false
    },
    {
        "id": "PFL",
        "text": "Petfre Limited",
        "deprecated": false
    },
    {
        "id": "PG1",
        "text": "PremierGaming Limited",
        "deprecated": false
    },
    {
        "id": "PG2",
        "text": "Patriot Gaming \u0026 Electronics, Inc.",
        "deprecated": false
    },
    {
        "id": "PGA",
        "text": "\u0027Perpetual Gaming, LLC dba Slot Works Gaming",
        "deprecated": false
    },
    {
        "id": "PGB",
        "text": "Pokagon Band Gaming Commission",
        "deprecated": false
    },
    {
        "id": "PGC",
        "text": "Potawatomi Gaming Commission",
        "deprecated": false
    },
    {
        "id": "PGD",
        "text": "Panter G d.o.o.",
        "deprecated": false
    },
    {
        "id": "PGE",
        "text": "PG Enterprise AG",
        "deprecated": false
    },
    {
        "id": "PGG",
        "text": "Peoples Gaming Group",
        "deprecated": false
    },
    {
        "id": "PGH",
        "text": "Psmtec GmbH",
        "deprecated": false
    },
    {
        "id": "PGI",
        "text": "PROGRESSIVE GAMES",
        "deprecated": false
    },
    {
        "id": "PGL",
        "text": "PhumelelaG\u0026L Limited",
        "deprecated": false
    },
    {
        "id": "PGM",
        "text": "Pacific Gaming",
        "deprecated": false
    },
    {
        "id": "PGO",
        "text": "PG Soluciones Informaticas S.A. dba Panorama Gaming",
        "deprecated": false
    },
    {
        "id": "PGP",
        "text": "Progressive Gaming Power Peru SAC",
        "deprecated": false
    },
    {
        "id": "PGS",
        "text": "Professional Games SRL",
        "deprecated": false
    },
    {
        "id": "PGT",
        "text": "PLAY GAMES 21 LCC",
        "deprecated": false
    },
    {
        "id": "PH3",
        "text": "Phase Three Pty Ltd",
        "deprecated": false
    },
    {
        "id": "PHA",
        "text": "Pharos Gaming Inc",
        "deprecated": false
    },
    {
        "id": "PHI",
        "text": "Phi Gaming LLC",
        "deprecated": false
    },
    {
        "id": "PHL",
        "text": "Philweb Corporation",
        "deprecated": false
    },
    {
        "id": "PHM",
        "text": "Phoenix Mecano",
        "deprecated": false
    },
    {
        "id": "PHO",
        "text": "Phoenix Entertainment LLC",
        "deprecated": false
    },
    {
        "id": "PHR",
        "text": "Penchora Limited",
        "deprecated": false
    },
    {
        "id": "PHT",
        "text": "Philsync Technologies Limited",
        "deprecated": false
    },
    {
        "id": "PHW",
        "text": "Phoenix Wings General Trading LLC",
        "deprecated": false
    },
    {
        "id": "PIA",
        "text": "PointsBet Iowa LLC",
        "deprecated": false
    },
    {
        "id": "PIE",
        "text": "Prosperous (International) Entertainment Technology Ltd",
        "deprecated": false
    },
    {
        "id": "PIG",
        "text": "Pueblo of Isleta Gaming Regulatory Agency",
        "deprecated": false
    },
    {
        "id": "PIL",
        "text": "PILOT",
        "deprecated": false
    },
    {
        "id": "PIN",
        "text": "Pinballsales.com",
        "deprecated": false
    },
    {
        "id": "PIO",
        "text": "Pilot Games",
        "deprecated": false
    },
    {
        "id": "PIR",
        "text": "PIRL Limited",
        "deprecated": false
    },
    {
        "id": "PIT",
        "text": "Pit River Gaming",
        "deprecated": false
    },
    {
        "id": "PIV",
        "text": "Penn Interactive Ventures, LLC",
        "deprecated": false
    },
    {
        "id": "PJL",
        "text": "Pat Jack 18, LLC",
        "deprecated": false
    },
    {
        "id": "PJT",
        "text": "Projeta Technologies, Inc.",
        "deprecated": false
    },
    {
        "id": "PKA",
        "text": "Poker-Academy PLC",
        "deprecated": false
    },
    {
        "id": "PKB",
        "text": "Pokerblock Limited",
        "deprecated": false
    },
    {
        "id": "PKC",
        "text": "Prairie Knights Casino \u0026 Resort",
        "deprecated": false
    },
    {
        "id": "PKM",
        "text": "POKER MAGIC",
        "deprecated": false
    },
    {
        "id": "PKT",
        "text": "Pocket Games Software",
        "deprecated": false
    },
    {
        "id": "PL1",
        "text": "Plannatech Enterprises Limited",
        "deprecated": false
    },
    {
        "id": "PLA",
        "text": "PLAYMATIC s.r.l.",
        "deprecated": false
    },
    {
        "id": "PLB",
        "text": "PLANET BINGO",
        "deprecated": false
    },
    {
        "id": "PLC",
        "text": "Polco Ltd",
        "deprecated": false
    },
    {
        "id": "PLD",
        "text": "Postcode Lotterie DT gGmbH",
        "deprecated": false
    },
    {
        "id": "PLE",
        "text": "Platinum Entertainment International Limited",
        "deprecated": false
    },
    {
        "id": "PLG",
        "text": "Planet Games s.r.l.",
        "deprecated": false
    },
    {
        "id": "PLI",
        "text": "Playline, Inc.",
        "deprecated": false
    },
    {
        "id": "PLK",
        "text": "PLAYTRAK Sistemas de Monitoreo SA de CV",
        "deprecated": false
    },
    {
        "id": "PLL",
        "text": "Postcode Lottery Ltd.",
        "deprecated": false
    },
    {
        "id": "PLM",
        "text": "PALM, 29",
        "deprecated": false
    },
    {
        "id": "PLN",
        "text": "Planet Gaming, Inc. (Formerly Lieng Vang)",
        "deprecated": false
    },
    {
        "id": "PLP",
        "text": "Plainridge Park Casino",
        "deprecated": false
    },
    {
        "id": "PLR",
        "text": "Polar Limited",
        "deprecated": false
    },
    {
        "id": "PLS",
        "text": "Playtech Services (Cyprus) Ltd.",
        "deprecated": false
    },
    {
        "id": "PLT",
        "text": "Pueblo of Laguna Tribal Gaming Regulatory Authority",
        "deprecated": false
    },
    {
        "id": "PLV",
        "text": "Player Verify LLC",
        "deprecated": false
    },
    {
        "id": "PLW",
        "text": "Playware Limited",
        "deprecated": false
    },
    {
        "id": "PLY",
        "text": "PlayInvest s.r.l.",
        "deprecated": false
    },
    {
        "id": "PLZ",
        "text": "Plaza las Delicias, Inc",
        "deprecated": false
    },
    {
        "id": "PMA",
        "text": "Prairie Meadows Racetrack and Casino",
        "deprecated": false
    },
    {
        "id": "PMC",
        "text": "PMC Turfsport CC",
        "deprecated": false
    },
    {
        "id": "PME",
        "text": "PropMe, LLC",
        "deprecated": false
    },
    {
        "id": "PMG",
        "text": "Premiere Megaplex, S.A.",
        "deprecated": false
    },
    {
        "id": "PMI",
        "text": "Pima-Maricopa Indian",
        "deprecated": false
    },
    {
        "id": "PML",
        "text": "PMC SA limited",
        "deprecated": false
    },
    {
        "id": "PMN",
        "text": "Product Madness Inc.",
        "deprecated": false
    },
    {
        "id": "PMO",
        "text": "Platte County Missouri Prosecuting Attorney’s Office",
        "deprecated": false
    },
    {
        "id": "PMP",
        "text": "Pong Marketing and Promotions Inc.",
        "deprecated": false
    },
    {
        "id": "PMS",
        "text": "Pragmatic Solutions Ltd",
        "deprecated": false
    },
    {
        "id": "PMT",
        "text": "Playmaster Gaming Corporation Limited",
        "deprecated": false
    },
    {
        "id": "PMV",
        "text": "Park MGM Las Vegas",
        "deprecated": false
    },
    {
        "id": "PNC",
        "text": "Pawnee Nation Gaming Commission",
        "deprecated": false
    },
    {
        "id": "PNG",
        "text": "Penn National Gaming, Inc",
        "deprecated": false
    },
    {
        "id": "PNJ",
        "text": "Play\u0027n Go Malta Ltd.",
        "deprecated": false
    },
    {
        "id": "PNK",
        "text": "Programutvecklarna i Norrköping AB",
        "deprecated": false
    },
    {
        "id": "PNL",
        "text": "Prima Networks Limited",
        "deprecated": false
    },
    {
        "id": "PNN",
        "text": "Peninsula Gaming, LLC",
        "deprecated": false
    },
    {
        "id": "PNO",
        "text": "Pawnee Nation of Oklahoma",
        "deprecated": false
    },
    {
        "id": "PNV",
        "text": "Picante N.V. dba Picante Gaming",
        "deprecated": false
    },
    {
        "id": "PO1",
        "text": "Poland (non-billable)",
        "deprecated": false
    },
    {
        "id": "POA",
        "text": "PCI Gaming Authority dba Wind Creek Hospitality",
        "deprecated": false
    },
    {
        "id": "POE",
        "text": "Penn Online Entertainment, LLC",
        "deprecated": false
    },
    {
        "id": "POK",
        "text": "Pokerstars",
        "deprecated": false
    },
    {
        "id": "POL",
        "text": "Polla Chilena de Beneficencia",
        "deprecated": false
    },
    {
        "id": "PON",
        "text": "Pony Technology Co., Limited",
        "deprecated": false
    },
    {
        "id": "POO",
        "text": "Poollotto.com LLC",
        "deprecated": false
    },
    {
        "id": "POR",
        "text": "Port Gamble S’Klallam Tribal Gaming Agency",
        "deprecated": false
    },
    {
        "id": "POS",
        "text": "Picapertor Online SA",
        "deprecated": false
    },
    {
        "id": "POT",
        "text": "POTTER VALLEY RANCH",
        "deprecated": false
    },
    {
        "id": "POW",
        "text": "Powerhouse Gaming, Inc.",
        "deprecated": false
    },
    {
        "id": "PPB",
        "text": "PLAYER PLACE BINGO",
        "deprecated": false
    },
    {
        "id": "PPC",
        "text": "A.W.G. Management NVI dba Paradise Plaza Casino",
        "deprecated": false
    },
    {
        "id": "PPG",
        "text": "Papaya Gaming, Inc.",
        "deprecated": false
    },
    {
        "id": "PPL",
        "text": "PragmaticPlay Ltd",
        "deprecated": false
    },
    {
        "id": "PPM",
        "text": "PPM Racing Nevada, LLC",
        "deprecated": false
    },
    {
        "id": "PPO",
        "text": "Pockaj Pohistvo doo",
        "deprecated": false
    },
    {
        "id": "PPS",
        "text": "Palpitos S.R.L.",
        "deprecated": false
    },
    {
        "id": "PPT",
        "text": "Premium Play Technologies Ltd.",
        "deprecated": false
    },
    {
        "id": "PPW",
        "text": "PP World Games LLC",
        "deprecated": false
    },
    {
        "id": "PPY",
        "text": "Pariplay Malta Limited",
        "deprecated": false
    },
    {
        "id": "PR1",
        "text": "Program-Ace",
        "deprecated": false
    },
    {
        "id": "PRA",
        "text": "Para Enterprises Co., LTD.",
        "deprecated": false
    },
    {
        "id": "PRC",
        "text": "Paradisus Hotel Resort and Casino",
        "deprecated": false
    },
    {
        "id": "PRF",
        "text": "Puerto Rico Fiscal Agency and Financial Advisory Authority",
        "deprecated": false
    },
    {
        "id": "PRG",
        "text": "Prohm Gaming",
        "deprecated": false
    },
    {
        "id": "PRI",
        "text": "Practice Alternatives, Inc.",
        "deprecated": false
    },
    {
        "id": "PRJ",
        "text": "PROJECT",
        "deprecated": false
    },
    {
        "id": "PRL",
        "text": "Parlay Games Inc.",
        "deprecated": false
    },
    {
        "id": "PRM",
        "text": "Prime Table Games, LLC",
        "deprecated": false
    },
    {
        "id": "PRN",
        "text": "Pronosticos para la Asistencia Publica",
        "deprecated": false
    },
    {
        "id": "PRO",
        "text": "PRODUCTION SRL",
        "deprecated": false
    },
    {
        "id": "PRP",
        "text": "Peerplays Blockchain Standards Association",
        "deprecated": false
    },
    {
        "id": "PRR",
        "text": "Puerto Rico Thoroughbred Racing Administration",
        "deprecated": false
    },
    {
        "id": "PRS",
        "text": "Premier Gaming Solutions, Inc",
        "deprecated": false
    },
    {
        "id": "PRT",
        "text": "Puerto Rico Tourism Company",
        "deprecated": false
    },
    {
        "id": "PRZ",
        "text": "Proizvira D.O.O.",
        "deprecated": false
    },
    {
        "id": "PSA",
        "text": "Pueblo of Santa Ana Gaming Regulatory Commission",
        "deprecated": false
    },
    {
        "id": "PSB",
        "text": "Playtech Software Bulgaria EOOD",
        "deprecated": false
    },
    {
        "id": "PSC",
        "text": "PSM Tech SRL",
        "deprecated": false
    },
    {
        "id": "PSD",
        "text": "PALMETTO STATE DIST.",
        "deprecated": false
    },
    {
        "id": "PSE",
        "text": "Proactive Gaming Scandinavia AB",
        "deprecated": false
    },
    {
        "id": "PSF",
        "text": "Playersoft Inc.",
        "deprecated": false
    },
    {
        "id": "PSG",
        "text": "Pennsylvania Gaming Control Board",
        "deprecated": false
    },
    {
        "id": "PSI",
        "text": "Psiclone Games Ltd",
        "deprecated": false
    },
    {
        "id": "PSL",
        "text": "PSL",
        "deprecated": false
    },
    {
        "id": "PSM",
        "text": "PSM s.r.l.",
        "deprecated": false
    },
    {
        "id": "PSO",
        "text": "Playtech Software Alderney Ltd.",
        "deprecated": false
    },
    {
        "id": "PSS",
        "text": "Prism Software Solutions LLC",
        "deprecated": false
    },
    {
        "id": "PST",
        "text": "Pointstreak Sports Technologies Inc.",
        "deprecated": false
    },
    {
        "id": "PSY",
        "text": "Play Tech Systems Limited dba Island Luck",
        "deprecated": false
    },
    {
        "id": "PSZ",
        "text": "Prostarz Inc.",
        "deprecated": false
    },
    {
        "id": "PT1",
        "text": "Potawatomi Gaming Commission Kansas",
        "deprecated": false
    },
    {
        "id": "PT2",
        "text": "Portugal (non-billable)",
        "deprecated": false
    },
    {
        "id": "PT3",
        "text": "PT Transpacific Company Limited",
        "deprecated": false
    },
    {
        "id": "PT4",
        "text": "PT Services (Delaware) LLC",
        "deprecated": false
    },
    {
        "id": "PTC",
        "text": "Playtech PLC",
        "deprecated": false
    },
    {
        "id": "PTD",
        "text": "Proyectos Technologicos Del Sur S.L.",
        "deprecated": false
    },
    {
        "id": "PTE",
        "text": "Playground Technology INC",
        "deprecated": false
    },
    {
        "id": "PTF",
        "text": "PT.FARM INC",
        "deprecated": false
    },
    {
        "id": "PTG",
        "text": "PRIMETABLE GAMES",
        "deprecated": false
    },
    {
        "id": "PTI",
        "text": "PokerTek, Inc.",
        "deprecated": false
    },
    {
        "id": "PTK",
        "text": "Pocket Kings Ltd",
        "deprecated": false
    },
    {
        "id": "PTM",
        "text": "Prestige IOM Limited",
        "deprecated": false
    },
    {
        "id": "PTO",
        "text": "PTP Online Corp.",
        "deprecated": false
    },
    {
        "id": "PTP",
        "text": "Par 3 Poker",
        "deprecated": false
    },
    {
        "id": "PTR",
        "text": "PIT RIVER TRIBAL",
        "deprecated": false
    },
    {
        "id": "PTT",
        "text": "POINTONE TECHNOLOGY",
        "deprecated": false
    },
    {
        "id": "PTX",
        "text": "Paytronix Systems (Pty) Ltd",
        "deprecated": false
    },
    {
        "id": "PUB",
        "text": "Publishers Clearing House",
        "deprecated": false
    },
    {
        "id": "PUL",
        "text": "PAUMA OF LUISENO",
        "deprecated": false
    },
    {
        "id": "PUN",
        "text": "Punto y Banca S.A.",
        "deprecated": false
    },
    {
        "id": "PUY",
        "text": "Puyallup Tribal Gaming Regulatory Office",
        "deprecated": false
    },
    {
        "id": "PVA",
        "text": "PVS Australia Pty Ltd",
        "deprecated": false
    },
    {
        "id": "PVG",
        "text": "Pervasive Gaming, LLC",
        "deprecated": false
    },
    {
        "id": "PVI",
        "text": "Playtech Software Limited (Isle of Man)",
        "deprecated": false
    },
    {
        "id": "PVJ",
        "text": "Plain Support S.A. (Vera \u0026 John)",
        "deprecated": false
    },
    {
        "id": "PVS",
        "text": "Professional Virtual Sports",
        "deprecated": false
    },
    {
        "id": "PWL",
        "text": "Power Leisure Bookmakers Ltd.",
        "deprecated": false
    },
    {
        "id": "PWV",
        "text": "P.W. Vegas Sp. z o.o.",
        "deprecated": false
    },
    {
        "id": "PXL",
        "text": "Pixel Digital Limited",
        "deprecated": false
    },
    {
        "id": "PY1",
        "text": "Paraguay (non-billable)",
        "deprecated": false
    },
    {
        "id": "PYB",
        "text": "Place Your Bet P/L",
        "deprecated": false
    },
    {
        "id": "PYI",
        "text": "Playgon Interactive Inc.",
        "deprecated": false
    },
    {
        "id": "PYL",
        "text": "Playson Limited",
        "deprecated": false
    },
    {
        "id": "PYS",
        "text": "Playstudios Inc.",
        "deprecated": false
    },
    {
        "id": "PYT",
        "text": "Pascua Yaqui Tribal Gaming Commission",
        "deprecated": false
    },
    {
        "id": "PYY",
        "text": "Playyoo SA",
        "deprecated": false
    },
    {
        "id": "PZC",
        "text": "PRESIDENT\u0027S CASINO",
        "deprecated": false
    },
    {
        "id": "PZD",
        "text": "Playzido Limited",
        "deprecated": false
    },
    {
        "id": "QBD",
        "text": "Queen B \u0026 D",
        "deprecated": false
    },
    {
        "id": "QBR",
        "text": "Quinault Beach Resort and Casino",
        "deprecated": false
    },
    {
        "id": "QCL",
        "text": "Quickcount Limited",
        "deprecated": false
    },
    {
        "id": "QDT",
        "text": "QuantumDoor Technologies, Inc",
        "deprecated": false
    },
    {
        "id": "QET",
        "text": "Quest Entertainment",
        "deprecated": false
    },
    {
        "id": "QIT",
        "text": "Qufan Internet Technology Limited",
        "deprecated": false
    },
    {
        "id": "QJI",
        "text": "Qian Jin Investment Limited",
        "deprecated": false
    },
    {
        "id": "QKD",
        "text": "Qikid Pty Ltd",
        "deprecated": false
    },
    {
        "id": "QLS",
        "text": "Quantum Leap Solutions Limited",
        "deprecated": false
    },
    {
        "id": "QLT",
        "text": "QLot Consulting",
        "deprecated": false
    },
    {
        "id": "QPY",
        "text": "QUICKPLAY",
        "deprecated": false
    },
    {
        "id": "QSL",
        "text": "QUBE SOFT LTD",
        "deprecated": false
    },
    {
        "id": "QSM",
        "text": "Quality Surgical Management, Inc.",
        "deprecated": false
    },
    {
        "id": "QSO",
        "text": "Qso Italia Srl",
        "deprecated": false
    },
    {
        "id": "QST",
        "text": "Qstars S.A.S.",
        "deprecated": false
    },
    {
        "id": "QTA",
        "text": "Quechan Tribal Gaming Agency",
        "deprecated": false
    },
    {
        "id": "QTG",
        "text": "QUICKTRACK GAMING",
        "deprecated": false
    },
    {
        "id": "QTL",
        "text": "Quixant LTD",
        "deprecated": false
    },
    {
        "id": "QTR",
        "text": "Quechan Tribal Gaming Office",
        "deprecated": false
    },
    {
        "id": "QUF",
        "text": "Qufun Network Technology Limited",
        "deprecated": false
    },
    {
        "id": "QUG",
        "text": "Quapaw Tribal Gaming Agency",
        "deprecated": false
    },
    {
        "id": "QUS",
        "text": "Quanta Services Ltd",
        "deprecated": false
    },
    {
        "id": "QUT",
        "text": "Quik Tech Ltd dba Quik Gaming Studios",
        "deprecated": false
    },
    {
        "id": "QWC",
        "text": "The Quantum World Corporation dba ComScire",
        "deprecated": false
    },
    {
        "id": "QXU",
        "text": "Quixant USA Inc.",
        "deprecated": false
    },
    {
        "id": "R2G",
        "text": "R2 Gaming Inc.",
        "deprecated": false
    },
    {
        "id": "RAA",
        "text": "Raadius, LLC",
        "deprecated": false
    },
    {
        "id": "RAC",
        "text": "RAINBOW CRANE CO.",
        "deprecated": false
    },
    {
        "id": "RAD",
        "text": "Radon B V",
        "deprecated": false
    },
    {
        "id": "RAG",
        "text": "Ragnarok Corporation N.V.",
        "deprecated": false
    },
    {
        "id": "RAJ",
        "text": "Riad Aljaber",
        "deprecated": false
    },
    {
        "id": "RAL",
        "text": "RAL Interactive Ltd.",
        "deprecated": false
    },
    {
        "id": "RAM",
        "text": "RAMSTAR",
        "deprecated": false
    },
    {
        "id": "RAN",
        "text": "Rank Malta Operations PLC",
        "deprecated": false
    },
    {
        "id": "RAP",
        "text": "Rabbit Entertainment Espana plc",
        "deprecated": false
    },
    {
        "id": "RAR",
        "text": "Sportradar Solutions LLC",
        "deprecated": false
    },
    {
        "id": "RAT",
        "text": "Race Tech, LLC",
        "deprecated": false
    },
    {
        "id": "RBE",
        "text": "Roboreus Ltd.",
        "deprecated": false
    },
    {
        "id": "RBL",
        "text": "Reactive Betting Limited",
        "deprecated": false
    },
    {
        "id": "RBP",
        "text": "Rob Phillips",
        "deprecated": false
    },
    {
        "id": "RBS",
        "text": "Ring Bingo Sweden AB",
        "deprecated": false
    },
    {
        "id": "RBT",
        "text": "Rafflebox Technologies Inc.",
        "deprecated": false
    },
    {
        "id": "RBV",
        "text": "REAC B.V.",
        "deprecated": false
    },
    {
        "id": "RCA",
        "text": "Red Hawk Casino (CA)",
        "deprecated": false
    },
    {
        "id": "RCC",
        "text": "The Casino at Ritz Carlton San Juan",
        "deprecated": false
    },
    {
        "id": "RCD",
        "text": "Racing Card Derby",
        "deprecated": false
    },
    {
        "id": "RCG",
        "text": "Riverside Casino Resort",
        "deprecated": false
    },
    {
        "id": "RCH",
        "text": "Reserve Casino Hotel",
        "deprecated": false
    },
    {
        "id": "RCI",
        "text": "Rubik Cube Technology India Private Limited",
        "deprecated": false
    },
    {
        "id": "RCN",
        "text": "Red Canary PLC",
        "deprecated": false
    },
    {
        "id": "RCO",
        "text": "RoyalCasino.com Ltd",
        "deprecated": false
    },
    {
        "id": "RCS",
        "text": "Rivers Casino and Resort - Schenectady",
        "deprecated": false
    },
    {
        "id": "RCT",
        "text": "RCT USA LLC",
        "deprecated": false
    },
    {
        "id": "RCY",
        "text": "Rush City Creations LLC",
        "deprecated": false
    },
    {
        "id": "RD1",
        "text": "Rank Digital Gaming (Alderney) Ltd",
        "deprecated": false
    },
    {
        "id": "RDC",
        "text": "Global Gaming RP, LLC, dba Remington Park",
        "deprecated": false
    },
    {
        "id": "RDD",
        "text": "Red Double Pte. Ltd.",
        "deprecated": false
    },
    {
        "id": "RDG",
        "text": "Real Deal Gaming LLC",
        "deprecated": false
    },
    {
        "id": "RDI",
        "text": "RFranco Digital, SAU",
        "deprecated": false
    },
    {
        "id": "RDJ",
        "text": "Red Jack",
        "deprecated": false
    },
    {
        "id": "RDL",
        "text": "REEL Denmark Limited",
        "deprecated": false
    },
    {
        "id": "RDN",
        "text": "Rednines Gaming Ltd",
        "deprecated": false
    },
    {
        "id": "RDP",
        "text": "RD Packaging",
        "deprecated": false
    },
    {
        "id": "RDR",
        "text": "REDDING RANCHERIA",
        "deprecated": false
    },
    {
        "id": "RDS",
        "text": "Redsharks Limited",
        "deprecated": false
    },
    {
        "id": "RDT",
        "text": "Resorts Digital Gaming LLC",
        "deprecated": false
    },
    {
        "id": "RDV",
        "text": "REDWOOD VALLEY POMO",
        "deprecated": false
    },
    {
        "id": "REA",
        "text": "Everi Payments, Inc (Previously Resort Advantage)",
        "deprecated": false
    },
    {
        "id": "REC",
        "text": "RESORTS EAST CHICAGO",
        "deprecated": false
    },
    {
        "id": "RED",
        "text": "REELS \u0026 DEALS",
        "deprecated": false
    },
    {
        "id": "REE",
        "text": "REEL, Italy LTD",
        "deprecated": false
    },
    {
        "id": "REG",
        "text": "Realistic Games",
        "deprecated": false
    },
    {
        "id": "REL",
        "text": "ReelGames, Inc.",
        "deprecated": false
    },
    {
        "id": "REM",
        "text": "Realm Entertainment Ltd",
        "deprecated": false
    },
    {
        "id": "REN",
        "text": "Renegade Table Games LLC",
        "deprecated": false
    },
    {
        "id": "REP",
        "text": "REPUBLIC CORP. SERVI",
        "deprecated": false
    },
    {
        "id": "RES",
        "text": "REEL Spain PLC",
        "deprecated": false
    },
    {
        "id": "RET",
        "text": "Rational Entertainment Enterprises Ltd",
        "deprecated": false
    },
    {
        "id": "REU",
        "text": "REEL Europe Limited",
        "deprecated": false
    },
    {
        "id": "RFG",
        "text": "Reflex Gaming VOF",
        "deprecated": false
    },
    {
        "id": "RFM",
        "text": "Route Monitoring (RF) (Pty) Ltd",
        "deprecated": false
    },
    {
        "id": "RFR",
        "text": "R.Franco Technologies, S.L.U.",
        "deprecated": false
    },
    {
        "id": "RFS",
        "text": "Rational FT Services Limited",
        "deprecated": false
    },
    {
        "id": "RFT",
        "text": "Rational FT Enterprises malta Limited",
        "deprecated": false
    },
    {
        "id": "RFX",
        "text": "Reflex Gaming Limited",
        "deprecated": false
    },
    {
        "id": "RGA",
        "text": "Route Gaming Solutions pty LTD",
        "deprecated": false
    },
    {
        "id": "RGB",
        "text": "RGB Games",
        "deprecated": false
    },
    {
        "id": "RGC",
        "text": "River Gaming Concept",
        "deprecated": false
    },
    {
        "id": "RGE",
        "text": "Rational Gaming Europe Limited",
        "deprecated": false
    },
    {
        "id": "RGG",
        "text": "REDGREEN2822 GAMING",
        "deprecated": false
    },
    {
        "id": "RGI",
        "text": "ROJAMI GAMING INC.",
        "deprecated": false
    },
    {
        "id": "RGL",
        "text": "Relax Gaming LTD",
        "deprecated": false
    },
    {
        "id": "RGM",
        "text": "RGB Macau Ltd.",
        "deprecated": false
    },
    {
        "id": "RGN",
        "text": "Regent News LLC",
        "deprecated": false
    },
    {
        "id": "RGO",
        "text": "River Game Operations Ltd",
        "deprecated": false
    },
    {
        "id": "RGP",
        "text": "R\u0026G Soluciones para el desarrollo s.a.c.",
        "deprecated": false
    },
    {
        "id": "RGS",
        "text": "RODEO GAMING SYSTEMS",
        "deprecated": false
    },
    {
        "id": "RHC",
        "text": "Red Hawk Casino",
        "deprecated": false
    },
    {
        "id": "RHD",
        "text": "Red Hertz Digital Consulting and Solutions Company Ltd",
        "deprecated": false
    },
    {
        "id": "RHN",
        "text": "ROHNERVILLE RANCH",
        "deprecated": false
    },
    {
        "id": "RHO",
        "text": "Resorts Casino Hotel",
        "deprecated": false
    },
    {
        "id": "RIB",
        "text": "Richard Baker",
        "deprecated": false
    },
    {
        "id": "RIC",
        "text": "Richar Inc.",
        "deprecated": false
    },
    {
        "id": "RID",
        "text": "ROYAL IMAGE LTD.",
        "deprecated": false
    },
    {
        "id": "RIG",
        "text": "Rigoberto Perez Silva",
        "deprecated": false
    },
    {
        "id": "RIL",
        "text": "RHODE ISLAND LOTTERY",
        "deprecated": false
    },
    {
        "id": "RIN",
        "text": "RINCON RIVER OATS CA",
        "deprecated": false
    },
    {
        "id": "RIO",
        "text": "Rio Entertainment Limited",
        "deprecated": false
    },
    {
        "id": "RIR",
        "text": "Rincon Band of Luiseño Indians Tribal Gaming Commission",
        "deprecated": false
    },
    {
        "id": "RIS",
        "text": "Randomness and Integrity Services Ltd (T/A RANDOM.ORG)",
        "deprecated": false
    },
    {
        "id": "RIT",
        "text": "RITE SOLUTIONS",
        "deprecated": false
    },
    {
        "id": "RIV",
        "text": "Riviera Black Hawk Casino",
        "deprecated": false
    },
    {
        "id": "RK1",
        "text": "Rockstar Games, Inc.",
        "deprecated": false
    },
    {
        "id": "RKM",
        "text": "Rocky Mountain Industries, LLC",
        "deprecated": false
    },
    {
        "id": "RLD",
        "text": "IN Bet, LLC",
        "deprecated": false
    },
    {
        "id": "RLG",
        "text": "Red Lake Gaming Commission",
        "deprecated": false
    },
    {
        "id": "RLI",
        "text": "Robert Louis Investment (Proprietary) Limited",
        "deprecated": false
    },
    {
        "id": "RLL",
        "text": "Royalty League S.A.S.",
        "deprecated": false
    },
    {
        "id": "RLS",
        "text": "Rosilsport SRL",
        "deprecated": false
    },
    {
        "id": "RLT",
        "text": "Reliato Ltd.",
        "deprecated": false
    },
    {
        "id": "RLV",
        "text": "Relevance Gaming LLC",
        "deprecated": false
    },
    {
        "id": "RLZ",
        "text": "RICHARD LEGENZA",
        "deprecated": false
    },
    {
        "id": "RMA",
        "text": "Renato Martinelli",
        "deprecated": false
    },
    {
        "id": "RME",
        "text": "Real Gaming Entretenimientos Eireli - Me",
        "deprecated": false
    },
    {
        "id": "RMG",
        "text": "ROCKY MNTN GAMING",
        "deprecated": false
    },
    {
        "id": "RMI",
        "text": "RMI Limited",
        "deprecated": false
    },
    {
        "id": "RML",
        "text": "REEL Malta Limited",
        "deprecated": false
    },
    {
        "id": "RMM",
        "text": "RAM Gaming Inc",
        "deprecated": false
    },
    {
        "id": "RMS",
        "text": "Rocky Mountain Senior Care",
        "deprecated": false
    },
    {
        "id": "RMT",
        "text": "Rocky Mountain Turf Club Inc.",
        "deprecated": false
    },
    {
        "id": "RND",
        "text": "ROUND VALLEY INDIAN",
        "deprecated": false
    },
    {
        "id": "RNG",
        "text": "RANDOM GAMING",
        "deprecated": false
    },
    {
        "id": "RO1",
        "text": "Romania (non-billable)",
        "deprecated": false
    },
    {
        "id": "ROB",
        "text": "ROYCE AND BACH",
        "deprecated": false
    },
    {
        "id": "ROC",
        "text": "Rocky Gap Casino",
        "deprecated": false
    },
    {
        "id": "ROD",
        "text": "Roar Digital LLC",
        "deprecated": false
    },
    {
        "id": "ROG",
        "text": "Rogue Gaming Studio",
        "deprecated": false
    },
    {
        "id": "ROK",
        "text": "Big Rock Casino",
        "deprecated": false
    },
    {
        "id": "ROL",
        "text": "RoBET Limited",
        "deprecated": false
    },
    {
        "id": "ROU",
        "text": "Rouleno Games",
        "deprecated": false
    },
    {
        "id": "ROY",
        "text": "Royalsoft Gaming Solutions Inc.",
        "deprecated": false
    },
    {
        "id": "RP1",
        "text": "Red Panda Ltd",
        "deprecated": false
    },
    {
        "id": "RPA",
        "text": "Royale Palace",
        "deprecated": false
    },
    {
        "id": "RPC",
        "text": "Royal Plaza Casino",
        "deprecated": false
    },
    {
        "id": "RPD",
        "text": "RPDC, LLC (Remington Park Dissemination Company, LLC)",
        "deprecated": false
    },
    {
        "id": "RPN",
        "text": "Royal Panda Limited",
        "deprecated": false
    },
    {
        "id": "RPT",
        "text": "Redpoint Technologies Sdn. Bhd.",
        "deprecated": false
    },
    {
        "id": "RQL",
        "text": "Racing Queensland Limited",
        "deprecated": false
    },
    {
        "id": "RR1",
        "text": "Red Rake Solutions, S.L.",
        "deprecated": false
    },
    {
        "id": "RRC",
        "text": "River Rock Casino",
        "deprecated": false
    },
    {
        "id": "RRD",
        "text": "ROCK N ROLL DICE",
        "deprecated": false
    },
    {
        "id": "RRE",
        "text": "Red Rhino Espana plc",
        "deprecated": false
    },
    {
        "id": "RRG",
        "text": "RED RIVER GAMING",
        "deprecated": false
    },
    {
        "id": "RRP",
        "text": "ROBINSON RANCHERIA",
        "deprecated": false
    },
    {
        "id": "RRT",
        "text": "Raging River Trading (Pty) Ltd",
        "deprecated": false
    },
    {
        "id": "RSA",
        "text": "RADsoft, AS",
        "deprecated": false
    },
    {
        "id": "RSC",
        "text": "Rosebud Sioux Gaming Commission",
        "deprecated": false
    },
    {
        "id": "RSD",
        "text": "Rising Digital Corporation",
        "deprecated": false
    },
    {
        "id": "RSE",
        "text": "RW Services Pte Ltd",
        "deprecated": false
    },
    {
        "id": "RSG",
        "text": "RESIGHINI RANCHERIA",
        "deprecated": false
    },
    {
        "id": "RSI",
        "text": "RSI International Systems Inc. dba RoomKeyPMS",
        "deprecated": false
    },
    {
        "id": "RSL",
        "text": "TSG Interactive Services Limited",
        "deprecated": false
    },
    {
        "id": "RSM",
        "text": "Rivalry Services Limited",
        "deprecated": false
    },
    {
        "id": "RST",
        "text": "REEL Estonia Limited",
        "deprecated": false
    },
    {
        "id": "RSV",
        "text": "Red Rhino Svenskar Limited",
        "deprecated": false
    },
    {
        "id": "RSW",
        "text": "Rabbit Entertainment SWE Limited",
        "deprecated": false
    },
    {
        "id": "RTA",
        "text": "Rus Tony Activities",
        "deprecated": false
    },
    {
        "id": "RTB",
        "text": "Rag Table Games Ltd.",
        "deprecated": false
    },
    {
        "id": "RTE",
        "text": "Reel Time Gaming Enterprises Pty Ltd",
        "deprecated": false
    },
    {
        "id": "RTG",
        "text": "RICOCHET TABLE GAMES",
        "deprecated": false
    },
    {
        "id": "RTI",
        "text": "Reel TV, Inc.",
        "deprecated": false
    },
    {
        "id": "RTL",
        "text": "Red Turtle Limited",
        "deprecated": false
    },
    {
        "id": "RTM",
        "text": "Red Tiger Gaming (Malta) Limited",
        "deprecated": false
    },
    {
        "id": "RTS",
        "text": "RTSB Limited trading as Match Bingo",
        "deprecated": false
    },
    {
        "id": "RTZ",
        "text": "Rootz Ltd",
        "deprecated": false
    },
    {
        "id": "RUB",
        "text": "Ruby Royal",
        "deprecated": false
    },
    {
        "id": "RUG",
        "text": "Rubicon Gaming ",
        "deprecated": false
    },
    {
        "id": "RUM",
        "text": "RUMSEY RANCHERIA",
        "deprecated": false
    },
    {
        "id": "RUS",
        "text": "Rush Street Interactive, LLC",
        "deprecated": false
    },
    {
        "id": "RUT",
        "text": "Rubytech Bilisim Trading Ltd",
        "deprecated": false
    },
    {
        "id": "RVC",
        "text": "Rivers Casino",
        "deprecated": false
    },
    {
        "id": "RWG",
        "text": "Reda World Games Srl",
        "deprecated": false
    },
    {
        "id": "RWM",
        "text": "Resorts World Manila",
        "deprecated": false
    },
    {
        "id": "RWN",
        "text": "Realwin B.V.",
        "deprecated": false
    },
    {
        "id": "RWR",
        "text": "RW GAMING",
        "deprecated": false
    },
    {
        "id": "RWS",
        "text": "Resorts World at Sentosa Pte Ltd",
        "deprecated": false
    },
    {
        "id": "RWW",
        "text": "Racing and Wagering Western Australia",
        "deprecated": false
    },
    {
        "id": "RXR",
        "text": "Roxor Gaming Limited",
        "deprecated": false
    },
    {
        "id": "RYB",
        "text": "ROYAL BELL",
        "deprecated": false
    },
    {
        "id": "RYC",
        "text": "R.L. Chinn",
        "deprecated": false
    },
    {
        "id": "RYL",
        "text": "Royal Wins Pty Ltd",
        "deprecated": false
    },
    {
        "id": "RZ1",
        "text": "Recreatieprojecten Zeeland B.V.",
        "deprecated": false
    },
    {
        "id": "S11",
        "text": "Starting 11 LLC dba Starting Eleven",
        "deprecated": false
    },
    {
        "id": "S4C",
        "text": "Skill4cash LLC",
        "deprecated": false
    },
    {
        "id": "S50",
        "text": "Split 50-50 Pty Ltd",
        "deprecated": false
    },
    {
        "id": "S88",
        "text": "SC88 TECHNOLOGY, INC.",
        "deprecated": false
    },
    {
        "id": "S8C",
        "text": "Sports888 Corporation",
        "deprecated": false
    },
    {
        "id": "SA1",
        "text": "Sampdore Adventures Limited",
        "deprecated": false
    },
    {
        "id": "SAA",
        "text": "Santa Ana Star Casino",
        "deprecated": false
    },
    {
        "id": "SAB",
        "text": "SOUTH AFRICAN BETTIN",
        "deprecated": false
    },
    {
        "id": "SAC",
        "text": "S.O. Asher Consultants Ltd.",
        "deprecated": false
    },
    {
        "id": "SAD",
        "text": "Sportium Apuestas Deportivas S.A.",
        "deprecated": false
    },
    {
        "id": "SAF",
        "text": "Sac and Fox Nation of Missouri in Kansas and Nebraska Gaming Commission",
        "deprecated": false
    },
    {
        "id": "SAG",
        "text": "SAFARI GAMING TECH. GROUP",
        "deprecated": false
    },
    {
        "id": "SAI",
        "text": "Suertia Interactiva, S.A.",
        "deprecated": false
    },
    {
        "id": "SAK",
        "text": "Smarkets Ltd",
        "deprecated": false
    },
    {
        "id": "SAL",
        "text": "Sally Corporation",
        "deprecated": false
    },
    {
        "id": "SAM",
        "text": "SAMMY USA CORP",
        "deprecated": false
    },
    {
        "id": "SAN",
        "text": "San Manuel Gaming Commission",
        "deprecated": false
    },
    {
        "id": "SAP",
        "text": "Sigma Spa",
        "deprecated": false
    },
    {
        "id": "SAR",
        "text": "Saratoga Casino Black Hawk",
        "deprecated": false
    },
    {
        "id": "SAS",
        "text": "Salento Slot",
        "deprecated": false
    },
    {
        "id": "SAT",
        "text": "William Hill Australia Wagering Pty Ltd.",
        "deprecated": false
    },
    {
        "id": "SAU",
        "text": "Skycity Adelaide Casino",
        "deprecated": false
    },
    {
        "id": "SAV",
        "text": "Samvo",
        "deprecated": false
    },
    {
        "id": "SAZ",
        "text": "Scientific Games Corporation - AZ Lottery",
        "deprecated": false
    },
    {
        "id": "SB1",
        "text": "SBTECH (GLOBAL) LIMITED (Isle of Man)",
        "deprecated": false
    },
    {
        "id": "SB2",
        "text": "Sports Booth Limited",
        "deprecated": false
    },
    {
        "id": "SBA",
        "text": "Grand Casino Baden AG",
        "deprecated": false
    },
    {
        "id": "SBB",
        "text": "SB Betting Software",
        "deprecated": false
    },
    {
        "id": "SBC",
        "text": "Sportingbet IT Services",
        "deprecated": false
    },
    {
        "id": "SBD",
        "text": "Soreo Bon Dia \u0026 Ent. N.V.",
        "deprecated": false
    },
    {
        "id": "SBE",
        "text": "SBTech",
        "deprecated": false
    },
    {
        "id": "SBG",
        "text": "Sportsbetting.com.au Pty Ltd",
        "deprecated": false
    },
    {
        "id": "SBH",
        "text": "Scientific Games Corporation - Lottery - Brenda Smith",
        "deprecated": false
    },
    {
        "id": "SBI",
        "text": "Star Bright Entertainment and Investment Co., Ltd",
        "deprecated": false
    },
    {
        "id": "SBL",
        "text": "Server Based Solutions Limited",
        "deprecated": false
    },
    {
        "id": "SBM",
        "text": "Sycuan Band Mission",
        "deprecated": false
    },
    {
        "id": "SBN",
        "text": "Selman Breitman LLP",
        "deprecated": false
    },
    {
        "id": "SBO",
        "text": "Soboba Casino",
        "deprecated": false
    },
    {
        "id": "SBP",
        "text": "Seabase Pty. Ltd.",
        "deprecated": false
    },
    {
        "id": "SBR",
        "text": "Scientific Games Corporation - Illinois Gaming Systems Center - Barry Greenberg",
        "deprecated": false
    },
    {
        "id": "SBS",
        "text": "SANTA BARBARA CORP",
        "deprecated": false
    },
    {
        "id": "SBT",
        "text": "Shoshone-Bannock Tribes",
        "deprecated": false
    },
    {
        "id": "SBV",
        "text": "Stanleybet Belgium NV",
        "deprecated": false
    },
    {
        "id": "SBY",
        "text": "Stone Bay Software",
        "deprecated": false
    },
    {
        "id": "SC1",
        "text": "Scommesseitalia Ltd",
        "deprecated": false
    },
    {
        "id": "SC3",
        "text": "Société du Casino de Crans-Montana SA",
        "deprecated": false
    },
    {
        "id": "SC4",
        "text": "South Carolina Department of Mental Health",
        "deprecated": false
    },
    {
        "id": "SC5",
        "text": "Soft Construct Limited (Isle of Man)",
        "deprecated": false
    },
    {
        "id": "SCA",
        "text": "SAN CARLOS APACHE",
        "deprecated": false
    },
    {
        "id": "SCB",
        "text": "SC Baum Online SRL",
        "deprecated": false
    },
    {
        "id": "SCC",
        "text": "Sizony Consulting CC",
        "deprecated": false
    },
    {
        "id": "SCD",
        "text": "Sonoma County District Attourney Office",
        "deprecated": false
    },
    {
        "id": "SCE",
        "text": "South Carolina Education Lottery",
        "deprecated": false
    },
    {
        "id": "SCG",
        "text": "St. Croix Gaming Enforcement ",
        "deprecated": false
    },
    {
        "id": "SCH",
        "text": "SuperGame Casino Heerlen",
        "deprecated": false
    },
    {
        "id": "SCJ",
        "text": "Superintendencia de Casinos de Juego de Chile",
        "deprecated": false
    },
    {
        "id": "SCL",
        "text": "Santa Claran Casino",
        "deprecated": false
    },
    {
        "id": "SCM",
        "text": "Saginaw Chippewa Gaming Commission",
        "deprecated": false
    },
    {
        "id": "SCN",
        "text": "Scan Coin, North Amer",
        "deprecated": false
    },
    {
        "id": "SCO",
        "text": "Score Gaming",
        "deprecated": false
    },
    {
        "id": "SCQ",
        "text": "Societe des Casinos du Quebec",
        "deprecated": false
    },
    {
        "id": "SCR",
        "text": "Singapore Casino Regulatory Authority",
        "deprecated": false
    },
    {
        "id": "SCS",
        "text": "The Star Pty Ltd",
        "deprecated": false
    },
    {
        "id": "SCT",
        "text": "SCOTTS VALLEY RANCH",
        "deprecated": false
    },
    {
        "id": "SCU",
        "text": "Seneca Cayuga Gaming Commission",
        "deprecated": false
    },
    {
        "id": "SCV",
        "text": "Solverde, Sociedade de Investimentos Turisticos da Costa Verde SA",
        "deprecated": false
    },
    {
        "id": "SCX",
        "text": "SC EveryMatrix SRL",
        "deprecated": false
    },
    {
        "id": "SCY",
        "text": "Skycity Auckland Ltd.",
        "deprecated": false
    },
    {
        "id": "SD1",
        "text": "Sportsbetting Denmark ApS",
        "deprecated": false
    },
    {
        "id": "SDA",
        "text": "Stanleybet Danmark ApS",
        "deprecated": false
    },
    {
        "id": "SDB",
        "text": "SOCIEDAD DE BENEFICENCIA DE CARAZ",
        "deprecated": false
    },
    {
        "id": "SDC",
        "text": "S.DAKOTA COMM.GAMING",
        "deprecated": false
    },
    {
        "id": "SDE",
        "text": "SipDev Solutions",
        "deprecated": false
    },
    {
        "id": "SDH",
        "text": "Sky Dancer Hotel and Casino",
        "deprecated": false
    },
    {
        "id": "SDI",
        "text": "SOFT DEV Inc.",
        "deprecated": false
    },
    {
        "id": "SDL",
        "text": "S. Dakota Lottery",
        "deprecated": false
    },
    {
        "id": "SDN",
        "text": "SidePlay Entertainment Limited",
        "deprecated": false
    },
    {
        "id": "SDO",
        "text": "Scommettendo S.r.l",
        "deprecated": false
    },
    {
        "id": "SDP",
        "text": "South Dakota Office of the Attorney General",
        "deprecated": false
    },
    {
        "id": "SDR",
        "text": "Sportradar Gaming AG ",
        "deprecated": false
    },
    {
        "id": "SDS",
        "text": "SDS",
        "deprecated": false
    },
    {
        "id": "SDT",
        "text": "STRUCTURED DATA SYST",
        "deprecated": false
    },
    {
        "id": "SDV",
        "text": "Saracen Development, LLC",
        "deprecated": false
    },
    {
        "id": "SE1",
        "text": "SafeEnt Limited",
        "deprecated": false
    },
    {
        "id": "SE2",
        "text": "Software Engineering Guild d.o.o.",
        "deprecated": false
    },
    {
        "id": "SEA",
        "text": "SEA VISION",
        "deprecated": false
    },
    {
        "id": "SEC",
        "text": "State of Michigan Assistant Attorney General",
        "deprecated": false
    },
    {
        "id": "SED",
        "text": "SED, Inc. of South Carolina d/b/a SED Gaming",
        "deprecated": false
    },
    {
        "id": "SEE",
        "text": "Seeben s.a.",
        "deprecated": false
    },
    {
        "id": "SEF",
        "text": "Season\u0027s Find 1090 CC",
        "deprecated": false
    },
    {
        "id": "SEG",
        "text": "SEGA",
        "deprecated": false
    },
    {
        "id": "SEI",
        "text": "SEIDEL",
        "deprecated": false
    },
    {
        "id": "SEM",
        "text": "FL Seminole Tribal Gaming Co",
        "deprecated": false
    },
    {
        "id": "SEN",
        "text": "Sente S.A.",
        "deprecated": false
    },
    {
        "id": "SEO",
        "text": "Selton S.A.",
        "deprecated": false
    },
    {
        "id": "SEP",
        "text": "SCE Partners, LLC dba Hard Rock Hotel \u0026 Casino Sioux City",
        "deprecated": false
    },
    {
        "id": "SER",
        "text": "Servicios Contables y Sistemas del Paraguay SA (Sercon)",
        "deprecated": false
    },
    {
        "id": "SES",
        "text": "Session Gaming, LLC",
        "deprecated": false
    },
    {
        "id": "SET",
        "text": "SET-Europe",
        "deprecated": false
    },
    {
        "id": "SEV",
        "text": "7 SYSTEMS",
        "deprecated": false
    },
    {
        "id": "SEY",
        "text": "Star Entertainment (Seychelles) Ltd.",
        "deprecated": false
    },
    {
        "id": "SF1",
        "text": "The Sports Futures Exchange Ltd",
        "deprecated": false
    },
    {
        "id": "SFB",
        "text": "Spiffbet AB",
        "deprecated": false
    },
    {
        "id": "SFC",
        "text": "Black Mesa Casino",
        "deprecated": false
    },
    {
        "id": "SFF",
        "text": "Shuffle Master GmbH \u0026 Co KG",
        "deprecated": false
    },
    {
        "id": "SFG",
        "text": "Sociedade Figueira Praia, S.A.",
        "deprecated": false
    },
    {
        "id": "SFM",
        "text": "Sac and Fox of Mississippi in Iowa Gaming Commission",
        "deprecated": false
    },
    {
        "id": "SFP",
        "text": "San Felipe Pueblo Gaming Regulatory Commission",
        "deprecated": false
    },
    {
        "id": "SFR",
        "text": "South Florida Racing",
        "deprecated": false
    },
    {
        "id": "SFS",
        "text": "Stars Fantasy Sports SubCo, LLC",
        "deprecated": false
    },
    {
        "id": "SFT",
        "text": "Soft Construct (Malta) Limited",
        "deprecated": false
    },
    {
        "id": "SFW",
        "text": "Softwaregrid Its Ltd",
        "deprecated": false
    },
    {
        "id": "SG1",
        "text": "Sharp Gaming Limited",
        "deprecated": false
    },
    {
        "id": "SG2",
        "text": "Storm Games",
        "deprecated": false
    },
    {
        "id": "SG3",
        "text": "Super Game B.V.",
        "deprecated": false
    },
    {
        "id": "SG4",
        "text": "Synot Games Limited",
        "deprecated": false
    },
    {
        "id": "SG5",
        "text": "Superlative Gaming Private Limited",
        "deprecated": false
    },
    {
        "id": "SG8",
        "text": "Silver Goose 8 International Inc.",
        "deprecated": false
    },
    {
        "id": "SGA",
        "text": "Service Games s.r.l.",
        "deprecated": false
    },
    {
        "id": "SGB",
        "text": "Scientific Games (Gibraltar) Limited",
        "deprecated": false
    },
    {
        "id": "SGD",
        "text": "SG Gaming Ltd",
        "deprecated": false
    },
    {
        "id": "SGE",
        "text": "Societe de Gestion De la Loterie Nationale",
        "deprecated": false
    },
    {
        "id": "SGH",
        "text": "SportPesa Global Holdings Limited",
        "deprecated": false
    },
    {
        "id": "SGI",
        "text": "Scientific Games Corporation - Atlantic Lottery Systems \u0026 Operations - Eric Roy",
        "deprecated": false
    },
    {
        "id": "SGL",
        "text": "Scenario Games LLC",
        "deprecated": false
    },
    {
        "id": "SGM",
        "text": "Special Games S.r.l.",
        "deprecated": false
    },
    {
        "id": "SGN",
        "text": "SGS Canada Inc.",
        "deprecated": false
    },
    {
        "id": "SGO",
        "text": "Social Games Online S.L.",
        "deprecated": false
    },
    {
        "id": "SGP",
        "text": "Shawman Global PTE LTD",
        "deprecated": false
    },
    {
        "id": "SGR",
        "text": "Suncity Group Manila Inc.",
        "deprecated": false
    },
    {
        "id": "SGS",
        "text": "SIRIO GAMES SRL",
        "deprecated": false
    },
    {
        "id": "SGT",
        "text": "SPINTEK GAMING TECH",
        "deprecated": false
    },
    {
        "id": "SGU",
        "text": "Smarc Group International Limited",
        "deprecated": false
    },
    {
        "id": "SHB",
        "text": "SportsHub Games Network, Inc.",
        "deprecated": false
    },
    {
        "id": "shc",
        "text": "Shumash Casino",
        "deprecated": false
    },
    {
        "id": "SHE",
        "text": "Shenzhen YingQian Sport Information Technology Co. Ltd",
        "deprecated": false
    },
    {
        "id": "SHF",
        "text": "Shft Mgr, LLC",
        "deprecated": false
    },
    {
        "id": "SHG",
        "text": "Shingle Springs Band of Miwok Indians",
        "deprecated": false
    },
    {
        "id": "SHH",
        "text": "Shanghai Bingo \u0026 Lotto Equipment Co., Ltd",
        "deprecated": false
    },
    {
        "id": "SHI",
        "text": "Solutions Hanzo Solutions Inc.",
        "deprecated": false
    },
    {
        "id": "SHK",
        "text": "Shark Trap Gaming \u0026 Security Systems, LLC",
        "deprecated": false
    },
    {
        "id": "SHL",
        "text": "Smernax Holdings Limited",
        "deprecated": false
    },
    {
        "id": "SHM",
        "text": "Sunplus Holding (Isle of Man)",
        "deprecated": false
    },
    {
        "id": "SHO",
        "text": "Showball Informatica LTDA",
        "deprecated": false
    },
    {
        "id": "SHP",
        "text": "Shakopee Mdewakanton Sioux Community Gaming Commission",
        "deprecated": false
    },
    {
        "id": "SHR",
        "text": "Innovative Table Games, LLC",
        "deprecated": false
    },
    {
        "id": "SHS",
        "text": "Shingle Springs Tribal Gaming Commission",
        "deprecated": false
    },
    {
        "id": "SHT",
        "text": "Shuffle Tech International LLC",
        "deprecated": false
    },
    {
        "id": "SHU",
        "text": "Bally West",
        "deprecated": false
    },
    {
        "id": "SHZ",
        "text": "SHOUTZ Inc.",
        "deprecated": false
    },
    {
        "id": "SI1",
        "text": "SIA “BASE LOGIC”",
        "deprecated": false
    },
    {
        "id": "SIA",
        "text": "Saskatchewan Indian Gaming Authority",
        "deprecated": false
    },
    {
        "id": "SIB",
        "text": "Simbo Ltd",
        "deprecated": false
    },
    {
        "id": "SIC",
        "text": "SIC Innovations LLC",
        "deprecated": false
    },
    {
        "id": "SID",
        "text": "Sid Lawrence",
        "deprecated": false
    },
    {
        "id": "SIE",
        "text": "Simetron GMBH",
        "deprecated": false
    },
    {
        "id": "SII",
        "text": "SA Gaming, Inc.",
        "deprecated": false
    },
    {
        "id": "SIM",
        "text": "DRGT Africa (Pty) Ltd",
        "deprecated": false
    },
    {
        "id": "SIN",
        "text": "Sun International Management Limited",
        "deprecated": false
    },
    {
        "id": "SIP",
        "text": "SIPEM SRL",
        "deprecated": false
    },
    {
        "id": "SIR",
        "text": "SIRMO GAMES",
        "deprecated": false
    },
    {
        "id": "SIS",
        "text": "Sisal Entertainment S.p.A.",
        "deprecated": false
    },
    {
        "id": "SIT",
        "text": "Sigmatic Asia Technologies Inc.",
        "deprecated": false
    },
    {
        "id": "SJA",
        "text": "SAS Apostas Sociais, Jogos e Apostas Online, S.A.",
        "deprecated": false
    },
    {
        "id": "SJF",
        "text": "St Jo Frontier Casino",
        "deprecated": false
    },
    {
        "id": "SJM",
        "text": "Marriott PR Management Corporation dba Marriott Resort \u0026 Stellaris Casino",
        "deprecated": false
    },
    {
        "id": "SJP",
        "text": "Spectre Gaming Inc",
        "deprecated": false
    },
    {
        "id": "SJT",
        "text": "Sherrer, Jones \u0026 Terry, P.C.",
        "deprecated": false
    },
    {
        "id": "SKA",
        "text": "Skar Advertising",
        "deprecated": false
    },
    {
        "id": "SKB",
        "text": "SKY Bet",
        "deprecated": false
    },
    {
        "id": "SKC",
        "text": "SASKATCHEWAN CASINO",
        "deprecated": false
    },
    {
        "id": "SKE",
        "text": "SKEE-BALL Inc.",
        "deprecated": false
    },
    {
        "id": "SKG",
        "text": "Saskatchewan Gaming Corp",
        "deprecated": false
    },
    {
        "id": "SKH",
        "text": "Skywind Holdings Limited",
        "deprecated": false
    },
    {
        "id": "SKI",
        "text": "Skill Software S.r.l.",
        "deprecated": false
    },
    {
        "id": "SKL",
        "text": "101292600 Saskatchewan Ltd dba Echo Lotto",
        "deprecated": false
    },
    {
        "id": "SKO",
        "text": "SkillonNet Ltd",
        "deprecated": false
    },
    {
        "id": "SKP",
        "text": "Sky Pearl Global Services Inc.",
        "deprecated": false
    },
    {
        "id": "SKR",
        "text": "Skirmony Ltd",
        "deprecated": false
    },
    {
        "id": "SKW",
        "text": "Hopland / Sho-Ka-Wah",
        "deprecated": false
    },
    {
        "id": "SKY",
        "text": "SKY GAMES",
        "deprecated": false
    },
    {
        "id": "SKZ",
        "text": "Skyllzone LLC",
        "deprecated": false
    },
    {
        "id": "SL3",
        "text": "Slovenia (non-billable)",
        "deprecated": false
    },
    {
        "id": "SLA",
        "text": "Slot Aces LLC",
        "deprecated": false
    },
    {
        "id": "SLC",
        "text": "Sielcon S.R.L.",
        "deprecated": false
    },
    {
        "id": "SLD",
        "text": "Solodek LLC",
        "deprecated": false
    },
    {
        "id": "SLE",
        "text": "Services aux Loteries en Europe, Scrl (SLE)",
        "deprecated": false
    },
    {
        "id": "SLG",
        "text": "Spectrum Gaming Group",
        "deprecated": false
    },
    {
        "id": "SLI",
        "text": "Solitairus",
        "deprecated": false
    },
    {
        "id": "SLL",
        "text": "Stillaguamish Tribal Gaming Commission",
        "deprecated": false
    },
    {
        "id": "SLM",
        "text": "Slats S.C. Limited",
        "deprecated": false
    },
    {
        "id": "SLO",
        "text": "Slot Constructor LLC",
        "deprecated": false
    },
    {
        "id": "SLS",
        "text": "SLI Global Solutions",
        "deprecated": false
    },
    {
        "id": "SLT",
        "text": "Sileuta PLC",
        "deprecated": false
    },
    {
        "id": "SLU",
        "text": "Stanleybet Lietuva UAB",
        "deprecated": false
    },
    {
        "id": "SLX",
        "text": "Slotmetrix, Inc.",
        "deprecated": false
    },
    {
        "id": "SM1",
        "text": "SolsticeMedia Ltd.",
        "deprecated": false
    },
    {
        "id": "SMA",
        "text": "SLOT MACHINES",
        "deprecated": false
    },
    {
        "id": "SMB",
        "text": "San Manuel Bingo",
        "deprecated": false
    },
    {
        "id": "SMC",
        "text": "SPIRIT MOUNTAIN CSNO",
        "deprecated": false
    },
    {
        "id": "SMD",
        "text": "Sm \u0026 Sm D.O.O.",
        "deprecated": false
    },
    {
        "id": "SME",
        "text": "SMART ENTERTAINMENT",
        "deprecated": false
    },
    {
        "id": "SMF",
        "text": "Skadden, Arps, Slate, Meagher \u0026 Flom LLP",
        "deprecated": false
    },
    {
        "id": "SMG",
        "text": "Seminole Nation Gaming Agency",
        "deprecated": false
    },
    {
        "id": "SMH",
        "text": "Slots Machines S A",
        "deprecated": false
    },
    {
        "id": "SMI",
        "text": "Sona Mobile, Inc",
        "deprecated": false
    },
    {
        "id": "SMJ",
        "text": "Scott Mihajlovic Associates Ltd.",
        "deprecated": false
    },
    {
        "id": "SMK",
        "text": "SMK",
        "deprecated": false
    },
    {
        "id": "SML",
        "text": "Sasian Macau Limited",
        "deprecated": false
    },
    {
        "id": "SMM",
        "text": "Stride Management",
        "deprecated": false
    },
    {
        "id": "SMN",
        "text": "San Manuel Band of Mission Indians",
        "deprecated": false
    },
    {
        "id": "SMO",
        "text": "Smartgames Technologies Ltd",
        "deprecated": false
    },
    {
        "id": "SMP",
        "text": "Smartplay Intl.",
        "deprecated": false
    },
    {
        "id": "SMR",
        "text": "SMARTY TECHNOLOGIES",
        "deprecated": false
    },
    {
        "id": "SMT",
        "text": "SMITH RIVER RANCH",
        "deprecated": false
    },
    {
        "id": "SMW",
        "text": "SMSC Gaming Enterprise (Shakopee Mdewakanton Sioux Community Gaming Enterprise)",
        "deprecated": false
    },
    {
        "id": "SMX",
        "text": "SC Max Bet Jocuri Electronice",
        "deprecated": false
    },
    {
        "id": "SMY",
        "text": "Shenzhen Mingyou Athletics Co., Ltd.",
        "deprecated": false
    },
    {
        "id": "SNA",
        "text": "Snaitech S.p.A.",
        "deprecated": false
    },
    {
        "id": "SNB",
        "text": "Spinsell AB",
        "deprecated": false
    },
    {
        "id": "SNC",
        "text": "Scan Coin North America, Inc",
        "deprecated": false
    },
    {
        "id": "SND",
        "text": "Sandia Tribal Gaming Commission",
        "deprecated": false
    },
    {
        "id": "SNE",
        "text": "Spin Entertainment S.A.",
        "deprecated": false
    },
    {
        "id": "SNG",
        "text": "Sierra Nevada Gaming",
        "deprecated": false
    },
    {
        "id": "SNH",
        "text": "Seneca Niagra Casino \u0026 Hotel",
        "deprecated": false
    },
    {
        "id": "SNI",
        "text": "SPORTS NET",
        "deprecated": false
    },
    {
        "id": "SNK",
        "text": "Seneca Gaming Authority",
        "deprecated": false
    },
    {
        "id": "SNL",
        "text": "Spielothek Nederland B.V.",
        "deprecated": false
    },
    {
        "id": "SNM",
        "text": "Seneca Gaming Corporation",
        "deprecated": false
    },
    {
        "id": "SNN",
        "text": "SenSen Networks Limited",
        "deprecated": false
    },
    {
        "id": "SNO",
        "text": "SG North Inc.",
        "deprecated": false
    },
    {
        "id": "SNR",
        "text": "Sonnenschein Nath Rosenthal",
        "deprecated": false
    },
    {
        "id": "SNS",
        "text": "S\u0026S Cruzalosdedos, Sociedad de Responsabilidad Limitada de Capital Variable / S\u0026S Cruzalosdedos, S de RL de CV",
        "deprecated": false
    },
    {
        "id": "SNT",
        "text": "Snoqualmie Tribal Gaming Commission",
        "deprecated": false
    },
    {
        "id": "SNV",
        "text": "Summit One Investment",
        "deprecated": false
    },
    {
        "id": "SNW",
        "text": "Sportnews SA",
        "deprecated": false
    },
    {
        "id": "SNY",
        "text": "SANTA YNEZ BAND",
        "deprecated": false
    },
    {
        "id": "SO1",
        "text": "Services and Operations for Amusement Responsible Galicis S.L. (SOAR GALICIS, SL)",
        "deprecated": false
    },
    {
        "id": "SOA",
        "text": "State of Arkansas DFA Administrative Services",
        "deprecated": false
    },
    {
        "id": "SOB",
        "text": "Soboba Tribal Gaming Commission ",
        "deprecated": false
    },
    {
        "id": "SOC",
        "text": "State of Colorado",
        "deprecated": false
    },
    {
        "id": "SOE",
        "text": "Soaring Eagle Casino Resort",
        "deprecated": false
    },
    {
        "id": "SOF",
        "text": "Softec Digital (Hong Kong) Limited",
        "deprecated": false
    },
    {
        "id": "SOG",
        "text": "SO.GE.M.A",
        "deprecated": false
    },
    {
        "id": "SOH",
        "text": "Sohoo Limited",
        "deprecated": false
    },
    {
        "id": "SOL",
        "text": "SOLUTIA GAMING",
        "deprecated": false
    },
    {
        "id": "SOM",
        "text": "Somec Srl",
        "deprecated": false
    },
    {
        "id": "SON",
        "text": "Sona Mobile Inc",
        "deprecated": false
    },
    {
        "id": "SOP",
        "text": "Services \u0026 Operations for Amusement Responsible SL",
        "deprecated": false
    },
    {
        "id": "SOR",
        "text": "Sorteo Games",
        "deprecated": false
    },
    {
        "id": "SOT",
        "text": "Softec Digital Solutions Inc.",
        "deprecated": false
    },
    {
        "id": "SOV",
        "text": "Somnia N.V.",
        "deprecated": false
    },
    {
        "id": "SPA",
        "text": "SPA Casino",
        "deprecated": false
    },
    {
        "id": "SPB",
        "text": "San Pasqual Tribal Gaming Commission",
        "deprecated": false
    },
    {
        "id": "SPC",
        "text": "Spectronix",
        "deprecated": false
    },
    {
        "id": "SPD",
        "text": "Sportsdeck Pty Ltd.",
        "deprecated": false
    },
    {
        "id": "SPE",
        "text": "SPE-IGT",
        "deprecated": false
    },
    {
        "id": "SPF",
        "text": "Springfly Ltd.",
        "deprecated": false
    },
    {
        "id": "SPG",
        "text": "SAN JUAN PUEBLO GAMI",
        "deprecated": false
    },
    {
        "id": "SPI",
        "text": "Salt River Pima-Maricopa Indian Community Regulatory Agency",
        "deprecated": false
    },
    {
        "id": "SPL",
        "text": "SMS Pariaz LTD",
        "deprecated": false
    },
    {
        "id": "SPN",
        "text": "Spin Games LLC",
        "deprecated": false
    },
    {
        "id": "SPO",
        "text": "South Point Poker LLC",
        "deprecated": false
    },
    {
        "id": "SPP",
        "text": "Slot Point Production SRL",
        "deprecated": false
    },
    {
        "id": "SPR",
        "text": "Sheraton Puerto Rico Convention Center Hotel and Casino",
        "deprecated": false
    },
    {
        "id": "SPS",
        "text": "SCA Promotions Inc",
        "deprecated": false
    },
    {
        "id": "SPT",
        "text": "SportingBet",
        "deprecated": false
    },
    {
        "id": "SPU",
        "text": "Spilnu.dk A/S",
        "deprecated": false
    },
    {
        "id": "SPV",
        "text": "Singapore Pools (Private) Limited",
        "deprecated": false
    },
    {
        "id": "SPY",
        "text": "Sirplay Ltd",
        "deprecated": false
    },
    {
        "id": "SQG",
        "text": "Squaxin Island Gaming Commission",
        "deprecated": false
    },
    {
        "id": "SQZ",
        "text": "Squarz LLC",
        "deprecated": false
    },
    {
        "id": "SRA",
        "text": "City of Santa Rosa-City Attorneys Office",
        "deprecated": false
    },
    {
        "id": "SRC",
        "text": "SMITH RIVER CASINO",
        "deprecated": false
    },
    {
        "id": "SRD",
        "text": "Soar d.o.o.",
        "deprecated": false
    },
    {
        "id": "SRE",
        "text": "Salt River Community Gaming Enterprises",
        "deprecated": false
    },
    {
        "id": "SRF",
        "text": "Surfnote, Inc.",
        "deprecated": false
    },
    {
        "id": "SRG",
        "text": "San Remo Games S.r.l.",
        "deprecated": false
    },
    {
        "id": "SRI",
        "text": "Scientific Games Corporation - Implementations and Online Lottery Product Development - Eric Deaton",
        "deprecated": false
    },
    {
        "id": "SRM",
        "text": "St.Regis Mohawk Cas.",
        "deprecated": false
    },
    {
        "id": "SRO",
        "text": "Sandia Resort \u0026 Casino",
        "deprecated": false
    },
    {
        "id": "SRP",
        "text": "SHARP IMAGE GAMING",
        "deprecated": false
    },
    {
        "id": "SRR",
        "text": "Smith River Rancheria Tribal Gaming Agency (Lucky 7 Casino)",
        "deprecated": false
    },
    {
        "id": "SRS",
        "text": "Shoshone Rose Casino",
        "deprecated": false
    },
    {
        "id": "SRT",
        "text": "Santa Rosa Rancheria Tribal Gaming Commission",
        "deprecated": false
    },
    {
        "id": "SRW",
        "text": "Sherwood Valley Tribal Gaming Commission",
        "deprecated": false
    },
    {
        "id": "SRX",
        "text": "Standing Rock Sioux Gaming Commission",
        "deprecated": false
    },
    {
        "id": "SS1",
        "text": "S\u0026S Ecommerce Limited",
        "deprecated": false
    },
    {
        "id": "SSA",
        "text": "SportPesa (Pty) Ltd",
        "deprecated": false
    },
    {
        "id": "SSB",
        "text": "Score Digital Sports Ventures Inc. dba theScore Bet",
        "deprecated": false
    },
    {
        "id": "SSC",
        "text": "SILVER STAR CASINO",
        "deprecated": false
    },
    {
        "id": "SSD",
        "text": "SCI Software Development S.A.S.",
        "deprecated": false
    },
    {
        "id": "SSE",
        "text": "SingleShot Entertainment, Inc.",
        "deprecated": false
    },
    {
        "id": "SSF",
        "text": "SmartSoft Technology Ltd.",
        "deprecated": false
    },
    {
        "id": "SSG",
        "text": "STANDING STONE GAMIN",
        "deprecated": false
    },
    {
        "id": "SSH",
        "text": "Sisoft Healthcare Information Systems",
        "deprecated": false
    },
    {
        "id": "SSI",
        "text": "Silver State Inc.",
        "deprecated": false
    },
    {
        "id": "SSL",
        "text": "Smagroup Slot Machines",
        "deprecated": false
    },
    {
        "id": "SSM",
        "text": "SAULT STE.MARIE TRIB",
        "deprecated": false
    },
    {
        "id": "SSN",
        "text": "Sunshine Games, LLC",
        "deprecated": false
    },
    {
        "id": "SSP",
        "text": "Sogei Spa",
        "deprecated": false
    },
    {
        "id": "SSR",
        "text": "Societe de la Loterie de la Suise Romande",
        "deprecated": false
    },
    {
        "id": "SSS",
        "text": "SOUTH SHORE SLOT",
        "deprecated": false
    },
    {
        "id": "SST",
        "text": "SANDE STEWART TV",
        "deprecated": false
    },
    {
        "id": "SSV",
        "text": "Susanville Tribal Rancheria Gaming Commission",
        "deprecated": false
    },
    {
        "id": "SSW",
        "text": "Simple Software Ltd",
        "deprecated": false
    },
    {
        "id": "SSY",
        "text": "Statabase Software Systems LLC",
        "deprecated": false
    },
    {
        "id": "ST1",
        "text": "Stanleybet Malta Ltd",
        "deprecated": false
    },
    {
        "id": "ST2",
        "text": "STARVEGAS S.r.l.s.",
        "deprecated": false
    },
    {
        "id": "ST3",
        "text": "Sporting Technologies East Africa Ltd",
        "deprecated": false
    },
    {
        "id": "ST4",
        "text": "SOL-TECH TECHNOLOGY COMPANY",
        "deprecated": false
    },
    {
        "id": "STA",
        "text": "STATION CASINO",
        "deprecated": false
    },
    {
        "id": "STB",
        "text": "Smart TV Broadcasting",
        "deprecated": false
    },
    {
        "id": "STC",
        "text": "Sportech",
        "deprecated": false
    },
    {
        "id": "STD",
        "text": "Singular Trading Limited",
        "deprecated": false
    },
    {
        "id": "STE",
        "text": "Nektan PLC",
        "deprecated": false
    },
    {
        "id": "STH",
        "text": "South America Gaming SAC",
        "deprecated": false
    },
    {
        "id": "STI",
        "text": "Stanleybet International Betting Ltd",
        "deprecated": false
    },
    {
        "id": "STK",
        "text": "Stadium Technology Group",
        "deprecated": false
    },
    {
        "id": "STL",
        "text": "SOUTHLAND DISTRIBUTO",
        "deprecated": false
    },
    {
        "id": "STM",
        "text": "Sportium Apostes Catalunya, S.A.",
        "deprecated": false
    },
    {
        "id": "STN",
        "text": "Station Casinos LLC",
        "deprecated": false
    },
    {
        "id": "STO",
        "text": "Star Elettronica S.r.l.",
        "deprecated": false
    },
    {
        "id": "STP",
        "text": "Stephen J. Powell Esq",
        "deprecated": false
    },
    {
        "id": "STR",
        "text": "Strikeman Elliot Law Firm",
        "deprecated": false
    },
    {
        "id": "STS",
        "text": "SpotsBall Inc",
        "deprecated": false
    },
    {
        "id": "STT",
        "text": "Optibet",
        "deprecated": false
    },
    {
        "id": "STU",
        "text": "STUART ENTERTAINMENT",
        "deprecated": false
    },
    {
        "id": "STV",
        "text": "Stravinci srl",
        "deprecated": false
    },
    {
        "id": "STW",
        "text": "Sams Town",
        "deprecated": false
    },
    {
        "id": "STX",
        "text": "Star Express International Group Ltd.",
        "deprecated": false
    },
    {
        "id": "STY",
        "text": "StayCool OÜ",
        "deprecated": false
    },
    {
        "id": "STZ",
        "text": "Siletz Tribal Gaming Commission",
        "deprecated": false
    },
    {
        "id": "SUC",
        "text": "Super Class Limited",
        "deprecated": false
    },
    {
        "id": "SUD",
        "text": "SuDoKuPDQ, LLC",
        "deprecated": false
    },
    {
        "id": "SUG",
        "text": "Southern Ute Growth Fund (SUGF) dba Southern Ute Shared Services",
        "deprecated": false
    },
    {
        "id": "SUI",
        "text": "Southern Ute Indian Tribe",
        "deprecated": false
    },
    {
        "id": "SUL",
        "text": "SuprPlay Limited",
        "deprecated": false
    },
    {
        "id": "SUN",
        "text": "SUNKIST",
        "deprecated": false
    },
    {
        "id": "SUP",
        "text": "Singular Connect, Uniperssoal LDA",
        "deprecated": false
    },
    {
        "id": "SUR",
        "text": "Suretech Spa",
        "deprecated": false
    },
    {
        "id": "SUS",
        "text": "Super Slot SRL",
        "deprecated": false
    },
    {
        "id": "SUT",
        "text": "Southern Ute Tribal Gaming Commision",
        "deprecated": false
    },
    {
        "id": "SUV",
        "text": "Supreme Ventures Lotteries Limited",
        "deprecated": false
    },
    {
        "id": "SV1",
        "text": "SV Gaming Limited T/A Betking.com",
        "deprecated": false
    },
    {
        "id": "SVB",
        "text": "Sonny Vobouxasinh",
        "deprecated": false
    },
    {
        "id": "SVD",
        "text": "Servicios Distrired S.A.S.",
        "deprecated": false
    },
    {
        "id": "SVG",
        "text": "Sportradar Virtual Gaming GmbH",
        "deprecated": false
    },
    {
        "id": "SVI",
        "text": "Southland Gaming of the Virgin Islands",
        "deprecated": false
    },
    {
        "id": "SVL",
        "text": "Scientific Games Corporation - SG Games- Sathish Anantharaman",
        "deprecated": false
    },
    {
        "id": "SVM",
        "text": "Steve McCrery",
        "deprecated": false
    },
    {
        "id": "SVN",
        "text": "Seven ABC Solution AG",
        "deprecated": false
    },
    {
        "id": "SVS",
        "text": "L2W Limited",
        "deprecated": false
    },
    {
        "id": "SVT",
        "text": "SEVENTECH",
        "deprecated": false
    },
    {
        "id": "SVV",
        "text": "SIA viensviens.lv",
        "deprecated": false
    },
    {
        "id": "SW1",
        "text": "Sweden (non-billable)",
        "deprecated": false
    },
    {
        "id": "SW2",
        "text": "Switzerland (non-billable)",
        "deprecated": false
    },
    {
        "id": "SWA",
        "text": "Synot W Athens Greek Branch",
        "deprecated": false
    },
    {
        "id": "SWB",
        "text": "Sky Warrior Bahamas Ltd",
        "deprecated": false
    },
    {
        "id": "SWC",
        "text": "SHINOK WINDS CASINO",
        "deprecated": false
    },
    {
        "id": "SWE",
        "text": "Swan Entertainment, LLC",
        "deprecated": false
    },
    {
        "id": "SWG",
        "text": "Smile World Global Limited",
        "deprecated": false
    },
    {
        "id": "SWH",
        "text": "Sweet Hope",
        "deprecated": false
    },
    {
        "id": "SWI",
        "text": "Swiss Casinos Brands AG",
        "deprecated": false
    },
    {
        "id": "SWR",
        "text": "Skywire",
        "deprecated": false
    },
    {
        "id": "SYA",
        "text": "Synchronization Anywhere For You Inc.",
        "deprecated": false
    },
    {
        "id": "SYB",
        "text": "Sylvester Burford",
        "deprecated": false
    },
    {
        "id": "SYC",
        "text": "SYCUAN TRIBAL COMM",
        "deprecated": false
    },
    {
        "id": "SYG",
        "text": "Synergy Blue LLC",
        "deprecated": false
    },
    {
        "id": "SYI",
        "text": "Synot International Limited",
        "deprecated": false
    },
    {
        "id": "SYM",
        "text": "Symon Communications, Inc.",
        "deprecated": false
    },
    {
        "id": "SYN",
        "text": "Synergy Gaming",
        "deprecated": false
    },
    {
        "id": "SYO",
        "text": "SANYO",
        "deprecated": false
    },
    {
        "id": "SYS",
        "text": "SYSTEM SOURCE",
        "deprecated": false
    },
    {
        "id": "SYT",
        "text": "Shenzhen Yihua Technology Co., Ltd",
        "deprecated": false
    },
    {
        "id": "SYU",
        "text": "Sycuan Casino",
        "deprecated": false
    },
    {
        "id": "SYW",
        "text": "SYNOT W, a.s.",
        "deprecated": false
    },
    {
        "id": "SZS",
        "text": "Szrek2Solutions",
        "deprecated": false
    },
    {
        "id": "T1I",
        "text": "TitanOne International Limited",
        "deprecated": false
    },
    {
        "id": "T40",
        "text": "Twelve40 Ltd",
        "deprecated": false
    },
    {
        "id": "T4W",
        "text": "Trade4Win, S.A.",
        "deprecated": false
    },
    {
        "id": "TAB",
        "text": "TABCORP Corporate",
        "deprecated": false
    },
    {
        "id": "TAG",
        "text": "Tonto Apache Tribal Gaming Office",
        "deprecated": false
    },
    {
        "id": "TAH",
        "text": "TAB-Austria Holding GmbH",
        "deprecated": false
    },
    {
        "id": "TAI",
        "text": "Taito Corporation",
        "deprecated": false
    },
    {
        "id": "TAJ",
        "text": "Tele Apostuak Promotora de Juegos y Apuestas S.A.",
        "deprecated": false
    },
    {
        "id": "TAL",
        "text": "TALON MANUFACTURING",
        "deprecated": false
    },
    {
        "id": "TAM",
        "text": "Talisman S.A.",
        "deprecated": false
    },
    {
        "id": "TAN",
        "text": "Tangiamo AB",
        "deprecated": false
    },
    {
        "id": "TAO",
        "text": "Taos Pueblo Gaming Commission",
        "deprecated": false
    },
    {
        "id": "TAP",
        "text": "Tap 5050 Event Consultants Ltd",
        "deprecated": false
    },
    {
        "id": "TAS",
        "text": "Tecno Azar S.A.",
        "deprecated": false
    },
    {
        "id": "TAT",
        "text": "The Three Affiliated Tribes",
        "deprecated": false
    },
    {
        "id": "TBC",
        "text": "The Bingo Company",
        "deprecated": false
    },
    {
        "id": "TBD",
        "text": "Tain Betting Promotion Ltd",
        "deprecated": false
    },
    {
        "id": "TBG",
        "text": "Turbo Gaming LLC",
        "deprecated": false
    },
    {
        "id": "TBI",
        "text": "Tuolumne Band oe Me-Wuk Indians",
        "deprecated": false
    },
    {
        "id": "TBL",
        "text": "The BiLLe Lotto Ltd",
        "deprecated": false
    },
    {
        "id": "TBM",
        "text": "Table Mountain Tribal Gaming Commission",
        "deprecated": false
    },
    {
        "id": "TBN",
        "text": "Two Black Nines, LLC",
        "deprecated": false
    },
    {
        "id": "TBO",
        "text": "Tonybet OU",
        "deprecated": false
    },
    {
        "id": "TBP",
        "text": "Tain Betting Promotion International N.V.",
        "deprecated": false
    },
    {
        "id": "TBR",
        "text": "Table Bluff Ranch",
        "deprecated": false
    },
    {
        "id": "TBS",
        "text": "T.B.M. S.r.l.",
        "deprecated": false
    },
    {
        "id": "TBT",
        "text": "Tuolumne Band of Me-Wuk Tribal Gaming Agency",
        "deprecated": false
    },
    {
        "id": "TCA",
        "text": "TECH ART",
        "deprecated": false
    },
    {
        "id": "TCH",
        "text": "Trump Casino and Hotel",
        "deprecated": false
    },
    {
        "id": "TCI",
        "text": "Tapcentive, Inc.",
        "deprecated": false
    },
    {
        "id": "TCK",
        "text": "Teckne srl",
        "deprecated": false
    },
    {
        "id": "TCL",
        "text": "Trestle Corporation Ltd.",
        "deprecated": false
    },
    {
        "id": "TCO",
        "text": "Task Consolidated Inc",
        "deprecated": false
    },
    {
        "id": "TCP",
        "text": "Tech. Consumer Prod.",
        "deprecated": false
    },
    {
        "id": "TCR",
        "text": "Treasure Chest Riverboat Casino",
        "deprecated": false
    },
    {
        "id": "TCS",
        "text": "TCS - Americas - John Huxley",
        "deprecated": false
    },
    {
        "id": "TCT",
        "text": "Transcity Pty Ltd.",
        "deprecated": false
    },
    {
        "id": "TCW",
        "text": "TCPW Holdings LLC",
        "deprecated": false
    },
    {
        "id": "TCY",
        "text": "Triple Cherry, S.L.",
        "deprecated": false
    },
    {
        "id": "TDC",
        "text": "TDC",
        "deprecated": false
    },
    {
        "id": "TDN",
        "text": "TDN MONEY SYSTEMS",
        "deprecated": false
    },
    {
        "id": "TDO",
        "text": "Tioga Downs Casino",
        "deprecated": false
    },
    {
        "id": "TDR",
        "text": "Tecnologia Del Recreativo Casta Calida",
        "deprecated": false
    },
    {
        "id": "TDY",
        "text": "TIDY INT’L CORPORATION",
        "deprecated": false
    },
    {
        "id": "TEB",
        "text": "Tebonclass SAC",
        "deprecated": false
    },
    {
        "id": "TEC",
        "text": "TECHLINK",
        "deprecated": false
    },
    {
        "id": "TED",
        "text": "Tedbets Ltd",
        "deprecated": false
    },
    {
        "id": "TEE",
        "text": "Terra Entertainment LTD",
        "deprecated": false
    },
    {
        "id": "TEI",
        "text": "Technology Exclusive Inc.",
        "deprecated": false
    },
    {
        "id": "TEL",
        "text": "Tennessee Educational Lottery Corporation",
        "deprecated": false
    },
    {
        "id": "TEN",
        "text": "T.E.N.P S.A",
        "deprecated": false
    },
    {
        "id": "TEU",
        "text": "TCS John Huxley Europe Limited",
        "deprecated": false
    },
    {
        "id": "TEV",
        "text": "Tees Valley",
        "deprecated": false
    },
    {
        "id": "TF1",
        "text": "True Flip Tech Limited",
        "deprecated": false
    },
    {
        "id": "TFE",
        "text": "Fulgent Enterprise (USA), Inc.",
        "deprecated": false
    },
    {
        "id": "TFP",
        "text": "The Football Pools Limited",
        "deprecated": false
    },
    {
        "id": "TGA",
        "text": "Typhoon Gaming PTY LTD",
        "deprecated": false
    },
    {
        "id": "TGB",
        "text": "TG LAB UAB",
        "deprecated": false
    },
    {
        "id": "TGC",
        "text": "TUNICA-BILOXI GAMING",
        "deprecated": false
    },
    {
        "id": "TGD",
        "text": "Takara Global Development Limited",
        "deprecated": false
    },
    {
        "id": "TGE",
        "text": "TPG Enterprises",
        "deprecated": false
    },
    {
        "id": "TGF",
        "text": "TELECOM GAME FACTORY",
        "deprecated": false
    },
    {
        "id": "TGG",
        "text": "TGG Interactive Limited",
        "deprecated": false
    },
    {
        "id": "TGK",
        "text": "TGK INC.",
        "deprecated": false
    },
    {
        "id": "TGL",
        "text": "Tricorp Gaming Pty Ltd.",
        "deprecated": false
    },
    {
        "id": "TGM",
        "text": "Teceng Gaming Inc.",
        "deprecated": false
    },
    {
        "id": "TGN",
        "text": "Top Gun Gaming, LLC",
        "deprecated": false
    },
    {
        "id": "TGP",
        "text": "Tattersalls Gaming Pty Ltd",
        "deprecated": false
    },
    {
        "id": "TGR",
        "text": "Tiger Resort, Leisure and Entertainment Inc. dba Okada Manila",
        "deprecated": false
    },
    {
        "id": "TGS",
        "text": "Teceng Gaming S.A.",
        "deprecated": false
    },
    {
        "id": "TGT",
        "text": "Turn Games Technologies Ltd.",
        "deprecated": false
    },
    {
        "id": "TH2",
        "text": "Tom Horn Gaming Limited",
        "deprecated": false
    },
    {
        "id": "THA",
        "text": "Thamian Tra",
        "deprecated": false
    },
    {
        "id": "THB",
        "text": "Thunderbite Limited",
        "deprecated": false
    },
    {
        "id": "THE",
        "text": "THE Holdings Ltd.",
        "deprecated": false
    },
    {
        "id": "THG",
        "text": "The Games Company.com Limited",
        "deprecated": false
    },
    {
        "id": "THI",
        "text": "Teng Hsin Information Co., Ltd.",
        "deprecated": false
    },
    {
        "id": "THK",
        "text": "Thunderkick Malta Ltd",
        "deprecated": false
    },
    {
        "id": "THL",
        "text": "The Health Lottery Limited",
        "deprecated": false
    },
    {
        "id": "THP",
        "text": "THREE HAND PINOCHLE",
        "deprecated": false
    },
    {
        "id": "THU",
        "text": "Thunderbird Casino",
        "deprecated": false
    },
    {
        "id": "TIC",
        "text": "Toshiba International Corp.",
        "deprecated": false
    },
    {
        "id": "TIE",
        "text": "TSG Interactive Gaming Europe Limited",
        "deprecated": false
    },
    {
        "id": "TIG",
        "text": "T.I.G. Investments Ltd.",
        "deprecated": false
    },
    {
        "id": "TIH",
        "text": "Travellers International Hotel Group",
        "deprecated": false
    },
    {
        "id": "TIL",
        "text": "TAIN (IOM) Ltd",
        "deprecated": false
    },
    {
        "id": "TIM",
        "text": "Tim Eaton",
        "deprecated": false
    },
    {
        "id": "TIN",
        "text": "TITANCOMPANY S.R.L.",
        "deprecated": false
    },
    {
        "id": "TIO",
        "text": "Tioli Gaming LLC",
        "deprecated": false
    },
    {
        "id": "TIP",
        "text": "Tipico Co LTD",
        "deprecated": false
    },
    {
        "id": "TIS",
        "text": "Atlantis Casino Resort Spa",
        "deprecated": false
    },
    {
        "id": "TIT",
        "text": "Titan Gaming Ltd.",
        "deprecated": false
    },
    {
        "id": "TIV",
        "text": "Tivoli A/S",
        "deprecated": false
    },
    {
        "id": "TJM",
        "text": "Timothy J. Mumphrey",
        "deprecated": false
    },
    {
        "id": "TJP",
        "text": "Toshiba Infrastructure Systems \u0026 Solutions Corporation (Japan)",
        "deprecated": false
    },
    {
        "id": "TKG",
        "text": "Thoughtkeg Application Services Corporation",
        "deprecated": false
    },
    {
        "id": "TKM",
        "text": "Ticketmaster",
        "deprecated": false
    },
    {
        "id": "TKR",
        "text": "Takara Gaming Solutions Ltd.",
        "deprecated": false
    },
    {
        "id": "TKT",
        "text": "CrowdTorch LLC",
        "deprecated": false
    },
    {
        "id": "TL1",
        "text": "Transigo Limited dba Digiwheel",
        "deprecated": false
    },
    {
        "id": "TLC",
        "text": "Tulalip Casino",
        "deprecated": false
    },
    {
        "id": "TLD",
        "text": "The Lodge Casino",
        "deprecated": false
    },
    {
        "id": "TLF",
        "text": "The Lotto Factory",
        "deprecated": false
    },
    {
        "id": "TLG",
        "text": "Touch-Less Gaming, LLC",
        "deprecated": false
    },
    {
        "id": "TLI",
        "text": "Trinitial Associates Limited",
        "deprecated": false
    },
    {
        "id": "TLL",
        "text": "Taiwan Lottery Co.",
        "deprecated": false
    },
    {
        "id": "TLO",
        "text": "Terrible\u0027s Lakeside",
        "deprecated": false
    },
    {
        "id": "TLU",
        "text": "TOTALUP, Inc.",
        "deprecated": false
    },
    {
        "id": "TMA",
        "text": "Torres Martinez Gaming Agency",
        "deprecated": false
    },
    {
        "id": "TMC",
        "text": "Turtle Mountain Casino",
        "deprecated": false
    },
    {
        "id": "TMG",
        "text": "Tailor Made Gaming LLC",
        "deprecated": false
    },
    {
        "id": "TMH",
        "text": "The Mill Casino Hotel",
        "deprecated": false
    },
    {
        "id": "TMN",
        "text": "Table Max North Amer",
        "deprecated": false
    },
    {
        "id": "TMO",
        "text": "Table Mountain Casino",
        "deprecated": false
    },
    {
        "id": "TMR",
        "text": "Table Mountain Ranch",
        "deprecated": false
    },
    {
        "id": "TMT",
        "text": "3M Touch Systems",
        "deprecated": false
    },
    {
        "id": "TNI",
        "text": "Acres 4.0",
        "deprecated": false
    },
    {
        "id": "TNJ",
        "text": "Tecnojoy S.r.l.",
        "deprecated": false
    },
    {
        "id": "TNK",
        "text": "TECHNIK",
        "deprecated": false
    },
    {
        "id": "TNP",
        "text": "Tecnoplay S.p.A.",
        "deprecated": false
    },
    {
        "id": "TNV",
        "text": "TNV Holdings, LLC",
        "deprecated": false
    },
    {
        "id": "TOB",
        "text": "Top Odds Betting",
        "deprecated": false
    },
    {
        "id": "TOD",
        "text": "Todds Enterprises LLC",
        "deprecated": false
    },
    {
        "id": "TOG",
        "text": "Tohono Oodham Gaming Enterprises",
        "deprecated": false
    },
    {
        "id": "TOK",
        "text": "Toke Gaming Corp.",
        "deprecated": false
    },
    {
        "id": "TOL",
        "text": "Taupe Overseas LTD",
        "deprecated": false
    },
    {
        "id": "TON",
        "text": "Tonalty Amusement NV",
        "deprecated": false
    },
    {
        "id": "TOO",
        "text": "Tohono O\u0027dham Tribal Gaming Office",
        "deprecated": false
    },
    {
        "id": "TOP",
        "text": "Totopartners S.r.l.",
        "deprecated": false
    },
    {
        "id": "TOR",
        "text": "Casino Tornado",
        "deprecated": false
    },
    {
        "id": "TOT",
        "text": "UBET TAS PTY LTD",
        "deprecated": false
    },
    {
        "id": "TOU",
        "text": "Tourvision S.A.",
        "deprecated": false
    },
    {
        "id": "TPA",
        "text": "Technologia Industrial Paraguaya de la Comunicacion Sociedad Anonima (TIPCSA)",
        "deprecated": false
    },
    {
        "id": "TPB",
        "text": "TopBet Roodepoort",
        "deprecated": false
    },
    {
        "id": "TPC",
        "text": "The President Casino",
        "deprecated": false
    },
    {
        "id": "TPE",
        "text": "TimePlay Entertainment",
        "deprecated": false
    },
    {
        "id": "TPG",
        "text": "Tipping Point Gaming, LLC",
        "deprecated": false
    },
    {
        "id": "TPM",
        "text": "Twin Pine Casino Middletown Rancheria Gaming Commission",
        "deprecated": false
    },
    {
        "id": "TPS",
        "text": "Tipster Ltd.",
        "deprecated": false
    },
    {
        "id": "TPX",
        "text": "Tiny Pixels B.V.",
        "deprecated": false
    },
    {
        "id": "TQG",
        "text": "The Quality Group - Lottery Technology Systems GmbH",
        "deprecated": false
    },
    {
        "id": "TRA",
        "text": "Tradologic",
        "deprecated": false
    },
    {
        "id": "TRB",
        "text": "Triple Crown d.o.o.",
        "deprecated": false
    },
    {
        "id": "TRC",
        "text": "Twin River Casino",
        "deprecated": false
    },
    {
        "id": "TRD",
        "text": "Trine d.o.o.",
        "deprecated": false
    },
    {
        "id": "TRG",
        "text": "The Rank Group",
        "deprecated": false
    },
    {
        "id": "TRH",
        "text": "Terry A. Houk",
        "deprecated": false
    },
    {
        "id": "TRI",
        "text": "TRI-STATE",
        "deprecated": false
    },
    {
        "id": "TRL",
        "text": "TicketReturn LLC",
        "deprecated": false
    },
    {
        "id": "TRN",
        "text": "Trinidad Rancheria",
        "deprecated": false
    },
    {
        "id": "TRP",
        "text": "Tripp Enterprises",
        "deprecated": false
    },
    {
        "id": "TRR",
        "text": "Triple Rainbow Inc",
        "deprecated": false
    },
    {
        "id": "TRS",
        "text": "TECH RESULTS, INC",
        "deprecated": false
    },
    {
        "id": "TRT",
        "text": "Transact Technologies",
        "deprecated": false
    },
    {
        "id": "TS1",
        "text": "Toptec Software, LLC dba MDToolbox",
        "deprecated": false
    },
    {
        "id": "TSB",
        "text": "The Second Byte Inc. dba SB Gaming Systems",
        "deprecated": false
    },
    {
        "id": "TSC",
        "text": "Turning Stone Casino",
        "deprecated": false
    },
    {
        "id": "TSE",
        "text": "TSE Data Processing Limited",
        "deprecated": false
    },
    {
        "id": "TSG",
        "text": "The Song Group Inc.",
        "deprecated": false
    },
    {
        "id": "TSH",
        "text": "Teshi Limited",
        "deprecated": false
    },
    {
        "id": "TSI",
        "text": "TEN STIX, Inc",
        "deprecated": false
    },
    {
        "id": "TSJ",
        "text": "Terrible\u0027s St. Jo Frontier",
        "deprecated": false
    },
    {
        "id": "TSK",
        "text": "TUSK RESORTS",
        "deprecated": false
    },
    {
        "id": "TSL",
        "text": "Tri-State Lotto Commission",
        "deprecated": false
    },
    {
        "id": "TSO",
        "text": "TSOGO SUN",
        "deprecated": false
    },
    {
        "id": "TSS",
        "text": "Textron Software Solutions Inc.",
        "deprecated": false
    },
    {
        "id": "TSV",
        "text": "Tsevo LLC",
        "deprecated": false
    },
    {
        "id": "TSW",
        "text": "Taiwan Skyline Worldwide Limited",
        "deprecated": false
    },
    {
        "id": "TSY",
        "text": "Tangam Systems",
        "deprecated": false
    },
    {
        "id": "TSZ",
        "text": "Totalizator Sportowy Sp. z o.o.",
        "deprecated": false
    },
    {
        "id": "TTC",
        "text": "TriMed Technologies Corporation",
        "deprecated": false
    },
    {
        "id": "TTG",
        "text": "Titan Technology Group",
        "deprecated": false
    },
    {
        "id": "TTI",
        "text": "TABLE TECH, INC",
        "deprecated": false
    },
    {
        "id": "TTL",
        "text": "Townview Trading Ltd.",
        "deprecated": false
    },
    {
        "id": "TTO",
        "text": "TotoGaming",
        "deprecated": false
    },
    {
        "id": "TTR",
        "text": "Table Trac, Inc.",
        "deprecated": false
    },
    {
        "id": "TTS",
        "text": "Tencent Technology (Shenzhen) Co. Ltd.",
        "deprecated": false
    },
    {
        "id": "TTT",
        "text": "TESTING DATABASE",
        "deprecated": false
    },
    {
        "id": "TUK",
        "text": "Tabcorp UK Limited",
        "deprecated": false
    },
    {
        "id": "TUL",
        "text": "TULE RIVER",
        "deprecated": false
    },
    {
        "id": "TUM",
        "text": "TUMINI GROUP s.r.l.",
        "deprecated": false
    },
    {
        "id": "TUR",
        "text": "Turtle Mountain Gaming Commission",
        "deprecated": false
    },
    {
        "id": "TUS",
        "text": "TSG Interactive US Services Limited",
        "deprecated": false
    },
    {
        "id": "TVC",
        "text": "Thunder Valley Casino Resort",
        "deprecated": false
    },
    {
        "id": "TVG",
        "text": "TV Global Enterprises Ltd",
        "deprecated": false
    },
    {
        "id": "TVT",
        "text": "Trevi Technology Co., Ltd.",
        "deprecated": false
    },
    {
        "id": "TVV",
        "text": "TECVIVO S.R.L. (Formerly S.C. Grup Global DMS S.R.L.)",
        "deprecated": false
    },
    {
        "id": "TVZ",
        "text": "TV Zaidimai Ltd.",
        "deprecated": false
    },
    {
        "id": "TWH",
        "text": "Trans World Hotels \u0026 Entertainment, a.s.",
        "deprecated": false
    },
    {
        "id": "TWI",
        "text": "Tiger Wheel Incorporated",
        "deprecated": false
    },
    {
        "id": "TWL",
        "text": "Twlv Technology Ltd.",
        "deprecated": false
    },
    {
        "id": "TWN",
        "text": "TWENTY-NINE PALMS",
        "deprecated": false
    },
    {
        "id": "TWR",
        "text": "Three Way Roulette Ltd.",
        "deprecated": false
    },
    {
        "id": "TXL",
        "text": "Texas Lottery",
        "deprecated": false
    },
    {
        "id": "TXR",
        "text": "Texas Racing Commission",
        "deprecated": false
    },
    {
        "id": "TXS",
        "text": "Texas Switch Inc.",
        "deprecated": false
    },
    {
        "id": "TYC",
        "text": "Tyche Gaming LLC",
        "deprecated": false
    },
    {
        "id": "TYM",
        "text": "TYME MAIDU TRIBE",
        "deprecated": false
    },
    {
        "id": "TZS",
        "text": "Tanzania Solid Limited",
        "deprecated": false
    },
    {
        "id": "U1G",
        "text": "U1 Gaming",
        "deprecated": false
    },
    {
        "id": "UAB",
        "text": "Olympic Casino Group Baltija uab",
        "deprecated": false
    },
    {
        "id": "UAG",
        "text": "United Auburn Gaming Commission",
        "deprecated": false
    },
    {
        "id": "UAN",
        "text": "Universidad Autonoma De Nuevo Leon",
        "deprecated": false
    },
    {
        "id": "UAS",
        "text": "UAB Savas Kazino",
        "deprecated": false
    },
    {
        "id": "UBB",
        "text": "UAB Baltic Bet",
        "deprecated": false
    },
    {
        "id": "UBG",
        "text": "Ubetcha Games",
        "deprecated": false
    },
    {
        "id": "UCA",
        "text": "Unisys Canada Inc.",
        "deprecated": false
    },
    {
        "id": "UCD",
        "text": "Universal Comercial y de Servicios, SA de C.V.",
        "deprecated": false
    },
    {
        "id": "UCI",
        "text": "Unlimited Concepts, Inc",
        "deprecated": false
    },
    {
        "id": "UCM",
        "text": "United Coin Machine Co.",
        "deprecated": false
    },
    {
        "id": "UCT",
        "text": "ULTRACADE TECH.",
        "deprecated": false
    },
    {
        "id": "UDJ",
        "text": "U.S.V.I. Department of Justice",
        "deprecated": false
    },
    {
        "id": "UEC",
        "text": "Universal Entertainment Corporation",
        "deprecated": false
    },
    {
        "id": "UEH",
        "text": "UE Holdings Group Inc.",
        "deprecated": false
    },
    {
        "id": "UEJ",
        "text": "Unidad Editorial Juegos, SA",
        "deprecated": false
    },
    {
        "id": "UGA",
        "text": "Ultimate Gaming",
        "deprecated": false
    },
    {
        "id": "UGE",
        "text": "U.N. Gaming Equipment",
        "deprecated": false
    },
    {
        "id": "UGI",
        "text": "Utterly Global Investment Limited",
        "deprecated": false
    },
    {
        "id": "UGL",
        "text": "UNOGLOBAL Limited",
        "deprecated": false
    },
    {
        "id": "UGM",
        "text": "Umlingo Gaming Magic",
        "deprecated": false
    },
    {
        "id": "UGR",
        "text": "Universal Gaming Resources",
        "deprecated": false
    },
    {
        "id": "UGS",
        "text": "Ultra Gaming System Bolivia SRL",
        "deprecated": false
    },
    {
        "id": "UGT",
        "text": "United Game Tech Management Ltd (LVQ-QUASCO)",
        "deprecated": false
    },
    {
        "id": "UHI",
        "text": "United States Attorney’s Office, District of Hawaii",
        "deprecated": false
    },
    {
        "id": "UHW",
        "text": "University of Florida, Herbert Wertheim College of Engineering",
        "deprecated": false
    },
    {
        "id": "UIH",
        "text": "University of Illinois Hospital",
        "deprecated": false
    },
    {
        "id": "UJP",
        "text": "UnionJackpots.com LTD",
        "deprecated": false
    },
    {
        "id": "UK1",
        "text": "United Kingdom (non-billable)",
        "deprecated": false
    },
    {
        "id": "UK2",
        "text": "IGT (UK2) Limited",
        "deprecated": false
    },
    {
        "id": "ULM",
        "text": "Ulrich Medical Concepts, Inc.",
        "deprecated": false
    },
    {
        "id": "ULT",
        "text": "Ultracell Ltd",
        "deprecated": false
    },
    {
        "id": "UMA",
        "text": "UMATILLA INDIAN RESE",
        "deprecated": false
    },
    {
        "id": "UMC",
        "text": "UTE Mountain Casino",
        "deprecated": false
    },
    {
        "id": "UMD",
        "text": "UMD Holding Limited",
        "deprecated": false
    },
    {
        "id": "UMG",
        "text": "UMG Events, LLC",
        "deprecated": false
    },
    {
        "id": "UNB",
        "text": "Unibet",
        "deprecated": false
    },
    {
        "id": "UND",
        "text": "UNIDESA",
        "deprecated": false
    },
    {
        "id": "UNG",
        "text": "United Gaming",
        "deprecated": false
    },
    {
        "id": "UNI",
        "text": "UNIVERSAL",
        "deprecated": false
    },
    {
        "id": "UNK",
        "text": "Unikrn Limited",
        "deprecated": false
    },
    {
        "id": "UNL",
        "text": "University of Nevada",
        "deprecated": false
    },
    {
        "id": "UNP",
        "text": "Universidad Nacional de la Plata",
        "deprecated": false
    },
    {
        "id": "UNT",
        "text": "United Tote Company",
        "deprecated": false
    },
    {
        "id": "UNU",
        "text": "UNICUM",
        "deprecated": false
    },
    {
        "id": "UNV",
        "text": "University of New Brunswick",
        "deprecated": false
    },
    {
        "id": "UPC",
        "text": "The U.S. Playing Card Co.",
        "deprecated": false
    },
    {
        "id": "URS",
        "text": "Ultimate Redemption System Games LLC",
        "deprecated": false
    },
    {
        "id": "USC",
        "text": "Upper Sioux Community Gaming Commission",
        "deprecated": false
    },
    {
        "id": "USD",
        "text": "U.S. Department of Homeland Security",
        "deprecated": false
    },
    {
        "id": "USG",
        "text": "US GAMES",
        "deprecated": false
    },
    {
        "id": "USV",
        "text": "The United States Virgin Islands",
        "deprecated": false
    },
    {
        "id": "UTE",
        "text": "Southern Ute Division of Gaming",
        "deprecated": false
    },
    {
        "id": "UTS",
        "text": "UTE Scientifico Games - Indra Sistemas",
        "deprecated": false
    },
    {
        "id": "UUN",
        "text": "UAB Unigames",
        "deprecated": false
    },
    {
        "id": "UVS",
        "text": "Unisyn Voting Solutions, Inc.",
        "deprecated": false
    },
    {
        "id": "VAA",
        "text": "VIVA Games srl",
        "deprecated": false
    },
    {
        "id": "VAC",
        "text": "Virginia Charitable",
        "deprecated": false
    },
    {
        "id": "VAI",
        "text": "Vegas Amusement Inc",
        "deprecated": false
    },
    {
        "id": "VAL",
        "text": "VIRGINIA LOTTERY",
        "deprecated": false
    },
    {
        "id": "VBH",
        "text": "Videobet",
        "deprecated": false
    },
    {
        "id": "VBL",
        "text": "Viva Bingo Kolonade Pty Ltd",
        "deprecated": false
    },
    {
        "id": "VBM",
        "text": "Viejas Band Mission",
        "deprecated": false
    },
    {
        "id": "VBS",
        "text": "Vantage Business Systems Inc.",
        "deprecated": false
    },
    {
        "id": "VCC",
        "text": "Victory Casino Cruises",
        "deprecated": false
    },
    {
        "id": "VCG",
        "text": "Victorian Commission for Gambling and Liquor Regulation (VCGLR)",
        "deprecated": false
    },
    {
        "id": "VCH",
        "text": "Victor Chandler International",
        "deprecated": false
    },
    {
        "id": "VCI",
        "text": "Venture Catalyst Inc.",
        "deprecated": false
    },
    {
        "id": "VCP",
        "text": "Viva d.o.o. dba Casino Paquito - igralni salon",
        "deprecated": false
    },
    {
        "id": "VDA",
        "text": "Virginia Dept. of Agriculture and Consumer Services",
        "deprecated": false
    },
    {
        "id": "VDC",
        "text": "Vending Data Corp",
        "deprecated": false
    },
    {
        "id": "VDG",
        "text": "Video Game Technologies",
        "deprecated": false
    },
    {
        "id": "VDH",
        "text": "Vic Development Holdings Ltd",
        "deprecated": false
    },
    {
        "id": "VDK",
        "text": "VIDEO KING",
        "deprecated": false
    },
    {
        "id": "VDT",
        "text": "VEND TECH",
        "deprecated": false
    },
    {
        "id": "VEC",
        "text": "VECTOR",
        "deprecated": false
    },
    {
        "id": "VEL",
        "text": "Viaden Enterprises Limited",
        "deprecated": false
    },
    {
        "id": "VEN",
        "text": "VENDO",
        "deprecated": false
    },
    {
        "id": "VER",
        "text": "Veros Digital",
        "deprecated": false
    },
    {
        "id": "VFU",
        "text": "Virtue Fusion Alderney LTD",
        "deprecated": false
    },
    {
        "id": "VGA",
        "text": "Video Gaming Technology",
        "deprecated": false
    },
    {
        "id": "VGC",
        "text": "VISTA GAMING CORP.",
        "deprecated": false
    },
    {
        "id": "VGE",
        "text": "VG Electro Games",
        "deprecated": false
    },
    {
        "id": "VGG",
        "text": "Vegas Game Point, LLC",
        "deprecated": false
    },
    {
        "id": "VGH",
        "text": "VG Technology Solutions, Inc.",
        "deprecated": false
    },
    {
        "id": "VGI",
        "text": "Visayan Gaming Inc.",
        "deprecated": false
    },
    {
        "id": "VGL",
        "text": "Virtual Generation Limited",
        "deprecated": false
    },
    {
        "id": "VGM",
        "text": "Victory Gaming Solutions Inc.",
        "deprecated": false
    },
    {
        "id": "VGO",
        "text": "Virtue Gaming Operations Ltd",
        "deprecated": false
    },
    {
        "id": "VGP",
        "text": "VItal Games Project SLot",
        "deprecated": false
    },
    {
        "id": "VGS",
        "text": "VGS",
        "deprecated": false
    },
    {
        "id": "VGT",
        "text": "Video Gaming Technologies, Inc.",
        "deprecated": false
    },
    {
        "id": "VI1",
        "text": "VisionOne Inc.",
        "deprecated": false
    },
    {
        "id": "VIA",
        "text": "Viaden Media",
        "deprecated": false
    },
    {
        "id": "VIB",
        "text": "Vilniaus Bumerangas",
        "deprecated": false
    },
    {
        "id": "VIC",
        "text": "Virgin Island Casino Control Commission",
        "deprecated": false
    },
    {
        "id": "VID",
        "text": "Videomatic S.a.s.",
        "deprecated": false
    },
    {
        "id": "VIE",
        "text": "Viejas Tribal Gaming Commission",
        "deprecated": false
    },
    {
        "id": "VIF",
        "text": "Viral Interactive Ltd dba Finnplay",
        "deprecated": false
    },
    {
        "id": "VIG",
        "text": "VISION GAMING \u0026 TECH",
        "deprecated": false
    },
    {
        "id": "VIL",
        "text": "U.S. Virgin Islands Lottery",
        "deprecated": false
    },
    {
        "id": "VIT",
        "text": "Vital Games s.r.l.",
        "deprecated": false
    },
    {
        "id": "VIV",
        "text": "Vivo Gaming LLC",
        "deprecated": false
    },
    {
        "id": "VKO",
        "text": "Veikkaus Oy",
        "deprecated": false
    },
    {
        "id": "VL1",
        "text": "Vivaro Limited",
        "deprecated": false
    },
    {
        "id": "VLC",
        "text": "VLC",
        "deprecated": false
    },
    {
        "id": "VLM",
        "text": "Vita Limited",
        "deprecated": false
    },
    {
        "id": "VLS",
        "text": "Virtualand SAS",
        "deprecated": false
    },
    {
        "id": "VLT",
        "text": "Virtualsoft Latam S.A.S.",
        "deprecated": false
    },
    {
        "id": "VMC",
        "text": "Vilnius Metrology Center",
        "deprecated": false
    },
    {
        "id": "VMG",
        "text": "Vermantia Media Group Limited",
        "deprecated": false
    },
    {
        "id": "VMI",
        "text": "Virtual Marketing Services Italia Ltd.",
        "deprecated": false
    },
    {
        "id": "VNE",
        "text": "V.N.E. Srl",
        "deprecated": false
    },
    {
        "id": "VNS",
        "text": "Vnet Services GmbH",
        "deprecated": false
    },
    {
        "id": "VNV",
        "text": "VriendenLoterij N.V.",
        "deprecated": false
    },
    {
        "id": "VOI",
        "text": "Voidbridge Limited",
        "deprecated": false
    },
    {
        "id": "VRC",
        "text": "Virginia Racing Commission",
        "deprecated": false
    },
    {
        "id": "VRO",
        "text": "Versus Online, S.A.",
        "deprecated": false
    },
    {
        "id": "VSA",
        "text": "VAN Speelautomaten, sectie amusementscentra",
        "deprecated": false
    },
    {
        "id": "VSE",
        "text": "Victory Sports Entertainment LLC",
        "deprecated": false
    },
    {
        "id": "VSL",
        "text": "VideoSlots Ltd",
        "deprecated": false
    },
    {
        "id": "VSO",
        "text": "VSolutions S.r.l.",
        "deprecated": false
    },
    {
        "id": "VSP",
        "text": "Verheul Speelautomaten B.V.",
        "deprecated": false
    },
    {
        "id": "VSR",
        "text": "Video Star Roma SRL",
        "deprecated": false
    },
    {
        "id": "VSX",
        "text": "Visix",
        "deprecated": false
    },
    {
        "id": "VTB",
        "text": "V-Tabs LLC",
        "deprecated": false
    },
    {
        "id": "VTE",
        "text": "Velta Technology, LLC",
        "deprecated": false
    },
    {
        "id": "VTG",
        "text": "Voyager Gaming Technologies",
        "deprecated": false
    },
    {
        "id": "VTK",
        "text": "V-Teck k.s.",
        "deprecated": false
    },
    {
        "id": "VTL",
        "text": "Vermont Lottery",
        "deprecated": false
    },
    {
        "id": "VTO",
        "text": "Vittoria Bet 2009 S.r.l.",
        "deprecated": false
    },
    {
        "id": "VTR",
        "text": "Videotronics for Iowa Racing and Gaming",
        "deprecated": false
    },
    {
        "id": "VUK",
        "text": "Vserv (UK) Ltd trading as VSoftCo",
        "deprecated": false
    },
    {
        "id": "VVC",
        "text": "Valley View Casino",
        "deprecated": false
    },
    {
        "id": "VWA",
        "text": "VW Group Asia, Inc.",
        "deprecated": false
    },
    {
        "id": "W50",
        "text": "Win50 ltd",
        "deprecated": false
    },
    {
        "id": "WAC",
        "text": "WAC Media Interactive N.V.",
        "deprecated": false
    },
    {
        "id": "WAG",
        "text": "WagerLogic",
        "deprecated": false
    },
    {
        "id": "WAI",
        "text": "WaSioux Inc",
        "deprecated": false
    },
    {
        "id": "WAM",
        "text": "Winning Asia Technology Macau Limited",
        "deprecated": false
    },
    {
        "id": "WAN",
        "text": "WannaBet Gaming",
        "deprecated": false
    },
    {
        "id": "WAP",
        "text": "Whoop Ass Poker, Inc",
        "deprecated": false
    },
    {
        "id": "WAR",
        "text": "WARR Gaming LLC",
        "deprecated": false
    },
    {
        "id": "WAT",
        "text": "Witchita and Affiliated Tribes",
        "deprecated": false
    },
    {
        "id": "WAZ",
        "text": "Wazdan Limited",
        "deprecated": false
    },
    {
        "id": "WBI",
        "text": "Wynnbig Inc.",
        "deprecated": false
    },
    {
        "id": "WBJ",
        "text": "War Blackjack Inc",
        "deprecated": false
    },
    {
        "id": "WCD",
        "text": "Walker County District Attorney",
        "deprecated": false
    },
    {
        "id": "WCG",
        "text": "West Coast Gaming Inc.",
        "deprecated": false
    },
    {
        "id": "WCL",
        "text": "WEST.CAN .LOT. CORP.",
        "deprecated": false
    },
    {
        "id": "WCN",
        "text": "W\u0026C N.V.",
        "deprecated": false
    },
    {
        "id": "WCR",
        "text": "Western Cape Gambling and Racing Board",
        "deprecated": false
    },
    {
        "id": "WDG",
        "text": "Wisconsin Division of Gaming",
        "deprecated": false
    },
    {
        "id": "WDL",
        "text": "Walker Digital",
        "deprecated": false
    },
    {
        "id": "WDO",
        "text": "Well Do InfoTech Co.,Ltd.",
        "deprecated": false
    },
    {
        "id": "WEB",
        "text": "Webentually S.r.l.",
        "deprecated": false
    },
    {
        "id": "WEG",
        "text": "White Earth Tribal Gaming Commission",
        "deprecated": false
    },
    {
        "id": "WEH",
        "text": "West End Software Holdings Ltd",
        "deprecated": false
    },
    {
        "id": "WEL",
        "text": "Welton",
        "deprecated": false
    },
    {
        "id": "WES",
        "text": "Weststar Technologies Inc.",
        "deprecated": false
    },
    {
        "id": "WG1",
        "text": "Winnebago Gaming Commission",
        "deprecated": false
    },
    {
        "id": "WGA",
        "text": "Wichita Gaming Commission",
        "deprecated": false
    },
    {
        "id": "WGC",
        "text": "Rocket Gaming Systems, LLC",
        "deprecated": false
    },
    {
        "id": "WGL",
        "text": "Woza Gaming (Pty) Ltd",
        "deprecated": false
    },
    {
        "id": "WGS",
        "text": "Worldwide Gaming Systems Corporation",
        "deprecated": false
    },
    {
        "id": "WH2",
        "text": "Will Hill Limited",
        "deprecated": false
    },
    {
        "id": "WHC",
        "text": "William Hill Credit Limited",
        "deprecated": false
    },
    {
        "id": "WHE",
        "text": "WHEELING DOWNS",
        "deprecated": false
    },
    {
        "id": "WHG",
        "text": "WHG (International) Limited",
        "deprecated": false
    },
    {
        "id": "WHH",
        "text": "White Hat Gaming Limited",
        "deprecated": false
    },
    {
        "id": "WHI",
        "text": "William Hill Online",
        "deprecated": false
    },
    {
        "id": "WHL",
        "text": "Wazdan Holding Limited",
        "deprecated": false
    },
    {
        "id": "WHM",
        "text": "William Hill Malta PLC",
        "deprecated": false
    },
    {
        "id": "WHO",
        "text": "WILLIAM HILL ORG",
        "deprecated": false
    },
    {
        "id": "WHR",
        "text": "Wild Horse Resort and Casino",
        "deprecated": false
    },
    {
        "id": "WHU",
        "text": "Whuper Entertainment LLC dba Eikon Gaming LLC",
        "deprecated": false
    },
    {
        "id": "WHW",
        "text": "WatchandWager.com LLC",
        "deprecated": false
    },
    {
        "id": "WI2",
        "text": "Wisconsin Forest County Potawatomi (non-billable)",
        "deprecated": false
    },
    {
        "id": "WII",
        "text": "SG Interactive",
        "deprecated": false
    },
    {
        "id": "WIL",
        "text": "Wisconsin Lottery",
        "deprecated": false
    },
    {
        "id": "WIN",
        "text": "WIN-TEK s.r.l.",
        "deprecated": false
    },
    {
        "id": "WIR",
        "text": "Wheeling Island Racetrack and Gaming Center",
        "deprecated": false
    },
    {
        "id": "WKL",
        "text": "WEIKE PTE LTD",
        "deprecated": false
    },
    {
        "id": "WKM",
        "text": "Wilkin Marketing",
        "deprecated": false
    },
    {
        "id": "WLC",
        "text": "Wyoming Lottery Corporation",
        "deprecated": false
    },
    {
        "id": "WLD",
        "text": "Wild Rose Casino",
        "deprecated": false
    },
    {
        "id": "WLG",
        "text": "Willowdale Group Limited",
        "deprecated": false
    },
    {
        "id": "WLI",
        "text": "Windland Limitada",
        "deprecated": false
    },
    {
        "id": "WLR",
        "text": "Wachtell, Lipton, Rosen \u0026 Katz",
        "deprecated": false
    },
    {
        "id": "WMA",
        "text": "World Match Ltd",
        "deprecated": false
    },
    {
        "id": "WML",
        "text": "WDG Macau Limited",
        "deprecated": false
    },
    {
        "id": "WMS",
        "text": "WMS",
        "deprecated": false
    },
    {
        "id": "WMX",
        "text": "Winamax",
        "deprecated": false
    },
    {
        "id": "WNC",
        "text": "Wyandotte Nation Casino",
        "deprecated": false
    },
    {
        "id": "WNI",
        "text": "Wanin International Co., Ltd.",
        "deprecated": false
    },
    {
        "id": "WNN",
        "text": "Winnersnet Co Ltd",
        "deprecated": false
    },
    {
        "id": "WNR",
        "text": "WINERGAME LIMITED",
        "deprecated": false
    },
    {
        "id": "WOR",
        "text": "Worte S.L.",
        "deprecated": false
    },
    {
        "id": "WPC",
        "text": "Windyplan Co. Ltd.",
        "deprecated": false
    },
    {
        "id": "WPG",
        "text": "World Premium Gaming S.A.",
        "deprecated": false
    },
    {
        "id": "WPM",
        "text": "Wyoming Pari-Mutual Commission",
        "deprecated": false
    },
    {
        "id": "WPT",
        "text": "Well Power Tech. Corp.",
        "deprecated": false
    },
    {
        "id": "WR1",
        "text": "WR Digital GmbH",
        "deprecated": false
    },
    {
        "id": "WRC",
        "text": "Wind River Casino",
        "deprecated": false
    },
    {
        "id": "WRD",
        "text": "Will Rogers Downs Casino",
        "deprecated": false
    },
    {
        "id": "WRE",
        "text": "Wild Rose Emmet",
        "deprecated": false
    },
    {
        "id": "WRM",
        "text": "Wyndham Grand Rio Mar Beach Resort \u0026 Spa",
        "deprecated": false
    },
    {
        "id": "WSA",
        "text": "Win Sistemas S.A.",
        "deprecated": false
    },
    {
        "id": "WSC",
        "text": "Wellsoft Corporation",
        "deprecated": false
    },
    {
        "id": "WSG",
        "text": "State of Wisconsin, Office of Indian Gaming \u0026 Regulatory Compliance",
        "deprecated": false
    },
    {
        "id": "WSH",
        "text": "Wishland Software Technology Inc.",
        "deprecated": false
    },
    {
        "id": "WSI",
        "text": "Win Systems International Holdings",
        "deprecated": false
    },
    {
        "id": "WSL",
        "text": "WASH. STATE LOTTERY",
        "deprecated": false
    },
    {
        "id": "WSP",
        "text": "WHG Spain PLC",
        "deprecated": false
    },
    {
        "id": "WSR",
        "text": "OR WARM SPRINGS RESV",
        "deprecated": false
    },
    {
        "id": "WSS",
        "text": "Winga Spain S.A.",
        "deprecated": false
    },
    {
        "id": "WST",
        "text": "Worldsmart Technology",
        "deprecated": false
    },
    {
        "id": "WTI",
        "text": "WORLD TOUCH INC.",
        "deprecated": false
    },
    {
        "id": "WTK",
        "text": "Win Technologies S de RL de CV",
        "deprecated": false
    },
    {
        "id": "WTL",
        "text": "Win Technologies Limited",
        "deprecated": false
    },
    {
        "id": "WTM",
        "text": "Wanfang Technology Management Inc.",
        "deprecated": false
    },
    {
        "id": "WTW",
        "text": "Watch World Luxury Ltd",
        "deprecated": false
    },
    {
        "id": "WUS",
        "text": "Win Technologies USA, LLC",
        "deprecated": false
    },
    {
        "id": "WVA",
        "text": "West Virginia Attorney General\u0027s Office",
        "deprecated": false
    },
    {
        "id": "WVG",
        "text": "WinView, Inc. dba WinView Games",
        "deprecated": false
    },
    {
        "id": "WVL",
        "text": "West Virginia Lot",
        "deprecated": false
    },
    {
        "id": "WW1",
        "text": "Worldwide",
        "deprecated": false
    },
    {
        "id": "WWC",
        "text": "Wildwood Casino",
        "deprecated": false
    },
    {
        "id": "WWL",
        "text": "Wonderful World (Group) Limited",
        "deprecated": false
    },
    {
        "id": "WWO",
        "text": "WagerWorks",
        "deprecated": false
    },
    {
        "id": "WWW",
        "text": "Wilshire Worldwide Company Limited",
        "deprecated": false
    },
    {
        "id": "WYD",
        "text": "Wyoming Downs",
        "deprecated": false
    },
    {
        "id": "WYG",
        "text": "Wymac Gaming Solutions",
        "deprecated": false
    },
    {
        "id": "WYH",
        "text": "Wyoming Horse Racing LLC",
        "deprecated": false
    },
    {
        "id": "WYN",
        "text": "Wynn Resorts",
        "deprecated": false
    },
    {
        "id": "X18",
        "text": "X18BET",
        "deprecated": false
    },
    {
        "id": "XAN",
        "text": "Xanadu Consultancy Ltd",
        "deprecated": false
    },
    {
        "id": "XDG",
        "text": "Xunta de Galicia, Subdirección General de Juego y Espectáculos Públicos",
        "deprecated": false
    },
    {
        "id": "XEL",
        "text": "Xela Services Ltd",
        "deprecated": false
    },
    {
        "id": "XGM",
        "text": "X-Gaming",
        "deprecated": false
    },
    {
        "id": "XPE",
        "text": "XPERTX",
        "deprecated": false
    },
    {
        "id": "XPL",
        "text": "XPLUS NETWORK N.V.",
        "deprecated": false
    },
    {
        "id": "XPR",
        "text": "Xpress-Systems LLC",
        "deprecated": false
    },
    {
        "id": "XSG",
        "text": "Xiang Shang Games Co., Ltd.",
        "deprecated": false
    },
    {
        "id": "XSI",
        "text": "Xiang Shang International Co Ltd",
        "deprecated": false
    },
    {
        "id": "XSS",
        "text": "Xtreme Software Solutions d.o.o.",
        "deprecated": false
    },
    {
        "id": "XST",
        "text": "XS Technology, Inc.",
        "deprecated": false
    },
    {
        "id": "XSW",
        "text": "Xatronic Software AG",
        "deprecated": false
    },
    {
        "id": "XTC",
        "text": "Xionwei Technology Co. Ltd. Incorporated",
        "deprecated": false
    },
    {
        "id": "XTD",
        "text": "Xin Tian Di Entertainment Limited",
        "deprecated": false
    },
    {
        "id": "XTE",
        "text": "Xterra Games Ltd dba Leander Games ",
        "deprecated": false
    },
    {
        "id": "XTT",
        "text": "Xtek Srl",
        "deprecated": false
    },
    {
        "id": "XUD",
        "text": "Xcelrate UDI, Inc.",
        "deprecated": false
    },
    {
        "id": "XUE",
        "text": "Xuenn Private Limited",
        "deprecated": false
    },
    {
        "id": "XYV",
        "text": "XYVerify Corporation",
        "deprecated": false
    },
    {
        "id": "YAH",
        "text": "Yahoo EMEA Ltd",
        "deprecated": false
    },
    {
        "id": "YAI",
        "text": "Yahoo Inc.",
        "deprecated": false
    },
    {
        "id": "YAV",
        "text": "Yavapai Prescott Tribal Gaming Office",
        "deprecated": false
    },
    {
        "id": "YBD",
        "text": "Yaniv Bason Development Ltd dba iDobet ",
        "deprecated": false
    },
    {
        "id": "YDW",
        "text": "Yocha Dehe Wintun Nation Tribal Gaming Agency",
        "deprecated": false
    },
    {
        "id": "YGG",
        "text": "Yggdrasil Gaming Limited",
        "deprecated": false
    },
    {
        "id": "YGL",
        "text": "Young G. Lee",
        "deprecated": false
    },
    {
        "id": "YOB",
        "text": "Yobetit.com Ltd",
        "deprecated": false
    },
    {
        "id": "YPT",
        "text": "Yama Play Technology Co Ltd",
        "deprecated": false
    },
    {
        "id": "YSE",
        "text": "Yellow Stone Entertainment N.V.",
        "deprecated": false
    },
    {
        "id": "YSL",
        "text": "YSLETA DEL SUR PUEBL",
        "deprecated": false
    },
    {
        "id": "YST",
        "text": "You Star Technology Co., Ltd.",
        "deprecated": false
    },
    {
        "id": "YUG",
        "text": "Yumicra Games s.r.o.",
        "deprecated": false
    },
    {
        "id": "YUN",
        "text": "Yung Choi",
        "deprecated": false
    },
    {
        "id": "ZAF",
        "text": "Zaffe Investments Limited",
        "deprecated": false
    },
    {
        "id": "ZAO",
        "text": "SET-Production ZAO",
        "deprecated": false
    },
    {
        "id": "ZCI",
        "text": "ZipChart, Inc.",
        "deprecated": false
    },
    {
        "id": "ZDS",
        "text": "ZDS",
        "deprecated": false
    },
    {
        "id": "ZEN",
        "text": "Zen Entertainment",
        "deprecated": false
    },
    {
        "id": "ZES",
        "text": "Zest Gaming",
        "deprecated": false
    },
    {
        "id": "ZFX",
        "text": "ZFX Gaming LLC",
        "deprecated": false
    },
    {
        "id": "ZGL",
        "text": "Zhonghwa Group Limited, Inc.",
        "deprecated": false
    },
    {
        "id": "ZHG",
        "text": "ZHAREV Games Limited",
        "deprecated": false
    },
    {
        "id": "ZIN",
        "text": "Zeal International Ltd",
        "deprecated": false
    },
    {
        "id": "ZIP",
        "text": "Zitro IP S.A.R.L.",
        "deprecated": false
    },
    {
        "id": "ZIT",
        "text": "Zitro",
        "deprecated": false
    },
    {
        "id": "ZNT",
        "text": "ZapNET Limited",
        "deprecated": false
    },
    {
        "id": "ZPR",
        "text": "Zjednoczone Przedsiębiorstwa Rozrywkowe S.A.",
        "deprecated": false
    },
    {
        "id": "ZSA",
        "text": "Zitro S.a.r.l.",
        "deprecated": false
    },
    {
        "id": "ZSV",
        "text": "Zeus Services PTLD",
        "deprecated": false
    },
    {
        "id": "ZUU",
        "text": "ZUUM d.o.o",
        "deprecated": false
    },
    {
        "id": "ZYN",
        "text": "Zynga Inc",
        "deprecated": false
    }
]

export function paginateData(term, count, pageNum) {
    const pageNumber = Number(pageNum);
    const pageSize = Number(count);
    const matches = testDataSet.filter(d => d.id !== null 
        && (d.id.toLowerCase().startsWith(term.toLowerCase())
            || d.text.toLowerCase().startsWith(term.toLowerCase())
        ));
    const offset = (pageNumber - 1) * pageSize;
    const set = matches.slice(offset, offset + pageSize);
    const result = {
        "Total": matches.length,
        "Results": set
    };
    return result;
}