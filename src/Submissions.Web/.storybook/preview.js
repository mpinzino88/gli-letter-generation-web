import { configure } from '@storybook/vue';
import api from './api/index';
import lodash from 'lodash';

import Vue from 'vue';

import upperFirst from 'lodash/upperFirst';
import camelCase from 'lodash/camelCase';

// prototypes
Object.defineProperty(Vue.prototype, '$http', { value: api });
Object.defineProperty(Vue.prototype, '$lodash', { value: lodash });

// STORYBOOK SPECIFIC
function Em(str) {
	return (!str || str.length === 0);
}
// For the Base Lookup, we need to recreate at least one of the Select2 formatters.
// This is transferred from gli.js
window["formatManufacturers"] = data => {
	if (Em(data.id)) {
		return data;
	}

	return data.text + "<span class='dd-ch'>" + data.id + "</span>";
};

// Include all base components by default
// https://vuejs.org/v2/guide/components-registration.html#Automatic-Global-Registration-of-Base-Components

const requireComponent = require.context(
	// The relative path of the components folder
	'../Vue/components/base',
	// Whether or not to look in subfolders
	true,
	// The regular expression used to match base component filenames
	/Base[A-Z]\w+\.(vue|js)$/
);

requireComponent.keys().forEach(fileName => {
	// Get component config
	const componentConfig = requireComponent(fileName)

	// Get PascalCase name of component
	const componentName = upperFirst(
		camelCase(
			// Gets the file name regardless of folder depth
			fileName
				.split('/')
				.pop()
				.replace(/\.\w+$/, '')
		)
	)

	// Register component globally
	Vue.component(
		componentName,
		// Look for the component options on `.default`, which will
		// exist if the component was exported with `export default`,
		// otherwise fall back to module's root.
		componentConfig.default || componentConfig
	)
});

// configure(require.context('../Vue/components/stories/', true, /\.stories\.js$/), module);
export const parameters = {
  actions: { argTypesRegex: "^on[A-Z].*" },
}