﻿/// <binding ProjectOpened='Watch - Development' />
const path = require('path');
const VueLoaderPlugin = require('vue-loader/lib/plugin');

function resolve() {
	return path.join(__dirname + '/Vue/');
}

module.exports = env => {
	const mode = process.env.NODE_ENV === 'production' ? 'production' : 'development';

	return {
		entry: ['@babel/polyfill', './Vue/main.js'],
		mode: mode,
		output: {
			path: resolve(),
			filename: 'bundle.js',
			chunkFilename: 'vendors.js'
		},
		resolve: {
			extensions: ['.js', '.vue', '.json'],
			alias: {
				'vue$': 'vue/dist/vue.esm.js',
				'@': resolve()
			}
		},
		module: {
			rules: [
				{
					test: /\.vue$/,
					loader: 'vue-loader'
				},
				{
					test: /\.js$/,
					loader: 'babel-loader',
					exclude: /node_modules/
				},
				{
					test: /\.css$/,
					use: [
						'vue-style-loader',
						'css-loader'
					]
				},
				{
					test: /\.s[ac]ss$/,
					use: [
						'vue-style-loader',
						'css-loader',
						// Compiles Sass to CSS
						'sass-loader',
					],
				},
			]
		},
		optimization: {
			splitChunks: {
				chunks: 'all'
			}
		},
		plugins: [
			new VueLoaderPlugin()
		]
	};
};