﻿using Submissions.Common;
using System.Collections.Generic;

namespace Submissions.Web
{
	public static class GLISession
	{
		public static Dictionary<Permission, AccessRight> AccessRights
		{
			get { return ComUtil.GetSession<Dictionary<Permission, AccessRight>>("GLISession_AccessRights"); }
		}

		public static int DepartmentId
		{
			get { return ComUtil.GetSession<int>("GLISession_DepartmentId"); }
		}

		public static Department Department
		{
			get { return ComUtil.GetSession<Department>("GLISession_Department"); }
		}

		public static int LocationId
		{
			get { return ComUtil.GetSession<int>("GLISession_LocationId"); }
		}

		public static string Location
		{
			get { return ComUtil.GetSession<string>("GLISession_Location"); }
		}

		public static string LocationName
		{
			get { return ComUtil.GetSession<string>("GLISession_LocationName"); }
		}

		public static int? ManagerId
		{
			get { return ComUtil.GetSession<int?>("GLISession_ManagerId"); }
		}

		public static int RoleId
		{
			get { return ComUtil.GetSession<int>("GLISession_RoleId"); }
		}

		public static Role Role
		{
			get { return ComUtil.GetSession<Role>("GLISession_Role"); }
		}

		public static IUser User
		{
			get { return ComUtil.GetSession<User>("GLISession_User"); }
		}
	}
}