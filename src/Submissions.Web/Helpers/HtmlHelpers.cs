using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using System.Web.Routing;

namespace Submissions.Web
{
	public static class HtmlHelpers
	{
		public static MvcHtmlString CheckBoxCopyFor<TModel, TProperty>
		(
			this HtmlHelper<TModel> helper,
			Expression<Func<TModel, TProperty>> expressionName,
			object htmlAttributes
		)
		{
			var name = helper.NameFor(expressionName) + "Copy";

			var attributes = new RouteValueDictionary();
			AddHtmlAttributes(htmlAttributes, attributes);
			attributes.Add("id", helper.IdFor(expressionName) + "Copy");
			attributes.Add("title", "Check to copy " + helper.DisplayNameFor(expressionName) + " to other records of this file");
			var checkBox = helper.CheckBox(name, attributes).ToHtmlString();

			return MvcHtmlString.Create(checkBox.Substring(0, checkBox.IndexOf("<input", 1)));
		}

		public static MvcHtmlString CheckBoxCopyFor<TModel, TProperty>
		(
			this HtmlHelper<TModel> helper,
			Expression<Func<TModel, TProperty>> expressionName
		)
		{
			return helper.CheckBoxCopyFor(expressionName, null);
		}

		public static MvcHtmlString LookupFor<TModel, TProperty1, TProperty2>
		(
			this HtmlHelper<TModel> helper,
			Expression<Func<TModel, TProperty1>> expressionId,
			Expression<Func<TModel, TProperty2>> expressionText,
			LookupAjax lookupAjax,
			bool validate,
			object htmlAttributes
		)
		{
			var initialId = expressionId.Compile()(helper.ViewData.Model);

			var initialSelectList = new List<SelectListItem>();
			initialSelectList.Add(new SelectListItem());
			if (initialId != null)
			{
				var initialText = expressionText.Compile()(helper.ViewData.Model);
				initialSelectList.Add(new SelectListItem { Value = initialId.ToString(), Text = initialText.ToString(), Selected = true });
			}

			var attributes = new RouteValueDictionary();
			AddHtmlAttributes(htmlAttributes, attributes);
			attributes.Add("class", "form-control");
			attributes.Add("data-lookupajax", lookupAjax);
			attributes.Add("data-lookuptextfield", helper.IdFor(expressionText));

			if (lookupAjax == LookupAjax.Jurisdictions || lookupAjax == LookupAjax.Manufacturers)
			{
				attributes.Add("data-templatename", "format" + lookupAjax.ToString());
			}

			var hiddenField = helper.HiddenFor(expressionText);
			var lookupField = helper.DropDownListFor(expressionId, initialSelectList, attributes);

			var validateField = new MvcHtmlString("");
			if (validate)
			{
				validateField = helper.ValidationMessageFor(expressionId);
			}

			var sb = new StringBuilder();
			sb.Append(hiddenField.ToHtmlString());
			sb.Append(lookupField.ToHtmlString());
			sb.Append(validateField.ToHtmlString());

			return MvcHtmlString.Create(sb.ToString());
		}

		public static MvcHtmlString LookupFor<TModel, TProperty1, TProperty2>
		(
			this HtmlHelper<TModel> helper,
			Expression<Func<TModel, TProperty1>> expressionId,
			Expression<Func<TModel, TProperty2>> expressionText,
			LookupAjax lookupAjax,
			bool validate
		)
		{
			return helper.LookupFor(expressionId, expressionText, lookupAjax, validate, null);
		}

		public static MvcHtmlString LookupMultipleFor<TModel, TProperty1>
		(
			this HtmlHelper<TModel> helper,
			Expression<Func<TModel, TProperty1>> expressionId,
			IList<SelectListItem> initial,
			LookupAjax lookupAjax,
			bool validate,
			object htmlAttributes
		)
		{
			var attributes = new RouteValueDictionary();
			AddHtmlAttributes(htmlAttributes, attributes);
			attributes.Add("class", "form-control hidden");
			attributes.Add("data-lookupmultipleajax", lookupAjax);

			if (lookupAjax == LookupAjax.Jurisdictions || lookupAjax == LookupAjax.Manufacturers)
			{
				attributes.Add("data-templatename", "format" + lookupAjax.ToString());
			}
			if (initial == null)
			{
				initial = new List<SelectListItem>();
			}
			var lookupField = helper.ListBoxFor(expressionId, initial, attributes);

			var validateField = new MvcHtmlString("");
			if (validate)
			{
				validateField = helper.ValidationMessageFor(expressionId);
			}

			var sb = new StringBuilder();
			sb.Append(lookupField.ToHtmlString());
			sb.Append(validateField.ToHtmlString());

			return MvcHtmlString.Create(sb.ToString());
		}

		public static MvcHtmlString LookupMultipleFor<TModel, TProperty1>
		(
			this HtmlHelper<TModel> helper,
			Expression<Func<TModel, TProperty1>> expressionId,
			IList<SelectListItem> initial,
			LookupAjax lookupAjax,
			bool validate
		)
		{
			return helper.LookupMultipleFor(expressionId, initial, lookupAjax, validate, null);
		}

		public static MvcHtmlString TagsFor<TModel, TProperty1>
		(
			this HtmlHelper<TModel> helper,
			Expression<Func<TModel, TProperty1>> expressionId,
			bool validate,
			object htmlAttributes
		)
		{
			var tags = expressionId.Compile()(helper.ViewData.Model) as string[];

			var initialSelectList = new List<SelectListItem>();
			initialSelectList.Add(new SelectListItem());
			if (tags != null)
			{
				var tagList = tags.OrderBy(x => x).Select(x => new SelectListItem { Value = x, Text = x }).ToList();
				initialSelectList.AddRange(tagList);
			}

			var attributes = new RouteValueDictionary();
			AddHtmlAttributes(htmlAttributes, attributes);
			attributes.Add("class", "form-control");
			attributes.Add("data-tags", true);

			var tagsField = helper.ListBoxFor(expressionId, initialSelectList, attributes);

			var validateField = new MvcHtmlString("");
			if (validate)
			{
				validateField = helper.ValidationMessageFor(expressionId);
			}

			var sb = new StringBuilder();
			sb.Append(tagsField.ToHtmlString());
			sb.Append(validateField.ToHtmlString());

			return MvcHtmlString.Create(sb.ToString());
		}

		public static MvcHtmlString TagsFor<TModel, TProperty1>
		(
			this HtmlHelper<TModel> helper,
			Expression<Func<TModel, TProperty1>> expressionId,
			bool validate
		)
		{
			return helper.TagsFor(expressionId, validate, null);
		}

		public static MvcHtmlString EnumToString<T>(this HtmlHelper helper)
		{
			var values = Enum.GetValues(typeof(T)).Cast<int>();
			var enumDictionary = values.ToDictionary(value => Enum.GetName(typeof(T), value));

			return new MvcHtmlString(JsonConvert.SerializeObject(enumDictionary));
		}

		private static void AddHtmlAttributes(object htmlAttributes, RouteValueDictionary attributes)
		{
			if (htmlAttributes != null)
			{
				if (htmlAttributes is Dictionary<string, object>)
				{
					foreach (var item in (Dictionary<string, object>)htmlAttributes)
					{
						attributes.Add(item.Key, item.Value);
					}
				}
				else
				{
					foreach (PropertyDescriptor property in TypeDescriptor.GetProperties(htmlAttributes))
					{
						attributes.Add(property.Name.Replace('_', '-'), property.GetValue(htmlAttributes));
					}
				}
			}
		}
	}
}