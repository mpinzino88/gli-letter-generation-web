﻿using System.Web.Configuration;

namespace Submissions.Web.Helpers
{
	using Submissions.Common;

	public class ConfigContext : IConfigContext
	{
		public ConfigContext(IConfig config)
		{
			Config = config;
		}

		public string MonitorAccountPassword { get { return WebConfigurationManager.AppSettings["MonitorAccountPassword"] ?? ""; } }
		public string MonitorAccountUsername { get { return WebConfigurationManager.AppSettings["MonitorAccountUsername"] ?? ""; } }
		public string PCSAttachmentPath { get { return WebConfigurationManager.AppSettings["PCSAttachmentPath"] ?? ""; } }
		public string PCSUploadPath { get { return WebConfigurationManager.AppSettings["PCSUploadPath"] ?? ""; } }
		public string PCTAttachmentPath { get { return WebConfigurationManager.AppSettings["PCTAttachmentPath"] ?? ""; } }
		public string PCTUploadPath { get { return WebConfigurationManager.AppSettings["PCTUploadPath"] ?? ""; } }
		public string SubmissionsBaseUrl { get { return WebConfigurationManager.AppSettings["submissions.baseurl"] ?? ""; } }
		public string VerifySerivceAuthKey { get { return WebConfigurationManager.AppSettings["VerifySerivceAuthKey"] ?? ""; } }
		public IConfig Config { get; set; }
	}
}