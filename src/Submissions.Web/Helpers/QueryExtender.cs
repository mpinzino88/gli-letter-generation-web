﻿using EF.Submission;
using Submissions.Common;
using Submissions.Common.JQGrid;
using Submissions.Common.Linq.Dynamic;
using Submissions.Web.Models.Query;
using System;
using System.Collections.Generic;
using System.Data.SqlTypes;
using System.Linq;
using System.Web.Mvc;

namespace Submissions.Web
{
	public static class QueryExtender
	{
		/// <summary>
		/// Stolen from http://tech.pro/blog/2226/extension-method-distinct-objects
		/// </summary>
		/// <typeparam name="T">Any object.</typeparam>
		/// <typeparam name="TKey">A value to be used as the primary key on the object T</typeparam>
		/// <param name="source">An IEnumerable of objects T</param>
		/// <param name="keySelector">A function that returns the TKey value on an object T</param>
		/// <returns>Only the unique elements in the list.</returns>
		public static IEnumerable<T> DistinctBy<T, TKey>(this IQueryable<T> source, Func<T, TKey> keySelector)
		{
			HashSet<TKey> seenKeys = new HashSet<TKey>();
			foreach (T element in source)
				if (seenKeys.Add(keySelector(element)))
					yield return element;
		}
		public static IQueryable<T> GenericSort<T>(this IQueryable<T> query, ISort filters)
		{
			if (!string.IsNullOrWhiteSpace(filters.Sort1))
			{
				if (string.IsNullOrWhiteSpace(filters.Sort2))
				{
					query = query.OrderBy(filters.Sort1 + " " + filters.Sort1Order);
				}
				else
				{
					query = query.OrderBy(filters.Sort1 + " " + filters.Sort1Order + ", " + filters.Sort2 + " " + filters.Sort2Order);
				}
			}
			else
			{
				query = query.OrderBy("Id");
			}

			return query;
		}

		public static IQueryable<Submission> SubmissionSearch(this IQueryable<Submission> query, SubmissionSearch filters)
		{
			var _dbSubmission = DependencyResolver.Current.GetService<ISubmissionContext>();
			var endOfDayToday = ComUtil.EndOfDayToday();

			var startSubmitDate = (filters.StartSubmitDate == null) ? (DateTime)SqlDateTime.MinValue : Convert.ToDateTime(filters.StartSubmitDate).Date;
			startSubmitDate = (filters.RollingSubmitDateDays == null) ? startSubmitDate : DateTime.Today.AddDays((int)filters.RollingSubmitDateDays * -1);
			var endSubmitDate = (filters.EndSubmitDate == null) ? (DateTime)SqlDateTime.MaxValue : Convert.ToDateTime(filters.EndSubmitDate).Date.AddDays(1).AddTicks(-1);
			endSubmitDate = (filters.RollingSubmitDateDays == null) ? endSubmitDate : endOfDayToday;
			var startReceiveDate = (filters.StartReceiveDate == null) ? (DateTime)SqlDateTime.MinValue : Convert.ToDateTime(filters.StartReceiveDate).Date;
			startReceiveDate = (filters.RollingReceiveDateDays == null) ? startReceiveDate : DateTime.Today.AddDays((int)filters.RollingReceiveDateDays * -1);
			var endReceiveDate = (filters.EndReceiveDate == null) ? (DateTime)SqlDateTime.MaxValue : Convert.ToDateTime(filters.EndReceiveDate).Date.AddDays(1).AddTicks(-1);
			endReceiveDate = (filters.RollingReceiveDateDays == null) ? endReceiveDate : endOfDayToday;

			query = query.Where(x => x.SubmitDate >= startSubmitDate && x.SubmitDate <= endSubmitDate);
			query = query.Where(x => x.ReceiveDate >= startReceiveDate && x.ReceiveDate <= endReceiveDate);

			if (filters.SingleInputSearch != null)
			{
				try
				{
					if (filters.SingleInputSearch.Count(x => x == '-') < 4)
					{
						throw new Exception("Not a valid File Number so we can't search with Manufacturer Code");
					}

					var validFileNumber = new FileNumber(filters.SingleInputSearch);
					var fileNumber = validFileNumber.ToString();
					var manufacturerCode = validFileNumber.ManufacturerCode;

					query = query.Where(x => x.ManufacturerCode == manufacturerCode);
					query = query.Where(x => x.FileNumber.StartsWith(fileNumber));
				}
				catch
				{
					query = query.Where(x =>
						x.FileNumber.StartsWith(filters.SingleInputSearch) ||
						x.GameName.Contains(filters.SingleInputSearch) ||
						x.IdNumber.Contains(filters.SingleInputSearch));
				}

				return query;
			}

			if (filters.ArchiveLocationId != null)
			{
				var archiveLocation = _dbSubmission.trLaboratories.Where(x => x.Id == filters.ArchiveLocationId).Select(x => x.Location).Single();
				query = query.Where(x => x.ArchiveLocation == archiveLocation);
			}

			if (filters.CertificationLabId != null)
			{
				var certificationLab = _dbSubmission.trLaboratories.Where(x => x.Id == filters.CertificationLabId).Select(x => x.Location).Single();
				query = query.Where(x => x.CertificationLab == certificationLab);
			}

			if (!string.IsNullOrEmpty(filters.ChipType))
			{
				query = query.Where(x => x.ChipType == filters.ChipType);
			}

			if (filters.CompanyId != null)
			{
				query = query.Where(x => x.CompanyId == filters.CompanyId);
			}

			if (filters.ConditionalRevoke != null)
			{
				query = query.Where(x => x.ConditionalRevoke == filters.ConditionalRevoke);
			}

			if (!string.IsNullOrEmpty(filters.ContractNumber))
			{
				query = query.Where(x => x.ContractNumber.StartsWith(filters.ContractNumber));
			}

			if (filters.ContractTypeId != null)
			{
				query = query.Where(x => x.ContractTypeId == filters.ContractTypeId);
			}

			if (filters.CurrencyId != null)
			{
				query = query.Where(x => x.CurrencyId == filters.CurrencyId);
			}

			if (!string.IsNullOrEmpty(filters.DateCode))
			{
				query = query.Where(x => x.DateCode.StartsWith(filters.DateCode));
			}

			if (filters.ExternalLabId != null)
			{
				query = query.Where(x => x.ExternalLabId == filters.ExternalLabId);
			}

			if (!string.IsNullOrEmpty(filters.FileNumber))
			{
				query = query.Where(x => x.FileNumber.StartsWith(filters.FileNumber));
			}

			if (!string.IsNullOrEmpty(filters.Function))
			{
				query = query.Where(x => x.Function.StartsWith(filters.Function));
			}

			if (!string.IsNullOrEmpty(filters.GameName))
			{
				query = query.Where(x => x.GameName.StartsWith(filters.GameName));
			}

			if (!string.IsNullOrEmpty(filters.GameType))
			{
				query = query.Where(x => x.GameType.StartsWith(filters.GameType));
			}

			if (!string.IsNullOrEmpty(filters.IdNumber))
			{
				query = query.Where(x => x.IdNumber.StartsWith(filters.IdNumber));
			}

			if (!string.IsNullOrEmpty(filters.JurisdictionId))
			{
				query = query.Where(x => x.JurisdictionId == filters.JurisdictionId);
			}

			if (filters.LabId != null)
			{
				var lab = _dbSubmission.trLaboratories.Where(x => x.Id == filters.LabId).Select(x => x.Location).Single();
				query = query.Where(x => x.Lab == lab);
			}

			if (!string.IsNullOrEmpty(filters.LetterNumber))
			{
				query = query.Where(x => x.LetterNumber.StartsWith(filters.LetterNumber));
			}

			if (!string.IsNullOrEmpty(filters.ManufacturerCode))
			{
				query = query.Where(x => x.ManufacturerCode == filters.ManufacturerCode);
			}

			if (!string.IsNullOrEmpty(filters.Operator))
			{
				query = query.Where(x => x.Operator.StartsWith(filters.Operator));
			}

			if (!string.IsNullOrEmpty(filters.PartNumber))
			{
				query = query.Where(x => x.PartNumber.StartsWith(filters.PartNumber));
			}

			if (!string.IsNullOrEmpty(filters.Position))
			{
				query = query.Where(x => x.Position.StartsWith(filters.Position));
			}

			if (!string.IsNullOrEmpty(filters.ProjectDetail))
			{
				query = query.Where(x => x.ProjectDetail.Contains(filters.ProjectDetail));
			}

			if (!string.IsNullOrEmpty(filters.PurchaseOrder))
			{
				query = query.Where(x => x.PurchaseOrder.StartsWith(filters.PurchaseOrder));
			}

			if (filters.RegressionTesting != null)
			{
				query = query.Where(x => x.IsRegressionTesting == filters.RegressionTesting);
			}

			if (!string.IsNullOrEmpty(filters.ReplaceWith))
			{
				query = query.Where(x => x.ReplaceWith.StartsWith(filters.ReplaceWith));
			}

			if (filters.Sequence != null)
			{
				var sequence = filters.Sequence.ToJurisdictionIdString();
				query = query.Where(x => x.Sequence == sequence);
			}

			if (!string.IsNullOrEmpty(filters.Status))
			{
				query = query.Where(x => x.Status == filters.Status);
			}

			if (!string.IsNullOrEmpty(filters.System))
			{
				query = query.Where(x => x.System.StartsWith(filters.System));
			}

			if (filters.TestingPerformedId != null)
			{
				query = query.Where(x => x.TestingPerformedId == filters.TestingPerformedId);
			}

			if (filters.TestingTypeId != null)
			{
				query = query.Where(x => x.TestingTypeId == filters.TestingTypeId);
			}

			if (filters.TypeId != null)
			{
				var type = _dbSubmission.tbl_lst_SubmissionType.Where(x => x.Id == filters.TypeId).Select(x => x.Code).Single();
				query = query.Where(x => x.Type == type);
			}

			if (!string.IsNullOrEmpty(filters.Vendor))
			{
				query = query.Where(x => x.Vendor.StartsWith(filters.Vendor));
			}

			if (!string.IsNullOrEmpty(filters.Version))
			{
				query = query.Where(x => x.Version.StartsWith(filters.Version));
			}

			if (filters.WorkPerformedId != null)
			{
				query = query.Where(x => x.WorkPerformedId == filters.WorkPerformedId);
			}

			if (!string.IsNullOrEmpty(filters.Year))
			{
				var year = filters.Year.Trim().Right(2);
				query = query.Where(x => x.Year == year);
			}

			return query;
		}
	}
}