﻿using Chart.Mvc.ComplexChart;
using Chart.Mvc.SimpleChart;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace Submissions.Web
{
	public static class LookupsStandardSubmissions
	{
		/// <summary>
		/// Display Simple and Complex Chart Types in One List
		/// </summary>
		public static IEnumerable<SelectListItem> ChartTypes
		{
			get
			{
				var chartTypes = new List<SelectListItem>();

				var simpleChartTypes = Enum.GetValues(typeof(SimpleChartType)).Cast<SimpleChartType>()
					.Select(x => new SelectListItem
					{
						Text = x.ToString(),
						Value = x.ToString()
					});

				var complextChartTypes = Enum.GetValues(typeof(ComplexChartType)).Cast<ComplexChartType>()
					.Where(x => x.ToString() != "Line" & x.ToString() != "Radar") // exclude for now
					.Select(x => new SelectListItem
					{
						Text = x.ToString(),
						Value = x.ToString()
					});

				chartTypes.AddRange(simpleChartTypes);
				chartTypes.AddRange(complextChartTypes);
				chartTypes.Sort((x, y) => x.Text.CompareTo(y.Text));

				return chartTypes;
			}
		}
	}
}