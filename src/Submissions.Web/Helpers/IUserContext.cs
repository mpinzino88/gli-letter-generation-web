using Submissions.Common;
using System.Collections.Generic;

namespace Submissions.Web
{
	public interface IUserContext
	{
		Dictionary<Permission, AccessRight> AccessRights { get; set; }
		int? DefaultDashboardId { get; set; }
		int DepartmentId { get; set; }
		Department Department { get; set; }
		int LocationId { get; set; }
		string Location { get; set; }
		string LocationName { get; set; }
		int? ManagerId { get; set; }
		int RoleId { get; set; }
		Role Role { get; set; }
		int? TeamId { get; set; }
		IUser User { get; set; }
	}
}