namespace Submissions.Web
{
	/// <summary>
	/// Only add const variables
	/// Non-const should be injected for unit-testing
	/// </summary>
	public static class Globals
	{
		public const string DefaultDashboardName = "Default Dashboard";
		public const string LettersOldRootPath = @"\\gaminglabs.net\glimain\letters\";

		// Widgets and Charts
		public const int CanvasLegendHeight = 25;
		public const string WidgetDefaultPanelColor = "#477fac";
		public const int WidgetDimension = 166;
		public const int WidgetMargin = 5;
		public const int WidgetPanelGridHeight = 31;
		public const int WidgetToolbarSearchHeight = 30;
		public const int WidgetTotalMargin = Globals.WidgetMargin * 2;
	}
}
