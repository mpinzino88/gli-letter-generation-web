using Submissions.Common;
using System.Collections.Generic;

namespace Submissions.Web
{
	public class UserContext : IUserContext
	{
		public Dictionary<Permission, AccessRight> AccessRights
		{
			get { return ComUtil.GetSession<Dictionary<Permission, AccessRight>>("GLISession_AccessRights"); }
			set { ComUtil.SetSession<Dictionary<Permission, AccessRight>>("GLISession_AccessRights", value); }
		}

		public int? DefaultDashboardId
		{
			get { return ComUtil.GetSession<int?>("GLISession_DefaultDashboardId"); }
			set { ComUtil.SetSession<int?>("GLISession_DefaultDashboardId", value); }
		}

		public int DepartmentId
		{
			get { return ComUtil.GetSession<int>("GLISession_DepartmentId"); }
			set { ComUtil.SetSession<int>("GLISession_DepartmentId", value); }
		}

		public Department Department
		{
			get { return ComUtil.GetSession<Department>("GLISession_Department"); }
			set { ComUtil.SetSession<Department>("GLISession_Department", value); }
		}

		public int LocationId
		{
			get { return ComUtil.GetSession<int>("GLISession_LocationId"); }
			set { ComUtil.SetSession<int>("GLISession_LocationId", value); }
		}

		public string Location
		{
			get { return ComUtil.GetSession<string>("GLISession_Location"); }
			set { ComUtil.SetSession<string>("GLISession_Location", value); }
		}

		public string LocationName
		{
			get { return ComUtil.GetSession<string>("GLISession_LocationName"); }
			set { ComUtil.SetSession<string>("GLISession_LocationName", value); }
		}

		public int? ManagerId
		{
			get { return ComUtil.GetSession<int?>("GLISession_ManagerId"); }
			set { ComUtil.SetSession<int?>("GLISession_ManagerId", value); }
		}

		public int RoleId
		{
			get { return ComUtil.GetSession<int>("GLISession_RoleId"); }
			set { ComUtil.SetSession<int>("GLISession_RoleId", value); }
		}

		public Role Role
		{
			get { return ComUtil.GetSession<Role>("GLISession_Role"); }
			set { ComUtil.SetSession<Role>("GLISession_Role", value); }
		}

		public int? TeamId
		{
			get { return ComUtil.GetSession<int?>("GLISession_TeamId"); }
			set { ComUtil.SetSession<int?>("GLISession_TeamId", value); }
		}

		public IUser User
		{
			get { return ComUtil.GetSession<User>("GLISession_User"); }
			set { ComUtil.SetSession<User>("GLISession_User", value); }
		}
	}
}