﻿using System;
using System.IO;
using System.Web.Configuration;
using System.Web.Hosting;

namespace Submissions.Web
{
	using Submissions.Common;

	public class PathUtil
	{
		private static readonly string _fileServer;
		private static readonly string _letter;
		private static readonly string _glimain;

		static PathUtil()
		{
			_fileServer = WebConfigurationManager.AppSettings["gli.gaminglabs.fileserverpath"];
			_letter = WebConfigurationManager.AppSettings["gli.gaminglabs.letterpath"];
			_glimain = WebConfigurationManager.AppSettings["gli.gaminglabs.glimain"];
		}

		#region Physical Paths
		/// <summary>
		/// Physical path to where all web-site documents should exist
		/// </summary>
		/// <returns>Physical path to submissions/gliintranet/fileserver/sitedata</returns>
		public static string FileServer()
		{
			return FileServer(String.Empty);
		}

		/// <summary>
		/// Physical path to where all web-site documents should exist
		/// </summary>
		/// <param name="appendPath">Append additional folder path</param>
		/// <returns>Physical path to submissions/gliintranet/fileserver/sitedata/[appendPath]</returns>
		public static string FileServer(string appendPath)
		{
			if (GLISession.User.Environment == Environment.Development)
			{
				return Path.Combine(_fileServer, appendPath);
			}
			else
			{
				var fileServer = HostingEnvironment.MapPath("~" + _fileServer);
				return Path.Combine(fileServer, appendPath);
			}
		}

		/// <summary>
		/// Physical path to pdf letters
		/// </summary>
		/// <returns>Physical path to submissions/gliintranet/fileserver</returns>
		public static string Letter(string pdfPath)
		{
			if (GLISession.User.Environment == Environment.Development)
			{
				return pdfPath;
			}
			else
			{
				var letterPath = HostingEnvironment.MapPath("~" + _letter);
				pdfPath = pdfPath.Replace(Globals.LettersOldRootPath, "");
				return Path.Combine(letterPath, pdfPath);
			}
		}

		/// <summary>
		/// File Number's network share path
		/// </summary>
		/// <param name="archiveLocation">Submission Archive Location Code</param>
		/// <param name="fileNumber">File Number</param>
		/// <returns></returns>
		public static string NetworkDrive(string archiveLocation, FileNumber fileNumber)
		{
			if (archiveLocation == null)
			{
				return null;
			}

			switch (archiveLocation)
			{
				case "EU":
					archiveLocation = "NL";
					break;
				case "VN":
					archiveLocation = "BC";
					break;
			}

			// pulled this fullyear code from .asp
			var fullYear = fileNumber.Year.Substring(0, 1) == "9" ? "19" + fileNumber.Year : "20" + fileNumber.Year;

			var root = Path.Combine(_glimain, "Public", archiveLocation, "Submissions");
			var newPath = Path.Combine(root, fullYear, fileNumber.ManufacturerCode, fileNumber.AccountingFormat);

			if (Directory.Exists(newPath))
			{
				return newPath;
			}

			return Path.Combine(root, fileNumber.ManufacturerCode, fullYear, fileNumber.AccountingFormat);
		}

		/// <summary>
		/// Physical path to temp folder
		/// </summary>
		public static string Temp()
		{
			return PathUtil.Temp(String.Empty);
		}

		public static string Temp(string appendPath)
		{
			return Path.Combine(HostingEnvironment.MapPath("~/Temp/"), appendPath);
		}

		/// <summary>
		/// User profile pictures used on the Contact Us link in Access
		/// </summary>
		public static string UserProfilePicture()
		{
			if (GLISession.User.Environment == Environment.Development)
			{
				return PathUtil.Temp(@"Submissions\ProfilePictures");
			}
			else
			{
				return PathUtil.FileServer(@"Submissions\ProfilePictures");
			}
		}

		public static string TKNumberPath()
		{
			if (GLISession.User.Environment == Environment.Development)
			{
				return PathUtil.Temp(@"Submissions\TKNumbers");
			}
			else
			{
				return PathUtil.FileServer(@"Submissions\TKNumbers");
			}
		}
		#endregion

		#region Url paths
		/// <summary>
		/// Url path to where all web-site documents should exist
		/// </summary>
		/// <param name="appendPath">Append additional folder path</param>
		/// <returns>
		/// <para>Development: Url path to /Temp/[appendPath]</para>
		/// <para>Production: Url path to submissions/gliintranet/fileserver/sitedata/[appendPath]</para>
		/// </returns>
		public static string FileServerUrl(string appendPath)
		{
			var baseUrl = ComUtil.GetBaseUrl();
			if (GLISession.User.Environment == Environment.Development)
			{
				var baseUri = new Uri(baseUrl + "Temp/");
				return new Uri(baseUri, appendPath).ToString();
			}
			else
			{
				var baseuri = new Uri(baseUrl + "FileServer/SiteData/");
				return new Uri(baseuri, appendPath).ToString();
			}
		}

		public static string UserProfilePictureUrl()
		{
			return PathUtil.FileServerUrl(@"Submissions\ProfilePictures");
		}

		public static string TKNumberUrl()
		{
			return PathUtil.FileServerUrl(@"Submissions\TKNumbers");
		}
		#endregion
	}
}