﻿using System;
using System.Web;

namespace Submissions.Web
{
	/// <summary>
	/// Strongly Typed Session initialized in SessionConfig.cs of App_Start
	/// </summary>
	[Serializable]
	public class SessionMgr
	{
		private SessionMgr() { }

		public static SessionMgr Current
		{
			get
			{
				var session = (SessionMgr)HttpContext.Current.Session["GLISession"];
				if (session == null)
				{
					session = new SessionMgr();
					HttpContext.Current.Session["GLISession"] = session;
				}
				return session;
			}
		}

		public Guid TransferRequestDetailId { get; set; }
		public int WithdrawalRequestDetailId { get; set; }
		public Guid WithdrawRequestDetailId { get; set; }
		public string SubmissionDetailPartialView { get; set; }
	}
}