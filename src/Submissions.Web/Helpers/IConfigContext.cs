﻿using Submissions.Common;

namespace Submissions.Web
{
	public interface IConfigContext
	{
		string MonitorAccountPassword { get; }
		string MonitorAccountUsername { get; }
		string PCSAttachmentPath { get; }
		string PCSUploadPath { get; }
		string PCTAttachmentPath { get; }
		string PCTUploadPath { get; }
		string SubmissionsBaseUrl { get; }
		string VerifySerivceAuthKey { get; }
		IConfig Config { get; set; }
	}
}
