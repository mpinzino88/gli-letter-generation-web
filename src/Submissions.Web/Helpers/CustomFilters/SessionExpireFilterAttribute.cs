﻿using Submissions.Common;
using System.Web;
using System.Web.Mvc;

namespace Submissions.Web.Helpers.CustomFilters
{
	public class SessionExpireFilterAttribute : ActionFilterAttribute, IActionFilter
	{
		void IActionFilter.OnActionExecuting(ActionExecutingContext filterContext)
		{
			if (HttpContext.Current.Session == null || HttpContext.Current.Session["GLISession_User"] == null)
			{
				SessionConfig.Register();
			}

			base.OnActionExecuting(filterContext);
		}
	}
}