﻿using Submissions.Common;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Submissions.Web
{
	public class AuthorizeUserAttribute : AuthorizeAttribute
	{
		public Permission Permission { get; set; }
		public AccessRight HasAccessRight { get; set; }
		public new Role[] Roles { get; set; }
		public Permission[] Permissions { get; set; }

		protected override bool AuthorizeCore(HttpContextBase httpContext)
		{
			if (!base.AuthorizeCore(httpContext))
			{
				return false;
			}
			if (this.Permissions != null)
			{
				return Util.HasAccess(this.Permissions, this.HasAccessRight, this.Roles);
			}
			else
			{
				return Util.HasAccess(this.Permission, this.HasAccessRight, this.Roles);
			}

		}

		protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
		{
			int missingPermission;
			if (this.Permissions != null)
			{
				missingPermission = (int)this.Permissions[0];
			}
			else
			{
				missingPermission = (int)this.Permission;
			}
			filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary(new { controller = "Home", action = "PermissionDenied", area = "", Id = missingPermission }));
		}
	}
}