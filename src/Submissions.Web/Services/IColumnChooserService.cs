﻿namespace Submissions.Web
{
	public interface IColumnChooserService
	{
		string LoadColumns(ColumnChooserGrid grid);
		string LoadColumns(ColumnChooserGrid grid, int ColChooserId);
		string LoadDefaultColumns(ColumnChooserGrid grid);
		void SaveColumns(ColumnChooserGrid grid, string columns);
		void SaveColumns(ColumnChooserGrid grid, string columns, int ColChooserId, string ColChooserName = "");
	}
}
