﻿using EF.Submission;
using System.Linq;
using Z.EntityFramework.Plus;

namespace Submissions.Web
{
	public class ColumnChooserService : IColumnChooserService
	{
		private readonly ISubmissionContext _dbSubmission;
		private readonly IUserContext _userContext;

		public ColumnChooserService(ISubmissionContext dbSubmission, IUserContext userContext)
		{
			_dbSubmission = dbSubmission;
			_userContext = userContext;
		}

		public string LoadColumns(ColumnChooserGrid grid)
		{
			var columns = _dbSubmission.tbl_ColumnChooserGrids
				.Where(x => x.UserId == _userContext.User.Id && x.Grid == grid.ToString() && x.Current == true)
				.Select(x => x.Columns)
				.FirstOrDefault();

			return columns;
		}

		public string LoadColumns(ColumnChooserGrid grid, int colChooserId)
		{
			var columns = _dbSubmission.tbl_ColumnChooserGrids
				.Where(x => x.UserId == _userContext.User.Id && x.Grid == grid.ToString() && x.Id == colChooserId)
				.Select(x => x.Columns)
				.SingleOrDefault();

			return columns;
		}

		//When User does not have any saved
		public string LoadDefaultColumns(ColumnChooserGrid grid)
		{
			var columns = _dbSubmission.tbl_ColumnChooserGrids
				.Where(x => x.UserId == -1 && x.Grid == grid.ToString())
				.Select(x => x.Columns)
				.SingleOrDefault();

			return columns;
		}

		public void SaveColumns(ColumnChooserGrid grid, string columns)
		{
			var columnChooserGrid = _dbSubmission.tbl_ColumnChooserGrids
				.Where(x => x.UserId == _userContext.User.Id && x.Grid == grid.ToString())
				.FirstOrDefault();

			if (columnChooserGrid == null)
			{
				var newColumnChooserGrid = new tbl_ColumnChooserGrid
				{
					UserId = _userContext.User.Id,
					Grid = grid.ToString(),
					Columns = columns,
					Name = "Default",
					Current = true
				};

				_dbSubmission.tbl_ColumnChooserGrids.Add(newColumnChooserGrid);
			}
			else
			{
				columnChooserGrid.Columns = columns;
				columnChooserGrid.Current = true;
			}

			_dbSubmission.SaveChanges();
		}

		public void SaveColumns(ColumnChooserGrid grid, string columns, int ColChooserId, string ColChooserName = "")
		{
			_dbSubmission.tbl_ColumnChooserGrids
				.Where(x => x.UserId == _userContext.User.Id && x.Grid == grid.ToString())
				.Update(x => new tbl_ColumnChooserGrid { Current = false });


			var columnChooserGrid = _dbSubmission.tbl_ColumnChooserGrids
				.Where(x => x.UserId == _userContext.User.Id && x.Grid == grid.ToString() && x.Id == ColChooserId)
				.SingleOrDefault();

			if (columnChooserGrid == null || ColChooserId == 0)
			{
				var newColumnChooserGrid = new tbl_ColumnChooserGrid
				{
					UserId = _userContext.User.Id,
					Grid = grid.ToString(),
					Columns = columns,
					Name = ColChooserName,
					Current = true
				};

				_dbSubmission.tbl_ColumnChooserGrids.Add(newColumnChooserGrid);
			}
			else
			{
				columnChooserGrid.Columns = columns;
				columnChooserGrid.Current = true;
			}

			_dbSubmission.SaveChanges();
		}
	}
}