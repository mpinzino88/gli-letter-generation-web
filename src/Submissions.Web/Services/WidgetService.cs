﻿using EF.Submission;
using Newtonsoft.Json;
using Submissions.Web.Models.Widgets;

namespace Submissions.Web
{
	public class WidgetService : IWidgetService
	{
		private readonly ISubmissionContext _dbSubmission;

		public WidgetService(ISubmissionContext dbSubmission)
		{
			_dbSubmission = dbSubmission;
		}

		public void AddWidget(tbl_Widget widget)
		{
			_dbSubmission.tbl_Widgets.Add(widget);
			_dbSubmission.SaveChanges();

			widget.Position = JsonConvert.SerializeObject(new Position { WidgetId = widget.Id });

			_dbSubmission.Entry(widget).Property(x => x.Position).IsModified = true;
			_dbSubmission.SaveChanges();
		}

		public void AddWidget(tbl_Widget widget, Position position)
		{
			_dbSubmission.tbl_Widgets.Add(widget);
			_dbSubmission.SaveChanges();

			position.WidgetId = widget.Id;
			widget.Position = JsonConvert.SerializeObject(position);

			_dbSubmission.Entry(widget).Property(x => x.Position).IsModified = true;
			_dbSubmission.SaveChanges();
		}
	}
}