﻿using EF.Submission;
using Submissions.Web.Models.Widgets;

namespace Submissions.Web
{
	public interface IWidgetService
	{
		void AddWidget(tbl_Widget widget);
		void AddWidget(tbl_Widget widget, Position position);
	}
}