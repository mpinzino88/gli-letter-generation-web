﻿using Elmah;
using Submissions.Common;
using System;
using System.Web;

namespace Submissions.Web
{
	public class Logger : ILogger
	{
		public void LogActivity()
		{
			throw new NotImplementedException();
		}

		/// <summary>
		/// Log Error to Elmah
		/// </summary>
		/// <param name="ex">The exception caught in the try/catch</param>
		public void LogError(Exception ex)
		{
			LogError(ex, null);
		}

		/// <summary>
		/// Log Error to Elmah with Additional Information
		/// </summary>
		/// <param name="ex">The exception caught in the try/catch</param>
		/// <param name="message">Any additional information</param>
		public void LogError(Exception ex, string message)
		{
			try
			{
				if (message != null)
				{
					var annotatedException = new Exception(message, ex);
					ErrorSignal.FromCurrentContext().Raise(annotatedException, HttpContext.Current);
				}
				else
				{
					ErrorSignal.FromCurrentContext().Raise(ex, HttpContext.Current);
				}
			}
			catch (Exception)
			{
				// ignore logging exception
			}
		}
	}
}