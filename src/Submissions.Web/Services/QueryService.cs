using EF.Submission;
using Fissoft.EntityFramework.Fts;
using LinqKit;
using Submissions.Common;
using Submissions.Common.Linq.Dynamic;
using Submissions.Core;
using Submissions.Core.Models.ProjectService;
using Submissions.Web.Models.LookupsAjax;
using Submissions.Web.Models.Query;
using Submissions.Web.Models.QueryService;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.SqlTypes;
using System.Linq;
using System.Linq.Expressions;

namespace Submissions.Web
{
	public class QueryService : IQueryService
	{
		private readonly ISubmissionContext _dbSubmission;
		private readonly EF.GLIAccess.IGLIAccessContext _dbGLIAccess;
		private readonly IMiscService _miscService;
		private readonly DateTime _endOfDayToday = ComUtil.EndOfDayToday();

		public QueryService
		(
			ISubmissionContext dbSubmission,
			EF.GLIAccess.IGLIAccessContext dbGLIAccess,
			IMiscService miscService
		)
		{
			_dbSubmission = dbSubmission;
			_dbGLIAccess = dbGLIAccess;
			_miscService = miscService;
		}

		#region PointClick
		public IQueryable<EF.GLIAccess.electronicSubmissionMaster> SubmitRequestSearch(SubmitRequestSearch filters)
		{
			var query = _dbGLIAccess.electronicSubmissionMasters.AsNoTracking().AsQueryable();

			if (filters.TestingTypeId != null)
			{
				query = query.Where(x => x.TestingTypeId == filters.TestingTypeId);
			}

			return query;
		}

		public IQueryable<EF.GLIAccess.TransferRequestHeader> TransferRequestSearch(TransferRequestSearch filters)
		{
			var isConfirmed = !filters.ShowUnconfirmedOnly;

			var query = _dbGLIAccess.TransferRequestHeaders.AsNoTracking().AsQueryable();
			query = query.Where(x => x.IsConfirmed == isConfirmed);

			if (!string.IsNullOrEmpty(filters.TransferRequestStatus))
			{
				query = query.Where(x => x.TransferRequestItems.Any(y => y.Status == filters.TransferRequestStatus));
			}

			if (!string.IsNullOrEmpty(filters.FileNumber))
			{
				query = query.Where(x => x.TransferRequestItems.Any(y => y.Submission.FileNumber == filters.FileNumber));
			}

			return query;
		}

		public IQueryable<EF.GLIAccess.TransferRequestItem> TransferRequestDetailSearch(TransferRequestDetailSearch filters)
		{
			var query = _dbGLIAccess.TransferRequestItems
				.Include(x => x.Submission)
				.Include(x => x.Jurisdiction)
				.AsNoTracking()
				.AsQueryable();

			if (filters.TransferRequestId != null)
			{
				query = query.Where(x => x.TransferRequestHeaderId == (Guid)filters.TransferRequestId);
			}

			return query;
		}

		public IQueryable<EF.GLIAccess.WithdrawalRequest> WithdrawRequestSearch(WithdrawRequestSearch filters)
		{
			var query = _dbGLIAccess.WithdrawalRequests
				.Include(x => x.Submission.JurisdictionalData)
				.AsNoTracking();

			// Withdraw Filters
			var startRequestDate = (filters.StartRequestDate == null) ? (DateTime)SqlDateTime.MinValue : Convert.ToDateTime(filters.StartRequestDate);
			var endRequestDate = (filters.EndRequestDate == null) ? (DateTime)SqlDateTime.MaxValue : Convert.ToDateTime(filters.EndRequestDate).AddDays(1).AddTicks(-1);

			query = query.Where(x => x.RequestDate >= startRequestDate && x.RequestDate <= endRequestDate);

			if (!string.IsNullOrEmpty(filters.Status))
			{
				var _submissionStatusWD = SubmissionStatus.WD.ToString();
				var _submissionStatusRJ = SubmissionStatus.RJ.ToString();

				if (filters.Status == WithdrawStatus.Cancelled.ToString())
				{
					query = query.Where(x => (x.ProcessDate != null && x.JurisdictionalDataId == null && (x.Submission.Status != _submissionStatusWD && x.Submission.Status != _submissionStatusRJ)) ||
											(x.ProcessDate != null && x.JurisdictionalDataId != null && (x.JurisdictionalData.Status != _submissionStatusWD && x.JurisdictionalData.Status != _submissionStatusRJ)));
				}
				else if (filters.Status == WithdrawStatus.Pending.ToString())
				{
					query = query.Where(x => x.ProcessDate == null);
				}
				else if (filters.Status == WithdrawStatus.Processed.ToString())
				{
					query = query.Where(x => (x.ProcessDate != null && x.JurisdictionalDataId != null && (x.JurisdictionalData.Status == _submissionStatusWD || x.JurisdictionalData.Status == _submissionStatusRJ)) ||
						(x.ProcessDate != null && x.JurisdictionalDataId == null && (x.Submission.Status == _submissionStatusWD || x.Submission.Status == _submissionStatusRJ)));
				}
			}

			if (filters.RequestId != null)
			{
				query = query.Where(x => x.RequestId == filters.RequestId);
			}

			if (filters.OrderNumber != null)
			{
				query = query.Where(x => x.OrderNumber == filters.OrderNumber);
			}

			if (filters.UserId != null)
			{
				query = query.Where(x => x.AddUserId == filters.UserId);
			}

			if (!string.IsNullOrWhiteSpace(filters.FileNumber))
			{
				query = query.Where(x => x.Submission.FileNumber.StartsWith(filters.FileNumber));
			}

			if (!string.IsNullOrWhiteSpace(filters.ManufacturerCode))
			{
				query = query.Where(x => x.Submission.Manufacturer == filters.ManufacturerCode);
			}

			if (!string.IsNullOrWhiteSpace(filters.GameName))
			{
				query = query.Where(x => x.Submission.GameName.StartsWith(filters.GameName));
			}

			if (!string.IsNullOrWhiteSpace(filters.IdNumber))
			{
				query = query.Where(x => x.Submission.IdNumber.StartsWith(filters.IdNumber));
			}

			if (!string.IsNullOrWhiteSpace(filters.Version))
			{
				query = query.Where(x => x.Submission.Version.StartsWith(filters.Version));
			}

			if (!string.IsNullOrWhiteSpace(filters.JurisdictionId))
			{
				query = query.Where(x => x.Submission.JurisdictionalData.Any(j => j.JurisdictionId == filters.JurisdictionId));
			}

			if (!string.IsNullOrWhiteSpace(filters.Version))
			{
				query = query.Where(x => x.Submission.Version.StartsWith(filters.Version));
			}

			if (filters.IsJurisdiction != null)
			{
				if ((bool)filters.IsJurisdiction)
				{
					query = query.Where(x => x.JurisdictionalDataId != null);
				}
				else
				{
					query = query.Where(x => x.JurisdictionalDataId == null);
				}
			}
			query.OrderByDescending(x => x.RequestDate);

			return query;
		}
		#endregion

		#region Search
		public IQueryable<Submission> AdvancedSearch(AdvancedSearchSearch filters)
		{
			var endOfDayToday = ComUtil.EndOfDayToday();
			var query = _dbSubmission.Submissions.AsExpandable().AsNoTracking().AsQueryable();

			// Submission
			var startSubmitDate = (filters.Submission.StartSubmitDate == null) ? (DateTime)SqlDateTime.MinValue : Convert.ToDateTime(filters.Submission.StartSubmitDate).Date;
			var endSubmitDate = (filters.Submission.EndSubmitDate == null) ? (DateTime)SqlDateTime.MaxValue : Convert.ToDateTime(filters.Submission.EndSubmitDate).Date.AddDays(1).AddTicks(-1);
			var startReceiveDate = (filters.Submission.StartReceiveDate == null) ? (DateTime)SqlDateTime.MinValue : Convert.ToDateTime(filters.Submission.StartReceiveDate).Date;
			var endReceiveDate = (filters.Submission.EndReceiveDate == null) ? (DateTime)SqlDateTime.MaxValue : Convert.ToDateTime(filters.Submission.EndReceiveDate).Date.AddDays(1).AddTicks(-1);

			query = query.Where(x => x.SubmitDate >= startSubmitDate && x.SubmitDate <= endSubmitDate);
			query = query.Where(x => x.ReceiveDate >= startReceiveDate && x.ReceiveDate <= endReceiveDate);

			if (filters.Submission.ArchiveLocationId != null)
			{
				var archiveLocation = _dbSubmission.trLaboratories.Where(x => x.Id == filters.Submission.ArchiveLocationId).Select(x => x.Location).Single();
				query = query.Where(x => x.ArchiveLocation == archiveLocation);
			}

			if (filters.Submission.CertificationLabId != null)
			{
				var certificationLab = _dbSubmission.trLaboratories.Where(x => x.Id == filters.Submission.CertificationLabId).Select(x => x.Location).Single();
				query = query.Where(x => x.CertificationLab == certificationLab);
			}

			if (!string.IsNullOrEmpty(filters.Submission.ChipType))
			{
				query = query.Where(x => x.ChipType == filters.Submission.ChipType);
			}

			if (filters.Submission.CompanyId != null)
			{
				query = query.Where(x => x.CompanyId == filters.Submission.CompanyId);
			}

			if (filters.Submission.ConditionalRevoke != null)
			{
				query = query.Where(x => x.ConditionalRevoke == filters.Submission.ConditionalRevoke);
			}

			if (!string.IsNullOrEmpty(filters.Submission.ContractNumber))
			{
				var term = filters.Submission.ContractNumber;
				if (term.Contains("*"))
				{
					term = term.Replace("*", "%");
					query = query.Where(x => DbFunctions.Like(x.ContractNumber, term));
				}
				else
				{
					query = query.Where(x => x.ContractNumber == term);
				}
			}

			if (filters.Submission.ContractTypeId != null)
			{
				query = query.Where(x => x.ContractTypeId == filters.Submission.ContractTypeId);
			}

			if (filters.Submission.CurrencyId != null)
			{
				query = query.Where(x => x.CurrencyId == filters.Submission.CurrencyId);
			}

			if (!string.IsNullOrEmpty(filters.Submission.DateCode))
			{
				var term = filters.Submission.DateCode;
				if (term.Contains("*"))
				{
					term = term.Replace("*", "%");
					query = query.Where(x => DbFunctions.Like(x.DateCode, term));
				}
				else
				{
					query = query.Where(x => x.DateCode == term);
				}
			}

			if (filters.Submission.ExternalLabId != null)
			{
				query = query.Where(x => x.ExternalLabId == filters.Submission.ExternalLabId);
			}

			if (!string.IsNullOrEmpty(filters.Submission.FileNumber))
			{
				var term = filters.Submission.FileNumber;
				try
				{
					if (term.Count(x => x == '-') < 4 || term.Contains("*"))
					{
						throw new Exception("Not a valid File Number so we can't search with Manufacturer Code");
					}

					var validFileNumber = new FileNumber(term);
					var fileNumber = validFileNumber.ToString();
					var manufacturerCode = validFileNumber.ManufacturerCode;

					query = query.Where(x => x.ManufacturerCode == manufacturerCode);
					query = query.Where(x => x.FileNumber == fileNumber);
				}
				catch
				{
					if (term.Contains("*"))
					{
						term = term.Replace("*", "%");
						query = query.Where(x => DbFunctions.Like(x.FileNumber, term));
					}
					else
					{
						query = query.Where(x => x.FileNumber == term);
					}
				}
			}

			if (!string.IsNullOrEmpty(filters.Submission.Function))
			{
				query = query.Where(x => x.Function == filters.Submission.Function);
			}

			if (!string.IsNullOrEmpty(filters.Submission.GameName))
			{
				var term = filters.Submission.GameName;
				if (term.Contains("*"))
				{
					term = term.Replace("*", "%");
					query = query.Where(x => DbFunctions.Like(x.GameName, term));
				}
				else
				{
					query = query.Where(x => x.GameName == term);
				}
			}

			if (!string.IsNullOrEmpty(filters.Submission.GameType))
			{
				query = query.Where(x => x.Function == filters.Submission.GameType);
			}

			if (!string.IsNullOrEmpty(filters.Submission.IdNumber))
			{
				var term = filters.Submission.IdNumber;
				if (term.Contains("*"))
				{
					term = term.Replace("*", "%");
					query = query.Where(x => DbFunctions.Like(x.IdNumber, term));
				}
				else
				{
					query = query.Where(x => x.IdNumber == term);
				}
			}

			if (filters.Submission.LabId != null)
			{
				var lab = _dbSubmission.trLaboratories.Where(x => x.Id == filters.Submission.LabId).Select(x => x.Location).Single();
				query = query.Where(x => x.Lab == lab);
			}

			if (!string.IsNullOrEmpty(filters.Submission.LetterNumber))
			{
				var term = filters.Submission.LetterNumber;
				if (term.Contains("*"))
				{
					term = term.Replace("*", "%");
					query = query.Where(x => DbFunctions.Like(x.LetterNumber, term));
				}
				else
				{
					query = query.Where(x => x.LetterNumber == term);
				}
			}

			if (!string.IsNullOrEmpty(filters.Submission.ManufacturerBuildId))
			{
				var term = filters.Submission.ManufacturerBuildId;
				if (term.Contains("*"))
				{
					term = term.Replace("*", "%");
					query = query.Where(x => DbFunctions.Like(x.ManufacturerBuildId, term));
				}
				else
				{
					query = query.Where(x => x.ManufacturerBuildId == term);
				}
			}

			if (!string.IsNullOrEmpty(filters.Submission.ManufacturerCode))
			{
				query = query.Where(x => x.ManufacturerCode == filters.Submission.ManufacturerCode);
			}

			if (!string.IsNullOrEmpty(filters.Submission.Operator))
			{
				var term = filters.Submission.Operator;
				if (term.Contains("*"))
				{
					term = term.Replace("*", "%");
					query = query.Where(x => DbFunctions.Like(x.Operator, term));
				}
				else
				{
					query = query.Where(x => x.Operator == term);
				}
			}

			if (!string.IsNullOrEmpty(filters.Submission.OrgIGTMaterial))
			{
				var term = filters.Submission.OrgIGTMaterial;
				if (term.Contains("*"))
				{
					term = term.Replace("*", "%");
					query = query.Where(x => DbFunctions.Like(x.OrgIGTMaterial, term));
				}
				else
				{
					query = query.Where(x => x.OrgIGTMaterial == term);
				}
			}

			if (!string.IsNullOrEmpty(filters.Submission.PartNumber))
			{
				var term = filters.Submission.PartNumber;
				if (term.Contains("*"))
				{
					term = term.Replace("*", "%");
					query = query.Where(x => DbFunctions.Like(x.PartNumber, term));
				}
				else
				{
					query = query.Where(x => x.PartNumber == term);
				}
			}

			if (!string.IsNullOrEmpty(filters.Submission.Position))
			{
				var term = filters.Submission.Position;
				if (term.Contains("*"))
				{
					term = term.Replace("*", "%");
					query = query.Where(x => DbFunctions.Like(x.Position, term));
				}
				else
				{
					query = query.Where(x => x.Position == term);
				}
			}

			if (!string.IsNullOrEmpty(filters.Submission.ProjectDetail))
			{
				var term = filters.Submission.ProjectDetail;
				if (term.Contains("*"))
				{
					term = term.Replace("*", "%");
					query = query.Where(x => DbFunctions.Like(x.ProjectDetail, term));
				}
				else
				{
					query = query.Where(x => x.ProjectDetail == term);
				}
			}

			if (!string.IsNullOrEmpty(filters.Submission.PurchaseOrder))
			{
				var term = filters.Submission.PurchaseOrder;
				if (term.Contains("*"))
				{
					term = term.Replace("*", "%");
					query = query.Where(x => DbFunctions.Like(x.PurchaseOrder, term));
				}
				else
				{
					query = query.Where(x => x.PurchaseOrder == term);
				}
			}

			if (filters.Submission.RegressionTesting != null)
			{
				query = query.Where(x => x.IsRegressionTesting == filters.Submission.RegressionTesting);
			}

			if (!string.IsNullOrEmpty(filters.Submission.ReplaceWith))
			{
				var term = filters.Submission.ReplaceWith;
				if (term.Contains("*"))
				{
					term = term.Replace("*", "%");
					query = query.Where(x => DbFunctions.Like(x.ReplaceWith, term));
				}
				else
				{
					query = query.Where(x => x.ReplaceWith == term);
				}
			}

			if (!string.IsNullOrEmpty(filters.Submission.Status))
			{
				query = query.Where(x => x.Status == filters.Submission.Status);
			}

			if (!string.IsNullOrEmpty(filters.Submission.System))
			{
				var term = filters.Submission.System;
				if (term.Contains("*"))
				{
					term = term.Replace("*", "%");
					query = query.Where(x => DbFunctions.Like(x.System, term));
				}
				else
				{
					query = query.Where(x => x.System == term);
				}
			}

			if (filters.Submission.TestingPerformedId != null)
			{
				query = query.Where(x => x.TestingPerformedId == filters.Submission.TestingPerformedId);
			}

			if (filters.Submission.TestingTypeId != null)
			{
				query = query.Where(x => x.TestingTypeId == filters.Submission.TestingTypeId);
			}

			if (filters.Submission.TypeId != null)
			{
				var type = _dbSubmission.tbl_lst_SubmissionType.Where(x => x.Id == filters.Submission.TypeId).Select(x => x.Code).Single();
				query = query.Where(x => x.Type == type);
			}

			if (!string.IsNullOrEmpty(filters.Submission.Vendor))
			{
				var term = filters.Submission.Vendor;
				if (term.Contains("*"))
				{
					term = term.Replace("*", "%");
					query = query.Where(x => DbFunctions.Like(x.Vendor, term));
				}
				else
				{
					query = query.Where(x => x.Vendor == term);
				}
			}

			if (!string.IsNullOrEmpty(filters.Submission.Version))
			{
				var term = filters.Submission.Version;
				if (term.Contains("*"))
				{
					term = term.Replace("*", "%");
					query = query.Where(x => DbFunctions.Like(x.Version, term));
				}
				else
				{
					query = query.Where(x => x.Version == term);
				}
			}

			if (filters.Submission.WorkPerformedId != null)
			{
				query = query.Where(x => x.WorkPerformedId == filters.Submission.WorkPerformedId);
			}

			if (!string.IsNullOrEmpty(filters.Submission.Year))
			{
				var year = filters.Submission.Year.Trim().Right(2);
				query = query.Where(x => x.Year == year);
			}

			if (filters.Submission.CodeStorageLocationId != null)
			{
				query = query.Where(x => x.SourceCodeStorage.Any(y => y.LabId == filters.Submission.CodeStorageLocationId));
			}

			if (!string.IsNullOrEmpty(filters.Submission.CodeStorage))
			{
				query = query.Where(x => x.SourceCodeStorage.Any(y => y.CodeStorage.ToString() == filters.Submission.CodeStorage));
			}

			if (filters.Submission.BugBoxLocationId != null)
			{
				query = query.Where(x => x.BugBox.Any(y => y.LabId == filters.Submission.BugBoxLocationId));
			}

			if (!string.IsNullOrEmpty(filters.Submission.BugBox))
			{
				query = query.Where(x => x.BugBox.Any(y => y.BugBox == filters.Submission.BugBox));
			}

			if (!string.IsNullOrEmpty(filters.Submission.AristocratType))
			{
				var aristocratData = _dbSubmission.AristocratData.Where(x => x.AristocratType == filters.Submission.AristocratType).Select(x => x.FileNumber).AsQueryable();
				query = query.Where(x => aristocratData.Contains(x.FileNumber));
			}

			if (!string.IsNullOrEmpty(filters.Submission.AristocratComplexity))
			{
				var aristocratData = _dbSubmission.AristocratData.Where(x => x.AristocratComplexity == filters.Submission.AristocratComplexity).Select(x => x.FileNumber).AsQueryable();
				query = query.Where(x => aristocratData.Contains(x.FileNumber));
			}

			if (!string.IsNullOrEmpty(filters.Submission.AristocratMarket))
			{
				var aristocratMarkets = _dbSubmission.AristocratDataMarket.Where(x => x.Market == filters.Submission.AristocratMarket && x.IsPrime == true).Select(x => x.AristocratDataId).AsQueryable();
				var aristocratData = _dbSubmission.AristocratData.Where(x => aristocratMarkets.Contains(x.Id)).Select(x => x.FileNumber).AsQueryable();
				query = query.Where(x => aristocratData.Contains(x.FileNumber));
			}

			// Jurisdiction
			var predicateJurisdiction = PredicateBuilder.New<JurisdictionalData>();

			if (!string.IsNullOrEmpty(filters.Jurisdiction.ClientNumber))
			{
				var term = filters.Jurisdiction.ClientNumber;
				if (term.Contains("*"))
				{
					term = term.Replace("*", "%");
					predicateJurisdiction = predicateJurisdiction.And(x => DbFunctions.Like(x.ClientNumber, term));
				}
				else
				{
					predicateJurisdiction = predicateJurisdiction.And(x => x.ClientNumber == term);
				}
			}

			if (filters.Jurisdiction.CompanyId != null)
			{
				predicateJurisdiction = predicateJurisdiction.And(x => x.CompanyId == filters.Jurisdiction.CompanyId);
			}

			if (!string.IsNullOrEmpty(filters.Jurisdiction.ContractNumber))
			{
				var term = filters.Jurisdiction.ContractNumber;
				if (term.Contains("*"))
				{
					term = term.Replace("*", "%");
					predicateJurisdiction = predicateJurisdiction.And(x => DbFunctions.Like(x.ContractNumber, term));
				}
				else
				{
					predicateJurisdiction = predicateJurisdiction.And(x => x.ContractNumber == term);
				}
			}

			if (filters.Jurisdiction.ContractTypeId != null)
			{
				predicateJurisdiction = predicateJurisdiction.And(x => x.ContractTypeId == filters.Jurisdiction.ContractTypeId);
			}

			if (filters.Jurisdiction.CurrencyId != null)
			{
				predicateJurisdiction = predicateJurisdiction.And(x => x.CurrencyId == filters.Jurisdiction.CurrencyId);
			}

			if (filters.Jurisdiction.ConditionalRevoke != null)
			{
				predicateJurisdiction = predicateJurisdiction.And(x => x.ConditionalRevoke == filters.Jurisdiction.ConditionalRevoke);
			}

			if (filters.Jurisdiction.JurisdictionIds != null)
			{
				predicateJurisdiction = predicateJurisdiction.And(x => filters.Jurisdiction.JurisdictionIds.Contains(x.JurisdictionId));
			}

			if (!string.IsNullOrEmpty(filters.Jurisdiction.LetterNumber))
			{
				var term = filters.Jurisdiction.LetterNumber;
				if (term.Contains("*"))
				{
					term = term.Replace("*", "%");
					predicateJurisdiction = predicateJurisdiction.And(x => DbFunctions.Like(x.LetterNumber, term));
				}
				else
				{
					predicateJurisdiction = predicateJurisdiction.And(x => x.LetterNumber == term);
				}
			}

			if (!string.IsNullOrEmpty(filters.Jurisdiction.RegulatorRefNum))
			{
				var term = filters.Jurisdiction.RegulatorRefNum;
				if (term.Contains("*"))
				{
					term = term.Replace("*", "%");
					predicateJurisdiction = predicateJurisdiction.And(x => DbFunctions.Like(x.RegulatorRefNum, term));
				}
				else
				{
					predicateJurisdiction = predicateJurisdiction.And(x => x.RegulatorRefNum == term);
				}
			}

			if (!string.IsNullOrEmpty(filters.Jurisdiction.CertNumber))
			{
				var term = filters.Jurisdiction.CertNumber;
				if (term.Contains("*"))
				{
					term = term.Replace("*", "%");
					predicateJurisdiction = predicateJurisdiction.And(x => x.JurisdictionalDataCertificationNumbers.Any(jc => DbFunctions.Like(jc.CertificationNumber.CertNumber, term)));
				}
				else
				{
					predicateJurisdiction = predicateJurisdiction.And(x => x.JurisdictionalDataCertificationNumbers.Any(jc => jc.CertificationNumber.CertNumber == term));
				}
			}


			if (filters.Jurisdiction.StartAuthorizeDate != null || filters.Jurisdiction.EndAuthorizeDate != null)
			{
				var startAuthorizeDate = (filters.Jurisdiction.StartAuthorizeDate == null) ? (DateTime)SqlDateTime.MinValue : Convert.ToDateTime(filters.Jurisdiction.StartAuthorizeDate).Date;
				var endAuthorizeDate = (filters.Jurisdiction.EndAuthorizeDate == null) ? (DateTime)SqlDateTime.MaxValue : Convert.ToDateTime(filters.Jurisdiction.EndAuthorizeDate).Date.AddDays(1).AddTicks(-1);

				predicateJurisdiction = predicateJurisdiction.And(x => x.QuoteAuthDate.AuthorizeDate >= startAuthorizeDate && x.QuoteAuthDate.AuthorizeDate <= endAuthorizeDate);
			}

			if (filters.Jurisdiction.StartCloseDate != null || filters.Jurisdiction.EndCloseDate != null)
			{
				var startCloseDate = (filters.Jurisdiction.StartCloseDate == null) ? (DateTime)SqlDateTime.MinValue : Convert.ToDateTime(filters.Jurisdiction.StartCloseDate).Date;
				var endCloseDate = (filters.Jurisdiction.EndCloseDate == null) ? (DateTime)SqlDateTime.MaxValue : Convert.ToDateTime(filters.Jurisdiction.EndCloseDate).Date.AddDays(1).AddTicks(-1);

				predicateJurisdiction = predicateJurisdiction.And(x => x.CloseDate >= startCloseDate && x.CloseDate <= endCloseDate);
			}

			if (filters.Jurisdiction.StartDraftDate != null || filters.Jurisdiction.EndDraftDate != null)
			{
				var startDraftDate = (filters.Jurisdiction.StartDraftDate == null) ? (DateTime)SqlDateTime.MinValue : Convert.ToDateTime(filters.Jurisdiction.StartDraftDate).Date;
				var endDraftDate = (filters.Jurisdiction.EndDraftDate == null) ? (DateTime)SqlDateTime.MaxValue : Convert.ToDateTime(filters.Jurisdiction.EndDraftDate).Date.AddDays(1).AddTicks(-1);

				predicateJurisdiction = predicateJurisdiction.And(x => x.DraftDate >= startDraftDate && x.DraftDate <= endDraftDate);
			}

			if (filters.Jurisdiction.StartObsoleteDate != null || filters.Jurisdiction.EndObsoleteDate != null)
			{
				var startObsoleteDate = (filters.Jurisdiction.StartObsoleteDate == null) ? (DateTime)SqlDateTime.MinValue : Convert.ToDateTime(filters.Jurisdiction.StartObsoleteDate).Date;
				var endObsoleteDate = (filters.Jurisdiction.EndObsoleteDate == null) ? (DateTime)SqlDateTime.MaxValue : Convert.ToDateTime(filters.Jurisdiction.EndObsoleteDate).Date.AddDays(1).AddTicks(-1);

				predicateJurisdiction = predicateJurisdiction.And(x => x.ObsoleteDate >= startObsoleteDate && x.ObsoleteDate <= endObsoleteDate);
			}

			if (filters.Jurisdiction.StartQuoteDate != null || filters.Jurisdiction.EndQuoteDate != null)
			{
				var startQuoteDate = (filters.Jurisdiction.StartQuoteDate == null) ? (DateTime)SqlDateTime.MinValue : Convert.ToDateTime(filters.Jurisdiction.StartQuoteDate).Date;
				var endQuoteDate = (filters.Jurisdiction.EndQuoteDate == null) ? (DateTime)SqlDateTime.MaxValue : Convert.ToDateTime(filters.Jurisdiction.EndQuoteDate).Date.AddDays(1).AddTicks(-1);

				predicateJurisdiction = predicateJurisdiction.And(x => x.QuoteAuthDate.QuoteDate >= startQuoteDate && x.QuoteAuthDate.QuoteDate <= endQuoteDate);
			}

			if (filters.Jurisdiction.StartRevokeDate != null || filters.Jurisdiction.EndRevokeDate != null)
			{
				var startRevokeDate = (filters.Jurisdiction.StartRevokeDate == null) ? (DateTime)SqlDateTime.MinValue : Convert.ToDateTime(filters.Jurisdiction.StartRevokeDate).Date;
				var endRevokeDate = (filters.Jurisdiction.EndRevokeDate == null) ? (DateTime)SqlDateTime.MaxValue : Convert.ToDateTime(filters.Jurisdiction.EndRevokeDate).Date.AddDays(1).AddTicks(-1);

				predicateJurisdiction = predicateJurisdiction.And(x => x.RevokeDate >= startRevokeDate && x.RevokeDate <= endRevokeDate);
			}

			if (filters.Jurisdiction.StartUpdateDate != null || filters.Jurisdiction.EndUpdateDate != null)
			{
				var startUpdateDate = (filters.Jurisdiction.StartUpdateDate == null) ? (DateTime)SqlDateTime.MinValue : Convert.ToDateTime(filters.Jurisdiction.StartUpdateDate).Date;
				var endUpdateDate = (filters.Jurisdiction.EndUpdateDate == null) ? (DateTime)SqlDateTime.MaxValue : Convert.ToDateTime(filters.Jurisdiction.EndUpdateDate).Date.AddDays(1).AddTicks(-1);

				predicateJurisdiction = predicateJurisdiction.And(x => x.UpdateDate >= startUpdateDate && x.UpdateDate <= endUpdateDate);
			}

			if (filters.Jurisdiction.Statuses != null)
			{
				predicateJurisdiction = predicateJurisdiction.And(x => filters.Jurisdiction.Statuses.Contains(x.Status));
			}

			if (!string.IsNullOrEmpty(filters.Jurisdiction.WON))
			{
				var term = filters.Jurisdiction.WON;
				if (term.Contains("*"))
				{
					term = term.Replace("*", "%");
					predicateJurisdiction = predicateJurisdiction.And(x => DbFunctions.Like(x.MasterBillingProject, term));
				}
				else
				{
					predicateJurisdiction = predicateJurisdiction.And(x => x.MasterBillingProject == term);
				}
			}

			if (predicateJurisdiction.IsStarted)
			{
				Expression<Func<JurisdictionalData, bool>> jurisdictionAnyExpression = predicateJurisdiction;
				query = query.Where(x => x.JurisdictionalData.Any(jurisdictionAnyExpression.Compile()));
			}

			// Signature
			var predicateSignature = PredicateBuilder.New<tblSignature>();

			if (filters.Signature.ScopeId != null)
			{
				predicateSignature = predicateSignature.And(x => x.ScopeOfId == filters.Signature.ScopeId);
			}

			if (!string.IsNullOrEmpty(filters.Signature.Signature))
			{
				var term = filters.Signature.Signature;
				if (term.Contains("*"))
				{
					term = term.Replace("*", "%");
					predicateSignature = predicateSignature.And(x => DbFunctions.Like(x.Signature, term));
				}
				else
				{
					predicateSignature = predicateSignature.And(x => x.Signature == term);
				}
			}

			if (filters.Signature.TypeId != null)
			{
				predicateSignature = predicateSignature.And(x => x.TypeOfId == filters.Signature.TypeId);
			}

			if (predicateSignature.IsStarted)
			{
				Expression<Func<tblSignature, bool>> signatureAnyExpression = predicateSignature;
				query = query.Where(x => x.Signatures.Any(signatureAnyExpression.Compile()));
			}

			// Memo
			if (!string.IsNullOrEmpty(filters.Memo.Memo))
			{
				var term = FullTextSearchModelUtil.Contains(filters.Memo.Memo, true);
				if (filters.Memo.Type.HasValue)
				{
					if (filters.Memo.Type == MemoType.GN)
					{
						query = query.Where(x => x.Memos.Any(y => y.Memo.Contains(term)));
					}
					else if (filters.Memo.Type == MemoType.OH)
					{
						query = query.Where(x => x.OHMemos.Any(y => y.Memo.Contains(term)));
					}
				}
				else
				{
					query = query.Where(x => x.Memos.Any(y => y.Memo.Contains(term)) || x.OHMemos.Any(y => y.Memo.Contains(term)));
				}
			}

			// Machine Certificate
			var predicateMachineCertificate = PredicateBuilder.New<tbl_machine_cert>();

			if (!string.IsNullOrEmpty(filters.MachineCertificate.CertificateNumber))
			{
				var term = filters.MachineCertificate.CertificateNumber;
				if (term.Contains("*"))
				{
					term = term.Replace("*", "%");
					predicateMachineCertificate = predicateMachineCertificate.And(x => DbFunctions.Like(x.CertificateNumber, term));
				}
				else
				{
					predicateMachineCertificate = predicateMachineCertificate.And(x => x.CertificateNumber == term);
				}
			}

			if (!string.IsNullOrEmpty(filters.MachineCertificate.SerialNumber))
			{
				var term = filters.MachineCertificate.SerialNumber;
				if (term.Contains("*"))
				{
					term = term.Replace("*", "%");
					predicateMachineCertificate = predicateMachineCertificate.And(x => DbFunctions.Like(x.SerialNumber, term));
				}
				else
				{
					predicateMachineCertificate = predicateMachineCertificate.And(x => x.SerialNumber == term);
				}
			}

			if (!string.IsNullOrEmpty(filters.MachineCertificate.SealNumber))
			{
				var term = filters.MachineCertificate.SealNumber;
				if (term.Contains("*"))
				{
					term = term.Replace("*", "%");
					predicateMachineCertificate = predicateMachineCertificate.And(x => DbFunctions.Like(x.SealNumber, term));
				}
				else
				{
					predicateMachineCertificate = predicateMachineCertificate.And(x => x.SealNumber == term);
				}
			}

			if (filters.MachineCertificate.StartCertificateDate != null || filters.MachineCertificate.EndCertificateDate != null)
			{
				var startCertificateDate = (filters.MachineCertificate.StartCertificateDate == null) ? (DateTime)SqlDateTime.MinValue : Convert.ToDateTime(filters.MachineCertificate.StartCertificateDate).Date;
				var endCertificateDate = (filters.MachineCertificate.EndCertificateDate == null) ? (DateTime)SqlDateTime.MaxValue : Convert.ToDateTime(filters.MachineCertificate.EndCertificateDate).Date.AddDays(1).AddTicks(-1);

				predicateMachineCertificate = predicateMachineCertificate.And(x => x.CertificateDate >= startCertificateDate && x.CertificateDate <= endCertificateDate);
			}

			if (!string.IsNullOrEmpty(filters.MachineCertificate.DeviceType))
			{
				var term = filters.MachineCertificate.DeviceType;
				if (term.Contains("*"))
				{
					term = term.Replace("*", "%");
					predicateMachineCertificate = predicateMachineCertificate.And(x => DbFunctions.Like(x.DeviceType, term));
				}
				else
				{
					predicateMachineCertificate = predicateMachineCertificate.And(x => x.DeviceType == term);
				}
			}

			if (!string.IsNullOrEmpty(filters.MachineCertificate.CertificateThatWasReplaced))
			{
				var term = filters.MachineCertificate.CertificateThatWasReplaced;
				if (term.Contains("*"))
				{
					term = term.Replace("*", "%");
					predicateMachineCertificate = predicateMachineCertificate.And(x => x.ReplacementCertificates.Any(y => DbFunctions.Like(x.CertificateNumber, term)));
				}
				else
				{
					predicateMachineCertificate = predicateMachineCertificate.And(x => x.ReplacementCertificates.Any(y => y.CertificateNumber == term));
				}
			}

			if (predicateMachineCertificate.IsStarted)
			{
				Expression<Func<tbl_machine_cert, bool>> machineCertificateAnyExpression = predicateMachineCertificate;
				query = query.Where(x => x.MachineCertificates.Any(machineCertificateAnyExpression.Compile()));
			}

			return query;
		}

		public IQueryable<changeOfStatusMaster> ChangeOfStatusSearch(string fileNumber)
		{
			var query = _dbSubmission.changeOfStatusMasters.Where(x => x.FileNumber == fileNumber).AsQueryable();
			return query;
		}

		public IQueryable<tbl_lst_GamingGuidelines> GamingGuidelineSearch(GamingGuidelineSearch filters)
		{
			var query = _dbSubmission.tbl_lst_GamingGuidelines
				.Include(x => x.Jurisdiction)
				.Include("GamingGuidelineAttributes.GamingGuidelineAttribute")
				.Include("GamingGuidelineLimits.GamingGuidelineLimit")
				.Include("GamingGuidelineProhibitedGameTypes.GameType")
				.Include("GamingGuidelineProtocolTypes.ProtocolType")
				.Include("GamingGuidelineRTPRequirements.GameType")
				.Include("GamingGuidelineTechTypes.GamingGuidelineTechType");

			// for each filter option in filters, create sub query, return union
			if (filters.JurisdictionIds.Count() != 0)
			{
				query = query.Where(x => filters.JurisdictionIds.Contains(x.JurisdictionId));
			}
			if (filters.AttributeIds.Count() != 0)
			{
				// Should probabaly have a filter by attribute type option too
				query = query.Where(x => x.GamingGuidelineAttributes.Select(y => (int)y.AttributeId).Intersect(filters.AttributeIds).Any());
			}
			if (filters.LimitIds.Count() != 0)
			{
				query = query.Where(x => x.GamingGuidelineLimits.Select(y => (int)y.LimitTypeId).Intersect(filters.LimitIds).Any());
			}
			if (filters.ProhibitedGameTypeIds.Count() != 0)
			{
				query = query.Where(x => x.GamingGuidelineProhibitedGameTypes.Select(y => (int)y.GameTypeId).Intersect(filters.ProhibitedGameTypeIds).Any());
			}
			if (filters.ProtocolIds.Count() != 0)
			{
				query = query.Where(x => x.GamingGuidelineProtocolTypes.Select(y => (int)y.ProtocolTypeId).Intersect(filters.ProtocolIds).Any());
			}
			if (filters.RTPTypeIds.Count() != 0)
			{
				query = query.Where(x => x.GamingGuidelineRTPRequirements.Select(y => (int)y.GameTypeId).Intersect(filters.RTPTypeIds).Any());
			}
			if (filters.RTPMinValueIds != null)
			{
				query = query.Where(x => x.GamingGuidelineRTPRequirements.Count > 0 && x.GamingGuidelineRTPRequirements.Select(y => y.RTP).All(y => y >= filters.RTPMinValueIds));
			}
			if (filters.RTPMaxValueIds != null)
			{
				query = query.Where(x => x.GamingGuidelineRTPRequirements.Count > 0 && x.GamingGuidelineRTPRequirements.Select(y => y.RTP).All(y => y <= filters.RTPMaxValueIds));
			}
			if (filters.OddsValueIds.Count() != 0)
			{
				query = query.Where(x => x.GamingGuidelineOddsRequirements.Select(y => y.Odds).Intersect(filters.OddsValueIds).Any());
			}
			if (filters.LimitValueIds.Count() != 0)
			{
				query = query.Where(x => x.GamingGuidelineLimits.Select(y => y.Limit).Any(y => y >= filters.LimitValueIds.Min() && y <= filters.LimitValueIds.Max()));
			}
			if (!String.IsNullOrEmpty(filters.Alphabet))
			{
				query = query.Where(x => x.Jurisdiction.Name.StartsWith(filters.Alphabet));
			}
			query = query.Where(x => x.Active == !filters.InactiveToggle);
			return query;
		}

		public IQueryable<GamingGuidelineSportsBetting> GamingGuidelineSportsBettingSearch(GamingGuidelinesSportsBettingSearch filters)
		{
			IQueryable<GamingGuidelineSportsBetting> query = _dbSubmission.GamingGuidelineSportsBetting;

			if (filters.JurisdictionIds.Count() != 0)
			{
				var intFilters = new List<int>();
				foreach (var item in filters.JurisdictionIds)
				{
					intFilters.Add(Int32.Parse(item));
				}
				query = query.Where(x => intFilters.Contains(x.JurisdictionId));
			}

			return query;
		}

		public IQueryable<JiraItem> GetJiraIssues(int projectId)
		{
			var query = _dbSubmission.Database.SqlQuery<JiraItem>("exec GetProjectJiraCount " + projectId + ",'',1").AsQueryable();
			return query;
		}

		public IQueryable<UnassignedTaskHoursDetail> GetBPData(List<int> ProjectIds)
		{
			var query = "exec GetDynamicsData '" + string.Join(",", ProjectIds) + "'";
			var results = _dbSubmission.Database.SqlQuery<UnassignedTaskHoursDetail>(query).AsQueryable();
			return results;
		}

		public IQueryable<JurisdictionalData> JurisdictionalDataSearch(JurisdictionalDataSearch filters)
		{
			var query = _dbSubmission.JurisdictionalData.Include(x => x.Submission).AsQueryable();

			// Submission Filters
			var startSubmitDate = (filters.StartSubmitDate == null) ? (DateTime)SqlDateTime.MinValue : Convert.ToDateTime(filters.StartSubmitDate).Date;
			startSubmitDate = (filters.RollingSubmitDateDays == null) ? startSubmitDate : DateTime.Today.AddDays((int)filters.RollingSubmitDateDays * -1);
			var endSubmitDate = (filters.EndSubmitDate == null) ? (DateTime)SqlDateTime.MaxValue : Convert.ToDateTime(filters.EndSubmitDate).Date.AddDays(1).AddTicks(-1);
			endSubmitDate = (filters.RollingSubmitDateDays == null) ? endSubmitDate : _endOfDayToday;
			var startReceiveDate = (filters.StartReceiveDate == null) ? (DateTime)SqlDateTime.MinValue : Convert.ToDateTime(filters.StartReceiveDate).Date;
			startReceiveDate = (filters.RollingReceiveDateDays == null) ? startReceiveDate : DateTime.Today.AddDays((int)filters.RollingReceiveDateDays * -1);
			var endReceiveDate = (filters.EndReceiveDate == null) ? (DateTime)SqlDateTime.MaxValue : Convert.ToDateTime(filters.EndReceiveDate).Date.AddDays(1).AddTicks(-1);
			endReceiveDate = (filters.RollingReceiveDateDays == null) ? endReceiveDate : _endOfDayToday;

			query = query.Where(x => x.Submission.SubmitDate >= startSubmitDate && x.Submission.SubmitDate <= endSubmitDate);
			query = query.Where(x => x.Submission.ReceiveDate >= startReceiveDate && x.Submission.ReceiveDate <= endReceiveDate);

			if (!string.IsNullOrEmpty(filters.ChipType))
			{
				query = query.Where(x => x.Submission.ChipType == filters.ChipType);
			}

			if (filters.ConditionalRevoke != null)
			{
				query = query.Where(x => x.Submission.ConditionalRevoke == filters.ConditionalRevoke);
			}

			if (!string.IsNullOrEmpty(filters.DateCode))
			{
				query = query.Where(x => x.Submission.DateCode.StartsWith(filters.DateCode));
			}

			if (!string.IsNullOrEmpty(filters.IdNumber))
			{
				query = query.Where(x => x.Submission.IdNumber.StartsWith(filters.IdNumber));
			}

			if (!string.IsNullOrEmpty(filters.FileNumber))
			{
				query = query.Where(x => x.Submission.FileNumber.StartsWith(filters.FileNumber));
			}

			if (!string.IsNullOrEmpty(filters.Function))
			{
				query = query.Where(x => x.Submission.Function.StartsWith(filters.Function));
			}

			if (!string.IsNullOrEmpty(filters.GameName))
			{
				query = query.Where(x => x.Submission.GameName.StartsWith(filters.GameName));
			}

			if (filters.JurisdictionIds != null)
			{
				query = query.Where(x => filters.JurisdictionIds.Contains(x.JurisdictionId));
			}

			if (filters.ManufacturerCodes != null)
			{
				query = query.Where(x => filters.ManufacturerCodes.Contains(x.Submission.ManufacturerCode));
			}

			if (!string.IsNullOrEmpty(filters.Position))
			{
				query = query.Where(x => x.Submission.Position.StartsWith(filters.Position));
			}

			if (!string.IsNullOrEmpty(filters.ReplaceWith))
			{
				query = query.Where(x => x.Submission.ReplaceWith.StartsWith(filters.ReplaceWith));
			}

			if (filters.Statuses != null)
			{
				query = query.Where(x => filters.Statuses.Contains(x.Status));
			}

			if (filters.SubmissionId != null)
			{
				query = query.Where(x => x.SubmissionId == filters.SubmissionId);
			}

			if (filters.TypeId != null)
			{
				var type = _dbSubmission.tbl_lst_SubmissionType.Where(x => x.Id == filters.TypeId).Select(x => x.Code).Single();
				query = query.Where(x => x.Submission.Type == type);
			}

			if (!string.IsNullOrEmpty(filters.Version))
			{
				query = query.Where(x => x.Submission.Version.StartsWith(filters.Version));
			}

			if (!string.IsNullOrEmpty(filters.Year))
			{
				var year = filters.Year.Trim().Right(2);
				query = query.Where(x => x.Submission.Year == year);
			}

			// Jurisdiction Filters
			if (filters.StartAuthorizeDate != null || filters.EndAuthorizeDate != null || filters.RollingAuthorizeDateDays != null)
			{
				var startAuthorizeDate = (filters.StartAuthorizeDate == null) ? (DateTime)SqlDateTime.MinValue : Convert.ToDateTime(filters.StartAuthorizeDate).Date;
				startAuthorizeDate = (filters.RollingAuthorizeDateDays == null) ? startAuthorizeDate : DateTime.Today.AddDays((int)filters.RollingAuthorizeDateDays * -1);
				var endAuthorizeDate = (filters.EndAuthorizeDate == null) ? (DateTime)SqlDateTime.MaxValue : Convert.ToDateTime(filters.EndAuthorizeDate).Date.AddDays(1).AddTicks(-1);
				endAuthorizeDate = (filters.RollingAuthorizeDateDays == null) ? endAuthorizeDate : _endOfDayToday;

				query = query.Where(x => x.QuoteAuthDate.AuthorizeDate >= startAuthorizeDate && x.QuoteAuthDate.AuthorizeDate <= endAuthorizeDate);
			}

			if (filters.StartCloseDate != null || filters.EndCloseDate != null || filters.RollingCloseDateDays != null)
			{
				var startCloseDate = (filters.StartCloseDate == null) ? (DateTime)SqlDateTime.MinValue : Convert.ToDateTime(filters.StartCloseDate).Date;
				startCloseDate = (filters.RollingCloseDateDays == null) ? startCloseDate : DateTime.Today.AddDays((int)filters.RollingCloseDateDays * -1);
				var endCloseDate = (filters.EndCloseDate == null) ? (DateTime)SqlDateTime.MaxValue : Convert.ToDateTime(filters.EndCloseDate).Date.AddDays(1).AddTicks(-1);
				endCloseDate = (filters.RollingCloseDateDays == null) ? endCloseDate : _endOfDayToday;

				query = query.Where(x => x.CloseDate >= startCloseDate && x.CloseDate <= endCloseDate);
			}

			if (filters.StartDraftDate != null || filters.EndDraftDate != null || filters.RollingDraftDateDays != null)
			{
				var startDraftDate = (filters.StartDraftDate == null) ? (DateTime)SqlDateTime.MinValue : Convert.ToDateTime(filters.StartDraftDate).Date;
				startDraftDate = (filters.RollingDraftDateDays == null) ? startDraftDate : DateTime.Today.AddDays((int)filters.RollingDraftDateDays * -1);
				var endDraftDate = (filters.EndDraftDate == null) ? (DateTime)SqlDateTime.MaxValue : Convert.ToDateTime(filters.EndDraftDate).Date.AddDays(1).AddTicks(-1);
				endDraftDate = (filters.RollingDraftDateDays == null) ? endDraftDate : _endOfDayToday;

				query = query.Where(x => x.DraftDate >= startDraftDate && x.DraftDate <= endDraftDate);
			}

			if (filters.StartObsoleteDate != null || filters.EndObsoleteDate != null || filters.RollingObsoleteDateDays != null)
			{
				var startObsoleteDate = (filters.StartObsoleteDate == null) ? (DateTime)SqlDateTime.MinValue : Convert.ToDateTime(filters.StartObsoleteDate).Date;
				startObsoleteDate = (filters.RollingObsoleteDateDays == null) ? startObsoleteDate : DateTime.Today.AddDays((int)filters.RollingObsoleteDateDays * -1);
				var endObsoleteDate = (filters.EndObsoleteDate == null) ? (DateTime)SqlDateTime.MaxValue : Convert.ToDateTime(filters.EndObsoleteDate).Date.AddDays(1).AddTicks(-1);
				endObsoleteDate = (filters.RollingObsoleteDateDays == null) ? endObsoleteDate : _endOfDayToday;

				query = query.Where(x => x.ObsoleteDate >= startObsoleteDate && x.ObsoleteDate <= endObsoleteDate);
			}

			if (filters.StartQuoteDate != null || filters.EndQuoteDate != null || filters.RollingQuoteDateDays != null)
			{
				var startQuoteDate = (filters.StartQuoteDate == null) ? (DateTime)SqlDateTime.MinValue : Convert.ToDateTime(filters.StartQuoteDate).Date;
				startQuoteDate = (filters.RollingQuoteDateDays == null) ? startQuoteDate : DateTime.Today.AddDays((int)filters.RollingQuoteDateDays * -1);
				var endQuoteDate = (filters.EndQuoteDate == null) ? (DateTime)SqlDateTime.MaxValue : Convert.ToDateTime(filters.EndQuoteDate).Date.AddDays(1).AddTicks(-1);
				endQuoteDate = (filters.RollingQuoteDateDays == null) ? endQuoteDate : _endOfDayToday;

				query = query.Where(x => x.QuoteAuthDate.QuoteDate >= startQuoteDate && x.QuoteAuthDate.QuoteDate <= endQuoteDate);
			}

			if (filters.StartRevokeDate != null || filters.EndRevokeDate != null || filters.RollingRevokeDateDays != null)
			{
				var startRevokeDate = (filters.StartRevokeDate == null) ? (DateTime)SqlDateTime.MinValue : Convert.ToDateTime(filters.StartRevokeDate).Date;
				startRevokeDate = (filters.RollingRevokeDateDays == null) ? startRevokeDate : DateTime.Today.AddDays((int)filters.RollingRevokeDateDays * -1);
				var endRevokeDate = (filters.EndRevokeDate == null) ? (DateTime)SqlDateTime.MaxValue : Convert.ToDateTime(filters.EndRevokeDate).Date.AddDays(1).AddTicks(-1);
				endRevokeDate = (filters.RollingRevokeDateDays == null) ? endRevokeDate : _endOfDayToday;

				query = query.Where(x => x.RevokeDate >= startRevokeDate && x.RevokeDate <= endRevokeDate);
			}

			if (filters.StartUpdateDate != null || filters.EndUpdateDate != null || filters.RollingUpdateDateDays != null)
			{
				var startUpdateDate = (filters.StartUpdateDate == null) ? (DateTime)SqlDateTime.MinValue : Convert.ToDateTime(filters.StartUpdateDate).Date;
				startUpdateDate = (filters.RollingUpdateDateDays == null) ? startUpdateDate : DateTime.Today.AddDays((int)filters.RollingUpdateDateDays * -1);
				var endUpdateDate = (filters.EndUpdateDate == null) ? (DateTime)SqlDateTime.MaxValue : Convert.ToDateTime(filters.EndUpdateDate).Date.AddDays(1).AddTicks(-1);
				endUpdateDate = (filters.RollingUpdateDateDays == null) ? endUpdateDate : _endOfDayToday;

				query = query.Where(x => x.UpdateDate >= startUpdateDate && x.UpdateDate <= endUpdateDate);
			}

			return query;
		}

		public IQueryable<tbl_LetterBook> LetterBookSearch(LetterBookSearch filters)
		{
			var query = _dbSubmission.tbl_LetterBook.AsNoTracking().AsQueryable();

			if (filters.DepartmentId != null)
			{
				query = query.Where(x => x.DepartmentId == filters.DepartmentId);
			}

			if (filters.BillingUserId != null)
			{
				query = query.Where(x => x.BillingUserId == filters.BillingUserId);
			}

			if (filters.BatchNumber != null)
			{
				query = query.Where(x => x.BatchNumber == filters.BatchNumber);
			}

			if (filters.BatchStatus.Length > 0)
			{
				query = query.Where(x => filters.BatchStatus.Contains(x.ACTStatus));
			}

			if (!string.IsNullOrWhiteSpace(filters.FileNumber))
			{
				query = query.Where(x => x.DocumentVersion.DocumentVersionLinks.FirstOrDefault().Project.FileNumber.Contains(filters.FileNumber));
			}

			if (filters.ReportStartDate != null)
			{
				var startDate = filters.ReportStartDate.Value.Date;
				var endDate = filters.ReportEndDate != null ? filters.ReportEndDate.Value.Date : startDate.Date.AddDays(1).AddTicks(-1);
				query = query.Where(x => x.ReportDate >= startDate && x.ReportDate <= endDate);
			}

			if (filters.ShowBillable && filters.ShowNonBillable)
			{
				//need all
			}
			else if (filters.ShowBillable)
			{
				query = query.Where(x => x.Billable);
			}
			else if (filters.ShowNonBillable)
			{
				query = query.Where(x => !x.Billable);
			}

			if (filters.OnlyMajorRevisions)
			{
				query = query.Where(x => x.DocumentVersion.Version != 1 && x.DocumentVersion.Version != 0.01);
			}

			if (filters.LetterTypeIds.Length > 0)
			{
				query = query.Where(x => filters.LetterTypeIds.Contains(x.DocumentVersion.Document.DocumentTypeId));
			}

			if (filters.LaboratoryId != null)
			{
				query = query.Where(book => book.LaboratoryId == filters.LaboratoryId);
			}

			if (filters.CurrencyIds?.Any() == true)
			{
				var currencies = filters.CurrencyIds.Select(curId => ((Currency)curId).ToString()).ToList();
				query = query.Where(book => currencies.Contains(book.DocumentVersion.DocumentVersionLinks.FirstOrDefault().JurisdictionalData.Currency.Code));
			}
			return query;
		}

		public IQueryable<tbl_lst_LetterContacts> LetterContactsSearch(LetterContactsSearch filters)
		{
			var query = _dbSubmission.tbl_lst_LetterContacts.AsQueryable();

			if (filters.ManufacturerCodes != null && filters.ManufacturerCodes.Count > 0)
			{
				query = query.Where(lc => lc.Manufacturer.Select(lcmj => lcmj.ManufacturerId).Any(m => filters.ManufacturerCodes.Contains(m)));
			}

			if (filters.JurisdictionCodes != null && filters.JurisdictionCodes.Count > 0)
			{
				query = query.Where(lc => lc.Jurisdiction.Select(lcjj => lcjj.JurisdictionId).Any(m => filters.JurisdictionCodes.Contains(m)));
			}
			if (filters.ContactTypes != null && filters.ContactTypes.Count > 0)
			{
				query = query.Where(lc => lc.ContactTypes.Select(lcct => lcct.ContactTypeId).Any(m => filters.ContactTypes.Contains(m)));
			}
			if (!string.IsNullOrWhiteSpace(filters.SearchTerm))
			{
				var searchTerm = filters.SearchTerm.ToLower();
				query = query.Where(lc => lc.Name.ToLower().Contains(searchTerm) || lc.AddressLine1.ToLower().Contains(searchTerm) || lc.AddressLine2.ToLower().Contains(searchTerm) || lc.AddressLine3.ToLower().Contains(searchTerm) || lc.AddressLine4.ToLower().Contains(searchTerm) || lc.AddressLine5.ToLower().Contains(searchTerm));
			}

			return query;
		}

		public IQueryable<ManufacturerGroups> ManufacturerGroupSearch(ManufacturerGroupSearch filters)
		{
			var query = _dbSubmission.ManufacturerGroups.AsQueryable();

			if (filters.ManufacturerGroupCategoryIds != null && filters.ManufacturerGroupCategoryIds.Count > 0)
			{
				query = query.Where(mg => filters.ManufacturerGroupCategoryIds.Contains(mg.ManufacturerGroupCategory.Id));
			}

			if (filters.ManufacturerCodes != null && filters.ManufacturerCodes.Count > 0)
			{
				query = query.Where(mg => mg.ManufacturerLinks.Select(fmmg => fmmg.ManufacturerCode).Any(mc => filters.ManufacturerCodes.Contains(mc)));
			}

			return query;
		}

		public IQueryable<trMathComplete> MathCompleteSearch(MathSearch filters)
		{
			var query = _dbSubmission.trMathCompletes.AsQueryable();

			if (!string.IsNullOrEmpty(filters.Status))
			{
				query = query.Where(x => x.Status == filters.Status);
			}

			if (filters.LaboratoryId != 0 && filters.LaboratoryId != null)
			{
				query = query.Where(x => x.Project.LabId == filters.LaboratoryId).AsQueryable();
			}
			if (filters.LaboratoryIds != null && filters.LaboratoryIds.Count > 0)
			{
				query = query.Where(x => filters.LaboratoryIds.Contains(x.Project.LabId.ToString()));
			}

			if (!string.IsNullOrEmpty(filters.FileNumber))
			{
				query = query.Where(x => x.Project.FileNumber.StartsWith(filters.FileNumber)).AsQueryable();
			}

			if (!string.IsNullOrEmpty(filters.ManufacturerShort))
			{
				query = query.Where(x => x.Project.FileNumber.Contains(filters.ManufacturerShort)).AsQueryable();
			}
			if (filters.MAManagerId != 0 && filters.MAManagerId != null)
			{
				query = query.Where(x => x.Project.MAManagerId == filters.MAManagerId).AsQueryable();
			}
			if (filters.ManufacturerGroupIds != null && filters.ManufacturerGroupIds.Count > 0)
			{
				var manufs = _dbSubmission.ManufacturerGroups.Where(x => filters.ManufacturerGroupIds.Contains(x.Id))
					.SelectMany(x => x.ManufacturerLinks.Select(y => y.ManufacturerCode)).ToList();

				if (filters.ExcludeMathQueueGroups)
				{
					query = query.Where(x => !manufs.Any(y => x.Project.FileNumber.Contains(y)));
				}
				else
				{
					query = query.Where(x => manufs.Any(y => x.Project.FileNumber.Contains(y)));
				}
			}

			return query;
		}

		public IQueryable<trMathSubmit> MathReviewSearch(MathSearch filters)
		{
			var query = _dbSubmission.trMathSubmits.AsQueryable();
			if (!string.IsNullOrEmpty(filters.Status))
			{
				query = query.Where(x => x.Status == filters.Status).AsQueryable();
			}

			if (filters.LaboratoryId != 0 && filters.LaboratoryId != null)
			{
				query = query.Where(x => x.Project.LabId == filters.LaboratoryId).AsQueryable();
			}
			if (filters.LaboratoryIds != null && filters.LaboratoryIds.Count > 0)
			{
				query = query.Where(x => filters.LaboratoryIds.Contains(x.Project.LabId.ToString()));
			}

			if (!string.IsNullOrEmpty(filters.FileNumber))
			{
				query = query.Where(x => x.Project.FileNumber.StartsWith(filters.FileNumber)).AsQueryable();
			}

			if (!string.IsNullOrEmpty(filters.ManufacturerShort))
			{
				query = query.Where(x => x.Project.FileNumber.Contains(filters.ManufacturerShort)).AsQueryable();
			}

			if (filters.MAManagerId != 0 && filters.MAManagerId != null)
			{
				query = query.Where(x => x.Project.MAManagerId == filters.MAManagerId).AsQueryable();
			}

			if (filters.ManufacturerGroupIds != null && filters.ManufacturerGroupIds.Count > 0)
			{
				var manufs = _dbSubmission.ManufacturerGroups.Where(x => filters.ManufacturerGroupIds.Contains(x.Id))
					.SelectMany(x => x.ManufacturerLinks.Select(y => y.ManufacturerCode)).ToList();

				if (filters.ExcludeMathQueueGroups)
				{
					query = query.Where(x => !manufs.Any(y => x.Project.FileNumber.Contains(y)));
				}
				else
				{
					query = query.Where(x => manufs.Any(y => x.Project.FileNumber.Contains(y)));
				}
			}

			return query;
		}

		public IQueryable<vw_ProjectNotes> ProjectAllNoteSearch(ProjectNoteSearch filters)
		{
			var query = _dbSubmission.vw_ProjectNotes.AsQueryable();

			if (filters.ProjectId != null)
			{
				query = query.Where(x => x.Id == filters.ProjectId);
			}

			return query;
		}

		public IQueryable<AssociatedProject> ProjectAssociation(int projectID)
		{
			var project = _dbSubmission.trProjects.Where(x => x.Id == projectID).Select(x => new { IsTransfer = x.IsTransfer, FileNumber = x.FileNumber }).FirstOrDefault();
			var query = (from p in _dbSubmission.trProjects
						 where (p.IsTransfer == !(project.IsTransfer) && p.FileNumber == project.FileNumber)
						 select new AssociatedProject { Id = p.Id, Name = p.ProjectName, IsTransfer = p.IsTransfer }
						).AsQueryable();

			return query;
		}

		public IQueryable<trHistory> ProjectHistoryLogSearch(ProjectHistoryLogSearch filters)
		{
			var query = _dbSubmission.trHistories.AsQueryable();
			if (filters.ProjectId != null)
			{
				query = query.Where(x => x.ProjectId == filters.ProjectId);
			}
			return query;
		}

		public IQueryable<trProjectJurisdictionalData> ProjectJurisdictionItemSearch(ProjectJurisdictionItemSearch filters)
		{
			var query = _dbSubmission.trProjectJurisdictionalData.AsQueryable();

			if (filters.ProjectIds.Count > 0)
			{
				query = query.Where(x => filters.ProjectIds.Contains(x.ProjectId));
			}
			return query;
		}

		public IQueryable<ProjectMetrics> ProjectMetricsSearch(ProjectMetricsSearch filters)
		{
			var query = _dbSubmission.Database.SqlQuery<ProjectMetrics>("exec getProjectMetrics " + filters.ProjectId + "," + filters.DepartmentId).AsQueryable();
			return query;
		}

		public IQueryable<trProjectNotes> ProjectNoteSearch(ProjectNoteSearch filters)
		{
			var query = _dbSubmission.trProjectNotes.AsQueryable();

			if (filters.ProjectId != null)
			{
				query = query.Where(x => x.ProjectId == filters.ProjectId);
			}

			return query;
		}

		public IQueryable<trOH_History> ProjectOHHistory(int projectId)
		{
			var query = _dbSubmission.trOH_Histories.Where(t => t.ProjectId == projectId).AsQueryable();
			return query;
		}

		public IQueryable<Resubmission> ProjectResubmission(int projectID)
		{
			var project = _dbSubmission.trProjects.Where(x => x.Id == projectID).FirstOrDefault();
			var query = (from s in _dbSubmission.Submissions
						 where (s.FileNumber == project.ProjectName && (s.IsResubmission == true || s.IsContinuation == true || s.IsPreCertification == true))
						 group s by s.FileNumber into fileGroup
						 select new Resubmission
						 {
							 MasterFile = fileGroup.FirstOrDefault().MasterFileNumber,
							 IsResubmission = (fileGroup.Sum(x => (x.IsResubmission == true) ? 1 : 0) > 0) ? true : false,
							 IsContinuation = (fileGroup.Sum(x => ((bool)x.IsContinuation == true) ? 1 : 0) > 0) ? true : false,
							 IsPreCertification = (fileGroup.Sum(x => ((bool)x.IsPreCertification == true) ? 1 : 0) > 0) ? true : false,
						 }).AsQueryable();

			return query;
		}

		public IQueryable<tbl_Reviews> ProjectReviewSearch(ProjectReviewSearch filters)
		{
			var query = _dbSubmission.tbl_Reviews.Include(x => x.Details).AsQueryable();
			if (filters.DepartmentId != null)
			{
				query = query.Where(x => x.DepartmentId == filters.DepartmentId);
			}
			if (!filters.StatusIds.IsNullOrEmpty())
			{
				query = query.Where(x => filters.StatusIds.Contains(x.Status));
			}
			if (filters.UserId.Count != 0)
			{
				query = query.Where(x => x.ReviewUsers.Any(y => filters.UserId.Contains(y.UserId)) || (filters.UserId.Contains((int)x.Project.ENManagerId) && x.DepartmentId == 1) || (filters.UserId.Contains((int)x.Project.MAManagerId) && x.DepartmentId == 2));
			}
			if (!string.IsNullOrWhiteSpace(filters.SearchTerm))
			{
				var searchTerm = filters.SearchTerm.ToLower();
				query = query.Where(review => review.Details.Any(detail => detail.Description.ToLower().Contains(searchTerm)));
			}
			if (filters.ProjectId != null)
			{
				query = query.Where(x => x.ProjectId == filters.ProjectId);
			}

			return query;
		}

		public IQueryable<vw_trProject> ProjectSearch(ProjectSearch filters)
		{
			var query = _dbSubmission.vw_Projects.AsQueryable();

			if (!string.IsNullOrEmpty(filters.SingleInputSearch))
			{
				try
				{
					if (filters.SingleInputSearch.Count(x => x == '-') < 4)
					{
						throw new Exception("Not a valid File Number so we can't search with Manufacturer Code");
					}

					var validFileNumber = new FileNumber(filters.SingleInputSearch);
					var fileNumber = validFileNumber.ToString();
					var manufacturerCode = validFileNumber.ManufacturerCode;

					query = query.Where(x => x.Manufacturer == manufacturerCode);
					query = query.Where(x => x.ProjectName.StartsWith(fileNumber));
				}
				catch
				{
					if (filters.SingleInputSearchProjectOnly)
					{
						query = query.Where(x =>
							x.ProjectName.StartsWith(filters.SingleInputSearch));
					}
					else
					{
						query = query.Where(x =>
							x.ProjectName.StartsWith(filters.SingleInputSearch) ||
							x.ProjectJurisdictionalData.Any(y => y.JurisdictionalData.Submission.GameName.Contains(filters.SingleInputSearch)) ||
							x.ProjectJurisdictionalData.Any(y => y.JurisdictionalData.Submission.IdNumber.Contains(filters.SingleInputSearch)));
					}
				}

				return query;
			}

			if (!string.IsNullOrEmpty(filters.ProjectName))
			{
				query = query.Where(x => x.ProjectName.StartsWith(filters.ProjectName));
			}

			if (!string.IsNullOrEmpty(filters.FileNumber))
			{
				query = query.Where(x => x.FileNumber.StartsWith(filters.FileNumber));
			}

			if (filters.TestLabIds != null && filters.TestLabIds.Count > 0)
			{
				query = query.Where(x => filters.TestLabIds.Contains(x.TestLabId.ToString()));
			}
			else if (filters.TestLabId != null)
			{
				query = query.Where(x => x.TestLabId == filters.TestLabId);
			}

			if (filters.LaboratoryIds != null && filters.LaboratoryIds.Count > 0)
			{
				query = query.Where(x => filters.LaboratoryIds.Contains(x.LaboratoryId.ToString()));
			}
			else if (filters.LaboratoryId != null)
			{
				query = query.Where(x => x.LaboratoryId == filters.LaboratoryId);
			}

			if (filters.ManagerIds != null && filters.ManagerIds.Count > 0)
			{
				var iManagerIds = filters.ManagerIds.Select(s => int.Parse(s)).ToList();
				query = query.Where(x => iManagerIds.Contains((int)x.ManagerId) || iManagerIds.Contains((int)x.MAManagerId));
			}
			else if (filters.ManagerId != null)
			{
				query = query.Where(x => x.ManagerId == filters.ManagerId || x.MAManagerId == filters.ManagerId);
			}

			if (filters.QASupervisorId != null)
			{
				query = query.Where(x => x.QASupervisorId == filters.QASupervisorId);
			}

			if (filters.QALaboratoryIds != null && filters.QALaboratoryIds.Count > 0)
			{
				query = query.Where(x => filters.QALaboratoryIds.Contains(x.QALocationId.ToString()));
			}
			else if (filters.QALocationId != null)
			{
				if (filters.QALocationId > 0)
				{
					query = query.Where(x => x.QAOfficeId == filters.QALocationId);
				}
				else
				{
					query = query.Where(x => x.QAOfficeId == null);
				}
			}

			if (filters.ReviewerIds != null && filters.ReviewerIds.Count > 0)
			{
				var iReviewerIds = filters.ReviewerIds.Select(s => int.Parse(s)).ToList();
				query = query.Where(x => iReviewerIds.Contains((int)x.ReviewerId));
			}
			else if (filters.ReviewerId != null)
			{
				query = query.Where(x => x.ReviewerId == filters.ReviewerId);
			}

			if (filters.DevelopmentRepIds != null && filters.DevelopmentRepIds.Count > 0)
			{
				query = query.Where(x => filters.DevelopmentRepIds.Contains(x.DevRep.ToString()));
			}

			if (filters.SeniorEngineerIds != null && filters.SeniorEngineerIds.Count > 0)
			{
				query = query.Where(x => filters.SeniorEngineerIds.Contains(x.SeniorEngineerId.ToString()));
			}
			else if (filters.SeniorEngineerId != null)
			{
				query = query.Where(x => x.SeniorEngineerId == filters.SeniorEngineerId);
			}

			if (filters.MAProjectLeadIds != null && filters.MAProjectLeadIds.Count > 0)
			{
				query = query.Where(x => filters.MAProjectLeadIds.Contains(x.MAProjectLeadId.ToString()));
			}
			else if (filters.MAProjectLeadId != null)
			{
				query = query.Where(x => x.MAProjectLeadId == filters.MAProjectLeadId);
			}

			if (filters.IsTransfer == ProjectType.Projects)
			{
				query = query.Where(x => x.IsTransfer == false);
			}
			else if (filters.IsTransfer == ProjectType.Transfers)
			{
				query = query.Where(x => x.IsTransfer == true);
			}

			if (filters.IsRush == true)
			{
				query = query.Where(x => x.IsRush == 1);
			}

			if (filters.IsDeclined == true)
			{
				query = query.Where(x => x.IsDeclined == true);
			}
			else
			{
				query = query.Where(x => x.IsDeclined == false);
			}

			if (!string.IsNullOrEmpty(filters.MainJurisdiction))
			{
				query = query.Where(x => x.Jurisdiction == filters.MainJurisdiction);
			}

			if (filters.ManufacturerShorts.Count > 0 && filters.ManufacturerShorts != null)
			{
				query = query.Where(x => filters.ManufacturerShorts.Contains(x.Manufacturer));
			}
			else if (!string.IsNullOrEmpty(filters.ManufacturerShort))
			{
				query = query.Where(x => x.Manufacturer == filters.ManufacturerShort);
			}

			if (filters.SubmissionType != null)
			{
				query = query.Where(x => x.SubmissionType == filters.SubmissionType);
			}

			if (filters.ReceivedFrom != null)
			{
				query = query.Where(x => x.ReceiveDate >= filters.ReceivedFrom);
			}

			if (filters.ReceivedTo != null)
			{
				query = query.Where(x => x.ReceiveDate <= filters.ReceivedTo);
			}

			if (filters.TargetDateFrom != null)
			{
				query = query.Where(x => x.TargetDate >= filters.TargetDateFrom);
			}

			if (filters.TargetDateTo != null)
			{
				query = query.Where(x => x.TargetDate <= filters.TargetDateTo);
			}

			if (filters.QAAcceptedFrom != null)
			{
				query = query.Where(x => x.QAAcceptDate >= filters.QAAcceptedFrom);
			}

			if (filters.QAAcceptedTo != null)
			{
				query = query.Where(x => x.QAAcceptDate <= filters.QAAcceptedTo);
			}

			if (filters.EngCompletedFrom != null)
			{
				query = query.Where(x => x.EngCompleteDate >= filters.EngCompletedFrom);
			}

			if (filters.EngCompletedTo != null)
			{
				query = query.Where(x => x.EngCompleteDate <= filters.EngCompletedTo);
			}

			if (!string.IsNullOrEmpty(filters.ClientNumber))
			{
				var clientNumbers = filters.ClientNumber.Split(new[] { ',', ';' }).Select(x => x.Trim()).ToList();
				query = query.Where(x => x.ProjectJurisdictionalData.Any(y => clientNumbers.Contains(y.JurisdictionalData.ClientNumber)));
			}

			if (!string.IsNullOrEmpty(filters.BundleStatus))
			{
				if (filters.BundleStatus.IndexOf(QAProjectStatusFilter.OPEN.ToString()) >= 0)
				{
					var openQAStatuses = ComUtil.OpenQABundleStatuses().Select(x => x.ToString()).ToList();
					query = query.Where(x => openQAStatuses.Contains(x.BundleStatus));
				}
				else if (filters.BundleStatus.IndexOf(QAProjectStatusFilter.ENG.ToString()) >= 0)
				{
					query = query.Where(x => x.BundleStatus == "" && x.Status != ProjectStatus.DN.ToString() && x.Status != ProjectStatus.QA.ToString() && x.OnHold != true);
				}
				else
				{
					var allStatus = filters.BundleStatus.Split(',').ToList();
					var ohexists = allStatus.Any(x => x == QAProjectStatusFilter.OH.ToString());
					if (ohexists)
					{
						query = query.Where(x => allStatus.Contains(x.BundleStatus) || (x.OnHold == true));
					}
					else
					{
						if (filters.BundleStatus.IndexOf(QAProjectStatusFilter.DN.ToString()) >= 0)
						{
							if (filters.QALWCompletedFrom != null || filters.QALWCompletedTo != null)
							{
								if (filters.QALWCompletedFrom == null)
								{
									query = query.Where(x => allStatus.Contains(x.BundleStatus) && (x.BundleStatus == BundleStatus.DN.ToString() && x.QALWCompletedTime <= filters.QALWCompletedTo) && x.OnHold != true);
								}
								else if (filters.QALWCompletedTo == null)
								{
									query = query.Where(x => allStatus.Contains(x.BundleStatus) && (x.BundleStatus == BundleStatus.DN.ToString() && x.QALWCompletedTime >= filters.QALWCompletedFrom) && x.OnHold != true);
								}
								else
								{
									query = query.Where(x => allStatus.Contains(x.BundleStatus) && (x.BundleStatus == BundleStatus.DN.ToString() && x.QALWCompletedTime >= filters.QALWCompletedFrom && x.QALWCompletedTime <= filters.QALWCompletedTo) && x.OnHold != true);
								}
							}
							else
							{
								query = query.Where(x => allStatus.Contains(x.BundleStatus) && (x.BundleStatus == BundleStatus.DN.ToString()) && x.OnHold != true);
							}
						}
						else
						{
							query = query.Where(x => allStatus.Contains(x.BundleStatus) && x.OnHold != true);
						}
					}
				}
			}

			//to allow multi select or may be have another list<string> multistatus property
			if (!string.IsNullOrEmpty(filters.Status))
			{
				if (filters.Status.IndexOf(ENProjectStatusFilter.OPEN.ToString()) >= 0)
				{
					var openStatuses = ComUtil.OpenProjectStatuses().Select(x => x.ToString()).ToList();
					query = query.Where(x => (openStatuses.Contains(x.Status) && x.Status != ProjectStatus.UU.ToString()) || x.OnHold == true);
				}
				else
				{
					var allStatus = filters.Status.Split(',').ToList();
					var OHFilter = allStatus.Any(x => x == ProjectStatus.OH.ToString());
					var DCRQFilter = allStatus.Any(x => x == ENProjectStatusFilter.DCRQ.ToString());
					var DCOHFilter = allStatus.Any(x => x == ENProjectStatusFilter.DCOH.ToString());

					if (filters.Status.IndexOf(ENProjectStatusFilter.DN.ToString()) >= 0)
					{
						if (filters.CompleteFrom != null || filters.CompleteTo != null)
						{
							if (filters.CompleteFrom == null)
							{
								query = query.Where(x => allStatus.Contains(x.Status) && (x.Status == ProjectStatus.DN.ToString() && x.QACompleteDate <= filters.CompleteTo) && x.OnHold != true);
							}
							else if (filters.CompleteTo == null)
							{
								query = query.Where(x => allStatus.Contains(x.Status) && (x.Status == ProjectStatus.DN.ToString() && x.QACompleteDate >= filters.CompleteFrom) && x.OnHold != true);
							}
							else
							{
								query = query.Where(x => allStatus.Contains(x.Status) && (x.Status == ProjectStatus.DN.ToString() && x.QACompleteDate >= filters.CompleteFrom && x.QACompleteDate <= filters.CompleteTo) && x.OnHold != true);
							}
						}
						else
						{
							query = query.Where(x => allStatus.Contains(x.Status) && (x.Status == ProjectStatus.DN.ToString()) && x.OnHold != true);
						}
					}
					else if (filters.Status.IndexOf(ENProjectStatusFilter.UU.ToString()) >= 0)
					{
						query = query.Where(x => allStatus.Contains(x.Status)); //Accept Queue needs to include OnHold Project
					}
					else
					{
						query = query.Where(x => (allStatus.Contains(x.Status) && x.OnHold == false) ||
							(DCRQFilter && x.OnHold == false && x.Status == ProjectStatus.DC.ToString()) ||
							(DCOHFilter && x.OnHold == true && x.Status == ProjectStatus.DC.ToString()) ||
							(OHFilter && x.OnHold == true && x.Status != ProjectStatus.DC.ToString()));
					}
				}
			}

			if (filters.UserFavoriteId != null && filters.UserFavoriteId != 0)
			{
				query = query.Join(_dbSubmission.trFavoriteFiles, vp => vp.Id, tp => tp.ProjectId, (vp, tp) => new { vp, tp }).Where(x => x.tp.UserId == filters.UserFavoriteId).Select(x => x.vp).Distinct().AsQueryable();
			}

			if (filters.EngineerId != null)
			{
				query = query.Join(_dbSubmission.trTasks, vp => vp.Id, tp => tp.ProjectId, (vp, tp) => new { vp, tp }).Where(x => x.tp.UserId == filters.EngineerId).Select(x => x.vp).Distinct().AsQueryable();
			}

			if (filters.EngineerIds != null && filters.EngineerIds.Count > 0)
			{
				query = query.Join(_dbSubmission.trTasks, vp => vp.Id, tp => tp.ProjectId, (vp, tp) => new { vp, tp }).Where(x => filters.EngineerIds.Contains(x.tp.UserId.ToString())).Select(x => x.vp).Distinct().AsQueryable();
			}

			if (!string.IsNullOrEmpty(filters.Jurisdiction))
			{
				query = query.Where(x => x.ProjectJurisdictionalData.Any(y => y.JurisdictionalData.JurisdictionId == filters.Jurisdiction));
			}
			//Engineering Tasks

			if (filters.CheckHasEngTask)
			{
				query = query.Where(x => x.ProjectTasks.Count == 0);
			}
			//QA Task Target
			if (filters.QATaskTarget != null)
			{
				query = query.Join(_dbSubmission.QABundles_Task, vp => vp.Id, tp => tp.BundleID, (vp, tp) => new { vp, tp }).Where(x => x.tp.TargetDate == filters.QATaskTarget).Select(x => x.vp).Distinct().AsQueryable();
			}

			if (filters.ExcludeFileTypes.Count > 0)
			{
				var SubmissionTypeIds = _dbSubmission.tbl_lst_SubmissionType.Where(x => filters.ExcludeFileTypes.Contains(x.Code)).Select(x => x.Id).ToList();
				query = query.Where(x => !SubmissionTypeIds.Contains((int)x.SubmissionType));
			}

			return query;
		}

		public IQueryable<trTargetDate> ProjectTargetDateHistory(int projectId)
		{
			var query = _dbSubmission.trTargetDate.Where(t => t.ProjectId == projectId && t.JurisdictionalDataId == null).AsQueryable();
			return query;
		}

		public IQueryable<UnassignedTaskHoursDetail> ProjectUnassignedTaskHours(int projectId)
		{
			var query = "exec GetDynamicsData '" + projectId + "'";
			var results = _dbSubmission.Database.SqlQuery<UnassignedTaskHoursDetail>(query).AsQueryable();
			return results;
		}

		public IQueryable<QABundles_History> QABundleHistoryLogSearch(ProjectHistoryLogSearch filters)
		{
			var query = _dbSubmission.QABundles_History.AsQueryable();
			if (filters.ProjectId != null)
			{
				query = query.Where(x => x.QABundle.ProjectId == filters.ProjectId);
			}

			return query;
		}

		public IQueryable<vw_QABundlesTask> QATaskSearch(TaskSearch filters)
		{
			var query = _dbSubmission.vw_QABundlesTask.AsNoTracking().AsQueryable();

			if (filters.ProjectId != null)
			{
				query = query.Where(x => x.ProjectId == filters.ProjectId);
			}

			if (!string.IsNullOrEmpty(filters.ProjectName))
			{
				var projectId = _dbSubmission.trProjects
					.Where(x => x.ProjectName == filters.ProjectName)
					.Select(x => x.Id)
					.SingleOrDefault();

				query = query.Where(x => x.ProjectId == projectId);
			}

			if (!string.IsNullOrEmpty(filters.ManufacturerShort))
			{
				query = query.Where(x => x.Project.FileNumber.Contains(filters.ManufacturerShort)).AsQueryable();
			}

			if (filters.LaboratoryId != null)
			{
				query = query.Where(x => x.Project.LabId == filters.LaboratoryId).AsQueryable();
			}

			if (!string.IsNullOrEmpty(filters.FileNumber))
			{
				query = query.Where(x => x.Project.FileNumber.StartsWith(filters.FileNumber)).AsQueryable();
			}

			if (filters.ManagerId != null)
			{
				query = query.Where(x => x.Project.ENManagerId == filters.ManagerId);
			}

			if (!string.IsNullOrEmpty(filters.FileNumber))
			{
				query = query.Where(x => x.Project.FileNumber.StartsWith(filters.FileNumber));
			}

			if (filters.QATaskViewStatus != null)
			{
				if (filters.QATaskViewStatus == QATaskStatusFilter.Closed)
				{
					query = query.Where(x => x.QATaskStatus == filters.QATaskViewStatus.ToString());
				}
				else
				{
					if (filters.QATaskViewStatus == QATaskStatusFilter.Open)
					{
						query = query.Where(x => (x.QATaskStatus == QATaskStatusFilter.NotStarted.ToString() || x.QATaskStatus == QATaskStatusFilter.InProgress.ToString() || x.QATaskStatus == QATaskStatusFilter.Finalization.ToString() || x.QATaskStatus == QATaskStatusFilter.EngReReview.ToString() || x.QATaskStatus == QATaskStatusFilter.OnHold.ToString() || x.QATaskStatus == QATaskStatusFilter.ClosedIR.ToString()) && x.BundleStatus != BundleStatus.DN.ToString());
					}
					else
					{
						query = query.Where(x => (x.QATaskStatus == filters.QATaskViewStatus.ToString() && x.BundleStatus != BundleStatus.DN.ToString()));
					}
				}
			}

			if (filters.StartAssignDate != null || filters.EndAssignDate != null || filters.RollingAssignDateDays != null)
			{
				var startAssignDate = (filters.StartAssignDate == null) ? (DateTime)SqlDateTime.MinValue : Convert.ToDateTime(filters.StartAssignDate).Date;
				startAssignDate = (filters.RollingAssignDateDays == null) ? startAssignDate : DateTime.Today.AddDays((int)filters.RollingAssignDateDays * -1);
				var endAssignDate = (filters.EndAssignDate == null) ? (DateTime)SqlDateTime.MaxValue : Convert.ToDateTime(filters.EndAssignDate).Date.AddDays(1).AddTicks(-1);
				endAssignDate = (filters.RollingAssignDateDays == null) ? endAssignDate : _endOfDayToday;

				query = query.Where(x => x.AssignDate >= startAssignDate && x.AssignDate <= endAssignDate);
			}

			if (filters.StartStartDate != null || filters.EndStartDate != null || filters.RollingStartDateDays != null)
			{
				var startStartDate = (filters.StartStartDate == null) ? (DateTime)SqlDateTime.MinValue : Convert.ToDateTime(filters.StartStartDate).Date;
				startStartDate = (filters.RollingStartDateDays == null) ? startStartDate : DateTime.Today.AddDays((int)filters.RollingStartDateDays * -1);
				var endStartDate = (filters.EndStartDate == null) ? (DateTime)SqlDateTime.MaxValue : Convert.ToDateTime(filters.EndStartDate).Date.AddDays(1).AddTicks(-1);
				endStartDate = (filters.RollingStartDateDays == null) ? endStartDate : _endOfDayToday;

				query = query.Where(x => x.StartDate >= startStartDate && x.StartDate <= endStartDate);
			}

			if (filters.StartTargetDate != null || filters.EndTargetDate != null || filters.RollingTargetDateDays != null)
			{
				var startTargetDate = (filters.StartTargetDate == null) ? (DateTime)SqlDateTime.MinValue : Convert.ToDateTime(filters.StartTargetDate).Date;
				startTargetDate = (filters.RollingTargetDateDays == null) ? startTargetDate : DateTime.Today.AddDays((int)filters.RollingTargetDateDays * -1);
				var endTargetDate = (filters.EndTargetDate == null) ? (DateTime)SqlDateTime.MaxValue : Convert.ToDateTime(filters.EndTargetDate).Date.AddDays(1).AddTicks(-1);
				endTargetDate = (filters.RollingTargetDateDays == null) ? endTargetDate : _endOfDayToday;

				query = query.Where(x => x.TargetDate >= startTargetDate && x.TargetDate <= endTargetDate);
			}

			if (filters.StartCompleteDate != null || filters.EndCompleteDate != null || filters.RollingCompleteDateDays != null)
			{
				var startCompleteDate = (filters.StartCompleteDate == null) ? (DateTime)SqlDateTime.MinValue : Convert.ToDateTime(filters.StartCompleteDate).Date;
				startCompleteDate = (filters.RollingCompleteDateDays == null) ? startCompleteDate : DateTime.Today.AddDays((int)filters.RollingCompleteDateDays * -1);
				var endCompleteDate = (filters.EndCompleteDate == null) ? (DateTime)SqlDateTime.MaxValue : Convert.ToDateTime(filters.EndCompleteDate).Date.AddDays(1).AddTicks(-1);
				endCompleteDate = (filters.RollingCompleteDateDays == null) ? endCompleteDate : _endOfDayToday;

				query = query.Where(x => x.CompleteDate >= startCompleteDate && x.CompleteDate <= endCompleteDate);
			}

			// finds anything that's within a start and end date
			if (filters.StartEncapsulateDate != null || filters.EndEncapsulateDate != null)
			{
				var startEncapsulateDate = (filters.StartEncapsulateDate == null) ? (DateTime)SqlDateTime.MinValue : Convert.ToDateTime(filters.StartEncapsulateDate).Date;
				var endEncapsulateDate = (filters.EndEncapsulateDate == null) ? (DateTime)SqlDateTime.MaxValue : Convert.ToDateTime(filters.EndEncapsulateDate).Date;

				query = query.Where(x =>
					(x.StartDate >= startEncapsulateDate && x.StartDate <= endEncapsulateDate) ||
					(x.EstimateEndDate >= startEncapsulateDate && x.EstimateEndDate <= endEncapsulateDate) ||
					(x.StartDate <= startEncapsulateDate && x.EstimateEndDate >= endEncapsulateDate));
			}

			if (filters.UserId != null)
			{
				query = query.Where(x => x.UserId == filters.UserId);
			}

			if (filters.IsTransfer == ProjectType.Projects)
			{
				query = query.Where(x => x.Project.IsTransfer == false);
			}
			else if (filters.IsTransfer == ProjectType.Transfers)
			{
				query = query.Where(x => x.Project.IsTransfer == true);
			}

			return query;
		}

		public IQueryable<Submission> SubmissionSearch(SubmissionSearch filters)
		{
			var query = _dbSubmission.Submissions.AsNoTracking().AsQueryable();
			query = query.SubmissionSearch(filters);

			return query;
		}

		public IQueryable<trSubProject> SubProjectSearch(SubProjectSearch filters)
		{
			var query = _dbSubmission.trSubProject.AsNoTracking().AsQueryable();

			if (filters.ProjectId != null)
			{
				query = query.Where(x => x.ProjectId == filters.ProjectId);
			}

			if (filters.TeamIds != null && filters.TeamIds.Count > 0)
			{
				query = query.Where(x => filters.TeamIds.Contains(x.TypeId));
			}

			if (filters.AssigneeIds != null && filters.AssigneeIds.Count > 0)
			{
				query = query.Where(x => x.Tasks.Any(y => filters.AssigneeIds.Contains(y.UserId ?? 0)));
			}

			if (filters.Statuses != null && filters.Statuses.Count > 0)
			{
				var completed = SubProjectStatus.Closed.ToString();
				var notStarted = SubProjectStatus.NotStarted.ToString();
				var inProgress = SubProjectStatus.InProgress.ToString();
				var unassigned = SubProjectStatus.Unassigned.ToString();

				query = query.Where(x => 1 == (
					filters.Statuses.Contains(completed) && x.Tasks.Any(y => y.CompleteDate != null) ? 1 :
					filters.Statuses.Contains(notStarted) && x.Tasks.Any(y => y.StartDate == null && y.UserId != null) ? 1 :
					filters.Statuses.Contains(inProgress) && x.Tasks.Any(y => y.StartDate != null && y.CompleteDate == null) ? 1 :
					filters.Statuses.Contains(unassigned) && !x.Tasks.Any() ? 1 :
					0
				));
			}

			return query;
		}

		public IQueryable<vw_trTask> TaskSearch(TaskSearch filters)
		{
			var query = _dbSubmission.vw_trTask.AsNoTracking().AsQueryable();

			if (filters.ProjectId != null)
			{
				query = query.Where(x => x.ProjectId == filters.ProjectId);
			}

			if (!string.IsNullOrEmpty(filters.ProjectName))
			{
				var projectId = _dbSubmission.trProjects
					.Where(x => x.ProjectName == filters.ProjectName)
					.Select(x => x.Id)
					.SingleOrDefault();

				query = query.Where(x => x.ProjectId == projectId);
			}

			if (!string.IsNullOrEmpty(filters.ManufacturerShort))
			{
				query = query.Where(x => x.Project.FileNumber.Contains(filters.ManufacturerShort)).AsQueryable();
			}

			if (filters.LaboratoryId != null)
			{
				query = query.Where(x => x.Project.LabId == filters.LaboratoryId).AsQueryable();
			}

			if (filters.LaboratoryIds != null && filters.LaboratoryIds.Count > 0)
			{
				query = query.Where(x => filters.LaboratoryIds.Contains(x.Project.LabId.ToString()));
			}

			if (!string.IsNullOrEmpty(filters.FileNumber))
			{
				query = query.Where(x => x.Project.FileNumber.StartsWith(filters.FileNumber)).AsQueryable();
			}

			if (filters.ManagerId != null)
			{
				query = query.Where(x => x.Project.ENManagerId == filters.ManagerId);
			}
			if (filters.MAManagerId != null)
			{
				query = query.Where(x => x.Project.MAManagerId == filters.MAManagerId);
			}

			if (!string.IsNullOrEmpty(filters.FileNumber))
			{
				query = query.Where(x => x.Project.FileNumber.StartsWith(filters.FileNumber));
			}

			if (filters.Status.HasValue)
			{
				if (filters.Status == TaskStatusFilter.Open)
				{
					var openTaskStatuses = ComUtil.OpenTaskStatuses().Select(x => x.ToString()).ToList();
					query = query.Where(x => openTaskStatuses.Contains(x.TaskStatus) && x.Project.ENStatus != ProjectStatus.DN.ToString());
				}
				else
				{
					if (filters.Status == TaskStatusFilter.Closed)
					{
						query = query.Where(x => x.TaskStatus == filters.Status.ToString());
					}
					else
					{
						query = query.Where(x => x.TaskStatus == filters.Status.ToString() && x.Project.ENStatus != ProjectStatus.DN.ToString());
					}
				}
			}

			if (filters.StartAssignDate != null || filters.EndAssignDate != null || filters.RollingAssignDateDays != null)
			{
				var startAssignDate = (filters.StartAssignDate == null) ? (DateTime)SqlDateTime.MinValue : Convert.ToDateTime(filters.StartAssignDate).Date;
				startAssignDate = (filters.RollingAssignDateDays == null) ? startAssignDate : DateTime.Today.AddDays((int)filters.RollingAssignDateDays * -1);
				var endAssignDate = (filters.EndAssignDate == null) ? (DateTime)SqlDateTime.MaxValue : Convert.ToDateTime(filters.EndAssignDate).Date.AddDays(1).AddTicks(-1);
				endAssignDate = (filters.RollingAssignDateDays == null) ? endAssignDate : _endOfDayToday;

				query = query.Where(x => x.AssignDate >= startAssignDate && x.AssignDate <= endAssignDate);
			}

			if (filters.StartStartDate != null || filters.EndStartDate != null || filters.RollingStartDateDays != null)
			{
				var startStartDate = (filters.StartStartDate == null) ? (DateTime)SqlDateTime.MinValue : Convert.ToDateTime(filters.StartStartDate).Date;
				startStartDate = (filters.RollingStartDateDays == null) ? startStartDate : DateTime.Today.AddDays((int)filters.RollingStartDateDays * -1);
				var endStartDate = (filters.EndStartDate == null) ? (DateTime)SqlDateTime.MaxValue : Convert.ToDateTime(filters.EndStartDate).Date.AddDays(1).AddTicks(-1);
				endStartDate = (filters.RollingStartDateDays == null) ? endStartDate : _endOfDayToday;

				query = query.Where(x => x.StartDate >= startStartDate && x.StartDate <= endStartDate);
			}

			if (filters.StartTargetDate != null || filters.EndTargetDate != null || filters.RollingTargetDateDays != null)
			{
				var startTargetDate = (filters.StartTargetDate == null) ? (DateTime)SqlDateTime.MinValue : Convert.ToDateTime(filters.StartTargetDate).Date;
				startTargetDate = (filters.RollingTargetDateDays == null) ? startTargetDate : DateTime.Today.AddDays((int)filters.RollingTargetDateDays * -1);
				var endTargetDate = (filters.EndTargetDate == null) ? (DateTime)SqlDateTime.MaxValue : Convert.ToDateTime(filters.EndTargetDate).Date.AddDays(1).AddTicks(-1);
				endTargetDate = (filters.RollingTargetDateDays == null) ? endTargetDate : _endOfDayToday;

				query = query.Where(x => x.TargetDate >= startTargetDate && x.TargetDate <= endTargetDate);
			}

			if (filters.StartCompleteDate != null || filters.EndCompleteDate != null || filters.RollingCompleteDateDays != null)
			{
				var startCompleteDate = (filters.StartCompleteDate == null) ? (DateTime)SqlDateTime.MinValue : Convert.ToDateTime(filters.StartCompleteDate).Date;
				startCompleteDate = (filters.RollingCompleteDateDays == null) ? startCompleteDate : DateTime.Today.AddDays((int)filters.RollingCompleteDateDays * -1);
				var endCompleteDate = (filters.EndCompleteDate == null) ? (DateTime)SqlDateTime.MaxValue : Convert.ToDateTime(filters.EndCompleteDate).Date.AddDays(1).AddTicks(-1);
				endCompleteDate = (filters.RollingCompleteDateDays == null) ? endCompleteDate : _endOfDayToday;

				query = query.Where(x => x.CompleteDate >= startCompleteDate && x.CompleteDate <= endCompleteDate);
			}

			// finds anything that's within a start and end date
			if (filters.StartEncapsulateDate != null || filters.EndEncapsulateDate != null)
			{
				var startEncapsulateDate = (filters.StartEncapsulateDate == null) ? (DateTime)SqlDateTime.MinValue : Convert.ToDateTime(filters.StartEncapsulateDate).Date;
				var endEncapsulateDate = (filters.EndEncapsulateDate == null) ? (DateTime)SqlDateTime.MaxValue : Convert.ToDateTime(filters.EndEncapsulateDate).Date;

				query = query.Where(x =>
					(x.StartDate >= startEncapsulateDate && x.StartDate <= endEncapsulateDate) ||
					(x.EstimateEndDate >= startEncapsulateDate && x.EstimateEndDate <= endEncapsulateDate) ||
					(x.StartDate <= startEncapsulateDate && x.EstimateEndDate >= endEncapsulateDate));
			}

			if (filters.UserId != null)
			{
				query = query.Where(x => x.UserId == filters.UserId);
			}

			if (filters.DepartmentId != null)
			{
				query = query.Where(x => x.DepartmentId == filters.DepartmentId);
			}

			if (filters.IsTransfer == ProjectType.Projects)
			{
				query = query.Where(x => x.Project.IsTransfer == false);
			}
			else if (filters.IsTransfer == ProjectType.Transfers)
			{
				query = query.Where(x => x.Project.IsTransfer == true);
			}

			if (filters.IsRush == true)
			{
				query = query.Where(x => x.IsRush == 1);
			}

			if (filters.ManufacturerGroupIds != null && filters.ManufacturerGroupIds.Count > 0)
			{
				var manufs = _dbSubmission.ManufacturerGroups.Where(x => filters.ManufacturerGroupIds.Contains(x.Id))
					.SelectMany(x => x.ManufacturerLinks.Select(y => y.ManufacturerCode)).ToList();

				if (filters.ExcludeMathQueueGroups)
				{
					query = query.Where(x => !manufs.Any(y => x.Project.FileNumber.Contains(y)));
				}
				else
				{
					query = query.Where(x => manufs.Any(y => x.Project.FileNumber.Contains(y)));
				}
			}

			if (filters.ShowETAQueue)
			{
				query = query.Where(x => x.ETAQueue && x.UserId == null);
			}

			return query;
		}

		public IQueryable<trTaskTemplate> TaskTemplateSearch()
		{
			var query = _dbSubmission.trTaskTemplates.AsNoTracking().AsQueryable();

			return query;
		}
		public IQueryable<tbl_lst_TechnicalStandards> TechnicalStandardsSearch()
		{
			var query = _dbSubmission.tbl_lst_TechnicalStandards
				.Include(tech => tech.Jurisdictions)
				.AsNoTracking().AsQueryable();

			return query;
		}

		public IQueryable<JurisdictionalData> TransfersSearch(ProjectJurisdictionItemSearch filters)
		{
			var query = _dbSubmission.JurisdictionalData.AsQueryable();

			if (filters.ProjectIds.Count > 0) //Gives all jurs for Project to Exclude and Create Transfer
			{
				query = query.Where(x => x.JurisdictionalDataProjects.Any(p => filters.ProjectIds.Contains(p.ProjectId))).AsQueryable();
			}

			if (!string.IsNullOrEmpty(filters.FileNumber))
			{
				query = query.Where(x => x.Submission.FileNumber == filters.FileNumber).AsQueryable();
			}

			if (!string.IsNullOrEmpty(filters.ManufacturerShort))
			{
				query = query.Where(x => x.Submission.ManufacturerCode == filters.ManufacturerShort).AsQueryable();
			}

			if (!string.IsNullOrWhiteSpace(filters.Jurisdiction))
			{
				query = query.Where(x => x.JurisdictionId == filters.Jurisdiction).AsQueryable();
			}

			if (filters.IsTransfer == true) //Actual Transfer available for bundling
			{
				query = query.Where(x => x.TransferAvailable == true && (x.Status == JurisdictionStatus.RA.ToString() || x.Status == JurisdictionStatus.CP.ToString() || x.Status == JurisdictionStatus.OH.ToString())).AsQueryable();
			}
			else if (filters.ProjectTestingType == BundleType.OriginalTesting)
			{
				query = query.Where(x => (x.Status == JurisdictionStatus.RA.ToString() || x.Status == JurisdictionStatus.CP.ToString() || x.Status == JurisdictionStatus.OH.ToString()) && (x.TransferCreated == false || x.TransferCreated == null)).AsQueryable();
			}
			else if (filters.ProjectTestingType == BundleType.ClosedTesting)
			{
				query = query.Where(x => (x.Status != JurisdictionStatus.RA.ToString() && x.Status != JurisdictionStatus.CP.ToString() && x.Status != JurisdictionStatus.OH.ToString())).AsQueryable();
				query = query.Where(x => !(x.JurisdictionalDataProjects.Any(y => y.Project.ENStatus != ProjectStatus.DN.ToString()))).AsQueryable();
			}

			if (filters.LocationId != null)
			{
				query = query.Where(x => x.Lab.Id == filters.LocationId).AsQueryable();
			}

			return query;
		}

		public IQueryable<trLogin> UserSearch(UserSearch filters)
		{
			var query = _dbSubmission.trLogins.AsNoTracking().AsQueryable();

			if (filters.ManagerId != null)
			{
				query = query.Where(x => x.ManagerId == filters.ManagerId);
			}
			if (filters.TeamId != null)
			{
				if (filters.NotInTeam)
				{
					query = query.Where(x => x.TeamId != filters.TeamId);
				}
				else
				{
					query = query.Where(x => x.TeamId == filters.TeamId);
				}
			}
			if (filters.SupplierFocusId != null)
			{
				if (filters.NotFocusedOnSupplier)
				{
					query = query.Where(x => x.SupplierFocusId != filters.SupplierFocusId);
				}
				else
				{
					query = query.Where(x => x.SupplierFocusId == filters.SupplierFocusId);
				}
			}
			if (filters.BusinessOwnerId != null)
			{
				if (filters.NotInBussinessOwner)
				{
					query = query.Where(x => x.BusinessOwnerId != filters.BusinessOwnerId);
				}
				else
				{
					query = query.Where(x => x.BusinessOwnerId == filters.BusinessOwnerId);
				}
			}

			return query;
		}
		#endregion
	}
}