﻿using EF.Submission;
using Submissions.Core.Models.ProjectService;
using Submissions.Web.Models.LookupsAjax;
using Submissions.Web.Models.Query;
using Submissions.Web.Models.QueryService;
using System.Collections.Generic;
using System.Linq;

namespace Submissions.Web
{
	public interface IQueryService
	{
		IQueryable<Submission> AdvancedSearch(AdvancedSearchSearch filters);
		IQueryable<changeOfStatusMaster> ChangeOfStatusSearch(string fileNumber);
		IQueryable<tbl_lst_GamingGuidelines> GamingGuidelineSearch(GamingGuidelineSearch filters);
		IQueryable<GamingGuidelineSportsBetting> GamingGuidelineSportsBettingSearch(GamingGuidelinesSportsBettingSearch filters);
		IQueryable<JiraItem> GetJiraIssues(int projectId);
		IQueryable<UnassignedTaskHoursDetail> GetBPData(List<int> ProjectIds);
		IQueryable<JurisdictionalData> JurisdictionalDataSearch(JurisdictionalDataSearch filters);
		IQueryable<tbl_LetterBook> LetterBookSearch(LetterBookSearch filters);
		IQueryable<tbl_lst_LetterContacts> LetterContactsSearch(LetterContactsSearch filters);
		IQueryable<ManufacturerGroups> ManufacturerGroupSearch(ManufacturerGroupSearch filters);
		IQueryable<trMathComplete> MathCompleteSearch(MathSearch filters);
		IQueryable<trMathSubmit> MathReviewSearch(MathSearch filters);
		IQueryable<vw_ProjectNotes> ProjectAllNoteSearch(ProjectNoteSearch filters);
		IQueryable<AssociatedProject> ProjectAssociation(int projectId);
		IQueryable<trHistory> ProjectHistoryLogSearch(ProjectHistoryLogSearch filters);
		IQueryable<trProjectJurisdictionalData> ProjectJurisdictionItemSearch(ProjectJurisdictionItemSearch filters);
		IQueryable<ProjectMetrics> ProjectMetricsSearch(ProjectMetricsSearch filters);
		IQueryable<trProjectNotes> ProjectNoteSearch(ProjectNoteSearch filters);
		IQueryable<trOH_History> ProjectOHHistory(int projectId);
		IQueryable<tbl_Reviews> ProjectReviewSearch(ProjectReviewSearch filters);
		IQueryable<Resubmission> ProjectResubmission(int projectId);
		IQueryable<vw_trProject> ProjectSearch(ProjectSearch filters);
		IQueryable<trTargetDate> ProjectTargetDateHistory(int projectId);
		IQueryable<UnassignedTaskHoursDetail> ProjectUnassignedTaskHours(int projectId);
		IQueryable<QABundles_History> QABundleHistoryLogSearch(ProjectHistoryLogSearch filters);
		IQueryable<vw_QABundlesTask> QATaskSearch(TaskSearch filters);
		IQueryable<Submission> SubmissionSearch(SubmissionSearch filters);
		IQueryable<EF.GLIAccess.electronicSubmissionMaster> SubmitRequestSearch(SubmitRequestSearch filters);
		IQueryable<trSubProject> SubProjectSearch(SubProjectSearch filters);
		IQueryable<vw_trTask> TaskSearch(TaskSearch filters);
		IQueryable<trTaskTemplate> TaskTemplateSearch();
		IQueryable<tbl_lst_TechnicalStandards> TechnicalStandardsSearch();
		IQueryable<EF.GLIAccess.TransferRequestItem> TransferRequestDetailSearch(TransferRequestDetailSearch filters);
		IQueryable<EF.GLIAccess.TransferRequestHeader> TransferRequestSearch(TransferRequestSearch filters);
		IQueryable<JurisdictionalData> TransfersSearch(ProjectJurisdictionItemSearch filters);
		IQueryable<trLogin> UserSearch(UserSearch filters);
		IQueryable<EF.GLIAccess.WithdrawalRequest> WithdrawRequestSearch(WithdrawRequestSearch filters);
	}
}